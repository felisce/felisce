# Include extension to FELiScE

# ------------------------------------------------------ #
# ------- CurveGenerator ------------------------------- #
# ------------------------------------------------------ #
if(FELISCE_WITH_CURVEGEN)
    message(STATUS "[CurveGenerator] FELiScE will be compiled with CurveGenerator extension.")
    # Add subdirectories
    add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/CurveGenerator")
    # Add required libraries
    set(FELISCE_WITH_LUA ON)
else()
    message(STATUS "[CurveGenerator] FELiScE will be compiled without CurveGenerator extension.")
endif()


# ------------------------------------------------------ #
# ------- FELiScE-NS ----------------------------------- #
# ------------------------------------------------------ #
if(FELISCE_WITH_NSINRIA)
    message(STATUS "[FELiScE-NS] FELiScE will be compiled with FELiScE-NS extension.")
    # Check provided path
    check_path_exists(NSINRIA_DIR)
    # Add subdirectories
    add_subdirectory("${NSINRIA_DIR}/src" "${CMAKE_BINARY_DIR}/src-ns")
else()
    message(STATUS "[FELiScE-NS] FELiScE will be compiled without FELiScE-NS extension.")
endif()