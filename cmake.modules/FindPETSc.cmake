# Find the PETSc includes and libraries
#
# PETSc, the Portable, Extensible Toolkit for Scientific Computation, pronounced PET-see (/ˈpɛt-siː/), is for the scalable (parallel) solution of scientific applications modeled by partial differential equations (PDEs).
# It can be found at:
# 	https://petsc.org/release/
#
# PETSC_INCLUDE_DIR - where to find includes files
# PETSC_LIBRARIES   - list of fully qualified libraries to link against.

# Check if PETSC_DIR is provided
if(PETSC_DIR)

    if(PETSC_ARCH)
        set(PETSC_ARCH_DIR "${PETSC_DIR}/${PETSC_ARCH}")
    else()
        set(PETSC_ARCH_DIR "${PETSC_DIR}")        
    endif()
    
    # Print message for the currect case
    message(STATUS "[PETSc Detection] Searching for PETSc in the user-provided directory: ${PETSC_ARCH_DIR}")
    
    # Define error message
    set(ERROR_MSG "Please verify your setup:\nFor 'out-of-place' installations (using --prefix during PETSc configuration):\n\tSet PETSC_DIR to the installation path specified in --prefix.\nFor 'in-place' installations (inside the PETSc source tree):\n\tSet PETSC_DIR to the source tree path.\n\tAdditionally, define PETSC_ARCH (e.g., arch-darwin-c-opt or arch-linux-c-debug).")
else()

    set(PETSC_ARCH     "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(PETSC_ARCH_DIR "${CMAKE_SYSTEM_PREFIX_PATH}")
    
    # Print message for the currect case
    message(STATUS "[PETSc Detection] Searching for PETSc in system directories.")

    # Define error message
    set(ERROR_MSG "")
endif()

# Find PETSc include directory
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscao.h"     OUT PETSC_INCLUDE_DIR_AO   ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscerror.h"  OUT PETSC_INCLUDE_DIR_ERR  ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscis.h"     OUT PETSC_INCLUDE_DIR_IS   ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscksp.h"    OUT PETSC_INCLUDE_DIR_KSP  ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscmat.h"    OUT PETSC_INCLUDE_DIR_MAT  ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscpc.h"     OUT PETSC_INCLUDE_DIR_PC   ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscsnes.h"   OUT PETSC_INCLUDE_DIR_SNES ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscsys.h"    OUT PETSC_INCLUDE_DIR_SYS  ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscvec.h"    OUT PETSC_INCLUDE_DIR_VEC  ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "PETSc" DIRS "${PETSC_DIR};${PETSC_ARCH_DIR}" INC "petscviewer.h" OUT PETSC_INCLUDE_DIR_VIEW ERR_MSG "${ERROR_MSG}")

# Create PETSc include directories list
set(PETSC_INCLUDE_DIR ${PETSC_INCLUDE_DIR_AO}
                      ${PETSC_INCLUDE_DIR_ERR}
                      ${PETSC_INCLUDE_DIR_IS}
                      ${PETSC_INCLUDE_DIR_KSP}
                      ${PETSC_INCLUDE_DIR_MAT}
                      ${PETSC_INCLUDE_DIR_PC}
                      ${PETSC_INCLUDE_DIR_SNES}
                      ${PETSC_INCLUDE_DIR_SYS}
                      ${PETSC_INCLUDE_DIR_VEC}
                      ${PETSC_INCLUDE_DIR_VIEW}
)

# Find PETSc library
felisce_find_library(NAME "PETSc" DIRS "${PETSC_ARCH_DIR}" LIB "petsc" OUT PETSC_LIBRARIES OPT "" ERR_MSG ${ERROR_MSG})

# Output message
message(STATUS "[PETSc Detection] PETSc successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${PETSC_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS PETSC_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()


# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${PETSC_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${PETSC_LIBRARIES})


# ------------------------------------------------------ #
# ------- ScaLAPACK ------------------------------------ #
# ------------------------------------------------------ #
include(FindScaLAPACK)


# ------------------------------------------------------ #
# ------- MUMPS ---------------------------------------- #
# ------------------------------------------------------ #
include(FindMUMPS)


# ------------------------------------------------------ #
# ------- Metis ---------------------------------------- #
# ------------------------------------------------------ #
include(FindMETIS)


# ------------------------------------------------------ #
# ------- ParMetis ------------------------------------- #
# ------------------------------------------------------ #
include(FindPARMETIS)


# ------------------------------------------------------ #
# ------- SuperLU_DIST --------------------------------- #
# ------------------------------------------------------ #
include(FindSuperLUDist)


# ------------------------------------------------------ #
# ------- Hypre ---------------------------------------- #
# ------------------------------------------------------ #
include(FindHypre)


# ------------------------------------------------------ #
# ------- SLEPc ---------------------------------------- #
# ------------------------------------------------------ #
include(FindSLEPc)