# This CMake file manages the inclusion of subdirectories and the inclusion of 
# modules for locating third-party packages. It leverages the interface libraries 
# `FELiScE_dep_includes` and `FELiScE_dep_libraries` to store the necessary include 
# directories and libraries for the project. These interface libraries are used during 
# the compilation of the project library and executables, ensuring that all external 
# dependencies are properly linked and available for the build process.

# Create the interface library for dependency includes
add_library(FELiScE_dep_includes INTERFACE)

# Interface library for dependency libraries
add_library(FELiScE_dep_libraries INTERFACE)


# ------------------------------------------------------ #
# ------- GetPot --------------------------------------- #
# ------------------------------------------------------ #
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/getpot-cpp")


# ------------------------------------------------------ #
# ------- Intrusive pointer ---------------------------- #
# ------------------------------------------------------ #
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/intrusive_ptr")


# ------------------------------------------------------ #
# ------- LibMeshb ------------------------------------- #
# ------------------------------------------------------ #
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/libMeshb")


# ------------------------------------------------------ #
# ------- Tabulate ------------------------------------- #
# ------------------------------------------------------ #
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/tabulate")


# ------------------------------------------------------ #
# ------- BitMap --------------------------------------- #
# ------------------------------------------------------ #
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/bitmap")


# ------------------------------------------------------ #
# ------- SciPlot -------------------------------------- #
# ------------------------------------------------------ #
add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/sciplot")


# ------------------------------------------------------ #
# ------- Boost ---------------------------------------- #
# ------------------------------------------------------ #
include(FindBoostLibrary)


# ------------------------------------------------------ #
# ------- OpenMPI -------------------------------------- #
# ------------------------------------------------------ #
include(FindMPILibrary)


# ------------------------------------------------------ #
# ------- PETSc ---------------------------------------- #
# ------------------------------------------------------ #
include(FindPETSc)


# ------------------------------------------------------ #
# ------- libXfm --------------------------------------- #
# ------------------------------------------------------ #
include(FindlibXfm)


# ------------------------------------------------------ #
# ------- PVM ------------------------------------------ #
# ------------------------------------------------------ #
include(FindPVM)


# ------------------------------------------------------ #
# ------- ZeroMQ --------------------------------------- #
# ------------------------------------------------------ #
include(FindZeroMQ)


# ------------------------------------------------------ #
# ------- QT ------------------------------------------- #
# ------------------------------------------------------ #
include(FindQtLibrary)


# ------------------------------------------------------ #
# ------- CVGRAPH -------------------------------------- #
# ------------------------------------------------------ #
include(FindCVGRAPH)


# ------------------------------------------------------ #
# ------- LUA ------------------------------------------ #
# ------------------------------------------------------ #
include(FindLuaLibrary)


# ------------------------------------------------------ #
# ------- Testing -------------------------------------- #
# ------------------------------------------------------ #
if(FELISCE_WITH_TESTS)

    # Define an interface library for doctest headers
    add_library(FELiScE_doctests_includes INTERFACE)

    # Define an interface library for doctest libraries
    add_library(FELiScE_doctests_libraries INTERFACE)

    # Include the doctest library
    add_subdirectory("${CMAKE_CURRENT_SOURCE_DIR}/external/doctest")
    
endif()


# ------------------------------------------------------ #
# ------- External includes and libraries -------------- #
# ------------------------------------------------------ #
include(FindExternal)