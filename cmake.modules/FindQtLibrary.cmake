# Find QT includes and libraries
# 
# ZEROMQ_INCLUDE_DIR - where to find includes files
# ZEROMQ_LIBRARIES   - list of fully qualified libraries to link against.

# Check Qt if required
if(NOT FELISCE_WITH_QT)
    message(STATUS "[QT Detection] Compiling FELiScE without QT support. To enable QT, set the option FELISCE_WITH_QT to ON.")
    return()
endif()

# Find QT package
set(OLD_CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}) 
find_package(Qt6 COMPONENTS Core Widgets REQUIRED)
set(CMAKE_MODULE_PATH ${OLD_CMAKE_MODULE_PATH})

# Append include directories
set(QT_INCLUDE_DIRS "")
list(APPEND QT_INCLUDE_DIRS ${Qt6Core_INCLUDE_DIRS} ${Qt6Widgets_INCLUDE_DIRS})

# Append library targets
set(QT_LIBRARIES "")
list(APPEND QT_LIBRARIES Qt6::Core Qt6::Widgets)

# Output message
message(STATUS "[QT Detection] QT successfully found.")
message("\t\t\t\tIncludes: ${QT_INCLUDE_DIRS}")
message("\t\t\t\tLibraries:")
foreach(lib IN LISTS QT_LIBRARIES)
    message("\t\t\t\t\t\t${lib}") 
endforeach()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${QT_INCLUDE_DIRS}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${QT_LIBRARIES})