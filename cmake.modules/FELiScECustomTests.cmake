# Function to install a test executable
#
# This function simplifies the process of creating, linking, and installing a test
# executable. It parses input arguments, compiles the executable, links necessary
# libraries, and installs the binary to a specified directory.
#
# Arguments:
#   NAME    - The name of the test executable.
#   INSTALL - The directory name where the test will be installed, relative to
#             the CMake installation prefix.
#   SOURCES - A list of source files for compiling the executable.
#
# Example usage:
#   felisce_install_test_exe(NAME "test_example" INSTALL "example_tests" SOURCES "test_example.cpp")
function(felisce_install_test_exe)

    # Parse arguments
    cmake_parse_arguments(ARGS "" "NAME;INSTALL" "SOURCES" ${ARGN})    

    # Create the executable
    add_executable(${ARGS_NAME} ${ARGS_SOURCES})

    # Include necessary directories for the test
    target_include_directories(${ARGS_NAME} PRIVATE $<TARGET_PROPERTY:FELiScE,INTERFACE_INCLUDE_DIRECTORIES>
                                                    $<TARGET_PROPERTY:FELiScE_dep_includes,INTERFACE_INCLUDE_DIRECTORIES>
                                                    $<TARGET_PROPERTY:FELiScE_doctests_includes,INTERFACE_INCLUDE_DIRECTORIES>
    )

    # Link required libraries to the test
    target_link_libraries(${ARGS_NAME} PRIVATE  $<TARGET_PROPERTY:FELiScE_dep_libraries,INTERFACE_LINK_LIBRARIES>
                                                $<TARGET_PROPERTY:FELiScE_doctests_libraries,INTERFACE_LINK_LIBRARIES>
                                                FELiScE
    )

    # Generate install directory path
    set(install_dir_path "${CMAKE_INSTALL_PREFIX}/tests/${ARGS_INSTALL}")

    # Install the compiled test binary
    install(TARGETS ${ARGS_NAME} DESTINATION ${install_dir_path} COMPONENT tests)  

    message("\t- Compiled test executable: ${ARGS_NAME}")

endfunction()


# Function to install test-related data such as configuration files, meshes, and additional directories
#
# This function simplifies the process of copying and configuring test-related files and
# directories, including configuration files, data files, and meshes. It also handles the
# relative path generation when building for Continuous Integration (CI).
#
# Arguments:
#   NAME    - The name of the test.
#   EXEC    - The executable name of the test.
#   INSTALL - The directory name where the test data will be installed, relative to
#             the CMake installation prefix.
#   CONFIG  - List of configuration files to be processed and installed.
#   FILES   - List of files to be installed.
#   DIRS    - List of directories to be installed.
#
# Example usage:
#   felisce_install_tests_data(NAME "test_example" EXEC "test_exec" INSTALL "example_tests"
#                               CONFIG "config1.cfg" FILES "data1.txt" DIRS "mesh_folder")
function(felisce_install_tests_data)

    # Parse arguments
    cmake_parse_arguments(ARGS "" "NAME;EXEC;INSTALL" "CONFIG;FILES;DIRS" ${ARGN})  

    # Generate install directory path
    set(install_dir_path "${CMAKE_INSTALL_PREFIX}/tests/${ARGS_INSTALL}")

    # Set additional variables
    set(test_name "${ARGS_NAME}")
    set(exec_name "${ARGS_EXEC}")
    set(mesh_folder    "${CMAKE_INSTALL_PREFIX}/tests/${meshes_directory}")
    set(install_prefix "${CMAKE_INSTALL_PREFIX}")                 
    if(FELISCE_FOR_CI)
        file(RELATIVE_PATH mesh_folder    ${install_dir_path} "${mesh_folder}")
        file(RELATIVE_PATH install_prefix ${install_dir_path} "${install_prefix}")
    endif()

    # Handle CONFIG files: Configure files if provided
    file(MAKE_DIRECTORY ${install_dir_path})
    configure_file("${tool_directory}/${run_test_tool}" ${install_dir_path})
    if(${compare_tool} MATCHES "compare_results.py")
        configure_file("${tool_directory}/${compare_tool}" ${install_dir_path})
    endif()
    if(${prepare_tool} MATCHES "prepare_fsi.py")
        configure_file("${tool_directory}/${prepare_tool}" ${install_dir_path})
    endif()
    if(ARGS_CONFIG)
        foreach(config_file IN LISTS ARGS_CONFIG)
            configure_file(${config_file} ${install_dir_path})
        endforeach()
    endif()

    # Handle FILES: Copy files if provided
    if(ARGS_FILES)
        foreach(file IN LISTS ARGS_FILES)
            install(FILES ${file} DESTINATION ${install_dir_path})
        endforeach()
    endif()

    # Handle DIRS: Copy directories if provided
    if(ARGS_DIRS)
        foreach(dir IN LISTS ARGS_DIRS)
            install(DIRECTORY ${dir} DESTINATION ${install_dir_path})
        endforeach()
    endif()

    message("\t- Configured test: ${ARGS_NAME}")

endfunction()


# Function to add a test using CTest and configure the installation directory
#
# This function adds a test to the build system, using the CTest framework. It calculates
# the appropriate working directory for the test based on whether it is building for
# Continuous Integration (CI) and adds the test to the CTest suite.
#
# Arguments:
#   NAME    - The name of the test.
#   INSTALL - The directory name where the test data will be installed, relative to
#             the CMake installation prefix.
#
# Example usage:
#   felisce_add_test(NAME "example_test" INSTALL "example_tests")
function(felisce_add_test)
    
    # Parse arguments
    cmake_parse_arguments(ARGS "" "NAME;INSTALL" "" ${ARGN})  

    # Set the absolute installation directory
    set(abs_install_dir_path "${CMAKE_INSTALL_PREFIX}/tests/${ARGS_INSTALL}")

    # Calculate relative install folder for tests
    file(RELATIVE_PATH rel_install_dir_path "${CMAKE_BINARY_DIR}" ${abs_install_dir_path})

    # Use CTest to add the test
    add_test(NAME ${ARGS_NAME} 
             COMMAND ${Python_EXECUTABLE} ${run_test_tool} 
             WORKING_DIRECTORY $<IF:$<BOOL:${FELISCE_FOR_CI}>,${rel_install_dir_path},${abs_install_dir_path}> 
    )
    
endfunction()


# Function to add a Python test using CTest and configure the installation directory
#
# This function configures a Python test by copying the necessary Python script to the
# installation directory, setting up the working directory, and then adding the test 
# to CTest with the correct command and working directory. The test will run the Python 
# script using the specified Python executable.
#
# Arguments:
#   NAME    - The name of the test.
#   EXEC    - The Python script to be executed for the test.
#   INSTALL - The directory name where the test and related files will be installed, 
#             relative to the CMake installation prefix.
#
# Example usage:
#   felisce_add_python_test(NAME "python_test" EXEC "path/to/test_script.py" INSTALL "python_tests")
function(felisce_add_python_test)

    # Parse arguments
    cmake_parse_arguments(ARGS "" "NAME;EXEC;INSTALL" "" ${ARGN})  

    # Generate install directory path
    set(install_dir_path "${CMAKE_INSTALL_PREFIX}/tests/${ARGS_INSTALL}")

    # Set additional variables
    set(mesh_folder    "${CMAKE_INSTALL_PREFIX}/tests/${meshes_directory}")
    set(install_prefix "${CMAKE_INSTALL_PREFIX}")  
    set(binary_folder  "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}")   
    if(FELISCE_FOR_CI)
        file(RELATIVE_PATH mesh_folder    ${install_dir_path} "${mesh_folder}")
        file(RELATIVE_PATH install_prefix ${install_dir_path} "${install_prefix}")
        file(RELATIVE_PATH binary_folder  ${install_dir_path} "${binary_folder}")
    endif()

    # Configure test file
    file(MAKE_DIRECTORY ${install_dir_path})
    configure_file("${ARGS_EXEC}" "${install_dir_path}/${ARGS_NAME}.py")

    # Set the absolute installation directory
    set(abs_install_dir_path "${CMAKE_INSTALL_PREFIX}/tests/${ARGS_INSTALL}")

    # Calculate relative install folder for tests
    file(RELATIVE_PATH rel_install_dir_path "${CMAKE_BINARY_DIR}" ${abs_install_dir_path})

    # Use CTest to add the test
    add_test(NAME ${ARGS_NAME} 
             COMMAND ${Python_EXECUTABLE} "${ARGS_NAME}.py" 
             WORKING_DIRECTORY $<IF:$<BOOL:${FELISCE_FOR_CI}>,${rel_install_dir_path},${abs_install_dir_path}> 
    )

    message("\t- Configured test: ${ARGS_NAME}")

endfunction()