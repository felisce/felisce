# Find the libXfm includes and libraries
# 
# XFM_INCLUDE_DIR - where to find includes files
# XFM_LIBRARIES   - list of fully qualified libraries to link against.

# Check libXFM if required
if(NOT FELISCE_WITH_LIBXFM)
    message(STATUS "[LibXFM Detection] Compiling FELiScE without LibXFM support. To enable LibXFM, set the option FELISCE_WITH_LIBXFM to ON.")
    return()
endif()

if(LIBXFM_DIR)
    message(STATUS "[LibXFM Detection] Searching for LibXFM in the user-provided directory: ${LIBXFM_DIR}")
else()
    set(LIBXFM_DIR "${CMAKE_CURRENT_SOURCE_DIR}/external/Xfm")
    
    message(STATUS "[LibXFM Detection] Searching for LibXFM in the repository directory: ${LIBXFM_DIR}")

    if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
        
        DetectLinuxOSVersion(DISTRO_NAME DISTRO_VERSION)

        if(${DISTRO_NAME} MATCHES "Ubuntu")

            if(${DISTRO_VERSION} MATCHES "24.04")
                set(LIBXFM_VERSION "Ubuntu22")
            elseif (${DISTRO_VERSION} MATCHES "22.04")
                set(LIBXFM_VERSION "Ubuntu22")    
            elseif (${UBUNTU_VERSION} MATCHES "20.04")
                set(LIBXFM_VERSION "Ubuntu20")
            elseif (${UBUNTU_VERSION} MATCHES "20.04")
                set(LIBXFM_VERSION "Ubuntu16")
            else()
                message(FATAL_ERROR "[LibXFM Detection] The repository does not provide LibXFM for the release ${DISTRO_VERSION} of the operative system ${DISTRO_NAME}. Try to set LIBXFM_DIR to the LibXFM directory path.")
            endif()
        else()
            message(FATAL_ERROR "[LibXFM Detection] The repository does not provide LibXFM for operative system ${DISTRO_NAME}. Try to set LIBXFM_DIR to the LibXFM directory path.")
        endif()
        
    elseif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
        
        execute_process(COMMAND uname -m OUTPUT_VARIABLE MAC_ARCHITECTURE OUTPUT_STRIP_TRAILING_WHITESPACE)

        if(${MAC_ARCHITECTURE} MATCHES "x86_64")
            set(LIBXFM_VERSION "MacOSIntel")
        elseif(${MAC_ARCHITECTURE} MATCHES "arm64")
            set(LIBXFM_VERSION "MacOSSilicon")
        else()
            message(FATAL_ERROR "[LibXFM Detection] The repository does not provide LibXFM for the architecture ${MAC_ARCHITECTURE} of the operative system MacOS. Try to set LIBXFM_DIR to the LibXFM directory path.")
        endif()       
    else()
        message(FATAL_ERROR "[LibXFM Detection] The repository does not provide LibXFM for operative system ${CMAKE_SYSTEM_NAME}. Try to set LIBXFM_DIR to the LibXFM directory path.")
    endif()


endif()

# Search for the MPI include directory
felisce_find_include_dirs(NAME "LibXFM" DIRS "${LIBXFM_DIR}" INC "xfm_library.h" OUT XFM_INCLUDE_DIR ERR_MSG "")

# Find required MPI libraries (shared preferred, fallback to static)
felisce_find_library(NAME "LibXFM" DIRS "${LIBXFM_DIR}" LIB "Xfm" OUT XFM_LIBRARIES OPT ${LIBXFM_VERSION} ERR_MSG "")

# Output message
message(STATUS "[LibXFM Detection] LibXFM successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${XFM_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS XFM_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${XFM_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${XFM_LIBRARIES})

# Install library
install(FILES ${XFM_LIBRARIES} DESTINATION ${CMAKE_INSTALL_PREFIX}/lib COMPONENT libraries)