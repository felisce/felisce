# Find the HYPRE includes and libraries
# 
# HYPRE: Livermore’s hypre library of linear solvers makes possible larger, more detailed 
# simulations by solving problems faster than traditional methods at large scales. 
# It offers a comprehensive suite of scalable solvers for large-scale scientific simulation, 
# featuring parallel multigrid methods for both structured and unstructured grid problems. 
# The hypre library is highly portable and supports a number of languages.
# It can be found at:
#   https://computing.llnl.gov/projects/hypre-scalable-linear-solvers-multigrid-methods
# 
# HYPRE_INCLUDE_DIR - where to find includes files
# HYPRE_LIBRARIES   - list of fully qualified libraries to link against.

if(NOT FELISCE_WITH_HYPRE)
    message(STATUS "[HYPRE Detection] Compiling FELiScE without HYPRE support. To enable HYPRE, set the option FELISCE_WITH_HYPRE to ON.")
    return()
endif()

# Set HYPRE_DIR to PETSC_ARCH_DIR if not defined
set(HYPRE_DIR "${PETSC_ARCH_DIR}" CACHE STRING "HYPRE Directory")

if(HYPRE_DIR)
    message(STATUS "[HYPRE Detection] Searching for HYPRE in the user-provided directory: ${HYPRE_DIR}")
    set(INC_SEARCHING_PATHS "${HYPRE_DIR}")
    set(LIB_SEARCHING_PATHS "${HYPRE_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[HYPRE Detection] Searching for HYPRE in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting HYPRE_DIR to the root of your HYPRE installation.")
endif()

# Search for the HYPRE include directory
felisce_find_include_dirs(NAME "HYPRE" DIRS "${INC_SEARCHING_PATHS}" INC "HYPRE.h" OUT HYPRE_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Find required HYPRE libraries (shared preferred, fallback to static)
felisce_find_library(NAME "HYPRE" DIRS "${LIB_SEARCHING_PATHS}" LIB "HYPRE" OUT HYPRE_LIBRARIES OPT "" ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[HYPRE Detection] HYPRE successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${HYPRE_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS HYPRE_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${HYPRE_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${HYPRE_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)