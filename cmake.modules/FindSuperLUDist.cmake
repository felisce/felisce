# Find the SuperLU_DIST includes and libraries
#
# SuperLU_DIST contains a set of subroutines to solve a sparse linear system A*X=B. It uses Gaussian elimination 
# with static pivoting (GESP). Static pivoting is a technique that combines the numerical stability of partial pivoting 
# with the scalability of Cholesky (no pivoting), to run accurately and efficiently on large numbers of processors. 

# SuperLU_DIST is a parallel extension to the serial SuperLU library. It is targeted for the distributed memory parallel 
# machines. SuperLU_DIST is implemented in ANSI C, with OpenMP for on-node parallelism and MPI for off-node 
# communications. We are actively developing multi-GPU acceleration capabilities.
# It can be found at:
# 	https://github.com/xiaoyeli/superlu_dist
# 
# SUPERLU_DIST_INCLUDE_DIR - where to find includes files
# SUPERLU_DIST_LIBRARIES   - list of fully qualified libraries to link against.

if(NOT FELISCE_WITH_SUPERLU)
    message(STATUS "[SuperLU_DIST Detection] Compiling FELiScE without SuperLU_DIST support. To enable SuperLU_DIST, set the option FELISCE_WITH_SUPERLU to ON.")
    return()
endif()

# Set SUPERLU_DIST_DIR  to PETSC_ARCH_DIR if not defined
set(SUPERLU_DIST_DIR "${PETSC_ARCH_DIR}" CACHE STRING "SuperLU_DIST Directory")

if(SUPERLU_DIST_DIR)
    message(STATUS "[SuperLU_DIST Detection] Searching for SuperLU_DIST in the user-provided directory: ${SUPERLU_DIST_DIR}")
    set(INC_SEARCHING_PATHS "${SUPERLU_DIST_DIR}")
    set(LIB_SEARCHING_PATHS "${SUPERLU_DIST_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[SuperLU_DIST Detection] Searching for SuperLU_DIST in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting SUPERLU_DIST_DIR to the root of your SuperLU_DIST installation.")
endif()

# Search for the SuperLU_DIST include directory
felisce_find_include_dirs(NAME "SuperLU_DIST" DIRS "${INC_SEARCHING_PATHS}" INC "superlu_ddefs.h" OUT SUPERLU_DIST_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Find required SuperLU_DIST libraries (shared preferred, fallback to static)
felisce_find_library(NAME "SuperLU_DIST" DIRS ${LIB_SEARCHING_PATHS} LIB "superlu_dist" OUT SUPERLU_DIST_LIBRARIES OPT "" ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[SuperLU_DIST Detection] SuperLU_DIST successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${SUPERLU_DIST_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS SUPERLU_DIST_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${SUPERLU_DIST_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${SUPERLU_DIST_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)