if(NOT FELISCE_FOR_CPACK)
    return()
endif()

# ------------------------------------------------------ #
# ------- Change options ------------------------------- #
# ------------------------------------------------------ #
# ------- Extensions ----------------------------------- #
set(FELISCE_WITH_NSINRIA OFF)
# ------- External libraries --------------------------- #
set(FELISCE_WITH_CVGRAPH OFF)
set(FELISCE_WITH_LIBXFM  OFF)
set(FELISCE_WITH_PVM     OFF)
set(FELISCE_WITH_QT      OFF)
set(FELISCE_WITH_SCIPLOT OFF)
set(FELISCE_WITH_ZMQ  OFF)
# ------- Tests ---------------------------------------- #
set(FELISCE_WITH_TESTS   OFF)
# ------- Benchmarking & Examples ---------------------- #
set(FELISCE_WITH_BENCHMARKS OFF)
# ------- Avoid external inc, lib or exe --------------- #
set(EXTERNAL_EXECUTABLE_DIRS "")
set(EXTERNAL_LIBRARIES       "")
set(EXTERNAL_INCLUDE_DIRS    "")


# ------------------------------------------------------ #
# ------- Package metadata ----------------------------- #
# ------------------------------------------------------ #
set(CPACK_PACKAGE_NAME "${CMAKE_PROJECT_NAME}")
set(CPACK_PACKAGE_DESCRIPTION
    "FELiScE (Finite Elements for Life Sciences and Engineering) is a parallel finite element library written in C++.
    It was initiated by the Inria M3DISIM and REO project-teams and is currently developed by the COMMEDIA project-team.
    The library provides a unified software environment for the development of state-of-the-art finite element solvers for the spatial discretization of partial differential equations (PDE) in 1D, 2D, and 3D.
    Several finite element interpolations are available. The current version handles mainly globally continuous approximations.
    Interfacial discontinuities can be incorporated via the Nitsche-XFEM unfitted mesh framework.
    Local arrays and linear algebra operations are implemented with the Boost uBLAS C++ template class library.
    The global (parallel) data structure and the solvers are based on the PETSc package.")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY
    "FELiScE (Finite Elements for Life Sciences and Engineering) is a parallel finite element library written in C++.")


# ------------------------------------------------------ #
# ------- Vendor and License Information --------------- #
# ------------------------------------------------------ #
set(CPACK_PACKAGE_VENDOR "Institut National de Recherche en Informatique et en Automatique (INRIA)")
set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_PACKAGE_DESCRIPTION_FILE "${CMAKE_CURRENT_SOURCE_DIR}/README.md")


# ------------------------------------------------------ #
# ------- Package Versioning --------------------------- #
# ------------------------------------------------------ #
set(CPACK_PACKAGE_VERSION_MAJOR "${FELiScE_MAJOR_VERSION}")
set(CPACK_PACKAGE_VERSION_MINOR "${FELiScE_MINOR_VERSION}")
set(CPACK_PACKAGE_VERSION_PATCH "${FELiScE_PATCH_VERSION}")


# ------------------------------------------------------ #
# ------- Output and Installation Directories ---------- #
# ------------------------------------------------------ #
set(CPACK_PACKAGE_DIRECTORY "${CMAKE_SOURCE_DIR}/cpack")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "FELiScE-${FELiScE_MAJOR_VERSION}.${FELiScE_MINOR_VERSION}.${FELiScE_PATCH_VERSION}")


# ------------------------------------------------------ #
# ------- Define Components of the Package ------------- #
# ------------------------------------------------------ #
set(CPACK_COMPONENTS_ALL executables libraries headers)

# Set descriptions for each component
set(CPACK_COMPONENT_EXECUTABLES_DESCRIPTION "An executable that makes use of FELiScE")
set(CPACK_COMPONENT_LIBRARIES_DESCRIPTION   "Dynamic libraries used to build programs with FELiScE")
set(CPACK_COMPONENT_HEADERS_DESCRIPTION     "C/C++ header files for use with FELiScE")


# ------------------------------------------------------ #
# ------- Define Package file Name --------------------- #
# ------------------------------------------------------ #
if(CMAKE_SYSTEM_PROCESSOR MATCHES "x86_64")
    set(ARCH_SUFFIX "x86_64")
elseif(CMAKE_SYSTEM_PROCESSOR MATCHES "arm64")
    set(ARCH_SUFFIX "arm64")
else()
    set(ARCH_SUFFIX "unknown")
endif()

set(CPACK_PACKAGE_FILE_NAME "FELiScE-${FELiScE_MAJOR_VERSION}.${FELiScE_MINOR_VERSION}.${FELiScE_PATCH_VERSION}-${ARCH_SUFFIX}")


# ------------------------------------------------------ #
# ------- Platform-Specific Configuration -------------- #
# ------------------------------------------------------ #
if(${CMAKE_SYSTEM_NAME} MATCHES "Linux")
    set(CPACK_GENERATOR        "TGZ;ZIP;DEB")
    set(CPACK_SOURCE_GENERATOR "TGZ;ZIP;DEB")
elseif(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(CPACK_SOURCE_GENERATOR "TGZ;ZIP;DragNDrop")
    set(CPACK_GENERATOR "TGZ;ZIP;DragNDrop")
endif()


# ------------------------------------------------------ #
# ------- CPack Variable Handling ---------------------- #
# ------------------------------------------------------ #
set(CPACK_VERBATIM_VARIABLES ON)


# ------------------------------------------------------ #
# ------- Debian Package Configuration (Linux/DEB) ----- #
# ------------------------------------------------------ #
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Daniele Carlo Corti <daniele-carlo.corti@inria.fr>")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "build-essential;gfortran;libboost-all-dev;libopenmpi-dev;python3;libblas-dev;liblapack-dev;libzmq3-dev;liblua5.3-dev;cvgraph") # to check
set(CPACK_DEBIAN_PACKAGE_RECOMMENDS "python3-pip")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Daniele Carlo Corti <daniele-carlo.corti@inria.fr>")
set(CPACK_DEBIAN_PACKAGE_DEPENDS "build-essential;gfortran;libboost-all-dev;libopenmpi-dev;python3;libblas-dev;liblapack-dev;libzmq3-dev;liblua5.3-dev;cvgraph") # to check
set(CPACK_DEBIAN_PACKAGE_RECOMMENDS "python3-pip")


# ------------------------------------------------------ #
# ------- Include CPack -------------------------------- #
# ------------------------------------------------------ #
include(CPack)
