# Find ZeroMQ includes and libraries
# 
# ZeroMQ (also spelled ØMQ, 0MQ or ZMQ) is a high-performance asynchronous messaging library, aimed at use in distributed or concurrent applications. 
# It provides a message queue, but unlike message-oriented middleware, a ZeroMQ system can run without a dedicated message broker. 
# The library's API is designed to resemble Berkeley sockets.
# It can be found at:
#   https://zeromq.org
# 
# ZEROMQ_INCLUDE_DIR - where to find includes files
# ZEROMQ_LIBRARIES   - list of fully qualified libraries to link against.

# Check ZeroMQ if required
if(NOT FELISCE_WITH_ZMQ)
    message(STATUS "[ZeroMQ Detection] Compiling FELiScE without ZeroMQ support. To enable ZeroMQ, set the option FELISCE_WITH_ZeroMQ to ON.")
    return()
endif()
    
if(ZMQ_DIR)
    message(STATUS "[ZeroMQ Detection] Searching for ZeroMQ in the user-provided directory: ${ZMQ_DIR}")
    set(INC_SEARCHING_PATHS "${ZMQ_DIR}")
    set(LIB_SEARCHING_PATHS "${ZMQ_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[ZeroMQ Detection] Searching for ZeroMQ in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting ZMQ_DIR to the root of your ZeroMQ installation.")
endif()

if(ZMQ_BINDING_DIR)
    message(STATUS "[ZeroMQ Detection] Searching for ZeroMQ language binding in the user-provided directory: ${ZMQ_BINDING_DIR}")
    set(BIND_SEARCHING_PATHS "${ZMQ_BINDING_DIR}")
    set(ERROR_MSG_BIND "")
else()
    message(STATUS "[ZeroMQ Detection] Searching for ZeroMQ language binding in system directories.")
    set(BIND_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG_BIND "Please try setting ZMQ_BINDING_DIR to the root of your ZeroMQ binding installation.")
endif()

# Get optional parameter for felisce_find_library
set(OPT_DIR "")
if( ${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
    if( ${CMAKE_SYSTEM_PROCESSOR} MATCHES "x86_64" )
        set(OPT_DIR "x86_64-linux-gnu")
    elseif( ${CMAKE_SYSTEM_PROCESSOR} MATCHES "aarch64" )
        set(OPT_DIR "aarch64-linux-gnu")
    else()
        message(FATAL_ERROR "[ZeroMQ Detection] Architecture ${CMAKE_SYSTEM_PROCESSOR} not supported for ${CMAKE_SYSTEM_NAME} system.")
    endif()
endif()

# Search for the ZeroMQ include directory
felisce_find_include_dirs(NAME "ZeroMQ" DIRS "${INC_SEARCHING_PATHS}" INC "zmq.h" OUT ZEROMQ_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Search for the ZeroMQ binding include directory
felisce_find_include_dirs(NAME "ZeroMQ" DIRS "${BIND_SEARCHING_PATHS}" INC "zmq.hpp" OUT ZEROMQ_BINDING_INCLUDE_DIR ERR_MSG "${ERROR_MSG_BIND}")

# Find required ZeroMQ libraries (shared preferred, fallback to static)
felisce_find_library(NAME "ZeroMQ" DIRS ${LIB_SEARCHING_PATHS} LIB "zmq" OUT ZEROMQ_LIBRARIES OPT ${OPT_DIR} ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[ZeroMQ Detection] ZeroMQ successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${ZEROMQ_INCLUDE_DIR}")
    message("\t\t\t\tBinding:  ${ZEROMQ_BINDING_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS ZEROMQ_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${ZEROMQ_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${ZEROMQ_BINDING_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${ZEROMQ_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(BIND_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)