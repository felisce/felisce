# Find PVM includes and libraries
# 
# PVM (Parallel Virtual Machine) is a software system that enables a collection of heterogeneous computers to be used as a coherent and flexible concurrent computational resource. 
# The individual computers may be shared- or local-memory multiprocessors, vector supercomputers, specialized graphics engines, or scalar workstations, that may be interconnected 
# by a variety of networks, such as ethernet, FDDI, etc. PVM support software executes on each machine in a user-configurable pool, and presents a unified, general, and powerful 
# computational environment of concurrent applications. User programs written in C or Fortran are provided access to PVM through the use of calls to PVM library routines for 
# functions such as process initiation, message transmission and reception, and synchronization via barriers or rendezvous. Users may optionally control the execution location 
# of specific application components. The PVM system transparently handles message routing, data conversion for incompatible architectures, and other tasks that are necessary 
# for operation in a heterogeneous, network environment.
# It can be found at:
#   https://www.netlib.org/pvm3/
# 
# LIBPVM_INCLUDE_DIR - where to find includes files
# LIBPVM_LIBRARIES   - list of fully qualified libraries to link against.

# Check PVM if required
if(NOT FELISCE_WITH_PVM)
    message(STATUS "[PVM Detection] Compiling FELiScE without PVM support. To enable PVM, set the option FELISCE_WITH_PVM to ON.")
    return()
endif()

if(LIBPVM_DIR)
    message(STATUS "[PVM Detection] Searching for PVM in the user-provided directory: ${LIBPVM_DIR}")
    set(INC_SEARCHING_PATHS "${LIBPVM_DIR}")
    set(LIB_SEARCHING_PATHS "${LIBPVM_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[PVM Detection] Searching for PVM in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting LIBPVM_DIR to the root of your PVM installation.")
endif()

# Get optional parameter for felisce_find_library
set(OPT_DIR "")
if( ${CMAKE_SYSTEM_NAME} MATCHES "Linux" )
    if( ${CMAKE_SYSTEM_PROCESSOR} MATCHES "x86_64" )
        set(OPT_DIR "x86_64-linux-gnu")
    elseif( ${CMAKE_SYSTEM_PROCESSOR} MATCHES "aarch64" )
        set(OPT_DIR "aarch64-linux-gnu")
    else()
        message(FATAL_ERROR "[PVM Detection] Architecture ${CMAKE_SYSTEM_PROCESSOR} not supported for ${CMAKE_SYSTEM_NAME} system.")
    endif()
endif()

# Search for the PVM include directory
felisce_find_include_dirs(NAME "PVM" DIRS "${INC_SEARCHING_PATHS}" INC "pvm3.h" OUT LIBPVM_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Find required PVM libraries (shared preferred, fallback to static)
felisce_find_library(NAME "PVM" DIRS ${LIB_SEARCHING_PATHS} LIB "pvm3" OUT LIBPVM_LIBRARIES OPT ${OPT_DIR} ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[PVM Detection] PVM successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${LIBPVM_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS LIBPVM_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${LIBPVM_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${LIBPVM_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)