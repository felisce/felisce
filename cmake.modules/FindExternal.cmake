# Find the external include directories and libraries provided via:

# - EXTERNAL_INCLUDE_DIRS
# - EXTERNAL_LIBRARIES

# Add libraries
foreach(lib IN LISTS EXTERNAL_LIBRARIES)

    # Get library name and path
    get_filename_component(lib_name "${lib}" NAME)
    get_filename_component(lib_path "${lib}" PATH)

    message(STATUS "[External ${lib_name} Detection] Searching for ${lib_name}.")

    # Check if lib is an absolute path
    if(IS_ABSOLUTE "${lib}")
        message(STATUS "[External ${lib_name} Detection] Searching for ${lib_name} in the user-provided directory: ${lib_path}.")
        
        if(NOT EXISTS "${lib}")
            message(FATAL_ERROR "[External ${lib_name} Detection] Could not find library: ${lib_name} in ${lib_path}.")
        endif()
        
        set(${lib_name}_LIBRARY "${lib}")
    else()
        message(STATUS "[External ${lib_name} Detection] Searching for ${lib_name} in system directories.")
        
        find_library(${lib_name}_LIBRARY NAMES "${lib}")
        
        if(NOT ${lib_name}_LIBRARY)
            message(FATAL_ERROR "[External ${lib_name} Detection] Could not find ${lib_name} in system paths.")
        endif()
        
    endif()
              
    message(STATUS "[External ${lib_name} Detection] ${lib_name} successfully found.")
    if(FELISCE_CMAKE_VERBOSE)
        message("\t\t\t\tLibrary: ${${lib_name}_LIBRARY}")
    endif()

    # Link third-party library binaries
    target_link_libraries(FELiScE_dep_libraries INTERFACE ${${lib_name}_LIBRARY})

endforeach()

# Add include directories
foreach(INCLUDE_DIR IN LISTS EXTERNAL_INCLUDE_DIRS)

    message(STATUS "[External Include Detection] Searching for ${INCLUDE_DIR}.")
      
    if(NOT EXISTS "${INCLUDE_DIR}")
        message(FATAL_ERROR "[External Include Detection] Could not find directory: ${INCLUDE_DIR}.")
    endif()
    
    message(STATUS "[External Include Detection] successfully found.")
    if(FELISCE_CMAKE_VERBOSE)
        message("\t\t\t\tIncludes: ${INCLUDE_DIR}")
    endif()

    # Add include directories for third-party dependencies
    target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

endforeach()