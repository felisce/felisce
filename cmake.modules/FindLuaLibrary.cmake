# Find the Lua includes, libraries and binaries
# 
# LUA_INCLUDE_DIR - where to find includes files
# LUA_LIBRARIES   - list of fully qualified libraries to link against

# Check Lua if required
if(NOT FELISCE_WITH_LUA)
    message(STATUS "[Lua Detection] Compiling FELiScE without Lua support. To enable Lua, set the option FELISCE_WITH_LUA to ON.")
    return()
endif()

if(LUA_DIR)

    message(STATUS "[Lua Detection] Searching for Lua in the user-provided directory: ${LUA_DIR}")

    # Search for the Lua include directory
    felisce_find_include_dirs(NAME "Lua" DIRS "${LUA_DIR}" INC "lualib.h" OUT LUA_INCLUDE_DIR ERR_MSG "")

    # Find required Lua libraries (shared preferred, fallback to static)
    felisce_find_library(NAME "Lua" DIRS "${LUA_DIR}" LIB "lua" OUT LUA_LIBRARIES OPT "" ERR_MSG "")

else()
    
    message(STATUS "[Lua Detection] Searching for Lua in system directories.")

    # Find MPI package
    find_package(Lua REQUIRED QUIET)

    if(NOT LUA_FOUND)
        message(FATAL_ERROR "[Lua Detection] Lua not found. Try to set LUA_DIR to the Lua directory path.")
    endif()
endif()

# Output message
message(STATUS "[Lua Detection] Lua successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${LUA_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS LUA_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${LUA_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${LUA_LIBRARIES})