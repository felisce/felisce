# Find the CVGRAPH includes and libraries
#
# It can be found at:
#   https://team.inria.fr/reo/team-members/saverio-smaldone/cvgraph/
# 
# CVGRAPH_INCLUDE_DIR - where to find includes files
# CVGRAPH_LIBRARIES   - list of fully qualified libraries to link against.

# Check CVGraph if required
if(NOT FELISCE_WITH_CVGRAPH)
    message(STATUS "[CVGraph Detection] Compiling FELiScE without CVGraph support. To enable CVGraph, set the option FELISCE_WITH_CVGRAPH to ON.")
    return()
endif()

if(CVGRAPH_DIR)
    message(STATUS "[CVGraph Detection] Searching for CVGraph in the user-provided directory: ${CVGRAPH_DIR}")
else()
    message(FATAL_ERROR "[CVGraph Detection] CVGraph directory path CVGRAPH_DIR must be defined.")
endif()

# Search for the CVGraph include directory
felisce_find_include_dirs(NAME "CVGraph" DIRS "${CVGRAPH_DIR}/include/cvgraph" INC "Graph/cvgraph.hpp" OUT CVGRAPH_INCLUDE_DIR ERR_MSG "")

# Find required CVGraph libraries (shared preferred, fallback to static)
felisce_find_library(NAME "CVGraph" DIRS ${CVGRAPH_DIR} LIB "CVGraph" OUT CVGRAPH_LIBRARIES OPT "" ERR_MSG "")

# Output message
message(STATUS "[CVGraph Detection] CVGraph successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${CVGRAPH_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS CVGRAPH_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${CVGRAPH_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${CVGRAPH_LIBRARIES})