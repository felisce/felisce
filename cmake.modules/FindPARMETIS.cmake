# Find the PARMETIS includes and libraries
#
# ParMETIS is an MPI-based parallel library that implements a variety of algorithms for
# partitioning unstructured graphs, meshes, and for computing fill-reducing orderings of
# sparse matrices. It can be found at:
# 	http://www-users.cs.umn.edu/~karypis/metis/parmetis/index.html
#
# PARMETIS_INCLUDE_DIR - where to find includes files
# PARMETIS_LIBRARIES   - List of fully qualified libraries to link against.

# Set PARMETIS_DIR to PETSC_ARCH_DIR if not defined
set(PARMETIS_DIR "${PETSC_ARCH_DIR}" CACHE STRING "ParMETIS Directory")

if(PARMETIS_DIR)
    message(STATUS "[ParMETIS Detection] Searching for ParMETIS in the user-provided directory: ${PARMETIS_DIR}")
    set(INC_SEARCHING_PATHS "${PARMETIS_DIR}")
    set(LIB_SEARCHING_PATHS "${PARMETIS_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[ParMETIS Detection] Searching for ParMETIS in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting PARMETIS_DIR to the root of your ParMETIS installation.")
endif()

# Search for the ParMETIS include directory
felisce_find_include_dirs(NAME "ParMETIS" DIRS "${INC_SEARCHING_PATHS}" INC "parmetis.h" OUT PARMETIS_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Find required ParMETIS libraries (shared preferred, fallback to static)
felisce_find_library(NAME "ParMETIS" DIRS ${LIB_SEARCHING_PATHS} LIB "parmetis" OUT PARMETIS_LIBRARIES OPT "" ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[ParMETIS Detection] ParMETIS successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${PARMETIS_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS PARMETIS_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${PARMETIS_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${PARMETIS_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)
