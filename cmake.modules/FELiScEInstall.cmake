# ------------------------------------------------------ #
# ------- Install FELiScE library ---------------------- #
# ------------------------------------------------------ #

# Link dependencies
target_link_libraries(FELiScE PRIVATE FELiScE_dep_includes FELiScE_dep_libraries)

# Link the interface library to your main library
target_link_libraries(FELiScE PUBLIC FELiScE_includes)

# Installation: install the library
install(TARGETS FELiScE DESTINATION "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}" COMPONENT libraries)

# Installation: install the library
install(CODE [[
    file(GET_RUNTIME_DEPENDENCIES
        LIBRARIES "$<TARGET_FILE:FELiScE>"
        RESOLVED_DEPENDENCIES_VAR resolved_deps
        UNRESOLVED_DEPENDENCIES_VAR unresolved_deps
        DIRECTORIES "${INSTALL_LIBRARIES_DIR}"
    )

    foreach(dep IN LISTS resolved_deps)

        # Install the dependency (symlink or regular file)
        file(INSTALL FILES "${dep}" DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")

        # Check if it's a symlink: REALPATH differs from the original path if it's a symlink
        get_filename_component(real_path "${dep}" REALPATH)
        if(NOT real_path STREQUAL "${dep}")
            # Install the resolved target of the symlink
            file(INSTALL FILES "${real_path}" DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")
        endif()
    endforeach()

    if(unresolved_deps)
        message(WARNING "Unresolved dependencies: ${unresolved_deps}")
    endif()
]])


# ------------------------------------------------------ #
# ------- Write FELiScE header file -------------------- #
# ------------------------------------------------------ #

# Retrieve the current list of header files associated with the target FELiScE_includes
get_target_property(FELiScE_headers FELiScE_includes HEADERS)

# Sort for better output
list(SORT FELiScE_headers)

# Path for the generated header
set(generated_header ${CMAKE_BINARY_DIR}/src/felisce.hpp)

# Generate include directives for the headers
set(FELISCE_INCLUDES "")
foreach(header IN LISTS FELiScE_headers)
    # Generate relative paths for inclusion
    set(FELISCE_INCLUDES "${FELISCE_INCLUDES}#include \"${header}\"\n")
endforeach()

# Configure the felisce.hpp file from the template
configure_file("${CMAKE_MODULE_PATH}/felisce.hpp.in" ${generated_header} @ONLY)

# Install the generated header
install(FILES ${generated_header} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR} COMPONENT headers)


# ------------------------------------------------------ #
# ------- Compile executables -------------------------- #
# ------------------------------------------------------ #

# Retrieve the final list of source files for the target after the update
get_target_property(felisce_exe_sources FELiScE_executables EXEC_SOURCES)

# Get the number of source files found
list(LENGTH felisce_exe_sources content_count)
message(STATUS "Number of binaries to compile: ${content_count}")

# Sort the source files for better visibility
list(SORT felisce_exe_sources)
message(STATUS "List of binaries to compile:")

# Iterate over the felisce_exe_sources list
foreach(exe_code ${felisce_exe_sources})
    # Process each file from the list
    compile_executables(${exe_code})
endforeach()


# ------------------------------------------------------ #
# ------- Compile external executables ----------------- #
# ------------------------------------------------------ #

# Get the number of source files found
list(LENGTH EXTERNAL_EXECUTABLE_DIRS content_count)
message(STATUS "Number of external folder to compile: ${content_count}")

# Sort the source files for better visibility
list(SORT EXTERNAL_EXECUTABLE_DIRS)
message(STATUS "List of external binaries to compile:")

# Iterate over the external_compilation_folder list
foreach(folder IN LISTS EXTERNAL_EXECUTABLE_DIRS)
    # Process each directory from the list
    compile_external_directory(${folder})
endforeach()