# Find the MUMPS includes and libraries
#
# MUMPS (MUltifrontal Massively Parallel Sparse direct Solver) can solve very large linear systems through in/out-of-core LDLt or LU factorisation.
# It can be found at:
# 	http://mumps.enseeiht.fr/
#
# MUMPS_INCLUDE_DIR - where to find includes files
# MUMPS_LIBRARIES   - list of fully qualified libraries to link against.

# Set MUMPS_DIR to PETSC_ARCH_DIR if not defined
set(MUMPS_DIR "${PETSC_ARCH_DIR}" CACHE STRING "MUMPS Directory")

if(MUMPS_DIR)
    message(STATUS "[MUMPS Detection] Searching for MUMPS in the user-provided directory: ${MUMPS_DIR}")
    set(INC_SEARCHING_PATHS "${MUMPS_DIR}")
    set(LIB_SEARCHING_PATHS "${MUMPS_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[MUMPS Detection] Searching for MUMPS in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting MUMPS_DIR to the root of your MUMPS installation.")
endif()

# Search for the MUMPS include directory
felisce_find_include_dirs(NAME "MUMPS" DIRS "${INC_SEARCHING_PATHS}" INC "cmumps_c.h" OUT MUMPS_INCLUDE_DIR_C ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "MUMPS" DIRS "${INC_SEARCHING_PATHS}" INC "dmumps_c.h" OUT MUMPS_INCLUDE_DIR_D ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "MUMPS" DIRS "${INC_SEARCHING_PATHS}" INC "smumps_c.h" OUT MUMPS_INCLUDE_DIR_S ERR_MSG "${ERROR_MSG}")
felisce_find_include_dirs(NAME "MUMPS" DIRS "${INC_SEARCHING_PATHS}" INC "zmumps_c.h" OUT MUMPS_INCLUDE_DIR_C ERR_MSG "${ERROR_MSG}")

# Create MUMPS include directories list
set(MUMPS_INCLUDE_DIR ${MUMPS_INCLUDE_DIR_C}
                      ${MUMPS_INCLUDE_DIR_D}
                      ${MUMPS_INCLUDE_DIR_S}
                      ${MUMPS_INCLUDE_DIR_C}
)

# Find required MUMPS libraries (shared preferred, fallback to static)
felisce_find_library(NAME "MUMPS" DIRS ${LIB_SEARCHING_PATHS} LIB "cmumps"       OUT MUMPS_LIBRARY_C      OPT "" ERR_MSG "${ERROR_MSG}")
felisce_find_library(NAME "MUMPS" DIRS ${LIB_SEARCHING_PATHS} LIB "dmumps"       OUT MUMPS_LIBRARY_D      OPT "" ERR_MSG "${ERROR_MSG}")
felisce_find_library(NAME "MUMPS" DIRS ${LIB_SEARCHING_PATHS} LIB "smumps"       OUT MUMPS_LIBRARY_S      OPT "" ERR_MSG "${ERROR_MSG}")
felisce_find_library(NAME "MUMPS" DIRS ${LIB_SEARCHING_PATHS} LIB "zmumps"       OUT MUMPS_LIBRARY_Z      OPT "" ERR_MSG "${ERROR_MSG}")
felisce_find_library(NAME "MUMPS" DIRS ${LIB_SEARCHING_PATHS} LIB "mumps_common" OUT MUMPS_LIBRARY_COMMON OPT "" ERR_MSG "${ERROR_MSG}")
felisce_find_library(NAME "MUMPS" DIRS ${LIB_SEARCHING_PATHS} LIB "pord"         OUT MUMPS_LIBRARY_PORD   OPT "" ERR_MSG "${ERROR_MSG}")

# Create MUMPS libraries list
set(MUMPS_LIBRARIES ${MUMPS_LIBRARY_C}
                    ${MUMPS_LIBRARY_D}
                    ${MUMPS_LIBRARY_S}
                    ${MUMPS_LIBRARY_Z}
                    ${MUMPS_LIBRARY_COMMON}
                    ${MUMPS_LIBRARY_PORD}
)

# Output message
message(STATUS "[MUMPS Detection] MUMPS successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${MUMPS_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS MUMPS_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${MUMPS_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${MUMPS_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)
