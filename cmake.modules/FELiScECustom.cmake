# - add_compiler_flag
#
# Adds specified compiler flags for given languages and configurations,
# ensuring that each flag is supported by the current compiler.
#
# Parameters:
#   OPT    : (Optional) If provided, treats the flags as optional. If a flag
#            is not supported and OPT is set, a warning is issued instead of
#            a fatal error.
#   LANG   : A list of programming languages (e.g., C, CXX) to which the flags
#            should be applied.
#   CONFIG : (Optional) A list of build configurations (e.g., Debug, Release)
#            for which the flags should be added. If not specified, the flags
#            are added for all configurations.
#   FLAGS  : A list of compiler flags to be added.
#
# Behavior:
# - Parses the provided arguments to extract LANG, CONFIG, FLAGS, and OPT.
# - Validates that LANG and FLAGS are provided; issues a fatal error if either
#   is missing.
# - For each specified language and flag:
#     - Converts the flag by replacing '-' with '_' to form a variable name.
#     - If FELISCE_CMAKE_VERBOSE is set:
#         - Checks if the current compiler supports the flag:
#             - For C language, uses check_c_compiler_flag.
#             - For CXX language, uses check_cxx_compiler_flag.
#         - If the flag is supported:
#             - Adds the flag to the compile options for the specified language
#               and configurations.
#             - Logs a status message indicating support.
#         - If the flag is not supported:
#             - If OPT is set, issues a warning.
#             - If OPT is not set, issues a fatal error.
#     - If FELISCE_CMAKE_VERBOSE is not set:
#         - Adds the flag to the compile options for the specified language
#           and configurations without performing support checks.
#
# Usage:
#   add_compiler_flag(
#     LANG   <languages>
#     FLAGS  <flags>
#     [CONFIG <configurations>]
#     [OPT]
#   )
#
# Example:
#   add_compiler_flag(
#     LANG   CXX
#     FLAGS  -Wall -Wextra
#     CONFIG Debug
#   )
function(add_compiler_flag)
    # Parse arguments
    cmake_parse_arguments(
        PARSED_ARGS
        "OPT"               # Boolean options
        ""                  # Single-value arguments
        "LANG;CONFIG;FLAGS" # Multi-value arguments
        ${ARGN}
    )

    # Ensure LANG is provided
    if(NOT PARSED_ARGS_LANG)
        message(FATAL_ERROR "add_compiler_flag: No language provided.")
    endif()

    # Ensure FLAGS is provided
    if(NOT PARSED_ARGS_FLAGS)
        message(FATAL_ERROR "add_compiler_flag: No flags provided.")
    endif()

    foreach(lang IN LISTS PARSED_ARGS_LANG)
            
        foreach(flag IN LISTS PARSED_ARGS_FLAGS)
            # Replace '-' with '_' for variable naming
            string(REGEX REPLACE "-" "_" flag_var "${flag}")

            if(FELISCE_CMAKE_VERBOSE)

                # Check if the compiler supports the flag
                if ( ${lang} STREQUAL C )
                    check_c_compiler_flag("${flag}" SUPPORT_${lang}_${flag_var})
                elseif( ${lang} STREQUAL CXX )
                    check_cxx_compiler_flag("${flag}" SUPPORT_${lang}_${flag_var})
                else()
                    message(FATAL_ERROR "add_compiler_flag: Language ${lang} not supported.")
                endif()
                
                if(SUPPORT_${lang}_${flag_var})

                    # Add the ${flag} to compile options for ${lang} language
                    foreach(config IN LISTS PARSED_ARGS_CONFIG)
                        add_compile_options($<$<AND:$<COMPILE_LANGUAGE:${lang}>,${config}>:${flag}>)
                    endforeach()

                    message(STATUS "add_compiler_flag: The ${lang} compiler \"${CMAKE_${lang}_COMPILER_ID}\" supports the flag \"${flag}\".")
                else()
                    # Handle unsupported flags
                    if(PARSED_ARGS_OPT)
                        message(WARNING "add_compiler_flag: The ${lang} compiler \"${CMAKE_CXX_COMPILER_ID}\" does not support the optional flag \"${flag}\".")
                    else()
                        message(FATAL_ERROR "add_compiler_flag: The ${lang} compiler \"${CMAKE_CXX_COMPILER_ID}\" requires the flag \"${flag}\".")
                    endif()
                endif()

            else()

                foreach(config IN LISTS PARSED_ARGS_CONFIG)
                    # Add the ${flag} to compile options for ${lang} language
                    add_compile_options($<$<AND:$<COMPILE_LANGUAGE:${lang}>,${config}>:${flag}>)
                endforeach()

            endif()
            
        endforeach()

    endforeach()

endfunction()


# - felisce_find_include_dirs
#
# This function searches for a specified header file within provided directories, appending "/include" 
# to each directory to look specifically in standard include locations. If found, the directory 
# containing the header is assigned to an output variable.
#
# Parameters:
#   NAME    : A descriptive name for the library/component (used in messages and logs).
#   INC     : The header file to locate (e.g., "someheader.h").
#   OUT     : The output variable that will store the path to the directory containing the header file.
#   ERR_MSG : (Optional) A custom error message displayed when the header file is not found.
#   DIRS    : A list of base directories to search for the header file.
#
# Search Behavior:
# 1. Parses input arguments to extract the name, header file, output variable, optional error message, and search directories.
# 2. Constructs a list of search paths by:
#    - Using each directory from DIRS directly.
#    - Adding "/include" to each directory in DIRS.
# 3. Attempts to locate the specified header file (INC) in each of the constructed paths.
# 4. If the header file is found, sets the output variable (OUT) to the path of the containing directory and returns.
# 5. If the header file is not found:
#    - Sets the output variable (OUT) to an empty string.
#    - Issues a fatal error message, including the provided ERR_MSG if any, along with a clear indication of the missing file.
#
# Usage:
#   felisce_find_include_dirs(
#     NAME    <component_name>
#     INC     <header_file>
#     OUT     <output_variable>
#     ERR_MSG "<custom_error_message>"
#     DIRS    <directory1> <directory2> ...
#   )
#
# Example:
#   felisce_find_include_dirs(
#     NAME    MyLibrary
#     INC     mylibrary.h
#     OUT     MYLIBRARY_INCLUDE_DIR
#     ERR_MSG "MyLibrary headers are required but could not be found."
#     DIRS    /usr/local /opt/libs
#   )
#
#   # Search order:
#   # - /usr/local/mylibrary.h
#   # - /usr/local/include/mylibrary.h
#   # - /opt/libs/mylibrary.h
#   # - /opt/libs/include/mylibrary.h
#
#   # If found, MYLIBRARY_INCLUDE_DIR contains the directory where mylibrary.h is located.
#   # If not found, CMake raises a fatal error with the specified ERR_MSG.
function(felisce_find_include_dirs)

    # Parse arguments
    cmake_parse_arguments(
        ARGS
        ""                      # Boolean options
        "NAME;INC;OUT;ERR_MSG"  # Single-value arguments
        "DIRS"                  # Multi-value arguments
        ${ARGN}
    )

    # Initialize list with ARGS_DIRS
    set(PATHS_LIST ${ARGS_DIRS})

    # Append "/include" to each directory in ARGS_DIRS
    foreach(DIR IN LISTS ARGS_DIRS)
        list(APPEND PATHS_LIST "${DIR}/include")
    endforeach()

    # Look for include directory
    foreach(DIR ${PATHS_LIST})
        find_path(${ARGS_OUT}_AUX NAMES ${ARGS_INC} PATHS "${DIR}")
        if(NOT ${${ARGS_OUT}_AUX} STREQUAL "${ARGS_OUT}_AUX-NOTFOUND")
            set(${ARGS_OUT} ${${ARGS_OUT}_AUX} PARENT_SCOPE)
            return()
        endif()
    endforeach()

    # Return error if include dir not detected
    set(${ARGS_OUT} "" PARENT_SCOPE)
    message(FATAL_ERROR "[${ARGS_NAME} Detection] Could not find include directory for ${ARGS_INC} in ${PATHS_LIST}.\n${ARGS_ERR_MSG}")

endfunction()


# - felisce_find_library
#
# Searches for a specified library within a set of provided directories and assigns the
# path of the found library to an output variable. Both shared and static versions are considered,
# with shared libraries being prioritized if both are found.
#
# Parameters:
#   NAME    : A descriptive name for the library (used for logging and error messages).
#   LIB     : The base name of the library (without the "lib" prefix and without file extension).
#   OUT     : The output variable that will store the full path to the detected library.
#   OPT     : (Optional) Subdirectory to append to each directory in DIRS (useful for
#             architecture-specific paths like "arch-linux-c-opt").
#   DIRS    : List of base directories in which to search for the library.
#   ERR_MSG : (Optional) Additional error message to display if the library is not found.
#
# Search Logic:
# 1. The function constructs a list of search directories from the provided DIRS.
#    The following directories are checked for the library:
#      - Each directory in DIRS directly.
#      - Each directory in DIRS with "/lib" appended.
#      - If OPT is provided:
#        - Each directory with "/${OPT}" appended.
#        - Each directory with "/lib/${OPT}" appended.
#
# 2. Searches for a shared library file named "lib<LIB><shared_library_suffix>" (e.g., libmylib.so on Linux).
#    If found, assigns the path to OUT and returns immediately.
#
# 3. If no shared library is found, searches for a static library file named "lib<LIB><static_library_suffix>"
#    (e.g., libmylib.a).
#    If found, assigns the path to OUT and returns.
#
# 4. If neither shared nor static versions are found:
#    - The output variable OUT is set to an empty string.
#    - A fatal error is raised, printing the list of searched directories and the optional ERR_MSG.
#
# Example Usage:
#   felisce_find_library(
#     NAME PETSc
#     LIB  petsc
#     OUT  PETSC_LIBRARY
#     OPT  arch-linux-c-opt
#     DIRS /usr/local /opt/petsc
#     ERR_MSG "Please ensure PETSc is correctly installed and the PETSC_DIR is set."
#   )
#
#   # This will search for:
#   #   /usr/local/lib/libpetsc.so
#   #   /usr/local/lib/libpetsc.a
#   #   /usr/local/lib/arch-linux-c-opt/libpetsc.so
#   #   /usr/local/lib/arch-linux-c-opt/libpetsc.a
#   #   /usr/local/arch-linux-c-opt/libpetsc.so
#   #   /usr/local/arch-linux-c-opt/libpetsc.a
#   #   (and the same paths under /opt/petsc)
#
#   # If a library is found, PETSC_LIBRARY will be set to its full path.
#   # If not found, CMake will terminate with a clear error message.
#
function(felisce_find_library)

    # Parse arguments
    cmake_parse_arguments(
        ARGS
        ""                         # Boolean options
        "NAME;LIB;OUT;OPT;ERR_MSG" # Single-value arguments
        "DIRS"                     # Multi-value arguments
        ${ARGN}
    )

    # Initialize list with ARGS_DIRS
    set(PATHS_LIST ${ARGS_DIRS})

    # Append "/lib" to each directory in ARGS_DIRS
    foreach(DIR IN LISTS ARGS_DIRS)
        list(APPEND PATHS_LIST "${DIR}/lib")
    endforeach()

    # If ARGS_OPT is provided, append "/lib/${ARGS_OPT}" to each directory
    if(ARGS_OPT)
        foreach(DIR IN LISTS ARGS_DIRS)
            list(APPEND PATHS_LIST "${DIR}/${ARGS_OPT}")    
            list(APPEND PATHS_LIST "${DIR}/lib/${ARGS_OPT}")
        endforeach()
    endif()

    # Look for shared library
    foreach(DIR ${PATHS_LIST})
        find_library(${ARGS_OUT}_SHARED "lib${ARGS_LIB}${CMAKE_SHARED_LIBRARY_SUFFIX}" PATHS "${DIR}" NO_DEFAULT_PATH)
        if(NOT ${${ARGS_OUT}_SHARED} STREQUAL "${ARGS_OUT}_SHARED-NOTFOUND")
            set(${ARGS_OUT} ${${ARGS_OUT}_SHARED} PARENT_SCOPE)
            return()
        endif()
    endforeach()
    
    # Look for static library
    foreach(DIR ${PATHS_LIST})
        find_library(${ARGS_OUT}_STATIC "lib${ARGS_LIB}${CMAKE_STATIC_LIBRARY_SUFFIX}" PATHS "${DIR}" NO_DEFAULT_PATH)
        if(NOT ${${ARGS_OUT}_STATIC} STREQUAL "${ARGS_OUT}_STATIC-NOTFOUND")
            set(${ARGS_OUT} ${${ARGS_OUT}_STATIC} PARENT_SCOPE)
            return()
        endif()
    endforeach()

    # Return error if library not detected
    set(${ARGS_OUT} "" PARENT_SCOPE)
    message(FATAL_ERROR "[${ARGS_NAME} Detection] Could not find library: lib${ARGS_LIB} in ${PATHS_LIST}.\n${ERR_MSG}")

endfunction()


# - felisce_find_program
#
# Locates a specified executable within given directories and sets an output
# variable to its path. If the executable is not found, the function issues
# a fatal error.
#
# Parameters:
#   NAME : The name of the executable to locate.
#   EXE  : The variable to store the path of the found executable.
#   OUT  : The output variable to hold the path of the executable.
#   OPT  : (Optional) A subdirectory within 'bin' where the executable might reside.
#   DIRS : A list of directories to search for the executable.
#
# Behavior:
# - Parses the provided arguments to extract the executable name (NAME),
#   output variable (OUT), and optional subdirectory (OPT).
# - Constructs potential paths to the executable:
#     - If OPT is provided, appends each DIR in DIRS with '/bin/OPT'.
#     - If OPT is not provided, appends each DIR in DIRS with '/bin'.
# - Uses 'find_program' to search for the executable in the constructed paths.
# - If the executable is found:
#     - Sets the OUT variable to the path of the executable.
# - If the executable is not found:
#     - Sets the EXE variable to an empty string.
#     - Issues a fatal error indicating the executable could not be found
#       in the specified paths.
#
# Usage:
#   felisce_find_program(
#     NAME <executable_name>
#     EXE  <exe_variable>
#     OUT  <output_variable>
#     [OPT <optional_subdirectory>]
#     DIRS <directory1> <directory2> ...
#   )
#
# Example:
#   felisce_find_program(
#     NAME my_executable
#     EXE  MY_EXE_PATH
#     OUT  FOUND_EXE_PATH
#     OPT  custom_subdir
#     DIRS /usr/local /opt/tools
#   )
#   # If 'my_executable' is found, 'FOUND_EXE_PATH' will be set to its path.
#   # Otherwise, a fatal error will be raised.
function(felisce_find_program)

    # Parse arguments
    cmake_parse_arguments(
        ARGS
        ""                  # Boolean options
        "NAME;EXE;OUT;OPT"  # Single-value arguments
        "DIRS"              # Multi-value arguments
        ${ARGN}
    )

    # Handle the optional parameter
    if(NOT ARGS_OPT)
        set(EXE_PATHS "${ARGS_DIRS}/bin")
    else()
        set(EXE_PATHS "")  
        foreach(DIR ${ARGS_DIRS})
            list(APPEND EXE_PATHS "${DIR}/bin/${ARGS_OPT}")
        endforeach()
    endif()

    # Look for exe program
    find_program(${ARGS_EXE}_FOUND "${ARGS_EXE}" PATHS "${EXE_PATHS}" NO_DEFAULT_PATH)

    # Return variable or error
    if(${ARGS_EXE}_FOUND)
        set(${ARGS_OUT} ${${ARGS_EXE}_FOUND} PARENT_SCOPE)
    else()
        set(${ARGS_OUT} "" PARENT_SCOPE)
        message(FATAL_ERROR "[${ARGS_NAME} Detection] Could not find program: ${ARGS_EXE} in ${EXE_PATHS}.")
    endif()
endfunction()


# - DetectLinuxOSVersion
#
# Determines the name and version of the Linux operating system and assigns
# these values to the specified output variables.
#
# Parameters:
#   os_name_var    : The variable to store the detected operating system name.
#   os_version_var : The variable to store the detected operating system version.
#
# Behavior:
# - Initializes the output variables to "Unknown".
# - Checks for the existence of the '/etc/os-release' file:
#     - If present:
#         - Reads its content to extract the OS name and version.
#         - Assigns the extracted values to 'os_name' and 'os_version'.
#     - If absent or extraction fails:
#         - Attempts to retrieve the OS name and version using the 'lsb_release' command.
# - Sets the output variables to the detected OS name and version, making them
#   available in the parent scope.
#
# Usage:
#   DetectLinuxOSVersion(<os_name_variable> <os_version_variable>)
#
# Example:
#   DetectLinuxOSVersion(OS_NAME OS_VERSION)
#   message(STATUS "Detected OS: ${OS_NAME} ${OS_VERSION}")
function(DetectLinuxOSVersion os_name_var os_version_var)
    # Initialize outputs
    set(${os_name_var} "Unknown" PARENT_SCOPE)
    set(${os_version_var} "Unknown" PARENT_SCOPE)

    # Check if /etc/os-release exists
    if (EXISTS "/etc/os-release")
        file(READ "/etc/os-release" os_release_content)
        
        # Extract the OS name (e.g., Ubuntu, Fedora)
        string(REGEX MATCH "NAME=\"([^\"]+)\"" _os_name_match ${os_release_content})
        if (DEFINED CMAKE_MATCH_1)
            set(os_name "${CMAKE_MATCH_1}")
        endif()

        # Extract the OS version (e.g., 22.04, 37)
        string(REGEX MATCH "VERSION_ID=\"([^\"]+)\"" _os_version_match ${os_release_content})
        if (DEFINED CMAKE_MATCH_1)
            set(os_version "${CMAKE_MATCH_1}")
        endif()
    endif()

    # Fallback for systems without /etc/os-release
    if (os_name STREQUAL "Unknown" OR os_version STREQUAL "Unknown")
        execute_process(
            COMMAND lsb_release -ds
            OUTPUT_VARIABLE os_name
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET
        )
        execute_process(
            COMMAND lsb_release -rs
            OUTPUT_VARIABLE os_version
            OUTPUT_STRIP_TRAILING_WHITESPACE
            ERROR_QUIET
        )
    endif()

    # Return the results
    set(${os_name_var} "${os_name}" PARENT_SCOPE)
    set(${os_version_var} "${os_version}" PARENT_SCOPE)
endfunction()


# - check_path_exists
#
# Verifies that a specified path variable is both defined and non-empty,
# and that the path it references exists in the file system.
#
# Parameters:
#   PATH_VAR : The name of the variable holding the path to be checked.
#
# Behavior:
# - Checks if the variable ${PATH_VAR} is defined and not empty.
#   - If undefined or empty, it issues a fatal error message:
#     "The path variable '${PATH_VAR}' is not defined or is empty."
# - Checks if the path specified by ${PATH_VAR} exists.
#   - If the path does not exist, it issues a fatal error message:
#     "The path '${${PATH_VAR}}' does not exist."
#
# Usage:
#   check_path_exists(<PathVariableName>)
#
# Example:
#   set(MY_PATH "/path/to/directory")
#   check_path_exists(MY_PATH)
function(check_path_exists PATH_VAR)
    # Check if the variable is defined and not empty
    if(NOT DEFINED ${PATH_VAR} OR ${${PATH_VAR}} STREQUAL "")
        message(FATAL_ERROR "The path variable '${PATH_VAR}' is not defined or is empty.")
    endif()

    # Check if the path exists
    if(NOT EXISTS ${${PATH_VAR}})
        message(FATAL_ERROR "The path '${${PATH_VAR}}' does not exist.")
    endif()
endfunction()


# - compile_executables
#
# This function creates an executable target from a specified C++ source file.
# It sets up the necessary include directories, links required libraries,
# and installs the resulting executable.
#
# Parameters:
#   exe_code : The path to the C++ source file to be compiled into an executable.
#
# Behavior:
# - Extracts the executable name from the source file name (without extension).
# - Determines the directory path of the source file.
# - Logs the executable name and its corresponding source file.
# - Creates an executable target using the extracted name and source file.
# - Adds the source file's directory and relevant include directories to the target's include paths.
# - Links the executable with the necessary libraries, including 'FELiScE' and its dependencies.
# - Installs the executable to the directory specified by 'CMAKE_INSTALL_BINDIR'.
#
# Usage:
#   compile_executables(<path_to_source_file>)
#
# Example:
#   compile_executables(${CMAKE_SOURCE_DIR}/src/main.cpp)
function(compile_executables exe_code)
    
    # Extract the executable name from the source file path
    get_filename_component(exe_name ${exe_code} NAME_WE)
    
    # Get the directory path of the source file
    get_filename_component(exe_path ${exe_code} DIRECTORY)
    
    # Print the executable name and its corresponding source file
    message("\t-${exe_name}: ${exe_code}")

    # Create an executable target from the source file
    add_executable(${exe_name} "${exe_code}")

    # Add include directories for the executable
    target_include_directories(${exe_name} PRIVATE ${exe_path}
                                                    $<TARGET_PROPERTY:FELiScE_dep_includes,INTERFACE_INCLUDE_DIRECTORIES>
                                                    $<TARGET_PROPERTY:FELiScE,INTERFACE_INCLUDE_DIRECTORIES>
    )

    # Link libraries to the executable
    target_link_libraries(${exe_name} PRIVATE $<TARGET_PROPERTY:FELiScE_dep_libraries,INTERFACE_LINK_LIBRARIES> FELiScE)

    # Install the executable
    install(TARGETS ${exe_name} DESTINATION ${CMAKE_INSTALL_BINDIR} COMPONENT executables)
    
endfunction()


# - compile_external_directory
#
# This function automates the compilation of multiple executables from a specified
# directory containing C++ source files. It processes all '.cpp' files within the
# given directory, identifies files starting with 'main', and creates separate
# executables for each 'main*.cpp' file, linking them with the common source files.
#
# Parameters:
#   directory : The path to the directory containing the C++ source files.
#
# Behavior:
# - Recursively collects all '.cpp' files in the specified directory.
# - Identifies 'main*.cpp' files to serve as entry points for executables.
# - Excludes these 'main*.cpp' files from the list of common source files to prevent
#   multiple inclusions.
# - For each 'main*.cpp' file:
#     - Derives an executable name based on the parent directory and the 'main' file name.
#     - Creates an executable target with the 'main' file and the common source files.
#     - Adds the specified directory and relevant include directories to the target's
#       include paths.
#     - Links the executable with the necessary libraries, including 'FELiScE' and its
#       dependencies.
#     - Installs the executable to the directory containing the 'main' file.
#
# Usage:
#   compile_external_directory(<path_to_source_directory>)
#
# Example:
#   compile_external_directory(${CMAKE_SOURCE_DIR}/src)

function(compile_external_directory directory)
    # Collect all .cpp files and identify main*.cpp files
    file(GLOB_RECURSE cpp_files  "${directory}/*.cpp")
    file(GLOB_RECURSE main_files "${directory}/main*.cpp")
   
    # Remove main*.cpp from the general cpp_files to avoid double inclusion
    list(REMOVE_ITEM cpp_files ${main_files})

    foreach(main_file IN LISTS main_files)
        # Derive executable name from the parent directory and main file
        get_filename_component(main_dir_name ${directory} NAME)
        get_filename_component(main_name ${main_file} NAME_WE)
        string(REPLACE "main" "" main_name ${main_name})
        set(executable_name "${main_dir_name}${main_name}")
        
        # Print the executable name and its corresponding source file
        message("\t-${executable_name}: ${main_file}")

        # Create executable target
        add_executable(${executable_name} ${main_file} ${cpp_files})

        # Add include directories
        target_include_directories(${executable_name} PRIVATE ${directory}
                                                              $<TARGET_PROPERTY:FELiScE,INTERFACE_INCLUDE_DIRECTORIES>
                                                              $<TARGET_PROPERTY:FELiScE_dep_includes,INTERFACE_INCLUDE_DIRECTORIES>
        )

        # Link libraries
        target_link_libraries(${executable_name} PRIVATE $<TARGET_PROPERTY:FELiScE_dep_libraries,INTERFACE_LINK_LIBRARIES> FELiScE)

        # Install executable to the directory containing the main file
        install(TARGETS ${executable_name} RUNTIME DESTINATION ${directory} COMPONENT executables)

    endforeach()
endfunction()
