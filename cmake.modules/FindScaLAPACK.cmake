# Find the SCALAPACK includes and libraries
#
# The ScaLAPACK (or Scalable LAPACK) library includes a subset of LAPACK routines redesigned for distributed memory MIMD parallel computers. It is currently written in a Single-Program-Multiple-Data style using explicit message passing for interprocessor communication. It assumes matrices are laid out in a two-dimensional block cyclic decomposition.
# ScaLAPACK is designed for heterogeneous computing and is portable on.
# It can be found at:
# 	https://www.netlib.org/scalapack/
#
# SCALAPACK_LIBRARIES   - List of fully qualified libraries to link against.

# Set SCALAPACK_DIR to PETSC_ARCH_DIR if not defined
set(SCALAPACK_DIR "${PETSC_ARCH_DIR}" CACHE STRING "ScaLAPACK Directory")

if(SCALAPACK_DIR)
    message(STATUS "[ScaLAPACK Detection] Searching for ScaLAPACK in the user-provided directory: ${SCALAPACK_DIR}")
    set(LIB_SEARCHING_PATHS "${SCALAPACK_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[ScaLAPACK Detection] Searching for ScaLAPACK in system directories.")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting SCALAPACK_DIR to the root of your ScaLAPACK installation.")
endif()

# Find required ScaLAPACK libraries (shared preferred, fallback to static)
felisce_find_library(NAME "ScaLAPACK" DIRS ${LIB_SEARCHING_PATHS} LIB "scalapack" OUT SCALAPACK_LIBRARIES OPT "" ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[ScaLAPACK Detection] ScaLAPACK successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS SCALAPACK_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${SCALAPACK_LIBRARIES})

# Unset searching paths
unset(LIB_SEARCHING_PATHS)