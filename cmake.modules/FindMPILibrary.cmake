# Find the MPI includes, libraries and binaries
# 
# MPI_CXX_INCLUDE_DIRS - where to find includes files
# MPI_LIBRARIES        - list of fully qualified libraries to link against
# MPIEXEC

if(MPI_DIR)

    message(STATUS "[MPI Detection] Searching for MPI in the user-provided directory: ${MPI_DIR}")

    # Search for the MPI include directory
    felisce_find_include_dirs(NAME "MPI" DIRS "${MPI_DIR}" INC "mpi.h" OUT MPI_CXX_INCLUDE_DIRS ERR_MSG "")
    
    # Find required MPI libraries (shared preferred, fallback to static)
    felisce_find_library(NAME "MPI" DIRS "${MPI_DIR}" LIB "mpi" OUT MPI_LIBRARIES OPT "" ERR_MSG "")

    # Find required MPI programs
    felisce_find_program(NAME "MPI" DIRS "${MPI_DIR}" EXE "mpiexec" OUT MPIEXEC)

else()
    
    message(STATUS "[MPI Detection] Searching for MPI in system directories.")

    # Find MPI package
    find_package(MPI REQUIRED QUIET)

    if(NOT MPI_FOUND)
        message(FATAL_ERROR "[MPI Detection] MPI not found. Try to set MPI_DIR to the MPI directory path.")
    endif()
endif()

# Output message
message(STATUS "[MPI Detection] MPI successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${MPI_CXX_INCLUDE_DIRS}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS MPI_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
    message("\t\t\t\tBinaries:")
    message("\t\t\t\t\t\tmpiexec : ${MPIEXEC}")
endif()

# Add include directories for third-party dependencies
foreach(MPI_INCLUDE_DIR ${MPI_CXX_INCLUDE_DIRS})
    target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${MPI_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)
endforeach()

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${MPI_LIBRARIES})