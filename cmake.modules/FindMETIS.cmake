# Find the METIS includes and libraries
#
# METIS is a set of serial programs for partitioning graphs, partitioning finite element meshes, 
# and producing fill reducing orderings for sparse matrices. The algorithms implemented in METIS 
# are based on the multilevel recursive-bisection, multilevel k-way, and multi-constraint 
# partitioning schemes developed in our lab. It can be found at:
# 	https://github.com/KarypisLab/METIS
#
# METIS_INCLUDE_DIR - where to find includes files
# METIS_LIBRARIES   - list of fully qualified libraries to link against.

# Set MUMPS_DIR to PETSC_ARCH_DIR if not defined
set(METIS_DIR "${PETSC_ARCH_DIR}" CACHE STRING "METIS Directory")

if(METIS_DIR)
    message(STATUS "[METIS Detection] Searching for METIS in the user-provided directory: ${METIS_DIR}")
    set(INC_SEARCHING_PATHS "${METIS_DIR}")
    set(LIB_SEARCHING_PATHS "${METIS_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[METIS Detection] Searching for METIS in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting METIS_DIR to the root of your METIS installation.")
endif()

# Search for the METIS include directory
felisce_find_include_dirs(NAME "METIS" DIRS "${INC_SEARCHING_PATHS}" INC "metis.h" OUT METIS_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Find required METIS libraries (shared preferred, fallback to static)
felisce_find_library(NAME "METIS" DIRS ${LIB_SEARCHING_PATHS} LIB "metis" OUT METIS_LIBRARIES OPT "" ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[METIS Detection] METIS successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${METIS_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS METIS_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${METIS_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${METIS_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)