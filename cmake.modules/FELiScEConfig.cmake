# ------------------------------------------------------ #
# ------- Compilers options ---------------------------- #
# ------------------------------------------------------ #
include(CheckCXXCompilerFlag)
include(CheckLinkerFlag)

# Specify the C++ standard
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Export compile option to file
# if(CMAKE_BUILD_TYPE MATCHES "Coverage")
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
# endif()

# Corresponds to -fPIC flag
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# Compiler specific flags
if( ${CMAKE_CXX_COMPILER_ID} MATCHES "Clang|AppleClang" )

    # add_compiler_flag(LANG CXX CONFIG $<CONFIG:Release> FLAGS -O3)
    # add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -O0 -g)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -march=native -mtune=native)
    # add_compiler_flag(LANG CXX CONFIG $<CONFIG:Release> FLAGS -DNDEBUG)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -fno-inline)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -fdiagnostics-show-option)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -unroll-loops -Wno-unused-command-line-argument)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -fno-common)
    # add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -Weverything)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -Wall)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -Wextra -Werror=sign-compare)
    # add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -Wextra -Werror=sign-compare -fdiagnostics-show-option -Wunused-parameter -Wundef -Wno-long-long -Wcomment -pedantic)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -Wsuggest-override)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -fcolor-diagnostics)
    add_compile_options($<$<OR:$<CONFIG:Debug>,$<CONFIG:Coverage>>:-DVERBOSE_DEBUG>)
    
    add_link_options(-Wl,-no_compact_unwind)
    # add_link_options(-Wl,-undefined,dynamic_lookup)

    # Errors to add 
    # -Werror=delete-non-abstract-non-virtual-dtor 
    # -Werror=suggest-override 
    # -Werror=extra-tokens 
    # -Werror=defaulted-function-deleted 
    # -Werror=potentially-evaluated-expression 
    # -Werror=instantiation-after-specialization 
    # -Werror=undefined-var-template 
    # -Werror=unused-lambda-capture 
    # -Werror=unused-const-variable 
    # -Werror=parentheses 
    # -Werror=pessimizing-move 
    # -Werror=logical-op-parentheses

elseif( ${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" )

    # add_compiler_flag(LANG C CXX CONFIG $<CONFIG:Release> FLAGS -O3)
    # add_compiler_flag(LANG C CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -O0 -g)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -march=native)
    # add_compiler_flag(LANG C CXX CONFIG $<CONFIG:Release> FLAGS -DNDEBUG)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -fno-inline)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -fdiagnostics-show-option)
    # check_cxx_compiler_flag("-funroll-loops" SUPPORT_FUNROLL_LOOPS)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -funroll-loops)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -fno-common)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -Wall)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -Wextra -Werror=sign-compare)
    # add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -Wextra -Werror=sign-compare -show-option -Wunused-parameter -Wundef -Wno-long-long -Wcomment -pedantic)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> FLAGS -Wsuggest-override)
    add_compiler_flag(LANG CXX CONFIG $<CONFIG:Debug> $<CONFIG:Coverage> $<CONFIG:Release> FLAGS -fdiagnostics-color=always)
    add_compile_options($<$<OR:$<CONFIG:Debug>,$<CONFIG:Coverage>>:-DVERBOSE_DEBUG>)

    # add_link_options(-Wl,-undefined,dynamic_lookup)
    
    add_compile_options($<$<CONFIG:Coverage>:--coverage>)
    add_link_options($<$<CONFIG:Coverage>:--coverage>)

    # Errors to add 
    # -Werror=ignored-qualifiers 
    # -Werror=suggest-override 
    # -Werror=unused-variable 
    # -Werror=misleading-indentation 
    # -Werror=return-type 
    # -Werror=unused-but-set-variable 
    # -Werror=unused-local-typedefs 
    # -Werror=reorder 
    # -Werror=maybe-uninitialized 
    # -Werror=comment 
    # -Werror=parentheses

else()
    message(FATAL_ERROR "Error: Compiler ${CMAKE_CXX_COMPILER_ID} is not supported.")
endif()

# Debugging options
add_compile_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_ASAN}>>:-fno-omit-frame-pointer>)
add_compile_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_ASAN}>>:-fsanitize=address>)
add_compile_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_LSAN}>>:-fsanitize=leak>)
add_compile_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_MSAN}>>:-fsanitize=memory>)
add_compile_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_TSAN}>>:-fsanitize=thread>)
add_compile_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_USAN}>>:-fsanitize=undefined>)

add_link_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_ASAN}>>:-fno-omit-frame-pointer>)
add_link_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_ASAN}>>:-fsanitize=address>)
add_link_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_LSAN}>>:-fsanitize=leak>)
add_link_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_MSAN}>>:-fsanitize=memory>)
add_link_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_TSAN}>>:-fsanitize=thread>)
add_link_options($<$<AND:$<CONFIG:Debug>,$<BOOL:${FELISCE_WITH_USAN}>>:-fsanitize=undefined>)


# ------------------------------------------------------ #
# ------- InterProcedural Optimization ----------------- #
# ------------------------------------------------------ #
# include(CheckIPOSupported)
# check_ipo_supported(RESULT ipo_supported OUTPUT ipo_error)
# if(ipo_supported)
#     message(STATUS "[IPO Detection] InterProcedural Optimization is supported and enabled.")
#     set(CMAKE_INTERPROCEDURAL_OPTIMIZATION ON)
# else()
#     message(WARNING "[IPO Detection] InterProcedural Optimization is not supported: ${ipo_error}")
# endif()


# ------------------------------------------------------ #
# ------- Colored diagnostics -------------------------- #
# ------------------------------------------------------ #
set(CMAKE_COLOR_DIAGNOSTIC ON)


# ------------------------------------------------------ #
# ------- Linking options ------------------------------ #
# ------------------------------------------------------ #

# Set different RPATHs
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")

# macOS-specific RPATH handling
if(APPLE)
    set(CMAKE_MACOSX_RPATH ON)
endif()

# Ensure RPATH is used in the build tree
set(CMAKE_SKIP_BUILD_RPATH FALSE)

# Add linker paths to install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# Ensure build uses the install RPATH
set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)