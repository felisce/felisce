# Find the SLEPc includes and libraries
# 
# SLEPc: SLEPc is a software library for the solution of large scale sparse eigenvalue problems on parallel computers. 
# It is an extension of PETSc and can be used for linear eigenvalue problems in either standard or generalized form,
#  with real or complex arithmetic. It can also be used for computing a partial SVD of a large, sparse, rectangular matrix, 
# and to solve nonlinear eigenvalue problems (polynomial or general). 
# Additionally, SLEPc provides solvers for the computation of the action of a matrix function on a vector
# It can be found at:
#   https://slepc.upv.es
# 
# SLEPC_INCLUDE_DIR - where to find includes files
# SLEPC_LIBRARIES   - list of fully qualified libraries to link against.

# Check SLEPc if required
if(NOT FELISCE_WITH_SLEPC)
    message(STATUS "[SLEPc Detection] Compiling FELiScE without SLEPc support. To enable SLEPc, set the option FELISCE_WITH_SLEPC to ON.")
    return()
endif()
    
# Set SLEPC_DIR to PETSC_ARCH_DIR if not defined
set(SLEPC_DIR "${PETSC_ARCH_DIR}" CACHE STRING "SLEPc Directory")

if(SLEPC_DIR)
    message(STATUS "[SLEPc Detection] Searching for SLEPc in the user-provided directory: ${SLEPC_DIR}")
    set(INC_SEARCHING_PATHS "${SLEPC_DIR}")
    set(LIB_SEARCHING_PATHS "${SLEPC_DIR}")
    set(ERROR_MSG "")
else()
    message(STATUS "[SLEPc Detection] Searching for SLEPc in system directories.")
    set(INC_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(LIB_SEARCHING_PATHS "${CMAKE_SYSTEM_PREFIX_PATH}")
    set(ERROR_MSG "Please try setting SLEPC_DIR to the root of your SLEPc installation.")
endif()

# Search for the SLEPc include directory
felisce_find_include_dirs(NAME "SLEPc" DIRS "${INC_SEARCHING_PATHS}" INC "slepc.h" OUT SLEPC_INCLUDE_DIR ERR_MSG "${ERROR_MSG}")

# Find required SLEPc libraries (shared preferred, fallback to static)
felisce_find_library(NAME "SLEPc" DIRS ${LIB_SEARCHING_PATHS} LIB "slepc" OUT SLEPC_LIBRARIES OPT "" ERR_MSG "${ERROR_MSG}")

# Output message
message(STATUS "[SLEPc Detection] SLEPc successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${SLEPC_INCLUDE_DIR}")
    message("\t\t\t\tLibraries:")
    foreach(lib IN LISTS SLEPC_LIBRARIES)
        message("\t\t\t\t\t\t${lib}") 
    endforeach()
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${SLEPC_INCLUDE_DIR}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)

# Link third-party library binaries
target_link_libraries(FELiScE_dep_libraries INTERFACE ${SLEPC_LIBRARIES})

# Unset searching paths
unset(INC_SEARCHING_PATHS)
unset(LIB_SEARCHING_PATHS)