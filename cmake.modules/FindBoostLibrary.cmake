# Find the Boost:numeric:ublas includes (headers only library)
# 
# Boost_INCLUDE_DIRS - where to find includes files

if(BOOST_DIR)

    message(STATUS "[Boost Detection] Searching for Boost in the user-provided directory: ${BOOST_DIR}")

    # Search for the Boost include directory
    felisce_find_include_dirs(NAME "Boost" DIRS "${BOOST_DIR}/boost/numeric/ublas" INC "vector.hpp" OUT Boost_INCLUDE_DIRS_VEC ERR_MSG "")
    felisce_find_include_dirs(NAME "Boost" DIRS "${BOOST_DIR}/boost/numeric/ublas" INC "matrix.hpp" OUT Boost_INCLUDE_DIRS_MAT ERR_MSG "")

    set(Boost_INCLUDE_DIRS ${Boost_INCLUDE_DIRS_VEC} ${Boost_INCLUDE_DIRS_MAT})

else()

    message(STATUS "[Boost Detection] Searching for Boost in system directories.")

    # Find MPI package
    find_package(Boost REQUIRED QUIET)

    if(NOT BOOST_FOUND)
        message(FATAL_ERROR "[Boost Detection] Boost not found. Try to set BOOST_DIR to the Boost directory path.")
    endif()
endif()

# Output message
message(STATUS "[Boost Detection] Boost successfully found.")
if(FELISCE_CMAKE_VERBOSE)
    message("\t\t\t\tIncludes: ${Boost_INCLUDE_DIRS}")
endif()

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${Boost_INCLUDE_DIRS}> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)