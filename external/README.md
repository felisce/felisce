# External libraries

Here the external libraries not required to download are included. Contains:

- [**bitmap**](bitmap)
- [**CurveGenerator**](CurveGenerator)
- [**doctest**](doctest)
- [**getpot-cpp**](getpot-cpp)
- [**intrusive_ptr**](intrusive_ptr)
- [**libMeshb**](libMeshb)
- [**sciplot**](sciplot)
- [**tabulate**](tabulate)
- [**Xfm**](Xfm)
