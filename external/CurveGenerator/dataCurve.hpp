#ifndef __DATACURVE_HPP_
#define __DATACURVE_HPP_

#include <string>
#include <vector>
#include <ostream>

extern "C" {
  
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
  
}

// TO DO : use the possibility to return first and second derivative (spline and others...)

/*

 type 0: linear interpolation
 
  time_val = { time = {...}, val = {...} }
  then the value are linearly interpolated 
 
 type 1: Spline
 
  time_val = { time = {...}, val = {...} } as for type 0
 
    Example 1: spline quadratic over the first and last interval
      spline = {}
    
    Example 2: spline with given first derivative over the first interval, and second deriv over the last
 
    spline =  {
      first_knot_type = 1, first_knot_data = 1.,
      last_knot_type = 2, last_knot_data = 1/2.
    }

    Example 3: spline not-a-knot:
 
    spline = {
    first_knot_type = 3,
    last_knot_type = 3
    }
 
 type 2: Fourier cos / std::sin
 
 type 3: Fourier cos / phi  
 
 type 4: Fourier std::sin / phi
 
 */

class DataCurve{
  int m_type;
  double m_t_min;
  double m_t_max;
  std::string m_title;
  std::string m_info;
  std::vector<double> m_time;
  std::vector<double> m_val;
  std::vector<double> m_a_n;
  std::vector<double> m_b_n;
  std::vector<double> m_phi_n;
  double* m_spline;

  mutable std::vector<double> m_time_spline;// simple copy of time, mutable to be used with a C function that does not respect 'const'
  mutable std::vector<double> m_val_spline; // simple copy of val, mutable to be used with a C function that does not respect 'const'
  int m_spline_first_knot_type;
  /* 
   0: the cubic spline should be a quadratic over the first interval;
   1: the first derivative at the left endpoint should be m_spline_first_knot_data;
   2: the second derivative at the left endpoint should be m_spline_first_knot_data;
   3: Not-a-knot: the third derivative is continuous at m_time[1].
   */
  double m_spline_first_knot_data; // when knot_type = 1 : data = first deriv, when knot_type = 2, data = scd deriv
  int m_spline_last_knot_type;
  /*
  0: the cubic spline should be a quadratic over the last interval;
  1: the first derivative at the right endpoint should be m_spline_last_knot_data;
  2: the second derivative at the right endpoint should be m_spline_last_knot_data;
  3: Not-a-knot: the third derivative is continuous at m_time[n-2].
   */
  double m_spline_last_knot_data;// when knot_type = 1 : data = first deriv, when knot_type = 2, data = scd deriv

public:
  DataCurve(std::string filename);
  ~DataCurve();
  double val(const double& t) const;
  inline double operator() (const double& t) const {return val(t);}
  void dump(std::ostream& out,double t_start,double t_end,int dim) const;
  friend std::ostream& operator << (std::ostream& out,const DataCurve& dc);
  const int& type() const {return m_type;}
  const double& t_min() const {return m_t_min;}
  const double& t_max() const {return m_t_max;}
  const std::string& title() const {return m_title;}
  const std::string& info() const {return m_info;}
};
#endif
