#include "curveGen.hpp"
#include <algorithm>
#include <cmath>
#include <iostream>

double computeIntegral(const std::vector<double>& time, const std::vector<double>& val)
{
  if(time.size() != val.size()){
    std::cout << "Integral: size mismatch" << std::endl;
    exit(1);
  }
  double integ = 0.;
  for(std::size_t i=0;i<time.size()-1;i++){
    integ += 0.5 * (time[i+1] - time[i]) * (val[i+1] + val[i]);
  }
  return integ;
}

double computeIntegral(const std::vector<double>& time, const std::vector<double>& val,double t0,double t1)
{
  if(time.size() != val.size()){
    std::cout << "Integral: size mismatch" << std::endl;
    exit(1);
  }
  if (t0>t1){
    std::cout << "Integral: t0 > t1" << std::endl;
    exit(1);
  }
  if( t0 < time[0] ){
    std::cout << "Integral: t0 out of range" << std::endl;
    exit(1);
  }
  if( t1 > time[time.size()-1] ){
    std::cout << "Integral: t1 out of range" << std::endl;
    exit(1);
  }
  double integ = 0.;
  for(std::size_t i=0;i<time.size()-1;i++){
    if(time[i+1]>t0 && time[i] < t1)integ += 0.5 * (time[i+1] - time[i]) * (val[i+1] + val[i]);
  }
  return integ;
}

CurveGen::CurveGen(const CurveGen& cg):
m_dim(cg.m_dim),
m_t_min(cg.m_t_min),
m_t_max(cg.m_t_max),
m_val_min(cg.m_val_min),
m_val_max(cg.m_val_max),
m_verbose(cg.m_verbose)
{
  m_time.resize(m_dim);
  m_val.resize(m_dim);
  for(std::size_t i=0;i<m_dim;i++){
    m_time[i] = cg.m_time[i];
    m_val[i] = cg.m_val[i];
  }
}

CurveGen::CurveGen(const DataCurve& dc,std::size_t num_interval,int verbose):
m_dim(num_interval+1),m_verbose(verbose)
{
  m_time.resize(m_dim);
  m_val.resize(m_dim);
  m_t_min = dc.t_min();
  m_t_max = dc.t_max();
  double t,dt = (m_t_max - m_t_min) / (m_dim-1);
  for(std::size_t i=0;i<m_dim;i++){
    t = m_t_min + i*dt;
    m_time[i] = t;
    m_val[i] = dc.val(t);
  }
  m_t_min = *std::min_element(m_time.begin(),m_time.end());
  m_t_max = *std::max_element(m_time.begin(),m_time.end());
  m_val_min = *std::min_element(m_val.begin(),m_val.end());
  m_val_max = *std::max_element(m_val.begin(),m_val.end());
  if(m_verbose){
    std::cout << "- CurveGen: " << std::endl;
    std::cout << "  " << dc.title() << std::endl;
    std::cout << "  " << dc.info() << std::endl;
    std::cout << "  t_min = " << m_t_min << ", t_max = " << m_t_max << std::endl;
    std::cout << "  val_min = " << m_val_min << ", val_max = " << m_val_max << std::endl;
    std::cout << "  integral = " << integral() << std::endl;
  }
}

void CurveGen::rescaleTimeLinear(double new_t_min,double new_t_max){
  for(std::size_t i=0;i<m_dim;i++){
    m_time[i] = (new_t_max - new_t_min) * (m_time[i] - m_t_min)/(m_t_max - m_t_min) + new_t_min;
  }
  m_t_min = *std::min_element(m_time.begin(),m_time.end());
  m_t_max = *std::max_element(m_time.begin(),m_time.end());
  if(m_verbose){
    std::cout << "- CurveGen: new t_min = " << m_t_min << ", new t_max = " << m_t_max << std::endl;
  }
}

void CurveGen::rescaleValLinear(double new_val_min,double new_val_max){
  for(std::size_t i=0;i<m_dim;i++){
    m_val[i] = (new_val_max - new_val_min) * (m_val[i] - m_val_min)/(m_val_max - m_val_min) + new_val_min;
  }
  m_val_min = *std::min_element(m_val.begin(),m_val.end());
  m_val_max = *std::max_element(m_val.begin(),m_val.end());
  if(m_verbose){
    std::cout << "- CurveGen: new val_min = " << m_val_min << ", new val_max = " << m_val_max << std::endl;
  }
}

void CurveGen::rescaleVal(double scaling_factor){
  for(std::size_t i=0;i<m_dim;i++){
    m_val[i] = scaling_factor * m_val[i];
  }
  m_val_min = *std::min_element(m_val.begin(),m_val.end());
  m_val_max = *std::max_element(m_val.begin(),m_val.end());
  if(m_verbose){
    std::cout << "- CurveGen: new val_min = " << m_val_min << ", new val_max = " << m_val_max << std::endl;
  }
}

void CurveGen::rescaleValToMatchIntegral(double new_integral){
  double old_integral = integral();
  if(std::fabs(old_integral) < 1e-8 ){
    std::cout << "- CurveGen::rescaleValToMatchIntegral: integral value close to zero. Consider a shift."
    << std::endl;
    exit(1);
  }
  rescaleVal(new_integral/old_integral);
  if(m_verbose){
    std::cout << "- CurveGen: new integral = " << integral() << std::endl;
  }
}

void CurveGen::rescaleValToMatchIntegralOver60(double integral_over_60){
  /*
   A fake signal is computed by extending the signal by periodicity until t_max > t_min + 60
   The integral of the fake signal is computed over (t_min, t_min+60).
   The values of the real signal are then rescaled to match the given value of the integral.
   */

  int num_period = 1 + 60 / (m_t_max - m_t_min) ;
  CurveGen fake = *this;
  fake.extendByPeriodicity(num_period);
  rescaleVal(integral_over_60 / fake.integral(m_t_min, m_t_min + 60));
  if(m_verbose){
    std::cout << "- CurveGen: new integral over 60 = " << fake.integral(m_t_min, m_t_min + 60) << std::endl;
  }
}

void CurveGen::extendByPeriodicity(int num_period){
  std::vector<double> time_tmp = m_time;
  std::vector<double> val_tmp = m_val;
  std::size_t dim_tmp = m_dim;
  m_dim *= num_period;
  m_time.resize(m_dim);
  m_val.resize(m_dim);
  double T = m_t_max - m_t_min;
  m_t_max = m_t_min + num_period * T;
  std::size_t j;
  for(std::size_t i=0;i<m_dim;i++){
    j = i % dim_tmp;
    m_val[i] = val_tmp[j];
    m_time[i] = time_tmp[j] + i/dim_tmp * T;
  }
  if(m_verbose){
    std::cout << "- CurveGen: new t_min = " << m_t_min << ", new t_max = " << m_t_max << std::endl;
  }
}

void CurveGen::extendByPeriodicityUntilTime(double final_time){
  int num_period = ceil( final_time / (m_t_max - m_t_min) );
  extendByPeriodicity(num_period);
}

CurveGen CurveGen::derivative(bool is_periodic){
  /*
   Second order finite difference approximation of the first derivative
   The boundary terms are only first order, except if the signal is periodic (is_periodic = true)
   */
  CurveGen dcg;
  dcg.m_dim = m_dim;
  dcg.m_time.resize(m_dim);
  dcg.m_val.resize(m_dim);

  for(std::size_t i=0;i<m_dim;i++){
    dcg.m_time[i] = m_time[i];
  }

  if(is_periodic){
    dcg.m_val[0] = ((m_val[1]-m_val[0])/(m_time[1] - m_time[0])*(m_time[m_dim-1] - m_time[m_dim-2])
                    + (m_val[m_dim-1]-m_val[m_dim-2])/(m_time[m_dim-1] - m_time[m_dim-2])*(m_time[1] - m_time[0]) ) / (m_time[m_dim-1] - m_time[m_dim-2] + m_time[1] - m_time[0]);
    dcg.m_val[m_dim-1] = dcg.m_val[0];
  }else{
    dcg.m_val[0] = (m_val[1] - m_val[0])/(m_time[1] - m_time[0]);
    dcg.m_val[m_dim-1] = (m_val[m_dim-1] - m_val[m_dim-2])/(m_time[m_dim-1] - m_time[m_dim-2]);
  }
  for(std::size_t i=1;i<m_dim-1;i++){
    double frac = (m_time[i] - m_time[i-1])/(m_time[i+1] - m_time[i]);
    dcg.m_val[i] = ((m_val[i+1]-m_val[i])*frac
                    + (m_val[i]-m_val[i-1])/frac ) / (m_time[i+1] - m_time[i-1]) ;
  }

  dcg.m_t_min = *std::min_element(m_time.begin(),m_time.end());
  dcg.m_t_max = *std::max_element(m_time.begin(),m_time.end());
  dcg.m_val_min = *std::min_element(m_val.begin(),m_val.end());
  dcg.m_val_max = *std::max_element(m_val.begin(),m_val.end());

  return dcg;
}

double CurveGen::integral(){
  double integ = computeIntegral(m_time,m_val);
  return integ;
}

double CurveGen::integral(double t0,double t1){
  return computeIntegral(m_time,m_val,t0,t1);
}

double CurveGen::integralOver60(){
  /*
   A fake signal is computed by extending the signal by periodicity until t_max > t_min + 60
   The integral of the fake signal is computed over (t_min, t_min+60).
   */

  int num_period = 1 + 60 / (m_t_max - m_t_min) ;
  CurveGen fake = *this;
  fake.extendByPeriodicity(num_period);
  return fake.integral(m_t_min, m_t_min + 60);
}

double CurveGen::average(){
  return 1./(m_t_max-m_t_min) * computeIntegral(m_time,m_val);
}

double CurveGen::average(double t0,double t1){
  return 1./(t1-t0) * computeIntegral(m_time,m_val,t0,t1);
}

double CurveGen::operator()(double t){
  if(t<m_t_min || t>m_t_max) return 0.;
  std::size_t i = 1;
  while(t>m_time[i] && i<(m_dim-1)) i++;
  return (m_val[i]*(t-m_time[i-1])/(m_time[i] - m_time[i-1]) + m_val[i-1]*(m_time[i]-t)/(m_time[i] - m_time[i-1])  );
}

void CurveGen::dump(std::ostream& out){
  out.setf(std::ios_base::scientific);
  out.precision(5);
  out.width(12);
  for(std::size_t i=0;i<m_dim;i++){
    out << m_time[i] << " " << m_val[i] << std::endl;
  }
}
