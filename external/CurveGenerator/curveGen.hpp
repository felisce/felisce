#ifndef __CURVEGEN_HPP_
#define __CURVEGEN_HPP_
#include "dataCurve.hpp"
#include <vector>

double computeIntegral(const std::vector<double>& time,const std::vector<double>& val);
double computeIntegral(const std::vector<double>& time,const std::vector<double>& val,
                       double t0,double t1);

class CurveGen{
  std::size_t m_dim;
  std::vector<double> m_time;
  std::vector<double> m_val;
  double m_t_min;
  double m_t_max;
  double m_val_min;
  double m_val_max;
  int m_verbose;
public:
  CurveGen()= default;;
  CurveGen(const DataCurve& dc_,std::size_t num_interval,int verbose=1);
  CurveGen(const CurveGen& cg);
  CurveGen derivative(bool is_periodic=false);
  std::size_t dim() const {return m_dim;}
  void rescaleTimeLinear(double new_t_min,double new_t_max);
  void rescaleVal(double scaling_factor);
  void rescaleValLinear(double new_val_min,double new_val_max);
  void rescaleValToMatchIntegral(double new_integral);
  void rescaleValToMatchIntegralOver60(double integral_over_60);
  void extendByPeriodicity(int num_period);
  void extendByPeriodicityUntilTime(double final_time);
  void dump(std::ostream& out);
  double t_min() const{return m_t_min;}
  double t_max() const{return m_t_max;}
  double val_min() const{return m_val_min;}
  double val_max() const{return m_val_max;}
  double integral();
  double integralOver60();
  double integral(double t0,double t1);
  double average();
  double average(double t0,double t1);
  double operator() (double t); // return a value for any t. If t < t_min  or t > t_max return 0.
};
#endif
