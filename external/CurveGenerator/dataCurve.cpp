#include "dataCurve.hpp"
#include "spline.h"
#include <algorithm>       // std::numeric_limits
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <limits>       // std::numeric_limits

#include <sys/types.h> // to test if the file exists
#include <sys/stat.h>

DataCurve::~DataCurve() {
  if (m_spline != nullptr )
    delete [] m_spline;
}


DataCurve::DataCurve(std::string filename)
{
  m_spline = nullptr;

  struct stat info;
  if( stat( filename.c_str(), &info ) != 0 ){
    std::cout << "DataCurve::DataCurve cannot access " << filename << std::endl;
    exit(1);
  }


  lua_State* L = luaL_newstate();

  if(luaL_loadfile(L,filename.c_str()) || lua_pcall(L,0,0,0))
    printf("Error failed to load %s",lua_tostring(L,-1));

  lua_getglobal(L,"title");
  m_title = lua_tostring(L, -1);

  lua_getglobal(L,"info");
  m_info = lua_tostring(L, -1);

  lua_getglobal(L, "type");
  m_type = lua_tonumber(L,-1);


  if(m_type==0 || m_type==1){
    //======== Time values ============
    lua_getglobal(L, "time_val");

    lua_getfield(L, -1, "time");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_time.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);

    lua_getfield(L, -1, "val");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_val.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);

    m_t_min = *std::min_element(m_time.begin(),m_time.end());
    m_t_max = *std::max_element(m_time.begin(),m_time.end());

    if(m_val.size() != m_time.size() ){
      std::cout << "DataCurve: The 'time' and 'val' data have different sizes" << std::endl;
      exit(1);
    }

    if(m_type == 1){
      m_spline_first_knot_type = 0;
      m_spline_last_knot_type = 0;
      m_spline_first_knot_data = 0.;
      m_spline_last_knot_data = 0.;

      //======== Time values - cubic spline ============
      lua_getglobal(L, "spline");

      lua_getfield(L, -1, "first_knot_type");
      m_spline_first_knot_type = lua_tonumber(L,-1);

      lua_getfield(L, -2, "first_knot_data");
      m_spline_first_knot_data = lua_tonumber(L,-1);

      lua_getfield(L, -3, "last_knot_type");
      m_spline_last_knot_type = lua_tonumber(L,-1);

      lua_getfield(L, -4, "last_knot_data");
      m_spline_last_knot_data = lua_tonumber(L,-1);

      //std::cout << " first knot : " << m_spline_first_knot_type << ", " << m_spline_first_knot_data << std::endl;
      //std::cout << " last knot : " << m_spline_last_knot_type << ", " << m_spline_last_knot_data << std::endl;

      m_spline = spline_cubic_set(m_time.size(), &m_time[0], &m_val[0],
                                  m_spline_first_knot_type, m_spline_first_knot_data,
                                  m_spline_last_knot_type, m_spline_first_knot_data) ;
      m_val_spline = m_val;
      m_time_spline = m_time;
    }
  }




  if(m_type==2){
    //======== Fourier cos std::sin ============
    lua_getglobal(L, "t_min");
    m_t_min = lua_tonumber(L,-1);

    lua_getglobal(L, "t_max");
    m_t_max = lua_tonumber(L,-1);

    lua_getglobal(L, "fourier_cos_sin");

    lua_getfield(L, -1, "a_n");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_a_n.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);

    lua_getfield(L, -1, "b_n");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_b_n.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);
    if(m_a_n.size() != m_b_n.size() ){
      std::cout << "DataCurve: The a_n and b_n Fourier coefficients have different sizes" << std::endl;
      exit(1);
    }
    if(m_b_n[0] != 0){
      std::cout << "DataCurve: Be careful b_n[0] is non-zero. Are you sure ?" << std::endl;
    }

    //==============================
  }

  if(m_type==3){
    //======== Fourier cos phi ============
    lua_getglobal(L, "t_min");
    m_t_min = lua_tonumber(L,-1);

    lua_getglobal(L, "t_max");
    m_t_max = lua_tonumber(L,-1);

    lua_getglobal(L, "fourier_cos_phi");

    lua_getfield(L, -1, "a_n");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_a_n.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);

    lua_getfield(L, -1, "phi_n");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_phi_n.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);
    if(m_a_n.size() != m_phi_n.size() ){
      std::cout << "DataCurve: The a_n and phi_n coefficients have different sizes" << std::endl;
      exit(1);
    }
    //==============================
  }

  if(m_type==4){
    //======== Fourier std::sin phi ============
    lua_getglobal(L, "t_min");
    m_t_min = lua_tonumber(L,-1);

    lua_getglobal(L, "t_max");
    m_t_max = lua_tonumber(L,-1);

    lua_getglobal(L, "fourier_sin_phi");

    lua_getfield(L, -1, "b_n");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_b_n.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);

    lua_getfield(L, -1, "phi_n");
    lua_pushnil(L);
    while(lua_next(L,-2)){
      if(lua_isnumber(L,-1)){
        m_phi_n.push_back( lua_tonumber(L,-1) );
      }
      lua_pop(L,1);
    }
    lua_pop(L,1);
    if(m_b_n.size() != m_phi_n.size() ){
      std::cout << "DataCurve: The b_n and phi_n coefficients have different sizes" << std::endl;
      exit(1);
    }
    if(m_b_n[0] != 0){
      std::cout << "DataCurve: Be careful b_n[0] is non-zero. Are you sure ?" << std::endl;
      exit(1);
    }
    //==============================
  }
  lua_close(L);
}

double DataCurve::val(const double& t) const{
  double val;
  switch(m_type){
    case 0:
    {
      if(t<m_t_min || t>m_t_max) return 0.;
      std::size_t i = 1;
      while(t>m_time[i] && i<(m_time.size()-1)) i++;
      return (m_val[i]*(t-m_time[i-1])/(m_time[i] - m_time[i-1]) + m_val[i-1]*(m_time[i]-t)/(m_time[i] - m_time[i-1])  );
      break;
    }
    case 1:
    {
      double ypval,yppval;
      return spline_cubic_val (m_time.size(),&m_time_spline[0],&m_val_spline[0],m_spline,t, &ypval, &yppval );
    }
    case 2:
    {
      val = m_a_n[0];
      double T = m_t_max - m_t_min;
      for(std::size_t i=1; i<m_a_n.size(); i++)
        val += m_a_n[i] * std::cos(2 * M_PI * i * t / T) + m_b_n[i] * std::sin(2 * M_PI * i * t / T);
      break;
    }
    case 3:
    {
      val = m_a_n[0];
      double T = m_t_max - m_t_min;
      for(std::size_t i=1; i<m_a_n.size(); i++)
        val += m_a_n[i] * std::cos(2 * M_PI * i * t / T + m_phi_n[i]);
      break;
    }
    case 4:
    {
      val = 0.;
      double T = m_t_max - m_t_min;
      for(std::size_t i=1; i<m_b_n.size(); i++)
        val += m_b_n[i] * std::sin(2 * M_PI * i * t / T + m_phi_n[i]);
      break;
    }
    default:
      std::cout << "DataCurve::val Unknown type" << std::endl;
      exit(1);
  }
  return val;
}

void DataCurve::dump(std::ostream& out,double t_start,double t_end,int dim) const{
  out.setf(std::ios_base::scientific);
  out.precision(5);
  out.width(12);
  double dt = (t_end - t_start) / dim,t;
  for(int i=0;i<=dim;i++){
    t = t_start + i*dt;
    out << t << " " << val(t) << std::endl;
  }
}

std::ostream& operator << (std::ostream& out,const DataCurve& dc){
  out << "title = '" << dc.m_title << "'" << std::endl;
  out << "info = '" << dc.m_info << "'" << std::endl;
  out << "type = " << dc.m_type << std::endl;
  out << "t_min = " << dc.m_t_min << std::endl;
  out << "t_max = " << dc.m_t_max << std::endl;
  if(dc.m_type==2){
    out << "FourierCoef = {" << std::endl;
    out << "a_n = {" << std::endl;
    for(std::size_t i=0;i<dc.m_a_n.size()-1;i++) out << dc.m_a_n[i] << ",";
    out << dc.m_a_n[dc.m_a_n.size()-1] << "}" << std::endl;
    out << "}," << std::endl;
    out << "b_n = {" << std::endl;
    for(std::size_t i=0;i<dc.m_b_n.size()-1;i++) out << dc.m_b_n[i] << ",";
    out << dc.m_b_n[dc.m_b_n.size()-1] << "}" << std::endl;
    out << "}" << std::endl;
  }
  return out;
}

