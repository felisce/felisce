# Purpose

Curvegen provides 

- A database of "curves" (time dependent quantities): flow rate, pressure, ...
- A library of functions to easily manipulate those curves (rescale, extend, etc.)

# Installation

Requirement: Curvegen only needs the library [lua](http://www.lua.org). In order to compile and include Curvegen with **FELiScE** add to your configure the following: `-DFELISCE_WITH_CURVEGEN=ON  \`

# Test

To see what you can do with curvegen, take a look at the test folder. In order to visualize the results:

```
$ python postproc.py
```

# Data

They are in the Data [repository](https://gitlab.inria.fr/felisce/extra/data/-/tree/master/CurveGenerator). All the data have to be in this directory. The idea is to gather a database a functions. So do not hesitate to add new ones to svn (with an explicit name).

Format of the data:

- type 0: give values and time instants: linear interpolation. See e.g. testlinear.dat
- type 1: give values and time instants: spline interpolation. See e.g. testspline.dat
- type 2: give the cos and sin coefficients of Fourier expansion. See e.g. q_asc_aorta_rest_miccai.dat
- type 3: give the cos and phi coefficients of Fourier expansion. See e.g. p_asc_aorta_rest_miccai.dat 
- type 4: give the sin and phi coefficients of Fourier expansion. See e.g. q_asc_aorta_rest_miccai_bis_deriv.dat




