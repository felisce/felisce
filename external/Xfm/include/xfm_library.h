

extern "C"
{

typedef short int sint2d[2];
typedef short int sint3d[3];
typedef short int sint4d[4];
typedef short int sint5d[5];
  
typedef int int1d;
typedef int int2d[2];
typedef int int3d[3];
typedef int int4d[4];
typedef int int5d[5];
typedef int int6d[6];
typedef int int7d[7];
typedef int int8d[8];
typedef int int9d[9];
typedef int int10d[10];
typedef int int12d[12];

typedef double double1d;
typedef double double2d[2];
typedef double double3d[3];
typedef double double4d[4];
typedef double double5d[5];
typedef double double6d[6];
typedef double double7d[7];
typedef double double8d[8];
typedef double double9d[9];

enum Operation  {None, ConfCutByItfMesh, NonConfCutByItfMesh, CutByLvlSet, ComputeLvlSetFromItf, CrackPropagation}; 
/*

  *************************  ATTENTION ************************************************

  Ne peux pas allouer plusieurs XFEM sur la m�me machine et les tourner en parallele en thread 
  Ne peux pas appeler en parallel chaque fonction de l'interface avec le meme XFEM
  Car 2 variables globales sont utilisees

  All the arrays are starting at the index 1 ! It means that one needs to allocate one
  more element when allocating these arrays AND reads the array starting at 1 and ending
  at size+1.

  *************************  ATTENTION ************************************************

*/
// --------------------------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------- //
// ------------------------------ XFEM MODULE -------------------------------------------- //
// --------------------------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------- //
// --- Allocate XFEMLibrary data structure: return XFM pointer which is needed for each call
//
// Inputs
// --- Dim       : space dimension
// --- ItfNam    : filename of the intersected interface mesh (not use right now)
// --- MshNam    : filename of the intersected mesh (not use right now)
// --- Operation : operation to be performed
// --- Verbosity : less than 6 almost nothing, 6 all the data + CPU
// --- Sort      : 0=No sort of the meshes data, 1=sort the meshes data
//
// Outputs
// --- void *XFM : pointer to the data structure
void * Xfm_NewLibrary(int1d Dim, char *ItfNam, char *MshNam, Operation Operation, int1d Verbosity, int1d Sort);


// --- Free XFEMLibrary data structure
//
// Inputs
// --- XFM : data structure
void * Xfm_FreeLibrary(void *XFM);



// --- Set XFEMLibrary epsilon for mesh intersection. Default value is 10^{-5}
//
// Inputs
// --- XFM     : data structure
// --- Espilon : epsilon value
void Xfm_SetIntersectionEpsilon(void *XFM, double Epsilon);



// --- Set XFEMLibrary Verbose for mesh intersection
//
// Inputs
// --- XFM       : data structure
// --- Verbosity : verbose value
void Xfm_SetVerboseValue(void *XFM, int1d Verbosity);



// --- Set XFEMLibrary Debug for mesh intersection
//
// Inputs
// --- XFM       : data structure
// --- Debug     : debug value
void Xfm_SetDebugValue(void *XFM, int1d Debug);



// --- Set XFEMLibrary CrackMod for crack algorithm
//
// Inputs
// --- XFM       : data structure
// --- CrackMod  : crack value
void Xfm_SetCrackMod(void *XFM, int1d CrackMod);



// --- Initialize Mesh and Interface, Intersected Mesh and Interface, Mesh and Interface XFEM Data
//
// Inputs
// --- XFM    : data structure
// --- ItfNam : filename of the interface mesh
// --- MshNam : filename of the mesh
void Xfm_LibraryInitialization_File(void *XFM, char *ItfNam, char *MshNam);


// --- Initialize Mesh and Interface, Intersected Mesh and Interface, Mesh and Interface XFEM Data
//
// Inputs
// --- XFM           : data structure
// --- Msh_NbrVer    : number of vertices in the mesh
// --- Msh_Crd       : 3d coordinates of the vertices of the mesh (start at index 1, has the size Msh_NbrVer + 1)
// --- Msh_NbrElt_3D : number of element in the mesh
// --- Msh_Elt_3D    : id of the vertices of the tetrahedra of the mesh (start at index 1, has the size Msh_NbrElt + 1, NULL if in 2D)
// --- Msh_NbrIntElt_1D : number of internal elements 2D
// --- Msh_NbrElt_2D : number of element in the mesh (number of boundary elements if in 3D)
// --- Msh_Elt_2D    : id of the vertices of the triangles of the mesh (start at index 1, has the size Msh_NbrElt + 1, NULL or boundary elements if in 3D)
//                     !!!! It is a 4D array like tets !!!!
// --- Msh_NbrIntElt_1D : number of internal elements 1D
// --- Msh_NbrElt_1D : number of boundary elements ( NULL if in 3D)
// --- Msh_Elt_1D    : id of the vertices of the edges of the mesh (start at index 1, has the size Msh_NbrElt + 1, NULL if in 3D)
// --- Itf_NbrVer    : number of vertices in the interface mesh
// --- Itf_Crd       : 3d coordinates of the vertices of the interface mesh (start at index 1, has the size Itf_NbrVer + 1)
// --- Itf_NbrElt    : number of element of the interface mesh
// --- Itf_Elt_3D    : id of the vertices of the triangles of the interface mesh (start at index 1, has the size Itf_NbrElt + 1, NULL if in 2D)
//                     !!!! It is a 4D array !!!!
// --- Itf_Elt_2D    : id of the vertices of the edges of the interface mesh (start at index 1, has the size Itf_NbrElt + 1, NULL if in 3D)
//                     !!!! It is a 3D array !!!!
void Xfm_LibraryInitialization_Array(void *XFM, int1d Msh_NbrVer, double3d *Msh_Crd, int1d Msh_NbrElt_3D, int4d *Msh_Elt_3D, int1d Msh_NbrIntElt_2D, int1d Msh_NbrElt_2D, int4d *Msh_Elt_2D, int1d Msh_NbrIntElt_1D, int1d Msh_NbrElt_1D, int3d *Msh_Elt_1D,
                                                int1d Itf_NbrVer, double3d *Itf_Crd, int1d Itf_NbrElt,    int4d *Itf_Elt_3D, int3d *Itf_Elt_2D, int1d *Itf_Elt_Ref);

// --- 3D Initialization when crack is provided by level sets
void Xfm_LibraryInitialization_Array_LS(void *XFM, double1d *phi, double1d *psi);

// --- 3D Initialization when crack is provided by a mesh file
// 
// Outputs: 
// --- phi, psi level sets
void Xfm_LibraryInitialization_Array_Mesh(void *XFM, int1d Msh_NbrVer, double3d *Msh_Crd, int1d Msh_NbrElt_3D, int4d *Msh_Elt_3D, int1d Msh_NbrElt_2D, int4d *Msh_Elt_2D);



// ---- Interface initialization 
//
void Xfm_LibraryInitialization_Array_Itf(void *XFM, int1d Itf_NbrVer, double3d *Itf_Crd, int1d Itf_NbrFac, int4d *Itf_Fac, int1d Itf_NbrEdg, int3d *Itf_Edg);



// --- Compute the intersection between the Mesh and the Interface
//
// Inputs
// --- XFM      : data structure
void Xfm_LibraryIntersection(void *XFM, int1d TODO);

// --- Compute the intersection between the Mesh and the level set
void Xfm_LibraryIntersection_LS(void *XFM);

// --- Compute level sets from mesh surface and volume mesh.
void Xfm_LibraryComputeCrackLevelSets(void *XFM);

// --- Compute crack front basis vector of interface mesh (itf)
//     
// It computes only basis on front vertices. Basis vector for others
// egde vertices are set to 0
void  Xfm_LibraryComputeCrackFrontBasis(void *XFM);


// --- Propagate the crack (Interface) and compute the intersection between the Mesh and the new Interface
//
// Inputs
// --- XFM      : data structure
void Xfm_LibraryPropagateCrack(void *XFM, int1d NbrVerFro, double3d *VecPro);


// --- Initialize the crack front ie set-up data defining the front from the given interface mesh
//
// Requirement
// --- Itf    : Crack surface mesh
// --- XfmItf : Crack data
//
// Inputs
// --- XFM : data structure
void Xfm_LibraryInitializeCrack3d(void *XFM);


// --- Propagate the crack front (growth of the interface mesh) from the given propagation vector field
//
// Requirement
// --- Itf    : Crack surface mesh
// --- XfmItf : Crack data
//
// Inputs
// --- ItePro    : Index of the propagation iteration
// --- NbrVecPro : Number of propagation vectors
// --- VecPro    : Propagation vector list 
void Xfm_LibraryPropagateCrack3d(void *XFM, int ItePro, int1d NbrVecPro, double3d *VecPro);


// --- Update mesh interface coordinates
//
// Inputs
// --- XFM    : data structure
// --- NbrVer : number of vertices of the interface
// --- Crd    : 3d coordinates of the vertices (start at 1, size NbrVer + 1)
void Xfm_LibraryUpdateInterfaceCoordinates(void *XFM, int1d NbrVer, double3d *Crd);


// --- Re-initialize Xfm structure and mesh for new intersection
//
// Inputs
// --- XFM    : data structure
void Xfm_LibraryReinitializeMeshesAndDataStructures(void *XFM);


// --- Update level sets from the crack mesh surface. This function is used during crack propagation when level sets must be computed from the expanded mesh surface
//  
// Outputs:
// --- phi, psi level sets 
void  Xfm_LibraryUpdateCrackLevelSets(void *XFM, double1d **phi, double1d **psi);


// --- Write level sets in ".sol" format
//
// Inputs
// --- XFM    : data structure
// --- SolNam : output file name 
void Xfm_LibraryWriteLevelSets(void *XFM, char *SolNam);


// --- Write 3d array in sol file
//
void Xfm_LibraryWriteSolArray3d(char *SolNam, int1d NbrSol, double3d *Sol);


// --- Write resulting intersected mesh in ".mesh(b)" format
//
// Inputs
// --- Mod    : 0 = write also the intersected interface mesh / 1 = ignore the intersected interface mesh
// --- XFM    : data structure
// --- MshNam : filename of the out mesh
void Xfm_LibraryWriteIntersectMesh(int1d Mod, void *XFM, char *MshNam);


// --- Write resulting intersected interface mesh in ".mesh(b)" format
//
// Inputs
// --- XFM    : data structure
// --- MshNam : filename of the out interface mesh
void Xfm_LibraryWriteIntersectInterfaceMesh(void *XFM, char *ItfNam);


// --- Write resulting merged mesh (ie initial mesh union intersected mesh) in ".mesh(b)" format
//
// Inputs
// --- XFM    : data structure
// --- MshNam : filename of the out mesh
void Xfm_LibraryWriteMergedMesh(void *XFM, char *MshNam);


// --- Write Interface mesh
//
void Xfm_LibraryWriteInterfaceMesh(void *XFM, char *MshNam);


// --- Write Tip element mesh
//
void Xfm_LibraryWriteTipElementMesh(void *XFM, char *MshNam);

void Xfm_LibraryWriteTipElement(char *MshNam, int1d Dim, int1d NbrVer, double3d *Crd, int4d *PtrTipSrf, int1d *LstTipSrf, int2d *TipSrfSgn, int4d *Fac, int1d iElt);
void Xfm_LibraryWriteElement(char *MshNam, int1d Dim, int1d NbrVer, double3d *Crd, int1d *PtrIntElt, int1d *LstIntElt, int1d *IntEltSgn, int4d *Tet, int1d iElt);



// --- Write Backgroung mesh
//
void Xfm_LibraryWriteBackgroundMesh(void *XFM, char *MshNam);



// --------------------------------------------------------- //
// --- The following function returns data for the users --- //
// --------------------------------------------------------- //

// --- All data (typ* for values and typ** for pointers) have to be provided by reference to be updated --- //


// --- Return all data of the mesh
//
// Inputs
// --- XFM          : data structure
//
// Outputs
// --- SizLstIntElt : number of element in the intersected mesh
// --- PtrIntElt    : for each id of an element in the original mesh, gives the index of LstIntElt where its first sub element is.
//                    The number of sub element of the i-th original element is PtrIntElt[i+1] - PtrIntElt[i]. This array has the
//                    size of the total number of element in the original mesh + 1 and starts at index 1
// --- LstIntElt    : list of the sub elements (starts at 0, size SizLstIntElt, see PtrIntElt to loop over them)
// --- SizLstItfElt : number of element in the intersected interface mesh
// --- PtrItfElt    : for each id of an element in the original mesh, gives the index of LstItfElt where its first interface sub
//                    element is. The number of sub interface element of the i-th original element is PtrItfElt[i+1] - PtrItfElt[i].
//                    This array has the size of the total number of element in the original mesh + 1 and starts at index 1.
// --- LstItfElt    : list of the sub interface elements (starts at 0, size SizLstItfElt, see PtrItfElt to loop over them)
// --- VerSgn       : side of the interface to where this MESH vertex is (start at 1, size NbrVer + 1 (of the given mesh))
//
void Xfm_LibraryReturnMeshData(void *XFM, int1d *SizLstIntElt, int1d **PtrIntElt, int1d **LstIntElt,
                                          int1d *SizLstItfElt, int1d **PtrItfElt, int1d **LstItfElt, int1d **VerSgn);

// --- Return all data of boundaries of the mesh
//
// Inputs
// --- XFM          : data structure
//
// Outputs
// --- SizLstBndElt : number of element in the boundary intersected mesh
// --- PtrBndElt    : for each id of a boundary element in the original mesh, gives the index of LstBndElt where its first sub element is.
//                    The number of sub element of the i-th original boundary element is PtrBndElt[i+1] - PtrBndElt[i]. This array has the
//                    size of the total number of element in the original mesh + 1 and starts at index 1
// --- LstBndElt    : list of the sub boundary elements (starts at 0, size SizLstBndElt, see PtrBndElt to loop over them)
// --- NbrElt       : number of boundary elements in the intersected interface mesh
// --- Elt_2D       : id of the vertices of the edges (start at 1, size NbrElt + 1)
// --- Elt_3D       : id of the vertices of the triangles (start at 1, size NbrElt + 1)
//
void Xfm_LibraryReturnBndMeshData(void *XFM, int1d *SizLstBndElt, int1d **PtrBndElt, int1d **LstBndElt, int1d *NbrElt, int3d **Elt_2D, int4d **Elt_3D);

// --- Return all data of the interface mesh
//
// Inputs
// --- XFM          : data structure
//
// Outputs
// --- SizLstIntElt : number of element in the intersected interface mesh
// --- PtrIntElt    : For each id of an element of the original interface element, gives the index in LstIntElt where is first sub 
//                    element is. The number of sub interface element of the i-th original element is PtrIntElt[i+1] - PtrIntElt[i].
//                    This array has the size of the total number of interface element in the original interface mesh + 1 and starts
//                    at index 1.
// --- LstIntElt    : list of the sub interface elements (starts at 0, size SizLstIntElt, see PtrIntElt to loop over them).
//
void Xfm_LibraryReturnInterfaceMeshData(void *XFM, int1d *SizLstIntElt, int1d **PtrIntElt, int1d **LstIntElt);


// --- Return all data of the intersected mesh
//
// Inputs
// --- XFM        : data structure
//
// Outputs
// --- NbrVer     : number of vertices of the intersected mesh
// --- Crd        : 3d coordinates of the vertices (start at 1, size NbrVer + 1)
// --- IntVerIdx  : Index of Mesh vertex similar to the vertex of Intersect Mesh, =0 if not a vertex of the mesh
//                  This index is <0 if it is a degenerated case, ie a interface or intersection vertex coincides with a mesh vertex
// --- VerSgn     : side of the interface to where this intersected mesh vertex is. =0 if on the interface. (start at 1, size NbrVer + 1)
// --- NbrElt     : number of element of the intersected mesh
// --- Elt        : id of the vertices of the element of the intersected mesh (start at 1, size NbrElt + 1)
// --- IntEltIdx  : for each element of the intersected mesh, id of the element in the original mesh to which it belongs (start at 1, size NbrElt + 1)
// --- EltSgn     : side of the interface to where this element is (start at 1, size NbrElt + 1)
void Xfm_LibraryReturnIntersectMeshData(void *XFM, int1d *NbrVer, double3d **Crd, int1d **IntVerIdx, int1d **IntVerSgn,  
                                        int1d *NbrElt, int4d **Elt, int1d **IntEltIdx, int1d **EltSgn);


// --- Return all data of the intersected mesh
//
// Inputs
// --- XFM        : data structure
//
// Outputs
// --- SizLstTipSrf : number of sub tip element in the intersected mesh
// --- PtrTipSrf    : for each id of a tip element in the original mesh, gives the index of LstIntElt where its first sub element is.
//                    The number of sub element of the i-th original element is PtrIntElt[i+1] - PtrIntElt[i]. This array has the
//                    size of the total number of element in the original mesh + 1 and starts at index 1
// --- LstTipSrf    : list of the sub tip elements (starts at 0, size SizLstIntElt, see PtrIntElt to loop over them)
// --- NbrTip       : number of tip element
// --- IntTipIdx    : for each tip element (big one), id of the element in the original mesh
// --- NbrTipSrf    : number of sub-faces in a tip element (sub-faces = all the splitted faces of the initial tetrahedron)
// --- TipSrf3D     : id of the vertices of the triangles (start at 1, size NbrTipSrf + 1)
// --- TipSuf2D     : id of the vertices of the edges (start at 1, size NbrTipSrf + 1)
// --- TipSrfSgn    : side of the tip sub-face to where this element is (start at 1, size NbrElt + 1)
void Xfm_LibraryReturnTipMeshData(void *XFM, int1d *SizLstTipSrf, int4d **PtrTipSrf, int1d **LstTipSrf, int1d *NbrTip, 
                                  int1d **IntTipIdx, int1d *NbrTipSrf, int4d **TipSrf3D, int2d **TipSuf2D, int2d **TipSrfSgn);


// --- Return all data of the front points
//
// Inputs
// --- XFM        : data structure
//
// Outputs
// --- NbrVerFro  : number of interface front vertex
// --- VerFro     : interface vertices front data (start at 1, size NbrVerFro + 1)
//                  VerFro[i][0] = MshInt (Itf) vertex id, VerFro[i][1] = INTERSECTED MESH element id containing the vertex (use IntEltIdx to get MESH elt id)
//                  VerFro[i][2] = MshInt opposite vertex which defines with this vertex the edge separating + and - regions 
// --- VecFro     : interface vertices front vectors (start at 1, size NbrVerFro + 1): 2D E_gamma / 3D E_gamma and N_gamma
void Xfm_LibraryReturnFrontPointsData(void *XFM, int1d *NbrVerFro, int3d **VerFro, double6d **VecFro);


void Xfm_LibraryReturnIntersectingItfIdx(void *XFM, int1d **MshItfIdx, int1d **BndItfIdx);



// --- Return all data of the intersected ls mesh
//
// Inputs
// --- XFM        : data structure
//
// Outputs
void Xfm_LibraryReturnIntersectMeshData_LS(void *XFM, 
                                           int1d *IntNbrVer, double3d **IntCrd, 
                                           int1d **IntVerIdx, int1d **IntVerSgn, 
                                           int1d *IntNbrElt, int4d **IntElt, int1d **IntSupElt, int1d **IntEltSgn, 
                                           int1d *ItfNbrVer, double3d **ItfCrd, int1d **ItfBij, 
                                           int1d *ItfNbrElt, int4d **ItfElt, int1d **ItfRefElt, int1d *ItfNbrEfr, int3d **ItfEfr,
                                           int1d *FroNbrVer, int3d **FroVer, int1d *FroNbrEdg, int3d **FroEdg, 
                                           int1d *FroNbrVerFak, int2d **FroVerFak, double6d **FroBas);


// --- Return all data of the intersected interface mesh
//
// Inputs
// --- XFM    : data structure
//
// Outputs
// --- NbrVer : number of vertices in the intersected interface mesh
// --- Crd    : 3d coordinates of the vertices in the intersected interface mesh (start at 1, size NbrVer + 1)
// --- NbrElt : number of elements in the intersected interface mesh
// --- Elt_2D : id of the vertices of the edges (start at 1, size NbrElt + 1)
// --- Elt_3D : id of the vertices of the triangles (start at 1, size NbrElt + 1)
void Xfm_LibraryReturnIntersectInterfaceMeshData(void *XFM, int1d *NbrVer, double3d **Crd, int1d *NbrElt, int3d **Elt_2D, int4d **Elt_3D,  
                                                 int1d **Elt2MshElt, int1d **Elt2ItfElt);


// --- Return 3D interface mesh data (to use after propagation)
//
// Outputs:
// --- NbrVer: number of vertices
// --- Crd: coordinates of vertices
// --- NbrElt: number of element
// --- Elt: element connectivity
// --- NbrFroEdg: number of edges (edges correspond to the crack front)
// --- FroEdg: egdes connectivity
// --- NbrFroVer: number of vertices on crack front
// --- FroVer: array of front vertices (see Xfm_LibraryReturnIntersectMeshData)
//
void Xfm_LibraryReturnCrackInterfaceMeshData(void *XFM, int1d *NbrVer, double3d **Crd, 
                                             int1d *NbrElt, int4d **Elt, int1d *NbrFroEdg,
                                             int3d **FroEdg, int1d *NbrFroVer, int3d **FroVer,
                                             int1d **FroVerTag);


// --- Return crack front data
//
// This function is used after init front crack 3D in order to get froVer from edges.
//
// Outputs:
// --- NbrFroVer: number of crack front vertices 
// --- FroVer: array of front vertices (Xfm_LibraryReturnIntersectMeshData)
//
void Xfm_LibraryReturnCrack3dFrontData(void *XFM, int1d *NbrFroVer, int3d **FroVer, int1d **FroVerTag);


// --- Return vertices on interface edge
//
// Outputs:
// --- NbrVerEdg: number of vertices on edge
// --- VerEdg: list of vertice index on edge
//     VerEdg[0]: vertex index
//     VerEdg[1]: index in VerEdg[0] of first neighboor
//     VerEdg[2]: index in VerEdg[0] of second neighboor
//     VerEdg[3]: default -1, any container
//
void Xfm_LibraryReturnInterfaceEdgeVertices(void *XFM, int *NbrVerEdg, int4d **VerEdg);


// --- Return front basis vector at vertices
//
// Outputs:
// --- VecFro: array of crack front basis vector
//
void  Xfm_LibraryReturnCrackFrontBasis(void *XFM, double6d **VecFro);


// --- Return element index in back mesh which
//     contains front vertices
//
// Inputs
// --- germ: an element index for the first germ
//
  void Xfm_LibraryReturnFrontLocalizationInBackMesh(void *XFM, int germ, int1d **idxElt);

  
// --- Return all data for crack problem
//
// Inputs
// --- XFM          : data structure
//
// Outputs
// --- IntVerCrkMap : Intersected mesh vertices interface crack map (start at 1, size NbrVer + 1)
//                    if a duplicated vertex, idx of its associated vertex. Else, =0.
// --- IntVerShfSgn : Intersected mesh elements shifted sign 
void Xfm_LibraryReturnCrackProblemData(void *XFM, int1d **IntVerCrkMap, int4d **IntEltShfSgn);
              

// --- Return level sets from surface mesh. 
//
// Outputs:
// --- phi, psi level sets 
void Xfm_LibraryReturnCrackLevelSets(void *XFM, double1d **phi, double1d **psi);



// --------------------------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------- //
// ------------------------------- M1G MODULE -------------------------------------------- //
// --------------------------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------- //
// --- Allocate M1GLibrary data structure: return M1G pointer which is needed for each call
//
// Inputs
// --- Dim       : space dimension
// --- MshNam    : filename of the mesh (not use right now)
// --- Verbosity : less than 6 almost nothing, 6 all the data + CPU
// --- Sort      : 0=No sort of the meshes data, 1=sort the meshes data
//
// Outputs
// --- void *M1G : pointer to the data structure
void * M1G_NewLibrary(int1d Dim, char *MshNam, int1d Verbosity, int1d Sort);


// --- Free M1GLibrary data structure
//
// Inputs
// --- M1G : data structure
void * M1G_FreeLibrary(void *m1g);


// --- Set scale factor for new mesh H
//
// Inputs
// --- M1G  : data structure
// --- Scale: scaling factor
void M1G_LibrarySetHScaleFactor(void *m1g, double scale);

// --- Set flag to enable/disable the mesh orientation checking
//
// Inputs
// --- M1G  : data structure
// --- ChkOri: flag = 1 check active, = 0 not active
void M1G_LibrarySetCheckOrientation(void *m1g, int1d chkOri);

// --- Initialize Input Mesh and Output Mesh
//
// Inputs
// --- M1G           : data structure
// --- Msh_NbrVer    : number of vertices in the mesh
// --- Msh_Crd       : 3d coordinates of the vertices of the mesh (start at index 1, has the size Msh_NbrVer + 1)
// --- Msh_NbrElt    : number of element in the mesh
// --- Msh_Elt       : id of the vertices of the tetrahedra of the mesh (start at index 1, has the size Msh_NbrElt + 1)
void M1G_LibraryInitialization_Array_Mesh(void *m1g, int1d Msh_NbrVer, double3d *Msh_Crd, int1d Msh_NbrElt, int3d *Msh_Elt);


// --- Set M1GLibrary germs for first triangle and and 123 process
//
// Inputs
// --- M1G     : data structure
// --- GermTri : vertices of the first triangle
// --- GermVer : vertices to begin 123 process
void M1G_LibrarySetGermVertices(void *m1g, int1d NbrTri, int1d* GerTri, int1d NbrVer, int1d* GerVer);


// --- Create a surface mesh using 123 tecnique
//
// Inputs
// --- M1G      : data structure
void M1G_LibraryMesh123Generator(void *m1g);


// --- Update mesh coordinates
//
// Inputs
// --- M1G    : data structure
// --- NbrVer : number of vertices of the boundaries
// --- Crd    : 3d coordinates of the vertices (start at 1, size NbrVer + 1)
void M1G_LibraryUpdateCoordinates(void *m1g, int1d NbrVer, double3d *Crd);


// --- Return all data of the mesh
//
// Inputs
// --- M1G          : data structure
//
// Outputs
// --- NbrVer : number of vertices in the 123mesh
// --- Crd    : 3d coordinates of the vertices in the 123mesh (start at 1, size NbrVer + 1)
// --- NbrElt : number of elements in the 123mesh
// --- Elt    : id of the vertices of the triangles (start at 1, size NbrElt + 1)
void M1G_LibraryReturnMeshData(void *m1g, int1d *NbrVer, double3d **Crd, int1d *NbrElt, int4d **Elt);


// --- Write resulting 123mesh in ".mesh(b)" format
//
// Inputs
// --- M1G    : data structure
// --- MshNam : filename of the out mesh
void M1G_LibraryWriteMesh(void *m1g, char *MshNam);

}        





              
              
                                               
