# Include the libbitmap library

# Log the inclusion of Bitmap
message(STATUS "[Bitmap Inclusion] Bitmap successfully included.")

# Add include directories for third-party dependencies
target_include_directories(FELiScE_dep_includes INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/bitmap> $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/thirdparty>)