# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased] 

## [2.0.dev] - 2020-12-01

Here we write upgrading notes for brands. It's a team effort to make them as straightforward as possible.

### Added
- [Migration from GForce](https://gforge.inria.fr/projects/felisce)
  This is the initial migration from Inria GForce to Inria Gitlab.
- Added Gitlab CI/CD
- Adding new classes: Testing classes and utilities


### Changed

 - Mainly all Makefiles have been replaced with the corresponding CMakeLIsts.txt and cmake files.
 - Added some guards and refactor a little bit the code for faster compilation.
 - Refactor tests in order to make it work with the CTest standard.
 - Several clean ups of the existing code to avoid duplications and simplify the code with the STL.

### Fixed

 - Some minor compilation issues have been solved in order to adapt to CMake.
 - Some allocation bugs corrected.
