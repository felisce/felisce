#!/bin/bash

echo "===================================================================================="
echo "- Create input file for sonar-scanner"

# create the sonarqube config file
cat > sonar-project.properties << EOF
sonar.host.url=https://sonarqube.inria.fr/sonarqube
sonar.login=$SONAR_LOGIN
sonar.verbose=true

sonar.gitlab.url=https://gitlab.inria.fr
sonar.links.homepage=https://gitlab.inria.fr/felisce/felisce
sonar.links.scm=https://gitlab.inria.fr/felisce/felisce
sonar.scm.disabled=true
sonar.scm.provider=git

sonar.projectKey=felisce:felisce
sonar.projectDescription=FELiScE
sonar.projectVersion=$(git describe --tags --abbrev=0)

sonar.sourceEncoding=UTF-8

sonar.language=c++

sonar.sources=src
sonar.tests=tests

sonar.cxx.file.suffixes=.h,.c,.hpp,.tpp,.cpp
sonar.cxx.includeDirectories=./src

sonar.cxx.compiler.reportPath=felisce-build.log
sonar.cxx.cppcheck.reportPaths=felisce-cppcheck.xml
sonar.cxx.clangsa.reportPaths=analyzer_reports/*/*.plist
sonar.cxx.rats.reportPaths=felisce-rats.xml
sonar.cxx.vera.reportPaths=felisce-vera.xml
sonar.cxx.cobertura.reportPaths=felisce-gcovr.xml

sonar.qualitygate.wait=false

EOF

echo "- Done"
echo "===================================================================================="

echo "===================================================================================="
echo "- Running sonar-scanner"
$HOME/bin/sonar-scanner/bin/sonar-scanner -X 2>&1 | tee felisce-$tool.log
echo "- Done"
echo "===================================================================================="
