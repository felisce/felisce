import sys
import os
import textwrap

class Page:
    """Represents a single page with a name, URL, and description."""
    def __init__(self, name, url, description):
        self.name = name
        self.url = url
        self.description = description

class HTMLGenerator:
    """Generates an HTML file with links to pages."""
    
    def __init__(self, name, pages):
        """
        Initializes the HTML generator with a list of Page objects.
        :param pages: A list of Page objects.
        """
        self.name  = name
        self.pages = pages

    def generate_html(self):
        """Generates the HTML content for the index page."""
        # html_content = textwrap.dedent("""
        # <!DOCTYPE html>
        # <html lang="en">
        # <head>
        #     <meta charset="UTF-8">
        #     <meta name="viewport" content="width=device-width, initial-scale=1.0">
        #     <title>"""+self.name+""" GitLab Pages</title>
        # </head>



        html_content = textwrap.dedent("""
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>"""+self.name+""" GitLab Pages</title>
            <style>
                /* Reset some default styles */
                body, h1, p, ul {
                    margin: 0;
                    padding: 0;
                }

                /* Body styles */
                body {
                    font-family: Arial, sans-serif;
                    background-color: #f4f4f4;
                    color: #333;
                    line-height: 1.6;
                    padding: 20px;
                }

                /* Header styles */
                header {
                    background-color: #003d80;
                    color: white;
                    padding: 15px;
                    text-align: center;
                    border-radius: 8px;
                }

                header h1 {
                    font-size: 2.5em;
                }

                /* Navigation styles */
                nav {
                    background-color: #333;
                    padding: 10px;
                    margin-top: 20px;
                    border-radius: 8px;
                }

                nav ul {
                    list-style-type: none;
                    text-align: left;
                    padding-left: 0;
                }

                nav ul li {
                    margin-bottom: 15px;
                    font-size: 1.1em;
                    display: flex;
                    align-items: baseline;
                }

                nav ul li a {
                    color: white;
                    text-decoration: none;
                    margin-right: 0px;
                    font-weight: bold;
                    width: 10%;
                }

                nav ul li a:hover {
                    color: #ffcc00;
                }

                nav ul li .description {
                    color: #d3d3d3;
                    font-style: italic;
                    font-size: 0.9em;
                    margin-left: 0px;
                }

                /* Fancy dotted style for links */
                nav ul li::before {
                    content: "•";
                    color: #007BFF;
                    margin-right: 10px;
                    font-size: 1.5em;
                }

                /* Main content styles */
                main {
                    background-color: white;
                    padding: 20px;
                    border-radius: 8px;
                    margin-top: 20px;
                }

                main p {
                    font-size: 1.2em;
                    line-height: 1.8;
                }

            </style>
        </head>
        <body>
            <header>
                <h1>"""+self.name+""" Pages</h1>
            </header>
            <nav>
                <ul>
        """)
        
        # Add links to the pages
        for page in self.pages:
            html_content += f'            <li><a href="{page.url}" class="page-link">{page.name}</a> <span class="description">{page.description}</span></li>\n'


        # Close the HTML tags
        html_content += textwrap.dedent("""
                </ul>
            </nav>

            <main>
                <p>Welcome to the FELiScE GitLab Pages! Here, you'll find documentation and resources related to the FELiScE project.<p>
                <p>The links above lead to information on code coverage, Doxygen-generated documentation, and guides for both users and developers.<p>
                <p>These resources aim to help you understand and use FELiScE, and contribute to its ongoing development.</p>
            </main>
        </body>
        </html>
        """)
        
        return html_content

    def save_to_file(self, folder):
        """Saves the generated HTML content to a file in the specified folder."""
        html_content = self.generate_html()
        
        # Ensure the folder exists
        if not os.path.exists(folder):
            os.makedirs(folder)
        
        file_path = os.path.join(folder, "index.html")
        
        with open(file_path, "w") as file:
            file.write(html_content)
        print(f"index.html has been saved to {file_path}.")

# Example usage
if __name__ == "__main__":


    if len(sys.argv) != 3:
        print("Usage: python3 generate_index.py <path> <page>")
        sys.exit(1)

    file_path = sys.argv[1]
    page_name = sys.argv[2]

    # Create Page objects
    pages = [
        Page("Coverage", f"https://felisce.gitlabpages.inria.fr/{page_name.lower()}/coverage", 
             "View the code coverage reports for FELiScE to understand the testing and quality of the project."
             ),
        Page("Doxygen", f"https://felisce.gitlabpages.inria.fr/{page_name.lower()}/doxygen", 
             "Explore the Doxygen-generated documentation for a detailed overview of the FELiScE codebase."
             ),
        Page("Users doc", f"https://felisce.gitlabpages.inria.fr/{page_name.lower()}/latex/felisce_user_guide.pdf", 
             "Check out the user guide to get started with FELiScE and learn how to use its features."
             ),
        Page("Developers doc", f"https://felisce.gitlabpages.inria.fr/{page_name.lower()}/latex/felisce_dev_guide.pdf", 
             "For developers, this guide offers instructions on contributing to the FELiScE project."
             )
    ]

    # Create HTMLGenerator object
    generator = HTMLGenerator(page_name, pages)

    # Generate and save the index.html file
    generator.save_to_file(file_path)
