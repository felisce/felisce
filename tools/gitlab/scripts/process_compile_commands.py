import json
import os
import sys

class CompileCommandsProcessor:
    def __init__(self, compile_commands_path, starting_path, project_path):
        """
        Initializes the processor with paths to the compile_commands.json file,
        the starting path, and the project path.
        """
        self.compile_commands_path = os.path.abspath(compile_commands_path)
        if not os.path.isfile(self.compile_commands_path):
            raise FileNotFoundError(f"{self.compile_commands_path} does not exist or is not a file.")

        self.starting_path = os.path.abspath(starting_path)
        self.project_path = os.path.abspath(project_path)

    def _read_compile_commands(self):
        with open(self.compile_commands_path, 'r', encoding='utf-8') as f:
            return json.load(f)

    def _write_compile_commands(self, data):
        with open(self.compile_commands_path, 'w', encoding='utf-8') as f:
            json.dump(data, f, indent=2)

    def _compute_relative_path(self, abs_path):
        return os.path.relpath(abs_path, start=self.starting_path)

    def _replace_absolute_with_relative(self, text, relative_path):
        return text.replace(self.project_path, relative_path)

    def _process_command_output(self, command, directory):
        parts = command.split()
        for i, part in enumerate(parts):
            if part == "-o" and i + 1 < len(parts):
                abs_output_path = os.path.normpath(os.path.join(directory, parts[i + 1]))
                parts[i + 1] = self._compute_relative_path(abs_output_path)
        return " ".join(parts)

    def _process_output_path(self, output_path):
        abs_output_dir = os.path.join(self.project_path, "build", "Coverage", output_path)
        return self._compute_relative_path(abs_output_dir)

    def make_paths_relative(self):
        relative_path = self._compute_relative_path(self.project_path)
        compile_commands = self._read_compile_commands()

        for entry in compile_commands:
            entry['file'] = self._replace_absolute_with_relative(entry['file'], relative_path)
            entry['command'] = self._replace_absolute_with_relative(entry['command'], relative_path)
            entry['command'] = self._process_command_output(entry['command'], entry['directory'])
            entry["output"] = self._process_output_path(entry["output"])
            entry['directory'] = "."

        self._write_compile_commands(compile_commands)

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python process_compile_commands.py <path_to_compile_commands.json> <starting_path> <project_path>")
        sys.exit(1)

    try:
        processor = CompileCommandsProcessor(*sys.argv[1:])
        processor.make_paths_relative()
        # print("Paths successfully converted to relative.")
    except Exception as e:
        print(f"Error: {e}")