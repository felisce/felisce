#!/bin/sh

# Set variables
export FELISCE_SOURCE="${FELISCE_SOURCE:-"$(cd "$(dirname "$0")/.." && pwd -P)"}"
export FELISCE_BUILD="${FELISCE_SOURCE}/build"

# Prepare git and ensuring that everything is up-to-date
python3 ${FELISCE_SOURCE}/scripts/utils/git_submodules_prepare.py ${FELISCE_SOURCE}

# Set basic configuration
export FELISCE_BUILD_TYPE=${FELISCE_BUILD_TYPE:-"Release"}

# Clean
clear
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/cmake_install.cmake"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeCache.txt"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeFiles"

# Clean up bin folder if exists
rm -rf ${FELISCE_SOURCE}/bin

# Calculate required CPU
cpu_count=$(nproc)
cpu_count=$((cpu_count/2))

# Configure
cmake --no-warn-unused-cli                                         \
-S"${FELISCE_SOURCE}"                                              \
-B"${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}"                         \
-DCMAKE_BUILD_TYPE="${FELISCE_BUILD_TYPE}"                         \
-DCMAKE_UNITY_BUILD=${CMAKE_UNITY_BUILD}                           \
-DPETSC_DIR="/home/felisce/src/petsc"                              \
-DFELISCE_WITH_CURVEGEN=${FELISCE_WITH_CURVEGEN}                   \
-DFELISCE_WITH_NSINRIA=${FELISCE_WITH_NSINRIA}                     \
-DNSINRIA_DIR="${NSINRIA_DIR}"                                     \
-DFELISCE_WITH_HYPRE=${FELISCE_WITH_HYPRE}                         \
-DFELISCE_WITH_LIBXFM=${FELISCE_WITH_LIBXFM}                       \
-DFELISCE_WITH_PVM=${FELISCE_WITH_PVM}                             \
-DLIBPVM_DIR="/usr"                                                \
-DFELISCE_WITH_QT=${FELISCE_WITH_QT}                               \
-DFELISCE_WITH_SCIPLOT=${FELISCE_WITH_SCIPLOT}                     \
-DFELISCE_WITH_SLEPC=${FELISCE_WITH_SLEPC}                         \
-DFELISCE_WITH_SUPERLU=${FELISCE_WITH_SUPERLU}                     \
-DFELISCE_WITH_ZMQ=${FELISCE_WITH_ZMQ}                             \
-DZMQ_DIR="/usr"                                                   \
-DZMQ_BINDING_DIR="/usr"                                           \
-DFELISCE_WITH_TESTS=${FELISCE_WITH_TESTS}                         \
-DFELISCE_WITH_NIGHTLY_TESTS=${FELISCE_WITH_NIGHTLY_TESTS}         \
-DFELISCE_FOR_CI=${FELISCE_FOR_CI}                                 \
-DFELISCE_WITH_BENCHMARKS=${FELISCE_WITH_BENCHMARKS}               \
-DBENCHMARKS_DIR=${BENCHMARKS_DIR}                                 \
-DFELISCE_FOR_CPACK=${FELISCE_FOR_CPACK}                           \
-DMASTERFSI_PVM="/home/felisce/MasterFSIContact/bin/PVM/MasterFSI" \
-DMASTERFSI_ZMQ="/home/felisce/MasterFSIContact/bin/ZMQ/MasterFSI"

# Compile
if [ "$FELISCE_FOR_CPACK" = ON ] ; then
    cpack --config ${FELISCE_BUILD_TYPE}/CPackConfig.cmake
    echo "Deploy complete"
else
    echo "Compile"
    cmake --build "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" --target install -- -j ${cpu_count}
    echo "Build complete"

    FILE=${FELISCE_SOURCE}/bin/${FELISCE_BUILD_TYPE}/lib/libFELiScE.so
    if [ -f "$FILE" ]; then
        echo "$FILE exists."
    else
        echo "$FILE does not exist."
        exit 1
    fi
fi