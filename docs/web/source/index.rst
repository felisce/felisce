.. FELiScE documentation master file, created by
   sphinx-quickstart on Thu Nov 14 08:15:07 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FELiScE's documentation!
===================================

.. toctree::
   :maxdepth: 2

FELiScE stands for “Finite Elements for LIfe SCiences and Engineering” – is a
new finite element code which the MACS and REO project-teams have decided to
jointly develop in order to build up on their respective experiences concerning
finite element simulations. One specific objective of this code is to provide
in a unified software environment all the state-of-the-art tools needed to
perform simulations of the complex cardiovascular models considered in the two
teams – namely involving fluid and solid mechanics, electrophysiology, and the
various associated coupling phenomena. FELiScE is written in C++, based on the
Petsc library and may be later released as an opensource library. The FELiScE
project started in 2010. It already has more than 10 contributors and about 30
users, mainly at INRIA, Université P & M Curie (Paris 6), Université Paris-Sud
(Paris 11), Université Technologique de Compiègne (UTC), Weierstrass Institute
(WIAS, Berlin) and Université Paris Descartes (Paris 5).


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

