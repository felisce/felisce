============================================
Tutorial: Poisson Equation
============================================

--------------------------------------------
Introduction
--------------------------------------------

- mathematics

.. math::

    \Delta U = f

- test configuration
-   geometry
-   mesh
-   bondaay condition
-   initial condition
-   source term: f

--------------------------------------------
Poisson
--------------------------------------------

This case is just an extension of Laplace example.
We present here the protocol to add a source term f.

Go to the ``$FELISCE_ROOT/Tests/Example/Heat``, you will find 5 files:
   - ``main.cpp``: contain the main progam which is executed.
   - ``userHeat.hpp`` and  ``userHeat.cpp``: where we specify how to build the linear system.
   - ``Makefile``: to build the programm.
   - ``data``: configuration file.


Consider the file ``main.cpp``:

.. code-block:: c++

    #include "Model/heatModel.hpp"
    #include <userHeat.hpp>
    using namespace felisce;

    int main(const int argc, const char** argv)
    {
      HeatModel model;
      model.initializeModel(argc, argv);
      
      model.initializeLinearProblem( new UserHeat());

      //Loop time
      while (!model.hasFinished()){
        model.forward();		
      }
    }

This new physical problem doesn't imply any changes in the main program with the Felisce structure.

The UserHeat class gives some possibilities at the user to change: boundary conditions, source term, etc. With these modification we complete the system :math:`Ax = b` to be consistent with user formulation.

.. code-block:: c++

	#include <Solver/linearProblemHeat.hpp>
	#include <math.h>

	namespace felisce
	{
	 class MyFunctionTime{
	 public:
	   double operator() (double x,double y,double z, double t) const
	   {
			return (1.-x)*x*(1.-y)*y*cos(t)+2.*sin(t)*(y*(1.-y) + x*(1.-x));
	   }
	 };

	 class UserHeat:
	 public LinearProblemHeat
	 {
	 public:
	   UserHeat(); 
	   ~UserHeat();
	   void initPerElementType(ElementType eltType, const int& flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);
	   void computeElementArray(vector<Point*>& elemPoint, vector<int>& elemIdPoint, int& iel, const int& flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);
	   void defineBC();
	   void finalizeBCConstant();
	 private:
	   ElementField* _elemFieldTemperature;
	   MyFunctionTime _fctTime;
	 };
	}
	
We use a functor to define the function :math:`f` and to use specific mathematic function you must include the ``math.h`` file.

.. code-block:: c++

	class MyFunctionTime{
	public:
	  double operator() (double x,double y,double z, double t) const
	  {
		return (1.-x)*x*(1.-y)*y*cos(t)+2.*sin(t)*(y*(1.-y) + x*(1.-x));
	  }
	};
	
	
After that just declare a function in the class:

.. code-block:: c++

	MyFunctionTime _fctTime;
	
	
To use this function we are working with a pointer on the Felisce object: **ElemField**.
An ElementField contains the values of a scalar field, for example a diffusion coefficient or a source term, at the element level.
It can be either constant, defined on the degree of freedom of the finite element or defined on the quadrature points.

.. code-block:: c++

   	ElementField* _elemFieldTemperature;


In the matrix and second member assembling procedure (usually call: assembly loop) the two virtual functions **initPerElementType** and **computeElementArray** are call in the class **LinearProblem**. To use source term we add some details on it that why we declare the functions. 

.. code-block:: c++
	
	void initPerElementType(ElementType eltType, const int& flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);
   	void computeElementArray(vector<Point*>& elemPoint, vector<int>& elemIdPoint, int& iel, const int& flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);


The ``userheat.cpp`` file implement these methods:

.. code-block:: c++

	#include <userHeat.hpp>
	namespace felisce
	{   
	  UserHeat::UserHeat():
	  LinearProblemHeat(),
	  _elemFieldTemperature(NULL)
	  {}

	  UserHeat::~UserHeat()
	  {
	    if (_elemFieldTemperature)
		    delete _elemFieldTemperature;
	  }

	  void UserHeat::initPerElementType(ElementType eltType, const int& flagMatrixRHS)
	  {
	    LinearProblemHeat::initPerElementType(eltType);
	    int iTemperature = _listVariable.getVariableIdList(temperature);
	    CurrentFiniteElement& fe = *_listCurrentFiniteElement.listCurrentFiniteElement()[iTemperature];
  
	    //ElementField initialisation
	    _elemFieldTemperature = new ElementField(ElementField::QUAD_POINT_FIELD, fe);
	  }

	  void UserHeat::computeElementArray(std::vector<Point*>& elemPoint, std::vector<int>& elemIdPoint, int& iel, const int& flagMatrixRHS)
	  {   
	    LinearProblemHeat::computeElementArray(elemPoint,elemIdPoint, iel);

	    int iTemperature = _listVariable.getVariableIdList(temperature);
	    CurrentFiniteElement& fe = *_listCurrentFiniteElement.listCurrentFiniteElement()[iTemperature];

	    _elemFieldTemperature->setValue(_fctTime, fe,_fstransient->time,0);
		_elemVec->source(1.,fe,*_elemFieldTemperature,0,1);
	  }

	  void UserHeat::defineBC()
	  {    
	    int iTemperature = _listVariable.getVariableIdList(temperature);
	    Variable temperature = _listVariable.listVariable()[iTemperature];
  
	    _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 0, temperature, Comp1));
		_BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 2, temperature, Comp1));
	  }

	  void UserHeat::finalizeBCConstant()
	  {
	    _BoundaryConditionList.getList()[0]->setValue(1.);
	    _BoundaryConditionList.getList()[1]->setValue(0.);
	  }
	}
	

Constructor/Destructor
--------------------------
Initialize value in the constructor.

.. code-block:: c++

	UserHeat::UserHeat():
	 LinearProblemHeat(),
	 _elemFieldTemperature(NULL)
	 {}


We add a command in the destructor to suppress pointer *_elemFieldTemperature* to free memory.

.. code-block:: c++

	UserHeat::~UserHeat()
	 {
	   if (_elemFieldTemperature)
	    delete _elemFieldTemperature;
	 }
	

Function **initPerElementType**
-------------------------------

Call function in the mother class. 


.. code-block:: c++

	LinearProblemHeat::initPerElementType(eltType);
	
The object **CurrentFiniteElement** contain all information (geometric finite element, reference finite element, quadrature point) about the finite element choose to support *temperature* variable. *fe* is a reference on this finite element and is used to allocate elementary array (elementary matrix, elemtary vector, elementary field).

.. code-block:: c++

    int iTemperature = _listVariable.getVariableIdList(temperature);
    CurrentFiniteElement& fe = *_listCurrentFiniteElement.listCurrentFiniteElement()[iTemperature];
    _elemFieldTemperature = new ElementField(ElementField::QUAD_POINT_FIELD, fe);

	
Function **computeElementArray**
---------------------------------

Call function in the mother class and take a reference on finite element associate to *temperature*.

.. code-block:: c++

	LinearProblemHeat::computeElementArray(elemPoint,elemIdPoint, iel);
	int iTemperature = _listVariable.getVariableIdList(temperature);
	CurrentFiniteElement& fe = *_listCurrentFiniteElement.listCurrentFiniteElement()[iTemperature];


The function **setValue** fill *_elemFieldTemperature* with value calculate from *_fctTime* (source term function define on ``userHeat.hpp``)`.

.. code-block:: c++

    _elemFieldTemperature->setValue(_fctTime, fe,_fstransient->time,0);


The function **source** evaluate the term :math:`\int_{T} f.u`.

.. code-block:: c++
	
	_elemVec->source(1.,fe,*_elemFieldTemperature,0,1);


Boundary Conditions
---------------------------------

We just apply constant Dirichlet boundary conditions.

.. code-block:: c++
	
    void UserHeat::defineBC()
    {    
      int iTemperature = _listVariable.getVariableIdList(temperature);
      Variable temperature = _listVariable.listVariable()[iTemperature];
      
      _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 0, temperature, Comp1));
      _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 1, temperature, Comp1));
      _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 2, temperature, Comp1));
      _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 3, temperature, Comp1));
    }
      
    void UserHeat::finalizeBCConstant()
    {
      _BoundaryConditionList.getList()[0]->setValue(0.);
      _BoundaryConditionList.getList()[1]->setValue(0.);
      _BoundaryConditionList.getList()[2]->setValue(0.);
      _BoundaryConditionList.getList()[3]->setValue(0.);
    }
	
