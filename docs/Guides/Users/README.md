# User guides

To create the documentation it is necessary to install *Sphinx* (http://sphinx.pocoo.org/). To do this, type the command in a terminal:

	python3 -m pip install -U sphinx

To generate the documentation, type in the UsersManual directory: 

	make html 
	
For doc generated in the _build / html directory.

or:
	
	make latex 
	
For generation of the .tex files in _build / latex, you must then type make all-pdf in this directory to obtain the .pdf.
