.. Felisce Documentation documentation master file, created by
   sphinx-quickstart on Tue Sep 27 17:20:19 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Felisce Documentation's documentation!
=================================================

Contents:

.. toctree::
   :maxdepth: 2

   laplace
   poisson

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

