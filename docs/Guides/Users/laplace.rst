============================================
Tutorial: Laplace Equation
============================================

--------------------------------------------
Introduction
--------------------------------------------

This example presents one the most famous partial differential
equation: heat equation.  

.. math::
  :nowrap:

    \begin{eqnarray}
    \frac{\partial\text{u}}{\partial\text{t}}-\Delta{u} & = & 0 \qquad\textrm{ in }\Omega\\
    \frac{\partial\text{u}}{\partial\text{n}} & = & 0  \qquad\textrm{ on d}\Omega\\
    u(x,0) & = & u_0(x) \qquad\textrm{ in } \Omega
    \end{eqnarray}


- test configuration
-   geometry
-   mesh
-   bondaay condition
-   initial condition

--------------------------------------------
Laplace
--------------------------------------------

Go to the ``$FELISCE_ROOT/Tests/Example/Laplace``, you will find 5 files:
   - ``main.cpp``: contain the main progam which is executed.
   - ``userHeat.hpp`` and  ``userHeat.cpp``: where we specify how to build the linear system.
   - ``Makefile``: to build the programm.
   - ``data``: configuration file.

Consider the file ``main.cpp``:

.. code-block:: c++

    #include "Model/heatModel.hpp"
    #include <userHeat.hpp>
    using namespace felisce;

    int main(const int argc, const char** argv)
    {
      HeatModel model;
      model.initializeModel(argc, argv);
      
      model.initializeLinearProblem( new UserHeat());

      //Loop time
      while (!model.hasFinished()){
        model.forward();		
      }
    }


.. code-block:: c++

    #include "Model/heatModel.hpp"
    #include <userHeat.hpp>

The first included header is the one of the model you will use to modelize the
variationnal problem.

The second included header is for the definition of our particular problem, in
this case for boundary conditions.

.. code-block:: c++

    using namespace felisce;

To use the objects define of the felisce library without preffixing with the namespace ``felisce::``.

.. code-block:: c++

      HeatModel model;

Here the class Model is instanciated (so the default constructor is used).


.. code-block:: c++

      model.initializeModel(argc, argv);

The model is initialized. The arguments ``argc`` and ``argv`` allow to pass command
line options to overide default values. For example, the default configuration
file name is ``data``. To use a different file, use: "--file='databis'".
      
.. code-block:: c++

      model.initializeLinearProblem( new UserHeat());

Here we speficy which to the model the linear system we are going to solve. The
``UserHeat`` class is explained just bellow. After this step we are ready to run the
time loop:

.. code-block:: c++

      while (!model.hasFinished()){
        model.forward();		
      }

Compile and run the program:

.. code-block:: bash

    make
    ./main

The solution files will be written in the ``Solution/`` directory, that you can
visualize with ensight:

.. code-block:: bash

    ensight Solution/temperature.case

--------------------------------------------
userHeat
--------------------------------------------

Now that we have run the program, let's look at the ``UserHeat`` class defined in
the ``userHeat.hpp``:

.. code-block:: c++

    #ifndef _USERHEAT_HPP
    #define _USERHEAT_HPP
    #include <Solver/linearProblemHeat.hpp>

    namespace felisce
    {
      class UserHeat:
      public LinearProblemHeat
      {
      public:
        UserHeat(); 
        ~UserHeat(){}; 
        void defineBC();
        void finalizeBCConstant();
      };
    }

    #endif


The ``linearProblemHeat`` class is reponsible to construct the system $A x = b$.
Its methods relative to boundary condition are virtual and we have implemented
them is ``userHeat`` class, which derive from ``linearProblemHeat``.

.. code-block:: c++

    UserHeat(); 
    ~UserHeat(){}; 
    void defineBC();
    void finalizeBCConstant();


 The two  first methods are the default constructor and destructor.
 In ``defineBC`` the boundary conditions are enumerated, and their values are set in
 ``finalizeBCConstant``. 


The ``userheat.cpp`` file implement these methods:

.. code-block:: c++

    #include <userHeat.hpp>

    namespace felisce
    {   
      UserHeat::UserHeat():
      LinearProblemHeat()
      {}
      
      void UserHeat::defineBC()
      {    
        int iTemperature = _listVariable.getVariableIdList(temperature);
        Variable temperature = _listVariable.listVariable()[iTemperature];
        
        _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 1, temperature, Comp1));
        _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 0, temperature, Comp1));
      }
      
      void UserHeat::finalizeBCConstant()
      {
        _BoundaryConditionList.getList()[0]->setValue(1.);
        _BoundaryConditionList.getList()[1]->setValue(0.);
      }
    }

.. code-block:: c++

        int iTemperature = _listVariable.getVariableIdList(temperature);
        Variable temperature = _listVariable.listVariable()[iTemperature];

We select the variable on which the boundary contion will apply: the temperature.

        
.. code-block:: c++

        _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 1, temperature, Comp1));

The boundary condition is a constant Dirchlet condition that apply to the
mesh region with label 1, on the first component of the physical variable temperature.

.. code-block:: c++

        _BoundaryConditionList.add(new BoundaryCondition(Dirichlet, Constant, 0, temperature, Comp1));

The similar boundary condition now apply on region of label 0.


.. code-block:: c++

        _BoundaryConditionList.getList()[0]->setValue(1.);
        _BoundaryConditionList.getList()[1]->setValue(0.);

We now specify the values corresponding to the two boundary conditions. 

.. code-block:: guess

    [transient]

    timeStep = 0.01
    timeMax = 0.2
    time = 0.

    [variable]

    variable = 'temperature'
    typeOfFiniteElement = '0'
    degreeOfExactness = '2'
    domain = 'all'

    [mesh]

    meshDir = ./../../../../data/mesh/
    inputMesh = square_tria_4kv.mesh
    outputMesh = temperature.geo
    resultDir = ./Solution/
    prefixName = temperature

    # list of eltType keyword: 
    # Nodes Segments2 Segments3 Triangles3 Triangles6 Quadrangles4
    # Quadrangles8 Quadrangles9 Tetrahedra4 Tetrahedra10 Pyramids5
    # Pyramids13 Prisms6 Prisms15 Hexahedra8 Hexahedra20 
    # Hexahedra26 Hexahedra27

    [Triangles3]

    label = '0'
    stringRef = 'domain'

    [Segments2]

    label = '0 1 2 3'
    stringRef = 'up down left right'

    [petsc]

    # List of solver :
    # chebychev cg gmres preonly bicg python
    #To use mumps: solver = preonly

    solver = gmres
     
    # List of preconditioner :
    # none jacobi sor lu bjacobi ilu asm cholesky
    #To use mumps: preconditioner = lu

    preconditioner = ilu
    relativeTolerance = 10.e-6
    absoluteTolerance = 10.e-10
    maxIteration = 10000

The configuration file is divided into sections. For now, interessing sections
are:

.. code-block:: guess

    [mesh]

    meshDir = ./../../../../data/mesh/
    inputMesh = square_tria_4kv.mesh
    outputMesh = temperature.geo
    resultDir = ./Solution/
    prefixName = temperature

In the, ``mesh`` section can be found:

    - meshDir: the directory of the mesh file.
    - inputMesh: the mesh file with extension determing the file format (.mesh
      for medit and .geo for ensight).
    - ouputMesh: the output mesh with extension.
    - resultDir: where the solution is written.
    - prefixName: the prefix of the solution file names.

.. code-block:: guess

    # list of eltType keyword: 
    # Nodes Segments2 Segments3 Triangles3 Triangles6 Quadrangles4
    # Quadrangles8 Quadrangles9 Tetrahedra4 Tetrahedra10 Pyramids5
    # Pyramids13 Prisms6 Prisms15 Hexahedra8 Hexahedra20 
    # Hexahedra26 Hexahedra27

Here is the list of the element types manageable and defined in Felisce.

.. code-block:: guess

    [Triangles3]

    label = '0'
    stringRef = 'domain'

    [Segments2]

    label = '0 1 2 3'
    stringRef = 'up down left right'

These sections define a mapping between the integer element labels in medit
format and the string element labels in ensight format (this is optional).
The string label are used during visualization.
