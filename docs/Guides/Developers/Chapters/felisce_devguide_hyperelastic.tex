\chapter{Hyperelastic laws}\label{sec:hyperelastic_laws}

\section{Hyperelastic materials}

Elastic materials for which the work is independent of the load path are said to be hyperelastic (or \textit{Green} elastic) materials\cite{belytschko2013nonlinear}. In this section, some general features of hyperelastic materials are described and then examples of hyperelastic constitutive models that are used in practice are given. Hyperelastic materials are characterized by the existence of a stored (or strain) energy function that is a potential for the stress:

\begin{equation}
    \sigma^{i j} = \frac{\partial \mathcal{W}}{\partial e_{i j}}
\end{equation}

\section{Isotropic hyperelastic materials. Generalised Hooke's law}

It can be shown (\textit{Malvern}, 1969) that the stored strain energy (potential) for a hyperelastic material which is isotropic with respect to the initial, unstressed configuration can be written as a function of the  principal invariants ($I_{1}$, $I_{2}$, $I_{3}$) of the right \textit{Cauchy–Green} deformation tensor. Concerning the generalised \textit{Hooke's law}, the above formulas apply with \eqref{eq:hooke}.

\begin{subequations}\label{eq:hooke}
    \begin{equation}
        \frac{\partial \mathcal{W}}{\partial e_{i j}}=\sigma^{i j}=H^{i j k l} e_{k l}
    \end{equation}

    \begin{equation}
        \frac{\partial^{2} \mathcal{W}}{\partial e_{i j} \partial e_{k l}}=\frac{\partial \sigma^{i j}}{\partial e_{k l}}=H^{i j k l}
    \end{equation}

    Defining:

    \begin{equation}
        H^{i j k l}=\lambda g^{i j} g^{k l}+\mu\left(g^{i k} g^{j l}+g^{i l} g^{j k}\right)
    \end{equation}
\end{subequations}

In the (more general) case of an energy density depending on the three tensor invariants ($I_1$, $I_2$, $I_3$), the material considered is described by functions giving the value of the first and second derivatives of the energy density compared to (and according to the ) three invariants. The calculation of the elementary tables corresponding to \eqref{eq:newtonsystem} then uses, through derivative composition formulas, the following identities giving the invariants and their derivatives compared to the components of the \textit{Cauchy-Green} tensor (in any coordinate system) \eqref{eq:eqinvariants}.

\begin{subequations}\label{eq:eqinvariants}
    \begin{equation}
        \left\{\begin{array}{l}
            I_{1}=C_{i}^{i} \\
            I_{2}=\frac{1}{2}\left(\left(I_{1}\right)^{2}-C_{i}^{j} C_{j}^{i}\right) \\
            I_{3}=\operatorname{det}\left(C_{i}^{j}\right)
        \end{array}\right.
    \end{equation}

    \begin{equation}\label{eq:tens1}
        \left\{\begin{array}{l}
            \frac{\partial I_{1}}{\partial C_{i j}}=g^{i j} \\
            \frac{\partial I_{2}}{\partial C_{i j}}=I_{1} g^{i j}-C^{i j} \\
            \frac{\partial I_{3}}{\partial C_{i j}}=\frac{1}{2} C_{m n} C_{k l} \epsilon^{i k m} \epsilon^{j l n}
        \end{array}\right.
    \end{equation}

    \begin{equation}\label{eq:tens2}
        \left\{\begin{array}{l}
            \frac{\partial^{2} I_{1}}{\partial C_{i j} \partial C_{k l}}=0 \\
            \frac{\partial^{2} I_{2}}{\partial C_{i j} \partial C_{k l}}=-g^{i k} g^{j l}+g^{i j} g^{k l} \\
            \frac{\partial^{2} I_{3}}{\partial C_{i j} \partial C_{k l}}=C_{m n} \epsilon^{i k m} \epsilon^{j l n}
        \end{array}\right.
    \end{equation}

    Where the $\epsilon^{i j k}$ are the components of the "perturbation" tensor defined by \eqref{eq:epsilon}.

    \begin{equation}\label{eq:epsilon}
        \left\{\begin{aligned}
            \epsilon^{i j k} &=0 & \text {if } 2 \text { indices coincident } \\
            &=1 / \sqrt{g} & \text { if }(i, j, k) \text { even permutation of }(1,2,3) \\
            &=-1 / \sqrt{g} & \text { if }(i, j, k) \text { uneven permutation of }(1,2,3)
        \end{aligned}\right.
    \end{equation}
\end{subequations}

Formulas \eqref{eq:tens1} and \eqref{eq:tens2} are easily demonstrated by verifying that they give the usual expressions in an orthonormal frame of reference, their validity in the general case then resulting from their obvious tensor character.

\section{Models}

\subsection{Saint Venant–Kirchhoff model}

The simplest hyperelastic material model is the \textit{Saint Venant–Kirchhoff} model\cite{wiki:Hyperelastic_material} which is just an extension of the geometrically linear elastic material model to the geometrically nonlinear regime. The strain-energy density function for the \textit{Saint Venant–Kirchhoff} model is:

\begin{equation}
    \mathcal{W}(\boldsymbol{E}) = \frac{\lambda}{2}[\text{tr}(\boldsymbol{E})]^2 + \mu \text{tr}(\boldsymbol{E}^2)
\end{equation}

And the second \textit{Piola–Kirchhoff} stress can be derived from the relation:

\begin{equation}
   \boldsymbol{S} = \cfrac{\partial \mathcal{W}}{\partial \boldsymbol{E}}
\end{equation}

This model has the general form and the isotropic form respectively:

\begin{equation}
    \boldsymbol{S} = \boldsymbol{C} : \boldsymbol{E}
\end{equation}

\begin{equation}
    \boldsymbol{S} = \lambda \text{tr}(\boldsymbol{E})\boldsymbol{\mathit{I}} + 2\mu\boldsymbol{E}
\end{equation}

Where $\boldsymbol{S}$ is the second \textit{Piola–Kirchhoff} stress, $\boldsymbol{C} : {\rm I\!R}^{3\times 3} \rightarrow {\rm I\!R}^{3\times 3}$ is a fourth order stiffness tensor and $\boldsymbol{E}$ is the \textit{Lagrangian-Green} strain given by $\mathbf{E} =\frac{1}{2}\left[ (\nabla_{\mathbf X}\mathbf u)^T + \nabla_{\mathbf X}\mathbf u + (\nabla_{\mathbf X}\mathbf u)^T \cdot\nabla_{\mathbf X}\mathbf u\right]$
$\lambda$ and $\mu$ are the \textit{Lame constants}, and $\boldsymbol{\mathit{I}}$ is the second order unit tensor.

\subsection{Ciarlet-Geymonat model}

We also consider a \textit{Ciarlet-Geymonat} model \cite{colin2018experimental} which is a realistic model of 3D deformations. It models a hyperelastic material with the following energy density:

\begin{equation}\label{eq:constraintCiarlet}
    \mathcal{W}(\boldsymbol{E}) = a I_{1}+\left(\frac{\mu}{2}-a\right) I_{2}+\left(\frac{\lambda}{4}-\frac{\mu}{2}+a\right) I_{3}-\left(\frac{\mu}{2}+\frac{\lambda}{4}\right) \ln \left(I_{3}\right)
\end{equation}

Where $\lambda$, $\mu$, and $a$ describe the material and $I_{1}$, $I_{2}$, $I_{3}$ are the invariants. When the strain tends to $0$, this is asymptotic to a linear material satisfying \textit{Hooke’s} law with \textit{Lamé} parameters $\lambda$, $\mu$. The constitutive parameters are required to satisfy the following constraints:

\begin{equation}
    \lambda, \mu>0, \quad \max \left(0, \frac{\mu}{2}-\frac{\lambda}{4}\right)<a<\frac{\mu}{2}
\end{equation}

It is proved in \cite{colin2018experimental} that such an energy density is polyconvex in the sense of \textit{Ball} when the constraints \eqref{eq:constraintCiarlet} are satisfied,  which ensures the existence of  solutions.  Moreover  this  energy  density  becomes  infinite  when  $det(\mathbf{C})\rightarrow$, which is a desirable property: it ensures that no admissible deformation satisfies $det(\mathbf{C}) = 0$. The derivatives of the energy function with respect to the invariants are:

\begin{equation}
  \begin{array}{l}
    \partial_{1} \mathcal{W}=a \\
    \partial_{2} \mathcal{W}=\frac{\mu}{2}-a \\
    \partial_{3} \mathcal{W}=\frac{\lambda}{4}-\frac{\mu}{2}+a-\left(\frac{\mu}{2}+\frac{\lambda}{4}\right) \frac{1}{I_{3}}
  \end{array}
\end{equation}
