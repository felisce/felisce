\chapter{Benchmarks}

\section{Introduction}
We present in this work some benchmark computations using several numerical methods developed in {\felisce} for the incompressible Navier-Stokes equations.

%=====================================
\subsection{The fluid model}
We consider a domain $\Omega \subset \R^d$ ($d=2, 3$) with the boundary $\partial \Omega = \Gamma_d \cup \Gamma_n$ and the following incompressible Navier-Stokes equations

\begin{equation}\label{NS}
\displaystyle\left \{ \begin{aligned}
\rho \frac{\partial {\bf u}}{\partial t} + \rho {\bf u} \cdot \nabla {\bf u} + \nabla p - 2 \mu \nabla \cdot  \epsilon ({\bf u}) &= {\bf 0} &\text{ in } &\Omega \\
\nabla \cdot {\bf u} &= 0 &\text{ in } &\Omega \\
{\bf u} &= {\bf u}_d &\text{ on } &\Gamma_d \\
-p {\bf I} + 2 \mu \epsilon ({\bf u}) &= 0 &\text{ on } &\Gamma_n
\end{aligned}
\right.
\end{equation}
where ${\bf u} : \Omega \times \R^+ \rightarrow \R^d$ is the velocity, $p : \Omega \times \R^+ \rightarrow \R$ is the pressure, $\mu$ is the dynamic viscosity of the fluid, ${\bf u}_d$ corresponds to a given boundary velocity and $\epsilon ({\bf u}) = \displaystyle\frac{1}{2} (\nabla {\bf u} + \nabla {\bf u}^T)$.

%=====================================
\subsection{Numerical methods}
We use the following numerical methods for the test cases:
\begin{itemize}
\item monolithic model
\begin{itemize}
\item NS semi-implicit method, with P2/P1, P1b/P1 or P1/P1 stabilization discretization
\item method of characteristic, with P2/P1 discretization, option Euler or RK4
\end{itemize}
\item Fractional Step model: with P2/P1 or P1/P1 stabilization discretization
\end{itemize}

%=====================================
\section{Numerical results}

%=====================================
\subsection{Driven cavity}
We consider the classical $2$D driven cavity problem in the unit square domain $\Omega = [0,1] \times [0,1]$.
Boundary conditions on the top wall is given by $u_1=U_0, u_2=0$, and on the other walls are zero $u_1=u_2=0$.
The Reynolds number is computed by $Re=\displaystyle\frac{\rho U_0}{\mu}$.

\begin{table}[htbp]
\begin{center}
\begin{tabular}{| c | l | c | c | c |}
\hline
Model & Method & 1 processor & 4 processors & 8 processors \\
\hline
Monolithic & semi-implicit P2/P1 & $x=0.623, \ y=0.745$ & $x=0.623, \ y=0.745$ & $x=0.623, \ y=0.745$ \\
& semi-implicit P1b/P1 & $x=0.623, \ y=0.747$ & $x=0.622, \ y=0.746$ & $x=0.623, \ y=0.746$ \\
& semi-implicit P1/P1 stab. & $x=0.622, \ y=0.745$ & $x=0.622, \ y=0.745$ & $x=0.622, \ y=0.745$ \\
& Euler charact. P2/P1 & $x=0.622, \ y=0.747$ & - & -  \\
& RK4 charact. P2/P1 & $x=0.620, \ y=0.745$ &  - & - \\
\hline
Frac. Step & P2/P1 & $x=0.619, \ y=0.749$ & $x=0.619, \ y=0.748$ & $x=0.619, \ y=0.748$ \\
& P1/P1 stabilization & $x=0.619, \ y=0.748$ & $x=0.618, \ y=0.748$ & $x=0.618, \ y=0.748$ \\
\hline
\hline
References & Ghia et al.~\cite{ghia82} & $x=0.617, \ y=0.734$ & - & - \\
& NSIKE, MESH1~\cite{NSIKE} & $x=0.610, \ y=0.750$ & - & - \\
\hline
\end{tabular}
\caption{\label{tabular1} \em Driven cavity problem: position of the center of the main vortex at the steady-state with Reynolds number $100$, mesh of about $4,000$ vertices, time step $0.01$. }
\end{center}
\end{table}

In Table~\ref{tabular1}, we compare the position of the main vortex at the steady-state, obtained with different methods on a triangular mesh of about $4,000$ vertices and a time step $dt=0.01$, with the results in~\cite{ghia82} and~\cite{NSIKE}, where $Re=100$.
The steady-state is assumed to reach when
$$\max |\xi^{n+1} - \xi^n| < 10^{-4}$$
where $\xi^n = ({\bf u}^n, p^n)$ stands for the solution at $n$-th time step.
The results are quite the same both in sequential and parallel cases.
The center of the main vortex is presented in Figure~\ref{fig1}.

\begin{table}[htbp]
\begin{center}
\begin{tabular}{| c | l | r | r | r | r |}
\hline
Model & Method & 1 processor & 2 processors & 4 processors & 8 processors \\
\hline
Monolithic & semi-implicit P2/P1 & $474$ & $348$ & $218$ & $117$ \\
& semi-implicit P1/P1 stab. & $162$ & $98$ & $56$ & $34$ \\
& Euler charact. P2/P1 & $858$ & - & - & - \\
& RK4 charact. P2/P1 & $2621$ &  - & - & - \\
\hline
Frac. Step & P2/P1 & $242$ & $163$ & $95$ & $63$ \\
& P1/P1 stabilization & $94$ & $65$ & $43$ & $33$ \\
\hline
\end{tabular}
\caption{\label{tabular1_cputime} \em Driven cavity problem: CPU time (in seconds) to reach the steady-state with Reynolds number $100$, mesh of about $4,000$ vertices, time step $0.01$. }
\end{center}
\end{table}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.6\textwidth]{felisce-fig-cavity-NS-P2-Re100}
%\includegraphics[width=0.6\textwidth]{../../Users/Cuc/NS/ccc_cavity_autre_NS_P2P1_symStress_bdf1_1proc_semiImplicit_4K_dt0.01_viscosity0.01/vortex_crop}
\caption{\label{fig1} \em Driven cavity problem: center of the main vortex at $Re=100$ with model monolithic, semi-implicit P2/P1 method with 1 processor.}
\end{center}
\end{figure}

The CPU time to reach the steady-state is presented in the Table~\ref{tabular1_cputime}.
With the same discretization, it is most rapid with the Fractional Step method and slowest with the method of characteristics.
Of course, with the same method, P1/P1 discretization is faster than P2/P1 discretization.
In this $2$D test case, with the P1/P1 discretization, the scalability of the NS method is better than the Fractional Step method (cf. Figure~\ref{fig1-scalability}), while with the P2/P1 discretization, the scalability is quite the same over these two methods.
With the NS method, the scalability in P1/P1 discretization is better than in P2/P1 discretization.


\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{felisce-fig-cavity-scalability}
\caption{\label{fig1-scalability} \em Driven cavity problem: analysis of the scalability.}
\end{center}
\end{figure}

%=====================================
\subsection{Flow over a step}
We consider a $2$D computational domain of length $L=23$, with a step located at $x=5$ with the height $h_2=1$ (see the figure in~\cite{NSIKE} or Figure~\ref{fig2}).
A parabolic flow $(u_1, 0)$ is imposed at the inlet with $\max u_1 = U_0 = 3$, so the mean value $\bar{u}_1 = \displaystyle\frac{2}{3} U_0 = 2$.
Boundary conditions at the outlet is $\sigma \cdot {\bf n} = 0$ and on the remaining walls are zero $u_1=u_2=0$.
The Reynolds number is computed by $Re=\displaystyle\frac{\rho \bar{u}_1 (h_1 + h_2)}{\mu}$.

\begin{table}[htbp]
\begin{center}
\begin{tabular}{| c | l | c | c | c | c |}
\hline
Model & Method & 100 & 200 & 400 & 800 \\
\hline
Monolithic & semi-implicit P2/P1 & $2.87$ & $4.89$ & $8.27$ & - \\
& semi-implicit P1/P1 stab. & $2.86$ & $4.88$ & $8.40$ & - \\
& Euler charact. P2/P1 & $2.89$ & $4.90$ & - & - \\
\hline
Frac. Step & P2/P1 & $2.87$ & $4.89$ & $8.31$ & $12.00$ \\
& P1/P1 stabilization & $2.86$ & $4.90$ & $8.39$ & $12.12$ \\
\hline
\hline
References & PEGASE~\cite{armaly83} & $2.90$ & $4.90$ & $8.60$ & $14.40$ \\
& NSIKE~\cite{NSIKE} & $2.90$ & $4.90$ & $8.40$ & $12.20$ \\
\hline
\end{tabular}
\caption{\label{tabular2} \em Flow over a step: first reattachment length at the steady-state with different Reynolds numbers, mesh of about $6,000$ vertices, time step $0.02$. }
\end{center}
\end{table}

In table~\ref{tabular2}, we compare the first reattachment length at the steady-state, obtained with different methods on a triangular mesh of $6000$ vertices and a time step $dt=0.02$, with the experimental results in~\cite{armaly83} and with the results in~\cite{NSIKE}, at different Reynolds numbers.
It is observed that at moderate Reynolds numbers, the obtained results are in good agreement with the reference values, while at higher Reynolds numbers, they seem to diverge from experimental values (as remarked in several other simulations).
At a high Reynolds number $Re=800$, the monolithic methods have some difficulties to reach the steady-state criterion $\max |\xi^{n+1} - \xi^n| < 10^{-4}$ (not convergence after $20,000$ time steps), while it is not the case with the Fractional Step methods.
It is also remarked in Figure~\ref{fig2} that at $Re=800$, there is an additional recirculating-flow region at the upper wall downstream.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth]{felisce-fig-step-NS-P2-Re100}
        \includegraphics[width=\textwidth]{felisce-fig-step-NS-P2-Re200}
        \includegraphics[width=\textwidth]{felisce-fig-step-NS-P2-Re400}
        \includegraphics[width=\textwidth]{felisce-fig-step-FracStep-P2-Re800}
        %\includegraphics[width=\textwidth]{../../Users/Cuc/NS/ddd_step_autre_NS_P2P1_symStress_bdf1_1proc_semiImplicit_6K_dt0.02_viscosity0.04/streamlines_crop}
        %\includegraphics[width=\textwidth]{../../Users/Cuc/NS/ddd_step_autre_NS_P2P1_symStress_bdf1_1proc_semiImplicit_6K_dt0.02_viscosity0.02/streamlines_crop}
        %\includegraphics[width=\textwidth]{../../Users/Cuc/NS/ddd_step_autre_NS_P2P1_symStress_bdf1_1proc_semiImplicit_6K_dt0.02_viscosity0.01/streamlines_crop}
        %\includegraphics[width=\textwidth]{../../Users/Cuc/NS/ddd_step_autre_NSFracStep_P2P1_symStress_bdf1_12proc_semiImplicit_6K_dt0.02_viscosity0.005/streamlines_crop}
        \caption{\label{fig2} \em Flow over a step: velocity field and streamlines at $Re = 100, 200, 400$ (first three figures from top) with model monolithic, semi-implicit P2/P1 method and at $Re=800$ (bottom) with Fractional Step model, P2/P1 method.}
    \end{center}
\end{figure}

%=====================================
\subsection{Flow around a circular obstacle}

%=====================================
\subsection{3D aorta flow}
In this section, we consider a $3$D computational domain of the aorta with about $69,000$ vertices and $375,000$ tetrahedra (cf. Figure~\ref{fig4-mesh}).
The objectif is to realize a big $3$D computation, to analyse the scalability and to compare the results, for example between P2/P1 and P1/P1 stabilization discretizations with the same method NS semi-implicit.
We denote by label $4$ the inlet at the ascending aorta, by labels $2, 6, 5$ respectively the outlet at the brachiocephalic artery, the left common carotid artery, the left subclavian artery, and by label $3$ the outlet at the diaphragmatic aorta.
Boundary condition on the wall (label $1$) is no-slip condition $u_1=u_2=u_3=0$.
The fluid is supposed to be the blood, then the density and the viscosity are respectively $\rho=1$ and $\mu=0.04$ in CGS units.
\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.4\textwidth]{aorta/aorta_mesh}
\caption{\label{fig4-mesh} \em Aorta flow: computational domain.}
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/velo_L2TotErr_NNtrans}
\includegraphics[width=0.49\textwidth]{aorta/pres_L2TotErr_NNtrans}
\caption{\label{fig4-L2TotErr-NNtrans} \em Aorta flow with transient Neumann Normal condition at the inlet: $l^2(L^2)$ error in velocity (left) and pressure(right) versus time step.}
\end{center}
\end{figure}

%\vspace{0.3cm}
In the first test case, boundary conditions at the four outlets are $\sigma \cdot {\bf n} = 0$.
At the inlet, we use a time variant Neumann Normal condition: $\sigma \cdot {\bf n} = -10 \sin(t) \cdot {\bf n}$ where the initial time is $t_0 = 0$.
The computation is accomplished for the time interval $(t_0, T]$ with the NS semi-implicit method (using second-order time stepping $orderBdfNS=2$), either P2/P1 or P1/P1 stabilization discretization (in the latter case, stabilization parameters are $typeSUPG=1$, $stabSUPG=0.05$ and $stabdiv=0$).
Without any exact solution, we consider as reference $({\bf u}_{ref}, p_{ref})$ the sequential solution with P2/P1 discretization and a very small time step $dt=0.000125$.
With each sequential solution corresponding to a time step of the set ($0.0005, 0.001, 0.002, 0.004, 0.008, 0.016$), we compute the errors
$$
\left| || {\bf u}^n - {\bf u}^n_{ref} ||_{L^2(\Omega)} \right|_{l^2(t_0,T)}, \quad \left| || p^n - p^n_{ref} ||_{L^2(\Omega)} \right|_{l^2(t_0,T)} .
$$
Furthermore, at each outlet, we compute the flux  $\displaystyle\int_\Sigma {\bf u}^n \cdot {\bf n}$ and the mean pressure $\displaystyle\frac{1}{|\Sigma|}\int_\Sigma p^n$.
The reason why we should take the sequential solution in this test of precision is that the parallel result varies slightly from one attempt to another.

Figure~\ref{fig4-L2TotErr-NNtrans} shows that the convergence rate for the velocity is of order $2$ with P2/P1 discretization.
With P1/P1 discretization, this convergence rate is of order slightly higher than $1$ only (the accuracy is saturated for small time steps due to the spatial discretization error).
For the pressure, the convergence rate is of order nearly $1$ for both types of discretization.
We can observe that the $l^2(L^2)$ error with P2/P1 discretization is significantly lower than the corresponding error with P1/P1 discretization and with the same time step.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_1proc_semiImplicit_69K_dtX_NNtrans_Psinus_-10}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P2P1_bdf2_1proc_semiImplicit_69K_dtX_NNtrans_Psinus_-10}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_1proc_semiImplicit_69K_dtX_NNtrans_Psinus_-10}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P2P1_bdf2_1proc_semiImplicit_69K_dtX_NNtrans_Psinus_-10}
\caption{\label{fig4-outlet5-NNtrans} \em Aorta flow with transient Neumann Normal condition at the inlet: flux (top) and mean pressure (bottom) at the outlet label $5$ with different time step and different discretization.}
\end{center}
\end{figure}

Figure~\ref{fig4-outlet5-NNtrans} presents the flux and the mean pressure curves at the outlet label 5 with different time step and different discretization.
It can be observed that the curves in P2/P1 fit in very well with the reference, while in P1/P1 there is a gap between the curves and the reference.
This gap is not negligible in case of pressure: the smaller the time step is, the more the curves approach the reference.
In order to understand the influence of the stabilization parameters to the P1/P1 result, we tried with two other values $0.005$ and $0.1$ of $stabSUPG$.
Figure~\ref{fig4-outlet5-stabSUPG-NNtrans} presents the mean pressure curves at the outlet label 5 with time step $dt = 0.001$ and various parameters $stabSUPG$.
We observe that the smaller the value of $stabSUPG$ ({\it i.e.} the less the problem is stable), the more difficult the resolution of linear systems, but in return the more accurate the result: the nearer the curve approaches the reference.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P1P1_SUPGtype1_stabSUPGX_stabdiv0_bdf2_1proc_semiImplicit_69K_dt1e-3_NNtrans_Psinus_-10}
\caption{\label{fig4-outlet5-stabSUPG-NNtrans} \em Aorta flow with transient Neumann Normal condition at the inlet: mean pressure at the outlet label $5$ with time step $dt = 0.001$ and various parameters $stabSUPG$ in P1/P1 discretization.}
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_Xproc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-10}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P2P1_bdf2_Xproc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-10}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_Xproc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-10}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P2P1_bdf2_Xproc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-10}
\caption{\label{fig4-outlet5-parallel-NNtrans} \em Aorta flow with transient Neumann Normal condition at the inlet: comparison between sequential and parallel of flux (top) and mean pressure(bottom) at the outlet label $5$ with time step $dt=0.002$ and different discretization.}
\end{center}
\end{figure}

Regarding the parallel resolution, we plot in Figure~\ref{fig4-outlet5-parallel-NNtrans} the flux and the mean pressure curves at the outlet label 5 with time step $dt=0.002$, in sequential and in parallel with $8$ processors.
We observe that all the parallel curves are superposable on the sequential ones.
The result is similar with any different time step.
It shows that the code {\felisce} runs well in parallel for this test case.

\vspace{0.3cm}
In the second test, the boundary condition at the inlet is $\sigma \cdot {\bf n} = \left( p_{in} - 1000 \sin(t) \right) \cdot {\bf n}$ where $t_0 = 0$ and $p_{in}$ is the initial pressure at the inlet.
We replace the Neumann Normal boundary conditions at the four outlets by the RCR Windkessel conditions, which are all implemented either explicitly or implicitly, combined with either P2/P1 or P1/P1 stabilization discretization of the whole problem.
The stabilization parameters are kept unchanged, in addition the coefficient of inflow/outflow stabilization is fixed at $stabilizationCoef=0.2$.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/velo_L2TotErr_NNWindk}
\includegraphics[width=0.49\textwidth]{aorta/pres_L2TotErr_NNWindk}
\caption{\label{fig4-L2TotErr-NNWindk} \em Aorta flow with Neumann Normal-Windkessel boundary conditions: $l^2(L^2)$ error in velocity (left) and pressure(right) versus time step. Dashed lines depict the slope of order $1$ in velocity and in pressure.}
\end{center}
\end{figure}

We consider as reference $({\bf u}_{ref}, p_{ref})$ the sequential solution with P2/P1 discretization, implicit Windkessel and the time step $dt=0.000125$.
For a quite large time step $dt \geq 0.004$, the computation with P2/P1 discretization stops before the final time $T=0.048$ due to the failure in linear system resolution.
Figure~\ref{fig4-L2TotErr-NNWindk} shows that the convergence rate for the velocity is of order $1$ with both types of discretization and both types of Windkessel implementation.
With the same discretization, the errors in explicit and implicit Windkessel are identical.
Furthermore, the error in P2/P1 is lower than that in P1/P1.
For the pressure, the order of convergence rate is slightly greater than $1$ for both types of discretization and both types of Windkessel implementation.
Not as expected, with the same Windkessel implementation, the error in P2/P1 is slightly higher than that in P1/P1 and with the same discretization, the error in implicit Windkessel is higher than in explicit Windkessel.
Note that this behavior is observed for this type of fluid only (the blood) and may not be similar for other fluid (the air for example).

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_1proc_semiImplicit_69K_dt5e-4_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_1proc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P2P1_bdf2_1proc_semiImplicit_69K_dt5e-4_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\includegraphics[width=0.49\textwidth]{aorta/flux_aorta_sang_NS_P2P1_bdf2_1proc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\caption{\label{fig4-outlet5-flux-NNWindk} \em Aorta flow with Neumann Normal-Windkessel boundary conditions: flux at the outlet label $5$ with different time step and different discretization.}
\end{center}
\end{figure}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_1proc_semiImplicit_69K_dt5e-4_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P1P1_SUPGtype1_stabSUPG5e-2_stabdiv0_bdf2_1proc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P2P1_bdf2_1proc_semiImplicit_69K_dt5e-4_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\includegraphics[width=0.49\textwidth]{aorta/pres_aorta_sang_NS_P2P1_bdf2_1proc_semiImplicit_69K_dt2e-3_NNtrans_Psinus_-1000_Windk_ImplExpl_bazilevsPartout_coef2e-1}
\caption{\label{fig4-outlet5-pres-NNWindk} \em Aorta flow with Neumann Normal-Windkessel boundary conditions: mean pressure at the outlet label $5$ with different time step and different discretization.}
\end{center}
\end{figure}

Figures~\ref{fig4-outlet5-flux-NNWindk} and~\ref{fig4-outlet5-pres-NNWindk} presents the flux and the mean pressure curves at the outlet label 5 with different time step and different discretization.
It can be observed that for a sufficiently small time step ($dt=0.0005$), all the curves corresponding to explicit or implicit implementation, to P2/P1 or P1/P1 discretization fit in very well with the reference.
For a greater time step ($dt=0.002$), the gap between explicit and implicit curves is more clear, while there is almost no difference between P2/P1 and P1/P1 curves.
We can resume that for this test case, the optimal option is a sufficiently small time step with explicit Windkessel implementation and P1/P1 discretization (see the CPU time in the following).

\begin{table}[htbp]
\begin{center}
\begin{tabular}{| c | r | r | r | r |}
\hline
Method & 1 processor & 2 processors & 4 processors & 8 processors \\
\hline
P1/P1 stab. - expl. Windk. & $741.61$ & $404.10$ & $207.52$ & $114.74$ \\
P1/P1 stab. - impl. Windk. & $910.58$ & $554.21$ & $314.44$ & $223.93$ \\
P2/P1 - expl. Windk. & $4742.42$ & $2402.27$ & $1328.58$ & $802.82$ \\
P2/P1 - impl. Windk. & $8811.32$ &  $6384.39$ & $4590.99$ & $4279.02$ \\
\hline
\end{tabular}
\caption{\label{tabular4_cputime} \em Aorta flow with Windkessel boundary conditions: CPU time (in seconds) after $5$ iterations, mesh of about $69,000$ vertices and $375,000$ tetrahedra, time step $0.0005$. }
\end{center}
\end{table}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=0.5\textwidth]{felisce-fig-aorta-Windk-scalability}
\caption{\label{fig4-scalability} \em Aorta flow with Windkessel boundary conditions: analysis of the scalability.}
\end{center}
\end{figure}

\vspace{0.3cm}
We keep now all the Windkessel boundary conditions at the four outlets and replace the Neumann Normal boundary condition at the inlet by the RC Windkessel condition.
These conditions are implemented either explicitly or implicitly, combined with either P2/P1 or P1/P1 stabilization discretization of the whole problem.
The initial time is $t_0 = 0.02$, the stabilization parameters are kept unchanged.
In order to analyze the scalability, we choose the time step $dt=0.0005$ and compare the CPU time after $5$ iterations with $1, 2, 4, 8$ processors, which is presented in the Table~\ref{tabular4_cputime}.
It is normal that the implicit computation is much slower than the explicit one since the matrix structure is changed by many terms added (due to the coupling of all the inlet/outlet).
It is observed by a team member that the waste of CPU time resides in the partial LU decomposition for the preconditioning.
The scalability of the explicit methods is very good (cf. Figure~\ref{fig4-scalability}).
However, with the implicit methods, the scalability is degraded because of several reasons, for instance the reunification of information from all the processors.

%=====================================
\section{Remarks, conclusions}

%=====================================
