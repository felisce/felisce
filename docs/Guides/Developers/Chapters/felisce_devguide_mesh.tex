\chapter{Mesh}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mesh: \felisce's basic concepts}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The geometric elements are defined in the {\sf Geometry} repository, in particular in the {\sf
  GeometricMeshRegion} class. This class contains the structure that are used for the
geometric entities. These entities are different from the finite elements objects that
are defined in the {\sf FiniteElement} repository.
The mesh is read/written using input/output functions that live in the
{\sf IO} class in the {\sf InputOutput} repository. 

During a typical {\felisce} execution, a global mesh is first read from a given format ({\sf medit} 
or {\sf ensight} are the only supported formats so far). Then it is transformed into 
the {\felisce} format (one vector per type of element, elements ordered by increasing reference, numbering 
from 0, \ldots). Later, after having partitionned the mesh elements, the global mesh is
broken into submeshes for each processor. In the sequel, the global mesh can be post-processed  
in the chosen format (not necessarily the same as the input mesh), along with the solution, when required.

%%%%=============
\subsection{The {\sf GeometricMeshRegion} class} 
%%%%=============


A {\felisce} mesh is described by a {\sf GeometricMeshRegion} class, that consists essentially of 
list of points and a list of elements stored in a particular fashion, plus some 
helper functions. Here follows some important features of {\sf GeometricMeshRegion}
(and related classes).

\begin{itemize}
\item {\bf The list of points:} {\tt vector< Point > \_listPoints} of size (\texttt{felInt
    \_numPoints}). It contains all the coordinates of the mesh points.
  \begin{itemize}
  \item A {\tt Point} is a class containing the 3 coordinates of the point ({\tt double \_coor[3]}).
    When a 2D mesh is read, to the read coordinates $x$ and $y$, the coordinate $z=0.0$ is added. 
  \end{itemize}
\item {\bf The list of elements:} it is a list of vectors 
  \texttt{felInt* \_listElements[ \_numTypesOfElement ]}.
  The idea is to store the elements and all the necessary information (number
  of elements, number of points per element, etc.) by {\bf types of element}.
  See an example in Figure~\ref{geomesh:smallmesh}.

  \begin{itemize}
  \item There are as many vectors in the list as the number of
    types of elements (\texttt{static const int \_numTypesOfElement = 18} types
    of elements are programmed today, see Table~\ref{tab:elements}). 
    Obviously, only some of the vectors are non-empty, depending on the actual mesh you are
    using.

  \item A single geometric element is made up of {\tt \_numPointsPerElt} integers
    which are the point IDs that define the element (vertices, edge points, face
    points, volume points). 

  \item Each vector stores all the
    elements of a certain type. For instance, for a mesh containing {\tt N} Q1 hexaedric elements,
    the vector {\tt \_listElements[Hexa8]} contains {\tt N *
      \_numPointsPerElt} integers, \ie{} {\tt 8*N} integers that are the
    8 point IDs of {\tt N} elements.

  \item The elements are stored in a vector form. 
    The point IDs are always numbered between 0 and {\tt
      \_numElements[eltType] * \_numPointsPerElt[eltType] - 1}.
    
    For example, the first hexaedron is stored between {\tt
      \_listElements[Hexa8][0]} and {\tt \_listElements[Hexa8][7]}, 
    the second  hexaedron is stored between {\tt \_listElements[Hexa8][8]} and {\tt
      \_listElements[Hexa8][15]}, and so on. (The vector of elements is {\em not} an array.)


  \item  {\bf The elements (of a given type) are sorted by increasing reference}, in order to
    provide an easy access to the elements having a certain reference.
    The sort is always performed right after reading the mesh.
    For instance, when reading a {\sf medit} format mesh, 
    the sort is made using the method {\tt reorderListElePerRef(map< int, vector<felInt> > \& 
      RefToElements, ElementType eltType)} in the {\sf Medit} class. 


    \warn To read/write the meshes (in the {\sf ensight} or {\sf medit} format, see the
    {\sf InputOutput} repository), it is necessary to decrement/increment the
    point IDs (as these formats use a numbering starting from 1).

  \item The access to an element can be performed using the methods {\tt
      getOneElement(...)} or {\tt setOneElement(...)}.

  \end{itemize}

\item {\bf A map: ref$\rightarrow<$pos, numEle$>$} (see 
  Figure~\ref{geomesh:smallmesh}), that associates an 
  element reference (or label) to a pair: first,
  the starting index to the elements having this reference, and second the
  number of elements having this reference,
  ({\tt map< int, pair<felInt,felInt> >  intRefToBegEndMaps[ \_numTypesOfElement]}).

\item {\bf A method that breaks a global mesh into submeshes}, if a partition of all the
existing elements by {\sf metis} or {\sf parmetis} is provided. The ordering of the elements by
increasing references is preserved. 

{\tt  setLocalMesh(const GeometricMeshRegion\& meshGlobal, const vector<int>\& eltPartition, 
const int\& rank, vector<felInt>\& loc2globElem)}.


\item there are 2 different notions to describe the dimension of the mesh:
  \begin{itemize}
  \item {\tt \_numCoor}: is the number of coordinates of the mesh, as read from mesh file. 
    In {\sf medit} format, this variable is called {\it Dimension} and can be equal to 2 ($x,y$) or 3 ($x,y,z$). 
    Note that a {\felisce} {\sf Point} always have 3 coordinates ({\tt Point.z()} is set to $0$ if {\tt \_numCoor=2}).

    This variable should have little impact on the code (only used for input/output of mesh).
    
  \item {\tt \_domainDim}: is the dimension of the mesh, depending on the existing type of elements. 
    \begin{itemize}
    \item When there are 3D geometric elements ({\tt Hexa8}, say), {\tt \_domainDim = GeoMesh3D (=3)}. 
    \item Else, when there are 2D geometric elements ({\tt Quad4}, say), {\tt \_domainDim = GeoMesh2D (=2)}.  
    \item Else, when there are 1D geometric elements ({\tt Seg2}, say), {\tt \_domainDim = GeoMesh1D (=1)}.  
    \item Else, when there are 0D geometric elements ({\tt Nod}), {\tt \_domainDim = GeoMesh0D (=0)}.
    \item Else, {\tt \_domainDim = GeoMeshUndefined}.
    \end{itemize}
  \end{itemize}

\item {\bf Some ``bags'' of elements} are available to help the manipulation.
  \begin{itemize}
  \item the bag of type of elements constituting the domain (in the mesh provided by the user) {\tt vector<ElementType> \_bagElementTypeDomain}. 
    It contains only the non-empty types of element of dimension {\tt \_domainDim}.

    
  \item the bag of type of elements constituting the boundary of the domain {\tt vector<ElementType> \_bagElementTypeDomainBoundary}. 
    It contains only the non-empty types of element of dimension {\tt \_domainDim - 1}.

    Ex.: let a 3D mesh containing {\tt \{Tetra4, Hexa27, Tria3, Quad9, Seg2,
      Seg3\}} be given by the user. The bag {\tt \_bagElementTypeDomain} is thus
    composed of the types of elements {\tt \{Tetra4, Hexa27\}}, whereas
    the bag of {\tt \_bagElementTypeDomainBoundary} is made up of {\tt \{Tria3, Quad9\}}.

  \item the bag of some useful types of element:
    \begin{itemize}
    \item all elements of dimension {\tt N}:
      {\tt vector<ElementType> \_bagElementTypeND}, where {\tt N} is equal to 0, 1, 2 or 3.

      Ex.: 2D elements are {\tt \{Tria3, Tria6, Quad4, Quad8, Quad9\} }.
    \item all linear elements:  {\tt \{Seg2, Tria3, Quad4, Tetra4, Pyram5, Prism6, Hexa8\} } are stored in
      {\tt vector<ElementType> \_bagElementTypeLinear}.
    \end{itemize}
  \end{itemize}
    
\item {\bf Tools to locate points} in the mesh. 
  
  \todo{These functions should be moved to another file or class. They also
    need documentation/renaming/consistency with the rest of the code...}

\item \ldots

\end{itemize}

\warn Note that the integers used for large vector indices are {\tt felInt}, \ie{} {\tt long int}.

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{felisce-fig-ex-small-mesh}
\end{center}
\caption{Example of a small mesh and its implementation in {\felisce}. Left: image and mesh
in a {\sf medit}-like format (note however that the numbering starts from 0 here). 
%
Right: mesh as implemented
in the {\sf GeometricMeshRegion} class. 
The tetrahedron IDs are in squared box, the triangle IDs are in magenta, the point IDs are denoted in 
red, and the references are in brown. The pointers are in italic blue, and the number of elements are 
in blue. 
%
The elements of a given type are contained in a vector. The 
elements are ordered by increasing references. An access to each pack of elements having a given 
reference is provided with a map: ref$\rightarrow<$ starting position, number of elements $>$.}
\label{geomesh:smallmesh}
\end{figure}

%%%%=============
\subsection{Types of geometric finite elements}
%%%%=============

The types of geometric elements that are available in {\felisce} are listed in the
Table~\ref{tab:elements} and illustrated in Figure~\ref{geomesh:FE}. For each
type of element, we provide
its {\sf enum} name, a short description, its finite element name ({\sf
  string}), its dimension, its number of nodes, of 
vertex nodes, of edge nodes,  
of face nodes, of volume nodes, the number and type
of faces, its order (linear, bilinear... The denomination (P1 or R2 for
instance) is the one used in the Finite Element definition). 



\hspace*{-1cm}\begin{tabular}{lllllllllll}\label{tab:elements}
Enum & Description       & {\sf string}   & dim &  nod & vert & edg  & fac & vol & faces & lin/bil. \\
\hline 
Nod  & nodal element & ``Nodes''      & 0 & 1 & 0 &  0 & 0 & 0 &  &  \\ 
\hline 
Seg2  & P1 segment elem & ``Segments2''      & 1 & 2 & 2 &  0 & 0 & 0 &  & P1 \\ 
Seg3  & P2 segment elem & ``Segments3''      & 1 & 3 & 2 &  1 & 0 & 0 &  & P2 \\ 
\hline 
Tria3  & P1 triangle elem & ``Triangles3''      & 2 & 3 & 3 &  0 & 0 & 0 & 1 tria & P1 \\ 
Tria6  & P2 triangle elem & ``Triangles6''      & 2 & 6 & 3 &  3 & 0 & 0 & 1 tria & P2 \\ 
\hline 
Quad4  & Q1 quadrangle elem & ``Quadrangles4''      & 2 & 4 & 4 &  0 & 0 & 0 & 1 quad & Q1 \\ 
Quad8  & Q2 quadrangle elem & ``Quadrangles8''      & 2 & 8 & 4 &  4 & 0 & 0 & 1 quad & Q2 \\ 
Quad9  & full Q2 quad elem & ``Quadrangles9'' & 2 & 9 & 4 &  4 & 1 & 0 & 1 quad & Q2 \\ 
\hline 
Tetra4  & P1 tetraedric elem  & ``Tetrahedra4'' & 3 & 4  & 4 &  0 & 0 & 0 & 4 tria & P1 \\ 
Tetra10 & P2 tetraedric elem & ``Tetrahedra10'' & 3 & 10 & 4 &  6 & 0 & 0 & 4 tria & P2 \\ 
\hline 
Pyram5  & P1 pyramid elem  & ``Pyramids5''  & 3 & 5 & 5 &  0 & 0 & 0 & 4 tria+1 quad & S1 \\ 
Pyram13 & P2 pyramid elem & ``Pyramids13'' & 3 & 13 & 5 &  8 & 0 & 0 & 4 tria+1 quad & S2 \\ 
\hline 
Prism6 & P1 prismatic elem  & ``Prisms6''  & 3 &  6 & 6 &  0 & 0 & 0 & 2 tria+3 quad & R1 \\ 
Prism15 & P2 prismatic elem & ``Prisms15'' & 3 & 15 & 6 &  9 & 0 & 0 & 2 tria+3 quad & R2 \\ 
\hline 
Hexa8 & Q1 hexaedric elem       & ``Hexaedra8''  & 3 &  8 & 8 &  0 & 0 & 0 & 6 quad & Q1 \\ 
Hexa20 & edge Q2 hexa elem & ``Hexaedra20'' & 3 & 20 & 8 & 12 & 0 & 0 & 6 quad & Q2 \\ 
Hexa26 & edge+face Q2 hexa     & ``Hexaedra26'' & 3 & 26 & 8 & 12 & 6 & 0 & 6 quad & Q2 \\ 
Hexa27 & full Q2 hexa elem & ``Hexaedra27'' & 3 & 27 & 8 & 12 & 6 & 1 & 6 quad & Q2 \\ 
\end{tabular} 
\vspace*{0.5cm}


\warn Note that not all the geometric elements have been used. For instance there are
{\bf no} finite elements implemented for the pyramid elements so far, and the
prism finite elements have been little tested (or not at all).



\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{felisce-fig-finite-elements}
\end{center}
\caption{Geometric finite elements that are available in {\felisce}'s geometric
  mesh class. The vertex and edge nodes are denoted with a circle, the face
  node with a square and the volumic node with a cross.}
\label{geomesh:FE}
\end{figure}



\newpage
%%%%=============
\subsection{Mesh: the edges}
%%%%=============

The {\felisce}'s {\sf ListEdges} class consists essentially of 2 arrays:
\begin{itemize}
\item a list of pointers to the edges starting with a given vertex ID {\tt
    vector<Edge*> \_listBeginEdge}.
    This list's length is the number of points in the mesh. As not all points
    are the beginning of an edge, some pointers are {\tt NULL}.

\item a list of pointers to the edges {\tt vector<Edge*>  \_listEdges}.

\item the number of edges {\tt felInt \_numEdges}.
  Once the list of edges construction is finished, the length of {\tt
    \_listEdges} is equal to {\tt  \_numEdges}.

\item the number of edges that were provided by the user's mesh  {\tt felInt
    \_numGivenEdges}. The first {\tt \_numGivenEdges} edges in {\tt
    \_listEdges} are the given edges (except if in the mesh, there exist
  redundant edges: in this case, the redundant edges are kept only once).
\end{itemize}

 
\noindent The {\sf Edge} (\todo change this name to {\sf GeoEdge}) class is made up of
\begin{itemize}
\item the ID of the starting and ending points {\tt felInt \_idBeg} and {\tt
    \_idEnd}, 
  
  with the {\bf convention} that {\tt \_idBeg < \_idEnd}.

\item the pointer to the next edge having the same starting point ID  ({\tt Edge*  \_ptrNext}).

\item the ID of the edge {\tt felInt \_id}. 

  The edge ID is the position in the list {\tt \_listEdges}, and corresponds to
  the insertion number of the edge (the edge 0 was the first inserted...).

\item a list of pointers to the neighbouring faces.

\item  another list of pointers to the neighbouring volumes.
\end{itemize}

The principle is that a geometric edge (given by two points, no matter the
orientation) is stored only once.  
This structure based on two pointer arrays allows an efficient construction 
and search of edges, see Figure~\ref{geomesh:build:edges}. 
In particular, during construction, 
the check whether an edge was already built or not is simple and cheap.

The algorithm for constructing a list of edges can be summarized as follows: for each (face) 
element, get the local edges. For each local edge, order the edge by increasing vertex identity, and insert
the edge in the array, if it does not exist already. 
The search/insert phase of an edge {\tt [pt0, pt1]} ({\tt pt0<pt1}) is very efficient
as you only search among the edges starting with the vertex identity {\tt pt0}, using the pointer to next 
(third column in {\tt ListEdges} in Figure~\ref{geomesh:build:edges}). The algorithm is
detailed in Section~\ref{sec:build:edges}.

Searching a particular edge is performed
in a very similar (efficient) manner, see Section~\ref{sec:find:edge}.
Note that the scan through all edges can be
done in two ways, see Section~\ref{sec:scan:edges}.

\smallskip
\detail{Note that the ID of the first point in the edge is redundant, as it is already stored as the index
of the corresponding entry in {\tt \_listBeginEdge}: in Figure~\ref{geomesh:build:edges}, 
the point {\tt pt0} in the first column of {\tt ListEdges} is equal to the index of the corresponding pointer
in  {\tt ListBegin}. Ex. for {\tt pt0=6}, all the edges ([6,9], [6,8], [6, 10])  
start from the pointer $1$ (in blue) which is precisely at index $6$({\tt = pt0}) (red)
in {\tt ListBegin}).
However, we keep this information for convenience, but 
it could be removed for efficiency.}

\begin{figure}[htbp]
\begin{center}
\includegraphics[width=1\textwidth]{felisce-fig-build-edges-step1}\\
\includegraphics[width=1\textwidth]{felisce-fig-build-edges-step5}
\end{center}
\caption{Exemple of construction of the edges in {\felisce} on a simple 2D mesh.
The {\sf ListEdge} class consists essentially of 2 arrays: {\tt \_listBeginEdge}
and {\tt \_listEdges}. 
Top: after the first step in the construction loop, bottom: after 5 steps.
The element IDs are in squared box, the point IDs are denoted in red and the 
pointers are in italic blue. The local edge numbering for each element is given on 
the right of the figure (the edges that have already been insterted are striked out).}
\label{geomesh:build:edges}
\end{figure}

%%--------
\subsubsection{Edges: algorithm for edge list construction} \label{sec:build:edges}
%%--------

The algorithm that builds the list of all edges {\tt ListEdges \_listEdges} is
detailed in the following lines. {\tt ListEdges::\_numEdges} is the counter of
edges that have been inserted in the list at each steps (it is set to 0 at the
construction of {\tt \_listEdges}).

\texttt{
\begin{tabbing}
\quad \= \quad \= \quad \= \quad \= \quad \\
void buildEdges() \{\\
\> {\bf for} itype = all types of elements \\
\> \> {\bf for} elem = all elements(itype)\\
\> \> \>{\bf for} locEdge = all edges(elem)\\
\> \> \> \> pt0 = min( locEdge(0), locEdge(1) )\\
\> \> \> \> pt1 = max( locEdge(0), locEdge(1) )\\
\> \> \> \> \_listEdges.searchAdd(pt0, pt1)\\
\> \> \>{\bf endfor} locEdge \\
\> \> {\bf endfor} elem \\
\> {\bf endfor} itype \\
\} \\
\end{tabbing}
}
where {\tt searchAdd} is as follows:
\texttt{
\begin{tabbing}
\quad \= \quad \= \quad \= \quad \= \quad \\
void searchAdd(pt0, pt1) \{\\
\> begin\_edge\_ptr = \_listBeginEdge[ pt0 ]; \\
\> {\bf if} ( begin\_edge\_ptr == NULL ) // no edge with starting vertex=pt0 so far: add it! \\
\> \> ptr\_new\_edge = createEdge(pt0, pt1) \\
\> \> \_listBeginEdge[ pt0 ] = ptr\_new\_edge  // store the pointer on this new edge\\
\> \> \_listEdges.push\_back( ptr\_new\_edge ) // store the new edge at the end of list\\
\> \> \_numEdges = \_numEdges + 1 \\
\> \> {\bf return} \\
\> {\bf endif} \\
\>  // an edge has already starting vertex=pt0: scan all such edges  \\
\> current\_edge\_ptr = begin\_edge\_ptr \\
\> {\bf do} \\
\> \> previous\_edge\_ptr = current\_edge\_ptr \\
\> \> {\bf if} ( previous\_edge\_ptr->idEnd != pt1 ) // compare 2 edges: edge not found \\
\> \> \> current\_edge\_ptr = previous\_edge\_ptr->ptrNext  // go to the next edge \\
\> \> {\bf else} // edge already inserted \\
\> \> \>  {\bf return} // do not insert this edge\\
\> \> {\bf endif} \\
\> {\bf while} ( current\_edge\_ptr != NULL ) \\
\> // new edge with starting vertex=pt0: add it!\\
\> ptr\_new\_edge = createEdge(pt0, pt1) \\
\> \_listEdges.push\_back( ptr\_new\_edge ) // store the new edge at the end of list\\
\> // link this new edge to the previous edge with starting vertex=pt0: \\
\> previous\_edge\_ptr->ptrNext = \_listEdges[\_numEdges]  \\
\> \_numEdges = \_numEdges + 1 \\
\}\\
\end{tabbing}
}
where {\tt createEdge} is as follows:
\texttt{
\begin{tabbing}
\quad \= \quad \= \quad \= \quad \= \quad \\
Edge* createEdge(pt0, pt1) \{\\
\> allocate NewEdge \\
\> NewEdge.idBeg   = pt0 \\
\> NewEdge.idEnd   = pt1 \\
\> NewEdge.id      = \_numEdges \\
\> NewEdge.ptrNext = NULL \\
\> return pointer on NewEdge \\
\}
\end{tabbing}
}

Note that one does during the search/add of edge, the linking to the neighbouring faces and
volumes. It is not reported here for the sake of simplicity.


%%--------
\subsubsection{Edges: algorithm for searching one edge} \label{sec:find:edge}
%%--------
Searching an edge in the list is done in the same way as the edge list construction:
\texttt{
\begin{tabbing}
\quad \= \quad \= \quad \= \quad \= \quad \\
int findEdge(pt0, pt1) \{\\
\> ASSERT(pt0 < pt1) \\
\> current\_edge\_ptr = \_listBeginEdge[ pt0 ] \\
\> {\bf if} ( begin\_edge\_ptr == NULL )  \\
\> \> WARNING('no such starting vertex') \\
\> \> return -1 \\
\> {\bf endif} \\
\> {\bf do} \\
\> \> {\bf if} ( current\_edge\_ptr->idEnd == pt1 ) \\
\> \> \> {\bf return} current\_edge\_ptr->id \\
\> \> {\bf else} // go to the next edge \\
\> \> \> current\_edge\_ptr = current\_edge\_ptr->ptrNext  \\
\> \> {\bf endif} \\
\> {\bf while} ( current\_edge\_ptr != NULL ) \\
\> WARNING('edge not found') \\
\> return -1 \\
\}
\end{tabbing}
}

%%--------
\subsubsection{Edges: algorithm for scanning all edge} \label{sec:scan:edges}
%%--------
When you want to scan all the edges, there are two possibilities:
\begin{itemize}
\item you can go through all the list of edges {\tt  \_listEdges} one by one
  (without using the pointer to the next edge {\tt \_ptrNext}). 

  In this way,
  the ordering will be by increasing edge ID.

\item you can go through all the list of pointers to edges starting with a
  given vertex {\tt \_listBeginEdge}. Then for each starting vertex, you scan
  all edges starting with this vertex, following the track given by the
  pointers to the next edge {\tt \_ptrNext}.

  In this way, the ordering will be by increasing starting point IDs.
\end{itemize}
For more details, have a look at {\tt ListEdges::print()}.

%%--------
\subsubsection{Edges:  neighbouring elements}
%%--------
Three almost identical neighbouring element classes are provided:
{\tt NeighFacesOfEdges}, {\tt NeighVolumesOfEdges} and {\tt
  NeighVolumesOfFaces}.

They are basically made up of:
\begin{itemize}
\item the ID of the element {\tt  felInt \_idFace} or {\tt  felInt \_idVolume} .

\item the type of element {\tt ElementType \_typeFace} or {\tt ElementType \_typeVolume}.

\item  the local position of the element in the neighbouring element {\tt int
    \_idLocalEdge} or {\tt int \_idLocalFace}. 
  
  Ex.: an edge can be in the local position $\{0, 1, 2\}$ of a neighbouring triangle.
\end{itemize}

\todo a good idea would be to create a single class instead of three.


%%%%=============
\subsection{Mesh: faces}
%%%%=============
The {\felisce}'s {\sf ListFaces} class is largely similar to the {\sf
  ListEdges} class, so we will only focus on the differences. These differences
 are related to the additionnal difficulty that a face can have 3 or 4 points,
 and a face can have two possible orientations, even when the first point is fixed.

\begin{itemize}
\item Differences in the {\sf Face} class (\todo change this name to {\sf GeoFace}):

  \begin{itemize}
  \item there is a list of point IDs defining the face: {\tt vector<felInt>
    \_vecFace}. A face can be either a triangle (3 points) or a quadrangle (4
    points). 

  \item there is no list of pointers to the neighbouring faces (not relevant),
    only to the neighbouring volumes. 
  \end{itemize}

\item The algorithms are identical, but are applied with some differences:
  \begin{itemize}
  \item the ordering of the points of face matters.    
    For the faces, we have the following {\bf convention}: 

    The first point is the smallest, the rest of the points are ordered to keep
    the orientation of the face. In the list of faces, the chosen orientation
    is the one of the first inserted face (so that the given faces keep their
    original orientations).

    Ex.: the triangle $\{129, 12, 34\}$ will be reordered as $\{12, 34, 129\}$,
    keeping its orientation.



  \item the comparison of two faces is more involved.
    \begin{itemize}
    \item It is necessary to check that all points (3 or 4) are equal.
    \item The same face can be viewed from the front or from the back. Thus,
      to check if two faces are geometrically equal, 
      two comparisons are necessary: frontview(face1)==frontview(face0)? 
      and frontview(face1)==backview(face0)?

      Ex.: the triangles $\{12, 34, 129\}$ and $\{12, 129, 34\}$ are 
      geometrically the same (although they have two different orientations),
      and the face should be inserted only once. The ``backview'' of $\{12, 34,
      129\}$ is indeed $\{12, 129, 34\}$.
      

    \end{itemize}
    
  \end{itemize}

\end{itemize}