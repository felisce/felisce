# Documentation

Here the documentation is defined.

- **Doxygen**: Contains the Doxygen configuration file (`Doxyfile`) and the generated documentation pages.
- **Guides**: The LaTeX guides for users and developers (code, the generated files can be found in the [documentation repository](https://gitlab.inria.fr/felisce/extra/documentation)).
