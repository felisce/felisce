//   ______ ______ _    _  _____    ______
//  |  ____|  ____| |  (_)/ ____|  |  ____|
//  | |__  | |__  | |   _| (___   ___| |__
//  |  __| |  __| | |  | |\___ \ / __|  __|
//  | |  | |____| |____| |____) | (__| |____
//  |_|  |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  Vicente Mataix Ferrandiz
//

#if !defined(FELISCE_TABLE_HPP_INCLUDED )
#define  FELISCE_TABLE_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>
#include <array>
#include <vector>

// External includes

// Project includes
#include "Core/shared_pointers.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class TableInterpolation
 * @brief This class represents the value of its variable depending to other variable.
 * @details TableInterpolation class stores the value of its second variable respect to the value of its first variable.
 * It also provides a double to double table with piecewise linear interpolator/extrapolator for getting intermediate values.
 * @author Vicente Mataix Ferrandoz
 */
class TableInterpolation
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of TableInterpolation
  FELISCE_CLASS_POINTER_DEFINITION(TableInterpolation);

  typedef std::array<double, 1> result_row_type;

  typedef std::pair<double, result_row_type> RecordType;

  typedef std::vector<RecordType> TableInterpolationContainerType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  TableInterpolation() : mData()
  {
  }

  /// Copy constructor.
  TableInterpolation(TableInterpolation const& rOther)
    : mData(rOther.mData),
      mName(rOther.mName),
      mVariablesNames(rOther.mVariablesNames)
  {

  }

  /// Matrix constructor. the template parameter must have (i,j) access operator and  size1 methods defined.
  template<class TMatrixType>
  TableInterpolation(TMatrixType const& rThisMatrix): mData()
  {
    for(std::size_t i = 0 ; i < rThisMatrix.size1() ; i++) {
      PushBack(rThisMatrix(i,0), rThisMatrix(i,1));
    }
  }

  /// Vector constructor
  template<class TVectorType>
  TableInterpolation(TVectorType const& rThisVector1, TVectorType const& rThisVector2) : mData()
  {
    for(std::size_t i = 0 ; i < rThisVector1.size() ; i++) {
      PushBack(rThisVector1[i], rThisVector2[i]);
    }
  }

  /// Destructor.
  virtual ~TableInterpolation()
  {
  }

  /// Assignment operator.
  TableInterpolation& operator=(TableInterpolation const& rOther)
  {
      mData = rOther.mData;
      mName = rOther.mName;
      mVariablesNames = rOther.mVariablesNames;
      return *this;
  }

  ///@}
  ///@name Operators
  ///@{

  // I want to put operator(i,j) for accessing, operator(i) for first column and operator[i] for getting the complete row

  // This operator calculates the piecewise linear interpolation for
  // given argument
  double operator()(const double X) const
  {
      return GetValue(X);
  }

  // This operator gives the result for the nearest value to argument found in table
  const double & operator[](const double X) const
  {
      return GetNearestValue(X);
  }

  // This operator gives the result for the nearest value to argument found in table
  double & operator[](double& X)
  {
      return GetNearestValue(X);
  }

  ///@}
  ///@name Operations
  ///@{

  // Get the value for the given argument using piecewise linear
  double GetValue(const double X) const;

  // Get the nesrest value for the given argument
  result_row_type& GetNearestRow(const double X);

  // Get the nesrest value for the given argument
  const double& GetNearestValue(const double X) const;

  // Get the nesrest value for the given argument
  double& GetNearestValue(const double X);

  // Interpolate the value
  double& Interpolate(const double X, const double X1, const double Y1, const double X2, const double Y2, double& Result) const;

  // inserts a row in a sorted position where Xi-1 < X < Xi+1 and fills the first column with Y
  void insert(const double X, const double Y);

  // inserts a row in a sorted position where Xi-1 < X < Xi+1
  void insert(const double X, const result_row_type& Y);

  // assumes that the X is the greater than the last argument and put the row at the end.
  // faster than insert.
  void PushBack(const double X, const double Y);

  // Get the derivative for the given argument using piecewise linear
  double GetDerivative(const double X) const;

  // This interpolates the derivative
  double& InterpolateDerivative( const double X1, const double Y1, const double X2, const double Y2, double& Result) const;

  /**
   * @brief This returns the table internal data
   * @return The internal data
   */
  const std::array<std::vector<double>, 2> GetTableAsVectors() const
  {
    const std::size_t size = mData.size();
    std::vector<double> vector_x(size);
    std::vector<double> vector_y(size);
    for(std::size_t i = 0 ; i < size ; i++) {
      vector_x[i] = mData[i].first;
      vector_y[i] = mData[i].second[0];
    }

    const std::array<std::vector<double>, 2> aux_vectors({vector_x, vector_y});
    return aux_vectors;
  }

  /**
   * @brief This method clears databse
   */
  void Clear()
  {
      mData.clear();
  }

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief This returns the table internal data
   * @return The internal data
   */
  TableInterpolationContainerType& Data()
  {
      return mData;
  }

  /**
   * @brief This returns the table internal data (constant version)
   * @return The internal data
   */
  TableInterpolationContainerType const& Data() const
  {
      return mData;
  }

  /**
   * @brief This method sets the variables names
   * @param rName1 The name of the first variable
   * @param rName2 The name of the second variable
   */
  void SetVariablesNames(
    const std::string& rName1,
    const std::string& rName2
    )
  {
    mVariablesNames[0] = rName1;
    mVariablesNames[1] = rName2;
  }

  /**
   * @brief This method retrieves the names of the variables in the table
   */
  std::array<std::string, 2>& GetVariablesNames()
  {
    return mVariablesNames;
  }

  /**
   * @brief This method retrieves the names of the variables in the table (constant version)
   */
  const std::array<std::string, 2>& GetVariablesNames() const
  {
    return mVariablesNames;
  }

  /**
   * @brief This method sets the variables names
   * @param rName The name of the table name
   */
  void SetTableName(const std::string& rName)
  {
    mName = rName;
  }

  /**
   * @brief This method retrieves the name of the table
   */
  std::string& GetTableName()
  {
    return mName;
  }

  /**
   * @brief This method retrieves the name of the table (constant version)
   */
  const std::string& GetTableName() const
  {
    return mName;
  }

  ///@}
  ///@name Inquiry
  ///@{


  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    return "Piecewise Linear TableInterpolation: " + mName;
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream& rOStream) const
  {
    rOStream << Info();
  }

  /// Print object's data.
  virtual void PrintData(std::ostream& rOStream) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  TableInterpolationContainerType mData;                                                               /// This stores the results

  std::string mName = "Table";                                                                         /// The name of the table

  std::array<std::string, 2> mVariablesNames = std::array<std::string, 2>({"Variable1", "Variable2"}); /// This stores the variables names if any

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Serialization
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}
}; // Class TableInterpolation

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream& operator >> (std::istream& rIStream,
                                  TableInterpolation& rThis);

/// output stream function
inline std::ostream& operator << (std::ostream& rOStream,
                                  const TableInterpolation& rThis)
{
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
}

///@}

}  // namespace felisce.

#endif // FELISCE_TABLE_HPP_INCLUDED  defined


