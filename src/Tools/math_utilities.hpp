//   ______ ______ _    _  _____    ______
//  |  ____|  ____| |  (_)/ ____|  |  ____|
//  | |__  | |__  | |   _| (___   ___| |__
//  |  __| |  __| | |  | |\___ \ / __|  __|
//  | |  | |____| |____| |____) | (__| |____
//  |_|  |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  Vicente Mataix Ferrandiz
//

#ifndef _MATH_UTILITIES_HPP
#define _MATH_UTILITIES_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce
{
class MathUtilities
{
public:
  /// The machine precision
  static constexpr double ZeroTolerance = std::numeric_limits<double>::epsilon();

  /**
   * @brief This method checks the condition number of  amtrix
   * @param rInputMatrix Is the input matrix (unchanged at output)
   * @param rInvertedMatrix Is the inverse of the input matrix
   * @param Tolerance The maximum tolerance considered
   */
  template<class TMatrix1, class TMatrix2>
  static inline bool CheckConditionNumber(
    const TMatrix1& rInputMatrix,
    TMatrix2& rInvertedMatrix,
    const double Tolerance = ZeroTolerance,
    const bool ThrowError = true
    )
  {
    // We want at least 4 significant digits
    const double max_condition_number = (1.0/Tolerance) * 1.0e-4;

    // Find the condition number to define is inverse is OK
    const double input_matrix_norm = norm_frobenius(rInputMatrix);
    const double inverted_matrix_norm = norm_frobenius(rInvertedMatrix);

    // Now the condition number is the product of both norms
    const double cond_number = input_matrix_norm * inverted_matrix_norm ;
    // Finally check if the condition number is low enough
    if (cond_number > max_condition_number) {
      if (ThrowError) {
        FELISCE_WATCH(rInputMatrix);
        FELISCE_WATCH(cond_number);
        FEL_ERROR("Condition number of the matrix is too high!");
      }
      return false;
    }

    return true;
  }

  /**
   * @brief This function is designed to be called when a dense linear system is needed to be solved
   * @param A System matrix
   * @param rX Solution vector. it's also the initial guess for iterative linear solvers.
   * @param rB Right hand side vector.
   */
  static inline void Solve(
    UBlasMatrix& rA,
    UBlasVector& rX,
    const UBlasVector& rB
    )
  {
    const std::size_t size1 = rA.size1();
    rX = rB;
    UBlasPermutationMatrix pm(size1);
    const int singular = lu_factorize(rA,pm);
    if(singular == 1) {
      FELISCE_WATCH(rA)
      FEL_ERROR("Matrix is singular");
    }
    lu_substitute(rA, pm, rX);
  }

  /**
   * @brief This function is designed to be called when a dense linear system is needed to be solved (copies to avoid changing A)
   * @param rA System matrix
   * @param rX Solution vector. it's also the initial guess for iterative linear solvers.
   * @param rB Right hand side vector.
   */
  static inline void SolveWithCopy(
    const UBlasMatrix& rA,
    UBlasVector& rX,
    const UBlasVector& rB
    )
  {
    UBlasMatrix copy_A(rA);
    Solve(copy_A, rX, rB);
  }

  /**
   * @brief It inverts matrices of order 2
   * @param rInputMatrix Is the input matrix (unchanged at output)
   * @param rInvertedMatrix Is the inverse of the input matrix
   * @param rInputMatrixDet Is the determinant of the input matrix
   */
  template<class TMatrix1, class TMatrix2>
  static void InvertMatrix2(
    const TMatrix1& rInputMatrix,
    TMatrix2& rInvertedMatrix,
    double& rInputMatrixDet
    )
  {
    if(rInvertedMatrix.size1() != 2 || rInvertedMatrix.size2() != 2) {
      rInvertedMatrix.resize(2,2,false);
    }

    rInputMatrixDet = rInputMatrix(0,0)*rInputMatrix(1,1)-rInputMatrix(0,1)*rInputMatrix(1,0);

    rInvertedMatrix(0,0) =  rInputMatrix(1,1);
    rInvertedMatrix(0,1) = -rInputMatrix(0,1);
    rInvertedMatrix(1,0) = -rInputMatrix(1,0);
    rInvertedMatrix(1,1) =  rInputMatrix(0,0);

    rInvertedMatrix/=rInputMatrixDet;
  }

  /**
   * @brief It inverts matrices of order 3
   * @param rInputMatrix Is the input matrix (unchanged at output)
   * @param rInvertedMatrix Is the inverse of the input matrix
   * @param rInputMatrixDet Is the determinant of the input matrix
   */
  template<class TMatrix1, class TMatrix2>
  static void InvertMatrix3(
    const TMatrix1& rInputMatrix,
    TMatrix2& rInvertedMatrix,
    double& rInputMatrixDet
    )
  {
    if(rInvertedMatrix.size1() != 3 || rInvertedMatrix.size2() != 3) {
      rInvertedMatrix.resize(3,3,false);
    }

    // Filling the inverted matrix with the algebraic complements
    // First column
    rInvertedMatrix(0,0) = rInputMatrix(1,1)*rInputMatrix(2,2) - rInputMatrix(1,2)*rInputMatrix(2,1);
    rInvertedMatrix(1,0) = -rInputMatrix(1,0)*rInputMatrix(2,2) + rInputMatrix(1,2)*rInputMatrix(2,0);
    rInvertedMatrix(2,0) = rInputMatrix(1,0)*rInputMatrix(2,1) - rInputMatrix(1,1)*rInputMatrix(2,0);

    // Second column
    rInvertedMatrix(0,1) = -rInputMatrix(0,1)*rInputMatrix(2,2) + rInputMatrix(0,2)*rInputMatrix(2,1);
    rInvertedMatrix(1,1) = rInputMatrix(0,0)*rInputMatrix(2,2) - rInputMatrix(0,2)*rInputMatrix(2,0);
    rInvertedMatrix(2,1) = -rInputMatrix(0,0)*rInputMatrix(2,1) + rInputMatrix(0,1)*rInputMatrix(2,0);

    // Third column
    rInvertedMatrix(0,2) = rInputMatrix(0,1)*rInputMatrix(1,2) - rInputMatrix(0,2)*rInputMatrix(1,1);
    rInvertedMatrix(1,2) = -rInputMatrix(0,0)*rInputMatrix(1,2) + rInputMatrix(0,2)*rInputMatrix(1,0);
    rInvertedMatrix(2,2) = rInputMatrix(0,0)*rInputMatrix(1,1) - rInputMatrix(0,1)*rInputMatrix(1,0);

    // Calculation of determinant (of the input matrix)
    rInputMatrixDet = rInputMatrix(0,0)*rInvertedMatrix(0,0) + rInputMatrix(0,1)*rInvertedMatrix(1,0) + rInputMatrix(0,2)*rInvertedMatrix(2,0);

    // Finalizing the calculation of the inverted matrix
    rInvertedMatrix /= rInputMatrixDet;
  }

  /**
   * @brief It inverts matrices of order 4
   * @param rInputMatrix Is the input matrix (unchanged at output)
   * @param rInvertedMatrix Is the inverse of the input matrix
   * @param rInputMatrixDet Is the determinant of the input matrix
   */
  template<class TMatrix1, class TMatrix2>
  static void InvertMatrix4(
    const TMatrix1& rInputMatrix,
    TMatrix2& rInvertedMatrix,
    double& rInputMatrixDet
    )
  {
    if (rInvertedMatrix.size1() != 4 || rInvertedMatrix.size2() != 4) {
      rInvertedMatrix.resize(4, 4, false);
    }

    /* Compute inverse of the Matrix */
    // First column
    rInvertedMatrix(0, 0) = -(rInputMatrix(1, 3) * rInputMatrix(2, 2) * rInputMatrix(3, 1)) + rInputMatrix(1, 2) * rInputMatrix(2, 3) * rInputMatrix(3, 1) + rInputMatrix(1, 3) * rInputMatrix(2, 1) * rInputMatrix(3, 2) - rInputMatrix(1, 1) * rInputMatrix(2, 3) * rInputMatrix(3, 2) - rInputMatrix(1, 2) * rInputMatrix(2, 1) * rInputMatrix(3, 3) + rInputMatrix(1, 1) * rInputMatrix(2, 2) * rInputMatrix(3, 3);
    rInvertedMatrix(0, 1) = rInputMatrix(0, 3) * rInputMatrix(2, 2) * rInputMatrix(3, 1) - rInputMatrix(0, 2) * rInputMatrix(2, 3) * rInputMatrix(3, 1) - rInputMatrix(0, 3) * rInputMatrix(2, 1) * rInputMatrix(3, 2) + rInputMatrix(0, 1) * rInputMatrix(2, 3) * rInputMatrix(3, 2) + rInputMatrix(0, 2) * rInputMatrix(2, 1) * rInputMatrix(3, 3) - rInputMatrix(0, 1) * rInputMatrix(2, 2) * rInputMatrix(3, 3);
    rInvertedMatrix(0, 2) = -(rInputMatrix(0, 3) * rInputMatrix(1, 2) * rInputMatrix(3, 1)) + rInputMatrix(0, 2) * rInputMatrix(1, 3) * rInputMatrix(3, 1) + rInputMatrix(0, 3) * rInputMatrix(1, 1) * rInputMatrix(3, 2) - rInputMatrix(0, 1) * rInputMatrix(1, 3) * rInputMatrix(3, 2) - rInputMatrix(0, 2) * rInputMatrix(1, 1) * rInputMatrix(3, 3) + rInputMatrix(0, 1) * rInputMatrix(1, 2) * rInputMatrix(3, 3);
    rInvertedMatrix(0, 3) = rInputMatrix(0, 3) * rInputMatrix(1, 2) * rInputMatrix(2, 1) - rInputMatrix(0, 2) * rInputMatrix(1, 3) * rInputMatrix(2, 1) - rInputMatrix(0, 3) * rInputMatrix(1, 1) * rInputMatrix(2, 2) + rInputMatrix(0, 1) * rInputMatrix(1, 3) * rInputMatrix(2, 2) + rInputMatrix(0, 2) * rInputMatrix(1, 1) * rInputMatrix(2, 3) - rInputMatrix(0, 1) * rInputMatrix(1, 2) * rInputMatrix(2, 3);

    // Second column
    rInvertedMatrix(1, 0) = rInputMatrix(1, 3) * rInputMatrix(2, 2) * rInputMatrix(3, 0) - rInputMatrix(1, 2) * rInputMatrix(2, 3) * rInputMatrix(3, 0) - rInputMatrix(1, 3) * rInputMatrix(2, 0) * rInputMatrix(3, 2) + rInputMatrix(1, 0) * rInputMatrix(2, 3) * rInputMatrix(3, 2) + rInputMatrix(1, 2) * rInputMatrix(2, 0) * rInputMatrix(3, 3) - rInputMatrix(1, 0) * rInputMatrix(2, 2) * rInputMatrix(3, 3);
    rInvertedMatrix(1, 1) = -(rInputMatrix(0, 3) * rInputMatrix(2, 2) * rInputMatrix(3, 0)) + rInputMatrix(0, 2) * rInputMatrix(2, 3) * rInputMatrix(3, 0) + rInputMatrix(0, 3) * rInputMatrix(2, 0) * rInputMatrix(3, 2) - rInputMatrix(0, 0) * rInputMatrix(2, 3) * rInputMatrix(3, 2) - rInputMatrix(0, 2) * rInputMatrix(2, 0) * rInputMatrix(3, 3) + rInputMatrix(0, 0) * rInputMatrix(2, 2) * rInputMatrix(3, 3);
    rInvertedMatrix(1, 2) = rInputMatrix(0, 3) * rInputMatrix(1, 2) * rInputMatrix(3, 0) - rInputMatrix(0, 2) * rInputMatrix(1, 3) * rInputMatrix(3, 0) - rInputMatrix(0, 3) * rInputMatrix(1, 0) * rInputMatrix(3, 2) + rInputMatrix(0, 0) * rInputMatrix(1, 3) * rInputMatrix(3, 2) + rInputMatrix(0, 2) * rInputMatrix(1, 0) * rInputMatrix(3, 3) - rInputMatrix(0, 0) * rInputMatrix(1, 2) * rInputMatrix(3, 3);
    rInvertedMatrix(1, 3) = -(rInputMatrix(0, 3) * rInputMatrix(1, 2) * rInputMatrix(2, 0)) + rInputMatrix(0, 2) * rInputMatrix(1, 3) * rInputMatrix(2, 0) + rInputMatrix(0, 3) * rInputMatrix(1, 0) * rInputMatrix(2, 2) - rInputMatrix(0, 0) * rInputMatrix(1, 3) * rInputMatrix(2, 2) - rInputMatrix(0, 2) * rInputMatrix(1, 0) * rInputMatrix(2, 3) + rInputMatrix(0, 0) * rInputMatrix(1, 2) * rInputMatrix(2, 3);

    // Third column
    rInvertedMatrix(2, 0) = -(rInputMatrix(1, 3) * rInputMatrix(2, 1) * rInputMatrix(3, 0)) + rInputMatrix(1, 1) * rInputMatrix(2, 3) * rInputMatrix(3, 0) + rInputMatrix(1, 3) * rInputMatrix(2, 0) * rInputMatrix(3, 1) - rInputMatrix(1, 0) * rInputMatrix(2, 3) * rInputMatrix(3, 1) - rInputMatrix(1, 1) * rInputMatrix(2, 0) * rInputMatrix(3, 3) + rInputMatrix(1, 0) * rInputMatrix(2, 1) * rInputMatrix(3, 3);
    rInvertedMatrix(2, 1) = rInputMatrix(0, 3) * rInputMatrix(2, 1) * rInputMatrix(3, 0) - rInputMatrix(0, 1) * rInputMatrix(2, 3) * rInputMatrix(3, 0) - rInputMatrix(0, 3) * rInputMatrix(2, 0) * rInputMatrix(3, 1) + rInputMatrix(0, 0) * rInputMatrix(2, 3) * rInputMatrix(3, 1) + rInputMatrix(0, 1) * rInputMatrix(2, 0) * rInputMatrix(3, 3) - rInputMatrix(0, 0) * rInputMatrix(2, 1) * rInputMatrix(3, 3);
    rInvertedMatrix(2, 2) = -(rInputMatrix(0, 3) * rInputMatrix(1, 1) * rInputMatrix(3, 0)) + rInputMatrix(0, 1) * rInputMatrix(1, 3) * rInputMatrix(3, 0) + rInputMatrix(0, 3) * rInputMatrix(1, 0) * rInputMatrix(3, 1) - rInputMatrix(0, 0) * rInputMatrix(1, 3) * rInputMatrix(3, 1) - rInputMatrix(0, 1) * rInputMatrix(1, 0) * rInputMatrix(3, 3) + rInputMatrix(0, 0) * rInputMatrix(1, 1) * rInputMatrix(3, 3);
    rInvertedMatrix(2, 3) = rInputMatrix(0, 3) * rInputMatrix(1, 1) * rInputMatrix(2, 0) - rInputMatrix(0, 1) * rInputMatrix(1, 3) * rInputMatrix(2, 0) - rInputMatrix(0, 3) * rInputMatrix(1, 0) * rInputMatrix(2, 1) + rInputMatrix(0, 0) * rInputMatrix(1, 3) * rInputMatrix(2, 1) + rInputMatrix(0, 1) * rInputMatrix(1, 0) * rInputMatrix(2, 3) - rInputMatrix(0, 0) * rInputMatrix(1, 1) * rInputMatrix(2, 3);

    // Fourth column
    rInvertedMatrix(3, 0) = rInputMatrix(1, 2) * rInputMatrix(2, 1) * rInputMatrix(3, 0) - rInputMatrix(1, 1) * rInputMatrix(2, 2) * rInputMatrix(3, 0) - rInputMatrix(1, 2) * rInputMatrix(2, 0) * rInputMatrix(3, 1) + rInputMatrix(1, 0) * rInputMatrix(2, 2) * rInputMatrix(3, 1) + rInputMatrix(1, 1) * rInputMatrix(2, 0) * rInputMatrix(3, 2) - rInputMatrix(1, 0) * rInputMatrix(2, 1) * rInputMatrix(3, 2);
    rInvertedMatrix(3, 1) = -(rInputMatrix(0, 2) * rInputMatrix(2, 1) * rInputMatrix(3, 0)) + rInputMatrix(0, 1) * rInputMatrix(2, 2) * rInputMatrix(3, 0) + rInputMatrix(0, 2) * rInputMatrix(2, 0) * rInputMatrix(3, 1) - rInputMatrix(0, 0) * rInputMatrix(2, 2) * rInputMatrix(3, 1) - rInputMatrix(0, 1) * rInputMatrix(2, 0) * rInputMatrix(3, 2) + rInputMatrix(0, 0) * rInputMatrix(2, 1) * rInputMatrix(3, 2);
    rInvertedMatrix(3, 2) = rInputMatrix(0, 2) * rInputMatrix(1, 1) * rInputMatrix(3, 0) - rInputMatrix(0, 1) * rInputMatrix(1, 2) * rInputMatrix(3, 0) - rInputMatrix(0, 2) * rInputMatrix(1, 0) * rInputMatrix(3, 1) + rInputMatrix(0, 0) * rInputMatrix(1, 2) * rInputMatrix(3, 1) + rInputMatrix(0, 1) * rInputMatrix(1, 0) * rInputMatrix(3, 2) - rInputMatrix(0, 0) * rInputMatrix(1, 1) * rInputMatrix(3, 2);
    rInvertedMatrix(3, 3) = -(rInputMatrix(0, 2) * rInputMatrix(1, 1) * rInputMatrix(2, 0)) + rInputMatrix(0, 1) * rInputMatrix(1, 2) * rInputMatrix(2, 0) + rInputMatrix(0, 2) * rInputMatrix(1, 0) * rInputMatrix(2, 1) - rInputMatrix(0, 0) * rInputMatrix(1, 2) * rInputMatrix(2, 1) - rInputMatrix(0, 1) * rInputMatrix(1, 0) * rInputMatrix(2, 2) + rInputMatrix(0, 0) * rInputMatrix(1, 1) * rInputMatrix(2, 2);

    // Calculation of determinant (of the input matrix)
    rInputMatrixDet = rInputMatrix(0, 1) * rInputMatrix(1, 3) * rInputMatrix(2, 2) * rInputMatrix(3, 0) - rInputMatrix(0, 1) * rInputMatrix(1, 2) * rInputMatrix(2, 3) * rInputMatrix(3, 0) - rInputMatrix(0, 0) * rInputMatrix(1, 3) * rInputMatrix(2, 2) * rInputMatrix(3, 1) + rInputMatrix(0, 0) * rInputMatrix(1, 2) * rInputMatrix(2, 3) * rInputMatrix(3, 1) - rInputMatrix(0, 1) * rInputMatrix(1, 3) * rInputMatrix(2, 0) * rInputMatrix(3, 2) + rInputMatrix(0, 0) * rInputMatrix(1, 3) * rInputMatrix(2, 1) * rInputMatrix(3, 2) + rInputMatrix(0, 1) * rInputMatrix(1, 0) * rInputMatrix(2, 3) * rInputMatrix(3, 2) - rInputMatrix(0, 0) * rInputMatrix(1, 1) * rInputMatrix(2, 3) * rInputMatrix(3, 2) + rInputMatrix(0, 3) * (rInputMatrix(1, 2) * rInputMatrix(2, 1) * rInputMatrix(3, 0) - rInputMatrix(1, 1) * rInputMatrix(2, 2) * rInputMatrix(3, 0) - rInputMatrix(1, 2) * rInputMatrix(2, 0) * rInputMatrix(3, 1) + rInputMatrix(1, 0) * rInputMatrix(2, 2) * rInputMatrix(3, 1) + rInputMatrix(1, 1) * rInputMatrix(2, 0) * rInputMatrix(3, 2) - rInputMatrix(1, 0) * rInputMatrix(2, 1) * rInputMatrix(3, 2)) + (rInputMatrix(0, 1) * rInputMatrix(1, 2) * rInputMatrix(2, 0) - rInputMatrix(0, 0) * rInputMatrix(1, 2) * rInputMatrix(2, 1) - rInputMatrix(0, 1) * rInputMatrix(1, 0) * rInputMatrix(2, 2) + rInputMatrix(0, 0) * rInputMatrix(1, 1) * rInputMatrix(2, 2)) * rInputMatrix(3, 3) + rInputMatrix(0, 2) * (-(rInputMatrix(1, 3) * rInputMatrix(2, 1) * rInputMatrix(3, 0)) + rInputMatrix(1, 1) * rInputMatrix(2, 3) * rInputMatrix(3, 0) + rInputMatrix(1, 3) * rInputMatrix(2, 0) * rInputMatrix(3, 1) - rInputMatrix(1, 0) * rInputMatrix(2, 3) * rInputMatrix(3, 1) - rInputMatrix(1, 1) * rInputMatrix(2, 0) * rInputMatrix(3, 3) + rInputMatrix(1, 0) * rInputMatrix(2, 1) * rInputMatrix(3, 3));

    // Finalizing the calculation of the inverted matrix
    rInvertedMatrix /= rInputMatrixDet;
  }

  /**
   * @brief It inverts matrices of order 2, 3 and 4
   * @param rInputMatrix Is the input matrix (unchanged at output)
   * @param rInvertedMatrix Is the inverse of the input matrix
   * @param rInputMatrixDet Is the determinant of the input matrix
   */
  template<class TMatrix1, class TMatrix2>
  static void InvertMatrix(
    const TMatrix1& rInputMatrix,
    TMatrix2& rInvertedMatrix,
    double& rInputMatrixDet,
    const double Tolerance = ZeroTolerance
    )
  {
    (void) Tolerance;
    const std::size_t size = rInputMatrix.size2();

    if(size == 1) {
      if(rInvertedMatrix.size1() != 1 || rInvertedMatrix.size2() != 1) {
        rInvertedMatrix.resize(1,1,false);
      }
      rInvertedMatrix(0,0) = 1.0/rInputMatrix(0,0);
      rInputMatrixDet = rInputMatrix(0,0);
    } else if (size == 2) {
      InvertMatrix2(rInputMatrix, rInvertedMatrix, rInputMatrixDet);
    } else if (size == 3) {
      InvertMatrix3(rInputMatrix, rInvertedMatrix, rInputMatrixDet);
    } else if (size == 4) {
      InvertMatrix4(rInputMatrix, rInvertedMatrix, rInputMatrixDet);
    } else if (std::is_same<TMatrix1, UBlasMatrix>::value) {

      const std::size_t size1 = rInputMatrix.size1();
      const std::size_t size2 = rInputMatrix.size2();
      if(rInvertedMatrix.size1() != size1 || rInvertedMatrix.size2() != size2) {
        rInvertedMatrix.resize(size1, size2,false);
      }

      UBlasMatrix A(rInputMatrix);

      UBlasPermutationMatrix pm(A.size1());
      const int singular = lu_factorize(A,pm);
      rInvertedMatrix.assign( UBlasIdentityMatrix(A.size1()));
      if(singular == 1) {
        std::cout << "Matrix is singular: " << rInputMatrix << std::endl;
        exit(1);
      }
      lu_substitute(A, pm, rInvertedMatrix);

      // Calculating determinant
      rInputMatrixDet = 1.0;

      for (std::size_t i = 0; i < size1; ++i) {
        std::size_t ki = pm[i] == i ? 0 : 1;
        rInputMatrixDet *= (ki == 0) ? A(i,i) : -A(i,i);
      }
     } else { // Bounded-matrix case
      const std::size_t size1 = rInputMatrix.size1();
      const std::size_t size2 = rInputMatrix.size2();

      UBlasMatrix A(rInputMatrix);
      UBlasMatrix invA(rInvertedMatrix);

      UBlasPermutationMatrix pm(size1);
      const int singular = lu_factorize(A,pm);
      invA.assign( UBlasIdentityMatrix(size1));
      if(singular == 1) {
        std::cout << "Matrix is singular: " << rInputMatrix << std::endl;
        exit(1);
      }
      lu_substitute(A, pm, invA);

      // Calculating determinant
      rInputMatrixDet = 1.0;

      for (std::size_t i = 0; i < size1;++i) {
        std::size_t ki = pm[i] == i ? 0 : 1;
        rInputMatrixDet *= (ki == 0) ? A(i,i) : -A(i,i);
      }

      for (std::size_t i = 0; i < size1;++i) {
        for (std::size_t j = 0; j < size2;++j) {
          rInvertedMatrix(i,j) = invA(i,j);
        }
      }
    }

    // Checking condition number
    // if (Tolerance > 0.0) { // Check is skipped for negative tolerances
    //   CheckConditionNumber(rInputMatrix, rInvertedMatrix, Tolerance);
    // }
  }

  /**
   * @brief It inverts non square matrices (https://en.wikipedia.org/wiki/Inverse_element#Matrices)
   * @param rInputMatrix Is the input matrix (unchanged at output)
   * @param rInvertedMatrix Is the inverse of the input matrix
   * @param rInputMatrixDet Is the determinant of the input matrix
   */
  template<class TMatrixType>
  static void GeneralizedInvertMatrix(
    const TMatrixType& rInputMatrix,
    TMatrixType& rInvertedMatrix,
    double& rInputMatrixDet
    )
  {
    const std::size_t size_1 = rInputMatrix.size1();
    const std::size_t size_2 = rInputMatrix.size2();

    if (size_1 == size_2) {
      InvertMatrix(rInputMatrix, rInvertedMatrix, rInputMatrixDet);
    } else if (size_1 < size_2) { // Right inverse
      if (rInvertedMatrix.size1() != size_2 || rInvertedMatrix.size2() != size_1) {
        rInvertedMatrix.resize(size_2, size_1, false);
      }
      const TMatrixType aux = prod(rInputMatrix, trans(rInputMatrix));
      TMatrixType auxInv;
      InvertMatrix(aux, auxInv, rInputMatrixDet);
      rInputMatrixDet = std::sqrt(rInputMatrixDet);
      noalias(rInvertedMatrix) = prod(trans(rInputMatrix), auxInv);
    } else { // Left inverse
      if (rInvertedMatrix.size1() != size_2 || rInvertedMatrix.size2() != size_1) {
        rInvertedMatrix.resize(size_2, size_1, false);
      }
      const TMatrixType aux = prod(trans(rInputMatrix), rInputMatrix);
      TMatrixType auxInv;
      InvertMatrix(aux, auxInv, rInputMatrixDet);
      rInputMatrixDet = std::sqrt(rInputMatrixDet);
      noalias(rInvertedMatrix) = prod(auxInv, trans(rInputMatrix));
    }
  }

  /**
   * @brief Calculates the determinant of a matrix of dimension 2x2 (no check performed on matrix size)
   * @param rA Is the input matrix
   * @return The determinant of the 2x2 matrix
   */
  template<class TMatrixType>
  static inline double Det2(const TMatrixType& rA)
  {
    return (rA(0,0)*rA(1,1)-rA(0,1)*rA(1,0));
  }

  /**
   * @brief Calculates the determinant of a matrix of dimension 3*3 (no check performed on matrix size)
   * @param rA Is the input matrix
   * @return The determinant of the 3x3 matrix
   */
  template<class TMatrixType>
  static inline double Det3(const TMatrixType& rA)
  {
    // Calculating the algebraic complements to the first line
    const double a = rA(1,1)*rA(2,2) - rA(1,2)*rA(2,1);
    const double b = rA(1,0)*rA(2,2) - rA(1,2)*rA(2,0);
    const double c = rA(1,0)*rA(2,1) - rA(1,1)*rA(2,0);

    return rA(0,0)*a - rA(0,1)*b + rA(0,2)*c;
  }

  /**
   * @brief Calculates the determinant of a matrix of dimension 4*4 (no check performed on matrix size)
   * @param rA Is the input matrix
   * @return The determinant of the 4x4 matrix
   */
  template<class TMatrixType>
  static inline double Det4(const TMatrixType& rA)
  {
    const double Det = rA(0,1)*rA(1,3)*rA(2,2)*rA(3,0)-rA(0,1)*rA(1,2)*rA(2,3)*rA(3,0)-rA(0,0)*rA(1,3)*rA(2,2)*rA(3,1)+rA(0,0)*rA(1,2)*rA(2,3)*rA(3,1)
              -rA(0,1)*rA(1,3)*rA(2,0)*rA(3,2)+rA(0,0)*rA(1,3)*rA(2,1)*rA(3,2)+rA(0,1)*rA(1,0)*rA(2,3)*rA(3,2)-rA(0,0)*rA(1,1)*rA(2,3)*rA(3,2)+rA(0,3)*(rA(1,2)*rA(2,1)*rA(3,0)-rA(1,1)*rA(2,2)*rA(3,0)-rA(1,2)*rA(2,0)*rA(3,1)+rA(1,0)*rA(2,2)*rA(3,1)+rA(1,1)*rA(2,0)*rA(3,2)
              -rA(1,0)*rA(2,1)*rA(3,2))+(rA(0,1)*rA(1,2)*rA(2,0)-rA(0,0)*rA(1,2)*rA(2,1)-rA(0,1)*rA(1,0)*rA(2,2)+rA(0,0)*rA(1,1)*rA(2,2))*rA(3,3)+rA(0,2)*(-(rA(1,3)*rA(2,1)*rA(3,0))+rA(1,1)*rA(2,3)*rA(3,0)+rA(1,3)*rA(2,0)*rA(3,1)-rA(1,0)*rA(2,3)*rA(3,1)-rA(1,1)*rA(2,0)*rA(3,3)+rA(1,0)*rA(2,1)*rA(3,3));
    return Det;
  }

  /**
   * @brief Calculates the determinant of a matrix of dimension 2x2 or 3x3 (no check performed on matrix size)
   * @param rA Is the input matrix
   * @return The determinant of the 2x2 matrix
   */
  template<class TMatrixType>
  static inline double Det(const TMatrixType& rA)
  {
    switch ( rA.size1() )
    {
      case 2:
        return Det2(rA);
        break;
      case 3:
        return Det3(rA);
        break;
      case 4:
        return Det4(rA);
      default:
        UBlasMatrix Aux(rA);
        UBlasPermutationMatrix pm(Aux.size1());
        bool singular = lu_factorize(Aux,pm);

        if (singular) {
          return 0.0;
        }

        double det = 1.0;

        for (std::size_t i = 0; i < Aux.size1();++i) {
          std::size_t ki = pm[i] == i ? 0 : 1;
          det *= std::pow(-1.0, ki) * Aux(i,i);
        }

        return det;
     }
  }

  /**
   * @brief Calculates the determinant of a matrix of dimension 2x2 or 3x3 (no check performed on matrix size)
   * @param rA Is the input matrix
   * @return The determinant of the 2x2 matrix
   */
  template<class TMatrixType>
  static inline double GeneralizedDet(const TMatrixType& rA)
  {
    double determinant;

    if (rA.size1() == rA.size2()) {
      determinant = Det(rA);
    } else if (rA.size1() < rA.size2()) { // Right determinant
      const UBlasMatrix AAT = prod( rA, trans(rA) );
      determinant = std::sqrt(Det(AAT));
    } else { // Left determinant
      const UBlasMatrix ATA = prod( trans(rA), rA );
      determinant = std::sqrt(Det(ATA));
    }

    return determinant;
  }

    /**
    * @brief Calculates the product operation B'DB
    * @param rA The resulting matrix
    * @param rD The "center" matrix
    * @param rB The matrices to be transposed
    * @tparam TMatrixType1 The type of matrix considered (1)
    * @tparam TMatrixType2 The type of matrix considered (2)
    * @tparam TMatrixType3 The type of matrix considered (3)
    */
  template<class TMatrixType1, class TMatrixType2, class TMatrixType3>
  static inline void BtDBProductOperation(
      TMatrixType1& rA,
      const TMatrixType2& rD,
      const TMatrixType3& rB
      )
  {
    // The sizes
    const std::size_t size1 = rB.size2();
    const std::size_t size2 = rB.size2();

    if (rA.size1() != size1 || rA.size2() != size2)
        rA.resize(size1, size2, false);

    // Direct multiplication
    // noalias(rA) = prod( trans( rB ), UBlasMatrix(prod(rD, rB)));

    // Manual multiplication
    rA.clear();
    for(std::size_t k = 0; k< rD.size1(); ++k) {
      for(std::size_t l = 0; l < rD.size2(); ++l) {
        const double Dkl = rD(k, l);
        for(std::size_t j = 0; j < rB.size2(); ++j) {
          const double DklBlj = Dkl * rB(l, j);
          for(std::size_t i = 0; i< rB.size2(); ++i) {
            rA(i, j) += rB(k, i) * DklBlj;
          }
        }
      }
    }
  }

  /**
    * @brief Calculates the product operation BDB'
    * @param rA The resulting matrix
    * @param rD The "center" matrix
    * @param rB The matrices to be transposed
    * @tparam TMatrixType1 The type of matrix considered (1)
    * @tparam TMatrixType2 The type of matrix considered (2)
    * @tparam TMatrixType3 The type of matrix considered (3)
    */
  template<class TMatrixType1, class TMatrixType2, class TMatrixType3>
  static inline void BDBtProductOperation(
      TMatrixType1& rA,
      const TMatrixType2& rD,
      const TMatrixType3& rB
      )
  {
    // The sizes
    const std::size_t size1 = rB.size1();
    const std::size_t size2 = rB.size1();

    if (rA.size1() != size1 || rA.size2() != size2)
      rA.resize(size1, size2, false);

    // Direct multiplication
    // noalias(rA) = prod(rB, UBlasMatrix(prod(rD, trans(rB))));

    // Manual multiplication
    rA.clear();
    for(std::size_t k = 0; k< rD.size1(); ++k) {
      for(std::size_t l = 0; l < rD.size2(); ++l) {
        const double Dkl = rD(k,l);
        for(std::size_t j = 0; j < rB.size1(); ++j) {
          const double DklBjl = Dkl * rB(j,l);
          for(std::size_t i = 0; i< rB.size1(); ++i) {
            rA(i, j) += rB(i, k) * DklBjl;
          }
        }
      }
    }
  }

  /**
   * @brief Heaviside function
   * @param x Input value
   * @return The resulting value
   */
  template<typename T>
  static inline T Heaviside(const T x)
  {
    return (x < 0) ? 0 : 1;
  }

  /**
   * @brief Positive part function
   * @param x Input value
   * @return The resulting value
   */
  template<typename T>
  static inline T Positive(const T x)
  {
    return (x < 0) ? 0 : x;
  }

  /**
   * @brief Performs the vector product of the two input vectors a,b
   * @details a,b are assumed to be of size 3 (no check is performed on vector sizes)
   * @param a First input vector
   * @param b Second input vector
   * @return The resulting vector
   */
  template<class T>
  static inline T CrossProduct(
      const T& a,
      const T& b
      )
  {
    T c(a);

    c[0] = a[1]*b[2] - a[2]*b[1];
    c[1] = a[2]*b[0] - a[0]*b[2];
    c[2] = a[0]*b[1] - a[1]*b[0];

    return c;
  }

  /**
   * @brief Performs the cross product of the two input vectors a,b
   * @details a,b are assumed to be of size 3 (check is only performed on vector sizes in debug mode)
   * @param a First input vector
   * @param b Second input vector
   * @param c The resulting vector
   */
  template< class T1, class T2 , class T3>
  static inline void CrossProduct(T1& c, const T2& a, const T3& b ){
    // if (c.size() != 3) c.resize(3);

    FEL_ASSERT(a.size() == 3 && b.size() == 3 && c.size() == 3);
    // if (a.size() != 3 || b.size() != 3 || c.size() != 3)
    //   FEL_ERROR("The size of the vectors is different of 3");

    c[0] = a[1]*b[2] - a[2]*b[1];
    c[1] = a[2]*b[0] - a[0]*b[2];
    c[2] = a[0]*b[1] - a[1]*b[0];
  }

  /**
   * @brief Performs the unitary cross product of the two input vectors a,b
   * @details a,b are assumed to be of size 3 (no check is performed on vector sizes)
   * @param a First input vector
   * @param b Second input vector
   * @param c The resulting vector
   */
  template< class T1, class T2 , class T3>
  static inline void UnitCrossProduct(T1& c, const T2& a, const T3& b ){
    CrossProduct(c,a,b);
    const double norm = norm_2(c);
    FEL_ASSERT(norm > 1000.0*ZeroTolerance);
    c/=norm;
  }

  /**
   * @brief This computes a orthonormal basis from a given vector (Frisvad method)
   * @param c The input vector
   * @param a First resulting vector
   * @param b Second resulting vector
   * @param Type The type of method employed, 0 is HughesMoeller, 1 is Frisvad and otherwise Naive
   */
  template< class T1, class T2 , class T3>
  static inline void OrthonormalBasis(const T1& c,T2& a,T3& b, const std::size_t Type = 0 ){
    if (Type == 0) {
      OrthonormalBasisHughesMoeller(c,a,b);
    } else if (Type == 1) {
      OrthonormalBasisFrisvad(c,a,b);
    } else {
      OrthonormalBasisNaive(c,a,b);
    }
  }

  /**
   * @brief This computes a orthonormal basis from a given vector (Hughes Moeller method)
   * @param c The input vector
   * @param a First resulting vector
   * @param b Second resulting vector
   * @note Orthonormal basis taken from: http://orbit.dtu.dk/files/126824972/onb_frisvad_jgt2012_v2.pdf
   */
  template< class T1, class T2 , class T3>
  static inline void OrthonormalBasisHughesMoeller(const T1& c,T2& a,T3& b){
    FEL_ASSERT(!(norm_2(c) < (1.0 - 1.0e-6) || norm_2(c) > (1.0 + 1.0e-6)));
    //  Choose a vector  orthogonal  to n as the  direction  of b2.
    if(std::abs(c[0]) > std::abs(c[2])) {
      b[0] =  c[1];
      b[1] = -c[0];
      b[2] =  0.0;
    } else {
      b[0] =   0.0;
      b[1] =   c[2];
      b[2]  = -c[1];
    }
    b /=  norm_2(b); //  Normalize  b
    UnitCrossProduct(a, b , c); //  Construct  a  using a cross  product
  }

  /**
   * @brief This computes a orthonormal basis from a given vector (Frisvad method)
   * @param c The input vector
   * @param a First resulting vector
   * @param b Second resulting vector
   * @note Orthonormal basis taken from: http://orbit.dtu.dk/files/126824972/onb_frisvad_jgt2012_v2.pdf
   */
  template< class T1, class T2 , class T3>
  static inline void OrthonormalBasisFrisvad(const T1& c,T2& a,T3& b){
    FEL_ASSERT(!(norm_2(c) < (1.0 - 1.0e-3) || norm_2(c) > (1.0 + 1.0e-3)));
    if ((c[2] + 1.0) > 1.0e4 * ZeroTolerance) {
      a[0] = 1.0 - std::pow(c[0], 2)/(1.0 + c[2]);
      a[1] = - (c[0] * c[1])/(1.0 + c[2]);
      a[2] = - c[0];
      const double norm_a = norm_2(a);
      a /= norm_a;
      b[0] = - (c[0] * c[1])/(1.0 + c[2]);
      b[1] = 1.0 - std::pow(c[1], 2)/(1.0 + c[2]);
      b[2] = -c[1];
      const double norm_b = norm_2(b);
      b /= norm_b;
    } else { // In case that the vector is in negative Z direction
      a[0] = 1.0;
      a[1] = 0.0;
      a[2] = 0.0;
      b[0] = 0.0;
      b[1] = -1.0;
      b[2] = 0.0;
    }
  }

  /**
   * @brief This computes a orthonormal basis from a given vector (Naive method)
   * @param c The input vector
   * @param a First resulting vector
   * @param b Second resulting vector
   * @note Orthonormal basis taken from: http://orbit.dtu.dk/files/126824972/onb_frisvad_jgt2012_v2.pdf
   */
  template< class T1, class T2 , class T3>
  static inline void OrthonormalBasisNaive(const T1& c,T2& a,T3& b){
    FEL_ASSERT(!(norm_2(c) < (1.0 - 1.0e-3) || norm_2(c) > (1.0 + 1.0e-3)));
    // If c is near  the x-axis , use  the y-axis. Otherwise  use  the x-axis.
    if(c[0] > 0.9f) {
      a[0] = 0.0;
      a[1] = 1.0;
      a[2] = 0.0;
    } else {
      a[0] = 1.0;
      a[1] = 0.0;
      a[2] = 0.0;
    }
    a  -= c * inner_prod(a, c); // Make a  orthogonal  to c
    a /=  norm_2(a);            //  Normalize  a
    UnitCrossProduct(b, c, a);  //  Construct  b  using a cross  product
  }

  /**
   * @brief Computes the Euler angles relative to the given axis of coordinates
   * @param a First vector coordinates
   * @param b Second vector coordinates
   * @param c Third vector coordinates
   * @param pre precession rotation
   * @param nut nutation rotation
   * @param rot intrinsic rotation
   * @tparam T1 The type of first vector coordinates
   * @tparam T2 The type of second vector coordinates
   * @tparam T3 The type of third vector coordinates
   */
  template< class T1, class T2 , class T3>
  static inline void LCS2Euler(const T1& a, const T2& b, const T3& c, double& pre, double& nut, double& rot) 
  {
      const double Z1xy = std::sqrt(std::pow(c[0], 2) + std::pow(c[1], 2));
      if (Z1xy > std::numeric_limits<double>::epsilon()) {
          pre = std::atan2(b[0] * c[1] - b[1] * c[0], a[0] * c[1] - a[1] * c[0]);
          nut = std::atan2(Z1xy, c[2]);
          rot = -std::atan2(-c[0], c[1]);
      } else {
          pre = 0.0;
          nut = (c[2] > 0.0) ? 0.0 : M_PI;
          rot = -std::atan2(a[1], a[0]);
      }
  }

  /**
   * @brief Computes the rotation matrix
   * @param rR The rotation matrix
   * @param pre precession rotation
   * @param nut nutation rotation
   * @param rot intrinsic rotation
   * @tparam T1 The type of rotation matrix
   */
  template< class T1>
  static inline void Euler2RotationMatrix(T1& rR, const double pre, const double nut, const double rot) 
  {
     rR(0,0) = std::cos(rot) * std::cos(nut);
     rR(0,1) = (std::cos(rot) * std::sin(nut) * std::sin(pre) - std::sin(rot) * std::cos(pre));
     rR(0,2) = (std::cos(rot) * std::sin(nut) * std::cos(pre) + std::sin(rot) * std::sin(pre));
     rR(1,0) = std::sin(rot) * std::cos(nut);
     rR(1,1) = (std::sin(rot) * std::sin(nut) * std::sin(pre) + std::cos(rot) * std::cos(pre));
     rR(1,2) = (std::sin(rot) * std::sin(nut) * std::cos(pre) - std::cos(rot) * std::sin(pre));
     rR(2,0) = -std::sin(nut);
     rR(2,1) = std::cos(nut) * std::sin(pre);
     rR(2,2) = std::cos(nut) * std::cos(pre);
  }

  /**
   * @brief This matrix extends the local matrix to all the dofs matrix
   * @param rLocalMatrix The local matrix
   * @param rAllDofsMatrix The global matrix
   * @tparam T1 The type of local matrix
   * @tparam T2 The type of all dofs matrix
   */
  template<class T1, class T2>
  static inline void ExtendToAllDofsLocalMatrix(
    const T1& rLocalMatrix,
    T2& rAllDofsMatrix
    )
  {
    rAllDofsMatrix.clear();
    for (std::size_t kk = 0; kk < rAllDofsMatrix.size1(); kk += rLocalMatrix.size1()) {
      for (std::size_t i = 0; i < rLocalMatrix.size1(); ++i) {
        for (std::size_t j = 0; j < rLocalMatrix.size2(); ++j) {
          if (std::abs(rLocalMatrix(i, j)) > ZeroTolerance) {
            rAllDofsMatrix(i + kk, j + kk) = rLocalMatrix(i, j);
          }
        }
      }
    }
  }

  /**
   * @brief This matrix extends the local matrix to all the dofs matrix
   * @details Following FELiScE ordering
   * @param rLocalMatrix The local matrix
   * @param rAllDofsMatrix The global matrix
   * @tparam T1 The type of local matrix
   * @tparam T2 The type of all dofs matrix
   */
  template<class T1, class T2>
  static inline void ExtendToAllDofsLocalMatrixFELiScEOrdering(
    const T1& rLocalMatrix,
    T2& rAllDofsMatrix,
    const std::size_t NumberNodes
    )
  {
    // Auxiliary value
    const std::size_t number_dofs_types = rAllDofsMatrix.size1() / (rLocalMatrix.size1() * NumberNodes);
    const std::size_t delta_increment_dof = rAllDofsMatrix.size1() / NumberNodes;

    // Clear
    rAllDofsMatrix.clear();

    // Iterate and assign
    for (std::size_t jj = 0; jj < number_dofs_types; ++jj) {
      for (std::size_t kk = 0; kk < NumberNodes; ++kk) {
        for (std::size_t i = 0; i < rLocalMatrix.size1(); ++i) {
          for (std::size_t j = 0; j < rLocalMatrix.size2(); ++j) {
            if (std::abs(rLocalMatrix(i, j)) > ZeroTolerance) {
              rAllDofsMatrix(NumberNodes * i + kk + jj * delta_increment_dof, NumberNodes * j + kk + jj * delta_increment_dof) = rLocalMatrix(i, j);
            }
          }
        }
      }
    }
  }


};

}

#endif
