//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include <tabulate.hpp>

// Project includes
#include "Model/model.hpp"
#include "Tools/test_utilities.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce
{

namespace TestUtilities
{

std::string GetCurrentFolder(
  const std::string rFilename,
  const std::string rFullFilename
  )
{
  std::size_t pos = rFullFilename.find(rFilename);
  return rFullFilename.substr(0,pos);
}

/***********************************************************************************/
/***********************************************************************************/

bool CompareTwoFiles(const std::string& file1, const std::string& file2)
{
  namespace fs = std::filesystem;

  // Check file sizes first — quick failure if they differ
  try {
      if (fs::file_size(file1) != fs::file_size(file2)) {
          std::cerr << "Files differ in size." << std::endl;
          return false;
      }
  } catch (const fs::filesystem_error& e) {
      std::cerr << "Filesystem error: " << e.what() << std::endl;
      return false;
  }

  // Open both files in binary mode
  std::ifstream stream1(file1, std::ios::binary);
  std::ifstream stream2(file2, std::ios::binary);

  if (!stream1.is_open() || !stream2.is_open()) {
      std::cerr << "Error: Unable to open one or both files." << std::endl;
      return false;
  }

  char byte1 = 0, byte2 = 0;
  std::size_t line = 1;
  std::size_t column = 0;

  while (true) {
    ++column;

    stream1.get(byte1);
    stream2.get(byte2);

    // Both files reached EOF simultaneously, they are identical. Break the loop
    if (stream1.eof() && stream2.eof()) {
      break;
    }

    // If one stream has ended before the other, the files differ
    if (stream1.eof() != stream2.eof()) {
      std::cerr << "Files differ: one ended before the other." << std::endl;
      return false;
    }

    if (byte1 != byte2) {
        std::cerr << "Files differ at line " << line << ", column " << column << std::endl;
        return false;
    }

    // Handle newlines (this is UNIX-style)
    if (byte1 == '\n') {
        ++line;
        column = 0;
    }
  }

  // If both files are read and no differences were found, they are identical
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshLine(const std::array<double, 3> Delta)
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Seg2) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(Delta[0], Delta[1], Delta[2]);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Seg2);
  std::vector<felInt> elem0({1,2});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Seg2, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Seg2);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTriangle()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 2;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh2D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Tria3) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Tria3);
  std::vector<felInt> elem0({1,2,3});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tria3, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tria3);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  // Set normals manually
  p_mesh_region->allocateListNormalsAndTangents();
  Point aux_normal(0.0,0.0,1.0);
  p_mesh_region->listNormal(0) = aux_normal;
  p_mesh_region->listNormal(1) = aux_normal;
  p_mesh_region->listNormal(2) = aux_normal;

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshInvertedTriangle()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 2;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh2D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Tria3) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Tria3);
  std::vector<felInt> elem0({3,2,1});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tria3, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tria3);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  // Set normals manually
  p_mesh_region->allocateListNormalsAndTangents();
  Point aux_normal(0.0,0.0,-1.0);
  p_mesh_region->listNormal(0) = aux_normal;
  p_mesh_region->listNormal(1) = aux_normal;
  p_mesh_region->listNormal(2) = aux_normal;

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTriangles()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0,1})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 2;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh2D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Tria3) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Tria3);
  std::vector<felInt> elem0({1,2,4});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tria3, 0, elem0, true);
  std::vector<felInt> elem1({2,3,4});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tria3, 1, elem1, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tria3);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshQuadrilaterals()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 2;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh2D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Quad4) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Quad4);
  std::vector<felInt> elem0({1,2,3,4});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Quad4, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Quad4);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTetrahedron()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Tetra4) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Tetra4);
  std::vector<felInt> elem0({1,2,3,4});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tetra4);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshInvertedTetrahedron()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Tetra4) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Tetra4);
  std::vector<felInt> elem0({4,1,2,3});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tetra4);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTetrahedra()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0,1,2,3,4})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Tetra4) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 1.0, 1.0);
  r_points.emplace_back(0.0, 1.0, 1.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Tetra4);
  std::vector<felInt> elem0({1,2,4,5});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 0, elem0, true);
  std::vector<felInt> elem1({2,3,4,7});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 1, elem1, true);
  std::vector<felInt> elem2({2,7,4,5});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 2, elem2, true);
  std::vector<felInt> elem3({2,6,7,5});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 3, elem3, true);
  std::vector<felInt> elem4({4,7,8,5});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Tetra4, 4, elem4, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tetra4);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshHexahedron()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Hexa8) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 1.0, 1.0);
  r_points.emplace_back(0.0, 1.0, 1.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Hexa8);
  std::vector<felInt> elem0({1,2,3,4,5,6,7,8});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Hexa8, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Hexa8);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshInvertedHexahedron()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Hexa8) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 1.0, 1.0);
  r_points.emplace_back(0.0, 1.0, 1.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Hexa8);
  std::vector<felInt> elem0({5,6,7,8,1,2,3,4});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Hexa8, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Hexa8);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshPrism()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Prism6) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 1.0, 1.0);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Prism6);
  std::vector<felInt> elem0({1,2,3,4,5,6});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Prism6, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Prism6);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshPrism9()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Prism9) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 1.0, 1.0);
  r_points.emplace_back(0.0, 0.0, 0.5);
  r_points.emplace_back(1.0, 0.0, 0.5);
  r_points.emplace_back(1.0, 1.0, 0.5);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Prism9);
  std::vector<felInt> elem0({1,2,3,4,5,6,7,8,9});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Prism9, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Prism9);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshPrism15()
{
  std::map<int,std::vector<felInt>> list_elements =
  {
      std::pair<int,std::vector<felInt>>(1, {0})
  };

  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

  p_mesh_region->numCoor() = 3;
  p_mesh_region->domainDim() = felisce::GeometricMeshRegion::GeoMesh3D;
  p_mesh_region->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
  p_mesh_region->numElements(felisce::GeometricMeshRegion::Prism15) = list_elements[1].size();

  auto& r_points = p_mesh_region->listPoints();
  r_points.emplace_back(0.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.0, 0.0);
  r_points.emplace_back(1.0, 1.0, 0.0);
  r_points.emplace_back(0.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.0, 1.0);
  r_points.emplace_back(1.0, 1.0, 1.0);
  r_points.emplace_back(0.5, 0.0, 0.0);
  r_points.emplace_back(1.0, 0.5, 0.0);
  r_points.emplace_back(0.5, 0.5, 0.0);
  r_points.emplace_back(0.5, 0.0, 1.0);
  r_points.emplace_back(1.0, 0.5, 1.0);
  r_points.emplace_back(0.5, 0.5, 1.0);
  r_points.emplace_back(0.0, 0.0, 0.5);
  r_points.emplace_back(1.0, 0.0, 0.5);
  r_points.emplace_back(1.0, 1.0, 0.5);

  p_mesh_region->allocateElements(felisce::GeometricMeshRegion::Prism15);
  std::vector<felInt> elem0({0,1,2,3,4,5,6,7,8,9,10,11,12,13,14});
  p_mesh_region->setOneElement(felisce::GeometricMeshRegion::Prism15, 0, elem0, true);

  p_mesh_region->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Prism15);
  p_mesh_region->setBagElementTypeDomain();
  p_mesh_region->setBagElementTypeDomainBoundary();

  return p_mesh_region;
}

/***********************************************************************************/
/***********************************************************************************/

template<class TFEType>
typename TFEType::Pointer GenerateFiniteElement(
  const felisce::GeometricMeshRegion::ElementType eltType,
  const std::vector<felisce::DegreeOfExactness>& rDegreeOfExactnessVector,
  const std::array<double, 3> Delta
  )
{
  // Retrieve mesh
  GeometricMeshRegion::Pointer p_mesh = nullptr;

  if (eltType == felisce::GeometricMeshRegion::Seg2) {
    p_mesh = GenerateMinimalMeshLine(Delta);
  } else if (eltType == felisce::GeometricMeshRegion::Tria3) {
    p_mesh = GenerateMinimalMeshTriangle();
  } else if (eltType == felisce::GeometricMeshRegion::Quad4) {
    p_mesh = GenerateMinimalMeshQuadrilaterals();
  } else if (eltType == felisce::GeometricMeshRegion::Tetra4) {
    p_mesh = GenerateMinimalMeshTetrahedra();
  } else if (eltType == felisce::GeometricMeshRegion::Hexa8) {
    p_mesh = GenerateMinimalMeshHexahedron();
  } else if (eltType == felisce::GeometricMeshRegion::Prism6) {
    p_mesh = GenerateMinimalMeshPrism();
  } else if (eltType == felisce::GeometricMeshRegion::Prism9) {
    p_mesh = GenerateMinimalMeshPrism9();
  } else if (eltType == felisce::GeometricMeshRegion::Prism15) {
    p_mesh = GenerateMinimalMeshPrism15();
  } else { // Triangle
    p_mesh = GenerateMinimalMeshTriangle();
  }

  // Generate element from previous mesh
  const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  const int typeOfFiniteElement = 0;
  const RefElement* refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *p_mesh);
  auto p_element = felisce::make_shared<TFEType>(*refEle,*geoEle, rDegreeOfExactnessVector);

  /* Now, we generate the integration weights */

  //Points of the current element.
  std::vector<felisce::Point*> elemPoint;

  //Id of points of the element.
  std::vector<felisce::felInt> elemIdPoint;

  // Counter of elements
  felInt numElement = 0;

  // Loop on element types
  const auto numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  elemPoint.resize(numPointPerElt, nullptr);
  elemIdPoint.resize(numPointPerElt,0);

  p_mesh->getOneElement(eltType, numElement, elemIdPoint, elemPoint);
  ++numElement;

  // Compute integration points
  p_element->updateMeas(0, elemPoint);

  return p_element;
}

// Explicit instantiation.
template felisce::CurBaseFiniteElement::Pointer GenerateFiniteElement<felisce::CurBaseFiniteElement>(const felisce::GeometricMeshRegion::ElementType eltType, const std::vector<felisce::DegreeOfExactness>& rDegreeOfExactnessVector, const std::array<double, 3> Delta);
template felisce::CurvilinearFiniteElement::Pointer GenerateFiniteElement<felisce::CurvilinearFiniteElement>(const felisce::GeometricMeshRegion::ElementType eltType, const std::vector<felisce::DegreeOfExactness>& rDegreeOfExactnessVector, const std::array<double, 3> Delta);
template felisce::CurrentFiniteElement::Pointer GenerateFiniteElement<felisce::CurrentFiniteElement>(const felisce::GeometricMeshRegion::ElementType eltType, const std::vector<felisce::DegreeOfExactness>& rDegreeOfExactnessVector, const std::array<double, 3> Delta);

/***********************************************************************************/
/***********************************************************************************/

template<class TFEType>
typename TFEType::Pointer GenerateFiniteElement(
  const felisce::GeometricMeshRegion::ElementType eltType,
  const felisce::DegreeOfExactness DegreeOfExactnessVector,
  const std::array<double, 3> Delta
  )
{
  return GenerateFiniteElement<TFEType>(eltType, std::vector<felisce::DegreeOfExactness>(1, DegreeOfExactnessVector), Delta);
}

// Explicit instantiation.
template felisce::CurBaseFiniteElement::Pointer GenerateFiniteElement<felisce::CurBaseFiniteElement>(const felisce::GeometricMeshRegion::ElementType eltType, const felisce::DegreeOfExactness DegreeOfExactnessVector, const std::array<double, 3> Delta);
template felisce::CurvilinearFiniteElement::Pointer GenerateFiniteElement<felisce::CurvilinearFiniteElement>(const felisce::GeometricMeshRegion::ElementType eltType, const felisce::DegreeOfExactness DegreeOfExactnessVector, const std::array<double, 3> Delta);
template felisce::CurrentFiniteElement::Pointer GenerateFiniteElement<felisce::CurrentFiniteElement>(const felisce::GeometricMeshRegion::ElementType eltType, const felisce::DegreeOfExactness DegreeOfExactnessVector, const std::array<double, 3> Delta);

/***********************************************************************************/
/***********************************************************************************/

felisce::ElementVector GenerateElementVector(
  const felisce::GeometricMeshRegion::ElementType eltType,
  const std::size_t NumberOfComponents
  )
{
  auto p_elem = GenerateFiniteElement<felisce::CurBaseFiniteElement>(eltType);
  return ElementVector(std::vector<const felisce::CurBaseFiniteElement*>{p_elem.get()}, std::vector<std::size_t>{NumberOfComponents});
}

/***********************************************************************************/
/***********************************************************************************/

felisce::ElementMatrix GenerateElementMatrix(
  const felisce::GeometricMeshRegion::ElementType eltType,
  const std::size_t NumberOfComponents
  )
{
  auto p_elem = GenerateFiniteElement<felisce::CurBaseFiniteElement>(eltType);
  return ElementMatrix(std::vector<const felisce::CurBaseFiniteElement*>{p_elem.get()},std::vector<std::size_t>{NumberOfComponents},std::vector<std::size_t>{NumberOfComponents});
}

/***********************************************************************************/
/***********************************************************************************/

template<class TFEType>
felisce::ElementField GenerateElementField(
  const TFEType& rElement,
  const std::size_t NumberOfComponents,
  const ElementFieldType FieldType,
  const double Value
  )
{
  auto field = ElementField();
  field.initialize(FieldType, rElement, NumberOfComponents);
  if (FieldType == CONSTANT_FIELD) {
    field.update(Value);
  }
  return field;
}

// Explicit instantiation.
template felisce::ElementField GenerateElementField<felisce::CurvilinearFiniteElement>(const felisce::CurvilinearFiniteElement& rElement, const std::size_t NumberOfComponents, const ElementFieldType FieldType, const double Value);
template felisce::ElementField GenerateElementField<felisce::CurrentFiniteElement>(const felisce::CurrentFiniteElement& rElement, const std::size_t NumberOfComponents, const ElementFieldType FieldType, const double Value);

/***********************************************************************************/
/***********************************************************************************/

void RetrieveSolutionTable(
  felisce::LinearProblem& rLinearProblem,
  const std::string FileType
  )
{
  std::vector<double> valueVec(rLinearProblem.numDof());
  rLinearProblem.sequentialSolution().getAllValuesInAppOrdering(rLinearProblem.ao(), valueVec);

  std::size_t dl_global = 0;
  const std::size_t number_nodes = rLinearProblem.supportDofUnknown()[0].listNode().size();

  tabulate::Table solution;
  solution.add_row({"NODE", "DL", "DL GLOBAL", "SOLUTION"});
  for (std::size_t i_node = 0; i_node < number_nodes; ++i_node) {
    for (std::size_t i_dim = 0; i_dim < 3; ++i_dim) {
      std::stringstream buffer;
      buffer << std::setprecision(16) << std::scientific << valueVec[dl_global];
      if (i_dim == 0) {
        solution.add_row({std::to_string(i_node + 1), std::to_string(i_dim + 1), std::to_string(dl_global + 1), buffer.str()});
      } else {
        solution.add_row({"", std::to_string(i_dim + 1), std::to_string(dl_global + 1), buffer.str()});
      }
      dl_global++;
    }
  }

  // Right align NODE, DL, DL GLOBAL column
  solution.column(0).format().font_align(tabulate::FontAlign::right);
  solution.column(1).format().font_align(tabulate::FontAlign::right);
  solution.column(2).format().font_align(tabulate::FontAlign::right);

  // Center align SOLUTION column
  solution.column(3).format().font_align(tabulate::FontAlign::center);

  // Center-align and color header cells
  for (std::size_t i = 0; i < 4; ++i) {
    solution[0][i].format()
      .font_color(tabulate::Color::yellow)
      .font_align(tabulate::FontAlign::center)
      .font_style({tabulate::FontStyle::bold});
  }

  // Exported TXT
  if (FileType == "TXT") {
    std::ofstream txt_file;
    txt_file.open("solution.txt");
    txt_file << solution.str() << std::endl;
    txt_file.close();
  } else if (FileType == "MARKDOWN") { // Export to Markdown
    tabulate::MarkdownExporter exporter_md;
    auto markdown = exporter_md.dump(solution);

    // Exported Markdown
    std::ofstream markdown_file;
    markdown_file.open("solution.md");
    markdown_file << markdown << std::endl;
    markdown_file.close();
  } else if (FileType == "LATEX") { // Export to LaTeX
    tabulate::LatexExporter exporter_latex;
    auto latex = exporter_latex.dump(solution);

    // Exported LaTeX
    std::ofstream latex_file;
    latex_file.open("solution.tex");
    latex_file << latex << std::endl;
    latex_file.close();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void FillTablesResults(
  felisce::LinearProblem& rLinearProblem,
  std::unordered_map<int, felisce::TableInterpolation>& rTables
  )
{
  // Retrieve the solution in the current time step
  std::vector<double> valueVec(rLinearProblem.numDof());
  rLinearProblem.sequentialSolution().getAllValuesInAppOrdering(rLinearProblem.ao(), valueVec);

  // Retrieve curremt time
  const double time = rLinearProblem.model()->fstransient()->time;

  // Fill the tables
  for (auto& r_table_pair : rTables) {
    const auto idof = r_table_pair.first;
    auto& r_table = r_table_pair.second;
    r_table.PushBack(time, valueVec[idof]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ImposeSolution(
  felisce::LinearProblem& rLinearProblem,
  const std::vector<double>& rSolution
  )
{
  // Set solution
  auto& u = rLinearProblem.solution();
  const felInt numDof = u.getSize();
  felInt identityMap[numDof];
  for (felInt i=0; i<numDof; i++) {
    identityMap[i] = i;
  }
  AOApplicationToPetsc(rLinearProblem.ao(),numDof,identityMap);
  u.setValues(numDof,identityMap, rSolution.data(), INSERT_VALUES);
  u.assembly();
  rLinearProblem.gatherSolution();
  auto& u_nl = rLinearProblem.evaluationState();
  u_nl.copyFrom(u);
  rLinearProblem.gatherEvaluationState();
}

/***********************************************************************************/
/***********************************************************************************/

void SaveSolutionToFile(
  felisce::LinearProblem& rLinearProblem,
  const std::string& rAppendixName,
  const std::size_t Precision,
  const bool EvaluationState
  )
{
  // Retrieve the solution in the current time step
  const std::size_t number_of_points = rLinearProblem.mesh()->numPoints(); 
  const std::size_t number_of_dofs = rLinearProblem.numDof();
  std::vector<double> valueVec(number_of_dofs);
  const std::size_t number_dof_per_node = number_of_dofs / number_of_points;
  if (EvaluationState) {
    rLinearProblem.seqEvaluationState().getAllValuesInAppOrdering(rLinearProblem.ao(), valueVec);
  } else {
    rLinearProblem.sequentialSolution().getAllValuesInAppOrdering(rLinearProblem.ao(), valueVec);
  }

  // Save solution to file
  std::ofstream out_file;
  const std::string file_name = "solution" + rAppendixName + ".out";
  out_file.open(file_name);
  out_file.precision(Precision);
  out_file << std::scientific;
  for (std::size_t i_point = 0; i_point < number_of_points; ++i_point) {
    for (std::size_t i_dof = 0; i_dof < number_dof_per_node; ++i_dof) {
      if (valueVec[i_point * number_dof_per_node + i_dof] >= 0.0) out_file << " ";
      out_file << valueVec[i_point * number_dof_per_node + i_dof];
      out_file << "\t";
    }
    out_file << "\n";
  }
  out_file << std::endl;
  out_file.close();
}

/***********************************************************************************/
/***********************************************************************************/

void SaveRHSToFile(
  felisce::LinearProblem& rLinearProblem,
  const std::string& rAppendixName,
  const std::size_t Precision
  )
{
  // Retrieve the solution in the current time step
  const std::size_t number_of_points = rLinearProblem.mesh()->numPoints(); 
  const std::size_t number_of_dofs = rLinearProblem.numDof();
  std::vector<double> valueVec(number_of_dofs);
  const std::size_t number_dof_per_node = number_of_dofs / number_of_points;
  rLinearProblem.vector().getAllValuesInAppOrdering(rLinearProblem.ao(), valueVec);

  // Save RHS to file
  std::ofstream out_file;
  const std::string file_name = "rhs" + rAppendixName + ".out";
  out_file.open(file_name);
  out_file.precision(Precision);
  out_file << std::scientific;
  for (std::size_t i_point = 0; i_point < number_of_points; ++i_point) {
    for (std::size_t i_dof = 0; i_dof < number_dof_per_node; ++i_dof) {
      if (valueVec[i_point * number_dof_per_node + i_dof] >= 0.0) out_file << " ";
      out_file << valueVec[i_point * number_dof_per_node + i_dof];
      out_file << "\t";
    }
    out_file << "\n";
  }
  out_file << std::endl;
  out_file.close();
}

/***********************************************************************************/
/***********************************************************************************/

std::vector<double> GetSolutionFromFile(const std::string& rFileName)
{
  // Retrieve the solution in the current file
  std::vector<double> solution;

  // Opening both file in read only mode
  FILE* fp = fopen(rFileName.c_str(), "r");

  if (fp == NULL) {
      std::cout << "Error : Files not open" << std::endl;
      return solution;
  }

  // Fetching character
  char ch = getc(fp);

  // iterate loop till end of file
  std::string word;
  while (ch != EOF) {
      // If both variable encounters new line then line variable is incremented and pos variable is set to 0
      if ((ch == ' ') || (ch == '\t') || (ch == '\r') || (ch == '\n') || (ch == ';') || (ch == ',')) {
          if (word.size() > 0) {
            solution.push_back(std::stod(word));
          }
          ch = getc(fp);
          word.clear();
          continue;
      } else {
          // If ch is not a space then it is a number
          word += ch;
      }

      // Fetching character until end of file
      ch = getc(fp);
  }

  // Closing both files
  fclose(fp);

  return solution;
}

} // namespace TestUtilities

} // namespace felisce
