//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _PETSC_UTILITIES_HPP
#define _PETSC_UTILITIES_HPP

// System includes

// External includes

// Project includes
#include "PETScInterface/petscVector.hpp"
#include "PETScInterface/petscMatrix.hpp"

namespace felisce
{

namespace PETScUtilities
{
  /**
   * @brief This method generates a sparse matrix plot (apy)
   * @param rA The PetscMatrix to generate spy plot
   * @param rFilename The name of the file
   * @param structure If the colours will be based on the structure of the matrix (0, postive, negative) and itd values (gradient of values)
   */
  void SparsePlot(
    const PetscMatrix& rA,
    const std::string& rFilename,
    const bool structure = true
    );
}

}

#endif
