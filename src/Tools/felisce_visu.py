#!/usr/bin/env python

"""
Simple Tkinter/VTK/Matplotlib visualizations of test cases.

All visualisation class implement a widget with an update method that
take the simulation time as argument.

TimePlayer widget can be used to interactively modify simulation time,
and call a update method, which can be used to update as many
visualization widgets as needed.
"""

import os
import sys

#======================================================================
# check LC_ALL
#======================================================================

if os.getenv('LC_ALL') != 'C':
    print "Please export the LC_ALL environment variable to C."
    print "Otherwise, it may cause errors in C input/output functions."
    sys.exit()

#======================================================================
# import modules
#======================================================================

try:
    import numpy as np
except ImportError:
    print 'Failed to import numpy'
    sys.exit()

try:
    import Tkinter as Tk
    from Tkinter import Frame
except ImportError:
    print 'Failed to import Tkinter'
    sys.exit()

try:
    import vtk
    from vtk.tk.vtkTkRenderWindowInteractor import vtkTkRenderWindowInteractor
    from vtk.util.numpy_support import vtk_to_numpy
    from vtk.util.numpy_support import numpy_to_vtk
except ImportError:
    print 'Failed to import from vtk. Try to install python-vtk package'
    sys.exit()

try:
    import matplotlib
    matplotlib.use('TkAgg')
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
    from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg
    from matplotlib.figure import Figure
except ImportError:
    print 'Failed to import matplotlik.'
    sys.exit()

#======================================================================
# ugly tricks to make toplevel windows not overlap
#======================================================================

class WmPlacer:

    def __init__(self,screen_width,screen_heigth):
        self.x = 0
        self.y = 0
        self.screen_width = screen_width
        self.screen_heigth = screen_heigth

    def set_screen_size(self,(screen_width,screen_height)):
        self.screen_width = screen_width
        self.screen_height = screen_height
        print screen_width, screen_height

    def place(self,window):

        width,height = window.size()

        l = 400

        window.geometry("+%i+%i" % (self.x, self.y))

        self.x += l

        if self.x+l > self.screen_width:
            self.x = 0
            self.y += l

        if self.y+l > self.screen_height:
            self.y = 0

wmplacer= WmPlacer(300,300)

root = None

def get_toplevel():
    global root

    if root:
        return Tk.Toplevel() 
    else:
        root = Tk.Tk()
        wmplacer.set_screen_size(root.maxsize())
        return root

def start():
    root.mainloop()

#======================================================================
# visualization classes
#======================================================================

class HeatFileReader:

    """
    Reader for Tests/Examples/Heat_[0123]_*

    todo:
      In method _update_time, label correponding to triangles is hardcoded to 1:
         self.triangularData = self.multiBlock.GetBlock(1)
      (others label are for boundary lines.)
    """

    def __init__(self,filename):
        """
        Open file and read times
        """

        if not os.path.isfile(filename):
            raise ValueError, '%r si not a valid file' % (filename,)

        # Read the source file
        self.reader = vtk.vtkGenericEnSightReader()
        self.reader.SetCaseFileName(filename)
        self.reader.Update()
        self.times = vtk_to_numpy( self.reader.GetTimeSets().GetItem(0) )

    def get_times(self):
        """
        Return times as a numpy array
        """

        return self.times


    def get_points(self,itime=None,time=None,convert_numpy=False):
        """
        Read and return points at the i-st time.
        """

        self._update_time(itime,time)
        vtk_points = self.triangularData.GetPoints()

        if convert_numpy:
            return vtk_to_numpy( vtk_points.GetData() )
        else:
            return vtk_points

    def get_triangles(self,itime=None,time=None,convert_numpy=False):
        """
        Read and return triangles at the i-st time.
        """

        self._update_time(itime,time)
        vtk_triangles = self.triangularData.GetCells()

        if convert_numpy:
            return vtk_to_numpy( vtk_triangles.GetData() )
        else:
            return vtk_triangles

    def read(self,itime=None,time=None,convert_numpy=False):
        """
        Read and return temperature at the i-st time
        """

        if time:
            time = np.float64(time)

        self._update_time(itime,time)
        vtk_temperature = self.triangularData.GetPointData().GetScalars()

        if convert_numpy:
            return vtk_to_numpy( vtk_temperature )
        else:
            return vtk_temperature

    def _update_time(self,itime,time):
        """
        update data to i-st time.
        """

        two_values = itime==None and time==None
        zero_values = itime!=None and time!=None

        if two_values or zero_values:
            raise ValueError, "itime OR time must be specified, got %r and %r" % (itime,time)

        if itime!=None:
            self.time = self.times[itime]

        if time!=None:
            self.time = time

        self.reader.SetTimeValue(self.time)
        self.reader.Update()

        self.multiBlock = self.reader.GetOutput()
        self.triangularData = self.multiBlock.GetBlock(1)

class TriangularScalarMap(Frame):

    """
    Tkinter/VTK visualization of triangular data
    """

    def __init__(self,
                 vtk_points,
                 vtk_triangles,
                 scalar_range=None,
                 parent=None,
                 autopack=True,
                 title="No title",
                 ):

        if not parent:
            parent = get_toplevel()

        Frame.__init__(self,parent)

        mainframe = Tk.Frame(self)

        # create a custom lut. The ltu is used at the mapper and at the scalar
        # bar
        self.lut = vtk.vtkLookupTable()
        self.lut.Build()

        # vtk_points must be a vtk object. If it is a numpy object, converting it to vtk
        if isinstance(vtk_points,np.ndarray):
            npoints = len(vtk_points)
            vtk_array = numpy_to_vtk(vtk_points)
            vtk_points = vtk.vtkPoints()
            vtk_points.SetData( vtk_array )

        else:
            npoints = len( numpy_to_vtk(vtk_points) )

        vtk_temperature = numpy_to_vtk( np.zeros( (npoints,) ) )

        # create vtkUnstructuredGrid
        triangle_type = 5
        self.triangularData = vtk.vtkUnstructuredGrid()
        self.triangularData.SetPoints( vtk_points )
        self.triangularData.SetCells( triangle_type, vtk_triangles )
        self.triangularData.GetPointData().SetScalars( vtk_temperature )
        self.triangularData.Update()
        if scalar_range:
            self.scalar_range = scalar_range
        else:
            self.scalar_range = self.triangularData.GetScalarRange()

        # create the Mapper
        self.mapper = vtk.vtkDataSetMapper()
        self.mapper.SetInput(self.triangularData)
        self.mapper.SetScalarRange(self.scalar_range)
        self.mapper.SetLookupTable(self.lut)

        # create the Actor
        self.actor = vtk.vtkActor()
        self.actor.SetMapper(self.mapper)

        # Create the Renderer
        self.renderer = vtk.vtkRenderer()
        self.renderer.AddActor(self.actor)
        self.renderer.SetBackground(1, 1, 1) # Set background to white

        # create the WindowInteractor
        self.win = vtkTkRenderWindowInteractor(self, width=400, height=400)
        self.win.GetRenderWindow().AddRenderer(self.renderer)
        style = vtk.vtkInteractorStyleTrackballCamera()
        self.win.SetInteractorStyle(style)
        self.win.pack(fill='both', expand=1, side='bottom')

        # create the scalar_bar
        self.scalar_bar = vtk.vtkScalarBarActor()
        self.scalar_bar.SetLookupTable(self.lut)
        self.renderer.AddActor2D(self.scalar_bar)

        # create the scalar_bar_widget
        self.win.Initialize()
        self.win.Start()

        if autopack:
            self.pack()
            Tk.Label(mainframe,text=title).pack(side="top")
            mainframe.pack()
            wmplacer.place(parent)
            parent.title(title)

    def update(self,vtk_temperature):

        # vtk_temperature must be a vtk object. If it is a numpy object, converting it to vtk
        if isinstance(vtk_temperature,np.ndarray):
            vtk_temperature = numpy_to_vtk(vtk_temperature)

        self.triangularData.GetPointData().SetScalars( vtk_temperature )
        self.mapper.SetInput(self.triangularData)
        self.mapper.SetScalarRange(self.scalar_range)
        self.mapper.SetLookupTable(self.lut)
        self.win.Render()

class TimePlayer(Frame):

    """
    TkPlayer is a widget that looks like this:

    +--------------------------------------------------------------+
    |Prev  Next  First  Last  Label                                |
    |                                                              |
    |  <=====================###================================>  |
    +--------------------------------------------------------------+


    Changing time call the update function.
    """

    def __init__(self, time_range, update, parent=None, autopack=True):

        if not parent:
            parent = get_toplevel()

        Frame.__init__(self,parent)

        # frametop will contains the top buttons/label (Prev,Next, ... , Quit)
        frametop = Tk.Frame(self)

        self.from_ = time_range[0]
        self.to = time_range[1]
        self.update = update

        # create the buttons/label into frametop
        Tk.Button(frametop,text="Prev",command=self.go_previous).pack(side="left")
        Tk.Button(frametop,text="Next",command=self.go_next).pack(side="left")
        Tk.Button(frametop,text="First",command=self.go_first).pack(side="left")
        Tk.Button(frametop,text="Last",command=self.go_last).pack(side="left")
        Tk.Label(frametop,text="").pack(side="left")

        frametop.pack(side='top')

        # scale bar
        cnf = {'orient':'horizontal','from':self.from_,'to':self.to, 'command':self.update_scale}
        self.scale = Tk.Scale(self, cnf=cnf)
        self.scale.pack(side="top",fill='x')

        if autopack:
            self.pack()
            wmplacer.place(parent)
            parent.title("TimePlayer")

    def go_previous(self):
        if self.current == self.from_:
            print "This is the first element"
        else:
            self.current -= 1
            self.scale.set(self.current)

    def go_next(self):
        if self.current == self.to:
            print "This is the last element."
        else:
            self.current += 1
            self.scale.set(self.current)

    def go_first(self):
        self.current = self.from_
        self.scale.set(self.current)

    def go_last(self):
        self.current = self.to
        self.scale.set(self.current)

    def update_scale(self,value):
        self.current = int(value)
        self.update(self.current)

class Plot2d(Frame):

    """
    Tk/Matplotlib widget where to create y = f(x) plot.
    """

    def __init__(self,scalar_range=None,parent=None,autopack=True):

        if not parent:
            parent = get_toplevel()

        Frame.__init__(self,parent)

        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig, master=parent)
        NavigationToolbar2TkAgg(self.canvas, parent)
        self.canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)
        self.scalar_range = scalar_range

        if autopack:
            self.pack()
            wmplacer.place(parent)
            parent.title("plot2d")

    def line(self,x,style,label):

        return Line(self.canvas,self.fig,self.ax,x,style,self.scalar_range,label)


class Line:

    """
    Simple y = f(x) plot, with update method
    """

    def __init__(self,canvas,fig,ax,x,style,scalar_range,label):
        
        self.fig = fig
        self.ax = ax
        self.canvas = canvas
        self.x = x 
        self.style = style
        self.empty = True
        self.scalar_range = scalar_range
        self.label = label

    def create(self,y):

        self.line, = self.ax.plot( self.x, y, self.style, label=self.label)
        self.empty = False

        if self.scalar_range:
            self.ax.set_ylim( self.scalar_range )

        self.xlim = self.ax.get_xlim()
        self.ylim = self.ax.get_ylim()

    def update(self,y):

        if self.empty:
            self.create(y)
        else:
            self.line.set_ydata(y)

        self.ax.set_xlim(self.xlim)
        self.ax.set_ylim(self.ylim)
        self.ax.legend()
        self.canvas.draw()
