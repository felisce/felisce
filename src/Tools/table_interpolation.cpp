//   ______ ______ _    _  _____    ______
//  |  ____|  ____| |  (_)/ ____|  |  ____|
//  | |__  | |__  | |   _| (___   ___| |__
//  |  __| |  __| | |  | |\___ \ / __|  __|
//  | |  | |____| |____| |____) | (__| |____
//  |_|  |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  Vicente Mataix Ferrandiz
//

// System includes
#include <iomanip>

// External includes
#include <tabulate.hpp>

// Project includes
#include "Tools/table_interpolation.hpp"

namespace felisce
{
double TableInterpolation::GetValue(const double X) const
{
  const std::size_t size = mData.size();

  if(size == 0) std::cout <<  "Get value from empty table" << std::endl;

  if(size == 1) { // Constant table. Returning the only value we have.
    return mData.begin()->second[0];
  }

  double result;
  if(X <= mData[0].first) {
    return Interpolate(X, mData[0].first, mData[0].second[0], mData[1].first, mData[1].second[0], result);
  }

  for(std::size_t i = 1 ; i < size ; i++) {
    if(X <= mData[i].first) {
      return Interpolate(X, mData[i-1].first, mData[i-1].second[0], mData[i].first, mData[i].second[0], result);
    }
  }

  // Now the x is outside the table and we hae to extrapolate it using last two records of table.
  return Interpolate(X, mData[size-2].first, mData[size-2].second[0], mData[size-1].first, mData[size-1].second[0], result);
}

/***********************************************************************************/
/***********************************************************************************/

TableInterpolation::result_row_type& TableInterpolation::GetNearestRow(const double X)
{
  const std::size_t size = mData.size();

  if(size == 0) std::cout <<  "Get value from empty table" << std::endl;

  if(size == 1) { // Constant table. Returning the only value we have.
      return mData.begin()->second;
  }

  if(X <= mData[0].first) {
    return mData[0].second;
  }

  for(std::size_t i = 1 ; i < size ; i++) {
    if(X <= mData[i].first) {
      return ((X - mData[i-1].first) < (mData[i].first - X)) ? mData[i-1].second : mData[i].second;
    }
  }

  // Now the x is outside the table and we hae to extrapolate it using last two records of table.
  return mData[size-1].second;
}

/***********************************************************************************/
/***********************************************************************************/

const double& TableInterpolation::GetNearestValue(const double X)  const
{
  const std::size_t size = mData.size();

  if(size == 0) std::cout <<  "Get value from empty table" << std::endl;

  if(size == 1) { // Constant table. Returning the only value we have.
    return mData.begin()->second[0];
  }

  if(X <= mData[0].first) {
    return mData[0].second[0];
  }

  for(std::size_t i = 1 ; i < size ; i++) {
    if(X <= mData[i].first) {
      return ((X - mData[i-1].first) < (mData[i].first - X)) ? mData[i-1].second[0] : mData[i].second[0];
    }
  }

  // Now the x is outside the table and we have to extrapolate it using last two records of table.
  return mData[size-1].second[0];
}

/***********************************************************************************/
/***********************************************************************************/

double& TableInterpolation::GetNearestValue(const double X)
{
  const std::size_t size = mData.size();

  if(size == 0) std::cout <<  "Get value from empty table" << std::endl;

  if(size == 1) { // Constant table. Returning the only value we have.
    return mData.begin()->second[0];
  }

  if(X <= mData[0].first) {
    return mData[0].second[0];
  }

  for(std::size_t i = 1 ; i < size ; i++) {
    if(X <= mData[i].first) {
      return ((X - mData[i-1].first) < (mData[i].first - X)) ? mData[i-1].second[0] : mData[i].second[0];
    }
  }

  // Now the x is outside the table and we have to extrapolate it using last two records of table.
  return mData[size-1].second[0];
}

/***********************************************************************************/
/***********************************************************************************/

double& TableInterpolation::Interpolate(const double X, const double X1, const double Y1, const double X2, const double Y2, double& Result) const
{
  const double epsilon = 1e-12;

  const double dx = X2 - X1;
  const double dy = Y2 - Y1;

  double scale = 0.0;

  if (dx > epsilon) {
      scale = (X - X1) / dx;
  }

  Result = Y1 + dy * scale;

  return Result;
}

/***********************************************************************************/
/***********************************************************************************/

void TableInterpolation::insert(const double X, const double Y)
{
    const result_row_type a = {{Y}};
    insert(X,a);
}

/***********************************************************************************/
/***********************************************************************************/

void TableInterpolation::insert(const double X, const result_row_type& Y)
{
  const std::size_t size = mData.size();

  if(size == 0) {
    mData.push_back(RecordType(X,Y));
  } else if(X <= mData[0].first) {
    mData.insert(mData.begin(), RecordType(X,Y));
  } else if(X > mData.back().first) {
    mData.push_back(RecordType(X,Y));
  } else {
    for(std::size_t i = 1 ; i < size ; i++) {
      if((X > mData[i-1].first) && (X <= mData[i].first)) {
        mData.insert(mData.begin() + i, RecordType(X,Y));
        break;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void TableInterpolation::PushBack(const double X, const double Y)
{
    const result_row_type a = {{Y}};
    mData.push_back(RecordType(X,a));
}

/***********************************************************************************/
/***********************************************************************************/

double TableInterpolation::GetDerivative(const double X) const
{
  const std::size_t size = mData.size();

  if(size == 0) std::cout <<  "Get value from empty table" << std::endl;

  if(size == 1) { // Constant table. Returning the only value we have.
    return 0.0;
  }

  double result = 0.0;
  if(X <= mData[0].first) {
    //return Interpolate(X, mData[0].first, mData[0].second[0], mData[1].first, mData[1].second[0], result);
    return 0.0;
  }

  for(std::size_t i = 1 ; i < size ; i++) {
    if(X <= mData[i].first) {
      return InterpolateDerivative( mData[i-1].first, mData[i-1].second[0], mData[i].first, mData[i].second[0], result);
    }
  }

  // If it lies outside the table values we will return 0.0.
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double& TableInterpolation::InterpolateDerivative( const double X1, const double Y1, const double X2, const double Y2, double& Result) const
{
  const double epsilon = 1e-12;
  double dx = X2 - X1;
  const double dy = Y2 - Y1;
  if (dx < epsilon) {
    dx=epsilon;
    std::cout
    << "*******************************************\n"
    << "*** ATTENTION: SMALL dX WHEN COMPUTING  ***\n"
    << "*** DERIVATIVE FROM TABLE. SET TO 1E-12 ***\n"
    << "*******************************************" 
    <<std::endl;
  }
  Result= dy/dx;
  return Result;
}

/***********************************************************************************/
/***********************************************************************************/

void TableInterpolation::PrintData(std::ostream& rOStream) const
{
  tabulate::Table table;
  table.add_row({"X", "Y"});
  const auto& r_data = Data();
  const std::size_t size = r_data.size();
  for(std::size_t i = 0 ; i < size ; i++) {
    std::stringstream buffer1, buffer2;
    buffer1 << std::setprecision(16) << std::scientific << r_data[i].first;
    buffer2 << std::setprecision(16) << std::scientific << r_data[i].second[0];
    table.add_row({buffer1.str(), buffer2.str()});
  }

  // Center align X, Y
  table.column(0).format().font_align(tabulate::FontAlign::center);
  table.column(1).format().font_align(tabulate::FontAlign::center);

  // Center-align and color header cells
  for (std::size_t i = 0; i < 2; ++i) {
    table[0][i].format()
      .font_color(tabulate::Color::yellow)
      .font_align(tabulate::FontAlign::center)
      .font_style({tabulate::FontStyle::bold});
  }

  rOStream << table << std::endl;
}

};

