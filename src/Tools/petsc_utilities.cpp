//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <cmath>
#include <limits>

// External includes
#include <bitmap_image.hpp>

// Project includes
#include "Tools/petsc_utilities.hpp"
#include "Core/filesystemUtil.hpp"

namespace felisce
{

namespace PETScUtilities
{

void SparsePlot(
  const PetscMatrix& rA,
  const std::string& rFilename,
  const bool structure
  )
{
  // Work directly with PETSc matrix
  const Mat& A = rA.toPetsc();

  // Get the size of the matrix
  PetscInt m, n;
  MatGetSize(A, &m, &n);

  // Initialize image
  bitmap_image spy(m, n);
  spy.clear();

  PetscInt i, j, rows, cols;
  
  // Fill the bitmap with white by default
  for (i = 0; i < m; i++) {
      for (j = 0; j < n; j++) {
          spy.set_pixel(i, j, 255, 255, 255);
      }
  }

  // Get the row indices and values (for accessing the sparse matrix data)
  PetscScalar max;
  felisce::PetscVector max_vector;
  max_vector.createMPIInstance(m);

  // Get the maximum absolute value for scaling
  MatGetRowMaxAbs(A, max_vector.toPetsc(), NULL);
  max = max_vector.max();

  // Get the number of columns in the matrix
  // cols = A->rmap->n;
  MatGetLocalSize(A, &rows, &cols);

  if (structure) {
      // For structure visualization: non-zero positions are marked in blue or red
      for (i = 0; i < cols; i++) {
          const PetscScalar* row_vals;
          const PetscInt* row_cols;
          PetscInt ncols;

          // Get the i-th row
          MatGetRow(A, i, &ncols, &row_cols, &row_vals);

          // Iterate through the row to identify non-zero values
          for (j = 0; j < ncols; j++) {
              if (row_vals[j] > 0.0) {
                  spy.set_pixel(i, row_cols[j], 0, 0, 204);  // Blue for positive values
              } else if (row_vals[j] < 0.0) {
                  spy.set_pixel(i, row_cols[j], 204, 0, 0);  // Red for negative values
              }
          }

          // Restore the row after processing
          MatRestoreRow(A, i, &ncols, &row_cols, &row_vals);
      }
  } else {
      // For value visualization: scale values based on max
      PetscScalar s;
      for (i = 0; i < cols; i++) {
          const PetscScalar* row_vals;
          const PetscInt* row_cols;
          PetscInt ncols;

          // Get the i-th row
          MatGetRow(A, i, &ncols, &row_cols, &row_vals);

          // Iterate through the row to apply color mapping based on values
          for (j = 0; j < ncols; j++) {
              s = std::abs(row_vals[j]) / max;
              const std::size_t aux = (1.0 - s) * 499;
              if (row_vals[j] > 0.0) {
                  spy.set_pixel(i, row_cols[j], hot_colormap[aux]);
              } else {
                  const std::size_t aux_neg = s * 500 + 499;
                  spy.set_pixel(i, row_cols[j], hot_colormap[aux_neg]);
              }
          }

          // Restore the row after processing
          MatRestoreRow(A, i, &ncols, &row_cols, &row_vals);
      }
  }

  // Save the bitmap image
  spy.save_image(rFilename);

}

/***********************************************************************************/
/***********************************************************************************/

} // namespace PETScUtilities

} // namespace felisce
