//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#ifdef FELISCE_WITH_SCIPLOT
#include "Core/NoThirdPartyWarning/Sciplot/sciplot.hpp"
#endif

// Project includes
#include "Tools/sciplot_utilities.hpp"

namespace felisce
{
  
namespace SciplotUtilities
{

void Generate2DPlot(
  const felisce::TableInterpolation& rTable,
  const std::string FileType,
  const bool ShowPlot
  )
{
  const std::vector<felisce::TableInterpolation> aux_tables = {rTable};
  Generate2DPlot(aux_tables, FileType, ShowPlot);
}

/***********************************************************************************/
/***********************************************************************************/

void Generate2DPlot(
  const std::vector<felisce::TableInterpolation>& rTables,
  const std::string FileType,
  const bool ShowPlot
  )
{
// ignore unused argument
#ifndef FELISCE_WITH_SCIPLOT
static_cast<void>(rTables);
static_cast<void>(FileType);
static_cast<void>(ShowPlot);
std::cout << "Sciplot SciplotUtilities NO" << std::endl;
#endif
//end ignore unused argument

#ifdef FELISCE_WITH_SCIPLOT

    // Create a Plot2D object
    sciplot::Plot2D plot;

    // Set the width and height of the plot in points (72 points = 1 inch)
    plot.size(360, 360);

    // Set the font name and size
    plot.fontName("Helvetica");
    plot.fontSize(12);

    // Set the x and y labels
    auto& r_var_names = rTables[0].GetVariablesNames(); // Get the names of the variables, assuming always same name
    plot.xlabel(r_var_names[0]);
    plot.ylabel(r_var_names[1]);

    // Set some options for the legend
    plot.legend()
      .atTop()
      .fontSize(10)
      .displayHorizontal()
      .displayExpandWidthBy(2);

    // Plotting all tables
    for (auto& r_table : rTables) {
      auto& r_names = r_table.GetVariablesNames();
      const auto vectors = r_table.GetTableAsVectors();

      // The x vector
      const sciplot::Vec x(vectors[0].data(), vectors[0].size());

      // The y vector
      const sciplot::Vec y(vectors[1].data(), vectors[1].size());

      // Plot the function
      plot.drawCurveWithPoints(x, y).label(r_names[1]);
    }

    // Create figure to hold plot
    sciplot::Figure fig = {{plot}};

    // Create canvas to hold figure
    sciplot::Canvas canvas = {{fig}};

    // Show the plot in a pop-up window
    if (ShowPlot) {
      canvas.show();
    }
    std::cout << "Sciplot SciplotUtilities YES" << std::endl;
    // Save the plot to a PDF file
    canvas.save(rTables[0].GetTableName() + "." + FileType);
#endif
}

} // namespace SciplotUtilities
} // namespace felisce
