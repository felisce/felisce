//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#include <petscmat.h>
#include <petscksp.h>
#include "Core/configure.hpp"
#include "Core/NoThirdPartyWarning/Petsc/sys.hpp" // Jacques 13/07/22: added due to definition of FELISCE_PETSC_NULLPTR

// Project includes
#include "Core/filesystemUtil.hpp"


#if !defined(FELISCE_KSP_SOLVER_H_INCLUDED )
#define  FELISCE_KSP_SOLVER_H_INCLUDED

int SolveMatrix(
  const std::string FilenameMatrix,
  const std::string FilenameVector,
  const std::string SolverType,
  const std::string PreconditionerType = "none"
  )
{
  PetscErrorCode ierr;
  PetscInt       its,m,n,mvec;
  PetscReal      norm;
  Vec            x,b,u;
  Mat            A;
  KSP            ksp;
  PetscViewer    fd;
  PetscLogDouble t1, t2;
  const PetscReal rtol = 1.e-8;

  /* Read matrix and RHS */
  const char* file_matrix = FilenameMatrix.c_str();
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,file_matrix,FILE_MODE_READ,&fd);
  CHKERRQ(ierr);
  ierr = MatCreate(PETSC_COMM_WORLD,&A);
  CHKERRQ(ierr);
  ierr = MatSetType(A,MATMPIAIJ);
  CHKERRQ(ierr);
  ierr = MatLoad(A,fd);
  CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&fd);
  CHKERRQ(ierr);

  const char* file_vector = FilenameVector.c_str();
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD,file_vector,FILE_MODE_READ,&fd);
  CHKERRQ(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD,&b);
  CHKERRQ(ierr);
  ierr = VecSetType(b,VECMPI);
  CHKERRQ(ierr);
  ierr = VecLoad(b,fd);
  CHKERRQ(ierr);
  ierr = PetscViewerDestroy(&fd);
  CHKERRQ(ierr);

  /*
     If the load matrix is larger then the vector, due to being padded
     to match the blocksize then create a new padded vector
  */
  ierr = MatGetSize(A,&m,&n);
  CHKERRQ(ierr);
  ierr = VecGetSize(b,&mvec);
  CHKERRQ(ierr);
  if (m > mvec) {
    Vec         tmp;
    PetscScalar *bold,*bnew;
    /* create a new vector b by padding the old one */
    ierr = VecCreate(PETSC_COMM_WORLD,&tmp);
    CHKERRQ(ierr);
    ierr = VecSetSizes(tmp,PETSC_DECIDE,m);
    CHKERRQ(ierr);
    ierr = VecSetFromOptions(tmp);
    CHKERRQ(ierr);
    ierr = VecGetArray(tmp,&bnew);
    CHKERRQ(ierr);
    ierr = VecGetArray(b,&bold);
    CHKERRQ(ierr);
    ierr = PetscArraycpy(bnew,bold,mvec);
    CHKERRQ(ierr);
    ierr = VecDestroy(&b);
    CHKERRQ(ierr);
    b    = tmp;
  }

  /* Set up solution */
  ierr = VecDuplicate(b,&x);
  CHKERRQ(ierr);
  ierr = VecDuplicate(b,&u);
  CHKERRQ(ierr);
  ierr = VecSet(x,0.0);
  CHKERRQ(ierr);

  /* Solve system */
  ierr = KSPCreate(PETSC_COMM_WORLD,&ksp);
  CHKERRQ(ierr);
  ierr = KSPSetOperators(ksp,A,A);
  CHKERRQ(ierr);

  // Retrieve the preconditioner
  PC pc;
  KSPGetPC(ksp, &pc);

  // Assigning the solver
  if (SolverType == "MUMPS") {
    KSPSetType(ksp, KSPPREONLY);
    PCSetType(pc, PCLU);
    PCFactorSetMatSolverType(pc, MATSOLVERMUMPS);
  // } else if (SolverType == "PASTIX") { // NOTE: Does not work properly yet with PETSc integration
  //   KSPSetType(ksp, KSPPREONLY);
  //   PC pc;
  //   KSPGetPC(ksp, &pc);
  //   PCSetType(pc, PCLU);
  //   PCFactorSetMatSolverType(pc, MATSOLVERPASTIX);
#ifdef FELISCE_WITH_SUPERLU
  } else if (SolverType == "SUPERLU_DIST") {
    KSPSetType(ksp, KSPPREONLY);
    PC pc;
    KSPGetPC(ksp, &pc);
    PCSetType(pc, PCLU);
    PCFactorSetMatSolverType(pc,  MATSOLVERSUPERLU_DIST);
#endif
#ifdef FELISCE_WITH_HYPRE
  } else if (SolverType == "hypre") {
    KSPSetType(ksp, "gmres");
    PCSetType(pc, "hypre");
    PCHYPRESetType(pc, PreconditionerType.c_str());
#endif
  } else {
    KSPSetType(ksp, SolverType.c_str());
    PCSetType(pc, PreconditionerType.c_str());
  }
  PetscOptionsSetValue(NULL, "-mat_mumps_icntl_20", "0");

  CHKERRQ(ierr);
  ierr = KSPSetTolerances(ksp, rtol, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT);
  CHKERRQ(ierr);
  ierr = KSPSetFromOptions(ksp);
  CHKERRQ(ierr);
  ierr = PetscTime(&t1);
  CHKERRQ(ierr);
  ierr = KSPSolve(ksp,b,x);
  CHKERRQ(ierr);
  ierr = PetscTime(&t2);
  CHKERRQ(ierr);

  /* Show result */
  ierr = MatMult(A,x,u);
  CHKERRQ(ierr);
  ierr = VecAXPY(u,-1.0,b);
  CHKERRQ(ierr);
  ierr = VecNorm(u,NORM_2,&norm);
  CHKERRQ(ierr);
  ierr = KSPGetIterationNumber(ksp,&its);
  CHKERRQ(ierr);
  const char* solver_type = SolverType.c_str();
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Solver = %s \n", solver_type);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Elapse time: %lf s \n", t2 - t1);
  CHKERRQ(ierr);
  KSPConvergedReason reason;
  ierr = KSPGetConvergedReason(ksp, &reason);
  CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Converged reason: %s \n", KSPConvergedReasons[reason]);
  CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Number of iterations = %3d \n",its);
  CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD, "Residual norm %g \n",(double)norm);
  CHKERRQ(ierr);
  ierr = KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);
  CHKERRQ(ierr);

  /* Cleanup */
  ierr = KSPDestroy(&ksp);
  CHKERRQ(ierr);
  ierr = VecDestroy(&x);
  CHKERRQ(ierr);
  ierr = VecDestroy(&b);
  CHKERRQ(ierr);
  ierr = VecDestroy(&u);
  CHKERRQ(ierr);
  ierr = MatDestroy(&A);
  CHKERRQ(ierr);

  return ierr;
}

#endif
