//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _TEST_UTILITIES_HPP
#define _TEST_UTILITIES_HPP

// System includes
#include <iostream>
#include <string>
#include <vector>
#include <array>

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"
#include "Solver/linearProblem.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "Tools/table_interpolation.hpp"

#define FELISCE_FILE_DATA __FILENAME__, __FILE__

namespace felisce
{

namespace TestUtilities
{
  /**
   * @brief This method returns the folder of the current file
   * @details Requires as input FELISCE_FILE_DATA
   * @param rFilename The name of the file (just name)
   * @param rFullFilename The full path of the file
   * @return The path to the current file
   */
  std::string GetCurrentFolder(
    const std::string rFilename,
    const std::string rFullFilename
    );

  /**
   * @brief This method compares two files
   * @details It checks if the two files are equal
   * @param rFile1 The first file path
   * @param rFile1 The second file path
   */
  bool CompareTwoFiles(
    const std::string& rFile1,
    const std::string& rFile2
    );

  /**
   * @brief It generates a minimal mesh of one line
   * @param Delta The increment of coordinates
   * @return It returns a minimal mesh of one line
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshLine(const std::array<double, 3> Delta = {1.0,0.0,0.0});

  /**
   * @brief It generates a minimal mesh of one triangle
   * @return It returns a minimal mesh of one triangle
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTriangle();

  /**
   * @brief It generates a minimal mesh of one triangle (inverted)
   * @return It returns a minimal mesh of one triangle (inverted)
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshInvertedTriangle();

  /**
   * @brief It generates a minimal mesh of triangles
   * @return It returns a minimal mesh of triangles
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTriangles();

  /**
   * @brief It generates a minimal mesh of quadrilaterals
   * @return It returns a minimal mesh of quadrilaterals
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshQuadrilaterals();

  /**
   * @brief It generates a minimal mesh of Tetrahedron
   * @return It returns a minimal mesh of Tetrahedron
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTetrahedron();

  /**
   * @brief It generates a minimal mesh of inverted Tetrahedron
   * @return It returns a minimal mesh of inverted Tetrahedron
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshInvertedTetrahedron();

  /**
   * @brief It generates a minimal mesh of Tetrahedra
   * @return It returns a minimal mesh of Tetrahedra
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshTetrahedra();

  /**
   * @brief It generates a minimal mesh of Hexahedron
   * @return It returns a minimal mesh of Hexahedron
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshHexahedron();

  /**
   * @brief It generates a minimal mesh of inverted Hexahedron
   * @return It returns a minimal mesh of inverted Hexahedron
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshInvertedHexahedron();

  /**
   * @brief It generates a minimal mesh of Prism
   * @return It returns a minimal mesh of Prism
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshPrism();

  /**
   * @brief It generates a minimal mesh of Prism9
   * @return It returns a minimal mesh of Prism9
   */
  felisce::GeometricMeshRegion::Pointer GenerateMinimalMeshPrism9();

  /**
   * @brief Generate FE
   * @param eltType The type of element considered
   * @param rDegreeOfExactnessVector The vector containing the degree of exactness
   * @param Delta The increment of coordinates
   * @tparam TFEType The type of FE type
   * @return It returns a minimal FE
   */
  template<class TFEType>
  typename TFEType::Pointer GenerateFiniteElement(
    const felisce::GeometricMeshRegion::ElementType eltType,
    const std::vector<felisce::DegreeOfExactness>& rDegreeOfExactnessVector,
    const std::array<double, 3> Delta = {1.0,0.0,0.0}
    );

  /**
   * @brief Generate FE
   * @param eltType The type of element considered
   * @param rDegreeOfExactnessVector The vector containing the degree of exactness
   * @param Delta The increment of coordinates
   * @tparam TFEType The type of FE type
   * @return It returns a minimal FE
   */
  template<class TFEType>
  typename TFEType::Pointer GenerateFiniteElement(
    const felisce::GeometricMeshRegion::ElementType eltType,
    const felisce::DegreeOfExactness DegreeOfExactnessVector = felisce::DegreeOfExactness::DegreeOfExactness_1,
    const std::array<double, 3> Delta = {1.0,0.0,0.0}
    );

  /**
   * @brief Generate an element vector
   * @param eltType The type of element considered
   * @param NumberOfComponents Of the variable considered
   * @return It returns an element vector
   */
  felisce::ElementVector GenerateElementVector(
    const felisce::GeometricMeshRegion::ElementType eltType,
    const std::size_t NumberOfComponents = 1
    );

  /**
   * @brief Generate an element vector
   * @param eltType The type of element considered
   * @param NumberOfComponents Of the variable considered
   * @return It returns an element matrix
   */
  felisce::ElementMatrix GenerateElementMatrix(
    const felisce::GeometricMeshRegion::ElementType eltType,
    const std::size_t NumberOfComponents = 1
    );

  /**
   * @brief Generate an element field
   * @param rElement The element considered
   * @param NumberOfComponents Of the variable considered
   * @param FieldType The field type
   * @param Value The value of the field
   * @tparam TFEType The type of FE type
   * @return It returns an element field
   */
  template<class TFEType>
  felisce::ElementField GenerateElementField(
    const TFEType& rElement,
    const std::size_t NumberOfComponents = 1,
    const ElementFieldType FieldType = CONSTANT_FIELD,
    const double Value = 1.0
    );

  /**
   * @brief Retrieves a solution table
   * @param rLinearProblem The linear problem with the solution
   * @param FileType The file type considered
   */
  void RetrieveSolutionTable(
    felisce::LinearProblem& rLinearProblem,
    const std::string FileType = "TXT"
    );

  /**
   * @brief Fills several TableInterpolation related with different dofs
   * @details The X value is the given time step
   * @param rLinearProblem The linear problem with the solution
   * @param rTables The tables containing the solution tracked
   */
  void FillTablesResults(
    felisce::LinearProblem& rLinearProblem,
    std::unordered_map<int, felisce::TableInterpolation>& rTables
    );

  /**
   * @brief Fills solution of the linear problem with the given solution
   * @param rLinearProblem The linear problem with the solution
   * @param rSolution The vector containing the solution tracked
   */
  void ImposeSolution(
    felisce::LinearProblem& rLinearProblem,
    const std::vector<double>& rSolution
    );

  /**
   * @brief Retrieves a solution to file
   * @param rLinearProblem The linear problem with the solution
   * @param rAppendixName The appendix name
   * @param rPrecision The precision
   * @param EvaluationState If returning the evaluation state or the solution
   */
  void SaveSolutionToFile(
    felisce::LinearProblem& rLinearProblem,
    const std::string& rAppendixName = "",
    const std::size_t Precision = 16,
    const bool EvaluationState = false
    );

  /**
   * @brief Retrieves a RHS to file
   * @param rLinearProblem The linear problem with the RHS
   * @param rAppendixName The appendix name
   * @param rPrecision The precision
   */
  void SaveRHSToFile(
    felisce::LinearProblem& rLinearProblem,
    const std::string& rAppendixName = "",
    const std::size_t Precision = 16
    );

  /**
   * @brief This method retrieves a solution vector from a solution file
   * @param rFileName The name of the file
   * @return A vector containing the solution of the file
   */
  std::vector<double> GetSolutionFromFile(const std::string& rFileName);
}

}

#endif
