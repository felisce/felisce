//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _CONTOUR_UTILITIES_HPP
#define _CONTOUR_UTILITIES_HPP

// System includes
#include <unordered_map>

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce
{

namespace ContourUtilities
{

  // Defining the number of nodes of the geometries
  static std::unordered_map<GeometricMeshRegion::ElementType, std::size_t> NumberOfNodesGeometries =
  {
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Tria3,  3),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Tetra4, 4),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Quad4,  4),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Quad6,  6),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Hexa8,  8),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Hexa12,12),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Prism6, 6),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Prism9, 9)
  };

  // Defining the number of faces of the geometries
  static std::unordered_map<GeometricMeshRegion::ElementType, std::size_t> NumberOfContours =
  {
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Tria3,  3),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Quad4,  4),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Tetra4, 4),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Hexa8,  6),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Hexa12, 6),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Prism6, 5),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Prism9, 5)
  };

  // Defining the faces connectivity
  static std::unordered_map<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>> ContourTypes =
  {
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Tria3,  std::vector<GeometricMeshRegion::ElementType>(3,GeometricMeshRegion::Seg2)),
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Quad4,  std::vector<GeometricMeshRegion::ElementType>(4,GeometricMeshRegion::Seg2)),
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Tetra4, std::vector<GeometricMeshRegion::ElementType>(4,GeometricMeshRegion::Tria3)),
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Hexa8,  std::vector<GeometricMeshRegion::ElementType>(8,GeometricMeshRegion::Quad4)),
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Hexa12, {GeometricMeshRegion::Quad4,GeometricMeshRegion::Quad4,GeometricMeshRegion::Quad6,GeometricMeshRegion::Quad6,GeometricMeshRegion::Quad6,GeometricMeshRegion::Quad6}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Prism6, {GeometricMeshRegion::Tria3,GeometricMeshRegion::Tria3,GeometricMeshRegion::Quad4,GeometricMeshRegion::Quad4,GeometricMeshRegion::Quad4}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<GeometricMeshRegion::ElementType>>(GeometricMeshRegion::Prism9, {GeometricMeshRegion::Tria3,GeometricMeshRegion::Tria3,GeometricMeshRegion::Quad6,GeometricMeshRegion::Quad6,GeometricMeshRegion::Quad6})
  };

  // Defining the faces connectivity // TODO: Use the database already existing (maybe same class, but call the database)
  static std::unordered_map<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>> ContourConnectivities =
  {
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Tria3,  {{0,1},{1,2},{2,0}}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Quad4,  {{0,1},{1,2},{2,3},{3,0}}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Tetra4, {{2,3,1},{0,3,2},{0,1,3},{0,2,1}}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Hexa8,  {{3,2,1,0},{0,1,5,4},{2,6,5,1},{2,3,7,6},{3,0,4,7},{4,5,6,7}}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Hexa12, {{0,3,2,1},{4,5,6,7},{0,1,5,4,9,8},{1,2,6,5,10,9},{2,3,7,6,11,10},{3,0,4,7,8,11}}),    
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Prism6, {{0,2,1},{3,4,5},{1,2,5,4},{0,3,5,2},{0,1,4,3}}),
    std::pair<GeometricMeshRegion::ElementType, std::vector<std::vector<felInt>>>(GeometricMeshRegion::Prism9, {{0,2,1},{3,4,5},{1,2,5,4,8,7},{2,0,3,5,6,8},{0,1,4,3,7,6}})
  };

  /**
   * @brief It generates a contour mesh in a given mesh
   * @param rMesh The given mesh to generate the contour
   * @param ImposeParentReferences If we impose the references of the parent elements
   */
  void GenerateContourMesh(
    GeometricMeshRegion& rMesh,
    const bool ImposeParentReferences = false
    );

  /**
   * @brief It generates a contour mesh in a given mesh
   * @param rMesh The given mesh to generate the contour
   * @param rManualRefListNodes Manual list of references for nodes
   * @param ImposeManualReferences If we impose those references diectly
   */
  void GenerateContourMesh(
    GeometricMeshRegion& rMesh,
    const std::unordered_map<felInt,felInt>& rManualRefListNodes,
    const bool ImposeManualReferences = false
    );

  /**
   * @brief It generates a contour mesh in a given mesh for a given element type
   * @param rMesh The given mesh to generate the contour
   * @param rElementType The element type
   * @param rManualRefListNodes Manual list of references for nodes
   */
  void GenerateContourMeshGivenGeometry(
    GeometricMeshRegion& rMesh,
    const GeometricMeshRegion::ElementType& rElementType,
    int& rRef,
    const std::unordered_map<felInt,felInt>& rManualRefListNodes
    );
}

}

#endif
