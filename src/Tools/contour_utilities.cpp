//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <vector>
#include <set>
#include <unordered_set>

// External includes

// Project includes
#include "Core/key_hash.hpp"
#include "Core/felisceParam.hpp"
#include "Tools/fe_utilities.hpp"
#include "Tools/contour_utilities.hpp"

namespace felisce
{

namespace ContourUtilities
{

void GenerateContourMesh(
  GeometricMeshRegion& rMesh,
  const bool ImposeParentReferences
  )
{
  std::unordered_map<felInt,felInt> auxiliar_list;

  // Filling with the parent references
  if (ImposeParentReferences) {
    felInt startindex = 0, numElemsPerRef = 0;
    int the_ref = 0;
    bool incr_vert_id = false;
    std::vector<felInt> elem;
    std::vector<GeometricMeshRegion::ElementType> eltType_list({GeometricMeshRegion::Tria3, GeometricMeshRegion::Quad4, GeometricMeshRegion::Tetra4, GeometricMeshRegion::Hexa8, GeometricMeshRegion::Prism6, GeometricMeshRegion::Prism9});
    for (auto& eltType : eltType_list) {
      elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0);
      for ( auto it_ref = rMesh.intRefToBegEndMaps[eltType].begin();it_ref != rMesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
        the_ref = it_ref->first;
        startindex     =  it_ref->second.first;
        numElemsPerRef =  it_ref->second.second;
        for(felInt iel = 0; iel < numElemsPerRef; iel++) {
          rMesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
          for (auto r_id : elem) {
            auxiliar_list[r_id] = the_ref;
          }
        }
      }
    }
  }
  GenerateContourMesh(rMesh, auxiliar_list, ImposeParentReferences);
}

/***********************************************************************************/
/***********************************************************************************/

void GenerateContourMesh(
  GeometricMeshRegion& rMesh,
  const std::unordered_map<felInt,felInt>& rManualRefListNodes,
  const bool ImposeManualReferences
  )
{
  // The initial reference
  int ref = ImposeManualReferences ? 0 : 100;

  /**
   * The following geometries are supported to generate a skin. Extend then to support new geometries:
   * - Triangle (P1)
   * - Quadrilateral (P1)
   * - Tetrahedra (P1)
   * - Hexahedra (P1 and P1-P2, 12 nodes)
   * - Prism (P1 and P1-P2, 9 nodes)
   */
  // P1 triangle
  if (rMesh.numElements(GeometricMeshRegion::Tria3) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Tria3, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // P1 quadrilateral
  if (rMesh.numElements(GeometricMeshRegion::Quad4) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Quad4, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // P1 tetrahedra
  if (rMesh.numElements(GeometricMeshRegion::Tetra4) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Tetra4, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // P1 hexahedra
  if (rMesh.numElements(GeometricMeshRegion::Hexa8) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Hexa8, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // P1-P2 hexahedra
  if (rMesh.numElements(GeometricMeshRegion::Hexa12) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Hexa12, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // P1 prisms
  if (rMesh.numElements(GeometricMeshRegion::Prism6) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Prism6, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // P1-P2 prisms
  if (rMesh.numElements(GeometricMeshRegion::Prism9) > 0) {
    GenerateContourMeshGivenGeometry(rMesh, GeometricMeshRegion::Prism9, ref, rManualRefListNodes);
    if(!ImposeManualReferences) ++ref;
  }

  // Check if the mesh is inverted
  if (FelisceParam::instance().checkVolumeOrientation) {
    if (FEUtilities::CheckVolumeIsInverted(rMesh)) {
      FEL_ERROR("Mesh is inverted. Check connectivity");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GenerateContourMeshGivenGeometry(
  GeometricMeshRegion& rMesh,
  const GeometricMeshRegion::ElementType& rElementType,
  int& rRef,
  const std::unordered_map<felInt,felInt>& rManualRefListNodes
  )
{
  /// Definition of the vector indexes considered
  typedef std::vector<felInt> VectorIndexType;

  /// Definition of the hasher considered
  typedef VectorIndexHasher<VectorIndexType> VectorIndexHasherType;

  /// Definition of the key comparor considered
  typedef VectorIndexComparor<VectorIndexType> VectorIndexComparorType;

  /// Definition of the ordered key comparor considered
  typedef VectorIndexComparorLess<VectorIndexType> VectorIndexComparorLessType;

  /// Define the set considered for element pointers
  typedef std::unordered_set<VectorIndexType, VectorIndexHasherType, VectorIndexComparorType > HashSetVectorIntType;

  /// Define the map considered for face ids
  typedef std::map<VectorIndexType, VectorIndexType, VectorIndexComparorLessType > HashMapVectorIntType;

  /// Define the map considered to create contour
  typedef std::unordered_map<VectorIndexType, GeometricMeshRegion::ElementType, VectorIndexHasherType, VectorIndexComparorType > ContourMapType;

  /// Define the map considered for face types counter
  std::unordered_map<GeometricMeshRegion::ElementType, std::size_t> geometry_counter = {
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Seg2,   0),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Tria3,  0),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Quad4,  0),
    std::pair<GeometricMeshRegion::ElementType, std::size_t>(GeometricMeshRegion::Quad6,  0)
  };

  // Defining references
  auto& r_int_ref_to_enum = rMesh.intRefToEnum();

  // Definition of the inverse map
  HashMapVectorIntType inverse_face_map;
  ContourMapType contour_map;

  const std::size_t number_of_nodes_element = NumberOfNodesGeometries[rElementType];
  const std::size_t potential_number_neighbours = NumberOfContours[rElementType];
  const auto& r_boundary_geometries = ContourConnectivities[rElementType];
  const auto& r_boundary_types = ContourTypes[rElementType];
  const auto number_of_elements = rMesh.numElements(rElementType);
  std::vector<felInt> origin_elem(number_of_nodes_element, 0);
  for (int i_elem = 0; i_elem < number_of_elements; ++i_elem) {
    rMesh.getOneElement(rElementType, i_elem, origin_elem, false);

    for (std::size_t i_face = 0; i_face < potential_number_neighbours; ++i_face) {
      /* FACES/EDGES */
      const std::size_t number_nodes = r_boundary_geometries[i_face].size();
      VectorIndexType vector_ids(number_nodes);
      VectorIndexType ordered_vector_ids(number_nodes);

      /* FACE/EDGE */
      for (std::size_t i_node = 0; i_node < number_nodes; ++i_node) {
        vector_ids[i_node] = origin_elem[r_boundary_geometries[i_face][i_node]];
        ordered_vector_ids[i_node] = vector_ids[i_node];
      }

      /*** THE ARRAY OF IDS MUST BE ORDERED!!! ***/
      std::sort(vector_ids.begin(), vector_ids.end());
      // Check if the elements already exist in the HashMapVectorIntType
      auto it_check = inverse_face_map.find(vector_ids);

      if(it_check == inverse_face_map.end() ) {
        // If it doesn't exist it is added to the database
        inverse_face_map.insert(std::pair<VectorIndexType, VectorIndexType>(vector_ids, ordered_vector_ids));
        contour_map.insert(std::pair<VectorIndexType, GeometricMeshRegion::ElementType>(vector_ids, r_boundary_types[i_face]));
        geometry_counter[r_boundary_types[i_face]] += 1;
      }
    }
  }

  // Create the face_set
  HashSetVectorIntType face_set;

  for (int i_elem = 0; i_elem < number_of_elements; ++i_elem) {
    rMesh.getOneElement(rElementType, i_elem, origin_elem, false);
    for (std::size_t i_face = 0; i_face < potential_number_neighbours; ++i_face) {

      /* FACES/EDGES */
      const std::size_t number_nodes = r_boundary_geometries[i_face].size();
      VectorIndexType vector_ids(number_nodes);

      /* FACE/EDGE */
      for (std::size_t i_node = 0; i_node < number_nodes; ++i_node) {
        vector_ids[i_node] = origin_elem[r_boundary_geometries[i_face][i_node]];
      }

      /*** THE ARRAY OF IDS MUST BE ORDERED!!! ***/
      std::sort(vector_ids.begin(), vector_ids.end());
      // Check if the elements already exist in the HashSetVectorIntType
      auto it_check = face_set.find(vector_ids);

      if(it_check != face_set.end() ) {
        // If it exists we remove from the inverse map
        inverse_face_map.erase(vector_ids);
        contour_map.erase(vector_ids);
        geometry_counter[r_boundary_types[i_face]] -= 1;
      } else {
        // If it doesn't exist it is added to the database
        face_set.insert(vector_ids);
      }
    }
  }

  // Allocate contour
  std::size_t counter_check = 0;
  for (auto& r_counter : geometry_counter) {
    if (r_counter.second > 0) {
      counter_check += r_counter.second;
      rMesh.numElements(r_counter.first) = r_counter.second;
      rMesh.allocateElements(r_counter.first);
    }
  }

  // Check
  if (counter_check != inverse_face_map.size()) {
    std::cout << "WARNING: Inconsistent clean up of unused BC" << std::endl;
  }

  // Generate contour
  std::unordered_map<GeometricMeshRegion::ElementType, felInt> contour_counter = {
    std::pair<GeometricMeshRegion::ElementType, felInt>(GeometricMeshRegion::Seg2,   0),
    std::pair<GeometricMeshRegion::ElementType, felInt>(GeometricMeshRegion::Tria3,  0),
    std::pair<GeometricMeshRegion::ElementType, felInt>(GeometricMeshRegion::Quad4,  0),
    std::pair<GeometricMeshRegion::ElementType, felInt>(GeometricMeshRegion::Quad6,  0)
  };
  std::unordered_map<GeometricMeshRegion::ElementType, std::map<int,std::vector<felInt>>> list_elements = {
    std::pair<GeometricMeshRegion::ElementType, std::map<int,std::vector<felInt>>>(GeometricMeshRegion::Seg2,   {}),
    std::pair<GeometricMeshRegion::ElementType, std::map<int,std::vector<felInt>>>(GeometricMeshRegion::Tria3,  {}),
    std::pair<GeometricMeshRegion::ElementType, std::map<int,std::vector<felInt>>>(GeometricMeshRegion::Quad4,  {}),
    std::pair<GeometricMeshRegion::ElementType, std::map<int,std::vector<felInt>>>(GeometricMeshRegion::Quad6,  {})
  };
  // Default reference
  if (rManualRefListNodes.size() == 0) {
    for (auto& r_countour_index : inverse_face_map) {
      const auto& r_connectivity = r_countour_index.second;
      const auto type_contour = contour_map[r_countour_index.first];
      auto& r_counter = contour_counter[type_contour];
      rMesh.setOneElement(type_contour, r_counter, r_connectivity, false);
      ++r_counter;
    }
  } else { // Manual references
    for (auto& r_component : list_elements) {
      auto& r_map = r_component.second;
      r_map.insert(std::pair<int, std::vector<felInt>>(rRef, {}));
      for (auto& r_manual : rManualRefListNodes) {
        if (r_map.find(rRef + r_manual.second) == r_map.end()) {
          r_map.insert(std::pair<int, std::vector<felInt>>(rRef + r_manual.second, {}));
        }
      }
    }
    std::unordered_map<GeometricMeshRegion::ElementType, std::unordered_map<int,std::set<felInt>>> auxiliar_list_elements = {
      std::pair<GeometricMeshRegion::ElementType, std::unordered_map<int,std::set<felInt>>>(GeometricMeshRegion::Seg2,   {}),
      std::pair<GeometricMeshRegion::ElementType, std::unordered_map<int,std::set<felInt>>>(GeometricMeshRegion::Tria3,  {}),
      std::pair<GeometricMeshRegion::ElementType, std::unordered_map<int,std::set<felInt>>>(GeometricMeshRegion::Quad4,  {}),
      std::pair<GeometricMeshRegion::ElementType, std::unordered_map<int,std::set<felInt>>>(GeometricMeshRegion::Quad6,  {})
    };
    for (auto& r_component : auxiliar_list_elements) {
      auto& r_map = r_component.second;
      r_map.insert(std::pair<int, std::set<felInt>>(rRef, {}));
      for (auto& r_manual : rManualRefListNodes) {
        if (r_map.find(rRef + r_manual.second) == r_map.end()) {
          r_map.insert(std::pair<int, std::set<felInt>>(rRef + r_manual.second, {}));
        }
      }
    }
    for (auto& r_countour_index : inverse_face_map) {
      const auto& r_connectivity = r_countour_index.second;
      const auto type_contour = contour_map[r_countour_index.first];
      auto& r_counter = contour_counter[type_contour];
      rMesh.setOneElement(type_contour, r_counter, r_connectivity, false);
      auto& r_list_elements = auxiliar_list_elements[type_contour];
      std::unordered_map<int, std::size_t> node_counter;
      for (auto& r_manual : rManualRefListNodes) {
        node_counter.insert(std::pair<int, std::size_t>(rRef + r_manual.second, 0));
      }

      for (auto& r_id : r_connectivity) {
        auto find_id = rManualRefListNodes.find(r_id);
        if (find_id == rManualRefListNodes.end()) { // If one node is not in the manual list assigned to the default ref
          r_list_elements[rRef].insert(r_counter);
          break;
        } else {
          ++node_counter[rRef + find_id->second];
        }
      }
      // Adding the corresponding ref if all nodes are manually set
      bool added = false;
      for (auto& r_count : node_counter) {
        if (r_count.second == r_connectivity.size()) {
          r_list_elements[r_count.first].insert(r_counter);
          added = true;
        }
      }
      // Otherwise added to default
      if (!added) {
        r_list_elements[rRef].insert(r_counter);
      }
      ++r_counter;
    }
    // Copy the auxiliary list into our target list
    for (auto& r_element_types : std::vector<GeometricMeshRegion::ElementType>({GeometricMeshRegion::Seg2, GeometricMeshRegion::Tria3, GeometricMeshRegion::Quad4, GeometricMeshRegion::Quad6})) {
      auto& r_map = list_elements[r_element_types];
      auto& r_auxiliar_map = auxiliar_list_elements[r_element_types];
      for (auto& r_component : r_auxiliar_map) {
        auto& r_list = r_map[r_component.first];
        auto& r_auxiliar_list = r_component.second;
        r_list.resize(r_auxiliar_list.size());
        std::size_t counter = 0;
        for (auto& r_id : r_auxiliar_list) {
          r_list[counter] = r_id;
          ++counter;
        }
      }
    }
  }

  // Last calls
  for (auto& r_counter : geometry_counter) {
    if (r_counter.second > 0) {
      // Reference list
      std::map<int,std::vector<felInt>>& r_list_elements = list_elements[r_counter.first];

      // Generate references
      if (rManualRefListNodes.size() == 0) {
        r_list_elements =
        {
            std::pair<int,std::vector<felInt>>(rRef, std::vector<felInt>(r_counter.second, 0))
        };
        auto& r_vector = r_list_elements[rRef];
        for (std::size_t i = 1; i < r_vector.size(); ++i) {
          r_vector[i] = i;
        }
      }

      // Adding references
      for (auto& r_id : r_list_elements) {
        if (r_id.second.size() > 0) {
          r_int_ref_to_enum[r_id.first].insert(r_counter.first);
        }
      }

      // Calling API
      rMesh.reorderListElePerRef(r_list_elements, r_counter.first);
    }
  }

  rMesh.setBagElementTypeDomain();
  rMesh.setBagElementTypeDomainBoundary();
}
} // namespace ContourUtilities

} // namespace felisce
