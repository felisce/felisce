//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _FE_UTILITIES_HPP
#define _FE_UTILITIES_HPP

// System includes
#include <vector>
#include <functional>

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce
{

namespace FEUtilities
{
    /**
     * @brief This method checks if the mesh is inverted
     * @param rMesh The given mesh to generate to check
     * @param rBagElement The element type to be considered
     * @param pFunction The function to call for the element loop
     * @tparam TFunction The type of function of pFunction
     * @param pFunctionPreFE The function to call before the element loop
     * @tparam TFunctionPreFE The type of function of pFunctionPreFE
     * @param pFunctionPostFE The function to call after the element loop
     * @tparam TFunctionPostFE The type of function of pFunctionPostFE
     * @todo This function in the future can be adapted for STL loops
     */
    template<
      typename TFunction, 
      typename TFunctionPreFE = std::function<void(const GeometricMeshRegion::ElementType&)>, 
      typename TFunctionPostFE = std::function<void(const GeometricMeshRegion::ElementType&)>
      >
    inline double LoopOverElements(
        GeometricMeshRegion& rMesh,
        const std::vector<GeometricMeshRegion::ElementType>& rBagElement,
        const TFunction* pFunction,
        const TFunctionPreFE* pFunctionPreFE = nullptr,
        const TFunctionPostFE* pFunctionPostFE = nullptr
        )
    {
      // Auxiliary double
      double auxiliary_double = 0.0;

      // Assemblyloop
      GeometricMeshRegion::ElementType eltType; // Geometric element type in the mesh.
      int numPointPerElt = 0;                   // Number of points per geometric element.
      felInt numEltPerLabel = 0;                // Number of element for one label and one eltType.

      // Use to define a "global" numbering of element in the mesh.
      felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
      for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
        numElement[static_cast<GeometricMeshRegion::ElementType>(ityp)] = 0;
      }

      std::vector<Point*> elemPoint;
      std::vector<felInt> elemIdPoint;
      felInt ielSupportDof;

      // Assembly loop.
      // First loop on geometric type.
      for (std::size_t i = 0; i < rBagElement.size(); ++i) {
        eltType =  rBagElement[i];

        // Resize array.
        numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
        elemPoint.resize(numPointPerElt, nullptr);
        elemIdPoint.resize(numPointPerElt, 0);

        // Allocate array use for assemble with PETSc
        if (pFunctionPreFE) {
          (*pFunctionPreFE)(eltType);
        }

        // Second loop on region of the mesh.
        for(auto itRef = rMesh.intRefToBegEndMaps[eltType].begin(); itRef != rMesh.intRefToBegEndMaps[eltType].end(); ++itRef) {
          numEltPerLabel = itRef->second.second;

          // Third loop on element in the region with the type: eltType. ("real" loop on elements).
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            auxiliary_double += (*pFunction)(eltType, numElement[eltType], elemPoint, elemIdPoint, ielSupportDof);
            numElement[eltType]++;
          }
        }
        // Desallocate array use for assemble with PETSc
        if (pFunctionPostFE) {
          (*pFunctionPostFE)(eltType);
        }
      } 

      return auxiliary_double;   
    }

    /**
     * @brief This method checks if the mesh is inverted
     * @param rMesh The given mesh to generate to check
     * @param jacobianCheck If the check is to be done considering the jacobian or the triple product check
     */
    bool CheckVolumeIsInverted(
      GeometricMeshRegion& rMesh,
      const bool jacobianCheck = false
      );
}

}

#endif
