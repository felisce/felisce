//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _SIMPLE_CMD_HPP
#define _SIMPLE_CMD_HPP

// System includes
#include <functional>   // std::function
#include <iostream>     // std::cout, std::endl
#include <map>          // std::map
#include <memory>       // std::unique_ptr
#include <string>       // std::string
#include <sstream>      // std::stringstream
#include <string_view>  // std::string_view
#include <variant>      // std::variant
#include <vector>       // std::vector

// External includes

// Project includes

/**
 * @brief This class provides a simplified CMD interface
 * @author Vicente Mataix Ferrandiz
 */
template <class Opts>
struct CmdOptions : Opts
{
  using Property = std::variant<std::string Opts::*, int Opts::*, double Opts::*, bool Opts::*>;
  using Argument = std::pair<std::string, Property>;

  ~CmdOptions() = default;

  Opts parse(int argc, const char* argv[])
  {
    std::vector<std::string_view> vargv(argv, argv+argc);
    for (int idx = 0; idx < argc; ++idx)
      for (auto& cbk : callbacks)
        cbk.second(idx, vargv);

    return static_cast<Opts>(*this);
  }

  static std::unique_ptr<CmdOptions> Create(std::initializer_list<Argument> args)
  {
    auto cmdOpts = std::unique_ptr<CmdOptions>(new CmdOptions());
    for (auto arg : args) cmdOpts->register_callback(arg);
    return cmdOpts;
  }

private:
  using callback_t = std::function<void(int, const std::vector<std::string_view>&)>;
  std::map<std::string, callback_t> callbacks;

  CmdOptions() = default;
  CmdOptions(const CmdOptions&) = delete;
  CmdOptions(CmdOptions&&) = delete;
  CmdOptions& operator=(const CmdOptions&) = delete;
  CmdOptions& operator=(CmdOptions&&) = delete;

  auto register_callback(std::string name, Property prop)
  {
    callbacks[name] = [this, name, prop](int idx, const std::vector<std::string_view>& argv) {
      if (argv[idx] == name) {
        visit(
          [this, idx, &argv](auto&& arg) {
            if (idx < static_cast<int>(argv.size()) - 1) {
              std::stringstream value;
              value << argv[idx+1];
              value >> this->*arg;
            }
          },
          prop);
      }
    };
  };

  auto register_callback(Argument p) { return register_callback(p.first, p.second); }
};

#endif
