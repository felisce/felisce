//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <unordered_set>
#include <unordered_map>

// External includes

// Project includes
#include "Tools/fe_utilities.hpp"
#include "FiniteElement/refElement.hpp"
#include "FiniteElement/currentFiniteElement.hpp"

namespace felisce
{

namespace FEUtilities
{

// List of the FE triple product compatible elements
static std::unordered_set<GeometricMeshRegion::ElementType> compatible_triple_product_fe = 
{
  GeometricMeshRegion::Tetra4,
  GeometricMeshRegion::Hexa8
};
static std::unordered_map<GeometricMeshRegion::ElementType, std::vector<std::array<int, 4>>> vertex_tetrahedra = 
{
  {GeometricMeshRegion::Tetra4, {{0,1,2,3}}},
  {GeometricMeshRegion::Hexa8, { // http://www.math.udel.edu/~szhang/research/p/subtettest.pdf
                                {0,1,2,6}, {1,2,3,7}, {2,3,0,4}, {3,0,1,5}, {4,7,6,2}, {7,6,5,1},
                                {6,5,4,0}, {5,4,7,3}, {1,0,4,7}, {3,2,6,5}, {0,3,7,6}, {2,1,5,4},
                                {0,1,2,4}, {1,2,3,5}, {2,3,0,6}, {3,0,1,7}, {4,7,6,0}, {7,6,5,3},
                                {6,5,4,2}, {5,4,7,1}, {4,0,3,5}, {2,6,7,1}, {3,7,4,2}, {1,5,6,0}, 
                                {0,1,3,4}, {1,2,0,5}, {2,3,1,6}, {3,0,2,7}, {4,7,5,0}, {5,4,6,1}, {6,5,7,2}, {7,6,4,3}
                                }}  
};

/***********************************************************************************/
/***********************************************************************************/

double volumeTetrahedra(const std::vector<Point*>& elemPoint, const std::vector<std::array<int, 4>>& vertexTetrahedra)
{
  // Auxiliary double
  double auxiliary_double = 0.0;

  // Loop over the tetrahedra
  for(std::size_t i = 0; i < vertexTetrahedra.size(); ++i) {
    // Get the vertices
    const auto& r_array = vertexTetrahedra[i];
    const Point* p0 = elemPoint[r_array[0]];
    const Point* p1 = elemPoint[r_array[1]];
    const Point* p2 = elemPoint[r_array[2]];
    const Point* p3 = elemPoint[r_array[3]];

    const double x10 = p1->x() - p0->x();
    const double y10 = p1->y() - p0->y();
    const double z10 = p1->z() - p0->z();

    const double x20 = p2->x() - p0->x();
    const double y20 = p2->y() - p0->y();
    const double z20 = p2->z() - p0->z();

    const double x30 = p3->x() - p0->x();
    const double y30 = p3->y() - p0->y();
    const double z30 = p3->z() - p0->z();

    // Compute the volume
    const double volume = (x10 * y20 * z30 - x10 * y30 * z20 + y10 * z20 * x30 - y10 * x20 * z30 + z10 * x20 * y30 - z10 * y20 * x30)/6.0;
    if (volume < 0.0) return -1.0;
    auxiliary_double += volume;
  }

  // Return the volume
  return auxiliary_double;
}

/***********************************************************************************/
/***********************************************************************************/

double volumeFE(GeometricMeshRegion& rMesh, GeometricMeshRegion::ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt& ielSupportDof)
{
  IGNORE_UNUSED_ARGUMENT(ielSupportDof);
  // Generate element from previous mesh
  const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  const RefElement* refEle = geoEle->defineFiniteEle(eltType, 0, rMesh);
  std::vector<DegreeOfExactness> degree_of_exactness(1, DegreeOfExactness::DegreeOfExactness_1);
  auto element = CurrentFiniteElement(*refEle,*geoEle, degree_of_exactness);

  rMesh.getOneElement(eltType, iel, elemIdPoint, elemPoint);

  // Compute integration points
  element.updateMeas(0, elemPoint);

  return element.measure();
}

/***********************************************************************************/
/***********************************************************************************/

bool CheckVolumeIsInverted(
  GeometricMeshRegion& rMesh,
  const bool jacobianCheck
  )
{
  /* Loop function */
  // Execute the loop
  std::unordered_set<GeometricMeshRegion::ElementType> aux_bag_set;
  const auto& elements_bag = rMesh.bagElementTypeDomain();
  std::copy(elements_bag.begin(), elements_bag.end(), std::inserter(aux_bag_set, aux_bag_set.begin()));
  if (!jacobianCheck) {
    for (auto& compatible_eltType : compatible_triple_product_fe) {
      const int num_elem = rMesh.numElements(compatible_eltType);
      if (num_elem > 0) {
        aux_bag_set.erase(compatible_eltType);
        const std::vector<GeometricMeshRegion::ElementType> aux_bag(1, compatible_eltType);
        const auto& connectivity = vertex_tetrahedra[compatible_eltType];
        auto function_triple_product = [&rMesh,&connectivity](GeometricMeshRegion::ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt& ielSupportDof) {
          IGNORE_UNUSED_ARGUMENT(ielSupportDof);
          rMesh.getOneElement(eltType, iel, elemIdPoint, elemPoint);
          const double volume = volumeTetrahedra(elemPoint, connectivity);
          if (volume < 0.0) {
            std::cout << "WARNING:: The element " << iel << ", which type is " << eltType << " is inverted. Check connectivity" << std::endl;
            return 1.0;
          } else {
            return 0.0;
          }
        };
        const double aux = FEUtilities::LoopOverElements(rMesh, aux_bag, &function_triple_product);
        if (aux > 0.0) {
          return true;
        }
      }
    }
  }
  std::vector<GeometricMeshRegion::ElementType> effective_elements_bag;
  std::copy(aux_bag_set.begin(), aux_bag_set.end(), std::back_inserter(effective_elements_bag));

  // Generic jacobian check
  /* Loop function */
  auto function_jacobian = [&rMesh](GeometricMeshRegion::ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt& ielSupportDof) {
    const double volume = volumeFE(rMesh, eltType, iel, elemPoint, elemIdPoint, ielSupportDof);
    if (volume < 0.0) {
      std::cout << "WARNING:: The element " << iel << ", which type is " << eltType << " is inverted. Check connectivity" << std::endl;
      return 1.0;
    } else {
      return 0.0;
    }
  };
  const double aux = FEUtilities::LoopOverElements(rMesh, effective_elements_bag, &function_jacobian);
  if (aux > 0.0) {
    return true;
  } else {
    return false;
  }
}

} // namespace FEUtilities

} // namespace felisce
