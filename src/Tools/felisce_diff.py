#!/usr/bin/env python
import sys,os

sys.path.insert(0, os.path.join( os.path.dirname(__file__), '../Python/' ) )
import pytools as pt

def parse_command_line():

    if len(sys.argv) != 3:
        sys.stderr.write('Error: wrong number of arguments')
        sys.stderr.write('Usage: felisce_diff.py FILE1 FILE2')
        sys.exit(1)

    filename1 = sys.argv[1]
    filename2 = sys.argv[2]

    return filename1, filename2

if __name__ == '__main__':

    filename1, filename2 = parse_command_line()

    array1 = pt.reader.read_ensight_file(filename1)
    array2 = pt.reader.read_ensight_file(filename2)

    rtol = 1.E-05
    #array_comparer = pt.compare_array.arrays_almost_equal_simple_symmetric
    array_comparer = pt.compare_array.discrete_l2_norm
    #array_comparer = pt.compare_array.infinity_norm

    pt.compare_array.assertArrayAlmostEqual(array1,array2,array_comparer,args=[rtol])

    print "Tests passed successfully"
