#!/usr/bin/env python
import sys
import os
import datetime
import random
import glob

sys.path.append( os.path.join( os.path.dirname(__file__), 'thirdparty') )
import argparse

import numpy as np

def set_figure_size(fig,
    ax_width_cm  =  8 ,
    ax_height_cm =  8 ,
    left_mm      =  10,
    right_mm     =  10,
    top_mm       =  10,
    bottom_mm    =  10,
    ):

    #unity
    inches_per_cm = 0.393701
    inches_per_mm = 0.0393701

    #distances in inches of the axe to size figure sides
    left = left_mm *inches_per_mm
    right = right_mm *inches_per_mm
    top = top_mm *inches_per_mm
    bottom = bottom_mm *inches_per_mm

    #axe size in inches
    ax_width = ax_width_cm *inches_per_cm
    ax_height = ax_height_cm *inches_per_cm

    #entiere figure size in inches
    fig_width =  left + ax_width + right
    fig_height =  bottom + ax_height + top
    fig_size =  [fig_width,fig_height]

    #axe size ratio
    left_ratio = left / fig_width
    bottom_ratio = bottom / fig_height

    ax_width_ratio = ax_width / fig_width
    ax_height_ratio = ax_height / fig_height

    fig.set_size_inches(fig_size)

    ax = fig.axes[0]
    ax.set_position([left_ratio, bottom_ratio, ax_width_ratio, ax_height_ratio])

def parse_command_line():

    # top-level parser
    parser = argparse.ArgumentParser()

    # subparser
    subparsers = parser.add_subparsers(title='commands',
        description='access help with: %s COMMAND --help' % (parser.prog,))

    # stat
    parser_stat = subparsers.add_parser('stat', help='Print statics')
    parser_stat.add_argument('chronofile', metavar='CHRONO_FILE', nargs=1, help="File containing chrono values")
    parser_stat.set_defaults(func=stat)

    # interval
    parser_interval = subparsers.add_parser('interval', help='Compute and print chrono interval')
    parser_interval.add_argument('chronofile', metavar='CHRONO_FILE', nargs=1, help="File containing chrono values")
    parser_interval.set_defaults(func=interval)

    # check
    parser_interval = subparsers.add_parser('check', help='Check chrono is in expected interval')
    parser_interval.add_argument('chronofile', metavar='CHRONO_FILE ', nargs=1, help="File containing chrono values")
    parser_interval.add_argument('intervalfile', metavar='INTERVAL_FILE ', nargs=1, help="File containing chrono values")
    parser_interval.set_defaults(func=check)

    # genbench
    parser_genbench = subparsers.add_parser('genbench', help='Generate fake benchmark data for tests')
    parser_genbench.add_argument('--nfiles', '-n',type=int, default=10, help="How many files are generated")
    parser_genbench.add_argument('chrono_name', metavar='CHRONO_NAME', nargs=1, help="name of the chrono")
    parser_genbench.add_argument('revision', metavar='REVISION', nargs=1, type=int,help="SVN revision")
    parser_genbench.add_argument('mean', metavar='MEAN', type=float, nargs=1, help="chrono mean value (seconds)")
    parser_genbench.set_defaults(func=genbench)

    # plotbench
    parser_plotbench = subparsers.add_parser('plotbench', help='Plot benchmark results')
    parser_plotbench.add_argument('--title', '-t', default='', help="Figure title")
    parser_plotbench.add_argument('--output', '-o', default='', help="Output filename")
    parser_plotbench.add_argument('--arch', action='append',  required=True, nargs=1, help="Architecture the chronos files are produce on")
    parser_plotbench.add_argument('--chronos', action='append',  required=True, nargs=1, help="Files containing chrono values")
    parser_plotbench.set_defaults(func=plotbench)

    return parser.parse_args()

def stat(args):

    chronos = chronofile_read(args.chronofile[0])

    deviation = abs(chronos - chronos.mean())

    print 'values = ', chronos
    print 'mean = ', chronos.mean()
    print 'deviation = ', deviation
    print 'standard deviation = ', chronos.std()
    print 'max deviation = ', deviation.max()
    print 'min deviation = ', deviation.min()

def interval(args):

    chronos = chronofile_read(args.chronofile[0])

    mean = chronos.mean()
    std = chronos.std()

    variation = max( std, mean*0.2 )

    print '%.3f %.3f' % (mean-variation, mean+variation,)

def check(args):

    chronos = chronofile_read(args.chronofile[0])
    interval = np.loadtxt(args.intervalfile[0],unpack=True)

    mean = chronos.mean()

    if interval[0] < mean < interval[1]:
        print "info: %s: mean value %.3f is in expected interval [%.3f, %.3f]" % \
            (args.chronofile[0], mean, interval[0], interval[1], )
    else:
        print "warning: %s: mean value %.3f is NOT expected interval [%.3f, %.3f]" % \
            (args.chronofile[0], mean, interval[0], interval[1], )

def chronofile_filename_build(rev,when,chrononame,fake=False):
    rev_str = '%7.7i' %  (rev,)
    when_str = when.strftime('%Y-%m-%d-%Hh-%Mm-%Ss-%fmu')

    elements = (rev_str, when_str, chrononame)

    if fake:
        return '_'.join(elements) + '.fake.txt'
    else:
        return '_'.join(elements) + '.txt'

def chronofile_filename_extract_revision(filename):
    filename = os.path.basename(filename)
    return int( filename.split('_')[0] )

def chronofile_filename_extract_revision(filename):
    filename = os.path.basename(filename)
    return int( filename.split('_')[0] )

def chronofile_filename_isfake(filename):
    filename = os.path.basename(filename)
    return filename.endswith('.fake.txt')

def chronofile_filename_isold(filename):
    filename = os.path.basename(filename)
    return filename.endswith('.old.txt')

def chronofile_write(filename,chrono):

    lines = ["# seconds, one line per processor\n",]

    for c in chrono:
        lines.append('%.2f\n' % (c,) )

    with open(filename,'w') as f:
        f.writelines(lines)

def chronofile_read(filename):
    chronos = np.loadtxt(filename,unpack=True)
    if len(chronos) > 1:
        #seems that first processor works less than orthers
        return chronos[1:]
    else:
        return chronos

def genbench(args):

    nprocs = 4

    today = datetime.datetime.today()
    oneday = datetime.timedelta(days=1)
    bound0 = 0.8*args.mean[0]
    bound1 = 1.2*args.mean[0]

    for i in range(args.nfiles):
        when = today - i*oneday
        rev = args.revision[0]-i
        chronofilename = chronofile_filename_build(rev, when, args.chrono_name[0], fake=True)
        value = random.uniform(bound0,bound1) 
        chronos = [ value+random.uniform(-0.01*value,+0.01*value) for iproc in range(nprocs) ]
        chronofile_write(chronofilename,chronos)
        print chronofilename

def plotbench(args):

    import matplotlib
    matplotlib.use('agg') 
    import matplotlib.pyplot as plt
    from matplotlib.ticker import FormatStrFormatter

    la = len(args.arch) 
    lc = len(args.chronos)
    markers = ['o','s','v','*','^','p','+']

    if la != lc:
        raise ValueError, 'There are %i --arch, but %i --chronos' % (la,lc,)
    if la > len(markers):
        raise ValueError, 'Too much arch, no enough markers'

    fig = plt.figure()
    ax = plt.subplot(111)

    ax.xaxis.set_major_formatter( FormatStrFormatter('%i') )

    for iarch,(arch,pattern) in enumerate(zip(args.arch, args.chronos)):

        marker = markers[iarch]

        is_first = True

        for ifile,filename in enumerate( glob.glob(pattern[0]) ):
            chronos = chronofile_read(filename)
            isfake = chronofile_filename_isfake(filename)
            isold = chronofile_filename_isold(filename)
            svn_revision = chronofile_filename_extract_revision(filename)

            if isfake:
                color = 'purple'
            elif isold:
                color = 'yellow'
            else:
                color = 'blue'

            if is_first:
                plt.plot([svn_revision,]*len(chronos), chronos, color=color,marker=marker,label=arch[0]) 
                is_first = False
            else:
                plt.plot([svn_revision,]*len(chronos), chronos, color=color,marker=marker) 
            print filename

    #extend xlim
    rev0,rev1 = plt.xlim()
    plt.xlim(rev0-5,rev1+5)

    plt.xlabel('SVN revision number')
    plt.ylabel('seconds (1 run)')
    plt.title(args.title)
    plt.grid()

    plt.legend()

    set_figure_size(fig,
        ax_width_cm  =  20,
        ax_height_cm =   4,
        left_mm      =  20,
        right_mm     =  10,
        top_mm       =  15,
        bottom_mm    =  15,
        )

    plt.savefig(args.output)
    print args.output

def main():
    args = parse_command_line()

    args.func(args)

if __name__ == '__main__':
    main()
