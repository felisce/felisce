//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _ANALYTICFUNCTION_HPP
#define _ANALYTICFUNCTION_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce
{
/*!
  \class ParabolicProfile
  \authors J-F. Gerbeau
  \date january 2012
  \brief Parabolic (trinomial) function (e.g. for Poiseuille flow)

  \todo Manage 2D and 3D in this class

  ParabolicProfile fct;
  fct.init(component,direction,a,b,vmax);

  The std::vector function has a nonzero component only along 'component', and

  fct(icomp,x,y,z) is 4 * vmax * (a-xi)*(xi-b) / (a-b)^2 if icomp = component
  and 0 otherwise

  where xi = x if direction=0, xi = y if direction=1, xi=z if direction=2

  Example:
  fct.init(0,1,-1.,1.,2.);
  will be a std::vector along 0x, dependent only on coordinate y, with 2 roots in -1 and +1, withe a maximum value 2.

  Remarks:

  Value vmax can be negative (in such a case it is the minimum value of the trinomial)
  Value vmax can be modified (for example in time): fct.vmax() = ...;

  */
class ParabolicProfile
{
  int m_direction; //! 0: x , 1: y, 2: z
  int m_component; //! 0: x, 1: y, 2: z
  double m_a; //! 1st root of the trinomial
  double m_b; //! 2d root of the trinomial
  double m_vmax; //! maximum value of the trinomial (i.e. value at (a+b)/2)
public:
  ParabolicProfile() = default;
  void init(int component,int direction,double a,double b,double vmax);
  double& vmax() {
    return m_vmax;
  }
  double vmax() const {
    return m_vmax;
  }
  inline double operator()(int icomp,double x,double y,double z) const {
    if(icomp != m_component) return 0.;

    double xi = 0.;
    switch(m_direction) {
    case 0:
      xi=x;
      break;
    case 1:
      xi=y;
      break;
    case 2:
      xi=z;
      break;
    default:
      FEL_ERROR("ParabolicProfile: direction must be 0,1 or 2");
    }
    return( 4.*m_vmax/((m_a-m_b)*(m_a-m_b)) * (m_a-xi)*(xi-m_b) );
  }
};

}

#endif
