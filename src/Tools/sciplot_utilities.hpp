//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _SCIPLOT_UTILITIES_HPP
#define _SCIPLOT_UTILITIES_HPP

// System includes
#include <string>

// External includes

// Project includes
#include "Tools/table_interpolation.hpp"

namespace felisce
{

namespace SciplotUtilities
{
  /**
   * @brief This generates a 2D plot from a given table
   * @param rTable The table holding the values
   * @param FileType The file type considered
   * @param ShowPlot If the plot is shown
   */
  void Generate2DPlot(
    const felisce::TableInterpolation& rTable,
    const std::string FileType = "pdf",
    const bool ShowPlot = false
    );

  /**
   * @brief This generates a 2D plot from a given table
   * @param rTable The table holding the values
   * @param FileType The file type considered
   * @param ShowPlot If the plot is shown
   */
  void Generate2DPlot(
    const std::vector<felisce::TableInterpolation>& rTables,
    const std::string FileType = "pdf",
    const bool ShowPlot = false
    );
}

}

#endif