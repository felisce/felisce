//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

#ifndef _SCHAFSOLVER_HPP
#define _SCHAFSOLVER_HPP

// System includes

// External includes

// Project includes
#include "Solver/ionicSolver.hpp"

namespace felisce {
  class SchafSolver:
    public IonicSolver {
  public:
    ///Constructor.
    SchafSolver(FelisceTransient::Pointer fstransient);
    ///Destructor.
    ~SchafSolver() override;

    ///Compute RHS in EDO.
    void computeRHS() override;
    ///Solve the EDO.
    void solveEDO() override;
    ///Computation of ionic current from EDO solution.
    void computeIon() override;


    /// Access functions.
    virtual inline const HeteroTauClose& fctTauClose() const {
      return m_heteroTauClose;
    }
    virtual inline HeteroTauClose& fctTauClose() {
      return m_heteroTauClose;
    }

    inline const std::vector<double>& tauClose() const {
      return m_tauClose;
    }
    inline std::vector<double>& tauClose() {
      return m_tauClose;
    }

    virtual inline const HeteroTauOut& fctTauOut() const {
      return m_heteroTauOut;
    }
    virtual inline HeteroTauOut& fctTauOut() {
      return m_heteroTauOut;
    }

    inline const std::vector<double>& tauOut() const {
      return m_tauOut;
    }
    inline std::vector<double>& tauOut() {
      return m_tauOut;
    }

    inline const HeteroTauIn& fctTauIn() const {
      return m_heteroTauIn;
    }
    inline HeteroTauIn& fctTauIn() {
      return m_heteroTauIn;
    }

    inline const std::vector<double>& tauIn() const {
      return m_tauIn;
    }
    inline std::vector<double>& tauIn() {
      return m_tauIn;
    }
  protected:
    HeteroTauClose m_heteroTauClose;
    std::vector<double> m_tauClose;
    ///To simulate infarction.
    HeteroTauOut m_heteroTauOut;
    std::vector<double> m_tauOut;
    HeteroTauIn m_heteroTauIn;
    std::vector<double> m_tauIn;
  };
}

#endif
