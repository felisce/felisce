//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _LUMPEDMODELBC_HPP
#define _LUMPEDMODELBC_HPP

// System includes
#include <string>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/geoElement.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce {
  class LumpedModelBC {
    FelisceTransient::Pointer m_fstransient;
    std::size_t m_size;
    std::vector<double> m_Rd;
    std::vector<double> m_Rp;
    std::vector<double> m_C;
    std::vector<double> m_Pv;
    std::vector<double> m_Pd0;
    std::vector<double> m_Pd;
    std::vector<double> m_PdOld;
    std::vector<double> m_Pp;
    std::vector<double> m_PpOld;
    std::vector<double> m_Q;
    std::vector<std::string> m_name;

    // parameters for non linear compliance C=C(V)
    //==============================
    // volume at the last time step (explicit)
    double m_volume_n_1;
    double m_volumeMax;
    double m_volumeMin;
    double m_volumeMaxTilde;
    double m_volumeMinTilde;
    // volume at rest
    double m_volume0;
    double m_volume0ref;
    int m_powerNonLinearCompliance;

    double computeNonLinearCompliance(std::size_t& idBCwindk);

  public:
    //!Constructor.
    LumpedModelBC(FelisceTransient::Pointer fstransient);
    //!Destructor.
    ~LumpedModelBC() = default;
    void iterate();
    void print(int verbose, std::ostream& c = std::cout) const;

    void setRd( const std::vector<double>& rd ) {
      m_Rd = rd;
    }
    void setC( const std::vector<double>& c ) {
      m_C = c;
    }

    // access function
    double& Q(int i) {
      return m_Q[i];
    }
    double Q(int i) const {
      return m_Q[i];
    }

    double& Pd(int i) {
      return m_Pd[i];
    }
    double Pd(int i) const {
      return m_Pd[i];
    }

    double Pp(int i) const {
      return m_Pp[i];
    }
    
    double PpOld(int i) const {
      return m_PpOld[i];
    }

    inline const std::vector<double>& Pp() const {
      return m_Pp;
    }
    inline std::vector<double>& Pp() {
      return m_Pp;
    }

    inline const std::size_t & size() const {
      return m_size;
    }
    inline std::size_t & size() {
      return m_size;
    }

    double& volume_n_1() {
      return m_volume_n_1;
    }
    double volume_n_1() const {
      return m_volume_n_1;
    }

    double& volume0() {
      return m_volume0;
    }
    double volume0() const {
      return m_volume0;
    }

    double& C(int i) {
      return m_C[i];
    }
    double C(int i) const {
      return m_C[i];
    }

    void write(std::string filename) const;

  };
}

#endif

