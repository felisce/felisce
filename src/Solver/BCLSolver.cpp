//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    F. Raphel
//

// System includes

// External includes

// Project includes
#include "Solver/BCLSolver.hpp"

#ifdef FELISCE_WITH_SUNDIALS

//For CVODE
#define Ith(v,i)    NV_Ith_S(v,i)       /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) DENSE_ELEM(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */

static constexpr double RTOL = RCONST(1.0e-8);
static constexpr double ATOL = RCONST(1.0e-4); /* std::vector absolute tolerance components */

#endif

namespace felisce {
  BCLSolver::BCLSolver(FelisceTransient::Pointer fstransient):
    IonicSolver(fstransient) {
    m_nComp = 4;

    if (FelisceParam::instance().CellsType != "hetero") {
      tauhp.resize(1);
      tauhm.resize(1);
      taufp.resize(1);
      taufm.resize(1);
      taurp.resize(1);
      taurm.resize(1);
      tausp.resize(1);
      tausm.resize(1);
      gfi.resize(1);
      Vfi.resize(1);
      gso.resize(1);
      gsi.resize(1);
      beta1.resize(1);
      beta2.resize(1);
      V1.resize(1);
      V2.resize(1);
      gto.resize(1);
      Vc.resize(1);
      Vs.resize(1);

      if (FelisceParam::instance().CellsType == "endo") {
        tauhp[0] = 10.8;
        tauhm[0] = 10.8;
        taufp[0] = 355.;
        taufm[0] = 52.3;
        taurp[0] = 7.54;
        taurm[0] = 6.07;
        tausp[0] = 29.1;
        tausm[0] = 10.4;
        gfi[0] = 1.72;
        Vfi[0] = 1.24;
        gso[0] = 0.00891;
        gsi[0] = 0.414;
        beta1[0] = 22.8;
        beta2[0] = 2.95;
        V1[0] = 0.522;
        V2[0] = 0.596;
        gto[0] = 0.300;
        Vc[0] = 0.130;
        Vs[0] = 0.6;
      } else if (FelisceParam::instance().CellsType == "M") {
        tauhp[0] = 11.3;
        tauhm[0] = 1.88;
        taufp[0] = 101.;
        taufm[0] = 228.;
        taurp[0] = 2.15;
        taurm[0] = 0.371;
        tausp[0] = 4.67;
        tausm[0] = 1.75;
        gfi[0] = 2.62;
        Vfi[0] = 1.60;
        gso[0] = 0.0278;
        gsi[0] = 0.103;
        beta1[0] = 27.1;
        beta2[0] = 6.12;
        V1[0] = 0.668;
        V2[0] = 1.08;
        gto[0] = 1.36;
        Vc[0] = 0.130;
        Vs[0] = 0.3;
      } else if (FelisceParam::instance().CellsType == "epi") {
        tauhp[0] = 17.9;
        tauhm[0] = 11.4;
        taufp[0] = 123.;
        taufm[0] = 183.;
        taurp[0] = 2.51;
        taurm[0] = 2.00;
        tausp[0] = 57.0;
        tausm[0] = 10.6;
        gfi[0] = 4.00;
        Vfi[0] = 1.46;
        gso[0] = 0.0161;
        gsi[0] = 0.176;
        beta1[0] = 3.99;
        beta2[0] = 1.56;
        V1[0] = 0.529;
        V2[0] = 0.386;
        gto[0] = 2.10;
        Vc[0] = 0.130;
        Vs[0] = 0.3;
      } else if (FelisceParam::instance().CellsType == "LRd") {
        tauhp[0] = 25.8;
        tauhm[0] = 0.950;
        taufp[0] = 488.;
        taufm[0] = 25.4;
        taurp[0] = 5.10;
        taurm[0] = 13.1;
        tausp[0] = 300.;
        tausm[0] = 7.11;
        gfi[0] = 10.0;
        Vfi[0] = 1.20;
        gso[0] = 0.0316;
        gsi[0] = 2.32;
        beta1[0] = 6.90;
        beta2[0] = 6.36;
        V1[0] = 0.453;
        V2[0] = 0.828;
        gto[0] = 2.50;
        Vc[0] = 0.380;
        Vs[0] = 0.6;
      } else if (FelisceParam::instance().CellsType == "TNNP") {
        tauhp[0] = 9035;
        tauhm[0] = 6.61;
        taufp[0] = 43.1;
        taufm[0] = 181.;
        taurp[0] = 13.5;
        taurm[0] = 2.20;
        tausp[0] = 99.9;
        tausm[0] = 4.34;
        gfi[0] = 14.0;
        Vfi[0] = 1.18;
        gso[0] = 0.0498;
        gsi[0] = 0.138;
        beta1[0] = 11.8;
        beta2[0] = 8.05;
        V1[0] = 0.200;
        V2[0] = 1.02;
        gto[0] = 9.82;
        Vc[0] = 0.350;
        Vs[0] = 0.6;
      }

    } else {

      tauhp.resize(4);
      tauhm.resize(4);
      taufp.resize(4);
      taufm.resize(4);
      taurp.resize(4);
      taurm.resize(4);
      tausp.resize(4);
      tausm.resize(4);
      gfi.resize(4);
      Vfi.resize(4);
      gso.resize(4);
      gsi.resize(4);
      beta1.resize(4);
      beta2.resize(4);
      V1.resize(4);
      V2.resize(4);
      gto.resize(4);
      Vc.resize(4);
      Vs.resize(4);

      // RV
      tauhp[0] = 17.9;
      tauhm[0] = 11.4;
      taufp[0] = 123.;
      taufm[0] = 183.;
      taurp[0] = 2.51;
      taurm[0] = 2.00;
      tausp[0] = 57.0;
      tausm[0] = 10.6;
      gfi[0] = 4.00;
      Vfi[0] = 1.46;
      gso[0] = 0.0161;
      gsi[0] = 0.176;
      beta1[0] = 3.99;
      beta2[0] = 1.56;
      V1[0] = 0.529;
      V2[0] = 0.386;
      gto[0] = 2.10;
      Vc[0] = 0.130;
      Vs[0] = 0.3;

      // Endo
      tauhp[1] = 10.8;
      tauhm[1] = 10.8;
      taufp[1] = 355.;
      taufm[1] = 52.3;
      taurp[1] = 7.54;
      taurm[1] = 6.07;
      tausp[1] = 29.1;
      tausm[1] = 10.4;
      gfi[1] = 1.72;
      Vfi[1] = 1.24;
      gso[1] = 0.00891;
      gsi[1] = 0.414;
      beta1[1] = 22.8;
      beta2[1] = 2.95;
      V1[1] = 0.522;
      V2[1] = 0.596;
      gto[1] = 0.300;
      Vc[1] = 0.130;
      Vs[1] = 0.6;

      // M-Cells
      tauhp[2] = 11.3;
      tauhm[2] = 1.88;
      taufp[2] = 101.;
      taufm[2] = 228.;
      taurp[2] = 2.15;
      taurm[2] = 0.371;
      tausp[2] = 4.67;
      tausm[2] = 1.75;
      gfi[2] = 2.62;
      Vfi[2] = 1.60;
      gso[2] = 0.0278;
      gsi[2] = 0.103;
      beta1[2] = 27.1;
      beta2[2] = 6.12;
      V1[2] = 0.668;
      V2[2] = 1.08;
      gto[2] = 1.36;
      Vc[2] = 0.130;
      Vs[2] = 0.3;

      // Epi
      tauhp[3] = 17.9;
      tauhm[3] = 11.4;
      taufp[3] = 123.;
      taufm[3] = 183.;
      taurp[3] = 2.51;
      taurm[3] = 2.00;
      tausp[3] = 57.0;
      tausm[3] = 10.6;
      gfi[3] = 4.00;
      Vfi[3] = 1.46;
      gso[3] = 0.0161;
      gsi[3] = 0.176;
      beta1[3] = 3.99;
      beta2[3] = 1.56;
      V1[3] = 0.529;
      V2[3] = 0.386;
      gto[3] = 2.10;
      Vc[3] = 0.130;
      Vs[3] = 0.3;
    }
  }

  BCLSolver::~BCLSolver()
  = default;

  void BCLSolver::initialize(std::vector<PetscVector>& sol_0) {
#ifdef FELISCE_WITH_SUNDIALS
    int flag;//Informations for CVODE (if flag==0: OK)
#else
    IGNORE_UNUSED_ARGUMENT(sol_0);
#endif
    m_userData.resize(m_size);
    m_cvode_mem.resize(m_size);
#ifdef FELISCE_WITH_SUNDIALS
    m_initvalue.resize(m_size);
    m_abstol.resize(m_size);
#endif

    double value_uExtrap;
    double Vmin = FelisceParam::instance().vMin;
    double Vmax = FelisceParam::instance().vMax;

    int iType = 0;

    felInt pos;
    for (felInt i = 0; i < m_size; i++) {
      // Set m_userData.u[i] with transmembrane potential extrapolate (rescaled on [0,1])
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);
      value_uExtrap = (value_uExtrap-Vmin)/(Vmax-Vmin);
      m_userData[i].u = value_uExtrap;

      if (FelisceParam::instance().CellsType == "hetero")
        iType = m_cellType[pos];

      m_userData[i].iType = iType;
      m_userData[i].pos = i;

      m_cvode_mem[i] = nullptr;
#ifdef FELISCE_WITH_SUNDIALS
      m_initvalue[i] = N_VNew_Serial(m_nComp);
      m_abstol[i] = N_VNew_Serial(m_nComp);

      double valuem_solEDO;//0=h, 1=f, 2=r and 3=s
      double w0value;
      for (std::size_t j=0; j<m_nComp; j++) {
        sol_0[j].getValues(1,&pos,&w0value);
        Ith(m_initvalue[i],j) = w0value;//h
        Ith(m_abstol[i],j) = ATOL;
      }

      m_cvode_mem[i] = CVodeCreate(CV_BDF,CV_NEWTON);//CV_BDF or CV_ADAMS and CV_NEWTON or CV_FUNCTIONAL || (CV_BDF,CV_NEWTON) for stiff pb
      m_data = &m_userData[i];

      flag = CVodeSetUserData(m_cvode_mem[i],m_data);
      flag = CVodeInit(m_cvode_mem[i],M_f,0.0,m_initvalue[i]);
      flag = CVodeSVtolerances(m_cvode_mem[i],RTOL,m_abstol[i]);
      flag = CVDense(m_cvode_mem[i],m_nComp);

      //CVODE Options:
      //
      flag = CVodeSetMaxOrd(m_cvode_mem[i],5);//default: 5, order max of BDF
      flag = CVodeSetMaxNumSteps(m_cvode_mem[i],500);//default: 500
      flag = CVodeSetMaxStep(m_cvode_mem[i],FelisceParam::instance().timeStep);//0.1 is the step interpolation
#endif
    }
  }


  void BCLSolver::solveEDO() {

#ifdef FELISCE_WITH_SUNDIALS
    int flag;//Informations for CVODE (if flag==0: OK)
#endif
    double value_uExtrap;
    double Vmin = FelisceParam::instance().vMin;
    double Vmax = FelisceParam::instance().vMax;


    felInt pos;
    for (felInt i = 0; i < m_size; i++) {
      // Set m_userData.u[i] with transmembrane potential extrapolate (rescaled on [0,1])
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);
      value_uExtrap = (value_uExtrap-Vmin)/(Vmax-Vmin);
      m_userData[i].u = value_uExtrap;

#ifdef FELISCE_WITH_SUNDIALS
      int iType = 0;
      if (FelisceParam::instance().CellsType == "hetero")
        iType = m_cellType[pos];

      double time = m_fstransient->time;
      double valuem_solEDO;//0=h, 1=f, 2=r and 3=s
      realtype tout;
      if (iType < 4) {
        flag = CVode(m_cvode_mem[i], time, m_initvalue[i], &tout, CV_NORMAL);
        if (  flag != CV_SUCCESS ) {
          FEL_ERROR("CVODE solver failed ");
        }

        for(std::size_t j=0; j<m_nComp; j++) {
          valuem_solEDO = Ith(m_initvalue[i],j);
          m_vecSolEDO[j].setValue(pos,valuem_solEDO, INSERT_VALUES);
        }
      } else {
        valuem_solEDO = 0.;
        for(std::size_t j=0; j<m_nComp; j++) {
          m_vecSolEDO[j].setValue(pos,valuem_solEDO, INSERT_VALUES);
        }
      }
#endif
    }
    for(std::size_t j=0; j<m_nComp; j++) {
      m_vecSolEDO[j].assembly();
    }

  }

  void BCLSolver::computeIon() {
    std::cout << "  void BCLSolver::computeIon()" << std::endl;
    double value_uExtrap;
    std::vector<double> valuem_solEDO;
    valuem_solEDO.resize(4);
    double value_ion;

    double Jfi;
    double Jsi;
    double Jto;
    double Jso;

    double Vmin = FelisceParam::instance().vMin;
    double Vmax = FelisceParam::instance().vMax;

    felInt iType = 0;

    felInt pos;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      value_uExtrap = (value_uExtrap-Vmin)/(Vmax-Vmin);

      for(int j=0; j<4; j++) {
        m_vecSolEDO[j].getValues(1,&pos,&valuem_solEDO[j]);
      }

      //Calculation of minf,dinf,fpinf and kinf-----fpinf=f'm_inf
      if (FelisceParam::instance().CellsType == "hetero")
        iType = m_cellType[pos];

      if (iType < 4) {
        double hinf;
        double finf;
        double rinf;
        double tauh;
        double tauf;
        double taur;

        hgate(hinf, tauh, value_uExtrap, iType);
        sgate(finf, tauf, value_uExtrap, iType);
        rgate(rinf, taur, value_uExtrap, iType);

        double H;
        double minf;
        double dinf;
        double kinf;
        double fpinf;

        H = Heaviside(value_uExtrap,Vc[iType]);

        minf = (value_uExtrap-Vc[iType])*H;
        dinf = /*0.5**/H*(1.+std::tanh(beta1[iType]*(value_uExtrap-V1[iType])));
        kinf = (value_uExtrap/Vc[iType])+H*(1.-(value_uExtrap/Vc[iType]));
        fpinf = /*0.5**/(1.-std::tanh(beta2[iType]*(value_uExtrap-V2[iType])));

        Jfi = -gfi[iType]*valuem_solEDO[0]*minf*(Vfi[iType]-value_uExtrap);
        Jsi = -gsi[iType]*dinf*valuem_solEDO[1]*fpinf;
        Jto = gto[iType]*valuem_solEDO[2]*valuem_solEDO[3]*(value_uExtrap);//U-Vto, but Vto=0
        Jso = gso[iType]*kinf;
      } else {
        Jfi = 0.;
        Jsi = 0.;
        Jto = 0.;
        Jso = 0.;
      }

      //Currents:
      const double Cm = FelisceParam::instance().Cm; //about 1 uF cm-2------1 avant
      const double dV = Vmax-Vmin;//mV----100mV

      value_ion = - ( Jfi + Jsi + Jto + Jso )*Cm*dV;

      m_ion.setValue(pos,value_ion, INSERT_VALUES);

    }
    m_ion.assembly();

  }


  //******************************************************************************************************************
  //                                                Gates + Heaviside
  //******************************************************************************************************************

  void BCLSolver::hgate(double& hinf,double& tauh, double u, int iType) {

    if( iType < 4 ) {
      const double H = BCLSolver::Heaviside(u,Vc[iType]);
      hinf = 1.-H;
      tauh = tauhp[iType]-H*(tauhp[iType]-tauhm[iType]);
    } else {
      hinf = 0.;
      tauh = 1.e+06;
    }

  }

  void BCLSolver::fgate(double& finf,double& tauf, double u, int iType) {

    if( iType < 4 ) {
      const double H = BCLSolver::Heaviside(u,Vc[iType]);
      finf = 1.-H;
      tauf = taufp[iType]-H*(taufp[iType]-taufm[iType]);
    } else {
      finf = 0.;
      tauf = 1.e+06;
    }

  }

  void BCLSolver::rgate(double& rinf,double& taur, double u, int iType) {
    //    const double H = BCLSolver::Heaviside(u,0.6);//Vr=0.6
    const double H = BCLSolver::Heaviside(u,Vs[iType]); // V_r = V_s (typo in the paper)
    rinf = H;


    if( iType < 4 ) {
      taur = taurp[iType]-H*(taurp[iType]-taurm[iType]);
    } else {
      taur = 0.;
      rinf = 1.e+06;
    }
  }

  void BCLSolver::sgate(double& sinf,double& taus, double u, int iType) {


    if( iType < 4 ) {
      const double H = BCLSolver::Heaviside(u,Vc[iType]);
      sinf = 1.-H;
      taus = tausp[iType]-H*(tausp[iType]-tausm[iType]);
    } else {
      sinf = 0.;
      taus = 1.e+06;
    }

  }

  double BCLSolver::Heaviside(double V,double Vsubstr) {
    double H = 0.;
    const double substr = V-Vsubstr;

    if(substr>=0.) {
      H = 1.;
    }
    return H;
  }


  //******************************************************************************************************************
  //******************************************************************************************************************


  //******************************************************************************************************************
  //                                              For CVODE
  //******************************************************************************************************************

#ifdef FELISCE_WITH_SUNDIALS

  int BCLSolver::M_f(realtype t, N_Vector y, N_Vector ydot, void *f_data) {
    (void) t;

    BCLUserData* data = static_cast< BCLUserData* >(f_data);
    const int pos = data->pos;
    const double u = data->u;
    const int iType = data->iType;

    double hinf;
    double tauh;
    BCLSolver::hgate(hinf,tauh,u,iType);

    double finf;
    double tauf;
    BCLSolver::fgate(finf,tauf,u,iType);

    double rinf;
    double taur;
    BCLSolver::rgate(rinf,taur,u,iType);

    double sinf;
    double taus;
    BCLSolver::sgate(sinf,taus,u,iType);


    Ith(ydot,0) = (hinf-Ith(y,0))/tauh;//hdot
    Ith(ydot,1) = (finf-Ith(y,1))/tauf;//fdot
    Ith(ydot,2) = (rinf-Ith(y,2))/taur;//rdot
    Ith(ydot,3) = (sinf-Ith(y,3))/taus;//sdot

    return 0;
  }
#endif

  //******************************************************************************************************************
  //******************************************************************************************************************
}


