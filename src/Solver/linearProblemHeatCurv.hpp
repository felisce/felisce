//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

#ifndef _LINEARPROBLEMHEATCURV_HPP
#define _LINEARPROBLEMHEATCURV_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
  /*!
    \class LinearProblemHeatCurv
    \authors A. Collin
    \date 08/05/2011
    \brief  ???
  */
  class LinearProblemHeatCurv:
    public LinearProblem {
  public:
    LinearProblemHeatCurv();
    ~LinearProblemHeatCurv() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void desallocateElementArray();
  protected:
    felInt m_iTemperature;
    CurvilinearFiniteElement* m_feTemp;
  private:
    ElementField m_elemField;
  };
}

#endif
