//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Solver/schafRevisedSolver.hpp"

// I_{ion} = reac_amp* (w/tau_in)  (u-v_min) * (u-v_min) *(v_max - u)/(v_min - v_max)  - (u- v_min) /(tau_out * (v_min- v_max))
//
// dw/dt = a(v) * (1-w) - b(v) * w
//
// a(v) = (1-f) / (tau_open + (tau_open-tau_close)*f)
//
// b(v) = f / (tau_open + (tau_open-tau_close)*f)
//
// f(v) = 1/2 * (1 + tanh(k(u-v_g))) ~ 1/2 * (1 + k(u-v_g) - 1/3 * k^3*(u-v_g)^3 )
//
//w_0 = (v_max-v_min)^{-2}

namespace felisce {
  SchafRevisedSolver::SchafRevisedSolver(FelisceTransient::Pointer fstransient):
    SchafSolver(fstransient),
    m_fTanh(nullptr),
    m_fA(nullptr),
    m_fB(nullptr)
  {}


  SchafRevisedSolver::~SchafRevisedSolver() {
    delete [] m_fTanh;
    delete [] m_fA;
    delete [] m_fB;
  }

  void SchafRevisedSolver::computeTanhFunction(double* u) {
    double& vGate = FelisceParam::instance().vGate;
    double& k = FelisceParam::instance().kTanhMSR;

    if (m_fTanh == nullptr) {
      m_fTanh = new double[m_size];
    }
    for (felInt i=0; i<m_size; i++) {
      m_fTanh[i] = 0.5 * (1 + k*(u[i]-vGate) - 1/3 * k*k*k * (u[i]-vGate)*(u[i]-vGate)*(u[i]-vGate));
    }
  }

  void SchafRevisedSolver::computeAFunction() {
    double& tauOpen  = FelisceParam::instance().tauOpen;
    double& tauClose = FelisceParam::instance().tauClose;

    if (m_fA == nullptr) {
      m_fA = new double[m_size];
    }

    for (felInt i=0; i<m_size; i++) {
      if (FelisceParam::instance().hasHeteroTauClose) {
        m_fA[i] = (1-m_fTanh[i])/(tauOpen+(m_tauClose[i]-tauOpen)*m_fTanh[i]);

      } else {
        m_fA[i] = (1-m_fTanh[i])/(tauOpen+(tauClose-tauOpen)*m_fTanh[i]);
      }
    }
  }

  void SchafRevisedSolver::computeBFunction() {
    double& tauOpen  = FelisceParam::instance().tauOpen;
    double& tauClose = FelisceParam::instance().tauClose;

    if (m_fB == nullptr) {
      m_fB = new double[m_size];
    }

    for (felInt i=0; i<m_size; i++) {
      if (FelisceParam::instance().hasHeteroTauClose) {
        m_fB[i] = m_fTanh[i]/(tauOpen+(m_tauClose[i]-tauOpen)*m_fTanh[i]);
      } else {
        m_fB[i] = m_fTanh[i]/(tauOpen+(tauClose-tauOpen)*m_fTanh[i]);
      }
    }
  }

  void SchafRevisedSolver::computeRHS() {
    double& dt = m_fstransient->timeStep;

    m_bdf.computeRHSTime(dt, m_RHS);

    felInt pos;
    double* value_uExtrap = new double[m_size];
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap[i]);//value_uExtrap = m_uExtrap(i)
    }
    computeTanhFunction(value_uExtrap);
    computeAFunction();
    computeBFunction();

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_RHS.setValue(pos,m_fA[i], ADD_VALUES);
    }
    m_RHS.assembly();
  }

  void SchafRevisedSolver::solveEDO() {
    double& coeffDeriv = m_bdf.coeffDeriv0();
    double& dt = m_fstransient->timeStep;

    felInt pos;
    double value_RHS;
    double valuem_solEDO;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_RHS.getValues(1,&pos,&value_RHS);//value_RHS = m_RHS(i)
      valuem_solEDO = value_RHS * 1./(coeffDeriv/dt + m_fA[i] + m_fB[i]);
      m_solEDO.setValue(pos,valuem_solEDO, INSERT_VALUES);
    }
    m_solEDO.assembly();
  }

  void SchafRevisedSolver::computeIon() {
    double& tauOut  = FelisceParam::instance().tauOut;
    double& tauIn = FelisceParam::instance().tauIn;
    double& vMin  = FelisceParam::instance().vMin;
    double& vMax  = FelisceParam::instance().vMax;

    felInt pos;
    double value_uExtrap;
    double value_m;
    double valuem_solEDO;
    double value_ion;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_solEDO.getValues(1,&pos,&valuem_solEDO);//valuem_solEDO = m_solEDO(i)
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      value_m = value_uExtrap;
      if (value_uExtrap < vMin) {
        value_m = vMin;
      } else if (value_uExtrap > vMax) {
        value_m = vMax;
      }
      if (FelisceParam::instance().hasInfarct) {
        value_ion = valuem_solEDO/tauIn*(value_m-vMin)*(value_m-vMin)*(vMax-value_uExtrap)/(vMax-vMin)-(value_uExtrap-vMin)/(m_tauOut[pos] *(vMax-vMin));
      } else {
        value_ion = valuem_solEDO/tauIn*(value_m-vMin)*(value_m-vMin)*(vMax-value_uExtrap)/(vMax-vMin)-(value_uExtrap-vMin)/(tauOut *(vMax-vMin));
      }
      m_ion.setValue(pos,value_ion, INSERT_VALUES);
    }
    m_ion.assembly();
  }

}
