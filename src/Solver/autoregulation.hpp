//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __AUTOREGULATION_HPP__
#define __AUTOREGULATION_HPP__

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"

namespace felisce 
{
  /*!
    \brief A class to handle boundary conditions data for (retinal) autoregulation
    
    The class handle several aspects of the data concerning boundary conditions:
    - input pressure: \n
      shape, mean value over a cardiac cycle, difference with respect to a given pressure story
    - autoregulation:\n
      both in fibers and in windkessel
    - post-process
   */
  class Autoregulation{
  public:
    /*! \brief id of the master proc */    
    static const int MASTER = 0;
    
    std::unordered_map<std::string, std::vector<int> > initialize(std::string elemeBoarderType);
    /**
       @{ \name Downstream autoregulation
     */
    void handleLumpedModel(const double& t, LumpedModelBC* lumpedModel);
    /**@}*/
    void computeExtraPressure( const int &nC);
    
    double control( const double &t ) const;    
    double inletPressure( const double &t, const int& caseTest ) const;
    /** 
       @{ \name Post-process functions
    */
    void exportSummary( const int& rank) const;
    void exportControlFunctions(const int &numCycle);
    void print(const int & rank);
    void update( const double& time, double Q, double P, std::vector<double> pout, const int& rank );
    /**@}*/

    /*! \brief PI */    
    static constexpr double PI = 3.14159265;
    /*! \brief Conversion constant from mm of mercury to Pascal */    
    static constexpr double mmHg2DynPerCm2 = 1333.32;
    /*! \brief duration of the systole */    
    static constexpr double m_systoleDuration = 0.25;
    /*! \brief duration of the cardiac cycle */    
    static constexpr double m_cycleDuration = 0.8;

  private:

    void meanPressure2PeakAndDiast(double const& meanP, double& peakP, double& diastP ) const;
    double computeMeanInflowPress( const int &Case, const double &t0 ) const;
    double interpPressure( const double &t ) const;

    /**
       @{ \name Sigmoidal function and parameters
       all the data members and function, usefull to describe the shape of the sigmoidal function.
    */
    /*!\brief Sigmoidal function used for the autoregulation       

      \param[in] extraP \f$\delta p = p-p_{ref}\f$ is the difference between a pressure p and the reference value
      \return the value of the sigmoidal function in that point
      
      The sigmoidal function:
      \code 
      return ( 1 - exp( - m_slope*extraP ) )/ ( 1 + exp( -m_slope*(extraP-m_center ) ) );
      \endcode
      Function limits:
      - \a extraP goes to  infinity it returns 1
      - \a extraP goes to -infinity it returns \code - exp(m_slope * m_center) \endcode
      - in zero is equal to zero

      Another possible parametrization (used in the paper) is:
      \code 
      return ( 1 - exp( - m_slope*extraP ) )/ ( 1 + 1/w*exp( -m_slope*(extraP) ) );
      \endcode
      In this case \a w represents the minimum value of the sigmoidal function (in absolute value).
      And m_slope can be approximated via 
      \code
      m_slope=1/fp.up*std::log((1+q/w)/(1-q))
      \endcode
    */
    inline double sigmoid( const double &extraP) const {
      return ( 1 - exp( - m_slope*extraP ) )/ ( 1 + exp( -m_slope*(extraP-m_center ) ) );
    }
    /*! \brief slope of the sigmodail function \f$[\mbox{mmHg}^-1]\f$
       
      \f$ 2/(\mbox{fp.up-fp.down})\ln(\mbox{fp.q}/(1-\mbox{fp.q}))\f$
    */
    double m_slope;
    /*! \brief relative center of the sigmoidal function \f$[\mbox{mmHg}]\f$
      
      \f$ (\mbox{fp.up} + \mbox{fp.down})/2\f$
    */
    double m_center;
    /**@}*/
    
    /**
       @{ \name Downstream autoregulation
     */
    void setLumpedModel(const double& t);
    /*! \brief Distal resistance -- reference value */
    std::vector<double> m_RdistRef;
    /*! \brief Distal resistance -- current value */
    std::vector<double> m_Rdist;
    /*! \brief Capacitance -- reference value */
    std::vector<double> m_CRef;
    /*! \brief Capacitance -- current value */
    std::vector<double> m_C;
    /*! \brief Regulation parameter */
    std::vector<double> m_regParam;
    /**@}*/
    /*! \brief difference between mean pressure and reference pressure.

      extraPressure is a std::vector that contains for each cycle the difference 
      of the mean pressure from idcase with respect to the idcontrolFreeCase;
    */
    std::vector<double> m_extraPressure;
    
    /*! \brief of cardiac cycles */
    int m_nC;
    /*! \brief Label(s) of the inlet*/
    std::vector<int> m_labelInlet;
    /*! \brief Label(s) of the outlet*/
    std::vector<int> m_labelOutlet;
    /*! \brief Label(s) of the structure*/
    std::vector<int> m_labelStructure;
    
    /*! \brief Flow at the inlet*/
    std::vector<double> m_QInflow;
    /*! \brief Mean pressure at the inlet*/
    std::vector<double> m_PInflow;
};


}
#endif
