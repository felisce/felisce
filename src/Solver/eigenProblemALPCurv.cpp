//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Solver/eigenProblemALPCurv.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementField.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  /*! Construtor
   */
  EigenProblemALPCurv::EigenProblemALPCurv():
    EigenProblemALP()
  {}

  EigenProblemALPCurv::~EigenProblemALPCurv()
  = default;

  // Define Physical Variable associate to the problem
  void EigenProblemALPCurv::initPerElementTypeBD() {
    switch(m_problem) {
    case 0: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMembCurv = m_listCurvilinearFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMembCurv);
      break;
    }
    case 1: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMembCurv = m_listCurvilinearFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMembCurv);
      break;
    }
    case 2: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMembCurv = m_listCurvilinearFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMembCurv);
      break;
    }
    default:
      FEL_ERROR("Model not defined for ALP solver.");
      break;
    }

    if (FelisceParam::instance().hasInfarct) {
      m_elemFieldFhNf0.initialize(DOF_FIELD,*m_fePotTransMembCurv);
    }
  }

  // Assemble Matrix
  void EigenProblemALPCurv::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel) {
    IGNORE_UNUSED_ELEM_ID_POINT;

    m_fePotTransMembCurv->updateMeasNormalContra(0, elemPoint);

    switch(m_problem) {
    case 0: { // FKPP
      // m_Matrix[0] = grad(phi_i) * grad(phi_j)
      m_elementMatBD[m_idL]->grad_phi_i_grad_phi_j(1.,*m_fePotTransMembCurv,0,0,1);

      if (FelisceParam::instance().hasInitialCondition) {
        // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
        m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMatBD[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMembCurv,0,0,1);
      }

      // Matrix[1] = phi_i * phi_j
      m_elementMatBD[m_idG]->phi_i_phi_j(1.,*m_fePotTransMembCurv,0,0,1);

      if (!FelisceParam::instance().solveEigenProblem) {
        if (m_useImprovedRec) {
          // m_Matrix[m_numberOfMatrix-1] = grad(phi_i) * grad(phi_j)
          m_elementMatBD[m_idK]->grad_phi_i_grad_phi_j(1.,*m_fePotTransMembCurv,0,0,1);
        }
      }
      break;
    }
    case 1: { // Monodomain
      // m_Matrix[0] = grad(phi_i) * grad(phi_j)
      m_elementMatBD[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);

      if (FelisceParam::instance().testCase == 1) {
        std::vector<double> elemFiber;
        getFiberDirection(iel,m_ipotTransMemb, elemFiber);
        // m_Matrix[0] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
        m_elementMatBD[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMembCurv,0,0,1);
      }

      if (FelisceParam::instance().hasInitialCondition) {
        // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
        m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMatBD[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMembCurv,0,0,1);
      }

      // m_Matrix[1] = phi_i * phi_j
      m_elementMatBD[m_idG]->phi_i_phi_j(1.,*m_fePotTransMembCurv,0,0,1);

      if (!FelisceParam::instance().solveEigenProblem) {

        if (FelisceParam::instance().hasInfarct) {
          // m_Matrix[1] = B_ij = s(x) * phi_i * phi_j
          m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
          m_elementMatBD[m_idGs]->a_phi_i_phi_j(1.,m_elemFieldFhNf0,*m_fePotTransMembCurv,0,0,1);
        } else {
          double s = FelisceParam::instance().f0;
          // m_Matrix[1] = B_ij = s * phi_i * phi_j
          m_elementMatBD[m_idGs]->phi_i_phi_j(s,*m_fePotTransMembCurv,0,0,1);
        }

        if ( ( (m_tensorOrder == 4 ) & (m_useImprovedRec) ) || (m_tensorOrder == 3 ) ) {
          // m_Matrix[2] = E_ij = grad(phi_i) * grad(phi_j)
          m_elementMatBD[m_idK]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);
          if (FelisceParam::instance().testCase == 1) {
            std::vector<double> elemFiber;
            getFiberDirection(iel,m_ipotTransMemb, elemFiber);
            // m_Matrix[2] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
            m_elementMatBD[m_idK]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMembCurv,0,0,1);
          }
        }

        if (m_tensorOrder == 3 ) {
          if (FelisceParam::instance().hasInfarct) {
            if (FelisceParam::instance().testCase == 2) {
              // m_Matrix[3] = E^s_ij = s(x) * \sigma_i^t grad(phi_i), grad(phi_j)
              m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
              m_elementMatBD[m_idKs]->a_grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,m_elemFieldFhNf0,*m_fePotTransMembCurv,0,0,1);
            } else if (FelisceParam::instance().testCase == 1) {
              FEL_ERROR("Error: infarct in fiber directed conductivity case not yet implemented.");
            }
          } else {
            double s = FelisceParam::instance().f0;
            if (FelisceParam::instance().testCase == 2) {
              // m_Matrix[3] = E^s_ij = s*\sigma_i^t grad(phi_i), grad(phi_j)
              m_elementMatBD[m_idKs]->grad_phi_i_grad_phi_j(s*FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);
            } else if (FelisceParam::instance().testCase == 1) {
              std::vector<double> elemFiber;
              getFiberDirection(iel,m_ipotTransMemb, elemFiber);
              // m_Matrix[3] = E^s_ij +=  s*(\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
              m_elementMatBD[m_idKs]->tau_grad_phi_i_tau_grad_phi_j(s*(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMembCurv,0,0,1);
            }
          }
        }
      }
      break;
    }
    case 2: {

      // m_Matrix[0] = grad(phi_i) * grad(phi_j)
      m_elementMatBD[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);

      if (FelisceParam::instance().testCase == 1) {
        std::vector<double> elemFiber;
        getFiberDirection(iel,m_ipotTransMemb, elemFiber);
        // m_Matrix[0] = \sigma_i^t \grad V_m
        m_elementMatBD[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);
        // m_Matrix[0] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
        m_elementMatBD[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMembCurv,0,0,1);
      }

      if (FelisceParam::instance().hasInitialCondition) {
        // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
        m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMatBD[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMembCurv,0,0,1);
      }

      // m_Matrix[1] = phi_i * phi_j
      m_elementMatBD[m_idG]->phi_i_phi_j(1.,*m_fePotTransMembCurv,0,0,1);

      if (!FelisceParam::instance().solveEigenProblem) {
        if (FelisceParam::instance().hasInfarct) {
          // m_Matrix[1] = B_ij = s(x) * phi_i * phi_j
          m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
          m_elementMatBD[m_idGs]->a_phi_i_phi_j(1.,m_elemFieldFhNf0,*m_fePotTransMembCurv,0,0,1);
        } else {
          double s = FelisceParam::instance().f0;
          // m_Matrix[1] = B_ij = s * phi_i * phi_j
          m_elementMatBD[m_idGs]->phi_i_phi_j(s,*m_fePotTransMembCurv,0,0,1);
        }

        // Matrix[2] = E_ij = \sigma_i^t grad(phi_i), grad(phi_j)
        m_elementMatBD[m_idK]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);
        // Matrix[3] = Q_ij = (\sigma_i^t+\sigma_e^t) grad(phi_i), grad(phi_j)
        m_elementMatBD[m_idKie]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotTransMembCurv,0,0,1);
        if (FelisceParam::instance().testCase == 1) {
          std::vector<double> elemFiber;
          getFiberDirection(iel,m_ipotTransMemb, elemFiber);
          // Matrix[2] = Q_ij = (\sigma_i^t+\sigma_e^t) grad(phi_i), grad(phi_j)
          m_elementMatBD[m_idKie]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotTransMembCurv,0,0,1);
          // Matrix[3] = Q_ij += (\sigma_i^l-\sigma_i^t+\sigma_e^l-\sigma_e^t) a vec a grad(phi_i) * grad(phi_j)
          m_elementMatBD[m_idKie]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraFiberTensor-FelisceParam::instance().extraTransvTensor,elemFiber,*m_fePotTransMembCurv,0,0,1);
        }

        if (m_tensorOrder == 3 ) {
          if (FelisceParam::instance().hasInfarct) {
            if (FelisceParam::instance().testCase == 2) {
              // m_Matrix[3] = E^s_ij = s(x) * \sigma_i^t grad(phi_i), grad(phi_j)
              m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMembCurv, iel, m_ipotTransMemb, m_ao, m_dof);
              m_elementMatBD[m_idKs]->a_grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,m_elemFieldFhNf0,*m_fePotTransMembCurv,0,0,1);
            } else if (FelisceParam::instance().testCase == 1) {
              FEL_ERROR("Error: infarct in fiber directed conductivity case not yet implemented.");
            }
          } else {
            double s = FelisceParam::instance().f0;
            if (FelisceParam::instance().testCase == 2) {
              // m_Matrix[3] = E^s_ij = s*\sigma_i^t grad(phi_i), grad(phi_j)
              m_elementMatBD[m_idKs]->grad_phi_i_grad_phi_j(s*FelisceParam::instance().intraTransvTensor,*m_fePotTransMembCurv,0,0,1);
            } else if (FelisceParam::instance().testCase == 1) {
              std::vector<double> elemFiber;
              getFiberDirection(iel,m_ipotTransMemb, elemFiber);
              // m_Matrix[3] = E^s_ij +=  s*(\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
              m_elementMatBD[m_idKs]->tau_grad_phi_i_tau_grad_phi_j(s*(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMembCurv,0,0,1);
            }
          }
        }
      }
      break;
    }
    default:
      FEL_ERROR("Model not defined for ALP solver.");
      break;
    }
  }

}
