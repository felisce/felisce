//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#ifdef FELISCE_WITH_SLEPC
#include "Core/NoThirdPartyWarning/Slepc/slepceps.hpp"
#endif

// Project includes
#include "Solver/steklovBanner.hpp"
#include "Solver/linearProblem.hpp"
#include "InputOutput/io.hpp"
#include "Core/filesystemUtil.hpp"

namespace felisce {
  //! The constructor
  template<class volumeProblem>
  ReducedSteklov<volumeProblem>::ReducedSteklov(PetscMatrix& mass, PetscMatrix& laplacian, MPI_Comm comm, LinearProblemReducedSteklov<volumeProblem>* pb, typename LinearProblemReducedSteklov<volumeProblem>::imgType imType, ChronoInstance::Pointer chrono):
    m_comm(comm), m_mass(mass), m_laplacian(laplacian), m_lpb(pb),m_chronoRS(chrono)
  {
    //! We need to know if we are going to extract the Dirichlet data or the Neumann data from the volume problem solution
    m_imgType=imType;

    //! We try to load the off-line basis from file.

    //! The quantities: rank, nlap, numberofstekloveigenfunctions determine the folder where we hope to find the off-line basis
    //! The folder will be SteklovDataDir/NUMPROC_NBLAP_LOWRANK
    //! By default steklovDataDir = ./
    //! This folder should be a symbolic link to another folder.
    std::stringstream folder;
    folder<<FelisceParam::instance().steklovDataDir
          <<MpiInfo::numProc()<<"_"
          <<FelisceParam::instance().nbOfLaplacianEigenFunction<<"_"
          <<FelisceParam::instance().lowRank<<"/";
    //! if the reading fails, it mains that the off-line basis is not available!
    m_loaded = this->loadFromFile(folder.str());
  }

  template<class volumeProblem>
  bool
  ReducedSteklov<volumeProblem>::loadFromFile(std::string folder) {
    //! We try to load the following files
    //! 1. Steklov eigenVectors:
    //!        eigenVectorsStreklov{0,1,2,...}.mb
    //!
    //! 2. Steklov eigenvalues:
    //!        steklovEigenvalues.txt
    //!
    //! To compute these quantities you have to run the code once with some flags std::set to true (TODO)

    //! (1) steklov eigenVectors
    for ( int i = 0; i < FelisceParam::instance().lowRank; ++i ) {
      PetscVector tmp;
      if (m_lpb->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        m_laplacian.getVecs(FELISCE_PETSC_NULLPTR,tmp);
      }
      std::stringstream filename;
      filename<<"eigenVectorsSteklov"<<i;
      std::stringstream filenameBin;
      filenameBin<< folder << filename.str() << ".mb";
      if ( !filesystemUtil::fileExists(filenameBin.str()) ) {
        std::stringstream msg;
        msg<<"Off-line basis not found in folder: "<<folder<<std::endl;
        PetscPrintf(m_comm,"%s",msg.str().c_str());
        m_eVecW.clear();
        return false;
      }
      if (m_lpb->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        tmp.loadFromBinaryFormat(m_comm, filename.str(), folder);
      }
      m_eVecW.push_back(tmp);
    }
    m_offLineBasisDim = m_eVecW.size();
    std::stringstream msg;
    msg<<"Correctly loaded "<<m_offLineBasisDim<<" basis function."<<std::endl;
    PetscPrintf(m_comm,"%s",msg.str().c_str());

    //! (2) loading the eigenvalues
    if ( ! Tools::loadVectorFromFile(m_eValues,"steklovEigenvalues.txt",folder) )
      return false;
    if ( m_eValues.size() != std::size_t(FelisceParam::instance().lowRank) )
      return false;

    //! Finally a scaling is needed for compatibility with the rest of the code.
    for ( int i(0); i < FelisceParam::instance().lowRank; ++i )
      m_eVecW[i].scale( std::sqrt(m_eValues[i]) );

    return true;

  }
  /*! \brief method that uses slepc to compute the eigenvalues/eigenvectors of the laplace-beltrami
   *  operator at the interface.
   *
   *  the generalized Neumann eigenproblem is solved \f[Av=\lambda Bv\f],
   *  where A is neumann laplacian on the boundary, and B is the corresponding mass matrix.
   */
  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::solveLaplacianEP() {
    // Necessary to compile felisce with Slepc
#ifdef FELISCE_WITH_SLEPC

    FEL_ASSERT(m_laplacianEVec.size()==0);

    m_laplacianEVec.resize( FelisceParam::instance().nbOfLaplacianEigenFunction );

    //! We require more eigenpairs than necessary because Slepc does not always return enough of them
    int requestedNumberOfEigen = static_cast<int>( ceil( FelisceParam::instance().nbOfLaplacianEigenFunction * ( 1. + FelisceParam::instance().percentageOfExtraEig/100. ) ) );

    //! We create an EPS object
    EPS laplacianEigenProblem;
    EPSCreate(m_comm,&laplacianEigenProblem);
    EPSSetOperators(laplacianEigenProblem,m_laplacian.toPetsc(),m_mass.toPetsc());                 //<! We std::set the laplacian and the mass matrix as the two operators of the generalized problem
    EPSSetProblemType(laplacianEigenProblem,EPS_GHEP);                                             //<! We tell Slepc that the problem is GHEP: Generalized Hermitian Problem.
    EPSSetDimensions(laplacianEigenProblem,requestedNumberOfEigen,PETSC_DEFAULT,PETSC_DEFAULT);
    EPSSetWhichEigenpairs(laplacianEigenProblem,EPS_SMALLEST_MAGNITUDE);


    //! Necessary only when using Petsc-3.6 and only in parallel
    ST st; KSP ksp; PC pc;
    EPSGetST  ( laplacianEigenProblem, &st);
    STGetKSP  ( st,                    &ksp);
    KSPGetPC  ( ksp,                   &pc);
    PCSetType ( pc,                    PCREDUNDANT);

    EPSSetFromOptions(laplacianEigenProblem);                                                      //<! more options can be std::set in the command line.

    //! We solve the eigen problem.
    EPSSolve(laplacianEigenProblem);
    if ( FelisceParam::verbose() > 2 ) {
      EPSView(laplacianEigenProblem, PETSC_VIEWER_STDOUT_(m_comm) );
    }

    // We get the number of eigenpairs there were actually computed.
    PetscInt nconv;
    EPSGetConverged(laplacianEigenProblem,&nconv);
    PetscPrintf( m_comm," Number of converged eigenpairs: %d \n", nconv);
    if ( FelisceParam::instance().nbOfLaplacianEigenFunction > nconv )
      FEL_ERROR("Number of converged eigenpairs is no enough!");

    std::vector<double> eigenvalues;
    for ( int iEig(0); iEig < FelisceParam::instance().nbOfLaplacianEigenFunction; ++iEig ) {
      // The parallel layout of the eigen functions is taken from the laplacian matrix.
      m_laplacian.getVecs(FELISCE_PETSC_NULLPTR,m_laplacianEVec[iEig]);

      // We extract the result of the EPS object
      double eigenValue(0);
      EPSGetEigenpair(laplacianEigenProblem,iEig,&eigenValue,nullptr,m_laplacianEVec[iEig].toPetsc(),nullptr);
      eigenvalues.push_back(eigenValue);
      if ( FelisceParam::verbose() > 1 ) {
        std::stringstream master;
        master<<"Laplacian eigenValues: k="<<iEig<<": "<< eigenValue << std::endl;
        PetscPrintf(m_comm,"%s",master.str().c_str());
        MPI_Barrier(m_comm);
      }
      // The eigenvector related to zero is badly approximated by Slepc: we set it by hand
      if ( eigenValue< 1e-7 ) {
        std::stringstream master;
        master<<"****************** Setting the "<<iEig<<" laplacian eigenValue to zero and the corresponding eigenVector to the correct std::vector."<<std::endl;
        PetscPrintf(m_comm,"%s",master.str().c_str());
        MPI_Barrier(m_comm);

        m_lpb->userSetNullSpace( m_laplacianEVec[iEig], iEig );

        // Normalization
        PetscVector aus;
        aus.duplicateFrom( m_laplacianEVec[iEig]);
        mult(m_mass, m_laplacianEVec[iEig], aus);
        double L2norm;
        dot(aus,m_laplacianEVec[iEig],&L2norm);
        m_laplacianEVec[iEig].scale(1./std::sqrt(L2norm) );
      }
      if ( FelisceParam::instance().exportLaplacianEigenVectors ) {
        std::stringstream filename;
        filename<<"laplacianEigenVector"<<iEig;
        m_laplacianEVec[iEig].saveInBinaryFormat(m_comm,filename.str(),FelisceParam::instance().resultDir);
      }
    }
    // Destruction of the EPS object.
    EPSDestroy(&laplacianEigenProblem);
    if ( FelisceParam::instance().exportLaplacianEigenValues ) {
      Tools::saveVectorInFile(eigenvalues,"laplacianEigenValues",FelisceParam::instance().resultDir);
    }
#endif
  }



  /*! \brief For each eigenvector of the surface laplacian its image through the steklov operator is computed
   *
   * The result is stored in m_imgLaplacianEVec
   */
  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::applySteklovOperatorOnLEV(felInt imesh) {

    std::vector<IO::Pointer> io(1);
    if ( FelisceParam::instance().exportOffLineVolumeSolution ) {
      io[0] = felisce::make_shared<IO>(FelisceParam::instance().inputDirectory, FelisceParam::instance().inputFile, FelisceParam::instance().inputMesh[imesh],
                                       FelisceParam::instance().outputMesh[imesh], FelisceParam::instance().meshDir, FelisceParam::instance().resultDir,
                                       "offLineSteklovSolution");
      io[0]->writeMesh(*m_lpb->mesh(imesh));
      io[0]->initializeOutput();
    }

    m_lpb->useSteklovDataBegin();
    FelisceParam::linearSolverVerboseOFF();
    // Banner to improve communications on the screen
    steklovBanner banner(m_lpb->m_dofBD[0/*iBD*/].numLocalDofInterface(), MpiInfo::rankProc(), m_comm, m_laplacianEVec.size() );

    //The matrix 0 is cleared
    m_lpb->clearMatrixRHS( FlagMatrixRHS::only_matrix );
    if ( m_lpb->numberOfMatrices() == 2 ) {
      //Also matrix 1 is cleared if there is one.
      m_lpb->clearMatrix(1);
    }
    // Matrix 0 and 1 are computed and summed toghter into matrix 0.
    m_lpb->assembleVolumeSystem( FlagMatrixRHS::only_matrix );
    banner.initComputation();
    for ( int iLap(0); iLap < FelisceParam::instance().nbOfLaplacianEigenFunction; ++iLap, ++banner) {
      m_lpb->clearMatrixRHS(FlagMatrixRHS::only_rhs);
      m_lpb->setSteklovData(m_laplacianEVec[iLap]);
      m_lpb->assembleVolumeSystem(FlagMatrixRHS::only_rhs);

      m_chronoRS->start();
      m_lpb->solve(MpiInfo::rankProc(),MpiInfo::numProc());
      m_chronoRS->stop();

      m_lpb->gatherSolution();
      double time=iLap;
      if ( FelisceParam::instance().exportOffLineVolumeSolution ) {
        m_lpb->writeSolutionFromVec(m_lpb->sequentialSolution(), MpiInfo::rankProc(), io, time, iLap, std::string("offLineVolumeSolution"));
      }
      FEL_ASSERT(io[0] != nullptr)
      m_imgLaplacianEVec.push_back( m_lpb->getSteklovImg() );
      if (MpiInfo::rankProc() == 0 && FelisceParam::instance().exportOffLineVolumeSolution && io[0]->typeOutput == 1 ) { //TODO and what if proc 0 has no dofs on the boundary?
        io[0]->postProcess(time/*, !m_sameGeometricMeshForVariable*/);
      }
    }
    banner.finalizeComputation();
    // restoring the normal state
    FelisceParam::linearSolverVerboseON();
    m_lpb->useSteklovDataEnd();
  }
  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::steklovEVecEstimate() {
    //! The Laplacian EigenVectors are realigned to be the eigenVectors.
    double coeff;
    std::vector<PetscVector> coefficients;
    this->approximateSteklovEigenVec(coefficients);
    //! The approximated eigenVectors are computed
    for ( std::size_t j(0); j < coefficients.size(); ++j ) {
      PetscVector currentEigenVec;
      currentEigenVec.duplicateFrom( m_laplacianEVec[0]);
      currentEigenVec.set(0.0);
      for ( int i(0); std::size_t(i) < m_laplacianEVec.size(); ++i ) {
        coefficients[j].getValues(1,&i,&coeff);
        currentEigenVec.axpy(coeff,m_laplacianEVec[i]);
      }
      if ( FelisceParam::instance().exportSteklovEigenVectors ) {
        std::stringstream filename;
        filename<<"eigenVectorsSteklov"<<j;
        currentEigenVec.saveInBinaryFormat(m_comm,filename.str(),FelisceParam::instance().resultDir);
      }
      currentEigenVec.scale( std::sqrt(m_eValues[j]) );
      m_eVecW.push_back(currentEigenVec);
      coefficients[j].destroy();
    }
    m_offLineBasisDim = m_eVecW.size();
    m_onLineBasisDim  = 0;
    m_currentBasisDim = m_offLineBasisDim;

    //! for future loading of the off-line bais:

    //! rank, nlap, numberofstekloveigenfunctions determine the folder where we hope to find the off-line basis
    std::stringstream folder;
    folder<<FelisceParam::instance().steklovDataDir
          <<MpiInfo::numProc()<<"_"
          <<FelisceParam::instance().nbOfLaplacianEigenFunction<<"_"
          <<FelisceParam::instance().lowRank;

    std::stringstream createdir;
    createdir<<"mkdir -p "<<FelisceParam::instance().steklovDataDir;

    std::stringstream createlink;
    std::stringstream aus;
    if( FelisceParam::instance().resultDir.c_str()[0] != '/' )
      aus<<"../"<<FelisceParam::instance().resultDir;
    else
      aus<<FelisceParam::instance().resultDir;
    createlink<<"ln -s "<<aus.str() <<" "<<folder.str();

    if ( MpiInfo::rankProc() == 0 ) {

      int ierr=system(createdir.str().c_str());
      if ( ierr>0 )
        std::cout<<"problems with: "<<createdir.str()<<std::endl;
      std::cout<<createlink.str()<<std::endl;
      ierr=system(createlink.str().c_str());
      if ( ierr>0 )
        std::cout<<"problems with: "<<createlink.str()<<std::endl;

    }
  }
  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::assembleReducedMatrix( PetscMatrix& ReducedEigenMatrix ) {
    if ( m_lpb->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {

      //int lowRank = FelisceParam::instance().lowRank;
      int nbOfLapEV = m_laplacianEVec.size();
      PetscVector tmpVec;

      // This matrix is supposed to be small we do not care too much about the efficiency of this code.
      //! The entries of this symmetric matrix are: (M lev_i)^T(S lev_j)
      ReducedEigenMatrix.createSeqDense( PETSC_COMM_SELF, nbOfLapEV, nbOfLapEV, nullptr );
      tmpVec.duplicateFrom(m_laplacianEVec[0]);
      for ( int i(0); i < nbOfLapEV; ++i ) {
        std::vector<double> valuesOfCurrentRow(nbOfLapEV-i,0.0);
        std::vector<int> id(nbOfLapEV-i,0);
        if ( m_imgType == LinearProblemReducedSteklov<volumeProblem>::dirichlet) {
          //! M lev_i
          mult(m_mass,m_laplacianEVec[i],tmpVec );
        } else {// We do not need it in the Neumann case since it is embedded in the residual
          tmpVec=m_laplacianEVec[i];
        }

        for ( int j(i); j < nbOfLapEV; ++j ) {
          double value(0);
          //! (M lev_i)^T(S lev_j)
          dot(tmpVec, m_imgLaplacianEVec[j], &value);
          valuesOfCurrentRow[j-i]=value;
          id[j-i]=j;
        }
        //! the matrix is symmetric: we first std::set the row and then the column
        ReducedEigenMatrix.setValues(        1 , &i     , nbOfLapEV-i, &id[0], &valuesOfCurrentRow[0], INSERT_VALUES);
        ReducedEigenMatrix.setValues( nbOfLapEV-i-1 , &id[1] ,      1,     &i, &valuesOfCurrentRow[1], INSERT_VALUES);
      }
      ReducedEigenMatrix.assembly(MAT_FINAL_ASSEMBLY);

      if (FelisceParam::instance().exportReducedEigenMatrix) {
        std::stringstream filename; filename<<"reducedEigenMatrix"<<MpiInfo::rankProc();
        ReducedEigenMatrix.saveInBinaryFormat(PETSC_COMM_SELF,filename.str(),FelisceParam::instance().resultDir);
      }

    }
  }
  /*!
   */
  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::approximateSteklovEigenVec(std::vector<PetscVector> &coefficients) {
      (void) coefficients;
#ifdef FELISCE_WITH_SLEPC
    if (m_lpb->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      int lowRank = FelisceParam::instance().lowRank;
      //int nbOfLapEV = m_laplacianEVec.size();


      // This matrix is supposed to be small we do not care too much about the efficiency of this code.
      //! The entries of this symmetric matrix are: (M lev_i)^T(S lev_j)
      PetscMatrix ReducedEigenMatrix;
      this->assembleReducedMatrix(ReducedEigenMatrix);

      //! We create an EPS object using the sub-comunicator of the procs that have at least on dof on the boundary.
      //! The matrix is supposed to be small: we do it in serial.
      EPS reducedEigenProblem;
      EPSCreate(PETSC_COMM_SELF,&reducedEigenProblem);
      //! We std::set that matrix as the only operator of the eigenproblem.
      EPSSetOperators(reducedEigenProblem,ReducedEigenMatrix.toPetsc(), nullptr );
      //! We inform slepc that the problem is HEP: Hermitian Problem.
      EPSSetProblemType(reducedEigenProblem,EPS_HEP);
      //! We require the eigenparis associated with the N largest eigenvalues, N=lowRank
      EPSSetDimensions(reducedEigenProblem,lowRank,PETSC_DEFAULT,PETSC_DEFAULT);
      if ( m_imgType == LinearProblemReducedSteklov<volumeProblem>::dirichlet) {
        EPSSetWhichEigenpairs(reducedEigenProblem,EPS_LARGEST_MAGNITUDE);
      } else {
        EPSSetWhichEigenpairs(reducedEigenProblem,EPS_SMALLEST_MAGNITUDE);
      }
      //EPSKrylovSchurSetRestart(reducedEigenProblem,0.5);
      //EPSSetTolerances(reducedEigenProblem,1e-8,500);
      EPSSetFromOptions(reducedEigenProblem);

      //! We solve the eigen problem.
      EPSSolve(reducedEigenProblem);
      if ( FelisceParam::verbose() > 1 ) {
        EPSView(reducedEigenProblem,PETSC_VIEWER_STDOUT_SELF );
      }

      // We get the number of eigenpairs there were actually computed.
      PetscInt nconv; EPSGetConverged(reducedEigenProblem,&nconv);
      PetscPrintf(m_comm," Number of converged eigenpairs for the small eigenvalue problem: %d \n",nconv);
      FEL_ASSERT( lowRank <= nconv );
      coefficients.resize(lowRank);
      m_eValues.resize(lowRank);
      for ( int iEig(0); iEig < lowRank; ++iEig ) {
        // The parallel layout of the eigen functions is taken from the operator of the problem.
        ReducedEigenMatrix.getVecs(FELISCE_PETSC_NULLPTR,coefficients[iEig]);

        // We extract the result of the EPS object
        EPSGetEigenpair(reducedEigenProblem,iEig,&m_eValues[iEig],nullptr,coefficients[iEig].toPetsc(),nullptr);
        if ( FelisceParam::verbose() > 1) {
          std::stringstream master;
          master<<"Eigen Values: "<<iEig<<" , "<< m_eValues[iEig] << std::endl;
          PetscPrintf(m_comm,"%s",master.str().c_str());
          MPI_Barrier(m_comm);
        }
        if (FelisceParam::instance().exportFTOfSteklovEigenVectors && MpiInfo::rankProc() == 0 ) {
          std::stringstream filename; filename<<"fourierTransformOfSteklovEigenVectors"<<iEig;
          coefficients[iEig].saveInBinaryFormat(PETSC_COMM_SELF,filename.str(),FelisceParam::instance().resultDir);
        }
      }
      // destruction of the EPS object.
      EPSDestroy(&reducedEigenProblem);
      ReducedEigenMatrix.destroy();
      if (FelisceParam::instance().exportSteklovEigenValues) {
        Tools::saveVectorInFile(m_eValues,"steklovEigenvalues.txt",FelisceParam::instance().resultDir);
      }
    }
#else
     std::cerr<<"You have to compile this program with the flag FELISCE_WITH_SLEPC"<< std::endl;
#endif
  }


  /*! \brief The action of the low rank steklov on an a given input std::vector
   *
   *  \param[in] in, the given std::vector
   *  \param[out] out, the outcome: it is supposed to be already allocated.
   *
   *  We use the formula \f[ S = V L V^T M \f], where V is matrix with the eigenvectors, L of the eigenvalues and M is the mass.
   *  If the tryAcceleration parameter is on:
   *  First we check if the input is well represented on the current basis, if not it is added to the basis after having solved the volume problem
   */
  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::applyLowRankSteklov(PetscVector& in, PetscVector& out) {
    m_chronoRS->start();
    // Initialization phase, out reset to zero since we comulate on it in the different for loops
    out.set(0.0);

    // Auxiliary double
    double partialResult(0);

    PetscVector tmpVec,ausForNeu;
    tmpVec.duplicateFrom(in);
    ausForNeu.duplicateFrom(in);
    // The quantity M*in is stored since it will used several times
    mult(m_mass,in,tmpVec );


    if ( ! FelisceParam::instance().tryAcceleration ) {
      for ( std::size_t i(0); i < m_offLineBasisDim; ++i ) {
        if ( ! FelisceParam::instance().onlyConstResp ) {
          //! \f[ partialResult = q_i \cdot M in \f]
          dot( m_eVecW[i], tmpVec, &partialResult);
          if ( m_imgType == LinearProblemReducedSteklov<volumeProblem>::neumann) {
            // We pre-multiply the result by -M because we want to pass the equivalent of a residual to the coupled problem
            partialResult *= -1;
            mult(m_mass, m_eVecW[i], ausForNeu);
            out.axpy( partialResult, ausForNeu); //out = - pr M q (we want the residual not the traction ) //DEBUG
          } else {
            //! \f[ out += partialResult q_i \f]
            out.axpy( partialResult, m_eVecW[i]);
          }
        }
      }
    } else {
      if ( m_imgType == LinearProblemReducedSteklov<volumeProblem>::neumann) {
        std::cout<<"WARNING: this part has not been checked for dirichlet 2 neumann problem"<<std::endl;
        std::cout<<__FILE__<<":"<<__LINE__<<std::endl;
      }
      std::size_t curOnLineDim = m_onLineBasisDim;
      //! 1) COMPUTATION OF THE TRUE L2-NORM OF THE INPUT
      // L2 norm of the input std::vector
      double L2Norm(0);
      dot(in,tmpVec, &L2Norm);
      L2Norm=std::sqrt(L2Norm);

      //! 2)    COMPUTATION OF THE FOURIER COEFFICIENTS
      //! 2bis) COMPUTATION OF THE L2-NORM OF THE INPUT'S PROJECTION
      // now we compute the norm of the projection onto the basis
      double projNorm(0);
      // Vector that'll contains all the scalar product w.r.t. the vectors of the basis
      std::vector<double> partialResults( m_currentBasisDim );
      // loop on the offline basis
      for ( std::size_t i(0); i < m_offLineBasisDim; ++i ) {
        // be careful: the eigenVector contains the square root of the eigenValue
        dot(m_eVecW[i], tmpVec, &partialResult);
        partialResults[i]=partialResult;
        projNorm += std::pow( partialResult / std::sqrt( m_eValues[i] ), 2 );
      }
      for ( std::size_t i(0); i< m_onLineBasisDim; ++i ) {
        dot(m_onlineInputs[i],tmpVec, &partialResult);
        partialResults[ i + m_offLineBasisDim ] = partialResult;
        projNorm += std::pow( partialResult, 2 );
      }
      projNorm=std::sqrt(projNorm);

      //! 3) COMPUTATION OF THE RATIO: ||P(X_in)||/|| X_in ||
      double ratio(projNorm/L2Norm);
      if( MpiInfo::rankProc() == 0 )
        std::cout<<"percentage of input vectors that is taken into account by the approximation: "<<ratio*100<<std::endl;

      if ( ratio < FelisceParam::instance().projOnlineRatio )//0.995
      {
        if( MpiInfo::rankProc() == 0 )
          std::cout<<"Too complicated input, the volume problem will be solved!"<<std::endl;
        //! 4) CONSTRUCTION OF A NEW BASIS VECTOR
        // Reconstruct the projection of in, compute in - P(in)
        PetscVector newBasisVector;
        // first we take In and then we remove its projections on the current basis
        newBasisVector.duplicateFrom(in);
        newBasisVector.copyFrom(in);
        for ( std::size_t i(0); i< m_offLineBasisDim; ++i ) {
          newBasisVector.axpy( - partialResults[i] / m_eValues[i], m_eVecW[i]);
        }
        for ( std::size_t i(0); i<m_onLineBasisDim; ++i ) {
          newBasisVector.axpy( - partialResults[ i + m_eVecW.size() ], m_onlineInputs[i] );
        }
        // renormalize it, with respect to Mass (we know the norm by difference)
        newBasisVector.scale( 1./( std::sqrt(L2Norm*L2Norm - projNorm*projNorm) ) );

        //--- DEBUG
        // double check;
        // PetscVector tmpVec2;
        // //Computing L2-norm
        // tmpVec2.duplicateFrom(newBasisVector);
        // mult(m_mass,newBasisVector,tmpVec2 );
        // dot(tmpVec2, newBasisVector, &check);
        // tmpVec2.destroy();
        // //checking that it has been normalized
        // if ( MpiInfo::rankProc() == 0 )
        //   std::cout<<"added a new input Vector, norm should be one and it is: "<< check << std::endl;
        // FEL_ASSERT( Tools::almostEqual(check,1.,1.e-12) );
        //--- DEBUG

        //! 5) ADDITION OF THE NEW VECTOR TO THE ONLINE BASIS
        m_onlineInputs.push_back(newBasisVector);
        ++m_onLineBasisDim;
        ++m_currentBasisDim;

        //! 6) COMPUTATION OF THE IMAGE OF THE NEW VECTOR
        m_lpb->useSteklovDataBegin();
        m_lpb->setSteklovData( newBasisVector );
        m_lpb->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
        m_lpb->assembleVolumeSystem(FlagMatrixRHS::matrix_and_rhs);
        m_lpb->solve(MpiInfo::rankProc(),MpiInfo::numProc());
        out = m_lpb->getSteklovImg();
        m_lpb->useSteklovDataEnd();
        //! 7) SAVING OF THE IMAGE OF THE NEW VECTOR
        // save in a list along with its image.
        PetscVector imageOfNewBasisVec;
        imageOfNewBasisVec.duplicateFrom(out);
        imageOfNewBasisVec.copyFrom(out);
        m_onlineOutputs.push_back(imageOfNewBasisVec);
        //! 8) COMPUTATION OF THE CURRENT IMAGE ON THE NEW BASIS
        // the output is rescaled with the correct norm
        out.scale(std::sqrt(L2Norm*L2Norm - projNorm*projNorm));
      }
      //! 9) COMPUTATION OF THE CURRENT IMAGE WITH THE REST OF THE BASIS
      for ( std::size_t i(0); i < m_offLineBasisDim; ++i ) {
        out.axpy( partialResults[i], m_eVecW[i]);
      }
      for ( std::size_t i(0); i < curOnLineDim; ++i ) {
        out.axpy( partialResults[ i + m_offLineBasisDim ], m_onlineOutputs[i]);
      }
    }
    if ( m_constantResponse.size() == 1) {
      //TODO: case with multiple constant responses
      double coeff = m_lpb->userInletPressure() * ( ( m_imgType == LinearProblemReducedSteklov<volumeProblem>::neumann ) ? -1 : 1 );
      out.axpy( coeff,m_constantResponse[0]);
    }
    m_chronoRS->stop();
  }

  template<class volumeProblem>
  void
  ReducedSteklov<volumeProblem>::exportAllEig( felInt imesh ) {

    std::vector<IO::Pointer> ioSteklov = {felisce::make_shared<IO>(FelisceParam::instance().inputDirectory, FelisceParam::instance().inputFile, FelisceParam::instance().inputMesh[imesh],
                                          FelisceParam::instance().outputMesh[imesh], FelisceParam::instance().meshDir, FelisceParam::instance().resultDir,
                                          "steklovEigen")};

    std::vector<IO::Pointer> ioLaplacian = {felisce::make_shared<IO>(FelisceParam::instance().inputDirectory, FelisceParam::instance().inputFile, FelisceParam::instance().inputMesh[imesh],
                                            FelisceParam::instance().outputMesh[imesh], FelisceParam::instance().meshDir, FelisceParam::instance().resultDir,
                                            "laplacianEigen")};

    ioSteklov[0]   ->writeMesh(*m_lpb->m_mesh[0]);
    ioLaplacian[0] ->writeMesh(*m_lpb->m_mesh[0]);

    ioSteklov[0]   ->initializeOutput();
    ioLaplacian[0] ->initializeOutput();

    PetscVector ausPara,ausSeq;
    ausPara.duplicateFrom(m_lpb->solution());
    ausSeq.duplicateFrom(m_lpb->sequentialSolution());

    int nEVECWl = m_eVecW.size();
    int nEVECW(0);
    MPI_Allreduce( &nEVECWl,&nEVECW,1,MPI_INT,MPI_MAX,MpiInfo::petscComm());

    if ( m_eValues.size() < (std::size_t)nEVECW )
      m_eValues.resize(nEVECW);

    for ( std::size_t k(0); k<(std::size_t)nEVECW; ++k) {
      m_lpb->dofBD(/*iBD*/0).extendOnVolume( ausPara, m_eVecW[k]);
      m_lpb->gatherVector(ausPara,ausSeq);
      ausSeq.scale( 1./std::sqrt( m_eValues[k] ) );
      double time=k;
      m_lpb->writeSolutionFromVec(ausSeq, MpiInfo::rankProc(), ioSteklov, time, k, std::string("steklovEigenVec"));
      if (MpiInfo::rankProc() == 0) {
        if ( ioSteklov[0]->typeOutput == 1 ) // 1 : ensight{
          ioSteklov[0]->postProcess(time/*, !m_sameGeometricMeshForVariable*/);
      }
    }


    int nLAPl = m_laplacianEVec.size();
    int nLAP(0);
    MPI_Allreduce( &nLAPl,&nLAP,1,MPI_INT,MPI_MAX,MpiInfo::petscComm());

    for ( std::size_t k(0); k<(std::size_t)nLAP; ++k) {
      m_lpb->dofBD(/*iBD*/0).extendOnVolume( ausPara, m_laplacianEVec[k]);
      m_lpb->gatherVector(ausPara,ausSeq);
      double time=k;
      m_lpb->writeSolutionFromVec(ausSeq, MpiInfo::rankProc(), ioLaplacian, time, k, std::string("laplacianEigenVec"));
      if (MpiInfo::rankProc() == 0) {
        if ( ioLaplacian[0]->typeOutput == 1 ) // 1 : ensight{
          ioLaplacian[0]->postProcess(time/*, !m_sameGeometricMeshForVariable*/);
      }
    }
  }
}

