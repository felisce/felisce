//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes
#include <limits>
#include <numeric>

// External includes
#include <petsc/private/snesimpl.h>
#include "Core/NoThirdPartyWarning/Petsc/sys.hpp"

// Project includes
#include "Core/key_hash.hpp"
#include "Model/model.hpp"
#include "Solver/linearProblem.hpp"
#include "Solver/bdf.hpp"
#include "Tools/fe_utilities.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "PETScInterface/SNESInterface.hpp"

namespace felisce
{

void SnesContext::initialize(LinearProblem& aLinearProblem, int aRankProc, int aSizeProc,
                             ApplyNaturalBoundaryConditionsType aDo_apply_natural, FlagMatrixRHS flag) 
{
  linearProblem = &aLinearProblem;
  rankProc = aRankProc;
  sizeProc = aSizeProc;
  do_apply_natural = aDo_apply_natural;
  flag_matrix_rhs = flag;
}

/***********************************************************************************/
/***********************************************************************************/

LinearProblem::LinearProblem(const std::string name, const std::size_t numMat, const std::size_t numVec) : 
  m_name(name)
{
  m_matrices.resize(numMat);
  m_vectors.resize(numVec);
}

/***********************************************************************************/
/***********************************************************************************/

LinearProblem::~LinearProblem() 
{
  if(m_initMappingElem) {
    for (std::size_t i = 0; i < m_mappingElem.size(); ++i)
      ISLocalToGlobalMappingDestroy(&m_mappingElem[i]);
  }

  if(m_initMappingElemSupport && FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
      ISLocalToGlobalMappingDestroy(m_mappingElemSupportPerUnknown[iUnknown]);
  }

  if(m_initMappingNodes) {
    ISLocalToGlobalMappingDestroy(&m_mappingNodes);
  }

  if(m_initMappingLocalFelisceToGlobalPetsc) {
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
      ISLocalToGlobalMappingDestroy(&m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown]);
  }

  if(m_initAO) {
    AODestroy(&m_ao);
  }

  if(FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
    if(m_areOriginalSupportDofMeshAllocated) {
      for(std::size_t i=0; i<m_supportDofUnknownOriginal.size(); ++i) {
        delete m_supportDofUnknownOriginal[i];
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, MPI_Comm& comm, bool doUseSNES)
{
  IGNORE_UNUSED_ARGUMENT(comm);
  m_mesh = mesh;

  m_dimesion = m_mesh[0]->spatialDim();
  if ( ! std::all_of(m_mesh.begin(), m_mesh.end(), [dim = m_dimesion](auto mesh){ return dim == mesh->spatialDim(); } ) ) {
    auto& r_instance = FelisceParam::instance(this->instanceIndex());

    if ( r_instance.dimension.size() > m_identifier_problem )
      m_dimesion = r_instance.dimension[m_identifier_problem];
    else
      FEL_ERROR("Meshes have different dimensions and problem dimension not provided by input file");
  }

  m_pSNESInterface->doUseSNES() = doUseSNES;
  m_verbosity = FelisceParam::verbose();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::definePhysicalVariable(const std::vector<PhysicalVariable>& listVariable, const std::vector<std::size_t>& listNumComp) 
{
  for (std::size_t iVar = 0; iVar < listVariable.size(); iVar++)
    m_listVariable.addVariable(listVariable[iVar],listNumComp[iVar],this->instanceIndex());

  if (listVariable.size() != m_listVariable.size()) {
    FEL_ERROR("Problem with your input file. Not enough physical variable defined for this problem!");
  }

  //link between unknown of the linear system and variable defined in the problem.
  Variable variable;
  bool unknownFound = false;
  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
    for (std::size_t iVar = 0; iVar < listVariable.size() && unknownFound == false; iVar++) {
      variable = m_listVariable[iVar];
      if ( m_listUnknown[iUnknown] == variable.physicalVariable()) {
        m_listUnknown.idVariable(iUnknown) = iVar;
        m_listVariable.listIdUnknownOfVariable(iVar) = static_cast<int>(iUnknown);
        m_meshUnknown.push_back(m_listVariable[iVar].idMesh());
        unknownFound = true;
      }
    }
    unknownFound = false;
  }

  //set current mesh
  m_currentMesh = m_meshUnknown.front();
  if ( std::adjacent_find( m_meshUnknown.begin(), m_meshUnknown.end(), std::not_equal_to<std::size_t>()) != m_meshUnknown.end() )
    FEL_WARNING( "Unknows defined over different meshes: m_currentMesh = " + std::to_string(m_currentMesh) );


  if (m_verbosity) {
    PetscPrintf(MpiInfo::petscComm(), "\n/========== Information about the linear system ==========/\n");
    PetscPrintf(MpiInfo::petscComm(), "<%s>.\n", m_name.c_str());
    PetscPrintf(MpiInfo::petscComm(), "Unknowns are: ");
    for (std::size_t iUnkonwn = 0; iUnkonwn < m_listUnknown.size(); iUnkonwn++) {
      PetscPrintf(MpiInfo::petscComm(), "<%s> ", m_listVariable[m_listUnknown.idVariable(iUnkonwn)].name().c_str());
    }
    PetscPrintf(MpiInfo::petscComm(), "\nPhysical variables associated: ");
    std::stringstream msg;
    m_listVariable.print(m_verbosity,msg);
    PetscPrintf(MpiInfo::petscComm(), "%s",msg.str().c_str());
  }

  m_listUnknown.setDefaultMask(m_listVariable);
  m_listUnknown.print(1);
  m_numDofUnknown.resize(m_listUnknown.size());
  m_numDofLocalUnknown.resize(m_listUnknown.size());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeDof(int numProc, int idProc) 
{
  int idVar, iMesh;

  /// For each unknown, create the global support dof
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    idVar = m_listUnknown.idVariable(iUnknown);
    iMesh = m_listVariable[idVar].idMesh();
    m_supportDofUnknown.emplace_back(m_listVariable[idVar], m_mesh[iMesh]);
    
    if(FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
      if(!m_areOriginalSupportDofMeshAllocated) {
        m_supportDofUnknownOriginal.push_back(new SupportDofMesh(m_listVariable[idVar], m_mesh[iMesh]));

        if(iUnknown == m_listUnknown.size() - 1)
          m_areOriginalSupportDofMeshAllocated = true;
      }
    } else
      m_supportDofUnknownOriginal.push_back(&supportDofUnknown(iUnknown));

    // Print info on the global supportDofMesh
    if (m_verbosity) {
      PetscPrintf(MpiInfo::petscComm(), "\n/======= Global support dof mesh associate to the unknown: <%s>.======/\n", m_listVariable[m_listUnknown.idVariable(iUnknown)].name().c_str());
      supportDofUnknown(iUnknown).print(m_verbosity);
    }
  }

  initSupportDofDerivedProblem();

  // print info
  if (m_verbosity && FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
      PetscPrintf(MpiInfo::petscComm(), "\n/======= NEW Global support dof mesh associate to the unknown: <%s>.======/\n", m_listVariable[m_listUnknown.idVariable(iUnknown)].name().c_str());
      supportDofUnknown(iUnknown).print(m_verbosity);
    }
  }

  /// Copy the list of unknowns, variables and all the support meshes in m_dof.
  std::vector<SupportDofMesh*> pSupportDofUnknown(m_supportDofUnknown.size(), nullptr);
  for(std::size_t iUnknown=0; iUnknown < m_supportDofUnknown.size(); ++iUnknown)
    pSupportDofUnknown[iUnknown] = &supportDofUnknown(iUnknown);

  m_dof.setDof(m_listUnknown, m_listVariable, pSupportDofUnknown);

  /// Compute numDof and numDofPerUnknown in m_dof.
  /// Initialize pattern, random repartition on processors before call ParMetis.
  m_dof.initializePattern(numProc, idProc);

  /// Copy numDof and numDofPerUnknown here.
  m_numDof = m_dof.numDof();
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
    m_numDofUnknown[iUnknown] = m_dof.numDofPerUnknown()[iUnknown];

  // Print information about dof
  if (m_verbosity) {
    PetscPrintf(MpiInfo::petscComm(), "\n/========== Information about dof ==========/\n");
    std::stringstream msg;
    m_dof.print(m_verbosity,msg);
    PetscPrintf(MpiInfo::petscComm(), "%s\n",msg.str().c_str());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getSupportCoordinate(ElementType& eltType, felInt iel, felInt idSupport, int iUnknown, Point& pt) 
{
  // Even if the support dof are duplicated, the coordinates are the same. We can take the coordinate of the first element support for this element.
  felInt idEltInSupportLocal;
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(eltType, iel, idEltInSupportLocal);
  FEL_ASSERT(m_supportDofUnknownLocal[iUnknown].iEle()[idEltInSupportLocal] + idSupport < int( m_supportDofUnknownLocal[iUnknown].iSupportDof().size() ) );
  felInt id = m_supportDofUnknownLocal[iUnknown].iSupportDof()[m_supportDofUnknownLocal[iUnknown].iEle()[idEltInSupportLocal] + idSupport];
  if ( FelisceParam::instance(this->instanceIndex()).FusionDof ) {
    // IMPORTANT: Here have to std::unordered_map back the supportDof to the original unfusioned (mesh) data.
    // This also applies to other type of BC. TO BE DONE
    auto it = find(supportDofUnknown(iUnknown).listPerm().begin(),supportDofUnknown(iUnknown).listPerm().end(),id);
    id = it- supportDofUnknown(iUnknown).listPerm().begin();
  }
  pt = m_supportDofUnknownLocal[iUnknown].listNode()[id];
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getSupportCoordinate(felInt idElt, felInt idSupport, int iUnknown, Point& pt) 
{
  // Even if the support dof are duplicated, the coordinates are the same. We can take the coordinate of the first element support for this element.
  felInt idEltInSupportLocal;
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(idElt, idEltInSupportLocal);
  felInt id = m_supportDofUnknownLocal[iUnknown].iSupportDof()[m_supportDofUnknownLocal[iUnknown].iEle()[idEltInSupportLocal] + idSupport];
  if ( FelisceParam::instance(this->instanceIndex()).FusionDof ) {
    // IMPORTANT: Here have to std::unordered_map back the supportDof to the original unfusioned (mesh) data.
    // This also applies to other type of BC. TO BE DONE
    auto it = find(supportDofUnknown(iUnknown).listPerm().begin(),supportDofUnknown(iUnknown).listPerm().end(),id);
    id = it- supportDofUnknown(iUnknown).listPerm().begin();
  }
  pt = m_supportDofUnknownLocal[iUnknown].listNode()[id];
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::cutMesh() 
{
  /// partitionDof: partition the dof with parmetis and the elements.
  partitionDof(static_cast<idx_t>(MpiInfo::numProc()),MpiInfo::rankProc(), MpiInfo::petscComm());
  //partitionDof_Petsc(static_cast<idx_t>(MpiInfo::numProc()), rankProc, comm);

  /// set local mesh and local supportDofMeshes
  setLocalMeshAndSupportDofMesh(MpiInfo::rankProc());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::partitionDof( idx_t numPart, int rankPart, MPI_Comm comm) 
{
  /*!
   Partitioning of the dof.
   */

  /*
   Declaration of the parameters for ParMetis.
   idx_t define the type for ParMetis int.
   */

  // Input parameters
  idx_t   num_flag = 0;                        // C-style array index.
  idx_t   ncon = 1;                            // number of weight per vertices of the graph.
  idx_t   wgtFlag = 0;                         // No weight in the graph.
  idx_t   option[3];                           // verbose level and random seed for parmetis.
  idx_t   vertexdist[numPart + 1];             // Repartition of dof between the processes.

  real_t  ubvec = static_cast<real_t>(1.05);   // Imbalance tolerance
  real_t  tpwgts[numPart*ncon];                // See parmetis manual for description

  // Output parameters
  idx_t   edgecut;                             // Number of edge in the graph that are cut by the partitioning.
  int numRows = m_dof.pattern().numRows();
  std::vector<idx_t> dofPartitionning(numRows, 0);  // The partition of the dof.

  /*
   Initialisation of the parameters.
   */

  // vertexdist
  // vertexdist[j] = sum(size in process i), for i=0,..,j-1.
  // initialize the first value (always 0)
  vertexdist[0] = 0;

  // Gather all the size in vertexdist, starting at index 1 (because vertexdist[0] is 0)
  MPI_Allgather(&numRows, 1, MPI_INT, &vertexdist[1], 1, MPI_INT, comm);

  // Compute the partial sum
  for (int i = 1; i < numPart; i ++)
    vertexdist[i + 1] += vertexdist[i];

  // option
  option[0] = 1;     // 0 = default values, 1 = user defined, to print all that ugly information.
  option[1] = 0;     // verbose level.
  option[2] = 100;   // random seed.

  // tpwgts
  for ( felInt i= 0; i < numPart*ncon; i++)
    tpwgts[i] = static_cast<real_t>(1.)/numPart;

  /*
   Parmetis call
   */
  MPI_Comm comm_bis = PETSC_COMM_WORLD;
  ParMETIS_V3_PartKway(vertexdist, &m_dof.pattern().rowPointer(0), &m_dof.pattern().columnIndex(0), nullptr, nullptr, &wgtFlag, &num_flag, &ncon, &numPart, tpwgts, &ubvec, option, &edgecut, dofPartitionning.data(), &comm_bis);

  /*
   Gather the partitioning to all process
   */
  m_dofPart.resize(m_dof.numDof(), 0);

  felInt recvcount[numPart];
  for(int i=0; i<numPart; i++)
    recvcount[i] = vertexdist[i+1] - vertexdist[i];

  MPI_Allgatherv(dofPartitionning.data(), dofPartitionning.size(), MPI_INT, m_dofPart.data(), recvcount, vertexdist, MPI_INT, comm);

  // Count the number of local dofs
  m_numDofLocal = 0;
  for (felInt i = 0; i < m_dof.numDof(); i++) {
    if (m_dofPart[i] == rankPart)
      m_numDofLocal++;
  }

  /*
   Build the matrix pattern
   */
  m_dof.buildPattern(rankPart, m_dofPart.data());

  // Pattern operations:
  // add complementary pattern if there is one
  if(m_dof.patternIsChanged()) {
    m_dof.mergeLocalCompPattern(MpiInfo::petscComm(), numPart, rankPart, m_dofPart);

    // free memory, clear the complementary pattern now that is has been merge with the pattern
    m_dof.clearPatternC();
  }

  /*!
   Global ordering: AO is an application between global mesh ordering and global ordering of Petsc
   AO: globalordering to globalordering
   the action is:
   - Take the global index associated to the i-th proc
   - Create an application to a new global index in order to have contiguous global indexes on the processor

   both global ordering are on input (loc2glob and pordering)
   AOCreateBasic(comm, m_numDofLocal, loc2Glob, pordering, &m_ao);

   Example:
   Proc 0:
   glob2loc = [0 1 2 3 8]
   pordering =[0 1 2 3 4]

   Proc 1:
   glob2loc = [4 5 6 7]
   pordering =[5 6 7 8]
   */

  felInt rstart;
  felInt cptDof = 0;

  felInt loc2Glob[m_numDofLocal];
  for (felInt i = 0; i < m_numDofLocal; i++)
    loc2Glob[i] = 0;

  for (felInt i = 0; i < m_dof.numDof(); i++) {
    if (m_dofPart[i] == rankPart) {
      // cptDof (local) -> i (global)
      loc2Glob[cptDof] = i;
      cptDof++;
    }
  }

  // inclusive sum (rank_0 = numdoflocal_0, rank_i = sum_j=0^rank_i numdoflocal_i)
  MPI_Scan(&m_numDofLocal, &rstart, 1, MPIU_INT, MPI_SUM, comm);
  rstart -= m_numDofLocal;

  felInt pordering[m_numDofLocal];
  for (felInt i= 0; i < m_numDofLocal; i++) {
    // pordering: so global indexes are contiguous on a processor; pordering: global->global
    pordering[i] = rstart + i;
  }

  AOCreateBasic(comm, m_numDofLocal, loc2Glob, pordering, &m_ao);
  //AOView(m_ao,PETSC_VIEWER_STDOUT_WORLD);
  m_initAO = true;

  /*!
   Compute the element repartition.
   An element is owned by a process k if most of its dof are on process k.
   In case of a draw, the element belongs to the process with the smallest id.

   matteo feb 14, changed the way in which surface elements are associated to different proc: now a surface element is associated to the proc who owns also the volume element.
   the algorithm implemented works correctly with triangles, but no with quadrilateral and not in 2D: in such a case some surface elements will be associated to the wrong proc.
   this may cause some problems when dealing with complicated BC such as EmbedFSI
   It can classify uncorrectly the boundary element in 2D and the quadrangular 3D elements.

   benoit sept 14, a surface boundary element is always associated to the proc of its volume element. If there are surface element inside the domain (not boundary), they will
   be associated to the proc of one of their volume element (the last one in the loop).

   matteo - benoit august 15, works in 2D and 3D.
   */

  /// Definition of the vector indexes considered
  typedef std::vector<felInt> VectorIndexType;

  /// Definition of the hasher considered
  typedef VectorIndexHasher<VectorIndexType> VectorIndexHasherType;

  /// Definition of the key comparor considered
  typedef VectorIndexComparor<VectorIndexType> VectorIndexComparorType;

  // Declaration of some variables
  felInt max;                                                     // used to compute indice_max.
  felInt indice_max;                                              // id of the process that owns the element.
  felInt idDof;                                                   // Local number of a dof.
  felInt ielSupportDof;                                           // Global number of a dof.
  felInt numElemsPerRef = 0;                                      // Number of elements per label.
  int    idVar;                                                   // id of a physical variable.
  felInt numEltTot;                                               // Total number of element (to be computed).
  felInt numEltVol;                                               // total number of volume element.
  felInt numEltPerType[GeometricMeshRegion::m_numTypesOfElement]; // Total number of element per type (to be computed).
  felInt numDofPerProcs[numPart];                                 // Number of dofs per process. //per element to be computed for each element and then cleared
  VectorIndexType vecSupport;                                     // Ids of the support elements of a mesh element
  GeometricMeshRegion::ElementType eltType;                       // type of an element
  VectorIndexType ptOfElem;                                       // id of the vertices of an element
  VectorIndexType ptOfBoundaryElem;                               // id of the vertices of a boundary element
  std::unordered_map<VectorIndexType, felInt, VectorIndexHasherType, VectorIndexComparorType> bndElements;         // ids of the points of the boundary elements
  std::vector<int> listUnknown;
  std::size_t iUnknown;

  // Initialization
  m_eltPart.resize(m_mesh.size());
  for (std::size_t iMesh = 0; iMesh < m_mesh.size(); ++iMesh){

    m_eltPart[iMesh].resize(m_mesh[iMesh]->getNumElement(), 0);

    bndElements.clear();
    listUnknown.clear();
    for (iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++)
      if ( iMesh == m_listVariable[m_listUnknown.idVariable(iUnknown)].idMesh() ) 
        listUnknown.emplace_back(iUnknown);

    if ( listUnknown.size() == 0 )
      continue; 

    for (int ityp = 0; ityp <  GeometricMeshRegion::m_numTypesOfElement; ityp++)
      numEltPerType[ityp] = 0;

    numEltTot = 0;
    numEltVol = m_mesh[iMesh]->getNumDomainElement();

    const std::vector<ElementType>& bagElementTypeDomain = m_mesh[iMesh]->bagElementTypeDomain();
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_mesh[iMesh]->bagElementTypeDomainBoundary();
    std::vector<felInt>::iterator bndRefIt;

    // first, a loop on the surface elements to store them in bndElements
    for (std::size_t iEltType = 0; iEltType < bagElementTypeDomainBoundary.size(); ++iEltType) {
      eltType =  bagElementTypeDomainBoundary[iEltType];
      ptOfBoundaryElem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType]);
  
      for(auto itRef = m_mesh[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_mesh[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        numElemsPerRef = itRef->second.second;

        // notBoundarySurfaceLabels = list of surfaces/edges that are part of the domain but are not boundaries of volumes/surfaces
        bndRefIt = find(FelisceParam::instance(this->instanceIndex()).notBoundarySurfaceLabels.begin(), FelisceParam::instance(this->instanceIndex()).notBoundarySurfaceLabels.end(), itRef->first);

          // If current element IS a boundary element
        if(bndRefIt == FelisceParam::instance(this->instanceIndex()).notBoundarySurfaceLabels.end()) {
          for (felInt iel = 0; iel < numElemsPerRef; iel++) {
            // get the ids of the vertices of the element
            m_mesh[iMesh]->getOneElement(eltType, numEltPerType[eltType], ptOfBoundaryElem, 0);

            // sort them
            std::sort(ptOfBoundaryElem.begin(),ptOfBoundaryElem.end());

            // add the element in the unordered_map
            bndElements[ptOfBoundaryElem] = numEltTot + numEltVol;

            // increment the counters
            ++numEltTot;
            ++numEltPerType[eltType];
          }
        } else { // If current element IS NOT a boundary element
          // for each local element with the current label
          for (felInt iel = 0; iel < numElemsPerRef; iel++) {

            // Clear local variables
            max = 0;
            indice_max = 0;
            for ( int j = 0; j < numPart; j++)
              numDofPerProcs[j] = 0;

            // for each unknown of the linear system
            for (auto itUnknown = listUnknown.begin(); itUnknown < listUnknown.end(); itUnknown++) { 
              iUnknown = *itUnknown;

              // get the global id of the current element
              idVar = m_listUnknown.idVariable(iUnknown);
              m_supportDofUnknown[iUnknown].getIdElementSupport(eltType, numEltPerType[eltType], vecSupport);

              // loop over all the support elements
              for (std::size_t it = 0; it < vecSupport.size(); it++) {
                // get the id of the support
                ielSupportDof = vecSupport[it];

                // for each support dof of the current element
                for (felInt iSupport = 0; iSupport < m_supportDofUnknown[iUnknown].getNumSupportDof(ielSupportDof); iSupport++) {

                  // for each components of the current unknown
                  for (std::size_t iComp = 0; iComp < m_listVariable[idVar].numComponent(); iComp++) {
                    // get the global id of the dof and increased numDofPerProcs
                    m_dof.loc2glob(ielSupportDof, iSupport, idVar, iComp, idDof);
                    ++numDofPerProcs[m_dofPart[idDof]];
                  }
                }
              }
            }

            // Find the rank of the process that owns the most dof in the current element.
            for (int j = 0; j < numPart; j++) {
              if (max < numDofPerProcs[j]) {
                max = numDofPerProcs[j];
                indice_max = j;
              }
            }

            // Fill m_eltPart
            m_eltPart[iMesh][numEltTot + numEltVol] = indice_max;

            ++numEltTot;
            ++numEltPerType[eltType];
          }
        }
      }
    }

    // Initialization for the domain elements
    felInt numPointPerBdEle = 0;
    felInt numBdElePerDomainElem = 0;
    numEltTot = 0;

    // For each domain element type
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      eltType =  bagElementTypeDomain[i];
      ptOfElem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType]);
      numBdElePerDomainElem = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

      // for each label where the current element type can be found
      for(auto itRef = m_mesh[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_mesh[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        numElemsPerRef = itRef->second.second;

        // for each local element with the current label
        for (felInt iel = 0; iel < numElemsPerRef; iel++) {

          // Clear local variables
          max = 0;
          indice_max = 0;
          for ( int j = 0; j < numPart; j++)
            numDofPerProcs[j] = 0;

          // for each unknown of the linear system
          for (auto itUnknown = listUnknown.begin(); itUnknown < listUnknown.end(); itUnknown++) { 
            iUnknown = *itUnknown;

            // get the global id of the current element
            idVar = m_listUnknown.idVariable(iUnknown);
            supportDofUnknown(iUnknown).getIdElementSupport(eltType, numEltPerType[eltType], vecSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vecSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vecSupport[it];

              // for each support dof of the current element
              for (felInt iSupport = 0; iSupport <  supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof); iSupport++) {

                // for each components of the current unknown
                for (std::size_t iComp = 0; iComp < m_listVariable[idVar].numComponent(); iComp++) {
                  // get the global id of the dof and increased numDofPerProcs
                  m_dof.loc2glob(ielSupportDof, iSupport, idVar, iComp, idDof);
                  ++numDofPerProcs[m_dofPart[idDof]];
                }
              }
            }
          }

          // Find the rank of the process that owns the most dof in the current element.
          for (int j = 0; j < numPart; j++) {
            if (max < numDofPerProcs[j]) {
              max = numDofPerProcs[j];
              indice_max = j;
            }
          }

          // Fill m_eltPart
          m_eltPart[iMesh][numEltTot] = indice_max;

          // find the boundary element of this domain element
          // get the current volume element
          m_mesh[iMesh]->getOneElement(eltType, numEltPerType[eltType], ptOfElem, 0);

          // for each of the faces
          for (felInt iBdEle = 0; iBdEle < numBdElePerDomainElem; iBdEle++) {
            numPointPerBdEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numPointPerBdEle( iBdEle );

          // save the IDs of the points of the face
          ptOfBoundaryElem.resize( numPointPerBdEle, 0);
          for (int iptBdEle = 0; iptBdEle < numPointPerBdEle; ++iptBdEle ) {
            // global point numbering of the local face points:
            ptOfBoundaryElem[iptBdEle] = ptOfElem[ GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->pointOfBdEle(iBdEle, iptBdEle) ];
            }

            // sort them
            std::sort(ptOfBoundaryElem.begin(), ptOfBoundaryElem.end());

            // if they are the same IDs as of those of the surface element then assign the proc of the volume element to the surface element
            const auto bndEltIt = bndElements.find(ptOfBoundaryElem);
            if(bndEltIt != bndElements.end()) {
              // this face is one of the boundary elements
              m_eltPart[iMesh][bndEltIt->second] = indice_max;
            } 
          }

          // Increment the counter
          ++numEltTot;
          ++numEltPerType[eltType];
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/
/*
void LinearProblem::partitionDof_Petsc( idx_t numPart, int rankPart, MPI_Comm comm) 
{
  // ================= Temporary to get rid of warning about usuned arguments...
  std::cerr << "BEWARE: two arguments of this function were commented" << std::endl;
  IGNORE_UNUSED_ARGUMENT(numPart);
  IGNORE_UNUSED_ARGUMENT(comm);
  // =================

  PetscMatrix Adj;
  MatPartitioning part;
  IS is, isg;
  AO ao;
  PetscInt* ia;
  PetscInt* ja;
  const std::size_t iaSize = m_dof.pattern().numRows() + 1;
  const std::size_t jaSize = m_dof.pattern().numNonzeros();
  PetscMalloc(iaSize*sizeof(PetscInt),&ia);
  PetscMalloc(jaSize*sizeof(PetscInt),&ja);

  for (std::size_t i=0; i<iaSize; ++i)
    ia[i] = m_dof.pattern().rowPointer(i);
  for (std::size_t i=0; i<jaSize; ++i)
    ja[i] = m_dof.pattern().columnIndex(i);

  const PetscInt m = iaSize-1;
  const PetscInt n = m_dof.numDof();

  std::cout << " ===== Processor: " << rankPart << std::endl;
  std::cout <<  " m = " << m << std::endl;
  std::cout <<  " n = " << n << std::endl;
  for (std::size_t i=0; i<iaSize; ++i)
    std::cout << ia[i] <<  "  " ;
  std::cout << std::endl;
  for (std::size_t i=0; i<jaSize; ++i)
    std::cout << ja[i] <<  "  " ;
  std::cout << std::endl;

  Adj.createMPIAdj(MpiInfo::petscComm(),m,n,ia,ja,FELISCE_PETSC_NULLPTR);
  MatPartitioningCreate(MpiInfo::petscComm(),&part);
  MatPartitioningSetAdjacency(part,Adj.toPetsc());
  MatPartitioningSetFromOptions(part);
  MatPartitioningApply(part,&is);
  ISPartitioningToNumbering(is,&isg);
  ISView(is,PETSC_VIEWER_STDOUT_WORLD);
  ISView(isg,PETSC_VIEWER_STDOUT_WORLD);

  AOCreateBasicIS(isg,FELISCE_PETSC_NULLPTR,&ao);
  // AOView(ao,PETSC_VIEWER_STDOUT_WORLD);

  MatPartitioningDestroy(&part);
  Adj.destroy();

  PetscFinalize();
  //exit(1);
}
*/
/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setLocalMeshAndSupportDofMesh(int rankPart) 
{
  std::vector<std::vector<felInt> > loc2GlobElem(m_mesh.size());

  m_meshLocal.resize(m_mesh.size());
  m_mappingElem.resize(m_mesh.size());
  m_mappingElemSupportPerUnknown.resize(m_listUnknown.size());

  for (std::size_t iMesh = 0; iMesh < m_mesh.size(); ++iMesh){
    /*
     * Set local mesh and local to global mapping for elements
     */
    // Set the local mesh and fill the local to global mapping for the support elements (loc2GlobSupportElem)
    if ( !m_meshLocal[iMesh] )
      m_meshLocal[iMesh] = felisce::make_shared<GeometricMeshRegion>();
    m_meshLocal[iMesh]->setLocalMesh(*m_mesh[iMesh], m_eltPart[iMesh], rankPart, loc2GlobElem[iMesh]);
    
    // Create the local to global mapping for the elements
    IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(MpiInfo::petscComm(), 1, loc2GlobElem[iMesh].size(), loc2GlobElem[iMesh].data(), PETSC_COPY_VALUES, &m_mappingElem[iMesh]);
  }
  m_initMappingElem = true;

  /*
   * Set local support dof mesh and local to global mapping for support elements
   */
  // for each unknown, create the support dof and put them in m_supportDofUnknownLocal
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    const int idVar = m_listUnknown.idVariable(iUnknown);
    const int iMesh = m_listVariable[idVar].idMesh();

    // create the local supportdofmeshes
    std::vector<felInt> loc2GlobElemSupport = loc2GlobElem[iMesh];
    SupportDofMesh supportDofMesh(m_listVariable[idVar], m_meshLocal[iMesh], loc2GlobElemSupport, m_supportDofUnknown[iUnknown]);

    // save them in m_supportDofUnknownLocal
    m_supportDofUnknownLocal.push_back(supportDofMesh);

    // Create the mapping
    if(FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
      m_mappingElemSupportPerUnknown[iUnknown] = new ISLocalToGlobalMapping;
      IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(MpiInfo::petscComm(), 1, loc2GlobElemSupport.size(), loc2GlobElemSupport.data(), PETSC_COPY_VALUES, m_mappingElemSupportPerUnknown[iUnknown]);
    } else
      m_mappingElemSupportPerUnknown[iUnknown] = &m_mappingElem[iMesh];
  }
  m_initMappingElemSupport = true;

  /*
   * Print info on the local mesh and supportDofMeshes
   */
  if (m_verbosity) {
    // print the local mesh
    for (std::size_t iMesh = 0; iMesh < m_mesh.size(); ++iMesh) {
      PetscPrintf(MpiInfo::petscComm(), "\n/================== Local mesh %lu ================/\n", iMesh);
      for ( int k = 0; k < MpiInfo::numProc(); ++k ) {
        if( MpiInfo::rankProc() == k )
          m_meshLocal[iMesh]->print(m_verbosity);
      }
    }

    // print the local supportDofMesh for each unknown
    for(std::size_t iUnknown=0; iUnknown<m_listUnknown.size(); iUnknown++) {
      int idVar = m_listUnknown.idVariable(iUnknown);
      PetscPrintf(MpiInfo::petscComm(), "\n/=========== Local support dof mesh associate to the unknown: <%s> ===========/\n", m_listVariable[idVar].name().c_str());
      m_supportDofUnknownLocal[iUnknown].print(m_verbosity);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::allocateMatrix() 
{
  // Create the local to global mapping for support dofs.
  std::vector<felInt> loc2Glob(m_numDofLocal,0);
  felInt cptDof= 0;
  for ( felInt iDof = 0; iDof < m_dof.numDof(); iDof++) {
    if ( m_dofPart[iDof] == MpiInfo::rankProc() ) {
      loc2Glob[cptDof] = iDof;
      cptDof++;
    }
  }

  // Global to local mapping on nodes
  IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(MpiInfo::petscComm(), 1, m_numDofLocal, &loc2Glob[0], PETSC_COPY_VALUES, &m_mappingNodes);
  // ISLocalToGlobalMappingView( m_mappingNodes, PETSC_VIEWER_STDOUT_WORLD);
  m_initMappingNodes = true;

  // Allocation of the matrix
  if ( MpiInfo::numProc() > 1 ) {
    // In parallel
    // copy to not change the pattern
    std::vector<felInt> jCSR = m_dof.pattern().columnIndices();
    AOApplicationToPetsc(m_ao, m_dof.pattern().numNonzeros(), jCSR.data());

    for (std::size_t ii = 0; ii < m_matrices.size(); ii++) {
      m_matrices[ii].createAIJ(MpiInfo::petscComm(), m_numDofLocal, m_numDofLocal, m_dof.numDof(), m_dof.numDof(), 0, FELISCE_PETSC_NULLPTR, 0, FELISCE_PETSC_NULLPTR);
      m_matrices[ii].mpiAIJSetPreallocationCSR(m_dof.pattern().rowPointer().data(), jCSR.data(), nullptr);
      m_matrices[ii].setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      m_matrices[ii].setFromOptions();
    }

    // Create rhs (parallel vector)
    for (std::size_t index = 0, Nvector = m_vectors.size(); index < Nvector; ++index)
      m_vectors[index].createMPI(MpiInfo::petscComm(), m_numDofLocal, m_dof.numDof());
  } else {
    // In serial
    const std::size_t numRows = m_dof.pattern().numRows();
    std::vector<felInt> nnz(numRows);
    for ( felInt idof = 0; idof < static_cast<felInt>(numRows); idof++) {
      nnz[idof] = m_dof.pattern().numNonzerosInRow(idof);
    }

    for (std::size_t ii = 0; ii < m_matrices.size(); ii++) {
      m_matrices[ii].createSeqAIJ(MpiInfo::petscComm(), numRows, numRows, 0, &nnz[0]);
      m_matrices[ii].setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      m_matrices[ii].setFromOptions();
    }

    for (std::size_t index = 0, Nvector = m_vectors.size(); index < Nvector; ++index) {
      m_vectors[index].create(MpiInfo::petscComm());
      m_vectors[index].setSizes(numRows,numRows);
      m_vectors[index].setFromOptions();
    }
  }

  m_numDof = m_dof.numDof();

  // Create the vector solution
  m_sol.createMPI(MpiInfo::petscComm(), m_numDofLocal, m_numDof);
  m_sol.set( 0.);

  if (this->snesInterface().doUseSNES()) {
    m_evaluationState.createMPI(MpiInfo::petscComm(), m_numDofLocal, m_numDof);
    m_evaluationState.zeroEntries();
  }

  allocateSequentialSolution();
  m_buildSystem = true;

  // Get the local size of all process
  std::vector<felInt> localSizeOfVector( MpiInfo::numProc(), 0);

  felInt sizeLocal = 0;
  m_sol.getLocalSize(&sizeLocal);

  MPI_Allgather(&sizeLocal, 1, MPI_INT, localSizeOfVector.data(), 1, MPI_INT, MpiInfo::petscComm());

  // For each unknown, create a local to global mapping to map the local id of dof to their global id in the petsc ordering.
  felInt numDofPreviousUnknowns = 0;
  felInt idGlobalDof = 0;
  felInt sumPreviousSizeLocal = 0;
  std::vector<felInt> loc2GlobPerUnknown;
  felInt sizeIdDof;
  m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc.resize(m_listUnknown.size());

  for ( int iProc = 0; iProc < MpiInfo::rankProc(); iProc++)
    sumPreviousSizeLocal += localSizeOfVector[iProc];

  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
    m_numDofLocalUnknown[iUnknown] = 0;

  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    for ( felInt iDof = 0; iDof < m_numDofUnknown[iUnknown]; iDof++) {
      idGlobalDof = numDofPreviousUnknowns + iDof;
      AOApplicationToPetsc(m_ao, 1, &idGlobalDof);
      if ( (sumPreviousSizeLocal <= idGlobalDof) && (idGlobalDof < localSizeOfVector[MpiInfo::rankProc()]+sumPreviousSizeLocal) ) {
        m_numDofLocalUnknown[iUnknown]++;
        loc2GlobPerUnknown.push_back(idGlobalDof);
      }
    }
    numDofPreviousUnknowns += m_numDofUnknown[iUnknown];
    sizeIdDof = loc2GlobPerUnknown.size();
    IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(MpiInfo::petscComm(), 1, sizeIdDof, loc2GlobPerUnknown.data(), PETSC_COPY_VALUES, &m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown] );
    // ISLocalToGlobalMappingView(m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown], PETSC_VIEWER_STDOUT_WORLD);
    loc2GlobPerUnknown.clear();
  }
  m_initMappingLocalFelisceToGlobalPetsc = true;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::allocateSequentialSolution() 
{
  m_seqSol.createSeq(PETSC_COMM_SELF, m_numDof);
  m_seqSol.set(0.);
  m_seqSolAllocated = true;

  if ( this->snesInterface().doUseSNES() ) {
    m_seqEvaluationState.createSeq(PETSC_COMM_SELF, m_numDof);
    m_seqEvaluationState.set( 0.);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::assembleMatrixRHS(int rank, FlagMatrixRHS flagMatrixRHS) 
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // Geometric element type in the mesh.
  int numPointPerElt = 0;        // Number of points per geometric element.
  int currentLabel = 0;          // Number of label domain.
  felInt numEltPerLabel = 0;     // Number of element for one label and one eltType.

  // Retrieve instance
  auto& r_instance = FelisceParam::instance(this->instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;

  r_timer.Start("LinearProblem" + std::to_string(m_identifier_problem) + "::assembleMatrixRHS", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  // TODO: The solution should maybe be modified in here in order to impose the natural BC for NL problems.

  // Use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // Contains points of the current element.
  std::vector<Point*> elemPoint;

  // Contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // Use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType = bagElementTypeDomain[i];

    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // Define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);

    // Element matrix and vector initialisation
    initElementArray();

    // Allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // Virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // Used by user to add specific term (source term for example with elemField).
    userElementInit();

    // Second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      // By default this virtual defines variable m_currentLabel: (m_currentLabel=label).
      // We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);

      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        FEL_ASSERT(!m_elementVector.empty());

        if(!r_instance.duplicateSupportDof) {
          // Return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);

          // Clear elementary matrices
          for (std::size_t j = 0, size = m_matrices.size(); j < size ; j++)
            m_elementMat[j]->zero();

          // Clear elementary std::vector.
          for (std::size_t j = 0, size = m_vectors.size(); j < size ; j++)
            m_elementVector[j]->zero();

          // Function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          if (r_instance.NSequationFlag == 1 && r_instance.characteristicMethod != 0) {
            // Use method of characteristics for N-S
            computeElementArrayCharact(elemPoint, elemIdPoint, ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
          } else{
            computeElementArray(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
          }

          // Compute specific term of users.
          userElementCompute(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);

          // Add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHS(ielSupportDof, ielSupportDof, flagMatrixRHS);
        } else {
          m_duplicateSupportDofAssemblyLoop(rank, eltType, numElement[eltType], elemPoint, elemIdPoint, flagMatrixRHS);
        }

        numElement[eltType]++;
      }
    }
    // Desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly loop for LumpedModelBC in case of NS model with implicit implementation
  if (r_instance.lumpedModelBCLabel.size()>0 && r_instance.model == "NS") {
    for (std::size_t iLabel = 0; iLabel < r_instance.lumpedModelBCLabel.size(); iLabel++) {
      if(r_instance.lumpedModelBCAlgo[iLabel] == 2) // use a Enum here... 08/13 VM TODO
        assembleMatrixImplLumpedModelBC(rank, iLabel);
    }
  }

  // Assembly / addition of crack term block of the solid model if it exists in the problem
  derivedAssembleMatrixCrackModel(rank);

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    // Real assembling of the matrix manage by PETsC.
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }
  // Call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    // Real assembling of the right hand side (RHS).
    for (std::size_t i = 0; i < m_vectors.size(); i++) {
      m_vectors[i].assembly();
    }
  }

  // Call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);

  r_timer.Stop("LinearProblem" + std::to_string(m_identifier_problem) + "::assembleMatrixRHS", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineFiniteElement(const ElementType& eltType)
{
  m_listCurrentFiniteElement.clear();

  std::size_t idMesh;
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurrentFiniteElement* fe;
  geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  for ( std::size_t iVar = 0, size = m_listVariable.size(); iVar < size; iVar++) {
    idMesh = m_listVariable[iVar].idMesh();
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType(); // 0 = linear, 1 = quadratic, 2 = bubble
    refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_meshLocal[idMesh]); // TODO D.C. here mesh is used just to check i the current element is a bd elt... maybe a simple flag is better...
    fe = new CurrentFiniteElement(*refEle,*geoEle,m_listVariable[iVar].getDegreeOfExactness());
    FEL_ASSERT(fe);
    m_listCurrentFiniteElement.add(fe);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineFiniteElement(const std::vector<ElementType>& eltType)
{
  m_listCurrentFiniteElement.clear();

  //Initialisation of Finite Element current problem
  std::size_t idMesh;
  const RefElement *refEle;
  const GeoElement *geoEle;  
  int typeOfFiniteElement = 0;
  CurrentFiniteElement* fe;
  for ( std::size_t iVar = 0, size = m_listVariable.size(); iVar < size; iVar++) {
    idMesh = m_listVariable[iVar].idMesh();
    geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType[iVar]].second;
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType(); // 0 = linear, 1 = quadratic, 2 = bubble
    refEle = geoEle->defineFiniteEle(eltType[iVar],typeOfFiniteElement, *m_meshLocal[idMesh]);
    fe = new CurrentFiniteElement(*refEle,*geoEle,m_listVariable[iVar].getDegreeOfExactness());
    FEL_ASSERT(fe);
    m_listCurrentFiniteElement.add(fe);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::initElementArray(const bool initTranspose) 
{
  m_elementMat.clear();
  m_elementMatT.clear();
  m_elementVector.clear();
  
  int idVar;
  std::size_t rowSize = m_listUnknown.getUnknownsRows().size();
  std::size_t colSize = m_listUnknown.getUnknownsCols().size();
  std::vector<std::size_t>                 numberCmp1(rowSize), numberCmp2(colSize);
  std::vector<const CurBaseFiniteElement*> finiteElt1(rowSize), finiteElt2(colSize);

  for (std::size_t n = 0; n < rowSize; n++){
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[n]);
    numberCmp1[n] = m_listVariable[idVar].numComponent();
    finiteElt1[n] = m_listCurrentFiniteElement[idVar];
  }

  for (std::size_t n = 0; n < colSize; n++){
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[n]);
    numberCmp2[n] = m_listVariable[idVar].numComponent();
    finiteElt2[n] = m_listCurrentFiniteElement[idVar];
  }

  for (std::size_t i = 0; i < m_vectors.size(); i++)
    m_elementVector.push_back(felisce::make_shared<ElementVector>(finiteElt1, numberCmp1));

  for (std::size_t i = 0; i < m_matrices.size(); i++)
    m_elementMat.push_back(felisce::make_shared<ElementMatrix>(finiteElt1, numberCmp1, finiteElt2, numberCmp2));
      
  if (initTranspose)
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_elementMatT.push_back(felisce::make_shared<ElementMatrix>(finiteElt2, numberCmp2, finiteElt1, numberCmp1));
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::initPerDomain(int label, FlagMatrixRHS /*flagMatrixRHS*/) 
{
  m_currentLabel = label;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, std::vector<felInt>& vectorSupport) 
{
  // We assume that the support elements are the same for all the unknown.
  const int iUnknown = m_listUnknown.getUnknownsRows()[0]; // TODO D.C.
  const int iMesh = m_meshUnknown[iUnknown];
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(eltType, iel, vectorSupport);
  ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[iUnknown], vectorSupport.size(), vectorSupport.data(), vectorSupport.data());
  
  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  m_meshLocal[iMesh]->getOneElement(eltType, iel, elemIdPoint, 0);
  for (int iPoint = 0; iPoint < numPointsPerElt; iPoint++) {
    elemPoint[iPoint] = &m_meshLocal[iMesh]->listPoints()[elemIdPoint[iPoint]];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint,
                                 std::vector<felInt>& elemIdPoint, felInt* ielSupportDof)
{
  // We assume that the support elements are the same for all the unknown defined on the current mesh.
  const int iUnknown = m_listUnknown.getUnknownsRows()[0]; // TODO D.C.
  const int iMesh = m_meshUnknown[iUnknown];
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(eltType, iel, *ielSupportDof);
  ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[iUnknown], 1, ielSupportDof, ielSupportDof);

  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  m_meshLocal[iMesh]->getOneElement(eltType, iel, elemIdPoint, 0);
  for (int iPoint = 0; iPoint < numPointsPerElt; iPoint++) {
    elemPoint[iPoint] = &m_meshLocal[iMesh]->listPoints()[elemIdPoint[iPoint]];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueMatrixRHS(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, FlagMatrixRHS flagMatrixRHS, bool buildMatT)
{
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_rhs              ? !m_elementMat.empty()    : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_rhs && buildMatT ? !m_elementMatT.empty()   : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_matrix           ? !m_elementVector.empty() : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_rhs              ? m_GposColumn != nullptr : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_matrix           ? m_GposLine   != nullptr : true );

  const felInt numDofTotal1 = m_elementMat[0]->mat().size1();
  const felInt numDofTotal2 = m_elementMat[0]->mat().size2();
  int idVar;
  felInt cptGpos;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. for the rows of the matrix or RHS)
  cptGpos = 0;
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    m_dof.loc2glob(ielSupportDof1[iUnknown], m_listCurrentFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);  

    for(std::size_t i=0; i<loc2globTmp.size(); ++i)
      m_GposLine[cptGpos++] = loc2globTmp[i];
  }

  switch (flagMatrixRHS) {
    case FlagMatrixRHS::matrix_and_rhs :
      
      // Build the loc2glob map m_GposColumn for the second support element (i.e. for the columns of the matrix)
      if(ielSupportDof1 == ielSupportDof2 && m_listUnknown.getUnknownsRows() == m_listUnknown.getUnknownsCols() ) {
        // if the two support element are the same (always the case unless we have duplicated support element)
        for ( felInt i = 0; i < numDofTotal1; ++i )
          m_GposColumn[i] = m_GposLine[i];
      } else {
        // if they are different, compute it.
        cptGpos = 0;
        for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
          idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
          m_dof.loc2glob(ielSupportDof2[iUnknown], m_listCurrentFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

          for(std::size_t i=0; i<loc2globTmp.size(); ++i)
            m_GposColumn[cptGpos++] = loc2globTmp[i];
        }
      }

      // Set values
      AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
      AOApplicationToPetsc(m_ao,numDofTotal2,m_GposColumn);
      for (std::size_t i = 0; i < m_matrices.size(); i++) // TODO D.C. why for every matrix?????
        m_matrices[i].setValues(numDofTotal1,m_GposLine,numDofTotal2,m_GposColumn,m_elementMat[i]->mat().data().begin(),ADD_VALUES);
      for (std::size_t i = 0; i < m_matrices.size() && buildMatT; i++)
        m_matrices[i].setValues(numDofTotal2,m_GposColumn,numDofTotal1,m_GposLine,m_elementMatT[i]->mat().data().begin(),ADD_VALUES);
      for (std::size_t i = 0; i < m_vectors.size(); i++)
        m_vectors[i].setValues(numDofTotal1,m_GposLine, m_elementVector[i]->vec().data().begin(), ADD_VALUES);
      break;

    case FlagMatrixRHS::only_matrix :
      
      // Build the loc2glob map m_GposColumn for the second support element (i.e. for the columns of the matrix)
      if(ielSupportDof1 == ielSupportDof2 && m_listUnknown.getUnknownsRows() == m_listUnknown.getUnknownsCols() ) {
        // if the two support element are the same (always the case unless we have duplicated support element)
        for ( felInt i = 0; i < numDofTotal1; ++i )
          m_GposColumn[i] = m_GposLine[i];
      } else {
        // if they are different, compute it.
        cptGpos = 0;
        for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
          idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
          m_dof.loc2glob(ielSupportDof2[iUnknown], m_listCurrentFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

          for(std::size_t i=0; i<loc2globTmp.size(); ++i)
            m_GposColumn[cptGpos++] = loc2globTmp[i];
        }
      }

      // Set values
      AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
      AOApplicationToPetsc(m_ao,numDofTotal2,m_GposColumn);
      for (std::size_t i = 0; i < m_matrices.size(); i++)
        m_matrices[i].setValues(numDofTotal1,m_GposLine,numDofTotal2,m_GposColumn,m_elementMat[i]->mat().data().begin(),ADD_VALUES);
      for (std::size_t i = 0; i < m_matrices.size() && buildMatT; i++)
        m_matrices[i].setValues(numDofTotal2,m_GposColumn,numDofTotal1,m_GposLine,m_elementMatT[i]->mat().data().begin(),ADD_VALUES);
      break;

    case FlagMatrixRHS::only_rhs :

      // Set values
      AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
      for (std::size_t i = 0; i < m_vectors.size(); i++)
        m_vectors[i].setValues(numDofTotal1,m_GposLine,m_elementVector[i]->vec().data().begin(),ADD_VALUES);
      break;
    
    default:
      FEL_ERROR("Problem with Matrix/RHS flag.");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueMatrixRHS(const felInt ielSupportDof1, const felInt ielSupportDof2, FlagMatrixRHS flagMatrixRHS, bool buildMatT)
{
  std::vector<felInt> vecIelSupportDof1(m_listUnknown.getUnknownsRows().size(), ielSupportDof1);
  std::vector<felInt> vecIelSupportDof2(m_listUnknown.getUnknownsCols().size(), ielSupportDof2);
  setValueMatrixRHS(vecIelSupportDof1, vecIelSupportDof2, flagMatrixRHS, buildMatT);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomMatrix(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, PetscMatrix& mat, bool buildMatT)
{
  FEL_ASSERT( !m_elementMat.empty() );
  FEL_ASSERT( buildMatT ? !m_elementMatT.empty() : true );
  FEL_ASSERT( m_GposColumn );
  FEL_ASSERT( m_GposLine   );

  const felInt numDofTotal1 = m_elementMat[0]->mat().size1();
  const felInt numDofTotal2 = m_elementMat[0]->mat().size2();
  int idVar;
  felInt cptGpos;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. for the rows of the matrix or RHS)
  cptGpos = 0;
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    m_dof.loc2glob(ielSupportDof1[iUnknown], m_listCurrentFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);         
              
    for(std::size_t i=0; i<loc2globTmp.size(); ++i)
      m_GposLine[cptGpos++] = loc2globTmp[i];
  }

  // Build the loc2glob map m_GposColumn for the second support element (i.e. for the columns of the matrix)
  if(ielSupportDof1 == ielSupportDof2 && m_listUnknown.getUnknownsRows() == m_listUnknown.getUnknownsCols() ) {
    // if the two support element are the same (always the case unless we have duplicated support element) just copy the first one
    for ( felInt i = 0; i < numDofTotal1; ++i )
      m_GposColumn[i] = m_GposLine[i];
  } else {
    // if they are different, compute it.
    cptGpos = 0;
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
      idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
      m_dof.loc2glob(ielSupportDof2[iUnknown], m_listCurrentFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

      for(std::size_t i=0; i<loc2globTmp.size(); ++i)
        m_GposColumn[cptGpos++] = loc2globTmp[i];
    }
  }

  AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
  AOApplicationToPetsc(m_ao,numDofTotal2,m_GposColumn);
  mat.setValues(numDofTotal1,m_GposLine,numDofTotal2,m_GposColumn,m_elementMat[0]->mat().data().begin(),ADD_VALUES);
  if (buildMatT)
    mat.setValues(numDofTotal2,m_GposColumn,numDofTotal1,m_GposLine,m_elementMatT[0]->mat().data().begin(),ADD_VALUES);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomMatrix(const felInt ielSupportDof1, const felInt ielSupportDof2, PetscMatrix& mat, bool buildMatT)
{
  std::vector<felInt> vecIelSupportDof1(m_listUnknown.getUnknownsRows().size(), ielSupportDof1);
  std::vector<felInt> vecIelSupportDof2(m_listUnknown.getUnknownsCols().size(), ielSupportDof2);
  setValueCustomMatrix(vecIelSupportDof1, vecIelSupportDof2, mat, buildMatT);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomVector(const std::vector<felInt>& ielSupportDof1, InsertMode mode, PetscVector& vec) 
{
  FEL_ASSERT(!m_elementVector.empty());
  FEL_ASSERT( m_GposLine );

  const felInt numDofTotal = m_elementVector[0]->vec().size();
  int idVar;
  felInt cptGpos;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. RHS)
  cptGpos = 0;    
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    m_dof.loc2glob(ielSupportDof1[iUnknown], m_listCurrentFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

    for(std::size_t i=0; i<loc2globTmp.size(); ++i)
      m_GposLine[cptGpos++] = loc2globTmp[i];
  }

  // Set values
  AOApplicationToPetsc(m_ao,numDofTotal,m_GposLine);
  vec.setValues(numDofTotal,m_GposLine,m_elementVector[0]->vec().data().begin(),mode);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomVector(const felInt ielSupportDof1, InsertMode mode, PetscVector& vec) 
{
  std::vector<felInt> vecIelSupportDof1(m_listUnknown.getUnknownsRows().size(), ielSupportDof1);
  setValueCustomVector(vecIelSupportDof1, mode, vec);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::allocateArrayForAssembleMatrixRHS(FlagMatrixRHS flagMatrixRHS)
{
  felInt numDofTotalL = 0;
  int idVar;
  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    numDofTotalL += m_listCurrentFiniteElement[idVar]->numDof()*m_listVariable[idVar].numComponent();
  }
  m_GposLine = new felInt[numDofTotalL];

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    felInt numDofTotalC = 0;
    for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
      idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
      numDofTotalC += m_listCurrentFiniteElement[idVar]->numDof()*m_listVariable[idVar].numComponent();
    }
    m_GposColumn = new felInt[numDofTotalC];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS /*flagMatrixRHS*/)
{
  if ( m_GposLine ) {
    delete [] m_GposLine;
    m_GposLine = nullptr;
  }

  if ( m_GposColumn ) {
    delete [] m_GposColumn;
    m_GposColumn = nullptr;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::assembleMatrixRHSBD(int rank, FlagMatrixRHS flagMatrixRHS) 
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;              //geometric element type in the mesh.
  int numPointPerElt = 0;           //number of points per geometric element.
  int currentLabel = 0;             //number of label domain.
  felInt numEltPerLabel = 0;        //number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains normals of the current element.
  std::vector<Point*> elemNormal;

  // contains tangents of the current element.
  std::vector <std::vector<Point*> > elemTangent;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // contains the ids of the support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  //Assembly loop.
  //First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);
    if (m_meshLocal[m_currentMesh]->createNormalTangent()) {
      elemNormal.resize(numPointPerElt, nullptr);
      elemTangent.resize(m_mesh[m_currentMesh]->domainDim());
      for (std::size_t i=0 ; i<elemTangent.size() ; i++)
        elemTangent[i].resize(numPointPerElt, nullptr);
    }

    //define all current finite element use in the problem from data
    //file configuration and allocate elemMat and elemVec (question: virtual ?).
    defineCurvilinearFiniteElement(eltType);

    // Element matrix and std::vector initialisation
    initElementArrayBD();

    // Allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);

    // Virtual function use in derived problem to allocate elemenField necessary.
    initPerElementTypeBD(eltType, flagMatrixRHS);

    // Use by user to add specific term (term source for example with elemField).
    userElementInitBD();

    // Second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      //By default this virtual define variable m_currentLabel: (m_currentLabel=label).
      //We can switch on label region with that and define some parameters.
      initPerDomainBD(currentLabel, flagMatrixRHS);

      //Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.

        if (m_meshLocal[m_currentMesh]->createNormalTangent()) {
          setElemPointNormalTangent(eltType, numElement[eltType], elemPoint, elemNormal, elemTangent, elemIdPoint, vectorIdSupport);
        } else {
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
        }
        // Loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          // clear elementary matrix.
          for (std::size_t j = 0; j < m_matrices.size(); j++)
            m_elementMatBD[j]->zero();

          // clear elementary vector.
          for (std::size_t j = 0; j < m_vectors.size(); j++)
            m_elementVectorBD[j]->zero();

          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          if (m_meshLocal[m_currentMesh]->createNormalTangent())
            computeElementArrayBD(elemPoint, elemIdPoint, elemNormal, elemTangent, ielSupportDof, flagMatrixRHS);
          else
            computeElementArrayBD(elemPoint, elemIdPoint, ielSupportDof,flagMatrixRHS);

          // compute specific term of users.
          userElementComputeBD(elemPoint, elemIdPoint, ielSupportDof);

          // add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHSBD(ielSupportDof, ielSupportDof, flagMatrixRHS);
        }
        numElement[eltType]++;
      }
    }
    // Deallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly / addition of crack term block of the solid model if it exists in the problem
  derivedAssembleMatrixCrackModel(rank);

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    //real assembling of the matrix manage by PETsC.
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }
  // Call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
    // Real assembling of the right hand side (RHS).
    for (std::size_t index = 0; index < m_vectors.size(); index++) {
      m_vectors[index].assembly();
    }
  }
  //call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineCurvilinearFiniteElement(const ElementType& eltType)
{
  m_listCurvilinearFiniteElement.clear();

  //Initialisation of Finite Element curvlinear problem
  std::size_t idMesh;
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurvilinearFiniteElement* fe;
  geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  for ( std::size_t iVar = 0; iVar < m_listVariable.size(); iVar++) {
    idMesh = m_listVariable[iVar].idMesh();
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType();
    refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_meshLocal[idMesh]); // TODO D.C. i don't like that it requires the mesh, maybe a flag ifBfElt is better...
    fe = new CurvilinearFiniteElement(*refEle,*geoEle,m_listVariable[iVar].degreeOfExactness());
    FEL_ASSERT(fe);
    if( FelisceParam::instance(this->instanceIndex()).flipNormal )
      fe->m_sign = -1;
    m_listCurvilinearFiniteElement.add(fe);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineCurvilinearFiniteElement(const std::vector<ElementType>& eltType)
{
  m_listCurvilinearFiniteElement.clear();

  //Initialisation of Finite Element curvlinear problem
  std::size_t idMesh;
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurvilinearFiniteElement* fe;
  for ( std::size_t iVar = 0; iVar < m_listVariable.size(); iVar++) {
    idMesh = m_listVariable[iVar].idMesh();
    geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType[iVar]].second;
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType();
    refEle = geoEle->defineFiniteEle(eltType[iVar],typeOfFiniteElement, *m_meshLocal[idMesh]);
    fe = new CurvilinearFiniteElement(*refEle,*geoEle,m_listVariable[iVar].degreeOfExactness());
    FEL_ASSERT(fe);
    if( FelisceParam::instance(this->instanceIndex()).flipNormal )
      fe->m_sign = -1;
    m_listCurvilinearFiniteElement.add(fe);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineCurrentFiniteElementWithBd(const ElementType& eltType) 
{
  m_listCurrentFiniteElementWithBd.clear();

  //Initialisation of Finite Element current with boundary problem
  std::size_t idMesh;
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurrentFiniteElementWithBd* fewbd;
  geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  for ( std::size_t iVar = 0; iVar < m_listVariable.size(); iVar++) {
    idMesh = m_listVariable[iVar].idMesh();
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType();
    refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_meshLocal[idMesh]);
    fewbd = new CurrentFiniteElementWithBd(*refEle,*geoEle,m_listVariable[iVar].getDegreeOfExactness(),m_listVariable[iVar].degreeOfExactness()); //maybe it causes a memory leak
    m_listCurrentFiniteElementWithBd.add(fewbd);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineCurrentFiniteElementWithBd(const std::vector<ElementType>& eltType) 
{
  m_listCurrentFiniteElementWithBd.clear();

  //Initialisation of Finite Element current with boundary problem
  std::size_t idMesh;
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurrentFiniteElementWithBd* fewbd;
  for ( std::size_t iVar = 0; iVar < m_listVariable.size(); iVar++) {
    idMesh = m_listVariable[iVar].idMesh();
    geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType[iVar]].second;
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType();
    refEle = geoEle->defineFiniteEle(eltType[iVar],typeOfFiniteElement, *m_meshLocal[idMesh]);
    fewbd = new CurrentFiniteElementWithBd(*refEle,*geoEle,m_listVariable[iVar].getDegreeOfExactness(),m_listVariable[iVar].degreeOfExactness()); //maybe it causes a memory leak
    m_listCurrentFiniteElementWithBd.add(fewbd);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::initElementArrayBD(const bool initTranspose) 
{
  m_elementMatBD.clear();
  m_elementMatTBD.clear();
  m_elementVectorBD.clear();

  int idVar;
  std::size_t rowSize = m_listUnknown.getUnknownsRows().size();
  std::size_t colSize = m_listUnknown.getUnknownsCols().size();  
  std::vector<std::size_t>                 numberCmp1(rowSize), numberCmp2(colSize);
  std::vector<const CurBaseFiniteElement*> finiteElt1(rowSize), finiteElt2(colSize);

  for (std::size_t n = 0; n < rowSize; n++){
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[n]);
    numberCmp1[n] = m_listVariable[idVar].numComponent();
    finiteElt1[n] = m_listCurvilinearFiniteElement[idVar];
  }

  for (std::size_t n = 0; n < colSize; n++){
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[n]);
    numberCmp2[n] = m_listVariable[idVar].numComponent();
    finiteElt2[n] = m_listCurvilinearFiniteElement[idVar];
  }

  for (std::size_t i = 0; i < m_vectors.size(); i++)
    m_elementVectorBD.push_back(felisce::make_shared<ElementVector>(finiteElt1, numberCmp1));

  for (std::size_t i = 0; i < m_matrices.size(); i++)
    m_elementMatBD.push_back(felisce::make_shared<ElementMatrix>(finiteElt1, numberCmp1, finiteElt2, numberCmp2));
    
  if (initTranspose)
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_elementMatTBD.push_back(felisce::make_shared<ElementMatrix>(finiteElt2, numberCmp2, finiteElt1, numberCmp1));
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::initPerDomainBD(int label, FlagMatrixRHS /*flagMatrixRHS*/) 
{
  m_currentLabel = label;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setElemPointNormalTangent(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint,
                                              std::vector<Point*>& elemNormal, std::vector< std::vector<Point*> >& elemTangent,
                                              std::vector<felInt>& elemIdPoint,std::vector<felInt>& vectorSupport)
{
  // We assume that the support elements are the same for all the unknown defined on the current mesh.
  const int iUnknown = m_listUnknown.getUnknownsRows()[0]; // TODO D.C.
  const int iMesh = m_meshUnknown[iUnknown];
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(eltType, iel, vectorSupport);
  ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[iUnknown], vectorSupport.size(), vectorSupport.data(), vectorSupport.data());

  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  m_meshLocal[iMesh]->getOneElement(eltType, iel, elemIdPoint, 0);
  for (int iPoint = 0; iPoint < numPointsPerElt; iPoint++) {
    elemPoint[iPoint] = &m_meshLocal[iMesh]->listPoints()[elemIdPoint[iPoint]];
    elemNormal[iPoint] = &m_meshLocal[iMesh]->listNormals()[elemIdPoint[iPoint]];
    for (std::size_t j=0 ; j<elemTangent.size() ; j++)
      elemTangent[j][iPoint] = &m_meshLocal[iMesh]->listTangents(j)[elemIdPoint[iPoint]];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueMatrixRHSBD(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, FlagMatrixRHS flagMatrixRHS, bool buildMatT)
{
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_rhs              ? !m_elementMatBD.empty()    : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_rhs && buildMatT ? !m_elementMatTBD.empty()   : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_matrix           ? !m_elementVectorBD.empty() : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_rhs              ? m_GposColumn != nullptr : true );
  FEL_ASSERT( flagMatrixRHS != FlagMatrixRHS::only_matrix           ? m_GposLine   != nullptr : true );

  const felInt numDofTotal1 = m_elementMatBD[0]->mat().size1();
  const felInt numDofTotal2 = m_elementMatBD[0]->mat().size2();
  int idVar;
  felInt cptGpos;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. for the rows of the matrix or RHS)
  cptGpos = 0;
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    m_dof.loc2glob(ielSupportDof1[iUnknown], m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

    for(std::size_t i=0; i<loc2globTmp.size(); ++i)
      m_GposLine[cptGpos++] = loc2globTmp[i];
  }

  switch (flagMatrixRHS) {
    case FlagMatrixRHS::matrix_and_rhs :

      // Build the loc2glob map m_GposColumn for the second support element (i.e. for the columns of the matrix)
      if(ielSupportDof1 == ielSupportDof2 && m_listUnknown.getUnknownsRows() == m_listUnknown.getUnknownsCols() ) {
        // if the two support element are the same (always the case unless we have duplicated support element)
        for ( felInt i = 0; i < numDofTotal1; ++i )
          m_GposColumn[i] = m_GposLine[i];
      } else {
        // if they are different, compute it.
        cptGpos = 0;
        for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
          idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
          m_dof.loc2glob(ielSupportDof2[iUnknown], m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

          for(std::size_t i=0; i<loc2globTmp.size(); ++i)
            m_GposColumn[cptGpos++] = loc2globTmp[i];
        }
      }

      // Set values
      AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
      AOApplicationToPetsc(m_ao,numDofTotal2,m_GposColumn);
      for (std::size_t i = 0; i < m_matrices.size(); i++)
        m_matrices[i].setValues(numDofTotal1,m_GposLine,numDofTotal2,m_GposColumn,m_elementMatBD[i]->mat().data().begin(),ADD_VALUES);
      for (std::size_t i = 0; i < m_matrices.size() && buildMatT; i++)
        m_matrices[i].setValues(numDofTotal2,m_GposColumn,numDofTotal1,m_GposLine,m_elementMatTBD[i]->mat().data().begin(),ADD_VALUES);
      for (std::size_t i = 0; i < m_vectors.size(); i++)
        m_vectors[i].setValues(numDofTotal1,m_GposLine, m_elementVectorBD[i]->vec().data().begin(), ADD_VALUES);
      break;

    case FlagMatrixRHS::only_matrix :

      // Build the loc2glob map m_GposColumn for the second support element (i.e. for the columns of the matrix)
      if(ielSupportDof1 == ielSupportDof2 && m_listUnknown.getUnknownsRows() == m_listUnknown.getUnknownsCols() ) {
        // if the two support element are the same (always the case unless we have duplicated support element)
        for ( felInt i = 0; i < numDofTotal1; ++i )
          m_GposColumn[i] = m_GposLine[i];
      } else {
        // if they are different, compute it.
        cptGpos = 0;
        for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
          idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
          m_dof.loc2glob(ielSupportDof2[iUnknown], m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

          for(std::size_t i=0; i<loc2globTmp.size(); ++i)
            m_GposColumn[cptGpos++] = loc2globTmp[i];
        }
      }

      // Set values
      AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
      AOApplicationToPetsc(m_ao,numDofTotal2,m_GposColumn);
      for (std::size_t i = 0; i < m_matrices.size(); i++)
        m_matrices[i].setValues(numDofTotal1,m_GposLine,numDofTotal2,m_GposColumn,m_elementMatBD[i]->mat().data().begin(),ADD_VALUES);
      for (std::size_t i = 0; i < m_matrices.size() && buildMatT; i++)
        m_matrices[i].setValues(numDofTotal2,m_GposColumn,numDofTotal1,m_GposLine,m_elementMatTBD[i]->mat().data().begin(),ADD_VALUES);
      break;

    case FlagMatrixRHS::only_rhs :

      // Set values
      AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
      for (std::size_t i = 0; i < m_vectors.size(); i++)
        m_vectors[i].setValues(numDofTotal1,m_GposLine, m_elementVectorBD[i]->vec().data().begin(), ADD_VALUES);
      break;

    default:
      FEL_ERROR("Problem with Matrix/RHS flag.");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueMatrixRHSBD(const felInt ielSupportDof1, const felInt ielSupportDof2, FlagMatrixRHS flagMatrixRHS, bool buildMatT)
{
  std::vector<felInt> vecIelSupportDof1(m_listUnknown.getUnknownsRows().size(), ielSupportDof1);
  std::vector<felInt> vecIelSupportDof2(m_listUnknown.getUnknownsCols().size(), ielSupportDof2);
  setValueMatrixRHSBD(vecIelSupportDof1, vecIelSupportDof2, flagMatrixRHS, buildMatT);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomMatrixBD(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, PetscMatrix& mat, bool buildMatT)
{
  FEL_ASSERT( !m_elementMatBD.empty()  );
  FEL_ASSERT( buildMatT ? !m_elementMatTBD.empty() : true );
  FEL_ASSERT( m_GposColumn );
  FEL_ASSERT( m_GposLine   );

  const felInt numDofTotal1 = m_elementMatBD[0]->mat().size1();
  const felInt numDofTotal2 = m_elementMatBD[0]->mat().size2();
  int idVar;
  felInt cptGpos;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. for the rows of the matrix or RHS)
  cptGpos = 0;
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    m_dof.loc2glob(ielSupportDof1[iUnknown], m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);         
              
    for(std::size_t i=0; i<loc2globTmp.size(); ++i)
      m_GposLine[cptGpos++] = loc2globTmp[i];
  }

  // Build the loc2glob map m_GposColumn for the second support element (i.e. for the columns of the matrix)
  if(ielSupportDof1 == ielSupportDof2 && m_listUnknown.getUnknownsRows() == m_listUnknown.getUnknownsCols() ) {
    // if the two support element are the same (always the case unless we have duplicated support element) just copy the first one
    for ( felInt i = 0; i < numDofTotal1; ++i )
      m_GposColumn[i] = m_GposLine[i];
  } else {
    // if they are different, compute it.
    cptGpos = 0;
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
      idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
      m_dof.loc2glob(ielSupportDof2[iUnknown], m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);

      for(std::size_t i=0; i<loc2globTmp.size(); ++i)
        m_GposColumn[cptGpos++] = loc2globTmp[i];
    }
  }

  AOApplicationToPetsc(m_ao,numDofTotal1,m_GposLine);
  AOApplicationToPetsc(m_ao,numDofTotal2,m_GposColumn);
  mat.setValues(numDofTotal1,m_GposLine,numDofTotal2,m_GposColumn,m_elementMatBD[0]->mat().data().begin(),ADD_VALUES);
  if (buildMatT)
    mat.setValues(numDofTotal2,m_GposColumn,numDofTotal1,m_GposLine,m_elementMatTBD[0]->mat().data().begin(),ADD_VALUES);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomMatrixBD(const felInt ielSupportDof1, const felInt ielSupportDof2, PetscMatrix& mat, bool buildMatT)
{
  std::vector<felInt> vecIelSupportDof1(m_listUnknown.getUnknownsRows().size(), ielSupportDof1);
  std::vector<felInt> vecIelSupportDof2(m_listUnknown.getUnknownsCols().size(), ielSupportDof2);
  setValueCustomMatrixBD(vecIelSupportDof1, vecIelSupportDof2, mat, buildMatT); 
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomVectorBD(const std::vector<felInt>& ielSupportDof1, InsertMode mode, PetscVector& vec) 
{
  FEL_ASSERT(!m_elementVectorBD.empty());
  FEL_ASSERT( m_GposLine );

  const felInt numDofTotal = m_elementVectorBD[0]->vec().size();
  int idVar;
  felInt cptGpos;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. for the rows of the matrix or RHS)
  cptGpos = 0; 
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    m_dof.loc2glob(ielSupportDof1[iUnknown], m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);
     
    for(std::size_t i=0; i<loc2globTmp.size(); ++i)
      m_GposLine[cptGpos++] = loc2globTmp[i];
  }

  // Set values
  AOApplicationToPetsc(m_ao,numDofTotal,m_GposLine);
  vec.setValues(numDofTotal,m_GposLine,m_elementVectorBD[0]->vec().data().begin(),mode);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueCustomVectorBD(const felInt ielSupportDof1, InsertMode mode, PetscVector& vec)
{
  std::vector<felInt> vecIelSupportDof1(m_listUnknown.getUnknownsRows().size(), ielSupportDof1);
  setValueCustomVectorBD(vecIelSupportDof1, mode, vec);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS flagMatrixRHS) 
{
  int idVar;
  felInt numDofTotalL = 0;
  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsRows()[iUnknown]);
    numDofTotalL += m_listCurvilinearFiniteElement[idVar]->numDof()*m_listVariable[idVar].numComponent();
  }
  m_GposLine = new felInt[numDofTotalL];

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    felInt numDofTotalC = 0;
    for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.getUnknownsCols().size(); iUnknown++) {
      idVar = m_listUnknown.idVariable(m_listUnknown.getUnknownsCols()[iUnknown]);
      numDofTotalC += m_listCurvilinearFiniteElement[idVar]->numDof()*m_listVariable[idVar].numComponent();
    }
    m_GposColumn = new felInt[numDofTotalC];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::assembleMatrixRHSCurrentAndCurvilinearElement(int rank, FlagMatrixRHS flagMatrixRHS) 
{
  IGNORE_UNUSED_RANK;

  ElementType eltType;           //geometric element type in the mesh.
  int numPointPerElt = 0;        //number of points per geometric element.
  int currentLabel = 0;          //number of label domain.
  felInt numEltPerLabel = 0;     //number of element for one label and one eltType.

  // Use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // Contains points of the current element.
  std::vector<Point*> elemPoint;

  // Contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // Contains ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // Use to get element number in support dof mesh ordering.
  felInt ielSupportDof;
  felInt ielSupportDofCurv;

  //Assembly loop current.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
      
    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // Define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);

    //Element matrix and vector initialisation
    initElementArray();

    // Allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // Virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // use by user to add specific term (source term for example with elemField).
    userElementInit();

    // Second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      //By default this virtual defnie variable m_currentLabel: (m_currentLabel=label).
      //We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);

        // Third loop on element in the region with the type: eltType. ("real" loop on elements).
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {

          // Return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

          // Loop over all the support elements
          for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
            // Get the id of the support
            ielSupportDof = vectorIdSupport[it];

            for (std::size_t j = 0; j < m_matrices.size(); j++)
              m_elementMat[j]->zero();

            // Clear elementary vector.
            for (std::size_t j = 0; j < m_vectors.size(); j++)
              m_elementVector[j]->zero();

            // Function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
            computeElementArray(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);

            // Compute specific term of users.
            //userElementCompute(elemPoint, elemIdPoint, ielSupportDof);

            // Add values of elemMat in the global matrix: m_matrices[0].
            setValueMatrixRHS(ielSupportDof, ielSupportDof, flagMatrixRHS);
        }
        numElement[eltType]++;
    }
    }
    // Desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  //Assembly loop curvilinear.
  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    eltType =  bagElementTypeDomainBoundary[i];

    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // Define all current finite element use in the problem from data
    defineCurvilinearFiniteElement(eltType);

    // Element matrix and vector initialisation
    initElementArrayBD();

    //allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);

    //virtual function use in derived problem to allocate elemenField necessary.
    initPerElementTypeBoundaryCondition(eltType, flagMatrixRHS);

    // use by user to add specific term (term source for example with elemField).
    userElementInitNaturalBoundaryCondition();

    FEL_ASSERT(!m_elementVectorBD.empty());

    // allocate elem field and setValue if BC = constant in space
    // if BC = function : setValue in users
    //      allocateElemFieldBoundaryCondition(idBCforLinCombMethod);

    //second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      //By default this virtual define variable m_currentLabel: (m_currentLabel=label).
      //We can switch on label region with that and define some parameters.
      //We can also fill the elemField allocated in initPerElementTypeBoundaryCondition()
      initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDofCurv, eltType, numElement[eltType], flagMatrixRHS);

      //Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // Loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // Get the id of the support
          ielSupportDofCurv = vectorIdSupport[it];

          // clear elementary matrix.
          for (std::size_t j = 0; j < m_matrices.size(); j++)
            m_elementMatBD[j]->zero();

          // Clear elementary std::vector.
          for (std::size_t j = 0; j < m_vectors.size(); j++)
            m_elementVectorBD[j]->zero();

          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          computeElementArraySurfaceModel(elemPoint, elemIdPoint, ielSupportDofCurv,flagMatrixRHS);

          // add values of elemMat in the global matrix: m_matrices[0].
          // notBoundarySurfaceLabels = list of surfaces/edges that are part of the domain but are not boundaries of volumes/surfaces
          auto bndRefIt = find(FelisceParam::instance(this->instanceIndex()).notBoundarySurfaceLabels.begin(), FelisceParam::instance(this->instanceIndex()).notBoundarySurfaceLabels.end(), currentLabel);

          if(bndRefIt != FelisceParam::instance(this->instanceIndex()).notBoundarySurfaceLabels.end()) {
            //computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDofCurv,flagMatrixRHS);
            setValueMatrixRHSBD(ielSupportDofCurv, ielSupportDofCurv, flagMatrixRHS);
          }
        }
        numElement[eltType]++;
      }
    }

    // Deallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    //real assembling of the matrix manage by PETsC.
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }
  //call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    //real assembling of the right hand side (RHS).
    for (std::size_t index = 0; index < m_vectors.size(); index++) {
      m_vectors[index].assembly();
    }
  }
  //call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::applyBC(const int & applicationOption, int rank, FlagMatrixRHS flagMatrixRHSEss, FlagMatrixRHS flagMatrixRHSNat,
                            const int idBCforLinCombMethod, bool doPrintBC,
                            ApplyNaturalBoundaryConditionsType do_apply_natural)
{
  // Get the timer
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  auto& r_timer = r_instance.timer;

  r_timer.Start("LinearProblem" + std::to_string(m_identifier_problem) + "::applyBC", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  if (doPrintBC && m_verbosity > 2) {
    PetscPrintf(MpiInfo::petscComm(), "\n/============== Information about boundary condition.===============/\n");
    m_boundaryConditionList.print(m_verbosity);
  }
  // In some dynamic problems we might want to be able to skip natural conditions, hence this flag.
  // Its role is absolutely not the same as the other flag addedBoundaryFlag: in a same problem
  // FelisceParam::instance(this->instanceIndex()).addedBoundaryFlag is probably the same all the way whereas do_apply_natural is deemed to be
  // true in the first time iteration and then false otherwise in some time schemes.
  if (do_apply_natural == ApplyNaturalBoundaryConditions::yes) {
    // Natural boundary condition (Neumann, Neumann Normal, Robin, RobinNormal, EmbedFSI, Natural LumpedModelBC, Backflow stabilization for now).
    if (m_boundaryConditionList.NaturalBoundaryCondition()) {
      // Assemble Matrix and/or RHS (depends on flagMatrixRHSNat)
      assembleMatrixRHSNaturalBoundaryCondition(rank, flagMatrixRHSNat, idBCforLinCombMethod);
    }
  }

  #ifdef FELISCE_WITH_CVGRAPH
  if ( FelisceParam::instance(this->instanceIndex()).withCVG ) {
    cvgAddExternalResidualToRHS();
  }
  #endif
  
  // Essential boundary condition (Dirichlet, Essential lumpedModelBC).
  if (m_boundaryConditionList.EssentialBoundaryCondition() || FelisceParam::instance(this->instanceIndex()).useEssDerivedBoundaryCondition) {
    applyEssentialBoundaryCondition(applicationOption, rank, flagMatrixRHSEss);
  }

  r_timer.Stop("LinearProblem" + std::to_string(m_identifier_problem) + "::applyBC", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::assembleMatrixRHSNaturalBoundaryCondition(int rank, FlagMatrixRHS flagMatrixRHS, const int idBCforLinCombMethod) 
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // Geometric element type in the mesh.
  int numPointPerElt = 0;        // Number of points per geometric element.
  int currentLabel = 0;          // Number of label domain.
  felInt numEltPerLabel = 0;     // Number of element for one label and one eltType.

  // Retrieve instance
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  
  // Use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // I need to define the CurrentFEWithBd to correctly compute the stress tensor
  if(m_meshLocal[m_currentMesh]->statusFaces() == false) {
    m_meshLocal[m_currentMesh]->buildFaces();
  }
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t ielt = 0; ielt < bagElementTypeDomain.size(); ++ielt) {
    eltType =  bagElementTypeDomain[ielt];
    defineCurrentFiniteElementWithBd(eltType); // TODO no sense here if bagElementTypeDomain.size() > 1 and probably not needed
  }

  // Contains points of the current element.
  std::vector<Point*> elemPoint;

  // Contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // Contains ids of all the support elements associated to a mesh element
  std::vector<felInt> vectorIdSupport;

  // Use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  allocateVectorBoundaryConditionDerivedLinPb();

  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    eltType = bagElementTypeDomainBoundary[i];
    
    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // Define all current finite element use in the problem from data
    // file configuration and allocate elemMat and elemVec (question: virtual ?).
    defineCurvilinearFiniteElement(eltType);

    // Element matrix and vector initialisation
    initElementArrayBD();

    // Allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);

    // Function (1), (2), (3) doing the same stuff for different use: allocate/initialize elementField
    // (1) Virtual function use in derived problem to allocate elemenField necessary.
    initPerElementTypeBoundaryCondition(eltType, flagMatrixRHS);

    // (2) Use by user to add specific term (term source for example with elemField).
    userElementInitNaturalBoundaryCondition();

    // (3) Allocate Elemfield and setValue if BC = constant in space
    // if BC = function : setValue in users
    allocateElemFieldBoundaryCondition(idBCforLinCombMethod);
    
    // Second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      //By default this virtual function assign currentLabel to the member variable m_currentLabel: (m_currentLabel=currentLabel).
      //We can switch on label region with that and define some parameters.
      //We can also fill the elemField allocated in initPerElementTypeBoundaryCondition()
      initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDof, eltType, numElement[eltType], flagMatrixRHS); //it updates label
      //Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {

        if(!r_instance.duplicateSupportDof) {
          // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

          // Loop over all the support elements
          for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
            // Get the id of the support
            ielSupportDof = vectorIdSupport[it];
            // Clear elementary matrix.
            for (std::size_t j = 0; j < m_matrices.size(); j++)
              m_elementMatBD[j]->zero();

            // Clear elementary vector.
            for (std::size_t j = 0; j < m_vectors.size(); j++)
              m_elementVectorBD[j]->zero();

            // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).initialized in (1)
            computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof,flagMatrixRHS); //it updates fe..
            // compute specific term of users (Neumann transient for example) initialized in (2)
            userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, currentLabel);
            // apply bc initialized in (3) if constant or initialized in (1) or (2) if functions
            applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
            // add values of elemMat in the global matrix: m_matrices[0].
            setValueMatrixRHSBD(ielSupportDof, ielSupportDof, flagMatrixRHS);
          }
        } else {

          m_duplicateSupportDofAssemblyLoopBoundaryCondition(rank, eltType, numElement[eltType], elemPoint, elemIdPoint, flagMatrixRHS);
        }

        numElement[eltType]++;
      }
    }
    //allocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    //real assembling of the matrix manage by PETsC.
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }
  //call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    //real assembling of the right hand side (RHS).
    for (std::size_t index = 0; index < m_vectors.size(); index++) {
      m_vectors[index].assembly();
    }
  }

  //call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::allocateElemFieldBoundaryCondition(const int idBCforLinCombMethod) 
{
  //  if Function case: setValue in User's or derived problems
  int iVariable = -1;
  std::size_t idBC = -1;
  CurvilinearFiniteElement* fe;
  std::vector <double> valueOfBC;
  for (std::size_t i=0; i < m_boundaryConditionList.numNeumannBoundaryCondition(); i++) {
    const BoundaryCondition& BC = *m_boundaryConditionList.Neumann(i);
    const int num_comp = static_cast<int>(BC.getComp().size());

    iVariable = m_listVariable.getVariableIdList(BC.getVariable().physicalVariable());
    idBC = m_boundaryConditionList.idBCOfNeumann(i);
    switch (BC.typeValueBC()) {
      case Constant:
        m_elemFieldNeumann[i].initialize(CONSTANT_FIELD, num_comp);

        for (int j = 0; j < num_comp; j++) {
          valueOfBC.push_back(FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)+j]);
        }
        m_elemFieldNeumann[i].setValue(valueOfBC);
        break;
      case FunctionS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldNeumann[i].initialize(QUAD_POINT_FIELD, *fe, num_comp);
        break;
      case FunctionT:
        m_elemFieldNeumann[i].initialize(CONSTANT_FIELD, num_comp);
        break;
      case FunctionTS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldNeumann[i].initialize(QUAD_POINT_FIELD, *fe, num_comp);
        break;
      case EnsightFile:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldNeumann[i].initialize(DOF_FIELD, *fe, num_comp);
        break;
      case Vector:
        //FEL_ERROR("Impossible to define elemFieldNeumann with this type of Boundary Condition.");
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldNeumann[i].initialize(DOF_FIELD, *fe, num_comp);
        break;

    }
    if (m_verbosity > 3) {
      std::cout << "i : " << i << std::endl;
      std::cout << "Neummann: ";
      m_elemFieldNeumann[i].print(2);
    }
    valueOfBC.clear();
  }

  for (std::size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); i++) {

    const BoundaryCondition& BC = *m_boundaryConditionList.NeumannNormal(i);

    FEL_ASSERT_EQUAL(BC.getComp().size(), 1);

    iVariable = m_listVariable.getVariableIdList(BC.getVariable().physicalVariable());
    idBC = m_boundaryConditionList.idBCOfNeumannNormal(i);
    switch (BC.typeValueBC()) {
      case Constant:
        m_elemFieldNeumannNormal[i].initialize(CONSTANT_FIELD);
        // no setValue if there is no value in datafile (i.e. implicit lumpedModelBC)
        if ( static_cast<std::size_t>(m_boundaryConditionList.startIndiceOfValue(idBC)) < FelisceParam::instance(this->instanceIndex()).value.size() ) {
          valueOfBC.push_back(FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)]);
          m_elemFieldNeumannNormal[i].setValue(valueOfBC);
        }
        break;
      case FunctionS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldNeumannNormal[i].initialize(QUAD_POINT_FIELD, *fe);
        break;
      case FunctionT:
        m_elemFieldNeumannNormal[i].initialize(CONSTANT_FIELD);
        break;
      case FunctionTS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldNeumannNormal[i].initialize(QUAD_POINT_FIELD, *fe);
        break;
      case Vector:
      case EnsightFile:
        FEL_ERROR("Impossible to define elemFieldNeumannNormal with this type of Boundary Condition.");
        break;
    }
    valueOfBC.clear();
    if (m_verbosity > 3) {
      std::cout << "NeummannNormal: ";
      m_elemFieldNeumannNormal[i].print(2);
    }
  }

  for (std::size_t i=0; i < m_boundaryConditionList.numRobinBoundaryCondition(); i++) {
    const BoundaryCondition& BC = *m_boundaryConditionList.Robin(i);
    const int numComp = static_cast<int>(BC.getComp().size());

    iVariable = m_listVariable.getVariableIdList(BC.getVariable().physicalVariable());
    idBC = m_boundaryConditionList.idBCOfRobin(i);
    switch (BC.typeValueBC()) {
      case Constant:
        m_elemFieldRobin[i].initialize(CONSTANT_FIELD,numComp);
        for (int j = 0; j < numComp; j++) {
          valueOfBC.push_back(FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)+j]);
        }
        m_elemFieldRobin[i].setValue(valueOfBC);
        break;
      case FunctionS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldRobin[i].initialize(QUAD_POINT_FIELD, *fe,numComp);
        break;
      case FunctionT:
        m_elemFieldRobin[i].initialize(CONSTANT_FIELD,numComp);
        break;
      case FunctionTS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldRobin[i].initialize(QUAD_POINT_FIELD, *fe,numComp);
        break;
      case EnsightFile:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldRobin[i].initialize(DOF_FIELD, *fe,numComp);
        break;
      case Vector:
        FEL_ERROR("Impossible to define elemFieldRobin with this type of Boundary Condition.");
        break;
    }
    valueOfBC.clear();
    if (m_verbosity > 3) {
      std::cout << "Robin: ";
      m_elemFieldRobin[i].print(2);
    }
  }

  //RobinNormal
  for (std::size_t i=0; i < m_boundaryConditionList.numRobinNormalBoundaryCondition(); i++) {
    const BoundaryCondition& BC = *m_boundaryConditionList.RobinNormal(i);

    FEL_ASSERT_EQUAL(BC.getComp().size(), 1);

    iVariable = m_listVariable.getVariableIdList(BC.getVariable().physicalVariable());
    idBC = m_boundaryConditionList.idBCOfRobinNormal(i);
    switch (BC.typeValueBC()) {
      case Constant:
        m_elemFieldRobinNormal[i].initialize(CONSTANT_FIELD);
        if ( static_cast<std::size_t>(m_boundaryConditionList.startIndiceOfValue(idBC)) < FelisceParam::instance(this->instanceIndex()).value.size() ) {
          valueOfBC.push_back(FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)]);
          m_elemFieldRobinNormal[i].setValue(valueOfBC);
        }
        break;
      case FunctionS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldRobinNormal[i].initialize(QUAD_POINT_FIELD, *fe);
        break;
      case FunctionT:
        m_elemFieldRobinNormal[i].initialize(CONSTANT_FIELD);
        break;
      case FunctionTS:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldRobinNormal[i].initialize(QUAD_POINT_FIELD, *fe);
        break;
      case EnsightFile:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldRobinNormal[i].initialize(DOF_FIELD, *fe);
        break;
      case Vector:
        FEL_ERROR("Impossible to define elemFieldRobinNormal with this type of Boundary Condition.");
        break;
    }

    valueOfBC.clear();
    if (m_verbosity > 3) {
      std::cout << "RobinNormal: ";
      m_elemFieldRobinNormal[i].print(2);
    }
  }

  //EmbedFSI new: matteo Feb 2014
  for (std::size_t i=0; i < m_boundaryConditionList.numEmbedFSIBoundaryCondition(); i++) {
    const BoundaryCondition& BC = *m_boundaryConditionList.EmbedFSI(i);

    iVariable = m_listVariable.getVariableIdList(BC.getVariable().physicalVariable());
    idBC = m_boundaryConditionList.idBCOfEmbedFSI(i);

    FEL_ASSERT_EQUAL( BC.getComp().size(), m_mesh[m_listVariable[iVariable].idMesh()]->domainDim());
    FEL_ASSERT_EQUAL( BC.typeValueBC(),    EnsightFile);

    fe = m_listCurvilinearFiniteElement[iVariable];
    FEL_ASSERT(fe);

    switch (BC.typeValueBC()) {
      case Constant:
        m_elemFieldEmbedFSI[i].initialize(CONSTANT_FIELD);
        if ( static_cast<std::size_t>(m_boundaryConditionList.startIndiceOfValue(idBC)) < FelisceParam::instance(this->instanceIndex()).value.size() ) {
          valueOfBC.push_back(FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)]);
          m_elemFieldEmbedFSI[i].setValue(valueOfBC);
        }
        break;
      case FunctionS:
        m_elemFieldEmbedFSI[i].initialize(QUAD_POINT_FIELD, *fe, BC.getComp().size());
        break;
      case FunctionT:
        m_elemFieldEmbedFSI[i].initialize(CONSTANT_FIELD, BC.getComp().size());
        break;
      case FunctionTS:
        m_elemFieldEmbedFSI[i].initialize(QUAD_POINT_FIELD, *fe, BC.getComp().size());
        break;
      case EnsightFile:
        m_elemFieldEmbedFSI[i].initialize(DOF_FIELD, *fe, BC.getComp().size());
        break;
      case Vector:
        FEL_ERROR("Impossible to define elemFieldEmbedFSI with this type of Boundary Condition.");
        break;
    }

    m_elemFieldNormalForEmbedFSI[i].initialize( DOF_FIELD, *fe, fe->numCoor() );
    m_elemFieldAlphaForEmbedFSI[i].initialize( DOF_FIELD, *fe, fe->numCoor() );

    valueOfBC.clear();
    if (m_verbosity > 3) {
      std::cout << "EmbedFSI: ";
      m_elemFieldEmbedFSI[i].print(2);
      std::cout << "NormalForEmbedFSI: ";
      m_elemFieldNormalForEmbedFSI[i].print(2);
      std::cout << "AlphaForEmbedFSI: ";
      m_elemFieldAlphaForEmbedFSI[i].print(2);
    }
  }

  // Backflow stabilization, june 2018
  for (std::size_t i=0; i < m_boundaryConditionList.numBackflowStabBoundaryCondition(); i++) {
    const BoundaryCondition& BC = *m_boundaryConditionList.BackflowStabBC(i);

    FEL_ASSERT_EQUAL(BC.getComp().size(), 1); // for backflow stab use only Comp1 to provide the stab coef

    iVariable = m_listVariable.getVariableIdList(BC.getVariable().physicalVariable()); // physicalVariable must be Velocity
    idBC = m_boundaryConditionList.idBCOfBackflowStab(i);
    switch (BC.typeValueBC()) {
      case Constant:
        fe = m_listCurvilinearFiniteElement[iVariable];
        FEL_ASSERT(fe);
        m_elemFieldBackflowStab[i].initialize(DOF_FIELD,*fe,dimension());
        break;
      default:
        FEL_ERROR("For backflow stabilization b.c., the only possible type is 'constant'");
        break;
    }

    valueOfBC.clear();
    if (m_verbosity > 3) {
      std::cout << "BackflowStab: ";
      m_elemFieldBackflowStab[i].print(2);
    }
  }

  allocateElemFieldBoundaryConditionDerivedLinPb(idBCforLinCombMethod);//used in linearProblemNSFracStepProj
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  IGNORE_UNUSED_ELEM_ID_POINT;
  IGNORE_UNUSED_IEL;
  // \todo Dec 2, 2011: Warning: it is inefficient to update all current bd fe here.
  // Should depend on the variable where it is necessary (cf boundary condition fields).
  // Beside, the normal should not be systematically computed

  for (std::size_t iFe = 0; iFe < m_listCurvilinearFiniteElement.size(); iFe++) {
    m_listCurvilinearFiniteElement[iFe]->updateMeasNormal(0, elemPoint);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::applyNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_IEL;
  IGNORE_UNUSED_ELEM_POINT;
  IGNORE_UNUSED_ELEM_ID_POINT;
  BoundaryCondition* BC = nullptr;
  CurvilinearFiniteElement* fe = nullptr;
  int iUnknown = -1;
  int iblock = -1;
  int cptComp = -1;

  FEL_ASSERT(!m_elementVectorBD.empty());

  //Neumann
  for (std::size_t iN=0; iN < m_boundaryConditionList.numNeumannBoundaryCondition(); iN++) {
    BC = m_boundaryConditionList.Neumann(iN);
    const int iVariable = m_listVariable.getVariableIdList(BC->getVariable().physicalVariable());
    iUnknown = BC->getUnknown();
    fe = m_listCurvilinearFiniteElement[iVariable];
    FEL_ASSERT(fe);
    for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
      if(*it_labelNumber == m_currentLabel) {
        cptComp = 0;
        if (m_verbosity>4) {
          std::cout << "LABEL = " << m_currentLabel << std::endl;
          std::cout << "NEUMANN= ";
          m_elemFieldNeumann[iN].print(2);
        }
        if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
          for(auto it_comp = BC->getComp().begin(); it_comp != BC->getComp().end(); ++it_comp) {
            iblock = m_listUnknown.getBlockPosition(iUnknown,*it_comp);
            m_elementVectorBD[0]->sourceForComp(1.,*fe,m_elemFieldNeumann[iN],iblock,cptComp);
            cptComp++;
          }
        }
      }
    }
  }

  //Neumann Normal
  for (std::size_t iNN = 0; iNN < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); iNN++) {
    BC = m_boundaryConditionList.NeumannNormal(iNN);
    const int iVariable = m_listVariable.getVariableIdList(BC->getVariable().physicalVariable());
    iUnknown = BC->getUnknown();
    fe = m_listCurvilinearFiniteElement[iVariable];
    FEL_ASSERT(fe);
    for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
      if(*it_labelNumber == m_currentLabel) {
        iblock = m_listUnknown.getBlockPosition(iUnknown,0);
        if (m_verbosity>3 ) {
          std::cout << "LABEL = " << m_currentLabel << std::endl;
          std::cout << "NEUMANN Normal= ";
          m_elemFieldNeumannNormal[iNN].print(2);
        }
        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
          m_elementVectorBD[0]->f_phi_i_scalar_n(1.,*fe,m_elemFieldNeumannNormal[iNN],iblock);
        }
      }
    }
  }

  //Robin
  for (std::size_t iRobin=0; iRobin<m_boundaryConditionList.numRobinBoundaryCondition(); iRobin++ ) {
    BC = m_boundaryConditionList.Robin(iRobin);
    const int iVariable = m_listVariable.getVariableIdList(BC->getVariable().physicalVariable());
    iUnknown = BC->getUnknown();
    fe = m_listCurvilinearFiniteElement[iVariable];
    FEL_ASSERT(fe);
    for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
      if(*it_labelNumber == m_currentLabel) {
        cptComp = 0;
        iblock = m_listUnknown.getBlockPosition(iUnknown,0);

        double coef = FelisceParam::instance(this->instanceIndex()).alphaRobin[iRobin]; //default 0
        const double coef2= FelisceParam::instance(this->instanceIndex()).betaRobin[iRobin]; //default 1
        coef=coef/coef2;
        if (m_verbosity>3 ) {
          std::cout << "LABEL = " << m_currentLabel << std::endl;
          std::cout << "ROBIN = ";
          m_elemFieldRobin[iRobin].print(2);
          std::cout<< "coef "<<coef<< " coef2 "<<coef2<<std::endl;
        }
        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
          m_elementMatBD[0]->phi_i_phi_j(coef,*fe,iblock,iblock,m_listVariable[iVariable].numComponent());
        }
        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
          for(auto it_comp = BC->getComp().begin(); it_comp != BC->getComp().end(); ++it_comp) {
            iblock = m_listUnknown.getBlockPosition(iUnknown,*it_comp);
            m_elementVectorBD[0]->sourceForComp(1./coef2,*fe,m_elemFieldRobin[iRobin],iblock,cptComp);
            cptComp++;
          }
        }

      }
    }
  }

  //RobinNormal
  for (std::size_t iRobinNormal=0; iRobinNormal<m_boundaryConditionList.numRobinNormalBoundaryCondition(); iRobinNormal++ ) {
    BC = m_boundaryConditionList.RobinNormal(iRobinNormal);
    const int iVariable = m_listVariable.getVariableIdList(BC->getVariable().physicalVariable());
    fe = m_listCurvilinearFiniteElement[iVariable];
    iUnknown = BC->getUnknown();
    FEL_ASSERT(fe);
    for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
      if(*it_labelNumber == m_currentLabel) {
        iblock = m_listUnknown.getBlockPosition(iUnknown,0);
        if (m_verbosity>3 ) {
          std::cout << "LABEL = " << m_currentLabel << std::endl;
          std::cout << "RobinNormal = ";
          m_elemFieldRobinNormal[iRobinNormal].print(2);
        }
        double coef = FelisceParam::instance(this->instanceIndex()).alphaRobinNormal[iRobinNormal];
        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
          m_elementMatBD[0]->phi_i_dot_n_phi_j_dot_n(coef, *fe, iblock, iblock, 0);
        }
        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
          m_elementVectorBD[0]->f_phi_i_scalar_n(1.,*fe,m_elemFieldRobinNormal[iRobinNormal],iblock);
        }
      }
    }
  }

  //EmbedFSI
  for (std::size_t iEmbedFSI=0; iEmbedFSI<m_boundaryConditionList.numEmbedFSIBoundaryCondition(); iEmbedFSI++ ) {
    //Identifying the variables
    const int iUnknownPres = m_listUnknown.getUnknownIdList(pressure);
    const int idVarPres = m_listUnknown.idVariable(iUnknownPres);
    const int iMeshPres = m_listVariable[idVarPres].idMesh();
    const int iUnknownVel = m_listUnknown.getUnknownIdList(velocity);
    const int idVarVel = m_listUnknown.idVariable(iUnknownVel);
    const int iMeshVel = m_listVariable[idVarVel].idMesh();

    if ( iMeshPres != iMeshVel )
      FEL_ERROR("Variables must be defined on same mesh");


    CurvilinearFiniteElement& fePres  = *m_listCurvilinearFiniteElement[idVarPres];
    fePres.updateMeasNormal(0, elemPoint);

    // Retriving the Curvlinear FE (as usual..)
    BC = m_boundaryConditionList.EmbedFSI(iEmbedFSI);
    fe = m_listCurvilinearFiniteElement[idVarVel];
    FEL_ASSERT(fe);
    fe->updateMeasNormal(0, elemPoint); //maybe it is not needed here

    for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++ ) {
      if( *it_labelNumber == m_currentLabel ) {

        cptComp = 0;
        iblock = m_listUnknown.getBlockPosition(iUnknownVel,0);
        int presBlock = m_listUnknown.getBlockPosition(iUnknownPres,0);
        if (m_verbosity>3 ) {
          std::cout << "LABEL = " << m_currentLabel << std::endl;
          std::cout << "EmbedFSI = ";
          m_elemFieldEmbedFSI[iEmbedFSI].print(2);
          std::cout << "Normal for EmbedFSI = ";
          m_elemFieldNormalForEmbedFSI[iEmbedFSI].print(2);
          std::cout << "Alpha for EmbedFSI = ";
          m_elemFieldAlphaForEmbedFSI[iEmbedFSI].print(2);
        }

        const double PenalizationParameter = FelisceParam::instance(this->instanceIndex()).penValueForEmbedFSI;

        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix ) {
          //first order transpiration
          //penalization of -p, we do that on the nodes!
          m_elementMatBD[0]->psi_j_phi_i_dot_n_on_nodes(-PenalizationParameter, m_elemFieldNormalForEmbedFSI[iEmbedFSI], *fe, fePres, iblock, presBlock);
          m_elementMatBD[0]->a_phi_i_phi_j( PenalizationParameter, m_elemFieldAlphaForEmbedFSI[iEmbedFSI], *fe, iblock,iblock, m_listVariable[idVarVel].numComponent());
        }

        if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
          // first order transpiration + force term
          m_elementVectorBD[0]->source( PenalizationParameter, *fe, m_elemFieldEmbedFSI[iEmbedFSI], iblock, m_mesh[iMeshVel]->domainDim());
        }
      }
    }
  }

  //Backflow stab
  for (std::size_t iBackflow=0; iBackflow<m_boundaryConditionList.numBackflowStabBoundaryCondition(); iBackflow++ ) {
    BC = m_boundaryConditionList.BackflowStabBC(iBackflow);
    const int iVelocity = m_listVariable.getVariableIdList(BC->getVariable().physicalVariable());
    fe = m_listCurvilinearFiniteElement[iVelocity];
    iUnknown = BC->getUnknown();
    FEL_ASSERT(fe);
    for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
      if(*it_labelNumber == m_currentLabel) {
        iblock = m_listUnknown.getBlockPosition(iUnknown,0);
        if (m_verbosity>3 ) {
          std::cout << "LABEL = " << m_currentLabel << std::endl;
          std::cout << "BackflowStab = ";
          m_elemFieldBackflowStab[iBackflow].print(2);
        }
        std::size_t idBC = m_boundaryConditionList.idBCOfBackflowStab( iBackflow );
        double coef = FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)]*FelisceParam::instance(this->instanceIndex()).density;
        m_elemFieldBackflowStab[iBackflow].setValue(sequentialSolution(),*fe, iel, iVelocity, m_ao, dof());
        m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef,*fe,m_elemFieldBackflowStab[iBackflow], 0, 0, this->dimension(),2);
        if(flagMatrixRHS == FlagMatrixRHS::only_rhs) {
          std::cout << "WARNING !!!! Strange to assemble only the RHS std::vector if you want bacflow stab. To be checked ?" << std::endl;
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeAndApplyElementNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof1, felInt& ielSupportDof2, felInt& ielGeoGlobal, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_ARGUMENT(ielSupportDof2);
  IGNORE_UNUSED_ARGUMENT(ielGeoGlobal);
  // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).initialized in (1)
  computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, flagMatrixRHS);
  // compute specific term of users (Neumann transient for example) initialized in (2)
  // It seems to be unnecessary to pass currentLabel to this function.
  // In fact similar functions such as userElementCompute does not require this argument.
  userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, ielSupportDof2, ielGeoGlobal, m_currentLabel);
  // apply bc initialized in (3) if constant or initialized in (1) or (2) if functions
  applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, flagMatrixRHS);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::defineBC() 
{
  //TODO:
  // maybe variables are no longer needed
  // for now I have improved the comments (matteo)
  // I changed the parts when a not-unknown variable is found (the elses where the fake BC was created)
  int iVar = 0;
  Variable variable;
  std::vector <int> listTmp;
  int start = 0;
  int startValue = 0;
  bool unknownFound = false;
  int idUnknown = -1;
  // for each boundary condition
  for ( std::size_t i = 0; i < FelisceParam::instance(this->instanceIndex()).type.size(); i++) {
    // if we remove the distinction between variables and unknowns (so that we only have unknowns)
    // the code would be something like
    // variable = FelisceParam.instance().variable[i];
    // some type convertion on variable
    // if variable in variableList:
    //   create the BC
    // else
    //  skip it
    //
    // and all this part > > >
    // get the variable associated to the boundary condition

    iVar = m_listVariable.getVariableIdList(FelisceParam::instance(this->instanceIndex()).variable[i]);
    // if the read variable belongs to a different linearProblem,but in the same model it may not be present in the varialelist: hence this check
    FEL_CHECK(iVar>=0,"hello, You have to declare all the variables of the model into each linear problem")
    variable = m_listVariable[iVar];
    // reset flag
    unknownFound = false;
    for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size() && unknownFound == false ; iUnknown++) {
      if ( m_listUnknown.idVariable(iUnknown) == iVar) {
        idUnknown = iUnknown;
        unknownFound = true;
      }
    }

    if (unknownFound) { // if it is an unknown:
      // for each of the label associated to that BC, store the labels in the listTmp //TODO rename listTmp
      for (int j = 0; j < FelisceParam::instance(this->instanceIndex()).numLabel[i]; j++)
        listTmp.push_back(FelisceParam::instance(this->instanceIndex()).label[j+start]);
      start += FelisceParam::instance(this->instanceIndex()).numLabel[i];

      // create the object bc
      BoundaryCondition* bc = new BoundaryCondition(FelisceParam::instance(this->instanceIndex()).type[i], FelisceParam::instance(this->instanceIndex()).typeValue[i], listTmp, variable, FelisceParam::instance(this->instanceIndex()).component[i]);
      //set its unknown id
      bc->setUnknown(idUnknown);

      // get the correct starting point for reading the values: PAY ATTENTION even if the type of the BC is not constant you have to define a number in the data file!
      startValue += bc->getComp().size();
      if (m_boundaryConditionList.size() == 0) //if it is the first BC to be added in the list
        m_boundaryConditionList.startIndiceOfValue().push_back(startValue - bc->getComp().size()); //this is not simply zero.

      // add the starting value
      m_boundaryConditionList.startIndiceOfValue().push_back(startValue);
      //add also the BC
      m_boundaryConditionList.add(bc);
      //clear the list of labels
      listTmp.clear();
    } else {
      // now it is not necessary to create a BC
      startValue += BoundaryCondition::determineNumComponent( FelisceParam::instance(this->instanceIndex()).component[i] );
      start += FelisceParam::instance(this->instanceIndex()).numLabel[i];
    }
  }

  if (FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size()>0) {
    // explicit/implicit and fractional step model // TODO D.C. why are here??? maybe these conditions should be in the appropriate linearProblem...
    //====================================
    if(FelisceParam::instance(this->instanceIndex()).model == "NSFracStep") {
      iVar = m_listVariable.getVariableIdList(pressure);
      variable = m_listVariable[iVar];
      for ( std::size_t i = 0; i < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); i++) {
        if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[i] == 1) {
          for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size() && unknownFound == false ; iUnknown++) {
            if (m_listUnknown.idVariable(iUnknown) == iVar) {
              idUnknown = iUnknown;
              unknownFound = true;
            }
          }
          if (unknownFound) {
            BoundaryCondition* BC = new BoundaryCondition(EssentialLumpedModelBC,Constant,FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel[i],variable,Comp1);
            BC->getUnknown() = idUnknown;
            m_boundaryConditionList.add(BC);
          }
          unknownFound = false;
        } else if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[i] == 2) {
          FEL_ERROR("LinearProblem::defineBC() : check the implicit algo with NSFracStep model");
        }
      }
    } 
    else if(FelisceParam::instance(this->instanceIndex()).model == "NS") { // explicit/implicit and monolithic model
      iVar = m_listVariable.getVariableIdList(velocity);
      variable = m_listVariable[iVar];
      for ( std::size_t i = 0; i < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); i++) {
        if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[i] == 1 || FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[i] == 2) {
          for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size() && unknownFound == false ; iUnknown++) {
            if ( m_listUnknown.idVariable(iUnknown) == iVar) {
              idUnknown = iUnknown;
              unknownFound = true;
            }
          }
          if (unknownFound) {
            BoundaryCondition* BC = new BoundaryCondition(NaturalLumpedModelBC,Constant,FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel[i],variable,Comp1);
            BC->getUnknown() = idUnknown;
            m_boundaryConditionList.add(BC);
          }
          unknownFound = false;
        }
      }
    } 
    else if(FelisceParam::instance(this->instanceIndex()).model == "NSlinComb") { // implicit and NSlinCombModel
      // we define the lumpedModel BC as NeumannNormal boundary conditions
      iVar = m_listVariable.getVariableIdList(velocity);
      variable = m_listVariable[iVar];
      for ( std::size_t i = 0; i < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); i++) {
        if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[i] == 2) {
          for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size() && unknownFound == false ; iUnknown++) {
            if ( m_listUnknown.idVariable(iUnknown) == iVar) {
              idUnknown = iUnknown;
              unknownFound = true;
            }
          }
          if (unknownFound) {
            BoundaryCondition* BC = new BoundaryCondition(NeumannNormal,Constant,FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel[i],variable,Comp1);
            BC->getUnknown() = idUnknown;
            // define startIndiceOfValue for 'artificial' NN BC
            startValue += BC->getComp().size();
            if (m_boundaryConditionList.size() == 0)
              m_boundaryConditionList.startIndiceOfValue().push_back(startValue - BC->getComp().size());
            m_boundaryConditionList.startIndiceOfValue().push_back(startValue);
            m_boundaryConditionList.add(BC);
          } else {
            //OLD
            //BoundaryCondition bc(NeumannNormal, Constant, FelisceParam::instance().lumpedModelBCLabel[i],variable, Comp1);
            //startValue += bc.getComp().size();
            //NEW
            startValue += BoundaryCondition::determineNumComponent( FelisceParam::instance(this->instanceIndex()).component[i] );
          }
          unknownFound = false;
        }
      }
    } 
    else { // default case
      FEL_ERROR("You have to use the right model with the right algorithm for lumpedModelBC boundary conditions : explicit algorithm is implemented with NSModel or NSFracStepModel, and implicit one with NSModel or NSlinCombModel. You have to take care in your datafile.");
    }
  }

  m_elemFieldNeumann.resize(m_boundaryConditionList.numNeumannBoundaryCondition());
  m_elemFieldNeumannNormal.resize(m_boundaryConditionList.numNeumannNormalBoundaryCondition());
  m_elemFieldRobin.resize(m_boundaryConditionList.numRobinBoundaryCondition());
  m_elemFieldRobinNormal.resize(m_boundaryConditionList.numRobinNormalBoundaryCondition());
  m_elemFieldEmbedFSI.resize(m_boundaryConditionList.numEmbedFSIBoundaryCondition());
  m_elemFieldNormalForEmbedFSI.resize(m_boundaryConditionList.numEmbedFSIBoundaryCondition());
  m_elemFieldAlphaForEmbedFSI.resize(m_boundaryConditionList.numEmbedFSIBoundaryCondition());
  m_elemFieldBackflowStab.resize(m_boundaryConditionList.numBackflowStabBoundaryCondition());
}

/***********************************************************************************/
/***********************************************************************************/

template<>
void LinearProblem::applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::symmetricPseudoElimination>(FlagMatrixRHS flagMatrixRHS)
{
  std::vector<felInt> rows;
  for (int i=0, numDirichletBC = m_boundaryConditionList.numDirichletBoundaryCondition(); i < numDirichletBC; i++) {
    const BoundaryCondition* const BC = m_boundaryConditionList.Dirichlet(i);
    applyEssentialBoundaryConditionViaSymmetricPseudoEliminationOnBC(flagMatrixRHS,BC,rows);
  }
  m_matrices[0].zeroRowsColumns(rows.size(), &rows[0], 1.);
}

/***********************************************************************************/
/***********************************************************************************/

template<>
void LinearProblem::applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::nonSymmetricPseudoElimination>(FlagMatrixRHS flagMatrixRHS)
{
  std::unordered_set<felInt> idDofBC;
  std::unordered_map<felInt, double> idBCAndValue;
  std::unordered_set<felInt> idDofOfAllDirichletBC;

  for (std::size_t i=0; i < m_boundaryConditionList.numDirichletBoundaryCondition(); i++) {
    const BoundaryCondition* BC = m_boundaryConditionList.Dirichlet(i);
    idBCAndValue.clear();
    idDofBC.clear();

    getListDofOfBC(*BC, idDofBC, idBCAndValue);

    for(auto itDofBC = idDofBC.begin(); itDofBC != idDofBC.end(); ++itDofBC) {
      idDofOfAllDirichletBC.insert(*itDofBC);
      if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
        vector().setValue(*itDofBC, idBCAndValue[*itDofBC], INSERT_VALUES);
      }
    }
  }

  // Get the number of dof in each process for all Dirichlet BC
  int sizeTmp[MpiInfo::numProc()];
  int sizeDofBC[MpiInfo::numProc()];
  for (int rankProc = 0; rankProc < MpiInfo::numProc(); rankProc++) {
    sizeTmp[rankProc] = 0;
  }

  sizeTmp[MpiInfo::rankProc()] = idDofOfAllDirichletBC.size();

  MPI_Allreduce(sizeTmp, sizeDofBC, MpiInfo::numProc(), MPI_INT, MPI_SUM, MpiInfo::petscComm());

  int startDof = 0;
  for (int rankProc = 0; rankProc < MpiInfo::rankProc(); rankProc++) {
    startDof += sizeDofBC[rankProc];
  }

  // get the number of dof in all processes = sum of sizeDofBC[] (some dof are counted several times)
  int numDofBCTotal = 0;
  for (int rankProc = 0; rankProc < MpiInfo::numProc(); rankProc++) {
    numDofBCTotal += sizeDofBC[rankProc];
  }

  // put all the id dof in an array (some dof are counted several times)
  int dofBCTmp[numDofBCTotal];
  int dofBCArray[numDofBCTotal];
  for (int iDof = 0; iDof < numDofBCTotal; iDof++)
    dofBCTmp[iDof] = 0;

  int iDof = 0;
  for(auto itDofBC = idDofOfAllDirichletBC.begin(); itDofBC != idDofOfAllDirichletBC.end(); ++itDofBC) {
    dofBCTmp[startDof + iDof] = *itDofBC;
    iDof++;
  }

  MPI_Allreduce(&dofBCTmp[0], &dofBCArray[0], numDofBCTotal, MPI_INT, MPI_SUM, MpiInfo::petscComm());

  // rearrange the id dof in the increasing order (each dof is counted only once)
  idDofOfAllDirichletBC.clear();
  for (int iDof2 = 0; iDof2 < numDofBCTotal; iDof2++) {
    idDofOfAllDirichletBC.insert(dofBCArray[iDof2]);
  }
  numDofBCTotal = idDofOfAllDirichletBC.size();

  iDof = 0;
  for(auto itDofBC = idDofOfAllDirichletBC.begin(); itDofBC != idDofOfAllDirichletBC.end(); ++itDofBC) {
    dofBCArray[iDof] = *itDofBC;
    iDof++;
  }

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    m_matrices[0].zeroRows(numDofBCTotal, &dofBCArray[0], 1.0);
  }
}

/***********************************************************************************/
/***********************************************************************************/

template<>
void LinearProblem::applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::penalization>(FlagMatrixRHS flagMatrixRHS)
{
  for (std::size_t i=0; i < m_boundaryConditionList.numDirichletBoundaryCondition(); i++) {
    const BoundaryCondition* BC = m_boundaryConditionList.Dirichlet(i);
    applyEssentialBoundaryConditionViaPenalizationOnBC(flagMatrixRHS,BC);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::applyEssentialBoundaryCondition(int applicationOption, int rank, FlagMatrixRHS flagMatrixRHS)
{
  switch (applicationOption) {
    case DirichletApplicationOptions::nonSymmetricPseudoElimination:
      applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::nonSymmetricPseudoElimination>(flagMatrixRHS);
      break;
    case DirichletApplicationOptions::symmetricPseudoElimination:
      applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::symmetricPseudoElimination>(flagMatrixRHS);
      break;
    case DirichletApplicationOptions::penalization:
      applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::penalization>(flagMatrixRHS);
      break;
    default:
      FEL_ERROR("Three methods only: nonSymmetricPseudoElimination, symmetricPseudoElimination and penalization");
  }

  if (FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size()>0 && applicationOption!=0 && FelisceParam::instance(this->instanceIndex()).model == "NSFracStep")
    FEL_ERROR("Only one method is implemented to apply lumpedModelBC boundary conditons with fractional step method");

  // Other essential boundary conditions
  applyEssentialBoundaryConditionDerivedProblem(rank,flagMatrixRHS);

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    m_matrices[0].assembly(MAT_FINAL_ASSEMBLY);
  }

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    vector().assembly();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::determineDofAssociateToLabel()
{
  BoundaryCondition* BC;

  for (std::size_t idbc = 0 ; idbc < m_boundaryConditionList.numDirichletBoundaryCondition(); idbc ++) {
    BC = m_boundaryConditionList.Dirichlet(idbc);
    determineElementSuppDofAssociateToBC(BC);
  }

  for (std::size_t ilpbc = 0; ilpbc < m_boundaryConditionList.numEssentialLumpedModelBoundaryCondition(); ilpbc ++) {
    BC = m_boundaryConditionList.EssentialLumpedModelBC(ilpbc);
    determineElementSuppDofAssociateToBC(BC);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::determineElementSuppDofAssociateToBC(BoundaryCondition* BC)
{
  ElementType eltType;
  int theLabelMesh = 0; /// distinguish between labels from the mesh and the labels from BC (std::set in data files)
  felInt numElemsPerLabelMesh = 0;
  felInt ielSupportDof = 0;

  /// scan to find element with labelBC == labelMesh
  const int idUnknown = BC->getUnknown();
  if(BC->listLabel().size() == 0) { /// std::set the list of (integer) labels in BC
    for(auto it_nameLabelToLabelNumber = GeometricMeshRegion::descriptionLineEnsightToListLabel.begin(); it_nameLabelToLabelNumber != GeometricMeshRegion::descriptionLineEnsightToListLabel.end(); it_nameLabelToLabelNumber++) {
      if (strcmp(BC->nameLabel().c_str(),it_nameLabelToLabelNumber->first.c_str())==0) {
        for(auto it_labelNumber = it_nameLabelToLabelNumber->second.begin();it_labelNumber != it_nameLabelToLabelNumber->second.end(); it_labelNumber++) {
          BC->listLabel().insert(*it_labelNumber);
        }
      }
    }
  }

  const int idVar = m_listUnknown.idVariable(idUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();

  // Clear everything
  BC->idEltAndIdSupport().clear();
  theLabelMesh = 0;
  numElemsPerLabelMesh = 0;
  ielSupportDof = 0;

  // start from the boundary elements
  felInt idLocalElt = m_meshLocal[iMesh]->getNumDomainElement();

  felInt cptEltPerLabel = 0;
  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMesh]->bagElementTypeDomainBoundary();

  // for each element type in the bag DomainBoundary
  for (std::size_t jj = 0; jj < bagElementTypeDomainBoundary.size(); ++jj) {
    eltType =  bagElementTypeDomainBoundary[jj];

    // for each label in the mesh
    for(auto itLabelMesh = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin();itLabelMesh != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itLabelMesh++) { /// scan all labels in the mesh
      theLabelMesh = itLabelMesh->first;
      numElemsPerLabelMesh = itLabelMesh->second.second;

      // for each label in the BC
      cptEltPerLabel = 0;
      for(auto it_labelBCNumber = BC->listLabel().begin(); it_labelBCNumber != BC->listLabel().end(); it_labelBCNumber++) {

        // if the label of the BC is the same as the one of the mesh
        if ( theLabelMesh == *it_labelBCNumber ) {
          // for each element of this label
          for ( felInt iel = 0; iel < numElemsPerLabelMesh; iel++) {
            // get the local id of the first support element
            m_supportDofUnknownLocal[idUnknown].getIdElementSupport(idLocalElt + cptEltPerLabel, ielSupportDof);

            // for each support dof in this element
            for (int iSupport = 0; iSupport < m_supportDofUnknownLocal[idUnknown].getNumSupportDof(ielSupportDof); iSupport++) {
              // add it to idEltAndIdSupport
              BC->idEltAndIdSupport().emplace_back(idLocalElt + cptEltPerLabel, iSupport);
            }

            ++cptEltPerLabel;
          }
        }
      }
      idLocalElt += numElemsPerLabelMesh;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::finalizeEssBCConstantInT()
{
  BoundaryCondition* BC;
  int idBC = 0;
  std::vector <double> valueOfBC;

  for (std::size_t idc = 0; idc < m_boundaryConditionList.numDirichletBoundaryCondition(); idc++) {
    BC = m_boundaryConditionList.Dirichlet(idc);
    idBC = m_boundaryConditionList.idBCOfDirichlet(idc);
    switch (BC->typeValueBC()) {
      case Constant:
        for (unsigned jdc = 0; jdc < BC->getComp().size(); jdc++) {
          valueOfBC.push_back(FelisceParam::instance(this->instanceIndex()).value[m_boundaryConditionList.startIndiceOfValue(idBC)+jdc]);
        }
        BC->setValue(valueOfBC); /// change the name ?
        break;
      case Vector:
      case FunctionS:
      case FunctionT:
      case FunctionTS:
      case EnsightFile:
        break;
        // Default case should appear with a warning at compile time instead of an error in runtime
        // (that's truly the point of using enums as switch cases)
        //  default:

        //FEL_ERROR("LinearProblem::finalizeEssBCConstantInT: Unknown type of boundary condition");
    }
    valueOfBC.clear();
  }

  userFinalizeEssBCConstantInT();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::finalizeEssBCTransient() 
{
  // Get the timer
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  auto& r_timer = r_instance.timer;

  r_timer.Start("LinearProblem" + std::to_string(m_identifier_problem) + "::finalizeEssBCTransient", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  #ifdef FELISCE_WITH_CVGRAPH
  cvgraphFinalizeEssBC();
  #endif
  finalizeEssBCTransientDerivedProblem();
  userFinalizeEssBCTransient();

  r_timer.Stop("LinearProblem" + std::to_string(m_identifier_problem) + "::finalizeEssBCTransient", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getListDofOfBC(const BoundaryCondition& BC, std::unordered_set<felInt>& idDofBC, std::unordered_map<int, double>& idBCAndValue) 
{
  const int iUnknown = BC.getUnknown();
  const int idVar = m_listUnknown.idVariable(iUnknown);
  std::vector<felInt> idSupportEltLocal;
  felInt idSupportEltGlobal;
  felInt iSupport;
  felInt idDof;
  std::unordered_set<felInt> idDof2;
  int cptComp = 0;

  for (std::size_t iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++) {
    // Get the local id of the support element
    m_supportDofUnknownLocal[iUnknown].getIdElementSupport(BC.idEltAndIdSupport()[iSupportDofBC].first, idSupportEltLocal);
    iSupport = BC.idEltAndIdSupport()[iSupportDofBC].second;

    for(std::size_t ielSup=0; ielSup<idSupportEltLocal.size(); ++ielSup) {
      // Get the global id of the support element
      ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[iUnknown], 1, &idSupportEltLocal[ielSup], &idSupportEltGlobal);

      cptComp = 0;
      for(auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++) {
        // Get the global id of the dof
        m_dof.loc2glob(idSupportEltGlobal, iSupport, idVar, *it_comp, idDof);

        // Get the global id in the petsc ordering
        AOApplicationToPetsc(m_ao, 1, &idDof);

        const bool is_new_element = idDof2.insert(idDof).second;
        if (is_new_element) {
          // insert the new dof
          idDofBC.insert(idDof);
          idBCAndValue[idDof] = BC.ValueBCInSupportDof()[BC.getComp().size()*iSupportDofBC+cptComp];
        }
        cptComp++;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getRings()
{
  if( !m_ringsComputed ) {
    m_ringsComputed = true;

    BoundaryCondition *BCLat=nullptr, *BCIn=nullptr, *BCOut=nullptr;

    userGetBC(BCLat,BCIn,BCOut);

    // TODO: Change to unordered if possible
    std::set<felInt> idDofBCIn,idDofBCOut,idDofBCLat,ausin,ausout,globresult;
    std::set<felInt> idDofBCInLoc,idDofBCOutLoc,idDofBCLatLoc;

    FEL_ASSERT(BCLat)

    determineElementSuppDofAssociateToBC(BCLat);
    getListDofOfSurfaceByBc( *BCLat, idDofBCLatLoc, MpiInfo::rankProc() );
    Tools::allGatherSet(idDofBCLatLoc,idDofBCLat);

    if ( BCIn ) {
      determineElementSuppDofAssociateToBC(BCIn);
      getListDofOfSurfaceByBc( *BCIn,  idDofBCInLoc , MpiInfo::rankProc() );
      Tools::allGatherSet(idDofBCInLoc,idDofBCIn);
      std::set_intersection(idDofBCIn.begin(),idDofBCIn.end(),idDofBCLat.begin(),idDofBCLat.end(),std::inserter(ausin,ausin.begin()));
    } else {
      if ( FelisceParam::verbose()>2 )
        std::cout<<__FILE__<<":"<<__LINE__<<" warning: BCIn is null"<<std::endl;
    }

    if ( BCOut ) {
      determineElementSuppDofAssociateToBC(BCOut);
      getListDofOfSurfaceByBc( *BCOut, idDofBCOutLoc, MpiInfo::rankProc() );
      Tools::allGatherSet(idDofBCOutLoc,idDofBCOut);
      std::set_intersection(idDofBCOut.begin(),idDofBCOut.end(),idDofBCLat.begin(),idDofBCLat.end(),std::inserter(ausout,ausout.begin()));
    } else {
      if ( FelisceParam::verbose()>2 )
        std::cout<<__FILE__<<":"<<__LINE__<<" warning: BCOut is null"<<std::endl;
    }

    std::set_union(ausin.begin(),ausin.end(),ausout.begin(),ausout.end(),std::inserter(globresult,globresult.begin()));

    for(auto iter = globresult.begin();
          iter != globresult.end(); iter++ ) {
      if ( m_dofPart[*iter] == MpiInfo::rankProc() ) {
        m_idDofRings.insert(m_idDofRings.end(),*iter);
      }
      m_idDofRingsSeq.insert(m_idDofRingsSeq.end(),*iter);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::userGetBC( BoundaryCondition* &/*BCLat*/,  BoundaryCondition* &/*BCIn*/, BoundaryCondition* &/*BCOut*/)
{
  if( FelisceParam::verbose()>2 ) {
    std::cout<<"File: "<<__FILE__<<" Line: "<<__LINE__<<std::endl;
    std::cout<<"you are calling the virtual function of the mother class..are you sure?"<<std::endl;
  }
  FEL_ERROR("You are using this important userdefined function from the mother class");
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getListDofOfSurfaceByBc(const BoundaryCondition& BC, std::set<felInt>& idDofBC, int rankProc )
{
  const int iUnknown = BC.getUnknown();
  const int idVar = m_listUnknown.idVariable(iUnknown);
  std::vector<felInt> idSupportEltLocal;
  felInt idSupportEltGlobal;
  felInt iSupport;
  felInt idDof;
  std::set<felInt> idDof2;
  int cptComp = 0;

  for (unsigned iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++){
    // get the local id of the support element
    m_supportDofUnknownLocal[iUnknown].getIdElementSupport(BC.idEltAndIdSupport()[iSupportDofBC].first, idSupportEltLocal);
    iSupport = BC.idEltAndIdSupport()[iSupportDofBC].second;

    for(std::size_t ielSup=0; ielSup<idSupportEltLocal.size(); ++ielSup){
      // get the global id of the support element
      ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[iUnknown], 1, &idSupportEltLocal[ielSup], &idSupportEltGlobal);

      cptComp = 0;
      for(int it_comp = 0; it_comp != this->dimension(); it_comp++){
        // get the global id of the dof
        dof().loc2glob(idSupportEltGlobal, iSupport, idVar, it_comp, idDof);

        if( m_dofPart[idDof] == rankProc ) {
          // get the global id in the petsc ordering
          AOApplicationToPetsc(m_ao, 1, &idDof);

          bool is_new_element = idDof2.insert(idDof).second;
          if (is_new_element) {
            // insert the new dof
            idDofBC.insert(idDof);
          }
        }
        cptComp++;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setValueBoundaryCondition(BoundaryCondition* bc, PetscVector& vecOfValue) 
{
  const int iUnknown = bc->getUnknown();
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int idMsh = m_listVariable[idVar].idMesh();
  if(bc->typeValueBC() == EnsightFile) {
    felInt idEltInSupportLocal = 0;
    felInt idEltInSupportGlobal = 0;
    felInt idDof;
    double aux =0.;
    bc->ValueBCInSupportDof().clear();
    for (unsigned iSupportDofBC = 0; iSupportDofBC < bc->idEltAndIdSupport().size(); iSupportDofBC++) {
      // I think this getIdElementSupport is often the identity,
      m_supportDofUnknownLocal[iUnknown].getIdElementSupport(bc->idEltAndIdSupport()[iSupportDofBC].first, idEltInSupportLocal);
      // loc2glob elementwise
      ISLocalToGlobalMappingApply(m_mappingElem[idMsh],1,&idEltInSupportLocal,&idEltInSupportGlobal);
      // for each component
      for(auto it_comp = bc->getComp().begin(); it_comp != bc->getComp().end(); it_comp++) {
        // log2glob idDof
        dof().loc2glob(idEltInSupportGlobal, bc->idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);
        AOApplicationToPetsc(m_ao,1,&idDof);
        vecOfValue.getValues( 1, &idDof, &aux);
        bc->ValueBCInSupportDof().push_back(aux);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::preSNESAssemble() 
{
  clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
  gatherVector(m_evaluationState, m_seqEvaluationState);
  preSNESAssembleAdditional();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::buildSolver() 
{
  // SNES case
  if (this->snesInterface().doUseSNES()) {
    m_pSNESInterface->init();

    // Retrieve KSP
    m_KSPInterface->initFromSNES(m_pSNESInterface);

    // Put that here: in iterativeSolve() it would duplicate the lines printed at each call.
    m_pSNESInterface->setMonitorSetLinearProblem(this);
  } 
  // Most usual cases
  else {
    m_KSPInterface->init();
  }

  // Setting the type, the GMRES restart, the PC type, tolerances,...
  const auto& r_instance = FelisceParam::instance(this->instanceIndex());
  const std::string solver         = r_instance.solver[m_identifier_solver];
  const std::string preconditioner = r_instance.preconditioner[m_identifier_solver];
  const double relativeTolerance   = r_instance.relativeTolerance[m_identifier_solver];
  const double absoluteTolerance   = r_instance.absoluteTolerance[m_identifier_solver];
  const int    maxIteration        = r_instance.maxIteration[m_identifier_solver];
  const int    gmresRestart        = r_instance.gmresRestart[m_identifier_solver];
  const bool   initPrevSolution    = r_instance.initSolverWithPreviousSolution[m_identifier_solver];
  m_KSPInterface->setKSPandPCType(solver, preconditioner);
  m_KSPInterface->setTolerances(relativeTolerance, absoluteTolerance, maxIteration, gmresRestart);
 
  if (this->snesInterface().doUseSNES()) {
    const double solutionTolerance = r_instance.solutionTolerance[m_identifier_solver];
    const int maxIterationSNES = r_instance.maxIterationSNES[m_identifier_solver];
    const int maxFunctionEvaluatedSNES = r_instance.maxFunctionEvaluatedSNES[m_identifier_solver];
    const double divergenceTolerance = r_instance.divergenceTolerance[m_identifier_solver];
    m_pSNESInterface->setTolerances(absoluteTolerance, relativeTolerance, solutionTolerance, maxIterationSNES, maxFunctionEvaluatedSNES, divergenceTolerance);
  }

  // Setting KSPpreonly, init with previous solution, setFromOptions,...
  m_KSPInterface->setKSPOptions(solver, initPrevSolution);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::solve(int /*rank*/, int /*size*/, bool new_matrix) 
{
  auto& r_instance = FelisceParam::instance(this->instanceIndex());

  if ( new_matrix ) {
    m_KSPInterface->setKSPOperator(matrix(), r_instance.setPreconditionerOption[m_identifier_solver]); 
  }

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(m_identifier_problem) + "::solve", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
  m_KSPInterface->solve( vector(), m_sol, r_instance.linearSolverVerbose);
  r_timer.Stop("LinearProblem" + std::to_string(m_identifier_problem) + "::solve", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  printSolution(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::solveWithSameMatrix() 
{
  // Get the timer
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  auto& r_timer = r_instance.timer;

  m_KSPInterface->setInitialGuessZero();
  r_timer.Start("LinearProblem" + std::to_string(m_identifier_problem) + "::solveWithSameMatrix", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
  m_KSPInterface->solve( vector(), m_sol, r_instance.linearSolverVerbose);
  r_timer.Stop("LinearProblem" + std::to_string(m_identifier_problem) + "::solveWithSameMatrix", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  printSolution(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::iterativeSolve(int /*rank*/, int /*size*/, ApplyNaturalBoundaryConditionsType /*do_apply_natural*/, FlagMatrixRHS /*flag_matrix_rhs*/) 
{
  FEL_ASSERT(this->snesInterface().doUseSNES() && "This method relies on SNES and therefore the flag should be set!");
  //SnesContext mySnesContext(*this, rank, size, do_apply_natural, flag_matrix_rhs);

  // Initialize the transient info
  this->model()->fstransient()->non_linear_iteration = 0;

  // Call the SNES solve method.
  m_pSNESInterface->solve(m_sol);

  printSolution(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::createAndCopyMatrixRHSWithoutBC(FlagMatrixRHS flag) 
{
  if(flag == FlagMatrixRHS::matrix_and_rhs || flag == FlagMatrixRHS::only_matrix) {
    if ( m_matrixWithoutBC.isNull() ) {
      m_matrixWithoutBC.duplicateFrom(this->matrix(),MAT_COPY_VALUES);
      m_matrixWithoutBC.copyFrom(this->matrix(),SAME_NONZERO_PATTERN);
    } else {
      m_matrixWithoutBC.copyFrom(this->matrix(),SAME_NONZERO_PATTERN);
    }
  }
  if(flag == FlagMatrixRHS::matrix_and_rhs || flag == FlagMatrixRHS::only_rhs) {
    if ( m_rhsWithoutBC.isNull() ) {
      m_rhsWithoutBC.duplicateFrom(this->vector());
      m_residual.duplicateFrom(this->vector());
      m_seqResidual.duplicateFrom(this->sequentialSolution());
      m_rhsWithoutBC.copyFrom(this->vector());
    } else {
      m_rhsWithoutBC.copyFrom(this->vector());
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeResidual() 
{
  m_matrixWithoutBC.scale(-1.);
  multAdd(m_matrixWithoutBC,this->solution(),m_rhsWithoutBC,m_residual);
  gatherVec(m_residual, m_seqResidual);
  m_matrixWithoutBC.scale(-1.);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::clearMatrixRHS(const FlagMatrixRHS flagMatrixRHS) 
{
  // Sebastien: really the gathering is nonsensical here: solution is absolutely not use hereafter, and
  // clearing Matrix or RHS has nothing to do with it...
  // However I can't remove it, because it could break some code: even if the place is very clumsy, some
  // problems might not do the gathering at a more fitting place and the result could therefore go astray.
  // gatherSolution();

  if (this->snesInterface().doUseSNES()) {
    gatherEvaluationState();
  }

  // Restart problem with vectors and matrix with 0 values
  if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    clearMatrix(0);
  }
  if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    clearVector(0);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::clearMatrix(std::size_t i) 
{
  FEL_ASSERT(i < numberOfMatrices());
  m_matrices[i].setUnfactored();
  m_matrices[i].zeroEntries();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::clearVector(std::size_t i) 
{
  FEL_ASSERT(i < m_vectors.size());
  m_vectors[i].zeroEntries();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getSolution(double* & solution, int sizeProc, int numProc)
{
  IGNORE_UNUSED_NUM_PROC;

  PetscVector xsol;
  xsol = m_seqSol; // NOTE: Behaviour of copy constructor has been changed in PETSc interface. In order to preserve the memory, we use the assign operator.
  felInt ix;
  felInt iout;
  double valueVec;
  for (felInt i=0; i<m_numDofLocal; i++) {
    ix=i;
    iout = i;
    if ( sizeProc > 1)
      ISLocalToGlobalMappingApply(m_mappingNodes,1,&ix,&iout);

    AOApplicationToPetsc(m_ao,1,&iout);
    m_seqSol.getValues(1,&iout,&valueVec);
    AOPetscToApplication(m_ao,1,&iout);
    xsol.setValues(1,&iout,&valueVec,INSERT_VALUES);
  }
  xsol.assembly();

  // Writing solution for visualization with ensight
  PetscVector xsol2;
  xsol.scatterToZero(xsol2,INSERT_VALUES,SCATTER_FORWARD);
  xsol2.getArray(&solution);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::setSolution(double* solution, int sizeProc, int numProc)
{
  IGNORE_UNUSED_NUM_PROC;
  IGNORE_UNUSED_SIZE_PROC;

  felInt iout;
  felReal valueVec;
  for (felInt i=0; i<m_numDof; i++) {
    iout = i;
    AOApplicationToPetsc(m_ao,1,&iout);
    valueVec = solution[iout];
    m_seqSol.setValues(1,&iout,&valueVec,INSERT_VALUES);
  }
  m_seqSol.assembly();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getSolutionUnknown(PhysicalVariable unknown, PetscVector& v) 
{
  v.zeroEntries();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  felInt shiftDof = 0;
  felInt iPos;
  felReal value;

  for (int iUnknown2 = 0; iUnknown2 < iUnknown; iUnknown2++)
    shiftDof += numDofPerUnknown(m_listUnknown[iUnknown2]);

  for (felInt iValue = 0; iValue < numDofPerUnknown(unknown); iValue++) {
    iPos = iValue + shiftDof;
    AOApplicationToPetsc(m_ao,1,&iPos);
    m_seqSol.getValues(1,&iPos,&value);
    v.setValue(iValue,value,INSERT_VALUES);
  }
  v.assembly();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::getVector(double* & value, PetscVector&  seqVec, int sizeProc, int numProc) 
{
  IGNORE_UNUSED_NUM_PROC;
  PetscVector xsol;
  xsol = seqVec; // NOTE: Behaviour of copy constructor has been changed in PETSc interface. In order to preserve the memory, we use the assign operator.
  felInt ix;
  felInt iout;
  double valueVec;
  for (felInt i=0; i<m_numDofLocal; i++) {
    ix=i;
    iout = i;
    if ( sizeProc > 1)
      ISLocalToGlobalMappingApply(m_mappingNodes,1,&ix,&iout);

    AOApplicationToPetsc(m_ao,1,&iout);
    seqVec.getValues(1,&iout,&valueVec);
    AOPetscToApplication(m_ao,1,&iout);
    xsol.setValues(1,&iout,&valueVec,INSERT_VALUES);
  }
  xsol.assembly();

  // Writing solution for visualization with ensight
  PetscVector xsol2;
  xsol.scatterToZero(xsol2,INSERT_VALUES,SCATTER_FORWARD);
  xsol2.getArray(&value); // Memory Leak one should call VecRestoreArray and then destroy xsol2, same problem in getSolution. Matteo and Benoit
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::findCoordinateWithIdDof(felInt i, Point& pt) 
{
  felInt inf = 0;
  felInt sup = 0;

  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
    
    if (iUnknown == 0) {
      inf = 0;
      sup = m_numDofUnknown[iUnknown];
    } else {
      inf += m_numDofUnknown[iUnknown-1];
      if (iUnknown == m_listUnknown.size()-1)
        sup = m_numDof;
      else
        sup += m_numDofUnknown[iUnknown];
    }

    if ( inf <= i && i < sup ) {
      const felInt idDofPerUnknown = i - inf;
      const int    idVar        = m_listUnknown.idVariable(iUnknown);
      const felInt idSupportDof = idDofPerUnknown / m_listVariable[idVar].numComponent();
      pt = m_supportDofUnknown[iUnknown].listNode()[idSupportDof];

      return;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::initPetscVectors()
{
  /*!
   The structure of the vectors initializated in #m_vecs and in #m_seqVecs is duplicated
   from the solution and from sequentialSolution of LinearProblem, respectively.\n
   They are also set to zero.
   */
  for ( auto it = m_vecs.begin(); it != m_vecs.end(); ++it ) {
    it->second.duplicateFrom(this->solution());
    it->second.set(0.0);
  }
  for ( auto it = m_seqVecs.begin(); it != m_seqVecs.end(); ++it ) {
    it->second.duplicateFrom(this->sequentialSolution());
    it->second.set(0.0);
  }
}

/***********************************************************************************/
/***********************************************************************************/

std::size_t LinearProblem::instanceIndex() const { return mOwnerModel->instanceIndex(); }

/***********************************************************************************/
/***********************************************************************************/

std::size_t& LinearProblem::instanceIndex() { return mOwnerModel->instanceIndex(); }

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::removeAverageFromSolution(PhysicalVariable unknown, int rankProc) 
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  //Ids of support elements for a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  double integralValue = 0.;
  double totalMeasure = 0.;
  double elementValue = 0.;
  //Id link element to its supportDof.
  felInt ielSupportDof;

  // initialize global numbering from mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];

  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  CurrentFiniteElement* fePtr = nullptr;
  int numComp = 0;
  felInt* idGlobalDof = nullptr;
  double* dofValue = nullptr;
  // todo generalize for every multiple type of element in mesh.
  // Do not consider proc where no "current elements" are saved - for surface/volume or surface/segment domains
  if(!bagElementTypeDomain.empty()) {
    fePtr = m_listCurrentFiniteElement[idVar];
    FEL_ASSERT(fePtr);

    numComp = variable.numComponent();
    // dofValue contains : values in dof by element.
    dofValue = new double[fePtr->numDof()*numComp];
    // idGlobalDof contains: global number od dof by element.
    idGlobalDof = new felInt[fePtr->numDof()*numComp];
  }

  // Loop on elementType.
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt,0);
    // Loop on region define in the mesh.
    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;

      // Loop on elements.
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {// numEltPerLabel: number of mesh's elements
        // get points of element.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport); // TODO D.C. inside here m_currentMesh is used. It may differ by iMesh

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          // compute measure of the current element.
          fePtr->updateMeas(0, elemPoint);
          // save measure
          totalMeasure += fePtr->measure();

          cpt = 0;
          // identify global id of Dof for the current element.
          for(int iComp = 0; iComp<numComp; iComp++) {
            for(int iDof=0; iDof<fePtr->numDof(); iDof++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;

              cpt++;
            }
          }

          //mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fePtr->numDof()*numComp,idGlobalDof);
          //gets values of associate dofs.
          m_seqSol.getValues(fePtr->numDof()*numComp,idGlobalDof,dofValue);

          //Interpolation
          for(int ig=0; ig<fePtr->numQuadraturePoint(); ig++) {
            elementValue = 0.;
            cpt = 0;
            for(int icomp=0; icomp<numComp; icomp++) {
              for(int j=0; j<fePtr->numDof(); j++) {
                elementValue +=  fePtr->phi[ig](j) * dofValue[cpt];
                cpt++;
              }
              integralValue += elementValue * fePtr->weightMeas(ig);
            }
          }
        }
        //End of interpolation

        numElement[eltType]++;
      }
    }
  }

  felInt positionForPetsc[m_numDofLocalUnknown[iUnknown]];
  felInt localPosition[m_numDofLocalUnknown[iUnknown]];
  double petscValue[m_numDofLocalUnknown[iUnknown]];

  //Receive moyenne:
  double valueReceive = 0.;
  MPI_Allreduce(&integralValue,&valueReceive, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  integralValue = valueReceive;
  valueReceive = 0.;
  MPI_Allreduce(&totalMeasure,&valueReceive, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  totalMeasure = valueReceive;

  petscValue[0] = -integralValue/totalMeasure;

  for (felInt i = 0; i < m_numDofLocalUnknown[iUnknown]; i++) {
    localPosition[i] = i;
    petscValue[i] = petscValue[0];
  }

  ISLocalToGlobalMappingApply( m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown],m_numDofLocalUnknown[iUnknown],&localPosition[0],&positionForPetsc[0]);

  //vecsetvalue global sur unknown

  // The coordinates associated with the global number should be retrieved
  m_sol.setValues(m_numDofLocalUnknown[iUnknown],positionForPetsc,petscValue,ADD_VALUES);
  m_sol.assembly();

  if(!bagElementTypeDomain.empty()) {
    delete [] idGlobalDof;
    delete [] dofValue;
  }
  gatherSolution();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::removeAverageFromSolutionCurv(PhysicalVariable unknown, int rankProc) 
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  Variable variable = m_listVariable[idVar];
  // todo generalize for every multiple type of element in mesh.
  CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[idVar];
  FEL_ASSERT(fePtr)
  CurvilinearFiniteElement& fe = *fePtr;

  int numComp = variable.numComponent();
  // dofValue contains : values in dof by element.
  double dofValue[fe.numDof()*numComp];
  // idGlobalDof contains: global number od dof by element.
  felInt idGlobalDof[fe.numDof()*numComp];

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements of a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  double integralValue = 0.;
  double totalMeasure = 0.;
  double elementValue = 0.;
  //Id link element to its supportDof.
  felInt ielSupportDof;

  // initialize global numbering from mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // Loop on elementType.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt,0);
    // Loop on region define in the mesh.
    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;
      // Loop on elements.
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // get points of element.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport); // TODO D.C. inside here iMesh is used. It may differ by iMesh

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          // compute measure of the current element.
          fe.updateMeas(0, elemPoint);
          // save measure
          totalMeasure += fe.measure();

          cpt = 0;
          // identify global id of Dof for the current element.
          for(int iComp = 0; iComp<numComp; iComp++) {
            for(int iDof=0; iDof<fe.numDof(); iDof++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;
              cpt++;
            }
          }

          //mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
          //gets values of associate dofs.
          m_seqSol.getValues(fe.numDof()*numComp,idGlobalDof,dofValue);

          //Interpolation
          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            elementValue = 0.;
            cpt = 0;
            for(int icomp=0; icomp<numComp; icomp++) {
              for(int j=0; j<fe.numDof(); j++) {
                elementValue +=  fe.phi[ig](j) * dofValue[cpt];
                cpt++;
              }
              integralValue += elementValue * fe.weightMeas(ig);
            }
          }
        }
        numElement[eltType]++;
      }
    }
  }

  felInt positionForPetsc[m_numDofLocalUnknown[iUnknown]];
  felInt localPosition[m_numDofLocalUnknown[iUnknown]];
  double petscValue[m_numDofLocalUnknown[iUnknown]];

  //Receive moyenne:
  double valueReceive = 0.;
  MPI_Allreduce(&integralValue,&valueReceive, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  integralValue = valueReceive;
  valueReceive = 0.;
  MPI_Allreduce(&totalMeasure,&valueReceive, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  totalMeasure = valueReceive;
  petscValue[0] = -integralValue/totalMeasure;

  for (felInt i = 0; i < m_numDofLocalUnknown[iUnknown]; i++) {
    localPosition[i] = i;
    petscValue[i] = petscValue[0];
  }

  ISLocalToGlobalMappingApply( m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown],m_numDofLocalUnknown[iUnknown],&localPosition[0],&positionForPetsc[0]);

  //vecsetvalue global sur unknown
  m_sol.setValues(m_numDofLocalUnknown[iUnknown],positionForPetsc,petscValue,ADD_VALUES);
  m_sol.assembly();

  gatherSolution();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::removeMaxSol(PhysicalVariable unknown, double valMax, int rankProc)
{
  IGNORE_UNUSED_RANK_PROC;

  gatherSolution();

  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int sizeVar = m_numDofLocalUnknown[iUnknown];
  double valueForPetsc[sizeVar];

  felInt idLocalValue[sizeVar];
  felInt idGlobalValue[sizeVar];

  for (felInt i = 0; i < sizeVar; i++) {
    idLocalValue[i] = i;
  }
  ISLocalToGlobalMappingApply(mappingDofLocalToDofGlobal(unknown),sizeVar,&idLocalValue[0],&idGlobalValue[0]);

  for (felInt i = 0; i < sizeVar; i++) {
    valueForPetsc[i] = -valMax;
  }
  m_sol.setValues(sizeVar,&idGlobalValue[0],&valueForPetsc[0],ADD_VALUES);
  m_sol.assembly();

  gatherSolution();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeMeanQuantity(PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& meanQuantity) 
{
  gatherSolution();
  computeMeanQuantity(m_seqSol,unknown,label,meanQuantity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeMeanQuantity(PetscVector& seqVec, PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& meanQuantity) 
{
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  //Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  meanQuantity.clear();
  meanQuantity.resize(label.size(),0.);

  std::vector<double> tmpQuantity(label.size(),0.);
  std::vector<double> measure(label.size(),0.);
  std::vector<double> tmpMeasure(label.size(),0.);

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[static_cast<ElementType>(ityp)] = 0;
    }

    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMesh]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];

      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();

      CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[idVar];
      FEL_ASSERT(fePtr);
      CurvilinearFiniteElement& fe = *fePtr;
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          // dofValue contains : values in dof by element.
          double dofValue[fe.numDof()*numComp];
          // idGlobalDof contains: global number od dof by element.
          felInt idGlobalDof[fe.numDof()*numComp];
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe.updateMeasNormal(0, elemPoint);
              if(numComp == 1) { // only for scalar case
                tmpMeasure[iLabel] += fe.measure() ;
              }
              cpt = 0;
              for(int iDof=0; iDof<fe.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
                  idGlobalDof[cpt] = idDof;
                  cpt++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
              //gets values of associate dofs.
              seqVec.getValues(fe.numDof()*numComp,idGlobalDof,dofValue);

              for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
                cpt = 0;
                for(int idof=0; idof<fe.numDof(); idof++) {
                  if( numComp == 1) { // pressure
                    tmpQuantity[iLabel] +=  fe.phi[ig](idof) * dofValue[cpt] * fe.weightMeas(ig);
                    cpt++;
                  } else { // velocity Flux
                    for (int icomp = 0; icomp < fe.numCoor(); icomp++) {
                      tmpQuantity[iLabel] +=  fe.phi[ig](idof) * dofValue[cpt] * fe.normal[ig](icomp) * fe.weightMeas(ig);
                      cpt++;
                    }
                  }
                }
              }
            }
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t iq = 0; iq < tmpQuantity.size(); iq++) {
    //Synchronise value of processors (every processors get a part of the meanQuantity and measure value).
    if(numComp == 1) { // only for scalar case
      MPI_Allreduce(&tmpMeasure[iq], &measure[iq], 1,MPI_DOUBLE, MPI_SUM,MpiInfo::petscComm());
    }
    MPI_Allreduce(&tmpQuantity[iq], &meanQuantity[iq], 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
    if(numComp == 1) { // only for scalar case
      meanQuantity[iq] /= measure[iq];
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeMeanExternalQuantity(PetscVector& seqVec, AO externalAO, PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& meanQuantity) 
{
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  //Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  meanQuantity.clear();
  meanQuantity.resize(label.size(),0.);

  std::vector<double> tmpQuantity(label.size(),0.);
  std::vector<double> measure(label.size(),0.);
  std::vector<double> tmpMeasure(label.size(),0.);

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[static_cast<ElementType>(ityp)] = 0;
    }

    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMesh]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];

      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();

      CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[idVar];
      FEL_ASSERT(fePtr);
      CurvilinearFiniteElement& fe = *fePtr;
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          // dofValue contains : values in dof by element.
          double dofValue[fe.numDof()*numComp];
          // idGlobalDof contains: global number od dof by element.
          felInt idGlobalDof[fe.numDof()*numComp];
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe.updateMeasNormal(0, elemPoint);
              if(numComp == 1) { // only for scalar case
                tmpMeasure[iLabel] += fe.measure() ;
              }
              cpt = 0;
              for(int iDof=0; iDof<fe.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
                  idGlobalDof[cpt] = idDof;
                  cpt++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(externalAO,fe.numDof()*numComp,idGlobalDof);
              //gets values of associate dofs.
              seqVec.getValues(fe.numDof()*numComp,idGlobalDof,dofValue);

              for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
                cpt = 0;
                for(int idof=0; idof<fe.numDof(); idof++) {
                  if( numComp == 1) { // pressure
                    tmpQuantity[iLabel] +=  fe.phi[ig](idof) * dofValue[cpt] * fe.weightMeas(ig);
                    cpt++;
                  } else { // velocity Flux
                    for (int icomp = 0; icomp < fe.numCoor(); icomp++) {
                      tmpQuantity[iLabel] +=  fe.phi[ig](idof) * dofValue[cpt] * fe.normal[ig](icomp) * fe.weightMeas(ig);
                      cpt++;
                    }
                  }
                }
              }
            }
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t iq = 0; iq < tmpQuantity.size(); iq++) {
    //Synchronise value of processors (every processors get a part of the meanQuantity and measure value).
    if(numComp == 1) { // only for scalar case
      MPI_Allreduce(&tmpMeasure[iq], &measure[iq], 1,MPI_DOUBLE, MPI_SUM,MpiInfo::petscComm());
    }
    MPI_Allreduce(&tmpQuantity[iq], &meanQuantity[iq], 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
    if(numComp == 1) { // only for scalar case
      meanQuantity[iq] /= measure[iq];
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeMeanQuantityDomain(PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& meanQuantity, std::vector<double>& measure) 
{
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  //Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  meanQuantity.clear();
  meanQuantity.resize(numComp*label.size(),0.0);
  measure.clear();
  measure.resize(label.size(), 0.0);
  std::vector<double> tmpQuantity(numComp*label.size(),0.0);
  std::vector<double> tmpMeasure(label.size(),0.0);

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];
  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[static_cast<ElementType>(ityp)] = 0;
    }
    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      eltType =  bagElementTypeDomain[i];

      defineFiniteElement(eltType);

      //Element matrix and vector initialisation
      initElementArray();

      CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
      FEL_ASSERT(fePtr);
      CurrentFiniteElement& fe = *fePtr;

      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];

      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          // dofValue contains : values in dof by element.
          double dofValue[fe.numDof()*numComp];
          // idGlobalDof contains: global number od dof by element.
          felInt idGlobalDof[fe.numDof()*numComp];
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe.updateMeas(0, elemPoint);
              tmpMeasure[iLabel] += fe.measure() ;
              cpt = 0;
              for(int iDof=0; iDof<fe.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,iUnknown,iComp,idDof);
                  idGlobalDof[cpt] = idDof;
                  cpt++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
              //gets values of associate dofs.
              m_seqSol.getValues(fe.numDof()*numComp,idGlobalDof,dofValue);

              for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
                cpt = 0;
                for(int idof=0; idof<fe.numDof(); idof++) {
                  for (int iComp=0; iComp<numComp; iComp++) {
                    tmpQuantity[iComp+iLabel*numComp] +=  fe.phi[ig](idof) * dofValue[cpt] * fe.weightMeas(ig);
                    cpt++;
                  }
                }
              }
            }
          numElement[eltType]++;
          }
        } else {
          numElement[eltType]+= numEltPerLabel;
        }
      }
    }
  }
  for (std::size_t iq = 0; iq < label.size(); iq++) {
    //Synchronise value of processors (every processors get a part of the meanQuantity and measure value).
    MPI_Allreduce(&tmpMeasure[iq], &measure[iq], 1,MPI_DOUBLE, MPI_SUM,MpiInfo::petscComm());
    for (int iComp=0; iComp<numComp; iComp++) {
      MPI_Allreduce(&tmpQuantity[iComp+iq*numComp], &meanQuantity[iComp+iq*numComp], 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
      meanQuantity[iComp+iq*numComp] /= measure[iq];
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeMeasure(const std::vector<int>& label, std::vector<double>& measure) 
{
  // Geometric element type in the mesh.
  ElementType eltType;
  
  // Number of points per geometric element.
  int numPointPerElt = 0;
  int currentLabel = 0;
  
  // Number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  
  //Points of the current element.
  std::vector<Point*> elemPoint;
  
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  
  //Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  std::vector<double> tmpMeasure(label.size(),0.);

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[static_cast<ElementType>(ityp)] = 0;
    }

    // TODO D.C. what if idVar 0 is not defined on m_currentMesh??
    // what is the purpose of this function? to compute the volume of elements? it is a geometric operation... why do we use FE?
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];

      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();

      CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[0]; // idVar = 0 (default FE)
      FEL_ASSERT(fePtr);
      CurvilinearFiniteElement& fe = *fePtr;
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              fe.updateMeas(0, elemPoint);
              tmpMeasure[iLabel] += fe.measure() ;
            }
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t iq = 0; iq < label.size(); iq++) {
    //Synchronise value of processors (every processors get a part of the measure value).
    MPI_Allreduce(&tmpMeasure[iq], &measure[iq], 1,MPI_DOUBLE, MPI_SUM,MpiInfo::petscComm());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeMeasureNormal(const std::vector<int>& label, std::vector<double>& measure, std::vector<double>& normal) 
{
  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  //Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  std::vector<double> tmpMeasure(label.size(),0.);
  std::vector<double> tmpNormal(3*label.size(),0.);
  std::vector<int> tmpCounter(label.size(),0);
  std::vector<int> counter(label.size(),0);
  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[static_cast<ElementType>(ityp)] = 0;
    }

    // TODO D.C. what if idVar 0 is not defined on m_currentMesh??
    // what is the purpose of this function? to compute the volume of elements? it is a geometric operation... why do we use FE?
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];

      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();

      CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[0]; // idVar = 0 (default FE)
      FEL_ASSERT(fePtr);
      CurvilinearFiniteElement& fe = *fePtr;
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
            fe.updateMeasNormal(0, elemPoint);
            tmpMeasure[iLabel] += fe.measure() ;
            for (int icomp = 0; icomp < fe.numCoor(); icomp++) tmpNormal[3*iLabel+icomp] +=  fe.normal[0](icomp); // 1st gauss point
            tmpCounter[iLabel]++;
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t iq = 0; iq < label.size(); iq++) {
    //Synchronise value of processors (every processors get a part of the measure value).
    MPI_Allreduce(&tmpMeasure[iq], &measure[iq], 1,MPI_DOUBLE, MPI_SUM,MpiInfo::petscComm());
    MPI_Allreduce(&tmpCounter[iq], &counter[iq], 1,MPI_INT, MPI_SUM,MpiInfo::petscComm());
    for (int icomp = 0; icomp < 3; icomp++) {
      MPI_Allreduce(&tmpNormal[3*iq+icomp], &normal[3*iq+icomp], 1,MPI_DOUBLE, MPI_SUM,MpiInfo::petscComm());
      normal[3*iq+icomp] /= counter[iq];
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::errorL2(PhysicalVariable unknown, std::vector<double>& fctExactSolOnDof, double& resultL2Norm) 
{
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  // todo generalize for every multiple type of element in mesh.
  CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
  FEL_ASSERT(fePtr);
  CurrentFiniteElement& fe = *fePtr;

  const auto feNumDof = fe.numDof();
  const int numComp = variable.numComponent();
  // dofValue contains : values in dof by element.
  double dofValue[feNumDof*numComp];
  // idGlobalDof contains: global number id dof by element.
  felInt idGlobalDof[feNumDof*numComp];
  felInt idGlobalDof1[feNumDof*numComp];
  felInt fctExactSolOnDofSize = fctExactSolOnDof.size();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  resultL2Norm = 0.;
  double tmpL2Norm = 0.;

  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];

  /// initialize id global of elements.
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // Loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          cpt = 0;
          for(int iDof=0; iDof<feNumDof; iDof++) {
            for(int iComp = 0; iComp<numComp; iComp++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;

              if (variable.physicalVariable() == velocity)
                idGlobalDof1[cpt] = idDof;
              else if (variable.physicalVariable() == pressure)
                idGlobalDof1[cpt] = idDof - numDofPerUnknown(velocity); //because the variable order is usually 'velocity pressure'
              else
                FEL_ERROR("Variable unknown in this errorL2 function.");
              cpt++;
            }
          }

          // Mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,feNumDof*numComp,idGlobalDof);
          // Gets values of associate dofs.
          m_seqSol.getValues(feNumDof*numComp,idGlobalDof,dofValue);

          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            double c1 = 0.;
            for(int iComp=0; iComp<numComp; iComp++) {
              double uicomp = 0.;
              for(int idof = 0; idof<feNumDof; idof++) {
                cpt = idof*numComp+iComp;
                if (idGlobalDof1[cpt] < fctExactSolOnDofSize)
                  uicomp += fe.phi[ig](idof) * (dofValue[cpt] - fctExactSolOnDof[idGlobalDof1[cpt]]);
              }
              c1 += uicomp * uicomp;
            }
            tmpL2Norm += c1 * fe.weightMeas(ig);
          }
        }
        numElement[eltType]++;
      }
    }
  }

  MPI_Allreduce(&tmpL2Norm, &resultL2Norm, 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
  resultL2Norm = std::sqrt(resultL2Norm) ;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computel2NormSquared(PhysicalVariable unknown, int rankProc, double& l2NormSquared)
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();

  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;

  //number of points per geometric element.
  int numPointPerElt = 0;

  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;

  //Points of the current element.
  std::vector<Point*> elemPoint;

  //Id of points of the element.
  std::vector<felInt> elemIdPoint;

  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;

  //Id link element to its supportDof.
  felInt ielSupportDof;

  // l2 norms
  l2NormSquared = 0.;
  double tmpl2NormSquared = 0;

  // number of element by type
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // loop over element type
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define the finite element
    defineFiniteElement(eltType);

    //Element matrix and vector initialisation
    initElementArray();

    CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
    FEL_ASSERT(fePtr);

    // dofValue contains : values in dof by element.
    double dofValue[fePtr->numDof()*numComp];

    // idGlobalDof contains: global number od dof by element.
    felInt idGlobalDof[fePtr->numDof()*numComp];

    // loop over labels
    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;

      // loop over element
      for (felInt iel = 0; iel < numEltPerLabel; iel++) {
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
        fePtr->updateMeas(0, elemPoint);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          cpt = 0;
          for(int iDof=0; iDof<fePtr->numDof(); iDof++) {
            for(int iComp = 0; iComp<numComp; iComp++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;
              cpt++;
            }
          }

          //mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fePtr->numDof()*numComp,idGlobalDof);

          //gets values of associate dofs.
          m_seqSol.getValues(fePtr->numDof()*numComp,idGlobalDof,dofValue);

          for(int ig=0; ig<fePtr->numQuadraturePoint(); ig++) {
            double c1 = 0.;
            for(int iComp=0; iComp<numComp; iComp++) {
              double uicomp = 0.;
              for(int idof = 0; idof<fePtr->numDof(); idof++) {
                uicomp += fePtr->phi[ig](idof) * dofValue[idof*numComp+iComp] ;
              }
            c1 += uicomp * uicomp;
            }
            tmpl2NormSquared += c1 * fePtr->weightMeas(ig);
          }
        }
        numElement[eltType]++;
      }
    }
  }

  MPI_Allreduce(&tmpl2NormSquared,&l2NormSquared, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeSemiNormH1Squared(PhysicalVariable unknown, int rankProc, double& semiNormH1Squared)
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;

  //number of points per geometric element.
  int numPointPerElt = 0;

  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;

  //Points of the current element.
  std::vector<Point*> elemPoint;

  //Id of points of the element.
  std::vector<felInt> elemIdPoint;

  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;

  //Id link element to its supportDof.
  felInt ielSupportDof;
  semiNormH1Squared = 0.;
  double tmpsemiNormH1Squared = 0.;

  // number of element per type
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // loop over element type
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define finite elements
    defineFiniteElement(eltType);

    //Element matrix and vector initialisation
    initElementArray();

    CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
    FEL_ASSERT(fePtr);

    // dofValue contains : values in dof by element.
    double dofValue[fePtr->numDof()*numComp];
   
    // idGlobalDof contains: global number od dof by element.
    felInt idGlobalDof[fePtr->numDof()*numComp];

    // loop over labels
    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;

      // loop over elements
      for (felInt iel = 0; iel < numEltPerLabel; iel++) {
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
        fePtr->updateFirstDeriv(0, elemPoint);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          cpt = 0;
          for(int iDof=0; iDof<fePtr->numDof(); iDof++) {
            for(int iComp = 0; iComp<numComp; iComp++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;
              cpt++;
            }
          }

          // Mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fePtr->numDof()*numComp,idGlobalDof);

          // Gets values of associate dofs.
          m_seqSol.getValues(fePtr->numDof()*numComp,idGlobalDof,dofValue);

          for (int ig=0; ig<fePtr->numQuadraturePoint(); ig++) {
            double b1 = 0.;
            for (int icomp=0; icomp<numComp; icomp++) {
              for (int jcoor=0; jcoor<fePtr->numCoor(); jcoor++) {
                double a1 = 0.;
                for (int idof=0; idof<fePtr->numDof(); idof++) {
                  a1 += fePtr->dPhi[ig](jcoor,idof) * dofValue[idof*fePtr->numCoor()+icomp];
                }
              b1 += a1 * a1;
              }
            }
            tmpsemiNormH1Squared += b1 * fePtr->weightMeas(ig) ;
          }
        }
        numElement[eltType]++;
      }
    }
  }

  MPI_Allreduce(&tmpsemiNormH1Squared, &semiNormH1Squared, 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computePowerBDu(PhysicalVariable unknown, int rankProc, const std::vector<int>& label, double density, std::vector<double>& PowerBDu)
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];

  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //current region of mesh.
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  PowerBDu.clear();
  PowerBDu.resize(label.size(),0.);

  std::vector<double> tmpPowerBDu(label.size());

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    tmpPowerBDu[iLabel] = 0.;

    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }

    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMesh]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];
      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();
      CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[idVar];
      FEL_ASSERT(fePtr);
      CurvilinearFiniteElement& fe = *fePtr;
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          // dofValue contains : values in dof by element.
          double dofValue[fe.numDof()*numComp];
          // idGlobalDof contains: global number od dof by element.
          felInt idGlobalDof[fe.numDof()*numComp];
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe.updateMeasNormal(0, elemPoint);
              cpt = 0;
              for(int iDof=0; iDof<fe.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
                  idGlobalDof[cpt] = idDof;
                  cpt++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
              //gets values of associate dofs.
              m_seqSol.getValues(fe.numDof()*numComp,idGlobalDof,dofValue);

              for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
                double c1 = 0.;
                for(int icomp=0; icomp<fe.numCoor(); icomp++) {
                  double a1 = 0.;
                  for (int idof = 0; idof < fe.numDof(); idof++) {
                    a1 += fe.phi[ig](idof) * dofValue[idof*fe.numCoor()+icomp];
                  }
                  c1 += a1 * a1;
                }
                for (int icomp=0; icomp<fe.numCoor(); icomp++) {
                  for (int idof = 0; idof < fe.numDof(); idof++) {
                    tmpPowerBDu[iLabel] += c1 * fe.phi[ig](idof) * dofValue[idof*fe.numCoor()+icomp] * fe.normal[ig](icomp) * fe.weightMeas(ig) * density * 0.5;
                  }
                }
              }
            }
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t i = 0; i < tmpPowerBDu.size(); i++) {
    //Symchronise value of processors (every processors get a part of the flux value).
    MPI_Allreduce(&tmpPowerBDu[i],&PowerBDu[i], 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeConvBoundary(PhysicalVariable unknown, PetscVector& testFunction, int rankProc, const std::vector<int>& label, std::vector<double>& convBD)
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();

  /* switch test function to sequential */
  PetscVector seqTestFunction;
  if (MpiInfo::rankProc() == 0)
    seqTestFunction.duplicateFrom(this->sequentialSolution());
  gatherVector(testFunction, seqTestFunction);

  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];

  const int numComp = variable.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //current region of mesh.
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  convBD.clear();
  convBD.resize(label.size(),0.);

  std::vector<double> tmpconvBD(label.size());

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    tmpconvBD[iLabel] = 0.;

    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }

    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMesh]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];
      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();
      CurvilinearFiniteElement* fePtr = m_listCurvilinearFiniteElement[idVar];
      FEL_ASSERT(fePtr);
      CurvilinearFiniteElement& fe = *fePtr;
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          // dofValue contains : values in dof by element.
          double dofValue[fe.numDof()*numComp];
          double dofValue2[fe.numDof()*numComp];
          // idGlobalDof contains: global number od dof by element.
          felInt idGlobalDof[fe.numDof()*numComp];
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe.updateMeasNormal(0, elemPoint);
              cpt = 0;
              for(int iDof=0; iDof<fe.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
                  idGlobalDof[cpt] = idDof;
                  cpt++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
              //gets values of associate dofs.
              m_seqSol.getValues(fe.numDof()*numComp,idGlobalDof,dofValue);
              seqTestFunction.getValues(fe.numDof()*numComp,idGlobalDof,dofValue2);

              for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
                double c1 = 0.;
                for(int icomp=0; icomp<fe.numCoor(); icomp++) {
                  /* u \cdot w */
                  for (int idof = 0; idof < fe.numDof(); idof++) {
                    c1 += fe.phi[ig](idof) * dofValue[idof*fe.numCoor()+icomp] + fe.phi[ig](idof) * dofValue2[idof*fe.numCoor()+icomp];
                  }
                }
                for (int icomp=0; icomp<fe.numCoor(); icomp++) {
                  /* u \cdot n */
                  for (int idof = 0; idof < fe.numDof(); idof++) {
                    tmpconvBD[iLabel] += c1 * fe.phi[ig](idof) * dofValue[idof*fe.numCoor()+icomp] * fe.normal[ig](icomp) * fe.weightMeas(ig);
                  }
                }
              }
            }
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t i = 0; i < tmpconvBD.size(); i++) {
    //Symchronise value of processors (every processors get a part of the flux value).
    MPI_Allreduce(&tmpconvBD[i],&convBD[i], 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
    convBD[i] = convBD[i] * FelisceParam::instance(this->instanceIndex()).density;
  }
}

/***********************************************************************************/
/***********************************************************************************/

double LinearProblem::computeViscBoundary(int label, PetscVector& W) {

  const int iUnknownVel = m_listUnknown.getUnknownIdList(velocity);
  const int idVarVel = m_listUnknown.idVariable(iUnknownVel);
  const int iMeshVel = m_listVariable[idVarVel].idMesh();
  const Variable& variableVel = m_listVariable[idVarVel];

  const int numCompVel = variableVel.numComponent();

  //idGlobalDof contains: global number of dof by element.
  felInt idGlobalDofVel;
  //dofValue contains : values in dof by element.
  double dofValueVel;
  double dofValueW;

  //geometric element type in the mesh.
  ElementType eltType;
  //current region of mesh.
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //use to identify global id of dof.
  felInt idDof;

  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  if(!m_meshLocal[iMeshVel]->statusFaces()) {
    m_meshLocal[iMeshVel]->buildFaces();
  }

  UBlasMatrix valVel; // val(icomp,jdof)
  UBlasMatrix valW; // val(icomp,jdof)

  /// initialize id global of elements.
  for (int ityp=0; ityp <GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    numElement[static_cast<ElementType>(ityp)] = 0;
  }

  /// initialize CurrentFiniteElementWithBd on Domain
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMeshVel]->bagElementTypeDomain();
  for (std::size_t ielt = 0; ielt < bagElementTypeDomain.size(); ++ielt) {
    eltType =  bagElementTypeDomain[ielt];
    defineCurrentFiniteElementWithBd(eltType); // TODO no sense here if bagElementTypeDomain.size() > 1
  }

  CurrentFiniteElementWithBd* fewbdVelPtr = m_listCurrentFiniteElementWithBd[idVarVel];
  FEL_ASSERT(fewbdVelPtr);
  CurrentFiniteElementWithBd& fewbdVel = *fewbdVelPtr;

  UBlasVector tmpVecDof(fewbdVel.numDof());

  UBlasVector tmpVecComp(numCompVel);

  UBlasMatrix tmpMatDof(numCompVel,numCompVel);

  // contains the support elements ids of a mesh element
  std::vector<felInt> vectorIdSupport;

  double viscBoundaryLocal = 0;
  double viscBoundary;

  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMeshVel]->bagElementTypeDomainBoundary();
  for (std::size_t iet = 0; iet < bagElementTypeDomainBoundary.size(); ++iet) {
    eltType =  bagElementTypeDomainBoundary[iet];
    int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    std::vector<Point*> elemPoint(numPointPerElt); //Points of the current boundary element.
    std::vector<felInt> elemIdPoint(numPointPerElt, 0); //Id of points of the boundary element.

    for(auto itRef = m_meshLocal[iMeshVel]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMeshVel]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      if ( currentLabel == label ) {
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {

          /* Compute derivative by looking at associated domain tetra */
          std::vector<Point*> elemPointVol;
          std::vector<felInt> elemIdPointVol;

          int idLocFace = -1; // Local identity of the face of a domain element
          felInt idElemVol = -1; // ID of the domain element associated to the face with identity idLocFace

          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

          // retrieve volume info from the face (given by its point IDs)
          m_meshLocal[iMeshVel]->getElementFromBdElem(elemIdPoint, elemIdPointVol, elemPointVol, idLocFace, idElemVol);

          CurvilinearFiniteElement& feVel  = fewbdVel.bdEle(idLocFace);

          fewbdVel.updateFirstDeriv(0, elemPointVol);

          fewbdVel.computeCurrentQuadraturePointInternAndBd();
          fewbdVel.updateBdMeasNormal();
          feVel.updateMeasNormal(0, elemPoint);

          valVel.resize(numCompVel,fewbdVel.numDof());
          valVel.clear();

          valW.resize(numCompVel,fewbdVel.numDof());
          valW.clear();

          // not initialised properly below.
          // loop over all the support elements
          for(int iComp = 0; iComp < numCompVel; iComp++) {
            for(int iDof = 0; iDof < fewbdVel.numDof(); iDof++) {
              m_dof.loc2glob(idElemVol,iDof,idVarVel,iComp,idDof);
              idGlobalDofVel = idDof;
              m_sol.getValues(1,&idGlobalDofVel,&dofValueVel);
              W.getValues(1,&idGlobalDofVel,&dofValueW);
              valVel(iComp,iDof) = dofValueVel;
              valW(iComp, iDof) = dofValueW;
            }
          }

          ///Compute [Grad_u*n*v] The gradient is calculated over the tetraedra, where as the rest of the terms are calculated over the triangle
          tmpVecDof.clear();
          double tmpGrad;
          for(int icomp=0; icomp<numCompVel; icomp++) {
            FEL_ASSERT( feVel.hasNormal() and fewbdVel.hasFirstDeriv() );
            for(int ilocg=0; ilocg<fewbdVel.numQuadPointBdEle(idLocFace); ilocg++) {
              int ig = ilocg+fewbdVel.indexQuadPoint(idLocFace+1);
              // face idLocFace starts at idLocFace+1 (0 for internal quad points)
              /// Vector = [n \cdot \grad \phi]:
              tmpVecDof = prod(trans(feVel.normal[ilocg]), fewbdVel.dPhi[ig]);
              tmpGrad = 0.;


              for(int jdof=0; jdof<fewbdVel.numDof(); jdof++) {
                /// tmp = [n \cdot \grad f] = \sum f_jdof (grad phi_jdof \cdot n)
                tmpGrad+=tmpVecDof(jdof)*valVel(icomp,jdof);
              }
              tmpGrad *= feVel.weightMeas(ilocg);
              for(int idof=0; idof<feVel.numDof(); idof++) {
                /// df/dn *phi(idof):
                viscBoundaryLocal = tmpGrad * feVel.phi[ilocg](idof) * valW(icomp,idof);
              }
            }
          }

          numElement[eltType]++;
        }
      } else {
        numElement[eltType]+= numEltPerLabel;
      }
    }
  }
  /* make the sum visible for all procesors */
  MPI_Allreduce(&viscBoundaryLocal, &viscBoundary, 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
  viscBoundary = viscBoundary * FelisceParam::instance(this->instanceIndex()).viscosity;
  return viscBoundary;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computePowerBDpu(PhysicalVariable unknown1, PhysicalVariable unknown2, int rankProc, const std::vector<int>& label, std::vector<double>& PowerBDpu)
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown1 = m_listUnknown.getUnknownIdList(unknown1);
  const int idVar1 = m_listUnknown.idVariable(iUnknown1);
  const int iMesh1 = m_listVariable[idVar1].idMesh();
  const int iUnknown2 = m_listUnknown.getUnknownIdList(unknown2);
  const int idVar2 = m_listUnknown.idVariable(iUnknown2);
  const int iMesh2 = m_listVariable[idVar2].idMesh();
  const Variable& variable1 = m_listVariable[idVar1];
  const Variable& variable2 = m_listVariable[idVar2];

  if ( iMesh1 != iMesh2 )
    FEL_ERROR("Variables must be defined on same mesh");

  const int numComp1 = variable1.numComponent();
  const int numComp2 = variable2.numComponent();

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //current region of mesh.
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  PowerBDpu.clear();
  PowerBDpu.resize(label.size(),0.);

  std::vector<double> tmpPowerBDpu(label.size());

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    tmpPowerBDpu[iLabel] = 0.;

    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }

    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMesh1]->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];
      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();
      CurvilinearFiniteElement& fe1 = *m_listCurvilinearFiniteElement[idVar1];
      CurvilinearFiniteElement& fe2 = *m_listCurvilinearFiniteElement[idVar2];
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];        
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh1]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh1]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          // dofValue contains : values in dof by element.
          double dofValue1[fe1.numDof()*numComp1];
          double dofValue2[fe2.numDof()*numComp2];

          // idGlobalDof contains: global number of DoF by element.
          felInt idGlobalDof1[fe1.numDof()*numComp1];
          felInt idGlobalDof2[fe2.numDof()*numComp2];

          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe1.updateMeasNormal(0, elemPoint);
              fe2.updateMeasNormal(0, elemPoint);
              int cpt1 = 0;
              for(int iDof=0; iDof<fe1.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp1; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,idVar1,iComp,idDof);
                  idGlobalDof1[cpt1] = idDof;
                  cpt1++;
                }
              }
              int cpt2 = 0;
              for(int iDof=0; iDof<fe2.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp2; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,idVar2,iComp,idDof);
                  idGlobalDof2[cpt2] = idDof;
                  cpt2++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(m_ao,fe1.numDof()*numComp1,idGlobalDof1);
              AOApplicationToPetsc(m_ao,fe2.numDof()*numComp2,idGlobalDof2);
              //gets values of associate dofs.
              m_seqSol.getValues(fe1.numDof()*numComp1,idGlobalDof1,dofValue1);
              m_seqSol.getValues(fe2.numDof()*numComp2,idGlobalDof2,dofValue2);

              for(int ig=0; ig<fe2.numQuadraturePoint(); ig++) {
                double p1 = 0.;
                for(int idof=0; idof<fe1.numDof(); idof++) {
                  p1 += fe1.phi[ig](idof) * dofValue1[idof];
                }
                double u1 = 0.;
                cpt = 0;
                for (int idof = 0; idof<fe2.numDof(); idof++) {
                  for (int icomp = 0; icomp<numComp2; icomp++) {
                    u1 += fe2.phi[ig](idof) * dofValue2[cpt] * fe2.normal[ig](icomp);
                    cpt++;
                  }
                }
                tmpPowerBDpu[iLabel] += p1 * u1 * fe2.weightMeas(ig);
              }
            }
            numElement[eltType]++;
          }
        } else
          numElement[eltType]+= numEltPerLabel;
      }
    }
  }

  for (std::size_t i = 0; i < tmpPowerBDpu.size(); i++) {
    //Symchronise value of processors (every processors get a part of the flux value).
    MPI_Allreduce(&tmpPowerBDpu[i],&PowerBDpu[i], 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeProdUn_Un_1(PhysicalVariable unknown, int rankProc, Bdf& bdf, double& prodUn_Un_1)
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  // todo generalize for every multiple type of element in mesh.
  CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
  FEL_ASSERT(fePtr);
  CurrentFiniteElement& fe = *fePtr;

  const int numComp = variable.numComponent();
  // dofValue contains : values in dof by element.
  double dofValue1[fe.numDof()*numComp];
  double dofValue2[fe.numDof()*numComp];
  // idGlobalDof contains: global number od dof by element.
  felInt idGlobalDof[fe.numDof()*numComp];

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;

  //current region of mesh.

  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  prodUn_Un_1 = 0.;
  //double elementValue = 0.;

  double tmpprodUn_Un_1 = 0.;

  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];

  /// initialize id global of elements.
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;
      for (felInt iel = 0; iel < numEltPerLabel; iel++) {
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          cpt = 0;
          for(int iDof=0; iDof<fe.numDof(); iDof++) {
            for(int iComp = 0; iComp<numComp; iComp++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;
              cpt++;
            }
          }

          //mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
          //gets values of associate dofs.
          bdf.sol_n().getValues(fe.numDof()*numComp,idGlobalDof,dofValue1);
          bdf.sol_n_1().getValues(fe.numDof()*numComp,idGlobalDof,dofValue2);

          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            double c1 = 0.;
            for(int iComp = 0; iComp<numComp; iComp++) {
              double a1 = 0.;
              double b1 = 0.;
              for(int idof = 0; idof<fe.numDof(); idof++) {
                a1 += dofValue1[idof*numComp+iComp] * fe.phi[ig](idof);
                b1 += dofValue2[idof*numComp+iComp] * fe.phi[ig](idof);
              }
              c1 += a1 * b1;
            }
            tmpprodUn_Un_1 += c1 * fe.weightMeas(ig);
          }
        }
        numElement[eltType]++;
      }
    }
  }

  MPI_Allreduce(&tmpprodUn_Un_1,&prodUn_Un_1, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeDiffSquaredUn_Un_1(PhysicalVariable unknown, int rankProc, Bdf& bdf, double& diffSquareUn_Un_1) 
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  // todo generalize for every multiple type of element in mesh.
  CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
  FEL_ASSERT(fePtr);
  CurrentFiniteElement& fe = *fePtr;

  const int numComp = variable.numComponent();
  // dofValue contains : values in dof by element.
  double dofValue1[fe.numDof()*numComp];
  double dofValue2[fe.numDof()*numComp];
  // idGlobalDof contains: global number od dof by element.
  felInt idGlobalDof[fe.numDof()*numComp];

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;

  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  diffSquareUn_Un_1 = 0.;
  //double elementValue = 0.;

  double tmpdiffSquareUn_Un_1 = 0.;

  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];

  /// initialize id global of elements.
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;
      for (felInt iel = 0; iel < numEltPerLabel; iel++) {
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          cpt = 0;
          for(int iDof=0; iDof<fe.numDof(); iDof++) {
            for(int iComp = 0; iComp<numComp; iComp++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
             idGlobalDof[cpt] = idDof;
              cpt++;
            }
          }

          //mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
          //gets values of associate dofs.
          bdf.sol_n().getValues(fe.numDof()*numComp,idGlobalDof,dofValue1);
          bdf.sol_n_1().getValues(fe.numDof()*numComp,idGlobalDof,dofValue2);

          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            double c1 = 0.;
            for(int iComp = 0; iComp<numComp; iComp++) {
              double a1 = 0.;
              double b1 = 0.;
              for(int idof = 0; idof<fe.numDof(); idof++) {
                a1 += dofValue1[idof*numComp+iComp] * fe.phi[ig](idof);
                b1 += dofValue2[idof*numComp+iComp] * fe.phi[ig](idof);
              }
              c1 += (a1 - b1)*(a1 - b1);
            }
            tmpdiffSquareUn_Un_1 += c1 * fe.weightMeas(ig);
          }
        }
        numElement[eltType]++;
      }
    }
  }

  MPI_Allreduce(&tmpdiffSquareUn_Un_1,&diffSquareUn_Un_1, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeTemamTerm(PhysicalVariable unknown, int rankProc, Bdf& bdf, double& temamTerm) 
{
  IGNORE_UNUSED_RANK_PROC;
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  // todo generalize for every multiple type of element in mesh.
  CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
  FEL_ASSERT(fePtr);
  CurrentFiniteElement& fe = *fePtr;

  const int numComp = variable.numComponent();
  // dofValue contains : values in dof by element.
  double dofValue1[fe.numDof()*numComp];
  double dofValue2[fe.numDof()*numComp];
  // idGlobalDof contains: global number od dof by element.
  felInt idGlobalDof[fe.numDof()*numComp];

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;

  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  // Ids of support elements for a mesh element
  std::vector <felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  temamTerm = 0.;
  //double elementValue = 0.;

  double tmptemamTerm = 0.;

  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];

  /// initialize id global of elements.
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;
      for (felInt iel = 0; iel < numEltPerLabel; iel++) {
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          cpt = 0;
          for(int iDof=0; iDof<fe.numDof(); iDof++) {
            for(int iComp = 0; iComp<numComp; iComp++) {
              m_dof.loc2glob(ielSupportDof,iDof,idVar,iComp,idDof);
              idGlobalDof[cpt] = idDof;
              cpt++;
            }
          }

          //mapping to obtain new global numbering. (I/O array: idGlobalDof).
          AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
          //gets values of associate dofs.
          bdf.sol_n().getValues(fe.numDof()*numComp,idGlobalDof,dofValue1);
          bdf.sol_n_1().getValues(fe.numDof()*numComp,idGlobalDof,dofValue2);

          for (int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            // div(u(n-1))
            double b1 = 0.;
            for (int icomp=0; icomp<numComp; icomp++) {
              double a1 = 0.;
              for (int idof=0; idof<fe.numDof(); idof++) {
                a1 += fe.dPhi[ig](icomp,idof) * dofValue2[idof*fe.numCoor()+icomp];
              }
              b1 += a1;
            }
            // u(n) v
            double c1 = 0.;
            for (int icomp=0; icomp<numComp; icomp++) {
              double a1 = 0.;
              for (int idof=0; idof<fe.numDof(); idof++) {
                a1 += fe.phi[ig](idof) * dofValue1[idof*fe.numCoor()+icomp];
              }
              c1 += a1;
            }
            // \int_\Omega div(u(n-1)) \dot u(n) v
            tmptemamTerm += b1 * c1 * fe.weightMeas(ig) ;
          }
        }
        numElement[eltType]++;
      }
    }
  }

  MPI_Allreduce(&tmptemamTerm,&temamTerm, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeBoundaryStress(int coef, int label, std::vector<double>& stress) 
{
  const int iUnknownPres = m_listUnknown.getUnknownIdList(pressure);
  const int idVarPres = m_listUnknown.idVariable(iUnknownPres);
  const int iMeshPres = m_listVariable[idVarPres].idMesh();
  const int iUnknownVel = m_listUnknown.getUnknownIdList(velocity);
  const int idVarVel = m_listUnknown.idVariable(iUnknownVel);
  const int iMeshVel = m_listVariable[idVarVel].idMesh();
  const Variable& variablePres = m_listVariable[idVarPres];
  const Variable& variableVel = m_listVariable[idVarVel];

  const int numCompPres = variablePres.numComponent();
  const int numCompVel = variableVel.numComponent();

  if ( iMeshPres != iMeshVel )
    FEL_ERROR("Variables must be defined on same mesh");

  //idGlobalDof contains: global number of dof by element.
  felInt idGlobalDofPres;
  felInt idGlobalDofVel;
  //dofValue contains : values in dof by element.
  double dofValuePres;
  double dofValueVel;

  //geometric element type in the mesh.
  ElementType eltType;
  //current region of mesh.
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //use to identify global id of dof.
  felInt idDof;

  stress.assign(m_numDof,0.);
  const double visc = FelisceParam::instance(this->instanceIndex()).viscosity;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];

  if(m_meshLocal[iMeshVel]->statusFaces() == false) {
    m_meshLocal[iMeshVel]->buildFaces();
  }

  UBlasMatrix valPres; // val(icomp,jdof)
  UBlasMatrix valVel; // val(icomp,jdof)

  /// initialize id global of elements.
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    numElement[static_cast<ElementType>(ityp)] = 0;
  }

  /// initialize CurrentFiniteElementWithBd on Domain
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMeshVel]->bagElementTypeDomain();
  for (std::size_t ielt = 0; ielt < bagElementTypeDomain.size(); ++ielt) {
    eltType =  bagElementTypeDomain[ielt];
    defineCurrentFiniteElementWithBd(eltType); // TODO no sense here if bagElementTypeDomain.size() > 1
  }

  CurrentFiniteElementWithBd* fewbdPresPtr = m_listCurrentFiniteElementWithBd[idVarPres];
  FEL_ASSERT(fewbdPresPtr);
  CurrentFiniteElementWithBd& fewbdPres = *fewbdPresPtr;

  CurrentFiniteElementWithBd* fewbdVelPtr = m_listCurrentFiniteElementWithBd[idVarVel];
  FEL_ASSERT(fewbdVelPtr);
  CurrentFiniteElementWithBd& fewbdVel = *fewbdVelPtr;

  UBlasVector tmpVecDof(fewbdVel.numDof());
  UBlasVector tmpVecComp(numCompVel);
  UBlasMatrix tmpMatDof(numCompVel,numCompVel);

  // contains the support elements ids of a mesh element
  std::vector<felInt> vectorIdSupport;

  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[iMeshVel]->bagElementTypeDomainBoundary();
  for (std::size_t iet = 0; iet < bagElementTypeDomainBoundary.size(); ++iet) {
    eltType =  bagElementTypeDomainBoundary[iet];
    int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    std::vector<Point*>  elemPoint(numPointPerElt); //Points of the current boundary element.
    std::vector<felInt>  elemIdPoint(numPointPerElt, 0); //Id of points of the boundary element.

      for(auto itRef = m_meshLocal[iMeshVel]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMeshVel]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      if ( currentLabel == label ) {
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {

          std::vector<Point*> elemPointVol;   // Points of the current domain element.
          std::vector<felInt> elemIdPointVol; // Id of points of the domain element.
          int idLocFace = -1; // Local identity of the face of a domain element
          felInt idElemVol = -1; // ID of the domain element associated to the face with identity idLocFace

          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

          // retrieve volume info from the face (given by its point IDs)
          m_meshLocal[iMeshVel]->getElementFromBdElem(elemIdPoint, elemIdPointVol, elemPointVol, idLocFace, idElemVol);

          CurvilinearFiniteElement& fePres = fewbdPres.bdEle(idLocFace);
          CurvilinearFiniteElement& feVel  = fewbdVel.bdEle(idLocFace);
          felInt idDofVel[feVel.numDof()*numCompVel];

          fewbdVel.updateFirstDeriv(0, elemPointVol);

          fewbdVel.computeCurrentQuadraturePointInternAndBd();
          fewbdVel.updateBdMeasNormal();
          fePres.updateMeasNormal(0, elemPoint);
          feVel.updateMeasNormal(0, elemPoint);

          FEL_CHECK(fePres.numQuadraturePoint() == feVel.numQuadraturePoint(), "LinearProblem::computeBoundaryStress: The quadrature rules must be the same for the two elements");

          valPres.resize(numCompPres,fePres.numDof());
          valVel.resize(numCompVel,fewbdVel.numDof());
          valPres.clear();
          valVel.clear();

          std::vector<double> vecPress(feVel.numDof()*numCompVel,0);
          std::vector<double> vecVel(feVel.numDof()*numCompVel,0);
          std::vector<double> tmpStress(feVel.numDof()*numCompVel,0.);

          felInt ielSupportDof = -1;    //Id link element to its supportDof; dumb value to avoid undefined behaviour if
          // not initialised properly below.
          // loop over all the support elements
          for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
            // get the id of the support
            ielSupportDof = vectorIdSupport[it];
            for(int iComp = 0; iComp<numCompPres; iComp++) {
              for(int iDof=0; iDof<fePres.numDof(); iDof++) {
                m_dof.loc2glob(ielSupportDof,iDof,idVarPres,iComp,idDof);
                idGlobalDofPres = idDof; //and the petsc ordering?
                m_sol.getValues(1,&idGlobalDofPres,&dofValuePres);
                valPres(iComp,iDof) = dofValuePres;
              }
            }
          }

          for(int iComp = 0; iComp < numCompVel; iComp++) {
            for(int iDof = 0; iDof < fewbdVel.numDof(); iDof++) {
              m_dof.loc2glob(idElemVol,iDof,idVarVel,iComp,idDof);
              idGlobalDofVel = idDof;
              m_sol.getValues(1,&idGlobalDofVel,&dofValueVel);
              valVel(iComp,iDof) = dofValueVel;
            }
          }

          int cpt3 = 0;
          for(int iComp = 0; iComp<numCompVel; iComp++) {
            for(int iDof=0; iDof<feVel.numDof(); iDof++) {
              FEL_ASSERT(ielSupportDof != -1);
              m_dof.loc2glob(ielSupportDof,iDof,idVarVel,iComp,idDof);
              idDofVel[cpt3] = idDof;
              cpt3++;
            }
          }

          ///Compute int_{Sigma} p*n*v
          double tmpPress = 0.;
          for(int icomp=0; icomp<feVel.numCoor(); icomp++) {
            for(int ig=0; ig<feVel.numQuadraturePoint(); ig++) {
              tmpPress = 0.;
              for(int jdof=0; jdof<fePres.numDof(); jdof++) {
                /// tmp =  \sum_jdof ( f_jdof * phi_jdof )
                tmpPress +=  fePres.phi[ig](jdof) * valPres(0,jdof);
              }
              for(int idof=0; idof<feVel.numDof(); idof++) {
                ///summ_idof (f_idof *phim_idof \cdot n)
                vecPress[feVel.numDof()*icomp+idof]+= coef * tmpPress *feVel.normal[ig](icomp)*feVel.phi[ig](idof)* feVel.weightMeas(ig);
              }
            }
          }

          ///Compute int_{Sigma} [Grad_u*n*v]
          tmpVecDof.clear();
          double tmpGrad;
          for(int icomp=0; icomp<numCompVel; icomp++) {
            FEL_ASSERT( feVel.hasNormal() and fewbdVel.hasFirstDeriv() );
            for(int ilocg=0; ilocg<fewbdVel.numQuadPointBdEle(idLocFace); ilocg++) {
              int ig = ilocg+fewbdVel.indexQuadPoint(idLocFace+1);
              // face idLocFace starts at idLocFace+1 (0 for internal quad points)
              /// Vector = [n \cdot \grad \phi]:
              tmpVecDof = prod(trans(feVel.normal[ilocg]), fewbdVel.dPhi[ig]);
              tmpGrad = 0.;

              /* if(FelisceParam::instance(this->instanceIndex()).useSymmetricStress){ //to be tested
                /// tmpMatDof_ji = Sum_jdof( u_j * grad_i phi_jdof )
                tmpMatDof = prod(fewbdVel.dPhi[ig], trans(valVel));
                /// tmpVecComp =
                tmpVecComp = prod(tmpMatDof, feVel.normal[ilocg]);
                /// Vector = [n \cdot \grad \phi]:
                tmpGrad = 0.;
                tmpGrad = tmpVecComp(icomp);
                }
                else{
                tmpGrad = 0.;
                }*/


              for(int jdof=0; jdof<fewbdVel.numDof(); jdof++) {
                /// tmp = [n \cdot \grad f] = \sum f_jdof (grad phi_jdof \cdot n)
                tmpGrad+=tmpVecDof(jdof)*valVel(icomp,jdof);
              }
              tmpGrad *= coef*visc* feVel.weightMeas(ilocg);
              for(int idof=0; idof<feVel.numDof(); idof++) {
                /// df/dn *phi(idof):
                vecVel[feVel.numDof()*icomp+idof]+= tmpGrad * feVel.phi[ilocg](idof);
              }
            }
          }

          for(int jstr=0; jstr<feVel.numDof()*feVel.numCoor(); jstr++) {
            tmpStress[jstr] = vecVel[jstr]-vecPress[jstr];
          }
          numElement[eltType]++;

          //assembling local stress std::vector in global stress std::vector
          for(int istr = 0; istr < feVel.numDof()*numCompVel; istr++) {
            stress[idDofVel[istr]]+=tmpStress[istr];
          }
        }
      } else
        numElement[eltType]+= numEltPerLabel;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeDomainIntegralOfDivergence(PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& integralOfDivergence)
{
  gatherSolution();
  const int iUnknown = m_listUnknown.getUnknownIdList(unknown);
  const int idVar = m_listUnknown.idVariable(iUnknown);
  const int iMesh = m_listVariable[idVar].idMesh();
  const Variable& variable = m_listVariable[idVar];
  const int numComp = variable.numComponent();
  // dofValue contains : values in dof by element.
  std::vector<double> dofValue;
  // idGlobalDof contains: global number od dof by element.
  std::vector<felInt> idGlobalDof;

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  int currentLabel = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;
  //Ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  //Id link element to its supportDof.
  felInt ielSupportDof;
  integralOfDivergence.clear();
  integralOfDivergence.resize(label.size(),0.);

  std::vector<double> tmpQuantity(label.size(),0.);

  int searchLabel=-1;
  felInt numElement[GeometricMeshRegion::m_numTypesOfElement];
  for(std::size_t iLabel = 0; iLabel < label.size(); iLabel++) {
    searchLabel = label[iLabel];
    /// initialize id global of elements.
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[static_cast<ElementType>(ityp)] = 0;
    }
    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[iMesh]->bagElementTypeDomain();
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      eltType =  bagElementTypeDomain[i];

      defineFiniteElement(eltType);

      //Element matrix and vector initialisation
      initElementArray();

      CurrentFiniteElement* fePtr = m_listCurrentFiniteElement[idVar];
      FEL_ASSERT(fePtr);
      CurrentFiniteElement& fe = *fePtr;

      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];

      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if ( searchLabel == currentLabel) {
          dofValue.resize(fe.numDof()*numComp);
          idGlobalDof.resize(fe.numDof()*numComp);
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              ielSupportDof = vectorIdSupport[it];

              fe.updateFirstDeriv(0, elemPoint);
              cpt = 0;
              for(int iDof=0; iDof<fe.numDof(); iDof++) {
                for(int iComp = 0; iComp<numComp; iComp++) {
                  m_dof.loc2glob(ielSupportDof,iDof,iUnknown,iComp,idDof);
                  idGlobalDof[cpt] = idDof;
                  cpt++;
                }
              }

              //mapping to obtain new global numbering. (I/O array: idGlobalDof).
              AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof.data());
              //gets values of associate dofs.
              m_seqSol.getValues(fe.numDof()*numComp,idGlobalDof.data(),dofValue.data());

              for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
                cpt = 0;
                for(int idof=0; idof<fe.numDof(); idof++) {
                  for (int iComp=0; iComp<numComp; iComp++) {
                    tmpQuantity[iLabel] +=  fe.dPhi[ig](iComp,idof) * dofValue[cpt] * fe.weightMeas(ig);
                    cpt++;
                  }
                }
              }
            }
            numElement[eltType]++;
          }
        } else {
            numElement[eltType]+= numEltPerLabel;
        }
      }
    }
  }
  for (std::size_t iq = 0; iq < label.size(); iq++) {
    //Synchronise value of processors (every processors get a part of the meanQuantity and measure value).
    MPI_Allreduce(&tmpQuantity[iq], &integralOfDivergence[iq], 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::m_duplicateSupportDofAssemblyLoop(int rank, ElementType& eltType, felInt ielGeo, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, FlagMatrixRHS flagMatrixRHS) // TODO D.C.
{
  IGNORE_UNUSED_RANK;
  felInt ielSupportDof1, ielSupportDof2;

  // for one element, list of all the associated support elements.
  // it assumes that the number of support elements for a given element is
  // the same for all unknown. If it is not the case, one need to implement
  // an assembling of the matrix by block ?
  std::vector<felInt> vectorIdSupport;

  // get the global id of the element
  felInt ielGeoGlobal;
  ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &ielGeo, &ielGeoGlobal);

  // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
  setElemPoint(eltType, ielGeo, elemPoint, elemIdPoint, vectorIdSupport);

  // Fourth loop over all the support elements
  for (std::size_t it1 = 0; it1 < vectorIdSupport.size(); it1++) {
    // get the id of the support
    ielSupportDof1 = vectorIdSupport[it1];

    for(std::size_t it2 = 0; it2 < vectorIdSupport.size(); it2++) {
      // get the id of the support
      ielSupportDof2 = vectorIdSupport[it2];

      // clear elementary matrices
      for (std::size_t j = 0, size = m_matrices.size(); j < size ; j++)
        m_elementMat[j]->zero();

      // clear elementary std::vector.
      for (std::size_t j = 0, size = m_vectors.size(); j < size ; j++)
        m_elementVector[j]->zero();

      computeElementArray(elemPoint, elemIdPoint, ielSupportDof1, ielSupportDof2, eltType, ielGeoGlobal, flagMatrixRHS);

      // compute specific term of users. No need for ielSupportDof2 here yet
      userElementCompute(elemPoint, elemIdPoint, ielSupportDof1, ielSupportDof2, eltType, ielGeoGlobal, flagMatrixRHS);

      // add values of elemMat in the global matrix: m_matrices[0].
      setValueMatrixRHS(ielSupportDof1, ielSupportDof2, flagMatrixRHS);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::m_duplicateSupportDofAssemblyLoopBoundaryCondition(int rank, ElementType& eltType, felInt ielGeoByType, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  felInt ielSupportDof1, ielSupportDof2;

  // for one element, list of all the associated support elements.
  // it assumes that the number of support elements for a given element is
  // the same for all unknown. If it is not the case, one need to implement
  // an assembling of the matrix by block ?
  std::vector<felInt> vectorIdSupport;

  // get the global id of the element
  felInt ielGeoGlobal, ielGeo = 0;
  m_meshLocal[m_currentMesh]->getIdElemFromTypeElemAndIdByType(eltType, ielGeoByType, ielGeo);
  ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &ielGeo, &ielGeoGlobal);

  // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
  setElemPoint(eltType, ielGeoByType, elemPoint, elemIdPoint, vectorIdSupport);

  // Fourth loop over all the support elements
  for (std::size_t it1 = 0; it1 < vectorIdSupport.size(); it1++) {
    // get the id of the support
    ielSupportDof1 = vectorIdSupport[it1];

    for(std::size_t it2 = 0; it2 < vectorIdSupport.size(); it2++) {
      // get the id of the support
      ielSupportDof2 = vectorIdSupport[it2];

      // clear elementary matrices
      for (std::size_t j = 0; j < m_matrices.size(); j++)
        m_elementMatBD[j]->zero();

      // clear elementary vector.
      for (std::size_t j = 0; j < m_vectors.size(); j++)
        m_elementVectorBD[j]->zero();

      // compute boundary terms on element
      computeAndApplyElementNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, ielSupportDof2, ielGeoGlobal, flagMatrixRHS);
      
      // add values of elemMat in the global matrix: m_matrices[0].
      setValueMatrixRHSBD(ielSupportDof1, ielSupportDof2, flagMatrixRHS);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::applyEssentialBoundaryConditionViaPenalizationOnBC(FlagMatrixRHS flagMatrixRHS, const BoundaryCondition* const BC)
{
  std::unordered_map<int, double> idBCAndValue;
  std::unordered_set<felInt> idDofBC;
  getListDofOfBC(*BC, idDofBC, idBCAndValue);

  // double TGV(std::numeric_limits<double>::max()); using this only homogeneous BD
  double TGV(1e30); // TODO 
  for(auto itDofBC = idDofBC.begin(); itDofBC != idDofBC.end(); ++itDofBC) {
    if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix)
      m_matrices[0].setValue(*itDofBC, *itDofBC, TGV, INSERT_VALUES);
    if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs)
      vector().setValue(*itDofBC, idBCAndValue[*itDofBC]*TGV, INSERT_VALUES);
  }
}

/***********************************************************************************/
/***********************************************************************************/

namespace { // anonymous
  /// Wrapper around call to AOPetscToApplication
  felInt PetscToApplicationIndex(const AO& ao, felInt petscIndex) {
    felInt ia = petscIndex;
    AOPetscToApplication(ao, 1, &ia);
    return ia;
  }
} // namespace anonymous

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::applyEssentialBoundaryConditionViaSymmetricPseudoEliminationOnBC(FlagMatrixRHS flagMatrixRHS, const BoundaryCondition* const BC, std::vector<felInt>& rows)
{
  std::unordered_set<felInt> idDofBC;
  std::unordered_map<felInt, double> idBCAndValue;
  getListDofOfBC(*BC, idDofBC, idBCAndValue);
  for(auto itDofBC = idDofBC.begin(), endDofBC = idDofBC.end();itDofBC != endDofBC; ++itDofBC) {
    // First we consider the line related to the current degree of freedom
    felInt dofPetscIndex = *itDofBC;
    felInt dofApplicationIndex = PetscToApplicationIndex(m_ao, dofPetscIndex);
    // Modify the std::vector first
    if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
      vector().setValue(dofPetscIndex, idBCAndValue[dofPetscIndex], INSERT_VALUES);
    }
    // If the matrix is not to be changed, go directly to next iteration
    if(flagMatrixRHS != FlagMatrixRHS::matrix_and_rhs && flagMatrixRHS != FlagMatrixRHS::only_matrix)
      continue;

    // Data required for Petsc operation, that will be performed after the loop ended
    rows.push_back(dofApplicationIndex);
  }
}

/***********************************************************************************/
/***********************************************************************************/

#ifdef FELISCE_WITH_CVGRAPH

void LinearProblem::setValueMatrixBD(felInt ielSupportDof) 
{
  const felInt numDofTotal = this->m_elementMatBD[0]->mat().size1();
  felInt cptGpos;
  int idVar;
  std::vector<felInt> loc2globTmp;

  // Build the loc2glob map m_GposLine for the first support element (i.e. for the rows of the matrix or RHS)
  cptGpos = 0;
  for ( std::size_t iUnknown = 0; iUnknown < this->m_listUnknown.getUnknownsRows().size(); iUnknown++) {
    idVar = this->m_listUnknown.idVariable(this->m_listUnknown.getUnknownsRows()[iUnknown]);
    this->dof().loc2glob(ielSupportDof, this->m_listCurvilinearFiniteElement[idVar]->numDof(), idVar, m_listVariable[idVar].numComponent(), loc2globTmp);
    for(std::size_t i=0; i<loc2globTmp.size(); ++i){
      m_globPosRow[cptGpos]    = loc2globTmp[i];
      m_globPosColumn[cptGpos] = loc2globTmp[i];
      ++cptGpos;
    }
  }

  for (int i = 0; i < numDofTotal; ++i)
    for (int j = 0; j < numDofTotal; ++j)
      m_matrixValues[j + i*numDofTotal] = this->m_elementMatBD[0]->mat()(i,j);

  /// Now we have to compute where this numbers have to be put into the boundary matrix
  /// volume application -> petsc volume
  AOApplicationToPetsc(this->m_ao,numDofTotal,m_globPosRow.data());
  AOApplicationToPetsc(this->m_ao,numDofTotal,m_globPosColumn.data());
  std::vector<felInt> row;
  std::vector<felInt> col;
  std::vector<felInt> tmpCol;
  std::vector<double> val;
  /// petsc volume -> boundary application
  for ( int k=0; k < numDofTotal; ++k) {
    tmpCol.clear();
    try {
      // petscVol2ApplicationBD can throw an out_of_range while m_globPosRow[k] can not
      // The idea is that if this fails it means that this particular dof is not in the mapping
      // and that it can be discarded (e.g. pressure nodes when dealing with Stokes problem)
      row.push_back(this->m_auxiliaryDofBD->petscVol2ApplicationBD(m_globPosRow[k]));
      for ( int k2=0; k2 < numDofTotal; ++k2) {
        try {
          tmpCol.push_back(this->m_auxiliaryDofBD->petscVol2ApplicationBD(m_globPosColumn[k2]));
          val.push_back(m_matrixValues[k2+k*numDofTotal]);
        } catch ( const std::out_of_range& oor ){}
      }
    } catch ( const std::out_of_range& oor ){}
    if (tmpCol.size()>0) {
      col=tmpCol;
    }
  }

  /// boundary application -> petsc application
  AOApplicationToPetsc(this->m_auxiliaryDofBD->ao(),row.size(),row.data());
  AOApplicationToPetsc(this->m_auxiliaryDofBD->ao(),col.size(),col.data());
  /// set value
  m_auxiliaryMatrix.setValues(row.size(),row.data(),col.size(),col.data(),val.data(),ADD_VALUES);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::computeResidualOnDofs(PetscVector& residual, PetscVector& stressOut, PetscVector& seqStressOut, std::size_t iConn) 
{
  PetscVector tmpIn  = this->dofBD(iConn).allocateBoundaryVector(DofBoundary::parallel);
  PetscVector tmpOut = this->dofBD(iConn).allocateBoundaryVector(DofBoundary::parallel);
  this->dofBD(iConn).restrictOnBoundary(residual,tmpIn);
  if ( this->dofBD(iConn).hasDofsOnBoundary() ) {
    m_kspMassBD[iConn].solve(tmpIn, tmpOut, 0);
  }
  stressOut.zeroEntries();
  this->dofBD(iConn).extendOnVolume(stressOut,tmpOut);
  this->gatherVector(stressOut,seqStressOut);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::sendInitialCondition(std::size_t iConn) {
  if ( this->slave()->initialConditionNeeded(iConn) ) {
    this->sendZero(iConn);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::cvgAddExternalResidualToRHS() 
{
  // Loop over the cvgraph connections
  for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
    if ( this->slave()->sumDirectlyIntoRHS(iConn) ) {
      sumOnBoundaryCVGraph(vector(), m_seqVecs.Get("cvgraph"+this->slave()->neumannVariable(iConn)), dofBD(iConn) );
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::sumOnBoundaryCVGraph(PetscVector& v, PetscVector& b, const DofBoundary& dofBD) 
{
  felInt numLocalDofInterface = dofBD.numLocalDofInterface();
  std::vector<double> tmp(numLocalDofInterface);
  b.getValues(numLocalDofInterface, dofBD.loc2PetscVolPtr(), tmp.data() );
  v.setValues(numLocalDofInterface, dofBD.loc2PetscVolPtr(), tmp.data(),ADD_VALUES);
  v.assembly();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::prepareResidual(std::size_t iConn) 
{
  if ( m_stressOut.isNull() ) {
    m_stressOut.duplicateFrom(this->solution());
    m_seqStressOut.duplicateFrom(this->sequentialSolution());
  }
  if ( this->slave()->residualInterpolationType(iConn) == 0 ) {
    if ( m_massBD[iConn].isNull() ) {
      this->assembleMassBoundaryAndInitKSP(iConn);
    }
    this->computeResidualOnDofs( m_residual,m_stressOut,m_seqStressOut, iConn);
  } else {
    m_seqStressOut.copyFrom( m_seqResidual );
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::cvgraphFinalizeEssBC() 
{
  if( FelisceParam::instance(this->instanceIndex()).withCVG ) {
    if ( slave()->thereIsAtLeastOneDirichletCondition() ) {
      if ( m_fstransient->iteration > 0 ) {
        PetscPrintf(MpiInfo::petscComm(), "Finalizing essential BC transient with CVGRAPH data\n");
        // Loop over the cvgraph connections
        for ( std::size_t iConn(0); iConn < slave()->numConnections(); ++iConn ) {
          // Extract the labels of the current connection and store them in a set
          std::vector<int> tmp = slave()->interfaceLabels(iConn);
          std::unordered_set<int> labels(tmp.begin(),tmp.end());
          // Loop over the dirichlet BCs, looking for the correct one.
          BoundaryCondition* BC;
          for(std::size_t iBC(0); iBC < m_boundaryConditionList.numDirichletBoundaryCondition(); iBC++) {
            BC = m_boundaryConditionList.Dirichlet(iBC);
            if ( BC->listLabel() == labels ) {
              this->setValueBoundaryCondition(BC,m_seqVecs.Get(m_cvgDirichletVariable));
            }
          }
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::assembleRHSOnlyCVGraph() 
{
  ElementType eltType;           //geometric element type in the mesh.
  int numPointPerElt = 0;        //number of points per geometric element.
  int currentLabel = 0;          //number of label domain.
  felInt numEltPerLabel = 0;     //number of element for one label and one eltType.
  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t ielt = 0; ielt < bagElementTypeDomain.size(); ++ielt) {
    eltType =  bagElementTypeDomain[ielt];
    defineCurrentFiniteElementWithBd(eltType); // TODO no sense here if bagElementTypeDomain.size() > 1
  }

  std::vector<Point*> elemPoint;
  std::vector<felInt> elemIdPoint, vectorIdSupport;
  felInt ielSupportDof;

  allocateVectorBoundaryConditionDerivedLinPb();

  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    eltType =  bagElementTypeDomainBoundary[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);
    defineCurvilinearFiniteElement(eltType);
    initElementArrayBD();
    allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_rhs);

    initPerElementTypeBoundaryCondition(eltType, FlagMatrixRHS::only_rhs);
    userElementInitNaturalBoundaryCondition();
    allocateElemFieldBoundaryCondition(/*idBCforLinCombMethod*/-1);

    //second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;
      if ( Tools::insideVec(FelisceParam::instance(this->instanceIndex()).interfaceLabels,currentLabel) ) {
        initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDof, eltType, numElement[eltType], FlagMatrixRHS::only_rhs);
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
          for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
            ielSupportDof = vectorIdSupport[it];
            m_elementVectorBD[0]->zero();

            cvgraphNaturalBC(iel);
            applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, FlagMatrixRHS::only_rhs);
            setValueMatrixRHSBD(ielSupportDof, ielSupportDof, FlagMatrixRHS::only_rhs);
          }
          numElement[eltType]++;
        }
      } else {
        numElement[eltType]+=numEltPerLabel;
      }
    }
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_rhs);
  }
  m_vectors[0].assembly();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::applyOnlyCVGBoundaryCondition() 
{
  if ( FelisceParam::instance(this->instanceIndex()).withCVG ) {
    if ( FelisceParam::verbose() > 2) {
      PetscPrintf(MpiInfo::petscComm(), "\n/============== Information about boundary condition.===============/\n");
      m_boundaryConditionList.print(m_verbosity);
      PetscPrintf(MpiInfo::petscComm(), "IMPOSING ONLY CVGRAPH DATA\n");
    }
    // natural conditions to be applied with the assembly loop
    if ( slave()->thereIsAtLeastOneNaturalCondition() ) {
      assembleRHSOnlyCVGraph();
    }
    // natural conditions to be directly summed into RHS
    cvgAddExternalResidualToRHS();

    // applying dirichlet boundary conditions
    if ( slave()->thereIsAtLeastOneDirichletCondition() ) {
      for ( std::size_t iConn(0); iConn < slave()->numConnections(); ++iConn ) {
        // Extract the labels of the current connection and store them in a set
        std::vector<int> tmp = slave()->interfaceLabels(iConn);
        std::unordered_set<int> labels(tmp.begin(),tmp.end());
        for(std::size_t iBC(0); iBC < m_boundaryConditionList.numDirichletBoundaryCondition(); iBC++) {
          const BoundaryCondition* const BC = m_boundaryConditionList.Dirichlet(iBC);
          if ( BC->listLabel() == labels ) {
            switch ( FelisceParam::instance(this->instanceIndex()).essentialBoundaryConditionsMethod ) {
              case DirichletApplicationOptions::symmetricPseudoElimination:
              {
                std::vector<felInt> rows;//unused because of the only_rhs flag.
                applyEssentialBoundaryConditionViaSymmetricPseudoEliminationOnBC(FlagMatrixRHS::only_rhs, BC,rows);
                break;
              }
              case DirichletApplicationOptions::penalization:
                applyEssentialBoundaryConditionViaPenalizationOnBC(FlagMatrixRHS::only_rhs, BC);
                break;
              case DirichletApplicationOptions::nonSymmetricPseudoElimination:
              default:
                FEL_ERROR("Requested method not yet available for cvgraph");
            }
          }
        }
      }
      vector().assembly();
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblem::sendZero(std::size_t iConn ) 
{
  PetscVector zero;
  zero.duplicateFrom(this->sequentialSolution());
  zero.zeroEntries();
  std::vector<PetscVector> vecs;
  for ( std::size_t cVar(0); cVar<this->slave()->numVarToSend(iConn); ++cVar) {
    vecs.push_back(zero);
  }
  this->slave()->sendData(vecs,iConn);
}

#endif
} // felisce namespace
