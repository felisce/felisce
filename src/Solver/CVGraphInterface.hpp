//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: J. Foulon, S. Smaldone
//

#ifndef _CVGRAPHINTERFACE_HPP
#define _CVGRAPHINTERFACE_HPP

// System includes
#include <cstdio>
#include <iostream>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "DegreeOfFreedom/dof.hpp"
#include "DegreeOfFreedom/listVariable.hpp"
#include "PETScInterface/petscVector.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce 
{

/*
Class to manage interface for coupled problems.

*** Exemple de fichier de parametres:
[interface]
type        = 'Dirichlet           Neumann'
listOfNode  = 'inlet.match         outlet.match'
variable    = 'velocity            velocity'
component   = 'Comp123             Comp123'


*** Initialisation des classes ListOfInterface / Interface
(dans le meme esprit que listOfBoundaryCondition / BoundaryCondition)

dans Interface: on a en particulier 2 conteneurs:
std::vector<int> m_listSupportOfDOF
std::vector<int> m_listDof
std::vector<double> m_listValue

*** Dans NSModel.cpp,
après l'appel
m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, m_rankProc);
et juste avant m_linearProblem[0]->solve(m_rankProc, m_numProc);
On parcourt les interfaces,
si le type de l'interface[i[ est Neummann on ne fait rien
si le type de l'interface[i] est Dirichlet:
Mettre une boucle du type de celle qui est dans applyBC:
for(felInt iDofBC = 0; iDofBC < m_listDof.size(); iDofBC++){
  iabis[0] = m_listDof[iDofBC];
  ia[0] = iabis[0];
  ia2[0] = iabis[0];
  AOPetscToApplication(m_ao,1,ia2);
  if (m_dofPart[ia2[0]] == rank){
    ic[0] = 1;
    ISGlobalToLocalMappingApply(m_mappingNodes,IS_GTOLM_MASK,1,ia2,ic,iabis);
    felInt rowSize = m_dof.iCSR()[iabis[0]+1] - m_dof.iCSR()[iabis[0]];
    // REMPLACER ->
    for ( felInt iCol = 0; iCol < rowSize; iCol++) {
      ic[0] = m_dof.jCSR()[m_dof.iCSR()[iabis[0]] + iCol];
      AOApplicationToPetsc(m_ao,1,ic);
      ib[iCol] = ic[0];
      value[iCol] = 0.;
      if ( ia2[0] == m_dof.jCSR()[m_dof.iCSR()[iabis[0]] + iCol] )
        value[iCol] = 1.;
    }
    // <-- Par simplement : Mettre sur la diagonale la valeur deja presente * TGV
    // et IDEM pour le second membre
    int size = 1;
    if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix))
      m_Matrix[0].setValues(size,ia,rowSize,ib,value,INSERT_VALUES);
    if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs))
      _RHS.setValue(*ia,idBCAndValue[*itDofBC],INSERT_VALUES);
  }
  }

  Que faire pour le cas Neumann ?

  S'inspirer de ce qui est fait dans LinearProblem::defineBC()
  pour les conditions aux limites de lumpedModelBC dans le cas où FelisceParam::instance().model == "NS"
  i.e. on ajoute une BC de type Neumann
  Remarque: ce ne sera donc plus nécessaire de préciser dans le fichier de paramètre la "BoundaryCondition" sur l'interface.

  */

class CVGraphInterface 
{
public:
  /// Pointer definition of CVGraphInterface
  FELISCE_CLASS_POINTER_DEFINITION(CVGraphInterface);

  // Empty constructor. Setting pointers to NULL and integers to 0.
  CVGraphInterface();
  
  // Destructor
  ~CVGraphInterface();
  
  // Reading from .match file
  // This function read the .match file and store the ids in m_listOfNodeInInterface
  // than, after having computed the number of dof at the interface, it allocates two vectors
  // m_listOfValuesInInterface and m_StoredSolution.
  void initialize(ListVariable* listVariable, const std::string& fileName);
  
  // Moving from supportDof (as they are stored in the match file) to the dofs.
  void identifyIdDof(Dof& dof);

  // Impose the entire solution at the interface
  void ImposeSolution(double* ImpValue);

  // Store the entire solution at the interface. To apply after model.formward()
  void StoreSolution(double* StoringValue);

  // Impose the solution corresponding to the variable with idVariable and a computed variable needed by the users
  // Split the values in the std::vector of solution and the vector of a variable computed by the user
  void ImposeSolutionAndComputedVariable(double* ImpValue, int idVariable);

  // Extract the entire solution at the interface
  void ExtractSolution(PetscVector& VecOfSol, double* valueDofInterface);

  // Extract only the solution corresponding to the variable with idVariable
  void ExtractSolution(PetscVector& VecOfSol, double* valueDofInterface, int idVariable);

  // Extract the solution with idVariable and a specific variable computed by the user
  void ExtractSolutionAndComputedVariable(PetscVector& VecOfSol, std::vector<double>& VecComputedVar,
                                          double* valueDofInterface, int idVariable);

  void print(int verbose=0, std::ostream& c=std::cout) const;
  
  felInt totalNumberOfNode() const {
    return m_totalNumberOfNode;
  }

  double* TotSolToImpose() {
    return m_TotSolToImpose;
  }
  
  double* StoredSolution() {
    return m_StoredSolution;
  }
  
  double* SolutionToImpose() {
    return m_SolutionToImpose;
  }
  
  double* ComputedVariableToImpose() {
    return m_ComputedVariableToImpose;
  }

  felInt* listOfDofInInterface() {
    return m_listOfDofInInterface;
  }
  
  felInt* listOfDofInInterfaceForVariable(int iVariable);
  
  void identifyIdDofForVariable();
  
  felInt  TotNumDofOnInterface() const {
    return m_TotNumDofOnInterface;
  }
  
  felInt  SizeListOfDofForVariable() const {
    return m_SizeListOfDofForVariable;
  }
  
  double* listOfValuesInInterface() {
    return m_listOfValuesInInterface;
  }

private:
  felInt  m_totalNumberOfNode = 0;
  int     m_numberOfDofBySupport = 0;
  ListVariable* m_listVariable = nullptr;
  felInt* m_listOfNodeInInterface = nullptr;
  double* m_TotSolToImpose = nullptr;
  double* m_StoredSolution = nullptr;
  double* m_SolutionToImpose = nullptr;
  double* m_ComputedVariableToImpose = nullptr;
  felInt* m_listOfDofInInterface = nullptr;
  felInt  m_TotNumDofOnInterface = 0;
  double* m_listOfValuesInInterface = nullptr;
  felInt  m_SizeListOfDofForVariable = 0;
  felInt** m_listOfDofForVariable = nullptr;
};

}

#endif
