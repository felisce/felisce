//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemElasticCurvedBeam.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce
{
  LinearProblemElasticCurvedBeam::LinearProblemElasticCurvedBeam():
    LinearProblem("Elastic Curved Beam")
  {
    m_createNormalTangent = true;
  }


  LinearProblemElasticCurvedBeam::~LinearProblemElasticCurvedBeam()
  {

    Elasticity.clear();

  }


  void LinearProblemElasticCurvedBeam::userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel)
  {
    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
  }


  void LinearProblemElasticCurvedBeam::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES)
  {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;

    std::vector<PhysicalVariable> listVariable(2);
    std::vector<std::size_t> listNumComp(2);
    listVariable[0] = displacement;
    listNumComp[0] = 2;
    listVariable[1] =  rotation;
    listNumComp[1] = 1;

    //define unknown of the linear system.
    m_listUnknown.push_back(displacement);
    m_listUnknown.push_back(rotation);
    this->definePhysicalVariable(listVariable, listNumComp);

    //compute normal & tangent on node of the mesh
    m_mesh[m_currentMesh]->computeNormalTangent();
    m_mesh[m_currentMesh]->createNormalTangent() = m_createNormalTangent;
    //allocate matrix to build elementary elastic matrix
    Elasticity.resize(2,2);
    m_B0.resize(2,6); m_B1.resize(2,6); m_B2.resize(2,6); m_B.resize(2,6);
    m_phiGeo_reinterpol.resize(2);
    m_F0.resize(2,2); m_F1.resize(2,2);
    m_F0_reinterpol.resize(2,2); m_F1_reinterpol.resize(2,2);
    m_reinterpolation = FelisceParam::instance().reinterpolation;

  }

  void LinearProblemElasticCurvedBeam::initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS)
  {
    (void) eltType;
    (void) flagMatrixRHS;

    m_iDisp = m_listVariable.getVariableIdList(displacement);
    m_iRota = m_listVariable.getVariableIdList(rotation);
    m_feDisp = m_listCurvilinearFiniteElement[m_iDisp];
    m_feRota = m_listCurvilinearFiniteElement[m_iRota];
    m_numPoint = m_feDisp->numPoint();
    m_numCoor = m_feDisp->numCoor();
    m_matVel = new ElementMatrix(std::vector<const CurBaseFiniteElement*>{m_feRota}, 
                                 std::vector<std::size_t>{m_listVariable[m_iRota].numComponent()},
                                 std::vector<std::size_t>{m_listVariable[m_iRota].numComponent()});
    
    // define elemField to put term u_n_1.phi_j in RHS.
    m_elemField.initialize(DOF_FIELD,*m_feDisp,m_listVariable[m_iDisp].numComponent() );

  }


  void LinearProblemElasticCurvedBeam::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    (void) elemIdPoint;
    (void) elemPoint;
    (void) iel;
    (void) flagMatrixRHS;
  }


  void LinearProblemElasticCurvedBeam::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, const std::vector<Point*>& elemNormal, const std::vector< std::vector<Point*> >& elemTangent, felInt& iel, FlagMatrixRHS flagMatrixRHS)
  {
    (void) elemIdPoint;
    (void) elemPoint;
    (void) iel;
    (void) flagMatrixRHS;

    // parameters
    double rho  = FelisceParam::instance().densitySolid;
    double tau  = m_fstransient->timeStep;

    double young = FelisceParam::instance().young;
    double nu = FelisceParam::instance().poisson;
    double depth = FelisceParam::instance().beam.depth;
    double thickness = FelisceParam::instance().thickness;
    double massVel  = rho*thickness/(tau*tau);

    // update finite element shape functions and its first derivatives at quadrature points
    m_feDisp->updateBeam(0, elemPoint, elemNormal, elemTangent, thickness);
    m_feRota->updateBeam(0, elemPoint, elemNormal, elemTangent, thickness);


    //creation of quadrature point in z
    const DegreeOfExactness degOfExactness_z = DegreeOfExactness_2;
    m_quadratureRule_z = &listQuadratureRuleSegment.quadratureRuleByExactness(degOfExactness_z);
    m_numQuadraturePoint_z = m_quadratureRule_z->numQuadraturePoint();
    m_numQuadraturePoint_r = m_feDisp->numQuadraturePoint();
    m_weightMeas_z.resize(m_numQuadraturePoint_z);
    for (int iz = 0; iz<m_numQuadraturePoint_z; iz++)
      m_weightMeas_z[iz] =  m_quadratureRule_z->weight(iz);

    // elementary elastic matrix
    for(int ir=0; ir<m_feDisp->numQuadraturePoint(); ir++) {
      m_Grr0 = 0.; m_Grr1 = 0.; m_Grr2 = 0.;
      m_dPhiGeo = m_feDisp->dPhiGeo(ir);
      m_phiGeo = m_feDisp->phiGeo(ir);
      // convariant basis linear part
      m_F0 = m_feDisp->F0(ir);
      m_F1 = m_feDisp->F1(ir);
      // convariant component G_rr
      m_feDisp->compute_Grr0_Grr1_Grr2(m_F0, m_F1);
      m_Grr0 = m_feDisp->Grr0();
      m_Grr1 = m_feDisp->Grr1();
      m_Grr2 = m_feDisp->Grr2();
      // polynomial reinterpolation tying points in treatment of numeral locking
       if (m_reinterpolation)  {
       m_reinterpoledPhiGeo();
       m_compute_F0_F1_reinterpoled(elemNormal, elemPoint, thickness);
          }
      m_computeStiffnessMatrix(elemTangent, m_reinterpolation);

      for(int iz=0; iz<m_numQuadraturePoint_z; iz++) {
        Elasticity.clear();
        double z = m_quadratureRule_z->quadraturePointCoor(iz,0);
        m_Grr = m_Grr0+z*m_Grr1+z*z*m_Grr2;
        m_Girr = 1./m_Grr;
        m_B = m_B0 + z*m_B1 + z*z*m_B2;
        m_weightMeas = std::sqrt(0.25*thickness*thickness*m_Grr) * m_feDisp->weightMeas[ir] * m_weightMeas_z[iz];
        Elasticity(0,0) = young/(1-nu*nu)*m_Girr*m_Girr;
        Elasticity(1,1) = 2.*(young/(1+nu))/(thickness*thickness)*m_Girr;

        UBlasMatrix temp_type (2,6);
        temp_type= prod(trans(m_B),Elasticity);
        m_elementMatBD[0]->mat() += prod(temp_type, m_B) * m_weightMeas  * depth;
      }
    }

    // elementary velocity matrix
    m_feDisp->updateMeasQuadPt(0, elemPoint);
    m_matVel->mat().clear();
    m_matVel->phi_i_phi_j(1.,*m_feDisp,0,0,1);
    m_elementMatBD[0]->matBlock(0,0) += massVel*m_matVel->mat();
    m_elementMatBD[0]->matBlock(1,1) += massVel*m_matVel->mat();


    //rho*epsilon/tau^2 *  M  X^{n-1}
    m_elemField.setValue(externalVec(0), *m_feDisp, iel, m_iDisp, m_ao, dof());
    m_elementVectorBD[0]->vecBlock(0) += prod(massVel*m_matVel->mat(), row(m_elemField.get_val(),0));
    m_elementVectorBD[0]->vecBlock(1) += prod(massVel*m_matVel->mat(), row(m_elemField.get_val(),1));
  }


 // to compute in each quadrature point basis functions reinterpoled
  void LinearProblemElasticCurvedBeam::m_reinterpoledPhiGeo() {
    for (int iNode = 0; iNode<m_numPoint; iNode++)
      m_phiGeo_reinterpol(iNode) = 0.5;
  }


  //----------------------------------------------------------------------
  // to compute in a quadrature F0 & F1 reinterpoled
  void LinearProblemElasticCurvedBeam::m_compute_F0_F1_reinterpoled(const std::vector<Point*>& elemNormal, const std::vector<Point*>& elemPoint, double thickness) {

    int iRefcoor=0;
    double sum1, sum2, sum3;
    m_F0_reinterpol.clear(); m_F1_reinterpol.clear();

    for (int jcoor=0; jcoor<m_numCoor; jcoor++){
      sum1= 0.; sum2= 0.; sum3= 0.;
      for (int iNode=0; iNode<m_numPoint; iNode++){
        sum1 += m_dPhiGeo(iRefcoor,iNode)*elemPoint[iNode]->coor(jcoor);
        sum2 += 0.5*thickness*m_phiGeo_reinterpol(iNode)*elemNormal[iNode]->coor(jcoor);
        sum3 += 0.5*thickness*m_dPhiGeo(iRefcoor,iNode)*elemNormal[iNode]->coor(jcoor);
      }
      m_F0_reinterpol(0,jcoor) = sum1; m_F0_reinterpol(1,jcoor) = sum2;  m_F1_reinterpol(0,jcoor) = sum3;
    }
  }

  void LinearProblemElasticCurvedBeam::m_computeStiffnessMatrix(const std::vector< std::vector<Point*> >& elemTangent, bool reinterpolation) {
    double thickness = FelisceParam::instance().thickness;

    m_B0.clear();  m_B1.clear();  m_B2.clear();
    for (int iNode=0; iNode<m_numPoint; iNode++){
      m_B0(0,iNode) = m_dPhiGeo(0,iNode)*m_F0(0,0);
      m_B1(0,iNode) = m_dPhiGeo(0,iNode)*m_F1(0,0);
      m_B0(0,iNode+m_numPoint) = m_dPhiGeo(0,iNode)*m_F0(0,1);
      m_B1(0,iNode+m_numPoint) = m_dPhiGeo(0,iNode)*m_F1(0,1);
      m_B1(0,iNode+2*m_numPoint) = 0.5*thickness*m_dPhiGeo(0,iNode)*(m_F0(0,0)*elemTangent[0][iNode]->coor(0)+m_F0(0,1)*elemTangent[0][iNode]->coor(1));
      m_B2(0,iNode+2*m_numPoint) = 0.5*thickness*m_dPhiGeo(0,iNode)*(m_F1(0,0)*elemTangent[0][iNode]->coor(0)+m_F1(0,1)*elemTangent[0][iNode]->coor(1));

      //if reinterpolation B rz
      if (reinterpolation)  {
        m_B0(1,iNode) =  m_dPhiGeo(0,iNode)*m_F0_reinterpol(1,0);
        m_B0(1,iNode+m_numPoint) = m_dPhiGeo(0,iNode)*m_F0_reinterpol(1,1);
        m_B0(1,iNode+2*m_numPoint) = 0.5*thickness*m_phiGeo_reinterpol(iNode)*(m_F0_reinterpol(0,0)*elemTangent[0][iNode]->coor(0)+m_F0_reinterpol(0,1)*elemTangent[0][iNode]->coor(1));
        m_B1(1,iNode+2*m_numPoint) = 0.5*thickness*(m_dPhiGeo(0,iNode)*(m_F0_reinterpol(1,0)*elemTangent[0][iNode]->coor(0)+m_F0_reinterpol(1,1)*elemTangent[0][iNode]->coor(1))+
                                                    m_phiGeo_reinterpol(iNode)*(m_F1_reinterpol(0,0)*elemTangent[0][iNode]->coor(0)+m_F1_reinterpol(0,1)*elemTangent[0][iNode]->coor(1)));
      }
      else {
        m_B0(1,iNode) =  m_dPhiGeo(0,iNode)*m_F0(1,0);
        m_B0(1,iNode+m_numPoint) = m_dPhiGeo(0,iNode)*m_F0(1,1);
        m_B0(1,iNode+2*m_numPoint) = 0.5*thickness*m_phiGeo(iNode)*(m_F0(0,0)*elemTangent[0][iNode]->coor(0)+m_F0(0,1)*elemTangent[0][iNode]->coor(1));
        m_B1(1,iNode+2*m_numPoint) = 0.5*thickness*(m_dPhiGeo(0,iNode)*(m_F0(1,0)*elemTangent[0][iNode]->coor(0)+m_F0(1,1)*elemTangent[0][iNode]->coor(1))+
                                                    m_phiGeo(iNode)*(m_F1(0,0)*elemTangent[0][iNode]->coor(0)+m_F1(0,1)*elemTangent[0][iNode]->coor(1)));
      }
    }
  }

}

