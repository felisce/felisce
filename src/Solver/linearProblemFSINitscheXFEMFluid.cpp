//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemFSINitscheXFEMFluid.hpp"
#include "Core/felisceTools.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "Geometry/neighFacesOfEdges.hpp"

namespace felisce {
  LinearProblemFSINitscheXFEMFluid::LinearProblemFSINitscheXFEMFluid():
    LinearProblem("Fictitious Domain Fluid Structure Interaction"),
    m_viscosity(0.),
    m_density(0.),
    m_bdf(nullptr) {
    m_isSeqVelExtrapolAllocated = false;
    m_isSeqBdfRHSAllocated = false;
    m_isSeqOriginalSolAllocated = false;
    m_isSeqRNFluidExtrapolAllocated = false;
    m_feStruc = nullptr;
    m_elemFieldTimeSchemeType = nullptr;
    m_timeSchemeType = 0;
    m_numOriginalDof = 0;
    m_useCurrentFluidSolution = false;

    m_seqVelExtrapol.resize(FelisceParam::instance().orderBdfNS);
    for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i)
      m_seqVelExtrapol[i] = PetscVector::null();

    m_seqBdfRHS.resize(FelisceParam::instance().orderBdfNS);
    for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i)
      m_seqBdfRHS[i] = PetscVector::null();
  }

  LinearProblemFSINitscheXFEMFluid::~LinearProblemFSINitscheXFEMFluid() {
    m_bdf = nullptr;
    if (m_isSeqBdfRHSAllocated) {
      for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i)
        m_seqBdfRHS[i].destroy();
      m_seqBdfRHS.clear();
    }

    if (m_isSeqVelExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i)
        m_seqVelExtrapol[i].destroy();
      m_seqVelExtrapol.clear();
    }

    for(std::size_t iPt=0; iPt < m_mshPts.size(); ++iPt) {
      delete m_mshPts[iPt];
    }

    for(std::size_t iPt=0; iPt < m_subItfPts.size(); ++iPt) {
      delete m_subItfPts[iPt];
    }

    if (m_isSeqOriginalSolAllocated)
      m_tmpSeqOriginalSol.destroy();

    for(std::size_t i=0; i<m_solutionOld.size(); ++i)
      m_solutionOld[i].destroy();

    for(std::size_t i=0; i<m_sequentialSolutionOld.size(); ++i)
      m_sequentialSolutionOld[i].destroy();
    
    for(std::size_t i=0; i<m_aoOld.size(); ++i)
      AODestroy(&m_aoOld[i]);
  }

  void LinearProblemFSINitscheXFEMFluid::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    // calling initialize in LinearProblem
    LinearProblem::initialize(mesh,comm, doUseSNES);

    // the problem
    m_fstransient = fstransient;
    setNumberOfMatrix(1);

    // define variable of the linear system
    std::vector<PhysicalVariable> listVariable(2);
    std::vector<std::size_t> listNumComp(2);
    listVariable[0] = velocity;
    listNumComp[0]  = 2; // because it only works for 2d in the fluid
    listVariable[1] = pressure;
    listNumComp[1]  = 1;
      
    //define unknown of the linear system.
    m_listUnknown.push_back(velocity);
    m_listUnknown.push_back(pressure);

    // link between variable and unknowns
    definePhysicalVariable(listVariable, listNumComp);

    // data from the data file
    m_viscosity = FelisceParam::instance().viscosity;
    m_density = FelisceParam::instance().density;
    m_useSymmetricStress = FelisceParam::instance().useSymmetricStress;

    // get the time scheme type from the data file
    m_elemFieldTimeSchemeType = FelisceParam::instance().elementFieldDynamicValue("TimeSchemeType");
    if(m_elemFieldTimeSchemeType != nullptr)
      m_timeSchemeType = (felInt) m_elemFieldTimeSchemeType->constant(Comp1);

    if ((m_timeSchemeType == 0) || (m_timeSchemeType == 2)) {
      // Robin-Neumann scheme       
      // extrapolation order
      m_betaRN.resize(FelisceParam::instance().fsiRNscheme);
      if (FelisceParam::instance().fsiRNscheme == 1){
        m_betaRN[0] = 1.;
      } else if (FelisceParam::instance().fsiRNscheme == 2){
        m_betaRN[0] = 2.;
        m_betaRN[1] = -1.;
      }
    }
    
    // sub element
    m_numVerPerFluidElt = m_mesh[m_currentMesh]->domainDim() + 1;
    m_numVerPerSolidElt = m_mesh[m_currentMesh]->domainDim();

    // create the std::vector of points for the fluid elements
    m_mshPts.resize(m_numVerPerFluidElt);
    for(std::size_t iver=0; iver < m_mshPts.size(); ++iver) {
      m_mshPts[iver] = new Point();
    }

    m_itfPts.resize(m_numVerPerSolidElt);

    m_subItfPts.resize(m_numVerPerSolidElt);
    for(std::size_t iver=0; iver < m_subItfPts.size(); ++iver) {
      m_subItfPts[iver] = new Point();
    }
  }


  void LinearProblemFSINitscheXFEMFluid::addSolidVariable() {
    for(std::size_t iUnknown=0; iUnknown<m_pLinStruc->listUnknown().size(); ++iUnknown) {
      m_listVariable.addVariable(m_pLinStruc->listUnknown()[iUnknown], m_pLinStruc->listVariable()[m_pLinStruc->listUnknown().idVariable(iUnknown)].numComponent());
    }
  }


  void LinearProblemFSINitscheXFEMFluid::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    // Get id of the variables
    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    m_iPressure = m_listVariable.getVariableIdList(pressure);

    // Get the finite elements
    m_feVel = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];

    // Get the variables of the problem
    m_velocity = &m_listVariable[m_iVelocity];
    m_pressure = &m_listVariable[m_iPressure];


    // Initialize the element fields
    if ((m_timeSchemeType == 0) || (m_timeSchemeType == 2)){
      m_seqVelDofPts.resize(FelisceParam::instance().fsiRNscheme);
      m_seqPreDofPts.resize(FelisceParam::instance().fsiRNscheme);
    } else {
      m_seqVelDofPts.resize(1);
      m_seqPreDofPts.resize(1);
    }
    m_elemFieldAdv.initialize(DOF_FIELD, *m_feVel, dimension());
    m_elemFieldAdvDup.initialize(DOF_FIELD, *m_feVel, dimension());
    m_elemFieldAdvTmp.initialize(DOF_FIELD, *m_feVel, dimension());
    m_elemFieldRHS.initialize(DOF_FIELD, *m_feVel, dimension());
    m_elemFieldRHSbdf.initialize(DOF_FIELD, *m_feVel, dimension());
    m_elemFieldTotalPressureFormulation.initialize(QUAD_POINT_FIELD, *m_feVel, dimension());
    m_elemFieldNormal.initialize(CONSTANT_FIELD, m_feVel->numCoor());
    m_elemFieldNormalInit.initialize(CONSTANT_FIELD, m_feVel->numCoor());
    m_elemFieldAux.initialize(CONSTANT_FIELD, m_feVel->numCoor());
    for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
      m_seqVelDofPts[i].initialize(DOF_FIELD, *m_feVel, dimension());
      m_seqPreDofPts[i].initialize(DOF_FIELD, *m_fePres, 1);
    }

    // solid
    felInt idUnknownSolid = m_pLinStruc->listUnknown().getUnknownIdList(displacement);
    felInt idVarSolid = m_pLinStruc->listUnknown().idVariable(idUnknownSolid);
    m_strucVelQuadPts.initialize(QUAD_POINT_FIELD, *m_feVel, dimension());
    m_strucVelDofPts.initialize(DOF_FIELD, *m_feStruc, m_pLinStruc->listVariable()[idVarSolid].numComponent());
    m_ddotComp.initialize(QUAD_POINT_FIELD, *m_fePres, 1);
  }


  void LinearProblemFSINitscheXFEMFluid::initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    // get the id of the variables
    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    m_iPressure = m_listVariable.getVariableIdList(pressure);

    // Get the curvilinear finite elements
    m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
    m_curvFePress = m_listCurvilinearFiniteElement[m_iPressure];

    // Get the variables of the problem
    m_velocity = &m_listVariable[m_iVelocity];
    m_pressure = &m_listVariable[m_iPressure];
  }


  // Virtual function in linearProblem. To allocate additional setValue. For Essential boundary conditions type
  void LinearProblemFSINitscheXFEMFluid::finalizeEssBCTransientDerivedProblem() {

  }



  void LinearProblemFSINitscheXFEMFluid::userChangePattern(int numProc, int rankProc) {
    // There may be integrals that involve the dof of the two duplicated support elements.
    // The integrals for the ghost penalty modify also the pattern
    // same for the face-oriented stabilization
    (void) numProc;
    (void) rankProc;
    
    // definition of variables
    felInt currentIelGeo, oppositeIelGeo;
    felInt node1, node2;
    felInt idVar1, idVar2;
    felInt ielSupport, jelSupport;
    std::vector<felInt> vecSupportCurrent, vecSupportOpposite;
    bool isAnInnerFace;
    bool computeBothSide;
    bool addOppositeSupportElt;
    bool isDomainElement;

    GeometricMeshRegion::ElementType eltType, eltTypeOpp;
    felInt numEltPerLabel;
    felInt numElement[GeometricMeshRegion::m_numTypesOfElement];
    felInt idByType;
    felInt numFacesPerElement;

    const std::vector<felInt>& iSupportDof = m_supportDofUnknown[0].iSupportDof();
    const std::vector<felInt>& iEle = m_supportDofUnknown[0].iEle();
    const std::vector<GeometricMeshRegion::ElementType>& bagDomain =m_mesh[m_currentMesh]->bagElementTypeDomain();

    // the initial repartition of dof
    felInt numDof = dof().numDof();
    felInt numDofByProc = numDof/MpiInfo::numProc();
    felInt numDofProc = 0;
    felInt beginIdDofLocal = MpiInfo::rankProc()*numDofByProc;
    felInt endIdDofLocal;

    if(MpiInfo::rankProc() == MpiInfo::numProc()-1) {
      numDofProc = numDof - MpiInfo::rankProc()*numDofByProc;
      endIdDofLocal = numDof;
    } else {
      numDofProc = numDofByProc;
      endIdDofLocal = beginIdDofLocal + numDofProc;
    }

    
    // the complementary pattern
    std::vector<felInt> iCSR, jCSR;
    std::vector< std::set<felInt> > nodesNeighborhood(numDofProc);

    // build the edges or faces depending on the dimension
    // In this function, "faces" refers to either edges or faces.
    if(FelisceParam::instance().useGhostPenalty || FelisceParam::instance().NSStabType == 1) {
      if(dimension() == 2)
       m_mesh[m_currentMesh]->buildEdges();
      else
       m_mesh[m_currentMesh]->buildFaces();
    }
    
    
    
    // -------------------------------------------------------------------------- //
    // Complementary pattern for :                                                //
    //     - An interface with a tip inside the fluid domain
    //     - Robin-Neumann scheme (terms like u1 v2)                              //
    //     - ghost penalty without face-oriented stabilization (opposite element) //
    // -------------------------------------------------------------------------- //
    // loop over the unknowns of the problem
    for (std::size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); iUnknown1++) {
      idVar1 = m_listUnknown.idVariable(iUnknown1);
      
      // loop over all the intersected elements
      auto& idInterElt = m_duplicateSupportElements->getIntersectedMshElt();
      for (auto ielIt = idInterElt.begin(); ielIt != idInterElt.end(); ++ielIt) {
        // get the geometric id of the element and the ids of its support elements
        currentIelGeo = ielIt->first;
        m_mesh[m_currentMesh]->getTypeElemFromIdElem(currentIelGeo, eltType, idByType);
        
        isDomainElement = false;
        for(std::size_t i=0; i<bagDomain.size(); ++i)
          if(bagDomain[i] == eltType)
            isDomainElement = true;

        numFacesPerElement = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();
        
        m_supportDofUnknown[iUnknown1].getIdElementSupport(currentIelGeo, vecSupportCurrent);
        
        // loop over the support elements
        for(std::size_t ielSup=0; ielSup<vecSupportCurrent.size(); ++ielSup) {
          ielSupport = vecSupportCurrent[ielSup];
          
          // for all support dof in this support element
          for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
            // for all component of the unknown
            for (std::size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); iComp++) {
              // get the global id of the support dof
              dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);

              // If this support dof is owned by this process
              if(node1 >= beginIdDofLocal && node1 < endIdDofLocal) {
                // for all unknown in the linear problem
                for (std::size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                  idVar2 = m_listUnknown.idVariable(iUnknown2);
                  
                  // for all support element different than the ielSup
                  std::size_t jelSup = 0;
                  while(jelSup < vecSupportCurrent.size()) {
                    if(jelSup != ielSup) {
                      jelSupport = vecSupportCurrent[jelSup];
                      
                      // for all support dof in this support element
                      for (felInt jSup = 0; jSup < supportDofUnknown(iUnknown2).getNumSupportDof(jelSupport); ++jSup) {                            
                        // for all component of the second unknown
                        for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {      
                          // If the two current components are connected
                          felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);
                          felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                          if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                            // get the global id of the second support dof
                            dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                            if(node1 != node2)
                              nodesNeighborhood[node1].insert(node2);
                          }
                        }
                      }
                    }
                    ++jelSup;
                  }
                  
                  // Ghost penalty but no face-oriented stabilization
                  if(FelisceParam::instance().useGhostPenalty && FelisceParam::instance().NSStabType != 1 && isDomainElement) {
                    // loop over the boundaries of the element
                    for(felInt iface=0; iface < numFacesPerElement; ++iface) {
                      // check if this face is an inner face or a boundary
                      oppositeIelGeo = currentIelGeo;
                      eltTypeOpp = eltType;
                      isAnInnerFace = m_mesh[m_currentMesh]->getAdjElement(eltTypeOpp, oppositeIelGeo, iface);
                      
                      if(isAnInnerFace) {
                        // get the support element of the opposite element
                        supportDofUnknown(iUnknown2).getIdElementSupport(oppositeIelGeo, vecSupportOpposite);
                        
                        // check if the opposite element is duplicated or not
                        if(vecSupportOpposite.size() > 1) {
                          // take the element on the same side
                          jelSupport = vecSupportOpposite[ielSup];
                          computeBothSide = false;
                          addOppositeSupportElt = true;
                        } else {
                          jelSupport = vecSupportOpposite[0];
                          
                          // the opposite element is not duplicated, check if it is on the same side
                          std::size_t sizeCurrent = m_supportDofUnknown[0].getNumSupportDof(ielSupport);
                          std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(jelSupport);
                          
                          felInt countSupportDofInCommon = 0;
                          for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                            for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2) {
                              if(iSupportDof[iEle[jelSupport] + iSup2] == iSupportDof[iEle[ielSupport] + iSup1]) {
                                ++countSupportDofInCommon;
                              }
                            }
                          }
                          
                          if(countSupportDofInCommon > 1) {
                            // the elements are on the same side
                            computeBothSide = true;
                            addOppositeSupportElt = true;
                          } else {
                            computeBothSide = false;
                            addOppositeSupportElt = false;
                          }
                        }
                        
                        if(addOppositeSupportElt) {
                          // for all support dof in this support element
                          for (felInt jSup = 0; jSup < supportDofUnknown(iUnknown2).getNumSupportDof(jelSupport); ++jSup) {
                            // for all component of the second unknown
                            for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {
                              // If the two current components are connected
                              felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);
                              felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                              if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                                // get the global id of the second support dof
                                dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                                if(node1 != node2) {
                                  nodesNeighborhood[node1].insert(node2);
                                  
                                  if(computeBothSide)
                                    nodesNeighborhood[node2].insert(node1);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }


    // ----------------------------------------------------- //
    // Complementary pattern for face-oriented stabilization //
    // ----------------------------------------------------- //
    if(FelisceParam::instance().NSStabType == 1) {
      // loop on the unknown of the linear problem
      for (std::size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
        idVar1 = m_listUnknown.idVariable(iUnknown1);
        currentIelGeo = 0;

        // first loop on element type
        for (std::size_t i=0; i<m_mesh[m_currentMesh]->bagElementTypeDomain().size(); ++i) {
          eltType = m_mesh[m_currentMesh]->bagElementTypeDomain()[i];
          const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
          numElement[eltType] = 0;
          numFacesPerElement = geoEle->numBdEle();
          
          // second loop on region of the mesh.
          for(auto itRef = m_mesh[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef !=m_mesh[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
            numEltPerLabel = itRef->second.second;
            
            // Third loop on element
            for (felInt iel = 0; iel < numEltPerLabel; iel++) {
              m_supportDofUnknown[iUnknown1].getIdElementSupport(eltType, numElement[eltType], vecSupportCurrent);
              
              for(std::size_t ielSup=0; ielSup<vecSupportCurrent.size(); ++ielSup) {
                ielSupport = vecSupportCurrent[ielSup];
                
                // for all support dof in this support element
                for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
                  // for all component of the unknown
                  for (std::size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); iComp++) {
                    // get the global id of the support dof
                    dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);
                    
                    // if this node is on this process
                    if(node1 >= beginIdDofLocal && node1 < endIdDofLocal) {
                      // loop over the boundaries of the element
                      for(felInt iface=0; iface < numFacesPerElement; ++iface) {
                        // check if this face is an inner face or a boundary
                        oppositeIelGeo = currentIelGeo;
                        eltTypeOpp = eltType;
                        isAnInnerFace = m_mesh[m_currentMesh]->getAdjElement(eltTypeOpp, oppositeIelGeo, iface);
                        
                        if(isAnInnerFace) {
                          // for all unknown
                          for (std::size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                            idVar2 = m_listUnknown.idVariable(iUnknown2);
                            
                            // get the support element of the opposite element
                            supportDofUnknown(iUnknown2).getIdElementSupport(oppositeIelGeo, vecSupportOpposite);
                            
                            // check if the opposite element and the current element are both duplicated
                            if(vecSupportCurrent.size() == vecSupportOpposite.size()) {
                              // take the element on the same side
                              jelSupport = vecSupportOpposite[ielSup];
                              addOppositeSupportElt = true;
                            } else {
                              addOppositeSupportElt = false;
                              jelSupport = -1;
                              for(std::size_t jelSup=0; jelSup < vecSupportOpposite.size() && !addOppositeSupportElt; ++jelSup) {
                                jelSupport = vecSupportOpposite[jelSup];
                                
                                // the opposite element is not duplicated, check if it is on the same side
                                std::size_t sizeCurrent = m_supportDofUnknown[0].getNumSupportDof(ielSupport);
                                std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(jelSupport);
                                
                                felInt countSupportDofInCommon = 0;
                                for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                                  for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2) {
                                    if(iSupportDof[iEle[jelSupport] + iSup2] == iSupportDof[iEle[ielSupport] + iSup1]) {
                                      ++countSupportDofInCommon;
                                    }
                                  }
                                }
                                
                                if(countSupportDofInCommon > 1)
                                  addOppositeSupportElt = true;
                              }
                            }
                            
                            if(addOppositeSupportElt) {
                              // for all support dof in this support element
                              for (felInt jSup = 0; jSup < supportDofUnknown(iUnknown2).getNumSupportDof(jelSupport); ++jSup) {
                                // for all component of the second unknown
                                for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {
                                  // If the two current components are connected
                                  felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);
                                  felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                                  if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                                    // get the global id of the second support dof
                                    dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                                    if(node1 != node2)
                                      nodesNeighborhood[node1].insert(node2);
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              ++currentIelGeo;
              ++numElement[eltType];
            }
          }
        }
      }
    }

    
    // ----------------------------------------------------------------- //
    // build the complementary pattern and merge it with the current one //
    // ----------------------------------------------------------------- //
    felInt dofSize = 0;
    felInt pos;
    
    iCSR.resize(dof().pattern().numRows() + 1, 0);
    for(std::size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode)
      dofSize += nodesNeighborhood[iNode].size();
    
    if(dofSize > 0) {
      jCSR.resize(dofSize, 0);
      for(std::size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode) {
        iCSR[iNode + 1] = iCSR[iNode] + nodesNeighborhood[iNode].size();
        pos = 0;
        for (auto it=nodesNeighborhood[iNode].begin(); it != nodesNeighborhood[iNode].end(); ++it) {
          jCSR[iCSR[iNode] + pos] = *it;
          ++pos;
        }
      }
      
      // Now, call merge pattern
      dof().mergeGlobalPattern(iCSR, jCSR);
    }
  }



  void LinearProblemFSINitscheXFEMFluid::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel1, felInt& iel2, ElementType& eltType, felInt& ielGeo, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_ELEM_ID_POINT;

    std::vector<felInt> strucElemIds;

    // get the side of the support element
    // here iel2 is refering to the solution because it will std::set the column in the global matrix
    // (see the function setValueMatrixRHS in linearProblem.cpp)
    sideOfInterface sideOfSupportElementForSolution;
    double side;
    felInt ielSupportDof;
    m_supportDofUnknown[0].getIdElementSupport(ielGeo, ielSupportDof);
    sideOfSupportElementForSolution = ielSupportDof == iel2 ? LEFT : RIGHT;
    side = sideOfSupportElementForSolution == LEFT ? 1. : -1.;

    if(iel1 == iel2) {
      //////////////////////////////////
      // VOLUME ELEMENT - SUB ELEMENT //
      //////////////////////////////////
      felInt numSubElement = m_duplicateSupportElements->getNumSubMshEltPerMshElt(ielGeo);


      // -------------------------------------------
      // Get the support element of the old solutions
      std::vector<felInt> ielOld(m_solutionOld.size());  
      std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
      for(std::size_t i=0; i<oldSupportElement.size(); ++i) {
        m_supportDofUnknownOld[i][0].getIdElementSupport(ielGeo, oldSupportElement[i]);
      
        // std::set the id for the old solution
        if(oldSupportElement[i].size() > 1) {
          // this element is also duplicated for the old solution
          ielOld[i] = sideOfSupportElementForSolution == LEFT ? oldSupportElement[i][0] : oldSupportElement[i][1];
        } else {
          // this element is NOT duplicated for the old solution
          ielOld[i] = oldSupportElement[i][0];
        }
      }
      
      if(numSubElement > 0) {
        // Volume sub elements
        felInt subElemId;
        std::vector<double> tmpPt(3);

        // loop over the sub-elements
        for(felInt iSubElt=0; iSubElt < numSubElement; ++iSubElt) {
          subElemId = m_duplicateSupportElements->getSubMshEltIdxMsh(ielGeo, iSubElt);

          // if the side of the sub-element is the same as the element
          if(m_duplicateSupportElements->getSubMshEltSide(ielGeo, iSubElt) == sideOfSupportElementForSolution) {
            // --------------------------------------------------
            // fill the std::vector with the points of the sub-element
            for(std::size_t iPt=0; iPt < m_numVerPerFluidElt; ++iPt) {
              m_duplicateSupportElements->getSubMshEltVerCrd(subElemId, iPt, tmpPt);
              *m_mshPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
            }

            // ---------------------
            // Update finite element
            m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);
            m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);


            // -----------------------
            // build classic NS matrix
            computeElementMatrixRHS();
            for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i)
              computeElementMatrixRHSPartDependingOnOldTimeStep(iel1, ielOld[i], i);
          }
        }
      } else {
        // No volume sub elements
        for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
          if(oldSupportElement[i].size() > 1) {
            // old element is duplicated, get the sub elements
            felInt numSubElementOld = m_duplicateSupportElements->getNumSubMshEltMshOld(ielGeo, i);

            felInt subElemIdOld;
            std::vector<double> tmpPtOld(3);
            felInt ielOldTmp;
            
            for(felInt iSubElt=0; iSubElt < numSubElementOld; ++iSubElt) {
              subElemIdOld = m_duplicateSupportElements->getSubMshEltIdxMshOld(ielGeo, iSubElt, i);
            
              // --------------------------------------------------
              // fill the std::vector with the points of the sub-element
              for(std::size_t iPt=0; iPt < m_numVerPerFluidElt; ++iPt) {
                m_duplicateSupportElements->getSubMshEltVerCrdOld(subElemIdOld, iPt, tmpPtOld, i);
                *m_mshPts[iPt] = Point(tmpPtOld[0], tmpPtOld[1], tmpPtOld[2]);
              }

              // ---------------------
              // Update finite element
              m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);
              m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);
              
              
              // --------------------------------------------------------
              // Get the id of the support element owning the sub element
              ielOldTmp = m_duplicateSupportElements->getSubMshEltSideOld(subElemIdOld, i) == LEFT ? oldSupportElement[i][0] : oldSupportElement[i][1];


              // -----------------------
              // build classic NS matrix
              computeElementMatrixRHSPartDependingOnOldTimeStep(iel1, ielOldTmp, i);
            }
          }
        }

        // Update finite element if needed
        if(!m_feVel->hasOriginalQuadPoint() || !m_fePres->hasOriginalQuadPoint()) {
          m_feVel->updateBasisAndFirstDeriv(0, elemPoint);
          m_fePres->updateBasisAndFirstDeriv(0, elemPoint);
        } else {
          m_feVel->updateFirstDeriv(0, elemPoint);
          m_fePres->updateFirstDeriv(0, elemPoint);
        }
          
        // build classic NS matrix
        computeElementMatrixRHS();

        // build matrix and rhs depending on old time step if this element is not intersected
        // by the old solutions
        for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
          if(oldSupportElement[i].size() == 1)
            computeElementMatrixRHSPartDependingOnOldTimeStep(iel1, ielOld[i], i);
        }
      }



      ////////////////////////////
      // SUB STRUCTURE ELEMENTS //
      ////////////////////////////
      numSubElement = m_duplicateSupportElements->getNumSubItfEltPerMshElt(ielGeo);
      if(numSubElement > 0) {
        felInt subElemId;
        felInt elemId;
        std::vector<double> tmpPt(3);
        std::vector<double> itfNormal(dimension(),0);
        std::vector<double> itfNormalInit(dimension(),0);

        for(felInt iSubElt=0; iSubElt < numSubElement; ++iSubElt) {
          // get the id of the sub element
          subElemId = m_duplicateSupportElements->getSubItfEltIdxMsh(ielGeo, iSubElt);
          
          // get the id of the element owing this sub-element
          elemId = m_duplicateSupportElements->getItfEltIdxOfSubItfElt(subElemId);

          // get the normal to this sub element
          GeometricMeshRegion::ElementType eltTypeStruc = m_pLinStruc->mesh()->bagElementTypeDomain()[0];
          m_pLinStruc->mesh()->listElementNormal(eltTypeStruc, elemId).getCoor(itfNormal);
          m_pLinStruc->mesh()->listElementNormalInit(eltTypeStruc, elemId).getCoor(itfNormalInit);          

          // fill the std::vector with the points of the sub-element
          for(std::size_t iPt=0; iPt < m_numVerPerSolidElt; ++iPt) {
            m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
            *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
          }

          // Get the coordinates of the element owning this sub-element
          int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltTypeStruc];
          std::vector<felInt> strucElemPtsId(numPointsPerElt);

          m_pLinStruc->mesh()->getOneElement(eltTypeStruc, elemId, strucElemPtsId, 0);

          for (int iPoint = 0; iPoint < numPointsPerElt; ++iPoint)
            m_itfPts[iPoint] = &m_pLinStruc->mesh()->listPoint(strucElemPtsId[iPoint]);


          // update the finite element
          m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_feStruc);
          m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_feStruc);

          // Set element field
          if(FelisceParam::instance().useProjectionForNitscheXFEM || m_useCurrentFluidSolution) {
            for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
              if ((m_timeSchemeType == 0) || (m_timeSchemeType == 2)){ 
                m_seqVelDofPts[i].setValue(m_seqRNFluidExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
                m_seqPreDofPts[i].setValue(m_seqRNFluidExtrapol[i], *m_fePres, iel2, m_iPressure, m_ao, dof());
              }else{
                m_seqVelDofPts[i].setValue(sequentialSolution(), *m_feVel, iel2, m_iVelocity, m_ao, dof());
                m_seqPreDofPts[i].setValue(sequentialSolution(), *m_fePres, iel2, m_iPressure, m_ao, dof());
              }
            }
          } else {
            for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
              m_seqVelDofPts[i].setValue(m_sequentialSolutionOld[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
              m_seqPreDofPts[i].setValue(m_sequentialSolutionOld[i], *m_fePres, ielOld[i], m_iPressure, m_aoOld[i], m_dofOld[i]);
            }
          }

          m_elemFieldNormal.setValue(itfNormal);
          m_elemFieldNormalInit.setValue(itfNormalInit);


          // Inflow stabilization for NS
          if(FelisceParam::instance().NSequationFlag == 1 && FelisceParam::instance().useInterfaceFlowStab) {
            // compute (\beta \cdot n)_- u_i v_i
            m_elementMat[0]->f_dot_n_phi_i_phi_j(0.5 * side * FelisceParam::instance().density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, 0, 0, dimension(), 0);
          }

          // compute the elementary matrix and rhs
          switch(m_timeSchemeType) {
            case 0: {
              // Robin-Neumann schemes (direct implementation) (0th, 1st and 2nd order extrapolated variants)

              // MATRIX //
              // \rho_s \epsilon / dt \int_S  u_i . v_i
              double penaltyParam = FelisceParam::instance().densitySolid * FelisceParam::instance().thickness / m_fstransient->timeStep;
              m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, 0, 0, dimension());


              // RHS //
              // update the value of the structure on that sub element (std::set m_strucVelQuadPts)
              updateStructureVel(elemId, externalVec(0));
              updateSubStructureVel(m_itfPts, m_subItfPts);

              // \rho_s \epsilon / dt \int_S ( ddot^{n-1} + dt \pd ddot^{\star} ) . v_i
              m_elementVector[0]->source(penaltyParam, *m_feVel, m_strucVelQuadPts, 0, dimension());

              for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
                // velocity
                if(m_useSymmetricStress)
                  m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity * m_betaRN[i], m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());
                else
                  m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity * m_betaRN[i], m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());

                // pressure
                for(felInt idim=0; idim<dimension(); ++idim)
                  m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0) * m_betaRN[i], *m_feVel, m_seqPreDofPts[i], idim, 1);
              }

             break;
            }

            case 1: {
              // Robin-Robin
              // MATRIX //
              // Nitsche's penalty term, \gamma \nu / h u_i . v_i
              double penaltyParam = m_viscosity*FelisceParam::instance().NitschePenaltyParam/m_feVel->diameter();
              m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, 0, 0, dimension());

              // RHS //
              // Nitsche's consistency term in the RHS //
              // velocity
              if(m_useSymmetricStress)
                m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());
              else
                m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());

              // pressure
              for(felInt idim=0; idim<dimension(); ++idim)
                m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0), *m_feVel, m_seqPreDofPts[0], idim, 1);

              // update the value of the solid velocity (std::set m_strucVelDofPts and m_strucVelQuadPts)
              // m_strucVelDofPts.val.clear();
              updateStructureVel(elemId, externalVec(0));

              // m_strucVelQuadPts.val.clear();
              updateSubStructureVel(m_itfPts, m_subItfPts);

              // penalty term (RHS part)
              m_elementVector[0]->source(penaltyParam, *m_feVel, m_strucVelQuadPts, 0, dimension());

              break;
            }

            case 2: {
              // Robin-Neumann schemes a la Stenberg (0th, 1st and 2nd order extrapolated variants)

              // MATRIX // 

              double alpha = 0.5;    
              double RNParam = FelisceParam::instance().densitySolid * FelisceParam::instance().thickness / m_fstransient->timeStep; 	    
              double penaltyParam1 = (m_feVel->diameter()) / (m_viscosity*FelisceParam::instance().NitschePenaltyParam + RNParam*m_feVel->diameter());
              double penaltyParam2 = (FelisceParam::instance().NitschePenaltyParam*m_viscosity ) / (m_viscosity*FelisceParam::instance().NitschePenaltyParam + RNParam*m_feVel->diameter());

              // +  u_i . v_i
              m_elementMat[0]->phi_i_phi_j(alpha * penaltyParam2 * RNParam, *m_feVel, 0, 0, dimension());

              // - \sigma(u_i, p_i)n_i . v_i
              double coef = alpha * RNParam * penaltyParam1 + (1 - alpha);
              computeNitscheSigma_u_v(coef * side * m_viscosity,  coef * -side);

              // - \sigma(0, -q_i)n_i . u_i (flip of sign because of the implementation of NS terms crossed with q)
              computeNitscheSigma_v_u(0., alpha * penaltyParam1 * RNParam * -side);

              // - \sigma(u_i, p_i)n_i . \sigma(0, -q_i)n_i (flip of sign because of the implementation of NS terms crossed with q)
              computeStenbergSigma_u_Sigma_v( m_viscosity * alpha * penaltyParam1 , - alpha * penaltyParam1 );

              // RHS //

              // update the value of the structure on that sub element (std::set m_strucVelQuadPts)
              updateStructureVel(elemId, externalVec(0));
              updateSubStructureVel(m_itfPts, m_subItfPts);
              
              // + (ddot^{n-1} + dt \pd ddot^{\star}) . v_i
              m_elementVector[0]->source(alpha * penaltyParam2 * RNParam , *m_feVel, m_strucVelQuadPts, 0, dimension());

              // - (ddot^{n-1} + dt \pd ddot^{\star}) . \sigma(0, -q_i) n_i  (flip of sign because of the implementation of NS terms crossed with q)
              computeNitscheDpoint_Sigma_v(0., -side * alpha * penaltyParam1 * RNParam);
   
              // + \sigma( u^{\star}, p^{\star})n_i . v_i
              for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
                // velocity
                if(m_useSymmetricStress)
                  m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity * m_betaRN[i] * alpha * penaltyParam2 , m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());
                else
                  m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity * m_betaRN[i] * alpha * penaltyParam2, m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());

                // pressure
                for(felInt idim=0; idim<dimension(); ++idim)
                  m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0) * m_betaRN[i] * alpha * penaltyParam2, *m_feVel, m_seqPreDofPts[i], idim, 1);
              }

              // - \sigma( u^{\star}, p^{\star})n_i . \sigma(0, -q_i)n_i
              for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
                std::vector<double> vec(dimension());
                for(felInt idim=0; idim<dimension(); ++idim){
                  for(felInt j=0; j<dimension(); ++j){
                    vec[j] = 2 * m_elemFieldNormal.val(j,0) * m_elemFieldNormal.val(idim,0);
                  }
                  m_elemFieldAux.setValue(vec);
                  if(m_useSymmetricStress)
                    m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(2. * m_viscosity * m_betaRN[i] * alpha * penaltyParam1 , m_elemFieldAux, m_seqVelDofPts[i], *m_feVel, dimension(), 1);
                  else
                    m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i( m_viscosity * m_betaRN[i] * alpha * penaltyParam1 , m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, dimension(), 1);
                }
                m_elementVector[0]->source(- m_betaRN[i] * alpha * penaltyParam1, *m_feVel, m_seqPreDofPts[i], dimension(), 1);
              }


              break;
            }

            case 3: {
              ///////////////////////////////
              // stabilize explicit scheme //
              ///////////////////////////////

              // update the value of the solid velocity (std::set m_strucVelDofPts and m_strucVelQuadPts)
              updateStructureVel(elemId, externalVec(0));
              updateSubStructureVel(m_itfPts, m_subItfPts);
              
              if(FelisceParam::instance().usePenaltyFreeNitscheMethod) {
                double pressureStabilization = 1./FelisceParam::instance().NitschePenaltyParam;

                /*** MATRIX ***/
                // Nitsche's consistency terms, \sigma(v_i, q_i)n_i . u_i - \sigma(u_i, p_i)n_i . v_i
                computeNitscheSigma_u_v(side * m_viscosity, -side);
                computeNitscheSigma_v_u(-side * m_viscosity, -side);

                // stabilization of the coupling with the structure
                // m_elementMat[0]->phi_i_phi_j(-pressureStabilization * m_feVel->diameter() / m_viscosity, *m_fePres, dimension(), dimension(), 1);
                computeNitscheSigma_u_Sigma_v(pressureStabilization * m_feVel->diameter() / m_viscosity);

                /*** RHS ***/
                // -ddot \sigma(v_i, q_i) n_i //
                computeNitscheDpoint_Sigma_v(-side * m_viscosity, -side);
                
                // stabilization of the coupling with the structure
                // m_elementVector[0]->source(-pressureStabilization * m_feVel->diameter() / m_viscosity, *m_fePres, m_seqPreDofPts[0], dimension(), 1);
                computeNitscheSigma_uold_Sigma_v(pressureStabilization * m_feVel->diameter() / m_viscosity);
              } else {
                double pressureStabilization = 1.;
                double penaltyParam = m_viscosity*FelisceParam::instance().NitschePenaltyParam/m_feVel->diameter();

                /*** MATRIX ***/
                // Nitsche's penalty term, \gamma \mu / h u_i . v_i
                m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, 0, 0, dimension());
                
                // Nitsche's consistency terms, - \sigma(v_i, -q_i)n_i . u_i
                computeNitscheSigma_v_u(0., -side);
                
                // stabilization of the coupling with the structure
                m_elementMat[0]->phi_i_phi_j(-pressureStabilization/penaltyParam, *m_fePres, dimension(), dimension(), 1);

                /*** RHS ***/
                // penalty term, \gamma \mu / h \dot{d}^n v_i
                m_elementVector[0]->source(penaltyParam, *m_feVel, m_strucVelQuadPts, 0, dimension());

                // ddot \sigma(v_i, -q_i) n_i //
                computeNitscheDpoint_Sigma_v(0., -side);

                // stabilization of the coupling with the structure
                m_elementVector[0]->source(-pressureStabilization/penaltyParam, *m_fePres, m_seqPreDofPts[0], dimension(), 1);
              

                // RHS //
                // Nitsche's consistency term in the RHS \sigma(u_i^{n-1}, p_i^{n-1}n_i . v_i
                // velocity
                if(m_useSymmetricStress)
                  m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());
                else
                  m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());

                // pressure
                for(felInt idim=0; idim<dimension(); ++idim)
                  m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0), *m_feVel, m_seqPreDofPts[0], idim, 1);
              }

              break;
            }

            case 4: {
              // Robin-Neumann semi-implict schemes (0th, 1st and 2nd order extrapolated variants)
              // MATRIX
              // Nitsche's penalty term, \gamma \nu / h u_i . v_i
              double pressureStabilization = 1.;
              double penaltyParam = m_viscosity*FelisceParam::instance().NitschePenaltyParam/m_feVel->diameter();
              m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, 0, 0, dimension());

              // Nitsche's consistency terms, -\sigma(u_i, p_i)n_i . v_i - \sigma(v_i, q_i)n_i . u_i
              // u_i & v_i
              computeNitscheSigma_v_u(0., -side);

              // stabilization of the coupling with the structure
              m_elementMat[0]->phi_i_phi_j(-pressureStabilization/penaltyParam, *m_fePres, dimension(), dimension(), 1);

              // RHS //
              // Nitsche's consistency term in the RHS //
              // velocity
              if(m_useSymmetricStress)
                m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());
              else
                m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());

              // pressure
              for(felInt idim=0; idim<dimension(); ++idim)
                m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0), *m_feVel, m_seqPreDofPts[0], idim, 1);


              // RHS //
              // Nitsche's consistency term in the RHS //

              // update the value of the solid velocity (std::set m_strucVelDofPts and m_strucVelQuadPts)
              // m_strucVelDofPts.val.clear();
              updateStructureVel(elemId, m_pLinStruc->sequentialSolution());

              // m_strucVelQuadPts.val.clear();
              updateSubStructureVel(m_itfPts, m_subItfPts);

              // penalty term (RHS part)
              m_elementVector[0]->source(penaltyParam, *m_feVel, m_strucVelQuadPts, 0, dimension());

              // ddot \sigma(v_i, q_i) n_i //
              // computeNitscheDpoint_Sigma_v(side * m_viscosity, -side);
              computeNitscheDpoint_Sigma_v(0., -side);

              // stabilization of the coupling with the structure
              m_elementVector[0]->source(-pressureStabilization/penaltyParam, *m_fePres, m_seqPreDofPts[0], dimension(), 1);
              break;
            }

            default: {
              FEL_ERROR("Bad Time splitting scheme");
              break;
            }
          }
        }
      }
    } else {

      // -------------------------------------------
      // Get the support element of the old solutions
      std::vector<felInt> ielOld(m_solutionOld.size());
      std::vector<felInt> ielOldDup(m_solutionOld.size());
      std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
      for(std::size_t i=0; i<oldSupportElement.size(); ++i) {
        m_supportDofUnknownOld[i][0].getIdElementSupport(ielGeo, oldSupportElement[i]);
      
        // std::set the id for the old solution
        if(oldSupportElement[i].size() > 1) {
          // this element is also duplicated for the old solution
          if(sideOfSupportElementForSolution == LEFT) {
            ielOld[i] = oldSupportElement[i][0];
            ielOldDup[i] = oldSupportElement[i][1];
          } else {
            ielOld[i] = oldSupportElement[i][1];
            ielOldDup[i] = oldSupportElement[i][0];
          }
        } else {
          // this element is NOT duplicated for the old solution
          ielOld[i] = oldSupportElement[i][0];
          ielOldDup[i] = oldSupportElement[i][0];
        }
      }

      ////////////////////////////
      // SUB STRUCTURE ELEMENTS //
      ////////////////////////////
      felInt numSubElement = m_duplicateSupportElements->getNumSubItfEltPerMshElt(ielGeo);
      if(numSubElement > 0) {
        felInt subElemId;
        felInt elemId;
        std::vector<double> tmpPt(3);
        std::vector<double> itfNormal(dimension(),0);
        std::vector<double> itfNormalInit(dimension(),0);

        // std::set the extrapolation of velocity
        m_elemFieldAdv.val.clear();
        m_elemFieldAdvDup.val.clear();
        if(FelisceParam::instance().NSequationFlag == 1) {
          if(FelisceParam::instance().useProjectionForNitscheXFEM) {
            for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel1, m_iVelocity, m_ao, dof());
              m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
              m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
            }
          } else {
            for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
              m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOldDup[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
              m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
            }
          }
        }


        for(felInt iSubElt=0; iSubElt < numSubElement; ++iSubElt) {
          // get the id of the sub element
          subElemId = m_duplicateSupportElements->getSubItfEltIdxMsh(ielGeo, iSubElt);

          // get the id of the element owing this sub element
          elemId = m_duplicateSupportElements->getItfEltIdxOfSubItfElt(subElemId);

          // get the normal to this sub element
          GeometricMeshRegion::ElementType eltTypeStruc = m_pLinStruc->mesh()->bagElementTypeDomain()[0];
          m_pLinStruc->mesh()->listElementNormal(eltTypeStruc, elemId).getCoor(itfNormal);
          m_pLinStruc->mesh()->listElementNormalInit(eltTypeStruc, elemId).getCoor(itfNormalInit);

          // fill the std::vector with the points of the sub-element
          for(std::size_t iPt=0; iPt < m_numVerPerSolidElt; ++iPt) {
            m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
            *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
          }

          // Get the coordinates of the element owning this sub-element
          int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltTypeStruc];
          std::vector<felInt> strucElemPtsId(numPointsPerElt);

          m_pLinStruc->mesh()->getOneElement(eltTypeStruc, elemId, strucElemPtsId, 0);

          for (int iPoint = 0; iPoint < numPointsPerElt; ++iPoint)
            m_itfPts[iPoint] = &m_pLinStruc->mesh()->listPoint(strucElemPtsId[iPoint]);


          // update the finite element
          m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_feStruc);
          m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_feStruc);

          // Set element field
          if(FelisceParam::instance().useProjectionForNitscheXFEM || m_useCurrentFluidSolution > 0) {
            for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
              if ((m_timeSchemeType == 0) || (m_timeSchemeType == 2)){
                m_seqVelDofPts[i].setValue(m_seqRNFluidExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
                m_seqPreDofPts[i].setValue(m_seqRNFluidExtrapol[i], *m_fePres, iel2, m_iPressure, m_ao, dof());
              } else {
                m_seqVelDofPts[i].setValue(sequentialSolution(), *m_feVel, iel2, m_iVelocity, m_ao, dof());
                m_seqPreDofPts[i].setValue(sequentialSolution(), *m_fePres, iel2, m_iPressure, m_ao, dof());
              }
            }
          } else {
            for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
              m_seqVelDofPts[i].setValue(m_sequentialSolutionOld[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
              m_seqPreDofPts[i].setValue(m_sequentialSolutionOld[i], *m_fePres, ielOld[i], m_iPressure, m_aoOld[i], m_dofOld[i]);
            }
          }
          
          m_elemFieldNormal.setValue(itfNormal);
          m_elemFieldNormalInit.setValue(itfNormalInit);
          
          // compute Inflow stabilization for NS equation
          if(FelisceParam::instance().NSequationFlag == 1 && FelisceParam::instance().useInterfaceFlowStab) {
            m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * FelisceParam::instance().density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, 0, 0, dimension(), 0);
            m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * FelisceParam::instance().density, *m_feVel, m_elemFieldAdvDup, m_elemFieldNormal, 0, 0, dimension(), 0);
          }

          // compute the elementary matrix and rhs
          switch(m_timeSchemeType) {
            
            case 0: {
              // Robin-Neumann schemes (0th, 1st and 2nd order extrapolated variants)
              // MATRIX //
              computeNitscheSigma_u_v(-1 * side * m_viscosity,  side);

              // RHS //  
              for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
                // velocity
                if(m_useSymmetricStress)
                  m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity * m_betaRN[i], m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());
                else
                  m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity * m_betaRN[i], m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());

                // pressure
                for(felInt idim=0; idim<dimension(); ++idim)
                  m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0) * m_betaRN[i], *m_feVel, m_seqPreDofPts[i], idim, 1);
              }
              break;
            }

            case 1: {
              break;
            }

            case 2: {
              // Robin-Neumann schemes a la Stenberg (0th, 1st and 2nd order extrapolated variants)

              double alpha = 0.5;    
              double RNParam = FelisceParam::instance().densitySolid * FelisceParam::instance().thickness / m_fstransient->timeStep; 	    
              double penaltyParam1 = (m_feVel->diameter()) / (m_viscosity*FelisceParam::instance().NitschePenaltyParam + RNParam*m_feVel->diameter());
              double penaltyParam2 = (FelisceParam::instance().NitschePenaltyParam*m_viscosity ) / (m_viscosity*FelisceParam::instance().NitschePenaltyParam + RNParam*m_feVel->diameter());

              // MATRIX //

              // + \sigma(u_i, p_i)n_i . v_j
              computeNitscheSigma_u_v(-1 * side * m_viscosity * alpha * penaltyParam2,  side * alpha * penaltyParam2);

              // - \sigma(u_i, p_i)n_i . \sigma(0, -q_i)n_i
              computeStenbergSigma_u_Sigma_v( -m_viscosity * alpha * penaltyParam1 , alpha * penaltyParam1 );

              // RHS //

              // + \sigma( u^{\star}, p^{\star})n_i . v_i  
              for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
                // velocity
                if(m_useSymmetricStress)
                  m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * side * m_viscosity * m_betaRN[i]  * alpha * penaltyParam2 , m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());
                else
                  m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(-side * m_viscosity * m_betaRN[i] * alpha * penaltyParam2, m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, 0, dimension());

                // pressure
                for(felInt idim=0; idim<dimension(); ++idim)
                  m_elementVector[0]->source(side * m_elemFieldNormal.val(idim,0) * m_betaRN[i] * alpha * penaltyParam2, *m_feVel, m_seqPreDofPts[i], idim, 1);
              }

              // - \sigma( u^{\star}, p^{\star})n_i . \sigma(0, -q_i)n_i
              for(std::size_t i=0; i<m_seqVelDofPts.size(); ++i){
                std::vector<double> vec(dimension());
                for(felInt idim=0; idim<dimension(); ++idim){
                  for(felInt j=0; j<dimension(); ++j){
                    vec[j] = 2 * m_elemFieldNormal.val(j,0) * m_elemFieldNormal.val(idim,0);
                  }
                  m_elemFieldAux.setValue(vec);
                  if(m_useSymmetricStress)
                    m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(-2. * m_viscosity * m_betaRN[i] * alpha * penaltyParam1 , m_elemFieldAux, m_seqVelDofPts[i], *m_feVel, dimension(), 1);
                  else
                    m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i( -m_viscosity * m_betaRN[i] * alpha * penaltyParam1 , m_elemFieldNormal, m_seqVelDofPts[i], *m_feVel, dimension(), 1);
                }
                m_elementVector[0]->source(m_betaRN[i] * alpha * penaltyParam1, *m_feVel, m_seqPreDofPts[i], dimension(), 1);
              }

              break;
            }

            case 3:{
              break;
            }

            case 4:{
              break;
            }
          
            default: {
              FEL_ERROR("Bad Time splitting scheme");
              break;
            }
          }
        }
      }
    }
  }



  void LinearProblemFSINitscheXFEMFluid::assembleMatrixFrontPoints() {
    // Assemble the matrix to take the crack point into account
    // Enforce continuity accros the fictitious interface
    felInt nbrVerFro = m_duplicateSupportElements->getNumFrontPoints();

    if(nbrVerFro > 0) {
      std::vector<felInt> verFro;
      std::vector<felInt> verFro_temp; // generic front vertex std::vector for checking multi fronts within element
      felInt idElem;
      GeometricMeshRegion::ElementType eltType = m_mesh[m_currentMesh]->bagElementTypeDomain()[0];

      felInt numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      std::vector<Point*>  elemPoint(numPointPerElt);
      std::vector<felInt>  elemIdPoint(numPointPerElt, 0);

      std::vector<felInt> vectorIdSupport;
      std::vector<double> itfNormal(dimension(), 0.);

      // define finite element
      defineFiniteElement(eltType);
      initElementArray();

      // allocate arrays for assembling the matrix
      FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;
      allocateArrayForAssembleMatrixRHS(flag);

      // init variables
      initPerElementType(eltType, flag);

      for(felInt iVer=0; iVer<nbrVerFro; ++iVer) {
        // get the info on the front point
        m_duplicateSupportElements->getFrontPointInfo(iVer, verFro);

        // get the fluid element in which the fictitious interface is
        idElem = verFro[1];

        // get the support elements
        setElemPoint(eltType, idElem, elemPoint, elemIdPoint, vectorIdSupport);

        // fill the std::vector with the points of the fictitious interface
        *m_subItfPts[0] = Point(m_pLinStruc->mesh()->listPoint(verFro[0]));
        *m_subItfPts[1] = Point(m_mesh[m_currentMesh]->listPoint(verFro[2]));

      	// now check if two fronts are in same element--then opposite vertex is other front)
      	for(felInt iVer2=0; iVer2<nbrVerFro; ++iVer2) {
      	  if(iVer2!=iVer){
            m_duplicateSupportElements->getFrontPointInfo(iVer2, verFro_temp);
            if(verFro[1]==verFro_temp[1]){
              *m_subItfPts[1] = Point(m_pLinStruc->mesh()->listPoint(verFro[2]));
            }
          }
      	}
        // special initial case for pre-determined crack point when nbrVerFro is 1 for one crack initially with     the crack point being the same
        // NOTE THIS CURRENTLY ONLY WORKS FOR ONE CRACK IN A SOLID
        if(FelisceParam::instance().hasCrack && nbrVerFro==1) {
    	    *m_subItfPts[1] = Point(m_pLinStruc->mesh()->listPoint(verFro[2]));
        }


	
        // update the finite element
        m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_feStruc);
        m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_feStruc);

        // compute the normal
        itfNormal[0] = -(m_subItfPts[1]->y() - m_subItfPts[0]->y());
        itfNormal[1] = m_subItfPts[1]->x() - m_subItfPts[0]->x();

        double norm = std::sqrt(itfNormal[0]*itfNormal[0] + itfNormal[1]*itfNormal[1]);
        itfNormal[0] /= norm;
        itfNormal[1] /= norm;
        m_elemFieldNormal.setValue(itfNormal);

        // parameters
        double penaltyParam = m_viscosity*FelisceParam::instance().NitschePenaltyParam/m_feVel->diameter();

        // loop over the support elements
        for(std::size_t iSup1=0; iSup1 < vectorIdSupport.size(); ++iSup1) {
          felInt iel1 = vectorIdSupport[iSup1];
          for(std::size_t iSup2=0; iSup2 < vectorIdSupport.size(); ++iSup2) {
            felInt iel2 = vectorIdSupport[iSup2];

            // clear elementary matrix
            for (std::size_t j = 0, size = numberOfMatrices(); j < size ; j++)
              m_elementMat[j]->zero();
            
            
            // get the side
            sideOfInterface sideOfSupportElementForSolution;
            double side;
            sideOfSupportElementForSolution = iSup2 == 0 ? LEFT : RIGHT;
            side = sideOfSupportElementForSolution == LEFT ? 1. : -1.;

            // std::set the extrapolation of velocity
            if(FelisceParam::instance().NSequationFlag == 1 && FelisceParam::instance().useInterfaceFlowStab) {
              // Get the support element of the old solutions
              std::vector<felInt> ielOld(m_solutionOld.size());
              std::vector<felInt> ielOldDup(m_solutionOld.size());
              std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
              for(std::size_t i=0; i<oldSupportElement.size(); ++i) {
                m_supportDofUnknownOld[i][0].getIdElementSupport(idElem, oldSupportElement[i]);
                
                // std::set the id for the old solution
                if(oldSupportElement[i].size() > 1) {
                  // this element is also duplicated for the old solution
                  if(sideOfSupportElementForSolution == LEFT) {
                    ielOld[i] = oldSupportElement[i][0];
                    ielOldDup[i] = oldSupportElement[i][1];
                  } else {
                    ielOld[i] = oldSupportElement[i][1];
                    ielOldDup[i] = oldSupportElement[i][0];
                  }
                } else {
                  // this element is NOT duplicated for the old solution
                  ielOld[i] = oldSupportElement[i][0];
                  ielOldDup[i] = oldSupportElement[i][0];
                }
              }
              

              m_elemFieldAdv.val.clear();
              m_elemFieldAdvDup.val.clear();
              if(FelisceParam::instance().useProjectionForNitscheXFEM) {
                for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel1, m_iVelocity, m_ao, dof());
                  m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
                  m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                }
              } else {
                for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                  m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOldDup[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                  m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                }
              }
            }
            
            if(iSup1 == iSup2) {
              // enforce the continuity
              // Nitsche's penalty term, \gamma \nu / h u_i . v_i
              m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, 0, 0, dimension());

              // Nitsche's consistency terms, -\sigma(u_i, p_i)n_i . v_i - \sigma(v_i, q_i)n_i . u_i
              // u_i & v_i
              computeNitscheSigma_u_v(0.5 * side * m_viscosity, -0.5 * side);
              computeNitscheSigma_v_u(0.5 * side * m_viscosity, -0.5 * side);
              
              // stabilize inflow
              if(FelisceParam::instance().NSequationFlag == 1 && FelisceParam::instance().useInterfaceFlowStab){
                // (0.5 \rho_f \beta_1 \cdot n_2) (u_1 \cdot v_1)-(0.5 \rho_f \beta_2 \cdot n_2) (u_2 \cdot v_2)
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.5 * side * FelisceParam::instance().density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, 0, 0, dimension(), 0);
              }
            } else {
              m_elementMat[0]->phi_i_phi_j(-penaltyParam, *m_feVel, 0, 0, dimension());
              
              computeNitscheSigma_u_v(-0.5 * side * m_viscosity,  0.5 * side);
              computeNitscheSigma_v_u( 0.5 * side * m_viscosity, -0.5 * side);

              // stabilize inflow
              if(FelisceParam::instance().NSequationFlag == 1 && FelisceParam::instance().useInterfaceFlowStab) {
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * FelisceParam::instance().density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, 0, 0, dimension(), 0);
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * FelisceParam::instance().density, *m_feVel, m_elemFieldAdvDup, m_elemFieldNormal, 0, 0, dimension(), 0);
              }
            }
            
            // add values of elemMat in the global matrix: matrix(0).
            setValueMatrixRHS(vectorIdSupport[iSup1], vectorIdSupport[iSup2], flag);
          }
        }
      }

      // desallocate array use for assemble with petsc.
      desallocateArrayForAssembleMatrixRHS(flag);
    }
  }




  void LinearProblemFSINitscheXFEMFluid::assembleMatrixGhostPenalty() {
    // ------------- //
    // Ghost penalty //
    // ------------- //

    // get the types of elements
    GeometricMeshRegion::ElementType eltType = m_meshLocal[m_currentMesh]->bagElementTypeDomain()[0];
    felInt numEdgePerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numEdge();
    felInt numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];;
    std::vector<felInt> idOfFacesCurrent(numEdgePerType, -1);
    std::vector<felInt> idOfFacesOpposite(numEdgePerType, -1);

    // define finite element
    defineFiniteElement(eltType);
    initElementArray();
    defineCurrentFiniteElementWithBd(eltType);

    // allocate arrays for assembling the matrix
    FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;
    allocateArrayForAssembleMatrixRHS(flag);

    // init variables
    initPerElementType(eltType, flag);
    m_feVelWithBd = m_listCurrentFiniteElementWithBd[m_iVelocity];

    // get global supportDofMesh informations
    std::vector<felInt>& iEle = m_supportDofUnknown[0].iEle();
    std::vector<felInt>& iSupportDof = m_supportDofUnknown[0].iSupportDof();

    // element informations
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;
    std::vector<felInt> ielCurrentSupportElt;
    std::vector<felInt> ielOppositeSupportElt;
    std::vector<Point*> elemCurrentPoint(m_numVerPerFluidElt);
    std::vector<felInt> elemCurrentIdPoint(m_numVerPerFluidElt);
    std::vector<Point*> elemOppositePoint(m_numVerPerFluidElt);
    std::vector<felInt> elemOppositeIdPoint(m_numVerPerFluidElt);
    felInt currentIelGeo;
    felInt currentGlobalIelGeo;
    felInt oppositeGlobalIelGeo;
    felInt idCurrentSupElt;
    felInt idOppositeSupElt;

    // loop over all the intersected elements
    auto& idInterElt = m_duplicateSupportElements->getIntersectedMshElt();
    for (auto ielIt = idInterElt.begin(); ielIt != idInterElt.end() && ielIt->first < meshLocal()->numElements(eltType); ++ielIt) {
      // get the geometric id of the element and the ids of its support elements
      currentIelGeo = ielIt->first;
      ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &currentIelGeo, &currentGlobalIelGeo);

      // get all the faces of the element
      if(dimension() == 2)
        m_mesh[m_currentMesh]->getAllEdgeOfElement(currentGlobalIelGeo, idOfFacesCurrent);
      else
        m_mesh[m_currentMesh]->getAllFaceOfElement(currentGlobalIelGeo, idOfFacesCurrent);

      // get informations on the current element
      setElemPoint(eltType, currentIelGeo, elemCurrentPoint, elemCurrentIdPoint, ielCurrentSupportElt);

      // for each support elements
      for(std::size_t iSup=0; iSup<ielCurrentSupportElt.size(); ++iSup) {
        sideOfInterface side = iSup == 0 ? LEFT : RIGHT;
        idCurrentSupElt = ielCurrentSupportElt[iSup];

        // loop over all the edges of the current support element
        for(std::size_t iface=0; iface < idOfFacesCurrent.size(); ++iface) {
          bool computeGhostPenaltyOnThisEdge = false;
          bool computeBothSide = false;
          oppositeGlobalIelGeo = currentGlobalIelGeo;
          
          // get the neighboor. If it exists, this is an inner edge
          // first = true if there is an adjacent element
          // second = the ElementType of the adjacent element if first is true
          adjElt = m_mesh[m_currentMesh]->getAdjElement(oppositeGlobalIelGeo, iface);
          
          if(adjElt.first) {
            // This edge is an inner edge
            // get information on the opposite element
            m_supportDofUnknown[0].getIdElementSupport(oppositeGlobalIelGeo, ielOppositeSupportElt);
            m_mesh[m_currentMesh]->getOneElement(oppositeGlobalIelGeo, elemOppositeIdPoint);
            for (felInt iPoint=0; iPoint<numPointPerElt; ++iPoint)
              elemOppositePoint[iPoint] = &(m_mesh[m_currentMesh]->listPoints()[elemOppositeIdPoint[iPoint]]);


            idOppositeSupElt = ielOppositeSupportElt[0];
            if(ielOppositeSupportElt.size() == 2) {
              // the other element is also duplicated, take the one on the same side
              computeGhostPenaltyOnThisEdge = true;
              idOppositeSupElt = side == LEFT ? ielOppositeSupportElt[0] : ielOppositeSupportElt[1];
            } else {
              // the other element is not duplicated, nothing to do if it is not on the same side
              std::size_t sizeCurrent = m_supportDofUnknown[0].getNumSupportDof(idCurrentSupElt);
              std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(idOppositeSupElt);

              felInt countSupportDofInCommon = 0;
              for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2) {
                  if(iSupportDof[iEle[ielOppositeSupportElt[0]] + iSup2] == iSupportDof[iEle[idCurrentSupElt] + iSup1]) {
                    ++countSupportDofInCommon;
                  }
                }
              }
              
              if(countSupportDofInCommon > 1) {
                // the elements are on the same side
                idOppositeSupElt = ielOppositeSupportElt[0];
                computeGhostPenaltyOnThisEdge = true;
                computeBothSide = true;
              }
            }

            // add the contribution of this edge
            if(computeGhostPenaltyOnThisEdge) {
              // finite element with boundary for the opposite element
              const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
              felInt typeOfFiniteElement = m_velocity->finiteElementType();
              const RefElement *refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *m_meshLocal[m_currentMesh]);
              CurrentFiniteElementWithBd oppositeEltFEWithBd(*refEle, *geoEle, m_velocity->degreeOfExactness(), m_velocity->degreeOfExactness());


              // update the finite elements
              m_feVelWithBd->updateFirstDeriv(0, elemCurrentPoint);
              m_feVelWithBd->updateBdMeasNormal();
              oppositeEltFEWithBd.updateFirstDeriv(0, elemOppositePoint);
              oppositeEltFEWithBd.updateBdMeasNormal();


              // find the local id of the edge in the opposite element
              felInt localIdFaceOpposite = -1;
              if(dimension() == 2)
                m_mesh[m_currentMesh]->getAllEdgeOfElement(eltType, oppositeGlobalIelGeo, idOfFacesOpposite);
              else
                m_mesh[m_currentMesh]->getAllFaceOfElement(eltType, oppositeGlobalIelGeo, idOfFacesOpposite);
              
              for(std::size_t jface=0; jface<idOfFacesOpposite.size(); ++jface) {
                if(idOfFacesCurrent[iface] == idOfFacesOpposite[jface]) {
                  localIdFaceOpposite = jface;
                }
              }

              // std::set coefficient of the ghost penalty
              double gammag = FelisceParam::instance().coefGhostPenalty * m_viscosity * m_feVelWithBd->bdEle(iface).diameter();

              // ----------------------------------- //
              // current element for phi_i and phi_j //
              // ----------------------------------- //
              m_elementMat[0]->zero();
              m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, *m_feVelWithBd, *m_feVelWithBd, iface, iface, 0, 0, dimension());
              setValueMatrixRHS(idCurrentSupElt, idCurrentSupElt, flag);

              // -------------------------------------------------------- //
              // current element for phi_i and opposite element for phi_j //
              // -------------------------------------------------------- //
              m_elementMat[0]->zero();
              m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, oppositeEltFEWithBd, *m_feVelWithBd, localIdFaceOpposite, iface, 0, 0, dimension());
              setValueMatrixRHS(idCurrentSupElt, idOppositeSupElt, flag);

              // if the opposite element is not duplicated, it will not be handle in this loop
              // compute all the term in this case
              if(computeBothSide) {
                // -------------------------------------------------------- //
                // opposite element for phi_i and current element for phi_j //
                // -------------------------------------------------------- //
                m_elementMat[0]->zero();
                m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, *m_feVelWithBd, oppositeEltFEWithBd, iface, localIdFaceOpposite, 0, 0, dimension());
                setValueMatrixRHS(idOppositeSupElt, idCurrentSupElt, flag);

                // ------------------------------------ //
                // opposite element for phi_i and phi_j //
                // ------------------------------------ //
                m_elementMat[0]->zero();
                m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, oppositeEltFEWithBd, oppositeEltFEWithBd, localIdFaceOpposite, localIdFaceOpposite, 0, 0, dimension());
                setValueMatrixRHS(idOppositeSupElt, idOppositeSupElt, flag);
              }
            }
          }
        }
      }
    }

    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flag);
  }



  void LinearProblemFSINitscheXFEMFluid::assembleMatrixFaceOriented() {
    // Use Face-oriented stabilization
    // E. Burman, M. A. Fernandez and P. Hansbo, Continuous interior penalty finite element method for
    // Oseen's equations, 2006.
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;

    GeometricMeshRegion::ElementType eltType;     // Type of element
    GeometricMeshRegion::ElementType eltTypeOpp;  // Type of element

    bool addOppositeSupportElt;                // true if we need to compute the stabilization on this face
    
    felInt ielCurrentLocalGeo = 0;             // local geometric id of the current element
    felInt ielCurrentGlobalGeo = 0;            // global geometric id of the current element
    felInt ielOppositeGlobalGeo = 0;           // global geometric id of the opposite element
    felInt numFacesPerType;                    // number of faces of an element of a given type
    felInt numEltPerLabel;                     // number of element by reference
    felInt numPointPerElt;                     // number of vertices by element
    felInt currentSupport;                     // id of the current support element
    felInt oppositeSupport;                    // id of the opposite support element
    std::vector<felInt> ielCurrentGlobalSup;        // local support element id of the current element
    std::vector<felInt> ielOppositeGlobalSup;       // local support element id of the opposite element

    std::vector<felInt> idOfFacesCurrent;           // ids of the current element edges
    std::vector<felInt> idOfFacesOpposite;          // ids of the opposite element edges
    std::vector<felInt> currentElemIdPoint;         // ids of the vertices of the current element
    std::vector<felInt> oppositeElemIdPoint;        // ids of the vertices of the opposite element 
    std::vector<Point*> currentElemPoint;           // point coordinates of the current element vertices
    std::vector<Point*> oppositeElemPoint;          // point coordinates of the opposite element vertices
    std::vector<felInt> countElement;               // count the element by type

    FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;     // flag to only assemble the matrix

    ElementField elemFieldAdvFace;
    ElementField elemFieldAdvFaceTmp;

    const std::vector<felInt>& iSupportDof = m_supportDofUnknown[0].iSupportDof();
    const std::vector<felInt>& iEle = m_supportDofUnknown[0].iEle();

    // resize std::vector
    countElement.resize(GeometricMeshRegion::m_numTypesOfElement, 0);

    // volume bag
    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();


    // finite element with bd for the opposite element
    std::unordered_map<GeometricMeshRegion::ElementType, CurrentFiniteElementWithBd*> oppositeEltFEWithBdVel;
    std::unordered_map<GeometricMeshRegion::ElementType, CurrentFiniteElementWithBd*> oppositeEltFEWithBdPre;

    for (std::size_t i=0; i<bagElementTypeDomain.size(); ++i) {
      eltType =  bagElementTypeDomain[i];
      
      m_velocity = &m_listVariable[m_listVariable.getVariableIdList(velocity)];
      m_pressure = &m_listVariable[m_listVariable.getVariableIdList(pressure)];
      const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;

      felInt typeOfFEVel = m_velocity->finiteElementType();
      const RefElement *refEleVel = geoEle->defineFiniteEle(eltType, typeOfFEVel, *m_mesh[m_currentMesh]);
      oppositeEltFEWithBdVel[eltType] = new CurrentFiniteElementWithBd(*refEleVel, *geoEle, m_velocity->degreeOfExactness(), m_velocity->degreeOfExactness());

      felInt typeOfFEPre = m_pressure->finiteElementType();
      const RefElement *refElePre = geoEle->defineFiniteEle(eltType, typeOfFEPre, *m_mesh[m_currentMesh]);
      oppositeEltFEWithBdPre[eltType] = new CurrentFiniteElementWithBd(*refElePre, *geoEle, m_pressure->degreeOfExactness(), m_pressure->degreeOfExactness());
    }


    // ----------------------------
    // First loop on geometric type
    for (std::size_t itype=0; itype<bagElementTypeDomain.size(); ++itype) {
      // get element type and number of vertices and "faces"
      eltType =  bagElementTypeDomain[itype];
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      numFacesPerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

      // resize of vectors
      idOfFacesCurrent.resize(numFacesPerType, -1);
      idOfFacesOpposite.resize(numFacesPerType, -1);
      currentElemIdPoint.resize(numPointPerElt, -1);
      oppositeElemIdPoint.resize(numPointPerElt, -1);
      currentElemPoint.resize(numPointPerElt, nullptr);
      oppositeElemPoint.resize(numPointPerElt, nullptr);

      // define finite element
      defineFiniteElement(eltType);
      initElementArray();
      defineCurrentFiniteElementWithBd(eltType);

      // allocate arrays for assembling the matrix
      allocateArrayForAssembleMatrixRHS(flag);

      // init variables
      initPerElementType(eltType, flag);
      m_feVelWithBd = m_listCurrentFiniteElementWithBd[m_iVelocity];
      m_fePreWithBd = m_listCurrentFiniteElementWithBd[m_iPressure];


      // ---------------------------------
      // second loop on region of the mesh
      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        // get the number of element with this reference
        numEltPerLabel = itRef->second.second;

        // std::set the current label of LinearProblem to the current one of this loop
        initPerDomain(itRef->first, flag);

        // ---------------------
        // third loop on element
        for(felInt iel=0; iel<numEltPerLabel; ++iel) {
          // get the global id of the current geometric element
          ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &ielCurrentLocalGeo, &ielCurrentGlobalGeo);

          // get all the faces of the element
          if(dimension() == 2)
            m_mesh[m_currentMesh]->getAllEdgeOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);
          else
            m_mesh[m_currentMesh]->getAllFaceOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);

          // get informations on the current element
          setElemPoint(eltType, countElement[eltType], currentElemPoint, currentElemIdPoint, ielCurrentGlobalSup);

          // update the finite elements
          m_feVelWithBd->updateFirstDeriv(0, currentElemPoint);
          m_feVelWithBd->updateBdMeasNormal();
          m_fePreWithBd->updateFirstDeriv(0, currentElemPoint);
          m_fePreWithBd->updateBdMeasNormal();


          // loop over the support element of the current geometric element
          for(std::size_t ielSup=0; ielSup<ielCurrentGlobalSup.size(); ++ielSup) {
            currentSupport = ielCurrentGlobalSup[ielSup];

            // get the old support dof
            std::vector< std::vector<felInt> > oldSupportElement(FelisceParam::instance().orderBdfNS);
            std::vector<felInt> ielOld(FelisceParam::instance().orderBdfNS);
            
            for(felInt iextrap=0; iextrap<FelisceParam::instance().orderBdfNS; ++iextrap) {
              // get the old support element
              m_supportDofUnknownOld[iextrap][0].getIdElementSupport(ielCurrentGlobalGeo, oldSupportElement[iextrap]);
              
              // std::set the id for the old solution
              if(oldSupportElement[iextrap].size() > 1)
                ielOld[iextrap] = oldSupportElement[iextrap][ielSup];
              else
                ielOld[iextrap] = oldSupportElement[iextrap][0];
            }

            // compute coefficients
            m_elemFieldAdv.val.clear();
            if(FelisceParam::instance().useProjectionForNitscheXFEM) {
              for(felInt iextrap=0; iextrap<FelisceParam::instance().orderBdfNS; ++iextrap) {
                m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVel, currentSupport, m_iVelocity, m_ao, dof());
                m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              }
            } else {
              for(felInt iextrap=0; iextrap<FelisceParam::instance().orderBdfNS; ++iextrap) {
                m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVel, ielOld[iextrap], m_iVelocity, m_aoOld[iextrap], m_dofOld[iextrap]);
                m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              }
            }
          
            double normVelExtrap = 0;
            for(std::size_t jcol=0; jcol<m_elemFieldAdv.val.size2(); ++jcol)
              normVelExtrap = std::max(normVelExtrap, norm_2(column(m_elemFieldAdv.val, jcol)));
            
            double hK = m_feVelWithBd->diameter();
            double ReK = m_density * normVelExtrap * hK / m_viscosity;
            double coeffVel = FelisceParam::instance().stabFOVel * std::min(1., ReK) * hK * hK;

            double gammaP = 0.;
            if(FelisceParam::instance().NSequationFlag == 0) {
              gammaP   = FelisceParam::instance().stabFOPre * hK * hK * hK / m_viscosity;
            } else {
              double xiPre = ReK < 1 ? m_density * hK * hK * hK / m_viscosity : hK * hK / normVelExtrap;
              gammaP   = FelisceParam::instance().stabFOPre * xiPre;
            }
            
            // --------------------------------------------------------
            // loop over all the faces of the current geometric element
            for(std::size_t iface=0; iface<idOfFacesCurrent.size(); ++iface) {
              // check if this face is an inner face or a boundary
              ielOppositeGlobalGeo = ielCurrentGlobalGeo;
              
              // first = true if there is an adjacent element
              // second = the ElementType of the adjacent element if first is true
              adjElt =m_mesh[m_currentMesh]->getAdjElement(ielOppositeGlobalGeo, iface);
              
              // compute the stabilization only on inner faces
              if(adjElt.first) {
                // get the type of the opposite element
                eltTypeOpp = adjElt.second;
                
                // find the local id of the edge in the opposite element
                felInt localIdFaceOpposite = -1;
                if(dimension() == 2)
                  m_mesh[m_currentMesh]->getAllEdgeOfElement(ielOppositeGlobalGeo, idOfFacesOpposite);
                else
                  m_mesh[m_currentMesh]->getAllFaceOfElement(ielOppositeGlobalGeo, idOfFacesOpposite);
                
                for(std::size_t jface=0; jface<idOfFacesOpposite.size(); ++jface) {
                  if(idOfFacesCurrent[iface] == idOfFacesOpposite[jface]) {
                    localIdFaceOpposite = jface;
                  }
                }
                
                // get information on the opposite element
                // same as the function "setElemPoint" but here ielOppositeGlobalGeo is global
                // we assume that the supportDofMesh is the same for all unknown (like in setElemPoint)
                m_supportDofUnknown[0].getIdElementSupport(ielOppositeGlobalGeo, ielOppositeGlobalSup);
                m_mesh[m_currentMesh]->getOneElement(ielOppositeGlobalGeo, oppositeElemIdPoint);
                for (felInt iPoint=0; iPoint<numPointPerElt; ++iPoint)
                  oppositeElemPoint[iPoint] = &(m_mesh[m_currentMesh]->listPoints()[oppositeElemIdPoint[iPoint]]);
                
                // compare the number of support element
                if(ielCurrentGlobalSup.size() == ielOppositeGlobalSup.size()) {
                  // both are duplicated or both are not. Take the element on the same side
                  oppositeSupport = ielOppositeGlobalSup[ielSup];
                  addOppositeSupportElt = true;
                } else {
                  // One is duplicated, the other is not
                  addOppositeSupportElt = false;
                  for(std::size_t jelSup=0; jelSup < ielOppositeGlobalSup.size() && !addOppositeSupportElt; ++jelSup) {
                    oppositeSupport = ielOppositeGlobalSup[jelSup];
                    
                    // check if this opposite support element is on the same side as the current support element
                    std::size_t sizeCurrent = m_supportDofUnknown[0].getNumSupportDof(currentSupport);
                    std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(oppositeSupport);
                    
                    felInt countSupportDofInCommon = 0;
                    for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                      for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2) {
                        if(iSupportDof[iEle[oppositeSupport] + iSup2] == iSupportDof[iEle[currentSupport] + iSup1]) {
                          ++countSupportDofInCommon;
                        }
                      }
                    }
                    
                    if(countSupportDofInCommon > 1)
                      addOppositeSupportElt = true;
                  }
                }

                // if there really is an opposite support element
                // Even for an inner edge, it is possible that the current support element has no opposite
                // support element (if it is duplicated and the opposite element is not).
                if(addOppositeSupportElt) {
                  // update the opposite finite elements
                  oppositeEltFEWithBdVel[eltTypeOpp]->updateFirstDeriv(0, oppositeElemPoint);
                  oppositeEltFEWithBdVel[eltTypeOpp]->updateBdMeasNormal();
                  oppositeEltFEWithBdPre[eltTypeOpp]->updateFirstDeriv(0, oppositeElemPoint);
                  oppositeEltFEWithBdPre[eltTypeOpp]->updateBdMeasNormal();
              

                  // compute coefficient
                  elemFieldAdvFace.initialize(DOF_FIELD, m_feVelWithBd->bdEle(iface), dimension());
                  elemFieldAdvFaceTmp.initialize(DOF_FIELD, m_feVelWithBd->bdEle(iface), dimension());
                  if(FelisceParam::instance().useProjectionForNitscheXFEM) {
                    for(felInt iextrap=0; iextrap<FelisceParam::instance().orderBdfNS; ++iextrap) {
                      elemFieldAdvFaceTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVelWithBd, iface, currentSupport, m_iVelocity, m_ao, dof());
                      elemFieldAdvFace.val += elemFieldAdvFaceTmp.val;
                    }
                  } else {
                    for(felInt iextrap=0; iextrap<FelisceParam::instance().orderBdfNS; ++iextrap) {
                      elemFieldAdvFaceTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVelWithBd, iface, ielOld[iextrap], m_iVelocity, m_aoOld[iextrap], m_dofOld[iextrap]);
                      elemFieldAdvFace.val += elemFieldAdvFaceTmp.val;
                    }
                  }

                  UBlasVector normalVel = prod(trans(elemFieldAdvFace.val), m_feVelWithBd->bdEle(iface).normal[0]);
                  double normVelBd = norm_inf(normalVel);
                  double gammaU = coeffVel * normVelBd;
                  
                  // We can now compute all the integrals.
                  // There is a minus sign for gammaP because there is one for q div(u).
                  // current element for phi_i and phi_j
                  m_elementMat[0]->zero();
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammaU, *m_feVelWithBd, *m_feVelWithBd, iface, iface, 0, 0, dimension());
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *m_fePreWithBd, *m_fePreWithBd, iface, iface, dimension(), dimension(), 1);
                  // m_elementMat[0]->grad_phi_j_grad_psi_i(-gammaP, *m_fePreWithBd, *m_fePreWithBd, iface, iface, dimension(), dimension(), 1);
                  setValueMatrixRHS(currentSupport, currentSupport, flag);
                  
                  // current element for phi_i and opposite element for phi_j
                  m_elementMat[0]->zero();
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammaU, *oppositeEltFEWithBdVel[eltTypeOpp], *m_feVelWithBd, localIdFaceOpposite, iface, 0, 0, dimension());
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *oppositeEltFEWithBdPre[eltTypeOpp], *m_fePreWithBd, localIdFaceOpposite, iface, dimension(), dimension(), 1);
                  // m_elementMat[0]->grad_phi_j_grad_psi_i(gammaP, oppositeEltFEWithBdPre, *m_fePreWithBd, localIdFaceOpposite, iface, dimension(), dimension(), 1);
                  setValueMatrixRHS(currentSupport, oppositeSupport, flag);
                  
                  // current element for phi_i and opposite element for phi_j
                  m_elementMat[0]->mat() = trans(m_elementMat[0]->mat());
                  setValueMatrixRHS(oppositeSupport, currentSupport, flag);
                  
                  // current element for phi_i and phi_j
                  m_elementMat[0]->zero();
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammaU, *oppositeEltFEWithBdVel[eltTypeOpp], *oppositeEltFEWithBdVel[eltTypeOpp], localIdFaceOpposite, localIdFaceOpposite, 0, 0, dimension());
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *oppositeEltFEWithBdPre[eltTypeOpp], *oppositeEltFEWithBdPre[eltTypeOpp], localIdFaceOpposite, localIdFaceOpposite, dimension(), dimension(), 1);
                  // m_elementMat[0]->grad_phi_j_grad_psi_i(-gammaP, oppositeEltFEWithBdPre, oppositeEltFEWithBdPre, localIdFaceOpposite, localIdFaceOpposite, dimension(), dimension(), 1);
                  setValueMatrixRHS(oppositeSupport, oppositeSupport, flag);          
                }
              }
            }
          }
          ++countElement[eltType];
          ++ielCurrentLocalGeo;
        }
      }
    }

    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flag);
    
    // desallocate opposite finite elements
    for (std::size_t i=0; i<bagElementTypeDomain.size(); ++i) {
      eltType =  bagElementTypeDomain[i];
      delete oppositeEltFEWithBdVel[eltType];
      delete oppositeEltFEWithBdPre[eltType];
    }
  }




  void LinearProblemFSINitscheXFEMFluid::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;

    // call to the function in linear problem that updates the curvilinear finite element
    //LinearProblem::computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
    if(m_curvFeVel->hasOriginalQuadPoint()) {
      m_curvFeVel->updateMeasNormalContra(0, elemPoint);
      m_curvFePress->updateMeasNormalContra(0, elemPoint);
    } else {
      m_curvFeVel->updateBasisAndNormalContra(0, elemPoint);
      m_curvFePress->updateBasisAndNormalContra(0, elemPoint);
    }

    // we change the curvilinear finite element here
    // the integrals will automatically be computed on the physical part of the boundary

    // get the global id of the geometric element
    felInt iel = -1;
    bool isfound = false;
    std::vector<felInt> tmpSupport;
    felInt start = ielSupportDof > m_mesh[m_currentMesh]->getNumElement() - 1 ? m_mesh[m_currentMesh]->getNumElement() - 1 : ielSupportDof;
    for(felInt i=start; i>0 && !isfound; --i) {
      // we start at ielSupportDof (should be close to the geometric id)
      m_supportDofUnknown[0].getIdElementSupport(i, tmpSupport);
      for(std::size_t j=0; j<tmpSupport.size(); ++j) {
        if(tmpSupport[j] == ielSupportDof) {
          iel = i;
          isfound = true;
        }
      }
    }

    if(!isfound) {
      FEL_ERROR("geometric element not found, check the search function");
    }

    // check if this element is intersected
    std::vector< std::vector< Point > > bndElem;
    m_duplicateSupportElements->getSubBndElt(iel, bndElem);

    if(!bndElem.empty()) {
      // get the "side" of the current support element
      sideOfInterface side;
      felInt ielSupport;
      m_supportDofUnknown[0].getIdElementSupport(iel, ielSupport);
      side = ielSupport == ielSupportDof ? LEFT : RIGHT;

      // get the side of the sub elements
      std::vector<sideOfInterface> bndElemSide;
      m_duplicateSupportElements->getSubBndEltSide(iel, bndElemSide);

      // integration over the left or right sub element only
      for(std::size_t iSubElt=0; iSubElt<bndElemSide.size(); ++iSubElt) {
        if(bndElemSide[iSubElt] == side) {
          // get the points defining the sub element
          for(std::size_t iPt=0; iPt < m_numVerPerSolidElt; ++iPt) {
            *m_subItfPts[iPt] = bndElem[iSubElt][iPt];
          }

          // update measure
          m_curvFeVel->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);
          m_curvFePress->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);
        }
      }
    }
  }



  void LinearProblemFSINitscheXFEMFluid::computeNitscheSigma_u_v(double velCoeff, double preCoeff) {
    // \int_K (velCoeff \grad u vec - preCoeff p vec) \cdot v
    if(m_useSymmetricStress)
      m_elementMat[0]->phi_i_eps_phi_j_dot_vec(2.*velCoeff, m_elemFieldNormal, *m_feVel, 0, 0, dimension());
    else
      m_elementMat[0]->phi_i_grad_phi_j_dot_vec(velCoeff, m_elemFieldNormal, *m_feVel, 0, 0, dimension());

    // same finite element for the pressure and the velocity
    for(felInt idim=0; idim<dimension(); ++idim)
      m_elementMat[0]->phi_i_phi_j(preCoeff*m_elemFieldNormal.val(idim,0), *m_feVel, idim, dimension(), 1);
  }


  void LinearProblemFSINitscheXFEMFluid::computeNitscheSigma_v_u(double velCoeff, double preCoeff) {
    // \int_K (velCoeff \grad v vec - preCoeff q vec) \cdot u
    if(m_useSymmetricStress)
      m_elementMat[0]->eps_phi_i_dot_vec_phi_j(2.*velCoeff, m_elemFieldNormal, *m_feVel, 0, 0, dimension());
    else
      m_elementMat[0]->grad_phi_i_dot_vec_phi_j(velCoeff, m_elemFieldNormal, *m_feVel, 0, 0, dimension());

    // same finite element for the pressure and velocity
    for(felInt idim=0; idim<dimension(); ++idim)
      m_elementMat[0]->phi_i_phi_j(preCoeff*m_elemFieldNormal.val(idim,0), *m_feVel, dimension(), idim, 1);
  }


  void LinearProblemFSINitscheXFEMFluid::computeNitscheSigma_u_Sigma_v(double coeff) {
    // same finite element for pressure and velocity
    // eps(u)n . eps(v)n, eps(u)n . qn, -pn . eps(v)n
    if(m_useSymmetricStress) {
      m_elementMat[0]->eps_phi_j_vec_dot_eps_phi_i_vec(4.*m_viscosity*m_viscosity*coeff, m_elemFieldNormal, *m_feVel, 0, 0, dimension());

      for(felInt idim=0; idim<dimension(); ++idim) {
        m_elementMat[0]->phi_i_eps_phi_j_dot_vec(2.*m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, *m_feVel, dimension(), idim, 1);

        m_elementMat[0]->eps_phi_i_dot_vec_phi_j(-2.*m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, *m_feVel, idim, dimension(), 1);
      }
    } else {
      m_elementMat[0]->grad_phi_j_vec_dot_grad_phi_i_vec(m_viscosity*m_viscosity*coeff, m_elemFieldNormal, *m_feVel, 0, 0, dimension());

      for(felInt idim=0; idim<dimension(); ++idim) {
        m_elementMat[0]->phi_i_grad_phi_j_dot_vec(m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, *m_feVel, dimension(), idim, 1);

        m_elementMat[0]->grad_phi_i_dot_vec_phi_j(-m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, *m_feVel, idim, dimension(), 1);
      }
    }

    // -pn . qn = -pq
    m_elementMat[0]->phi_i_phi_j(-coeff, *m_fePres, dimension(), dimension(), 1);
  }




  void LinearProblemFSINitscheXFEMFluid::computeNitscheSigma_uold_Sigma_v(double coeff) {
    // same finite element for pressure and velocity
    // eps(uold)n . eps(v)n, eps(uold)n . qn, -poldn . eps(v)n
    if(m_useSymmetricStress) {
      m_elementVector[0]->eps_vec2_vec1_dot_eps_phi_i_vec1(4.*m_viscosity*m_viscosity*coeff, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());

      for(felInt idim=0; idim<dimension(); ++idim) {
        m_elementVector[0]->eps_vec2_dot_vec1_dot_phi_i(2.*m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, dimension(), 1);

        m_elementVector[0]->eps_phi_i_dot_vec1_dot_vec2(-2.*m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, idim, 1);
      }
    } else {
      m_elementVector[0]->grad_vec2_vec1_dot_grad_phi_i_vec1(m_viscosity*m_viscosity*coeff, m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, 0, dimension());

      for(felInt idim=0; idim<dimension(); ++idim) {
        m_elementVector[0]->grad_vec2_dot_vec1_dot_phi_i(m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, dimension(), 1);

        m_elementVector[0]->grad_phi_i_dot_vec1_dot_vec2(-m_viscosity*coeff*m_elemFieldNormal.val(idim,0), m_elemFieldNormal, m_seqVelDofPts[0], *m_feVel, idim, 1);
      }
    }

    // -poldn . qn = -pold q
    m_elementVector[0]->source(-coeff, *m_fePres, m_seqPreDofPts[0], dimension(), 1);

  }



  void LinearProblemFSINitscheXFEMFluid::computeNitscheDpoint_Sigma_v(double velCoeff, double preCoeff) {
    // \int_K ddot \cdot (velCoeff \grad v vec - preCoeff q vec)
    if(m_useSymmetricStress)
      m_elementVector[0]->eps_phi_i_dot_vec1_dot_vec2(2.*velCoeff, m_elemFieldNormal, m_strucVelQuadPts, *m_feVel, 0, dimension());
    else
      m_elementVector[0]->grad_phi_i_dot_vec1_dot_vec2(velCoeff, m_elemFieldNormal, m_strucVelQuadPts, *m_feVel, 0, dimension());

    for(felInt idim=0; idim<dimension(); ++idim) {
      for(felInt ig=0; ig<m_feVel->numQuadraturePoint(); ++ig) {
        m_ddotComp.val(0, ig) = m_strucVelQuadPts.val(idim, ig);
      }
      m_elementVector[0]->source(preCoeff*m_elemFieldNormal.val(idim,0), *m_feVel, m_ddotComp, dimension(), 1);
    }
  }

  void LinearProblemFSINitscheXFEMFluid::computeStenbergSigma_u_Sigma_v(double velCoeff, double preCoeff) {
    // \int_K (velCoeff \grad u vec - preCoeff p vec) \cdot q vec
    // assuming same finite element for the pressure and the velocity
    std::vector<double> vec(dimension());
    for(felInt idim=0; idim<dimension(); ++idim){
      for(felInt i=0; i<dimension(); ++i){
        vec[i] = 2 * m_elemFieldNormal.val(i,0) * m_elemFieldNormal.val(idim,0);
      }
      m_elemFieldAux.setValue(vec);
      if(m_useSymmetricStress)
        m_elementMat[0]->phi_i_eps_phi_j_dot_vec(2. * velCoeff , m_elemFieldAux, *m_feVel,  dimension(), idim, 1);
      else
        m_elementMat[0]->phi_i_grad_phi_j_dot_vec( velCoeff , m_elemFieldAux, *m_feVel, dimension(), idim, 1);
    }
    m_elementMat[0]->phi_i_phi_j(preCoeff, *m_feVel, dimension() , dimension(), 1);
  }




  void LinearProblemFSINitscheXFEMFluid::setStrucFiniteElement(CurvilinearFiniteElement* strucFE) {
    m_feStruc = strucFE;
  }

  void LinearProblemFSINitscheXFEMFluid::setStrucLinPrb(LinearProblem* pLinPrb) {
    m_pLinStruc = pLinPrb;
  }





  void LinearProblemFSINitscheXFEMFluid::updateStructureVel(felInt strucId, PetscVector& strucVel) {
    felInt idSupportEltGlobal;
    felInt idDof;
    felInt idUnknown = m_pLinStruc->listUnknown().getUnknownIdList(displacement);
    felInt idVar = m_pLinStruc->listUnknown().idVariable(idUnknown);

    // get the global support element
    // there should be only one for the structure
    m_pLinStruc->supportDofUnknown(idUnknown).getIdElementSupport(strucId, idSupportEltGlobal);

    // loop over all the support dof of this support element
    for (felInt iSupport = 0; iSupport < m_pLinStruc->supportDofUnknown(idUnknown).getNumSupportDof(idSupportEltGlobal); iSupport++) {
      // loop over the components
      // for(std::size_t iComp=0; iComp<m_pLinStruc->meshLocal()->numCoor(); ++iComp){
      for(std::size_t iComp=0; iComp<m_pLinStruc->listVariable()[idVar].numComponent(); ++iComp) {
        // local to global id of the dof
        m_pLinStruc->dof().loc2glob(idSupportEltGlobal, iSupport, idVar, iComp, idDof);

        // petsc ordering
        AOApplicationToPetsc(m_pLinStruc->ao(), 1, &idDof);

        // get values
        strucVel.getValues(1, &idDof, &m_strucVelDofPts.val(iComp, iSupport));
      }
    }
  }

  void LinearProblemFSINitscheXFEMFluid::updateSubStructureVel(const std::vector<Point*>& ptElem, const std::vector<Point*>& ptSubElem) {
    FEL_ASSERT(ptElem.size() == 2);

    // compute the value of the displacement at the new integration points
    // get the coordinate of the integration point in the sub element
    m_feStruc->update(0, ptSubElem);
    m_feStruc->computeCurrentQuadraturePoint();

    // compute the coordinate of these integration points in the reference element with the mapping of the
    // structure element (not the structure sub element)
    std::vector<Point> intPoints(m_feStruc->numQuadraturePoint());
    double dx, dy;
    dx = std::abs(ptElem[1]->x() - ptElem[0]->x());
    dy = std::abs(ptElem[1]->y() - ptElem[0]->y());
    if(dx >= dy) {
      for(int ig=0; ig<m_feStruc->numQuadraturePoint(); ++ig) {
        intPoints[ig][0] = 2.*(m_feStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/(ptElem[1]->x() - ptElem[0]->x()) - 1.;
        intPoints[ig][1] = 0.;
      }
    } else {
      for(int ig=0; ig<m_feStruc->numQuadraturePoint(); ++ig) {
        intPoints[ig][0]= 2.*(m_feStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/(ptElem[1]->y() - ptElem[0]->y()) - 1.;
        intPoints[ig][1] = 0;
      }
    }

    // update the structure finite element with the element (really needed ? we only want to access the basis
    // function directly to evaluate them at the computed integration points).
    m_feStruc->update(0, ptElem);


    // Now we can evaluate the basis function of the reference element at the integration points of
    // the sub element
    if(m_strucVelDofPts.numComp() == 1) {
      // The solid is a scalar function. The displacement is in the normal direction.
      for(int icomp=0; icomp<m_feVel->numCoor(); ++icomp) {
        for(int ig=0; ig<m_feStruc->numQuadraturePoint(); ++ig) {
          m_strucVelQuadPts.val(icomp, ig) = 0;
          for(int idof=0; idof<m_feStruc->numDof(); ++idof) {
            m_strucVelQuadPts.val(icomp, ig) += m_strucVelDofPts.val(0, idof) * m_feStruc->refEle().basisFunction().phi(idof, intPoints[ig]) * m_elemFieldNormalInit.val(icomp, 0);
          }
        }
      }
    } else {
      // The solid is a std::vector.
      for(int icomp=0; icomp<m_feVel->numCoor(); ++icomp) {
        for(int ig=0; ig<m_feStruc->numQuadraturePoint(); ++ig) {
          m_strucVelQuadPts.val(icomp, ig) = 0;
          for(int idof=0; idof<m_feStruc->numDof(); ++idof) {
            m_strucVelQuadPts.val(icomp, ig) += m_strucVelDofPts.val(icomp, idof) * m_feStruc->refEle().basisFunction().phi(idof, intPoints[ig]);
          }
        }
      }
    }
  }


  void LinearProblemFSINitscheXFEMFluid::computeElementMatrixRHS() {
    // mass coefficient
    double coef = m_density/m_fstransient->timeStep;

    //================================
    //            matrix
    //================================

    // Laplacian operator
    if (m_useSymmetricStress) {
      // \mu \int \epsilon u^{n+1} : \epsilon v
      m_elementMat[0]->eps_phi_i_eps_phi_j(2*m_viscosity,*m_feVel, 0, 0, dimension());
    } else {
      // \mu \int \nabla u^{n+1} : \nabla v
      m_elementMat[0]->grad_phi_i_grad_phi_j(m_viscosity, *m_feVel, 0, 0, dimension());
    }

    // div and grad operator
    // \int q \nabla  \cdot u^{n+1} and \int p \nabla  \cdot v
    m_elementMat[0]->psi_j_div_phi_i_and_psi_i_div_phi_j (*m_feVel, *m_fePres, 0, m_velocity->numComponent(), -1, -1);

    // mass matrix from the time scheme
    // \int u^{n+1} \cdot v
    m_elementMat[0]->phi_i_phi_j(m_bdf->coeffDeriv0()*coef, *m_feVel, 0, 0, dimension());
  }


  void LinearProblemFSINitscheXFEMFluid::computeElementMatrixRHSPartDependingOnOldTimeStep(felInt iel, felInt ielOld, felInt idTimeStep) {
    // --------------- //
    // convective term //
    // --------------- //
    switch(FelisceParam::instance().NSequationFlag) {
    case 0:
      // Stoke's model, no convective term
      break;

    case 1:
      // classic Navier Stokes convective term : \rho (u \cdot \grad) u
      // Natural condition: \sigma(u,p) \cdot n = 0
      // discretization : \rho (u^{n} \cdot \grad) u^{n+1}
      // DEFAULT METHOD
      if(FelisceParam::instance().useProjectionForNitscheXFEM)
        m_elemFieldAdv.setValue(m_seqVelExtrapol[idTimeStep], *m_feVel, iel, m_iVelocity, m_ao, dof());
      else
        m_elemFieldAdv.setValue(m_seqVelExtrapol[idTimeStep], *m_feVel, ielOld, m_iVelocity, m_aoOld[idTimeStep], m_dofOld[idTimeStep]);
      
      m_elementMat[0]->u_grad_phi_j_phi_i(m_density, m_elemFieldAdv, *m_feVel, 0, 0, dimension());


      // Temam's operator to stabilize the convective term
      // Convective term in energy balance :
      // ... + \rho/2 \int_Gamma (u^n.n)|u^n+1|^2 - \rho/2 \int_Omega (\nabla . u^n) |u^n+1|^2 + ...
      // Add \rho/2 \int_Omega (\nabla . u^n) u^n+1 v to delete the second term.
      // it's consistent with the exact solution (divergence = 0)
      m_elementMat[0]->div_u_phi_j_phi_i(0.5*m_density, m_elemFieldAdv, *m_feVel, 0, 0, dimension());
      break;

    default:
      FEL_ERROR("Error: Wrong or not implemented convection method, check the param NSequationFlag in the data file");
    }



    //================================
    //              RHS
    //================================
    // bdf
    // \frac{\rho}{\dt} \sum_{i=1}^k \alpha_i u_{n+1-i}
    // 'm_density' and not 'coef' because the time step is integrated into the bdf term
    if(FelisceParam::instance().useProjectionForNitscheXFEM)
      m_elemFieldRHSbdf.setValue(m_seqBdfRHS[idTimeStep], *m_feVel, iel, m_iVelocity, m_ao, dof());
    else
      m_elemFieldRHSbdf.setValue(m_seqBdfRHS[idTimeStep], *m_feVel, ielOld, m_iVelocity, m_aoOld[idTimeStep], m_dofOld[idTimeStep]);
    
    assert(!m_elementVector.empty());
    m_elementVector[0]->source(m_density, *m_feVel, m_elemFieldRHSbdf, 0, dimension());

    
    // --------------------------------------------------------------------- //
    // SUPG Stabilization (works with a bdf 1 but wrong with a higher order) //
    // --------------------------------------------------------------------- //
    if((m_velocity->finiteElementType() == m_pressure->finiteElementType()) || FelisceParam::instance().NSequationFlag == 1) {
      if(FelisceParam::instance().NSStabType == 0) {
        double stab1 = FelisceParam::instance().stabSUPG;
        double stab2 = FelisceParam::instance().stabdiv;
        int type = FelisceParam::instance().typeSUPG;
        double Re_elem, tau;
        
        // compute advection velocity
        if(FelisceParam::instance().useProjectionForNitscheXFEM)
          m_elemFieldAdv.setValue(m_seqVelExtrapol[idTimeStep], *m_feVel, iel, m_iVelocity, m_ao, dof());
        else
          m_elemFieldAdv.setValue(m_seqVelExtrapol[idTimeStep], *m_feVel, ielOld, m_iVelocity, m_aoOld[idTimeStep], m_dofOld[idTimeStep]);

        // Beware: if elemFieldRHS is modified in userNS, it wont be considered here...
        // (usually elemFieldRHS = 0 (no body force))
        m_elementMat[0]->stab_supg(m_fstransient->timeStep,
                                   stab1, stab2, m_density, m_viscosity, *m_feVel,
                                   m_elemFieldAdv, m_elemFieldRHS, *m_elementVector[0],
                                   Re_elem, tau, type);
      }
    }
  }





  /*!
   * Gather the extrapolation of the velocity into m_seqVelExtrapol.
   * If m_seqVelExtrapol is null, it will be created and allocated during the gathering 
   * according to the parallel shape of parallelVec
   */
  void LinearProblemFSINitscheXFEMFluid::gatherSeqVelExtrapol(std::vector<PetscVector>& parallelVec) {
    for(std::size_t i=0; i<parallelVec.size(); ++i)
      gatherVector(parallelVec[i], m_seqVelExtrapol[i]);
    
    m_isSeqVelExtrapolAllocated = true;
  }

  PetscVector& LinearProblemFSINitscheXFEMFluid::getVelExtrapol(felInt n) {
    return m_seqVelExtrapol[n];
  }

  
  /*!
   * Gather the rhs part of the time derivative of the velocity into m_seqBdfRHS.
   * If m_seqBdfRHS is null, it will be created and allocated during the gathering 
   * according to the parallel shape of bdf->vector()
   */
  void LinearProblemFSINitscheXFEMFluid::gatherSeqBdfRHS(std::vector<PetscVector>& parallelVec) {
    for(std::size_t i=0; i<parallelVec.size(); ++i)
      gatherVector(parallelVec[i], m_seqBdfRHS[i]);

    m_isSeqBdfRHSAllocated = true;
  }



  void LinearProblemFSINitscheXFEMFluid::addMatrixRHS() {
    matrix(0).axpy(1,matrix(1),SAME_NONZERO_PATTERN);
  }

  /*!
   \brief Duplicate the support dof intersected by the structure
  */
  void LinearProblemFSINitscheXFEMFluid::initSupportDofDerivedProblem() {
    ///////////////////////////////
    // duplicate the support dof //
    ///////////////////////////////
    m_duplicateSupportElements->duplicateIntersectedSupportElements(m_supportDofUnknown);
  }


  /*!
    \brief Initialize the bdf for the solution defined on the non duplicated support dof mesh
  */
  void LinearProblemFSINitscheXFEMFluid::initOriginalBdf(felInt timeScheme, felInt extrapolOrder) {
    // create the std::vector solution with the original supportDofMesh
    // first, compute the number of dof in the original supportDofMesh
    felInt idVar = 0;
    felInt numSupportOfDof = 0;
    m_numOriginalDof = 0;

    for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); ++iUnknown) {
      idVar = m_listUnknown.idVariable(iUnknown);
      numSupportOfDof = m_supportDofUnknownOriginal[iUnknown]->numSupportDof();
      m_numOriginalDof += numSupportOfDof * m_listVariable[idVar].numComponent();
    }

    m_tmpSeqOriginalSol.create(PETSC_COMM_SELF);
    m_tmpSeqOriginalSol.setType(VECSEQ);
    m_tmpSeqOriginalSol.setSizes(PETSC_DECIDE, m_numOriginalDof);
    m_tmpSeqOriginalSol.set(0.);

    // initialize the bdf storing all fluid solution with the original support dof mesh
    m_seqOriginalSolutions.defineOrder(FelisceParam::instance().orderBdfNS);

    if(FelisceParam::instance().orderBdfNS == 1 && timeScheme == 3 && extrapolOrder == 2) {
      m_seqOriginalSolutions.initialize(m_tmpSeqOriginalSol, m_tmpSeqOriginalSol);
    } else if ( ((timeScheme == 0) || (timeScheme == 2)) && extrapolOrder == 2 )  {
      m_seqOriginalSolutions.initialize(m_tmpSeqOriginalSol, m_tmpSeqOriginalSol); 
    } else  {
      m_seqOriginalSolutions.initialize(m_tmpSeqOriginalSol);
    }
    m_isSeqOriginalSolAllocated = true;
  }


  void LinearProblemFSINitscheXFEMFluid::setDuplicateSupportObject(DuplicateSupportDof* object) {
    m_duplicateSupportElements = object;
  }


  void LinearProblemFSINitscheXFEMFluid::copyCurrentSolToOriginalSol() {
    if(!m_isSeqOriginalSolAllocated) {
      FEL_ERROR("The bdf storing all the data with the original supportDofMesh should already be created at this point");
    }

    // get the last original solution (will be deleted after the update anyway)
    // m_tmpSeqOriginalSol.copyFrom(m_seqOriginalSolutions.sol_n());
    // m_tmpSeqOriginalSol.set(0.);
    felInt numTotalDof = 0;
    felInt numTotalDofOriginal = 0;

    // Copy only the value of the original support dofs
    // All the id of the duplicated points are at the end. The ids of the original points are thus from 0 to the
    // number of support dof in the original supportDofMesh
    for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); ++iUnknown) {
      felInt idVar = m_listUnknown.idVariable(iUnknown);
      felInt numOriginalSupportDof = m_supportDofUnknownOriginal[iUnknown]->numSupportDof();
      felInt numOriginalDofOfUnknown = numOriginalSupportDof * m_listVariable[idVar].numComponent();

      std::vector<felInt> ids(numOriginalDofOfUnknown);
      std::vector<double> values(numOriginalDofOfUnknown, 0.);
      for(std::size_t i=0; i<ids.size(); ++i) {
        ids[i] = i + numTotalDof;
      }

      sequentialSolution().getValues(numOriginalDofOfUnknown, ids.data(), values.data());

      for(std::size_t i=0; i<ids.size(); ++i) {
        ids[i] = i + numTotalDofOriginal;
      }

      m_tmpSeqOriginalSol.setValues(numOriginalDofOfUnknown, ids.data(), values.data(), INSERT_VALUES);

      numTotalDof += supportDofUnknown(iUnknown).numSupportDof() * m_listVariable[idVar].numComponent();
      numTotalDofOriginal += numOriginalDofOfUnknown;
    }

    m_tmpSeqOriginalSol.assembly();

    // update the bdf of the original support dof mesh
    m_seqOriginalSolutions.update(m_tmpSeqOriginalSol);
  }


  void LinearProblemFSINitscheXFEMFluid::copyOriginalToCurrent(PetscVector& vecSeqOriginal, PetscVector& vecSeqCurrent, PetscVector& vecParCurrent) {
    PetscVector tmpSeqVec;
    if(vecSeqCurrent.isNull()) {
      tmpSeqVec.duplicateFrom(sequentialSolution());
    } else {
      tmpSeqVec = vecSeqCurrent;
    }

    // copy the serial solution
    felInt numTotalDof = 0;
    felInt numTotalDofOriginal = 0;

    for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); ++iUnknown) {
      felInt idVar = m_listUnknown.idVariable(iUnknown);
      felInt numComp =  m_listVariable[idVar].numComponent();

      felInt numOriginalSupportDof = m_supportDofUnknownOriginal[iUnknown]->numSupportDof();
      felInt numOriginalDofOfUnknown = numOriginalSupportDof * numComp;

      felInt numSupportDof = supportDofUnknown(iUnknown).numSupportDof();
      felInt numDofOfUnknown = numSupportDof * numComp;

      std::vector<felInt> ids(numDofOfUnknown, -1);
      std::vector<double> values(numDofOfUnknown, 0.);

      // get the id of the dofs in the original std::vector (duplicate the id of the duplicate dof in the current)
      for(felInt i=0; i<numOriginalDofOfUnknown; ++i) {
        ids[i] = i + numTotalDofOriginal;
      }

      felInt count = numOriginalDofOfUnknown;
      auto& idVerMsh = m_duplicateSupportElements->getIntersectedVerMsh();
      for(auto it = idVerMsh.begin(); it != idVerMsh.end(); ++it) {
        for(felInt i=0; i<numComp; ++i) {
          ids[count++] = numTotalDofOriginal + it->first * numComp + i;
        }
      }

      // get the values in the serial original std::vector (we get multiple times the duplicated ones)
      vecSeqOriginal.getValues(numDofOfUnknown, ids.data(), values.data());

      // get the id in the current std::vector
      for(std::size_t i=0; i<ids.size(); ++i) {
        ids[i] = i + numTotalDof;
      }

      tmpSeqVec.setValues(numDofOfUnknown, ids.data(), values.data(), INSERT_VALUES);

      numTotalDof += numDofOfUnknown;
      numTotalDofOriginal += numOriginalDofOfUnknown;
    }

    tmpSeqVec.assembly();

    // scatter the serial solution to the parallel one
    if(vecParCurrent.isNotNull()) {
      tmpSeqVec.scatterToZeroNotCreatingVector(vecParCurrent, INSERT_VALUES, SCATTER_REVERSE);
    }

    if(vecSeqCurrent.isNull()) {
      tmpSeqVec.destroy();
    }
  }


  void LinearProblemFSINitscheXFEMFluid::deleteDynamicData() {
    // here, we delete all the matrices, vectors and mapping of the linear problem.
    // everything will be rebuilt at the next iteration
    // This is almost a copy of the destructor of LinearProblem

    // sequential solution
    bool& isSeqSolAlloc = isSequentialSolutionAllocated();
    if(isSeqSolAlloc) {      
      sequentialSolution().destroy();
      isSeqSolAlloc = false;
    }

    // element matrices and matrices
    m_elementMat.clear();

    // element vectors
    m_elementVector.clear();
    m_elementMatBD.clear();
    m_elementVectorBD.clear();

    // solution and rhs
    bool& areSolAndRhsAlloc = areSolutionAndRHSAllocated();
    if (areSolAndRhsAlloc) {
      for (unsigned int i = 0; i < numberOfVectors(); i++)
        vector(i).destroy();
      
      solution().destroy();
      areSolAndRhsAlloc = false;
    }

    // finite elements
    m_listCurrentFiniteElement.clear();
    m_listCurvilinearFiniteElement.clear();

    // mappings
    bool& isMapElemAlloc = isMappingElemAllocated();
    if(isMapElemAlloc) {
      ISLocalToGlobalMappingDestroy(&mappingElem());
      isMapElemAlloc = false;
    }

    bool& isMapElemSupportAlloc = isMappingElemSupportAllocated();
    if(isMapElemSupportAlloc) {
      for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
        ISLocalToGlobalMappingDestroy(mappingElemSupportPerUnknown(iUnknown));

      isMapElemSupportAlloc = false;
    }

    bool& isMapNodesAlloc = isMappingNodesAllocated();
    if(isMapNodesAlloc) {
      ISLocalToGlobalMappingDestroy(&mappingNodes());
      isMapNodesAlloc = false;
    }

    bool& isMapFelToPetscAlloc = isMappingLocalFelisceToGlobalPetscAllocated();
    if(isMapFelToPetscAlloc) {
      for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
        ISLocalToGlobalMappingDestroy(&mappingIdFelisceToPetsc(iUnknown));

      isMapFelToPetscAlloc = false;
    }

    // AO (copy the AO, it will be destroy with the old one)
    bool& isAOAlloc = isAOAllocated();
    if(isAOAlloc) {
      AODestroy(&ao());
      isAOAlloc = false;
    }


    // dofs (the pattern is useless for the old dof)
    dof().numDof() = 0;
    dof().numDofPerUnknown().clear();
    dof().clearPattern();


    // repartition of the dof
    m_dofPart.clear();
    m_eltPart.clear();


    // supportDofMesh (copy the old one)
    m_supportDofUnknown.clear();
    m_supportDofUnknownLocal.clear();


    // local mesh
    m_meshLocal[m_currentMesh]->domainDim() = GeometricMeshRegion::GeoMeshUndefined;
    m_meshLocal[m_currentMesh]->deleteElements();

    // extrapolation and time rhs of this class
    if(m_isSeqVelExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i) {
        m_seqVelExtrapol[i].destroy();
        m_seqVelExtrapol[i] = PetscVector::null();
      }
      m_isSeqVelExtrapolAllocated = false;
    }

    if(m_isSeqBdfRHSAllocated) {
      for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i) {
        m_seqBdfRHS[i].destroy();
        m_seqBdfRHS[i] = PetscVector::null();
      }
      m_isSeqBdfRHSAllocated = false;
    }

    if(m_isSeqRNFluidExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqRNFluidExtrapol.size(); ++i) {
        m_seqRNFluidExtrapol[i].destroy();
        m_seqRNFluidExtrapol[i] = PetscVector::null();
      }
      m_isSeqRNFluidExtrapolAllocated = false;
    }
  }


  void LinearProblemFSINitscheXFEMFluid::copyOriginalSolToCurrentSol() {
    if(!m_isSeqOriginalSolAllocated) {
      FEL_ERROR("The bdf storing all the data with the original supportDofMesh should already be created at this point");
    }

    copyOriginalToCurrent(m_seqOriginalSolutions.sol_n(), sequentialSolution(), solution());
  }


  void LinearProblemFSINitscheXFEMFluid::copyOriginalBdfToCurrentBdf(felInt timeScheme, felInt extrapolOrder) {
    if(!m_isSeqOriginalSolAllocated) {
      FEL_ERROR("The bdf storing all the data with the original supportDofMesh should already be created at this point");
    }

    // first, reinitialize the bdf with the solution to get vectors of right size
    felInt order = FelisceParam::instance().orderBdfNS;

    // copy all bdf solution
    PetscVector zero = PetscVector::null();
    if(order == 1) {
      if(timeScheme == 3 && extrapolOrder == 2) {
        m_bdf->reinitialize(order, solution(), solution(), zero);
        copyOriginalToCurrent(m_seqOriginalSolutions.sol_n_1(), zero, m_bdf->sol_n_1());
      } else if ( ((timeScheme == 0) || (timeScheme == 2)) && extrapolOrder == 2) {
        m_bdf->reinitialize(order, solution(), solution(), zero);
        copyOriginalToCurrent(m_seqOriginalSolutions.sol_n_1(), zero, m_bdf->sol_n_1());
      }else {
        m_bdf->reinitialize(order, solution(), zero, zero);
      }
    } else if(order == 2) {
      m_bdf->reinitialize(order, solution(), solution(), zero);
      copyOriginalToCurrent(m_seqOriginalSolutions.sol_n_1(), zero, m_bdf->sol_n_1());
    } else {
      FEL_ERROR("Wrong bdf order");
    }
  }


  
  void LinearProblemFSINitscheXFEMFluid::initOldObjects(felInt stabExplicitExtrapolOrder) {
    // compute how many old solution we need to store
    felInt order = std::max(stabExplicitExtrapolOrder, FelisceParam::instance().orderBdfNS);

    // resize std::vector
    m_sequentialSolutionOld.resize(order);
    m_solutionOld.resize(order);
    m_aoOld.resize(order);
    m_supportDofUnknownOld.resize(order);
    m_dofOld.resize(order);


    // init old solution    
    for(std::size_t i=0; i<m_sequentialSolutionOld.size(); ++i) {
      m_sequentialSolutionOld[i].duplicateFrom(sequentialSolution());
      m_sequentialSolutionOld[i].copyFrom(sequentialSolution());
    }

    for(std::size_t i=0; i<m_solutionOld.size(); ++i) {
      m_solutionOld[i].duplicateFrom(solution());
      m_solutionOld[i].copyFrom(solution());
    }


    // init old AO
    for(std::size_t i=0; i<m_aoOld.size(); ++i) {
      m_aoOld[i] = ao();
      PetscObjectReference((PetscObject)ao());
    }


    // init old support dof
    for(std::size_t i=0; i<m_supportDofUnknownOld.size(); ++i)
      m_supportDofUnknownOld[i] = m_supportDofUnknown;


    // init old dof
    std::vector<SupportDofMesh*> pSupportDofUnknown(m_supportDofUnknown.size(), nullptr);
    for(std::size_t i=0; i<m_dofOld.size(); ++i) {
      for(std::size_t iUnknown=0; iUnknown < m_supportDofUnknown.size(); ++iUnknown)
        pSupportDofUnknown[iUnknown] = &m_supportDofUnknownOld[i][iUnknown];

      m_dofOld[i].setDof(m_listUnknown, m_listVariable, pSupportDofUnknown);
      m_dofOld[i].numDof() = dof().numDof();
      m_dofOld[i].numDofPerUnknown() = dof().numDofPerUnknown();
    }
  }

  
  void LinearProblemFSINitscheXFEMFluid::updateOldSolution(felInt countInitOld) {
    if(countInitOld > 0) {
      // sequential solution
      m_sequentialSolutionOld[m_sequentialSolutionOld.size() - 1].destroy();
      for(std::size_t i=m_sequentialSolutionOld.size() - 1; i>0; --i)
        m_sequentialSolutionOld[i] = m_sequentialSolutionOld[i-1];
      
      m_sequentialSolutionOld[0].duplicateFrom(sequentialSolution());
      m_sequentialSolutionOld[0].copyFrom(sequentialSolution());
      

      // parallel solution
      m_solutionOld[m_solutionOld.size() - 1].destroy();
      for(std::size_t i=m_solutionOld.size() - 1; i>0; --i)
        m_solutionOld[i] = m_solutionOld[i-1];
      
      m_solutionOld[0].duplicateFrom(solution());
      m_solutionOld[0].copyFrom(solution());


      // AO
      AODestroy(&m_aoOld[m_aoOld.size() - 1]);
      for(std::size_t i=m_aoOld.size() - 1; i>0; --i)
        m_aoOld[i] = m_aoOld[i-1];
      m_aoOld[0] = ao();
      PetscObjectReference((PetscObject)ao());
      

      // supportDofMesh (copy the old one)
      for(std::size_t i=m_supportDofUnknownOld.size() - 1; i>0; --i)
        m_supportDofUnknownOld[i] = m_supportDofUnknownOld[i-1];
      m_supportDofUnknownOld[0] = m_supportDofUnknown;


      // dofs
      std::vector<SupportDofMesh*> pSupportDofUnknown(m_supportDofUnknown.size(), nullptr);
      for(std::size_t i=m_dofOld.size() - 1; i>0; --i) {
        m_dofOld[i].numDof() = m_dofOld[i-1].numDof();
        m_dofOld[i].numDofPerUnknown() = m_dofOld[i-1].numDofPerUnknown();
      }
      
      m_dofOld[0].numDof() = dof().numDof();
      m_dofOld[0].numDofPerUnknown() = dof().numDofPerUnknown();


      // delete extrapolations
      for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i) {
        m_seqVelExtrapol[i].destroy();
        m_seqVelExtrapol[i] = PetscVector::null();
      }
      m_isSeqVelExtrapolAllocated = false;
      
      // delete rhs time
      for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i) {
        m_seqBdfRHS[i].destroy();
        m_seqBdfRHS[i] = PetscVector::null();
      }
      m_isSeqBdfRHSAllocated = false;
    } else {
      // copy the solutions
      for(std::size_t i=m_sequentialSolutionOld.size() - 1; i>0; --i)
        m_sequentialSolutionOld[i].copyFrom(m_sequentialSolutionOld[i-1]);
      m_sequentialSolutionOld[0].copyFrom(sequentialSolution());

      for(std::size_t i=m_solutionOld.size() - 1; i>0; --i)
        m_solutionOld[i].copyFrom(m_solutionOld[i-1]);
      m_solutionOld[0].copyFrom(solution());
    }
  }


  /*!
   * Gather the extrapolation of the fluid velocity and pressure for the RN schemes 
   * into m_seqStabExplicitFluidExtrapol.
   * If m_seqStabExplicitFluidExtrapol is null, it will be created and allocated during the gathering 
   * according to the parallel shape of parFluidExtrapol
   */
  void LinearProblemFSINitscheXFEMFluid::gatherRNFluidExtrapol(std::vector<PetscVector>& parFluidExtrapol) {
    for(std::size_t i=0; i<parFluidExtrapol.size(); ++i)
      gatherVector(parFluidExtrapol[i], m_seqRNFluidExtrapol[i]);
    
    m_isSeqRNFluidExtrapolAllocated = true;
  }

  void LinearProblemFSINitscheXFEMFluid::gatherRNFluidExtrapol(PetscVector& parFluidExtrapol) {
    gatherVector(parFluidExtrapol, m_seqRNFluidExtrapol[0]);
    m_isSeqRNFluidExtrapolAllocated = true;
  }

  void LinearProblemFSINitscheXFEMFluid::setRNFluidExtrapol(PetscVector& seqFluidExtrapol) {
    m_seqRNFluidExtrapol[0].copyFrom(seqFluidExtrapol);
  }

  void LinearProblemFSINitscheXFEMFluid::initRNFluidExtrapol(felInt order) {
    if(m_isSeqRNFluidExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqRNFluidExtrapol.size(); ++i)
        m_seqRNFluidExtrapol[i].destroy();
    }
    
    m_seqRNFluidExtrapol.resize(order);
    for(std::size_t i=0; i<m_seqRNFluidExtrapol.size(); ++i)
      m_seqRNFluidExtrapol[i] = PetscVector::null();
  }
}
