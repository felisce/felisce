//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/autoregulation.hpp"
#include "Core/felisceTools.hpp"

namespace felisce {
  

  /*! \brief Initialization of the class, parameters taken from FelisceParam    
    
    \param[in] elemBoundaryType std::string with the keyword associated to the boundary element type
    \return a std::unordered_map that associates to a group of labels defined in a std::string the corresponding labels stored in a std::vector
   */
  std::unordered_map <std::string,std::vector< int> >
  Autoregulation::initialize(std::string elemBoundaryType) {

    /*! shape parameters for the function sigmoid() : #m_slope abd #m_center are std::set from data in FelisceParam */
    double k( FelisceParam::instance().q / ( 1 - FelisceParam::instance().q ) );
    m_slope = 2 * std::log(k)/(mmHg2DynPerCm2 * FelisceParam::instance().up  - mmHg2DynPerCm2 * FelisceParam::instance().down );
    m_center = (mmHg2DynPerCm2 * FelisceParam::instance().up  + mmHg2DynPerCm2 * FelisceParam::instance().down )/2;

    /*! reference values and regulation parameters to handle autoregulation in windkesel are std::set */
    if ( FelisceParam::instance().useRegulationInWindkessel  ) {

      m_RdistRef = FelisceParam::instance().lumpedModelBC_Rdist;      // resistances of the different outlets
      m_CRef     = FelisceParam::instance().lumpedModelBC_C;      // capacitances
      m_regParam = FelisceParam::instance().lumpedModelBC_regParam ;      // parameter for the regulation part

      // initialization of current quantities
      m_Rdist.resize(m_RdistRef.size(),0.0);    
      m_C.resize(m_CRef.size(),0.0);
    }
    
    /*! #m_nC the number of cardiac cycles is computed */
    m_nC = static_cast<int>(std::min(ceil(FelisceParam::instance().timeMax/m_cycleDuration), 
                                ceil(FelisceParam::instance().timeIterationMax*FelisceParam::instance().timeStep/m_cycleDuration)));

    // once that m_nC is known mean inlet quantities are resized
    m_QInflow.resize(m_nC,0.0);
    m_PInflow.resize(m_nC,0.0); 

    std::unordered_map<std::string,std::vector<int> > group2Labels;
    std::map<int, std::string> label2Group = FelisceParam::instance().felName2MapintRef2DescLine[ elemBoundaryType ];

    //Here the std::unordered_map stored in FelisceParam is "reversed"
    for(auto it = label2Group.begin(); it != label2Group.end(); ++it) {
      group2Labels[ it->second ].push_back( it->first );
    }

    /*! the list of labels: #m_labelInlet, #m_labelOutlet and #m_labelStucture are filled */
    m_labelInlet =  group2Labels["inflow"] ;
    m_labelOutlet = group2Labels["outflow"] ;
    m_labelStructure = group2Labels["lateral"] ;

    if ( FelisceParam::verbose() > 1 ) {
      std::stringstream master;
      master<<" inflow labels: ";
      for ( std::size_t iLab(0); iLab < m_labelInlet.size(); ++iLab ) {
        master<<m_labelInlet[iLab]<<'\t';
      }
      master<<std::endl;
      master<<" outflow labels: ";
      for ( std::size_t iLab(0); iLab < m_labelOutlet.size(); ++iLab ) {
        master<<m_labelOutlet[iLab]<<'\t';
      }
      master<<std::endl;
      master<<" structure labels: ";
      for ( std::size_t iLab(0); iLab < m_labelStructure.size(); ++iLab ) {
        master<<m_labelStructure[iLab]<<'\t';
      }
      master<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",master.str().c_str());
    }
    return group2Labels;
  }

  /*! \brief post-process function, displays quantities to video and possibly to a summary*/
  void
  Autoregulation::update( const double& time, double Q, double P, std::vector<double> pout, const int& rank ) {
    if( rank == MASTER ) {
      double n = time / m_cycleDuration;
      std::size_t nf = static_cast<std::size_t>(floor(n));

      std::cout<<"labelInlet "<<m_labelInlet[0]<<" T "<<time<<" nf "<<nf
          <<" Q "<<Q<<" P [mmHg]"<<P*m_cycleDuration/mmHg2DynPerCm2<<std::endl;
      if( Tools::equalTol(time, (nf+1)*m_cycleDuration , FelisceParam::instance().timeStep/2) ) {
        m_QInflow[nf] += FelisceParam::instance().timeStep*Q/2;
        m_PInflow[nf] += FelisceParam::instance().timeStep*P/2;
        if( nf + 1<m_QInflow.size()) {
          m_QInflow[nf+1] += FelisceParam::instance().timeStep*Q/2;
          m_PInflow[nf+1] += FelisceParam::instance().timeStep*P/2;
        }
        exportSummary(rank);
      } else if( Tools::equalTol(time, nf*m_cycleDuration , FelisceParam::instance().timeStep/2) ) {
        if( nf<m_QInflow.size() ) {
          m_QInflow[nf] += FelisceParam::instance().timeStep*Q/2;
          m_PInflow[nf] += FelisceParam::instance().timeStep*P/2;
        }
        m_QInflow[nf-1] += FelisceParam::instance().timeStep*Q/2;
        m_PInflow[nf-1] += FelisceParam::instance().timeStep*P/2;
        exportSummary(rank);
      } else {
        if( nf<m_QInflow.size() ) {
          m_QInflow[nf] += FelisceParam::instance().timeStep*Q;
          m_PInflow[nf] += FelisceParam::instance().timeStep*P;   
        }
      } 
      for( std::size_t  kp(0); kp<m_labelOutlet.size(); kp++) {
        pout[kp]=pout[kp]/mmHg2DynPerCm2;
        std::cout<<"pout label+"<<m_labelOutlet[kp]<<" = "<<pout[kp]<<std::endl;
      }
    }
  }

  /*! \brief it returns the pressure at the inlet at the current time step */
  double 
  Autoregulation::inletPressure(const double &t, const int& caseTest) const
  {
    double timeInCycle = ( t - static_cast<int>( t / m_cycleDuration )*m_cycleDuration );

    double peakPressure(0), diastPres(0);
    switch(caseTest)
    {
    case 0:
      meanPressure2PeakAndDiast( FelisceParam::instance().nominalPressure *mmHg2DynPerCm2, peakPressure, diastPres);
      break;
    case 3:
    case 9:
    case 10:
    case 11:
    case 26:
      meanPressure2PeakAndDiast( FelisceParam::instance().incomingPressure *mmHg2DynPerCm2, peakPressure, diastPres);
      break;
    default:
      FEL_ERROR("non existing (or removed) case required");
      break;
    }        

    bool systole = timeInCycle < m_systoleDuration;
    if ( systole )
      return - peakPressure * std::sin( PI * timeInCycle / m_systoleDuration ) - diastPres;
    else // diastole
      return - diastPres;
  }

  /*! \brief it enforce the autoregulation in the windkessels, by calling setLumpedModel()*/  
  void 
  Autoregulation::handleLumpedModel(const double& t, LumpedModelBC* lumpedModel ) {
    if ( FelisceParam::instance().useRegulationInWindkessel  ) {
      //computation of the regulated windkessels
      setLumpedModel(t);
      //here we change the parameter in FelisceParam ...
      FelisceParam::instance().lumpedModelBC_Rdist = m_Rdist;
      FelisceParam::instance().lumpedModelBC_C = m_C;
      lumpedModel->setRd(m_Rdist);
      lumpedModel->setC(m_C);
    }
  }
  /*! \brief for each cardiac cycle the difference between the mean pressure and the reference one is computed*/
  void  
  Autoregulation::computeExtraPressure( const int &nC ) {

    double startingTime(0.0);
    double currentP(0.0);
    double nominalP = computeMeanInflowPress( FelisceParam::instance().idControlFreeCase , startingTime);

    m_extraPressure.resize(nC);
    for (int k(0); k<nC; k++) {
      currentP = computeMeanInflowPress( FelisceParam::instance().idCase , k * m_cycleDuration);
      m_extraPressure[k] = nominalP-currentP;
    }
  }

  /*! \brief it returns the active part of the fibers pre-stress*/
  double 
  Autoregulation::control( const double &t ) const {
    double extraP( interpPressure( t ) );
    return FelisceParam::instance().maxT  * sigmoid( extraP ); 
  }

  /*! \brief post-process function, it writes a summary */
  void 
  Autoregulation::exportSummary( const int& rank ) const {
    if( rank == MASTER ) {
      std::string nameFile = FelisceParam::instance().resultDir + "../" + FelisceParam::instance().nameTest  + ".out";
      std::ofstream file( nameFile.c_str() );

      file<<FelisceParam::instance().nameTest <<std::endl;

      file<<"Flow at inlet [cgs]"<<std::endl;
      for ( int k(0); k<m_nC; k++ ) {
        file<<m_QInflow[k]<<'\t';
      }
      file<<std::endl;

      file<<"Mean pressure at inlet [cgs]"<<std::endl;
      for ( int k(0); k<m_nC; k++ ) {
        file<<m_PInflow[k]<<'\t';
      }
      file<<std::endl;
    }
  }
  /*! \brief post-process function, it writes a summary */
  void 
  Autoregulation::exportControlFunctions(const int& numCycle) {
    
    double dt = 0.1;
    unsigned int N = static_cast<unsigned int>(numCycle * m_cycleDuration / dt);
    unsigned int i;
    
    std::string filename;

    filename = FelisceParam::instance().resultDir + "extraP.txt" ;
    std::ofstream extraP( filename.c_str() );
    //Time
    for ( i=0; i < N; i++) {
      extraP << dt*i<<'\t';
    }
    extraP<<std::endl;
    //ExtraPressure interpolated
    for ( i=0; i < N; i++) {
      extraP << interpPressure(dt*i)<<'\t';
    }
    extraP<<std::endl;

    extraP.close(); 
  
    filename = FelisceParam::instance().resultDir + "cont.txt" ;
    std::ofstream cont(filename.c_str());
    //Time
    for ( i=0; i < N; i++) {
      cont << dt*i<<'\t';
    }
    cont<<std::endl;
    //Force value
    for ( i=0; i < N; i++) {
      cont << control(dt*i)<<'\t';
    }
    cont<<std::endl;

    cont.close(); 

    filename = FelisceParam::instance().resultDir + "W.txt" ;
    std::ofstream Windkessel(filename.c_str());
    //Time
    for ( i=0; i < N; i++) {
      Windkessel << dt*i<<'\t';
    }
    Windkessel<<std::endl;
    //Distal resistances
    for ( std::size_t nbOfW(0); nbOfW < m_Rdist.size(); nbOfW++) {
      for ( i = 0; i<N; i++) {
        setLumpedModel(dt*i);
        Windkessel << m_Rdist[nbOfW]<<'\t';
      }
      Windkessel<<std::endl;
    }
    Windkessel.close(); 
  }
  /*! \brief given a mean P value it computes parameters for the shape of the inlet pressure*/
  void
  Autoregulation::meanPressure2PeakAndDiast(double const& meanP, double& peakP, double& diastP ) const {
    //the mean pressure is divided between the diastolic and the sistolic minus diastolic
    double proportion = FelisceParam::instance().propCardiacCycle ;
    peakP = proportion*meanP*2*PI*m_cycleDuration;
    diastP = ( 1. - proportion )*meanP;
  }

  /*! \brief ??*/
  double 
  Autoregulation::computeMeanInflowPress( const int &Case, const double &t0) const {
    //quadrature points on a time cycle
    int N = 1000;

    //quadrature step
    double dt = m_cycleDuration/N;
    //first quadrature point
    double time = t0 + dt/2;
    //result
    double Pmean(0.0);

    for (int k(0); k<N; k++){
      //integral
      Pmean += dt*inletPressure(time, Case);
      time+=dt;
    }
    
    return Pmean/m_cycleDuration;
  }

  /*! \brief at the current time step it returns an interpolated value for extraP
    
    \param[in] t current time step
    \return the interpolated value

    for each cardiac cycle the mean pressure is computed and its difference with respect
    to the reference value is also computed.\n
    A piecewise linear interpolation is then used to obtain the value at the current time step.\n
    The value of m_extraPressure at the i-th cycle is given to last instant of the cardiac cycle.
   */
  double 
  Autoregulation::interpPressure( const double &t) const {
    
    //The value of the cycle is assigned to right node, the first node is arbitrarly std::set to zero
    int N = static_cast<int>(floor( t / m_cycleDuration));
    double tLoc = t-m_cycleDuration*N;
    if (N==0) {
      return tLoc/m_cycleDuration*m_extraPressure[0];
    } else {
      return m_extraPressure[N-1]+tLoc/m_cycleDuration*(m_extraPressure[N]-m_extraPressure[N-1]);
    }
  }
  /*! \brief it computes the current value for the windkessel parameters
     
     \param[in] t current time step

     At a given time step t, extraP is computed using interpPressure.
     Then, for each outlet, the distal resistance is updated by using
     \f$
     R_{dist}= q R_{dist,ref} + R_{dist,ref} ( 1 - q ) ( 1 + \mbox{regParam}\,\mbox{sigmoid}( \delta p ) )
     \f$
   */
  void Autoregulation::setLumpedModel(const double& t) {
    double extraP = interpPressure( t );
    double q = FelisceParam::instance().windkesselProp ;
    for( std::size_t i(0);i<m_RdistRef.size();i++) {
      m_Rdist[i] = m_RdistRef[i]*q + m_RdistRef[i]* (1-q) * ( 1 + m_regParam[i] * sigmoid(extraP));
      m_C[i] = m_CRef[i]/( q + (1-q) * m_regParam[i] * sigmoid(extraP) );
    }
  }
  /*! \brief the MASTER proc displays several value
   */
  void
  Autoregulation::print(const int & rank) {

    if (rank==MASTER) {
      if (FelisceParam::verbose() > 3 ) {
        std::cout<<"Shape Parameters for the control functions: "<<std::endl;
        std::cout<<"          Maximum value: "<<FelisceParam::instance().maxT <<std::endl;
        std::cout<<"          Minimum value: "<<FelisceParam::instance().maxT *sigmoid(-1.e5)<<std::endl;
        std::cout<<"           Center point: "<<m_center<<std::endl;
        std::cout<<"                  Slope: "<<m_slope<<std::endl;
        std::cout<<std::endl;
        std::cout<<std::endl;
    
        std::cout<<"Computed extraPressure"<<std::endl;
        for ( std::size_t i(0); i<m_extraPressure.size(); i++)
          std::cout<<i+1<<'\t'<<m_extraPressure[i]<<std::endl;
        std::cout<<std::endl;
        std::cout<<std::endl;
    
        Tools::printVector(m_labelInlet,"Inflow labels");
        Tools::printVector(m_labelOutlet,"Outflow labels");
        Tools::printVector(m_labelStructure,"Structural labels");
    
        if ( FelisceParam::instance().useRegulationInWindkessel  ) {
          std::cout<<"Windkessel parameters"<<std::endl;
          Tools::printVector(m_regParam, "Regulation Parameter");
          Tools::printVector(m_RdistRef, "Reference Distal Resistance value");
          Tools::printVector(m_CRef, "Reference capacitance value");
        }
        else
          std::cout<<"Regulation in windkessel switched off"<<std::endl;
      }
    }
  }

}
