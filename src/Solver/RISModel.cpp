//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S.Smaldone, A.This and L.Boilevin-Kayl
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Solver/RISModel.hpp"

namespace felisce {

  RISModel::RISModel(FelisceTransient::Pointer fstransient , std::vector<LinearProblem*> linearProblem): 
    m_fstransient(fstransient), 
    m_linearProblem(linearProblem) {

    if (!FelisceParam::instance().FusionDof) {
      FEL_ERROR("There are no fusioned labels whereas it is required for the RIS model!");
    }

    variables_initialization(); // initialisation of attributes for the general RIS model

    for(int rs = 0; rs <m_RISModelNum; rs++)
      RIS_models_initialization(rs); // initialisation of the resistance values of all open, closed and fake surfaces

    print(); // print status and resistance values of each closed and open surfaces

  }

  

  void RISModel::variables_initialization() {

    m_verboseRIS = FelisceParam::instance().verboseRIS; // verbose for the model

    m_R_active_surf = FelisceParam::instance().R_active_surf; // resistance of closed surface
    m_R_inactive_surf = FelisceParam::instance().R_inactive_surf; // resistance of open surface

    m_R_activeClosed_TwoValves_Intermediate = FelisceParam::instance().R_activeClosed_TwoValves_Intermediate; // resistance of closed valve just after the closing - only used for two valves
    m_R_activeClosed_TwoValves_Final = FelisceParam::instance().R_activeClosed_TwoValves_Final; // final resistance of closed valve after several iterations after the closing - only used for two valves

    m_R_FirstValve_ClosingSpeed = FelisceParam::instance().R_FirstValve_ClosingSpeed; // rate of speed for the evolution of the resistance of the closed surface of the first valve from R_Intermediate to R_Final - only used for two valves
    m_R_SecondValve_ClosingSpeed = FelisceParam::instance().R_SecondValve_ClosingSpeed; // rate of speed for the evolution of the resistance of the closed surface of the second valve from R_Intermediate to R_Final - only used for two valves

    m_ModelAdmissibleStatus = 1; // status of the model initially admissible
    m_cycl_RIS = 0; 

    m_nbTimeSteps_refractory_time = FelisceParam::instance().nbTimeSteps_refractory_time; // number of time steps used for defining the refractory time
    m_refractory_time = m_nbTimeSteps_refractory_time * m_fstransient->timeStep;
      
    m_linear_evolution_valves = false;
    m_rate_linear_transition = (m_R_active_surf -  m_R_inactive_surf)/ std::abs(FelisceParam::instance().nbTimeSteps_linear_transition * m_fstransient->timeStep); // rate of linear evolution of the valve
    m_rate_linear_transition_firstValve = (m_R_active_surf -  m_R_inactive_surf)/ std::abs(FelisceParam::instance().nbTimeSteps_linear_transition_twoValves[0] * m_fstransient->timeStep); // rate of the linear evolution of the first valve
    m_rate_linear_transition_secondValve = (m_R_active_surf -  m_R_inactive_surf)/ std::abs(FelisceParam::instance().nbTimeSteps_linear_transition_twoValves[1] * m_fstransient->timeStep); // rate of the linear evolution of the second valve

    if (FelisceParam::instance().nbTimeSteps_linear_transition > 0 || FelisceParam::instance().nbTimeSteps_linear_transition_twoValves[0] > 0 || FelisceParam::instance().nbTimeSteps_linear_transition_twoValves[1] > 0 ){
      PetscPrintf(PETSC_COMM_WORLD,"\n  **** Linear transition in the value of the resistances of open and closed valves **** \n");
      m_linear_evolution_valves = true;
    }

    felInt NumTotLabel = 100; // should be equal to the maximum label among open, closed and fake surfaces to spare some space in the array m_resistances
    m_resistances.resize(NumTotLabel, 0.);

    m_num_closed_surfs = FelisceParam::instance().closed_surf.size();
    m_num_open_surfs = FelisceParam::instance().open_surf.size();
    m_num_fake_surfs = FelisceParam::instance().fake_surf.size();

    m_RISModelNum = m_num_closed_surfs / 2; // the number of RIS model is equal to the number of coupled closed surfaces

    m_closed_surf.resize(m_num_closed_surfs, -1);
    m_open_surf.resize(m_num_open_surfs, -1);
    m_fake_surf.resize(m_num_fake_surfs, -1);

    m_ValveAdmissibleStatus.resize(m_RISModelNum, 1); // status of all valves initially open
    m_valveClosedFlag.resize(m_RISModelNum, 0); // all valves are initially considered open
    m_closingTime.resize(m_RISModelNum, 0.);
    m_openingTime.resize(m_RISModelNum, 0.);

    m_flow_ref.resize(m_RISModelNum, FelisceParam::instance().flow_ref_oneValve);

    if(m_RISModelNum == 2) {
      m_flow_ref[0] = FelisceParam::instance().flow_ref_twoValves[0];
      m_flow_ref[1] = FelisceParam::instance().flow_ref_twoValves[1];
    }

    if (m_RISModelNum > 2) {
      FEL_ERROR("RIS model cannot handle more than two valves at the moment!");
    }

  }

  void RISModel::RIS_models_initialization(int rs) {

    if(rs == FelisceParam::instance().initiallyOpenedValveIndex){
      m_valveClosedFlag[rs] = 0; // the valve of index initiallyOpenedValveIndex is initially open, meaning its open labels are active and its closed labels are inactive
    }
    else{
      m_valveClosedFlag[rs] = 1; // the others valve are initially closed, meaning their closed labels are active and their open labels are inactive
    }

    // ---------------- closed surfaces initialization ------------------

    double R_value = m_R_inactive_surf * (1 - m_valveClosedFlag[rs]) + m_valveClosedFlag[rs] * m_R_active_surf; // either R_inactive if valve is initially open or R_active if valve initally closed

    m_closed_surf[2*rs] = FelisceParam::instance().closed_surf[2*rs];
    m_closed_surf[2*rs+1] = FelisceParam::instance().closed_surf[2*rs+1];

    m_resistances[ m_closed_surf[2*rs] ] = R_value;
    m_resistances[ m_closed_surf[2*rs+1] ] = R_value;

    // ---------------- open surfaces initialization ------------------

    for(int io = rs; io < m_num_open_surfs/2; io++) { // to consider the case where there are more open surfaces than closed surfaces
      R_value = m_R_inactive_surf * (m_valveClosedFlag[rs]) + (1 - m_valveClosedFlag[rs]) * m_R_active_surf; // either R_inactive if valve is initially closed or R_active if valve initially open

      m_open_surf[2*io] = FelisceParam::instance().open_surf[2*io];
      m_open_surf[2*io+1] = FelisceParam::instance().open_surf[2*io+1];

      m_resistances[ m_open_surf[2*io] ] = R_value;
      m_resistances[ m_open_surf[2*io+1] ] = R_value;
    }

    // ---------------- fake surfaces initialization ------------------

    for(int is = rs; is < m_num_fake_surfs/2; is++) { // to consider the case where there are more fake surfaces than closed surfaces
      R_value = 0.; // fake surfaces do not have any influence, so resistance is std::set to 0

      m_fake_surf[2*is] = FelisceParam::instance().fake_surf[2*is];
      m_fake_surf[2*is+1] = FelisceParam::instance().fake_surf[2*is+1];

      m_resistances[ m_fake_surf[2*is] ] = 0.;
      m_resistances[ m_fake_surf[2*is+1] ] = 0.;
    }

  }

  // ******************** Update resistances ********************************

  void RISModel::UpdateResistances() {

    for(int rs = 0; rs <m_RISModelNum; rs++) {
      update_open_surf(rs); // std::set resistance of open surfaces to R_active if valve is open, to R_inactive if valve is closed
      update_closed_surf(rs); // std::set resistance of closed surfaces to R_inactive if valve is open, to R_active if valve is closed
    }

    print();

  }

  void RISModel::update_open_surf(int rs) {

    double R_value = 0.;

    for(int io = rs; io < m_num_open_surfs/2; io++) {     
      R_value = m_R_inactive_surf * (m_valveClosedFlag[rs]) + (1 - m_valveClosedFlag[rs]) * m_R_active_surf; // either R_active if open or R_inactive if closed
        
      if (m_linear_evolution_valves) { //Linear evolution for open valve/s
        m_tps = m_fstransient->time; // update current time
        double rate = 0.;
        
        if (m_RISModelNum == 2) { // set decreasing rate for the first and the second valve
          if (rs == 0)
            rate = m_rate_linear_transition_firstValve;
          if (rs == 1)
            rate = m_rate_linear_transition_secondValve;
        } else {
            rate = m_rate_linear_transition; // set decreasing rate for the valve
        }
          
        if ( m_openingTime[rs] > 0. && (m_tps - m_openingTime[rs]) < std::abs(FelisceParam::instance().nbTimeSteps_linear_transition * m_fstransient -> timeStep) )
          R_value = rate * (m_tps - m_openingTime[rs]) + m_R_inactive_surf;
        else if ( m_closingTime[rs] > 0. && (m_tps - m_closingTime[rs]) < std::abs(FelisceParam::instance().nbTimeSteps_linear_transition * m_fstransient -> timeStep) )
          R_value = - rate * (m_tps - m_closingTime[rs]) + m_R_active_surf;
      }

      m_resistances[ m_open_surf[2*io] ] = R_value;
      m_resistances[ m_open_surf[2*io+1] ] = R_value;
    }

  }

  void RISModel::update_closed_surf(int rs) {

    std::vector<double> flux_surf; // values of mean flux flow across each couple of closed surfaces (used when they are inactive)
    std::vector<double> absolute_flux_surf;
    std::vector<double> mesh_flux_surf;

    std::vector<double> pressure_surf; // values of mean pressure on each surface of closed surfaces (used when they are active)

    m_tps = m_fstransient->time;
    m_ValveAdmissibleStatus[rs] = 1;
    m_ModelAdmissibleStatus = 1;

    m_closedLabels.resize(2,0); // consider only a couple of closed surfaces
    m_closedLabels[0] = m_closed_surf[2*rs]; 
    m_closedLabels[1] = m_closed_surf[2*rs + 1];

    pressure_surf.resize(2, 0.); // size 2 for distal and proximal
    flux_surf.resize(2, 0.); // size 2 for distal and proximal
    absolute_flux_surf.resize(2,0.);
    mesh_flux_surf.resize(2,0.);	


    if (m_RISModelNum == 2) {
      two_valve_case(rs);
    }

    m_linearProblem[0]->computeMeanQuantity(pressure, m_closedLabels, pressure_surf);
    m_linearProblem[0]->computeMeanQuantity(velocity, m_closedLabels, absolute_flux_surf);
    if( m_fstransient->iteration > 1 && FelisceParam::instance().useALEformulation ){
       m_linearProblem[0]->computeMeanExternalQuantity(m_linearProblem[0]->externalVec(0), m_linearProblem[0]->externalAO(0), velocity, m_closedLabels, mesh_flux_surf);
    }
    for(int i = 0; i < 2; i++){
  	flux_surf[i] = absolute_flux_surf[i] - (-mesh_flux_surf[i]);
    }



	
    if(m_verboseRIS>0) {
      PetscPrintf(PETSC_COMM_WORLD,"\n######## Valve %d (P_0 = %e on label %d, P_1 = %e on label %d)\n",
                  rs+1, pressure_surf[0], m_closedLabels[0], pressure_surf[1], m_closedLabels[1]);

      PetscPrintf(PETSC_COMM_WORLD,"\n######## Valve %d  (flow =  %e (std::abs %e, mesh %e) on label %d)\n",
                  rs+1, flux_surf[0],absolute_flux_surf[0],mesh_flux_surf[0], m_closedLabels[0]);
    }

    if(m_valveClosedFlag[rs] == 1) { // if valve rs is closed check if it should open
      check_opening_condition(rs, pressure_surf); // on the pressure difference
    }
    if(m_valveClosedFlag[rs] == 0) { // else if valve rs is open check if it should close
      check_closing_condition(rs, flux_surf); // on the existence of a backflow
    }

  }

  // ************************************************************************

  void RISModel::two_valve_case(int rs) {

    double Rmitral = 0.;
    double Raortic = 0.;

    felInt displ_IterMax_RIS = FelisceParam::instance().timeMaxCaseFile; //  to do: it should be taken automatically from the displacement in file.case
    //Commenting displ_TimeMax as we no longer need it following generalization of RIS model to general opening and closing time
    //double displ_TimeMax_RIS = displ_IterMax_RIS * m_fstransient-->timeStep;

    if(m_fstransient->iteration == (m_cycl_RIS+1) * displ_IterMax_RIS) { // to read the file case in cycle
      m_cycl_RIS++;
    }

    if(rs == 0 && m_verboseRIS > 1 ) { // print just once
      PetscPrintf(PETSC_COMM_WORLD,"\n   --> m_cycl_RIS = %d ", m_cycl_RIS+1);
    }

    // 21 June ( Alexandre This ): Modification of the law to be stiffer for the valve closure (40 --> 100)
    if(rs == 0 && m_valveClosedFlag[0] == 1 ) {
      
      Rmitral = m_R_activeClosed_TwoValves_Intermediate + m_R_activeClosed_TwoValves_Final * exp( -exp( -m_R_FirstValve_ClosingSpeed * ( m_tps - m_closingTime[rs]))); // Exponential evolution of the resistance of the proximal valve
        
     // Linear evolution value of the resistance of the first valve
        if (m_linear_evolution_valves) {
          if ( m_closingTime[rs] > 0. && (m_tps - m_closingTime[rs]) < std::abs(FelisceParam::instance().nbTimeSteps_linear_transition_twoValves[0] * m_fstransient -> timeStep) )
            Rmitral = m_rate_linear_transition_firstValve * (m_tps - m_closingTime[rs]) + m_R_inactive_surf; // The proximal valve is closing, linear evolution from m_R_inactive_surf to m_R_active_surf
          else
            Rmitral = m_R_active_surf; // The first valve is completely closed
        }
        
      m_resistances[ m_closedLabels[0] ] = Rmitral;
      m_resistances[ m_closedLabels[1] ] = Rmitral;
    }

    // 21 June ( Alexandre This ): Modification of the law to be stiffer for the valve closure (30 --> 75)
    if(rs == 1 && m_valveClosedFlag[1] == 1){// and m_tps > (0.4 + m_cycl_RIS * displ_TimeMax_RIS) ) {
      // the 3th option is necessary, because in the beginning of a cardiac cycle
      // the aortic Resitance should be laready greater and not increasing,
      Raortic = m_R_activeClosed_TwoValves_Intermediate + m_R_activeClosed_TwoValves_Final * exp( -exp( -m_R_SecondValve_ClosingSpeed * ( m_tps - m_closingTime[rs] ))); // Exponential evolution of the resistance of the second valve
      
        // Linear evolution in the value of the resistance of the second valve
      if (m_linear_evolution_valves) {
        if ( m_closingTime[rs] > 0. && (m_tps - m_closingTime[rs]) < std::abs(FelisceParam::instance().nbTimeSteps_linear_transition_twoValves[1] * m_fstransient -> timeStep) )
          Raortic = m_rate_linear_transition_secondValve * (m_tps - m_closingTime[rs]) + m_R_inactive_surf; // The second valve is closing, linear evolution from m_R_inactive_surf to m_R_active_surf
        else
          Raortic = m_R_active_surf; // The second valve is completely closed
      }
        
      m_resistances[ m_closedLabels[0] ] = Raortic;
      m_resistances[ m_closedLabels[1] ] = Raortic;
    }


    if(m_valveClosedFlag.size() > 1) {
      if (m_valveClosedFlag[0] == 0 && m_valveClosedFlag[1] == 0)
        FEL_ERROR("Valves are open at the same time!");
    }

  }

  void RISModel::check_opening_condition(int rs, std::vector<double> pressure_surf) {

    if( ((pressure_surf[1] - pressure_surf[0]) <= 0) && ((m_tps - m_closingTime[rs]) > m_refractory_time) && (!(preventValvesFromOpeningTooFast(rs)))) { // surf_1 is the distal face, surf_0 the proximal face
      // negative difference pressure --> the valve must open
      double R_value = 0.;
        
      if(m_linear_evolution_valves)
        R_value = m_R_active_surf;
      else
        R_value = m_R_inactive_surf;
        
      m_resistances[ m_closedLabels[0] ] = R_value;
      m_resistances[ m_closedLabels[1] ] = R_value;

      PetscPrintf(PETSC_COMM_WORLD,"\n   -------> At time t = %e Valve %d OPENS due to a positive pressure difference (P_0 = %e, P_1 = %e)!\n",
                  m_tps,rs+1, pressure_surf[0], pressure_surf[1]);

      m_valveClosedFlag[rs] = 0;
      m_openingTime[rs] = m_tps; // update last time of opening

    }
    else {
      if (m_linear_evolution_valves && m_closingTime[rs] > 0. && m_RISModelNum == 1) { // Linear increasing in the value of the resistance of the closed valve/s
        double R_value = 0.;
          
        if ( (m_tps - m_closingTime[rs]) < std::abs(FelisceParam::instance().nbTimeSteps_linear_transition * m_fstransient -> timeStep) )
          R_value = m_rate_linear_transition * (m_tps - m_closingTime[rs]) + m_R_inactive_surf;
        else
          R_value = m_R_active_surf;
          
        m_resistances[ m_closedLabels[0] ] = R_value;
        m_resistances[ m_closedLabels[1] ] = R_value;
      }
    }
      
    if(preventValvesFromOpeningTooFast(rs)) { // print text if a valve has been prevented to open

      if(m_verboseRIS > 1) {
        PetscPrintf(PETSC_COMM_WORLD,"Closed valve of m_ris = %d would have normally opened but has been preventing from!\n", rs);
      }

    }

  }

  void RISModel::check_closing_condition(int rs, std::vector<double> flux_surf) {

    if( flux_surf[0] <= m_flow_ref[rs] && (m_tps - m_openingTime[rs]) > m_refractory_time) { // after "m_refractory_time" seconds the valve is open, it cannot close

      // existence of a backflow --> the valve must close
      if (m_RISModelNum == 2 ) {
        if(rs == 0 && !m_linear_evolution_valves)
          m_R_active_surf = m_R_activeClosed_TwoValves_Intermediate; // In the case of two valves, when the valve closes, its resistance is std::set to 1.e4 and then slightly evolves towards m_R_active_surf in two_valves_cases

        if(rs == 1 && !m_linear_evolution_valves)
          m_R_active_surf = m_R_activeClosed_TwoValves_Intermediate;
      }

      double R_value;
        
      if(m_linear_evolution_valves)
        R_value = m_R_inactive_surf; // In the case of linear evolution, when the valve closes, its resistance evolves from m_R_inactive_surf to m_R_active_surf
      else
        R_value = m_R_active_surf; // In the case outside of two valves, when the valves closes, its resistance is directly std::set to m_R_active_surf
        
      m_resistances[ m_closedLabels[0] ] = R_value;
      m_resistances[ m_closedLabels[1] ] = R_value;

      PetscPrintf(PETSC_COMM_WORLD,"\n   -------> At time t = %e valve %d CLOSES due to a backflow (flow = %e )!\n",
                  m_tps,rs+1, flux_surf[0] );

      m_valveClosedFlag[rs] = 1;
      m_closingTime[rs] = m_tps; // update last time of closing

    }
    else {
      if (m_linear_evolution_valves && m_openingTime[rs] > 0.) { // Linear decreasing in the value of the resistance of the closed valve/s
        double R_value = 0.;
        double rate = 0.;
          
        if (m_RISModelNum == 2) { // set decreasing rate for the first and the second valve
          if (rs == 0)
            rate = m_rate_linear_transition_firstValve;
          if (rs == 1)
            rate = m_rate_linear_transition_secondValve;
        } else {
          rate = m_rate_linear_transition; // set decreasing rate for the valve
        }
          
        if ( (m_tps - m_openingTime[rs]) < std::abs(FelisceParam::instance().nbTimeSteps_linear_transition * m_fstransient -> timeStep) )
          R_value = - rate * (m_tps - m_openingTime[rs]) + m_R_active_surf;
        else
          R_value = m_R_inactive_surf;
          
        m_resistances[ m_closedLabels[0] ] = R_value;
        m_resistances[ m_closedLabels[1] ] = R_value;
      }
    }
  }

  bool RISModel::preventValvesFromOpeningTooFast(int rs) { // returns 1 if one valve can open rapidly after the other closed, returns 0 if not. 

    if(m_RISModelNum != 2) { // always return 0 if there are not 2 valves
      return false;  
    }    

    else {

      if(FelisceParam::instance().preventValvesFromOpeningTooFast == 0) { // prevention not activated
        return false;
      }

      else {

        if(m_fstransient->iteration > FelisceParam::instance().nbIterationsPreventValvesFromOpeningTooFast) {

          if(rs == 0) { // if the opening condition is related to the first valve

            if(std::max(0., m_fstransient->time - m_closingTime[1]) < FelisceParam::instance().nbIterationsPreventValvesFromOpeningTooFast*FelisceParam::instance().timeStep) { // if the other valve (rs = 1) has recently closed, then return true
              std::cout << "!/CASE 1: mitral valve prevented from opening!/" << std::endl;
              return true;
            }

            else { // if not returns false
              std::cout << "!/CASE 2: mitral valve not prevented from opening it its conditions are met!/" << std::endl;
              return false;
            }
          }

          else { // if the opening condition is related to the second valve

            if(std::max(0., m_fstransient->time - m_closingTime[0]) < FelisceParam::instance().nbIterationsPreventValvesFromOpeningTooFast*FelisceParam::instance().timeStep) { // if the other valve (rs = 0) has recently closed, then return true
              std::cout << "!/CASE 3: aortic valve prevented from opening!/" << std::endl;            
              return true;
            }

            else { // if not returns false
              std::cout << "!/CASE 4: aortic valve not prevented from opening if its conditions are met!/" << std::endl; 
              return false;
            }
          }
        }

        else {
          return false;
        }
      }
    }

    return false;

  }


  // ******************* Check status admissibility *****************************

  void RISModel::CheckValveStatus() {

    for(int rs = 0; rs <m_RISModelNum; rs++) {
      m_closedLabels.resize(2,0);
      m_closedLabels[0] = m_closed_surf[2*rs];
      m_closedLabels[1] = m_closed_surf[2*rs + 1];

      if( m_valveClosedFlag[rs] == 1) { // the valve is closed
        PetscPrintf(PETSC_COMM_WORLD,"******** VALVE MODEL %d is CLOSED\n",rs+1);
        check_closed_status(rs);
      }
      if( m_valveClosedFlag[rs] == 0) { // the valve is open
        PetscPrintf(PETSC_COMM_WORLD,"******** VALVE MODEL %d is OPEN\n", rs+1);
        check_open_status(rs);
      } 

      m_ModelAdmissibleStatus *= m_ValveAdmissibleStatus[rs]; // admissibility of the model = product of valve admissible status (0 if not correct, 1 if correct)

    }

  }

  void RISModel::check_closed_status(int rs) {

    std::vector<double> pressure_surf;
    pressure_surf.resize(2, 0.);

    m_linearProblem[0]->computeMeanQuantity(pressure, m_closedLabels, pressure_surf);
    FEL_ASSERT(m_closedLabels.size() > 1);

    if(m_verboseRIS > 1) {
      PetscPrintf(PETSC_COMM_WORLD,"       Check fissure status: P_0 = %e on label %d, P_1 = %e on label %d\n",
                  pressure_surf[0], m_closedLabels[0], pressure_surf[1], m_closedLabels[1]);
    }
    if( m_resistances[ m_closedLabels[0] ] > 1 && ( pressure_surf[1] - pressure_surf[0] ) <= 0 and m_tps > 5 * m_refractory_time) {

      if(m_verboseRIS > 1)
        PetscPrintf(PETSC_COMM_WORLD,"\n       Status NOT ADMISSIBLE for valve %d\n", rs+1);
      // positive resistance --> valve is closed
      // negative pressure difference --> valve should be open
      // the status is then not admissible
      if(FelisceParam::instance().crashIfStatusNotAdmissible) {
        m_ValveAdmissibleStatus[rs] = 0; // fissure not ok        
      }
      else {
        m_ValveAdmissibleStatus[rs] = 1; // fissure still ok 
        PetscPrintf(PETSC_COMM_WORLD,"     Keep running the simulation even if status not admissible\n");       
      }
    } else { //the valve is open and fissure OK
      if(m_verboseRIS > 1)
        PetscPrintf(PETSC_COMM_WORLD,"\n       Status ADMISSIBLE for valve %d\n", rs+1);
      m_ValveAdmissibleStatus[rs] = 1;
    }

  }

  void RISModel::check_open_status(int rs) { 

    std::vector<double> flux_surf;
    flux_surf.resize(2, 0.);

    m_linearProblem[0]->computeMeanQuantity(velocity, m_closedLabels, flux_surf);

    if(m_verboseRIS > 1) {
      PetscPrintf(PETSC_COMM_WORLD,"       Check fissure status: F_0 = %e on label %d\n",
                  flux_surf[0], m_closedLabels[0]);
    }

    if( (Tools::equal(m_resistances[ m_closedLabels[0] ],0.)) && (flux_surf[0] <= m_flow_ref[0]) ) {
      PetscPrintf(PETSC_COMM_WORLD,"\n       Status NOT ADMISSIBLE for Valve%d\n", rs+1);
      // zero resistance --> valve is open
      // negative flow --> valve should be closed 
      // the status is then not admissible
      if(FelisceParam::instance().crashIfStatusNotAdmissible) {
        m_ValveAdmissibleStatus[rs] = 0; // fissure not ok
      }
      else{
        m_ValveAdmissibleStatus[rs] = 1; // fissure still ok
        PetscPrintf(PETSC_COMM_WORLD,"     Keep running the simulation even if status not admissible\n");  
      }

    } 

    else { // the valve is open and fissure OK

      if(m_verboseRIS > 1) {
        PetscPrintf(PETSC_COMM_WORLD,"\n     Status ADMISSIBLE for valve %d\n", rs+1);
      }

      m_ValveAdmissibleStatus[rs] = 1;
    }

  }

  /* void RISModel::check_open_status(int rs){
       if(m_verboseRIS>1)
         PetscPrintf(PETSC_COMM_WORLD,"      Status Admissible for Valve %d\n", rs+1);
       m_ValveAdmissibleStatus[rs] = 1;
   }*/

  int RISModel::areAllValvesClosed() const {

    int areAllValvesClosed = 1;

    for(int i = 0; i < m_RISModelNum; ++i) {

      if(m_valveClosedFlag[i] == 0) { // if current evaluated valve is open, std::set areAllValvesClosed to 0
        areAllValvesClosed = 0;
        break;
      }

      else{ // if current evaluated valve is closed, do nothing

      }

    }

    return areAllValvesClosed;

  }

  // *********************** Print resistances ***********************************

  void RISModel::print() const {

    for(int ic = 0; ic < m_num_closed_surfs/2; ic++) {
      PetscPrintf(PETSC_COMM_WORLD,"\n  RIS model number = %d --> labels closed surfaces = %d and %d  \n",ic+1, m_closed_surf[2*ic], m_closed_surf[2*ic+1]);
      PetscPrintf(PETSC_COMM_WORLD,"  with resistance = %e", m_resistances[ m_closed_surf[2*ic] ]);;
    }
    for(int io = 0; io < m_num_open_surfs/2; io++) {
      PetscPrintf(PETSC_COMM_WORLD,"\n  RIS model number = %d --> labels open surfaces = %d and %d  \n",io+1, m_open_surf[2*io], m_open_surf[2*io+1]);
      PetscPrintf(PETSC_COMM_WORLD,"  with resistance = %e \n",m_resistances[ m_open_surf[2*io] ]);
    }
    for(int is = 0; is < m_num_fake_surfs/2; is++) {
      PetscPrintf(PETSC_COMM_WORLD,"\n  RIS model number = %d --> labels open surfaces = %d and %d  \n",is+1, m_fake_surf[2*is], m_fake_surf[2*is+1]);
    }

  }

  // ****************************************************************************

  void RISModel::write(std::string fileName) const{

    static int RISInit = 1;
    std::ofstream outputFile;
    if(RISInit) {
      outputFile.open((fileName.c_str()));
    } else {
      outputFile.open((fileName.c_str()),std::ios::app);
    }

    for(int ic = 0; ic < m_num_closed_surfs/2; ic++) {
      outputFile << m_resistances[m_closed_surf[2*ic]] << " ;";
    }
    for(int io = 0; io < m_num_open_surfs/2; io++) {
      outputFile << m_resistances[ m_open_surf[2*io] ] << " ;" << std::endl;
    }
    RISInit = 0;
  }

}
