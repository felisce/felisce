//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _LinearProblemNSFracStepProj_HPP
#define _LinearProblemNSFracStepProj_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
  /*!
   \class LinearProblemNSFracStepProj
   \author J-F. Gerbeau
   \date 25/11/2011
   \brief Advection-Diffusion step of a fractional step algorithm for the Navier-Stokes equations
   */
  class LinearProblemNSFracStepProj:
    public LinearProblem {
  public:
    LinearProblemNSFracStepProj();
    ~LinearProblemNSFracStepProj() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initializeLumpedModelBC(LumpedModelBC* lumpedModelBC);

    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) override;
    void finalizeEssBCTransientDerivedProblem() override;

    void copyMatrixRHS() override;
    void addMatrixRHS() override;

    //Embedded interface
    virtual void userAssembleMatrixRHSEmbeddedInterface();
  protected:
    CurrentFiniteElement* m_feVel;
    CurrentFiniteElement* m_fePres;
    Variable* m_velocity;
    Variable* m_pressure;
    felInt m_iVelocity;
    felInt m_iPressure;
    double m_viscosity;
    double m_density;
    int m_explicitAdvection;
    LumpedModelBC* m_lumpedModelBC;
    ElementField m_elemFieldRHS;
  private:
    //! boolean which indicate if boundary condition for pressure is apply
    ElementField m_elemFieldAdv;
    ElementField m_elemFieldPress;
    PetscMatrix m_matrix;
    bool m_buildTeporaryMatrix;
    ElementField m_elemFieldDOF;
  };
}

#endif
