//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _LINEARPROBLEMHARMONICEXTENSION_HPP
#define _LINEARPROBLEMHARMONICEXTENSION_HPP

// System includes
#include <cmath>
#include <vector>

// External includes

// Project includes
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "Solver/bdf.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/linearProblem.hpp"

namespace felisce {
  /*!
    \file linearProblemHarmonicExtension.hpp

    \date May 2013
    \brief Matrix and rhs for the Poisson solver.
  */

  class LinearProblemHarmonicExtension:
    public LinearProblem {
  public:
    LinearProblemHarmonicExtension();
    ~LinearProblemHarmonicExtension() override;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void userFinalizeEssBCTransient() override;
    void readMatchFile_ALE(ListVariable listVariable, const std::string& fileName);
    void identifyIdDofToMatch_ALE(Dof& dof);
    void finalizeEssBCTransientDerivedProblem() override;
    void readDataDisplacement(std::vector<IO::Pointer>& io, double ReadTime);
    //Static part of the matrix in _A
    void copyMatrixRHS() override;
    void addMatrixRHS() override;

    felInt  totNumDofToMatch() const { return m_totNumDofToMatch; }

    inline PetscVector& HarmExtSol() { return m_HarmExtSol; }

    virtual void userStiffnessParameter(felInt iel, double& mu, double& lambda);

  protected:
    CurrentFiniteElement* m_fe;
    felInt m_iDisplacement;
    PetscVector m_HarmExtSol;

  private:
    PetscMatrix m_matrix;
    bool m_buildTeporaryMatrix;

    double m_scaleCoeff;
    // for matching
    felInt  m_totNumNodeToMatch;
    felInt  m_totNumDofToMatch;
    felInt  m_numDofBySupportToMatch;
    std::vector<int> m_listOfDofToMatch;
    felInt* m_listOfNodeToMatch;

    std::vector<double> m_vectorDisp;
    std::vector<double> m_oldVectorDisp;
    std::vector<double> m_oldOldVectorDisp;
    int m_oldVectorDispIteration;

    std::vector<felInt> stiffenElementIds;
    void initializeStiffnessVector();
  };
}

#endif
