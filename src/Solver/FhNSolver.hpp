//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    C. Corrado
//

#ifndef _FHNSOLVER_HPP
#define _FHNSOLVER_HPP

// System includes

// External includes

// Project includes
#include "Solver/ionicSolver.hpp"
#include "Solver/cardiacFunction.hpp"

/*!
 \class FhNSolver
 \brief Fitzhugh Nagumo Ionic model

 \c FhNSolver is the class implementing the Fitzhugh Nagumo Ionic model:

 \c  dw/dt = eps * (u*gamma - beta*w), w(t=0) = 0

 \c I_{ion} = f_0 * u * (u-alpha) * (1-u) - w

 \c This class inherits from ionicSolver

 \author  Cesare Corrado
 */


namespace felisce {
  class FhNSolver:
    public IonicSolver {
  public:
    //!Constructor.
    FhNSolver(FelisceTransient::Pointer fstransient);
    //!Destructor.
    ~FhNSolver() override;
    //!Compute RHS in EDO.
    void computeRHS() override;
    //!Solve the EDO.
    void solveEDO() override;
    //!Computation of ionic current from EDO solution.
    void computeIon() override;


    // functor and acces to variable properties fhn

    inline const std::vector<double>& f0Par() const {
      return m_f0Par;
    }
    inline std::vector<double>& f0Par() {
      return m_f0Par;
    }

    virtual inline const HeteroSparFHN& fctSpar() const {
      return m_heteroSpar;
    }
    virtual inline HeteroSparFHN& fctSpar() {
      return m_heteroSpar;
    }

  protected:
    HeteroSparFHN m_heteroSpar;
    std::vector<double> m_f0Par;

  };
}

#endif
