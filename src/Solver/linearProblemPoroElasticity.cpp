//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemPoroElasticity.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"

namespace felisce {
  LinearProblemPoroElasticity::LinearProblemPoroElasticity():
    LinearProblem("Linear Poro Elasticity",/*number of matrices*/2) {

    // Default value for the initial (constant) pressure
    m_pInit=0;

    //========Initialization of the PetscVectors=============//
    std::vector<std::string> listPar,listSeq;
    
    listPar.emplace_back("solutionPreviousTimeStep");
    listSeq.emplace_back("solutionPreviousTimeStep");
    
    listPar.emplace_back("filtrationVelocity");
    listSeq.emplace_back("filtrationVelocity");

    // We do not need the sequential counterpart.
    listPar.emplace_back("massTimesPressureGradient");

    listPar.emplace_back("pressureGradient");
    listSeq.emplace_back("pressureGradient");

    // We do not need the sequential counterpart;
    listPar.emplace_back("anisotropicDirection");
    
    for( std::size_t k(0); k<listPar.size();++k) {
      m_vecs.Init(listPar[k]);
    }
    for( std::size_t k(0); k<listSeq.size();++k) {
      m_seqVecs.Init(listSeq[k]);
    }

    
    // Default value
    m_matricesForFiltrationVelocityBuilt=false;

    //==============Scaling the value of the boundary conditions========// 
    // Not tested too much.
    if ( FelisceParam::instance().scalePressureEq ) {
      int c(0);
      for ( std::size_t k(0); k<FelisceParam::instance().variable.size(); ++k) {
        std::cout<<"Considering BC number: "<<k
                 <<" var: "<<FelisceParam::instance().variable[k]
                 <<" comp: "<<FelisceParam::instance().component[k]
                 <<" type: "<<FelisceParam::instance().type[k]
                 <<" value: "<<FelisceParam::instance().value[k]
                 <<std::endl;
        int n(2);
        if ( FelisceParam::instance().component[k] == "Comp123" )
          n=3;
        if ( FelisceParam::instance().component[k] == "Comp1" ||
             FelisceParam::instance().component[k] == "CompNA" )
          n=1;
        
        if ( FelisceParam::instance().variable[k] == "pressure" ) {
          if ( FelisceParam::instance().type[k] == "Neumann" ) {
            std::cout<<"Rescaling "<<k<<"th bc. Values from "<<c<<" to "<<c+n-1<<std::endl;
            for ( int j(0); j<n; ++j ) {
              std::cout<<FelisceParam::instance().value[c+j]<<" --> ";
              FelisceParam::instance().value[c+j] *= FelisceParam::instance().timeStep*FelisceParam::instance().biotModulus;
              std::cout<<FelisceParam::instance().value[c+j]<<std::endl;
            }
          } else {
            std::cout<<"Bc n."<<k<<" was not rescaled because it was not of type neumann"<<std::endl;
          }
        }
        c=c+n;
      }
    }
  }
  void LinearProblemPoroElasticity::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm,doUseSNES);

    //=================Setting the physical parameters=================//
    m_fstransient=fstransient;

    if ( FelisceParam::instance().young < 0 || FelisceParam::instance().poisson < 0 ) {
      m_mu = FelisceParam::instance().mu_lame;
      m_lambda = FelisceParam::instance().lambda_lame;
    } else {
      m_mu = FelisceParam::instance().young/2/(1+FelisceParam::instance().poisson);
      m_lambda = FelisceParam::instance().young*FelisceParam::instance().poisson/(1.+FelisceParam::instance().poisson)/(1.-2.*FelisceParam::instance().poisson);
    }
    m_deltaT = FelisceParam::instance().timeStep;
    m_b      = FelisceParam::instance().biotCoefficient;
    m_M      = FelisceParam::instance().biotModulus;    
    m_k0     = FelisceParam::instance().isoPermeab;
    m_k1     = FelisceParam::instance().anisoPermeab;

    this->displayPoroElasticProperties();
   

    //================Initializing variables and unknowns=============//
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;

    // Displacement
    listVariable.push_back(displacement);
    listNumComp.push_back(this->dimension());
    // Pore-pressure
    listVariable.push_back(pressure);
    listNumComp.push_back(1);
    
    userAddOtherVariables(listVariable,listNumComp);
    
    // Declare them as unknown of the linear system
    m_listUnknown.push_back(displacement);
    m_listUnknown.push_back(pressure);

    // Declare them as physical variable
    this->definePhysicalVariable(listVariable,listNumComp);

    // Save the variable id of the displacement and of the pressure
    m_iDisplacement = this->listVariable().getVariableIdList(displacement);
    m_iPressure     = this->listVariable().getVariableIdList(pressure);
  }

  void LinearProblemPoroElasticity::displayPoroElasticProperties() const {
    // drained parameters
    double shearD   = m_mu;
    double lambdaD  = m_lambda;
    double bulkD    = lambdaD+2./3.*shearD;
    double poissonD = (3*bulkD-2*shearD)/2/(3*bulkD+shearD);
    double youngD   = 9*bulkD*shearD/(3*bulkD+shearD);

    // undrained parameters
    double shearU   = shearD;
    double lambdaU  = lambdaD + m_b*m_b*m_M;
    double bulkU    = bulkD    + m_b*m_b*m_M;
    double poissonU = lambdaU / (2*(lambdaU+shearD));
    double youngU   = (1+poissonU)/(1+poissonD)*youngD;

    std::stringstream msg;
    msg<<"========================================" << std::endl;
    msg<<"--- DRAINED PROPERTIES (p=0):" << std::endl;
    msg<<" Shear Modulus (G or mu): "<< shearD << std::endl;
    msg<<" Lame's coefficient (lambda): "<< lambdaD << std::endl;
    msg<<" Bulk modulus (K): "<< bulkD << std::endl;
    msg<<" Young Modulus: " << youngD << std::endl;
    msg<<" Poisson ratio: "<< poissonD << std::endl;
    
    msg<<"--- UNDRAINED PROPERTIES (no extra mass):" << std::endl;
    msg<<" Shear Modulus (G or mu): "<< shearU << std::endl;
    msg<<" Lame's coefficient (lambda): "<< lambdaU << std::endl;
    msg<<" Bulk modulus (K): "<< bulkU << std::endl;
    msg<<" Young Modulus: " << youngU << std::endl;
    msg<<" Poisson ratio: "<< poissonU << std::endl;

    msg<<"--- OTHER PARAMETERS-----"<<std::endl;
    msg<<" Time step: "<< m_deltaT << std::endl;
    msg<<" Biot coefficient(b): "<< m_b << std::endl;
    msg<<" Biot modulus (M): "<< m_M << std::endl;
    msg<<" Isotropic permeability (k0)"<< m_k0<<std::endl;
    msg<<" Ansotropic permeability (k1)"<< m_k1<<std::endl;

    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
  }
  void LinearProblemPoroElasticity::initPerElementType(ElementType /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/) {
    // Pointers to the current finite element.
    m_feDisp = m_listCurrentFiniteElement[m_iDisplacement];
    m_fePre  = m_listCurrentFiniteElement[m_iPressure];

    // Element fields to store the old values of displacement and pore-pressure
    m_elemFieldDisplacement.initialize(DOF_FIELD,*m_feDisp,this->dimension());
    m_elemFieldPressure.initialize(DOF_FIELD,*m_fePre,1);
  }

  void LinearProblemPoroElasticity::advanceInTime() {

    // User dependent computations.
    userPostProcessing();

    if ( FelisceParam::instance().exportFiltrationVelocity) {
      // Computation of filtration velocity.
      if ( ! m_matricesForFiltrationVelocityBuilt ) {
        this->assembleMatricesForFiltrationVelocity();
        m_matricesForFiltrationVelocityBuilt = true;
      }    
      this->computeFiltrationVelocity();
    }
    // Move forward
    m_vecs.Get("solutionPreviousTimeStep").copyFrom(this->solution());
    this->gather("solutionPreviousTimeStep");
  }
  
  void LinearProblemPoroElasticity::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS flagMatrixRHS) {

    // UPDATE PHASE:
    // -- of the finite element
    m_feDisp->updateFirstDeriv(0, elemPoint);
    m_fePre->updateFirstDeriv(0, elemPoint);
    // -- of the anisotropic permeability direction
    updateAnisotropicDirection(elemPoint);
    // -- of the elementfields
    if (m_fstransient->iteration == 1) { // Setting intial condition on the pressure
      m_elemFieldPressure.setValue(m_pInit,0);
    } else {
      m_elemFieldPressure.setValue(m_seqVecs.Get("solutionPreviousTimeStep") ,*m_fePre,iel,m_iPressure,m_ao,dof());
    }
    m_elemFieldDisplacement.setValue(m_seqVecs.Get("solutionPreviousTimeStep"),*m_feDisp,iel,m_iDisplacement,m_ao,dof());
    
    
    // Wether the pressure equation should be multiplied by (dt*M) or by one.
    double scalingCoeff(1.);
    if ( FelisceParam::instance().scalePressureEq )
      scalingCoeff = m_deltaT*m_M;
    
    felInt pressureBlock( this->dimension() );
    felInt displacementBlock(0);
    if ( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
      if ( m_fstransient->iteration == 0 ) {
        std::size_t idMat = 1;// Everything should be placed in the static matrix (idMat=1)
        //! ==================================DISPLACEMENT EQUATION=======================================================
        //! Displacement equation: quasi-static, no mass here!
        //! Divergence of the elastic component of the stress tensor just as in linear elasticity
        m_elementMat[idMat]->eps_phi_i_eps_phi_j( 2*m_mu,   *m_feDisp, /*iblock*/displacementBlock, /*jblock*/displacementBlock, /*numOfBlockRepetitions*/this->dimension());
        m_elementMat[idMat]->div_phi_j_div_phi_i( m_lambda, *m_feDisp, /*iblock*/displacementBlock, /*jblock*/displacementBlock, /*numOfBlockRepetitions*/this->dimension());
        //! now the pressure part
        m_elementMat[idMat]->psi_j_div_phi_i(-m_b,*m_feDisp,*m_fePre , /*iblock*/displacementBlock, /*jblock*/pressureBlock);
      
        //! ==================================PRESSURE EQUATION===========================================================
        //! ------- First term coming from the time derivative of the divergence of the displacement
        m_elementMat[idMat]->psi_i_div_phi_j(scalingCoeff*m_b/m_deltaT,*m_feDisp,*m_fePre, /*iblock*/pressureBlock,/*jblock*/displacementBlock);
        //! ------- First term coming from the time derivative of the pressure
        m_elementMat[idMat]->phi_i_phi_j(scalingCoeff/(m_M*m_deltaT),*m_fePre,/*iblock*/pressureBlock,/*jblock*/pressureBlock,/*numOfBlockRepetitions*/1);
      
        // Pressure equation: laplacian terms.
        // Anisotropic part of the diffusive term
        m_elementMat[idMat]->tau_grad_phi_i_tau_grad_phi_j(scalingCoeff*m_k1,m_anisotropicDir,*m_fePre,/*iblock*/pressureBlock,/*jblock*/pressureBlock,/*nbOfBlockRep*/1,/*domain dim*/this->dimension());
        // Isotropic part
        m_elementMat[idMat]->grad_phi_i_grad_phi_j(scalingCoeff*m_k0,*m_fePre,/*iblock*/pressureBlock, /*jblock*/pressureBlock,/*numOfBlockRepetitions*/1);
      }
    }
    
    // Right hand side:
    if ( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
      // Pressure equation:
      // ------- Second term coming from the time derivative of the divergence of the displacement
      m_elementVector[0]->div_f_phi_i(scalingCoeff*m_b/m_deltaT, *m_feDisp,*m_fePre,m_elemFieldDisplacement,/*iblock*/pressureBlock);
      // ------- Second term coming from the time derivative of the pressure                         
      m_elementVector[0]->source(scalingCoeff/(m_M*m_deltaT), *m_fePre,m_elemFieldPressure,/*iblock*/pressureBlock,/*numOfBlockRepetitions*/1);
    }
    // OTHER TERMS:
    // Mass source and external load should be coded in userComputeElementArray
    // Boundary terms such as Neumann force terms should be standard and therefore implemented in linearProblem.cpp
    // Pay attention:
    //   if the pressure equation is multiplied by m_M you have to be consistent when coding the user files.
  }
  void LinearProblemPoroElasticity::updateAnisotropicDirection(const std::vector<Point*>& /*elemPoint*/) {
    // By default there is no anisotropic permeability direction.
    // --> Customize this function in the user
    if ( m_anisotropicDir.size() == 0) {
      m_anisotropicDir.resize(this->dimension()*m_fePre->numDof());
    } else {
      for (std::size_t i(0); i < m_anisotropicDir.size(); ++i) {
        m_anisotropicDir[i]=0.0;
      }
    }
  }
  // Exporting a zero-initial filtration velocity.
  void LinearProblemPoroElasticity::exportInitialFiltrationVelocity(std::vector<IO::Pointer>& io) {
    if(FelisceParam::instance().exportFiltrationVelocity) {
      PetscVector zero;
      zero.duplicateFrom(this->solution());
      zero.zeroEntries();
      std::stringstream msg;
      msg<<"Writing filtration velocity"<<std::endl; 
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      this->writeSolutionFromVec(zero, MpiInfo::rankProc(), io, m_fstransient->time, m_fstransient->iteration, std::string("filtrationVelocity"));
      // this->writeSolutionFromVec( zero, MpiInfo::rankProc(), io, m_fstransient->time, m_fstransient->iteration, std::string("anisotropicDirection"));
    }
  }
  
  void LinearProblemPoroElasticity::exportFiltrationVelocity(std::vector<IO::Pointer>& io) {
    if( FelisceParam::instance().exportFiltrationVelocity ) {
      std::stringstream msg;
      msg<<"Writing filtration velocity"<<std::endl; 
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      this->writeSolutionFromVec(m_seqVecs.Get("filtrationVelocity"),
                                 MpiInfo::rankProc(),
                                 io,
                                 m_fstransient->time,
                                 m_fstransient->iteration,
                                 std::string("filtrationVelocity"));      
      // this->writeSolutionFromVec(m_vecs.Get("anisotropicDirection"),
      //                            MpiInfo::rankProc(),
      //                            io,
      //                            m_fstransient->time,
      //                            m_fstransient->iteration,
      //                            std::string("anisotropicDirection"));
    }
  }

  void LinearProblemPoroElasticity::assembleMatricesForFiltrationVelocity() {  
    // Vector/Matrices intialization
    m_mass.duplicateFrom(matrix(0),MAT_DO_NOT_COPY_VALUES);
    m_mass.zeroEntries();
    m_derivative.duplicateFrom(matrix(0),MAT_DO_NOT_COPY_VALUES);
    m_derivative.zeroEntries();

    const auto& r_instance = FelisceParam::instance(this->instanceIndex());
    const std::string solver         = r_instance.solver[m_identifier_solver];
    const std::string preconditioner = r_instance.preconditioner[m_identifier_solver];
    const double relativeTolerance   = r_instance.relativeTolerance[m_identifier_solver];
    const double absoluteTolerance   = r_instance.absoluteTolerance[m_identifier_solver];
    const int    maxIteration        = r_instance.maxIteration[m_identifier_solver];
    const int    gmresRestart        = r_instance.gmresRestart[m_identifier_solver];
    const bool   initPrevSolution    = r_instance.initSolverWithPreviousSolution[m_identifier_solver];
    m_kspForFiltrationVelocity->init();
    m_kspForFiltrationVelocity->setKSPandPCType(solver, preconditioner);
    m_kspForFiltrationVelocity->setTolerances(relativeTolerance, absoluteTolerance, maxIteration, gmresRestart);
    m_kspForFiltrationVelocity->setKSPOptions(solver, initPrevSolution);

    // Loop over the domain
    // use to define a "global" numbering of element in the mesh.
    felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      ElementType eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }
    
    std::vector<Point*> elemPoint;
    std::vector<felInt> elemIdPoint;
    felInt ielSupportDof;

    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      ElementType eltType =  bagElementTypeDomain[i];
      
      // resize array.
      int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      defineFiniteElement(eltType);
      initElementArray();
      allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
      
      // virtual function use in derived problem to allocate elemenField necessary.
      initPerElementType(eltType, FlagMatrixRHS::only_matrix);
      // second loop on region of the mesh.
      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin();
          itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        // int currentLabel = itRef->first;
        felInt numEltPerLabel = itRef->second.second;
        // Third loop on element in the region with the type: eltType. ("real" loop on elements).
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);

          // FE update
          m_listCurrentFiniteElement[1/*pressure*/]->updateFirstDeriv(0, elemPoint);

          // MASS
          m_elementMat[0]->zero();
          m_elementMat[0]->phi_i_phi_j(1., *m_listCurrentFiniteElement[1],0,0,this->dimension()+1);
          setValueCustomMatrix(ielSupportDof,ielSupportDof,m_mass);

          // Derivative
          m_elementMat[0]->zero();
          for (int h(0); h<this->dimension();++h) {
            m_elementMat[0]->dpsi_j_dh_psi_i(1.0,*m_listCurrentFiniteElement[1],*m_listCurrentFiniteElement[1], h, h, this->dimension());
          }
          setValueCustomMatrix(ielSupportDof,ielSupportDof,m_derivative);
          
          numElement[eltType]++;
        }
      }
      desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
    }
    m_mass.assembly(MAT_FINAL_ASSEMBLY);
    m_derivative.assembly(MAT_FINAL_ASSEMBLY);

    const std::string preconditionerOption = r_instance.setPreconditionerOption[m_identifier_solver];
    m_kspForFiltrationVelocity->setKSPOperator(m_mass, preconditionerOption);
  }
  
  void LinearProblemPoroElasticity::computeFiltrationVelocity() {
    std::stringstream msg;
    msg<<"Solution of the linear system to compute the filtration velocity: "<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    mult(m_derivative,this->solution(),m_vecs.Get("massTimesPressureGradient")); // aux_i = < \nabla p, \phi_i >
    
    const auto& r_instance = FelisceParam::instance(this->instanceIndex());
    m_kspForFiltrationVelocity->solve(m_vecs.Get("massTimesPressureGradient"), m_vecs.Get("pressureGradient"), r_instance.linearSolverVerbose);
    this->gather("pressureGradient");
    
    // correctly Perform the product v=Kv;
    felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      ElementType eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }
    
    std::vector<Point*> elemPoint;
    std::vector<felInt> elemIdPoint;
    felInt ielSupportDof;

    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      ElementType eltType =  bagElementTypeDomain[i];
      
      // resize array.
      int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      defineFiniteElement(eltType);
      initElementArray();
      allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
      
      // virtual function use in derived problem to allocate elemenField necessary.
      initPerElementType(eltType, FlagMatrixRHS::only_matrix);
      // second loop on region of the mesh.
      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin();
          itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        // int currentLabel = itRef->first;
        felInt numEltPerLabel = itRef->second.second;
        // Third loop on element in the region with the type: eltType. ("real" loop on elements).
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);         
          this->updateAnisotropicDirection(elemPoint);

          m_elementVector[0]->zero();
          felInt counter(0);//we assume the displacement is the first unknown
          for (int icomp(0); icomp<this->dimension();++icomp) {
            for ( int idof(0); idof<m_fePre->numDof(); ++idof) {
              m_elementVector[0]->vec()(counter) = m_anisotropicDir[this->dimension()*idof+icomp];
              counter++;
            }
          }
          setValueCustomVector(ielSupportDof,INSERT_VALUES,m_vecs.Get("anisotropicDirection"));
          
          m_elementVector[0]->zero();
          counter=0;//we assume the displacement is the first unknown
          for (int icomp(0); icomp<this->dimension();++icomp) {
            for ( int idof(0); idof<m_fePre->numDof(); ++idof) {
              //identify physical point
              double x,y,z;
              if ( (std::size_t)idof >= elemPoint.size() ) {                // ONLY FOR P2, assuming prisms
                x=0.5*(elemPoint[idof-3]->x()+elemPoint[idof-6]->x());
                y=0.5*(elemPoint[idof-3]->y()+elemPoint[idof-6]->y());
                z=0.5*(elemPoint[idof-3]->z()+elemPoint[idof-6]->z());
              } else {      
                x=elemPoint[idof]->x();
                y=elemPoint[idof]->y();
                z=elemPoint[idof]->z();
              }
              for (int jcomp(0); jcomp<this->dimension();++jcomp) {
                //compute -phi(idof)*tensor(idof)_{icomp,jcomp}
                double val=m_k1*m_anisotropicDir[this->dimension()*idof+icomp]*m_anisotropicDir[this->dimension()*idof+jcomp];
                if (icomp==jcomp)
                  val+=m_k0;
                val = -val*this->userEvaluatePorosity(x,y,z);

                //retrieving the good value from pressureGradient
                double valOfGradP;
                felInt gdof;
                dof().loc2glob(ielSupportDof,idof,0/*idVariable*/,jcomp,gdof);
                AOApplicationToPetsc(ao(),1,&gdof);
                m_seqVecs.Get("pressureGradient").getValues(1,&gdof,&valOfGradP);

                //summing
                m_elementVector[0]->vec()(counter) += val*valOfGradP;
              }
              counter++;
            }
          }
          setValueCustomVector(ielSupportDof,INSERT_VALUES,m_vecs.Get("filtrationVelocity"));
          numElement[eltType]++;
        }
      }
      desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
    }
    m_vecs.Get("filtrationVelocity").assembly();
    this->gather("filtrationVelocity");    
  }

  #ifdef FELISCE_WITH_CVGRAPH
  /*! \brief this function assemble the mass matrix at the boundary.
   *  And the KSP
   */
  void LinearProblemPoroElasticity::assembleMassBoundaryAndInitKSP( std::size_t iBD ) {
    LinearProblem::assembleMassBoundaryAndInitKSP( &LinearProblemPoroElasticity::massMatrixComputer,
                                                   FelisceParam::instance().interfaceLabels,
                                                   &LinearProblemPoroElasticity::initPerETMass,
                                                   &LinearProblemPoroElasticity::updateFE,
                                                   iBD);
  }



  /*! \brief Function to assemble the mass matrix
   *  Function to be called in the assemblyLoopBoundaryGeneral
   */
  void LinearProblemPoroElasticity::massMatrixComputer(felInt ielSupportDof) {
    this->m_elementMatBD[0]->zero();
    this->m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFeDisp,/*iblock*/0,/*iblock*/0,this->dimension());
    this->setValueMatrixBD(ielSupportDof);
  }
  void
  LinearProblemPoroElasticity::initPerETMass() {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
    for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    
    m_curvFeDisp = this->m_listCurvilinearFiniteElement[ m_iDisplacement ];
  }
  void
  LinearProblemPoroElasticity::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
    m_curvFeDisp->updateMeasNormal(0, elemPoint);  
  }
  #endif
}
