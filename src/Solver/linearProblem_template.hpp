//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef LINEARPROBLEM_TPP
#define LINEARPROBLEM_TPP

#ifndef LINEARPROBLEM_HPP
#error Include linearProblem.hpp instead
#endif

namespace felisce 
{
  /*!
    \brief This function is a GENERIC ASSEMBLY LOOP on the boundary elements on a certain list of labels

    \param[in] functionOfTheLoop is the function that actually do the computation element-wise
    \param[in] labels a std::vector containing a list of integer labels where we want to loop
    \param[in] a function to initialize for each element-type (typically to initialize the FE)
    \param[in] updatefunction it could be implemented into the functionOfTheLoop, but like this is more general when dealing with duplicated supportDof


    For several examples on how to use this class check linearProblemNSSimplifiedFSI.hpp


    The template parameter theClass is going to be the class where the different functions are implemented.

    // Nerd alert!
    | I think that it is possible to avoid using a template by using some new binding techniques of c++11 to wrap
    | the function of the class into functions not belonging to any particular class. (i.e. by a creating a free-function from a class function)
    |
    | The problem is that I can not simply declare  void (*functionOfTheLoop)( felInt ), because the class of a
    | a member function is like the first argument of the function itself and it therefore alters the signature (I guess..)
    |
    | The main drawback of this implementation is that I am not sure you can call this function directly with
    | methods of different class, maybe if they belong to the same hierarchy is going to work.
    //
    */
  template<class theClass>
  void LinearProblem::assemblyLoopBoundaryGeneral( void (theClass::*functionOfTheLoop)( felInt ),
                                                   const std::vector<felInt> & labels,
                                                   void (theClass::*initPerElementType)(),
                                                   void (theClass::*updateFunction)(const std::vector<Point*>&,const std::vector<int>&)) {
    
    // some useful vector
    std::vector<Point*> elemPoint;
    std::vector<felInt> elemIdPoint;
    std::vector<felInt> vectorIdSupport;

    //to improve readability
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();

    //initialization of the counter numElement
    felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ]; //std::vector of size 23 number of the different type of elements
    for (int ityp=0; ityp < GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      numElement[ityp]=0;
    }

    //========loop on the element type of boundary (e.g. triangles..not a real loop)
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {

      ElementType eltType =  bagElementTypeDomainBoundary[i];
      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();

      (static_cast<theClass*>(this)->*initPerElementType)();

      int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      //=======loop on subregion of the surface, with different labels.
      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin();
          itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        int currentLabel = itRef->first;
        int numEltPerLabel = itRef->second.second;

        if ( std::find( labels.begin(), labels.end(), currentLabel) != labels.end() ) {
          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            // return each id of point of the element and coordinate in two arrays:
            // elemPoint and elemIdPoint.
            setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            (static_cast<theClass*>(this)->*updateFunction)(elemPoint,elemIdPoint);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              felInt ielSupportDof = vectorIdSupport[it];

              (static_cast<theClass*>(this)->*functionOfTheLoop)(ielSupportDof);
            }
            numElement[eltType]++;
          }
        } else {
          numElement[eltType] += numEltPerLabel;
        }
      }
    }
  }


  /*! \brief this function assemble the mass matrix at the boundary.
   *  And the KSP
   */
  #ifdef FELISCE_WITH_CVGRAPH
  template<class theClass>
  void LinearProblem::assembleMassBoundaryAndInitKSP( void (theClass::*functionOfTheLoop)( felInt ),
                                                      const std::vector<felInt> & labels,
                                                      void (theClass::*initPerElementType)(),
                                                      void (theClass::*updateFunction)(const std::vector<Point*>&,const std::vector<int>&),
                                                      std::size_t iConn ) 
  {
    this->m_dofBD[iConn].allocateMatrixOnBoundary( m_massBD[iConn] );
    m_auxiliaryMatrix=m_massBD[iConn];
    m_auxiliaryDofBD = &(this->dofBD(iConn));
    if ( m_auxiliaryDofBD->hasDofsOnBoundary() ) {
      this->assemblyLoopBoundaryGeneral( functionOfTheLoop,
                                         labels,
                                         initPerElementType,
                                         updateFunction);
      m_globPosColumn.clear();
      m_globPosRow.clear();
      m_matrixValues.clear();

      m_massBD[iConn].assembly(MAT_FINAL_ASSEMBLY);
      if ( FelisceParam::instance().exportMassMatrix ) {
        m_massBD[iConn].saveInBinaryFormat( this->m_dofBD[iConn].comm(), "Mass", FelisceParam::instance().resultDir);
      }
    }
    // Better to put it since m_auxiliaryMatrix will stay alive as long as the linear problem,
    // while we would like to deallocate this matrix when the mass matrix gets out of scope
    m_auxiliaryMatrix.destroy();
    if ( m_auxiliaryDofBD->hasDofsOnBoundary() ) {
      m_kspMassBD[iConn].init(this->dofBD(iConn).comm());

      // Setting the type, the GMRES restart, the PC type, tolerances,...
      const auto& r_instance = FelisceParam::instance(this->instanceIndex());
      const std::string solver         = r_instance.solver[m_identifier_solver];
      const std::string preconditioner = r_instance.preconditioner[m_identifier_solver];
      const double relativeTolerance   = r_instance.relativeTolerance[m_identifier_solver];
      const double absoluteTolerance   = r_instance.absoluteTolerance[m_identifier_solver];
      const int    maxIteration        = r_instance.maxIteration[m_identifier_solver];
      const int    gmresRestart        = r_instance.gmresRestart[m_identifier_solver];
      const bool   initPrevSolution    = r_instance.initSolverWithPreviousSolution[m_identifier_solver];
      const std::string preconOptions  = r_instance.setPreconditionerOption[m_identifier_solver];
      m_kspMassBD[iConn].setKSPandPCType(solver, preconditioner);
      m_kspMassBD[iConn].setTolerances(relativeTolerance, absoluteTolerance, maxIteration, gmresRestart);
      m_kspMassBD[iConn].setKSPOptions(solver, initPrevSolution);
      m_kspMassBD[iConn].setKSPOperator(m_massBD[iConn], preconOptions); 
    }
  }


  /*! \brief this function assemble the mass matrix at the boundary.
   *
   */
  template<class theClass>
  void LinearProblem::assembleMassBoundaryOnly( void (theClass::*functionOfTheLoop)( felInt ),
                                                      const std::vector<felInt> & labels,
                                                      void (theClass::*initPerElementType)(),
                                                      void (theClass::*updateFunction)(const std::vector<Point*>&,const std::vector<int>&),
                                                DofBoundary &dofBD, PetscMatrix& massMatrix ) {
    dofBD.allocateMatrixOnBoundary( massMatrix );
    m_auxiliaryMatrix = massMatrix;
    m_auxiliaryDofBD = &dofBD;
    if ( dofBD.hasDofsOnBoundary() ) {
      this->assemblyLoopBoundaryGeneral( functionOfTheLoop,
                                         labels,
                                         initPerElementType,
                                         updateFunction);
      m_globPosColumn.clear();
      m_globPosRow.clear();
      m_matrixValues.clear();

      massMatrix.assembly(MAT_FINAL_ASSEMBLY);
    }
    // Better to put it since m_auxiliaryMatrix will stay alive as long as the linear problem,
    // while we would like to deallocate this matrix when the mass matrix gets out of scope
    m_auxiliaryMatrix.destroy();
  }
  #endif


  template <class FunctorXYZ>
  inline void LinearProblem::set_U_0(const FunctorXYZ& functorXYZ, int iUnknown, int iComponent) {
    felInt dof = 0;
    Point pt;
    std::set<felInt> allDof;
    std::vector<double> value;
    felInt sizeDof = 0;
    felInt sizeUpdate = 0;
    felInt iPosGlob;
    double val;

    int idVar = m_listUnknown.idVariable(iUnknown);

    for ( felInt iel = 0; iel < (felInt)supportDofUnknown(iUnknown).iEle().size() - 1; iel++) {
      for ( int iSupport = 0; iSupport <  supportDofUnknown(iUnknown).getNumSupportDof(iel); iSupport++) {
        m_dof.loc2glob(iel, iSupport, idVar, iComponent, dof);
        allDof.insert(dof);
        sizeUpdate = allDof.size();
        if ( sizeUpdate > sizeDof ) {
          pt = supportDofUnknown(iUnknown).listNode()[supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel] + iSupport ]];
          val = functorXYZ(pt.x(), pt.y(), pt.z());
          iPosGlob = dof;
          AOApplicationToPetsc(m_ao, 1, &iPosGlob);
          m_seqSol.setValues( 1, &iPosGlob, &val, INSERT_VALUES);
          m_sol.setValues( 1, &iPosGlob, &val, INSERT_VALUES);
        }
        sizeDof = allDof.size();
      }
    }

    m_seqSol.assembly();
    m_sol.assembly();
  }

  template <class FunctorXYZ>
  inline void LinearProblem::set_U_0_parallel(const FunctorXYZ& functorXYZ, int iUnknown, int iComponent) 
  {
    const int sizeLocal = m_numDofLocalUnknown[iUnknown];
    const int idVar = m_listUnknown.idVariable(iUnknown);
    const int numCompOfVariable = m_listVariable[idVar].numComponent();
    felInt indexGlobal;
    Point pt;

    std::vector<double> valueForPETSC(sizeLocal);
    std::vector<felInt> idGlobalValue(sizeLocal,0);
    std::vector<felInt> idLocalValue(sizeLocal);
    std::iota(idLocalValue.begin(), idLocalValue.end(), 0);

    ISLocalToGlobalMappingApply(m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown], sizeLocal, idLocalValue.data(), idGlobalValue.data());

    for (felInt i = iComponent; i < sizeLocal; i+=numCompOfVariable) {
      indexGlobal = idGlobalValue[i];
      AOPetscToApplication(m_ao,1,&indexGlobal);
      findCoordinateWithIdDof(indexGlobal,pt);
      valueForPETSC[i] = functorXYZ(pt.x(), pt.y(), pt.z());
    }

    m_seqSol.setValues(sizeLocal, idGlobalValue.data(), valueForPETSC.data(), ADD_VALUES);
    m_seqSol.assembly();
    m_sol.setValues(sizeLocal, idGlobalValue.data(), valueForPETSC.data(), ADD_VALUES);
    m_sol.assembly();
  }

  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z());
    }
  }

  // Warning: extraData size has to be numDof (not number of mesh points).
  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array, std::vector<double>& extraData) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z(),extraData[i]);
    }
  }

  // Warning: extraData size has to be numDof (not number of mesh points).
  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func, std::vector<int>& array, std::vector<double>& extraData) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z(),extraData[i]);
    }
  }

  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func,double time, std::vector<double>& array) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z(),time);
    }
  }

  // Warning: extraData size has to be numDof (not number of mesh points).
  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array, std::vector<double>& extraData,double time) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z(),extraData[i],time);
    }
  }

  // Warning: extraData size has to be numDof (not number of mesh points).
  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func,double time, std::vector<double>& array, std::vector<double>& extraData) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z(),time,extraData[i]);
    }
  }

  template <class Templ_functor> void LinearProblem::evalFunctionOnDof(const Templ_functor& func,int iComp, std::vector<double>& array) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(iComp,pt.x(),pt.y(),pt.z());
    }
  }

  template <class Templ_functor> void LinearProblem::evalFunctionWithCompOnDof(const Templ_functor& func,std::vector<double>& array) {
    Point pt;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      for( int iComp = 0; iComp < 3; iComp++) { // change '3' to a m_numComp
        array.push_back(func(iComp,pt.x(),pt.y(),pt.z()));
      }
    }
  }
}

#endif // LINEAR_PROBLEM_TPP
