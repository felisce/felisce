//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/bdf.hpp"

namespace felisce {
  Bdf::Bdf():
    m_order(0),
    m_numberOfComp(1),
    m_size(0),
    m_build_order_2(false),
    m_build_order_3(false),
    m_build_RHS(false) {

  }

  Bdf::~Bdf() {
    m_alpha.clear();
    m_beta.clear();
    for (felInt i=0 ; i< m_numberOfComp; i++) {
      if (m_build_order_2)
        m_sol_n_1[i].destroy();
      if (m_build_order_3)
        m_sol_n_2[i].destroy();
      if (m_build_RHS) {
        m_sol_n[i].destroy();
        m_rhs[i].destroy();
      }
    }
  }

  void Bdf::defineOrder(int n, int nComp) {
    m_order = n;
    m_numberOfComp=nComp;
    m_alpha.resize(n+1);
    m_beta.resize(n);
    m_sol_n.resize(nComp);
    m_sol_n_1.resize(nComp);
    m_sol_n_2.resize(nComp);
    m_rhs.resize(nComp);
    m_solExtrapol.resize(nComp);

    if ( n <= 0 || n > BDF_MAX_ORDER ) {
      FEL_ERROR(" We support BDF order from 1 to 3 \n");
    }

    switch (n) {
    case 1:
      m_alpha[0] = 1.0; // Backward Euler
      m_alpha[1] = 1.0;
      m_beta[0] = 1.0;// u^{n+1} \approx u^n
      break;
    case 2:
      m_alpha[0] = 3.0 / 2.0;
      m_alpha[1] = 2.0;
      m_alpha[2] = -1.0 / 2.0;
      m_beta[0] = 2.0;
      m_beta[1] = -1.0;
      break;
    case 3:
      m_alpha[0] = 11.0 / 6.0;
      m_alpha[1] = 3.0;
      m_alpha[2] = -3.0 / 2.0;
      m_alpha[3] = 1.0 / 3.0;
      m_beta[0] = 3.0;
      m_beta[1] = -3.0;
      m_beta[2] = 1.0;
      break;
    }
  }


  void Bdf::initialize(const PetscVector& sol_0) {
    switch (m_order) {
    case 1:
      //!Get size.
      sol_0.getSize(&m_size);
      m_sol_n[0].duplicateFrom(sol_0);
      //!Copy because you want to keep sol_0.
      m_sol_n[0].copyFrom(sol_0);

      m_rhs[0].duplicateFrom(sol_0);
      m_build_RHS = true;
      break;
    case 2:
      sol_0.getSize(&m_size);
      m_sol_n[0].duplicateFrom(sol_0);
      m_sol_n[0].copyFrom(sol_0);

      m_sol_n_1[0].duplicateFrom(sol_0);
      m_sol_n_1[0].copyFrom(sol_0);
      m_build_order_2 = true;

      m_rhs[0].duplicateFrom(sol_0);
      m_build_RHS = true;
      break;
    case 3:
      sol_0.getSize(&m_size);
      m_sol_n[0].duplicateFrom(sol_0);
      m_sol_n[0].copyFrom(sol_0);

      m_sol_n_1[0].duplicateFrom(sol_0);
      m_sol_n_1[0].copyFrom(sol_0);
      m_build_order_2 = true;

      m_sol_n_2[0].duplicateFrom(sol_0);
      m_sol_n_2[0].copyFrom(sol_0);
      m_build_order_3 = true;

      m_rhs[0].duplicateFrom(sol_0);
      m_build_RHS = true;
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
  }

  void Bdf::initialize(std::vector<PetscVector>& sol_0) {
    switch (m_order) {
    case 1:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        //!Get size.
        sol_0[i].getSize(&m_size);
        m_sol_n[i].duplicateFrom(sol_0[i]);
        //!Copy because you want to keep sol_0.
        m_sol_n[i].copyFrom(sol_0[i]);

        m_rhs[i].duplicateFrom(sol_0[i]);
      }
      m_build_RHS = true;
      break;
    case 2:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        sol_0[i].getSize(&m_size);
        m_sol_n[i].duplicateFrom(sol_0[i]);
        m_sol_n[i].copyFrom(sol_0[i]);

        m_sol_n_1[i].duplicateFrom(sol_0[i]);
        m_sol_n_1[i].copyFrom(sol_0[i]);
      }
      m_build_order_2 = true;
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_rhs[i].duplicateFrom(sol_0[i]);
      }
      m_build_RHS = true;
      break;
    case 3:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        sol_0[i].getSize(&m_size);
        m_sol_n[i].duplicateFrom(sol_0[i]);
        m_sol_n[i].copyFrom(sol_0[i]);

        m_sol_n_1[i].duplicateFrom(sol_0[i]);
        m_sol_n_1[i].copyFrom(sol_0[i]);
      }
      m_build_order_2 = true;
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n_2[i].duplicateFrom(sol_0[i]);
        m_sol_n_2[i].copyFrom(sol_0[i]);
      }
      m_build_order_3 = true;
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_rhs[i].duplicateFrom(sol_0[i]);
      }
      m_build_RHS = true;
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
  }


  void Bdf::initialize(const PetscVector& sol_0, const PetscVector& sol_1) {
    sol_0.getSize(&m_size);

    m_sol_n[0].duplicateFrom(sol_1);
    //VecSwap(m_sol_n,sol_1);
    m_sol_n[0].copyFrom(sol_1);

    m_sol_n_1[0].duplicateFrom(sol_0);
    //VecSwap(m_sol_n_1,sol_0);
    m_sol_n_1[0].copyFrom(sol_0);

    m_build_order_2 = true;

    m_rhs[0].duplicateFrom(sol_0);
    m_build_RHS = true;
  }


  void Bdf::initialize(std::vector<PetscVector>& sol_0, std::vector<PetscVector>& sol_1) {

    for (felInt i=0 ; i< m_numberOfComp; i++) {
      sol_0[i].getSize(&m_size);

      m_sol_n[i].duplicateFrom(sol_1[i]);
      m_sol_n[i].copyFrom(sol_1[i]);

      m_sol_n_1[i].duplicateFrom(sol_0[i]);
      m_sol_n_1[i].copyFrom(sol_0[i]);
    }
    m_build_order_2 = true;
    for (felInt i=0 ; i< m_numberOfComp; i++) {
      m_rhs[i].duplicateFrom(sol_0[i]);
    }
    m_build_RHS = true;
  }

  void Bdf::initialize(const PetscVector& sol_0, const PetscVector& sol_1, const PetscVector& sol_2) {

    sol_0.getSize(&m_size);

    m_sol_n[0].duplicateFrom(sol_2);
    //VecSwap(m_sol_n,sol_2);
    m_sol_n[0].copyFrom(sol_2);

    m_sol_n_1[0].duplicateFrom(sol_1);
    //VecSwap(m_sol_n_1,sol_1);
    m_sol_n_1[0].copyFrom(sol_1);

    m_sol_n_2[0].duplicateFrom(sol_0);
    //VecSwap(m_sol_n_2,sol_0);
    m_sol_n_2[0].copyFrom(sol_0);

    m_build_order_3 = true;

    m_rhs[0].duplicateFrom(sol_0);
    m_build_RHS = true;
  }

  void Bdf::initialize(std::vector<PetscVector>& sol_0, std::vector<PetscVector>& sol_1, std::vector<PetscVector>& sol_2) {

    for (felInt i=0 ; i< m_numberOfComp; i++) {
      sol_0[i].getSize(&m_size);

      m_sol_n[i].duplicateFrom(sol_2[i]);
      //VecSwap(m_sol_n,sol_2);
      m_sol_n[i].copyFrom(sol_2[i]);

      m_sol_n_1[i].duplicateFrom(sol_1[i]);
      //VecSwap(m_sol_n_1,sol_1);
      m_sol_n_1[i].copyFrom(sol_1[i]);

      m_sol_n_2[i].duplicateFrom(sol_0[i]);
      //VecSwap(m_sol_n_2,sol_0);
      m_sol_n_2[i].copyFrom(sol_0[i]);
    }
    m_build_order_3 = true;
    for (felInt i=0 ; i< m_numberOfComp; i++) {
      m_rhs[i].duplicateFrom(sol_0[i]);
    }
    m_build_RHS = true;
  }

  void Bdf::reinitialize(int order, const PetscVector& sol0, const PetscVector& sol1, const PetscVector& sol2) {
    // destroy everything
    m_alpha.clear();
    m_beta.clear();
    if (m_build_order_2)
      m_sol_n_1[0].destroy();
    if (m_build_order_3)
      m_sol_n_2[0].destroy();
    if (m_build_RHS) {
      m_sol_n[0].destroy();
      m_rhs[0].destroy();
    }

    m_build_order_2 = false;
    m_build_order_3 = false;
    m_build_RHS = false;

    // reallocate everything
    defineOrder(order);

    if(sol2.isNotNull())
      initialize(sol0, sol1, sol2);
    else if(sol1.isNotNull())
      initialize(sol0, sol1);
    else
      initialize(sol0);
  }

  void Bdf::update(PetscVector& new_sol_n) {
    /*switch (m_order) {
    case 1:
      sol_n[0].copyFrom(new_sol_n);
      break;
    case 2:
      sol_n_1[0].copyFrom(m_sol_n[0]);
      sol_n[0].copyFrom(new_sol_n);
      break;
    case 3:
      sol_n_2[0].copyFrom(m_sol_n_1[0]);
      sol_n_1[0].copyFrom(m_sol_n[0]);
      sol_n[0].copyFrom(new_sol_n);
      break;
    default:
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
      break;
      }*/

    // use m_build_order instead of m_order in case someone wants to initialize the bdf with more time step
    if(m_build_order_3) {
      m_sol_n_2[0].copyFrom(m_sol_n_1[0]);
      m_sol_n_1[0].copyFrom(m_sol_n[0]);
      m_sol_n[0].copyFrom(new_sol_n);
    } else if(m_build_order_2) {
      m_sol_n_1[0].copyFrom(m_sol_n[0]);
      m_sol_n[0].copyFrom(new_sol_n);
    } else if(m_build_RHS) {
      m_sol_n[0].copyFrom(new_sol_n);
    } else {
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
    }
  }

  void Bdf::update(std::vector<PetscVector>& new_sol_n) {
    /*switch (m_order) {
    case 1:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n[i].copyFrom(new_sol_n[i]);
      }
      break;
    case 2:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n_1[i].copyFrom(m_sol_n[i]);
        m_sol_n[i].copyFrom(new_sol_n[i]);
      }
      break;
    case 3:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n_2[i].copyFrom(m_sol_n_1[i]);
        m_sol_n_1[i].copyFrom(m_sol_n[i]);
        m_sol_n[i].copyFrom(new_sol_n[i]);
      }
      break;
    default:
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
      break;
      }*/

    // use m_build_order instead of m_order in case someone wants to initialize the bdf with more time step
    if(m_build_order_3) {
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n_2[i].copyFrom(m_sol_n_1[i]);
        m_sol_n_1[i].copyFrom(m_sol_n[i]);
        m_sol_n[i].copyFrom(new_sol_n[i]);
      }
    } else if(m_build_order_2) {
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n_1[i].copyFrom(m_sol_n[i]);
        m_sol_n[i].copyFrom(new_sol_n[i]);
      }
    } else if(m_build_RHS) {
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        m_sol_n[i].copyFrom(new_sol_n[i]);
      }
    } else {
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
    }
  }


  void Bdf::computeRHSTime(double dt, PetscVector& RHSTime) {
    RHSTime.zeroEntries();
    switch (m_order) {
    case 1:
      //RHSTime = m_alpha[0](1)*m_sol_n
      RHSTime.axpy(m_alpha[1]/dt,m_sol_n[0]);
      break;
    case 2:
      //RHSTime = m_alpha[0](1)*m_sol_n
      RHSTime.axpy(m_alpha[1]/dt,m_sol_n[0]);
      //RHSTime = m_alpha[0](1)*m_sol_n + m_alpha[0](2)*m_sol_n_1
      RHSTime.axpy(m_alpha[2]/dt,m_sol_n_1[0]);
      break;
    case 3:
      RHSTime.axpy(m_alpha[1]/dt,m_sol_n[0]);
      RHSTime.axpy(m_alpha[2]/dt,m_sol_n_1[0]);
      RHSTime.axpy(m_alpha[3]/dt,m_sol_n_2[0]);
      break;
    default:
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
      break;
    }
  }

  void Bdf::computeRHSTime(double dt, std::vector<PetscVector>& RHSTime) {
    for (felInt i=0 ; i< m_numberOfComp; i++)
      RHSTime[i].zeroEntries();
    switch (m_order) {
    case 1:
      //RHSTime = m_alpha[0](1)*m_sol_n
      for (felInt i=0 ; i< m_numberOfComp; i++)
        RHSTime[i].axpy(m_alpha[1]/dt,m_sol_n[i]);
      break;
    case 2:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        //RHSTime = m_alpha[0](1)*m_sol_n
        RHSTime[i].axpy(m_alpha[1]/dt,m_sol_n[i]);
        //RHSTime = m_alpha[0](1)*m_sol_n + m_alpha[0](2)*m_sol_n_1
        RHSTime[i].axpy(m_alpha[2]/dt,m_sol_n_1[i]);
      }
      break;
    case 3:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        RHSTime[i].axpy(m_alpha[1]/dt,m_sol_n[i]);
        RHSTime[i].axpy(m_alpha[2]/dt,m_sol_n_1[i]);
        RHSTime[i].axpy(m_alpha[3]/dt,m_sol_n_2[i]);
      }
      break;
    default:
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
      break;
    }
  }

  void Bdf::computeRHSTimeByPart(double dt, std::vector<PetscVector>& RHSTimeByPart) {
    if(m_order != static_cast<felInt>(RHSTimeByPart.size()))
      FEL_ERROR("The bdf order and the size of the rhs part std::vector are not the same");
    
    for(std::size_t i=0; i<RHSTimeByPart.size(); ++i)
      RHSTimeByPart[i].zeroEntries();
    
    switch (m_order) {
    case 1:
      // RHSTime[0] = m_alpha[0](1)*m_sol_n
      RHSTimeByPart[0].axpy(m_alpha[1]/dt, m_sol_n[0]);
      break;
      
    case 2:
      // RHSTime[0] = m_alpha[0](1)*m_sol_n
      RHSTimeByPart[0].axpy(m_alpha[1]/dt, m_sol_n[0]);
      
      //RHSTime[1] = m_alpha[0](2)*m_sol_n_1
      RHSTimeByPart[1].axpy(m_alpha[2]/dt, m_sol_n_1[0]);
      break;

    case 3:
      RHSTimeByPart[0].axpy(m_alpha[1]/dt, m_sol_n[0]);
      RHSTimeByPart[1].axpy(m_alpha[2]/dt, m_sol_n_1[0]);
      RHSTimeByPart[2].axpy(m_alpha[3]/dt, m_sol_n_2[0]);
      break;

    default:
      FEL_ERROR("BDF not yet implemented for order grater than 3.");
      break;
    }
  }
  

  void Bdf::computeRHSTime(double dt) {
    computeRHSTime(dt,m_rhs[0]);
  }

  // Evaluates extrapolation
  void Bdf::extrapolate( PetscVector& extrap) {
    extrap.zeroEntries();
    switch (m_order) {
    case 1:
      extrap.axpy(m_beta[0],m_sol_n[0]);
      break;
    case 2:
      extrap.axpy(m_beta[0],m_sol_n[0]);
      extrap.axpy(m_beta[1],m_sol_n_1[0]);
      break;
    case 3:
      extrap.axpy(m_beta[0],m_sol_n[0]);
      extrap.axpy(m_beta[1],m_sol_n_1[0]);
      extrap.axpy(m_beta[2],m_sol_n_2[0]);
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
  }

  void Bdf::extrapolate( std::vector<PetscVector>& extrap) {
    for (felInt i=0 ; i< m_numberOfComp; i++)
      extrap[i].zeroEntries();
    switch (m_order) {
    case 1:
      for (felInt i=0 ; i< m_numberOfComp; i++)
        extrap[i].axpy(m_beta[0],m_sol_n[i]);
      break;
    case 2:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        extrap[i].axpy(m_beta[0],m_sol_n[i]);
        extrap[i].axpy(m_beta[1],m_sol_n_1[i]);
      }
      break;
    case 3:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        extrap[i].axpy(m_beta[0],m_sol_n[i]);
        extrap[i].axpy(m_beta[1],m_sol_n_1[i]);
        extrap[i].axpy(m_beta[2],m_sol_n_2[i]);
      }
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
  }

  void Bdf::extrapolateByPart(std::vector<PetscVector>& partExtrap) {
    if(m_order != static_cast<felInt>(partExtrap.size()))
      FEL_ERROR("The bdf order and the size of the extrapolation part std::vector are not the same");
    
    for(std::size_t i=0; i<partExtrap.size(); i++)
      partExtrap[i].zeroEntries();
      
    switch (m_order) {
    case 1:
      partExtrap[0].axpy(m_beta[0], m_sol_n[0]);
      break;
    case 2:
      partExtrap[0].axpy(m_beta[0], m_sol_n[0]);
      partExtrap[1].axpy(m_beta[1], m_sol_n_1[0]);
      break;
    case 3:
      partExtrap[0].axpy(m_beta[0], m_sol_n[0]);
      partExtrap[1].axpy(m_beta[1], m_sol_n_1[0]);
      partExtrap[2].axpy(m_beta[2], m_sol_n_2[0]);
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
  }

  // evaluates time derivative std::vector a posteriori; to call before update
  void Bdf::time_der( double dt, const PetscVector& m_vecSol,  PetscVector& deriv  ) const {
    deriv.zeroEntries();
    deriv.axpy(m_alpha[0],m_vecSol);

    switch (m_order) {
    case 1:
      deriv.axpy(-1.0*m_alpha[1],m_sol_n[0]);
      break;
    case 2:
      deriv.axpy(-1.0*m_alpha[1],m_sol_n[0]);
      deriv.axpy(-1.0*m_alpha[2],m_sol_n_1[0]);
      break;
    case 3:
      deriv.axpy(-1.0*m_alpha[1],m_sol_n[0]);
      deriv.axpy(-1.0*m_alpha[2],m_sol_n_1[0]);
      deriv.axpy(-1.0*m_alpha[3],m_sol_n_2[0]);
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
    deriv.scale(1./dt);
  }

  void Bdf::time_der( double dt, const std::vector<PetscVector>& m_vecSol,  std::vector<PetscVector>& deriv  ) const {
    for (felInt i=0 ; i< m_numberOfComp; i++) {
      deriv[i].zeroEntries();
      deriv[i].axpy(m_alpha[0],m_vecSol[i]);
    }
    switch (m_order) {
    case 1:
      for (felInt i=0 ; i< m_numberOfComp; i++)
        deriv[i].axpy(-1.0*m_alpha[1],m_sol_n[i]);
      break;
    case 2:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        deriv[i].axpy(-1.0*m_alpha[1],m_sol_n[i]);
        deriv[i].axpy(-1.0*m_alpha[2],m_sol_n_1[i]);
      }
      break;
    case 3:
      for (felInt i=0 ; i< m_numberOfComp; i++) {
        deriv[i].axpy(-1.0*m_alpha[1],m_sol_n[i]);
        deriv[i].axpy(-1.0*m_alpha[2],m_sol_n_1[i]);
        deriv[i].axpy(-1.0*m_alpha[3],m_sol_n_2[i]);
      }
      break;
    default:
      FEL_ERROR("unknown BDF order");
      break;
    }
    for (felInt i=0 ; i< m_numberOfComp; i++)
      deriv[i].scale(1./dt);
  }
}

