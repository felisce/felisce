//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNSFracStepExplAdv.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemNSFracStepExplAdv::LinearProblemNSFracStepExplAdv():
    LinearProblem("Navier-Stokes Fractional Step: Explicit Advection"),
    m_viscosity(0.),
    m_density(0.),
    m_explicitAdvection(0),
    m_buildTeporaryMatrix(false)
    //m_bdf(NULL),
    //allocateSeqVec(false),
    //allocateSeqVecExt(false)
  {}

  LinearProblemNSFracStepExplAdv::~LinearProblemNSFracStepExplAdv() {
    if(m_buildTeporaryMatrix)
      m_matrix.destroy();
  }

  void LinearProblemNSFracStepExplAdv::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    m_explicitAdvection = FelisceParam::instance().explicitAdvection;
    if (!m_explicitAdvection) {
      FEL_ERROR(" LinearProblemNSFracStepExplAdv should be only used when explicitAdvection>0: check your input file.");
    }
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;
    
    // list the variable used in the formulation
    listVariable.push_back(velocityAdvection);
    listNumComp.push_back(this->dimension());
    listVariable.push_back(velocity);
    listNumComp.push_back(this->dimension());
    // note: all the variables present in the
    // global problem should be defined
    listVariable.push_back(pressure);
    listNumComp.push_back(1);

    //define unknown of the linear system.
    m_listUnknown.push_back(velocityAdvection);

    definePhysicalVariable(listVariable,listNumComp);

    m_viscosity = FelisceParam::instance().viscosity;
    m_density = FelisceParam::instance().density;

    this->setNumberOfMatrix(2);
    _RungeKutta = 1;
  }

  void LinearProblemNSFracStepExplAdv::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    m_iVelocityAdvection = m_listVariable.getVariableIdList(velocityAdvection);
    m_iVelocity = m_listVariable.getVariableIdList(velocity);

    // get FE for each variable
    m_feVelAdv = m_listCurrentFiniteElement[m_iVelocityAdvection];
    m_feVel = m_listCurrentFiniteElement[m_iVelocity];
    m_velocityAdvection = &m_listVariable[m_iVelocityAdvection];
    m_velocity = &m_listVariable[m_iVelocity];

    // initialize element fields for different contributions
    m_elemFieldAdv.initialize(DOF_FIELD,*m_feVelAdv,this->dimension());
    m_elemFieldExt.initialize(DOF_FIELD,*m_feVel,this->dimension());

  }

  void LinearProblemNSFracStepExplAdv::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;

    m_feVel->updateFirstDeriv(0, elemPoint);
    m_feVelAdv->updateFirstDeriv(0, elemPoint);

    double dt = m_fstransient->timeStep;
    if ( m_fstransient->iteration == 0) {

      //================================
      //            matrix[0]
      //================================
      // mass matrix
      m_elementMat[0]->phi_i_phi_j(m_density,*m_feVelAdv,0,0,this->dimension());

    } else {

      m_elemFieldExt.setValue(externalVec(0), *m_feVel, iel, m_iVelocity, m_externalAO[0], *m_externalDof[0]);

      //================================
      //            matrix[1]
      //================================
      // advection matrix dt*rho*(U.grad(phi),phi)
      m_elementMat[1]->u_grad_phi_j_phi_i(m_density,m_elemFieldExt,*m_feVel,0,0,this->dimension());
      // + stabilization matrix
      // TODO


      //================================
      //            RHS
      //================================

      // to assemble advection explicitly on the RHS
      // RK-1
      if (_RungeKutta==1) {
        // -> assemble advection explicitly on the RHS
        assert(!m_elementVector.empty());
        m_elementVector[0]->u_grad_u_phi_i(-1*m_density*dt, *m_feVel, m_elemFieldExt,0,this->dimension());
        // -> rho(udiff^n,phi)
        m_elementVector[0]->source(m_density,*m_feVel,m_elemFieldExt,0,this->dimension());
      }


    }
  }


  // for boundary terms (weak boundary conditions at inflow)
  void LinearProblemNSFracStepExplAdv::initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    /*
     // is this function used??
     m_iVelocity = m_listVariable.getVariableIdList(velocity);
     CurvilinearFiniteElement* bdFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
     m_elemFieldDiffBD.initialize(DOF_FIELD, *bdFeVel, this->dimension());
     */
  }

  void LinearProblemNSFracStepExplAdv::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;
    double dt = m_fstransient->timeStep;
    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    CurvilinearFiniteElement* bdFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
    bdFeVel->updateMeasNormal(0, elemPoint);

    m_iVelocityAdvection = m_listVariable.getVariableIdList(velocityAdvection);
    CurvilinearFiniteElement* bdFeVelAdv = m_listCurvilinearFiniteElement[m_iVelocityAdvection];
    bdFeVelAdv->updateMeasNormal(0, elemPoint);

    m_elemFieldExt.setValue(externalVec(0),*bdFeVel,iel,m_iVelocity,m_externalAO[0],*m_externalDof[0]);
    //================================
    //           bd. matrix[0]
    //================================
    if (_RungeKutta==1) {
      m_elementMatBD[0]->abs_u_dot_n_phi_i_phi_j(dt*m_density,m_elemFieldExt,*bdFeVelAdv,0,0,this->dimension());
    } else if (_RungeKutta==2) {
      m_elementMatBD[1]->abs_u_dot_n_phi_i_phi_j(dt*m_density,m_elemFieldExt,*bdFeVelAdv,0,0,this->dimension());
    } else {
      FEL_ERROR("This order of Runge-Kutta has not been implemented yet");
    }

    //================================
    //           bd. RHS
    //================================
    // rho(udiff^n,phi)
    assert(!m_elementVector.empty());
    m_elementVectorBD[0]->abs_u_dot_n_u_dot_phi_i(dt*1.,m_elemFieldExt,*bdFeVel,0,this->dimension());

  }



  void LinearProblemNSFracStepExplAdv::copyMatrixRHS() {
    m_matrix.duplicateFrom(matrix(0),MAT_COPY_VALUES);
    m_matrix.assembly(MAT_FINAL_ASSEMBLY);
    m_buildTeporaryMatrix = true;
  }

  void LinearProblemNSFracStepExplAdv::addMatrixRHS() {
    matrix(0).axpy(1,m_matrix,SAME_NONZERO_PATTERN);
  }
}
