//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    C. Corrado
//

// System includes

// External includes

// Project includes
#include "Solver/FhNSolver.hpp"

// Am*Cm du/dt = Am * I_{ion}
//
// I_{ion} = f_0 * u * (u-alpha) * (1-u) - w
//
// dw/dt = eps * (u*gamma - beta*w)
//
//w_0 = 0


namespace felisce {
  FhNSolver::FhNSolver(FelisceTransient::Pointer fstransient):
    IonicSolver(fstransient)
  {}


  FhNSolver::~FhNSolver()
  = default;

  void FhNSolver::computeRHS() {
    double& epsilon = FelisceParam::instance().epsilon;
    double& vMin  = FelisceParam::instance().vMin;
    double& vMax  = FelisceParam::instance().vMax;
    double& dt = m_fstransient->timeStep;
    double& gammaEl = FelisceParam::instance().gammaEl;
    m_bdf.computeRHSTime(dt, m_RHS);
    felInt pos;
    double value_uExtrap;
    double value_RHS;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      value_RHS = gammaEl*epsilon*((value_uExtrap- vMin)/(vMax - vMin));
      m_RHS.setValue(pos,value_RHS, ADD_VALUES);
    }
    m_RHS.assembly();
  }

  void FhNSolver::solveEDO() {
    double& epsilon = FelisceParam::instance().epsilon;
    double& dt = m_fstransient->timeStep;
    double& beta = FelisceParam::instance().beta;
    double& coeffDeriv = m_bdf.coeffDeriv0();

    felInt pos;
    double value_RHS;
    double valuem_solEDO;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_RHS.getValues(1,&pos,&value_RHS);//value_RHS = _RHS(i)
      valuem_solEDO = value_RHS * 1./(coeffDeriv/dt + epsilon*beta);
      m_solEDO.setValue(pos,valuem_solEDO, INSERT_VALUES);
    }
    m_solEDO.assembly();
  }

  void FhNSolver::computeIon() {
    if (FelisceParam::instance().stateFilter) {
      if(!m_stabInit) {
        m_stabTerm.duplicateFrom(m_ion);
        m_stabInit=true;
      }
      m_stabTerm.zeroEntries();
    }
    double& vMin  = FelisceParam::instance().vMin;
    double& vMax  = FelisceParam::instance().vMax;
    double& f0  = FelisceParam::instance().f0;
    double& alpha = FelisceParam::instance().alpha;

    felInt pos;
    double value_uExtrap;
    double valuem_solEDO;
    double value_ion;
    double value_stab;
    double v_adim=0.0;
    double g_v=0.0;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_solEDO.getValues(1,&pos,&valuem_solEDO);//valuem_solEDO = m_solEDO(i)
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      v_adim = (value_uExtrap-vMin)/(vMax-vMin);
      if(FelisceParam::instance().hasInfarct) {
        value_ion =  m_f0Par[pos]*v_adim*(v_adim-alpha)*(1-v_adim) - valuem_solEDO;
      } else {
        value_ion =  f0*v_adim*(v_adim-alpha)*(1-v_adim) - valuem_solEDO;
      }

      if (FelisceParam::instance().stateFilter) {
        g_v=f0*(3.0*v_adim*v_adim-2.0*(1-0+alpha)*v_adim+alpha);
        value_stab =  0.5*(std::abs(g_v)-g_v)+FelisceParam::instance().gain;
        m_stabTerm.setValue(pos,value_stab, INSERT_VALUES);
      }
      m_ion.setValue(pos,value_ion, INSERT_VALUES);
    }

    m_ion.assembly();
    if (FelisceParam::instance().stateFilter) {
      m_stabTerm.assembly();
    }
  }

}
