//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E.Schenone C.Corrado
//

#ifndef _IONICSOLVER_HPP
#define _IONICSOLVER_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "Solver/bdf.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  class IonicSolver {
  public:
    /// Constructor.
    IonicSolver(FelisceTransient::Pointer fstransient);
    /// Destructor.
    virtual ~IonicSolver();
    /// Define order of BDF.
    void defineOrderBdf(int order, int nComp=1);
    /// Define size of schaf equations.
    void defineSizeAndMappingOfIonicProblem(felInt size, ISLocalToGlobalMapping& mapping, AO ao=FELISCE_PETSC_NULLPTR);
    /// Initialization of the extrapolate.
    void initializeExtrap(PetscVector& V_0);
    /// Initialization BDF 1.
    void initialize(PetscVector& sol_0);
    /// Initialization BDF 2.
    void initialize(PetscVector& sol_0,PetscVector& sol_1);
    /// Initialization BDF 3.
    void initialize(PetscVector& sol_0,PetscVector& sol_1,PetscVector& sol_2);

    virtual void initialize(std::vector<PetscVector>& sol_0);
    void initialize(std::vector<PetscVector>& sol_0,std::vector<PetscVector>& sol_1);
    void initialize(std::vector<PetscVector>& sol_0,std::vector<PetscVector>& sol_1,std::vector<PetscVector>& sol_2);


    /// Compute RHS in EDO.
    virtual void computeRHS() {}
    /// Solve the EDO.
    virtual void solveEDO()=0;
    /// Computation of ionic current from EDO solution.
    virtual void computeIon()=0;

    /// Update time scheme to next timestep.
    void updateBdf();
    /// Update extrapolate from result of EDP.
    void update(PetscVector& V_1);

    ///  Access functions.
    inline const PetscVector&  sol() const {
      return m_solEDO;
    }
    inline PetscVector&  sol() {
      return m_solEDO;
    }

    inline const PetscVector&  sol_n_1() const {
      return m_bdf.sol_n_1();
    }
    inline PetscVector&  sol_n_1() {
      return m_bdf.sol_n_1();
    }

    inline const PetscVector&  sol_n_2() const {
      return m_bdf.sol_n_2();
    }
    inline PetscVector&  sol_n_2() {
      return m_bdf.sol_n_2();
    }

    inline const PetscVector&  ion() const {
      return m_ion;
    }
    inline PetscVector&  ion() {
      return m_ion;
    }

    inline const Bdf & bdf() const {
      return m_bdf;
    }
    inline Bdf & bdf() {
      return m_bdf;
    }

    inline PetscVector&  stabTerm() {
      return m_stabTerm;
    }
    inline const PetscVector&  stabTerm() const {
      return m_stabTerm;
    }

    inline const std::vector<PetscVector> & vec_sol() const {
      return m_vecSolEDO;
    }
    inline std::vector<PetscVector> & vec_sol() {
      return m_vecSolEDO;
    }

    inline const PetscVector&  vec_sol(const int i) const {
      return m_vecSolEDO[i];
    }

    inline const std::vector<PetscVector> & vec_sol_n_1() const {
      return m_bdf.vec_sol_n_1();
    }
    inline std::vector<PetscVector> & vec_sol_n_1() {
      return m_bdf.vec_sol_n_1();
    }

    inline const std::vector<PetscVector> & vec_sol_n_2() const {
      return m_bdf.vec_sol_n_2();
    }
    inline std::vector<PetscVector> & vec_sol_n_2() {
      return m_bdf.vec_sol_n_2();
    }

  protected:
    /// Number of Dof for the trans membrane potential.
    felInt m_size;
    /// User data file of Felisce.
    /// User data file of Felisce (only transient parameters).
    FelisceTransient::Pointer m_fstransient;
    /// Number of currents
    std::size_t m_nComp;
    /// Cell types concerned by MV model
    felInt m_cellTypeCut;
    /// Extrapolate std::vector.
    PetscVector m_uExtrap;

    /// RHS std::vector.
    PetscVector m_RHS;
    /// EDO solution.
    PetscVector m_solEDO;
    /// Ionic current.
    PetscVector m_ion;
    /// 
    Bdf m_bdf;

    std::vector<PetscVector> m_vecRHS;
    std::vector<PetscVector> m_vecSolEDO;

    /*! stabilization term, for Luenberger observer
     \c  the stabilization term has to be implemented in computeIon
     */
    PetscVector m_stabTerm;
    ///  tells if stabilization term is initialized
    bool m_stabInit;

    ///  mapping between local petsc numbering and global petsc numbering
    ISLocalToGlobalMapping m_localDofToGlobalDof;
    ///  mapping between global petsc numbering and global "mesh" numbering
    AO m_ao;
  };
}

#endif
