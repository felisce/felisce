//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "InputOutput/io.hpp"
#include "Solver/linearProblem.hpp"

namespace felisce 
{
  // Print solution of the system solve.
  void LinearProblem::printSolution(int verbose, std::ostream& outstr) const {
    IGNORE_UNUSED_OUTSTR;

    if (verbose > 19) {
      PetscPrintf(MpiInfo::petscComm(),"\n/================ Solution of the assembly system Ax=b ================/\n");
      m_sol.view();
    }
  }

  //===============================================================
  // MATLAB FUNCTIONS FOR DEBUG
  // It is possible to save the objects also in binary format.
  // The functions to do that are in the wrappers class: petscVector and petscMatrix.
  // Also the use of this function is not always useful since you
  // can directly call the function of the wrapper.
  //
  // Write in file matrix.m, matrix in matlab format.
  void LinearProblem::writeMatrixForMatlab(int verbose, const std::string& filename) const {
    if (verbose > 20) {
      m_matrices[0].saveInMatlabFormat(MpiInfo::petscComm(),filename.c_str(),FelisceParam::instance().resultDir);
    }
  }
  //Write in file RHS.m, RHS in matlab format.
  void LinearProblem::writeRHSForMatlab(int verbose, const std::string& filename) const {
    if (verbose > 20) {
      vector().saveInMatlabFormat(MpiInfo::petscComm(),filename,FelisceParam::instance().resultDir);
    }
  }
  
  //Write in file sol.m, RHS in matlab format.
  void LinearProblem::writeSolForMatlab(int verbose, const std::string& filename) const {
    if (verbose > 20) {
      m_sol.saveInMatlabFormat(MpiInfo::petscComm(),filename,FelisceParam::instance().resultDir);
    }
  }

  //Write in file filename, a matrix in matlab format.
  void LinearProblem::writeMatrixForMatlab(std::string const& fileName, PetscMatrix& matrix) const {
    matrix.saveInMatlabFormat(MpiInfo::petscComm(),fileName.c_str(),FelisceParam::instance().resultDir);
  }

  //Write in file filename, a std::vector in matlab format.
  void LinearProblem::writeVectorForMatlab(std::string const& fileName, PetscVector& vector) const {
    vector.saveInMatlabFormat(MpiInfo::petscComm(),fileName.c_str(),FelisceParam::instance().resultDir);
  }

  // Write in file matrix.mb, matrix in matlab format.
  void LinearProblem::writeMatrixForMatlabBinary(int verbose, const std::string& filename) const {
    if (verbose > 20) {
      m_matrices[0].saveInBinaryFormat(MpiInfo::petscComm(),filename.c_str(),FelisceParam::instance().resultDir);
    }
  }
  //Write in file RHS.m, RHS in matlab format.
  void LinearProblem::writeRHSForMatlabBinary(int verbose, const std::string& filename) const {
    if (verbose > 20) {
      vector().saveInBinaryFormat(MpiInfo::petscComm(),filename,FelisceParam::instance().resultDir);
    }
  }

  //Write in file sol.mb, RHS in matlab format.
  void LinearProblem::writeSolForMatlabBinary(int verbose, const std::string& filename) const {
    if (verbose > 20) {
      m_sol.saveInBinaryFormat(MpiInfo::petscComm(),filename,FelisceParam::instance().resultDir);
    }
  }

  //Write in file filename, a matrix in matlab format.
  void LinearProblem::writeMatrixForMatlabBinary(std::string const& fileName, PetscMatrix& matrix) const {
    matrix.saveInBinaryFormat(MpiInfo::petscComm(),fileName.c_str(),FelisceParam::instance().resultDir);
  }

  //Write in file filename, a std::vector in matlab format.
  void LinearProblem::writeVectorForMatlabBinary(std::string const& fileName, PetscVector& vector) const {
    vector.saveInBinaryFormat(MpiInfo::petscComm(),fileName.c_str(),FelisceParam::instance().resultDir);
  }
  //================================================================
  
  void LinearProblem::writeSolution(int rank, std::vector<IO::Pointer>& io, double& time, int iteration) {
    // to be sure that m_seqSol is up to date
    gatherSolution();
    if ( rank != 0)
      return;

    std::vector<double> valueVec(m_numDof);
    m_seqSol.getAllValuesInAppOrdering(m_ao, valueVec); 

    // Write solution in ensight format
    int idVar, idVar2, iMesh;
    double* solution;
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
      felInt numValueStart = 0;
      idVar = m_listUnknown.idVariable(iUnknown);
      iMesh = m_listVariable[idVar].idMesh();

      for ( std::size_t iUnknown2 = 0; iUnknown2 < iUnknown; iUnknown2++) {
        idVar2 = m_listUnknown.idVariable(iUnknown2);

        // the number of the previous suppDofs could be different from the number of nodes
        //if NOT using FusionDof  numSupportDof() == listNode().size()
        //if  DO using FusionDof  numSupportDof() != listNode().size()
        numValueStart += m_supportDofUnknown[iUnknown2].numSupportDof()*m_listVariable[idVar2].numComponent();
      }

      switch (m_listVariable[idVar].numComponent()) {
        case 1:
          solution = new double[m_supportDofUnknown[iUnknown].listNode().size()];
          for (std::size_t i = 0; i < m_supportDofUnknown[iUnknown].listNode().size(); i++) {

            if (FelisceParam::instance().FusionDof) {
              solution[i] = valueVec[m_supportDofUnknown[iUnknown].listPerm()[i]+numValueStart];
            } else {
              solution[i] = valueVec[i+numValueStart];
            }
          }
          io[iMesh]->writeSolution(rank, time, iteration, 0, m_listVariable[idVar].name(), solution, m_supportDofUnknown[iUnknown].listNode().size(), dimension(), true);
          break;
        case 2:
          solution = new double[m_supportDofUnknown[iUnknown].listNode().size()*3];
          for (std::size_t i = 0; i < m_supportDofUnknown[iUnknown].listNode().size(); i++) {

            if (FelisceParam::instance().FusionDof) {
              solution[i*3]   = valueVec[m_supportDofUnknown[iUnknown].listPerm()[i]*2+numValueStart];
              solution[i*3+1] = valueVec[m_supportDofUnknown[iUnknown].listPerm()[i]*2+1+numValueStart];
              solution[i*3+2] = 0.;
            } else {
              solution[i*3] = valueVec[i*2+numValueStart];
              solution[i*3+1] = valueVec[i*2+1+numValueStart];
              solution[i*3+2] = 0.;
            }
          }
          io[iMesh]->writeSolution(rank, time, iteration, 1, m_listVariable[idVar].name(), solution, m_supportDofUnknown[iUnknown].listNode().size(), dimension(),true);
          break;
        case 3:
          solution = new double[m_supportDofUnknown[iUnknown].listNode().size()*3];
          for (std::size_t i = 0; i < m_supportDofUnknown[iUnknown].listNode().size(); i++) {

            if (FelisceParam::instance().FusionDof) {
              solution[i*3] = valueVec[m_supportDofUnknown[iUnknown].listPerm()[i]*3+numValueStart];
              solution[i*3+1] = valueVec[m_supportDofUnknown[iUnknown].listPerm()[i]*3+1+numValueStart];
              solution[i*3+2] = valueVec[m_supportDofUnknown[iUnknown].listPerm()[i]*3+2+numValueStart];
            } else {
              solution[i*3] = valueVec[i*3+numValueStart];
              solution[i*3+1] = valueVec[i*3+1+numValueStart];
              solution[i*3+2] = valueVec[i*3+2+numValueStart];
            }
          }
          io[iMesh]->writeSolution(rank, time, iteration, 1, m_listVariable[idVar].name(), solution, m_supportDofUnknown[iUnknown].listNode().size(), dimension(),true);
          break;
        default:
          FEL_ERROR("Problem with the dimension of your solution to write it.")
          break;
      }
    }
    // Do not need to delete here solution because it is deleted in EnsightCase::postProcess (manageMemory of io.writeSolution = true)
    //delete [] solution;
  }

  void LinearProblem::writeSolutionFromVec(PetscVector& v, int rank, std::vector<IO::Pointer>& io, double& time, int iteration, std::string prefix) {
    // Create a sequential vector with the size of sol
    PetscVector xsol;
    v.scatterToZero(xsol, INSERT_VALUES, SCATTER_FORWARD);
    if ( rank != 0) {
      return;
    }

    std::vector<double> valueVec( m_numDof );
    xsol.getAllValuesInAppOrdering( m_ao, valueVec );

    int idVar = -1;
    int idVar2 = -1;
    int iMesh = -1;
    // Write solution in ensight format
    double* solution;
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
      felInt numValueStart = 0;
      idVar = m_listUnknown.idVariable(iUnknown);
      iMesh = m_listVariable[idVar].idMesh();
      for ( std::size_t iUnknown2 = 0; iUnknown2 < iUnknown; iUnknown2++) {
        idVar2 = m_listUnknown.idVariable(iUnknown2);
        // the number of the previous suppDofs could be different from the number of nodes
        //if NOT using FusionDof  numSupportDof() == listNode().size()
        //if  DO using FusionDof  numSupportDof() != listNode().size()
        numValueStart += supportDofUnknown(iUnknown2).numSupportDof()*m_listVariable[idVar2].numComponent();
      }
      std::string name=prefix;

      switch (m_listVariable[idVar].numComponent()) {

      case 1:
        solution = new double[supportDofUnknown(iUnknown).listNode().size()];
        for (felInt i = 0; i < static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); i++) {

          if (FelisceParam::instance().FusionDof) {
            solution[i] = valueVec[supportDofUnknown(iUnknown).listPerm()[i]+numValueStart];
          } else {
            solution[i] = valueVec[i+numValueStart];
          }
        }
        name += std::string(m_listVariable[idVar].name());
        io[iMesh]->writeSolution(rank, time, iteration, 0,name, solution, supportDofUnknown(iUnknown).listNode().size(), dimension(),true);
        break;

      case 2:
        solution = new double[supportDofUnknown(iUnknown).listNode().size()*3];
        for (felInt i = 0; i < static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); i++) {

          if (FelisceParam::instance().FusionDof) {
            solution[i*3] = valueVec[supportDofUnknown(iUnknown).listPerm()[i]*2+numValueStart];
            solution[i*3+1] = valueVec[supportDofUnknown(iUnknown).listPerm()[i]*2+1+numValueStart];
            solution[i*3+2] = 0.;
          } else {
            solution[i*3] = valueVec[i*2+numValueStart];
            solution[i*3+1] = valueVec[i*2+1+numValueStart];
            solution[i*3+2] = 0.;
          }
        }
        name += std::string(m_listVariable[idVar].name());
        io[iMesh]->writeSolution(rank, time, iteration, 1,name, solution, supportDofUnknown(iUnknown).listNode().size(), dimension(),true);
        break;

      case 3:
        solution = new double[supportDofUnknown(iUnknown).listNode().size()*3];
        for (felInt i = 0; i < static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); i++) {

          if (FelisceParam::instance().FusionDof) {
            solution[i*3] = valueVec[supportDofUnknown(iUnknown).listPerm()[i]*3+numValueStart];
            solution[i*3+1] = valueVec[supportDofUnknown(iUnknown).listPerm()[i]*3+1+numValueStart];
            solution[i*3+2] = valueVec[supportDofUnknown(iUnknown).listPerm()[i]*3+2+numValueStart];
          } else {
            solution[i*3] = valueVec[i*3+numValueStart];
            solution[i*3+1] = valueVec[i*3+1+numValueStart];
            solution[i*3+2] = valueVec[i*3+2+numValueStart];
          }
        }
        name += std::string(m_listVariable[idVar].name());
        // in write solution there is a delete [] solution
        io[iMesh]->writeSolution(rank, time, iteration, 1,name, solution, supportDofUnknown(iUnknown).listNode().size(), dimension(),true);
        break;

      default:
        FEL_ERROR("Problem with the dimension of your solution to write it.")
        break;
      }
    }
  }

  /*!
   \brief write solution for backup in Ensight format.
   */
  void LinearProblem::writeSolutionBackup(const ListVariable& listVar, int rank, std::vector<IO::Pointer>& io, double& time, int iteration) {
    gatherSolution();

    std::vector<double> valueVec(m_numDof);        
    m_seqSol.getAllValuesInAppOrdering( m_ao, valueVec );
    
    std::vector<std::size_t> printingVar;

    int idVar = -1;
    int idVar2 = -1;
    int iMesh = -1;
    // Write solution in ensight format
    if ( rank == 0) {
      double* solution;
      for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
        felInt numValueStart = 0;
        idVar = m_listUnknown.idVariable(iUnknown);
        iMesh = m_listVariable[idVar].idMesh();
        for (std::size_t iListVar = 0; iListVar < listVar.size(); iListVar++) {
          if ( m_listVariable[idVar].name() == listVar[iListVar].name() ) {
            for ( std::size_t iUnknown2 = 0; iUnknown2 < iUnknown; iUnknown2++) {
              idVar2 = m_listUnknown.idVariable(iUnknown2);
              numValueStart += supportDofUnknown(iUnknown2).listNode().size()*m_listVariable[idVar2].numComponent();
            }

            switch (m_listVariable[idVar].numComponent()) {

            case 1:
              solution = new double[supportDofUnknown(iUnknown).listNode().size()];
              for (felInt i = 0; i < static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); i++) {
                solution[i] = valueVec[i+numValueStart];
              }
              io[iMesh]->writeSolution(rank, time, iteration, 0, m_listVariable[idVar].name()+"_bkp", solution,  supportDofUnknown(iUnknown).listNode().size(), dimension(),true);
              break;

            case 2:
              solution = new double[supportDofUnknown(iUnknown).listNode().size()*3];
              for (felInt i = 0; i < static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); i++) {
                solution[i*3] = valueVec[i*2+numValueStart];
                solution[i*3+1] = valueVec[i*2+1+numValueStart];
                solution[i*3+2] = 0.;
              }
              io[iMesh]->writeSolution(rank, time, iteration, 1, m_listVariable[idVar].name()+"_bkp", solution,  supportDofUnknown(iUnknown).listNode().size(), dimension(),true);
              break;

            case 3:
              solution = new double[supportDofUnknown(iUnknown).listNode().size()*3];
              for (felInt i = 0; i < static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); i++) {
                solution[i*3] = valueVec[i*3+numValueStart];
                solution[i*3+1] = valueVec[i*3+1+numValueStart];
                solution[i*3+2] = valueVec[i*3+2+numValueStart];
              }
              io[iMesh]->writeSolution(rank, time, iteration, 1, m_listVariable[idVar].name()+"_bkp", solution,  supportDofUnknown(iUnknown).listNode().size(), dimension(),true);
              break;

            default:
              FEL_ERROR("Problem with the dimension of your solution to write it.")
              break;
            }
          }
        }
        // Do not need to delete here solution because it is deleted in EnsightCase::postProcess (manageMemory of io.writeSolution = true)
        //delete [] solution;
      }
    }
  }

  /*!
   \brief move a Petsc std::vector to a double* in order to print it in an ensight file
   */
  void LinearProblem::fromVecToDoubleStar(double* solution, const PetscVector& sol, int rank, int dimension, felInt size) {
    if (FelisceParam::verbose() >= 40 )
      sol.view();
    felInt sizeVec;
    sol.getSize(&sizeVec);

    // Check if the demanded double* outpuy has the same dimension of the input Vec
    if (size == 0)
      size = sizeVec;

    PetscVector xsol;
    sol.scatterToZero(xsol, INSERT_VALUES, SCATTER_FORWARD);
    if(rank == 0) {
      felInt iout[size];
      felReal valueVec[size];
      for (felInt i=0; i<size; i++)
        iout[i] = i;

      AOApplicationToPetsc(m_ao, size, iout);
      xsol.getValues(size, iout, valueVec);

      switch (dimension) {
      case 1:
        for(felInt i=0; i < size; i++) {
          solution[i] = valueVec[i];
        }
        break;
      case 2:
        for(felInt i=0; i < size; i++) {
          solution[i*3] = valueVec[i*2];
          solution[i*3+1] = valueVec[i*2+1];
          solution[i*3+2] = 0.;
        }
        break;
      case 3:
        for(felInt i=0; i < size; i++) {
          solution[i*3] = valueVec[i*3];
          solution[i*3+1] = valueVec[i*3+1];
          solution[i*3+2] = valueVec[i*3+2];
        }
        break;
      default:
        FEL_ERROR("Problem with the dimension of your solution to write it.")
        break;
      }
    }
  }

  /*!
   \brief move a double* to a Petsc std::vector in order to read it from an ensight file
   */
  void LinearProblem::fromDoubleStarToVec(double* solution, PetscVector* sol, felInt size) {
    sol->duplicateFrom(m_sol);
    felInt ia;
    for (felInt i=0; i< size; i++) {
      ia = i;
      AOApplicationToPetsc(m_ao,1,&ia);
      sol->setValues(1,&ia,&solution[i],INSERT_VALUES);
    }

    sol->assembly();

    if (FelisceParam::verbose() >= 40 )
      sol->view();
  }

  /*!
   \brief write support variables for backup in ensight files
   Warning:
   dimension = 1 -> sclar; dimensio = 2 -> 2size 3D vectors; dimension = 3 -> 3size 3D vectors
   */
  void LinearProblem::writeSupportVariableBackup(std::string name, double* supportVariable, int size, int dimension, int rank, IO::Pointer io, double& time, int iteration, bool manageMemory) {
    io->writeSolution(rank, time, iteration, dimension-1, name, supportVariable, size, 1, manageMemory);
  }

  /*!
   \brief write mesh to read solution with specific mesh (P2, Q2,...) .
   */
  void LinearProblem::writeGeoForUnknown(int iUnknown, std::string nameOfGeoFile) {
    // Intialization
    // =============
    std::string fileNameGeo, fileNameRoot, fileName;
    ElementType eltType;
    std::string keyWord;
    int the_ref;
    short verbose = 0;
    FILE * pFile;
    felInt numElemsPerRef = 0;
    felInt ielSupportDof = 0;
    std::vector<felInt> elem;
    std::vector<felInt> vectorIdSupport;

    const int idVar = m_listUnknown.idVariable(iUnknown);
    const int iMesh = m_listVariable[idVar].idMesh();

    Ensight ens(FelisceParam::instance().inputDirectory,FelisceParam::instance().inputFile,FelisceParam::instance().inputMesh[iMesh],FelisceParam::instance().outputMesh[iMesh],FelisceParam::instance().meshDir,FelisceParam::instance().resultDir);

    felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }


    if(nameOfGeoFile.empty()) {
      fileNameGeo = listVariable().listVariable()[idVar].name()+".geo";
      fileNameRoot = FelisceParam::instance().resultDir;
      fileName = fileNameRoot + fileNameGeo;
    } else {
      fileName = nameOfGeoFile;
    }

    if (verbose>0)
      std::cout << "Writing file "<< fileName << std::endl;

    pFile = fopen (fileName.c_str(),"w");
    if (pFile==nullptr) {
      std::string command = "mkdir -p " + FelisceParam::instance().resultDir;
      int ierr = system(command.c_str());
      if (ierr>0) {
        FEL_ERROR("Impossible to write "+fileName+"  geo mesh");
      }
      pFile = fopen (fileName.c_str(),"w");
    }
    //! Descriptions lines (All Geometry files must contain these first six lines)
    fprintf(pFile,"Geometry file\n");
    fprintf(pFile,"Geometry file\n");
    fprintf(pFile,"node id assign\n");
    fprintf(pFile,"element id assign\n");
    fprintf(pFile,"coordinates\n");
    fprintf(pFile,"%8d\n",(int)supportDofUnknown(iUnknown).listNode().size());

    //! write the vertices
    if ( m_mesh[iMesh]->numCoor() == 3 ) {
      for(felInt iv=0; iv< static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); iv++) {
        fprintf( pFile,"%12.5e%12.5e%12.5e\n", supportDofUnknown(iUnknown).listNode()[iv].x(),
                 supportDofUnknown(iUnknown).listNode()[iv].y(),supportDofUnknown(iUnknown).listNode()[iv].z() );
      }
    } else if ( m_mesh[iMesh]->numCoor() == 2 ) {
      double zcoor = 0.;
      for(felInt iv=0; iv< static_cast<felInt>(supportDofUnknown(iUnknown).listNode().size()); iv++) {
        fprintf( pFile, "%12.5e%12.5e%12.5e\n", supportDofUnknown(iUnknown).listNode()[iv].x(), supportDofUnknown(iUnknown).listNode()[iv].y(), zcoor);
      }
    } else {
      std::ostringstream msg;
      msg << "Error in writing geo File " << fileNameGeo << ". "
          << "Expecting mesh with 2 or 3 coordinates (it is " << m_mesh[iMesh]->numCoor() << ")."
          << std::endl;
      FEL_ERROR(msg.str());
    }

    //!write the elements per references.
    //------------
    felInt cptpart = 0;
    std::string DescriptionLine,felName;
    GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  m_mesh[iMesh]->intRefToEnum();
    for(auto it_ref = intRefToEnum.begin(); it_ref != intRefToEnum.end(); ++it_ref) {
      the_ref = (*it_ref).first;

      for(auto it_elem = it_ref->second.begin(); it_elem != it_ref->second.end(); ++it_elem) {
        eltType = *it_elem;
        
        if (m_mesh[iMesh]->flagFormatMesh() == GeometricMeshRegion::FormatMedit) {
          cptpart++;
          fprintf ( pFile, "part%8d\n",cptpart );
          if ( m_listVariable[idVar].finiteElementType() == 0 )
            felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
          else if ( m_listVariable[idVar].finiteElementType() == 1 )
            felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltQuad[eltType]].first;
          else {
            if(m_mesh[iMesh]->domainDim() == GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numCoor())
              felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltBubble[eltType]].first;
            else
              felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
          }

          auto it_felName = FelisceParam::instance().felName2MapintRef2DescLine.find(felName);
          if ( it_felName == FelisceParam::instance().felName2MapintRef2DescLine.end() ) {
            //std::cout << "Warning: ----------->trying to access an unexisting element: " << felName << " in input file !!!!!!!" << std::endl;
            DescriptionLine = felName + "m_ref_" + std::to_string(the_ref);
          } else {
            auto it_ref2 = it_felName->second.find(the_ref);
            if ( it_ref2 == 	it_felName->second.end() ) {
              DescriptionLine = felName + "m_ref_" + std::to_string(the_ref);
            } else {
              DescriptionLine = it_ref2->second;
            }
          }
          fprintf (pFile, "%s", DescriptionLine.c_str() );
        }
        fprintf(pFile,"\n");
        keyWord = ens.eltFelNameToEns6Pair()[felName].second;
        fprintf( pFile, "%s", keyWord.c_str() );
        fprintf(pFile,"\n");

        numElemsPerRef = m_mesh[iMesh]->intRefToBegEndMaps[eltType][the_ref].second;
        if(!FelisceParam::instance().duplicateSupportDof) {
          fprintf(pFile,"%8d\n", numElemsPerRef);
        } else {
          felInt numSupportElem = 0;
          for ( felInt iel = numElement[eltType]; iel < numElement[eltType] + numElemsPerRef; iel++) {
            supportDofUnknown(iUnknown).getIdElementSupport(eltType, iel, vectorIdSupport);
            numSupportElem += vectorIdSupport.size();
          }
          fprintf(pFile,"%8d\n", numSupportElem);
        }

        for ( felInt iel = 0; iel < numElemsPerRef; iel++) {
          supportDofUnknown(iUnknown).getIdElementSupport(eltType, numElement[eltType], vectorIdSupport);

          for(std::size_t iSup=0; iSup<vectorIdSupport.size(); iSup++) {
            ielSupportDof = vectorIdSupport[iSup];
            elem.resize(supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof),0);
            for ( int iSupport = 0; iSupport < supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof); iSupport++) {
              elem[iSupport] = supportDofUnknown(iUnknown).iSupportDof()[ supportDofUnknown(iUnknown).iEle()[ielSupportDof] + iSupport];
            }
            for (unsigned int ivert = 0; ivert < elem.size(); ivert++) {
              fprintf( pFile, "%8d", elem[ivert]+1);
            }
            fprintf(pFile,"\n");
          }
          numElement[eltType]++;
        }
      }
    }
    fclose(pFile);
  }

  void LinearProblem::writeGeoForUnknownEnsightGold(int iUnknown, std::map<felInt, std::vector<felInt> >& refToListOfIds, std::string nameOfGeoFile) {
    const int idVar = m_listUnknown.idVariable(iUnknown);
    const int iMesh = m_listVariable[idVar].idMesh();

    if (m_mesh[iMesh]->flagFormatMesh() == GeometricMeshRegion::FormatUndefined) {
      felisce_error("Empty Data Structure!!! Have you read mesh file before?",__FILE__,__LINE__);
    }

    // init std::unordered_map
    std::unordered_map<std::string, std::pair<ElementType, std::string> > eltFelNameToEnsPair;
    eltFelNameToEnsPair["Nodes"]        = std::make_pair( GeometricMeshRegion::Nod,    "point"     );
    eltFelNameToEnsPair["Segments2"]    = std::make_pair( GeometricMeshRegion::Seg2,   "bar2"      );
    eltFelNameToEnsPair["Segments3"]    = std::make_pair( GeometricMeshRegion::Seg3,   "bar3"      );
    eltFelNameToEnsPair["Triangles3"]   = std::make_pair( GeometricMeshRegion::Tria3,  "tria3"     );
    eltFelNameToEnsPair["Triangles6"]   = std::make_pair( GeometricMeshRegion::Tria6,  "tria6"     );
    eltFelNameToEnsPair["Quadrangles4"] = std::make_pair( GeometricMeshRegion::Quad4,  "quad4"     );
    eltFelNameToEnsPair["Quadrangles8"] = std::make_pair( GeometricMeshRegion::Quad8,  "quad8"     );
    eltFelNameToEnsPair["Tetrahedra4"]  = std::make_pair( GeometricMeshRegion::Tetra4, "tetra4"    );
    eltFelNameToEnsPair["Tetrahedra10"] = std::make_pair( GeometricMeshRegion::Tetra10,"tetra10"   );
    eltFelNameToEnsPair["Pyramids5"]    = std::make_pair( GeometricMeshRegion::Pyram5, "pyramid5"  );
    eltFelNameToEnsPair["Pyramids13"]   = std::make_pair( GeometricMeshRegion::Pyram13,"pyramid13" );
    eltFelNameToEnsPair["Prisms6"]      = std::make_pair( GeometricMeshRegion::Prism6, "penta6"    );
    eltFelNameToEnsPair["Prisms15"]     = std::make_pair( GeometricMeshRegion::Prism15,"penta15"   );
    eltFelNameToEnsPair["Hexahedra8"]   = std::make_pair( GeometricMeshRegion::Hexa8,  "hexa8"     );
    eltFelNameToEnsPair["Hexahedra20"]  = std::make_pair( GeometricMeshRegion::Hexa20, "hexa20"    );

    // clear std::unordered_map
    refToListOfIds.clear();

    // Intialization
    // =============
    std::string fileNameGeo, fileNameRoot, fileName;
    std::string keyWord;
    FILE * pFile;

    if(nameOfGeoFile.empty()) {
      fileNameGeo = listVariable().listVariable()[idVar].name()+".geo";;
      fileNameRoot = FelisceParam::instance().resultDir;
      fileName = fileNameRoot + fileNameGeo;
    } else {
      fileName = nameOfGeoFile;
    }

    pFile = fopen (fileName.c_str(),"w");
    if (pFile==nullptr) {
      std::string command = "mkdir -p " + FelisceParam::instance().resultDir;
      int ierr = system(command.c_str());
      if (ierr>0) {
        FEL_ERROR("Impossible to write "+fileName+"  geo mesh");
      }
      pFile = fopen (fileName.c_str(),"w");
    }

    // Descriptions lines (All Geometry files must contain these first six lines)
    fprintf(pFile,"Geometry file\n");
    fprintf(pFile,"Ensight Gold format\n");
    fprintf(pFile,"node id given\n");
    fprintf(pFile,"element id assign\n");

    // list of type of element by labels
    GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  m_mesh[iMesh]->intRefToEnum();

    felInt label;
    bool(*comparison)(std::pair<felInt, Point >, std::pair<felInt, Point >) = Tools::pairComparison;

    felInt numElemsPerRef = 0;
    std::vector<felInt> vectorIdSupport;
    felInt ielSupportDof;
    felInt idSup;
    Point pt;
    std::vector<felInt> numElement(GeometricMeshRegion::m_numTypesOfElement, 0);
    std::vector<felInt> numElement2(GeometricMeshRegion::m_numTypesOfElement, 0);
    std::vector<felInt> numSupportElt(GeometricMeshRegion::m_numTypesOfElement, 0);

    // loop over references
    for (auto refIt = intRefToEnum.begin(); refIt != intRefToEnum.end(); ++refIt) {
      label = refIt->first;
      fprintf(pFile, "part\n");
      fprintf(pFile, "%10d\n", label + 1);

      std::string descriptionLine = "";
      for (auto typeIt = refIt->second.begin(); typeIt != refIt->second.end(); ++typeIt)
        descriptionLine += GeometricMeshRegion::eltEnumToFelNameGeoEle[*typeIt].first + "_";
      
      descriptionLine += "ref_";
      fprintf(pFile, "%s%d\n", descriptionLine.c_str(), label);
      fprintf(pFile, "coordinates\n");
      
      std::map<felInt, felInt> local2GlobalMapping;
      std::set<std::pair<felInt, Point >, bool(*)(std::pair<felInt, Point >, std::pair<felInt, Point >)> listOfNodes(comparison);
      
      // loop over the type of element having this reference
      for (auto typeIt = refIt->second.begin(); typeIt != refIt->second.end(); ++typeIt) {
        // fill the mappings
        numElemsPerRef = m_mesh[iMesh]->intRefToBegEndMaps[*typeIt][label].second;
        numSupportElt[*typeIt] = 0;

        // loop over the element
        for(felInt iel=0; iel<numElemsPerRef; ++iel) {
          supportDofUnknown(iUnknown).getIdElementSupport(*typeIt, numElement[*typeIt], vectorIdSupport);

          for(std::size_t iSup=0; iSup<vectorIdSupport.size(); ++iSup) {
            ielSupportDof = vectorIdSupport[iSup];
            for(int iSupport=0; iSupport<supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof); iSupport++) {
              idSup = supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[ielSupportDof] + iSupport];
              pt = supportDofUnknown(iUnknown).listNode()[idSup];
              listOfNodes.insert(std::make_pair(idSup, pt));
            }
            ++numSupportElt[*typeIt];
          }
          
          ++numElement[*typeIt];
        }
      }        
      
      // print part and vertices
      fprintf(pFile, "%10d\n", static_cast<int>(listOfNodes.size()));
      felInt localCounter = 1;
      for (auto nodeIt = listOfNodes.begin(); nodeIt != listOfNodes.end(); ++nodeIt) {
        refToListOfIds[label].push_back(nodeIt->first);
        
        fprintf(pFile, "%10d\n", nodeIt->first + 1);
        local2GlobalMapping[nodeIt->first] = localCounter;
        ++localCounter;
      }

      for(felInt icoor=0; icoor<3; ++icoor) {
        for (auto nodeIt = listOfNodes.begin(); nodeIt != listOfNodes.end(); ++nodeIt) {
          fprintf(pFile, "%12.5e\n", nodeIt->second[icoor]);
        }
      }
      
      for (auto typeIt = refIt->second.begin(); typeIt != refIt->second.end(); ++typeIt) {
        keyWord = eltFelNameToEnsPair[GeometricMeshRegion::eltEnumToFelNameGeoEle[*typeIt].first].second;
        fprintf(pFile, "%s\n", keyWord.c_str());
        fprintf(pFile, "%10d\n", numSupportElt[*typeIt]);

        // loop over the element
        numElemsPerRef = m_mesh[iMesh]->intRefToBegEndMaps[*typeIt][label].second;
        for(felInt iel=0; iel<numElemsPerRef; ++iel) {
          supportDofUnknown(iUnknown).getIdElementSupport(*typeIt, numElement2[*typeIt], vectorIdSupport);

          for(std::size_t iSup=0; iSup<vectorIdSupport.size(); ++iSup) {
            ielSupportDof = vectorIdSupport[iSup];
            for(int iSupport=0; iSupport<supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof); iSupport++) {
              fprintf(pFile, "%10d", local2GlobalMapping[supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[ielSupportDof] + iSupport]]);
            }
            fprintf(pFile, "\n");
          }
          
          ++numElement2[*typeIt];
        }
      }
    }
    
    fclose(pFile);
  }
}//
