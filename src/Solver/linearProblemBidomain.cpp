//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: A. Collin
//

// System includes
#include <iostream>
#include <fstream>

// External includes

// Project includes
#include "Solver/linearProblemBidomain.hpp"
#include "InputOutput/io.hpp"
#include "Core/felisceTools.hpp"
#include "Core/filesystemUtil.hpp"
#include "Tools/fe_utilities.hpp"

namespace felisce 
{
LinearProblemBidomain::LinearProblemBidomain():
  LinearProblem("Problem cardiac Bidomain",2),
  m_rom(nullptr),
  m_fePotTransMemb(nullptr),
  m_fePotExtraCell(nullptr),
  m_fePotTransMembCurv(nullptr),
  m_fePotExtraCellCurv(nullptr),
  m_sortedSol(nullptr),
  m_bdf(nullptr),
  m_initializeCVGraphInterface(false),
  m_electrodeNode(nullptr),
  m_electrodeMeasure(nullptr) {
  if (FelisceParam::instance().stateFilter)
    this->setNumberOfMatrix(3);


  if ( FelisceParam::instance().withCVG ) {
#ifdef FELISCE_WITH_CVGRAPH
    m_cvgDirichletVariable = "cvgraphPOTENTIAL";
#endif
    m_seqVecs.Init("cvgraphPOTENTIAL");
    m_seqVecs.Init("cvgraphCURRENT");
  }

}

LinearProblemBidomain::~LinearProblemBidomain() {
  m_rom = nullptr;
  m_fstransient = nullptr;
  m_fePotTransMemb = nullptr;
  m_fePotExtraCell = nullptr;
  m_bdf = nullptr;
  delete [] m_sortedSol;
  // Elisa, Dec. 2014 - Luenberger filter
  if (FelisceParam::instance().electrodeControl) {
    delete [] m_electrodeNode;
    for (felInt i=0; i<m_numElectrodeNode+1; i++) {
      delete [] m_electrodeMeasure[i];
    }
    delete [] m_electrodeMeasure;
    m_electrodeControl.destroy();
  }
}

void LinearProblemBidomain::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
  LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;

    std::vector<PhysicalVariable> listVariable(2);
    std::vector<std::size_t> listNumComp(2);

    listVariable[0] = potTransMemb;
    listNumComp[0] = 1;
    listVariable[1] = potExtraCell;
    listNumComp[1] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(potTransMemb);
    m_listUnknown.push_back(potExtraCell);
    definePhysicalVariable(listVariable,listNumComp);
  }

  void LinearProblemBidomain::readData(IO& io) {
    // Read fibers file (*.00000.vct)
    m_vectorFiber.resize(m_mesh[m_currentMesh]->numPoints()*3);
    io.readVariable(0, 0.,&m_vectorFiber[0], m_vectorFiber.size());
    // Read distance from endocardium file for Zygote geometry (*.00000.scl)
    if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "zygoteImproved") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
      m_vectorDistance.resize(m_numDof,0.);
      io.readVariable(1, 0.,&m_vectorDistance[0],m_mesh[m_currentMesh]->numPoints());
      if ( (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
        if (FelisceParam::instance().hasCoupledAtriaVent) {
          m_vectorAngle.resize(m_numDof,0.);
          io.readVariable(2, 0.,&m_vectorAngle[0],m_mesh[m_currentMesh]->numPoints());
          m_vectorRef.resize(m_numDof,0.);
          io.readVariable(3, 0.,&m_vectorRef[0],m_mesh[m_currentMesh]->numPoints());
      }
    }
  }
}

void LinearProblemBidomain::updateFibers(IO& io,double iteration) { //when the mesh moves
  m_vectorFiber.resize(m_mesh[m_currentMesh]->numPoints()*3);
  io.readVariable(0, iteration,&m_vectorFiber[0], m_vectorFiber.size());
}

void LinearProblemBidomain::readDataDisplacement(std::vector<IO::Pointer>& io,double iteration) {
  // Read displacement file (*.00000.vct)
  if ((FelisceParam::instance().typeOfAppliedCurrent == "zygote")&&(FelisceParam::instance().typeOfAppliedCurrent == "planarWave")){
    const int iMesh = m_listVariable[2].idMesh();
    m_vectorDisp.resize(m_mesh[iMesh]->numPoints()*3,0.);
    io[iMesh]->readVariable(2, iteration,&m_vectorDisp[0], m_vectorDisp.size());
  }
  else{
    const int iMesh = m_listVariable[1].idMesh();
    m_vectorDisp.resize(m_mesh[iMesh]->numPoints()*3,0.);
    io[iMesh]->readVariable(1, iteration,&m_vectorDisp[0], m_vectorDisp.size());
  }
  std::transform(m_vectorDisp.begin(), m_vectorDisp.end(), m_vectorDisp.begin(),std::bind(std::multiplies<double>(),FelisceParam::instance().spaceUnit, std::placeholders::_1));
}


void LinearProblemBidomain::writeEnsightScalar(double* solValue, int idIter, std::string varName) {
  std::string iteration;
  if (idIter < 10)
    iteration = "0000" + std::to_string(idIter);
  else if (idIter < 100)
    iteration = "000" + std::to_string(idIter);
  else if (idIter < 1000)
    iteration = "00" + std::to_string(idIter);
  else if (idIter < 10000)
    iteration = "0" + std::to_string(idIter);
  else if (idIter < 100000)
    iteration = std::to_string(idIter);

  std::string fileName = FelisceParam::instance().resultDir + "/" + varName + "." + iteration + ".scl";
  FILE * solFile;
  solFile = fopen(fileName.c_str(),"w");
  fprintf(solFile, "Scalar per node\n");
  int count = 0;
  for(int j=0; j < static_cast<int>(m_numDof/2); j++) {
    fprintf(solFile,"%12.5e", solValue[j]);
    count++;
    if(count == 6) {
      fprintf(solFile,"\n");
      count = 0;
    }
  }
  fprintf(solFile,"\n");
  fclose(solFile);
}

double LinearProblemBidomain::conductivityHom(int currentLabel) {
  double conductivity = 1.0;

  if (FelisceParam::instance().hasHeteroCondAtria) {
    if (std::fabs(currentLabel - 31.0) < 1.0e-12) { //SN
      conductivity = 0.3;
    } else if (std::fabs(currentLabel - 32.0) < 1.0e-12) { //CT
      conductivity = 3.0;
    } else if (std::fabs(currentLabel - 33.0) < 1.0e-12) { //BB
      conductivity = 4.75;
    } else if (std::fabs(currentLabel - 34.0) < 1.0e-12) { //RAI
      conductivity = 0.375;
    } else if (std::fabs(currentLabel - 35.0) < 1.0e-12) { //PM
      conductivity = 1.5;
    } else { //regular atria and F0
      conductivity = 1.0;
    }
  }

  return conductivity;
}


double LinearProblemBidomain::conductivityHet(int currentLabel) {
  double conductivity = 1.0;

  if (FelisceParam::instance().hasHeteroCondAtria) {
    if (std::fabs(currentLabel - 31.0) < 1.0e-12) { //SN
      conductivity = 0.36;
    } else if (std::fabs(currentLabel - 32.0) < 1.0e-12) { //CT
      conductivity = 4.5;
    } else if (std::fabs(currentLabel - 33.0) < 1.0e-12) { //BB
      conductivity = 7.75;
    } else if (std::fabs(currentLabel - 34.0) < 1.0e-12) { //RAI
      conductivity = 0.45;
    } else if (std::fabs(currentLabel - 35.0) < 1.0e-12) { //PM
      conductivity = 1.8;
    } else if (std::fabs(currentLabel - 36.0) < 1.0e-12) { //F0
      conductivity = 0.9;
    } else { //regular atria
      conductivity = 1.0;
    }
  }

  return conductivity;
}


void LinearProblemBidomain::writeEnsightCase(int numIt, std::string varName) {

  FILE * pFile;
  std::string caseName = FelisceParam::instance().resultDir + "/" + varName + ".case";
  pFile = fopen(caseName.c_str(),"w");
  fprintf(pFile,"FORMAT\n");
  fprintf(pFile,"type: ensight\n");
  fprintf(pFile, "GEOMETRY\n");
  std::string nameGeo = "model: 1 " + FelisceParam::instance().outputMesh[0] + "\n";
  fprintf(pFile,"%s", nameGeo.c_str());
  fprintf(pFile, "VARIABLE\n");
  std::string secName = "scalar per node: 1 " + varName + " " + varName +".*****.scl\n";
  fprintf(pFile, "%s", secName.c_str());
  fprintf(pFile, "TIME\n");
  fprintf(pFile, "time std::set: %d \n", 1);
  fprintf(pFile, "number of steps: %d \n", numIt);
  fprintf(pFile, "filename start number: %d\n", 0);
  fprintf(pFile, "filename increment: %d\n", 1);
  fprintf(pFile, "time values:\n");

  const double dt = FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution;
  int count=0;
  for(int j=0; j < numIt; j++) {
    double valT = j*dt;
    fprintf(pFile,"%lf ",valT);
    count++;
    if(count == 6) {
      fprintf(pFile,"\n");
      count=0;
    }
  }
  fclose(pFile);
}


void LinearProblemBidomain::writeEnsightCaseCurrents(int numIt, std::vector<std::string> varName) {

  FILE * pFile;
  std::string caseName = FelisceParam::instance().resultDir + "/" + "courants" + ".case";
  pFile = fopen(caseName.c_str(),"w");
  fprintf(pFile,"FORMAT\n");
  fprintf(pFile,"type: ensight\n");
  fprintf(pFile, "GEOMETRY\n");
  std::string nameGeo = "model: 1 " + FelisceParam::instance().outputMesh[currentMesh()] + "\n";
  fprintf(pFile,"%s", nameGeo.c_str());
  fprintf(pFile, "VARIABLE\n");

  const double nbvarName = varName.size();
  for(int i=0; i<nbvarName; i++) {
    std::string secName = "scalar per node: 1 " + varName[i] + " " + varName[i] +".*****.scl\n";
    fprintf(pFile, "%s", secName.c_str());
  }

  fprintf(pFile, "TIME\n");
  fprintf(pFile, "time std::set: %d \n", 1);
  fprintf(pFile, "number of steps: %d \n", numIt);
  fprintf(pFile, "filename start number: %d\n", 0);
  fprintf(pFile, "filename increment: %d\n", 1);
  fprintf(pFile, "time values:\n");

  const double dt = FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution;
  int count=0;
  for(int j=0; j < numIt; j++) {
    double valT = j*dt;
    fprintf(pFile,"%lf ",valT);
    count++;
    if(count == 6) {
      fprintf(pFile,"\n");
      count=0;
    }
  }
  fclose(pFile);
}


void LinearProblemBidomain::writeSolution(int rank, std::vector<IO::Pointer>& io, double& time, int iteration) {
  LinearProblem::writeSolution(rank,io, time, iteration);
  // Add fiber as an output variable.
  /*
    io.writeSolution(rank, time, iteration, 1, "fiber", m_fiber, m_supportDofUnknown[0].listNode().size(), m_dimension);
    if (FelisceParam::instance().typeOfAppliedCurrent == "zygote")
    {
    // Add distance std::unordered_map as an output variable.
    io.writeSolution(rank, time, iteration, 0, "distance", m_endocardiumDistance, m_supportDofUnknown[0].listNode().size(), m_dimension);
    }
    */
}


void LinearProblemBidomain::getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber) {
  //Warning: the fibers must be normalized.
  int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
  elemFiber.resize( numSupport*3,0.);
  for (int i = 0; i < numSupport; i++) {
    elemFiber[i*3] = m_vectorFiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
    elemFiber[i*3+1] = m_vectorFiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+1];
    elemFiber[i*3+2] = m_vectorFiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+2];
  }
}

void LinearProblemBidomain::getAngle(felInt iel, int iUnknown, std::vector<double>& elemAngle) {
  int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
  elemAngle.resize(numSupport,0.);
  for (int i = 0; i < numSupport; i++) {
    elemAngle[i] = m_vectorAngle[(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
  }
}

void LinearProblemBidomain::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_ELT_TYPE;
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  //Init pointer on Finite Element, Variable or idVariable
  m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
  m_ipotExtraCell = m_listVariable.getVariableIdList(potExtraCell);
  m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
  m_fePotExtraCell = m_fePotTransMemb;

}

void LinearProblemBidomain::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  IGNORE_UNUSED_ELEM_ID_POINT;
  m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
  m_fePotTransMemb->updateFirstDeriv(0, elemPoint);

  // Assembling matrices[0]
  double coefTime = 1./m_fstransient->timeStep;
  double coefAm = FelisceParam::instance().Am;
  double coefCm = FelisceParam::instance().Cm;
  double coef = m_bdf->coeffDeriv0()*coefTime*coefAm*coefCm;

  //1st equation : Am*Cm/dt V_m
  m_elementMat[0]->phi_i_phi_j(coef,*m_fePotTransMemb,0,0,1);

  if (FelisceParam::instance().testCase == 1) {
    std::vector<double> elemFiber;
    getFiberDirection(iel,m_ipotTransMemb, elemFiber);

    if (FelisceParam::instance().model == "Bidomain") {
      //1st equation : \sigma_i^t \grad V_m
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      //1st equation : (\sigma_i^l-\sigma_i^t) a vec a \grad V_m
      m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);

      //1st equation : \sigma_i^t \grad u_e
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotExtraCell,0,1,1);

      //1st equation : (\sigma_i^l-\sigma_i^t) a vec a \grad u_e
      m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotExtraCell,0,1,1);

      //2nd equation : \sigma_i^t \grad V_m
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,1,0,1);

      //2nd equation : (\sigma_i^l-\sigma_i^t) a vec a \grad V_m
      m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,1,0,1);

      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotExtraCell,1,1,1);

      //2nd equation : ((\sigma_i^l-\sigma_i^t)+(\sigma_e^l-\sigma_e^t)) a vec a \grad u_e
      m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraFiberTensor-FelisceParam::instance().extraTransvTensor,elemFiber,*m_fePotExtraCell,1,1,1);

    }
    else if (FelisceParam::instance().model == "Monodomain") {
      //1st equation : \sigma_i^t \grad V_m
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      //1st equation : (\sigma_i^l-\sigma_i^t) a vec a \grad V_m
      m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);

      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotExtraCell,1,1,1);

      //2nd equation : ((\sigma_i^l-\sigma_i^t)+(\sigma_e^l-\sigma_e^t)) a vec a \grad u_e
      m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraFiberTensor-FelisceParam::instance().extraTransvTensor,elemFiber,*m_fePotExtraCell,1,1,1);
    }
  }
  else {
    if (FelisceParam::instance().model == "Bidomain") {
      //1st equation : \sigma_i^t \grad V_m
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      //1st equation : \sigma_i^t \grad u_e
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotExtraCell,0,1,1);

      //2nd equation : \sigma_i^t \grad V_m
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,1,0,1);

      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotExtraCell,1,1,1);

    }
    else if (FelisceParam::instance().model == "Monodomain") {
      //1st equation : \sigma_i^t \grad V_m
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotExtraCell,1,1,1);
    }
  }
  //Assembling matrix(1) = mass (only first quadrant).
  m_elementMat[1]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);
  m_elementMat[1]->phi_i_phi_j(1.,*m_fePotTransMemb,1,1,1);

  if (FelisceParam::instance().stateFilter) {
    *m_elementMat[2]=*m_elementMat[0];
  }
}

void LinearProblemBidomain::computeElementArraySurfaceModel(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
  (void) elemIdPoint;
  (void) flagMatrixRHS;
  m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
  m_fePotTransMembCurv = m_listCurvilinearFiniteElement[m_ipotTransMemb];
  m_fePotExtraCellCurv = m_fePotTransMembCurv;

  m_fePotTransMembCurv->updateMeasNormalContra(0, elemPoint);

  auto bndRefIt = find(FelisceParam::instance().notBoundarySurfaceLabels.begin(), FelisceParam::instance().notBoundarySurfaceLabels.end(), m_currentLabel);

  if(bndRefIt != FelisceParam::instance().notBoundarySurfaceLabels.end()) {
//    if ((m_currentLabel > 29) && (m_currentLabel < 41)) {
    std::vector<double> elemAngle;
    getAngle(iel,m_ipotTransMemb,elemAngle);

    std::vector<double> elemFiber;
    getFiberDirection(iel,m_ipotTransMemb,elemFiber);

    // Assembling matrix(0)
    double coefTime = 1./m_fstransient->timeStep;
    double coefAm = FelisceParam::instance().Am;
    double coefCm = FelisceParam::instance().Cm;
    double coef = m_bdf->coeffDeriv0()*coefTime*coefAm*coefCm;

    double atriaThick=0.2; // cm - atria thickness

    //1st equation : Am*Cm/dt V_m
    m_elementMatBD[0]->phi_i_phi_j(atriaThick*coef,*m_fePotTransMembCurv,0,0,1);

    //1st equation : \sigma_i^t \grad V_m
    if (m_currentLabel < 40) { // regular atrial myocardium
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(atriaThick*FelisceParam::instance().intraTransvTensorAtria*conductivityHom(m_currentLabel),*m_fePotTransMembCurv,0,0,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j(atriaThick*(FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotTransMembCurv,0,0,1);
    }

    //1st equation : \sigma_i^t \grad u_e
    if (m_currentLabel < 40) { // regular atrial myocardium
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(atriaThick*FelisceParam::instance().intraTransvTensorAtria*conductivityHom(m_currentLabel),*m_fePotExtraCellCurv,0,1,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j(atriaThick*(FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotExtraCellCurv,0,1,1);
    }

    //2nd equation : \sigma_i^t \grad V_m
    if (m_currentLabel < 40) { // regular atrial myocardium
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(atriaThick*FelisceParam::instance().intraTransvTensorAtria*conductivityHom(m_currentLabel),*m_fePotTransMembCurv,1,0,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j(atriaThick*(FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotTransMembCurv,1,0,1);
    }

    //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
    double connectionCoeff = 3.e-4; // 0.0003
    if (m_currentLabel < 40) { // atrial myocardium
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(atriaThick*(FelisceParam::instance().intraTransvTensorAtria+FelisceParam::instance().extraTransvTensorAtria)*conductivityHom(m_currentLabel),*m_fePotExtraCellCurv,1,1,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j(atriaThick*(FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria + FelisceParam::instance().extraFiberTensorAtria - FelisceParam::instance().extraTransvTensorAtria)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotExtraCellCurv,1,1,1);
    } else if (std::fabs(m_currentLabel - 40) < 1.0e-8) { // connection aera
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(atriaThick*(FelisceParam::instance().extraTransvTensorAtria)*connectionCoeff,*m_fePotExtraCellCurv,1,1,1);
    }

    //Assembling matrix(1) = mass (only first quadrant if hasAppliedExteriorCurrent = false)
    m_elementMatBD[1]->phi_i_phi_j(atriaThick*1.,*m_fePotTransMembCurv,0,0,1);
    m_elementMatBD[1]->phi_i_phi_j(atriaThick*1.,*m_fePotTransMembCurv,1,1,1);
  }
}

void LinearProblemBidomain::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS /*flagMatrixRHS*/) {

  (void)iel;

  m_curvFe->updateMeas(0,elemPoint);
// #ifdef FELISCE_WITH_CVGRAPH
//     if (FelisceParam::instance().withCVG ) {
//       this->cvgraphNaturalBC(iel);
//     }
// #endif
}

void LinearProblemBidomain::addMatrixRHS() {

  //Rebuild the matrix with the filter
  if (FelisceParam::instance().stateFilter ) {
    matrix(0).copyFrom(matrix(2),SAME_NONZERO_PATTERN);
  }

  // _RHS = m_mass * _RHS.
  PetscVector tmpB;
  tmpB.duplicateFrom(vector());
  tmpB.copyFrom(vector());
  mult(matrix(1),tmpB,vector()); // _RHS = m_mass * tmpB
  tmpB.destroy();

  if (m_initializeCVGraphInterface) {
    vector(1).copyFrom(vector());
  }

  if(FelisceParam::instance().useROM) {
    if (m_fstransient->iteration == 1) {
      m_rom->fullToReducedMatrix(matrix(0));
    }
    m_rom->fullToReducedRHS(vector());
  }

}

void LinearProblemBidomain::readMatch() {
  // Input files.
  std::string matchFileName = FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().ECGmatchFile;// load the match file (heart nodes)

  std::ifstream matchFile(matchFileName.c_str());
  if(!matchFile) {
    FEL_ERROR("Fatal error: cannot open match file " + matchFileName );
  }
  if (FelisceParam::verbose() > 0) {
    std::cout << "Reading " << matchFileName << std::endl;
  }
  matchFile >> m_numElectrodeNode;
  m_electrodeNode = new felInt[m_numElectrodeNode];
  felInt value;
  for(int j=0; j<m_numElectrodeNode; j++) {
    matchFile >> value;
    m_electrodeNode[j] = value;
  }
  matchFile.close();
}

// Elisa, Dec. 2014 - read measures for Luenberger filter
void LinearProblemBidomain::readElectrodeMeasure() {
  // load the measures
  std::string inputFileName = FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().ECGfileName + ".tab";
  std::string outputFileName = FelisceParam::instance().resultDir + "/" + FelisceParam::instance().ECGfileName + "_control.tab";
  int ierr = 0;
  if ( !filesystemUtil::directoryExists(FelisceParam::instance().resultDir.c_str()) ) {
    std::string command = "mkdir -p " + FelisceParam::instance().resultDir;
    ierr = system(command.c_str());
    if (ierr) {
      std::cout << "Cannot copy control file in resultDir." << std::endl;
    }
  }
  if (ierr == 0) {
    filesystemUtil::copyFileSomewhereElse(inputFileName,outputFileName);
  }

  std::ifstream ecgFile(inputFileName.c_str(), std::ios_base::in);
  if ( !ecgFile.is_open() ) {
    FEL_ERROR(" ERROR: Can not open file " + inputFileName);
  }

  m_electrodeMeasure = new double* [m_numElectrodeNode+1];
  for (int i=0; i<m_numElectrodeNode+1; i++) {
    m_electrodeMeasure[i] = new double [FelisceParam::instance().timeIterationMax];
  }

  for (int j=0; j<FelisceParam::instance().timeIterationMax; j++) {
    for(int i=0; i<m_numElectrodeNode+1; i++) {
      ecgFile >> m_electrodeMeasure[i][j];
    }
  }
  ecgFile.close();

  m_electrodeControl.duplicateFrom(vector());
}

// Elisa, Dec. 2014 - Luenberger filter
void LinearProblemBidomain::addElectrodeCondtrol() {
  if (m_electrodeNode == nullptr) {
    readMatch();
  }
  if (m_electrodeMeasure == nullptr) {
    readElectrodeMeasure();
  }

  if (FelisceParam::instance().electrodeControl) {
    if ( !Tools::almostEqual(m_electrodeMeasure[0][m_fstransient->iteration-1], m_fstransient->time-m_fstransient->timeStep, m_fstransient->timeStep/10.) ) {
      FEL_WARNING("Measures physical time different from solver physical time.\n");
    }
  }

  m_electrodeControl.set(0.);

  int sizePot = numDofLocalPerUnknown(potExtraCell);
  double value[sizePot];
  felInt idLocalUe[sizePot];
  felInt idGlobalUe[sizePot];
  felInt idLocalVm[sizePot];
  felInt idGlobalVm[sizePot];

  for (felInt i = 0; i < sizePot; i++) {
    idLocalUe[i] = i;
    idLocalVm[i] = i;
    value[i] = 0.;
  }
  ISLocalToGlobalMappingApply(mappingDofLocalToDofGlobal(potTransMemb),sizePot,&idLocalVm[0],&idGlobalVm[0]);
  ISLocalToGlobalMappingApply(mappingDofLocalToDofGlobal(potExtraCell),sizePot,&idLocalUe[0],&idGlobalUe[0]);

  for (felInt i = 0; i < m_numElectrodeNode; i++) {
    felInt idElectrode = m_electrodeNode[i]-1;
    value[idGlobalVm[idElectrode]] = m_electrodeMeasure[i+1][m_fstransient->iteration-1];
    double valuePot;
    this->solution().getValues(1,&idGlobalUe[idElectrode],&valuePot);
    value[idGlobalVm[idElectrode]] -= valuePot;
  }

  m_electrodeControl.setValues(sizePot,&idGlobalVm[0],&value[0],INSERT_VALUES);

  m_electrodeControl.assembly();

  vector().axpy(-FelisceParam::instance().aFilter,m_electrodeControl);

  vector().assembly();

}

void LinearProblemBidomain::assembleOnlyMassMatrix(PetscMatrix& mass)  
{
  // Allocation
  mass.duplicateFrom(matrix(0),MAT_DO_NOT_COPY_VALUES);
  mass.zeroEntries();

  /* Loop function */
  auto function = [this,&mass](ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt& ielSupportDof) {
    setElemPoint(eltType, iel, elemPoint, elemIdPoint, &ielSupportDof);
    m_elementMat[0]->zero();
    userComputeMass(elemPoint, elemIdPoint, ielSupportDof);
    setValueCustomMatrix(ielSupportDof, ielSupportDof, mass);
    return 0.0;
  };
  /* Pre-function */
  auto functionpre = [this](ElementType& eltType) {
    defineFiniteElement(eltType);
    initElementArray();
    allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
  };
  /* Post-function */
  auto functionpost = [this](ElementType& eltType) {
    IGNORE_UNUSED_ARGUMENT(eltType);
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
  };
  FEUtilities::LoopOverElements(*m_meshLocal[m_currentMesh], m_meshLocal[m_currentMesh]->bagElementTypeDomain(), &function, &functionpre, &functionpost);

  // Final assembly
  mass.assembly(MAT_FINAL_ASSEMBLY);
}

void LinearProblemBidomain::initAndStoreMassMatrix() {
  assembleOnlyMassMatrix(m_massMatrix);
  // TO DO: This should not be here, it's only temporary. Should be moved to prepareCVGRobin().
  initMassBoundaryForStimulation();
#ifdef FELISCE_WITH_CVGRAPH
  if ( FelisceParam::instance().withCVG ) {
    initMassBoundaryForCVG();
  }
#endif
}

#ifdef FELISCE_WITH_CVGRAPH
void LinearProblemBidomain::initMassBoundaryForCVG() {
  this->assembleMassBoundaryOnly(&LinearProblemBidomain::massMatrixComputer,
                                        this->slave()->interfaceLabels(/*iConn*/0),
                                        &LinearProblemBidomain::initPerETMass,
                                        &LinearProblemBidomain::updateFE,
                                  this->dofBD(/*iConn*/0), this->massBDVec()[/*iConn*/0]);
}

/*! \brief Function to assemble the mass matrix
  *  Function to be called in the assemblyLoopBoundaryGeneral
  */
void LinearProblemBidomain::massMatrixComputer(felInt ielSupportDof) {
  this->m_elementMatBD[0]->zero();
  this->m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFe,1,1,1);
  this->setValueMatrixBD(ielSupportDof);
}
void
LinearProblemBidomain::initPerETMass() {
  felInt numDofTotal = 0;
  //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
  for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
    numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
  }
  m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);

  m_curvFe = this->m_listCurvilinearFiniteElement[ m_ipotExtraCell ];
}
void
LinearProblemBidomain::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
  m_curvFe->updateMeasNormal(0, elemPoint);
}
#endif

#ifdef FELISCE_WITH_CVGRAPH
void LinearProblemBidomain::readData() {
  for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
    //std::cout << "####### LinearProblemBidomain::readData(): " << this->slave()->numVarToRead(iConn) << " variables to read" << std::endl;
    std::vector<PetscVector> vecs;
    for ( std::size_t cVar(0); cVar<this->slave()->numVarToRead(iConn); ++cVar) {
      std::string varToRead = this->slave()->readVariable(iConn,cVar);
      if ( varToRead  == "CURRENT" ) {
        vecs.push_back(m_seqVecs.Get("cvgraphCURRENT"));
      } else if ( varToRead  == "POTENTIAL" ) {
        vecs.push_back(m_seqVecs.Get("cvgraphPOTENTIAL"));
      } else {
        std::cout<<"Connection: "<<iConn<<"--variable to read: "<< varToRead << std::endl;
        FEL_ERROR("Error in variable to read");
      }
    }
    this->slave()->receiveData(vecs,iConn);
  }
}
void LinearProblemBidomain::sendData() {
  //std::cout << "LinearProblemBidomain::sendData()" <<std::endl;
  this->gatherSolution();
  for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
    std::vector<PetscVector> vecs;
    for ( std::size_t cVar(0); cVar<this->slave()->numVarToSend(iConn); ++cVar) {
      std::string varToSend = this->slave()->sendVariable(iConn, cVar);
      if (  varToSend == "POTENTIAL" ) {
        vecs.push_back(this->sequentialSolution());
      } else if ( varToSend == "CURRENT" ) {
        // TODO: Add prepareResidual() and move this to startIterationCVG
        this->computeResidual();
        vecs.push_back(this->seqResidual());
      } else {
        //std::cout<<"Connection: "<<iConn<<"--variable to send: "<< varToSend << std::endl;
        FEL_ERROR("Error in variable to read");
      }
    }
    this->slave()->sendData(vecs,iConn);
  }
}
void LinearProblemBidomain::addCurrentToRHS() {
  //! for each dof on the boundary on this proc.
  felInt numLocalDofInterface=this->dofBD(/*iBD*/0).numLocalDofInterface();
  std::vector<double> tmp(numLocalDofInterface);
  m_seqVecs.Get("cvgraphCURRENT").getValues( numLocalDofInterface, this->dofBD(/*iBD*/0).loc2PetscVolPtr(), tmp.data() );
  this->vector().setValues(numLocalDofInterface,this->dofBD(/*iBD*/0).loc2PetscVolPtr(),tmp.data(),ADD_VALUES);
  this->vector().assembly();
}

void LinearProblemBidomain::cvgAddRobinToRHS(){
  //std::cout << "LinearProblemBidomain::cvgAddRobinToRHS()" << std::endl;
  //std::cout << "  alpha_robin = " << FelisceParam::instance().alphaRobin[0/*iRobin*/] << std::endl;
}

#endif

}
