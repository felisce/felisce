//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M.Aletti
//


#ifndef _LINEARPROBLEMPERFECTFLUID_HPP
#define _LINEARPROBLEMPERFECTFLUID_HPP

// System includes

// External includes

// Project includes
#include "FiniteElement/elementField.hpp"
#include "Solver/reducedSteklov.hpp"

namespace felisce {
  /*!
   \class LinearProblemPerfectFluid
   \authors M.Aletti
   \date 2015
   \brief Manage specific functions for perfect fluid for application to Intra Ocular Pressure (iop)
  */
  class LinearProblemPerfectFluid : public LinearProblem {
  public:
    // ====================================================================
    // Simple constructor/destructor                                      =
    // ====================================================================
    LinearProblemPerfectFluid();
    ~LinearProblemPerfectFluid() override= default;
    // ====================================================================
    // This function overrides LinearProblem::initialize                  =
    // ====================================================================
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    // ====================================================================
    //     With these two functions we are able to initialize the petsc   =
    //     vectors used at the interface                                  =
    // ====================================================================
    void initializePetscVectors();
    void resetInterfaceQuantities();
    // ====================================================================
    //  Getters                                                           =
    // ====================================================================
    inline felInt idVarIop() const      {      return m_idVarIop;       }
    inline felInt idUnknownIop() const  {      return m_iUnknownIop;    }
  protected:
    // ====================================================================
    //  This two functions are here to compute the matrix                 =
    //  of the system                                                     =
    // ====================================================================
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) override;  
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) override;
    // ====================================================================
    // relaxation parameter, std::set in the datafile, for the coupling        =
    // boundary condition                                                 =
    // ====================================================================
    double m_relaxParam;
    // ====================================================================
    // id variable and id unknown and a pointer to current fe
    // ====================================================================
    felInt m_idVarIop;
    felInt m_iUnknownIop;
    CurrentFiniteElement *m_feIop;
  };
}
#endif
