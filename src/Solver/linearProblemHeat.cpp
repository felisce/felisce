//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:     J. Foulon & J-F. Gerbeau & V. Martin
//

/*!
 \file linearProblemHeat.cpp
 \authors J. Foulon & J-F. Gerbeau & V. Martin
 \date 05/01/2011
 \brief File where is implemented laplacian class.
 */

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHeat.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"


namespace felisce {
  LinearProblemHeat::LinearProblemHeat():
    LinearProblem("Heat equation",/*numberOfMatrices*/2),
    m_sourceTermDynamicValue(nullptr)
  {
    m_seqVecs.Init("oldTemperature");
    // When using cvgraph, the following vectors are needed.
    // If we use a D2N or a N2D some of them may be unused
#ifdef FELISCE_WITH_CVGRAPH
    if ( FelisceParam::instance().withCVG ) {
      m_cvgDirichletVariable = "cvgraphTEMPERATURE";
      m_seqVecs.Init("cvgraphFLOWOFHEAT");
      m_seqVecs.Init("cvgraphTEMPERATURE");
      }
#endif
  }
  void LinearProblemHeat::updateOldQuantities() {
    m_seqVecs.Get("oldTemperature").copyFrom(sequentialSolution());
  }
  void LinearProblemHeat::readDataDisplacement(std::vector<IO::Pointer>& io, double iteration) {
    // Read displacement file (*.00000.vct)
    const int iMesh = m_listVariable[0].idMesh();
    m_vectorDisp.resize(m_mesh[iMesh]->numPoints()*3,0.);
    io[iMesh]->readVariable(0, iteration,&m_vectorDisp[0], m_vectorDisp.size());
  }

  void LinearProblemHeat::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = temperature;
    listNumComp[0] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(temperature);
    definePhysicalVariable(listVariable,listNumComp);

    m_iTemperature = m_listVariable.getVariableIdList(temperature);
    m_sourceTermDynamicValue = FelisceParam::instance().elementFieldDynamicValue("heatSourceTerm");
  }

  void LinearProblemHeat::initPerElementType(ElementType /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/) {
    m_feTemp = m_listCurrentFiniteElement[m_iTemperature];

    // define elemField to put term u_n_1.phi_j in RHS.
    // m_elemField contains u_n_1 values.
    m_elemField.initialize(DOF_FIELD,*m_feTemp);
  }

  void LinearProblemHeat::initPerElementTypeBoundaryCondition(ElementType& /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/) {
#ifdef FELISCE_WITH_CVGRAPH
    if (FelisceParam::instance().withCVG ) {
      m_curvFeTemp = m_listCurvilinearFiniteElement[m_iTemperature];
      if ( slave()->thereIsAtLeastOneRobinCondition() ){
        m_robinAux.initialize( DOF_FIELD, *m_curvFeTemp, this->dimension() );
      }
    }
#endif
  }
  void LinearProblemHeat::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    const double coef = 1./m_fstransient->timeStep;

    m_feTemp->updateFirstDeriv(0, elemPoint);

    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
      if ( m_fstransient->iteration == 0 ) {
        m_elementMat[1]->grad_phi_i_grad_phi_j(1.,*m_feTemp,0,0,1);
        m_elementMat[1]->phi_i_phi_j(coef,*m_feTemp,0,0,1);
      }
    }

    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
      if ( m_fstransient->iteration > 0 ) {
        // vector operators.
        m_elemField.setValue(m_seqVecs.Get("oldTemperature"), *m_feTemp, iel, m_iTemperature, m_ao, dof());
        m_elementVector[0]->source(coef,*m_feTemp,m_elemField,0,1);

        if (m_sourceTermDynamicValue  != nullptr) {
          m_sourceTerm.setValue(*m_feTemp, *m_sourceTermDynamicValue, m_fstransient->time);
          assert(!m_elementVector.empty());
          m_elementVector[0]->source(1.,*m_feTemp,m_sourceTerm,0,1);
        }
      }
    }
  }

  void LinearProblemHeat::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS /*flagMatrixRHS*/) {
    (void) iel;
    (void) elemPoint;
#ifdef FELISCE_WITH_CVGRAPH
    if (FelisceParam::instance().withCVG ) {
      m_curvFeTemp->updateMeasNormal(0, elemPoint);
      cvgraphNaturalBC(iel);
    }
#endif
  }
#ifdef FELISCE_WITH_CVGRAPH
  void LinearProblemHeat::sendData() {
    gatherSolution();
    for ( std::size_t iConn(0); iConn<slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for (std::size_t cVar(0); cVar<slave()->numVarToSend(iConn); ++cVar) {
        std::string varToSend=slave()->sendVariable(iConn,cVar);
        if (varToSend=="FLOWOFHEAT") {
          prepareResidual(iConn);
          vecs.push_back(m_seqStressOut);
        } else if(varToSend =="TEMPERATURE") {
          vecs.push_back(this->sequentialSolution());
        } else {
          std::cout<<"variable to send: "<<varToSend<<std::endl;
          FEL_ERROR("Error in variable to read");
        }
      }
      slave()->sendData(vecs,iConn);
    }
  }
  void LinearProblemHeat::readData() {
    for ( std::size_t iConn(0); iConn<slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for (std::size_t cVar(0); cVar<slave()->numVarToRead(iConn); ++cVar) {
        std::string varToRead=slave()->readVariable(iConn,cVar);
        if ( varToRead == "FLOWOFHEAT" ) {
          vecs.push_back(m_seqVecs.Get("cvgraphFLOWOFHEAT"));
        } else if  ( varToRead == "TEMPERATURE" ) {
          vecs.push_back(m_seqVecs.Get("cvgraphTEMPERATURE"));
        } else {
          std::cout<<"Connection: "<<iConn<<"variable to read: "<<varToRead<<std::endl;
          FEL_ERROR("Error in variable to read");
        }
      }
      slave()->receiveData(vecs,iConn);
    }
  }
  /*! \brief Function to assemble the mass matrix
   *  Function to be called in the assemblyLoopBoundaryGeneral
   */
  void
  LinearProblemHeat::initPerETMass() {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
    for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    m_curvFeTemp = this->m_listCurvilinearFiniteElement[ m_iTemperature ];
  }
  
  void LinearProblemHeat::massMatrixComputer(felInt ielSupportDof) {
    m_elementMatBD[0]->zero();
    m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFeTemp,/*iblock*/0,/*iblock*/0,1/*dim*/);
    setValueMatrixBD(ielSupportDof);
  }

  void LinearProblemHeat::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
    m_curvFeTemp->updateMeasNormal(0, elemPoint);
  }

  void LinearProblemHeat::cvgraphNaturalBC(felInt iel) {
    BoundaryCondition* BC;
    // Loop over the cvgraph connections
    for ( std::size_t iConn(0); iConn<slave()->numConnections(); ++iConn ) {
      // Extract the labels of the current connection
      std::vector<int> labels = slave()->interfaceLabels(iConn);
      // Verify if the current label is part of the interface of this connection
      if ( std::find( labels.begin(), labels.end(), m_currentLabel ) != labels.end() ) {
        switch ( slave()->numVarToRead(iConn) ) {
        case 1:  // only one var to read: neumann interface BC
          // Verify that we have to apply the stress in its strong form.
          if ( slave()->readVariable(iConn,/*cVar*/0) == "FLOWOFHEAT" && slave()->residualInterpolationType(iConn) == 0 ) {
            // Look through the different neumann conditions to find the correct one.
            for (std::size_t iNeumann=0; iNeumann<m_boundaryConditionList.numNeumannBoundaryCondition(); iNeumann++ ) {
              BC = m_boundaryConditionList.Neumann(iNeumann);//get the the BC
              const auto& list_bc = BC->listLabel();
              if ( list_bc.find(m_currentLabel) !=  list_bc.end()) {
                //compute the force term.
                m_elemFieldNeumann[iNeumann].setValue( m_seqVecs.Get("cvgraphFLOWOFHEAT"), *m_curvFeTemp  , iel, m_iTemperature, m_ao, dof());
              }
            }
          }
          break;
        case 2: //two variables to read: robin interface BC
          // Look through the different robin conditions to find the correct one.
          for (std::size_t iRobin=0; iRobin<m_boundaryConditionList.numRobinBoundaryCondition(); iRobin++ ) {
            BC = m_boundaryConditionList.Robin(iRobin);
            const auto& list_bc = BC->listLabel();
            if ( list_bc.find(m_currentLabel) !=  list_bc.end()) {
              if ( slave()->residualInterpolationType(iConn) == 0 ) {
                m_elemFieldRobin[iRobin].setValue( m_seqVecs.Get("cvgraphFLOWOFHEAT"), *m_curvFeTemp  , iel, m_iTemperature, m_ao, dof());
              } else {
                m_elemFieldRobin[iRobin].val *=0 ;
              }
              m_robinAux.setValue(m_seqVecs.Get("cvgraphTEMPERATURE"),*m_curvFeTemp  , iel, m_iTemperature, m_ao, dof());
              m_elemFieldRobin[iRobin].val += FelisceParam::instance().alphaRobin[iRobin]*m_robinAux.val;
            }
          }
          break;
        default:
          FEL_ERROR("Three variables to read not yet implemented for this linear problem.");
          break;
        }
      }
    }
  }
  void LinearProblemHeat::assembleMassBoundaryAndInitKSP( std::size_t iConn ) {
    LinearProblem::assembleMassBoundaryAndInitKSP(&LinearProblemHeat::massMatrixComputer,
                                                  this->slave()->interfaceLabels(iConn),
                                                  &LinearProblemHeat::initPerETMass,
                                                  &LinearProblemHeat::updateFE,
                                                  iConn);
  }
#endif
}
