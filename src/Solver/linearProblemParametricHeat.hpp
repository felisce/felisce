//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & M. Fragu
//

#ifndef _LINEARPROBLEMPARAMETRICHEAT_HPP
#define _LINEARPROBLEMPARAMETRICHEAT_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
  /*!
   \class LinearProblemParametricHeat
   \authors J. Foulon & M. Fragu
   \date 16/11/2011
   \brief Manage specific functions for parametric heat problem with 2 matrix
   and 2 vectors.
   */
  class LinearProblemParametricHeat:
    public LinearProblem {
  public:
    LinearProblemParametricHeat();
    ~LinearProblemParametricHeat() override;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      (void) label;
      (void) flagMatrixRHS;
    }

    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      (void) eltType;
      (void) flagMatrixRHS;
    }

    void initPerDomainBD(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      (void) label;
      (void) flagMatrixRHS;
    }

    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      (void) flagMatrixRHS;
      (void) elemIdPoint;
      (void) elemPoint;
      (void) iel;

    }


    void defineBC() override {}
    void finalizeEssBCConstantInT() {}
    void finalizeEssBCTransient() {}
    void copyMatrixRHS() override;
    void addScaleMatrix(double coef) override;
  protected:
    felInt m_iTemperature;
    CurrentFiniteElement* m_feTemp;
  private:
    ElementField m_elemField;
    PetscMatrix m_matrix;
    bool m_buildMatrix;
  };
}

#endif
