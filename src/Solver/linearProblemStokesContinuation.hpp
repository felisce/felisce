//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _LINEARPROBLEMSTOKESCONTINUATION_HPP
#define _LINEARPROBLEMSTOKESCONTINUATION_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"

/*!
 \file linearProblemStokesContinuation.hpp
 \date 17/03/2022
 \brief Continuation method for Stokes' equation
*/

namespace felisce {  
  class LinearProblemStokesContinuation:
    public LinearProblem {
  public:
    LinearProblemStokesContinuation();
    ~LinearProblemStokesContinuation(){};

    // usual methods
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override; // set the current finite element 
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override; // compute the elementary arrays for the block system
    
    // methods regarding the cip stabilization
    void userChangePattern(int numProc, int rankProc) override;
    void assembleCIPStabilization();
    void addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec);
    void updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fe, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup);
    
    // reading the data from the forward problem   
    void readData(IO& io,double iteration) override;
  protected:
    CurrentFiniteElement* m_feVel;
    CurrentFiniteElement* m_fePres;
    ElementField m_sourceTerm;
    ElementField m_dataTerm;
    Variable* m_velocity;
    Variable* m_pressure;
    felInt m_iVelocity;
    felInt m_iPressure;
    double m_viscosity;
    bool m_useSymmetricStress;
    PetscVector m_potData;
  };
}

#endif
