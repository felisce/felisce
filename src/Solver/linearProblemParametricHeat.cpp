//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & M. Fragu
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemParametricHeat.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemParametricHeat::LinearProblemParametricHeat():
    LinearProblem(),
    m_buildMatrix(false)
  {}

  LinearProblemParametricHeat::~LinearProblemParametricHeat() {
    if(m_buildMatrix)
      m_matrix.destroy();

  }

  void LinearProblemParametricHeat::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;

    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = temperature;
    listNumComp[0] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(temperature);
    definePhysicalVariable(listVariable,listNumComp);
  }

  void LinearProblemParametricHeat::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_iTemperature = m_listVariable.getVariableIdList(temperature);
    m_feTemp = m_listCurrentFiniteElement[m_iTemperature];

    m_elemField.initialize(DOF_FIELD,*m_feTemp);
  }

  void LinearProblemParametricHeat::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_elementMat[0]->zero();
    assert(!m_elementVector.empty());
    m_elementVector[0]->zero();

    m_feTemp->updateFirstDeriv(0, elemPoint);

    double coef = 1./m_fstransient->timeStep;
    if (m_fstransient->iteration == 0) {
      m_elementMat[0]->grad_phi_i_grad_phi_j(1.,*m_feTemp,0,0,1);
    } else {
      m_elementMat[0]->phi_i_phi_j(coef,*m_feTemp,0,0,1);
      m_elemField.setValue(this->sequentialSolution(), *m_feTemp, iel, m_iTemperature, m_ao, dof());
      m_elementVector[0]->source(coef,*m_feTemp,m_elemField,0,1);
    }
  }

  void LinearProblemParametricHeat::copyMatrixRHS() {
    m_matrix.duplicateFrom(matrix(0),MAT_COPY_VALUES);
    m_matrix.assembly(MAT_FINAL_ASSEMBLY);
    m_buildMatrix = true;
  }

  void LinearProblemParametricHeat::addScaleMatrix(double coef) {
    matrix(0).scale(coef);
    matrix(0).axpy(1,m_matrix,SAME_NONZERO_PATTERN);
  }
}
