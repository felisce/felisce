//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Boulakia
//

// System includes

// External includes

// Project includes
#include "Solver/MVSolver.hpp"

namespace felisce {
  MVSolver::MVSolver(FelisceTransient::Pointer fstransient):
    IonicSolver(fstransient) {
    m_nComp = 3;
    m_assimilation = false;
    m_cellTypeCut = 4; // E.Tixier: Test MV model for atria. This is ugly bu necessary (for now) to be able to pass the Buildbot tests.

    if (FelisceParam::instance().CellsType != "hetero") {
      m_uo.resize(1);
      m_uu.resize(1);
      m_thetav.resize(1);
      m_thetaw.resize(1);
      m_thetavm.resize(1);
      m_thetao.resize(1);
      m_tauvm1.resize(1);
      m_tauvm2.resize(1);
      m_tauvp.resize(1);
      m_tauwm1.resize(1);
      m_tauwm2.resize(1);
      m_kwm.resize(1);
      m_uwm.resize(1);
      m_tauwm.resize(1);
      m_taufi.resize(1);
      m_tauo1.resize(1);
      m_tauo2.resize(1);
      m_tauso1.resize(1);
      m_tauso2.resize(1);
      m_kso.resize(1);
      m_uso.resize(1);
      m_taus1.resize(1);
      m_taus2.resize(1);
      m_ks.resize(1);
      m_us.resize(1);
      m_tausi.resize(1);
      m_tauwinf.resize(1);
      m_winfstar.resize(1);
      m_gfi.resize(1);
      m_gso.resize(1);
      m_gsi.resize(1);
      m_gfi[0] = 1.;m_gso[0] = 1.;m_gsi[0] = 1.;
      if (FelisceParam::instance().CellsType == "endo") {
        m_uo[0] = 0.;         // u_o
        m_uu[0] = 1.56;       // u_u
        m_thetav[0] = 0.3;    // theta_v
        m_thetaw[0] = 0.13;   // theta_w
        m_thetavm[0] = 0.2;   // theta_v^-
        m_thetao[0] = 0.006;  // theta_o
        m_tauvm1[0] = 75.;    // tau_v1^-
        m_tauvm2[0] = 10.;    // tau_v2^-
        m_tauvp[0] = 1.4506;  // tau_v^+
        m_tauwm1[0] = 6.;		  // tau_w1^-
        m_tauwm2[0] = 140.;	  // tau_w2^+
        m_kwm[0] = 200.;		  // k_w^-
        m_uwm[0] = 0.016;     // u_w^-
        m_tauwm[0] = 280.;    // tau_w^+
        m_taufi[0] = 0.1;     // tau_fi
        m_tauo1[0] = 470.;    // tau_o1
        m_tauo2[0] = 6.;      // tau_o2
        m_tauso1[0] = 40.;    // tau_so1
        m_tauso2[0] = 1.2;    // tau_so2
        m_kso[0] = 2.;        // k_so
        m_uso[0] = 0.65;      // u_so
        m_taus1[0] = 2.7342;  // tau_s1
        m_taus2[0] = 2.;      // tau_s2
        m_ks[0] = 2.0994;     // k_s
        m_us[0] = 0.9087;     // u_s
        m_tausi[0] = 2.9013;  // tau_si
        m_tauwinf[0] = 0.0273;// tau_{w inf}
        m_winfstar[0] = 0.78; // w^*_{inf}
      }
      else if (FelisceParam::instance().CellsType == "M") {
        m_uo[0] = 0.;         // u_o
        m_uu[0] = 1.61;       // u_u
        m_thetav[0] = 0.3;    // theta_v
        m_thetaw[0] = 0.13;   // theta_w
        m_thetavm[0] = 0.1;   // theta_v^-
        m_thetao[0] = 0.005;  // theta_o
        m_tauvm1[0] = 80.;    // tau_v1^-
        m_tauvm2[0] = 1.4506; // tau_v2^-
        m_tauvp[0] = 1.4506;  // tau_v^+
        m_tauwm1[0] = 70.;		// tau_w1^-
        m_tauwm2[0] = 8.;	 	  // tau_w2^+
        m_kwm[0] = 200.;			// k_w^-
        m_uwm[0] = 0.016;     // u_w^-
        m_tauwm[0] = 280.;    // tau_w^+
        m_taufi[0] = 0.078;   // tau_fi
        m_tauo1[0] = 410.;    // tau_o1
        m_tauo2[0] = 7.;      // tau_o2
        m_tauso1[0] = 91.;    // tau_so1
        m_tauso2[0] = 0.8;    // tau_so2
        m_kso[0] = 2.1;       // k_so
        m_uso[0] = 0.6;       // u_so
        m_taus1[0] = 2.7342;  // tau_s1
        m_taus2[0] = 4.;      // tau_s2
        m_ks[0] = 2.0994;     // k_s
        m_us[0] = 0.9087;     // u_s
        m_tausi[0] = 3.3849;  // tau_si
        m_tauwinf[0] = 0.01;  // tau_{w inf}
        m_winfstar[0] = 0.5;	// w^*_{inf}
      }
      else if (FelisceParam::instance().CellsType == "epi") {
        m_uo[0] = 0.;         // u_o
        m_uu[0] = 1.55;       // u_u
        m_thetav[0] = 0.3;    // theta_v
        m_thetaw[0] = 0.13;   // theta_w
        m_thetavm[0] = 0.006; // theta_v^-
        m_thetao[0] = 0.006;  // theta_o
        m_tauvm1[0] = 60.;    // tau_v1^-
        m_tauvm2[0] = 1150.;  // tau_v2^-
        m_tauvp[0] = 1.4506;  // tau_v^+
        m_tauwm1[0] = 60.;		// tau_w1^-
        m_tauwm2[0] = 15.;	 	// tau_w2^+
        m_kwm[0] = 65.;			  // k_w^-
        m_uwm[0] = 0.03;      // u_w^-
        m_tauwm[0] = 200.;    // tau_w^+
        m_taufi[0] = 0.11;    // tau_fi
        m_tauo1[0] = 400.;    // tau_o1
        m_tauo2[0] = 6.;      // tau_o2
        m_tauso1[0] = 30.0181;// tau_so1
        m_tauso2[0] = 0.9957; // tau_so2
        m_kso[0] = 2.0458;    // k_so
        m_uso[0] = 0.65;      // u_so
        m_taus1[0] = 2.7342;  // tau_s1
        m_taus2[0] = 16.;     // tau_s2
        m_ks[0] = 2.0994;     // k_s
        m_us[0] = 0.9087;     // u_s
        m_tausi[0] = 1.8875;  // tau_si
        m_tauwinf[0] = 0.07;  // tau_{w inf}
        m_winfstar[0] = 0.94; // w^*_{inf}
      }
      else if (FelisceParam::instance().CellsType == "PB") {
        m_uo[0] = 0.;         // u_o
        m_uu[0] = 1.45;       // u_u
        m_thetav[0] = 0.35;   // theta_v
        m_thetaw[0] = 0.13;   // theta_w
        m_thetavm[0] = 0.175; // theta_v^-
        m_thetao[0] = 0.006;  // theta_o
        m_tauvm1[0] = 10.;    // tau_v1^-
        m_tauvm2[0] = 1150.;  // tau_v2^-
        m_tauvp[0] = 1.4506;  // tau_v^+
        m_tauwm1[0] = 140.;	  // tau_w1^-
        m_tauwm2[0] = 6.25;	  // tau_w2^+
        m_kwm[0] = 65.;			  // k_w^-
        m_uwm[0] = 0.015;     // u_w^-
        m_tauwm[0] = 326.;    // tau_w^+
        m_taufi[0] = 0.105;   // tau_fi
        m_tauo1[0] = 400.;    // tau_o1
        m_tauo2[0] = 6.;      // tau_o2
        m_tauso1[0] = 30.0181;// tau_so1
        m_tauso2[0] = 0.9957; // tau_so2
        m_kso[0] = 2.0458;    // k_so
        m_uso[0] = 0.65;      // u_so
        m_taus1[0] = 2.7342;  // tau_s1
        m_taus2[0] = 16.;     // tau_s2
        m_ks[0] = 2.0994;     // k_s
        m_us[0] = 0.9087;     // u_s
        m_tausi[0] = 1.8875;  // tau_si
        m_tauwinf[0] = 0.175; // tau_{w inf}
        m_winfstar[0] = 0.9;	// w^*_{inf}
      }
      else if (FelisceParam::instance().CellsType == "TNNP") {
        m_uo[0] = 0.;         // u_o
        m_uu[0] = 1.58;       // u_u
        m_thetaw[0] = 0.015;  // theta_w
        m_thetavm[0] = 0.015; // theta_v^-
        m_thetao[0] = 0.006;  // theta_o
        m_tauvm1[0] = 60.;    // tau_v1^-
        m_tauvm2[0] = 1150.;  // tau_v2^-
        m_tauvp[0] = 1.4506;  // tau_v^+
        m_tauwm1[0] = 70.;		// tau_w1^-
        m_tauwm2[0] = 20.;	 	// tau_w2^+
        m_kwm[0] = 65.;			  // k_w^-
        m_uwm[0] = 0.03;      // u_w^-
        m_tauwm[0] = 280.;    // tau_w^+
        m_taufi[0] = 0.11;    // tau_fi
        m_tauo1[0] = 6.;      // tau_o1
        m_tauo2[0] = 6.;      // tau_o2
        m_tauso1[0] = 43.;    // tau_so1
        m_tauso2[0] = 0.2;    // tau_so2
        m_kso[0] = 2.;        // k_so
        m_uso[0] = 0.65;      // u_so
        m_taus1[0] = 2.7342;  // tau_s1
        m_taus2[0] = 3.;      // tau_s2
        m_ks[0] = 2.0994;     // k_s
        m_us[0] = 0.9087;     // u_s
        m_tausi[0] = 2.8723;  // tau_si
        m_tauwinf[0] = 0.07;  // tau_{w inf}
        m_winfstar[0] = 0.94; // w^*_{inf}
      }

    }
    else if (FelisceParam::instance().CellsType == "hetero") {
      m_uo.resize(m_cellTypeCut);
      m_uu.resize(m_cellTypeCut);
      m_thetav.resize(m_cellTypeCut);
      m_thetaw.resize(m_cellTypeCut);
      m_thetavm.resize(m_cellTypeCut);
      m_thetao.resize(m_cellTypeCut);
      m_tauvm1.resize(m_cellTypeCut);
      m_tauvm2.resize(m_cellTypeCut);
      m_tauvp.resize(m_cellTypeCut);
      m_tauwm1.resize(m_cellTypeCut);
      m_tauwm2.resize(m_cellTypeCut);
      m_kwm.resize(m_cellTypeCut);
      m_uwm.resize(m_cellTypeCut);
      m_tauwm.resize(m_cellTypeCut);
      m_taufi.resize(m_cellTypeCut);
      m_tauo1.resize(m_cellTypeCut);
      m_tauo2.resize(m_cellTypeCut);
      m_tauso1.resize(m_cellTypeCut);
      m_tauso2.resize(m_cellTypeCut);
      m_kso.resize(m_cellTypeCut);
      m_uso.resize(m_cellTypeCut);
      m_taus1.resize(m_cellTypeCut);
      m_taus2.resize(m_cellTypeCut);
      m_ks.resize(m_cellTypeCut);
      m_us.resize(m_cellTypeCut);
      m_tausi.resize(m_cellTypeCut);
      m_tauwinf.resize(m_cellTypeCut);
      m_winfstar.resize(m_cellTypeCut);
      m_gfi.resize(m_cellTypeCut);
      m_gso.resize(m_cellTypeCut);
      m_gsi.resize(m_cellTypeCut);
      // Right Ventricle (RV)

      m_uo[0] = 0.;         // u_o
      m_uu[0] = 1.55;       // u_u
      m_thetav[0] = 0.3;    // theta_v
      m_thetaw[0] = 0.13;   // theta_w
      m_thetavm[0] = 0.006; // theta_v^-
      m_thetao[0] = 0.006;  // theta_o
      m_tauvm1[0] = 60.;    // tau_v1^-
      m_tauvm2[0] = 1150.;  // tau_v2^-
      m_tauvp[0] = 1.4506;  // tau_v^+
      m_tauwm1[0] = 60.;    // tau_w1^-
      m_tauwm2[0] = 15.;    // tau_w2^+
      m_kwm[0] = 65.;	    // k_w^-
      m_uwm[0] = 0.03;      // u_w^-
      m_tauwm[0] = 200.;    // tau_w^+
      m_taufi[0] = 0.11;    // tau_fi
      m_tauo1[0] = 400.;    // tau_o1
      m_tauo2[0] = 6.;      // tau_o2
      if (FelisceParam::instance().tau_so_1_rv < 0.) {
        m_tauso1[0] = 20.; //26.;    // tau_so1 (30.0181)
      }
      else {
        m_tauso1[0] = FelisceParam::instance().tau_so_1_rv;
      }
      m_tauso2[0] = 0.9957; // tau_so2
      m_kso[0] = 2.0458;    // k_so
      m_uso[0] = 0.65;      // u_so
      m_taus1[0] = 2.7342;  // tau_s1
      m_taus2[0] = 16.;     // tau_s2
      m_ks[0] = 2.0994;     // k_s
      m_us[0] = 0.9087;     // u_s
      m_tausi[0] = 1.8875;  // tau_si
      m_tauwinf[0] = 0.07;  // tau_{w inf}
      m_winfstar[0] = 0.94; // w^*_{inf}
      m_gfi[0] = FelisceParam::instance().gfi_rv;
      m_gso[0] = FelisceParam::instance().gso_rv;
      m_gsi[0] = FelisceParam::instance().gsi_rv;
      // Endo

      m_uo[1] = 0.;         // u_o
      m_uu[1] = 1.56;       // u_u
      m_thetav[1] = 0.3;    // theta_v
      m_thetaw[1] = 0.13;   // theta_w
      m_thetavm[1] = 0.2;   // theta_v^-
      m_thetao[1] = 0.006;  // theta_o
      m_tauvm1[1] = 75.;    // tau_v1^-
      m_tauvm2[1] = 10.;    // tau_v2^-
      m_tauvp[1] = 1.4506;  // tau_v^+
      m_tauwm1[1] = 6.;	    // tau_w1^-
      m_tauwm2[1] = 140.;   // tau_w2^+
      m_kwm[1] = 200.;	    // k_w^-
      m_uwm[1] = 0.016;     // u_w^-
      m_tauwm[1] = 280.;    // tau_w^+
      m_taufi[1] = 0.1;     // tau_fi
      m_tauo1[1] = 470.;    // tau_o1
      m_tauo2[1] = 6.;      // tau_o2
      if (FelisceParam::instance().tau_so_1_endo < 0.) {
        m_tauso1[1] = 30.; //40.;    // tau_so1 (40.)
      }
      else {
        m_tauso1[1] = FelisceParam::instance().tau_so_1_endo;
      }
      m_tauso2[1] = 1.2;    // tau_so2
      m_kso[1] = 2.;        // k_so
      m_uso[1] = 0.65;      // u_so
      m_taus1[1] = 2.7342;  // tau_s1
      m_taus2[1] = 2.;      // tau_s2
      m_ks[1] = 2.0994;     // k_s
      m_us[1] = 0.9087;     // u_s
      m_tausi[1] = 2.9013;  // tau_si
      m_tauwinf[1] = 0.0273;// tau_{w inf}
      m_winfstar[1] = 0.78; // w^*_{inf}
      m_gfi[1] = FelisceParam::instance().gfi_endo;
      m_gso[1] = FelisceParam::instance().gso_endo;
      m_gsi[1] = FelisceParam::instance().gsi_endo;
      
      // M-Cells

      m_uo[2] = 0.;         // u_o
      m_uu[2] = 1.61;       // u_u
      m_thetav[2] = 0.3;    // theta_v
      m_thetaw[2] = 0.13;   // theta_w
      m_thetavm[2] = 0.1;   // theta_v^-
      m_thetao[2] = 0.005;  // theta_o
      m_tauvm1[2] = 80.;    // tau_v1^-
      m_tauvm2[2] = 1.4506; // tau_v2^-
      m_tauvp[2] = 1.4506;  // tau_v^+
      m_tauwm1[2] = 70.;    // tau_w1^-
      m_tauwm2[2] = 8.;	    // tau_w2^+
      m_kwm[2] = 200.;	    // k_w^-
      m_uwm[2] = 0.016;     // u_w^-
      m_tauwm[2] = 280.;    // tau_w^+
      m_taufi[2] = 0.078;   // tau_fi
      m_tauo1[2] = 410.;    // tau_o1
      m_tauo2[2] = 7.;      // tau_o2
      if (FelisceParam::instance().tau_so_1_mcel < 0.) {
        m_tauso1[2] = 45.; //61.;    // tau_so1 (91.)
      }
      else {
        m_tauso1[2] = FelisceParam::instance().tau_so_1_mcel;
      }
      m_tauso2[2] = 0.8;    // tau_so2
      m_kso[2] = 2.1;       // km_so
      m_uso[2] = 0.6;       // u_so
      m_taus1[2] = 2.7342;  // tau_s1
      m_taus2[2] = 4.;      // tau_s2
      m_ks[2] = 2.0994;     // k_s
      m_us[2] = 0.9087;     // u_s
      m_tausi[2] = 3.3849;  // tau_si
      m_tauwinf[2] = 0.01;  // tau_{w inf}
      m_winfstar[2] = 0.5;  // w^*_{inf}
      m_gfi[2] = FelisceParam::instance().gfi_mid;
      m_gso[2] = FelisceParam::instance().gso_mid;
      m_gsi[2] = FelisceParam::instance().gsi_mid;
      // Epi

      m_uo[3] = 0.;         // u_o
      m_uu[3] = 1.55;       // u_u
      m_thetav[3] = 0.3;    // theta_v
      m_thetaw[3] = 0.13;   // theta_w
      m_thetavm[3] = 0.006; // theta_v^-
      m_thetao[3] = 0.006;  // theta_o
      m_tauvm1[3] = 60.;    // tau_v1^-
      m_tauvm2[3] = 1150.;  // tau_v2^-
      m_tauvp[3] = 1.4506;  // tau_v^+
      m_tauwm1[3] = 60.;    // tau_w1^-
      m_tauwm2[3] = 15.;    // tau_w2^+
      m_kwm[3] = 65.;	    // k_w^-
      m_uwm[3] = 0.03;      // u_w^-
      m_tauwm[3] = 200.;    // tau_w^+
      m_taufi[3] = 0.11;    // tau_fi
      m_tauo1[3] = 400.;    // tau_o1
      m_tauo2[3] = 6.;      // tau_o2
      if (FelisceParam::instance().tau_so_1_epi < 0.) {
        m_tauso1[3] = 19.; //25.;    // tau_so1 (30.0181)
      }
      else {
        m_tauso1[3] = FelisceParam::instance().tau_so_1_epi;
      }
      m_tauso2[3] = 0.9957; // tau_so2
      m_kso[3] = 2.0458;    // k_so
      m_uso[3] = 0.65;      // u_so
      m_taus1[3] = 2.7342;  // tau_s1
      m_taus2[3] = 16.;     // tau_s2
      m_ks[3] = 2.0994;     // k_s
      m_us[3] = 0.9087;     // u_s
      m_tausi[3] = 1.8875;  // tau_si
      m_tauwinf[3] = 0.07;  // tau_{w inf}
      m_winfstar[3] = 0.94; // w^*_{inf}
      m_gfi[3] = FelisceParam::instance().gfi_epi;
      m_gso[3] = FelisceParam::instance().gso_epi;
      m_gsi[3] = FelisceParam::instance().gsi_epi;
      
      // Atria (Atria-ventricular MV model)
      if (m_cellTypeCut > 4){
        m_uo[4] = 0.;         // u_o
        m_uu[4] = 1.55;       // u_u
        m_thetav[4] = 0.3;    // theta_v
        m_thetaw[4] = 0.13;   // theta_w
        m_thetavm[4] = 0.006; // theta_v^-
        m_thetao[4] = 0.006;  // theta_o
        m_tauvm1[4] = 60.;    // tau_v1^-
        m_tauvm2[4] = 1150.;  // tau_v2^-
        m_tauvp[4] = 1.4506;  // tau_v^+
        m_tauwm1[4] = 60.;    // tau_w1^-
        m_tauwm2[4] = 15.;    // tau_w2^+
        m_kwm[4] = 65.;	    // k_w^-
        m_uwm[4] = 0.03;      // u_w^-
        m_tauwm[4] = 200.;    // tau_w^+
        m_taufi[4] = 0.11;    // tau_fi
        m_tauo1[4] = 400.;    // tau_o1
        m_tauo2[4] = 6.;      // tau_o2
        m_tauso1[4] = 30.;    // tau_so1
        m_tauso2[4] = 0.9957; // tau_so2
        m_kso[4] = 2.0458;    // k_so
        m_uso[4] = 0.65;      // u_so
        m_taus1[4] = 2.7342;  // tau_s1
        m_taus2[4] = 16.;     // tau_s2
        m_ks[4] = 2.0994;     // k_s
        m_us[4] = 0.9087;     // u_s
        m_tausi[4] = 1.8875;  // tau_si
        m_tauwinf[4] = 0.07;  // tau_{w inf}
        m_winfstar[4] = 0.94; // w^*_{inf}
        m_gfi[4] = 1.;
        m_gso[4] = 1.;
        m_gsi[4] = 1.;
      }
    }
    else {
      FEL_ERROR("Cells type not defined.");
    }

  }

  MVSolver::~MVSolver()
  = default;

  void MVSolver::computeRHS() {

    double& dt = m_fstransient->timeStep;

    m_bdf.computeRHSTime(dt, m_vecRHS);

    double value_uExtrap;
    double value_RHSv;
    double value_RHSw;
    double value_RHSs;
    double hv;
    double hw;
    double hm;
    double ho;
    double vinf;
    double tauvm;
    double winf;
    double tauwm;
    double taus;

    double Vmin = FelisceParam::instance().vMin;
    double Vmax = FelisceParam::instance().vMax;

    int iType = 0;

    felInt pos;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)

      //________________________________________________________________________________________________________________________________________________________________

      if(m_assimilation ){

        //For assimilation
        if(m_fstransient->iteration == 1) {
          m_vEDO.resize(m_size);
          m_wEDO.resize(m_size);
          m_sEDO.resize(m_size);
          
          p_vEDO.resize(m_size);
          p_wEDO.resize(m_size);
          p_sEDO.resize(m_size);
        }
        
        if(m_fstransient->iteration == 1) {
          m_Vm.resize(m_size);
          p_Vm.resize(m_size);
          m_Vm[pos] = (m_Vm[pos] - Vmin)/(Vmax-Vmin);
          p_Vm[pos] = (p_Vm[pos] - Vmin)/(Vmax-Vmin);
        }
        
        if(m_fstransient->iteration > 1) {
          if(m_assimilation) {
            value_uExtrap = m_Vm[pos];
          }
        }
        
      }
      //________________________________________________________________________________________________________________________________________________________________

      value_uExtrap = (value_uExtrap - Vmin)/(Vmax-Vmin); // \in [0,1]

      if (FelisceParam::instance().CellsType == "hetero")
        iType = m_cellType[pos];

      if (iType < m_cellTypeCut) {

        // hm = H(u-theta_v^-)
        if (value_uExtrap > m_thetavm[iType])
          hm=1.;
        else
          hm=0.;

        // v_inf = 1 - H(u-theta_v^-)
        vinf = 1-hm;

        // hv = H(u-theta_v)
        if (value_uExtrap > m_thetav[iType])
          hv=1.;
        else
          hv=0.;

        // hw = H(u-theta_w)
        if (value_uExtrap > m_thetaw[iType])
          hw=1.;
        else
          hw=0.;

        // ho = H(u-theta_o)
        if(value_uExtrap > m_thetao[iType])
          ho = 1.;
        else
          ho = 0.;

        // w_inf = (1 - H(u-theta_o)) * (1 / u/tau_w_inf) + H(u-theat_o)*w_inf^*
        winf = (1-ho)*(1-value_uExtrap/m_tauwinf[iType]) + ho*m_winfstar[iType];

        // tau_v^- = (1 - H(u-theta_v^+))*tau_v1^- + H(u-theta_v^-)*tau_v2^-
        tauvm = (1-hm)*m_tauvm1[iType] + hm*m_tauvm2[iType];
        // dv/dt = (1 - H(u-theta_v))*(v_inf - v)/tau_v^- - H(u-theta_v)*v/tau_v^+
        value_RHSv = (1-hv)*vinf/tauvm;
        m_vecRHS[0].setValue(pos,value_RHSv, ADD_VALUES);

        // tau_w^- = tau_w1^- + (tau_w2^- - tau_W1^-) * (1 + tanh(k_w^- * (u-u-u_w^-)) )/2
        tauwm = m_tauwm1[iType] + (m_tauwm2[iType]-m_tauwm1[iType]) * (1 + tanh(m_kwm[iType]*(value_uExtrap-m_uwm[iType])) )/2;
        // dw/dt = (1 - H(u-theta_w))*(w_inf - w)/tau_w^- - H(u-theta_w)*w/tau_w^+
        value_RHSw = (1-hw)*winf/tauwm;
        m_vecRHS[1].setValue(pos,value_RHSw, ADD_VALUES);

        // tau_s = (1 - H(u-theta_w))*tau_s1 + H(u-theta_w)*tau_s2
        taus = (1-hw)*m_taus1[iType] + hw*m_taus2[iType];
        // ds/dt = ((1 + tanh(k_s * (u-u_s)) )/2 - s)/tau_s
        value_RHSs = (1 + tanh(m_ks[iType]*(value_uExtrap-m_us[iType])))/(2*taus);
        m_vecRHS[2].setValue(pos,value_RHSs, ADD_VALUES);
      }
      else if (FelisceParam::instance().typeOfIonicModel ==  "courtAtriaMvVent" ){
        felInt meshId = pos;
        AOPetscToApplication(m_ao,1,&meshId);
        felInt sizeVent = 45580;
        if (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart")
          sizeVent = 32320;
        if (meshId > (sizeVent-1) ) {
          m_vecRHS[0].setValue(pos,0., ADD_VALUES);
          m_vecRHS[1].setValue(pos,0., ADD_VALUES);
          m_vecRHS[2].setValue(pos,0., ADD_VALUES);
        }
        else {
          std::ostringstream msg;
          msg << "Problem in MVsolver atria/ventricles nodes : node " << pos << " not defined, " << iType << "." << std::endl;
          FEL_ERROR(msg.str());          
        }
      }
      else {
        m_vecRHS[0].setValue(pos,0., ADD_VALUES);
        m_vecRHS[1].setValue(pos,0., ADD_VALUES);
        m_vecRHS[2].setValue(pos,0., ADD_VALUES);
      }
    }

    m_vecRHS[0].assembly();
    m_vecRHS[1].assembly();
    m_vecRHS[2].assembly();
  }

  void MVSolver::solveEDO() {
    double& coeffDeriv = m_bdf.coeffDeriv0();
    double& dt = m_fstransient->timeStep;
    int& numIt = m_fstransient->iteration;
    double value_uExtrap;

    double value_RHSv;
    double value_RHSw;
    double value_RHSs;
    double valuem_solEDOv;
    double valuem_solEDOw;
    double valuem_solEDOs;

    double beta1;
    double beta2;
    double beta3;

    double tauvm;
    double tauwm;
    double taus;
    double hm;
    double hv;
    double hw;

    double Vmin = FelisceParam::instance().vMin;
    double Vmax = FelisceParam::instance().vMax;

    int iType = 0;

    felInt pos;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);

      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      m_vecRHS[0].getValues(1,&pos,&value_RHSv);//value_RHS = m_RHS(i)
      m_vecRHS[1].getValues(1,&pos,&value_RHSw);//value_RHS = m_RHS(i)
      m_vecRHS[2].getValues(1,&pos,&value_RHSs);//value_RHS = m_RHS(i)

      //________________________________________________________________________________________________________________________________________________________________
      
      if(m_assimilation) {
        if(numIt > 1) {
          value_uExtrap = m_Vm[pos];
        }
      }

      //________________________________________________________________________________________________________________________________________________________________

      value_uExtrap = (value_uExtrap - Vmin)/(Vmax-Vmin); // \in [0,1]

      if (FelisceParam::instance().CellsType == "hetero")
        iType = m_cellType[pos];

      if (iType < m_cellTypeCut) {

        // hm = H(u-theta_v^-)
        if (value_uExtrap > m_thetavm[iType])
          hm=1.;
        else
          hm=0.;

        // hv = H(u-theta_v)
        if (value_uExtrap > m_thetav[iType])
          hv=1.;
        else
          hv=0.;

        // hw = H(u-theta_w)
        if (value_uExtrap > m_thetaw[iType])
          hw=1.;
        else
          hw=0.;

        // tau_v^- = (1 - H(u-theta_v^-))*tau_v1^- + H(u-theta_v^-)*tau_v2^-
        tauvm = (1-hm)*m_tauvm1[iType] + hm*m_tauvm2[iType];
        beta1 = coeffDeriv/dt + (1-hv)/tauvm + hv/m_tauvp[iType];

        // tau_w^- = tau_w1^- + (tau_w2^- - tau_W1^-) * (1 + tanh(k_w^- * (u-u-u_w^-)) )/2
        tauwm = m_tauwm1[iType] + (m_tauwm2[iType]-m_tauwm1[iType]) * (1 + tanh(m_kwm[iType]*(value_uExtrap-m_uwm[iType])) )/2;
        beta2=coeffDeriv/dt + (1-hw)/tauwm + hw/m_tauwm[iType];

        // tau_s = (1 - H(u-theta_w))*tau_s1 + H(u-theta_w)*tau_s2
        taus = (1-hw)*m_taus1[iType] + hw*m_taus2[iType];
        beta3 = coeffDeriv/dt + 1./taus;

        // v^n+1 = RHSv /beta1
        valuem_solEDOv = value_RHSv * 1./beta1;
        // w^n+1 = RHSw / beta1
        valuem_solEDOw = value_RHSw * 1./beta2;
        // s^n+1 = RHSs / beta3
        valuem_solEDOs = value_RHSs * 1./beta3;

        if(m_assimilation)
        {

          if(numIt == 1) {
            valuem_solEDOv = 1.;
            valuem_solEDOw = 1.;
            valuem_solEDOs = 0.;
            
            m_vEDO[pos] = valuem_solEDOv;
            m_wEDO[pos] = valuem_solEDOw;
            m_sEDO[pos] = valuem_solEDOs;
            
          }
          
          //________________________________________________________________________________________________________________________________________________________________
          

          p_vEDO[pos] = valuem_solEDOv;
          p_wEDO[pos] = valuem_solEDOw;
          p_sEDO[pos] = valuem_solEDOs;

          valuem_solEDOv = m_vEDO[pos];
          valuem_solEDOw = m_wEDO[pos];
          valuem_solEDOs = m_sEDO[pos];
        }

        //________________________________________________________________________________________________________________________________________________________________

        m_vecSolEDO[0].setValue(pos,valuem_solEDOv, INSERT_VALUES);
        m_vecSolEDO[1].setValue(pos,valuem_solEDOw, INSERT_VALUES);
        m_vecSolEDO[2].setValue(pos,valuem_solEDOs, INSERT_VALUES);
      }
      else if (FelisceParam::instance().typeOfIonicModel ==  "courtAtriaMvVent" ){
        felInt meshId = pos;
        AOPetscToApplication(m_ao,1,&meshId);
        felInt sizeVent = 45580;
        if (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart")
          sizeVent = 32320;
        if (meshId > (sizeVent-1) ) {
          m_vecSolEDO[0].setValue(pos,0., INSERT_VALUES);
          m_vecSolEDO[1].setValue(pos,0., INSERT_VALUES);
          m_vecSolEDO[2].setValue(pos,0., INSERT_VALUES);
        }
        else {
          std::ostringstream msg;
          msg << "Problem in MVsolver atria/ventricles nodes : node " << pos << " not defined, " << iType << "." << std::endl;
          FEL_ERROR(msg.str());
        }
      }
      else {
        m_vecSolEDO[0].setValue(pos,0., INSERT_VALUES);
        m_vecSolEDO[1].setValue(pos,0., INSERT_VALUES);
        m_vecSolEDO[2].setValue(pos,0., INSERT_VALUES);
      }

    }

    m_vecSolEDO[0].assembly();
    m_vecSolEDO[1].assembly();
    m_vecSolEDO[2].assembly();
  }

  void MVSolver::computeIon() {
    double value_uExtrap;
    double valuem_solEDOv;
    double valuem_solEDOw;
    double valuem_solEDOs;
    double value_ion;

    double Jfi;
    double Jso;
    double Jsi;
    double hv;
    double hw;
    double ho;
    double tauo;
    double tauso;

    double Vmin = FelisceParam::instance().vMin;
    double Vmax = FelisceParam::instance().vMax;
    double correctionCoef;
    int iType = 0;

    felInt pos;
    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);

      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      m_vecSolEDO[0].getValues(1,&pos,&valuem_solEDOv);//valuem_solEDOv = m_vecSolEDO[0](i)
      m_vecSolEDO[1].getValues(1,&pos,&valuem_solEDOw);//valuem_solEDOw = m_vecSolEDO[1](i)
      m_vecSolEDO[2].getValues(1,&pos,&valuem_solEDOs);//valuem_solEDOs = m_vecSolEDO[2](i)


      //________________________________________________________________________________________________________________________________________________________________

      if(m_assimilation) {
        if(m_fstransient->iteration > 1) {
          value_uExtrap = m_Vm[pos];
        }
      }

      //________________________________________________________________________________________________________________________________________________________________


      value_uExtrap = (value_uExtrap - Vmin)/(Vmax-Vmin); // \in [0,1]

      if (FelisceParam::instance().CellsType == "hetero")
        iType = m_cellType[pos];

      if (iType < m_cellTypeCut) {

        if (value_uExtrap > m_thetav[iType])
          hv=1.;
        else
          hv=0.;

        if (value_uExtrap > m_thetaw[iType])
          hw=1.;
        else
          hw=0.;

        if (value_uExtrap > m_thetao[iType])
          ho=1.;
        else
          ho=0.;

        // tau_o = (1 - H(u-thea_o))
        tauo = (1-ho)*m_tauo1[iType] + ho*m_tauo2[iType];
        // tau_so = tau_so1 + (tau_so2 - tau_so1)*(1 + tanh(k_so * (u-u_so)) )/2
        tauso = m_tauso1[iType] + (m_tauso2[iType] - m_tauso1[iType])*(1 + tanh(m_kso[iType]*(value_uExtrap-m_uso[iType])) )/2;

        // J_fi = -v*H(u-theta_v)*(u-theta_v)*(u_u-u)/tau_fi
        Jfi = - valuem_solEDOv * hv * (value_uExtrap-m_thetav[iType]) * (m_uu[iType]-value_uExtrap) / m_taufi[iType];
        Jfi *= m_gfi[iType];
        // J_so = (u-u_o)*(1 - H(u-theta_w))/tau_o + H(u-theta_w)/tau_so
        Jso = (value_uExtrap - m_uo[iType]) * (1 - hw)/tauo + hw/tauso;
        Jso *=  m_gso[iType];
        // J_si = -H(u-theta_w)*w*s/tau_si
        Jsi = - hw * valuem_solEDOw * valuem_solEDOs / m_tausi[iType];
        Jsi *= m_gsi[iType];
        correctionCoef = FelisceParam::instance().Cm * (FelisceParam::instance().vMax - FelisceParam::instance().vMin);
        // Infarct
        if (FelisceParam::instance().hasInfarct) {
          Jfi *= m_tauOut[pos];
        }
        value_ion = - ( Jfi + Jso + Jsi )*correctionCoef;
        m_ion.setValue(pos,value_ion, INSERT_VALUES);
      }
      else if (FelisceParam::instance().typeOfIonicModel ==  "courtAtriaMvVent" ){
        felInt meshId = pos;
        AOPetscToApplication(m_ao,1,&meshId);
        felInt sizeVent = 45580;
        if (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart")
          sizeVent = 32320;
        if (meshId > (sizeVent-1) ) {
          m_ion.setValue(pos,0., INSERT_VALUES);
        }
        else {
          std::ostringstream msg;
          msg << "Problem in MVsolver atria/ventricles nodes : node " << pos << " not defined, " << iType << "." << std::endl;
          FEL_ERROR(msg.str());
        }
      }
      else {
        m_ion.setValue(pos,0., INSERT_VALUES);
      }
    }
    m_ion.assembly();
  }


  /////////////////////////
  // Setters and Getters //
  /////////////////////////


  void MVSolver::setParameters(std::vector<double> parameters) {


    if(parameters.size() == 28) {

      m_uo.resize(1);
      m_uu.resize(1);
      m_thetav.resize(1);
      m_thetaw.resize(1);
      m_thetavm.resize(1);
      m_thetao.resize(1);
      m_tauvm1.resize(1);
      m_tauvm2.resize(1);
      m_tauvp.resize(1);
      m_tauwm1.resize(1);
      m_tauwm2.resize(1);
      m_kwm.resize(1);
      m_uwm.resize(1);
      m_tauwm.resize(1);
      m_taufi.resize(1);
      m_tauo1.resize(1);
      m_tauo2.resize(1);
      m_tauso1.resize(1);
      m_tauso2.resize(1);
      m_kso.resize(1);
      m_uso.resize(1);
      m_taus1.resize(1);
      m_taus2.resize(1);
      m_ks.resize(1);
      m_us.resize(1);
      m_tausi.resize(1);
      m_tauwinf.resize(1);
      m_winfstar.resize(1);



      m_uo[0] = parameters[0];
      m_uu[0] = parameters[1];
      m_thetav[0] = parameters[2];
      m_thetaw[0] = parameters[3];
      m_thetavm[0] = parameters[4];
      m_thetao[0] = parameters[5];
      m_tauvm1[0] = parameters[6];
      m_tauvm2[0] = parameters[7];
      m_tauvp[0] = parameters[8];
      m_tauwm1[0] = parameters[9];
      m_tauwm2[0] = parameters[10];
      m_kwm[0] = parameters[11];
      m_uwm[0] = parameters[12];
      m_tauwm[0] = parameters[13];
      m_taufi[0] = parameters[14];
      m_tauo1[0] = parameters[15];
      m_tauo2[0] = parameters[16];
      m_tauso1[0] = parameters[17];
      m_tauso2[0] = parameters[18];
      m_kso[0] = parameters[19];
      m_uso[0] = parameters[20];
      m_taus1[0] = parameters[21];
      m_taus2[0] = parameters[22];
      m_ks[0] = parameters[23];
      m_us[0] = parameters[24];
      m_tausi[0] = parameters[25];
      m_tauwinf[0] = parameters[26];
      m_winfstar[0] = parameters[27];

    } else {

      for(unsigned int i=0 ; i<m_posparam.size() ; i++) {

        const int posp = m_posparam[i];

        switch(posp) {

        case 0:
          m_uo.resize(1);
          m_uo[0] = parameters[i];
          break;

        case 1:
          m_uu.resize(1);
          m_uu[0] = parameters[i];
          break;

        case 2:
          m_thetav.resize(1);
          m_thetav[0] = parameters[i];
          break;

        case 3:
          m_thetaw.resize(1);
          m_thetaw[0] = parameters[i];
          break;

        case 4:
          m_thetavm.resize(1);
          m_thetavm[0] = parameters[i];
          break;

        case 5:
          m_thetao.resize(1);
          m_thetao[0] = parameters[i];
          break;

        case 6:
          m_tauvm1.resize(1);
          m_tauvm1[0] = parameters[i];
          break;

        case 7:
          m_tauvm2.resize(1);
          m_tauvm2[0] = parameters[i];
          break;

        case 8:
          m_tauvp.resize(1);
          m_tauvp[0] = parameters[i];
          break;

        case 9:
          m_tauwm1.resize(1);
          m_tauwm1[0] = parameters[i];
          break;

        case 10:
          m_tauwm2.resize(1);
          m_tauwm2[0] = parameters[i];
          break;

        case 11:
          m_kwm.resize(1);
          m_kwm[0] = parameters[i];
          break;

        case 12:
          m_uwm.resize(1);
          m_uwm[0] = parameters[i];
          break;

        case 13:
          m_tauwm.resize(1);
          m_tauwm[0] = parameters[i];
          break;

        case 14:
          m_taufi.resize(1);
          m_taufi[0] = parameters[i];
          break;

        case 15:
          m_tauo1.resize(1);
          m_tauo1[0] = parameters[i];
          break;

        case 16:
          m_tauo2.resize(1);
          m_tauo2[0] = parameters[i];
          break;

        case 17:
          m_tauso1.resize(1);
          m_tauso1[0] = parameters[i];
          break;

        case 18:
          m_tauso2.resize(1);
          m_tauso2[0] = parameters[i];
          break;

        case 19:
          m_kso.resize(1);
          m_kso[0] = parameters[i];
          break;

        case 20:
          m_uso.resize(1);
          m_uso[0] = parameters[i];
          break;

        case 21:
          m_taus1.resize(1);
          m_taus1[0] = parameters[i];
          break;

        case 22:
          m_taus2.resize(1);
          m_taus2[0] = parameters[i];
          break;

        case 23:
          m_ks.resize(1);
          m_ks[0] = parameters[i];
          break;

        case 24:
          m_us.resize(1);
          m_us[0] = parameters[i];
          break;

        case 25:
          m_tausi.resize(1);
          m_tausi[0] = parameters[i];
          break;

        case 26:
          m_tauwinf.resize(1);
          m_tauwinf[0] = parameters[i];
          break;

        case 27:
          m_winfstar.resize(1);
          m_winfstar[0] = parameters[i];
          break;

        default:
          std::cout << "Numerotation error in setParameters of MVSolver" << std::endl;
          break;
        }

      }//End for

    }//End else

  }



  void MVSolver::getParameters(std::vector<double>& p) {

    std::vector<double> param;

    if(m_posparam.size() == 28) {

      param.push_back(m_uo[0]);
      param.push_back(m_uu[0]);
      param.push_back(m_thetav[0]);
      param.push_back(m_thetaw[0]);
      param.push_back(m_thetavm[0]);
      param.push_back(m_thetao[0]);
      param.push_back(m_tauvm1[0]);
      param.push_back(m_tauvm2[0]);
      param.push_back(m_tauvp[0]);
      param.push_back(m_tauwm1[0]);
      param.push_back(m_tauwm2[0]);
      param.push_back(m_kwm[0]);
      param.push_back(m_uwm[0]);
      param.push_back(m_tauwm[0]);
      param.push_back(m_taufi[0]);
      param.push_back(m_tauo1[0]);
      param.push_back(m_tauo2[0]);
      param.push_back(m_tauso1[0]);
      param.push_back(m_tauso2[0]);
      param.push_back(m_kso[0]);
      param.push_back(m_uso[0]);
      param.push_back(m_taus1[0]);
      param.push_back(m_taus2[0]);
      param.push_back(m_ks[0]);
      param.push_back(m_us[0]);
      param.push_back(m_tausi[0]);
      param.push_back(m_tauwinf[0]);
      param.push_back(m_winfstar[0]);

    } else {

      for(unsigned int i=0 ; i<m_posparam.size() ; i++) {

        const int posp = m_posparam[i];

        switch(posp) {

        case 0:
          param.push_back(m_uo[0]);
          break;

        case 1:
          param.push_back(m_uu[0]);
          break;

        case 2:
          param.push_back(m_thetav[0]);
          break;

        case 3:
          param.push_back(m_thetaw[0]);
          break;

        case 4:
          param.push_back(m_thetavm[0]);
          break;

        case 5:
          param.push_back(m_thetao[0]);
          break;

        case 6:
          param.push_back(m_tauvm1[0]);
          break;

        case 7:
          param.push_back(m_tauvm2[0]);
          break;

        case 8:
          param.push_back(m_tauvp[0]);
          break;

        case 9:
          param.push_back(m_tauwm1[0]);
          break;

        case 10:
          param.push_back(m_tauwm2[0]);
          break;

        case 11:
          param.push_back(m_kwm[0]);
          break;

        case 12:
          param.push_back(m_uwm[0]);
          break;

        case 13:
          param.push_back(m_tauwm[0]);
          break;

        case 14:
          param.push_back(m_taufi[0]);
          break;

        case 15:
          param.push_back(m_tauo1[0]);
          break;

        case 16:
          param.push_back(m_tauo2[0]);
          break;

        case 17:
          param.push_back(m_tauso1[0]);
          break;

        case 18:
          param.push_back(m_tauso2[0]);
          break;

        case 19:
          param.push_back(m_kso[0]);
          break;

        case 20:
          param.push_back(m_uso[0]);
          break;

        case 21:
          param.push_back(m_taus1[0]);
          break;

        case 22:
          param.push_back(m_taus2[0]);
          break;

        case 23:
          param.push_back(m_ks[0]);
          break;

        case 24:
          param.push_back(m_us[0]);
          break;

        case 25:
          param.push_back(m_tausi[0]);
          break;

        case 26:
          param.push_back(m_tauwinf[0]);
          break;

        case 27:
          param.push_back(m_winfstar[0]);
          break;

        default:
          std::cout << "Numerotation error in setParameters of MVSolver" << std::endl;
          break;

        }

      }//End for

    }//End else

    p = param;

  }



  void MVSolver::getEDOvalues(std::vector<double>& v,std::vector<double>& w,std::vector<double>& s) {

    v.resize(m_vEDO.size());
    w.resize(m_wEDO.size());
    s.resize(m_sEDO.size());

    v = m_vEDO;
    w = m_wEDO;
    s = m_sEDO;

  }

  void MVSolver::getEDOpvalues(std::vector<double>& v,std::vector<double>& w,std::vector<double>& s) {

    v.resize(p_vEDO.size());
    w.resize(p_wEDO.size());
    s.resize(p_sEDO.size());

    v = p_vEDO;
    w = p_wEDO;
    s = p_sEDO;

  }


  void MVSolver::setEDOvalues(std::vector<double> v,std::vector<double> w,std::vector<double> s) {

    m_vEDO = v;
    m_wEDO = w;
    m_sEDO = s;

  }

  void MVSolver::getVmpvalues(std::vector<double>& Vm) {

    Vm.resize(p_vEDO.size());

    Vm = p_Vm;

  }

  void MVSolver::getVmvalues(std::vector<double>& Vm) {

    Vm.resize(m_vEDO.size());

    Vm = m_Vm;

  }


  void MVSolver::setVmvalues(std::vector<double> Vm) {

    m_Vm = Vm;

  }

  void MVSolver::setAssimilation(bool assim) {

    m_assimilation = assim;

  }


  void MVSolver::setPosParam(std::vector<int> pp) {

    m_posparam.clear();

    for(unsigned ipp=0 ; ipp<pp.size() ; ipp++) {
      m_posparam.push_back(pp[ipp]);
    }

  }




}



