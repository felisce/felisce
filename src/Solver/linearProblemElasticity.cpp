//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes
#include <numeric>
#include <cmath>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/mat.hpp"

// Project includes
#include "linearProblemElasticity.hpp"
#include "Core/felisceTools.hpp"
#include "Core/felisceTransient.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{

namespace { // anonymous

  enum KinematicParameter2D {
    planeStress = 1,
    planeStrain = 2
  };

  /*!
    * \brief Matrix defined so that strain = Gradient2Strain x gradient(displacement)
    *
    * No definition provided on purpose: this function is intended to be specialized
    */
  template<int DimensionT>
  UBlasMatrix Gradient2Strain();


  /*!
    * \brief Engineering elasticity tensor
    *
    *
    * \param[in] E Young modulus
    * \param[in] nu Poisson coefficient
  */
  UBlasMatrix EngineeringElastTensor_2D(double E, double nu, int kinematic_param);

  UBlasMatrix EngineeringElastTensor_3D(double E, double nu);

  //! Initialize operator GradientBasedElastTensor
  UBlasMatrix InitializeGradientBasedElastTensor(int dimension);

  //! Assign the type of force applied from the integer read in the data file
  ElasticityNS::ForceType AssignForceType(int data_parameter);


  class ErrorInvalidVolumicMass : public std::exception {
  public:
    //! Constructor
    ErrorInvalidVolumicMass();

    //! Destructor.
    ~ErrorInvalidVolumicMass() noexcept override = default;

    //! Copy constructor, to avoid warning due to user-declared destructor.
    ErrorInvalidVolumicMass(const ErrorInvalidVolumicMass&) = default;

    //! what() overload
    const char* what() const noexcept override;

  private:

    //! Message to be displayed by what()
    std::string m_message;
  };

} // namespace anonymous

LinearProblemElasticityDynamic::LinearProblemElasticityDynamic(SpatialForce force)
  : LinearProblem("Dynamic elasticity equation"),
    m_feDisplacement(nullptr),
    m_force(force),
    transient_parameters_(nullptr),
    system_(ElasticityNS::static_)
{

}

/***********************************************************************************/
/***********************************************************************************/

LinearProblemElasticityDynamic::~LinearProblemElasticityDynamic()
= default;

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES)
{
  FEL_ASSERT(doUseSNES && "Should be true!!!");
  LinearProblem::initialize(mesh, comm, doUseSNES);

  transient_parameters_ = fstransient;
  m_typeOfForceApplied = AssignForceType(FelisceParam::instance().volumicOrSurfacicForce);

  std::vector<PhysicalVariable> listVariable(1);
  std::vector<std::size_t> listNumComp(1);
  listVariable[0] = displacement;
  listNumComp[0] = this->dimension();

  m_listUnknown.push_back(displacement);

  definePhysicalVariable(listVariable,listNumComp); // Must be called after m_listUnknown is initialized

  setNumberOfMatrix(3);

  m_gradientBasedElastTensor = std::move(InitializeGradientBasedElastTensor(this->dimension()));

  const double volumic_mass = FelisceParam::instance().volumic_mass;

  if (volumic_mass < 0.)
    throw ErrorInvalidVolumicMass();

  FEL_ASSERT(fstransient->timeStep > 1.e-10);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_ELT_TYPE;
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  m_iDisplacement = m_listVariable.getVariableIdList(displacement);
  m_feDisplacement = m_listCurrentFiniteElement[m_iDisplacement];

  m_elemField.initialize(QUAD_POINT_FIELD, *m_feDisplacement, this->dimension());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::InitializeDerivedAttributes()
{
  // Check here there are no Neumann or NeumannNormal boundary conditions in the case of the volumic force
  // (it is not stricto sensu a limitation, so it could be lifted later, but at the moment it is a good way
  // to make sure there is no mishap in the data parameter file, in which we could apply both volumic and
  // surfacic force without being aware of it.)
  if (m_typeOfForceApplied == ElasticityNS::volumic) {
    if (m_boundaryConditionList.numNeumannBoundaryCondition() || m_boundaryConditionList.numNeumannNormalBoundaryCondition()) {
      FEL_ERROR("Neumann condition found along with a volumic force; it is not allowed at the moment. "
                "Check there is no mishap in your data file.");
    }
  }

  current_displacement_.duplicateFrom(this->solution());
  current_velocity_.duplicateFrom(this->solution());

  current_velocity_.zeroEntries();
  current_displacement_.zeroEntries();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::computeElementArray(const std::vector<Point*>& elemPoint,
    const std::vector<felInt>& elemIdPoint,
    int& iel, FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  IGNORE_UNUSED_IEL;
  IGNORE_UNUSED_ELEM_ID_POINT;
  FEL_ASSERT(m_feDisplacement);

  FEL_ASSERT(this->numberOfMatrices() == 3);
  FEL_ASSERT(static_cast<std::size_t>(this->numberOfMatrices()) == m_elementMat.size());

  CurrentFiniteElement& feDisplacement = *m_feDisplacement; // alias

  feDisplacement.updateFirstDeriv(0, elemPoint);

  FEL_ASSERT(transient_parameters_);
  const FelisceTransient& transient_param = *transient_parameters_;

  // First iteration is static case.
  switch(system_) {
    case ElasticityNS::static_: {
      FEL_ASSERT(transient_param.iteration >= 0);
      if (transient_param.iteration == 0) {
        // Matrix operators.
        m_elementMat[0]->grad_phi_i_GradientBasedElastTensor_grad_phi_j(1.0, feDisplacement, m_gradientBasedElastTensor, 0, 0);

        // Set the source in the case of a volumic force. Surfacic ones are handled differently (through Neumann boundary condition)
        if (m_typeOfForceApplied == ElasticityNS::volumic) {
          // Vector operators.
          for (int iComp = 0; iComp < this->dimension(); ++iComp)
            m_elemField.setValueVec(m_force, feDisplacement, iComp);

          m_elementVector[0]->source(1., feDisplacement, m_elemField, 0, this->dimension());
        }
      }
      break;
    }
    case ElasticityNS::dynamic_: {
      // Matrix operators.
      FEL_ASSERT(m_elementMat[0]);
      ElementMatrix& complete_matrix = *m_elementMat[0];

      ElementMatrix mass_matrix;
      ElementMatrix half_stiffness_matrix; // stiffness matrix multiplied by 0.5

      // Calculate mass and stiffness element matrix
      {

        mass_matrix.duplicateFrom(*m_elementMat[0]);
        half_stiffness_matrix.duplicateFrom(mass_matrix);

        half_stiffness_matrix.grad_phi_i_GradientBasedElastTensor_grad_phi_j(0.5, feDisplacement, m_gradientBasedElastTensor, 0, 0);
        mass_matrix.phi_i_phi_j(1., feDisplacement, 0, 0, this->dimension());
      }

      // Calculate coefficients used in Newmark scheme.
      FEL_ASSERT(transient_param.timeStep > 1.e-10);
      const double inv_time_step = 1. / transient_param.timeStep;
      const double velocity_mass_coefficient = 2. * FelisceParam::instance().volumic_mass * inv_time_step;
      const double displacement_mass_coefficient = velocity_mass_coefficient * inv_time_step;

      // Build the system matrix
      complete_matrix = mass_matrix;
      complete_matrix *= displacement_mass_coefficient;
      complete_matrix += half_stiffness_matrix;

      // Now we must take into account the terms related to previous iteration.
      // We must for that fill two other matrixes: one for the displacement from previous iteration and the other
      // for the velocity. Once assembled, these matrixes will be multiplied by the vectors from previous iteration
      // and the RHS will hence be evaluated.
      ElementMatrix& current_displacement_matrix = *m_elementMat[ElasticityNS::current_displacement];

      current_displacement_matrix = mass_matrix;
      current_displacement_matrix *= displacement_mass_coefficient;
      current_displacement_matrix -= half_stiffness_matrix;

      ElementMatrix& current_velocity_matrix = *m_elementMat[ElasticityNS::current_velocity];
      current_velocity_matrix = mass_matrix;
      current_velocity_matrix *= velocity_mass_coefficient;
      break;
    }
  } // switch
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::ComputeRHS()
{
  PetscVector buf;
  buf.duplicateFrom(current_displacement_);

  mult(matrix(ElasticityNS::current_displacement), current_displacement_, buf);
  multAdd(matrix(ElasticityNS::current_velocity), current_velocity_, buf, vector());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint,
    const std::vector<felInt>& elemIdPoint,
    int& iel, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  IGNORE_UNUSED_IEL;
  IGNORE_UNUSED_ELEM_ID_POINT;

  CurvilinearFiniteElement& feCurvDisplacement = *m_listCurvilinearFiniteElement[m_iDisplacement];
  feCurvDisplacement.updateMeasNormal(0, elemPoint);

  // Below: only when spatial function
  const BoundaryCondition& neumann_condition = *m_boundaryConditionList.Neumann(0);

  if (neumann_condition.typeValueBC() == FunctionS) {
    const auto& comp_list = neumann_condition.getComp();

    for (auto component_index : comp_list) { // more efficient.
      m_elemFieldNeumann[0].setValueVec(m_force, feCurvDisplacement, component_index);
    }
    // Important: in my first approach I added the following line, to mimic what was done in volumic force case:
    //m_elemVecBD->source(1., feCurvDisplacement, m_elemFieldNeumannNormal[0], block, Ncomponent);
    // It was wrong: the source was added more properly in applyNaturalBoundaryCondition()
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::endIteration()
{
  PetscInt size;
  current_displacement_.getLocalSize(&size);

  PetscInt NdofLocalPerUnknown = numDofLocalPerUnknown(displacement);

  std::vector<PetscInt> local_indexing(NdofLocalPerUnknown); // std::vector that contains integers from 0 to local size - 1

//   std::iota(local_indexing.begin(), local_indexing.end(), 0);

  for (int i = 0; i < NdofLocalPerUnknown; ++i) // substitute that also works in C++ 98 (arguably more easy to understand anyway)
    local_indexing[i] = i;

  double vel_min = 1.e20;
  double vel_max = -1.e20;
  double disp_current_min = 1.e20;
  double disp_current_max = -1.e20;
  double disp_new_min = 1.e20;
  double disp_new_max = -1.e20;

  {
    PetscScalar * solution;
    this->solution().getArray(&solution);

    PetscScalar * displacement_prev;
    current_displacement_.getArray(&displacement_prev);

    PetscScalar * my_velocity;
    current_velocity_.getArray(&my_velocity);

    for (PetscInt i = 0; i < size; ++i) {
      my_velocity[i] *= -1.;
      my_velocity[i] += 2. / transient_parameters_->timeStep * (solution[i] - displacement_prev[i]);
      vel_min = std::min(vel_min, my_velocity[i]);
      vel_max = std::max(vel_max, my_velocity[i]);
      disp_current_min = std::min(disp_current_min, displacement_prev[i]);
      disp_current_max = std::max(disp_current_max, displacement_prev[i]);
    }

    this->solution().restoreArray(&solution);
    current_displacement_.restoreArray(&displacement_prev);
    current_velocity_.restoreArray(&my_velocity);
  }

  std::cout << "Displ prev [min, max] = [" << disp_current_min << ", " << disp_current_max << "]\n";
  std::cout << "Velocities [min, max] = [" << vel_min << ", " << vel_max << "]\n";

  // Make sure to do that after the velocity calculation!
  UpdateCurrentDisplacement();

  {
    PetscScalar * displacement_new;
    current_displacement_.getArray(&displacement_new);

    for (PetscInt i = 0u; i < size; ++i) {
      disp_new_min = std::min(disp_new_min, displacement_new[i]);
      disp_new_max = std::max(disp_new_max, displacement_new[i]);
    }

    current_displacement_.restoreArray(&displacement_new);

  }

  std::cout << "Displ new  [min, max] = [" << disp_new_min << ", " << disp_new_max << "]\n";
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::UpdateCurrentDisplacement() {
  current_displacement_.copyFrom(this->solution());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemElasticityDynamic::GoToDynamic() {
  system_ = ElasticityNS::dynamic_;
}

/***********************************************************************************/
/***********************************************************************************/

namespace { // anonymous

template<>
UBlasMatrix Gradient2Strain<3>() {
  UBlasMatrix ret(6, 9);
  ret.clear();

  ret(0, 0) = 1.;
  ret(1, 4) = 1.;
  ret(2, 8) = 1.;
  ret(3, 1) = 1.;
  ret(3, 3) = 1.;
  ret(4, 5) = 1.;
  ret(4, 7) = 1.;
  ret(5, 2) = 1.;
  ret(5, 6) = 1.;

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

template<>
UBlasMatrix Gradient2Strain<2>() {
  UBlasMatrix ret(3, 4);
  ret.clear();

  ret(0, 0) = 1.;
  ret(1, 3) = 1.;
  ret(2, 1) = 1.;
  ret(2, 2) = 1.;

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix EngineeringElastTensor_3D(const double E, const double nu)
{
  UBlasMatrix ret(6, 6);
  ret.clear();

  ret(0, 0) = ret(1, 1) = ret(2, 2) = 1.;

  const double inv = 1. / (1. - nu);
  ret(3, 3) = ret(4, 4) = ret(5, 5) = 0.5 * (1. - 2. * nu) * inv;

  {
    const double non_diag_value = nu * inv;

    for (std::size_t i = 0u; i < 3u; ++i) {
      for (std::size_t j = i + 1u; j < 3u; ++j)
        ret(i, j) = ret(j, i) = non_diag_value;
    }
  }

  const double factor = E * (1. - nu) / ((1. + nu) * (1. - 2. * nu));
  ret *= factor;

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix EngineeringElastTensor_2D(const double E, const double nu, const int kinematic_param)
{
  UBlasMatrix ret(3, 3);
  ret.clear();

  switch(kinematic_param) {
    case planeStress: {
      ret(0, 0) = 1.;
      ret(1, 1) = 1.;
      ret(2, 2) = 0.5 * (1. - nu);
      ret(0, 1) = nu;
      ret(1, 0) = nu;

      const double factor = E / (1. - std::pow(nu, 2));
      ret *= factor;

      return ret;
    }
    case planeStrain: {
      const double inv = 1. / (1. - nu);

      ret(0, 0) = 1.;
      ret(1, 1) = 1.;
      ret(2, 2) = 0.5 * inv * (1. - 2. * nu);
      ret(0, 1) = nu * inv;
      ret(1, 0) = nu * inv;

      const double factor = E * (1. - nu) / ((1. + nu) * (1. - 2 * nu));
      ret *= factor;

      return ret;
    }
  }

  FEL_ERROR("SHOULD NEVER BE REACHED!!!");
  return ret; // to avoid warning
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix InitializeGradientBasedElastTensor(const int dimension)
{
  UBlasMatrix engineering_elast_tensor, gradient_2_strain;

  // Use Poisson coefficient and Young modulus read in the input parameter file
  const double nu = FelisceParam::instance().poisson;
  const double E = FelisceParam::instance().young;
  const int kinematic_parameter = FelisceParam::instance().planeStressStrain;

  if (kinematic_parameter != planeStrain && kinematic_parameter != planeStress)
    FEL_ERROR("Must be 1 (plane stress) or 2 (plane strain)");

  FEL_ASSERT(std::fabs(nu -1.) > 1.e-10);

  switch(dimension) {
    case 2: {
      engineering_elast_tensor = std::move(EngineeringElastTensor_2D(E, nu, kinematic_parameter));
      gradient_2_strain = std::move(Gradient2Strain<2>());
      break;
    }
    case 3: {
      engineering_elast_tensor = std::move(EngineeringElastTensor_3D(E, nu));
      gradient_2_strain = std::move(Gradient2Strain<3>());
      break;
    }
    default: {
      FEL_ERROR("Dimension must be 2 or 3");
      exit(1);
    }
  }

  // Intermediate step as Boost ublas gets a lousy syntax...
  UBlasMatrix product1 = std::move(boost::numeric::ublas::prod(engineering_elast_tensor, gradient_2_strain));

  return std::move(prod(boost::numeric::ublas::trans(gradient_2_strain), std::move(product1)));
}

/***********************************************************************************/
/***********************************************************************************/

ElasticityNS::ForceType AssignForceType(const int data_parameter) {

  // Check whether a surfacic or volumic force has been chosen
  switch (data_parameter) {
    case static_cast<int>(ElasticityNS::surfacic):
      return ElasticityNS::surfacic;
    case static_cast<int>(ElasticityNS::volumic):
      return ElasticityNS::volumic;
    default:
      FEL_ERROR("Force applied should be either volumic or surfacic; make sure [solid] volumicOrSurfacicForce "
                "is correctly filled in your data file and encompass an acceptable value");
  }

  FEL_ERROR("Should never be reached; if so bug to correct in the function!!!");
  return ElasticityNS::volumic; // to avoid warning about missing warning
}

/***********************************************************************************/
/***********************************************************************************/

ErrorInvalidVolumicMass::ErrorInvalidVolumicMass() {
  m_message = std::move("No volumic mass read in input parameter file.");
}

/***********************************************************************************/
/***********************************************************************************/

const char* ErrorInvalidVolumicMass::what() const noexcept {
  return m_message.c_str();
}

} // namespace anonymous

} // namespace felisce
