//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J Fouchet & C Bui
//

#ifndef _COEFPROBLEM_HPP
#define _COEFPROBLEM_HPP

// System includes
#include <set>
#include <unordered_map>
#include <ostream>
#include <vector>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/mat.hpp"
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"
#include "Core/NoThirdPartyWarning/Petsc/pc.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ksp.hpp"
#include "Core/NoThirdPartyWarning/Petsc/error.hpp"
#include "Core/NoThirdPartyWarning/Mpi/mpi.hpp"

// Project includes
#include "PETScInterface/petscVector_fwd.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "Core/felisceParam.hpp"
#include "InputOutput/io.hpp"
#include "Core/chrono.hpp"
#include "PETScInterface/KSPInterface.hpp"

namespace felisce 
{
  /*!
   \class CoefProblem
   \authors J Fouchet & C Bui
   \date 04/01/2013
   \brief find the alpha_i coeficient.
   */

  class CoefProblem {
  public:

    // Constructor
    //============
    CoefProblem();
    virtual ~CoefProblem();

    void initializeCoefProblem(MPI_Comm& comm);

    // Allocate Matrix and RHS
    //========================
    void allocateMatrixRHS(int size, int rank);

    // Assemble Matrix and RHS
    //========================
    void assembleMatrix(const int idInflowOutflowBC, const std::vector<double> m_fluxLinCombBC, int rank);
    void assembleRHS(const std::vector<double> pressure, const std::vector<double> m_fluxLinCombBC, const std::vector<double> m_fluxLinCombBCtotal, int rank);

    // Write
    //========

    //Write in file matrix.m, matrix in matlab format.
    void writeMatrixForMatlab(int verbose = 0, std::ostream& outstr = std::cout) const;

    //Write in file RHS.m, RHS in matlab format.
    void writeRHSForMatlab(int verbose = 0, std::ostream& outstr = std::cout) const;

    void writeSolForMatlab(int verbose = 0, std::ostream& outstr = std::cout) const;

    //Write in file filename, matrix in matlab format.
    void writeMatrixForMatlab(std::string const& fileName, PetscMatrix& matrix) const;

    //Write in file filenam, std::vector in matlab format.
    void writeVectorForMatlab(std::string const& fileName, PetscVector& vector) const;

    // Print solution of the system solve.
    void printSolution(int verbose = 0, std::ostream& outstr = std::cout) const;

    //Solve linear system
    //===================
    void buildSolver();
    void solve(int rank, int size);

    // Access Functions
    //=================

    inline const PetscVector&  solution() const {
      return m_sol;
    }
    inline PetscVector&  solution() {
      return m_sol;
    }

    inline const PetscVector&  RHS() const {
      return _RHS;
    }
    inline PetscVector&  RHS() {
      return _RHS;
    }

    inline const PetscMatrix& Matrix() const {
      return m_Matrix;
    }
    inline PetscMatrix& Matrix() {
      return m_Matrix;
    }

  protected:

    int m_verbose;
    MPI_Comm m_petscComm;

    //! RHS of the system.
    PetscVector _RHS;

    //! solution of the problem.
    PetscVector m_sol;

    //! Matrix of the system.
    PetscMatrix m_Matrix;

    std::vector<double> timeStepOverC;
    std::vector<double> RplusTimeStepOverC;

    bool m_buildSolver;
    KSPInterface::Pointer m_ksp = felisce::make_shared<KSPInterface>();
  };

}

#endif
