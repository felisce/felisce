//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  CardiacFunction::CardiacFunction() {
    m_fstransient = nullptr;
  }

  CardiacFunction::~CardiacFunction() {
    m_fstransient = nullptr;
  }

  void CardiacFunction::initialize(FelisceTransient::Pointer fstransient) {
    m_fstransient = fstransient;
  }

  //A class to impose thorso/lungs/bone conductivity.
  ThoraxConductivity::ThoraxConductivity() = default;

  ThoraxConductivity::~ThoraxConductivity() = default;

  double ThoraxConductivity::sigmaT(int& label) const {
    double conductivity;

    // Thorax conuductivity (mesh region with label == 1)
    //if (label == 1){ // Old Zygote
    //if (label == 303){ // New systole heart
    if (label == 101) { // New end-systole heart && ZygoteOldFinest
      conductivity = FelisceParam::instance().sigmaThorax;
    }
    // Bones conuductivity (mesh region with label == 2)
    //else if (label == 2){ // Old Zygote
    //else if (label == 302){ // New systole heart
    else if (label == 103) { // New end-systole heart && ZygoteOldFinest
      conductivity = FelisceParam::instance().sigmaBone;
    }
    // Lungs conuductivity (mesh region with label == 3)
    //else if (label == 3){ // Old Zygote
    //else if (label == 301){ // New systole heart
    else if (label == 102) { // New end-systole heart && ZygoteOldFinest
      conductivity = FelisceParam::instance().sigmaLung;
    } else {
      if (FelisceParam::verbose() > 10) std::cout << "Warning ! Label " << label << " not found !\n";
      conductivity = FelisceParam::instance().sigmaThorax;
    }


    return conductivity;
  }

  double ThoraxConductivity::operator() (int& label) const {
    return sigmaT(label);
  }

  //A class to impose thorso/lungs/bone conductivity.
  HeartConductivity::HeartConductivity() = default;

  HeartConductivity::~HeartConductivity() = default;

  double HeartConductivity::sigmaH(int& label) const {
    double conductivity;

    if (label == 2) {
      conductivity = 0.0;
    } else if (label == 1) {
      conductivity = FelisceParam::instance().intraTransvTensor;
    } else {
      std::cout << "Warning ! Label " << label << " not found !\n";
      conductivity = 0.0;
    }

    return conductivity;
  }

  double HeartConductivity::operator() (int& label) const {
    return sigmaH(label);
  }

  //A class to impose a non-pathological applied current to the ellibi-geometry.
  AppCurrent::AppCurrent() = default;

  AppCurrent::~AppCurrent() = default;

  double AppCurrent::Iapp(double x, double y, double z, double t, double) const {
    double iapp=0.0;
    double delay = FelisceParam::instance().delayStim[0];
    double tPeriod=fmod(t,FelisceParam::instance().timePeriodVentricle);

    //current in ventricles:
    double pi = acos(-1.0);
    //left : left ventricle volume = { (x^2/4^2 + y^2/4^2 + z^2/7.2^2) < 1 } && { (x^2/(4-1.2)^2 + y^2/(4-1.2)^2 + z^2/(7.2-1.2)^2) > 1 } && { z < 2.76 }
    double a_L = 4.0;
    double b_L = 4.0;
    double c_L = 7.2;
    double e_L1  = 1.6*0.8;
    double e_L2 = 0.0;
    double sumL1 = x*x/((a_L-e_L1)*(a_L-e_L1))+y*y/((b_L-e_L1)*(b_L-e_L1))+z*z/((c_L-e_L1)*(c_L-e_L1))-1.0;
    double sumL2 = x*x/((a_L-e_L2)*(a_L-e_L2))+y*y/((b_L-e_L2)*(b_L-e_L2))+z*z/((c_L-e_L2)*(c_L-e_L2))-1.0;
    double angularVelocityLV = (pi/2.0)/FelisceParam::instance().stimTimeLV;
    double alphaLVmax = std::min((tPeriod-delay)*angularVelocityLV,2*pi);
    double alphaLV = std::atan((x+1.5)/(2.7586-z));

    //right : right ventricle volume =  { (x^2/7.95^2 + y^2/4^2 + z^2/6.84^2) < 1 } && { (x^2/(7.95-0.7)^2 + y^2/(4-0.7)^2 + z^2/(6.84-0.7)^2) > 1 } && { z < 2.76 }
    double a_R = 7.8;
    double b_R = 4.0;
    double c_R = 7.2*0.95; //=6.84
    double e_R  = 1.0*0.8;
    double sumR = x*x/((a_R-e_R)*(a_R-e_R))+y*y/((b_R-e_R)*(b_R-e_R))+z*z/((c_R-e_R)*(c_R-e_R))-1.0;
    double angularVelocityRV = (pi/2.0)/FelisceParam::instance().stimTimeRV ;
    double alphaRVmax = std::min((tPeriod-delay)*angularVelocityRV,2*pi);
    double alphaRV = std::atan((-3.7-x)/(2.9586-z));

    //left
    if ((tPeriod>=delay)&&(tPeriod<=(delay+FelisceParam::instance().stimTimeLV))) { //if t \in [delay,delay+t_{actLeft}]
      if (sumL1<0.15) { //if (x,y,z) \in S (endocardium of left ventricle)
        if (alphaLV < alphaLVmax) { //if std::atan((x-x0)/(z-z0)) < alpha(t)
          iapp = (-5.0/2.0*sumL1+0.375)*8.0/200.0;
        }
      }
    }

    //septum (right)
    if ((tPeriod>=delay)&&(tPeriod<=(delay+ FelisceParam::instance().stimTimeRV))) { //if t \in [delay,delay+t_{actRight}]
      if((sumL2>=-0.2)&&(sumL2<=0.0)&&(x<-2.4)) { //if (x,y,z) \in S (right part of septum)
        iapp= (5.0/2.0*sumL2+0.5)*8.0/200.0;
      }
    }

    //right
    if (tPeriod>=delay&&(tPeriod<=(delay+FelisceParam::instance().stimTimeRV))) { //if t \in [delay,delay+t_{actRight}]
      if ((x<-2.586)&&(sumL2>-0.2)&&(sumR>=0.0)&&(sumR<0.1)) { //if (x,y,z) \in S (right part of septum or endocardium of right ventricle)
        if (alphaRV < alphaRVmax) { //if std::atan((x-x0)/(z-z0)) < alpha(t)
          iapp = 0.5*8.0/200.0;
        }
      }
    }

    return iapp;

  }

  double AppCurrent::operator() (double x, double y, double z, double t, double) const {
    return Iapp(x,y,z,t);
  }

  //A class to impose a non-pathological applied current to the zygote-geometry.
  zygoteIapp::zygoteIapp():
    AppCurrent()
  {}

  double zygoteIapp::Iapp(double x, double y, double z, double t, double dist) const {

    double iapp = 0.0;
    double delay = FelisceParam::instance().delayStim[0];


    //APEX, node number: 80185 : 3.316424 31.496351 5.799850 (coordinates of "Nejib heart")
    double x_0= 3.316424;
    double y_0 = 31.496351;
    double z_0 = 5.799850;

    //APEX, node number: 80185 : 2.74681 28.9865 6.20925 (coordinates of "?? heart")
    /*double x_0= 2.74681;
     double y_0 =  28.9865;
     double z_0 = 6.20925;
     */
    //APEX, node number: 80185 : 2.74681 28.9865 6.20925 (coordinates of "?? heart")
    /*double x_0= 3.01795;
     double y_0 =  28.6617;
     double z_0 = 5.27786;
     */
    /*
     //APEX, node number: ?? : 29.4715 88.8100 62.8179 (coordinates of heart_024_ventricules_nRef_Vol.mesh)
     double x_0= 29.4715;
     double y_0 = 88.8100;
     double z_0 = 62.8179;
     */

    double tPeriod=fmod(t,FelisceParam::instance().timePeriodVentricle);
    double tPer2 = 4*(tPeriod-delay)*(tPeriod-delay);

    //if simulation starts later than t=0
    if ((tPeriod>=0.0)&&(tPeriod<delay)) {
      iapp=0.0;
    }

    double sum2 = (x-x_0)*(x-x_0) + (y-y_0)*(y-y_0) + (z-z_0)*(z-z_0);

    if ((tPeriod>=delay)&&(tPeriod<=(delay+ FelisceParam::instance().stimTimeLV))) { //if t \in [delay,delay+t_{actLeft}]
      if ( Tools::equal(dist,20.0) || (dist < -0.2) ) { // Left ventricle endocardium
        if ( (sum2 >= (tPer2-200.0) ) && (sum2 <= (3.0*tPer2)) ) {
          iapp=2.0/500.0;
        }
      }
    }

    if ((tPeriod>=delay)&&(tPeriod<=(delay+ FelisceParam::instance().stimTimeRV))) { //if t \in [delay,delay+t_{actRight}]
      if ( Tools::equal(dist,200.0) || Tools::equal(dist,10.0) ) { // Right ventricle endocardium
        if ( (sum2 >= (tPer2-200.0) ) && (sum2 <= (3.0*tPer2)) ) {
          iapp=2.0/500.0;
        }
      }
    }

    return iapp;

  }

  double zygoteIapp::operator() (double x, double y, double z, double t, double dist) const {
    return Iapp(x,y,z,t,dist);
  }


  //A class to impose a non-pathological applied current to the atria.
  AppCurrentAtria::AppCurrentAtria() = default;

  AppCurrentAtria::~AppCurrentAtria() = default;

  double AppCurrentAtria::IappAtria(double x, double y, double z, double t) const {
    double iapp=0.0;
    double delay = FelisceParam::instance().delayStim[0];
    double tPeriod=fmod(t,FelisceParam::instance().timePeriodAtria);
    double i0 = 0.01*2.5;

    //current in Atrium
    if (FelisceParam::instance().hasHeteroCondAtria) {
      i0 = 0.01*2.0;
    }

    //double x2879 = -1.926082;
    double y2879 = 17.376394;
    double z2879 = 1.520698;

    //double x3484 = -1.763522;
    double y3484 = 17.849342;
    double z3484 = 1.738376;


    //double xmid = (x2879 + x3484)/2.0;
    double ymid = (y2879 + y3484)/2.0;
    double zmid = (z2879 + z3484)/2.0;

    //double x3095 = -1.877060;
    double y3095 = 17.539717;
    double z3095 = 1.705356;


    double a = std::sqrt((z2879-z3484)*(z2879-z3484)+(y2879-y3484)*(y2879-y3484))/2.0;
    double b = std::sqrt((zmid-z3095)*(zmid-z3095)+(ymid-y3095)*(ymid-y3095));

    double xAxis = zmid - z3095;
    double yAxis = ymid - y3095;
    xAxis = xAxis/std::sqrt(xAxis*xAxis+yAxis*yAxis);
    yAxis = yAxis/std::sqrt(xAxis*xAxis+yAxis*yAxis);

    double theta = acos(xAxis*1.0);

    double X = std::cos(theta)*z-std::sin(theta)*y;
    double Y = std::sin(theta)*z+std::cos(theta)*y;
    double X0 = std::cos(theta)*zmid-std::sin(theta)*ymid;
    double Y0 = std::sin(theta)*zmid+std::cos(theta)*ymid;
    // if DA court
    if (FelisceParam::instance().dataAssimilation) {
      X0 -=0.5;
      Y0 +=0.5;
    }

    double sumA = (X-X0)*(X-X0)/(a*a)+(Y-Y0)*(Y-Y0)/(b*b);

    // Bachman bundle
    //    double xB = -0.287020;
    //    double zB = 1.695450;
    //
    //    double xC = 1.320040;
    //    double zC = 0.896870;
    //
    //    double xBCmid = (xB+xC)/2.0;
    //    double zBCmid = (zB+zC)/2.0;
    //
    //    double xxaxis = xBCmid - xB;
    //    double zzaxis = zBCmid - zB;
    //
    //    double theta2 = acos(xxaxis*1.0);
    //
    //    double XX = std::cos(theta2)*x-std::sin(theta2)*z;
    //    double ZZ = std::sin(theta2)*x+std::cos(theta2)*z;
    //    double XX0 = std::cos(theta2)*xBCmid-std::sin(theta2)*zBCmid;
    //    double ZZ0 = std::sin(theta2)*xBCmid+std::cos(theta2)*zBCmid;
    //
    //
    //    double sumB = (XX-XX0)*(XX-XX0)/5.0 + 30.0*(ZZ-ZZ0)*(ZZ-ZZ0);
    //
    //    double sumC = (x-xB)*(x-xB)+(z-zB)*(z-zB);
    //
    double radius_A = 1.0;
    if ((tPeriod>=delay)&&(tPeriod<=(delay+FelisceParam::instance().stimTime))) { //if t \in [delay,delay+t_{act}]
      if ((sumA < radius_A)&&(x < -1.40)) {
        iapp = i0;
      }
    }

    // Bachman bundle
    //    if ((tPeriod>=delay+20.0)&&(tPeriod<=(delay+FelisceParam::instance().stimTime+20.0))){ //if t \in [delay,delay+t_{act}]
    //      if ((sumB < 0.5)&&(y>=13.5)){
    //        iapp = i0;
    //      }
    //    }

    return iapp;
  }

  double AppCurrentAtria::operator() (double x, double y, double z, double t) const {
    return IappAtria(x,y,z,t);
  }


  // Heterogeneous schaf parameters:

  //A class to impose heterogeneous tau_close.
  HeteroTauClose::HeteroTauClose() = default;

  HeteroTauClose::~HeteroTauClose() = default;

  double HeteroTauClose::TauClose(double x, double y, double z, double dist, double t) const {
    double tauClose = FelisceParam::instance().tauClose;
    bool test = true;

    // During a short-long-short QT segment sequence - in order to simulate a Torsades de pointe
    // Warning: works only for "zygote" or "heart" ore "ellipseheart"
    if ( (FelisceParam::instance().torsade) && (t > FelisceParam::instance().torsadeTimeBegin) && (t < FelisceParam::instance().torsadeTimeEnd)  ) {
      if ( Tools::equal(dist,200.0) ) {
        tauClose =FelisceParam::instance().tauCloseRVTorsade;
        test = false;
      } else if ( ((dist >= -1.0) && (dist <= 0.0)) || Tools::equal(dist,20.0) ) {
        tauClose = FelisceParam::instance().tauCloseEndoTorsade;
        test = false;
      } else if ( (dist > 0.0) && (dist <= 0.33333) ) {
        tauClose =FelisceParam::instance().tauCloseCellTorsade;
        test = false;
      } else if ( (dist > 0.33333) && (dist <= 1.0) ) {
        tauClose =FelisceParam::instance().tauCloseEpiTorsade;
        test = false;
      } else if ( Tools::equal(dist,100.0) || Tools::equal(dist,10.0) ) {
        tauClose =FelisceParam::instance().tauCloseRVTorsade;
        test = false;
      } else if ( Tools::equal(dist,-100.0) ) {
        tauClose = 1.;
        test = false;
      } else if ( Tools::equal(dist,-200.0) ) { //atria (complete heart)
        tauClose = 1.0;
        test = false;
      }

      if (test) {
        std::cout << "WARNING ! \nPoint (" << x << "," << y << "," << z << ") has distance " << dist << " => tauClose not defined !\n";
      }
    } else {
      if ( FelisceParam::instance().typeOfAppliedCurrent == "atria" ) {
        if ( y > 6.7) {
          tauClose = 70.0;
        } else {
          tauClose = 80.0;
        }
      } else if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
        // Right Ventricle
        if ( Tools::equal(dist,200.0) ) {
          tauClose =FelisceParam::instance().tauCloseRV;
          test = false;
        }
        // Left Ventricle -- Endo
        else if ( ((dist >= -1.0) && (dist <= 0.0)) || Tools::equal(dist,20.0) ) {
          tauClose = FelisceParam::instance().tauCloseEndo;
          test = false;
        }
        // Left Ventricle -- Mcells
        else if ( (dist > 0.0) && (dist <= 0.33333) ) {
          tauClose =FelisceParam::instance().tauCloseCell;
          test = false;
        }
        // Left Ventricle -- Epi
        else if ( (dist > 0.33333) && (dist <= 1.0) ) {
          tauClose =FelisceParam::instance().tauCloseEpi;
          test = false;
        }
        // Right Ventricle
        else if ( Tools::equal(dist,100.0) || Tools::equal(dist,10.0) ) {
          tauClose =FelisceParam::instance().tauCloseRV;
          test = false;
        }
        // Right Ventricle
        else if ( Tools::equal(dist,-100.0) ) {
          tauClose =FelisceParam::instance().tauCloseRV;
          test = false;
        }
        // Atria (complete heart)
        else if ( Tools::equal(dist,-200.0) ) {
          tauClose = 75.0;
          test = false;
        }

        if (test) {
          std::cout << "WARNING ! \nPoint (" << x << "," << y << "," << z << ") has distance " << dist << " => tauClose not defined !\n";
        }
      } else {
        double a = 4.0;
        double b = 4.0;
        double c = 7.2;
        double e4 = 0.;
        double e3  = 0.5*0.8;
        double e2  = 1.0*0.8;
        double e1  = 1.6*0.8;

        double sum1 = x*x/( (a-e1)*(a-e1) ) +  y*y/( (b-e1)*(b-e1) ) +  z*z/( (c-e1)*(c-e1) ) -1.0;

        double sum2 = x*x/( (a-e2)*(a-e2) ) +  y*y/( (b-e2)*(b-e2) ) +  z*z/( (c-e2)*(c-e2) ) -1.0;

        double sum3 = x*x/( (a-e3)*(a-e3) ) +  y*y/( (b-e3)*(b-e3) ) +  z*z/( (c-e3)*(c-e3) ) -1.0;

        double sum4 = x*x/( (a-e4)*(a-e4) ) +  y*y/( (b-e4)*(b-e4) ) +  z*z/( (c-e4)*(c-e4) ) -1.0;

        if ( (sum1 >= -0.1 ) && (sum2 < 0) ) {
          tauClose = FelisceParam::instance().tauCloseEndo;
          test = false;
        } else if ( (sum2 >= 0.0) && (sum3 < 0)) {
          tauClose =FelisceParam::instance().tauCloseCell;
          test = false;
        } else if ( (sum3 >= 0.0) && (sum4 <= 0.1) ) {
          tauClose =FelisceParam::instance().tauCloseEpi;
          test = false;
        }
        if ( x < -2.344 ) {
          tauClose =FelisceParam::instance().tauCloseRV;
          test = false;
        }

        if(test) {
          tauClose = FelisceParam::instance().tauCloseRV;
        }
      }
    }
    return tauClose;
  }

  double HeteroTauClose::operator() (double x, double y, double z, double dist, double t) const {
    return TauClose(x,y,z,dist,t);
  }


  //A class to simulate infarction.
  HeteroTauOut::HeteroTauOut() = default;

  HeteroTauOut::~HeteroTauOut() = default;

  double HeteroTauOut::TauOut(double x, double y, double z) const {
    double tauOut = FelisceParam::instance().tauOut;

    double x1 = x - FelisceParam::instance().x_infarct;
    double y1 = y - FelisceParam::instance().y_infarct;
    double z1 = z - FelisceParam::instance().z_infarct;

    if ( FelisceParam::instance().typeOfAppliedCurrent == "circle" ) {
      double sum2 = std::sqrt(x1*x1 + y1*y1 + z1*z1);
      if (sum2 > FelisceParam::instance().radius_infarct)
        tauOut = 1.e+13;
      else
        tauOut = FelisceParam::instance().tauOut;
    } else if ( FelisceParam::instance().hasInfarct) {
      double sum2 = std::sqrt(x1*x1 + y1*y1 + z1*z1);

      if (sum2 < FelisceParam::instance().radius_infarct)
        tauOut = (FelisceParam::instance().tauOut)/10.0;
      else
        tauOut = FelisceParam::instance().tauOut;
    }
    return tauOut;

  }

  double HeteroTauOut::operator() (double x, double y, double z) const {
    return TauOut(x,y,z);
  }

  //A class to simulate infarction.
  HeteroTauIn::HeteroTauIn() = default;

  HeteroTauIn::~HeteroTauIn() = default;

  double HeteroTauIn::TauIn(double x, double y, double z) const {
    double tauIn = FelisceParam::instance().tauIn;

    double x1 = x - FelisceParam::instance().x_infarct;
    double y1 = y - FelisceParam::instance().y_infarct;
    double z1 = z - FelisceParam::instance().z_infarct;

    if ( FelisceParam::instance().typeOfAppliedCurrent == "circle" ) {
      double sum2 = std::sqrt(x1*x1 + y1*y1 + z1*z1);
      if (sum2 > FelisceParam::instance().radius_infarct) {
        tauIn = 1.e+13;
      } else {
        tauIn = FelisceParam::instance().tauIn;
      }
    }
    return tauIn;

  }

  double HeteroTauIn::operator() (double x, double y, double z) const {
    return TauIn(x,y,z);
  }

  //A class to impose heterogeneous Ito for the Courtemanche-Ramirez-Nattel model.
  HeteroCourtModelIto::HeteroCourtModelIto() = default;

  HeteroCourtModelIto::~HeteroCourtModelIto() = default;

  double HeteroCourtModelIto::CourtCondIto(double x, double y, double z) const {
    double courtCondIto = 0.0;
    double z1 = 0.5;
    double z2 = 4.0;
    double y1 = 10.7;
    double y2 = 17.4;


    double z0 = (z1+z2)/2.0;
    double y0 = (y1+y2)/2.0;

    double a = (z2-z1)/2.0;
    double b = (y2-y1)/2.0;

    if(((z-z0)*(z-z0)/(a*a)+(y-y0)*(y-y0)/(b*b) > 1.1) && (x < -1.6) && ((z-z0)*(z-z0)/(a*a)+(y-y0)*(y-y0)/(b*b) < 1.55) && (z < 1.7) && (y < 17.5)) {
      courtCondIto = 0.2215;
    } else {
      courtCondIto = 0.1652;
    }

    return courtCondIto;

  }

  double HeteroCourtModelIto::operator() (double x, double y, double z) const {
    return CourtCondIto(x,y,z);
  }

  //A class to impose heterogeneous ICaL for the Courtemanche-Ramirez-Nattel model.
  HeteroCourtModelICaL::HeteroCourtModelICaL() = default;

  HeteroCourtModelICaL::~HeteroCourtModelICaL() = default;

  double HeteroCourtModelICaL::CourtCondICaL(double x, double y, double z) const {
    double courtCondICaL = 0.0;
    double z1 = 0.5;
    double z2 = 4.0;
    double y1 = 10.7;
    double y2 = 17.4;


    double z0 = (z1+z2)/2.0;
    double y0 = (y1+y2)/2.0;

    double a = (z2-z1)/2.0;
    double b = (y2-y1)/2.0;

    if(((z-z0)*(z-z0)/(a*a)+(y-y0)*(y-y0)/(b*b) > 1.1) && (x < -1.6) && ((z-z0)*(z-z0)/(a*a)+(y-y0)*(y-y0)/(b*b) < 1.55) && (z < 1.7) && (y < 17.5)) {
      courtCondICaL = 0.2067;
    } else {
      courtCondICaL = 0.1238;
    }

    return courtCondICaL;

  }

  double HeteroCourtModelICaL::operator() (double x, double y, double z) const {
    return CourtCondICaL(x,y,z);
  }


  //A class to impose heterogeneous multiplicative coefficient for the Courtemanche-Ramirez-Nattel model.
  HeteroCourtModelMultCoeff::HeteroCourtModelMultCoeff() = default;

  HeteroCourtModelMultCoeff::~HeteroCourtModelMultCoeff() = default;

  double HeteroCourtModelMultCoeff::CourtCondMultCoeff(double x, double y, double z, double ref) const {
    IGNORE_UNUSED_ARGUMENT(x);
    IGNORE_UNUSED_ARGUMENT(y);
    IGNORE_UNUSED_ARGUMENT(z);

    double courtCondMultCoeff = 1.0;

    double g_Na_PM = FelisceParam::instance().g_Na_PM;
    double g_Na_CT = FelisceParam::instance().g_Na_CT;
    double g_Na_BB = FelisceParam::instance().g_Na_BB;    
    
    if (std::fabs(ref - 31.0) < 1.0e-12) { //SN
      courtCondMultCoeff = 1.0;   //1.1;
    } else if (std::fabs(ref - 32.0) < 1.0e-12) { //CT
      if (g_Na_CT < 0.) {
        courtCondMultCoeff = 4.0; //3.4;
      }
      else {
        courtCondMultCoeff = g_Na_CT;
      }
    } else if (std::fabs(ref - 33.0) < 1.0e-12) { //BB
      if (FelisceParam::instance().hasPartialBachmannBundleBlock)
        courtCondMultCoeff = FelisceParam::instance().valueBBBlock;
      else
        if (g_Na_BB < 0.) {
          courtCondMultCoeff = 6.0; //6.0;
        }
        else {
          courtCondMultCoeff = g_Na_BB;
        }
    } else if (std::fabs(ref - 34.0) < 1.0e-12) { //RAI
      courtCondMultCoeff = 1.0; //0.5;
    } else if (std::fabs(ref - 35.0) < 1.0e-12) { //PM
      if (g_Na_PM < 0.) {
        courtCondMultCoeff = 1.5; //1.3;
      }
      else {
        courtCondMultCoeff = g_Na_PM;
      }      
    } else if (std::fabs(ref - 36.0) < 1.0e-12) { //FO
      courtCondMultCoeff = 0.5;
    } else { //regular atria
      courtCondMultCoeff = 1.0;
    }

    return courtCondMultCoeff;

  }

  double HeteroCourtModelMultCoeff::operator() (double x, double y, double z, double ref) const {
    return CourtCondMultCoeff(x,y,z,ref);
  }


  // Class to impose variable "s" parameter in FHN
  HeteroSparFHN::HeteroSparFHN() = default;
  HeteroSparFHN::~HeteroSparFHN() = default;

  double HeteroSparFHN::SparFHN(double x, double y, double z) const {

//    IGNORE_UNUSED_ARGUMENT(z);
//    double ratioS = 10.0;
//    double y0 = 0.25;
//    double y1 = 0.75;
//
//    double sPar = FelisceParam::instance().f0;
//    if( (x<=0.5)&&(y>=y0)&&(y<=y1)) {
//      sPar = sPar*( ((ratioS-1)/ratioS) * (y-y1)/(y1-y0) + 1.0);
//      //sPar = 2*sPar*(1.0-ratioS)*y + sPar*(2*ratioS-1);
//    }
//    return sPar;

    double x1 = x - FelisceParam::instance().x_infarct;
    double y1 = y - FelisceParam::instance().y_infarct;
    double z1 = z - FelisceParam::instance().z_infarct;
    double sum1 = std::sqrt(x1*x1 + y1*y1 + z1*z1);

    double sPar = FelisceParam::instance().f0;
    if( sum1 < FelisceParam::instance().radius_infarct) {
      sPar = sPar/10.;
    }
    return sPar;

  }

  double HeteroSparFHN::operator() (double x, double y, double z) const {
    return SparFHN(x,y,z);
  }


  // Heterogeneous schaf parameters:

  //A class to impose heterogeneous tau_close.
  HeteroMVCoeff::HeteroMVCoeff() = default;

  HeteroMVCoeff::~HeteroMVCoeff() = default;

  int HeteroMVCoeff::MVCoeff(double x, double y, double z, double dist, double) const {
    int coeffType = -1;
    bool test = true;

    if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
      // Right Ventricle
      if ( Tools::equal(dist,200.0) ) {
        coeffType = 0; // RV
        test = false;
      }
      // Left Ventricle -- Endo
      else if ( ((dist >= -1.0) && (dist <= /*0.0*/ -0.1)) || Tools::equal(dist,20.0) ) {
        coeffType = 1; // Endo
        test = false;
      }
      // Left Ventricle -- Mcells
      else if ( (dist > /*0.0*/ -0.1) && (dist <= /*0.33333*/ 0.0 ) ) {
        coeffType = 2; // MCell
        test = false;
      }
      // Left Ventricle -- Epi
      else if ( (dist > /*0.33333*/ 0.0) && (dist <= 1.0) ) {
        coeffType = 3; // Epi
        test = false;
      }
      // Right Ventricle
      else if ( Tools::equal(dist,100.0) || Tools::equal(dist,10.0) ) {
        coeffType = 0; // RV
        test = false;
      }
      // Right Ventricle
      else if ( Tools::equal(dist,-100.0) ) {
        coeffType = 0;
        test = false;
      }
      else if ( Tools::equal(dist,-200.0) ) { //atria (complete heart)
        coeffType = 4;
        test = false;
      }

      if (test) {
        std::cout << "WARNING ! \nPoint (" << x << "," << y << "," << z << ") has distance " << dist << " => MV coeffType not defined !\n";
      }
    }
    return coeffType;
  }

  int HeteroMVCoeff::operator() (double x, double y, double z, double dist, double t) const {
    return MVCoeff(x,y,z,dist,t);
  }
}
