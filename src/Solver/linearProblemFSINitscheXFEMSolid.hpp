//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _LinearProblemFSINitscheXFEMSolid_HPP
#define _LinearProblemFSINitscheXFEMSolid_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Solver/linearProblemFSINitscheXFEMFluid.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementField.hpp"
#include "DegreeOfFreedom/duplicateSupportDof.hpp"
#include "Solver/bdf.hpp"

namespace felisce {
  /*!
    \file linearProblemFSINitscheXFEMSolid.hpp
    \authors B. Fabreges
    \date February 2013
    \brief Matrix and rhs for the elastic std::string solver.
  */

  template<class SolidPrb> class LinearProblemFSINitscheXFEMSolid:
    public SolidPrb {
  public:
    LinearProblemFSINitscheXFEMSolid();
    ~LinearProblemFSINitscheXFEMSolid();

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;


    void initPerElementTypeBD(GeometricMeshRegion::ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, const std::vector<Point*>& elemNormal, const std::vector< std::vector<Point*> >& elemTangent, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void setDuplicateSupportObject(DuplicateSupportDof* object);

    void setFluidLinPrb(LinearProblemFSINitscheXFEMFluid* pLinPrb);

    void setFluidFiniteElement(CurrentFiniteElement* velFE, CurrentFiniteElement* preFE);

    void setUserBoundaryCondition();

    void gatherSeqLastTimeSol(PetscVector& parStrucLastSol);
    void gatherSeqLastTimeVelSol(PetscVector& parStrucLastVelSol);
    void gatherStabExplicitFluidExtrapol(std::vector<PetscVector>& parFluidExtrapol);
    void gatherStabExplicitFluidExtrapol(PetscVector& parFluidExtrapol);
    void gatherRNStructExtrapol(PetscVector& parRNStrucExtrapol);
    void setStabExplicitFluidExtrapol(PetscVector& seqFluidExtrapol);
    void initStabExplicitFluidExtrapol(felInt order);

    void isAssemblingOfSolidProblemTerms(bool isit);

    void isStrucOperatorExplicitlyComputed(bool isit);
    inline bool isStrucOperatorExplicitlyComputed() {
      return m_isStrucOperatorExplicitlyComputed;
    }

    void deleteDynamicData();

    void setUseCurrentFluidSolution(bool useCurFluidSol) { m_useCurrentFluidSolution = useCurFluidSol;}
    bool getUseCurrentFluidSolution() { return m_useCurrentFluidSolution;}

    double computeL2Norm(PetscVector& inputVec);

    /**
     * @brief Assembly loop on elements pre-defined as crack if crack model
     */ 
    virtual void userComputeCrackStress() {};

  protected:
    virtual void userSetDirichletBC();

    felInt m_leftSide;         // 0 (default) to add the contribution of the left side, 1 to remove it.
    felInt m_rightSide;        // 0 (default) to add the contribution of the right side, 1 to remove it.

    // to std::set the DIrichlet BC for extremeties of the solid that are not front points
    std::vector<std::vector<double> >m_valBC;
    std::vector<bool> m_applyDirichletBC;

  private:
    // functions
    void updateFluidVel(felInt ielSupportDof, const PetscVector& fluidSol, felInt iold=0);
    void computeIntegrationPoints(std::vector<Point*>& ptElem);
    void computeFluidVelOnSubStructure(const std::vector<double>& strucNormal);
    void computeFluidNormalDerivativeJump(double sideCoeff, const std::vector<double>& strucNormal, const std::vector<double>& strucNormalInit);

    void assembleNitscheXFEMTerms(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS);

    // variables
    double m_massVel;
    felInt m_numSolidComponent;

    ElementField m_dispLastSol;
    ElementField m_elemNitscheXFEMSolidRHS;

    std::vector<ElementField> m_fluidDofSol;

    DuplicateSupportDof* m_duplicateSupportElements;
    LinearProblemFSINitscheXFEMFluid* m_pLinFluid;

    std::vector<std::vector<felInt> > m_idDofBCVertices;

    CurrentFiniteElement *m_feFluid;
    CurrentFiniteElement *m_fePres;

    std::vector<Point> m_intPoints;

    bool m_isSeqLastTimeSolAllocated;
    PetscVector m_seqLastTimeSol;

    bool m_isSeqLastTimeVelSolAllocated;
    PetscVector m_seqLastTimeVelSol;

    bool m_isSeqStabExplicitFluidExtrapolAllocated;
    std::vector<PetscVector> m_seqStabExplicitFluidExtrapol;

    bool m_isSeqRNStrucExtrapolAllocated;
    PetscVector m_seqRNStrucExtrapol;

    ElementFieldDynamicValue *m_elemFieldTimeSchemeType;
    felInt m_timeSchemeType;

    std::size_t m_numVerPerSolidElt;
    std::vector<Point*> m_subItfPts;

    bool m_assemblingOfSolidProblemTerms;

    felInt m_useCurrentFluidSolution;

    bool m_isStrucOperatorExplicitlyComputed;

    std::vector<double> m_betaRN;

    PetscVector m_seqVecL2Norm;

    felInt m_iDisplacement;
  };
}

#include "linearProblemFSINitscheXFEMSolid.tpp"

#endif
