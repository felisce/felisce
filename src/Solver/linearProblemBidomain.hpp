//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: A. Collin
//

#ifndef _LinearProblemBidomain_HPP
#define _LinearProblemBidomain_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacFunction.hpp"
#include "PETScInterface/romPETSC.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "FiniteElement/elementMatrix.hpp"

#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce 
{
/*!
  \class LinearProblemBidomain
  \authors A. Collin
  */
class LinearProblemBidomain:
  public LinearProblem {
public:
  LinearProblemBidomain();
  ~LinearProblemBidomain() override;
  void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
  void getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber);
  void getAngle(felInt iel, int iUnknown, std::vector<double>& elemAngle);
  void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
    m_currentLabel=label;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
  }
  void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  void computeElementArraySurfaceModel(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

  double conductivityHet(int currentLabel);
  double conductivityHom(int currentLabel);

  void initializeTimeScheme(Bdf* bdf) override {
    m_bdf = bdf;
  }
  void readData(IO& io) override;
  void readDataDisplacement(std::vector<IO::Pointer>& io, double iteration);
  void updateFibers(IO& io,double iteration);
  void writeEnsightScalar(double* solValue, int idIter, std::string varName);
  void writeEnsightCase(int numIt, std::string varName);
  void writeEnsightCaseCurrents(int numIt, std::vector<std::string> varName);


  void writeSolution(int rank, std::vector<IO::Pointer>& io, double& time, int iteration) override;

  void addMatrixRHS() override;

  std::vector<double> & EndocardiumDistance() {
    return m_vectorDistance;
  }

  void initializeROM(RomPETSC* rom) {
    m_rom = rom;
  }

  virtual void sortSolution() {}

  double* sortedSolution() {
    return m_sortedSol;
  }

  template <class Templ_functor> void evalFunctionOnElem(const Templ_functor& func, double& value, int& label);

  std::vector<double> & Reference() {
    return m_vectorRef;
  }

  void readMatch();
  void readElectrodeMeasure();
  void addElectrodeCondtrol();

  virtual void fillValueToImpose(double* /*valueToImpose*/) {}

  virtual void userComputeMass(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/){};

  void assembleOnlyMassMatrix(PetscMatrix& mass);
  void initAndStoreMassMatrix();
  PetscMatrix massMatrix(){ return m_massMatrix;}
  virtual void removeAverageForPotExtraCell(){};
  virtual void applyUserDefinedStimulation(double /*time*/){};
  virtual void initMassBoundaryForStimulation(){};
  void initMassBoundaryForCVG();

  inline std::vector<double> & Displacement() { return m_vectorDisp; }

  #ifdef FELISCE_WITH_CVGRAPH
  void massMatrixComputer(felInt ielSupportDof);
  void initPerETMass();
  void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&);
  #endif
  
protected:
  RomPETSC* m_rom;
  CurrentFiniteElement* m_fePotTransMemb;
  CurrentFiniteElement* m_fePotExtraCell;
  CurvilinearFiniteElement* m_fePotTransMembCurv;
  CurvilinearFiniteElement* m_fePotExtraCellCurv;
  felInt m_ipotTransMemb;
  felInt m_ipotExtraCell;
  double* m_sortedSol;
  CurvilinearFiniteElement* m_curvFe;
private:
  Bdf* m_bdf;
  std::vector<double> m_vectorFiber;
  std::vector<double> m_vectorAngle;
  std::vector<double> m_vectorDistance;
  std::vector<double> m_vectorRef;
  std::vector<double> m_vectorDisp;
  bool m_initializeCVGraphInterface;
  felInt* m_electrodeNode;
  felInt m_numElectrodeNode;
  double** m_electrodeMeasure;
  PetscVector m_electrodeControl;
  PetscMatrix m_massMatrix;


#ifdef FELISCE_WITH_CVGRAPH
public:
  void readData() override;
  void sendData();
  void addCurrentToRHS();
  virtual void cvgAddRobinToRHS();
#endif

};

template <class Templ_functor> void LinearProblemBidomain::evalFunctionOnElem(const Templ_functor& func, double& value, int& label) {
  value = func(label);
}

}

#endif
