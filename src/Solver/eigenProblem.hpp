//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _EIGENPROBLEM_HPP
#define _EIGENPROBLEM_HPP

// System includes
#include <vector>
#include <set>
#include <unordered_map>
#include <ostream>

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#include <parmetis.h>
#include "Core/NoThirdPartyWarning/Mpi/mpi.hpp"
#include "Core/NoThirdPartyWarning/Petsc/mat.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ksp.hpp"
#include "Core/NoThirdPartyWarning/Petsc/pc.hpp"
#include "Core/NoThirdPartyWarning/Petsc/error.hpp"
#ifdef FELISCE_WITH_SLEPC
#include "Core/NoThirdPartyWarning/Slepc/slepceps.hpp"
#include "Core/NoThirdPartyWarning/Slepc/slepcsvd.hpp"
#endif

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/geometricFaces.hpp"
#include "DegreeOfFreedom/listVariable.hpp"
#include "DegreeOfFreedom/supportDofMesh.hpp"
#include "DegreeOfFreedom/dof.hpp"
#include "DegreeOfFreedom/boundaryConditionList.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "InputOutput/io.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/listCurrentFiniteElement.hpp"
#include "FiniteElement/currentFiniteElement.hpp"
#include "FiniteElement/listCurvilinearFiniteElement.hpp"
#include "FiniteElement/currentFiniteElementWithBd.hpp"
#include "Core/chrono.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "PETScInterface/petscVector.hpp"

/*
 WARNING !
 EigenProblem class files need slepc library to work properly.
 */

namespace felisce 
{
/*!
  \class EigenProblem
  \authors E. Schenone
  \date 10/12/2012
  \brief Manage general functions of an eigenvalues problem.
  */
class EigenProblem
{
public:
  typedef GeometricMeshRegion::ElementType ElementType;

  // Constructor
  //============
  EigenProblem();
  
  virtual ~EigenProblem();

  void initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm);

  void fixIdOfTheProblemSolver(const int i) {
    m_idProblem = i;
  }

  //Define Physical Variable associate to the problem
  //=================================================
  void definePhysicalVariable(const std::vector<PhysicalVariable>& listVariable, const std::vector<std::size_t>& listNumComp);
  // Compute Dof
  //============
  /*!
  \brief compute degrees of freedom with support dof creation.
  Build pattern of the matrix.
  */
  void computeDof(int numProc, int idProc);
  //! for one element and a local suppordof id, find coordniate of support.
  void getSupportCoordinate(felInt idElt, felInt idSupport, int iUnknown, Point& pt);
  // Partition function
  //===================

  /*!
   \brief After partitionning we obtain element repartition and we create mesh region and "region" support dof.
  */
  void cutMesh();
  /*!
  \brief use ParMetis to partition dof (line of the matrix).
  */
  void partitionDof(idx_t numPart, int rankPart, MPI_Comm comm);

  // Build Mesh and SupportDofMesh local with partition arrays
  //==========================================================
  /*!
    \brief Create the local mesh and local supportDofMesh-> Create the local to global mapping for the elements
    One element is on rankPart if the majority of its dof is in the process.
    All points of the mesh are duplicated.
    \param[in] rankPart Rank of the current process.
  */
  void setLocalMeshAndSupportDofMesh(int rankPart);

  // Allocate Matrix
  //========================
  /*!
  \brief use the pattern define with dof to allocate memory for the matrix in CSR format.
  */
  void allocateMatrix();
  void allocateSequentialSolution();

  // Assemble Matrix
  //================
  //! Asssembly loop on domain element with current finite element. Ths function call following functions.
  /*!
  \brief elementary loop to build matrix.
  This function uses specific functions which are define in derived problem classes.
  */
  void assembleMatrix();
  void allocateArrayForAssembleMatrix();
  void desallocateArrayForAssembleMatrix();
  void setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt* ielSupportDof);
  void setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, std::vector<felInt>& vectorSupport);
  /*!
  \brief use elementary calculus to fill the global matrix.
  */
  void setValueMatrix(const felInt* ielSupportDof);
  //! Functions use to compute elementary operator for current finite element.
  //! Initialize element array and elemField object with currentFiniteElement object.
  void defineFiniteElement(const ElementType& eltType);
  void initElementArray();
  virtual void initPerElementType() {}

  virtual void initPerDomain(int label) {
    m_currentLabel = label;
  }


  //! Update finite element with element vertices coordinates and compute operators.
  virtual void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                    felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) {
    IGNORE_UNUSED_ELEM_POINT;
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_IEL;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
  }
  virtual void computeElementArrayBD(const std::vector<Point*>&, const std::vector<felInt>&, felInt&) {}

  //! Asssembly loop on domain element with curvilinear finite element. This function calls following functions.
  /*!
  \brief elementary loop to build matrix.
  This function uses specific functions which are define in derived problem classes.
  */
  void assembleMatrixBD();
  //! Functions use to compute elementary operator for curvilinear finite element.
  //! Initialize element array and elemField object with curvilinearFiniteElement object.
  void defineCurvilinearFiniteElement(const ElementType& eltType);
  void initElementArrayBD();
  virtual void userElementInitBD() {}
  virtual void initPerElementTypeBD() {}

  //! Initiallize some paramater associate to a mesh region.
  virtual void initPerDomainBD(int label) {
    m_currentLabel = label;
  }

  //! Fill global matrix with elementary values.
  /*!
  \brief use elementary calculus to fill the global matrix.
  */
  void setValueMatrixBD(const felInt* ielSupportDof);
  //!Allocate memory for assemble matrix and rhs.
  void allocateArrayForAssembleMatrixBD();


  //Vector and Matrix operators
  //===========================

  // m_Matrix[i] = coef*m_Matrix[i]
  void scaleMatrix(felReal coef, int i=0);

  // M = coef*M
  void scaleMatrix(felReal coef, PetscMatrix& M);

  // Print solution of the system solve.
  void printSolution(int verbose = 0) const;

  //Write in file matrix.m, matrix in matlab format.
  void writeMatrixForMatlab(int verbose = 0) const;

  //Write in file filename, matrix in matlab format.
  void writeMatrixForMatlab(std::string const& fileName, PetscMatrix& matrix) const;

  //Write in file filenam, std::vector in matlab format.
  void writeVectorForMatlab(std::string const& fileName, PetscVector& vector) const;

  // Print a Petsc Matrix.
  void printMatrix(PetscMatrix& M);

  // Print a Petsc Vector.
  void printVector(PetscVector& V);

  //Solve linear system
  //===================

  /*!
    \brief solve the linear solver with Petsc methods.
  */
  virtual void buildSolver();
  void solve();

  virtual void buildSVD();
  void solveSVD();


  //Write solution
  //==============
  void fromVecToDoubleStar(double* solution, PetscVector& sol, int rank, int dimension, felInt size=0);
  void fromDoubleStarToVec(double* solution, PetscVector& sol, felInt size);
  void writeSolution(int rank, IO& io, double& time, int iteration);
  void writeEnsightVector(double* solValue, int idIter, std::string varName);
  void writeEnsightCase(const int numIt, const double dt, std::string varName, const double time0=0.);
  void writeEnsightCase(const int numIt, const double dt, std::vector<std::string> varName, const double time0=0.);

  /*!
  \brief write mesh to read solution with specific mesh (P2, Q2,...) .
  */
  void writeGeoForUnknown(int iUnknown);

  //void initTime(double& time,double& timeStep){ m_time = time; m_timeStep = timeStep;}
  void clearMatrix(const int i=0);
  void gatherSolution();

  //Tools
  //=====
  virtual void readData(IO& io) {
    IGNORE_UNUSED_ARGUMENT(io);
  }

  //! get coodinate of support dof associate to the dof.
  void findCoordinateWithIdDof(felInt i, Point& pt);

  // Access Functions
  //=================
  inline const GeometricMeshRegion::Pointer mesh() const {
    return m_mesh;
  }
  inline GeometricMeshRegion::Pointer mesh() {
    return m_mesh;
  }

  inline const GeometricMeshRegion::Pointer meshLocal() const {
    return m_meshLocal;
  }
  inline GeometricMeshRegion::Pointer meshLocal() {
    return m_meshLocal;
  }

  inline const std::vector<SupportDofMesh> & supportDofUnknownLocal() const {
    return m_supportDofUnknownLocal;
  }
  inline std::vector<SupportDofMesh> & supportDofUnknownLocal() {
    return m_supportDofUnknownLocal;
  }

  inline const ISLocalToGlobalMapping & mappingElem() const {
    return m_mappingElem;
  }
  inline ISLocalToGlobalMapping & mappingElem() {
    return m_mappingElem;
  }

  inline const ListVariable & listVariable() const {
    return m_listVariable;
  }
  inline ListVariable & listVariable() {
    return m_listVariable;
  }

  inline const ListUnknown & listUnknown() const {
    return m_listUnknown;
  }
  inline ListUnknown & listUnknown() {
    return m_listUnknown;
  }

  inline const Dof & dof() const {
    return m_dof;
  }
  inline Dof & dof() {
    return m_dof;
  }

  inline const AO & ao() const {
    return m_ao;
  }
  inline AO & ao() {
    return m_ao;
  }

  inline const felInt & numDof() const {
    return m_numDof;
  }
  inline felInt & numDof() {
    return m_numDof;
  }

  inline const felInt & numDofLocal() const {
    return m_numDofLocal;
  }
  inline felInt & numDofLocal() {
    return m_numDofLocal;
  }


  inline const felInt & numDofPerUnknown(felInt i) const {
    return m_numDofUnknown[i];
  }
  inline felInt & numDofPerUnknown(felInt i) {
    return m_numDofUnknown[i];
  }

  inline felInt & numDof(PhysicalVariable unknown) {
    int iUnknown = m_listUnknown.getUnknownIdList(unknown);
    return m_numDofUnknown[iUnknown];
  }

  inline felInt & numDofLocalPerUnknown(PhysicalVariable unknown) {
    int iUnknown = m_listUnknown.getUnknownIdList(unknown);
    return m_numDofLocalUnknown[iUnknown];
  }

  inline ISLocalToGlobalMapping & mappingDofLocalToDofGlobal(PhysicalVariable unknown) {
    int iUnknown = m_listUnknown.getUnknownIdList(unknown);
    return m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown];
  }


  inline const PetscVector&  solution() const {
    return m_sol;
  }
  inline PetscVector&  solution() {
    return m_sol;
  }

  inline const PetscMatrix& Matrix(const int i=0) const {
    return m_Matrix[i];
  }
  inline PetscMatrix& Matrix(const int i=0) {
    return m_Matrix[i];
  }

  inline const ListCurrentFiniteElement & listCurrentFiniteElement() const {
    return m_listCurrentFiniteElement;
  }
  inline ListCurrentFiniteElement & listCurrentFiniteElement() {
    return m_listCurrentFiniteElement;
  }

  inline const PetscVector&  seqSolution() const {
    return m_seqSol;
  }
  inline PetscVector&  seqSolution() {
    return m_seqSol;
  }

  inline int dimension() {
    return m_mesh->domainDim();
  }

protected:
  MPI_Comm m_petscComm;
  PetscVector nullPetscVector;
#ifdef FELISCE_WITH_SLEPC
  EPS m_eps;
  SVD m_svd;
#endif
  FelisceTransient::Pointer m_fstransient = nullptr;
  //! input mesh which define domain of work.
  GeometricMeshRegion::Pointer m_mesh = nullptr;
  int m_idProblem = 0;
  int m_verbose = 0;
  // Number of matrix use in formulation.
  int m_numberOfMatrix = 1;
  //! small region of the global domain, obtain with a decomposition of the degrees of freedom.
  GeometricMeshRegion::Pointer m_meshLocal = nullptr;

  //! global information about support of the degrees of freedom.
  std::vector<SupportDofMesh> m_supportDofUnknown;
  //! specific information about support of the degrees of freedom for the local region of work for a processor.
  std::vector<SupportDofMesh> m_supportDofUnknownLocal;
  //! object which contains unknown of the linear system.
  ListUnknown m_listUnknown;
  //! object which contains all variable in the problem.
  ListVariable m_listVariable;
  //! array which contains degree of freedom repartition (size = numDof, value = rank of the processor which keep in memory the dof).
  std::vector<int> m_dofPart;
  //! array which contains element repartition (size = numDof, value = rank of the processor which keep in memory the element).
  std::vector<int> m_eltPart;
  //! object which manage degrees of freedom.
  Dof m_dof;
  //! solution of the problem.
  PetscVector m_sol;
  //! the whole solution of the problem gathered in the processor
  PetscVector m_seqSol;
  //! tells whenever sequential solution is allocated or not
  bool m_seqSolAllocated = 0;
  //! pointer to external vectors (e.g. vectors defined in another linearProblem)
  std::vector<PetscVector*> m_externalVec;
  std::vector<AO> m_externalAO;
  std::vector<Dof*> m_externalDof;
  //! number of dof contains by the current processor.
  felInt m_numDofLocal = 0;
  //! number of dof in the problem.
  felInt m_numDof = 0;
  //! number of dof for unknown
  felInt* m_numDofUnknown = nullptr;
  //! number of dof local for unknown
  felInt* m_numDofLocalUnknown = nullptr;
  //! mapping between global problem numbering and matrix dof numbering (specific to Petsc utilisation).
  AO m_ao;

  //! mapping between local to global ordering of dof.
  ISLocalToGlobalMapping m_mappingNodes;
  //! mapping between local to global ordering of element.
  ISLocalToGlobalMapping m_mappingElem;
  //! mapping between local to global ordering of element.
  std::vector<ISLocalToGlobalMapping> m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc;

  //! list of finite element in the problem.
  ListCurrentFiniteElement m_listCurrentFiniteElement;

  //! list of curvilinear finite element in the problem.
  ListCurvilinearFiniteElement m_listCurvilinearFiniteElement;

  int m_currentLabel;

  //!Element matrix.
  std::vector<ElementMatrix::Pointer> m_elementMat;
  //!Element matrix boundary element.
  std::vector<ElementMatrix::Pointer> m_elementMatBD;
  std::vector<PetscMatrix> m_Matrix;

  //!Temporary arrays use to assemble matrix
  felInt* m_GposLine = nullptr;
  felInt* m_GposColumn = nullptr;
  std::vector<double*> m_valueMat;
  //!Temporary arrays use to assemble matrix
  felInt* m_GposVec = nullptr;
  double* _GblockVec = nullptr;
  felInt* m_ia = nullptr;

  std::vector<felInt> m_localSizeOfVector;
  felInt m_shiftValue = 0;
  bool m_initNumDofUnknown = false;
  bool m_buildSystem = false;
  bool m_buildSolver = false;

  std::string nameOfTheProblem = "No-name";

  void pushBackExternalVec(PetscVector& extVec) {
    m_externalVec.push_back(&extVec);
  }
  void pushBackExternalAO(AO extAO) {
    m_externalAO.push_back(extAO);
  }
  void pushBackExternalDof(Dof& extDof) {
    m_externalDof.push_back(&extDof);
  }
};
}

#endif
