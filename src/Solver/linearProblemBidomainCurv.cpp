//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: A. Collin
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemBidomainCurv.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"

namespace felisce 
{
LinearProblemBidomainCurv::LinearProblemBidomainCurv():
  LinearProblem("Problem cardiac Bidomain",2),
  m_sortedSol(nullptr),
  allocateSeqVec(false),
  allocateLevelSet(false),
  allocateSol_n_1(false),
  m_avHeavU0(0.),
  m_avHeavMinusU0(0.) {
  if (allocateSeqVec) {
    m_seqIon.destroy();
    m_seqBdfRHS.destroy();
  }
  if (allocateLevelSet) {
    m_levelSet.destroy();
    m_levelSetSeq.destroy();
  }
  if (allocateSol_n_1) {
    m_sol_n_1.destroy();
  }
}

LinearProblemBidomainCurv::~LinearProblemBidomainCurv() {
  delete [] m_sortedSol;
}

void LinearProblemBidomainCurv::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
  LinearProblem::initialize(mesh,comm, doUseSNES);
  m_fstransient = fstransient;
  
  std::vector<PhysicalVariable> listVariable(2);
  std::vector<std::size_t> listNumComp(2);
    
  listVariable[0] = potTransMemb;
  listNumComp[0] = 1;
  listVariable[1] = potExtraCell;
  listNumComp[1] = 1;
    
  //define unknown of the linear system.
  m_listUnknown.push_back(potTransMemb);
  m_listUnknown.push_back(potExtraCell);
  definePhysicalVariable(listVariable,listNumComp);
}

void LinearProblemBidomainCurv::readData(IO& io) {
  m_vectorFiber.resize(m_mesh[m_currentMesh]->numPoints()*3);
  io.readVariable(0, 0.,&m_vectorFiber[0], m_vectorFiber.size());
  m_vectorAngle.resize(m_mesh[m_currentMesh]->numPoints());
  io.readVariable(1, 0.,&m_vectorAngle[0],m_vectorAngle.size());
  if (!FelisceParam::instance().hasHeteroCondAtria) {
    m_vectorRef.resize(m_mesh[m_currentMesh]->numPoints()); //m_numDof;
    io.readVariable(2, 0.,&m_vectorRef[0],m_vectorRef.size());
  }
}

void LinearProblemBidomainCurv::readDataForDA(IO& io, double iteration) {
  m_data.resize(m_mesh[m_currentMesh]->numPoints());
  if (FelisceParam::instance().typeOfAppliedCurrent != "atria")
    io.readVariable(2, iteration*m_fstransient->timeStep, &m_data[0],m_data.size());
  else
    io.readVariable(3, iteration*m_fstransient->timeStep, &m_data[0],m_data.size());
}

void LinearProblemBidomainCurv::getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber) {
  //Warning: the fibers must be normalized.
  int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
  elemFiber.resize( numSupport*3,0.);
  for (int i = 0; i < numSupport; i++) {
    elemFiber[i*3] = m_vectorFiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
    elemFiber[i*3+1] = m_vectorFiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+1];
    elemFiber[i*3+2] = m_vectorFiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+2];
  }
}

void LinearProblemBidomainCurv::getAngle(felInt iel, int iUnknown, std::vector<double>& elemAngle) {
  int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
  elemAngle.resize(numSupport,0.);
  for (int i = 0; i < numSupport; i++) {
    elemAngle[i] = m_vectorAngle[(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
  }
}

void LinearProblemBidomainCurv::getData(felInt iel, int iUnknown, std::vector<double>& elemData) {
  //double& vGate = FelisceParam::instance().vGate;
  double vGate = -67.;
  int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
  elemData.clear();
  elemData.resize(numSupport,0.);
  for (int i = 0; i < numSupport; i++) {
    if (m_data[(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])] > vGate) {
      elemData[i] = 1.0;
    } else
      elemData[i] = -1.0;
  }
}

void LinearProblemBidomainCurv::initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  IGNORE_UNUSED_ELT_TYPE;
  //Init pointer on Finite Element, Variable or idVariable
  m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
  m_ipotExtraCell = m_listVariable.getVariableIdList(potExtraCell);
  m_fePotTransMemb = m_listCurvilinearFiniteElement[m_ipotTransMemb];
  m_fePotExtraCell = m_fePotTransMemb;
  if (FelisceParam::instance().dataAssimilation) {
    m_elemFieldVm.initialize(DOF_FIELD,*m_fePotTransMemb,1);
    m_elemFieldLSVm.initialize(DOF_FIELD,*m_fePotTransMemb,1);
    m_avHeavU0 = averageLS(m_fePotTransMemb,m_ipotTransMemb,0);
    m_avHeavMinusU0 = averageLS(m_fePotTransMemb,m_ipotTransMemb,1);
  }
}

double LinearProblemBidomainCurv::conductivityHom(int currentLabel) {
  double conductivity = 1.0;

  if (FelisceParam::instance().hasHeteroCondAtria) {
    if (std::fabs(currentLabel - 31.0) < 1.0e-12) { //SN
      conductivity = 0.3;
    } else if (std::fabs(currentLabel - 32.0) < 1.0e-12) { //CT
      conductivity = 3.0;
    } else if (std::fabs(currentLabel - 33.0) < 1.0e-12) { //BB
      conductivity = 4.75;
    } else if (std::fabs(currentLabel - 34.0) < 1.0e-12) { //RAI
      conductivity = 0.375;
    } else if (std::fabs(currentLabel - 35.0) < 1.0e-12) { //PM
      conductivity = 1.5;
    } else { //regular atria and F0
      conductivity = 1.0;
    }
  }

  return conductivity;
}

double LinearProblemBidomainCurv::conductivityHet(int currentLabel) {
  double conductivity = 1.0;

  if (FelisceParam::instance().hasHeteroCondAtria) {
    if (std::fabs(currentLabel - 31.0) < 1.0e-12) { //SN
      conductivity = 0.36;
    } else if (std::fabs(currentLabel - 32.0) < 1.0e-12) { //CT
      conductivity = 4.5;
    } else if (std::fabs(currentLabel - 33.0) < 1.0e-12) { //BB
      conductivity = 7.75;
    } else if (std::fabs(currentLabel - 34.0) < 1.0e-12) { //RAI
      conductivity = 0.45;
    } else if (std::fabs(currentLabel - 35.0) < 1.0e-12) { //PM
      conductivity = 1.8;
    } else if (std::fabs(currentLabel - 36.0) < 1.0e-12) { //F0
      conductivity = 0.9;
    } else { //regular atria
      conductivity = 1.0;
    }
  }

  return conductivity;
}

void LinearProblemBidomainCurv::initLevelSet() {
  m_levelSet.duplicateFrom(solution());

  m_levelSet.zeroEntries();
  m_levelSet.assembly();

  m_levelSetSeq.createSeq(PETSC_COMM_SELF,m_numDof);
  m_levelSetSeq.set(0.0);

  allocateLevelSet = true;
}

void LinearProblemBidomainCurv::levelSet(double min, double max) {
  //double& vGate = FelisceParam::instance().vGate;
  double vGate = -67.;

  felInt pos;
  double value_sol;
  double value_levelSet;

  for (felInt i = 0; i < numDofLocalPerUnknown(potTransMemb) ; i++) {
    ISLocalToGlobalMappingApply(mappingDofLocalToDofGlobal(potTransMemb),1,&i,&pos);
    solution().getValues(1,&pos,&value_sol);
    if( value_sol > vGate) {
      value_levelSet = max;
    } else if( value_sol < vGate) {
      value_levelSet = min;
    } else
      value_levelSet = 0.0;

    m_levelSet.setValue(pos,value_levelSet, INSERT_VALUES);
  }
  m_levelSet.assembly();

  gatherVector(m_levelSet, m_levelSetSeq);
}

void LinearProblemBidomainCurv::initSolutionTimeBef() {
  m_sol_n_1.duplicateFrom(solution());
  m_sol_n_1.copyFrom(solution());

  allocateSol_n_1 = true;
}

void LinearProblemBidomainCurv::solutionTimeBef() {
  m_sol_n_1.copyFrom(solution());
}

double LinearProblemBidomainCurv::averageLS(CurvilinearFiniteElement* fePtr, int idfe, int minus) {
  FEL_ASSERT(fePtr)
  CurvilinearFiniteElement& fe = *fePtr;

  Variable variablePotTransMemb = m_listVariable[idfe];

  int numComp = variablePotTransMemb.numComponent();
  // dofValue contains : values in dof by element.
  double dofValueSeqSol[fe.numDof()*numComp];

  // idGlobalDof contains: global number od dof by element.
  felInt idGlobalDof[fe.numDof()*numComp];

  //geometric element type in the mesh.
  ElementType eltType;
  //number of points per geometric element.
  int numPointPerElt = 0;
  //number of element for one label and one eltType.
  felInt numEltPerLabel = 0;
  //Points of the current element.
  std::vector<Point*> elemPoint;
  //Id of points of the element.
  std::vector<felInt> elemIdPoint;

  // use to identify global id of dof.
  felInt idDof;
  felInt cpt = 0;
  double integralValue = 0.;
  double heavisideMeasure = 0.;
  double elementValueHeav = 0.;
  double elementValueHeavU0 = 0.;
  //Id link element to its supportDof.
  felInt ielSupportDof;

  double vGate = -67.;

  // initialize global numbering from mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // Loop on elementType.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt,0);
    // Loop on region define in the mesh.

    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin();
          itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      numEltPerLabel = itRef->second.second;

      // Loop on elements.
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // get points of element.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);
        // compute measure of the current element.
        fe.updateMeas(0, elemPoint);

        cpt = 0;
        // identify global id of Dof for the current element.
        for(int iComp = 0; iComp<numComp; iComp++) {
          for(int iDof=0; iDof<fe.numDof(); iDof++) {
            dof().loc2glob(ielSupportDof,iDof,m_ipotTransMemb,iComp,idDof);
            idGlobalDof[cpt] = idDof;
            cpt++;
          }
        }

        std::vector<double> dofValueData;
        getData(ielSupportDof, m_ipotTransMemb, dofValueData);

        //mapping to obtain new global numbering. (I/O array: idGlobalDof).
        AOApplicationToPetsc(m_ao,fe.numDof()*numComp,idGlobalDof);
        //gets values of associate dofs.

        this->sequentialSolution().getValues(fe.numDof()*numComp,idGlobalDof,dofValueSeqSol);

        //Interpolation
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          elementValueHeavU0 = 0.;
          elementValueHeav = 0.;
          cpt = 0;
          for(int icomp=0; icomp<numComp; icomp++) {
            for(int j=0; j<fe.numDof(); j++) {
              if (minus == 1) {
                if (dofValueSeqSol[cpt] < vGate) {
                  elementValueHeavU0 +=  fe.phi[ig](j) * 2. * dofValueData[cpt];
                  elementValueHeav +=  fe.phi[ig](j) * 2.;
                }
              } else {
                if (dofValueSeqSol[cpt] > vGate) {
                  elementValueHeavU0 +=  fe.phi[ig](j) * 2. * dofValueData[cpt];
                  elementValueHeav +=  fe.phi[ig](j) * 2.;
                }
              }

              cpt++;
            }
            integralValue += elementValueHeavU0 * fe.weightMeas(ig);
            heavisideMeasure += elementValueHeav * fe.weightMeas(ig);
          }
        }
        numElement[eltType]++;
      }
    }
  }

  double valueReceive = 0.;
  MPI_Allreduce(&integralValue,&valueReceive, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  integralValue = valueReceive;
  valueReceive = 0.;
  MPI_Allreduce(&heavisideMeasure,&valueReceive, 1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
  heavisideMeasure = valueReceive;

  return integralValue/(heavisideMeasure+0.000000001);
}


void LinearProblemBidomainCurv::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_ELEM_ID_POINT;

  m_fePotTransMemb->updateMeasNormalContra(0, elemPoint);

  if (flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    if (FelisceParam::instance().dataAssimilation) {
      std::vector<double> elemData;
      getData(iel,m_ipotTransMemb,elemData);

      m_elemFieldVm.setValue(this->sequentialSolution(), *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, dof());

      m_elemFieldLSVm.setValue(m_levelSetSeq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, dof());

      m_elementVectorBD[0]->grad_u_dot_grad_LSu_v(1.0, m_elemFieldVm, m_elemFieldLSVm, elemData, m_avHeavU0, m_avHeavMinusU0, FelisceParam::instance().insidePar, FelisceParam::instance().outsidePar, *m_fePotTransMemb,0);
    }
  } else {
    // Assembling matrix(0)
    const double coefTime = 1./m_fstransient->timeStep;
    const double coefAm = FelisceParam::instance().Am;
    const double coefCm = FelisceParam::instance().Cm;
    const double coef = m_bdf->coeffDeriv0()*coefTime*coefAm*coefCm;

    //1st equation : Am*Cm/dt V_m

    m_elementMatBD[0]->phi_i_phi_j(coef,*m_fePotTransMemb,0,0,1);

    if (FelisceParam::instance().testCase == 1) {
      std::vector<double> elemFiber;
      getFiberDirection(iel,m_ipotTransMemb,elemFiber);

      std::vector<double> elemAngle;
      getAngle(iel,m_ipotTransMemb,elemAngle);

      //1st equation : \sigma_i^t \grad V_m
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor*conductivityHom(m_currentLabel),*m_fePotTransMemb,0,0,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotTransMemb,0,0,1);
      //m_elementMatBD[0]->tau_grad_phi_i_tau_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMemb,0,0,1);

      //1st equation : \sigma_i^t \grad u_e
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor*conductivityHom(m_currentLabel),*m_fePotExtraCell,0,1,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotExtraCell,0,1,1);
      //m_elementMatBD[0]->tau_grad_phi_i_tau_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotExtraCell,0,1,1);

      //2nd equation : \sigma_i^t \grad V_m
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor*conductivityHom(m_currentLabel),*m_fePotTransMemb,1,0,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotTransMemb,1,0,1);
      //m_elementMatBD[0]->tau_grad_phi_i_tau_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMemb,1,0,1);

      //2nd equation :  \eps u_e
      double eps = 1.0e-6;
      m_elementMatBD[0]->phi_i_phi_j(eps,*m_fePotExtraCell,1,1,1);

      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      m_elementMatBD[0]->grad_phi_i_grad_phi_j((FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor)*conductivityHom(m_currentLabel),*m_fePotExtraCell,1,1,1);
      m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor + FelisceParam::instance().extraFiberTensor - FelisceParam::instance().extraTransvTensor)*conductivityHet(m_currentLabel),elemFiber,elemAngle,*m_fePotExtraCell,1,1,1);
      //m_elementMatBD[0]->tau_grad_phi_i_tau_grad_phi_j((FelisceParam::instance().intraFiberTensor - FelisceParam::instance().intraTransvTensor + FelisceParam::instance().extraFiberTensor - FelisceParam::instance().extraTransvTensor),elemFiber,*m_fePotExtraCell,1,1,1);
    } else {
      //1st equation : \sigma_i^t \grad V_m
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      //1st equation : \sigma_i^t \grad u_e
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotExtraCell,0,1,1);

      //2nd equation : \sigma_i^t \grad V_m
      m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,1,0,1);

      //2nd equation :  \eps u_e
      double eps = 1.0e-6;
      m_elementMatBD[0]->phi_i_phi_j(eps,*m_fePotExtraCell,1,1,1);

      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      m_elementMatBD[0]->grad_phi_i_grad_phi_j((FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor),*m_fePotExtraCell,1,1,1);

    }

    //Assembling matrix(1) = mass (only first quadrant if hasAppliedExteriorCurrent = false)
    m_elementMatBD[1]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);
    if (FelisceParam::instance().hasAppliedExteriorCurrent)
      m_elementMatBD[1]->phi_i_phi_j(1.,*m_fePotTransMemb,1,1,1);

    if (FelisceParam::instance().dataAssimilation) {
      std::vector<double> elemData;
      getData(iel,m_ipotTransMemb,elemData);
      m_elemFieldVm.setValue(this->sequentialSolution(), *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, dof());

      m_elemFieldLSVm.setValue(m_levelSetSeq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, dof());
      m_elementVectorBD[0]->grad_u_dot_grad_LSu_v(1.0, m_elemFieldVm, m_elemFieldLSVm, elemData, m_avHeavU0, m_avHeavMinusU0, FelisceParam::instance().insidePar, FelisceParam::instance().outsidePar, *m_fePotTransMemb,0);
    }
  }


}

void LinearProblemBidomainCurv::addMatrixRHS() {
  // _RHS = m_mass * _RHS.
  PetscVector tmpB;
  tmpB.duplicateFrom(vector());
  tmpB.copyFrom(vector());
  mult(matrix(1),tmpB,vector());
  tmpB.destroy();
}

void LinearProblemBidomainCurv::writeEnsightScalar(double* solValue, int idIter, std::string varName) {
  std::string iteration;
  if (idIter < 10)
    iteration = "0000" + std::to_string(idIter);
  else if (idIter < 100)
    iteration = "000" + std::to_string(idIter);
  else if (idIter < 1000)
    iteration = "00" + std::to_string(idIter);
  else if (idIter < 10000)
    iteration = "0" + std::to_string(idIter);
  else if (idIter < 100000)
    iteration = std::to_string(idIter);

  std::string fileName = FelisceParam::instance().resultDir + "/" + varName + "." + iteration + ".scl";
  FILE * solFile;
  solFile = fopen(fileName.c_str(),"w");
  fprintf(solFile, "Scalar per node\n");
  int count = 0;
  for(int j=0; j < static_cast<int>(m_numDof/2); j++) {
    fprintf(solFile,"%12.5e", solValue[j]);
    count++;
    if(count == 6) {
      fprintf(solFile,"\n");
      count = 0;
    }
  }
  fprintf(solFile,"\n");
  fclose(solFile);
}

void LinearProblemBidomainCurv::writeEnsightCase(int numIt, std::string varName) {

  FILE * pFile;
  std::string caseName = FelisceParam::instance().resultDir + "/" + varName + ".case";
  pFile = fopen(caseName.c_str(),"w");
  fprintf(pFile,"FORMAT\n");
  fprintf(pFile,"type: ensight\n");
  fprintf(pFile, "GEOMETRY\n");
  std::string nameGeo = "model: 1 " + FelisceParam::instance().outputMesh[0] + "\n";
  fprintf(pFile,"%s", nameGeo.c_str());
  fprintf(pFile, "VARIABLE\n");
  std::string secName = "scalar per node: 1 " + varName + " " + varName +".*****.scl\n";
  fprintf(pFile, "%s", secName.c_str());
  fprintf(pFile, "TIME\n");
  fprintf(pFile, "time std::set: %d \n", 1);
  fprintf(pFile, "number of steps: %d \n", numIt);
  fprintf(pFile, "filename start number: %d\n", 0);
  fprintf(pFile, "filename increment: %d\n", 1);
  fprintf(pFile, "time values:\n");

  const double dt = FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution;
  int count=0;
  for(int j=0; j < numIt; j++) {
    double valT = j*dt;
    fprintf(pFile,"%lf ",valT);
    count++;
    if(count == 6) {
      fprintf(pFile,"\n");
      count=0;
    }
  }
  fclose(pFile);
}
}
