//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _EIGENPROBLEMALPDEIM_HPP
#define _EIGENPROBLEMALPDEIM_HPP

// System includes

// External includes

// Project includes
#include "Solver/eigenProblemALP.hpp"

namespace felisce {
  /*!
   \class EigenProblemALPDeim
   \authors E. Schenone
   \date 23/07/2014
   \brief Manage functions of a ALP-DEIM eigenvalues problem and ALP-DEIM-ROM solver.
   */

  class EigenProblemALPDeim:
    public EigenProblemALP {
  public:
    // Constructor
    //============
    EigenProblemALPDeim();
    ~EigenProblemALPDeim() override;

    void initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm) override;

    // Assemble Matrix
    //================
    /// Update finite element with element vertices coordinates and compute operators.
    void initPerElementType() override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    // Projection functions from collocation points space to RO one and viceversa
    using EigenProblemALP::projectOnDof;
    using EigenProblemALP::projectOnBasis;
    void projectOnBasis(double* vectorDof, double* vectorBasis, bool flagMass=true);
    void projectOnDof(double* vectorBasis, double* vectorDof, int size);

    void readData(IO& io) override;
    void readCollocationPts();
    void initializeROM() override;
    void initializeSolution() override;

    using EigenProblemALP::MGS;
    void MGS(double** A, double** R, int size);
    void factorizationQR(double** A, double** Q, double** R, int sizeRow, int sizeCol);
    void inverseTriangular(double** A, double** Ainv, int sizeA);
    void computeMassDeim();

    void computeProjectionPiV();
    void writeEnsightSolution(const int iter) override;

    virtual void computeRHSDeim();
    void computeTheta();
    void computeMatrixM() override;
    void computeHatU();

    void computeTensor() override;
    void ConjGrad(double** A, double* b, double* x, double tol, int maxIter, int n, bool sym=false);

    void setHeteroTauClose(std::vector<double>& valueTauClose) override;
    virtual void setHeteroTauOut(std::vector<double>& valueTauOut);
    void setFhNf0(std::vector<double>& valuef0) override;
    void setIapp(std::vector<double>& iapp, int& idS) override;

    //Update functions
    void updateBasis() override;
    void setPotential() override;
    void updateBeta();
    void updateEigenvalue() override;

    void checkBasis(int size=0) override;
    void checkBasisCollocation();

    // Write an ECG
    void initializeECG() override {}
    void writeECG(int /*iteration*/) override {}
    void updateEcgOperator() override {}

    void readMeasureEcgFilter() override {}

  protected:
    bool m_resPoint;
    CurrentFiniteElement* m_fePotExtraCell;
    felInt m_ipotExtraCell;
    felInt m_dimCollocationPts;
    felInt* m_collocationNode;
    double** m_basisDeimColloc; // W
    double** m_basisDeim; // \Phi
    double** m_orthCompDeim; // \Psi
    int m_improvedRecType;
    double* m_hatU;
    double* m_hatW;
    double* m_hatUe;
    double* m_potential;
    double** m_hatBasis;
    double** m_theta;
    double** m_thetaOrth;
    double** m_massDeim;
    double** m_stiffDeim;
    double** m_invGK;
    double* m_resU;
    std::vector<PetscVector> m_projectorPiV;
    std::vector<PetscVector> m_basisGrad;
    double* m_hatF;
    double* m_hatG;
    double** m_matrixA;
    double** m_matrixB;
    double* m_sumG;
    double* m_coefFhNs;
    double* m_coefTauClose;
    double* m_coefTauOut;
    ElementField m_elemFieldUe0;
  };

}

#endif
