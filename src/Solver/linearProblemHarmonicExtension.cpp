//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Solver/linearProblemHarmonicExtension.hpp"
#include "InputOutput/io.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce {
  LinearProblemHarmonicExtension::LinearProblemHarmonicExtension():
    LinearProblem("Poisson equation"),
    m_buildTeporaryMatrix(false)
  {

  }

  LinearProblemHarmonicExtension::~LinearProblemHarmonicExtension() 
  {
    if(m_buildTeporaryMatrix)
      m_matrix.destroy();
  }

  void LinearProblemHarmonicExtension::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm,  bool doUseSNES) 
  {
    LinearProblem::initialize(mesh,comm,doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable(3);
    std::vector<std::size_t> listNumComp(3);

    listVariable[0] = velocity;
    listNumComp[0] = this->dimension();

    listVariable[1] = pressure;
    listNumComp[1] = 1;

    listVariable[2] = displacement;
    listNumComp[2] = this->dimension();

    //define unknown of the linear system.
    m_listUnknown.push_back(displacement);
    this->definePhysicalVariable(listVariable,listNumComp);
    m_iDisplacement = this->listVariable().getVariableIdList(displacement);

    const auto& r_felisce_param_instance = FelisceParam::instance();

    m_scaleCoeff = r_felisce_param_instance.scaleCoeff;

    if(r_felisce_param_instance.stiffening){
      initializeStiffnessVector();
    }
  }

  void LinearProblemHarmonicExtension::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) 
  {
    (void) eltType;
    (void) flagMatrixRHS;
    m_fe = m_listCurrentFiniteElement[m_iDisplacement];
  }

  void LinearProblemHarmonicExtension::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /* elemIdPoint */, felInt& iel, FlagMatrixRHS /* flagMatrixRHS */) 
  {
    const auto& r_felisce_param_instance = FelisceParam::instance();

    if ( r_felisce_param_instance.useElasticExtension ) {
      m_fe->updateFirstDeriv(0, elemPoint);
      // Elastic extension (à la Tezduyar).
      // Note: ( coef = 1/J -> to stiff small elements )

      double mu     = r_felisce_param_instance.mu_lame;
      double lambda = r_felisce_param_instance.lambda_lame;

      userStiffnessParameter(iel, mu, lambda);

      m_elementMat[0]->eps_phi_i_eps_phi_j(2*mu, *m_fe, 0, 0, this->dimension());
      m_elementMat[0]->div_phi_j_div_phi_i(lambda, *m_fe, 0, 0, this->dimension());

    } else if ( m_fstransient->iteration == 0 ) { // Check if this is really called? Because iteration begins at 1 for HarmonicExtension. (This-Boilevin, 2017).
      m_fe->updateFirstDeriv(0, elemPoint);
      // Harmonic extension
      m_elementMat[0]->grad_phi_i_grad_phi_j(1., *m_fe, 0, 0, this->dimension());
    }
  }

  void LinearProblemHarmonicExtension::copyMatrixRHS() {
    m_matrix.duplicateFrom(matrix(0),MAT_COPY_VALUES);
    m_matrix.assembly(MAT_FINAL_ASSEMBLY);
    m_buildTeporaryMatrix = true;
  }

  void LinearProblemHarmonicExtension::addMatrixRHS() {
    if ( ! FelisceParam::instance().useElasticExtension  )
      matrix(0).axpy(1,m_matrix,SAME_NONZERO_PATTERN);
  }

  void LinearProblemHarmonicExtension::userFinalizeEssBCTransient() {}


  //--------------------------------------------------------------------------------------
  void LinearProblemHarmonicExtension::userStiffnessParameter(felInt iel, double& mu, double& lambda)
  {
    
    if ( stiffenElementIds.size() > 0 && stiffenElementIds[iel] == 1 ){
      
      const auto& r_felisce_param_instance = FelisceParam::instance();

      if ( m_fstransient->iteration == 1 ){

        if ( r_felisce_param_instance.skipStiffeningFirstIteration == 0 ) {
          mu     = mu     / m_fe->measure() * r_felisce_param_instance.initStiff;
          lambda = lambda / m_fe->measure() * r_felisce_param_instance.initStiff;
        } else {
          mu     = mu     / m_fe->measure();
          lambda = lambda / m_fe->measure();
        }
      } else {
        mu     = mu     / m_fe->measure() * r_felisce_param_instance.finalStiff;
        lambda = lambda / m_fe->measure() * r_felisce_param_instance.finalStiff;
      }
    } else{
      mu     = mu     / m_fe->measure();
      lambda = lambda / m_fe->measure();
    }
  }

  void LinearProblemHarmonicExtension::readMatchFile_ALE(ListVariable , const std::string& fileName) {
    m_numDofBySupportToMatch = 0;
    //Read match file.
    FILE *pFile;
    pFile = fopen(fileName.c_str(),"r");
    if (pFile==nullptr) {
      FEL_WARNING("Impossible to read match file: "+fileName+".");
    } else {
      //Read supportDof in match file.
      if (! fscanf(pFile, "%d", &m_totNumNodeToMatch) )
        FEL_ERROR("totalNumberOfNode in interface not scanned in file.");
      if (!m_totNumNodeToMatch) {
        FEL_ERROR("Your match file is empty.\n");
      }
      m_listOfNodeToMatch = new felInt[m_totNumNodeToMatch];
      felInt id = 0;
      for (felInt indc = 0; indc < m_totNumNodeToMatch; indc++) {
        if (! fscanf(pFile, "%d", &id) )
          FEL_ERROR("id of interface supportDof not scanned in file.");
        m_listOfNodeToMatch[indc] = id - 1;
      }

      m_numDofBySupportToMatch = this->listVariable()[m_iDisplacement].numComponent();
      m_totNumDofToMatch = m_numDofBySupportToMatch * m_totNumNodeToMatch;
      m_vectorDisp.resize(m_totNumDofToMatch, 0.);
      m_listOfDofToMatch.resize(m_totNumDofToMatch, 0.);

    }
    fclose(pFile);
  }

  void LinearProblemHarmonicExtension::identifyIdDofToMatch_ALE(Dof&) 
  {
    std::vector<felInt> idDofBySupport;

    for(felInt isd = 0; isd<m_totNumNodeToMatch; isd++) {
      dof().supportDofToDof(m_listOfNodeToMatch[isd], idDofBySupport);

      for (std::size_t iComp = 0; iComp < this->listVariable()[m_iDisplacement].numComponent(); iComp++) {
        m_listOfDofToMatch[ isd * this->listVariable()[m_iDisplacement].numComponent() + iComp ] = idDofBySupport[ iComp ];
      }
      idDofBySupport.clear();
    }
  }


  //Author : Alexandre This
  //Date : 21 July 2016
  //This function could/should maybe be  renamed. Its purpose is not only to read displacement but is also used to read velocity.
  //The use of this function to read velocity predates this comment and the accompanying commit.
  //Note also that the function is not only reading the data but is applying a transform on it (the simplest one being a scale factor applied to the motion)

  // Modification by Ludovic Boilevin-Kayl/Alexandre This to use the correct numerical scheme used by M3DISIM (05/2017)
  void LinearProblemHarmonicExtension::readDataDisplacement(std::vector<IO::Pointer>& io, double ReadTime)
  {
    felInt idHE;

    if(m_fstransient->iteration==1){
      m_vectorDisp.resize(m_totNumDofToMatch, 0.);
      m_oldVectorDisp.resize(m_totNumDofToMatch, 0.);
      m_oldOldVectorDisp.resize(m_totNumDofToMatch, 0.);
    }
    //We need an initial displacement before being able to use the velocity
    if(m_fstransient->iteration == 1 or FelisceParam::instance().updateMeshByVelocity == false ) {
      idHE = 1; // displacement
    }
    else {
      idHE = 0; // velocity
    }

    const int iMesh = m_listVariable[idHE].idMesh();

    if(this->m_mesh[m_currentMesh]->domainDim() == 3) {
      io[iMesh]->readVariable(idHE, ReadTime, &m_vectorDisp[0], m_vectorDisp.size());
    } else if (this->m_mesh[m_currentMesh]->domainDim() == 2) {
      std::vector<double> auxDisp(3*m_totNumNodeToMatch, 0.);
      io[iMesh]->readVariable(idHE, ReadTime, auxDisp.data(), auxDisp.size());

      for (int i = 0; i < m_totNumNodeToMatch; ++i) {
        for (int j = 0; j < 2; ++j) {
           m_vectorDisp[2*i+j] = auxDisp[3*i+j];
        }
      }
    } else {
      FEL_ERROR("LinearProblemHarmonicExtension::readDataDisplacement: The value of m_dimension is different from 2 or 3 and this case if not handled.\n");
    }

    std::transform(m_vectorDisp.begin(), m_vectorDisp.end(), m_vectorDisp.begin(), std::bind(std::multiplies<double>(),FelisceParam::instance().spaceUnit, std::placeholders::_1));

    if (m_fstransient->iteration == 1 && FelisceParam::instance().updateMeshByVelocity ) { // for iteration = 1 and for use of velocity,
      io[iMesh]->readVariable(0, ReadTime, &m_oldVectorDisp[0], m_oldVectorDisp.size());
      std::transform(m_oldVectorDisp.begin(), m_oldVectorDisp.end(), m_oldVectorDisp.begin(), std::bind(std::multiplies<double>(),FelisceParam::instance().spaceUnit, std::placeholders::_1));
    }

    //If we are stuck at the same iteration (ie RIS model recomputing), we reuse the previous iteration
    //Miguel said that we maybe "shouldn't re-do the readDataDisplacement anyway"
    //Should something be done in the case we use the velocity ?
    if(FelisceParam::instance().updateMeshByVelocity == false && m_oldVectorDispIteration == m_fstransient->iteration){
      std::cout << "Resetting old values of displacement" << std::endl;
      for (felInt im = 0; im < m_totNumDofToMatch; im++) {
        m_vectorDisp[im] = m_oldVectorDisp[im];
        m_oldVectorDisp[im] = m_oldOldVectorDisp[im];
      }
    }


    if (FelisceParam::instance().readDisplFromFile || FelisceParam::instance().readPreStressedDisplFromFile ||  FelisceParam::instance().initALEDispByFile ) {
      HarmExtSol().duplicateFrom(this->sequentialSolution());
      HarmExtSol().set(0.);
      felInt iPos;
      double value;
      for (felInt im = 0; im < m_totNumDofToMatch; im++) {
        iPos = m_listOfDofToMatch[im];
        AOApplicationToPetsc(m_ao,1,&iPos);

        if(m_fstransient->iteration == 1){
          value = m_vectorDisp[im] * m_scaleCoeff; // value is always a displacement
        }
        else{
          if(FelisceParam::instance().useElasticExtension){ // if useElasticExtension = true, the value used is the incremental value = d^(n+1) - d^(n)
            if(FelisceParam::instance().updateMeshByVelocity){
              //value = (m_vectorDisp[im]*m_fstransient->timeStep) * m_scaleCoeff ; // --> Not the same numerical scheme as M3DISIM!
              value = (m_vectorDisp[im] + m_oldVectorDisp[im] ) * ( m_fstransient->timeStep / 2 ) * m_scaleCoeff ; // Based on a Newmark-beta time scheme with gamma = 1/2 and beta = 1/4
            }
            else{
              value = (m_vectorDisp[im] - m_oldVectorDisp[im]) * m_scaleCoeff ;
            }
          }
          else{ // if useElasticExtension = false, the value used is the absolute value = d^(n+1)
            value = m_vectorDisp[im]*m_scaleCoeff;
          }
        }
        HarmExtSol().setValue(iPos, value, INSERT_VALUES); // matching between _vecdisp and node of label

        if(FelisceParam::instance().useElasticExtension && !FelisceParam::instance().updateMeshByVelocity){
          m_oldOldVectorDisp[im] = m_oldVectorDisp[im];
          m_oldVectorDisp[im] = m_vectorDisp[im];
        }

        if(FelisceParam::instance().useElasticExtension && FelisceParam::instance().updateMeshByVelocity){
          m_oldVectorDisp[im] = m_vectorDisp[im]; // stock the current value of velocity into m_oldVectorDisp which will be used as the previous velocity
        }

      }
      HarmExtSol().assembly();
    }
    m_oldVectorDispIteration = m_fstransient->iteration;
  }

  void LinearProblemHarmonicExtension::finalizeEssBCTransientDerivedProblem() 
  {

    if (FelisceParam::instance().readDisplFromFile ) {

      if (m_fstransient->iteration > 0) {
        for(std::size_t iBC=0; iBC < m_boundaryConditionList.numDirichletBoundaryCondition(); iBC++) {
          BoundaryCondition& BC = * m_boundaryConditionList.Dirichlet(iBC);
          //===============getListDofOfBC===============//
          int iUnknown = BC.getUnknown();
          int idVar = m_listUnknown.idVariable(iUnknown);
          if(BC.typeValueBC() == EnsightFile) {
            felInt idEltInSupportLocal = 0;
            felInt idEltInSupportGlobal = 0;
            felInt idDof;
            double aux =0.;
            BC.ValueBCInSupportDof().clear();
            for (unsigned iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++) {
              m_supportDofUnknownLocal[iUnknown].getIdElementSupport(BC.idEltAndIdSupport()[iSupportDofBC].first, idEltInSupportLocal);
              ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh],1,&idEltInSupportLocal,&idEltInSupportGlobal);
              for(auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++) {
                dof().loc2glob(idEltInSupportGlobal, BC.idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);
                AOApplicationToPetsc(m_ao,1,&idDof);
                HarmExtSol().getValues( 1, &idDof, &aux);
                BC.ValueBCInSupportDof().push_back(aux);
              }

            }

          }
        }
      }

    }
  }

  void LinearProblemHarmonicExtension::initializeStiffnessVector()
  {

    std::cout << "Initializing stiffness std::vector" << std::endl;
    std::vector<int> stiffeningLabels = FelisceParam::instance().stiffeningLabels;
    std::cout << "Getting num of 3D elts" << std::endl;
    int nb3DElts = m_mesh[m_currentMesh]->getNumElement3D();
    std::cout << "I got the number of 3D elts" << std::endl;
    PetscPrintf(PETSC_COMM_WORLD, "\n\n *** Computing stiffening rule ***\n");
    stiffenElementIds.resize(nb3DElts);
    std::fill(stiffenElementIds.begin(), stiffenElementIds.end(), 0);

    felInt startIndex;
    felInt numRefEle;

    std::cout << "Starting to loop on the label list" << std::endl;
    for(unsigned int j = 0; j < stiffeningLabels.size(); ++j){
      startIndex = -1;
      numRefEle = 0;
      int ref = 0;

      ref = stiffeningLabels[j];
      std::cout << "Getting the elements with label " << ref << std::endl;
      m_mesh[m_currentMesh]->getElementsIDPerRef(m_mesh[m_currentMesh]->Tetra4, ref, &startIndex, &numRefEle);

      std::cout << "Filling the stiffenIds" << std::endl;
      if(startIndex != -1 && numRefEle != 0){
        PetscPrintf(PETSC_COMM_WORLD, "Found %d elements (starting at index %d) for reference %d. Activating stiffening for those elements.\n\n", numRefEle, startIndex, ref);
        for(int i = startIndex; i < startIndex + numRefEle; ++i){
          stiffenElementIds[i] = 1;
        }
      }
      else{
        PetscPrintf(PETSC_COMM_WORLD, "Error for the elements under the unknown label reference %d. Stiffening is std::set to 0 for them.\n\n", ref);
      }
    }
    std::cout << "Done." << std::endl;
  }

}
