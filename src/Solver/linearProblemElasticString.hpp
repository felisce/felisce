//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef _LinearProblemElasticString_hpp
#define _LinearProblemElasticString_hpp

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "FiniteElement/elementField.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce{
  class LinearProblemElasticString:public LinearProblem {
  public:
    LinearProblemElasticString();
    ~LinearProblemElasticString() override;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                               const std::vector<Point*>& elemNormal, const std::vector <std::vector<Point*> >& elemTangent,
                               felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;


     void gatherSeqStrucExtrapol(PetscVector& parStrucExtrapol);

  protected:
    felInt m_iDisplacement;
    CurvilinearFiniteElement* m_feDisp;

  private:
    double m_mass;
    double m_massVel;
    double m_lap;
    double m_0;

    ElementField m_elemDispVel;
    ElementField m_elemDisp;
    ElementField m_elemRHS;
  };
}

#endif
