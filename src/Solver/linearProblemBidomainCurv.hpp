//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: A. Collin
//

#ifndef _LinearProblemBidomainCurv_HPP
#define _LinearProblemBidomainCurv_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacFunction.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce 
{
/*!
  \class LinearProblemBidomainCurv
  \authors A. Collin
  */
class LinearProblemBidomainCurv:
  public LinearProblem {
public:
  LinearProblemBidomainCurv();
  ~LinearProblemBidomainCurv() override;
  void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

  void getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber);
  void getAngle(felInt iel, int iUnknown, std::vector<double>& elemAngle);
  void getData(felInt iel, int iUnknown, std::vector<double>& elemData);
  void initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  void initPerDomainBD(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_currentLabel=label;
  }
  double conductivityHet(int currentLabel);
  double conductivityHom(int currentLabel);
  void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  void initLevelSet();
  void levelSet(double min, double max);
  void initSolutionTimeBef();
  void solutionTimeBef();

  //    void initializeElecPhy(SchafSolver* schaf){ m_schaf = schaf;}
  void initializeTimeScheme(Bdf* bdf) override {
    m_bdf = bdf;
  }
  void readData(IO& io) override;
  void readDataForDA(IO& io, double iteration);
  std::vector<double> & Reference() {
    return m_vectorRef;
  }
  double averageLS(CurvilinearFiniteElement* fePtr, int idfe, int minus);
  virtual void sortSolution() {}
  double* sortedSolution() {
    return m_sortedSol;
  }


  virtual void readMatch(){}
  virtual void readElectrodeMeasure(){}
  virtual void addElectrodeCondtrol(){}

  void addMatrixRHS() override;
  void writeEnsightScalar(double* solValue, int idIter, std::string varName);
  void writeEnsightCase(int numIt, std::string varName);

protected:
  CurvilinearFiniteElement* m_fePotTransMemb;
  CurvilinearFiniteElement* m_fePotExtraCell;
  felInt m_ipotTransMemb;
  felInt m_ipotExtraCell;
  double* m_sortedSol;
private:
  //    SchafSolver* m_schaf;
  Bdf* m_bdf;
  std::vector<double> m_vectorFiber;
  std::vector<double> m_vectorAngle;
  std::vector<double> m_vectorRef;
  std::vector<double> m_data;
  PetscVector m_seqIon;
  PetscVector m_seqBdfRHS;
  bool allocateSeqVec;
  bool allocateLevelSet;
  bool allocateSol_n_1;
  PetscVector m_levelSet;
  PetscVector m_levelSetSeq;
  PetscVector m_sol_n_1;
  // felInt m_size;
  // ISLocalToGlobalMapping m_localDofToGlobalDof;
  ElementField m_elemFieldLSVm;
  ElementField m_elemFieldVm;
  double m_avHeavU0;
  double m_avHeavMinusU0;
};
}

#endif
