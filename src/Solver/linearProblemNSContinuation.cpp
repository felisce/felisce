//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes
#include <stack>

// External includes

// Project includes
#include "linearProblemNSContinuation.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

/*!
 \file linearProblemNSContinuation.cpp
 \date 22/02/2022
 \brief Continuation method for FSI, monolithic
*/

namespace felisce {

  LinearProblemNSContinuation::LinearProblemNSContinuation():
    LinearProblem()
  {
    m_useDataPreviousVelocity = true ; 
    m_previousVelocity = &m_oldVelocity; 
    m_aoPreviousVelocity = &m_ao; 
    m_dofPreviousVelocity = &m_dof; 
  }

  void LinearProblemNSContinuation::setPreviousVelocity(PetscVector & previousVelocityCandidate, AO & aoPreviousVelocity, Dof & dofPreviousVelocity)
  {
    m_previousVelocity = &previousVelocityCandidate; 
    m_aoPreviousVelocity = &aoPreviousVelocity; 
    m_dofPreviousVelocity = &dofPreviousVelocity; 

    m_useDataPreviousVelocity = false ; 
  }

  void LinearProblemNSContinuation::readVelocityData(IO& io,double iteration) {
    // Read velocity variable from the .case file
    // store previous velocity from data 
    if(m_useDataPreviousVelocity) // validation 
    {
        if(iteration==0 || (m_velocityData.isNull()) )
        {
          m_oldVelocity.duplicateFrom(this->sequentialSolution());
          m_oldVelocity.set(0.); 
        }else{
          m_oldVelocity.copyFrom(m_velocityData); 
        }
    }

    if(iteration==0 || (m_velocityData.isNull()) )
      m_velocityData.duplicateFrom(this->sequentialSolution());
    
    std::vector<double> dataVelocity;
    dataVelocity.resize(m_mesh[m_currentMesh]->numPoints()*3, 0.);
    io.readVariable(0,iteration, dataVelocity.data(), dataVelocity.size());

    int dim = this->dimension(); 

    m_vectorVelocityData.resize(m_mesh[m_currentMesh]->numPoints()*dim, 0.);

        
  	for (int i=0; i < m_mesh[m_currentMesh]->numPoints(); ++i) {
  	   for(int icomp=0; icomp<dim; icomp++) { 
  	      m_vectorVelocityData[dim*i+icomp]=dataVelocity[3*i+icomp];
  	    }
  	}
   
    std::vector<PetscInt> ao_potData( m_vectorVelocityData.size());
    for (size_t i=0; i < m_vectorVelocityData.size(); ++i) {
      ao_potData[i]=i;
    }

    AOApplicationToPetsc(this->ao(),m_vectorVelocityData.size(),ao_potData.data());
    VecSetValues(m_velocityData.toPetsc(),m_vectorVelocityData.size(),ao_potData.data(), m_vectorVelocityData.data(), INSERT_VALUES);
 
    //m_velocityData.view(PETSC_VIEWER_STDOUT_SELF);
  }


  void LinearProblemNSContinuation::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    this->m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;

    // primal variables
    listVariable.push_back(velocity);
    listNumComp.push_back(this->dimension());
    listVariable.push_back(pressure);
    listNumComp.push_back(1);

    // dual variables with dummy names
    listVariable.push_back(velocityDiv);
    listNumComp.push_back(this->dimension());
    listVariable.push_back(potThorax);
    listNumComp.push_back(1);

    if(FelisceParam::instance(this->instanceIndex()).useALEformulation)
    {
      listVariable.push_back(displacement);
      listNumComp.push_back(this->dimension());
    }
    
    
    //define unknown of the linear system.
    m_listUnknown.push_back(velocity);  
    m_listUnknown.push_back(pressure);
    m_listUnknown.push_back(velocityDiv);
    m_listUnknown.push_back(potThorax);

    userAddOtherUnknowns(listVariable,listNumComp);
    userAddOtherVariables(listVariable,listNumComp);
    definePhysicalVariable(listVariable,listNumComp);

    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    m_iPressure = m_listVariable.getVariableIdList(pressure);
    m_iVelocityDiv = m_listVariable.getVariableIdList(velocityDiv);
    m_iPotThorax  = m_listVariable.getVariableIdList(potThorax);

    m_viscosity = FelisceParam::instance(this->instanceIndex()).viscosity;
    m_useSymmetricStress = FelisceParam::instance(this->instanceIndex()).useSymmetricStress;


  }

  void LinearProblemNSContinuation::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ARGUMENT(eltType);
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
    m_feVel = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];

    // for the convective term + ALE
    m_elemFieldAdv.initialize(DOF_FIELD, *m_feVel, this->dimension());

    // define elemField to store the given velocity measurements and the source terms
    m_dataTerm.initialize(DOF_FIELD, *m_feVel, this->dimension());
    m_sourceTerm.initialize(DOF_FIELD, *m_feVel, this->dimension());

    //=========
    //   ALE
    //=========
    if(FelisceParam::instance(this->instanceIndex()).useALEformulation) {
      m_iDisplacement = m_listVariable.getVariableIdList(displacement);

      m_elemFieldVelMesh.initialize(DOF_FIELD, *m_feVel, this->dimension());
      m_beta.initialize(DOF_FIELD, *m_feVel, this->dimension());

      numDofExtensionProblem = m_externalDof[0]->numDof();

      m_petscToGlobal1.resize(numDofExtensionProblem);
      m_petscToGlobal2.resize(numDofExtensionProblem);
      m_auxvec.resize(numDofExtensionProblem);

      for (int i=0; i<numDofExtensionProblem; i++) {
        m_petscToGlobal1[i]=i;
        m_petscToGlobal2[i]=i;
      }
      AOApplicationToPetsc(m_externalAO[0],numDofExtensionProblem,m_petscToGlobal1.data());
      AOApplicationToPetsc(m_ao,numDofExtensionProblem,m_petscToGlobal2.data());
    }
  }

  
  void LinearProblemNSContinuation::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    m_feVel->updateFirstDeriv(0, elemPoint);
    m_fePres->updateFirstDeriv(0, elemPoint);

    double hK = m_feVel->diameter();

    // stabilization parameters
    double gamma_M = FelisceParam::instance(this->instanceIndex()).referenceValue2; // data fidelity coefficient
    double gamma_p = hK*hK*FelisceParam::instance(this->instanceIndex()).referenceValue3; // Brezzi-Pitkaranta coefficient
    double gamma_p_star = FelisceParam::instance(this->instanceIndex()).referenceValue4; // dual pressure coefficient
    double gamma_div = 0.1;
    double gamma_u_star = 0.1;

    // Luenberger parameter
    double gamma_L = FelisceParam::instance(this->instanceIndex()).NS_gamma;
    
    // time derivative coefficient
    double rhof = FelisceParam::instance(this->instanceIndex()).density;
    double coef_time = rhof/m_fstransient->timeStep;

    // defining the block matrix of the system
    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
      /// div stabilizing term acting on u, int \nabla \cdot u \nabla \cdot v
      m_elementMat[0]->div_phi_j_div_phi_i(gamma_div, *m_feVel, 0, 0, this->dimension()); // block (0,0)
      // data term, int_\Omega uv. to this block we will also add the cip stabilization
      m_elementMat[0]->phi_i_phi_j(gamma_M, *m_feVel, 0, 0, this->dimension()); // block (0,0)

      // stabilizing term acting on p
      m_elementMat[0]->grad_phi_i_grad_phi_j(gamma_p, *m_fePres, this->dimension(), this->dimension(), 1); // block (1,1)
      
      // time discretization for z
      m_elementMat[0]->phi_i_phi_j(coef_time, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)

      // Stokes blocks for z and y, int_\Omega \epsilon v : \epsilon z or int_\Omega \nabla v : \nabla z
      if (m_useSymmetricStress)
        m_elementMat[0]->eps_phi_i_eps_phi_j(2*m_viscosity, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)
      else
        m_elementMat[0]->grad_phi_i_grad_phi_j(m_viscosity, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)
      // \int y \nabla \cdot v and -\int q \nabla \cdot z
      m_elementMat[0]->psi_j_div_phi_i(1., *m_feVel, *m_fePres, 0, 1+2*this->dimension()); // block (0,3)
      m_elementMat[0]->psi_i_div_phi_j(-1., *m_feVel, *m_fePres, this->dimension(), 1+this->dimension()); // block (1,2)

      // time discretization for u
      m_elementMat[0]->phi_i_phi_j(coef_time, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)

      // Stokes blocks for u and p, int_\Omega \epsilon u : \epsilon v or int_\Omega \nabla u : \nabla v
      if (m_useSymmetricStress)
        m_elementMat[0]->eps_phi_i_eps_phi_j(2*m_viscosity, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
      else
        m_elementMat[0]->grad_phi_i_grad_phi_j(m_viscosity, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
      // -int p \nabla \cdot w and \int x \nabla \cdot u
      m_elementMat[0]->psi_j_div_phi_i(-1., *m_feVel, *m_fePres, 1+this->dimension(), this->dimension()); // block (2,1)
      m_elementMat[0]->psi_i_div_phi_j(1., *m_feVel, *m_fePres, 1+2*this->dimension(), 0); // block (3,0)

      // stabilizing term acting on z, -int_\Omega grad z : grad w
      m_elementMat[0]->grad_phi_i_grad_phi_j(-gamma_u_star, *m_feVel, 1+this->dimension(), 1+this->dimension(), this->dimension()); // block (2,2)
      // stabilizing term acting on y, -int_\Omega y * x
      m_elementMat[0]->phi_i_phi_j(-gamma_p_star, *m_fePres, 1+2*this->dimension(), 1+2*this->dimension(), 1); // block (3,3)

      // ALE
      if(FelisceParam::instance(this->instanceIndex()).useALEformulation) {
        // m_elemFielVelMesh -> -w^{n}
        m_elemFieldVelMesh.setValue(externalVec(0), *m_feVel, iel, m_iDisplacement, m_externalAO[0], *m_externalDof[0]);
        // ALE term for u
        m_elementMat[0]->div_u_phi_j_phi_i(rhof, m_elemFieldVelMesh, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
        // ALE term for z
        m_elementMat[0]->div_u_phi_j_phi_i(rhof, m_elemFieldVelMesh, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)


        m_beta.setValue(previousVelocity(), *m_feVel, iel, m_iVelocity, *m_aoPreviousVelocity, *m_dofPreviousVelocity);
        // Construction of m_beta = u^{n-1} - w^{n}; Recall that externalVec(0) = - w^{n}
        m_beta.get_val() = m_beta.get_val() + m_elemFieldVelMesh.get_val();

        if(FelisceParam::instance(this->instanceIndex()).NSequationFlag) { // for Navier-Stokes
          // Temam's stabilization trick
          m_elemFieldAdv.setValue(previousVelocity(), *m_feVel, iel, m_iVelocity,  aoPreviousVelocity(), dofPreviousVelocity()); 
          m_elementMat[0]->div_u_phi_j_phi_i(0.5*rhof, m_elemFieldAdv, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
          m_elementMat[0]->div_u_phi_j_phi_i(0.5*rhof, m_elemFieldAdv, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)


          //convective term for u
          m_elementMat[0]->u_grad_phi_j_phi_i(rhof, m_beta, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
          //convective term for z
          m_elementMat[0]->u_grad_phi_j_phi_i(rhof, m_beta, *m_feVel, 0, 1+this->dimension(), this->dimension(), true); // block (0,2)
        }
        else { // for Stokes
          //convective ALE term for u
          m_elementMat[0]->u_grad_phi_j_phi_i(rhof, m_elemFieldVelMesh, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
          //convective ALE term for z
          m_elementMat[0]->u_grad_phi_j_phi_i(rhof, m_elemFieldVelMesh, *m_feVel, 0, 1+this->dimension(), this->dimension(), true); // block (0,2)
        }    
      }
      else {
        if(FelisceParam::instance(this->instanceIndex()).NSequationFlag) { // for Navier-Stokes, no ALE
          // Temam's stabilization trick
          m_elemFieldAdv.setValue(previousVelocity(), *m_feVel, iel, m_iVelocity, aoPreviousVelocity(), dofPreviousVelocity());

          m_elementMat[0]->div_u_phi_j_phi_i(0.5*rhof, m_elemFieldAdv, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
          m_elementMat[0]->div_u_phi_j_phi_i(0.5*rhof, m_elemFieldAdv, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)

          // linearized convective term
          m_elementMat[0]->u_grad_phi_j_phi_i(rhof, m_elemFieldAdv, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
          m_elementMat[0]->u_grad_phi_j_phi_i(rhof, m_elemFieldAdv, *m_feVel, 0,1+this->dimension(),this->dimension(),true); // block (0,2)
        }
      }

      // Luenberger terms
      // int_\Omega v * z
      m_elementMat[0]->phi_i_phi_j(gamma_L, *m_feVel, 0, 1+this->dimension(), this->dimension()); // block (0,2)
      // int_\Omega u * w
      m_elementMat[0]->phi_i_phi_j(gamma_L, *m_feVel, 1+this->dimension(), 0, this->dimension()); // block (2,0)
    }

    // rhs terms in the current configuration
    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) {
      //rhs data terms, using the measurements
      m_dataTerm.setValue(m_velocityData, *m_feVel, iel, m_iVelocity, m_ao, dof());
      
      // gamma_M int_\Omega u_M * v
      m_elementVector[0]->source(gamma_M, *m_feVel, m_dataTerm, 0, this->dimension());

      // gamma_L int_\Omega u_M * w
      m_elementVector[0]->source(gamma_L, *m_feVel, m_dataTerm, 1+this->dimension(), this->dimension()); // Luenberger term
    }

    // rhs terms in the previous configuration
    if (flagMatrixRHS == FlagMatrixRHS::only_rhs) {
      // rhof/tau int_\Omega u^{n-1} * w
      m_sourceTerm.setValue(previousVelocity(), *m_feVel, iel, m_iVelocity,  aoPreviousVelocity(), dofPreviousVelocity());
      
      m_elementVector[0]->source(coef_time, *m_feVel, m_sourceTerm, 1+this->dimension(), this->dimension());
      // rhs f term, zero
    }
  }

  void LinearProblemNSContinuation::userChangePattern(int numProc, int rankProc) {
    IGNORE_UNUSED_ARGUMENT(numProc);
    IGNORE_UNUSED_ARGUMENT(rankProc);
    // compute initial (uniform) repartition of dof on processes
    felInt numDofByProc = m_numDof/MpiInfo::numProc();
    std::vector<felInt> numDofPerProc(MpiInfo::numProc());
    for(felInt i=0; i<MpiInfo::numProc(); ++i) {
      if(i == MpiInfo::numProc() - 1)
        numDofPerProc[i] = m_numDof - i*numDofByProc;
      else
        numDofPerProc[i] = numDofByProc;
    }
    
    felInt shiftDof = 0;
    for(felInt i=0; i<MpiInfo::rankProc(); ++i)
      shiftDof += numDofPerProc[i];
    
    std::vector<felInt> rankDof(m_numDof, -1);
    for(felInt i=0; i<numDofPerProc[MpiInfo::rankProc()]; ++i)
      rankDof[i + shiftDof] = MpiInfo::rankProc();
    
    // build the edges or faces depending on the dimension (for the global mesh)
    // In this function, "faces" refers to either edges or faces.
    if(dimension() == 2)
     m_mesh[m_currentMesh]->buildEdges();
    else
     m_mesh[m_currentMesh]->buildFaces();
    
    // variables
    GeometricMeshRegion::ElementType eltType, eltTypeOpp;
    felInt idVar1, idVar2;
    felInt node1, node2;
    felInt numFacesPerElement, numEltPerLabel;
    felInt ielSupport, jelSupport;
    felInt ielCurrentGeo, ielOppositeGeo;
    std::vector<felInt> vecSupport, vecSupportOpposite;
    std::vector<felInt> numElement(m_mesh[m_currentMesh]->m_numTypesOfElement, 0);
    std::vector< std::set<felInt> > nodesNeighborhood(m_numDof);
    
    
    // zeroth loop on the unknown of the linear problem
    for (size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
      idVar1 = m_listUnknown.idVariable(iUnknown1);
      
      ielCurrentGeo = 0;
      
      // first loop on element type
      for (size_t i=0; i<m_mesh[m_currentMesh]->bagElementTypeDomain().size(); ++i) {
        eltType = m_mesh[m_currentMesh]->bagElementTypeDomain()[i];
        const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
        numElement[eltType] = 0;
        numFacesPerElement = geoEle->numBdEle();
        
        // second loop on region of the mesh.
        for(GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef =m_mesh[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef !=m_mesh[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
          numEltPerLabel = itRef->second.second;
                
          // Third loop on element
          for (felInt iel = 0; iel < numEltPerLabel; iel++) {
            m_supportDofUnknown[iUnknown1].getIdElementSupport(eltType, numElement[eltType], vecSupport);
                  
            for(size_t ielSup=0; ielSup<vecSupport.size(); ++ielSup) {
              ielSupport = vecSupport[ielSup];
                    
              // for all support dof in this support element
              for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
                // for all component of the unknown
                for (size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); iComp++) {
                  // get the global id of the support dof
                  dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);
                              
                  // if this node is on this process
                  if(rankDof[node1] == MpiInfo::rankProc()) {
                    // loop over the boundaries of the element
                    for(felInt iface=0; iface < numFacesPerElement; ++iface) {
                      // check if this face is an inner face or a boundary
                      ielOppositeGeo = ielCurrentGeo;
                      eltTypeOpp = eltType;
                      bool isAnInnerFace =m_mesh[m_currentMesh]->getAdjElement(eltTypeOpp, ielOppositeGeo, iface);
                                  
                      if(isAnInnerFace) {
                        // for all unknown
                        for (size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                          idVar2 = m_listUnknown.idVariable(iUnknown2);
                                            
                          // get the support element of the opposite element
                          m_supportDofUnknown[iUnknown2].getIdElementSupport(ielOppositeGeo, vecSupportOpposite);
                                            
                          // for all support element
                          for(size_t jelSup=0; jelSup<vecSupportOpposite.size(); ++jelSup) {
                            jelSupport = vecSupportOpposite[jelSup];
                                              
                            // for all support dof in this support element
                            for (felInt jSup = 0; jSup < m_supportDofUnknown[iUnknown2].getNumSupportDof(jelSupport); ++jSup) {
                              
                              // for all component of the second unknown
                              for (size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {
                          
                                // If the two current components are connected
                                felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);
                                felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                                if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                                  // get the global id of the second support dof
                                  dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                                                          
                                  // remove diagonal term to define pattern and use it in Parmetis.
                                  // if the two global ids are different, they are neighboors
                                  if(node1 != node2)
                                    nodesNeighborhood[node1].insert(node2);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            ++ielCurrentGeo;
            ++numElement[eltType];
          }
        }
      }
    }
    
    // Store the pattern in CSR style
    std::vector<felInt> iCSR, jCSR;
    felInt dofSize = 0;
    felInt cptDof = 0;
    felInt pos;
    
    iCSR.resize(dof().pattern().numRows() + 1, 0);
    for(size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode)
      dofSize += nodesNeighborhood[iNode].size();
    
    jCSR.resize(dofSize, 0);
    for(size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode) {
      if(rankDof[iNode] == MpiInfo::rankProc()) {
        iCSR[cptDof + 1] = iCSR[cptDof] + nodesNeighborhood[iNode].size();
        pos = 0;
        for(std::set<felInt>::iterator it=nodesNeighborhood[iNode].begin(); it != nodesNeighborhood[iNode].end(); ++it) {
          jCSR[iCSR[cptDof] + pos] = *it;
          ++pos;
        }
        ++cptDof;
      }
    }
    
    // Now, call merge pattern
    dof().mergeGlobalPattern(iCSR, jCSR);
    //std::cout<<"After global merge"<<std::endl;
    //dof().pattern().print(1, std::cout);
  }

  // compared to the function linearProblemNS::assembleFaceOrientedStabilization, here we use different blocks and only the jumps in the velocity
  void LinearProblemNSContinuation::assembleCIPStabilization() {
    // definition of variables
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;
    GeometricMeshRegion::ElementType eltType;     // Type of element
    
    felInt ielCurrentLocalGeo = 0;             // local geometric id of the current element
    felInt ielCurrentGlobalGeo = 0;            // global geometric id of the current element
    felInt ielOppositeGlobalGeo = 0;           // global geometric id of the opposite element
    felInt numFacesPerType;                    // number of faces of an element of a given type
    felInt numPointPerElt;                     // number of vertices by element
    felInt ielCurrentGlobalSup;                // global support element id of the current element
    felInt ielOppositeGlobalSup;               // global support element id of the opposite element

    std::vector<felInt> idOfFacesCurrent;           // ids of the current element edges
    std::vector<felInt> idOfFacesOpposite;          // ids of the opposite element edges
    std::vector<felInt> currentElemIdPoint;         // ids of the vertices of the current element
    std::vector<felInt> oppositeElemIdPoint;        // ids of the vertices of the opposite element 
    std::vector<Point*> currentElemPoint;           // point coordinates of the current element vertices
    std::vector<Point*> oppositeElemPoint;          // point coordinates of the opposite element vertices

    FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;     // flag to only assemble the matrix
    felInt idFaceToDo;
    bool allDone = false;

    ElementField elemFieldAdvFace;
    
    felInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    // bag element type vector
    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
    eltType = bagElementTypeDomain[0];

     
    // Finite Element With Bd for the opposite element
    CurrentFiniteElementWithBd* ptmp;
    CurrentFiniteElementWithBd* oppositeFEWithBd;
      
    m_velocity = &m_listVariable[m_listVariable.getVariableIdList(velocity)];

    const GeoElement *geoEle = m_mesh[m_currentMesh]->eltEnumToFelNameGeoEle[eltType].second;
    felInt typeOfFEVel = m_velocity->finiteElementType();
    
    const RefElement *refEleVel = geoEle->defineFiniteEle(eltType, typeOfFEVel, *m_mesh[m_currentMesh]);
    oppositeFEWithBd = new CurrentFiniteElementWithBd(*refEleVel, *geoEle, m_velocity->degreeOfExactness(), m_velocity->degreeOfExactness());
    
    // initializing variables
    numPointPerElt = m_meshLocal[m_currentMesh]->m_numPointsPerElt[eltType];
    numFacesPerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

    // resize of vectors
    idOfFacesCurrent.resize(numFacesPerType, -1);
    idOfFacesOpposite.resize(numFacesPerType, -1);
    currentElemIdPoint.resize(numPointPerElt, -1);
    oppositeElemIdPoint.resize(numPointPerElt, -1);
    currentElemPoint.resize(numPointPerElt, NULL);
    oppositeElemPoint.resize(numPointPerElt, NULL);
    
    // define finite element
    defineFiniteElement(eltType);
    initElementArray();
    defineCurrentFiniteElementWithBd(eltType);

    // allocate arrays for assembling the matrix
    allocateArrayForAssembleMatrixRHS(flag);
    
    // init variables
    initPerElementType(eltType, flag);
    CurrentFiniteElementWithBd* feWithBd =  m_listCurrentFiniteElementWithBd[m_iVelocity];
    
    CurrentFiniteElementWithBd* firstCurrentFe = feWithBd;
    CurrentFiniteElementWithBd* firstOppositeFe = oppositeFEWithBd;
    
    // get informations on the current element
    setElemPoint(eltType, 0, currentElemPoint, currentElemIdPoint, &ielCurrentGlobalSup);
    
    // update the finite elements
    feWithBd->updateFirstDeriv(0, currentElemPoint);
    feWithBd->updateBdMeasNormal();

    // get the global id of the first geometric element
    ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &ielCurrentLocalGeo, &ielCurrentGlobalGeo);
    
    // the map to remember what contribution have already been computed
    std::map<felInt, std::vector<bool> > contribDone;
    addNewFaceOrientedContributor(numFacesPerType, ielCurrentGlobalGeo, contribDone[ielCurrentGlobalGeo]);

    // build the stack to know what is the next element
    std::stack<felInt> nextInStack; 
    nextInStack.push(ielCurrentGlobalGeo);

    // get all the faces of the element
    if(dimension() == 2)
      m_mesh[m_currentMesh]->getAllEdgeOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);
    else
      m_mesh[m_currentMesh]->getAllFaceOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);

    // loop over all the element (snake style)
    while(!nextInStack.empty()) {      
      // check the faces and use the first one that is an inner face and that has not been done yet.
      idFaceToDo = -1;
      allDone = true;
      for(size_t iface=0; iface<contribDone[ielCurrentGlobalGeo].size(); ++iface) {
        if(!contribDone[ielCurrentGlobalGeo][iface]) {
          // This is an inner face, check if the contribution has already been computed or not
          if(idFaceToDo == -1) {
            idFaceToDo = iface;
          } else {
            allDone = false;
          }
        }
      }

      // update the stack
      if(!allDone)
        nextInStack.push(ielCurrentGlobalGeo);
      
      if(nextInStack.top() == ielCurrentGlobalGeo && allDone)
        nextInStack.pop();
      

      // assemble terms
      if(idFaceToDo != -1) {
        // get the opposite id of the element
        ielOppositeGlobalGeo = ielCurrentGlobalGeo;
        adjElt =m_mesh[m_currentMesh]->getAdjElement(ielOppositeGlobalGeo, idFaceToDo);
  
        // get the type of the opposite element
        // eltTypeOpp = adjElt.second;
        
        // update the opposite finite element and set ielOppositeGlobalSup
        updateFaceOrientedFEWithBd(oppositeFEWithBd,  idOfFacesOpposite, numPointPerElt, ielOppositeGlobalGeo, ielOppositeGlobalSup);

        // find the local id of the edge in the opposite element
        felInt localIdFaceOpposite = -1;
        for(size_t jface=0; jface<idOfFacesOpposite.size(); ++jface) {
          if(idOfFacesCurrent[idFaceToDo] == idOfFacesOpposite[jface]) {
            localIdFaceOpposite = jface;
          }
        }

        // compute coefficients
        CurvilinearFiniteElement* curvFe = &feWithBd->bdEle(idFaceToDo);
        //CurvilinearFiniteElement* curvFeOP = &oppositeFEWithBd->bdEle(localIdFaceOpposite);
        double hK =  curvFe->diameter(); //0.5 * (feWithBd->diameter() + oppositeFEWithBd->diameter());
        //cout << hK - curvFeOP->diameter() << endl;
        double gamma_u = FelisceParam::instance(this->instanceIndex()).referenceValue1; // cip coefficient
        double gamma = hK*gamma_u;

        // We can now compute all the integrals.
        // current element for phi_i and phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma, *feWithBd, *feWithBd, idFaceToDo, idFaceToDo, 0, 0, this->dimension()); // block (0,0)
        setValueMatrixRHS(ielCurrentGlobalSup, ielCurrentGlobalSup, flag);

        // current element for phi_i and opposite element for phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma, *oppositeFEWithBd, *feWithBd, localIdFaceOpposite, idFaceToDo, 0, 0, this->dimension()); // block (0,0)
        setValueMatrixRHS(ielCurrentGlobalSup, ielOppositeGlobalSup, flag);

        // opposite element for phi_i and current element for phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma,  *feWithBd, *oppositeFEWithBd, idFaceToDo, localIdFaceOpposite, 0, 0, this->dimension()); // block (0,0)
        setValueMatrixRHS(ielOppositeGlobalSup, ielCurrentGlobalSup, flag);
        
        // opposite element for phi_i and phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma, *oppositeFEWithBd, *oppositeFEWithBd, localIdFaceOpposite, localIdFaceOpposite, 0, 0, this->dimension()); // block (0,0)
        setValueMatrixRHS(ielOppositeGlobalSup, ielOppositeGlobalSup, flag);

        // update the map to say that this face is done
        contribDone[ielCurrentGlobalGeo][idFaceToDo] = true;
       
        // check if the opposite element is on this proc
        if(m_eltPart[m_currentMesh][ielOppositeGlobalGeo] == rank) {
          if(contribDone.find(ielOppositeGlobalGeo) == contribDone.end()) {
            // not found in the map, add it and initialize it.
            addNewFaceOrientedContributor(numFacesPerType, ielOppositeGlobalGeo, contribDone[ielOppositeGlobalGeo]);
          }
          contribDone[ielOppositeGlobalGeo][localIdFaceOpposite] = true;
          
          // update the finite element as the opposite finite element.
          ptmp = feWithBd;
          feWithBd = oppositeFEWithBd;
          oppositeFEWithBd = ptmp;
    
          ielCurrentGlobalGeo = ielOppositeGlobalGeo;
          ielCurrentGlobalSup = ielOppositeGlobalSup;
          idOfFacesCurrent = idOfFacesOpposite;
        }
        else {
          // not on this rank, take the next one in the stack
          if(!nextInStack.empty()) {
            ielCurrentGlobalGeo = nextInStack.top();
            updateFaceOrientedFEWithBd(feWithBd, idOfFacesCurrent, numPointPerElt, ielCurrentGlobalGeo, ielCurrentGlobalSup);
          }
        }
      }
      else {
        // All contribution have been already computed on this element
        // take the next element in the stack
        if(!nextInStack.empty()) {
          ielCurrentGlobalGeo = nextInStack.top();
          updateFaceOrientedFEWithBd(feWithBd, idOfFacesCurrent, numPointPerElt, ielCurrentGlobalGeo, ielCurrentGlobalSup);
        }
      }
    }
    
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flag);
    
    // desallocate opposite finite elements
    feWithBd = firstCurrentFe;
    delete firstOppositeFe;
  }

  void LinearProblemNSContinuation::addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec) {
    felInt ielTmp;
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;
    
    vec.resize(size, false);
    for(felInt iface=0; iface<size; ++iface) {
      // check if this face is an inner face or a boundary
      ielTmp = idElt;
      adjElt =m_mesh[m_currentMesh]->getAdjElement(ielTmp, iface);
      
      if(!adjElt.first) {
        // not an inner face, set the contribution to done.
        vec[iface] = true;
      }
    }
  }

  void LinearProblemNSContinuation::updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fe, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup) {
    std::vector<felInt> elemIdPoint(numPoints, -1);
    std::vector<Point*> elemPoint(numPoints, NULL);   
    
    // get information on the opposite element
    // same as the function "setElemPoint" but here ielOppositeGlobalGeo is global
    // we assume that the supportDofMesh is the same for all unknown (like in setElemPoint)
    m_supportDofUnknown[0].getIdElementSupport(idElt, idSup);
    m_mesh[m_currentMesh]->getOneElement(idElt, elemIdPoint);
    for (felInt iPoint=0; iPoint<numPoints; ++iPoint)
      elemPoint[iPoint] = &(m_mesh[m_currentMesh]->listPoints()[elemIdPoint[iPoint]]);
   
    // update the finite elements
    fe->updateFirstDeriv(0, elemPoint);
    fe->updateBdMeasNormal();
    
    // get all the faces of the element
    if(dimension() == 2)
      m_mesh[m_currentMesh]->getAllEdgeOfElement(idElt, idOfFaces);
    else
      m_mesh[m_currentMesh]->getAllFaceOfElement(idElt, idOfFaces);
  } 
}
