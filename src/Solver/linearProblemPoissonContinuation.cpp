//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes
#include <stack>

// External includes

// Project includes
#include "Solver/linearProblemPoissonContinuation.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

/*!
 \file linearProblemPoisson.cpp
 \date 17/02/2022
 \brief Continuation method for Poisson's equation
*/
namespace felisce {
  LinearProblemPoissonContinuation::LinearProblemPoissonContinuation():
    LinearProblem("Poisson continuation equation")
  {
  }

  void LinearProblemPoissonContinuation::readData(IO& io,double iteration) {
    // Read variable from file
    std::vector<double> potData;
    potData.resize(m_mesh[m_currentMesh]->numPoints(),0.);
    io.readVariable(0,iteration, potData.data(), potData.size());
    
    m_potData.duplicateFrom(this->sequentialSolution());
    
    std::vector<PetscInt> ao_potData( potData.size());
    for (size_t i=0; i < potData.size(); ++i) {
      ao_potData[i]=i;
    }

    AOApplicationToPetsc(this->ao(),potData.size(),ao_potData.data());
    VecSetValues(m_potData.toPetsc(),potData.size(),ao_potData.data(), potData.data(), INSERT_VALUES);
  }
  
  void LinearProblemPoissonContinuation::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    this->m_fstransient = fstransient;

    std::vector<PhysicalVariable> listVariable(2);
    std::vector<size_t> listNumComp(2);
    listVariable[0] = temperature; // primal variable, the discrete solution u_h
    listNumComp[0]  = 1;
    listVariable[1] = potThorax; // dummy name for dual variable, the Langrange multiplier z_h
    listNumComp[1]  = 1;
    
    //define unknown of the linear system.
    m_listUnknown.push_back(temperature);
    m_listUnknown.push_back(potThorax);
    definePhysicalVariable(listVariable,listNumComp);

    m_iTemperature = m_listVariable.getVariableIdList(temperature);
    m_iPotThorax = m_listVariable.getVariableIdList(potThorax);
   }

  void LinearProblemPoissonContinuation::initPerElementType(ElementType /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/) {
    m_feTemp = m_listCurrentFiniteElement[m_iTemperature];

    // initialize elemField to store the data
    m_elemField.initialize(DOF_FIELD, *m_feTemp, 1);
  }

  void LinearProblemPoissonContinuation::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    m_feTemp->updateFirstDeriv(0, elemPoint);

    const double gamma_M = FelisceParam::instance(this->instanceIndex()).referenceValue2; // data fidelity coefficient
    const int labelDataSubdomain = FelisceParam::instance(this->instanceIndex()).testCase; // label of the subdomain where data is given; 0 for the whole domain

    // defining the block matrix of the system
    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
      // block (0,0): gamma_M * int_\Omega_data u * v; in this block we will also add the cip stabilization
      if (labelDataSubdomain == 0) {
        m_elementMat[0]->phi_i_phi_j(gamma_M, *m_feTemp, 0, 0, 1);
      }
      else if (m_currentLabel == labelDataSubdomain) {
        m_elementMat[0]->phi_i_phi_j(gamma_M, *m_feTemp, 0, 0, 1);
      }

      // block (0,1): int_\Omega grad v . grad z
      m_elementMat[0]->grad_phi_i_grad_phi_j(1., *m_feTemp, 0, 1, 1);

      // block (1,0): int_\Omega grad u . grad w
      m_elementMat[0]->grad_phi_i_grad_phi_j(1., *m_feTemp, 1, 0, 1);

      // block (1,1): -int_\Omega grad z . grad w
      m_elementMat[0]->grad_phi_i_grad_phi_j(-1., *m_feTemp, 1, 1, 1);
    }

    // defining the rhs of the system
    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
      // assuming that f is zero

      // rhs data term
      m_elemField.setValue(m_potData, *m_feTemp, iel, m_iTemperature, m_ao, dof());
      if (labelDataSubdomain == 0) {
        m_elementVector[0]->source(gamma_M, *m_feTemp, m_elemField, 0, 1);
      }
      else if (m_currentLabel == labelDataSubdomain) {
        m_elementVector[0]->source(gamma_M, *m_feTemp, m_elemField, 0, 1);
      }
    }
  }

  void LinearProblemPoissonContinuation::userChangePattern(int numProc, int rankProc) {
    IGNORE_UNUSED_ARGUMENT(numProc);
    IGNORE_UNUSED_ARGUMENT(rankProc);
    // compute initial (uniform) repartition of dof on processes
    felInt numDofByProc = m_numDof/MpiInfo::numProc();
    std::vector<felInt> numDofPerProc(MpiInfo::numProc());
    for(felInt i=0; i<MpiInfo::numProc(); ++i) {
      if(i == MpiInfo::numProc() - 1)
        numDofPerProc[i] = m_numDof - i*numDofByProc;
      else
        numDofPerProc[i] = numDofByProc;
    }
    
    felInt shiftDof = 0;
    for(felInt i=0; i<MpiInfo::rankProc(); ++i)
      shiftDof += numDofPerProc[i];
    
    std::vector<felInt> rankDof(m_numDof, -1);
    for(felInt i=0; i<numDofPerProc[MpiInfo::rankProc()]; ++i)
      rankDof[i + shiftDof] = MpiInfo::rankProc();
    
    // build the edges or faces depending on the dimension (for the global mesh)
    // In this function, "faces" refers to either edges or faces.
    if(dimension() == 2)
     m_mesh[m_currentMesh]->buildEdges();
    else
     m_mesh[m_currentMesh]->buildFaces();
    
    // variables
    GeometricMeshRegion::ElementType eltType, eltTypeOpp;
    felInt idVar1, idVar2;
    felInt node1, node2;
    felInt numFacesPerElement, numEltPerLabel;
    felInt ielSupport, jelSupport;
    felInt ielCurrentGeo, ielOppositeGeo;
    std::vector<felInt> vecSupport, vecSupportOpposite;
    std::vector<felInt> numElement(m_mesh[m_currentMesh]->m_numTypesOfElement, 0);
    std::vector< std::set<felInt> > nodesNeighborhood(m_numDof);
    
    // zeroth loop on the unknown of the linear problem
    for (size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
      idVar1 = m_listUnknown.idVariable(iUnknown1);
      
      ielCurrentGeo = 0;
      
      // first loop on element type
      for (size_t i=0; i<m_mesh[m_currentMesh]->bagElementTypeDomain().size(); ++i) {
        eltType = m_mesh[m_currentMesh]->bagElementTypeDomain()[i];
        const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
        numElement[eltType] = 0;
        numFacesPerElement = geoEle->numBdEle();
        
        // second loop on region of the mesh.
        for(GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef =m_mesh[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef !=m_mesh[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
          numEltPerLabel = itRef->second.second;
                
          // Third loop on element
          for (felInt iel = 0; iel < numEltPerLabel; iel++) {
            m_supportDofUnknown[iUnknown1].getIdElementSupport(eltType, numElement[eltType], vecSupport);
                  
            for(size_t ielSup=0; ielSup<vecSupport.size(); ++ielSup) {
              ielSupport = vecSupport[ielSup];
                    
              // for all support dof in this support element
              for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
                // for all component of the unknown
                for (size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); iComp++) {
                  // get the global id of the support dof
                  dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);
                              
                  // if this node is on this process
                  if(rankDof[node1] == MpiInfo::rankProc()) {
                    // loop over the boundaries of the element
                    for(felInt iface=0; iface < numFacesPerElement; ++iface) {
                      // check if this face is an inner face or a boundary
                      ielOppositeGeo = ielCurrentGeo;
                      eltTypeOpp = eltType;
                      bool isAnInnerFace =m_mesh[m_currentMesh]->getAdjElement(eltTypeOpp, ielOppositeGeo, iface);
                                  
                      if(isAnInnerFace) {
                        // for all unknown
                        for (size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                          idVar2 = m_listUnknown.idVariable(iUnknown2);
                                            
                          // get the support element of the opposite element
                          m_supportDofUnknown[iUnknown2].getIdElementSupport(ielOppositeGeo, vecSupportOpposite);
                                            
                          // for all support element
                          for(size_t jelSup=0; jelSup<vecSupportOpposite.size(); ++jelSup) {
                            jelSupport = vecSupportOpposite[jelSup];
                                              
                            // for all support dof in this support element
                            for (felInt jSup = 0; jSup < m_supportDofUnknown[iUnknown2].getNumSupportDof(jelSupport); ++jSup) {
                              
                              // for all component of the second unknown
                              for (size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {
                          
                                // If the two current components are connected
                                felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);
                                felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                                if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                                  // get the global id of the second support dof
                                  dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                                                          
                                  // remove diagonal term to define pattern and use it in Parmetis.
                                  // if the two global ids are different, they are neighboors
                                  if(node1 != node2)
                                    nodesNeighborhood[node1].insert(node2);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            ++ielCurrentGeo;
            ++numElement[eltType];
          }
        }
      }
    }
    
    // Store the pattern in CSR style
    std::vector<felInt> iCSR, jCSR;
    felInt dofSize = 0;
    felInt cptDof = 0;
    felInt pos;
    
    iCSR.resize(dof().pattern().numRows() + 1, 0);
    for(size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode)
      dofSize += nodesNeighborhood[iNode].size();
    
    jCSR.resize(dofSize, 0);
    for(size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode) {
      if(rankDof[iNode] == MpiInfo::rankProc()) {
        iCSR[cptDof + 1] = iCSR[cptDof] + nodesNeighborhood[iNode].size();
        pos = 0;
        for(std::set<felInt>::iterator it=nodesNeighborhood[iNode].begin(); it != nodesNeighborhood[iNode].end(); ++it) {
          jCSR[iCSR[cptDof] + pos] = *it;
          ++pos;
        }
        ++cptDof;
      }
    }
    
    // Now, call merge pattern
    dof().mergeGlobalPattern(iCSR, jCSR);
  }

  void LinearProblemPoissonContinuation::assembleFaceOrientedStabilization() {
    // definition of variables
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;
    GeometricMeshRegion::ElementType eltType;     // Type of element
    
    felInt ielCurrentLocalGeo = 0;             // local geometric id of the current element
    felInt ielCurrentGlobalGeo = 0;            // global geometric id of the current element
    felInt ielOppositeGlobalGeo = 0;           // global geometric id of the opposite element
    felInt numFacesPerType;                    // number of faces of an element of a given type
    felInt numPointPerElt;                     // number of vertices by element
    felInt ielCurrentGlobalSup;                // global support element id of the current element
    felInt ielOppositeGlobalSup;               // global support element id of the opposite element

    std::vector<felInt> idOfFacesCurrent;           // ids of the current element edges
    std::vector<felInt> idOfFacesOpposite;          // ids of the opposite element edges
    std::vector<felInt> currentElemIdPoint;         // ids of the vertices of the current element
    std::vector<felInt> oppositeElemIdPoint;        // ids of the vertices of the opposite element 
    std::vector<Point*> currentElemPoint;           // point coordinates of the current element vertices
    std::vector<Point*> oppositeElemPoint;          // point coordinates of the opposite element vertices

    FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;     // flag to only assemble the matrix
    felInt idFaceToDo;
    bool allDone = false;

    ElementField elemFieldAdvFace;
    
    felInt rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    // bag element type vector
    const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
    eltType = bagElementTypeDomain[0];

     
    // Finite Element With Bd for the opposite element
    CurrentFiniteElementWithBd* ptmp;
    CurrentFiniteElementWithBd* oppositeFEWithBd;
      
    m_temperature = &m_listVariable[m_listVariable.getVariableIdList(temperature)];

    const GeoElement *geoEle = m_mesh[m_currentMesh]->eltEnumToFelNameGeoEle[eltType].second;
    felInt typeOfFEVel = m_temperature->finiteElementType();
    
    const RefElement *refEleVel = geoEle->defineFiniteEle(eltType, typeOfFEVel, *m_mesh[m_currentMesh]);
    oppositeFEWithBd = new CurrentFiniteElementWithBd(*refEleVel, *geoEle, m_temperature->degreeOfExactness(), m_temperature->degreeOfExactness());
    
    // initializing variables
    numPointPerElt = m_meshLocal[m_currentMesh]->m_numPointsPerElt[eltType];
    numFacesPerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

    // resize of vectors
    idOfFacesCurrent.resize(numFacesPerType, -1);
    idOfFacesOpposite.resize(numFacesPerType, -1);
    currentElemIdPoint.resize(numPointPerElt, -1);
    oppositeElemIdPoint.resize(numPointPerElt, -1);
    currentElemPoint.resize(numPointPerElt, NULL);
    oppositeElemPoint.resize(numPointPerElt, NULL);
    
    // define finite element
    defineFiniteElement(eltType);
    initElementArray();
    defineCurrentFiniteElementWithBd(eltType);

    // allocate arrays for assembling the matrix
    allocateArrayForAssembleMatrixRHS(flag);
    
    // init variables
    initPerElementType(eltType, flag);
    CurrentFiniteElementWithBd* feWithBd =  m_listCurrentFiniteElementWithBd[m_iTemperature];
    
    CurrentFiniteElementWithBd* firstCurrentFe = feWithBd;
    CurrentFiniteElementWithBd* firstOppositeFe = oppositeFEWithBd;
    
    // get informations on the current element
    setElemPoint(eltType, 0, currentElemPoint, currentElemIdPoint, &ielCurrentGlobalSup);
    
    // update the finite elements
    feWithBd->updateFirstDeriv(0, currentElemPoint);
    feWithBd->updateBdMeasNormal();

    // get the global id of the first geometric element
    ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &ielCurrentLocalGeo, &ielCurrentGlobalGeo);
    
    // the map to remember what contribution have already been computed
    std::map<felInt, std::vector<bool> > contribDone;
    addNewFaceOrientedContributor(numFacesPerType, ielCurrentGlobalGeo, contribDone[ielCurrentGlobalGeo]);

    // build the stack to know what is the next element
    std::stack<felInt> nextInStack; 
    nextInStack.push(ielCurrentGlobalGeo);

    // get all the faces of the element
    if(dimension() == 2)
      m_mesh[m_currentMesh]->getAllEdgeOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);
    else
      m_mesh[m_currentMesh]->getAllFaceOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);

    // loop over all the element (snake style)
    while(!nextInStack.empty()) {      
      // check the faces and use the first one that is an inner face and that has not been done yet.
      idFaceToDo = -1;
      allDone = true;
      for(size_t iface=0; iface<contribDone[ielCurrentGlobalGeo].size(); ++iface) {
        if(!contribDone[ielCurrentGlobalGeo][iface]) {
          // This is an inner face, check if the contribution has already been computed or not
          if(idFaceToDo == -1) {
            idFaceToDo = iface;
          } else {
            allDone = false;
          }
        }
      }

      // update the stack
      if(!allDone)
        nextInStack.push(ielCurrentGlobalGeo);
      
      if(nextInStack.top() == ielCurrentGlobalGeo && allDone)
        nextInStack.pop();
      

      // assemble terms
      if(idFaceToDo != -1) {
        // get the opposite id of the element
        ielOppositeGlobalGeo = ielCurrentGlobalGeo;
        adjElt =m_mesh[m_currentMesh]->getAdjElement(ielOppositeGlobalGeo, idFaceToDo);
  
        // get the type of the opposite element
        // eltTypeOpp = adjElt.second;
        
        // update the opposite finite element and set ielOppositeGlobalSup
        updateFaceOrientedFEWithBd(oppositeFEWithBd,  idOfFacesOpposite, numPointPerElt, ielOppositeGlobalGeo, ielOppositeGlobalSup);

        // find the local id of the edge in the opposite element
        felInt localIdFaceOpposite = -1;
        for(size_t jface=0; jface<idOfFacesOpposite.size(); ++jface) {
          if(idOfFacesCurrent[idFaceToDo] == idOfFacesOpposite[jface]) {
            localIdFaceOpposite = jface;
          }
        }

        // compute coefficients
        const CurvilinearFiniteElement* curvFe = &feWithBd->bdEle(idFaceToDo);
        //CurvilinearFiniteElement* curvFeOP = &oppositeFEWithBd->bdEle(localIdFaceOpposite);
        double hK =  curvFe->diameter(); //0.5 * (feWithBd->diameter() + oppositeFEWithBd->diameter());
        const double gamma_u = FelisceParam::instance(this->instanceIndex()).referenceValue1; // cip stabilization parameter
        double gamma = hK*gamma_u;

        // We can now compute all the integrals.
        // current element for phi_i and phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma, *feWithBd, *feWithBd, idFaceToDo, idFaceToDo, 0, 0, 1); // block (0,0)
        setValueMatrixRHS(ielCurrentGlobalSup, ielCurrentGlobalSup, flag);

        // current element for phi_i and opposite element for phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma, *oppositeFEWithBd, *feWithBd, localIdFaceOpposite, idFaceToDo, 0, 0, 1); // block (0,0)
        setValueMatrixRHS(ielCurrentGlobalSup, ielOppositeGlobalSup, flag);

        // opposite element for phi_i and current element for phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma,  *feWithBd, *oppositeFEWithBd, idFaceToDo, localIdFaceOpposite, 0, 0, 1); // block (0,0)
        setValueMatrixRHS(ielOppositeGlobalSup, ielCurrentGlobalSup, flag);
        
        // opposite element for phi_i and phi_j
        m_elementMat[0]->zero();
        m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gamma, *oppositeFEWithBd, *oppositeFEWithBd, localIdFaceOpposite, localIdFaceOpposite, 0, 0, 1); // block (0,0)
        setValueMatrixRHS(ielOppositeGlobalSup, ielOppositeGlobalSup, flag);

        // update the map to say that this face is done
        contribDone[ielCurrentGlobalGeo][idFaceToDo] = true;
       
        // check if the opposite element is on this proc
        if(m_eltPart[m_currentMesh][ielOppositeGlobalGeo] == rank) {
          if(contribDone.find(ielOppositeGlobalGeo) == contribDone.end()) {
            // not found in the map, add it and initialize it.
            addNewFaceOrientedContributor(numFacesPerType, ielOppositeGlobalGeo, contribDone[ielOppositeGlobalGeo]);
          }
          contribDone[ielOppositeGlobalGeo][localIdFaceOpposite] = true;
          
          // update the finite element as the opposite finite element.
          ptmp = feWithBd;
          feWithBd = oppositeFEWithBd;
          oppositeFEWithBd = ptmp;
    
          ielCurrentGlobalGeo = ielOppositeGlobalGeo;
          ielCurrentGlobalSup = ielOppositeGlobalSup;
          idOfFacesCurrent = idOfFacesOpposite;
        }
        else {
          // not on this rank, take the next one in the stack
          if(!nextInStack.empty()) {
            ielCurrentGlobalGeo = nextInStack.top();
            updateFaceOrientedFEWithBd(feWithBd, idOfFacesCurrent, numPointPerElt, ielCurrentGlobalGeo, ielCurrentGlobalSup);
          }
        }
      }
      else {
        // All contribution have been already computed on this element
        // take the next element in the stack
        if(!nextInStack.empty()) {
          ielCurrentGlobalGeo = nextInStack.top();
          updateFaceOrientedFEWithBd(feWithBd, idOfFacesCurrent, numPointPerElt, ielCurrentGlobalGeo, ielCurrentGlobalSup);
        }
      }
    }
    
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flag);
    
    // desallocate opposite finite elements
    feWithBd = firstCurrentFe;
    delete firstOppositeFe;
  }

  void LinearProblemPoissonContinuation::addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec) {
    felInt ielTmp;
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;
    
    vec.resize(size, false);
    for(felInt iface=0; iface<size; ++iface) {
      // check if this face is an inner face or a boundary
      ielTmp = idElt;
      adjElt = m_mesh[m_currentMesh]->getAdjElement(ielTmp, iface);
      
      if(!adjElt.first) {
        // not an inner face, set the contribution to done.
        vec[iface] = true;
      }
    }
  }

  void LinearProblemPoissonContinuation::updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fe, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup) {
    std::vector<felInt> elemIdPoint(numPoints, -1);
    std::vector<Point*> elemPoint(numPoints, NULL);   
    
    // get information on the opposite element
    // same as the function "setElemPoint" but here ielOppositeGlobalGeo is global
    // we assume that the supportDofMesh is the same for all unknown (like in setElemPoint)
    m_supportDofUnknown[0].getIdElementSupport(idElt, idSup);
    m_mesh[m_currentMesh]->getOneElement(idElt, elemIdPoint);
    for (felInt iPoint=0; iPoint<numPoints; ++iPoint)
      elemPoint[iPoint] = &(m_mesh[m_currentMesh]->listPoints()[elemIdPoint[iPoint]]);
   
    // update the finite elements
    fe->updateFirstDeriv(0, elemPoint);
    fe->updateBdMeasNormal();
    
    // get all the faces of the element
    if(dimension() == 2)
      m_mesh[m_currentMesh]->getAllEdgeOfElement(idElt, idOfFaces);
    else
      m_mesh[m_currentMesh]->getAllFaceOfElement(idElt, idOfFaces);
  }
}
