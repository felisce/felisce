//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNSHeat.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "DegreeOfFreedom/boundaryConditionList.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce{
  LinearProblemNSHeat::LinearProblemNSHeat():LinearProblemNS() {
    if (FelisceParam::instance().exportP1Normal ) {
      m_vecs.Init("measStar");
      m_seqVecs.Init("measStar");

      m_vecs.Init("normalField");
      m_seqVecs.Init("normalField");
    }
  }
  void LinearProblemNSHeat::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblemNS::initialize(mesh,fstransient,comm,doUseSNES);
    m_iTemp        = m_listVariable.getVariableIdList(temperature);
    m_iUnknownTemp = m_listUnknown.getUnknownIdList(temperature);
    m_gravity = FelisceParam::instance().gravity;
    m_referenceTemperature = FelisceParam::instance().referenceTemperature;
    m_t0 = FelisceParam::instance().initialTemperature;
    m_coeffOfThermalExpansion = FelisceParam::instance().coeffOfThermalExpansion;
    m_thermalDiffusion = FelisceParam::instance().thermalDiffusion;
  }
  void LinearProblemNSHeat::userAddOtherUnknowns(std::vector<PhysicalVariable>& listVariable, std::vector<std::size_t>& listNumComp) {
    m_listUnknown.push_back(temperature);
    listVariable.push_back(temperature);
    listNumComp.push_back(1);
  }
  void LinearProblemNSHeat::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    LinearProblemNS::initPerElementType(eltType,flagMatrixRHS);

    m_feTemp = m_listCurrentFiniteElement[m_iTemp];
    m_elemFieldOldTemp.initialize(DOF_FIELD,*m_feTemp,1);
    m_elemFieldAdvection.initialize(DOF_FIELD,*m_feVel,m_velocity->numComponent());

    FEL_ASSERT(m_gravity.size() == m_velocity->numComponent());
  }
  void LinearProblemNSHeat::userElementInit() {
    //By using m_elemFieldRHS, we automatically include this part in the SUPG stabilization
    for (int i(0); i<this->dimension();++i) {
      m_elemFieldRHS.setValue( m_density*(1 + m_coeffOfThermalExpansion*m_referenceTemperature)*m_gravity[i] , i );
    }
  }
  void LinearProblemNSHeat::userElementInitNaturalBoundaryCondition() {
    m_curvFeVel=m_listCurvilinearFiniteElement[m_iVelocity];
    m_stevino.initialize(DOF_FIELD,*m_curvFeVel,1);
  }

  void LinearProblemNSHeat::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    LinearProblemNS::computeElementArray(elemPoint,elemIdPoint,iel,flagMatrixRHS);
    m_feTemp->updateFirstDeriv(0, elemPoint);

    std::size_t temperatureBlock = m_velocity->numComponent() + 1;
    std::size_t timeCoeff = 1./m_fstransient->timeStep;

    if ( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix ) {
      if (m_fstransient->iteration == 0) {
        std::size_t idMatrix = 1;
        //mass
        m_elementMat[idMatrix]->phi_i_phi_j(timeCoeff, *m_feTemp, temperatureBlock, temperatureBlock, 1);
        //Diffusion
        m_elementMat[idMatrix]->grad_phi_i_grad_phi_j(m_thermalDiffusion, *m_feTemp, temperatureBlock, temperatureBlock, 1);

        //Gravity term
        //We assume that we use the same finite element for velocity and temperature.
        //If it is not the case, we have to add another function to elementMatrix
        for (int i(0); i<this->dimension();++i) {
          // +\rho_0*\alpha*\vec g T
          m_elementMat[idMatrix]->phi_i_phi_j(m_density*m_coeffOfThermalExpansion*m_gravity[i],*m_feVel, i, m_velocity->numComponent()+1,1);
        }
      }

      //Advection
      m_elemFieldAdvection.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
      m_elementMat[0]->u_grad_phi_j_phi_i(1.,m_elemFieldAdvection,*m_feTemp, temperatureBlock, temperatureBlock, 1);

      //SUPG stabilization extra terms, the extra terms due to the non-zero rhs are correctly handled in linearProblemNS::computeElementArray()
      if ( m_velocity->finiteElementType() == m_pressure->finiteElementType()) {
        m_elementMat[0]->stab_supgNSHeat(m_fstransient->timeStep,FelisceParam::instance().stabSUPG,m_coeffOfThermalExpansion,m_density,m_viscosity,
                                         *m_feVel, m_elemFieldAdvection, FelisceParam::instance().typeSUPG, m_gravity);

      }
      if ( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs ) {
        // Advancing in time
        if ( m_fstransient->iteration == 1 ) {
          m_elemFieldOldTemp.setValue(m_t0,0);
        }
        else {
          m_elemFieldOldTemp.setValue(this->sequentialSolution(),*m_feTemp, iel, m_iTemp, m_ao, dof());//TODO in case of fix point iterations this has to be modified
        }
        m_elementVector[0]->source(timeCoeff,*m_feTemp,m_elemFieldOldTemp,m_velocity->numComponent()+1,1);

        // Velocity force term
        // +\rho_0*\vec g(1-\alpha*TRef)
        m_elementVector[0]->source(1., *m_feVel, m_elemFieldRHS,0,m_velocity->numComponent());
      }
    }
  }
  void LinearProblemNSHeat::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>&  /*elemIdPoint*/,felInt& /*ielSupportDof*/, int /*label*/) {
    // Check we are in the labels where the addition of the hydrostatic pressure (Stevino Pressure) is required.
    if ( std::find( FelisceParam::instance().stevinoLabels.begin(), FelisceParam::instance().stevinoLabels.end(), m_currentLabel) != FelisceParam::instance().stevinoLabels.end() )  {


      // We assume that, on this label, constant in time and space Dirichlet boundary condition is imposed on the Temperature.
      // We need this value to correctly compute the Stevino pressure.
      //
      // We loop over the boundary conditions to check the assumptions and to get the value
      BoundaryCondition* bc = nullptr;
      double value(0);
      bool foundACorrectBC = false;

      for ( int iDirBC(0); (std::size_t)iDirBC<m_boundaryConditionList.numDirichletBoundaryCondition(); ++iDirBC) {
        // Get the current Dirichlet boundary condition.
        bc=m_boundaryConditionList.Dirichlet(iDirBC);
        // Check that it is about the temperature
        if ( (std::size_t)bc->getUnknown() == m_iUnknownTemp ) {
          // Get the labels and check that this bc actually concerns the currentLabel
          const auto& lab = bc->listLabel();
          if ( std::find( lab.begin(), lab.end(), m_currentLabel) != lab.end() )  {
            // Check the type of BC is constant
            if ( bc->typeValueBC() != Constant ) {
              FEL_ERROR("Hydrostatic (Stevino) pressure is not yet available for non-constant Dirichlet boundary condition for the temperature");
            } else {
              // We can take the value
              value = bc->ValueBCInSupportDof()[0];
              foundACorrectBC = true;
            }
          }
        }
      }
      if ( ! foundACorrectBC ) {
        std::stringstream msg;
        msg<<"For some reasons, it was impossible to apply the Stevino pressure on label: "<<m_currentLabel<<std::endl;
        FEL_ERROR(msg.str().c_str());
      }

      // Filling of the elementField
      for ( int idof(0); idof<m_curvFeVel->numDof(); idof++) {
        if ( (std::size_t)idof < elemPoint.size() ) {
          m_stevino.val(0,idof)=0;
          for ( int icoor(0); icoor<this->dimension(); ++icoor ) {
            m_stevino.val(0,idof) += ( elemPoint[idof]->coor(icoor) - m_zedZeroStevino[icoor] )*m_gravity[icoor];
          }
        } else {
          if ( this->dimension() == 2 ) {
            // We are dealing with an edge is 1D, and we are using P2 finite-elements. idof must be equal to 2
            FEL_ASSERT(idof==2);
            // We don't have a point for this dof, but it simply the middle point
            m_stevino.val(0,idof) = ( 0.5 * elemPoint[0]->x() + 0.5 * elemPoint[1]->x() - m_zedZeroStevino[0] )*m_gravity[0]
              +( 0.5 * elemPoint[0]->y() + 0.5 * elemPoint[1]->y() - m_zedZeroStevino[1] )*m_gravity[1];
          } else {
            FEL_ERROR("Hydrostatic (Stevino) pressure not yet available for 3D-P2 finite-elements.");
          }
        }
      }
      // Adding it as a force term
      m_elementVectorBD[0]->f_phi_i_scalar_n( - m_density * ( 1. - m_coeffOfThermalExpansion * ( value - m_referenceTemperature ) ),*m_curvFeVel,m_stevino,0/*iblockVel*/);
    }
  }
}
