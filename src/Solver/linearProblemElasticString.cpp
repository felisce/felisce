//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemElasticString.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce
{

LinearProblemElasticString::LinearProblemElasticString():
  LinearProblem("ElasticString solver")
{}

LinearProblemElasticString::~LinearProblemElasticString() = default;


void LinearProblemElasticString::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
  LinearProblem::initialize(mesh, comm, doUseSNES);
  m_fstransient = fstransient;

  // only one variable: displacement
  std::vector<PhysicalVariable> listVariable(1);
  listVariable[0] = displacement;

  // number of components of each variable. The displacement is scalar here but the final solution
  // is a std::vector (scalar displacement * normal).
  std::vector<std::size_t> listNumComp(1);
  listNumComp[0]  = 1;

  // define unknown of the linear system.
  m_listUnknown.push_back(displacement);
  definePhysicalVariable(listVariable, listNumComp);

  // define parameters of the model
  const auto& r_instance = FelisceParam::instance();
  const double rho     = r_instance.densitySolid;
  const double young   = r_instance.young;
  const double nu      = r_instance.poisson;
  const double epsilon = r_instance.thickness;
  const double radius  = r_instance.solidRadius;
  const double tau     = m_fstransient->timeStep;

  m_mass = rho * epsilon / (tau * tau);
  m_massVel = rho * epsilon / tau;
  m_lap = young * epsilon / (2 * (1 + nu));

  if(Tools::equal(radius, 0.))
    m_0 = 0.;
  else
    m_0 = young * epsilon / (radius * radius * (1 - nu * nu));
}


void LinearProblemElasticString::initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
  (void) eltType;
  (void) flagMatrixRHS;

  m_iDisplacement = m_listVariable.getVariableIdList(displacement);
  m_feDisp = m_listCurvilinearFiniteElement[m_iDisplacement];

  // define elemField to put term u_n_1.phi_j in RHS.
  // m_dispLastSol contains the previous time step solution
  m_elemDispVel.initialize(DOF_FIELD, *m_feDisp);
  m_elemDisp.initialize(DOF_FIELD, *m_feDisp);

  // source term (the solution is scalar)
  m_elemRHS.initialize(QUAD_POINT_FIELD, *m_feDisp, 1);
}


void LinearProblemElasticString::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
  (void) elemIdPoint;
  (void) flagMatrixRHS;

  // update finite element shape functions and its first derivatives at quadrature points
  m_feDisp->updateMeasNormalContra(0, elemPoint);

  // MATRIX //
  // time scheme
  m_elementMatBD[0]->phi_i_phi_j(m_mass, *m_feDisp, 0, 0, 1);

  // 0-th order terms
  m_elementMatBD[0]->phi_i_phi_j(m_0, *m_feDisp, 0, 0, 1);

  // laplacian
  m_elementMatBD[0]->grad_phi_i_grad_phi_j(m_lap, *m_feDisp, 0, 0, 1);

  // RHS //
  // time scheme
  // rho * epsilon / tau^2 * (2*d^{n-1} - d^{n-2})
  m_elemDisp.setValue(externalVec(0), *m_feDisp, iel, m_iDisplacement, m_ao, dof());
  m_elementVectorBD[0]->source(m_mass, *m_feDisp, m_elemDisp, 0, 1);
}


void LinearProblemElasticString::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, const std::vector<Point*>& elemNormal, const std::vector< std::vector<Point*> >& elemTangent, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
  (void) elemIdPoint;
  (void) elemPoint;
  (void) elemNormal;
  (void) elemTangent;
  (void) iel;
  (void) flagMatrixRHS;
}
}
