//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Smaldone
//

#ifndef _CARDIACCYCLE_HPP
#define _CARDIACCYCLE_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/geoElement.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "Solver/RISModel.hpp"
#include "Tools/analyticFunction.hpp"

namespace felisce 
{
class CardiacCycle {
  FelisceTransient::Pointer m_fstransient;
  RISModel* m_ris;

  //! Linear Problem
  std::vector<LinearProblem*> m_linearProblem;
  double m_duration_ejection;
  double m_duration_isovol_relax;
  double m_duration_filling;
  double m_duration_isovol_contract;
  std::vector<double> m_pressure_in;
  std::vector<double> m_inflow;
  std::vector<int> _CardiacCycleLabel;
  std::vector<int> m_closed_surf;
  std::vector<double> m_p0_isovol_relax;
  double m_penalisationParam;
  double m_t0_ejection;
  double m_t0_isovol_relax;
  bool m_flag_valve_was_open ;
  double m_dt;
public:
  //!Constructor.
  CardiacCycle(FelisceTransient::Pointer fstransient,
                RISModel* risModel, std::vector<LinearProblem*> linearProblem);
  //!Destructor.
  ~CardiacCycle() = default;
  void ApplyCardiacCycleInput();
  void InflowCondition();
  void PressureInput();



  // access function
  std::vector<double> & Pressure_in() {
    return  m_pressure_in;
  }
  std::vector<double> & inflow() {
    return m_inflow;
  }
  double& penalisationParam() {
    return m_penalisationParam;
  }
  double& t0_ejection() {
    return m_t0_ejection;
  }
};

}

#endif

