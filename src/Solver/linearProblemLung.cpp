//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "Solver/linearProblemLung.hpp"

namespace felisce
{

void LinearProblemLung::computeElementArrayLung(Vec & divVel,Mat & divBasis, PetscVector&  rhsMinus ,int currentLabel,PetscVector&  prevVel, PetscVector&  prevDis, double currPre,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS)
{
  (void) divVel;
  (void) divBasis;
  (void) rhsMinus;
  (void) currentLabel;
  (void) prevVel;
  (void) prevDis;
  (void) currPre;
  (void) elemPoint;
  (void) elemIdPoint;
  (void) iel;
  (void) flagMatrixRHS;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::computeElementArrayLungDisplacement(PetscVector&  emphysemaCoeff,PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis, PetscVector&  rhsMinus,PetscVector&  localExpansion, PetscVector&  pCouplingB , PetscVector&  volumeB,int currentLabel,PetscVector&  prevVel, PetscVector&  prevDis, PetscVector&  displReal, double currPre,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                                        felInt& iel, FlagMatrixRHS flagMatrixRHS) 
{
  (void) emphysemaCoeff;
  (void) volReg;
  (void) volEvol;
  (void) divVel;
  (void) divBasis;
  (void) rhsMinus;
  (void) localExpansion;
  (void) pCouplingB;
  (void) volumeB;
  (void) currentLabel;
  (void) prevVel;
  (void) prevDis;
  (void) displReal;
  (void) currPre;
  (void) elemPoint;
  (void) elemIdPoint;
  (void) iel;
  (void) flagMatrixRHS;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::computeElementArrayLungPressure(PetscVector&  emphysemaCoeff, PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis, PetscVector&  rhsMinus,PetscVector&  localExpansion,PetscVector&  pCouplingB, PetscVector&  volumeB ,int currentLabel,PetscVector&  prevVel, PetscVector&  prevDis, double currPre,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                                    felInt& iel, FlagMatrixRHS flagMatrixRHS) 
{
  (void) emphysemaCoeff;
  (void) volReg;
  (void) volEvol;
  (void) divVel;
  (void) divBasis;
  (void) rhsMinus;
  (void) localExpansion;
  (void) pCouplingB;
  (void) volumeB;
  (void) currentLabel;
  (void) prevVel;
  (void) prevDis;
  (void) currPre;
  (void) elemPoint;
  (void) elemIdPoint;
  (void) iel;
  (void) flagMatrixRHS;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::computeElementLungVolume(int currentLabel, PetscVector&  volReg,PetscVector&  displ,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                              felInt& iel, FlagMatrixRHS flagMatrixRHS) 
{
  (void) currentLabel;
  (void) volReg;
  (void) displ;
  (void) elemPoint;
  (void) elemIdPoint;
  (void) iel;
  (void) flagMatrixRHS;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::assembleMatrixRHSlung(Vec & divVel,Mat & divBasis, PetscVector&  rhsMinus,PetscVector&  prevVel, PetscVector&  prevDis, double currPre, int rank, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  // Use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // Define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);
    initElementArray();

    // allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // Virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // Used by user to add specific term (source term for example with elemField).
    userElementInit();

    // Second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      // By default this virtual defines variable m_currentLabel: (m_currentLabel=label).
      // We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);

      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        FEL_ASSERT(!m_elementVector.empty());

        // if(!FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
          // Return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);

          // Clear elementary matrices
          for (std::size_t j = 0, size = m_matrices.size(); j < size ; j++)
            m_elementMat[j]->zero();

          // Clear elementary std::vector.
          for (std::size_t j = 0, size = m_vectors.size(); j < size ; j++)
            m_elementVector[j]->zero();

          // Function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          if (FelisceParam::instance(this->instanceIndex()).NSequationFlag == 1 && FelisceParam::instance(this->instanceIndex()).characteristicMethod != 0) {
            // Use method of characteristics for N-S
            computeElementArrayCharact(elemPoint, elemIdPoint, ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
          } else {
            computeElementArrayLung(divVel,divBasis,rhsMinus,currentLabel,prevVel, prevDis, currPre, elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
          }

          // Compute specific term of users.
          userElementCompute(elemPoint, elemIdPoint, ielSupportDof);

          // Add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHS(ielSupportDof, ielSupportDof, flagMatrixRHS);
        // } else {
        //   m_duplicateSupportDofAssemblyLoop(rank, eltType, numElement[eltType], elemPoint, elemIdPoint, flagMatrixRHS);
        // }

        numElement[eltType]++;
      }
    }
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly loop for LumpedModelBC in case of NS model with implicit implementation
  if (FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size()>0 &&  FelisceParam::instance(this->instanceIndex()).model == "NS") {
    for (std::size_t iLabel = 0; iLabel < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); iLabel++) {
      if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[iLabel] == 2) // use a Enum here... 08/13 VM
        assembleMatrixImplLumpedModelBC(rank, iLabel);
    }
  }

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    // real assembling of the matrix manage by PETsC.
    // TODO Jeremie 01 02 2012: assemble multiple matrix
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }

  // call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);
  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
    // real assembling of the right hand side (RHS).
    for (std::size_t i = 0; i < m_vectors.size(); i++) {
      m_vectors[i].assembly();
    }
  }
  // call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::assembleMatrixRHSlungDisplacement(PetscVector&  emphysemaCoeff, PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis, PetscVector&  rhsMinus, PetscVector&  localExpansion, PetscVector&  pCouplingB, PetscVector&  volumeB,PetscVector&  prevVel, PetscVector&  prevDis, PetscVector&  displReal, double currPre, int rank, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  // Use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);
    initElementArray();

    // allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // used by user to add specific term (source term for example with elemField).
    userElementInit();

    // second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      // By default this virtual defines variable m_currentLabel: (m_currentLabel=label).
      // We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);

      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        FEL_ASSERT(!m_elementVector.empty());

        // if(!FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
          // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);

          // clear elementary matrices
          for (std::size_t j = 0, size = m_matrices.size(); j < size ; j++)
            m_elementMat[j]->zero();

          // clear elementary std::vector.
          for (std::size_t j = 0, size = m_vectors.size(); j < size ; j++)
            m_elementVector[j]->zero();

          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          if (FelisceParam::instance(this->instanceIndex()).NSequationFlag == 1 && FelisceParam::instance(this->instanceIndex()).characteristicMethod != 0) {
            // use method of characteristics for N-S
            computeElementArrayCharact(elemPoint, elemIdPoint, ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
          } else {
            computeElementArrayLungDisplacement(emphysemaCoeff, volReg, volEvol,divVel,divBasis,rhsMinus,localExpansion,pCouplingB, volumeB,currentLabel,prevVel, prevDis,displReal, currPre, elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
          }

          // compute specific term of users.
          userElementCompute(elemPoint, elemIdPoint, ielSupportDof);

          // add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHS(ielSupportDof, ielSupportDof, flagMatrixRHS);
        // } else {
        //   m_duplicateSupportDofAssemblyLoop(rank, eltType, numElement[eltType], elemPoint, elemIdPoint, flagMatrixRHS);
        // }

        numElement[eltType]++;
      }
    }
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly loop for LumpedModelBC in case of NS model with implicit implementation
  if (FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size()>0 &&  FelisceParam::instance(this->instanceIndex()).model == "NS") {
    for (std::size_t iLabel = 0; iLabel < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); iLabel++) {
      if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[iLabel] == 2) // use a Enum here... 08/13 VM
        assembleMatrixImplLumpedModelBC(rank, iLabel);
    }
  }

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    // real assembling of the matrix manage by PETsC.
    // todo Jeremie 01 02 2012: assemble multiple matrix
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }

  // call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);
  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
    // real assembling of the right hand side (RHS).
    for (std::size_t i = 0; i < m_vectors.size(); i++) {
      m_vectors[i].assembly();
    }
  }
  // call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::assembleMatrixRHSlungPressure(PetscVector&  emphysemaCoeff,PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis, PetscVector&  rhsMinus,PetscVector&  localExpansion, PetscVector&  pCouplingB, PetscVector&  volumeB,PetscVector&  prevVel, PetscVector&  prevDis, double currPre, int rank, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);
    initElementArray();

    // allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // used by user to add specific term (source term for example with elemField).
    userElementInit();

    // second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      // By default this virtual defines variable m_currentLabel: (m_currentLabel=label).
      // We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);

      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        FEL_ASSERT(!m_elementVector.empty());

        // if(!FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
          // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);

          // clear elementary matrices
          for (std::size_t j = 0, size = m_matrices.size(); j < size ; j++)
            m_elementMat[j]->zero();

          // clear elementary std::vector.
          for (std::size_t j = 0, size = m_vectors.size(); j < size ; j++)
            m_elementVector[j]->zero();

          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          if (FelisceParam::instance(this->instanceIndex()).NSequationFlag == 1 && FelisceParam::instance(this->instanceIndex()).characteristicMethod != 0) {
            // use method of characteristics for N-S
            computeElementArrayCharact(elemPoint, elemIdPoint, ielSupportDof, eltType, numElement[eltType], flagMatrixRHS);
          } else
            computeElementArrayLungPressure(emphysemaCoeff, volReg, volEvol,divVel,divBasis,rhsMinus,localExpansion,pCouplingB,volumeB,currentLabel,prevVel, prevDis, currPre, elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);

          // compute specific term of users.
          userElementCompute(elemPoint, elemIdPoint, ielSupportDof);

          // add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHS(ielSupportDof, ielSupportDof, flagMatrixRHS);
        // } else {
        //   m_duplicateSupportDofAssemblyLoop(rank, eltType, numElement[eltType], elemPoint, elemIdPoint, flagMatrixRHS);
        // }

        numElement[eltType]++;
      }
    }
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly loop for LumpedModelBC in case of NS model with implicit implementation
  if (FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size()>0 &&  FelisceParam::instance(this->instanceIndex()).model == "NS") {
    for (std::size_t iLabel = 0; iLabel < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); iLabel++) {
      if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[iLabel] == 2) // use a Enum here... 08/13 VM
        assembleMatrixImplLumpedModelBC(rank, iLabel);
    }
  }

  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    // real assembling of the matrix manage by PETsC.
    // todo Jeremie 01 02 2012: assemble multiple matrix
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }

  // call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);
  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
    // real assembling of the right hand side (RHS).
    for (std::size_t i = 0; i < m_vectors.size(); i++) {
      m_vectors[i].assembly();
    }
  }
  // call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::computeVolumeLungRegions(PetscVector&  volReg, PetscVector&  displ, int rank, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    // defineFiniteElement(eltType);
    // initElementArray();

    // second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;


      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        FEL_ASSERT(!m_elementVector.empty());

        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);
        //
        computeElementLungVolume(currentLabel, volReg, displ, elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);


        numElement[eltType]++;
      }
    }
  }
  // desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::computeVolumeLungRegions2(PetscVector&  volReg,PetscVector&  displ,int rank, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define all current finite element use in the problem from data
    // file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);
    initElementArray();

    // allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHS(flagMatrixRHS);

    // virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType(eltType, flagMatrixRHS);

    // used by user to add specific term (source term for example with elemField).
    userElementInit();

    // second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;


      // By default this virtual defines variable m_currentLabel: (m_currentLabel=label).
      // We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel, flagMatrixRHS);

      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        FEL_ASSERT(!m_elementVector.empty());

        // if(!FelisceParam::instance(this->instanceIndex()).duplicateSupportDof) {
          // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);

          // clear elementary matrices
          for (std::size_t j = 0, size = m_matrices.size(); j < size ; j++)
            m_elementMat[j]->zero();

          // clear elementary std::vector.
          for (std::size_t j = 0, size = m_vectors.size(); j < size ; j++)
            m_elementVector[j]->zero();

          computeElementLungVolume(currentLabel, volReg, displ, elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);

          // compute specific term of users.
          userElementCompute(elemPoint, elemIdPoint, ielSupportDof);

          // add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHS(ielSupportDof, ielSupportDof, flagMatrixRHS);
        // } else {
        //   m_duplicateSupportDofAssemblyLoop(rank, eltType, numElement[eltType], elemPoint, elemIdPoint, flagMatrixRHS);
        // }

        numElement[eltType]++;
      }
    }
    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  // Assembly loop for LumpedModelBC in case of NS model with implicit implementation
  if (FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size()>0 &&  FelisceParam::instance(this->instanceIndex()).model == "NS") {
    for (std::size_t iLabel = 0; iLabel < FelisceParam::instance(this->instanceIndex()).lumpedModelBCLabel.size(); iLabel++) {
      if(FelisceParam::instance(this->instanceIndex()).lumpedModelBCAlgo[iLabel] == 2) // use a Enum here... 08/13 VM
        assembleMatrixImplLumpedModelBC(rank, iLabel);
    }
  }

  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
    // real assembling of the matrix manage by PETsC.
    // todo Jeremie 01 02 2012: assemble multiple matrix
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }

  // call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);
  if(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
    // real assembling of the right hand side (RHS).
    for (std::size_t i = 0; i < m_vectors.size(); i++) {
      m_vectors[i].assembly();
    }
  }
  // call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::normalLungMesh(PetscVector& modelVec, std::string modeLung, PetscVector&  normalVec, felInt idVecVariable){

  this->initPetscVectors();

  m_seqVecs.Init("normalField");
  m_seqVecs.Init("measStar");
  m_vecs.Init("normalField");
  m_vecs.Init("measStar");

  m_vecs.Get("normalField").duplicateFrom(modelVec);
  m_vecs.Get("measStar").duplicateFrom(modelVec);

  gatherVector(m_vecs.Get("normalField"),m_seqVecs.Get("normalField"));
  gatherVector(m_vecs.Get("measStar"),m_seqVecs.Get("measStar"));

  m_vecs.Get("normalField").set(0.0);
  m_vecs.Get("measStar").set(0.0);

  std::cout << "ENTER NORMAL COMPUTATION" << std::endl;

  std::cout << "size lis bc = " << m_boundaryConditionList.size() << std::endl;

  std::vector<felInt> m_nLabels;

  if(modeLung=="displacement"){
    for (std::size_t i=0; i < m_boundaryConditionList.numDirichletBoundaryCondition(); i++){
      std::cout << "dirichlet i = " << i << std::endl;
      BoundaryCondition* BCn = m_boundaryConditionList.Dirichlet(i);
      for(auto it_labelNumber = BCn->listLabel().begin(); it_labelNumber != BCn->listLabel().end(); it_labelNumber++) {
        m_nLabels.push_back(*it_labelNumber);
        std::cout << "het h " << std::endl;
      }
    }
  }

  if(modeLung=="pressure"){
    for (std::size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); i++){
      std::cout << "neumannNormal i = " << i << std::endl;
      BoundaryCondition* BCn = m_boundaryConditionList.NeumannNormal(i);
      for(auto it_labelNumber = BCn->listLabel().begin(); it_labelNumber != BCn->listLabel().end(); it_labelNumber++) {
        m_nLabels.push_back(*it_labelNumber);
        std::cout << "het h " << std::endl;
      }
    }
  }

  std::cout << "Gets to normal computation " << std::endl;

  computeNormalField(m_nLabels, idVecVariable);

  normalVec.duplicateFrom(m_seqVecs.Get("normalField"));
  normalVec.copyFrom(m_seqVecs.Get("normalField"));
  VecView(normalVec.toPetsc(),PETSC_VIEWER_STDOUT_WORLD);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::applyBoundaryConditionMecha(double ratioM,PetscVector& volCurr,PetscVector& volFRC,PetscVector& volTLC, double dt, double coeffReaction ,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_IEL;
  IGNORE_UNUSED_ELEM_POINT;
  IGNORE_UNUSED_ELEM_ID_POINT;
  BoundaryCondition* BC = nullptr;
  CurvilinearFiniteElement* fe = nullptr;
  int iUnknown = -1;
  int iblock = -1;
  int cptComp = -1;

  FEL_ASSERT(!m_elementVectorBD.empty());

  //initialization

  //used for mechanically induced ventilation in the lung model
  //add surface integral terms for diaphragm and chest rection

  //     double cl, cr;
  double coeff;
  int nbLobes;
  volCurr.getSize(&nbLobes);
  //     PetscScalar Vprev[nbLobes];
  PetscScalar Vcurr[nbLobes];
  PetscScalar Vtlc[nbLobes];
  PetscScalar Vfrc[nbLobes];
  PetscInt ix[nbLobes];
  for(int i=0;i<nbLobes;i++) {
    ix[i]=i;
  }
  volCurr.getValues(nbLobes,ix,Vcurr);
  volTLC.getValues(nbLobes,ix,Vtlc);
  volFRC.getValues(nbLobes,ix,Vfrc);

  for (std::size_t iNN = 0; iNN < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); iNN++) {
    BC = m_boundaryConditionList.NeumannNormal(iNN);
    int iVariable = m_listVariable.getVariableIdList(BC->getVariable().physicalVariable());
    iUnknown = BC->getUnknown();
    fe = m_listCurvilinearFiniteElement[iVariable];
    FEL_ASSERT(fe);
    for(auto it_labelNumber = BC->listLabel().begin();
          it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
      if(*it_labelNumber == m_currentLabel) {

        int iL=m_currentLabel-1;

        double ratioVol=0;
        ratioVol=std::abs((Vcurr[iL]*1000000+Vfrc[iL])/Vtlc[iL]);
        if(ratioVol<ratioM){
          coeff=coeffReaction*(Vtlc[iL]-Vfrc[iL])*(Vcurr[iL]*1000000)/(std::abs(Vtlc[iL]-Vcurr[iL]*1000000-Vfrc[iL]));   //conversion of the current volume into milliliters, unit in which Vfrc and Vtlc are provided
        } else{
          const double alph=Vtlc[iL]-Vfrc[iL];
          const double Vc=ratioM*Vtlc[iL]-Vfrc[iL];
          const double a=std::abs(coeffReaction*alph*alph/((alph-Vc)*(alph-Vc)));
          const double b=coeffReaction*alph*Vc/(alph-Vc)-a*Vc;
          coeff=a*Vcurr[iL]*1000000+b;
        }

        //    cl=dt*dt*coeff;
        //    cr=-dt*coeff;
        //dt useless, should be re;oved fro; the argument (Nicolas)
        //following lines to avoid warning
        double valTmp=0;
        valTmp=dt;
        dt=0;
        dt=valTmp;

        cptComp = 0;
        if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs){
          for(auto it_comp = BC->getComp().begin(); it_comp != BC->getComp().end(); ++it_comp) {
            iblock = m_listUnknown.getBlockPosition(iUnknown,*it_comp);
            m_elementMatBD[3]->setZerosLung(0,*fe,iblock,iblock,m_listVariable[iVariable].numComponent());  //to ensure the matrix is null before computation
            m_elementMatBD[3]->phi_i_phi_j(coeff,*fe,iblock,iblock,m_listVariable[iVariable].numComponent());
            cptComp++;
          }
        } else if (flagMatrixRHS == FlagMatrixRHS::only_rhs){
          for(auto it_comp = BC->getComp().begin(); it_comp != BC->getComp().end(); ++it_comp) {
            iblock = m_listUnknown.getBlockPosition(iUnknown,*it_comp);
            m_elementMatBD[3]->setZerosLung(0,*fe,iblock,iblock,m_listVariable[iVariable].numComponent());     //to ensure the matrix is null before computation
            m_elementMatBD[3]->phi_i_phi_j(coeff,*fe,iblock,iblock,m_listVariable[iVariable].numComponent());
            cptComp++;
          }
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::assembleMatrixRHSBoundaryConditionMecha(double ratioM, PetscVector& volCurr,PetscVector& volFRC,PetscVector& volTLC, double dt, double coeffReaction ,int rank, FlagMatrixRHS flagMatrixRHS, const int idBCforLinCombMethod) 
{
  IGNORE_UNUSED_RANK;
  ElementType eltType;           //geometric element type in the mesh.
  int numPointPerElt = 0;        //number of points per geometric element.
  int currentLabel = 0;          //number of label domain.
  felInt numEltPerLabel = 0;     //number of element for one label and one eltType.
  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // I need to define the CurrentFEWithBd to correctly compute the stress tensor
  if(m_meshLocal[m_currentMesh]->statusFaces() == false) {
    m_meshLocal[m_currentMesh]->buildFaces();
  }
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  for (std::size_t ielt = 0; ielt < bagElementTypeDomain.size(); ++ielt) {
    eltType =  bagElementTypeDomain[ielt];
    defineCurrentFiniteElementWithBd(eltType);
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // contains ids of all the support elements associated to a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  allocateVectorBoundaryConditionDerivedLinPb();

  //First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    eltType =  bagElementTypeDomainBoundary[i];
    //resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    //define all current finite element use in the problem from data
    //file configuration and allocate elemMat and elemVec (question: virtual ?).
    defineCurvilinearFiniteElement(eltType);
    initElementArrayBD();

    //allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);


    //Function (1), (2), (3) doing the same stuff for different use: allocate/initialize elementField
    // (1) virtual function use in derived problem to allocate elemenField necessary.
    initPerElementTypeBoundaryCondition(eltType, flagMatrixRHS);

    // (2) use by user to add specific term (term source for example with elemField).
    userElementInitNaturalBoundaryCondition();

    // (3) allocate Elemfield and setValue if BC = constant in space
    // if BC = function : setValue in users
    allocateElemFieldBoundaryCondition(idBCforLinCombMethod);
    //second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      //By default this virtual define variable m_currentLabel: (m_currentLabel=label).
      //We can switch on label region with that and define some parameters.
      //We can also fill the elemField allocated in initPerElementTypeBoundaryCondition()
      initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDof, eltType, numElement[eltType], flagMatrixRHS); //it updates label
      //Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];
          // clear elementary matrix.
          // todo Jeremie 01 02 2012: assemble multiple matrix
          for (std::size_t j = 0; j < m_matrices.size(); j++)
            m_elementMatBD[j]->zero();

          // clear elementary std::vector.
          for (std::size_t j = 0; j < m_vectors.size(); j++)
            m_elementVectorBD[j]->zero();

          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).initialized in (1)
          computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof,flagMatrixRHS); //it updates fe..
          // compute specific term of users (Neumann transient for example) initialized in (2)
          userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof, currentLabel);
          // apply bc initialized in (3) if constant or initialized in (1) or (2) if functions
          applyBoundaryConditionMecha(ratioM,volCurr, volFRC, volTLC, dt, coeffReaction ,elemPoint, elemIdPoint, ielSupportDof, flagMatrixRHS);
          // add values of elemMat in the global matrix: m_matrices[0].
          setValueMatrixRHSBD(ielSupportDof, ielSupportDof, flagMatrixRHS);
        }
        numElement[eltType]++;
      }
    }
    //allocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }
  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
    //real assembling of the matrix manage by PETsC.
    for (std::size_t i = 0; i < m_matrices.size(); i++)
      m_matrices[i].assembly(MAT_FINAL_ASSEMBLY);
  }

  //useless  - to avoid warning linked  to the fact prevDis is not used - to be corrected (Nicolas)
  //    VecAYPX(prevDis,0,prevDis);
  ////////////////////////////////////////////

  //call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbosity);
  if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
    //real assembling of the right hand side (RHS).
    for (std::size_t index = 0; index < m_vectors.size(); index++) {
      m_vectors[index].assembly();
    }
  }

  //call with high level of verbose to print right hand side in matlab format.
  writeRHSForMatlab(m_verbosity);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::applyBCMecha(double ratioM, PetscVector& volCurr,PetscVector& volFRC,PetscVector& volTLC, double dt, double coeffReaction, const int & applicationOption, int rank, FlagMatrixRHS flagMatrixRHSEss, FlagMatrixRHS flagMatrixRHSNat,
                                  const int idBCforLinCombMethod, bool doPrintBC,
                                  ApplyNaturalBoundaryConditionsType do_apply_natural) {
  if (doPrintBC && m_verbosity > 2) {
    PetscPrintf(MpiInfo::petscComm(), "\n/============== Information about boundary condition.===============/\n");
    m_boundaryConditionList.print(m_verbosity);
  }
  // In some dynamic problems we might want to be able to skip natural conditions, hence this flag.
  // Its role is absolutely not the same as the other flag addedBoundaryFlag: in a same problem
  // FelisceParam::instance(this->instanceIndex()).addedBoundaryFlag is probably the same all the way whereas do_apply_natural is deemed to be
  // true in the first time iteration and then false otherwise in some time schemes.
  if (do_apply_natural == ApplyNaturalBoundaryConditions::yes) {
    // Natural boundary condition (Neumann, Neumann Normal, Robin, RobinNormal, EmbedFSI, Natural LumpedModelBC, Backflow stabilization for now).
    if (m_boundaryConditionList.NaturalBoundaryCondition()) {
      // Assemble Matrix and/or RHS (depends on flagMatrixRHSNat)
      m_elementMat[3]->zero();
      assembleMatrixRHSBoundaryConditionMecha(ratioM, volCurr, volFRC, volTLC, dt, coeffReaction,rank, flagMatrixRHSNat, idBCforLinCombMethod);
    }
  }

  // Essential boundary condition (Dirichlet, Essential lumpedModelBC).
  if (m_boundaryConditionList.EssentialBoundaryCondition() || FelisceParam::instance(this->instanceIndex()).useEssDerivedBoundaryCondition)
    applyEssentialBoundaryCondition(applicationOption, rank, flagMatrixRHSEss);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::computeNormalField(const std::vector<int> & labels, felInt idVecVariable) 
{
  m_auxiliaryInts.resize(1);
  m_auxiliaryInts[0]=idVecVariable;//for instance m_iVelocity;
  /*!
    For each dof d of the surface the sum: \f$\sum_{K} m(K)\vec n(K)\f$ is computed.
    - K are all the boundary elements that share the dof d.
    - m(K) is the measure of the element
    - n(K) is the P0 normal defined on the element
    */
  this->assemblyLoopBoundaryGeneral(&LinearProblemLung::normalFieldComputer,labels,&LinearProblemLung::initPerETNormVel,&LinearProblemLung::updateFeNormVel);

  m_vecs.Get("normalField").assembly();
  m_vecs.Get("measStar").assembly();

  /// Than the vector is divided by \f$\sum_{K} m(K)\f$ to obtain the P1 field.
  pointwiseDivide(m_vecs.Get("normalField"), m_vecs.Get("normalField"), m_vecs.Get("measStar"));
  gatherVector( m_vecs.Get("normalField"), m_seqVecs.Get("normalField") );

  /// using normalizeComputer we normalize the result.
  this->assemblyLoopBoundaryGeneral(&LinearProblemLung::normalizeComputer,labels,&LinearProblemLung::initPerETNormVel,&LinearProblemLung::updateFeNormVel);

  m_vecs.Get("normalField").assembly();
  gatherVector( m_vecs.Get("normalField"), m_seqVecs.Get("normalField") );
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::normalFieldComputer(felInt ielSupportDof) 
{
  double value;
  for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numDof(); iSurfDof++) {
    double feMeas(m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->measure());
    for ( std::size_t iCoor(0); iCoor <  ( std::size_t ) m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numCoor(); iCoor++) {
      felInt iDofGlob;
      dof().loc2glob( ielSupportDof, iSurfDof, m_auxiliaryInts[0], iCoor, iDofGlob);
      AOApplicationToPetsc(m_ao,1,&iDofGlob);
      value=0.0;
      for ( std::size_t iQuadBd(0.0) ; iQuadBd <  ( std::size_t )m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numQuadraturePoint(); iQuadBd++) {
        value += m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->weightMeas( iQuadBd ) * m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->normal[iQuadBd](iCoor);
      }
      m_vecs.Get("normalField").setValue(iDofGlob, value, ADD_VALUES);
      m_vecs.Get("measStar").setValue(iDofGlob, feMeas, ADD_VALUES);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::updateFeNormVel(const std::vector<Point*>& elemPoint,const std::vector<int>& /*elemIdPoint*/)
{
  m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->updateMeasNormal(0,elemPoint);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLung::normalizeComputer(felInt ielSupportDof)
{
  const int numComp = m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numCoor( );
  const int numDof  = m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numDof( );

  std::vector< double > norm(numDof,0.0);
  std::vector< std::vector< double > > NN(numDof, std::vector< double >(numComp, 0.0));
  for ( int idof = 0; idof < numDof; idof++ ) {
    for ( int icomp = 0; icomp < numComp; icomp++ ) {
      int iDofGlob;
      double value;
      dof().loc2glob( ielSupportDof, idof, m_auxiliaryInts[0], icomp, iDofGlob);
      AOApplicationToPetsc(m_ao, 1, &iDofGlob);
      m_seqVecs.Get("normalField").getValues(1, &iDofGlob, &value);
      norm[idof] += value*value;
      NN[idof][icomp] = value;
    }
    norm[idof] = std::sqrt( norm[idof] );
    for ( int icomp = 0; icomp < numComp; icomp++ )
      NN[idof][icomp] = NN[idof][icomp]/norm[idof];
  }
  for ( int idof = 0; idof < numDof; idof++ ) {
    for ( int icomp = 0; icomp < numComp; icomp++ ) {
      int iDofGlob;
      dof().loc2glob( ielSupportDof, idof, m_auxiliaryInts[0], icomp, iDofGlob);
      AOApplicationToPetsc(m_ao, 1, &iDofGlob);
      m_vecs.Get("normalField").setValue(iDofGlob, NN[idof][icomp], INSERT_VALUES);
    }
  }
}

}
