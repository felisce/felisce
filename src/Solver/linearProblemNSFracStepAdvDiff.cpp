//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNSFracStepAdvDiff.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemNSFracStepAdvDiff::LinearProblemNSFracStepAdvDiff():
    LinearProblem("Navier-Stokes Fractional Step: Adv-Diff"),
    m_viscosity(0.),
    m_density(0.),
    m_explicitAdvection(0.),
    m_buildTeporaryMatrix(false),
    m_bdf(nullptr),
    allocateSeqVec(false),
    allocateSeqVecExt(false),
    allocateSeqVecIncremental(false)
  {}

  LinearProblemNSFracStepAdvDiff::~LinearProblemNSFracStepAdvDiff() {
    if(m_buildTeporaryMatrix)
      m_matrix.destroy();
    m_bdf = nullptr;
    if (allocateSeqVec) {
      m_seqBdfRHS.destroy();
    }
    if (allocateSeqVecExt) {
      m_solExtrapol.destroy();
    }
  }

  void LinearProblemNSFracStepAdvDiff::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;
      
    listVariable.push_back(velocity);
    listNumComp.push_back(this->dimension());
      
    listVariable.push_back(pressure);
    listNumComp.push_back(1);

    if(FelisceParam::instance().useALEformulation > 0 ) {
      FEL_CHECK(FelisceParam::instance().useALEformulation == 1,"Only standard ALE scheme implemented");
      listVariable.push_back(displacement);
      listNumComp.push_back(this->dimension());
    }

    //define unknown of the linear system.
    m_listUnknown.push_back(velocity);
    definePhysicalVariable(listVariable,listNumComp);

    m_viscosity = FelisceParam::instance().viscosity;
    m_density = FelisceParam::instance().density;
    m_useSymmetricStress = FelisceParam::instance().useSymmetricStress;
  }

  void LinearProblemNSFracStepAdvDiff::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    m_iPressure = m_listVariable.getVariableIdList(pressure);
    m_feVel = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];
    m_velocity = &m_listVariable[m_iVelocity];
    m_pressure = &m_listVariable[m_iPressure];

    /* if (m_explicitAdvection) {
       m_iVelocityAdvection = m_listVariable.getVariableIdList(velocityAdvection);
       m_feVelAdv = m_listCurrentFiniteElement[m_iVelocityAdvection];
       m_velocityAdvection = &m_listVariable[m_iVelocityAdvection];
       m_elemFieldAdv.initialize(DOF_FIELD,*m_feVelAdv,this->dimension());
       }
    */

    m_elemFieldRHS.initialize(DOF_FIELD,*m_feVel,this->dimension());
    m_elemFieldPres.initialize(DOF_FIELD,*m_fePres,1);
    m_elemFieldPresDelta.initialize(DOF_FIELD,*m_fePres,1);

    // Bdf
    m_elemFieldRHSbdf.initialize(DOF_FIELD,*m_feVel,this->dimension());
    m_elemFieldExt.initialize(DOF_FIELD,*m_feVel,this->dimension());

    //=========
    //   ALE
    //=========
    if(FelisceParam::instance().useALEformulation > 0) {

      m_iDisplacement = m_listVariable.getVariableIdList(displacement);
      m_displacement = &m_listVariable[m_iDisplacement];

      m_elemFieldVelMesh.initialize(DOF_FIELD,*m_feVel,this->dimension());

      numDofExtensionProblem = m_externalDof[1]->numDof();

      m_petscToGlobal1.resize(numDofExtensionProblem);
      m_petscToGlobal2.resize(numDofExtensionProblem);
      m_auxvec.resize(numDofExtensionProblem);

      for (int i=0; i<numDofExtensionProblem; i++) {
        m_petscToGlobal1[i]=i;
        m_petscToGlobal2[i]=i;
      }
      AOApplicationToPetsc(m_externalAO[1],numDofExtensionProblem,m_petscToGlobal1.data());
      AOApplicationToPetsc(m_ao,numDofExtensionProblem,m_petscToGlobal2.data());

      if ( m_fstransient->iteration > 0) {
        if ( m_fstransient->iteration == 1) {
          m_beta.duplicateFrom(m_solExtrapol);
        }

        //=========
        //   ALE
        //=========
        // Construction of m_beta = u^{n-1} - w^{n}
        // Recall that externalVec(1) = - w^{n}
        m_beta.copyFrom(m_solExtrapol);
        externalVec(1).getValues(numDofExtensionProblem,m_petscToGlobal1.data(),m_auxvec.data());
        m_beta.setValues(numDofExtensionProblem,m_petscToGlobal2.data(),m_auxvec.data(),ADD_VALUES);
      }
    }
  }

  void LinearProblemNSFracStepAdvDiff::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;

    // note:
    m_feVel->updateFirstDeriv(0, elemPoint);
    m_fePres->updateFirstDeriv(0, elemPoint);

    // in the NSFracStepExplicitModel, the unknowns are:
    // velocityAdv[0], velocity[1], Pressure[2],
    // otherwise: velocity[0], Pressure[1]/
    //if (m_explicitAdvection) {
    //  m_feVelAdv->updateFirstDeriv(0, elemPoint);
    //}

    double coef = m_density/m_fstransient->timeStep;

    if ( m_feVel->measure() < 0. ) {
      int iglo;
      ISLocalToGlobalMappingApply(this->m_mappingElem[m_currentMesh],1,&iel,&iglo);
      FEL_ERROR("Error: Element of negative measure.");
    }


    if ( (m_fstransient->iteration == 0 &&  FelisceParam::instance().useALEformulation == 0 ) ||
         (m_fstransient->iteration > 0  &&  FelisceParam::instance().useALEformulation > 0 ) ) {

      if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix) ) {
        //================================
        //            matrix
        //================================

        if (m_useSymmetricStress) {
          // \eta \int \epsilon u^{n+1} : \epsilon v
          m_elementMat[0]->eps_phi_i_eps_phi_j(2*m_viscosity,*m_feVel, 0, 0, this->dimension());
        } else {
          // \eta \int \nabla u^{n+1} \nabla v
          m_elementMat[0]->grad_phi_i_grad_phi_j(m_viscosity, *m_feVel, 0, 0, this->dimension());
        }

        // \dfrac{\rho}{\dt} u^{n+1} v
        // 1/ no bdf :
        // m_elementMat[0]->phi_i_phi_j(coef,*m_feVel,0,0,this->dimension());
        // 2/ bdf :
        m_elementMat[0]->phi_i_phi_j(m_bdf->coeffDeriv0()*coef,*m_feVel,0,0,this->dimension());

        //================================
        //         FE stabilization
        //================================

        if (this->verbosity()>3) {
          if ( m_velocity->finiteElementType() == m_pressure->finiteElementType())
            std::cout << "The FE stabilization term has to be implemented in LinearProblemNSFracStepAdvDiff class" << std::endl;
        }
      }

    }

    if ( m_fstransient->iteration > 0) {

      if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix) ) {

        switch(FelisceParam::instance().NSequationFlag) {
        case 0:
          //nothig to do: Stoke's model
          break;

        case 1:
          //================================
          //            matrix
          //================================

          //if (!m_explicitAdvection) {
          // convective term


          // 1/ no bdf
          // m_elemFieldAdv.setValue(m_seqSol, *m_feVel, iel, m_iVelocity, m_ao, m_dof);
          // m_elementMat[0]->u_grad_phi_j_phi_i(m_density,m_elemFieldAdv,*m_feVel,0,0,this->dimension());


          // Temam's operator to stabilize the convective term
          // --
          // Convective term in energy balance :
          // ... + \rho/2 \intGamma (u^n.n)|u^n+1|^2 - \rho/2 \intOmega (\nabla . u^n) |u^n+1|^2 + ...
          // Add \rho/2 \intOmega (\nabla . u^n) u^n+1 v to delete the second term, which is consistent with the exact solution (divergence = 0)
          // The flux of kinetic energy still remains...
          // m_elementMat[0]->div_u_phi_j_phi_i(0.5,m_density,m_elemFieldExt,*m_feVel,0,0,this->dimension());
          //}


          if(FelisceParam::instance().useALEformulation > 0) {
           //=========
            //   ALE
            //=========
            // m_elemFieldAdv -> m_beta,   where m_beta = u^{n-1} - w^{n} is defined in userElementInit()
            m_elemFieldExt.setValue(m_beta, *m_feVel, iel, m_iVelocity, m_ao, dof());
            m_elementMat[0]->u_grad_phi_j_phi_i(m_density,m_elemFieldExt,*m_feVel,0,0,this->dimension());
 
            // m_elemFielVelMesh -> -w^{n}
            m_elemFieldVelMesh.setValue(externalVec(1), *m_feVel, iel, m_iDisplacement, m_externalAO[1], *m_externalDof[1]);
	    m_elementMat[0]->div_u_phi_j_phi_i(m_density,m_elemFieldVelMesh,*m_feVel,0,0,this->dimension());
          } else {
            // 2/ bdf
            // extrapolation of u^n in (u^n \cdot \grad{u^{n+1}})
            m_elemFieldExt.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
            m_elementMat[0]->u_grad_phi_j_phi_i(m_density,m_elemFieldExt,*m_feVel,0,0,this->dimension());
          }
	  
	  // SUPG stabilization 
	  m_elementMat[0]->stab_supgAdvDiffCT(m_fstransient->timeStep, FelisceParam::instance().stabSUPG, 
					      FelisceParam::instance().stabdiv, m_density,  m_viscosity, *m_feVel, m_elemFieldExt);
          break;
        default:
          FEL_ERROR("Error: No Convection Method found for NSFracStep solver");
        }
      }

      if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs) ) {
        //================================
        //              RHS
        //================================

        //if (!m_explicitAdvection) {
        // pressure term (non incremental version, but all the equation is written with the velocity of the prediction stage)
        m_elemFieldPres.setValue(externalVec(0), *m_fePres, iel, m_iPressure, m_externalAO[0], *m_externalDof[0]);
        assert(!m_elementVector.empty());
	m_elementVector[0]->grad_f_phi_i(-1,*m_feVel,*m_fePres,m_elemFieldPres,0);
        // Incremental scheme
	if ( FelisceParam::instance().orderPressureExtrapolation > 0 ) { //  && (m_fstransient->iteration > 1)
          m_elemFieldPresDelta.setValue(m_seqPressureDelta, *m_fePres, iel, m_iPressure, m_externalAO[0], *m_externalDof[0]);
          m_elementVector[0]->grad_f_phi_i(-1.,*m_feVel,*m_fePres,m_elemFieldPresDelta,0);
	}

        // time scheme bdf
        // \frac{\rho}{\dt} \sum_{i=1}^k \alpha_i u_{n+1-i}
        m_elemFieldRHSbdf.setValue(m_seqBdfRHS, *m_feVel, iel, m_iVelocity, m_ao, dof());
        m_elementVector[0]->source(m_density,*m_feVel,m_elemFieldRHSbdf,0,this->dimension());
        // 'm_density' only because the time step is integrated into the bdf term
   
        // SUPG/PSPG stabilization
        if(FelisceParam::instance().NSequationFlag == 1) { 
	  if(FelisceParam::instance().useALEformulation > 0) 
	    m_elemFieldExt.setValue(m_beta, *m_feVel, iel, m_iVelocity, m_ao, dof());
	  else 
	    m_elemFieldExt.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof()); 
	}
	else 
	  m_elemFieldExt.val.clear(); // zero convective velocity 
	m_elementVector[0]->stab_supgAdvDiffCT(m_fstransient->timeStep, FelisceParam::instance().stabSUPG, m_density,  m_viscosity, 
					       *m_feVel, *m_fePres, m_elemFieldExt, m_elemFieldPres, m_elemFieldRHS);
      }
    } 
  }

  void LinearProblemNSFracStepAdvDiff::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_IEL;
    IGNORE_UNUSED_ELEM_ID_POINT;
    assert(!m_elementVectorBD.empty());
    
    // General updates
    
    for (std::size_t iFe = 0; iFe < m_listCurvilinearFiniteElement.size(); iFe++) {
      m_listCurvilinearFiniteElement[iFe]->updateMeasNormal(0, elemPoint);
    }
    
    // Saving ptr to velocity curvilinear finite element
    // CurvilinearFiniteElement* curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
    

  }

  void LinearProblemNSFracStepAdvDiff::initExtrapol(PetscVector& V_1) {
    if (allocateSeqVecExt == false) {
      m_solExtrapol.create(PETSC_COMM_SELF);
      m_solExtrapol.setType(VECSEQ);
      m_solExtrapol.setSizes(PETSC_DECIDE,m_numDof);
      allocateSeqVecExt = true;
    }
    gatherVector(V_1, m_solExtrapol);
  }

  void LinearProblemNSFracStepAdvDiff::updateExtrapol(PetscVector& V_1) {
    gatherVector(V_1, m_solExtrapol);
  }

  void LinearProblemNSFracStepAdvDiff::gatherVectorBeforeAssembleMatrixRHS() {
    if (allocateSeqVec == false) {
      m_seqBdfRHS.create(PETSC_COMM_SELF);
      m_seqBdfRHS.setType(VECSEQ);
      m_seqBdfRHS.setSizes(PETSC_DECIDE,m_numDof);
      allocateSeqVec = true;
    }
    gatherVector(m_bdf->vector(), m_seqBdfRHS);

    // Incremental scheme 
    if (FelisceParam::instance().orderPressureExtrapolation > 0) {
      if (allocateSeqVecIncremental == false) {
        m_seqPressureDelta.duplicateFrom(externalVec(0));
        m_seqPressure0.duplicateFrom(externalVec(0));
        m_seqPressure0.copyFrom(externalVec(0));
        allocateSeqVecIncremental = true;
      } 
      waxpy(m_seqPressureDelta, -1., m_seqPressure0, externalVec(0));
      m_seqPressure0.copyFrom(externalVec(0));     
    }

  }

  void LinearProblemNSFracStepAdvDiff::copyMatrixRHS() {
    m_matrix.duplicateFrom(matrix(0),MAT_COPY_VALUES);
    m_matrix.assembly(MAT_FINAL_ASSEMBLY);
    m_buildTeporaryMatrix = true;
  }

  void LinearProblemNSFracStepAdvDiff::addMatrixRHS() {
    matrix(0).axpy(1,m_matrix,SAME_NONZERO_PATTERN);
  }
}
