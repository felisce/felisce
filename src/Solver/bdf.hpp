//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _BDF_HPP
#define _BDF_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "PETScInterface/petscVector.hpp"

namespace felisce 
{
  const int BDF_MAX_ORDER = 3;

  /*!
   \class Bdf
   \brief Backward differencing formula time discretization

   A differential equation of the form

   \f$ M u' = A u + f \f$

   is discretized in time as

   \f$ M p'(t_{k+1}) = A u_{k+1} + f_{k+1} \f$

   where p denotes the polynomial of order n in t that interpolates
   (t_i,u_i) for i = k-n+1,...,k+1.

   The approximative time derivative \f$ p'(t_{k+1}) \f$ is a linear
   combination of state vectors u_i:

   \f$ p'(t_{k+1}) = \frac{1}{\Delta t} (\alpha_0 u_{k+1} - \sum_{i=1}^n \alpha_i u_{k+1-i} )\f$

   Thus we have

   \f$ \frac{\alpha_0}{\Delta t} M u_{k+1} = A u_{k+1} + f + M \bar{p} \f$

   with

   \f$ \bar{p} = \frac{1}{\Delta t} \sum_{i=1}^n \alpha_i u_{k+1-i} \f$

   This class stores the n last state vectors in order to be able to
   calculate \f$ \bar{p} \f$. It also provides alpha_i
   and can extrapolate the new state from the n last states with a
   polynomial of order n-1:

   \f$ u_{k+1} \approx \sum_{i=0}^{n-1} \beta_i u_{k-i} \f$
   */

  /*!
   \class Bdf
   \authors A. Collin
   \date 12/05/2011
   \brief ???
   */

  class Bdf {
  public:
    Bdf();
    ~Bdf();
    void defineOrder(int n, int nComp=1);

    //!Bdf1.
    void initialize(const PetscVector& sol_0);
    //!Bdf2.
    void initialize(const PetscVector& sol_0,const PetscVector& sol_1);
    //!Bdf3.
    void initialize(const PetscVector& sol_0,const PetscVector& sol_1,const PetscVector& sol_2);

    // // //!Bdf1.
    void initialize(std::vector<PetscVector>& sol_0);
    // //  //!Bdf2.
    void initialize(std::vector<PetscVector>& sol_0,std::vector<PetscVector>& sol_1);
    // //  //!Bdf3.
    void initialize(std::vector<PetscVector>& sol_0,std::vector<PetscVector>& sol_1,std::vector<PetscVector>& sol_2);

    void reinitialize(int order, const PetscVector& sol0, const PetscVector& sol1, const PetscVector& sol2);

    void update(PetscVector& sol_n);
    void update(std::vector<PetscVector>& sol_n);


    void computeRHSTime(double dt, PetscVector& RHSTime);
    void computeRHSTime(double dt, std::vector<PetscVector>& RHSTime);
    void computeRHSTimeByPart(double dt, std::vector<PetscVector>& RHSTimeByPart);
    void computeRHSTime(double dt);
    void extrapolate( PetscVector& extrap);
    void extrapolate( std::vector<PetscVector>& extrap);
    void extrapolateByPart(std::vector<PetscVector>& partExtrap);

    //return \alpha_0.
    inline const double & coeffDeriv0() const {
      return m_alpha[0];
    }
    inline double & coeffDeriv0() {
      return m_alpha[0];
    }

    // return alpha
    inline const std::vector<double> & alpha() const {
      return m_alpha;
    }
    inline std::vector<double> & alpha() {
      return m_alpha;
    }

    // return beta
    inline const std::vector<double> & beta() const {
      return m_beta;
    }
    inline std::vector<double> & beta() {
      return m_beta;
    }

    // return alpha[i]
    inline const double & alpha(int i) const {
      return m_alpha[i];
    }
    inline double & alpha(int i) {
      return m_alpha[i];
    }

    // return beta[i]
    inline const double & beta(int i) const {
      return m_beta[i];
    }
    inline double & beta(int i) {
      return m_beta[i];
    }

    //!Access functions.

    inline const std::vector<PetscVector> & vec_sol_n() const {
      return m_sol_n;
    }
    inline std::vector<PetscVector> & vec_sol_n() {
      return m_sol_n;
    }

    inline const std::vector<PetscVector> & vec_sol_n_1() const {
      return m_sol_n_1;
    }
    inline std::vector<PetscVector> & vec_sol_n_1() {
      return m_sol_n_1;
    }

    inline const std::vector<PetscVector> & vec_sol_n_2() const {
      return m_sol_n_2;
    }
    inline std::vector<PetscVector> & vec_sol_n_2() {
      return m_sol_n_2;
    }

    inline const std::vector<PetscVector> & vec_RHS() const {
      return m_rhs;
    }
    inline std::vector<PetscVector> & vec_RHS() {
      return m_rhs;
    }

    inline const PetscVector&  sol_n() const {
      return m_sol_n[0];
    }
    inline PetscVector&  sol_n() {
      return m_sol_n[0];
    }

    inline const PetscVector&  sol_n_1() const {
      return m_sol_n_1[0];
    }
    inline PetscVector&  sol_n_1() {
      return m_sol_n_1[0];
    }

    inline const PetscVector&  sol_n_2() const {
      return m_sol_n_2[0];
    }
    inline PetscVector&  sol_n_2() {
      return m_sol_n_2[0];
    }

    inline const PetscVector&  vector() const {
      return m_rhs[0];
    }
    inline PetscVector&  vector() {
      return m_rhs[0];
    }

    inline const int & order() const {
      return m_order;
    }
    inline int & order() {
      return m_order;
    }

    inline const int & numComp() const {
      return m_numberOfComp;
    }
    inline int & numComp() {
      return m_numberOfComp;
    }

    // Access fonction to use it in user file
    inline const std::vector<PetscVector> & vec_solExt() const {
      return m_solExtrapol;
    }
    inline std::vector<PetscVector> & vec_solExt() {
      return m_solExtrapol;
    }
    inline const PetscVector&  solExt() const {
      return m_solExtrapol[0];
    }
    inline PetscVector&  solExt() {
      return m_solExtrapol[0];
    }

    //! Returns the time derivative of the solution with u^{n+1}=vec
    void time_der( double dt, const PetscVector& m_vecSol,  PetscVector& deriv  ) const;
    void time_der( double dt, const std::vector<PetscVector>& m_vecSol,  std::vector<PetscVector>& deriv  ) const;


  private:
    //! Order of the BDF derivative/extrapolation: the time-derivative
    //! coefficients std::vector has size n+1, the extrapolation std::vector has size n
    int m_order;
    //! Number of components
    int m_numberOfComp;
    //! Size du vecteur solution
    felInt m_size;
    //! Coefficients \f$ \alpha_i \f$ of the time bdf discretization
    std::vector<double> m_alpha;
    //! Coefficients \f$ \beta_i \f$ of the extrapolation
    std::vector<double> m_beta;

    std::vector<PetscVector> m_sol_n;
    std::vector<PetscVector> m_sol_n_1;
    std::vector<PetscVector> m_sol_n_2;
    std::vector<PetscVector> m_rhs;

    // extrapolated solution when using bdf order > 1
    std::vector<PetscVector> m_solExtrapol;

    bool m_build_order_2;
    bool m_build_order_3;
    bool m_build_RHS;
  };
}

#endif
