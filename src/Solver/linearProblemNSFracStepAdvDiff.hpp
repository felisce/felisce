//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau  
//

#ifndef _LinearProblemNSFracStepAdvDiff_HPP
#define _LinearProblemNSFracStepAdvDiff_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
  /*!
   \class LinearProblemNSFracStepAdvDiff
   \author J-F. Gerbeau
   \date 25/11/2011
   \brief Advection-Diffusion step of a fractional step algorithm for the Navier-Stokes equations
   */
  class LinearProblemNSFracStepAdvDiff:
  public LinearProblem {
  public:
    LinearProblemNSFracStepAdvDiff();
    ~LinearProblemNSFracStepAdvDiff() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    
    void gatherVectorBeforeAssembleMatrixRHS() override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) override;
    // Time scheme (bdf):
    void initializeTimeScheme(Bdf* bdf) override {
      m_bdf = bdf;
    }
    void initExtrapol(PetscVector& V_1) override;
    void updateExtrapol(PetscVector& V_1) override;
    
    void copyMatrixRHS() override;
    void addMatrixRHS() override;

    inline int& pressureFWI() { return m_pressureFWI; }
    
  protected:
    CurrentFiniteElement* m_feVel;
    CurrentFiniteElement* m_feVelAdv;
    CurrentFiniteElement* m_fePres;
    Variable* m_velocity;
    Variable* m_velocityAdvection;
    Variable* m_pressure;
    Variable* m_displacement;
    felInt m_iVelocity;
    felInt m_iVelocityAdvection;
    felInt m_iPressure;
    felInt m_iDisplacement;
    double m_viscosity;
    double m_density;
    int m_explicitAdvection;
    bool m_useSymmetricStress;
    // bdf:
    PetscVector m_seqBdfRHS;
    PetscVector m_solExtrapol;
    
    ElementField m_elemFieldRHS;
  private:
    ElementField m_elemFieldAdv;
    ElementField m_elemFieldPres;
    int m_pressureFWI;
    
    // bdf:
    ElementField m_elemFieldRHSbdf;
    ElementField m_elemFieldExt;
    PetscMatrix m_matrix;
    bool m_buildTeporaryMatrix;
    // bdf:
    Bdf* m_bdf;
    bool allocateSeqVec;
    bool allocateSeqVecExt;
    
    // Incremental version
    bool allocateSeqVecIncremental;
    PetscVector m_seqPressure0;
    PetscVector m_seqPressureDelta;
    ElementField m_elemFieldPresDelta;
    
    //ALE
    ElementField m_elemFieldVelMesh;
    PetscVector m_beta;
    int numDofExtensionProblem;
    
    std::vector<PetscInt> m_petscToGlobal1;
    std::vector<PetscInt> m_petscToGlobal2;
    std::vector<PetscScalar> m_auxvec;
  };
}

#endif
