//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _REDUCEDSTEKLOV_HPP_
#define _REDUCEDSTEKLOV_HPP_

// System includes
#include <vector>

// External includes

// Project includes
#include "InputOutput/io.hpp"
#include "Core/chrono.hpp"

namespace felisce {

  // Forward declaration
  template< class volumeProblem >
  class LinearProblemReducedSteklov;

  template< class volumeProblem >
  class ReducedSteklov {

    typename LinearProblemReducedSteklov<volumeProblem>::imgType m_imgType;
    /// (0) Phase zero: initialization
  public:
    /// The constructor
    ReducedSteklov(PetscMatrix& mass, PetscMatrix& laplacian, MPI_Comm comm, LinearProblemReducedSteklov<volumeProblem>* pb, typename LinearProblemReducedSteklov<volumeProblem>::imgType imType,ChronoInstance::Pointer);
  private:
    /// The communicator
    MPI_Comm m_comm;
    /// The mass matrix
    PetscMatrix m_mass;
    /// The matrix of the laplacian operator
    PetscMatrix m_laplacian;
    /// The linearProblem;
    LinearProblemReducedSteklov<volumeProblem>* m_lpb;

    /// (1) Phase one: the solution of the laplacian eigenProblem
  public:
    void solveLaplacianEP();
  private:
    std::vector<PetscVector> m_laplacianEVec;
    std::vector<PetscVector> m_imgLaplacianEVec;
    /// (2) Phase two: steklov eigenPairs
  public:
    void steklovEVecEstimate();
    void applySteklovOperatorOnLEV(felInt imesh);
    void approximateSteklovEigenVec(std::vector<PetscVector> &coefficients);
    void assembleReducedMatrix( PetscMatrix& ReducedMatrix );
    /// The approximated Steklov eigen-values
    std::vector<double> m_eValues;
    std::vector<PetscVector> m_eVecW;
    /// (3) Phase three: online phase
    void applyLowRankSteklov(PetscVector& in, PetscVector& out);
    std::vector<PetscVector> m_onlineInputs;
    std::vector<PetscVector> m_onlineOutputs;
    /// Usefull parameters:
    std::size_t m_offLineBasisDim;
    std::size_t m_onLineBasisDim;
    std::size_t m_currentBasisDim;
    /// PostProcess
    void exportAllEig(felInt imesh);
    /// (4) loading/saving off-line basis
  private:
    bool m_loaded;
    bool loadFromFile(std::string folder);
    std::vector<PetscVector> m_constantResponse;
  public:
    inline bool loadedFromFile() { return m_loaded; }
    void addConstantResponse(const PetscVector& cR) {
      m_constantResponse.push_back(cR);
    }
    ChronoInstance:: Pointer m_chronoRS;
  };
}
// The implementation
#include "Solver/reducedSteklov.tpp"
#endif
