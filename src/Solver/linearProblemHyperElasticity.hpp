//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef _LINEAR_PROBLEM_HYPER_ELASTICITY_HPP
#define _LINEAR_PROBLEM_HYPER_ELASTICITY_HPP

// System includes
#include <cmath>
#include <numeric>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/felisceTools.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "Solver/linearProblem.hpp"
#include "Hyperelasticity/invariants.hpp"
#include "Hyperelasticity/enumHyperelasticity.hpp"
#include "Hyperelasticity/linearProblemHyperElasticityHelper.hpp"

namespace felisce {

  namespace HyperelasticityNS {

    /*!
     * \brief Tells which part of the system is being solved.
     *
     * There is first a static solve, and then we go to the dynamic resolution.
     */
    enum System {
      static_,
      dynamic_
    };


  } // namespace HyperelasticityNS


    /**
     * \brief Class in charge of hyperelasticity problem.
     */
  class LinearProblemHyperElasticity
    : public LinearProblem
{
  public:

    /*!
     * \brief Convenient typedef over the type of a spatial function
     *
     * Arguments are:
     *   first -> index of the component considered (0 for x, 1 for y, 2 for z)
     *   second-> x
     *   third -> y
     *   fourth-> z
     */
    typedef std::function<double(int, double, double, double)> SpatialForce;

  public:

    /*!
     * \brief Constructor
     * \param[in] force Spatial force to apply.
     */
    explicit LinearProblemHyperElasticity(SpatialForce force);

    //! Destructor.
    ~LinearProblemHyperElasticity() override;

    //! Overload of Felisce namesake method.
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    //! Overload of Felisce namesake method.
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    //! Overload of Felisce namesake method.
    void computeElementArray(const std::vector<Point*>& elemPoint,
                                     const std::vector<felInt>& elemIdPoint,
                                     int& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    //! Overload of Felisce namesake method.
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint,
        const std::vector<felInt>& elemIdPoint,
        int& iel, FlagMatrixRHS flagMatrixRHS) override;

    /*!
     * \brief After the assembling, compute the system matrix and RHS.
     */
    void computeSystemAfterAssembling() override;

    //! Clear the matrix with the new stiffness.
    void preSNESAssembleAdditional() override;

    //! Update for next time step. (not called after each dynamic iteration).
    void endIteration() override;

    //! Update current displacement. Already called in endIteration().
    void UpdateDisplacementBetweenTimeStep();

    //! Tells the problem the static resolution is done. Useful only when solving dynamic problems.
    void GoToDynamic();

  private:

    /*!
     * \brief Extract the displacement component by component for a given element.
     *
     * \param[in] iel Index of the finite element currently processed.
     * \param[in] evaluation_state Vector that holds the (local) evaluation of the unknowns.
     * \param[in] seq_evaluation_state Sequential std::vector which holds the global evaluation of the unknowns (required
     * until ghost vertices are correctly implemented).
     *
     * \return Displacement of the dof of the current finite element ordered by component.
     * So first element of the returned std::vector stands for the x component, second element for y component,
     * and if the mesh is 3D third element for z component.
     */
    std::vector<UBlasVector> extractDisplacementPerComponent(PetscInt iel,
        PetscVector& evaluation_state,
        PetscVector& seq_evaluation_state) const;

    /*!
     * \brief Extract the displacement component by component for a given element.
     *
     * Relevant only when IsIncompressibleT == HyperelasticityNS::Incompressible.
     *
     * \param[in] iel Index of the finite element currently processed.
     * \param[in] evaluation_state Vector that holds the (local) evaluation of the unknowns.
     * \param[in] seq_evaluation_state Sequential std::vector which holds the global evaluation of the unknowns (required
     * until ghost vertices are correctly implemented).
     *
     * \return Pressure.
     */
    UBlasVector extractPressure(PetscInt iel,
                                PetscVector& evaluation_state,
                                PetscVector& seq_evaluation_state) const;

    /**
      * \brief Extract the values related to a given unknown from a vector which structure is the same as solution().
      *
      * \param[in] mpi_vector Local (in MPI meaning) vector that stores the values of all the unknowns of the problem.
      * Typical choices would be solution() or evaluationState().
      * \param[in] seq_vector The global sequential vector that stores the values of all the unknowns of the problem.
      * This vector is required when a local processor doesn;'t know the value (there are noo ghost vertices yet in Felisce).
      * \param[in] index_unknown Index which specifies which unknown is currently considered.
      * \param[in] iComp Index related to the spatial component. For instance if displacement is considered, iComp = 0
      * will return the projection of the displacement on the x-axis.
      * \param[in] eltType Type of geometric element currently considered in the mesh.
      * \param[in] iel Index of the geometric element considered. Typically it should be the same as namesake index in
      * computeElementArray() method.
      *
      * \return Vector giving the values for the unknown at each support dof of the current geometric element.
      *
      */
    UBlasVector extractUnknownValue(std::size_t index_unknown,
                                    std::size_t iComp,
                                    ElementType eltType,
                                    std::size_t iel,
                                    PetscVector& mpi_vector,
                                    PetscVector& seq_vector) const;

    /*!
     * \brief Initialize some attributes at the very end of the #Model::initializeLinearProblem() method.
     *
     * #m_displacement is std::set here.
     */
    void InitializeDerivedAttributes() override;


    /*!
     * \brief Call this method once to very first dynamic iteration has been performed.
     *
     */
    void SetFirstDynamicIterationDone();

    /*!
     * \brief Init pressure data if relevant (i.e. for incompressible problems).
     */
    void InitPressureData();

  private:

    //! Index related to displacement.
    int m_iDisplacement;

    //! Finite element related to displacement.
    CurrentFiniteElement* m_feDisplacement;

    //! Field element. Used to give the force to Felisce in case of a volumic force.
    ElementField m_elemField;

    //! Whether the applied force is volumic or surfacic
    Private::HyperElasticity::ForceType m_typeOfForceApplied;

    //! The force applied, when it is a spatial one (constant ones are handled differently in Felisce).
    SpatialForce m_force;

    //! Store the current element type.
    ElementType m_currentElementType;

    //! Velocity from previous time iteration.
    PetscVector current_velocity_;

    //! Displacement from previous time iteration.
    PetscVector current_displacement_;

    //! Displacement from previous time iteration.
    PetscVector seq_current_displacement_;

    //! Vector in which midpoint position is stored.
    PetscVector midpoint_position_;
    PetscVector seq_midpoint_position_;

    //! Whether we are in static or dynamic part of the resolution.
    HyperelasticityNS::System system_;

    /*!
     * \brief Tells whether we are in the very first dynamic iteration or not.
     *
     * Some operations (assembling of the mass matrix, of the current stiffness matrix for half sum scheme) are
     * relevant only for the first iteration.
     */
    bool is_first_dynamic_iteration_done_;

  };
}

#endif
