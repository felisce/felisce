//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:   A. Collin 
//

#ifndef _COURTEMANCHESOLVER_HPP
#define _COURTEMANCHESOLVER_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"

// Project includes
#include "Model/model.hpp"
#include "Core/felisce.hpp"
#include "Solver/bdf.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce 
{
  class CourtemancheSolver {
  public:
    /// Constructor.
    CourtemancheSolver(FelisceTransient::Pointer fstransient);
    /// Destructor.
    ~CourtemancheSolver();

    void defineSizeAndMappingOfIonicProblem(felInt size, ISLocalToGlobalMapping& mapping, AO ao=FELISCE_PETSC_NULLPTR);

    void initializeExtrap(PetscVector& V_0);
    void updateExtrap(PetscVector& V_1);

    /// Compute Iion with value_uExtrap.
    double iIonTotal(double v, felInt pos);
    /// Update ion concentrations.
    void updateConcentrations();
    void initializeConcentrationsAndGateConditions(PetscVector& m,PetscVector& h, PetscVector& j, PetscVector& ao, PetscVector& io, PetscVector& ua, PetscVector& ui, PetscVector& xr, PetscVector& xs, PetscVector& d, PetscVector& f, PetscVector& fca, PetscVector& urel, PetscVector& vrel, PetscVector& wrel, PetscVector& nai, PetscVector& nao, PetscVector& cao, PetscVector& ki, PetscVector& ko, PetscVector& cai, PetscVector& naiont, PetscVector& kiont, PetscVector& caiont, PetscVector& ileak, PetscVector& iup, PetscVector& itr, PetscVector& irel, PetscVector& cmdn, PetscVector& trpn, PetscVector& nsr, PetscVector& jsr, PetscVector& csqn);

    PetscVector& ionicVariable(int iVar);

    void computeIon();

    /// Access function.
    inline const PetscVector&  ion() const {
      return m_ion;
    }
    inline PetscVector&  ion() {
      return m_ion;
    }

    inline const HeteroCourtModelIto& fctCourtCondIto() const {
      return m_heteroCourtModelIto;
    }
    inline HeteroCourtModelIto& fctCourtCondIto() {
      return m_heteroCourtModelIto;
    }

    inline const std::vector<double>& courtCondIto() const {
      return m_courtCondIto;
    }
    inline std::vector<double>& courtCondIto() {
      return m_courtCondIto;
    }

    inline const HeteroCourtModelICaL& fctCourtCondICaL() const {
      return m_heteroCourtModelICaL;
    }
    inline HeteroCourtModelICaL& fctCourtCondICaL() {
      return m_heteroCourtModelICaL;
    }

    inline const std::vector<double>& courtCondICaL() const {
      return m_courtCondICaL;
    }
    inline std::vector<double>& courtCondICaL() {
      return m_courtCondICaL;
    }

    inline const HeteroCourtModelMultCoeff& fctCourtCondMultCoeff() const {
      return m_heteroCourtModelMultCoeff;
    }
    inline HeteroCourtModelMultCoeff& fctCourtCondMultCoeff() {
      return m_heteroCourtModelMultCoeff;
    }

    inline const std::vector<double>& courtCondMultCoeff() const {
      return m_courtCondMultCoeff;
    }
    inline std::vector<double>& courtCondMultCoeff() {
      return m_courtCondMultCoeff;
    }

  protected:
    /// User data file of Felisce (only transient parameters).
    FelisceTransient::Pointer m_fstransient;
    /// Number of Dof for the trans membrane potential.
    felInt m_size;

    ISLocalToGlobalMapping m_localDofToGlobalDof;

  private:
    //Gate conditions
    // INa
    PetscVector m_m;
    PetscVector m_h;
    PetscVector m_j;
    // ITo
    PetscVector m_ao;
    PetscVector m_io;
    // IKur
    PetscVector m_ua;
    PetscVector m_ui;
    // IKr
    PetscVector m_xr;
    // IKs
    PetscVector m_xs;
    // ICaL
    PetscVector m_d;
    PetscVector m_f;
    PetscVector m_fca;
    // Irel
    PetscVector m_urel;
    PetscVector m_vrel;
    PetscVector m_wrel;

    // Ion concentrations
    PetscVector m_nai;
    PetscVector m_nao;
    PetscVector m_cao;
    PetscVector m_ki;
    PetscVector m_ko;
    PetscVector m_cai;
    //Currents
    PetscVector m_naiont;
    PetscVector m_kiont;
    PetscVector m_caiont;
    PetscVector m_ileak;
    PetscVector m_iup;
    PetscVector m_itr;
    PetscVector m_irel;
    // Ca:
    PetscVector m_cmdn;   /* Calmodulin Buffered Ca Concentration (mM) */
    PetscVector m_trpn;   /* Troponin Buffered Ca Concentration (mM) */
    PetscVector m_nsr;    /* NSR Ca Concentration (mM) */
    PetscVector m_jsr;    /* JSR Ca Concentration (mM) */
    PetscVector m_csqn;   /* Calsequestrin Buffered Ca Concentration (mM) */

    //Gate conditions
    // INa
    double m_value_m;
    double m_value_h;
    double m_value_j;
    // ITo
    double m_valuem_ao;
    double m_value_io;
    // IKur
    double m_value_ua;
    double m_value_ui;
    // IKr
    double m_value_xr;
    // IKs
    double m_value_xs;
    // ICaL
    double m_value_d;
    double m_value_f;
    double m_value_fca;
    // Irel
    double m_value_urel;
    double m_value_vrel;
    double m_value_wrel;

    // Ion concentrations
    double m_value_nai;
    double m_value_nao;
    double m_value_cao;
    double m_value_ki;
    double m_value_ko;
    double m_value_cai;
    //Currents
    double m_value_naiont;
    double m_value_kiont;
    double m_value_caiont;
    double m_value_ileak;
    double m_value_iup;
    double m_value_itr;
    double m_value_irel;
    // Ca:
    double m_value_cmdn;   /* Calmodulin Buffered Ca Concentration (mM) */
    double m_value_trpn;   /* Troponin Buffered Ca Concentration (mM) */
    double m_value_nsr;    /* NSR Ca Concentration (mM) */
    double m_value_jsr;    /* JSR Ca Concentration (mM) */
    double m_value_csqn;   /* Calsequestrin Buffered Ca Concentration (mM) */


    // Ionic current
    PetscVector m_ion;
    /// Extrapolate vector.
    PetscVector m_uExtrap;

    HeteroCourtModelIto m_heteroCourtModelIto;
    std::vector<double> m_courtCondIto;

    HeteroCourtModelICaL m_heteroCourtModelICaL;
    std::vector<double> m_courtCondICaL;

    HeteroCourtModelMultCoeff m_heteroCourtModelMultCoeff;
    std::vector<double> m_courtCondMultCoeff;
    
    AO m_aoPetsc;

  };
}

#endif
