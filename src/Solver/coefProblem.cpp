//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J Fouchet & C Bui
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Solver/coefProblem.hpp"
#include "PETScInterface/petscVector.hpp"

namespace felisce {
  /*! Construtor  */
  CoefProblem::CoefProblem():
    m_verbose(0),
    m_buildSolver(false)
  {}

  CoefProblem::~CoefProblem() {
    if(m_buildSolver) {
    }
    _RHS.destroy();
    m_sol.destroy();
    m_Matrix.destroy();
    timeStepOverC.clear();
    RplusTimeStepOverC.clear();
  }

  void CoefProblem::initializeCoefProblem(MPI_Comm& comm) {
    m_petscComm = comm;
    m_verbose = FelisceParam::verbose();

    timeStepOverC.resize(FelisceParam::instance().lumpedModelBC_Rprox.size(), 0.);
    RplusTimeStepOverC.resize(FelisceParam::instance().lumpedModelBC_Rprox.size());
    RplusTimeStepOverC = FelisceParam::instance().lumpedModelBC_Rprox;

    for(unsigned int i = 0; i < FelisceParam::instance().lumpedModelBC_Rprox.size(); i++) {
      if ( FelisceParam::instance().lumpedModelBCType[i] == 1 ) {
        // R lumpedModel bc, timeStepOverC = 0, RplusTimeStepOverC = R
        // RCR lumpedModel bc: not implemented yet
        if ( Tools::notEqual(FelisceParam::instance().lumpedModelBC_Rdist[i],0.) )
          FEL_WARNING("Implicit lumpedModelBC are only implemented for R and RC elements.\n");
      } else if ( FelisceParam::instance().lumpedModelBCType[i] == 2 ) {
        // RC lumpedModel bc, timeStepOverC = dt/C, RplusTimeStepOverC = R + timeStepOverC
        timeStepOverC[i] = FelisceParam::instance().timeStep / FelisceParam::instance().lumpedModelBC_C[i];
        RplusTimeStepOverC[i] = FelisceParam::instance().lumpedModelBC_Rprox[i] + timeStepOverC[i];
      }
    }
  }

  /*!
   \brief use the pattern define with dof to allocate memory for the matrix in CSR format.
   */
  void CoefProblem::allocateMatrixRHS(int size, int rank) {
    IGNORE_UNUSED_RANK;

    // Matrix
    //========

    m_Matrix.createDense(m_petscComm, PETSC_DECIDE, PETSC_DECIDE, size, size, FELISCE_PETSC_NULLPTR);
    m_Matrix.zeroEntries();

    // RHS
    //=====

    _RHS.createMPI(m_petscComm,PETSC_DECIDE,size);
    m_sol.createMPI(m_petscComm,PETSC_DECIDE,size);
    m_sol.zeroEntries();
  }

  void CoefProblem::assembleMatrix(const int idInflowOutflowBC, const std::vector<double> m_fluxLinCombBC, int rank) {
    IGNORE_UNUSED_RANK;
    // Matrix (I-A) is filled by columns (a "u_i" by column)
    // We fill the "idInflowOutflowBC"th column (corresponding to the "idInflowOutflowBC"th Neumann Normal boundary condition)

    PetscInt numLumpedModelBC = FelisceParam::instance().lumpedModelBC_Rprox.size() ;
    PetscInt numNeumannNormalBCwithoutWindkessel = m_fluxLinCombBC.size() - numLumpedModelBC;
    PetscScalar value;

    // loop on the row of this column
    // 1/ NeumannNormal boundary conditions without windkessel one (no resistance at the inlet)
    for(PetscInt idRow = 0; idRow < numNeumannNormalBCwithoutWindkessel; idRow++) {
      if (idRow == idInflowOutflowBC)
        value = 1.;
      else
        value = 0.;
      m_Matrix.setValues( 1, &idRow, 1, &idInflowOutflowBC, &value, INSERT_VALUES);
    }

    // 2/ windkessel bc
    for(PetscInt idRow = numNeumannNormalBCwithoutWindkessel; idRow < (PetscInt) m_fluxLinCombBC.size(); idRow++) {
      if (idRow == idInflowOutflowBC)
        value = 1. - RplusTimeStepOverC[idRow-numNeumannNormalBCwithoutWindkessel] * m_fluxLinCombBC[idRow];
      else
        value = - RplusTimeStepOverC[idRow-numNeumannNormalBCwithoutWindkessel] * m_fluxLinCombBC[idRow];
      m_Matrix.setValues( 1, &idRow, 1, &idInflowOutflowBC, &value, INSERT_VALUES);
    }

    m_Matrix.assembly(MAT_FINAL_ASSEMBLY);
  }

  void CoefProblem::assembleRHS(const std::vector<double> pressure, const std::vector<double> m_fluxLinCombBC, const std::vector<double> m_fluxLinCombBCtotal, int rank) {
    IGNORE_UNUSED_RANK;
    PetscInt numLumpedModelBC = FelisceParam::instance().lumpedModelBC_Rprox.size() ;
    PetscInt numNeumannNormalBCwithoutWindkessel = m_fluxLinCombBC.size() - numLumpedModelBC;
    PetscScalar value;

    // loop on the row of this column
    // 1/ NeumannNormal boundary conditions without windkessel one (no resistance at the inlet)
    for(PetscInt idRow = 0; idRow < numNeumannNormalBCwithoutWindkessel; idRow++) {
      // warning : b[i] = P_i and not -P_i
      value = - pressure[idRow];
      _RHS.setValues(1, &idRow, &value, INSERT_VALUES);
    }

    // 2/ windkessel bc
    for(PetscInt idRow = numNeumannNormalBCwithoutWindkessel; idRow < (PetscInt) m_fluxLinCombBC.size(); idRow++) {
      value = RplusTimeStepOverC[idRow-numNeumannNormalBCwithoutWindkessel] * m_fluxLinCombBC[idRow] + timeStepOverC[idRow-numNeumannNormalBCwithoutWindkessel] * m_fluxLinCombBCtotal[idRow];
      _RHS.setValues(1, &idRow, &value, INSERT_VALUES);
    }

    _RHS.assembly();
  }

  /*!
   \brief solve the linear solver with Petsc methods.
   */
  void CoefProblem::buildSolver() 
  {
    m_ksp->init();
    m_ksp->setKSPandPCType("preonly", "lu");
    m_ksp->setTolerances(10.e-6, 10.e-10, 1000, 0);
    m_buildSolver = true;
  }

  void CoefProblem::solve( int /*rank*/, int /*size*/ ) 
  {
    m_ksp->setKSPOperator(m_Matrix, FelisceParam::instance().setPreconditionerOption[0]);
    m_ksp->solve(_RHS,m_sol,0);

    printSolution(m_verbose);
  }

  // Write
  //==========

  //Write in file matrix.m, matrix in matlab format.
  void CoefProblem::writeMatrixForMatlab(int verbose, std::ostream& outstr) const {
    IGNORE_UNUSED_OUTSTR;
    if (verbose > 20) {
      PetscViewer viewer;
      PetscViewerCreate(m_petscComm,&viewer);
      PetscViewerASCIIOpen(m_petscComm,"__matrix.m",&viewer);
      PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
      m_Matrix.view(viewer);
    }
  }

  //Write in file RHS.m, RHS in matlab format.
  void CoefProblem::writeRHSForMatlab(int verbose, std::ostream& outstr) const {
    IGNORE_UNUSED_OUTSTR;
    if (verbose > 20) {
      PetscViewer viewer;
      PetscViewerCreate(m_petscComm,&viewer);
      PetscViewerASCIIOpen(m_petscComm,"__RHS.m",&viewer);
      PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
      _RHS.view(viewer);
    }
  }

  void CoefProblem::writeSolForMatlab(int verbose, std::ostream& outstr) const {
    IGNORE_UNUSED_OUTSTR;
    if (verbose > 20) {
      PetscViewer viewer;
      PetscViewerCreate(m_petscComm,&viewer);
      PetscViewerASCIIOpen(m_petscComm,"m_m_sol.m",&viewer);
      PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
      m_sol.view(viewer);
    }
  }

  //Write in file filename, matrix in matlab format.
  void CoefProblem::writeMatrixForMatlab(std::string const& fileName, PetscMatrix& matrix) const {
    PetscViewer viewer;
    PetscViewerCreate(m_petscComm,&viewer);
    PetscViewerASCIIOpen(m_petscComm,fileName.c_str(),&viewer);
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
    matrix.view(viewer);
  }

  //Write in file filenam, RHS in matlab format.
  void CoefProblem::writeVectorForMatlab(std::string const& fileName, PetscVector& vector) const {
    PetscViewer viewer;
    PetscViewerCreate(m_petscComm,&viewer);
    PetscViewerASCIIOpen(m_petscComm,fileName.c_str(),&viewer);
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
    vector.view(viewer);
  }


  // Print solution of the system solve.
  void CoefProblem::printSolution(int verbose, std::ostream& outstr) const {
    IGNORE_UNUSED_OUTSTR;
    if (verbose > 19) {
      PetscPrintf(m_petscComm,"\n/================Solution of the assembly system Ax=b================/\n");
      m_sol.view();
    }
  }

}
