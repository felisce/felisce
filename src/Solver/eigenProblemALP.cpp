//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes

// Project includes
#include "Solver/eigenProblemALP.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementField.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTools.hpp"

namespace felisce 
{
  /*! Construtor
   */
EigenProblemALP::EigenProblemALP():
  EigenProblem()
{}

EigenProblemALP::~EigenProblemALP() {
  m_fePotTransMemb = nullptr;
  delete [] m_eigenValue;
  if ( (FelisceParam::instance().ROMmethod == "ALP") || (FelisceParam::instance().solveEigenProblem) ) {
    for (int i=0; i<m_dimRomBasis; i++) {
      m_basis[i].destroy();
    }
  }
  if ( (m_useImprovedRec) && (FelisceParam::instance().ROMmethod == "ALP") ) {
    for (int i=0; i<m_dimOrthComp; i++) {
      m_orthComp[i].destroy();
    }
  }
  if (m_matrixM) {
    for (int i=0; i<m_dimRomBasis; i++) {
      delete [] m_matrixM[i];
    }
    delete [] m_matrixM;
  }
  if (m_zeta.size()) {
    for (int i=0; i<m_size2; i++) {
      m_zeta[i].destroy();
    }
  }
  if (m_tensorB) delete [] m_tensorB;
  if (m_tensorE) delete [] m_tensorE;
  if (m_tensorEs) delete [] m_tensorEs;
  if (m_tensorQ) delete [] m_tensorQ;
  if (m_tensorT) delete [] m_tensorT;
  if (m_tensorTs) delete [] m_tensorTs;
  if (m_tensorU) {
    for (int i=0; i<m_dimRomBasis; i++) {
      delete [] m_tensorU[i];
    }
    delete [] m_tensorU;
  }
  if (m_tensorY) delete [] m_tensorY;
  m_U_0.destroy();
  m_U_0_seq.destroy();
  m_W_0.destroy();
  m_W_0_seq.destroy();
  m_Ue_0.destroy();
  m_Ue_0_seq.destroy();
  if ( (FelisceParam::instance().hasInfarct) && (FelisceParam::instance().ROMmethod == "ALP") ) {
    m_FhNf0.destroy();
  }
  if (m_solutionInitialized) {
    delete [] m_beta;
    if (m_mu) delete [] m_mu;
    if (m_xi) delete [] m_xi;
    if (m_gamma) delete [] m_gamma;
    if (m_eta) delete [] m_eta;
    if (m_method == 4) {
      delete [] m_beta_n_1;
      delete [] m_mu_n_1;
      delete [] m_xi_n_1;
      delete [] m_gamma_extrap;
      delete [] m_eta_extrap;
    }
    if (FelisceParam::instance().hasSource) {
      for (int i=0; i<m_numSource; i++) {
        delete [] m_source[i];
      }
      delete [] m_source;
    }
    delete [] m_modeMean;
  }
  if (m_fiber) {
    delete [] m_fiber;
  }

  if (FelisceParam::instance().optimizePotential) {
    m_initPot.destroy();
  }

  if ( (m_method != 0 ) && (m_method != -1 ) && (!FelisceParam::instance().solveEigenProblem) && (FelisceParam::instance().ROMmethod == "ALP")) {
    m_solutionRom.destroy();
    m_rhsRom.destroy();
  }

}

void EigenProblemALP::initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm) {
  EigenProblem::initialize(mesh, fstransient, comm);

  std::unordered_map<std::string, int> mapOfPbm;
  mapOfPbm["FKPP"] = 0;
  mapOfPbm["Monodomain"] = 1;
  mapOfPbm["Bidomain"] = 2;
  mapOfPbm["BidomainCurv"] = 2;
  m_problem = mapOfPbm[FelisceParam::instance().model];

  m_dimRomBasis = FelisceParam::instance().dimRomBasis;
  m_useImprovedRec = FelisceParam::instance().useImprovedRec;
  m_dimOrthComp = FelisceParam::instance().dimOrthComp;

  m_tensorOrder = FelisceParam::instance().orderALP;
  m_numSource = FelisceParam::instance().numberOfSource;

  int id=0;
  m_idL = id;
  id++;
  m_idG = id;
  id++;
  if (!FelisceParam::instance().solveEigenProblem) {
    m_idGs = id;
    id++;
    if (FelisceParam::instance().model == "Monodomain") {
      if (m_tensorOrder == 3 ) {
        m_idK = id;
        id++;
        m_idKs = id;
        id++;
      }
    } else if ( (FelisceParam::instance().model == "Bidomain") || (FelisceParam::instance().model == "BidomainCurv") ) {
      m_idK = id;
      id++;
      m_idKie = id;
      id++;
      if (m_tensorOrder == 3 ) {
        m_idKs = id;
        id++;
      }
    }

    if ( (m_useImprovedRec) & (m_idK == 0) ) {
      m_idK = id;
      id++;
    }
  }
  m_numberOfMatrix = id;

  m_coefChi = FelisceParam::instance().chiSchrodinger;
  std::vector<PhysicalVariable> listVariable;
  std::vector<std::size_t> listNumComp;
  switch (m_problem) {
  case 0: {
    nameOfTheProblem = "Problem Fisher-Kolmogorov-Petrovski-Piskunov equation (FKPP)";
    listVariable.push_back(potTransMemb);
    listNumComp.push_back(1);
    //define unknown of the linear system.
    m_listUnknown.push_back(potTransMemb);
    break;
  }
  case 1: {
    nameOfTheProblem = "Problem cardiac Monodomain";
    listVariable.push_back(potTransMemb);
    listNumComp.push_back(1);
    //define unknown of the linear system.
    m_listUnknown.push_back(potTransMemb);
    break;
  }
  case 2: {
    nameOfTheProblem = "Problem cardiac Bidomain";
    listVariable.push_back(potTransMemb);
    listNumComp.push_back(1);
    //define unknown of the linear system.
    m_listUnknown.push_back(potTransMemb);
    break;
  }
  default:
    FEL_ERROR("Model not defined for ALP solver.");
    break;
  }
  definePhysicalVariable(listVariable,listNumComp);

}

void EigenProblemALP::initializeSystemSolver() {
  if (m_problem != 2) {
    FEL_ERROR("Method not implemented for this problem.");
  }
  felInt* nnz = new felInt[3*m_dimRomBasis+1];
  for ( felInt i = 0; i<m_dimRomBasis; i++) {
    if (m_method == 1 || m_method == 2) {
      nnz[i] = 1;
      nnz[i+m_dimRomBasis] = 1;
    } else if (m_method == 3 || m_method == 4) {
      nnz[i] = m_dimRomBasis;
      nnz[i+m_dimRomBasis] = m_dimRomBasis;
    }
    nnz[i+2*m_dimRomBasis] = 2*m_dimRomBasis+1;
  }
  nnz[3*m_dimRomBasis] = m_dimRomBasis+1;
  m_matrixRom.createSeqAIJ(PETSC_COMM_SELF, 3*m_dimRomBasis+1, 3*m_dimRomBasis+1, 0, nnz);
  m_matrixRom.setFromOptions();
  m_matrixRom.setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
  delete [] nnz;

  m_matrixRom.getVecs(m_solutionRom,m_rhsRom);

  int ii;
  for (int i=0; i<m_dimRomBasis; i++) {
    ii=i;
    m_solutionRom.setValues(1,&ii,&m_beta[i],INSERT_VALUES);
    ii=i+m_dimRomBasis;
    m_solutionRom.setValues(1,&ii,&m_mu[i],INSERT_VALUES);
    ii=i+2*m_dimRomBasis;
    m_solutionRom.setValues(1,&ii,&m_xi[i],INSERT_VALUES);
  }
  m_solutionRom.assembly();

  if (FelisceParam::verbose() > 40)
    m_solutionRom.view();

  m_kspRom->init();
  m_kspRom->setKSPandPCType(KSPGMRES,PCASM);
  m_kspRom->setTolerances(1.e-10,1.e-10,1000,200);
  m_kspRom->setKSPOptions(KSPGMRES,true);
}

// Define Physical Variable associate to the problem
void EigenProblemALP::initPerElementType() {
  switch(m_problem) {
  case 0: {
    //Init pointer on Finite Element, Variable or idVariable
    m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
    m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
    m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
    break;
  }
  case 1: {
    //Init pointer on Finite Element, Variable or idVariable
    m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
    m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
    m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
    break;
  }
  case 2: {
    //Init pointer on Finite Element, Variable or idVariable
    m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
    m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
    m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
    break;
  }
  default:
    FEL_ERROR("Model not defined for ALP solver.");
    break;
  }

  if (FelisceParam::instance().hasInfarct) {
    m_elemFieldFhNf0.initialize(DOF_FIELD,*m_fePotTransMemb);
  }
}

// Assemble Matrix
void EigenProblemALP::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_ELEM_ID_POINT;
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  switch(m_problem) {
  case 0: { // FKPP
    m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
    m_fePotTransMemb->updateFirstDeriv(0, elemPoint);


    // m_Matrix[0] = Am * grad(phi_i) * grad(phi_j)
    m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().Am,*m_fePotTransMemb,0,0,1);

    if (FelisceParam::instance().hasInitialCondition || FelisceParam::instance().schroFilter) {
      // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
      m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
      m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMemb,0,0,1);
    }

    // Matrix[1] = phi_i * phi_j
    m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);

    if (!FelisceParam::instance().solveEigenProblem) {
      if (m_useImprovedRec) {
        // m_Matrix[m_numberOfMatrix-1] = grad(phi_i) * grad(phi_j)
        m_elementMat[m_idK]->grad_phi_i_grad_phi_j(1.,*m_fePotTransMemb,0,0,1);
      }
    }
    break;
  }
  case 1: { // Monodomain
    m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
    m_fePotTransMemb->updateFirstDeriv(0, elemPoint);


    // m_Matrix[0] = grad(phi_i) * grad(phi_j)
    m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

    if (FelisceParam::instance().testCase == 1) {
      std::vector<double> elemFiber;
      getFiberDirection(iel,m_ipotTransMemb, elemFiber);
      // m_Matrix[0] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
      m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
    }

    if (FelisceParam::instance().hasInitialCondition || FelisceParam::instance().schroFilter) {
      // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
      m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
      m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMemb,0,0,1);
    }

    // m_Matrix[1] = phi_i * phi_j
    m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);

    if (!FelisceParam::instance().solveEigenProblem) {

      if (FelisceParam::instance().hasInfarct) {
        // m_Matrix[1] = B_ij = s(x) * phi_i * phi_j
        m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMat[m_idGs]->a_phi_i_phi_j(1.,m_elemFieldFhNf0,*m_fePotTransMemb,0,0,1);
      } else {
        double s = FelisceParam::instance().f0;
        // m_Matrix[1] = B_ij = s * phi_i * phi_j
        m_elementMat[m_idGs]->phi_i_phi_j(s,*m_fePotTransMemb,0,0,1);
      }

      if ( ( (m_tensorOrder == 4 ) & (m_useImprovedRec) ) || (m_tensorOrder == 3 ) ) {
        // m_Matrix[2] = E_ij = grad(phi_i) * grad(phi_j)
        m_elementMat[m_idK]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
        if (FelisceParam::instance().testCase == 1) {
          std::vector<double> elemFiber;
          getFiberDirection(iel,m_ipotTransMemb, elemFiber);
          // m_Matrix[2] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
          m_elementMat[m_idK]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
        }
      }

      if (m_tensorOrder == 3 ) {
        if (FelisceParam::instance().hasInfarct) {
          if (FelisceParam::instance().testCase == 2) {
            // m_Matrix[3] = E^s_ij = s(x) * \sigma_i^t grad(phi_i), grad(phi_j)
            m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
            m_elementMat[m_idKs]->a_grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,m_elemFieldFhNf0,*m_fePotTransMemb,0,0,1);
          } else if (FelisceParam::instance().testCase == 1) {
            FEL_ERROR("Error: infarct in fiber directed conductivity case not yet implemented.");
          }
        } else {
          double s = FelisceParam::instance().f0;
          if (FelisceParam::instance().testCase == 2) {
            // m_Matrix[3] = E^s_ij = s*\sigma_i^t grad(phi_i), grad(phi_j)
            m_elementMat[m_idKs]->grad_phi_i_grad_phi_j(s*FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
          } else if (FelisceParam::instance().testCase == 1) {
            std::vector<double> elemFiber;
            getFiberDirection(iel,m_ipotTransMemb, elemFiber);
            // m_Matrix[3] = E^s_ij +=  s*(\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
            m_elementMat[m_idKs]->tau_grad_phi_i_tau_grad_phi_j(s*(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMemb,0,0,1);
          }
        }
      }
    }
    break;
  }
  case 2: {
    m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
    m_fePotTransMemb->updateFirstDeriv(0, elemPoint);


    // m_Matrix[0] = grad(phi_i) * grad(phi_j)
    m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

    if (FelisceParam::instance().testCase == 1) {
      std::vector<double> elemFiber;
      getFiberDirection(iel,m_ipotTransMemb, elemFiber);
      // m_Matrix[0] = \sigma_i^t \grad V_m
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
      // m_Matrix[0] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
      m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
    }

    if (FelisceParam::instance().hasInitialCondition || FelisceParam::instance().schroFilter) {
      // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
      m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
      m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMemb,0,0,1);
    }

    // m_Matrix[1] = phi_i * phi_j
    m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);

    if (!FelisceParam::instance().solveEigenProblem) {
      if (FelisceParam::instance().hasInfarct) {
        // m_Matrix[1] = B_ij = s(x) * phi_i * phi_j
        m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMat[m_idGs]->a_phi_i_phi_j(1.,m_elemFieldFhNf0,*m_fePotTransMemb,0,0,1);
      } else {
        double s = FelisceParam::instance().f0;
        // m_Matrix[1] = B_ij = s * phi_i * phi_j
        m_elementMat[m_idGs]->phi_i_phi_j(s,*m_fePotTransMemb,0,0,1);
      }

      // Matrix[2] = E_ij = \sigma_i^t grad(phi_i), grad(phi_j)
      m_elementMat[m_idK]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
      // Matrix[3] = Q_ij = (\sigma_i^t+\sigma_e^t) grad(phi_i), grad(phi_j)
      m_elementMat[m_idKie]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotTransMemb,0,0,1);
      if (FelisceParam::instance().testCase == 1) {
        std::vector<double> elemFiber;
        getFiberDirection(iel,m_ipotTransMemb, elemFiber);
        // Matrix[2] = E_ij = (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i), grad(phi_j)
        m_elementMat[m_idK]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
        // Matrix[3] = Q_ij += (\sigma_i^l-\sigma_i^t+\sigma_e^l-\sigma_e^t) a vec a grad(phi_i) * grad(phi_j)
        m_elementMat[m_idKie]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraFiberTensor-FelisceParam::instance().extraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
      }

      if (m_tensorOrder == 3 ) {
        if (FelisceParam::instance().hasInfarct) {
          if (FelisceParam::instance().testCase == 2) {
            // m_Matrix[3] = E^s_ij = s(x) * \sigma_i^t grad(phi_i), grad(phi_j)
            m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
            m_elementMat[m_idKs]->a_grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,m_elemFieldFhNf0,*m_fePotTransMemb,0,0,1);
          } else if (FelisceParam::instance().testCase == 1) {
            FEL_ERROR("Error: infarct in fiber directed conductivity case not yet implemented.");
          }
        } else {
          double s = FelisceParam::instance().f0;
          if (FelisceParam::instance().testCase == 2) {
            // m_Matrix[3] = E^s_ij = s*\sigma_i^t grad(phi_i), grad(phi_j)
            m_elementMat[m_idKs]->grad_phi_i_grad_phi_j(s*FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
          } else if (FelisceParam::instance().testCase == 1) {
            std::vector<double> elemFiber;
            getFiberDirection(iel,m_ipotTransMemb, elemFiber);
            // m_Matrix[3] = E^s_ij +=  s*(\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
            m_elementMat[m_idKs]->tau_grad_phi_i_tau_grad_phi_j(s*(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMemb,0,0,1);
          }
        }
      }
    }
    break;
  }
  default:
    FEL_ERROR("Model not defined for ALP solver.");
    break;
  }
}

// Visualize modes as a variable
void EigenProblemALP::writeMode(const int iter) {
  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);

  std::string fileName;
  if (iter > 0)
    fileName = "basis" + std::to_string(iter);
  else
    fileName = "basis";

  double* tmpSolToSave = nullptr;
  tmpSolToSave = new double[m_numDof];

  for (int iBasis=0; iBasis<m_dimRomBasis; iBasis++) {
    fromVecToDoubleStar(tmpSolToSave, m_basis[iBasis], rankProc, 1);
    writeEnsightVector(tmpSolToSave, iBasis, fileName);
  }
  delete [] tmpSolToSave;

  if (rankProc == 0)
    writeEnsightCase(m_dimRomBasis, 1.,fileName);

}

void EigenProblemALP::writeEnsightSolution(const int iter) {
  PetscVector solUe;
  solUe.duplicateFrom(m_U_0);
  PetscVector solW;
  solW.duplicateFrom(m_W_0);

  if (iter > 0) {
    projectOnDof(m_beta,m_sol,m_dimRomBasis);
    if ( (m_problem == 1) || (m_problem == 2)) {
      if (FelisceParam::instance().printIonicVar) {
        projectOnDof(m_mu,solW,m_dimRomBasis);
      }
    }
    if (m_problem == 2) {
      projectOnDof(m_xi,solUe,m_dimRomBasis);
    }
  } else {
    m_sol.copyFrom(m_U_0);
    if ( (m_problem == 1) || (m_problem == 2)) {
      if (FelisceParam::instance().printIonicVar) {
        solW.copyFrom(m_W_0);
      }
    }
    if (m_problem == 2) {
      solUe.copyFrom(m_Ue_0);
    }
  }

  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);

  double* tmpSolToSave = nullptr;
  tmpSolToSave = new double[m_numDof];

  if (m_problem == 0) {
    fromVecToDoubleStar(tmpSolToSave, m_sol, rankProc, 1);
    writeEnsightVector(tmpSolToSave, iter, "sol");
  }

  if ( (m_problem == 1) || (m_problem == 2)) {
    fromVecToDoubleStar(tmpSolToSave, m_sol, rankProc, 1);
    writeEnsightVector(tmpSolToSave, iter, "potTransMemb");
    if (FelisceParam::instance().printIonicVar) {
      fromVecToDoubleStar(tmpSolToSave, solW, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "ionicVar");
    }
  }

  if (m_problem == 2) {
    fromVecToDoubleStar(tmpSolToSave, solUe, rankProc, 1);
    writeEnsightVector(tmpSolToSave, iter, "potExtraCell");
  }

  delete [] tmpSolToSave;
  solUe.destroy();
  solW.destroy();

  std::vector<std::string> varNames;
  if (m_problem == 0) {
    varNames.resize(1);
    varNames[0] = "sol";
  } else if (m_problem == 1) {
    if (FelisceParam::instance().printIonicVar) {
      varNames.resize(2);
      varNames[0] = "potTransMemb";
      varNames[1] = "ionicVar";
    } else {
      varNames.resize(1);
      varNames[0] = "potTransMemb";
    }
  } else if (m_problem == 2) {
    if (FelisceParam::instance().printIonicVar) {
      varNames.resize(3);
      varNames[0] = "potTransMemb";
      varNames[1] = "potExtraCell";
      varNames[2] = "ionicVar";
    } else {
      varNames.resize(2);
      varNames[0] = "potTransMemb";
      varNames[1] = "potExtraCell";
    }
  }
  if (rankProc == 0)
    writeEnsightCase(iter+1, FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution,varNames, FelisceParam::instance().time);

}

// Solve eigenproblem to build rom basis
void EigenProblemALP::buildSolver() {
  if (FelisceParam::verbose()) PetscPrintf(PETSC_COMM_WORLD,"\nBuilding Slepc Solver.\n");

#ifdef FELISCE_WITH_SLEPC
  // m_Matrix[0] * Phi = lambda * m_Matrix[1] * Phi
  EPSSetOperators(m_eps,m_Matrix[m_idL].toPetsc(),m_Matrix[m_idG].toPetsc());
  EPSSetDimensions(m_eps,2*m_dimRomBasis,PETSC_DECIDE,PETSC_DECIDE);
  EPSSetProblemType(m_eps,EPS_GHEP);
  EPSSetType(m_eps, EPSKRYLOVSCHUR);
  EPSSetTolerances(m_eps,1.0e-8,1000); // 1.e-05, 200
  EPSKrylovSchurSetRestart(m_eps,0.7);
  EPSSetFromOptions(m_eps);
#endif
}

void EigenProblemALP::solve() {
  if (FelisceParam::verbose()) PetscPrintf(PETSC_COMM_WORLD,"\nSolve eigenvalue problem.\n");
#ifdef FELISCE_WITH_SLEPC
  PetscInt nconv;
  //First request a singular value from one end of the spectrum
  EPSSetWhichEigenpairs(m_eps,EPS_SMALLEST_REAL);
  //EPSSetInterval(m_eps,-1.e-1,200.);
  if (m_verbose >= 10) {
    m_Matrix[m_idL].view();
    m_Matrix[m_idG].view();
    EPSView(m_eps,PETSC_VIEWER_STDOUT_WORLD);
  }

  EPSSolve(m_eps);

  //Get number of converged singular values
  EPSGetConverged(m_eps,&nconv);
  PetscPrintf(PETSC_COMM_WORLD," Number of converged values = %d.\n", nconv);

  if ( m_dimRomBasis > m_numDof ) {
    PetscPrintf(PETSC_COMM_WORLD," Warning! Number of function basis bigger than number of dof.\n");
    m_dimRomBasis = m_numDof;
  }

  if ( m_dimRomBasis > nconv ) {
    PetscPrintf(PETSC_COMM_WORLD," Warning! Number of function basis bigger than number of converged values.\n");
    m_dimRomBasis = nconv;
  }

  double sigma;
  //Get converged singular values: largest singular value is stored in sigma.
  if (nconv > 0) {
    for (int i=0; i < m_dimRomBasis; i++) {
      EPSGetEigenpair(m_eps,i,&sigma,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR);
    }
  } else {
    m_dimRomBasis = 0;
    PetscPrintf(PETSC_COMM_WORLD," Unable to compute smallest singular value!\n\n");
    exit(1);
  }
  PetscPrintf(PETSC_COMM_WORLD, "m_dimRomBasis = %d \n", m_dimRomBasis);

  if (m_eigenValue == nullptr)
    m_eigenValue = new double[m_dimRomBasis];
  m_basis.resize(m_dimRomBasis);
  double norm2;
  double normM;
  PetscVector tmpVec;
  m_Matrix[m_idG].getVecs(nullPetscVector,tmpVec);
  double imgpart = 0.;
  for (int i=0; i < m_dimRomBasis; i++) {
    m_Matrix[m_idG].getVecs(nullPetscVector,m_basis[i]);
    EPSGetEigenpair(m_eps,i,&m_eigenValue[i],&imgpart,m_basis[i].toPetsc(),FELISCE_PETSC_NULLPTR);
    if (Tools::notEqual(imgpart,0.0)) {
      PetscPrintf(PETSC_COMM_WORLD, "Warning! Im(m_eigenValue[%d]) = %e \n", i, imgpart);
    }
    m_basis[i].norm(NORM_2,&norm2);

    mult(m_Matrix[m_idG], m_basis[i], tmpVec);
    dot(m_basis[i],tmpVec,&normM);
    if (m_verbose >= 1) {
      PetscPrintf(PETSC_COMM_WORLD, "m_eigenValue[%d] = %e \n", i, m_eigenValue[i]);
      PetscPrintf(PETSC_COMM_WORLD, "norm2[%d] = %e \n", i, norm2);
      PetscPrintf(PETSC_COMM_WORLD, "normM[%d] = %e \n", i, normM);
    }
  }
  tmpVec.destroy();
  std::string fileName = FelisceParam::instance().resultDir + "/eigenValue";
  viewALP(m_eigenValue,m_dimRomBasis,fileName);
#endif
}

// Reduced model functions
//========================
// Initialization
void EigenProblemALP::readData(IO& io) {
  if (FelisceParam::verbose() > 0) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::readData.\n");
  m_Matrix[m_idG].getVecs(m_U_0,nullPetscVector);
  m_U_0.setFromOptions();

  m_U_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
  m_U_0_seq.setFromOptions();

  m_W_0.duplicateFrom(m_U_0);
  m_W_0.copyFrom(m_U_0);

  m_W_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
  m_W_0_seq.setFromOptions();

  m_Ue_0.duplicateFrom(m_U_0);
  m_Ue_0.copyFrom(m_U_0);

  m_Ue_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
  m_Ue_0_seq.setFromOptions();

  if (FelisceParam::instance().hasInitialCondition) {
    // Read initial solution file (*.00000.scl)
    int idInVar = 0;
    // potTransMemb
    double* initialSolution = nullptr;
    initialSolution = new double[m_mesh->numPoints()];
    io.readVariable(idInVar, 0.,initialSolution, m_mesh->numPoints());
    idInVar ++;
    // ionicVar
    double* ionicSolution = nullptr;
    if ( (m_problem == 1) || (m_problem == 2) ) {
      ionicSolution = new double[m_mesh->numPoints()];
      io.readVariable(idInVar, 0.,ionicSolution, m_mesh->numPoints());
      idInVar ++;
    }
    // potExtraCell
    double* extraCellSolution = nullptr;
    if (m_problem == 2) {
      extraCellSolution = new double[m_mesh->numPoints()];
      io.readVariable(idInVar, 0.,extraCellSolution, m_mesh->numPoints());
      idInVar ++;
    }
    // initialize (parallel) initial solution vectors
    fromDoubleStarToVec(initialSolution, m_U_0, m_mesh->numPoints());
    if ( (m_problem == 1) || (m_problem == 2) )
      fromDoubleStarToVec(ionicSolution, m_W_0, m_mesh->numPoints());
    if (m_problem == 2 )
      fromDoubleStarToVec(extraCellSolution, m_Ue_0, m_mesh->numPoints());

    // initialize SEQUENTIAL initial solution vectors (in order to use elemField and define collocation pts solution)
    felInt ia;
    for (felInt i=0; i< m_numDof; i++) {
      ia = i;
      AOApplicationToPetsc(m_ao,1,&ia);
      m_U_0_seq.setValues(1,&ia,&initialSolution[i],INSERT_VALUES);
      if ( (m_problem == 1) || (m_problem == 2) )
        m_W_0_seq.setValues(1,&ia,&ionicSolution[i],INSERT_VALUES);
      if (m_problem == 2)
        m_Ue_0_seq.setValues(1,&ia,&extraCellSolution[i],INSERT_VALUES);
    }
    m_U_0_seq.assembly();
    if ( (m_problem == 1) || (m_problem == 2) ) {
      m_W_0_seq.assembly();
    }
    if (m_problem == 2) {
      m_Ue_0_seq.assembly();
    }

    if (initialSolution) delete [] initialSolution;
    if (ionicSolution) delete [] ionicSolution;
    if (extraCellSolution) delete [] extraCellSolution;

    // Read fibers file (*.00000.vct)
    if (FelisceParam::instance().testCase == 1) {
      if (m_fiber == nullptr)
        m_fiber = new double[m_mesh->numPoints()*3];
      io.readVariable(idInVar, 0.,m_fiber, m_mesh->numPoints()*3);
      idInVar ++;
    }
  }
  else {
    PetscPrintf(PETSC_COMM_WORLD, "U_0 not read from file (zero vector).");
    m_U_0.set( 0.);
    m_U_0.assembly();
    if ( (m_problem == 1) || (m_problem == 2) ) {
      if (FelisceParam::instance().typeOfIonicModel == "schaf") {
        m_W_0.set( 1.);
        m_W_0_seq.set( 1.);
      }
      else {
        m_W_0.set( 0.);
        m_W_0_seq.set( 0.);
      }
      m_W_0.assembly();
      m_W_0_seq.assembly();
    }
    m_U_0_seq.set( 0.);
    m_U_0_seq.assembly();
    if (m_problem == 2) {
      m_Ue_0.set( 0.);
      m_Ue_0.assembly();
      m_Ue_0_seq.set( 0.);
      m_Ue_0_seq.assembly();
    }
  }

}

void EigenProblemALP::getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber) {
  int numSupport = static_cast<int>(m_supportDofUnknown[iUnknown].iEle()[iel+1] - m_supportDofUnknown[iUnknown].iEle()[iel]);
  elemFiber.resize( numSupport*3,0.);
  for (int i = 0; i < numSupport; i++) {
    elemFiber[i*3] = m_fiber[3*(m_supportDofUnknown[iUnknown].iSupportDof()[m_supportDofUnknown[iUnknown].iEle()[iel]+i])];
    elemFiber[i*3+1] = m_fiber[3*(m_supportDofUnknown[iUnknown].iSupportDof()[m_supportDofUnknown[iUnknown].iEle()[iel]+i])+1];
    elemFiber[i*3+2] = m_fiber[3*(m_supportDofUnknown[iUnknown].iSupportDof()[m_supportDofUnknown[iUnknown].iEle()[iel]+i])+2];
  }
}

void EigenProblemALP::readBasis() {
  if (FelisceParam::verbose() > 0 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::readBasis()\n");

  m_basis.resize(m_dimRomBasis);
  for (int i=0; i<m_dimRomBasis; i++) {
    m_Matrix[m_idG].getVecs(m_basis[i],nullPetscVector);
    m_basis[i].setFromOptions();
    double* vec = new double[m_numDof];
    readEnsightFile(vec,"basis",i,m_numDof);
    fromDoubleStarToVec(vec, m_basis[i], m_numDof);
    delete [] vec;
    if (FelisceParam::verbose() > 40) m_basis[i].view();
  }

  readEigenValueFromFile();

}

void EigenProblemALP::initializeROM() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::initializeROM()\n");

  m_basis.resize(m_dimRomBasis);
  for (int i=0; i<m_dimRomBasis; i++) {
    m_Matrix[m_idG].getVecs(m_basis[i],nullPetscVector);
    m_basis[i].setFromOptions();
    double* vec = new double[m_numDof];
    readEnsightFile(vec,"basis",i,m_numDof);
    fromDoubleStarToVec(vec, m_basis[i], m_numDof);
    delete [] vec;
    if (FelisceParam::verbose() > 40) m_basis[i].view();
  }

  if(m_useImprovedRec==true) {
    m_orthComp.resize(m_dimOrthComp);
    for(int i=0; i<m_dimOrthComp; i++) {
      int fileInd = i + m_dimRomBasis;
      m_Matrix[m_idG].getVecs(m_orthComp[i],nullPetscVector);
      m_orthComp[i].setFromOptions();
      double* vec = new double[m_numDof];
      readEnsightFile(vec,"basis",fileInd,m_numDof);
      fromDoubleStarToVec(vec, m_orthComp[i], m_numDof);
      delete [] vec;
      if (FelisceParam::verbose() > 40) m_orthComp[i].view();
    }
  }

  readEigenValueFromFile();
  initializeSolution();
}

void EigenProblemALP::readEnsightFile(double* vec, std::string varName, int idIter, felInt size) {
  std::string iteration;
  if (idIter < 10)
    iteration = "0000" + std::to_string(idIter);
  else if (idIter < 100)
    iteration = "000" + std::to_string(idIter);
  else if (idIter < 1000)
    iteration = "00" + std::to_string(idIter);
  else if (idIter < 10000)
    iteration = "0" + std::to_string(idIter);
  else if (idIter < 100000)
    iteration = std::to_string(idIter);

  std::string fileName = FelisceParam::instance().inputDirectory + "/" + varName + "." + iteration + ".scl";
  if (FelisceParam::verbose() > 1) PetscPrintf(PETSC_COMM_WORLD, "Read file %s\n", fileName.c_str());
  FILE* pFile = fopen (fileName.c_str(),"r");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to read "+fileName +".");
  }
  char str[80];
  std::string display;
  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_WARNING("Failed ot read str");
  }
  display = str;
  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_WARNING("Failed ot read str");
  }
  display = str;
  if ( fscanf(pFile,"%s", str) !=1) {
    FEL_WARNING("Failed ot read str");
  }
  display = str;
  //Read values
  int count = 0;
  for ( felInt i = 0; i < size; i++) {
    if ( fscanf(pFile,"%lf", &vec[i]) !=1 ) {
      FEL_WARNING("Failed ot read vec");
    }
    if ( count == 6 )  {
      if ( fscanf( pFile, "\n" ) !=0 ) {
        FEL_WARNING("Failed ot read");
      }
      count=0;
    }
    count++;
  }
  fclose(pFile);
}

void EigenProblemALP::readEigenValueFromFile() {
  std::string fileName = FelisceParam::instance().inputDirectory + "/eigenValue";
  if (FelisceParam::verbose() > 1) PetscPrintf(PETSC_COMM_WORLD, "Read file %s\n", fileName.c_str());
  FILE* pFile = fopen (fileName.c_str(),"r");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to read "+fileName +".");
  }
  //Read values
  m_eigenValue = new double[m_dimRomBasis];
  for ( felInt i = 0; i < m_dimRomBasis; i++) {
    if ( fscanf(pFile,"%lf", &m_eigenValue[i]) !=1 ) {
      FEL_WARNING("Failed ot read vec");
    }
    if ( fscanf( pFile, "\n" ) !=0 ) {
      FEL_WARNING("Failed ot read");
    }
  }
  fclose(pFile);

  if (FelisceParam::verbose() > 10) {
    PetscPrintf(PETSC_COMM_WORLD, "EigenValues:\n");
    for ( felInt i = 0; i < m_dimRomBasis; i++) {
      PetscPrintf(PETSC_COMM_WORLD, "[%d] %e \n", i, m_eigenValue[i]);
    }
  }

}

void EigenProblemALP::computeGamma() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::computeGamma()\n");

  if (m_gamma == nullptr)
    m_gamma = new double[m_dimRomBasis];

  switch (m_problem) {
  case 0: {
    // FKPP:
    double muFKPP = 10.;
    double coef = - (m_coefChi + muFKPP);
    // FKPP:
    // m_gamma_p = (muFKPP - lambda_p) * beta_p - (m_coefChi + muFKPP) * sum_{ij} beta_i * beta_j * T_{ijp}
    for (int p=0; p<m_dimRomBasis; p++) {

      m_gamma[p] = (muFKPP - m_eigenValue[p]) * m_beta[p];
      for (int i=0; i<m_dimRomBasis; i++) {
        for (int j=0; j<m_dimRomBasis; j++) {
          m_gamma[p] += coef * m_beta[i] * m_beta[j] * m_tensorT[thirdOrderGlobalIndex(i,j,p)];
        }
      }
    }
    break;
  }
  case 1:
  case 2: {
    // MONODOMAIN or BIDOMAIN:
    double Am = FelisceParam::instance().Am;
    double Cm = FelisceParam::instance().Cm;
    double a = FelisceParam::instance().alpha;
    double eps = FelisceParam::instance().epsilon;
    double gam = FelisceParam::instance().gammaEl;

    if (m_eta == nullptr)
      m_eta = new double[m_dimRomBasis];

    switch (m_tensorOrder) {
    case 3: {
      // MONODOMAIN:
      // gamma_p = - Am mu_p - A_m a sum_{i} (a + (a+1)lambda_i / chi + lambda_i^2/chi^2 )beta_i B_{ip} - sum_{i} beta_i E_{ip}  + Am sum_{i} ((a+1)/chi + lambda_i/chi^2) beta_i E^s_ip - Am/chi sum_{ij} beta_i beta_j U_ijp
      // eta_p = eps * ro * beta_p - eps * mu_p

      // BIDOMAIN:
      // gamma_p = - Am mu_p - A_m a sum_{i} (a + (a+1)lambda_i / chi + lambda_i^2/chi^2 )beta_i B_{ip} - sum_{i} beta_i E_{ip} - sum_{i} xi_i E_{ip}  + Am sum_{i} ((a+1)/chi + lambda_i/chi^2) beta_i E^s_ip - Am/chi sum_{ij} beta_i beta_j U_ijp
      // eta_p = eps * ro * beta_p - eps * mu_p

      for (int p=0; p<m_dimRomBasis; p++) {
        m_gamma[p] = - Am * m_mu[p];
        for (int i=0; i<m_dimRomBasis; i++) {
          m_gamma[p] += - Am*( a + (1+a)*m_eigenValue[i]/m_coefChi + (m_eigenValue[i]*m_eigenValue[i])/(m_coefChi*m_coefChi) ) * m_beta[i] * m_tensorB[secondOrderGlobalIndex(i,p)];
          m_gamma[p] += - m_beta[i]*m_tensorE[secondOrderGlobalIndex(i,p)];
          if (m_problem == 2) { // bidomain
            m_gamma[p] += - m_xi[i]*m_tensorE[secondOrderGlobalIndex(i,p)];
          }
          m_gamma[p] += Am*( (1+a)/m_coefChi + m_eigenValue[i]/(m_coefChi*m_coefChi) ) * m_beta[i]*m_tensorEs[secondOrderGlobalIndex(i,p)];
          for (int j=0; j<m_dimRomBasis; j++) {
            m_gamma[p] += - Am/m_coefChi * m_beta[i] * m_beta[j] * m_tensorU[i][secondOrderGlobalIndex(j,p)];
          }
        }
        if (FelisceParam::instance().hasSource) {
          double delay[m_numSource];
          double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
          for (int iS=0; iS<m_numSource; iS++) {
            delay[iS] = FelisceParam::instance().delayStim[iS];
            if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
              m_gamma[p] += Am*m_source[iS][p];
          }
        }

        m_gamma[p] = m_gamma[p] / (Am*Cm);

        m_eta[p] = eps * ( gam * m_beta[p] - m_mu[p] );

      }

      break;
    }
    case 4: {
      // MONODOMAIN:
      // gamma_p = - lambda_p beta_p - Am mu_p - A_m a sum_{i} beta_i B_{ip} - \chi sum_{ij} beta_i beta_j T_{ijp}  + (a+1)Am sum_{ij} beta_i beta_j T^s_ijp - Am sum_{ijk} beta_i beta_j beta_k Y_ijkp
      // eta_p = eps * ro * beta_p - eps * mu_p

      // BIDOMAIN:
      // gamma_p = - lambda_p beta_p - lambda_p xi_p- Am mu_p - A_m a sum_{i} beta_i B_{ip} - \chi sum_{ij} beta_i beta_j T_{ijp} - \chi sum_{ij} beta_i xi_j T_{ijp}  + (a+1)Am sum_{ij} beta_i beta_j T^s_ijp - Am sum_{ijk} beta_i beta_j beta_k Y_ijkp
      // eta_p = eps * ro * beta_p - eps * mu_p

      for (int p=0; p<m_dimRomBasis; p++) {
        m_gamma[p] = - m_eigenValue[p] * m_beta[p];
        m_gamma[p] += - Am * m_mu[p];
        if (m_problem == 2) { // bidomain
          m_gamma[p] += - m_eigenValue[p] * m_xi[p];
        }
        for (int i=0; i<m_dimRomBasis; i++) {
          m_gamma[p] += - a*Am * m_beta[i] * m_tensorB[secondOrderGlobalIndex(i,p)];
          for (int j=0; j<m_dimRomBasis; j++) {
            m_gamma[p] += (a+1)*Am * m_beta[i] * m_beta[j] * m_tensorTs[thirdOrderGlobalIndex(i,j,p)];
            m_gamma[p] += - m_coefChi * m_beta[i] * m_beta[j] * m_tensorT[thirdOrderGlobalIndex(i,j,p)];
            if (m_problem == 2) { // bidomain
              m_gamma[p] += - m_coefChi * m_beta[i] * m_xi[j] * m_tensorT[thirdOrderGlobalIndex(i,j,p)];
            }
            for (int k=0; k<m_dimRomBasis; k++) {
              m_gamma[p] += - Am * m_beta[i] * m_beta[j] * m_beta[k] * m_tensorY[fourthOrderGlobalIndex(i,j,k,p)];
            }
          }
        }

        if (FelisceParam::instance().hasSource) {
          double delay[m_numSource];
          double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
          for (int iS=0; iS<m_numSource; iS++) {
            delay[iS] = FelisceParam::instance().delayStim[iS];
            if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
              m_gamma[p] += Am*m_source[iS][p];
          }
        }

        m_gamma[p] = m_gamma[p] / (Am*Cm);

        m_eta[p] = eps * ( gam * m_beta[p] - m_mu[p] );

      }
      break;
    }
    default:
      break;
    }
    break;
  }
  default:
    FEL_ERROR("Model not defined for ALP solver.");
    break;
  }
}

void EigenProblemALP::computeGammaExtrap() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::computeGamma()\n");

  if (m_problem != 2) {
    FEL_ERROR("Time derivative method not implemented for this problem.");
  }

  if (m_gamma_extrap == nullptr)
    m_gamma_extrap = new double[m_dimRomBasis];
  if (m_eta_extrap == nullptr)
    m_eta_extrap = new double[m_dimRomBasis];

  double* beta_extrap = new double[m_dimRomBasis];
  double* mu_extrap = new double[m_dimRomBasis];
  double* xi_extrap = new double[m_dimRomBasis];

  double coefExtrap[2];
  coefExtrap[0] = 2.;
  coefExtrap[1] = -1.;

  for (int i=0; i<m_dimRomBasis; i++) {
    beta_extrap[i] = coefExtrap[0]*m_beta[i] + coefExtrap[1]*m_beta_n_1[i];
    mu_extrap[i] = coefExtrap[0]*m_mu[i] + coefExtrap[1]*m_mu_n_1[i];
    xi_extrap[i] = coefExtrap[0]*m_xi[i] + coefExtrap[1]*m_xi_n_1[i];
  }

  // MONODOMAIN or BIDOMAIN:
  double Am = FelisceParam::instance().Am;
  double Cm = FelisceParam::instance().Cm;
  double a = FelisceParam::instance().alpha;
  double eps = FelisceParam::instance().epsilon;
  double gam = FelisceParam::instance().gammaEl;


  switch (m_tensorOrder) {
  case 3: {
    // MONODOMAIN:
    // gamma_p = - Am mu_p - A_m a sum_{i} (a + (a+1)lambda_i / chi + lambda_i^2/chi^2 )beta_i B_{ip} - sum_{i} beta_i E_{ip}  + Am sum_{i} ((a+1)/chi + lambda_i/chi^2) beta_i E^s_ip - Am/chi sum_{ij} beta_i beta_j U_ijp
    // eta_p = eps * ro * beta_p - eps * mu_p

    // BIDOMAIN:
    // gamma_p = - Am mu_p - A_m a sum_{i} (a + (a+1)lambda_i / chi + lambda_i^2/chi^2 )beta_i B_{ip} - sum_{i} beta_i E_{ip} - sum_{i} xi_i E_{ip}  + Am sum_{i} ((a+1)/chi + lambda_i/chi^2) beta_i E^s_ip - Am/chi sum_{ij} beta_i beta_j U_ijp
    // eta_p = eps * ro * beta_p - eps * mu_p

    for (int p=0; p<m_dimRomBasis; p++) {
      m_gamma_extrap[p] = - Am * mu_extrap[p];
      for (int i=0; i<m_dimRomBasis; i++) {
        m_gamma_extrap[p] += - Am*( a + (1+a)*m_eigenValue[i]/m_coefChi + (m_eigenValue[i]*m_eigenValue[i])/(m_coefChi*m_coefChi) ) * beta_extrap[i] * m_tensorB[secondOrderGlobalIndex(i,p)];
        m_gamma_extrap[p] += - beta_extrap[i]*m_tensorE[secondOrderGlobalIndex(i,p)];
        m_gamma_extrap[p] += - xi_extrap[i]*m_tensorE[secondOrderGlobalIndex(i,p)];
        m_gamma_extrap[p] += Am*( (1+a)/m_coefChi + m_eigenValue[i]/(m_coefChi*m_coefChi) ) * beta_extrap[i]*m_tensorEs[secondOrderGlobalIndex(i,p)];
        for (int j=0; j<m_dimRomBasis; j++) {
          m_gamma_extrap[p] += - Am/m_coefChi * beta_extrap[i] * beta_extrap[j] * m_tensorU[i][secondOrderGlobalIndex(j,p)];
        }
      }
      if (FelisceParam::instance().hasSource) {
        double delay[m_numSource];
        double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
        for (int iS=0; iS<m_numSource; iS++) {
          delay[iS] = FelisceParam::instance().delayStim[iS];
          if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
            m_gamma_extrap[p] += Am*m_source[iS][p];
        }
      }

      m_gamma_extrap[p] = m_gamma_extrap[p] / (Am*Cm);

      m_eta_extrap[p] = eps * ( gam * beta_extrap[p] - mu_extrap[p] );

    }

    break;
  }
  case 4: {
    // MONODOMAIN:
    // gamma_p = - lambda_p beta_p - Am mu_p - A_m a sum_{i} beta_i B_{ip} - \chi sum_{ij} beta_i beta_j T_{ijp}  + (a+1)Am sum_{ij} beta_i beta_j T^s_ijp - Am sum_{ijk} beta_i beta_j beta_k Y_ijkp
    // eta_p = eps * ro * beta_p - eps * mu_p

    // BIDOMAIN:
    // gamma_p = - lambda_p beta_p - lambda_p xi_p- Am mu_p - A_m a sum_{i} beta_i B_{ip} - \chi sum_{ij} beta_i beta_j T_{ijp} - \chi sum_{ij} beta_i xi_j T_{ijp}  + (a+1)Am sum_{ij} beta_i beta_j T^s_ijp - Am sum_{ijk} beta_i beta_j beta_k Y_ijkp
    // eta_p = eps * ro * beta_p - eps * mu_p

    for (int p=0; p<m_dimRomBasis; p++) {
      m_gamma_extrap[p] = - m_eigenValue[p] * beta_extrap[p];
      m_gamma_extrap[p] += - Am * m_mu[p];
      m_gamma_extrap[p] += - m_eigenValue[p] * xi_extrap[p];
      for (int i=0; i<m_dimRomBasis; i++) {
        m_gamma_extrap[p] += - a*Am * beta_extrap[i] * m_tensorB[secondOrderGlobalIndex(i,p)];
        for (int j=0; j<m_dimRomBasis; j++) {
          m_gamma_extrap[p] += (a+1)*Am * beta_extrap[i] * beta_extrap[j] * m_tensorTs[thirdOrderGlobalIndex(i,j,p)];
          m_gamma_extrap[p] += - m_coefChi * beta_extrap[i] * beta_extrap[j] * m_tensorT[thirdOrderGlobalIndex(i,j,p)];
          m_gamma_extrap[p] += - m_coefChi * beta_extrap[i] * xi_extrap[j] * m_tensorT[thirdOrderGlobalIndex(i,j,p)];
          for (int k=0; k<m_dimRomBasis; k++) {
            m_gamma_extrap[p] += - Am * beta_extrap[i] * beta_extrap[j] * beta_extrap[k] * m_tensorY[fourthOrderGlobalIndex(i,j,k,p)];
          }
        }
      }

      if (FelisceParam::instance().hasSource) {
        double delay[m_numSource];
        double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
        for (int iS=0; iS<m_numSource; iS++) {
          delay[iS] = FelisceParam::instance().delayStim[iS];
          if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
            m_gamma_extrap[p] += Am*m_source[iS][p];
        }
      }

      m_gamma_extrap[p] = m_gamma_extrap[p] / (Am*Cm);

      m_eta_extrap[p] = eps * ( gam * beta_extrap[p] - mu_extrap[p] );

    }
    break;
  }
  default:
    break;
  }

  delete[] beta_extrap;
  delete[] mu_extrap;
  delete[] xi_extrap;

}

void EigenProblemALP::computeMatrixZeta() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::computeMatrixZeta()\n");
  if ( m_zeta.size() == 0 ) {
    m_size2 = static_cast<int>(m_dimRomBasis*(m_dimRomBasis+1)/2);
    m_zeta.resize(m_size2);
    for (int i=0; i<m_size2; i++)
      m_zeta[i].duplicateFrom(m_basis[0]);
  }

  for (int i=0; i<m_dimRomBasis; i++) { // row
    for (int j=0; j<i+1; j++) { // column
      // secondOrderGlobalIndex(i,j) = i*(i+1)/2 + j
      pointwiseMult(m_zeta[secondOrderGlobalIndex(i,j)],m_basis[i],m_basis[j]);
    }
  }

}

void EigenProblemALP::computeMatrixM() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::computeMatrixM()\n");
  if ( m_matrixM == nullptr ) {
    m_matrixM = new double*[m_dimRomBasis];
    for (int i=0; i<m_dimRomBasis; i++) {
      m_matrixM[i] = new double[m_dimRomBasis];
    }
  }

  double coef;
  double epsilonLambda = 5.e-01; //1.e-01;
  for (int i=0; i<m_dimRomBasis; i++) {
    m_matrixM[i][i] = 0.;
    for (int j=0; j<i; j++) {
      m_matrixM[i][j] = 0.;
      if ( std::abs(m_eigenValue[i] - m_eigenValue[j] ) > epsilonLambda ) {
        coef = m_coefChi / ( m_eigenValue[j] - m_eigenValue[i] );
        for (int k=0; k < m_dimRomBasis; k++) {
          m_matrixM[i][j] += coef * m_gamma[k] * m_tensorT[thirdOrderGlobalIndex(k,i,j)];
        }
      }
      m_matrixM[j][i] = (-1.0) * m_matrixM[i][j];
    }
  }
}

int EigenProblemALP::secondOrderGlobalIndex(int& i, int& j) {
  int id;

  if (i >= j) {
    id = static_cast<int>(i*(i+1)/2+j);
  } else {
    id = static_cast<int>(j*(j+1)/2+i);
  }

  return id;
}

int EigenProblemALP::thirdOrderGlobalIndex(int& i, int& j, int& k) {
  int id;
  int s[3];
  s[0] = i;
  s[1] = j;
  s[2] = k;
  int temp;
  for (int ii=0; ii<3; ii++) {
    for (int jj=0; jj<2; jj++) {
      if (s[jj] < s[jj+1]) {
        temp = s[jj];
        s[jj] = s[jj+1];
        s[jj+1] = temp;
      }
    }
  }
  id = static_cast<int>(s[0]*(s[0]+1)*(s[0]+2)/6);
  id += static_cast<int>(s[1]*(s[1]+1)/2);
  id += s[2];
  return id;
}

int EigenProblemALP::fourthOrderGlobalIndex(int& i, int& j, int& k, int& h) {
  int id;
  int s[4];
  s[0] = i;
  s[1] = j;
  s[2] = k;
  s[3] = h;
  int temp;
  for (int ii=0; ii<4; ii++) {
    for (int jj=0; jj<3; jj++) {
      if (s[jj] < s[jj+1]) {
        temp = s[jj];
        s[jj] = s[jj+1];
        s[jj+1] = temp;
      }
    }
  }
  id = static_cast<int>(s[0]*(s[0]+1)*(s[0]*s[0]+5*s[0]+6)/24);
  id += static_cast<int>(s[1]*(s[1]+1)*(s[1]+2)/6);
  id += static_cast<int>(s[2]*(s[2]+1)/2);
  id += s[3];
  return id;
}

void EigenProblemALP::computeTensor() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::computeTensor()\n");
  int n = m_dimRomBasis;

  switch (m_problem) {
  case 0: {
    // T_ijk = < phi_i, zeta_jk >
    m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
    if (m_tensorT == nullptr)
      m_tensorT = new double [m_size3];

    double valueT;
    PetscVector phiG;
    phiG.duplicateFrom(m_basis[0]);
    for (int i=0; i<m_dimRomBasis; i++) {
      for (int j=0; j<i+1; j++) {
        // phiG = zeta_ij^T * G
        mult(m_Matrix[m_idG],m_zeta[secondOrderGlobalIndex(i,j)],phiG);
        for (int k=0; k<j+1; k++) {
          // valueT = zeta_ij^T * G * phi_k
          dot(m_basis[k],phiG,&valueT);
          m_tensorT[thirdOrderGlobalIndex(i,j,k)] = valueT;
        }
      }
    }
    phiG.destroy();
    break;
  }
  case 1:
  case 2: {
    switch (m_tensorOrder) {
    case 3: {
      // B_ij = <s(x) phi_i, phi_j > = Phi^T G^s Phi
      if (m_tensorB == nullptr)
        m_tensorB = new double [m_size2];
      // E_ij = < sigma grad(phi_i), grad(phi_j) > = Phi^T K Phi
      if (m_tensorE == nullptr)
        m_tensorE = new double [m_size2];
      if (m_problem == 2) { // bidomain
        // Q_ij = < (sigma_I+sigma_E) grad(phi_i), grad(phi_j) > = Phi^T K_IE Phi
        if (m_tensorQ == nullptr)
          m_tensorQ = new double [secondOrderGlobalIndex(m_dimRomBasis,m_dimRomBasis)+1];
      }
      // Es_ij = < s(x) sigma grad(phi_i), grad(phi_j) > = Phi^T K^s Phi
      if (m_tensorEs == nullptr)
        m_tensorEs = new double [m_size2];
      // T_ijk = < phi_i, zeta_jk > = Phi^T G Z
      m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
      if (m_tensorT == nullptr)
        m_tensorT = new double [m_size3];
      // U_ijk = < s(x) sigma grad(phi_i), grad(zeta_jk) > = Phi^T K^s Z
      if (m_tensorU == nullptr) {
        m_tensorU = new double*[m_dimRomBasis];
        for (int i=0; i<m_dimRomBasis; i++) {
          m_tensorU[i] = new double[m_size2];
        }
      }

      double value;
      for (int i=0; i<m_dimRomBasis; i++) {
        // phiG = phi_i^T * G
        PetscVector phiG;
        phiG.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idG],m_basis[i],phiG);

        // phiGs = phi_i^T * G^s
        PetscVector phiGs;
        phiGs.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idGs],m_basis[i],phiGs);

        // phiK = phi_i^T * K
        PetscVector phiK;
        phiK.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idK],m_basis[i],phiK);

        // phiKs = phi_i^T * K^s
        PetscVector phiKs;
        phiKs.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idKs],m_basis[i],phiKs);

        // phiKie = phi_i^T * K_ie
        PetscVector phiKie;
        phiKie.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idKie],m_basis[i],phiKie);

        for (int j=0; j<i+1; j++) {
          // valueB = phi_i^T * G^s * phi_j
          dot(m_basis[j],phiGs,&value);
          m_tensorB[secondOrderGlobalIndex(i,j)] = value;

          // valueE = phi_i^T * K * phi_j
          dot(m_basis[j],phiK,&value);
          m_tensorE[secondOrderGlobalIndex(i,j)] = value;

          if (m_problem == 2) { //bidomain
            // valueQ = phi_i^T * K_ie * phi_j
            dot(m_basis[j],phiKie,&value);
            m_tensorQ[secondOrderGlobalIndex(i,j)] = value;
          }

          // valueEs = phi_i^T * K^s * phi_j
          dot(m_basis[j],phiKs,&value);
          m_tensorEs[secondOrderGlobalIndex(i,j)] = value;

          for (int k=0; k<j+1; k++) {
            // valueT = phi_i^T * G * zeta_jk
            dot(m_zeta[secondOrderGlobalIndex(j,k)],phiG,&value);
            m_tensorT[thirdOrderGlobalIndex(i,j,k)] = value;
          }
        }
        for (int j=0; j<m_dimRomBasis; j++) {
          for (int k=0; k<j+1; k++) {
            // valueU = phi_i^T * K^s * zeta_jk
            dot(m_zeta[secondOrderGlobalIndex(j,k)],phiKs,&value);
            m_tensorU[i][secondOrderGlobalIndex(j,k)] = value;
          }
        }
        value = 0.;
        if (m_problem == 2) { //bidomain
          // in order to impose \int(u_E)=0
          // valueQ = sum_m phi_i,l^T * G_lm
          phiG.sum(&value);
          m_tensorQ[secondOrderGlobalIndex(i,m_dimRomBasis)] = value;
        }

        phiG.destroy();
        phiGs.destroy();
        phiK.destroy();
        phiKie.destroy();
        phiKs.destroy();
      }
      if (m_problem == 2) //bidomain
        m_tensorQ[secondOrderGlobalIndex(m_dimRomBasis,m_dimRomBasis)] =0.;
      break;
    }
    case 4: {
      // B_ij = < s phi_i, phi_j > = Phi^T G^s Phi
      if (m_tensorB == nullptr)
        m_tensorB = new double [m_size2];
      if (m_problem == 2) { // bidomain
        // E_ij = < sigma grad(phi_i), grad(phi_j) > = Phi^T K Phi
        if (m_tensorE == nullptr)
          m_tensorE = new double [m_size2];
        // Q_ij = < (sigma_i+sigma_e) phi_i, phi_j > = Phi^T K_ie Phi
        if (m_tensorQ == nullptr)
          m_tensorQ = new double [secondOrderGlobalIndex(m_dimRomBasis,m_dimRomBasis)+1];
      }
      // T_ijk = < phi_i, zeta_jk > = Phi^T G Z
      m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
      if (m_tensorT == nullptr)
        m_tensorT = new double [m_size3];
      // Ts_ijk = < s(x) phi_i, zeta_jk > = Phi^T G^s Z
      m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
      if (m_tensorTs == nullptr)
        m_tensorTs = new double [m_size3];
      // Y_ijkh = < s(x) zeta_ij, zeta_kh > = Z^T G^s Z
      m_size4 = static_cast<int>(n*(n+1)*(n*n+5*n+6)/24);
      if (m_tensorY == nullptr)
        m_tensorY = new double [m_size4];

      double value;
      for (int i=0; i<m_dimRomBasis; i++) {
        // phiG = phi_i^T * G
        PetscVector phiG;
        phiG.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idG],m_basis[i],phiG);

        // phiGs = phi_i^T * Gs
        PetscVector phiGs;
        phiGs.duplicateFrom(m_basis[0]);
        mult(m_Matrix[m_idGs],m_basis[i],phiGs);

        // phiK = phi_i^T * K
        PetscVector phiK;
        phiK.duplicateFrom(m_basis[0]);
        if (m_problem == 2)
          mult(m_Matrix[m_idK],m_basis[i],phiK);

        // phiKie = phi_i^T * K_ie
        PetscVector phiKie;
        phiKie.duplicateFrom(m_basis[0]);
        if (m_problem ==2)
          mult(m_Matrix[m_idKie],m_basis[i],phiKie);

        for (int j=0; j<i+1; j++) {
          // valueB = phi_i^T * Gs * phi_j
          dot(m_basis[j],phiGs,&value);
          m_tensorB[secondOrderGlobalIndex(i,j)] = value;

          if (m_problem == 2) { // bidomain
            // valueE = phi_i^T * K * phi_j
            dot(m_basis[j],phiK,&value);
            m_tensorE[secondOrderGlobalIndex(i,j)] = value;
            // valueQ = phi_i^T * K_ie * phi_j
            dot(m_basis[j],phiKie,&value);
            m_tensorQ[secondOrderGlobalIndex(i,j)] = value;
          }

          // zetaG = zeta_ij^T * G
          PetscVector zetaG;
          zetaG.duplicateFrom(m_basis[0]);
          mult(m_Matrix[m_idG],m_zeta[secondOrderGlobalIndex(i,j)],zetaG);

          // zetaGs = zeta_ij^T * G^s
          PetscVector zetaGs;
          zetaGs.duplicateFrom(m_basis[0]);
          mult(m_Matrix[m_idGs],m_zeta[secondOrderGlobalIndex(i,j)],zetaGs);

          for (int k=0; k<j+1; k++) {
            // valueT = zeta_ij^T * G * phi_k
            dot(m_basis[k],zetaG,&value);
            m_tensorT[thirdOrderGlobalIndex(i,j,k)] = value;

            // valueTs = zeta_ij^T * Gs * phi_k
            dot(m_basis[k],zetaGs,&value);
            m_tensorTs[thirdOrderGlobalIndex(i,j,k)] = value;

            for (int h=0; h<k+1; h++) {
              // valueY = zeta_ij^T * Gs * zeta_kh
              dot(m_zeta[secondOrderGlobalIndex(k,h)],zetaGs,&value);
              m_tensorY[fourthOrderGlobalIndex(i,j,k,h)] = value;
            }
          }
          zetaG.destroy();
          zetaGs.destroy();
        }
        value = 0.;
        // in order to impose \int(u_E)=0
        if (m_problem == 2) { //bidomain
          // valueQ = sum_m phi_i,l^T * G_lm
          phiG.sum(&value);
          m_tensorQ[secondOrderGlobalIndex(i,m_dimRomBasis)] = value;
        }

        phiG.destroy();
        phiGs.destroy();
        phiK.destroy();
        phiKie.destroy();
      }
      if (m_problem == 2) //bidomain
        m_tensorQ[secondOrderGlobalIndex(m_dimRomBasis,m_dimRomBasis)] =0.;

      break;
    }
    default:
      break;
    }
    break;
  }
  default:
    FEL_ERROR("Model not defined for ALP solver.");
    break;
  }

}

void EigenProblemALP::initializeSolution() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::initializeSolution()\n");

  if ( !m_solutionInitialized) {
    if (m_beta == nullptr)
      m_beta = new double[m_dimRomBasis];
    projectOnBasis(m_U_0,m_beta,true);

    if ( (m_problem == 1) || (m_problem == 2) ) {
      if (m_mu == nullptr)
        m_mu = new double[m_dimRomBasis];
      projectOnBasis(m_W_0,m_mu,true);
    }

    if (m_problem == 2) {
      if (m_xi == nullptr)
        m_xi = new double[m_dimRomBasis];
      projectOnBasis(m_Ue_0,m_xi,true);

    }

    if (m_method == 4) {
      if (m_beta_n_1 == nullptr)
        m_beta_n_1 = new double[m_dimRomBasis];
      if (m_mu_n_1 == nullptr)
        m_mu_n_1 = new double[m_dimRomBasis];
      if (m_xi_n_1 == nullptr)
        m_xi_n_1 = new double[m_dimRomBasis];
      for (int i=0; i<m_dimRomBasis; i++) {
        m_beta_n_1[i] = m_beta[i];
        m_mu_n_1[i] = m_mu[i];
        m_xi_n_1[i] = m_xi[i];
      }
    }


    m_solutionInitialized = true;
  }

}

void EigenProblemALP::setHeteroTauClose(std::vector<double>& valueTauClose) {
  IGNORE_UNUSED_ARGUMENT(valueTauClose);
  FEL_WARNING("Hetero TauClose for Classical ALP not implemented.");
}

void EigenProblemALP::setFhNf0(std::vector<double>& valuef0) {
  m_FhNf0.createSeq(PETSC_COMM_SELF,m_numDof);
  m_FhNf0.setFromOptions();

  // initialize SEQUENTIAL vectors (in order to use elemField)
  felInt ia;
  for (felInt i=0; i< m_numDof; i++) {
    ia = i;
    AOApplicationToPetsc(m_ao,1,&ia);
    m_FhNf0.setValues(1,&ia,&valuef0[i],INSERT_VALUES);
  }
  m_FhNf0.assembly();

}

void EigenProblemALP::setIapp(std::vector<double>& iApp, int& idS) {
  if (m_source == nullptr) {
    m_source = new double*[m_numSource];
    for (int i=0; i<m_numSource; i++) {
      m_source[i] = new double[m_dimRomBasis];
    }
  }

  PetscVector RHS;
  RHS.createSeq(PETSC_COMM_SELF,m_numDof);
  RHS.setFromOptions();
  // initialize SEQUENTIAL vectors (in order to use elemField)
  felInt ia;
  for (felInt i=0; i< m_numDof; i++) {
    ia = i;
    AOApplicationToPetsc(m_ao,1,&ia);
    RHS.setValues(1,&ia,&iApp[i],INSERT_VALUES);
  }
  RHS.assembly();

  projectOnBasis(RHS, m_source[idS], true);

  RHS.destroy();
}


void EigenProblemALP::checkSolution(PetscVector& solution, PetscVector& refSol, double & error) {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::checkSolution\n");

  PetscVector tmpVec1;
  tmpVec1.duplicateFrom(refSol);

  PetscVector tmpVec2;
  tmpVec2.duplicateFrom(refSol);

  waxpy(tmpVec1,-1.0,refSol,solution);

  mult(m_Matrix[m_idG],tmpVec1,tmpVec2);

  dot(tmpVec1,tmpVec2,&error);

  tmpVec1.destroy();
  tmpVec2.destroy();
}

void EigenProblemALP::checkBasis(int size) {

  if (size == 0)
    size = m_dimRomBasis;

  if (!m_solutionInitialized) {
    initializeSolution();
  }

  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);
  PetscVector tmpSol;
  tmpSol.duplicateFrom(m_U_0);

  double* tmpSolToSave = nullptr;
  tmpSolToSave = new double[m_numDof];

  // Check convergence on m_U_0
  double convergence[size];
  for (int iBasis=0; iBasis < size; iBasis++) {
    double tmpVec[iBasis+1];
    for (int jBasis=0; jBasis<iBasis+1; jBasis++) {
      tmpVec[jBasis] = m_beta[jBasis];
    }

    projectOnDof(tmpVec,tmpSol,iBasis+1);

    checkSolution(tmpSol,m_U_0,convergence[iBasis]);

    fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
    if (m_problem == 0) {
      writeEnsightVector(tmpSolToSave, iBasis, "uConv");
    }
    if ( (m_problem == 1) || (m_problem == 2) ) {
      writeEnsightVector(tmpSolToSave, iBasis, "potTransMembConv");
    }

    tmpSol.axpy(-1.,m_U_0);
    tmpSol.abs();
    double maxU0;
    m_U_0.max(&maxU0);
    double minU0;
    m_U_0.min(&minU0);
    double deltaU0 = maxU0-minU0;
    tmpSol.scale(1./deltaU0);
    fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
    if (m_problem == 0) {
      writeEnsightVector(tmpSolToSave, iBasis, "uError");
    }
    if ( (m_problem == 1) || (m_problem == 2) ) {
      writeEnsightVector(tmpSolToSave, iBasis, "potTransMembError");
    }

    PetscPrintf(PETSC_COMM_WORLD, "convergence[%d] = %e \n", iBasis, convergence[iBasis]);

  }
  std::string fileName = FelisceParam::instance().resultDir + "/convergence";
  viewALP(convergence,size,fileName);

  // Check convergence on m_W_0
  if ( (m_problem == 1) || (m_problem == 2) ) {
    double convergenceW[size];
    for (int iBasis=0; iBasis < size; iBasis++) {
      double tmpVec[iBasis+1];
      for (int jBasis=0; jBasis<iBasis+1; jBasis++) {
        tmpVec[jBasis] = m_mu[jBasis];
      }

      projectOnDof(tmpVec,tmpSol,iBasis+1);

      checkSolution(tmpSol,m_W_0,convergenceW[iBasis]);

      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iBasis, "ionicVarConv");

      tmpSol.axpy(-1.,m_W_0);
      tmpSol.abs();
      double maxU0;
      m_W_0.max(&maxU0);
      double minU0;
      m_W_0.min(&minU0);
      double deltaU0 = maxU0-minU0;
      tmpSol.scale(1./deltaU0);
      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iBasis, "ionicVarError");

      PetscPrintf(PETSC_COMM_WORLD, "convergenceW[%d] = %e \n", iBasis, convergenceW[iBasis]);
    }
    fileName = FelisceParam::instance().resultDir + "/convergenceW";
    viewALP(convergenceW,size,fileName);
  }

  // Check convergence on m_Ue_0
  if (m_problem == 2) {
    double convergenceUe[size];
    for (int iBasis=0; iBasis < size; iBasis++) {
      double tmpVec[iBasis+1];
      for (int jBasis=0; jBasis<iBasis+1; jBasis++) {
        tmpVec[jBasis] = m_xi[jBasis];
      }

      projectOnDof(tmpVec,tmpSol,iBasis+1);

      checkSolution(tmpSol,m_Ue_0,convergenceUe[iBasis]);

      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iBasis, "potExtraCellConv");

      tmpSol.axpy(-1.,m_Ue_0);
      tmpSol.abs();
      double maxU0;
      m_Ue_0.max(&maxU0);
      double minU0;
      m_Ue_0.min(&minU0);
      double deltaU0 = maxU0-minU0;
      tmpSol.scale(1./deltaU0);
      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iBasis, "potExtraCellError");

      PetscPrintf(PETSC_COMM_WORLD, "convergenceUe[%d] = %e \n", iBasis, convergenceUe[iBasis]);
    }
    fileName = FelisceParam::instance().resultDir + "/convergenceUe";
    viewALP(convergenceUe,size,fileName);
  }

  std::vector<std::string> varNames;
  if (m_problem == 0) {
    varNames.resize(2);
    varNames[0] = "uConv";
    varNames[1] = "uError";
  } else if (m_problem == 1) {
    if (FelisceParam::instance().printIonicVar) {
      varNames.resize(4);
      varNames[0] = "potTransMembConv";
      varNames[1] = "ionicVarConv";
      varNames[2] = "potTransMembError";
      varNames[3] = "ionicVarError";
    } else {
      varNames.resize(2);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potTransMembError";
    }
  } else if (m_problem == 2) {
    if (FelisceParam::instance().printIonicVar) {
      varNames.resize(6);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potExtraCellConv";
      varNames[2] = "ionicVarConv";
      varNames[3] = "potTransMembError";
      varNames[4] = "potExtraCellError";
      varNames[5] = "ionicVarError";
    } else {
      varNames.resize(4);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potExtraCellConv";
      varNames[3] = "potTransMembError";
      varNames[4] = "potExtraCellError";
    }
  }
  if (rankProc == 0)
    writeEnsightCase(size, 1.,varNames);



  delete [] tmpSolToSave;
  tmpSol.destroy();
}

//Update functions
void EigenProblemALP::updateBasis() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateBasis()\n");

  std::vector<PetscVector> newPhi;
  newPhi.resize(m_dimRomBasis);
  for (int i=0; i<m_dimRomBasis; i++) {
    newPhi[i].duplicateFrom(m_basis[0]);
    newPhi[i].set(0.);
    for (int j=0; j<m_dimRomBasis; j++) {
      newPhi[i].axpy(- m_matrixM[j][i], m_basis[j]);
    }
  }

  std::vector<PetscVector> rPhi;

  if(m_useImprovedRec==true) {
    rPhi.resize(m_dimRomBasis);

    int psiSize = m_dimOrthComp*m_dimOrthComp;
    double* Psi1;
    Psi1 = new double [psiSize];
    double* Psi2;
    Psi2 = new double [psiSize];
    double* Psi;
    Psi = new double [psiSize];

    double valP1=0.0;
    double valP2=0.0;
    PetscVector phiK;
    phiK.duplicateFrom(m_basis[0]);
    for(int i=0; i<m_dimOrthComp; i++) {
      mult(m_Matrix[m_idK],m_orthComp[i],phiK);
      for(int j=0; j<=i; j++) {
        dot(m_orthComp[j],phiK,&valP1);
        Psi1[i*(m_dimOrthComp)+j] = valP1;
        Psi1[j*(m_dimOrthComp)+i] = valP1;
      }
    }
    phiK.destroy();

    PetscVector tmpU;
    tmpU.duplicateFrom(m_basis[0]);
    tmpU.set(0.);
    for(int i=0; i<m_dimRomBasis; i++) {
      tmpU.axpy(m_beta[i],m_basis[i]);
    }
    PetscVector psiG;
    psiG.duplicateFrom(m_basis[0]);
    mult(m_Matrix[m_idG],tmpU,psiG);



    for(int i=0; i<m_dimOrthComp; i++) {
      for(int j=0; j<=i; j++) {
        PetscVector zetaP;
        zetaP.duplicateFrom(m_basis[0]);
        pointwiseMult(zetaP,m_orthComp[i],m_orthComp[j]);
        dot(zetaP,psiG,&valP2);
        Psi2[i*(m_dimOrthComp)+j] = valP2;
        Psi2[j*(m_dimOrthComp)+i] = valP2;

        zetaP.destroy();
      }
    }
    psiG.destroy();
    tmpU.destroy();

    PetscVector tmpF;
    tmpF.duplicateFrom(m_basis[0]);
    tmpF.set(0.);
    for(int i=0; i<m_dimRomBasis; i++) {
      tmpF.axpy(m_gamma[i],m_basis[i]);
    }

    double tol = 1.0e-9;
    int maxIt = 2*m_dimOrthComp;
    double* tmpRhs;
    tmpRhs = new double[m_dimOrthComp];
    double* alpha;
    alpha = new double[m_dimOrthComp];


    for(int i=0; i<m_dimRomBasis; i++) {
      rPhi[i].duplicateFrom(m_basis[0]);
      rPhi[i].set(0.);

      PetscVector FPhiI;
      FPhiI.duplicateFrom(m_basis[0]);
      pointwiseMult(FPhiI,m_basis[i],tmpF);

      PetscVector FPhiG;
      FPhiG.duplicateFrom(m_basis[0]);
      mult(m_Matrix[m_idG],FPhiI,FPhiG);

      for (int l=0; l<m_dimOrthComp; l++) {
        double rhsVal=0.0;
        dot(m_orthComp[l],FPhiG,&rhsVal);
        tmpRhs[l] = m_coefChi * rhsVal;
      }

      for (int l=0; l<m_dimOrthComp; l++) {
        for(int m=0; m<m_dimOrthComp; m++) {
          Psi[l*m_dimOrthComp+m] = Psi1[l*m_dimOrthComp+m] - m_coefChi*Psi2[l*m_dimOrthComp+m];
          if(m==l) {
            Psi[l*m_dimOrthComp + l] = Psi[l*m_dimOrthComp + l] - m_eigenValue[i];
          }
        }
      }
      solveOrthComp(Psi, tmpRhs, alpha, tol, maxIt);

      for(int l=0; l<m_dimOrthComp; l++) {
        rPhi[i].axpy(alpha[l],m_orthComp[l]);
      }

      FPhiI.destroy();
      FPhiG.destroy();
    }
    tmpF.destroy();

    delete [] Psi1;
    delete [] Psi2;
    delete [] Psi;
    delete [] tmpRhs;
    delete [] alpha;


  }


  double dt = FelisceParam::instance().timeStep;
  for (int i=0; i<m_dimRomBasis; i++) {
    m_basis[i].axpy(dt,newPhi[i]);
    newPhi[i].destroy();

    if(m_useImprovedRec==true) {
      m_basis[i].axpy(dt,rPhi[i]);
      rPhi[i].destroy();
    }
  }

  MGS();
  if(m_useImprovedRec==true) {
    MGS(m_orthComp);
  }

  if (FelisceParam::instance().writeBasisEvolution)
    writeMode(m_fstransient->iteration);

}


void EigenProblemALP::setPotential()
{
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::setPotential()\n");

  int Nm = FelisceParam::instance().numApproxMode;
  int nCutOff = FelisceParam::instance().nCutOff;

  m_initPot.duplicateFrom(m_basis[0]);
  m_initPot.copyFrom(m_U_0);

  int sizePb = -1;
  std::vector<PetscVector> lagMult;
  std::vector<PetscVector> errorRep;
  if (m_problem == 0)
  { // FKPP
    sizePb = 1;
  }
  else if (m_problem == 1)
  { // Monodomain
    sizePb = 2;
  }
  else if (m_problem == 2)
  { // Bidomain
    sizePb = 3;
  }
  else
    FEL_ERROR("Problem not found.");
  lagMult.resize(sizePb);
  errorRep.resize(sizePb);

  for (int i=0; i<sizePb; i++) {
    lagMult[i].duplicateFrom(m_basis[0]);
    errorRep[i].duplicateFrom(m_basis[0]);
  }

  PetscVector tmpVec;
  tmpVec.duplicateFrom(m_basis[0]);

  double errorU = 1.;
  double errorW = 1.;
  double errorUe = 1.;

  std::vector<double*> coeffBeta;
  coeffBeta.resize(sizePb);

  if (m_beta == nullptr)
    m_beta = new double[Nm];
  projectOnBasis(m_U_0,m_beta,true,Nm);
  m_solutionInitialized = true;
  coeffBeta[0] = m_beta;

  errorRep[0].copyFrom(m_U_0);
  for (int i=0; i<Nm; i++) {
    double coef = -m_beta[i];
    errorRep[0].axpy(coef,m_basis[i]);
  }
  mult(m_Matrix[m_idG],errorRep[0],tmpVec);
  dot(errorRep[0],tmpVec,&errorU);
//    errorU = std::sqrt(errorU)/Nm;

  if (m_problem > 0) {
    if (m_mu == nullptr)
      m_mu = new double[Nm];
    projectOnBasis(m_W_0,m_mu,true,Nm);
    coeffBeta[1] = m_mu;

    errorRep[1].copyFrom(m_W_0);
    for (int i=0; i<Nm; i++) {
      double coef = -m_mu[i];
      errorRep[1].axpy(coef,m_basis[i]);
    }
    mult(m_Matrix[m_idG],errorRep[1],tmpVec);
    dot(errorRep[1],tmpVec,&errorW);
//      errorW = std::sqrt(errorW)/Nm;
  }
  if (m_problem == 2) {
    if (m_xi == nullptr)
      m_xi = new double[Nm];
    projectOnBasis(m_Ue_0,m_xi,true,Nm);
    coeffBeta[2] = m_xi;

    errorRep[2].copyFrom(m_Ue_0);
    for (int i=0; i<Nm; i++) {
      double coef = -m_xi[i];
      errorRep[2].axpy(coef,m_basis[i]);
    }
    mult(m_Matrix[m_idG],errorRep[2],tmpVec);
    dot(errorRep[2],tmpVec,&errorUe);
//      errorUe = std::sqrt(errorUe)/Nm;
  }

  for (int i=0; i<sizePb; i++) {
    lagMult[i].copyFrom(errorRep[i]);
    lagMult[i].scale(-1.);
  }

  PetscVector dLdU;
  dLdU.duplicateFrom(m_basis[0]);

  PetscVector phi_ij;
  phi_ij.duplicateFrom(m_basis[0]);

  PetscVector dUphi;
  dUphi.duplicateFrom(m_basis[0]);

  PetscVector dU;
  dU.duplicateFrom(m_basis[0]);

  PetscVector dUphiTG;
  dUphiTG.duplicateFrom(m_basis[0]);
  std::vector<PetscVector> lKeps;
  lKeps.resize(sizePb);
  for (int i=0; i<sizePb; i++) {
    lKeps[i].duplicateFrom(m_basis[0]);
  }

  int numIter = 0;
  double step = 0.25; // 1.e-1;
  std::array<double, 3> k;
  k[0] = 50.;
  k[1] = 10.;
  k[2] = 10.;

  double** q = new double* [sizePb];
  for (int iPb=0; iPb<sizePb; iPb++) {
    q[iPb] = new double[nCutOff];
  }
  double** theta = new double* [nCutOff];
  for (int i=0; i<nCutOff; i++) {
    theta[i] = new double[nCutOff];
  }

  PetscPrintf(PETSC_COMM_WORLD,"numIter = %i \n",numIter);
  PetscPrintf(PETSC_COMM_WORLD,"error = %e, %e, %e \n",errorU,errorW,errorUe);

  while ( (errorU > 1.e-08) && (errorW > 1.e-08) && (errorUe > 1.e-08) && (numIter < 5000) )
  {
    // q[iPb]_j = <lagMult[iPb]+k*eps[iPb], phi_j>
    for (int iPb=0; iPb<sizePb; iPb++) {
      lKeps[iPb].copyFrom(lagMult[iPb]);
      lKeps[iPb].axpy(k[iPb],errorRep[iPb]);
      projectOnBasis(lKeps[iPb], q[iPb], true, nCutOff);
    }

    // dL/dU = (U - u0)
    dLdU.copyFrom(m_initPot);
    dLdU.axpy(-1.,m_U_0);
    double reg = 1.;
    dLdU.scale(reg);
    // dL/dU -= \chi * \sum \sum ( sum_pb(beta_i q_j) / (lambda_j - lambda_i) phi_i phi_j
    for (int i=0; i<Nm; i++) {
      for (int j=0; j<nCutOff; j++) {
        if ( std::fabs(m_eigenValue[j] - m_eigenValue[i]) > 1.e-1 ) {
          pointwiseMult(phi_ij, m_basis[i], m_basis[j]);
          double coef = 0.;
          for (int iPb=0; iPb<sizePb; iPb++) {
            coef += coeffBeta[iPb][i]*q[iPb][j];
          }
          coef = - m_coefChi * coef/(m_eigenValue[j] - m_eigenValue[i]);
          dLdU.axpy(coef,phi_ij);
        }
      }
    }

    // dU = -s dL/dU
    dU.set(0.);
    dU.axpy(-step,dLdU);

    // theta_ij = <dU phi_i, phi_j>
    for (int i=0; i<nCutOff; i++) {
      pointwiseMult(dUphi, dU, m_basis[i]);
      mult(m_Matrix[m_idG], dUphi, dUphiTG);
      for (int j=0; j<=i; j++) {
        dot(dUphiTG, m_basis[j], &theta[i][j]);
        if (i != j)
          theta[j][i] = theta[i][j];
      }
    }

    // Update phi
    for (int i=0; i<nCutOff; i++) {
      for (int j=0; j<nCutOff; j++) {
        if ( std::fabs(m_eigenValue[j] - m_eigenValue[i]) > 1.e-1 ) {
          double coef = m_coefChi * theta[i][j]/(m_eigenValue[j] - m_eigenValue[i]);
          m_basis[i].axpy(coef, m_basis[j]);
        }
      }
    }
    MGS(nCutOff);

    // Update lambda
    for (int i=0; i<nCutOff; i++) {
      m_eigenValue[i] -= m_coefChi * theta[i][i];
    }

    // Update beta
    projectOnBasis(m_U_0,m_beta,true,Nm);
    if (m_problem > 0) {
      // Update mu
      projectOnBasis(m_W_0,m_mu,true,Nm);
    }
    if (m_problem == 2) {
      // Update xi
      projectOnBasis(m_Ue_0,m_xi,true,Nm);
    }

    // Compute error
    errorRep[0].copyFrom(m_U_0);
    for (int i=0; i<Nm; i++) {
      errorRep[0].axpy(-1.*m_beta[i],m_basis[i]);
    }
    mult(m_Matrix[m_idG],errorRep[0],tmpVec);
    dot(errorRep[0],tmpVec,&errorU);
//      errorU = std::sqrt(errorU)/Nm;
    if (m_problem > 0) {
      errorRep[1].copyFrom(m_W_0);
      for (int i=0; i<Nm; i++) {
        errorRep[1].axpy(-1.*m_mu[i],m_basis[i]);
      }
      mult(m_Matrix[m_idG],errorRep[1],tmpVec);
      dot(errorRep[1],tmpVec,&errorW);
//        errorW = std::sqrt(errorW)/Nm;
    }
    if (m_problem == 2) {
      errorRep[2].copyFrom(m_Ue_0);
      for (int i=0; i<Nm; i++) {
        errorRep[2].axpy(-1.*m_xi[i],m_basis[i]);
      }
      mult(m_Matrix[m_idG],errorRep[2],tmpVec);
      dot(errorRep[2],tmpVec,&errorUe);
//        errorUe = std::sqrt(errorUe)/Nm;
    }

    // Update Lagrangian Multiplicator
    for (int iPb=0; iPb<sizePb; iPb++) {
      lagMult[iPb].axpy(step, errorRep[iPb]);
    }

    // Update initial potential
    m_initPot.axpy(1.,dU);

    numIter++;

    PetscPrintf(PETSC_COMM_WORLD,"numIter = %i \n",numIter);
    PetscPrintf(PETSC_COMM_WORLD,"error = %e, %e, %e \n",errorU,errorW,errorUe);
  }

  for (int iPb=0; iPb<sizePb; iPb++) {
    lagMult[iPb].destroy();
    errorRep[iPb].destroy();
    lKeps[iPb].destroy();
  }
  dLdU.destroy();
  phi_ij.destroy();
  dUphi.destroy();
  dU.destroy();
  dUphiTG.destroy();
  tmpVec.destroy();

  for (int i=0; i<sizePb; i++) {
    delete [] q[i];
  }
  delete [] q;
  for (int i=0; i<nCutOff; i++) {
    delete [] theta[i];
  }
  delete [] theta;

  // Write initial potential in Ensight file
  double* tmpSolToSave = nullptr;
  tmpSolToSave = new double[m_numDof];
  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);
  fromVecToDoubleStar(tmpSolToSave, m_initPot, rankProc, 1);
  writeEnsightVector(tmpSolToSave, 0, "initialPotential");
  delete [] tmpSolToSave;

  std::string fileName = FelisceParam::instance().resultDir + "/eigenValue";
  viewALP(m_eigenValue,m_dimRomBasis,fileName);

  checkBasis(Nm);
}

// Modified Gram-Schmidt (see Golub & Van Loan)
void EigenProblemALP::MGS(int size)
{
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::MGS(int)\n");

  if (size == 0)
    size = m_dimRomBasis;

  for(int iBasis=0; iBasis<size; iBasis++)
  {
    PetscVector tmpVec;
    tmpVec.duplicateFrom(m_basis[0]);
    mult(m_Matrix[m_idG],m_basis[iBasis],tmpVec);
    double rii = 0.0;
    dot(tmpVec,m_basis[iBasis],&rii);
    rii = 1.0/std::sqrt(rii);
    m_basis[iBasis].scale(rii);

    for(int j=iBasis+1; j<size; j++) {
      double rij = 0.0;
      dot(tmpVec,m_basis[j],&rij);
      rij = -1.0 * rij;
      m_basis[j].axpy(rij,m_basis[iBasis]);
    }
    tmpVec.destroy();
  }
}


// Modified Gram-Schmidt (for an extra-space, orthonormal to the basis too)
// 1 - orthonormalize w.r.t. basis, 2 - orthonormalize extraspace;
void EigenProblemALP::MGS(std::vector<PetscVector>& extraSpace) {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::MGS(std::vector<Vec>)\n");

  for(int j=0; j<m_dimOrthComp; j++) {
    PetscVector tmpVec;
    tmpVec.duplicateFrom(m_basis[0]);
    mult(m_Matrix[m_idG],extraSpace[j],tmpVec);
    double* Sij;
    Sij = new double[m_dimRomBasis];
    for(int i =0; i<m_dimRomBasis; i++) {
      double sij = 0.0;
      dot(tmpVec,m_basis[i],&sij);
      Sij[i] = -1.0*sij;
    }
    double* Rjh;
    Rjh = new double[j];
    for(int h=0; h<j; h++) {
      double rjh = 0.0;
      dot(tmpVec,extraSpace[h],&rjh);
      Rjh[h] = -1.0*rjh;
    }

    for(int i=0; i<m_dimRomBasis; i++) {
      double wheight = Sij[i];
      extraSpace[j].axpy(wheight,m_basis[i]);
    }
    for(int h=0; h<j; h++) {
      double wheight = Rjh[h];
      extraSpace[j].axpy(wheight,extraSpace[h]);
    }

    PetscVector tmpNorm;
    tmpNorm.duplicateFrom(m_basis[0]);
    mult(m_Matrix[m_idG],extraSpace[j],tmpNorm);
    double rjj = 0.0;
    dot(tmpNorm,extraSpace[j],&rjj);
    rjj = 1.0/std::sqrt(rjj);
    extraSpace[j].scale(rjj);

    tmpVec.destroy();
    tmpNorm.destroy();
    delete [] Rjh;
    delete [] Sij;
  }
}

void EigenProblemALP::updateTensor() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateTensor()\n");

  double dt = FelisceParam::instance().timeStep;

  switch (m_problem) {
  case 0: {
    double deltaT[m_size3];
    // T^{n+1} = T^n + dt * [T^n, M^n]^{(3)}
    for (int i=0; i<m_dimRomBasis; i++) {
      for (int j=0; j<i+1; j++) {
        for (int k=0; k<j+1; k++) {
          deltaT[thirdOrderGlobalIndex(i,j,k)] = 0.;
          for (int l=0; l<m_dimRomBasis; l++) {
            // delta T_ijk = sum_l (M_il * T_ljk + M_jl * T_ilk + M_kl * T_ijl )
            deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[i][l] * m_tensorT[thirdOrderGlobalIndex(l,j,k)];
            deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[j][l] * m_tensorT[thirdOrderGlobalIndex(i,l,k)];
            deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[k][l] * m_tensorT[thirdOrderGlobalIndex(i,j,l)];
          }
        }
      }
    }
    for (int idT=0; idT<m_size3; idT++)
      m_tensorT[idT] += dt * deltaT[idT];
    break;
  }
  case 1:
  case 2: {
    switch (m_tensorOrder) {
    case 3: {
      double deltaB[m_size2];
      // B^{n+1} = B^n + dt * [B^n, M^n]^{(2)}
      double deltaE[m_size2];
      // E^{n+1} = E^n + dt * [E^n, M^n]^{(2)}
      double deltaEs[m_size2];
      // Es^{n+1} = Es^n + dt * [Es^n, M^n]^{(2)}
      double deltaQ[m_size2+m_dimRomBasis];
      // Q^{n+1} = Q^n + dt * [Q^n, M^n]^{(2)}
      double deltaT[m_size3];
      // T^{n+1} = T^n + dt * [T^n, M^n]^{(3)}
      double** deltaU = new double*[m_dimRomBasis];
      for (int i=0; i<m_dimRomBasis; i++)
        deltaU[i] = new double[m_size2];
      // U^{n+1} = U^n + dt * [U^n, M^n]^{(3)}

      for (int i=0; i<m_dimRomBasis; i++) {
        for (int j=0; j<i+1; j++) {
          deltaB[secondOrderGlobalIndex(i,j)] = 0.;
          deltaE[secondOrderGlobalIndex(i,j)] = 0.;
          deltaEs[secondOrderGlobalIndex(i,j)] = 0.;
          if (m_problem == 2) // bidomain
            deltaQ[secondOrderGlobalIndex(i,j)] = 0.;

          for (int l=0; l<m_dimRomBasis; l++) {
            // delta B_ij = sum_l ( M_il * B_lj + M_jl * B_il )
            deltaB[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorB[secondOrderGlobalIndex(l,j)];
            deltaB[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorB[secondOrderGlobalIndex(i,l)];
            // delta E_ij = sum_l ( M_il * E_lj + M_jl * E_il )
            deltaE[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorE[secondOrderGlobalIndex(l,j)];
            deltaE[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorE[secondOrderGlobalIndex(i,l)];
            // delta Es_ij = sum_l ( M_il * Es_lj + M_jl * Es_il )
            deltaEs[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorEs[secondOrderGlobalIndex(l,j)];
            deltaEs[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorEs[secondOrderGlobalIndex(i,l)];
            if (m_problem == 2) { // bidomain
              // delta Q_ij = sum_l ( M_il * Q_lj + M_jl * Q_il )
              deltaQ[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorQ[secondOrderGlobalIndex(l,j)];
              deltaQ[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorQ[secondOrderGlobalIndex(i,l)];
            }

          }
          for (int k=0; k<j+1; k++) {
            deltaT[thirdOrderGlobalIndex(i,j,k)] = 0.;
            for (int l=0; l<m_dimRomBasis; l++) {
              // delta T_ijk = sum_l (M_il * T_ljk + M_jl * T_ilk + M_kl * T_ijl )
              deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[i][l] * m_tensorT[thirdOrderGlobalIndex(l,j,k)];
              deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[j][l] * m_tensorT[thirdOrderGlobalIndex(i,l,k)];
              deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[k][l] * m_tensorT[thirdOrderGlobalIndex(i,j,l)];
            }
          }
        }
        for (int j=0; j<m_dimRomBasis; j++) {
          for (int k=0; k<j+1; k++) {
            deltaU[i][secondOrderGlobalIndex(j,k)] = 0.;
            for (int l=0; l<m_dimRomBasis; l++) {
              // delta U_ijk = sum_l (M_il * U_ljk + M_jl * U_ilk + M_kl * U_ijl )
              deltaU[i][secondOrderGlobalIndex(j,k)] += m_matrixM[i][l] * m_tensorU[l][secondOrderGlobalIndex(j,k)];
              deltaU[i][secondOrderGlobalIndex(j,k)] += m_matrixM[j][l] * m_tensorU[i][secondOrderGlobalIndex(l,k)];
              deltaU[i][secondOrderGlobalIndex(j,k)] += m_matrixM[k][l] * m_tensorU[i][secondOrderGlobalIndex(j,l)];
            }
          }
        }
        if (m_problem == 2) {
          deltaQ[secondOrderGlobalIndex(m_dimRomBasis,i)] = 0.;
          for (int l=0; l<m_dimRomBasis; l++) {
            // delta Q_Nj = sum_l ( M_jl * Q_lj )
            deltaQ[secondOrderGlobalIndex(m_dimRomBasis,i)] += m_matrixM[i][l] * m_tensorQ[secondOrderGlobalIndex(m_dimRomBasis,l)];
          }
        }
      }
      for (int i=0; i<m_size2; i++) {
        m_tensorB[i] += dt * deltaB[i];
        m_tensorE[i] += dt * deltaE[i];
        m_tensorEs[i] += dt * deltaEs[i];
      }
      if (m_problem == 2) {
        for (int i=0; i<m_size2+m_dimRomBasis; i++)
          m_tensorQ[i] += dt * deltaQ[i];
      }
      for (int i=0; i<m_size3; i++)
        m_tensorT[i] += dt * deltaT[i];
      for (int i=0; i<m_dimRomBasis; i++) {
        for (int j=0; j<m_size2; j++)
          m_tensorU[i][j] += dt * deltaU[i][j];
      }
      for (int i=0; i<m_dimRomBasis; i++)
        delete [] deltaU[i];
      delete [] deltaU;
      break;
    }
    case 4: {
      double deltaB[m_size2];
      // B^{n+1} = B^n + dt * [B^n, M^n]^{(2)}
      double deltaE[m_size2];
      // E^{n+1} = E^n + dt * [E^n, M^n]^{(2)}
      double deltaQ[m_size2+m_dimRomBasis];
      // Q^{n+1} = Q^n + dt * [Q^n, M^n]^{(2)}
      double deltaT[m_size3];
      // T^{n+1} = T^n + dt * [T^n, M^n]^{(3)}
      double deltaTs[m_size3];
      // Ts^{n+1} = Ts^n + dt * [Ts^n, M^n]^{(3)}
      double deltaY[m_size4];
      // Y^{n+1} = Y^n + dt * [Y^n, M^n]^{(4)}
      for (int i=0; i<m_dimRomBasis; i++) {
        for (int j=0; j<i+1; j++) {
          deltaB[secondOrderGlobalIndex(i,j)] = 0.;
          if (m_problem == 2) { // bidomain
            deltaE[secondOrderGlobalIndex(i,j)] = 0.;
            deltaQ[secondOrderGlobalIndex(i,j)] = 0.;
          }
          for (int l=0; l<m_dimRomBasis; l++) {
            // delta B_ijk = sum_l (M_il * B_lj + M_jl * B_il )
            deltaB[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorB[secondOrderGlobalIndex(l,j)];
            deltaB[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorB[secondOrderGlobalIndex(i,l)];
            if (m_problem == 2) { // bidomain
              // delta E_ijk = sum_l (M_il * E_lj + M_jl * E_il )
              deltaE[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorE[secondOrderGlobalIndex(l,j)];
              deltaE[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorE[secondOrderGlobalIndex(i,l)];
              // delta Q_ijk = sum_l (M_il * Q_lj + M_jl * Q_il )
              deltaQ[secondOrderGlobalIndex(i,j)] += m_matrixM[i][l] * m_tensorQ[secondOrderGlobalIndex(l,j)];
              deltaQ[secondOrderGlobalIndex(i,j)] += m_matrixM[j][l] * m_tensorQ[secondOrderGlobalIndex(i,l)];
            }
          }
          for (int k=0; k<j+1; k++) {
            deltaT[thirdOrderGlobalIndex(i,j,k)] = 0.;
            deltaTs[thirdOrderGlobalIndex(i,j,k)] = 0.;
            for (int l=0; l<m_dimRomBasis; l++) {
              // delta T_ijk = sum_l (M_il * T_ljk + M_jl * T_ilk + M_kl * T_ijl )
              deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[i][l] * m_tensorT[thirdOrderGlobalIndex(l,j,k)];
              deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[j][l] * m_tensorT[thirdOrderGlobalIndex(i,l,k)];
              deltaT[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[k][l] * m_tensorT[thirdOrderGlobalIndex(i,j,l)];

              deltaTs[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[i][l] * m_tensorTs[thirdOrderGlobalIndex(l,j,k)];
              deltaTs[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[j][l] * m_tensorTs[thirdOrderGlobalIndex(i,l,k)];
              deltaTs[thirdOrderGlobalIndex(i,j,k)] += m_matrixM[k][l] * m_tensorTs[thirdOrderGlobalIndex(i,j,l)];
            }
            for (int h=0; h<k+1; h++) {
              deltaY[fourthOrderGlobalIndex(i,j,k,h)] = 0.;
              for (int l=0; l<m_dimRomBasis; l++) {
                // delta Y_ijkh = sum_l ( M_il * Y_ljkh + M_jl * Y_ilkh + M_kl * Y_ijlh + M_hl * Y_ijkl )
                deltaY[fourthOrderGlobalIndex(i,j,k,h)] += m_matrixM[i][l] * m_tensorY[fourthOrderGlobalIndex(l,j,k,h)];
                deltaY[fourthOrderGlobalIndex(i,j,k,h)] += m_matrixM[j][l] * m_tensorY[fourthOrderGlobalIndex(i,l,k,h)];
                deltaY[fourthOrderGlobalIndex(i,j,k,h)] += m_matrixM[k][l] * m_tensorY[fourthOrderGlobalIndex(i,j,l,h)];
                deltaY[fourthOrderGlobalIndex(i,j,k,h)] += m_matrixM[h][l] * m_tensorY[fourthOrderGlobalIndex(i,j,k,l)];
              }
            }
          }
        }
        if (m_problem == 2) {
          deltaQ[secondOrderGlobalIndex(m_dimRomBasis,i)] = 0.;
          for (int l=0; l<m_dimRomBasis; l++) {
            // delta Q_Nj = sum_l ( M_jl * Q_lj )
            deltaQ[secondOrderGlobalIndex(m_dimRomBasis,i)] += m_matrixM[i][l] * m_tensorQ[secondOrderGlobalIndex(m_dimRomBasis,l)];
          }
        }
      }

      for (int id=0; id<m_size2; id++)
        m_tensorB[id] += dt * deltaB[id];
      if (m_problem == 2) {
        for (int id=0; id<m_size2; id++)
          m_tensorE[id] += dt * deltaE[id];
        for (int id=0; id<m_size2+m_dimRomBasis; id++)
          m_tensorQ[id] += dt * deltaQ[id];
      }
      for (int id=0; id<m_size3; id++) {
        m_tensorT[id] += dt * deltaT[id];
        m_tensorTs[id] += dt * deltaTs[id];
      }
      for (int id=0; id<m_size4; id++)
        m_tensorY[id] += dt * deltaY[id];
      break;
    }
    default:
      break;
    }
    break;
  }
  default:
    FEL_ERROR("Model not defined for ALP solver.");
    break;
  }
}

void EigenProblemALP::updateBeta() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateBeta()\n");

  double dt = FelisceParam::instance().timeStep;
  double deltaBeta[m_dimRomBasis];
  // beta^{n+1} = beta^n - dt * M^n * beta^n + dt * gamma^n
  for (int i=0; i<m_dimRomBasis; i++) {
    deltaBeta[i] = m_gamma[i];

    for (int h=0; h<m_dimRomBasis; h++) {
      deltaBeta[i] -= m_beta[h] * m_matrixM[h][i];
    }
  }
  for (int i=0; i<m_dimRomBasis; i++) {
    m_beta[i] += dt * deltaBeta[i];
  }

  if ( (m_problem == 1) || (m_problem == 2) ) {
    double deltaMu[m_dimRomBasis];
    // mu^{n+1} = mu^n - dt * M^n * mu^n + dt * eta^n
    for (int i=0; i<m_dimRomBasis; i++) {
      deltaMu[i] = m_eta[i];

      for (int h=0; h<m_dimRomBasis; h++) {
        deltaMu[i] -= m_mu[h] * m_matrixM[h][i];
      }
    }
    for (int i=0; i<m_dimRomBasis; i++) {
      m_mu[i] += dt * deltaMu[i];
    }
  }

  if (m_problem == 2) {
    double* newXi = new double[m_dimRomBasis+1];
    double* rhsXi = new double[m_dimRomBasis+1];
    for (int i=0; i<m_dimRomBasis; i++) {
      newXi[i] = m_xi[i];
      rhsXi[i] = 0.;
      for (int j=0; j<m_dimRomBasis; j++) {
        rhsXi[i] += - m_beta[j]*m_tensorE[secondOrderGlobalIndex(j,i)];
      }
    }
    newXi[m_dimRomBasis] = 0.;
    rhsXi[m_dimRomBasis] = 0.;

    ConjGrad(m_tensorQ, rhsXi, newXi, 1.e-09, 1000, m_dimRomBasis+1);

    for (int i=0; i<m_dimRomBasis; i++) {
      m_xi[i] = newXi[i];
    }
    delete [] newXi;
    delete [] rhsXi;

  }

  if (FelisceParam::instance().hasSource) {
    for (int idS=0; idS<m_numSource; idS++) {
      double* newSource = new double[m_dimRomBasis];
      // source^{n+1} = source^n - dt * M^n * source^n
      for (int i=0; i<m_dimRomBasis; i++) {
        newSource[i] = 0.;

        for (int h=0; h<m_dimRomBasis; h++) {
          newSource[i] += m_source[idS][h] * m_matrixM[i][h];
        }
      }
      for (int i=0; i<m_dimRomBasis; i++) {
        m_source[idS][i] += dt * newSource[i];
      }
      delete [] newSource;
    }
  }

}

void EigenProblemALP::updateBetaMonolithic() {
  if (FelisceParam::verbose() > 10 )
    PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateBetaMonolithic()\n");

  double dt = FelisceParam::instance().timeStep;
  double value;
  double rhs1;
  double rhs2;
  felInt ia;
  m_rhsRom.set(0.);

  // beta^{n+1} = beta^n + dt* gamma^n - dt * M * beta^n
  // mu^{n+1} = mu^n + dt * eta^n - dt * M * mu^n
  // E beta^{n+1} + Q xi^{n+1} = 0
  for (int i=0; i<m_dimRomBasis; i++) {
    rhs1 = m_beta[i] + dt * m_gamma[i];
    rhs2 = m_mu[i] + dt * m_eta[i];

    for (int h=0; h<m_dimRomBasis; h++) {
      rhs1 -= dt * m_beta[h] * m_matrixM[h][i];
      rhs2 -= dt * m_mu[h] * m_matrixM[h][i];
    }

    ia = i;
    m_rhsRom.setValue(ia,rhs1,INSERT_VALUES);

    ia = i+m_dimRomBasis;
    m_rhsRom.setValue(ia,rhs2,INSERT_VALUES);
  }
  m_rhsRom.assembly();

  // MatrixRom = [ I       0      0 ]
  //             [ 0       I      0 ]
  //             [ E       0      Q ]

  felInt ii;
  felInt jj;
  for (int i=0; i<m_dimRomBasis; i++) {
    value = 1.;

    ii = i;
    m_matrixRom.setValues(1,&ii,1,&ii,&value,INSERT_VALUES);

    ii = i+m_dimRomBasis;
    m_matrixRom.setValues(1,&ii,1,&ii,&value,INSERT_VALUES);

    for (int j=0; j<m_dimRomBasis; j++) {
      double valueMatrix = 0.;

      valueMatrix = m_tensorE[secondOrderGlobalIndex(i,j)];
      ii = i+2*m_dimRomBasis;
      jj = j;
      m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);

      valueMatrix = m_tensorQ[secondOrderGlobalIndex(i,j)];
      jj = j+2*m_dimRomBasis;
      m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
    }
  }
  ii = 3*m_dimRomBasis;
  for (int j=0; j<m_dimRomBasis; j++) {
    jj = j+2*m_dimRomBasis;
    double valueMatrix = m_tensorQ[secondOrderGlobalIndex(j,m_dimRomBasis)];
    m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
    m_matrixRom.setValues(1,&jj,1,&ii,&valueMatrix,INSERT_VALUES);
  }
  jj = 3*m_dimRomBasis;
  double valueMatrix = 0.;
  m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);

  m_matrixRom.assembly(MAT_FINAL_ASSEMBLY);

  m_solutionRom.set(0.);

  // Solve
  m_kspRom->setKSPOperator(m_matrixRom, "");
  m_kspRom->solve(m_rhsRom, m_solutionRom, 0);

  for (int i=0; i<m_dimRomBasis; i++) {
    ia = i;
    m_solutionRom.getValues(1,&ia,&value);
    m_beta[i] = value;
    ia = i+m_dimRomBasis;
    m_solutionRom.getValues(1,&ia,&value);
    m_mu[i] = value;
    ia = i+2*m_dimRomBasis;
    m_solutionRom.getValues(1,&ia,&value);
    m_xi[i] = value;
  }

}

void EigenProblemALP::updateBetaEI() {
  if (FelisceParam::verbose() > 10 )
    PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateBetaEI()\n");

  double dt = FelisceParam::instance().timeStep;
  double value;
  double rhs1;
  double rhs2;
  felInt ia;
  m_rhsRom.set(0.);

  // (I + dt M) beta^{n+1} = beta^n + dt* gamma^n
  // (I + dt M) mu^{n+1} = mu^n + dt * eta^n
  // E beta^{n+1} + Q xi^{n+1} = 0
  for (int i=0; i<m_dimRomBasis; i++) {
    rhs1 = m_beta[i] + dt * m_gamma[i];
    rhs2 = m_mu[i] + dt * m_eta[i];

    ia = i;
    m_rhsRom.setValue(ia,rhs1,INSERT_VALUES);

    ia = i+m_dimRomBasis;
    m_rhsRom.setValue(ia,rhs2,INSERT_VALUES);
  }
  m_rhsRom.assembly();

  // MatrixRom = [ I+dtM      0      0 ]
  //             [   0      I+dtM    0 ]
  //             [   E        0      Q ]

  felInt ii;
  felInt jj;
  for (int i=0; i<m_dimRomBasis; i++) {
    value = 1.;

    ii = i;
    m_matrixRom.setValues(1,&ii,1,&ii,&value,INSERT_VALUES);

    ii = i+m_dimRomBasis;
    m_matrixRom.setValues(1,&ii,1,&ii,&value,INSERT_VALUES);

    for (int j=0; j<m_dimRomBasis; j++) {
      double valueMatrix = 0.;

      valueMatrix = dt * m_matrixM[i][j];
      ii = i;
      jj = j;
      if (ii != jj) {
        m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
      }
      ii = i+m_dimRomBasis;
      jj = j+m_dimRomBasis;
      if (ii != jj) {
        m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
      }

      valueMatrix = dt * m_tensorE[secondOrderGlobalIndex(i,j)];
      ii = i+2*m_dimRomBasis;
      jj = j;
      m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);

      valueMatrix = dt * m_tensorQ[secondOrderGlobalIndex(i,j)];
      jj = j+2*m_dimRomBasis;
      m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
    }
  }
  ii = 3*m_dimRomBasis;
  for (int j=0; j<m_dimRomBasis; j++) {
    jj = j+2*m_dimRomBasis;
    double valueMatrix = dt * m_tensorQ[secondOrderGlobalIndex(j,m_dimRomBasis)];
    m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
    m_matrixRom.setValues(1,&jj,1,&ii,&valueMatrix,INSERT_VALUES);
  }
  jj = 3*m_dimRomBasis;
  double valueMatrix = 0.;
  m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);

  m_matrixRom.assembly(MAT_FINAL_ASSEMBLY);

  m_solutionRom.set(0.);

  // Solve
  m_kspRom->setKSPOperator(m_matrixRom, "");
  m_kspRom->solve(m_rhsRom, m_solutionRom, 0);

  for (int i=0; i<m_dimRomBasis; i++) {
    ia = i;
    m_solutionRom.getValues(1,&ia,&value);
    m_beta[i] = value;
    ia = i+m_dimRomBasis;
    m_solutionRom.getValues(1,&ia,&value);
    m_mu[i] = value;
    ia = i+2*m_dimRomBasis;
    m_solutionRom.getValues(1,&ia,&value);
    m_xi[i] = value;
  }

}

void EigenProblemALP::updateBetaBdf2() {

  if (FelisceParam::verbose() > 10 )
    PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateBetaEI()\n");

  double dt = FelisceParam::instance().timeStep;
  double value;
  double rhs1;
  double rhs2;
  double coefTimeDer[3];
  coefTimeDer[0] = 3./2.;
  coefTimeDer[1] = 2.;
  coefTimeDer[2] = -1./2.;
  felInt ia;
  m_rhsRom.set(0.);

  // (3/2 I + dt M) beta^{n+1} = 2 beta^n - 1/2 beta^{n-1}+ dt* gamma(beta_extrap = 2 beta^n - beta^{n-1})
  // (3/2 I + dt M) mu^{n+1} = 2 mu^n - 1/2 mu^{n-1} + dt * eta(mu_extrap = 2 mu^n - mu^{n-1))
  // E beta^{n+1} + Q xi^{n+1} = 0
  for (int i=0; i<m_dimRomBasis; i++) {
    rhs1 = coefTimeDer[1]*m_beta[i] + coefTimeDer[2]*m_beta_n_1[i] + dt * m_gamma_extrap[i];
    rhs2 = coefTimeDer[1]*m_mu[i] + coefTimeDer[2]*m_mu_n_1[i] + dt * m_eta_extrap[i];

    ia = i;
    m_rhsRom.setValue(ia,rhs1,INSERT_VALUES);

    ia = i+m_dimRomBasis;
    m_rhsRom.setValue(ia,rhs2,INSERT_VALUES);
  }
  m_rhsRom.assembly();

  // MatrixRom = [ 3/2I+dtM      0       0 ]
  //             [    0      3/2I+dtM    0 ]
  //             [    E          0       Q ]

  felInt ii;
  felInt jj;
  for (int i=0; i<m_dimRomBasis; i++) {
    value = coefTimeDer[0];

    ii = i;
    m_matrixRom.setValues(1,&ii,1,&ii,&value,INSERT_VALUES);

    ii = i+m_dimRomBasis;
    m_matrixRom.setValues(1,&ii,1,&ii,&value,INSERT_VALUES);

    for (int j=0; j<m_dimRomBasis; j++) {
      double valueMatrix = 0.;

      valueMatrix = dt * m_matrixM[i][j];
      ii = i;
      jj = j;
      if (ii != jj) {
        m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
      }
      ii = i+m_dimRomBasis;
      jj = j+m_dimRomBasis;
      if (ii != jj) {
        m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
      }

      valueMatrix = dt * m_tensorE[secondOrderGlobalIndex(i,j)];
      ii = i+2*m_dimRomBasis;
      jj = j;
      m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);

      valueMatrix = dt * m_tensorQ[secondOrderGlobalIndex(i,j)];
      jj = j+2*m_dimRomBasis;
      m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
    }
  }
  ii = 3*m_dimRomBasis;
  for (int j=0; j<m_dimRomBasis; j++) {
    jj = j+2*m_dimRomBasis;
    double valueMatrix = dt * m_tensorQ[secondOrderGlobalIndex(j,m_dimRomBasis)];
    m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);
    m_matrixRom.setValues(1,&jj,1,&ii,&valueMatrix,INSERT_VALUES);
  }
  jj = 3*m_dimRomBasis;
  double valueMatrix = 0.;
  m_matrixRom.setValues(1,&ii,1,&jj,&valueMatrix,INSERT_VALUES);

  m_matrixRom.assembly(MAT_FINAL_ASSEMBLY);

  m_solutionRom.set(0.);

  // Solve
  m_kspRom->setKSPOperator(m_matrixRom, "");
  m_kspRom->solve(m_rhsRom, m_solutionRom, 0);

  for (int i=0; i<m_dimRomBasis; i++) {
    m_beta_n_1[i] = m_beta[i];
    m_mu_n_1[i] = m_mu[i];
    m_xi_n_1[i] = m_xi[i];
  }

  for (int i=0; i<m_dimRomBasis; i++) {
    ia = i;
    m_solutionRom.getValues(1,&ia,&value);
    m_beta[i] = value;
    ia = i+m_dimRomBasis;
    m_solutionRom.getValues(1,&ia,&value);
    m_mu[i] = value;
    ia = i+2*m_dimRomBasis;
    m_solutionRom.getValues(1,&ia,&value);
    m_xi[i] = value;
  }

}

void EigenProblemALP::ConjGrad(double* A, double* b, double* x, double tol, int maxIter, int n) {
  double* res = new double[n];
  double* p = new double[n];
  double* Ax = new double[n];

  for (int i=0; i<n; i++) {
    Ax[i] = 0.;
    for (int j=0; j<n; j++) {
      Ax[i] += A[secondOrderGlobalIndex(i,j)]*x[j];
    }
  }
  for (int i=0; i<n; i++) {
    res[i] = b[i] - Ax[i];
    p[i] = res[i];
  }
  double res2 = 0.;
  double newRes2 = 0.;
  for (int i=0; i<n; i++) {
    res2 += res[i]*res[i];
  }

  double* Ap = new double[n];
  double pAp;
  double alpha;
  double beta;
  bool isConv = false;
  int iter = 1;

  if (res2 < tol) {
    isConv = true;
    if (FelisceParam::verbose()) {
      PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",0,res2);
    }
  }

  while (!isConv) {
    for (int i=0; i<n; i++) {
      Ap[i] = 0.;
      for (int j=0; j<n; j++) {
        Ap[i] += A[secondOrderGlobalIndex(i,j)]*p[j];
      }
    }
    pAp = 0.;
    for (int i=0; i<n; i++)
      pAp += p[i]*Ap[i];
    alpha = res2/pAp;

    for (int i=0; i<n; i++) {
      x[i] += alpha*p[i];
      res[i] -= alpha*Ap[i];
      newRes2 += res[i]*res[i];
    }

    if ( (newRes2 < tol) || (iter >= maxIter) ) {
      if (newRes2 < tol) {
        isConv = true;
        if (FelisceParam::verbose()) {
          PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",iter,newRes2);
        }
      } else {
        PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient: %i iterations, residual: %1.10f \n",iter,newRes2);
        FEL_ERROR("Conjugate Gradient did not converge.");
      }
      break;
    } else {
      beta = newRes2/res2;
      res2 = newRes2;
      newRes2 = 0.;
      for (int i=0; i<n; i++) {
        p[i] = res[i] + beta*p[i];
      }
      iter++;
    }
  }

  delete [] res;
  delete [] p;
  delete [] Ap;
  delete [] Ax;

}



void EigenProblemALP::solveOrthComp(double* A, double* b, double* x, double tol, int maxIter) {
  for (int i=0; i<m_dimOrthComp; i++) {
    x[i] = 0.0;
  }

  double* res = new double[m_dimOrthComp];
  double* p = new double[m_dimOrthComp];
  double* Ax = new double[m_dimOrthComp];

  for (int i=0; i<m_dimOrthComp; i++) {
    Ax[i] = 0.;
    for (int j=0; j<m_dimOrthComp; j++) {
      Ax[i] += A[i*(m_dimOrthComp)+j]*x[j];
    }
  }
  for (int i=0; i<m_dimOrthComp; i++) {
    res[i] = b[i] - Ax[i];
    p[i] = res[i];
  }
  double res2 = 0.;
  double newRes2 = 0.;
  for (int i=0; i<m_dimOrthComp; i++) {
    res2 += res[i]*res[i];
  }

  double* Ap = new double[m_dimOrthComp];
  double pAp;
  double alpha;
  double beta;
  bool isConv = false;
  int iter = 1;

  while (!isConv) {
    for (int i=0; i<m_dimOrthComp; i++) {
      Ap[i] = 0.;
      for (int j=0; j<m_dimOrthComp; j++) {
        Ap[i] += A[i*(m_dimOrthComp)+j]*p[j];
      }
    }
    pAp = 0.;
    for (int i=0; i<m_dimOrthComp; i++)
      pAp += p[i]*Ap[i];
    alpha = res2/pAp;

    for (int i=0; i<m_dimOrthComp; i++) {
      x[i] += alpha*p[i];
      res[i] -= alpha*Ap[i];
      newRes2 += res[i]*res[i];
    }

    if ( (newRes2 < tol) || (iter >= maxIter) ) {
      isConv = true;
      if (FelisceParam::verbose() > 10) {
        PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",iter,newRes2);
      }

      break;
    } else {
      beta = newRes2/res2;
      res2 = newRes2;
      newRes2 = 0.;
      for (int i=0; i<m_dimOrthComp; i++) {
        p[i] = res[i] + beta*p[i];
      }
      iter++;
    }
  }

  for (int i=0; i<m_dimOrthComp; i++) {
    Ax[i] = 0.;
    for (int j=0; j<m_dimOrthComp; j++) {
      Ax[i] += A[i*(m_dimOrthComp)+j]*x[j];
    }
  }

  delete [] res;
  delete [] p;
  delete [] Ap;
  delete [] Ax;

}



void EigenProblemALP::updateEigenvalue() {
  if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALP::updateEigenvalue()\n");

  double dt = FelisceParam::instance().timeStep;
  double newLambda[m_dimRomBasis];
  // lambda_i^{n+1} = lambda_i^n - dt * m_coefChi * sum_h ( gamma_h * T_hii )
  for (int i=0; i<m_dimRomBasis; i++) {
    newLambda[i] = 0.;
    for (int h=0; h<m_dimRomBasis; h++) {
      newLambda[i] += m_gamma[h] * m_tensorT[thirdOrderGlobalIndex(h,i,i)];
    }
  }
  for (int i=0; i<m_dimRomBasis; i++) {
    m_eigenValue[i] -= dt * m_coefChi * newLambda[i];
  }
}

// Projection functions from FE space to RO one
void EigenProblemALP::projectOnBasis(PetscVector& vectorDof, double* vectorBasis, bool flagMass, int size)
{
  if (size == 0)
    size = m_dimRomBasis;

  PetscVector tmpVec;
  tmpVec.duplicateFrom(vectorDof);

  if (flagMass) {
    for (int iBasis=0; iBasis<size; iBasis++) {
      mult(m_Matrix[m_idG], m_basis[iBasis],tmpVec);

      dot(tmpVec,vectorDof,&vectorBasis[iBasis]);

    }
  } else {
    for (int iBasis=0; iBasis<size; iBasis++) {
      dot( m_basis[iBasis],vectorDof,&vectorBasis[iBasis]);
    }
  }

  tmpVec.destroy();
}

// Projection functions from RO space to FE one
void EigenProblemALP::projectOnDof(double* vectorBasis, PetscVector& vectorDof, int size) {
  vectorDof.set(0.);
  for (int iBasis=0; iBasis<size; iBasis++) {
    vectorDof.axpy(vectorBasis[iBasis],m_basis[iBasis]);
  }
}

// Auxiliary function to debug purposes.
void EigenProblemALP::viewALP(double* vToPrint, int vSize, std::ofstream outStream) {
  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);
  if (rankProc == 0) {
    for(int i=0; i<vSize; i++) {
      outStream << vToPrint[i] << std::endl;
    }
    outStream.close();
  }
}


void EigenProblemALP::viewALP(double* vToPrint, int vSize, std::string fName) {
  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);
  if (rankProc == 0) {
    std::ofstream outStream(fName.c_str());
    for(int i=0; i<vSize; i++) {
      outStream << vToPrint[i] << std::endl;
    }
    outStream.close();
  }
}

}
