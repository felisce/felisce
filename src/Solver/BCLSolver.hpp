//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    F. Raphel
//

#ifndef _BCLSOLVER_HPP
#define _BCLSOLVER_HPP

// System includes

// External includes
#ifdef FELISCE_WITH_SUNDIALS
#include "cvode/cvode.h"          /* prototypes for CVODE fcts. and consts. */
#include "nvector/nvector_serial.h" /* serial N_Vector types, fcts., and macros */
#include "cvode/cvode_dense.h"    /* prototype for CVDense */
#include "sundials/sundials_dense.h" /* definitions DenseMat DENSE_ELEM */
#include "sundials/sundials_types.h" /* definition of type realtype */
#include "cvode/cvode_spgmr.h"//tests        /* prototypes & constants for CVSPGMR */
#include "cvode/cvode_band.h"        /* prototype for CVBand */
#include "sundials/sundials_band.h"  /* definitions of type DlsMat and macros */
#include "cvode/cvode_bandpre.h"     /* prototypes & constants for CVBANDPRE module */
#include "sundials/sundials_math.h"  /* definition of ABS */
#endif

// Project includes
#include <Solver/ionicSolver.hpp>
#include <Solver/cardiacFunction.hpp>

namespace felisce {

  class BCLUserData {
  public:
    double u;
    int pos;
    int iType;
  };



  class BCLSolver:
    public IonicSolver {
  public:
    //!Constructor.
    BCLSolver(FelisceTransient::Pointer fstransient);
    //!Destructor.
    ~BCLSolver() override;

    void initialize(std::vector<PetscVector>& sol_0) override;

    //!Solve the EDO.
    void solveEDO() override;
    //!Computation of ionic current from EDO solution.
    void computeIon() override;

    // functor and acces to variable properties fhn

    inline const std::vector<int>& cellType() const {
      return m_cellType;
    }
    inline std::vector<int>& cellType() {
      return m_cellType;
    }

    inline const HeteroMVCoeff& MVCoeff() const {
      return m_heteroCoeff;
    }
    inline HeteroMVCoeff& MVCoeff() {
      return m_heteroCoeff;
    }


#ifdef FELISCE_WITH_SUNDIALS
    static int M_f(realtype t, N_Vector y, N_Vector ydot, void *f_data);
#endif

    static void hgate(double&, double&, double, int);
    static void fgate(double&, double&, double, int);
    static void rgate(double&, double&, double, int);
    static void sgate(double&, double&, double, int);
    static double Heaviside(double,double);

    //Variables for CVODE
    std::vector<BCLUserData> m_userData;
    void * m_data;
    std::vector<void *> m_cvode_mem;
#ifdef FELISCE_WITH_SUNDIALS
    std::vector<N_Vector> m_initvalue;
    std::vector<N_Vector> m_abstol;
#endif

  protected:
    HeteroMVCoeff m_heteroCoeff;
    std::vector<int> m_cellType;
  private:
  };
  static std::vector<double> tauhp;   //tau_h+
  static std::vector<double> tauhm;   //tau_h-
  static std::vector<double> taufp;   //tau_f+
  static std::vector<double> taufm;   //tau_f-
  static std::vector<double> taurp;   //tau_r+
  static std::vector<double> taurm;   //tau_r-
  static std::vector<double> tausp;   //tau_s+
  static std::vector<double> tausm;   //tau_s-
  static std::vector<double> gfi;     //g_fi
  static std::vector<double> Vfi;     //V_fi
  static std::vector<double> gso;     //g_so
  static std::vector<double> gsi;     //g_si
  static std::vector<double> beta1;   //beta_1
  static std::vector<double> beta2;   //beta_2
  static std::vector<double> V1;      //V_1
  static std::vector<double> V2;      //V_2
  static std::vector<double> gto;     //g_to
  static std::vector<double> Vc;      //V_c
  static std::vector<double> Vs;      //V_s
}


#endif
