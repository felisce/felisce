//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

// System includes

// External includes

// Project includes
#include "Solver/schafSolver.hpp"

// I_{ion} = reac_amp* (w/tau_in)  (u-v_min) * (u-v_min) *(v_max - u)/(v_min - v_max)  - (u- v_min) /(tau_out * (v_min- v_max))
//
//           ((v_max-v_min)^{-2}  - w )/tau_open  if u < vcrit
// dw/dt ={
//            -w/tau_close   if u > vcrit
//
//w_0 = (v_max-v_min)^{-2}

namespace felisce {
  SchafSolver::SchafSolver(FelisceTransient::Pointer fstransient):
    IonicSolver(fstransient)
  {}


  SchafSolver::~SchafSolver() {
    m_tauClose.clear();
    m_tauOut.clear();
    m_tauIn.clear();
  }


  void SchafSolver::computeRHS() {
    double& vGate = FelisceParam::instance().vGate;
    double& vMin  = FelisceParam::instance().vMin;
    double& vMax  = FelisceParam::instance().vMax;
    double& dt = m_fstransient->timeStep;
    double& tauOpen = FelisceParam::instance().tauOpen;

    m_bdf.computeRHSTime(dt, m_RHS);

    felInt pos;
    double value_uExtrap;
    double value_RHS;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      if( value_uExtrap < vGate) {
        value_RHS = (1./((vMax - vMin)*(vMax - vMin) * tauOpen));
      } else
        value_RHS = 0.;
      m_RHS.setValue(pos,value_RHS, ADD_VALUES);
    }
    m_RHS.assembly();

  }

  void SchafSolver::solveEDO() {
    double& tauOpen  = FelisceParam::instance().tauOpen;
    double& vGate     = FelisceParam::instance().vGate;
    double& tauClose = FelisceParam::instance().tauClose;
    double& coeffDeriv = m_bdf.coeffDeriv0();
    double& dt = m_fstransient->timeStep;

    felInt pos;
    double value_uExtrap;
    double value_RHS;
    double valuem_solEDO;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      m_RHS.getValues(1,&pos,&value_RHS);//value_RHS = m_RHS(i)
      if( value_uExtrap < vGate) {
        valuem_solEDO = value_RHS * 1./(coeffDeriv/dt + 1./tauOpen);
      } else {
        if (FelisceParam::instance().hasHeteroTauClose) {
          valuem_solEDO = value_RHS * 1./(coeffDeriv/dt + 1./m_tauClose[pos]);
        } else {
          valuem_solEDO = value_RHS * 1./(coeffDeriv/dt + 1./tauClose);
        }
      }
      m_solEDO.setValue(pos,valuem_solEDO, INSERT_VALUES);
    }
    m_solEDO.assembly();
  }

  void SchafSolver::computeIon() {
    if (FelisceParam::instance().stateFilter) {
      if(!m_stabInit) {
        m_stabTerm.duplicateFrom(m_ion);
        m_stabInit=true;
      }
      m_stabTerm.zeroEntries();
    }

    double& tauOut  = FelisceParam::instance().tauOut;
    double& tauIn = FelisceParam::instance().tauIn;
    double& vMin  = FelisceParam::instance().vMin;
    double& vMax  = FelisceParam::instance().vMax;

    felInt pos;
    double value_uExtrap;
    double valuem_solEDO;
    double value_ion;
    felInt sizeVent = 45580;
    if (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart")
      sizeVent = 32320;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      felInt meshId = pos;
      AOPetscToApplication(m_ao,1,&meshId);

      m_solEDO.getValues(1,&pos,&valuem_solEDO);//valuem_solEDO = m_solEDO(i)
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)
      if (FelisceParam::instance().hasInfarct) {
        value_ion = valuem_solEDO/tauIn*(value_uExtrap-vMin)*(value_uExtrap-vMin)*(vMax-value_uExtrap)/(vMax-vMin)-(value_uExtrap-vMin)/(m_tauOut[pos] *(vMax-vMin));
        if (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") {
          if (meshId > (sizeVent-1) )
            value_ion = 0.0;
        }
      }
      else {
        value_ion = valuem_solEDO/tauIn*(value_uExtrap-vMin)*(value_uExtrap-vMin)*(vMax-value_uExtrap)/(vMax-vMin)-(value_uExtrap-vMin)/(tauOut *(vMax-vMin));
        if (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") {
          if (meshId > (sizeVent-1) )
            value_ion = 0.0;
        }
      }
      m_ion.setValue(pos,value_ion, INSERT_VALUES);
    }

    m_ion.assembly();
  }

}
