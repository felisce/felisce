//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

#ifndef _LinearProblemBidomainExtraCell_HPP
#define _LinearProblemBidomainExtraCell_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce 
{
  /*!
   \class LinearProblemBidomainExtraCell
   \authors E. Schenone C. Corrado
   \date 27/01/2012
   \brief ???
   */
  class LinearProblemBidomainExtraCell:
    public LinearProblem {
  public:
    LinearProblemBidomainExtraCell();
    ~LinearProblemBidomainExtraCell() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber);
    void getAngleFiber(felInt iel, int iUnknown, std::vector<double>& elemAngle);
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      m_currentLabel=label;
      IGNORE_UNUSED_FLAG_MATRIX_RHS;
    }
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void readData(IO& io) override;

    void addMatrixRHS() override;

    std::vector<double> & EndocardiumDistance() {
      return m_vectorDistance;
    }

    virtual void sortSolution() {}

    double* sortedSolution() {
      return m_sortedSol;
    }

  protected:
    CurrentFiniteElement* m_fePotExtraCell;
    CurvilinearFiniteElement* m_fePotExtraCellCurv;
    felInt m_ipotExtraCell;
    double* m_sortedSol;

  private:
    double* m_fiber;
    double* m_angleFiber;
    double* m_endocardiumDistance;
    std::vector<double> m_vectorDistance;
    // bool allocateSeqVec;
  };
}

#endif
