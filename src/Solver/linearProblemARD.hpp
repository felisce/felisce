//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:   J-F. Gerbeau 
//

#ifndef _LinearProblemARD_HPP
#define _LinearProblemARD_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "FiniteElement/elementField.hpp"


namespace felisce {
  /*!
   \class LinearProblemNSFracStepProj
   \author J-F. Gerbeau
   \date 25/11/2011
   \brief Advection-Diffusion step of a fractional step algorithm for the Navier-Stokes equations
   */
  class LinearProblemARD:
    public LinearProblem {
  public:
    LinearProblemARD();
    ~LinearProblemARD() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel,FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

  protected:
    CurrentFiniteElement* m_feTemp;
    felInt m_iTemp;
    double m_density;

  private:
    //! boolean which indicate if boundary condition for pressure is apply
    ElementField m_elemFieldAdv;
    ElementField m_elemFieldDif;
    ElementField m_elemFieldRea;
    ElementField m_elemFieldRHS;
    ElementField m_elemField;

    ElementFieldDynamicValue *m_rhsDynamicValue;
    ElementFieldDynamicValue *m_advDynamicValue;
    ElementFieldDynamicValue *m_difDynamicValue;
    ElementFieldDynamicValue *m_reaDynamicValue;
  };
}

#endif
