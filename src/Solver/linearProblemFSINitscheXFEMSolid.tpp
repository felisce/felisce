//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges
//

// System includes

// External includes

// Project includes
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce 
{
  /*!
    \brief Matrix and rhs for the elastic string solver.
  */
  template<class SolidPrb>
  LinearProblemFSINitscheXFEMSolid<SolidPrb>::LinearProblemFSINitscheXFEMSolid():
    SolidPrb()
  {
    m_isSeqLastTimeSolAllocated = false;
    m_isSeqLastTimeVelSolAllocated = false;
    m_isSeqStabExplicitFluidExtrapolAllocated = false;
    m_isSeqRNStrucExtrapolAllocated = false;

    m_elemFieldTimeSchemeType = NULL;
    m_timeSchemeType = 0;

    m_leftSide = 0;
    m_rightSide = 0;

    m_assemblingOfSolidProblemTerms = true;

    m_useCurrentFluidSolution = false;

    m_seqLastTimeSol = PetscVector::null();
    m_seqLastTimeVelSol = PetscVector::null();
    m_seqRNStrucExtrapol = PetscVector::null();

    m_seqVecL2Norm = PetscVector::null();

    m_isStrucOperatorExplicitlyComputed = false;
  }

  template<class SolidPrb>
  LinearProblemFSINitscheXFEMSolid<SolidPrb>::~LinearProblemFSINitscheXFEMSolid()
  {
    if(m_isSeqLastTimeSolAllocated)
      m_seqLastTimeSol.destroy();

   if(m_isSeqLastTimeVelSolAllocated)
      m_seqLastTimeVelSol.destroy();

    if(m_isSeqStabExplicitFluidExtrapolAllocated)
      for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i)
        m_seqStabExplicitFluidExtrapol[i].destroy();

    if(m_isSeqRNStrucExtrapolAllocated)
      m_seqRNStrucExtrapol.destroy();

    if(m_seqVecL2Norm.isNotNull())
      m_seqVecL2Norm.destroy();

    for(std::size_t iPt=0; iPt < m_subItfPts.size(); ++iPt) {
      delete m_subItfPts[iPt];
    }
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES)
  {
    // call the initialize function of the solid problem
    SolidPrb::initialize(mesh, fstransient, comm, doUseSNES);

    // add the fluid variable to the list of physical variables
    // unknowns are already define in the solid problem
    this->m_listVariable.addVariable(velocity, 2);
    this->m_listVariable.addVariable(pressure, 1);
    this->m_listVariable.print(this->verbosity());

    // send the unknown to the fluid problem (should be already initialize) to add them as variable there
    m_pLinFluid->addSolidVariable();

    // get the id of the displacement variable
    m_iDisplacement = this->m_listVariable.getVariableIdList(displacement);

    // get the number of component of the displacement
    m_numSolidComponent = this->m_listVariable[m_iDisplacement].numComponent();

    // get the time scheme type from the data file
    m_elemFieldTimeSchemeType = FelisceParam::instance().elementFieldDynamicValue("TimeSchemeType");
    if(m_elemFieldTimeSchemeType != NULL)
      m_timeSchemeType = (felInt) m_elemFieldTimeSchemeType->constant(Comp1);

    if (m_timeSchemeType == 2) {
      // Robin-Neumann scheme
      // extrapolation order
      m_betaRN.resize(2);
      if (FelisceParam::instance().fsiRNscheme == 0){
        m_betaRN[0] = 0.;
        m_betaRN[1] = 0.;
      } else if (FelisceParam::instance().fsiRNscheme == 1){
        m_betaRN[0] = 1.;
        m_betaRN[1] = 0.;
      } else if (FelisceParam::instance().fsiRNscheme == 2){
        m_betaRN[0] = 2.;
        m_betaRN[1] = -1.;
      }
    }

    // sub element
    m_numVerPerSolidElt = SolidPrb::mesh()->domainDim() + 1;

    // create the std::vector of points for the solid elements
    m_subItfPts.resize(m_numVerPerSolidElt);
    for(std::size_t iver=0; iver < m_subItfPts.size(); ++iver) {
      m_subItfPts[iver] = new Point();
    }

    // compute solid coefficient
    m_massVel = FelisceParam::instance().densitySolid * FelisceParam::instance().thickness / this->m_fstransient->timeStep;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::initPerElementTypeBD(GeometricMeshRegion::ElementType eltType, FlagMatrixRHS flagMatrixRHS)
  {
    // call the function of the solid problem
    SolidPrb::initPerElementTypeBD(eltType, flagMatrixRHS);

    // m_dispLastSol contains some old displacement
    m_dispLastSol.initialize(DOF_FIELD, *(this->m_feDisp), m_numSolidComponent);

    // source term coming from the coupling with the fluid and Nitsche-XFEM terms
    m_elemNitscheXFEMSolidRHS.initialize(QUAD_POINT_FIELD, *(this->m_feDisp), m_numSolidComponent);

    // allocate elementField for the fluid
    m_fluidDofSol.resize(2);
    m_fluidDofSol[0].initialize(DOF_FIELD, *m_feFluid, m_feFluid->numCoor());
    m_fluidDofSol[1].initialize(DOF_FIELD, *m_fePres, 1);

    // integration points
    m_intPoints.clear();
    m_intPoints.resize(this->m_feDisp->numQuadraturePoint());
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS)
  {
    // Assembling of the solid problem terms
    if(m_assemblingOfSolidProblemTerms) {
      if(!this->m_feDisp->hasOriginalQuadPoint())
        this->m_feDisp->updateBasisAndNormalContra(0, elemPoint);

      if(m_isStrucOperatorExplicitlyComputed) {
        // update finite element
        this->m_feDisp->updateMeasNormalContra(0, elemPoint);

        // MATRIX //
        // time scheme
        this->m_elementMatBD[0]->phi_i_phi_j(m_massVel, *(this->m_feDisp), 0, 0, m_numSolidComponent);

        // RHS //
        // time scheme
        m_dispLastSol.setValue(m_seqRNStrucExtrapol, *(this->m_feDisp), iel, m_iDisplacement, this->m_ao, this->dof());
        this->m_elementVectorBD[0]->source(m_massVel, *(this->m_feDisp), m_dispLastSol, 0, m_numSolidComponent);

      } else {
        SolidPrb::computeElementArrayBD(elemPoint, elemIdPoint, iel, flagMatrixRHS);
      }
    } else {
      assembleNitscheXFEMTerms(elemPoint, elemIdPoint, iel, flagMatrixRHS);
    }
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, const std::vector<Point*>& elemNormal, const std::vector< std::vector<Point*> >& elemTangent, felInt& iel, FlagMatrixRHS flagMatrixRHS)
  {
    // Assembling of the solid problem terms
    if(m_assemblingOfSolidProblemTerms) {
      if(!this->m_feDisp->hasOriginalQuadPoint())
        this->m_feDisp->updateBasisAndNormalContra(0, elemPoint);

      if(m_isStrucOperatorExplicitlyComputed) {
        // update finite element
        this->m_feDisp->updateMeasNormalContra(0, elemPoint);

        // MATRIX //
        // time scheme
        this->m_elementMatBD[0]->phi_i_phi_j(m_massVel, *(this->m_feDisp), 0, 0, m_numSolidComponent);

        // RHS //
        // time scheme
        m_dispLastSol.setValue(m_seqRNStrucExtrapol, *(this->m_feDisp), iel, m_iDisplacement, this->m_ao, this->dof());
        this->m_elementVectorBD[0]->source(m_massVel, *(this->m_feDisp), m_dispLastSol, 0, m_numSolidComponent);
      } else {
        SolidPrb::computeElementArrayBD(elemPoint, elemIdPoint, elemNormal, elemTangent, iel, flagMatrixRHS);
      }
    } else {
      assembleNitscheXFEMTerms(elemPoint, elemIdPoint, iel, flagMatrixRHS);
    }
  }



  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::assembleNitscheXFEMTerms(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS)
  {
    (void) elemIdPoint;
    (void) flagMatrixRHS;

    // Assembling of the terms coming from the coupling with the fluid and the Nitsche-XFEM formulation
    std::vector< std::vector<felInt> > vectorIdSupport;
    if((m_timeSchemeType == 3) || (m_timeSchemeType == 2) || (m_timeSchemeType == 4) )
      vectorIdSupport.resize(m_seqStabExplicitFluidExtrapol.size());
    else
      vectorIdSupport.resize(1);

    // loop over the sub structure to compute the integrals involving the fluid velocity and pressure
    felInt numSubElement = m_duplicateSupportElements->getNumSubItfEltPerItfElt(iel);
    if(numSubElement > 0) {
      felInt subElemId;
      felInt fluidElemId;
      std::vector<double> tmpPt(3);
      std::vector<double> itfNormal(3);
      std::vector<double> itfNormalInit(3);

      // get the normal and the initial normal to this sub element
      GeometricMeshRegion::ElementType eltTypeStruc = this->mesh()->bagElementTypeDomain()[0];
      this->mesh()->listElementNormal(eltTypeStruc, iel).getCoor(itfNormal);
      this->mesh()->listElementNormalInit(eltTypeStruc, iel).getCoor(itfNormalInit);

      for(felInt iSubElt=0; iSubElt < numSubElement; ++iSubElt) {
        // get the id of the sub element
        subElemId = m_duplicateSupportElements->getSubItfEltIdxItf(iel, iSubElt);

        // fill the std::vector with the points of the sub-element
        for(std::size_t iPt=0; iPt < m_numVerPerSolidElt; ++iPt) {
          m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
          *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
        }

        // update measure
        this->m_feDisp->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);


        // get the coordinate of the fluid element
        fluidElemId = m_duplicateSupportElements->getMshEltIdxOfSubItfElt(subElemId);

        GeometricMeshRegion::ElementType eltType = m_pLinFluid->mesh()->bagElementTypeDomain()[0];
        int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];

        std::vector<Point*> fluidElemPts(numPointsPerElt);
        std::vector<felInt> fluidElemPtsId(numPointsPerElt);

        m_pLinFluid->mesh()->getOneElement(eltType, fluidElemId, fluidElemPtsId, 0);
        for (int iPoint = 0; iPoint < numPointsPerElt; ++iPoint)
          fluidElemPts[iPoint] = &m_pLinFluid->mesh()->listPoints()[fluidElemPtsId[iPoint]];


        // update the element for the fluid finite element
        m_feFluid->updateFirstDeriv(0, fluidElemPts);

        // compute the integration point for this element
        computeIntegrationPoints(fluidElemPts);

        // get all the support elements for the fluid
        if(FelisceParam::instance().useProjectionForNitscheXFEM || m_useCurrentFluidSolution)
          for(std::size_t i=0; i<vectorIdSupport.size(); ++i)
            m_pLinFluid->supportDofUnknown(0).getIdElementSupport(fluidElemId, vectorIdSupport[i]);
        else
          for(std::size_t i=0; i<vectorIdSupport.size(); ++i)
            m_pLinFluid->supportDofUnknownOld(i,0).getIdElementSupport(fluidElemId, vectorIdSupport[i]);

        // compute the elementary matrix and rhs
        switch(m_timeSchemeType) {
        case 0: {
          // Robin-Neumann schemes (direct implementation) (0th, 1st and 2nd order extrapolated variants)
          for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[0].size() - m_rightSide; ++iSup) {
            // force of the fluid to the structure
            updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolution());

            computeFluidNormalDerivativeJump(2.*iSup - 1., itfNormal, itfNormalInit);
            this->m_elementVectorBD[0]->source(-1., *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);
          }
          break;
        }

        case 1: {
          // Robin - Robin //
          // Nitsche penalty term (derivative of the displacement)
          // This integral is here because the penalty parameter depends of the fluid element
          double penaltyTerm = FelisceParam::instance().viscosity*FelisceParam::instance().NitschePenaltyParam/m_feFluid->diameter();
          double tau = this->m_fstransient->timeStep;
          double coef = 2. - m_leftSide - m_rightSide;

          this->m_elementMatBD[0]->phi_i_phi_j(coef*penaltyTerm/tau, *(this->m_feDisp), 0, 0, m_numSolidComponent);

          m_dispLastSol.setValue(m_seqLastTimeSol, *(this->m_feDisp), iel, m_iDisplacement, this->m_ao, this->dof());
          this->m_elementVectorBD[0]->source(coef*penaltyTerm/tau, *(this->m_feDisp), m_dispLastSol, 0, m_numSolidComponent);


          for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[0].size() - m_rightSide; ++iSup) {
            // get the velocity and pressure at the dof of the structure
            // Nitsche penalty term
            if(FelisceParam::instance().useProjectionForNitscheXFEM)
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolution());
            else
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolutionOld(0));

            computeFluidVelOnSubStructure(itfNormalInit);
            this->m_elementVectorBD[0]->source(penaltyTerm, *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);

            computeFluidNormalDerivativeJump(2.*iSup - 1., itfNormal, itfNormalInit);
            this->m_elementVectorBD[0]->source(-1., *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);
          }
          break;
        }

        case 2: {
          // Robin-Neumann schemes a la Stenberg (0th, 1st and 2nd order extrapolated variants)
          double alpha = 0.5;
          double RNParam = FelisceParam::instance().densitySolid * FelisceParam::instance().thickness / this->m_fstransient->timeStep;
          double penaltyParam1 = (m_feFluid->diameter()) / (FelisceParam::instance().viscosity * FelisceParam::instance().NitschePenaltyParam + RNParam*m_feFluid->diameter());
          double penaltyParam2 =  (FelisceParam::instance().NitschePenaltyParam *FelisceParam::instance().viscosity ) / (FelisceParam::instance().viscosity  *FelisceParam::instance().NitschePenaltyParam + RNParam*m_feFluid->diameter());

          if (m_useCurrentFluidSolution) {
            m_dispLastSol.setValue(m_seqRNStrucExtrapol, *(this->m_feDisp), iel, m_iDisplacement, this->m_ao, this->dof());
            this->m_elementVectorBD[0]->source(-1.0 * penaltyParam2 * RNParam , *(this->m_feDisp), m_dispLastSol, 0, m_numSolidComponent);

            // get the velocity and pressure at the dof of the structure
            // Nitsche penalty term
            for(std::size_t iSup=m_leftSide; iSup< vectorIdSupport[0].size() - m_rightSide; ++iSup) {
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolution());

              computeFluidVelOnSubStructure(itfNormalInit);
              this->m_elementVectorBD[0]->source(alpha * penaltyParam2 * RNParam, *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);

              // force of the fluid to the structure
              computeFluidNormalDerivativeJump(2.*iSup - 1., itfNormal, itfNormalInit);
              this->m_elementVectorBD[0]->source(-1.0 * penaltyParam1 * RNParam , *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);
            }
          } else {
            // get the velocity and pressure at the dof of the structure
            // Nitsche penalty term
            for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i) {
              for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[i].size() - m_rightSide; ++iSup) {
                updateFluidVel(vectorIdSupport[i][iSup], m_seqStabExplicitFluidExtrapol[i], i);
                computeFluidNormalDerivativeJump(2.*iSup - 1., itfNormal, itfNormalInit);
                this->m_elementVectorBD[0]->source(-1.0 * penaltyParam2 * m_betaRN[i], *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);
              }
            }
          }

          break;
        }

        case 3:{
          // stabilized explicit //
          // Nitsche penalty term (derivative of the displacement)
          // This integral is here because the penalty parameter depends of the fluid element
          double penaltyTerm = FelisceParam::instance().viscosity*FelisceParam::instance().NitschePenaltyParam/m_feFluid->diameter();
          double tau = this->m_fstransient->timeStep;
          double coef = 2. - m_leftSide - m_rightSide;

          // Add the penalty terms in the solid if we are using the classic Nitsche's formulation
          if(!FelisceParam::instance().usePenaltyFreeNitscheMethod) {
            // displacement part
            this->m_elementMatBD[0]->phi_i_phi_j(coef*penaltyTerm/tau, *(this->m_feDisp), 0, 0, m_numSolidComponent); // gamma/(h tau) \int d . w
            m_dispLastSol.setValue(m_seqLastTimeSol, *(this->m_feDisp), iel, m_iDisplacement, this->m_ao, this->dof());
            this->m_elementVectorBD[0]->source(coef*penaltyTerm/tau, *(this->m_feDisp), m_dispLastSol, 0, m_numSolidComponent); // gamma/(h tau) \int dold . w

            // get the velocity and pressure at the dof of the structure
            // Fluid Nitsche penalty term
            for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i) {
              for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[i].size() - m_rightSide; ++iSup) {
                updateFluidVel(vectorIdSupport[i][iSup], m_seqStabExplicitFluidExtrapol[i], i);
                computeFluidVelOnSubStructure(itfNormalInit);
                this->m_elementVectorBD[0]->source(penaltyTerm, *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);   // gamma/(h ) \int uf^{n-1} . w
              }
            }
          }

          // Nitsche consistency term
          for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[0].size() - m_rightSide; ++iSup) {
            if(FelisceParam::instance().useProjectionForNitscheXFEM || m_useCurrentFluidSolution)
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolution());
            else
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolutionOld(0));

            computeFluidNormalDerivativeJump(2.*iSup - 1., itfNormal, itfNormalInit);
            this->m_elementVectorBD[0]->source(-1., *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent); // -int sigma(u^{n-1},p^{n-1})n . w
          }
          break;
        }


        case 4:{
          // Robin-Neumann semi-implict  (0th, 1st and 2nd order extrapolated variants) //
          // Nitsche penalty term (derivative of the displacement)
          // This integral is here because the penalty parameter depends of the fluid element
          double penaltyTerm = FelisceParam::instance().viscosity*FelisceParam::instance().NitschePenaltyParam/m_feFluid->diameter();
          double coef = 2. - m_leftSide - m_rightSide;

          if(!m_isStrucOperatorExplicitlyComputed) {
            m_dispLastSol.setValue(this->sequentialSolution(), *(this->m_feDisp), iel, m_iDisplacement, this->m_ao, this->dof());
            this->m_elementVectorBD[0]->source(-coef*penaltyTerm, *(this->m_feDisp), m_dispLastSol, 0, m_numSolidComponent);
          } else {
            // matrix part
            this->m_elementMatBD[0]->phi_i_phi_j(coef * penaltyTerm, *(this->m_feDisp), 0, 0, m_numSolidComponent);

            // All the extrapolation parts are added in the rhs later in the model.
            // There are indeed store in a std::vector during the real solid step and reused here later.
          }

          // get the velocity and pressure at the dof of the structure
          // Nitsche penalty term
          for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i) {
            for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[i].size() - m_rightSide; ++iSup) {
              updateFluidVel(vectorIdSupport[i][iSup], m_seqStabExplicitFluidExtrapol[i], i);
              computeFluidVelOnSubStructure(itfNormalInit);
              this->m_elementVectorBD[0]->source(penaltyTerm, *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);
            }
          }

          // Nitsche consistency term
          for(std::size_t iSup=m_leftSide; iSup<vectorIdSupport[0].size() - m_rightSide; ++iSup) {
            if(FelisceParam::instance().useProjectionForNitscheXFEM || m_useCurrentFluidSolution)
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolution());
            else
              updateFluidVel(vectorIdSupport[0][iSup], m_pLinFluid->sequentialSolutionOld(0));

            computeFluidNormalDerivativeJump(2.*iSup - 1., itfNormal, itfNormalInit);
            this->m_elementVectorBD[0]->source(-1., *(this->m_feDisp), m_elemNitscheXFEMSolidRHS, 0, m_numSolidComponent);
          }

          break;
        }

        default:{
          FEL_ERROR("Bad time splitting scheme");
          break;
        }
        }
      }
    }
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::setDuplicateSupportObject(DuplicateSupportDof* object)
  {
    m_duplicateSupportElements = object;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::setFluidLinPrb(LinearProblemFSINitscheXFEMFluid* pLinPrb)
  {
    m_pLinFluid = pLinPrb;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::setFluidFiniteElement(CurrentFiniteElement* velFE, CurrentFiniteElement* preFE)
  {
    m_feFluid = velFE;
    m_fePres = preFE;
  }


  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::isAssemblingOfSolidProblemTerms(bool isit)
  {
    m_assemblingOfSolidProblemTerms = isit;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::isStrucOperatorExplicitlyComputed(bool isit)
  {
    m_isStrucOperatorExplicitlyComputed = isit;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::setUserBoundaryCondition()
  {
    if(this->m_fstransient->iteration == 1) {
      // resize the arrays
      m_applyDirichletBC.resize(this->m_listUnknown.size());
      m_idDofBCVertices.resize(this->m_listUnknown.size());
      m_valBC.resize(this->m_listUnknown.size());

      // get the id of the boundary of the structure
      // we identify the points that are in only one edge. There should only be "boundaries" elements.
      std::vector<felInt> numEdgePerVertices(this->m_supportDofUnknown[0].numSupportDof(), 0);
      felInt cntDof = 0;
      felInt numComp = 0;
      felInt idof = 0;

      for(std::size_t ielSup=0; ielSup<this->m_supportDofUnknown[0].iEle().size()-1; ++ielSup) {
        for(felInt idSup=0; idSup<this->m_supportDofUnknown[0].getNumSupportDof(ielSup); ++idSup) {
          ++numEdgePerVertices[this->m_supportDofUnknown[0].iSupportDof()[this->m_supportDofUnknown[0].iEle()[ielSup] + idSup]];
        }
      }

      for(std::size_t ivert=0; ivert<numEdgePerVertices.size(); ++ivert) {
        if(numEdgePerVertices[ivert] == 1) {
          // we got a point on the border of the structure
          // add it only if it's not a front point (not attached, to change if one wants to modeled a rigid immersed wall)
          if(!m_duplicateSupportElements->checkFrontPoint(ivert)) {
            cntDof = 0;
            for(std::size_t iUnknown=0; iUnknown<this->m_listUnknown.size(); ++iUnknown) {
              numComp = this->m_listVariable[this->m_listUnknown.idVariable(iUnknown)].numComponent();
              for(felInt icoor=0; icoor<numComp; ++icoor) {
                idof = cntDof + ivert * numComp + icoor;
                AOApplicationToPetsc(this->ao(), 1, &idof);
                m_idDofBCVertices[iUnknown].push_back(idof);
              }

              cntDof += this->supportDofUnknown(iUnknown).numSupportDof() * numComp;
            }
          }
        }
      }

      // By default, apply an homogeneous Dirichlet BC to all unknown of the solid.
      for(std::size_t iUnknown=0; iUnknown<this->m_listUnknown.size(); ++iUnknown) {
        m_applyDirichletBC[iUnknown] = true;
        m_valBC[iUnknown].resize(m_idDofBCVertices[iUnknown].size(), 0.);
      }
    }

    if(!m_idDofBCVertices.empty()) {
      userSetDirichletBC();

      bool isDirichletBC = false;
      for(std::size_t iUnknown=0; iUnknown<this->m_listUnknown.size(); ++iUnknown) {
        if(m_applyDirichletBC[iUnknown]) {
          // Enforce the Dirichlet boundary conditions
          this->matrix(0).zeroRows(m_idDofBCVertices[iUnknown].size(), m_idDofBCVertices[iUnknown].data(), 1.);

          this->vector().setValues(m_idDofBCVertices[iUnknown].size(), m_idDofBCVertices[iUnknown].data(), m_valBC[iUnknown].data(), INSERT_VALUES);

          isDirichletBC = true;
        }
      }

      if(isDirichletBC)
        this->vector().assembly();
    }
  }


  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::userSetDirichletBC()
  {
    // nothing to do, to override in the user solid problem
  }


  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::updateFluidVel(felInt ielSupportDof, const PetscVector& fluidSol, felInt iold)
  {
    felInt idDof;

    for(std::size_t iUnknown=0; iUnknown<m_pLinFluid->listUnknown().size(); ++iUnknown) {
      felInt idVar = m_pLinFluid->listUnknown().idVariable(iUnknown);

      if(FelisceParam::instance().useProjectionForNitscheXFEM || m_useCurrentFluidSolution) {
        // get the value of the velocity and the pressure at the dofs
        for (felInt iSupport=0; iSupport < m_pLinFluid->supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof); iSupport++) {
          for(std::size_t iComp=0; iComp<m_pLinFluid->listVariable()[idVar].numComponent(); ++iComp) {
            // local to global id of the dof
            m_pLinFluid->dof().loc2glob(ielSupportDof, iSupport, idVar, iComp, idDof);

            // petsc ordering
            AOApplicationToPetsc(m_pLinFluid->ao(), 1, &idDof);

            // get values
            fluidSol.getValues( 1, &idDof, &m_fluidDofSol[iUnknown].val(iComp, iSupport));
          }
        }
      } else {
        // get the value of the velocity and the pressure at the dofs
        for (felInt iSupport=0; iSupport < m_pLinFluid->supportDofUnknownOld(iold, iUnknown).getNumSupportDof(ielSupportDof); iSupport++) {
          for(std::size_t iComp=0; iComp<m_pLinFluid->listVariable()[idVar].numComponent(); ++iComp) {
            // local to global id of the dof
            m_pLinFluid->dofOld(iold).loc2glob(ielSupportDof, iSupport, idVar, iComp, idDof);

            // petsc ordering
            AOApplicationToPetsc(m_pLinFluid->aoOld(iold), 1, &idDof);

            // get values
            fluidSol.getValues( 1, &idDof, &m_fluidDofSol[iUnknown].val(iComp, iSupport));
          }
        }
      }
    }
  }


  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::computeIntegrationPoints(std::vector<Point*>& ptElem)
  {
    // compute the integration of the substructure in the reference element of the structure element.
    this->m_feDisp->update(0, m_subItfPts);
    this->m_feDisp->computeCurrentQuadraturePoint();

    TypeShape fluidCellShape = (TypeShape) m_feFluid->geoEle().shape().typeShape();

    // mapping from the current fluid element to the fluid reference element
    if(fluidCellShape == Quadrilateral) {
      for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
        for(int icomp=0; icomp<m_feFluid->numCoor(); ++icomp) {
          m_intPoints[ig][icomp] = 2*(this->m_feDisp->currentQuadPoint[ig][icomp] - (*ptElem[0])[icomp])/((*ptElem[2])[icomp] - (*ptElem[0])[icomp]) - 1.;
        }
      }
    } else if(fluidCellShape == Triangle) {
      double det = (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->x() - ptElem[0]->x())
                   - (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->y() - ptElem[0]->y());

      for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
        m_intPoints[ig][0] = (ptElem[2]->y() - ptElem[0]->y())*(this->m_feDisp->currentQuadPoint[ig].x() - ptElem[0]->x())/det + (ptElem[0]->x() - ptElem[2]->x()) * (this->m_feDisp->currentQuadPoint[ig].y() - ptElem[0]->y())/det;

        m_intPoints[ig][1] = (ptElem[0]->y() - ptElem[1]->y())*(this->m_feDisp->currentQuadPoint[ig].x() - ptElem[0]->x())/det + (ptElem[1]->x() - ptElem[0]->x())*(this->m_feDisp->currentQuadPoint[ig].y() - ptElem[0]->y())/det;
      }

      // m_scalDer[0] = (ptElem[2]->y() - ptElem[1]->y())/det;
      // m_scalDer[1] = (ptElem[1]->x() - ptElem[2]->x())/det;
    } else {
      FEL_ERROR("This shape type is not implemented yet");
    }
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::computeFluidVelOnSubStructure(const std::vector<double>& strucNormal)
  {
    // Compute the velocity at the integration points of the sub structure
    // These points are computed in the function computeIntegrationPoints
    if(m_numSolidComponent < m_feFluid->numCoor()) {
      for(int icomp=0; icomp<this->m_feDisp->numRefCoor(); ++icomp) {
        for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
          m_elemNitscheXFEMSolidRHS.val(icomp, ig) = 0;
          for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
            for(int jcomp=0; jcomp<m_feFluid->numCoor(); ++jcomp) {
              m_elemNitscheXFEMSolidRHS.val(icomp, ig) += m_fluidDofSol[0].val(jcomp, idof) * m_feFluid->refEle().basisFunction().phi(idof, m_intPoints[ig]) * strucNormal[jcomp];
            }
          }
        }
      }
    } else {
      for(int icomp=0; icomp<m_numSolidComponent; ++icomp) {
        for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
          m_elemNitscheXFEMSolidRHS.val(icomp, ig) = 0;
          for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
            m_elemNitscheXFEMSolidRHS.val(icomp, ig) += m_fluidDofSol[0].val(icomp, idof) * m_feFluid->refEle().basisFunction().phi(idof, m_intPoints[ig]);
          }
        }
      }
    }
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::computeFluidNormalDerivativeJump(double sideCoeff, const std::vector<double>& strucNormal, const std::vector<double>& strucNormalInit)
  {
    // compute the tensor at integration points of the sub structure
    // These points are computed in the function computeIntegrationPoints
    double tmpval1 = 0;
    double tmpval2 = 0;

    if(m_numSolidComponent < m_feFluid->numCoor()) {
      for(int icomp=0; icomp<this->m_feDisp->numRefCoor(); ++icomp) {
        for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
          m_elemNitscheXFEMSolidRHS.val(icomp, ig) = 0;

          // velocity
          for(int kcomp=0; kcomp<m_feFluid->numCoor(); ++kcomp) {
            tmpval1 = 0;
            for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
              tmpval2 = 0;
              for(int jcomp=0; jcomp<m_feFluid->numCoor(); ++jcomp) {
                tmpval2 +=  m_fluidDofSol[0].val(jcomp, idof) * strucNormal[jcomp];
              }
              // tmpval1 += m_scalDer[kcomp] * m_feFluid->refEle().basisFunction().dPhi(idof, kcomp, m_intPoints[ig]) * tmpval2;
              tmpval1 += m_feFluid->dPhi[ig](kcomp, idof) * tmpval2;
            }
            m_elemNitscheXFEMSolidRHS.val(icomp, ig) += FelisceParam::instance().viscosity * tmpval1 * sideCoeff * strucNormalInit[kcomp];
          }

          // pressure
          tmpval1 = 0.;
          for(int kcomp=0; kcomp<m_feFluid->numCoor(); ++kcomp)
            tmpval1 += strucNormal[kcomp] * strucNormalInit[kcomp];

          for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
            m_elemNitscheXFEMSolidRHS.val(icomp, ig) -= sideCoeff * m_fluidDofSol[1].val(0, idof) * m_fePres->refEle().basisFunction().phi(idof, m_intPoints[ig]) * tmpval1;
          }
        }
      }

      // if we use the symmetric formulation for the stress
      if(FelisceParam::instance().useSymmetricStress) {
        for(int icomp=0; icomp<this->m_feDisp->numRefCoor(); ++icomp) {
          for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
            for(int jcomp=0; jcomp<m_feFluid->numCoor(); ++jcomp) {
              tmpval1 = 0;
              for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
                tmpval2 = 0;
                for(int kcomp=0; kcomp<m_feFluid->numCoor(); ++kcomp) {
                  tmpval2 +=  m_fluidDofSol[0].val(kcomp, idof) * sideCoeff * strucNormal[kcomp];
                }
                // tmpval1 += m_scalDer[jcomp] * m_feFluid->refEle().basisFunction().dPhi(idof, jcomp, m_intPoints[ig]) * tmpval2;
                tmpval1 += m_feFluid->dPhi[ig](jcomp, idof) * tmpval2;
              }
              m_elemNitscheXFEMSolidRHS.val(icomp, ig) += FelisceParam::instance().viscosity * tmpval1 * strucNormalInit[jcomp];
            }
          }
        }
      }
    } else {
      for(int icomp=0; icomp<m_numSolidComponent; ++icomp) {
        for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
          m_elemNitscheXFEMSolidRHS.val(icomp, ig) = 0;

          // velocity
          for(int jcomp=0; jcomp<m_feFluid->numCoor(); ++jcomp) {
            tmpval1 = 0;
            for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
              tmpval1 +=  m_fluidDofSol[0].val(icomp, idof) * m_feFluid->dPhi[ig](jcomp, idof);
            }

            m_elemNitscheXFEMSolidRHS.val(icomp, ig) += FelisceParam::instance().viscosity * tmpval1 * sideCoeff * strucNormal[jcomp];
          }

          // pressure
          for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
            m_elemNitscheXFEMSolidRHS.val(icomp, ig) -= sideCoeff * m_fluidDofSol[1].val(0, idof) * m_fePres->refEle().basisFunction().phi(idof, m_intPoints[ig]) * strucNormal[icomp];
          }
        }
      }

      // if we use the symmetric formulation for the stress
      if(FelisceParam::instance().useSymmetricStress) {
        for(int icomp=0; icomp<m_numSolidComponent; ++icomp) {
          for(int ig=0; ig<this->m_feDisp->numQuadraturePoint(); ++ig) {
            for(int idof=0; idof<m_feFluid->numDof(); ++idof) {
              tmpval1 = 0;
              for(int jcomp=0; jcomp<m_feFluid->numCoor(); ++jcomp) {
                tmpval1 +=  m_fluidDofSol[0].val(jcomp, idof) * strucNormal[jcomp];
              }

              m_elemNitscheXFEMSolidRHS.val(icomp, ig) += FelisceParam::instance().viscosity * tmpval1 * sideCoeff * m_feFluid->dPhi[ig](icomp, idof);
            }
          }
        }
      }
    }
  }

  /*!
   * Gather the last time step displacement into m_seqLastTimeSol.
   * If m_seqLastTimeSol is null, it will be created and allocated during the gathering
   * according to the parallel shape of parStrucLastSol
   */
  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::gatherSeqLastTimeSol(PetscVector& parStrucLastSol)
  {
    this->gatherVector(parStrucLastSol, m_seqLastTimeSol);
    m_isSeqLastTimeSolAllocated = true;
  }

  /*!
   * Gather the last time step displacement-velocity into m_seqLastTimeVelSol.
   * If m_seqLastTimeVelSol is null, it will be created and allocated during the gathering
   * according to the parallel shape of parStrucLastVelSol
   */
  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::gatherSeqLastTimeVelSol(PetscVector& parStrucLastVelSol)
  {
    this->gatherVector(parStrucLastVelSol, m_seqLastTimeVelSol);
    m_isSeqLastTimeVelSolAllocated = true;
  }

  /*!
   * Gather the extrapolation Robin-Neumann of the displacement into m_seqRNStrucExtrapol.
   * If m_seqRNStrucExtrapol is null, it will be created and allocated during the gathering
   * according to the parallel shape of parStrucExtrapol
   */
  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::gatherRNStructExtrapol(PetscVector& parRNStrucExtrapol) {
    this->gatherVector(parRNStrucExtrapol, m_seqRNStrucExtrapol);
    m_isSeqRNStrucExtrapolAllocated = true;
  }

  /*!
   * Gather the extrapolation of the fluid velocity for the stabilized explicit splitting scheme
   * into m_seqStabExplicitFluidExtrapol.
   * If m_seqStabExplicitFluidExtrapol is null, it will be created and allocated during the gathering
   * according to the parallel shape of parFluidExtrapol
   */
  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::gatherStabExplicitFluidExtrapol(std::vector<PetscVector>& parFluidExtrapol)
  {
    for(std::size_t i=0; i<parFluidExtrapol.size(); ++i)
      this->gatherVector(parFluidExtrapol[i], m_seqStabExplicitFluidExtrapol[i]);

    m_isSeqStabExplicitFluidExtrapolAllocated = true;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::gatherStabExplicitFluidExtrapol(PetscVector& parFluidExtrapol)
  {
    this->gatherVector(parFluidExtrapol, m_seqStabExplicitFluidExtrapol[0]);
    m_isSeqStabExplicitFluidExtrapolAllocated = true;
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::setStabExplicitFluidExtrapol(PetscVector& seqFluidExtrapol)
  {
    m_seqStabExplicitFluidExtrapol[0].copyFrom(seqFluidExtrapol);
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::initStabExplicitFluidExtrapol(felInt order)
  {
    if(m_isSeqStabExplicitFluidExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i)
        m_seqStabExplicitFluidExtrapol[i].destroy();

      m_isSeqStabExplicitFluidExtrapolAllocated = false;
    }

    m_seqStabExplicitFluidExtrapol.resize(order);
    for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i)
      m_seqStabExplicitFluidExtrapol[i] = PetscVector::null();
  }

  template<class SolidPrb>
  void LinearProblemFSINitscheXFEMSolid<SolidPrb>::deleteDynamicData()
  {
    if(m_isSeqStabExplicitFluidExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqStabExplicitFluidExtrapol.size(); ++i) {
        m_seqStabExplicitFluidExtrapol[i].destroy();
        m_seqStabExplicitFluidExtrapol[i] = PetscVector::null();
      }
      m_isSeqStabExplicitFluidExtrapolAllocated = false;
    }
  }

  template<class SolidPrb>
  double LinearProblemFSINitscheXFEMSolid<SolidPrb>::computeL2Norm(PetscVector& inputVec)
  {
    if(m_seqVecL2Norm.isNull())
      m_seqVecL2Norm.duplicateFrom(this->sequentialSolution());

    this->gatherVector(inputVec, m_seqVecL2Norm);

    GeometricMeshRegion::ElementType eltType;
    felInt numPointPerElt = 0;
    felInt numEltPerLabel = 0;

    std::vector<Point*> elemPoint;
    std::vector<felInt> elemIdPoint;
    std::vector <felInt> vectorIdSupport;

    // use to identify global id of dof.
    felInt idDof;
    felInt cpt = 0;

    // Id link element to its supportDof.
    felInt ielSupportDof;

    // return value
    double l2Norm = 0.;
    double tmpL2Norm = 0.;

    // initialize id global of elements.
    felInt numElement[GeometricMeshRegion::m_numTypesOfElement ];
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      eltType = (GeometricMeshRegion::ElementType)ityp;
      numElement[eltType] = 0;
    }

    CurvilinearFiniteElement *fePtr;
    std::vector<double> dofValue;
    std::vector<felInt> idGlobalDof;

    const std::vector<GeometricMeshRegion::ElementType>& bagElementTypeDomain = this->meshLocal()->bagElementTypeDomain();
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      eltType =  bagElementTypeDomain[i];
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, 0);
      elemIdPoint.resize(numPointPerElt, 0);

      // define the curvilinear finite element
      this->defineCurvilinearFiniteElement(eltType);
      this->initElementArrayBD();
      fePtr = this->m_listCurvilinearFiniteElement[m_iDisplacement];

      dofValue.resize(fePtr->numDof()*m_numSolidComponent, 0.);
      idGlobalDof.resize(fePtr->numDof()*m_numSolidComponent, 0);

      for(auto itRef = this->meshLocal()->intRefToBegEndMaps[eltType].begin(); itRef != this->meshLocal()->intRefToBegEndMaps[eltType].end(); itRef++) {
        numEltPerLabel = itRef->second.second;

        for (felInt iel = 0; iel < numEltPerLabel; iel++) {
          this->setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
          fePtr->updateMeas(0, elemPoint);

          // loop over all the support elements
          for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
            // get the id of the support
            ielSupportDof = vectorIdSupport[it];

            cpt = 0;
            for(felInt iDof=0; iDof<fePtr->numDof(); iDof++) {
              for(felInt iComp = 0; iComp<m_numSolidComponent; iComp++) {
                this->dof().loc2glob(ielSupportDof, iDof, m_iDisplacement, iComp, idDof);
                idGlobalDof[cpt] = idDof;
                ++cpt;
              }
            }

            // mapping to obtain new global numbering. (I/O array: idGlobalDof).
            AOApplicationToPetsc(this->m_ao, fePtr->numDof()*m_numSolidComponent, idGlobalDof.data());

            // gets values of associate dofs.
            m_seqVecL2Norm.getValues(fePtr->numDof()*m_numSolidComponent, idGlobalDof.data(), dofValue.data());

            for(felInt ig=0; ig<fePtr->numQuadraturePoint(); ++ig) {
              double tmpval = 0.;
              for(felInt iComp=0; iComp<m_numSolidComponent; ++iComp) {
                double uicomp = 0.;
                for(felInt idof = 0; idof<fePtr->numDof(); ++idof) {
                  uicomp += fePtr->phi[ig](idof) * dofValue[idof*m_numSolidComponent + iComp] ;
                }
                tmpval += uicomp * uicomp;
              }
              tmpL2Norm += tmpval * fePtr->weightMeas(ig);
            }
          }
          numElement[eltType]++;
        }
      }
    }

    MPI_Allreduce(&tmpL2Norm, &l2Norm, 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
    return l2Norm;
  }
}
