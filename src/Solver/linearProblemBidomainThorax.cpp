//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Solver/linearProblemBidomainThorax.hpp"
#include "InputOutput/io.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemBidomainThorax::LinearProblemBidomainThorax():
    LinearProblem("Problem cardiac Thorax"),
    m_fePotThorax(nullptr),
    m_potThoraxRes(nullptr),
    _ECGnode(nullptr) {

    if ( (FelisceParam::instance().writeMatrixECG)  )     {
      this->setNumberOfMatrix(2);
    }

    if ( FelisceParam::instance().withCVG ) {
      #ifdef FELISCE_WITH_CVGRAPH
      m_cvgDirichletVariable = "cvgraphPOTENTIAL";
      #endif
      m_seqVecs.Init("cvgraphPOTENTIAL");
      m_seqVecs.Init("cvgraphCURRENT");

    }

  }

  LinearProblemBidomainThorax::~LinearProblemBidomainThorax() {
    m_fstransient = nullptr;
    m_fePotThorax = nullptr;
    delete [] m_potThoraxRes;
    if (FelisceParam::instance().writeMatrixECG) {
      m_thoraxRes.destroy();
    }
    if (m_boundaryConditionList.numRobinBoundaryCondition())
      m_robinVec.destroy();
    if (_ECGnode) {
      delete [] _ECGnode;
    }
  }

  void LinearProblemBidomainThorax::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;

    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = potThorax;
    listNumComp[0] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(potThorax);
    definePhysicalVariable(listVariable,listNumComp);
  }

  void LinearProblemBidomainThorax::readData(IO& io) {
    IGNORE_UNUSED_ARGUMENT(io);
    m_potThoraxRes = new double[m_mesh[m_currentMesh]->numPoints()];
  }

  void LinearProblemBidomainThorax::writeSolution(int rank, std::vector<IO::Pointer>& io, double& time, int iteration) {
    LinearProblem::writeSolution(rank,io, time, iteration);
    if ( (FelisceParam::instance().writeMatrixECG) ) {
      PetscPrintf(MpiInfo::petscComm(),"Add new variable : potThoraxRes.\n");
      io[m_currentMesh]->writeSolution(rank, time, iteration, 0, "potThoraxRes", m_potThoraxRes, m_supportDofUnknown[0].listNode().size(), this->dimension());
    }
  }

  void LinearProblemBidomainThorax::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELT_TYPE;

    //Init pointer on Finite Element, Variable or idVariable
    m_ipotThorax = m_listVariable.getVariableIdList(potThorax);
    m_fePotThorax = m_listCurrentFiniteElement[m_ipotThorax];
  }


  void LinearProblemBidomainThorax::finalizeEssBCTransientDerivedProblem() {
	std::cout << "CALL TO LinearProblemBidomainThorax::finalizeEssBCTransientDerivedProblem()"<< std::endl;
	std::cout << "WARNING: THIS DIRECTLY READS NODEWISE DATA FROM AN ENSIGHT FILE REGARDLESS OF THE LABEL ORDERNING IN THE DATA FILE"<<std::endl;
    bool readPotentialFromFile = false;
    BoundaryCondition* BC;
    for(std::size_t iBC=0; iBC < m_boundaryConditionList.numDirichletBoundaryCondition(); iBC++) {
      BC = m_boundaryConditionList.Dirichlet(iBC);
      if(BC->typeValueBC() == EnsightFile)
        readPotentialFromFile = true;
    }

    if (m_boundaryConditionList.numDirichletBoundaryCondition()) {
      PetscVector tmpRHS;
      tmpRHS.duplicateFrom(this->sequentialSolution());
      tmpRHS.set(0.);

      if (readPotentialFromFile) {
        felInt numBcNodeHeart;

        if (m_thoraxMatchNode.size() == 0) {
          m_numBcNodeThorax = readMatch(m_thoraxMatchNode,FelisceParam::instance().ECGThoraxmatchFile);
        }
        if (m_heartMatchNode.size() == 0) {
          numBcNodeHeart = readMatch(m_heartMatchNode,FelisceParam::instance().ECGmatchFile);
          if (m_numBcNodeThorax != numBcNodeHeart) {
            FEL_ERROR(" ERROR : Number of match nodes in Thorax different from number of match nodes in Heart.\n");
          }
        }

        std::vector<double> scalarValueOnDof;
        readHeartValue(scalarValueOnDof);

        felInt iPos;
        double value;
        felInt sizeVent = 45580;

        for (felInt i = 0; i < m_numBcNodeThorax; i++) {
          iPos = m_thoraxMatchNode[i]-1;
          AOApplicationToPetsc(m_ao,1,&iPos);
          double penalty = FelisceParam::instance().alphaRobin[0]; // ventricles
          if (m_heartMatchNode[i]-1 > (sizeVent-1) ) {
            penalty = FelisceParam::instance().alphaRobin[1]; // atria
          }
          value = scalarValueOnDof[m_heartMatchNode[i]-1]*penalty;
          tmpRHS.setValue(iPos,value,INSERT_VALUES);
        }
        tmpRHS.assembly();
      }

      // Use the heart extra-cell potential solution to enforce the Dirichlet
      double solval;
      std::map<int, double> idBCAndValue;
      std::set<felInt> idDofBC;

      int rankProc;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rankProc);

      //BoundaryCondition* BC;
      for(std::size_t iBC=0; iBC < m_boundaryConditionList.numDirichletBoundaryCondition(); iBC++) {
        BC = m_boundaryConditionList.Dirichlet(iBC);

        if(BC->typeValueBC() == EnsightFile) {

          felInt idEltInSupportLocal = 0;
          felInt idEltInSupportGlobal = 0;
          felInt idDof;
          std::set<felInt> idDof2;

          std::vector< std::pair<felInt,felInt> > idEltAndIdSupport;
          idEltAndIdSupport = BC->idEltAndIdSupport();

          std::vector<double>& valueBCInSupportDof = BC->ValueBCInSupportDof();
          valueBCInSupportDof.resize(idEltAndIdSupport.size(), 0.);

          for (std::size_t iSupportDofBC = 0; iSupportDofBC < idEltAndIdSupport.size(); iSupportDofBC++) {
            idEltInSupportLocal = idEltAndIdSupport[iSupportDofBC].first;
            ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &idEltInSupportLocal, &idEltInSupportGlobal);
            dof().loc2glob(idEltInSupportGlobal, idEltAndIdSupport[iSupportDofBC].second, 0, 0, idDof);
            AOApplicationToPetsc(m_ao, 1, &idDof);
            bool is_new_element = idDof2.insert(idDof).second;

            if (is_new_element) {
              tmpRHS.getValues(1, &idDof, &solval);
              valueBCInSupportDof[iSupportDofBC] = solval;
            }
          }
        }
      }
      tmpRHS.destroy();
    }
  }

  void LinearProblemBidomainThorax::allocateVectorBoundaryConditionDerivedLinPb() {
    bool readPotentialFromFile = false;
    BoundaryCondition* BC;
    for(std::size_t iBC=0; iBC < m_boundaryConditionList.numRobinBoundaryCondition(); iBC++) {
      BC = m_boundaryConditionList.Robin(iBC);
      if(BC->typeValueBC() == EnsightFile)
        readPotentialFromFile = true;
    }

    if (readPotentialFromFile) {
      felInt numBcNodeHeart;

      if (m_thoraxMatchNode.size() == 0) {
        m_numBcNodeThorax = readMatch(m_thoraxMatchNode,FelisceParam::instance().ECGThoraxmatchFile);
      }
      if (m_heartMatchNode.size() == 0) {
        numBcNodeHeart = readMatch(m_heartMatchNode,FelisceParam::instance().ECGmatchFile);
        if (m_numBcNodeThorax != numBcNodeHeart) {
          FEL_ERROR(" ERROR : Number of match nodes in Thorax different from number of match nodes in Heart.\n");
        }
      }

      std::vector<double> scalarValueOnDof;
      readHeartValue(scalarValueOnDof);

      if (m_fstransient->iteration == 1) {
        m_robinVec.duplicateFrom(this->sequentialSolution());
      }

      m_robinVec.set(0.);

      felInt iPos;
      double value;

      for (felInt i = 0; i < m_numBcNodeThorax; i++) {
        iPos = m_thoraxMatchNode[i]-1;
        AOApplicationToPetsc(m_ao,1,&iPos);
        value = scalarValueOnDof[m_heartMatchNode[i]-1];
        m_robinVec.setValue(iPos,value,INSERT_VALUES);
      }
      m_robinVec.assembly();
    }
  }

  void LinearProblemBidomainThorax::readElectrodesNode(bool flagTranspos) {
    /* Leads position rules:

     V1 est placée sur le 4ème espace intercostal droit, au bord droit du sternum.
     V2 est placée sur le 4ème espace intercostal gauche, au bord gauche du sternum.
     V4 est placée sur le 5ème espace intercostal gauche, sur la ligne médioclaviculaire.
     V3 est placée entre V2 et V4.
     V5 est placée sur le 5ème espace intercostal gauche, sur la ligne axillaire antérieure.
     V6 est placée sur le 5ème espace intercostal gauche, sur la ligne axillaire moyenne.
     */

    // Standard transpos transfer matrix
    if (flagTranspos) {
      std::string electrodeFileName;
      electrodeFileName = FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().ECGelectrodeFile;// load the electrode file (thorax skin nodes)

      std::ifstream electrodeFile(electrodeFileName.c_str());
      if(!electrodeFile) {
        FEL_ERROR("Fatal error: cannot open match file " + electrodeFileName );
      }
      electrodeFile >> m_numSourceNode; // For standard 12 leads ECG = 9

      _ECGnode = new felInt[m_numSourceNode];

      for(felInt j=0; j<m_numSourceNode; j++) {
        electrodeFile >> _ECGnode[j];
      }
      electrodeFile.close();
    }
    // To calculate the transfer matrix on the whole heart surface, reading match files
    else {
      std::string matchFileName;
      matchFileName = FelisceParam::instance().inputDirectory + FelisceParam::instance().ECGmatchFile;// load the match file (heart nodes)

      std::ifstream matchFile(matchFileName.c_str());
      if(!matchFile) {
        FEL_ERROR("Fatal error: cannot open match file " + matchFileName );
      }
      matchFile >> m_numSourceNode;

      _ECGnode = new felInt[m_numSourceNode];

      for(felInt j=0; j<m_numSourceNode; j++) {
        matchFile >> _ECGnode[j];
      }
      matchFile.close();
    }

  }

  void LinearProblemBidomainThorax::calculateRHS() {
    if (m_fstransient->iteration == 1) {
      readElectrodesNode();
    }

    double itApply = m_fstransient->time;
    felInt iPos;

    vector().set(0.);
    // Impose a delta_x(x_i) for i = 1, ..., numSourceNode (at time = i)
    if ( ( static_cast<int>(itApply) <= m_numSourceNode ) && ( Tools::equal(fmod(itApply,1),0.) ) ) {
      int i =  _ECGnode[static_cast<int>(itApply)-1]-1;
      iPos = i;
      AOPetscToApplication(m_ao,1,&iPos);
      vector().setValue(iPos,1.0,INSERT_VALUES);
    }

    vector().assembly();

  }

  void LinearProblemBidomainThorax::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_IEL;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;

    m_fePotThorax->updateMeasQuadPt(0, elemPoint);
    m_fePotThorax->updateFirstDeriv(0, elemPoint);

    // Assembling matrix(0)
    // Evaluate conductivity.
    double sigmaTval;
    ThoraxConductivity* fctSigmaT = new ThoraxConductivity();
    fctSigmaT->initialize(m_fstransient);
    evalFunctionOnElem(*fctSigmaT,sigmaTval,m_currentLabel);
    // \sigma_T(x) \grad u_T
    m_elementMat[0]->grad_phi_i_grad_phi_j(sigmaTval,*m_fePotThorax,0,0,1);
    if ( (FelisceParam::instance().writeMatrixECG) ) {
      // matrix(1) = matrix(0) without BC (used to compute residual)
      m_elementMat[1]->grad_phi_i_grad_phi_j(sigmaTval,*m_fePotThorax,0,0,1);
    }
    delete fctSigmaT;

  }

  void LinearProblemBidomainThorax::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>&, felInt& iel, int) {
    //std::cout << "LinearProblemBidomainThorax::userElementComputeNaturalBoundaryCondition" << std::endl;
    if (m_boundaryConditionList.numRobinBoundaryCondition()) {
      BoundaryCondition* BC;
      CurvilinearFiniteElement* fe = m_listCurvilinearFiniteElement[0];
      fe -> updateMeas(0, elemPoint);

      for (std::size_t iRobin=0; iRobin<m_boundaryConditionList.numRobinBoundaryCondition(); iRobin++ ) {
        if ( m_elemFieldRobin[iRobin].getType() != CONSTANT_FIELD ) {
          BC = m_boundaryConditionList.Robin(iRobin);
          for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
            if(*it_labelNumber == m_currentLabel) {
              m_elemFieldRobin[iRobin].setValue(m_robinVec, *fe, iel, 0, m_ao, dof());
              double coef = FelisceParam::instance().alphaRobin[iRobin];
              FelisceParam::instance().betaRobin[iRobin] = 1./coef;
              //m_elementVectorBD[0]->sourceForComp(coef,*fe,m_elemFieldRobin[iRobin],0,0);
            }
          }
        }
      }
    }

  }

  felInt LinearProblemBidomainThorax::readMatch(std::vector<felInt>& matchNode, std::string matchName) {

    felInt numSourceNode;
    // Input files.
    std::string matchFileName = FelisceParam::instance().inputDirectory + "/" + matchName;// load the match file (heart nodes)

    std::ifstream matchFile(matchFileName.c_str());
    if(!matchFile) {
      FEL_ERROR("Fatal error: cannot open match file " + matchFileName );
    }
    if (FelisceParam::verbose() > 0) {
      std::cout << "Reading " << matchFileName << std::endl;
    }
    matchFile >> numSourceNode;
    matchNode.resize(numSourceNode);
    felInt value;
    for(int j=0; j<numSourceNode; j++) {
      matchFile >> value;
      matchNode[j]=value;
    }
    matchFile.close();

    return numSourceNode;

  }

  void LinearProblemBidomainThorax::readHeartValue(std::vector<double>& scalarValue) {

    std::ostringstream index;
    index << m_fstransient->iteration;
    std::string strindex = "";
    strindex = index.str();
    std::string varName = "potExtraCell";
    //    std::string varName = "potThorax";
    std::string sclFile;

    if ( m_fstransient->iteration < 10 ) {
      sclFile = varName + ".0000" + strindex;
    } else if ( m_fstransient->iteration < 100 ) {
      sclFile = varName + ".000" + strindex;
    } else if ( m_fstransient->iteration < 1000 ) {
      sclFile = varName + ".00" + strindex;
    } else if ( m_fstransient->iteration < 10000 ) {
      sclFile = varName + ".0" + strindex;
    } else {
      sclFile = varName + "." + strindex;
    }
    std::string bcFileName = FelisceParam::instance().bcCondDir + "/" + sclFile + ".scl";
    std::ifstream bcFile(bcFileName.c_str(), std::ios_base::in);

    if ( !bcFile ) {
      FEL_ERROR(" ERROR: Can not open file "+bcFileName+".");
    } else {
      if (FelisceParam::verbose() > 0) {
        std::cout << "Reading " << bcFileName << std::endl;
      }
    }

    char newline[1024];
    bcFile.getline(newline, 1024, '\n');
    double value;
    felInt i = 0;
    while (!bcFile.eof()) {
      bcFile >> value;
      scalarValue.push_back(value);
      i++;
    }
    bcFile.close();
  }

  void LinearProblemBidomainThorax::calculateRes() {
    if (m_fstransient->iteration == 1) {
      m_thoraxRes.duplicateFrom(vector());
    }
    m_thoraxRes.set(0.);

    // m_thoraxRes = _RHS_aux - matrix(1)*m_sol

    // 1. m_thoraxRes = matrix(1)*this->solution()
    mult(matrix(1),this->solution(),m_thoraxRes);

    // 2. m_thoraxRes = _RHS_aux - m_thoraxRes
    double a = -1.0;
    m_thoraxRes.scale(a); // _RHS_aux = zeros (_RHS without BC)

    felInt* iPos = new felInt[m_numDof];
    for (felInt i = 0; i < m_numDof; i++) {
      iPos[i] = i;
    }
    AOPetscToApplication(m_ao,m_numDof,iPos);
    m_thoraxRes.getValues(m_numDof,iPos,m_potThoraxRes);
    delete [] iPos;

  }

void LinearProblemBidomainThorax::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS /*flagMatrixRHS*/) {
    
    (void)iel;
    m_curvFe->updateMeas(0,elemPoint);
  }

#ifdef FELISCE_WITH_CVGRAPH
  void LinearProblemBidomainThorax::initMassBoundaryForCVG() {
    this->assembleMassBoundaryOnly(&LinearProblemBidomainThorax::massMatrixComputer,
                                         this->slave()->interfaceLabels(/*iConn*/0),
                                         &LinearProblemBidomainThorax::initPerETMass,
                                         &LinearProblemBidomainThorax::updateFE,
                                   this->dofBD(/*iConn*/0), this->massBDVec()[/*iConn*/0]);
  }
  
  void LinearProblemBidomainThorax::massMatrixComputer(felInt ielSupportDof) {
    this->m_elementMatBD[0]->zero();
    this->m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFe,0,0,1);
    this->setValueMatrixBD(ielSupportDof);
  }

  void
  LinearProblemBidomainThorax::initPerETMass() {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
    for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    //std::cout << "LinearProblemBidomainThorax::initPerETMass() m_ipotThorax = " << m_ipotThorax << std::endl;
    m_curvFe = this->m_listCurvilinearFiniteElement[ m_ipotThorax ];
  }

  void
  LinearProblemBidomainThorax::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
    m_curvFe->updateMeasNormal(0, elemPoint);  
  }

  void LinearProblemBidomainThorax::readData() {
    //std::cout << "LinearProblemBidomainThorax::readData()" <<std::endl;
    for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for ( std::size_t cVar(0); cVar<this->slave()->numVarToRead(iConn); ++cVar) {
        std::string varToRead = this->slave()->readVariable(iConn,cVar);
        //std::cout << "iConn = " << iConn << " cVar = " << cVar << " varToRead = " << varToRead << std::endl; 
        if ( varToRead  == "POTENTIAL" ) {
          vecs.push_back(m_seqVecs.Get("cvgraphPOTENTIAL"));
        } else if ( varToRead  == "CURRENT" ){
          vecs.push_back(m_seqVecs.Get("cvgraphCURRENT"));
        } else {
          std::cout<<"Connection: "<<iConn<<"--variable to read: "<< varToRead << std::endl;
          FEL_ERROR("Error in variable to read");
        }
      }
      this->slave()->receiveData(vecs,iConn);
    }
  }

  void LinearProblemBidomainThorax::sendData() {
    //std::cout << "LinearProblemBidomainThorax::sendData()" <<std::endl;
    this->gatherSolution();
    for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for ( std::size_t cVar(0); cVar<this->slave()->numVarToSend(iConn); ++cVar) {
        std::string varToSend = this->slave()->sendVariable(iConn, cVar);
        if (  varToSend == "POTENTIAL" ) {
          vecs.push_back(this->sequentialSolution());
        } else if  ( varToSend == "CURRENT" ) {
          this->computeResidual();
          vecs.push_back(this->seqResidual());   
        }
      }  
      this->slave()->sendData(vecs,iConn);
    }
  }

  void LinearProblemBidomainThorax::initMatrix() {
    this->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
    this->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
    this->createAndCopyMatrixRHSWithoutBC();
  }

  void LinearProblemBidomainThorax::addCurrentToRHS() {
    //! for each dof on the boundary on this proc.
    felInt numLocalDofInterface=this->dofBD(/*iBD*/0).numLocalDofInterface();
    std::vector<double> tmp(numLocalDofInterface);
    m_seqVecs.Get("cvgraphCURRENT").getValues( numLocalDofInterface, this->dofBD(/*iBD*/0).loc2PetscVolPtr(), tmp.data() );
    this->vector().setValues(numLocalDofInterface,this->dofBD(/*iBD*/0).loc2PetscVolPtr(),tmp.data(),ADD_VALUES);
    this->vector().assembly();
  }

  void LinearProblemBidomainThorax::cvgAddRobinToRHS(){
    //std::cout << "LinearProblemBidomain::cvgAddRobinToRHS()" << std::endl;
    //std::cout << "  alpha_robin = " << FelisceParam::instance().alphaRobin[0/*iRobin*/] << std::endl;
  }

#endif

}
