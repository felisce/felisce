//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __LINEARPROBLEMLINEARELASTICITY_HPP__
#define __LINEARPROBLEMLINEARELASTICITY_HPP__

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce {

  class LinearProblemLinearElasticity:
    public LinearProblem {
  public:
    LinearProblemLinearElasticity();
    ~LinearProblemLinearElasticity() override= default;;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void userFinalizeEssBCTransient() override{};
    void updateCurrentVelocity();
    void updateOldDisplacement();
    void initializeDofBoundaryAndBD2VolMaps();
    inline felInt iDisplacement() { return m_iDisplacement; };
    void readStressInterface(std::map<int, std::map<int,std::map<int,int> > > externalMap, const PetscVector& externalSeqStress, felInt iUC0);
    std::pair<double,double> computeTestQuantities();
    void testQuantitiesComputer(felInt ielSupportDof);
    void coupled(bool coupled) { m_coupled = coupled; };
    double computeL2ScalarProduct(std::string l, std::string r);
    void initPetscVecsForCoupling();
    void applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) override;
    bool coupled() const {return m_coupled;};
  
    /** 
     * @brief it computes the P1 normal field on the lateral surface
     * This function compute the P1 outward normal starting from the discontinuos normal.
     * It also computes, for each boundary dof, the measure of the all the boundary elements that share the dof it has to be called when you init the structure
     */
    void computeNormalField(const std::vector<int> & labels, felInt idVecVariable);


    /** 
     * @brief Used to compute the P1 outward normal
     */
    void normalFieldComputer(felInt ielSupportDof);

    /** 
     * @brief Used to update fe used for the computation of the P1 outward normal
     */
    void updateFeNormVel(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint);

    /** 
     * @brief Used to normalized the P1 outward normal right after its computation
     * It does a getValues from the sequential normalField and a setValue on the parallel normalField.
     * It is then necessary to call an assembly on the parallel normalField after finished using it.
     */
    void normalizeComputer(felInt ielSupportDof);

    /** 
     * @brief Virtual function to initialize element fields for the computation of the P1 outward normal
     */
    void initPerETNormVel() {};

    inline DofBoundary& dofBD(std::size_t iBD) { return m_dofBD[iBD]; };

    void sumOnBoundary(PetscVector& v, PetscVector& b, const DofBoundary& dofBD);

  protected:
    // Dof boundary utilities
    std::vector<DofBoundary> m_dofBD = std::vector<DofBoundary>(1, DofBoundary());

  protected:
    CurrentFiniteElement* m_fe;
    CurvilinearFiniteElement* m_curvFe;
    felInt m_iDisplacement;

    void initPerET();
    void updateFe(const std::vector<Point*>& elemPoint,const std::vector<int>& /*elemIdPoint*/);
  private:
    void scalarProductComputer(felInt ielSupportDof);

  public:
    void computeBoundaryStress(const std::vector<int>& labels);
  private:

    void computeRHS(felInt iel);
    //! Material properties
    double m_mu;
    double m_lambda;
    double m_density;

    double m_deltaT;
    double m_gammaNM;
    double m_betaNM;

    std::vector<int> m_interfaceLabels;

    bool m_coupled;
  protected:
    Tools::mapHolder<std::string,ElementField> m_elemFields;
    //--------data for computeBoundaryStress--------
  protected:
    PetscVector m_computedStress, m_seqComputedStress;

    std::vector<double> m_auxiliaryDoubles;
    std::vector<std::string> m_auxiliaryStrings;
    std::vector<int> m_auxiliaryInts;
  private:
    // Element fields (DOF_FIELD)
    // std::vector, domain elements, displacement values
    ElementField m_elemFieldDisplacementVolumeDofs;
    // std::vector, boundary elements, stress values
    ElementField m_elemFieldStress;
    // temporary strcutures used in the loop
    // matrix (d x d), grad(i,j) = d u^j / d x_i|_{current boundary dof}
    UBlasMatrix m_grad;
    // std::vector (d) stress = mu (grad n + grad^t n) +lambda tr(grad) n
    UBlasVector m_stress;
    ElementType m_volEltType,m_previousVolEltType;
    void boundaryStressComputer(felInt ielSupportDof);
    void initPerETBoundaryStress();
    void updateFEBoundaryStress(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint);

  public:
    void initFixedPointAcceleration();
    /// This two methods implement aitken acceleration algorithm to
    /// speed up fixed point iteration.
    /// First you call accelerationPreStep
    /// and you pass the input that you are about to use
    /// Than you use it and you obtain a new input
    /// you pass this input to accelerationPostStep
    /// To obtain a smarter input based on that.
    void accelerationPreStep(PetscVector& seqCurrentInput);
    void accelerationPostStep(PetscVector& seqCurrentInput,felInt cIteration);
  private:
    /// Different available techniques
    enum accelerationType {
        FixedPoint = 0,  /*!< Standard fixed point iterations. We need only x1 and only for computing norm of update */
        Relaxation = 1,  /*!< Relaxed fixed point iterations. We need only x1 */
        Aitken     = 2,  /*!< Aitken acceleration. We need only x0,x1,fx0,fx1 */
        IronsTuck  = 3   /*!< Variant of Aitken acceleration. We need only x0,x1,fx0,fx1 */
        };

    accelerationType m_accMethod; ///< The acceleration method in use
    double m_omegaAcceleration;   ///< Accelaration parameter, computed during acceleration or setted


#ifdef FELISCE_WITH_CVGRAPH
  public:
    void sendData();
    void readData() override;
  private:
    void assembleMassBoundaryAndInitKSP( std::size_t iConn ) override;
    void cvgraphNaturalBC(felInt iel) override;
  protected:
    ElementField m_robinAux;

    void massMatrixComputer(felInt ielSupportDof);
    void initPerETMass();
    void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&);
#endif
  };
}

#endif
