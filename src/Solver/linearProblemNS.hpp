//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _LinearProblemNS_HPP
#define _LinearProblemNS_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"
#include "Geometry/Tools/locator.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacCycle.hpp"
#include "Solver/linearProblem.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Solver/RISModel.hpp"

/*!
 \file linearProblemNS.hpp
 \authors J. Foulon & J-F. Gerbeau & V. Martin
 \date 05/01/2011
 \brief Monolithic algorithm for the Navier-Stokes equations
 */

// #define A_MIN3(a,b,c) ( (a) < (b) ? ((a)<(c) ? 0 : 2) : ((b)<(c) ? 1 : 2) )
// #define A_MAX3(a,b,c) ( (a) > (b) ? ((a)>(c) ? 0 : 2) : ((b)>(c) ? 1 : 2) )

namespace felisce {
  enum class FlagMeshUpdated {PreviousMesh = 0, CurrentMesh = 1};

  /*!
   \class LinearProblemNS
   \authors J. Foulon & J-F. Gerbeau & V. Martin
   \date 05/01/2011
   \brief Manage specific functions for Navier-Stokes problem
   */
  class LinearProblemNS:
    public LinearProblem {

  public:
    LinearProblemNS();
    ~LinearProblemNS() override;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    void initializeTimeScheme(Bdf* bdf) override { m_bdf = bdf; }

    void initializeLumpedModelBC(LumpedModelBC* lumpedModelBC);

    void initializeRISModel(RISModel* risModel);

    void initializeCardiacCycle(CardiacCycle* cardiacCycle);

    void InitializeDerivedAttributes() override;

    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs) override;

    void initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void allocateElemFieldBoundaryConditionDerivedLinPb(const int idBCforLinCombMethod=0) override;

    void initPerDomainBoundaryCondition(std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,int label,
                                        felInt numEltPerLabel, felInt* ielSupportDof, ElementType& eltType,
                                        felInt numElement_eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) override;


    void applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) override;
    void applyNaturalBoundaryConditionLumpedModelBC(std::vector<Point*>& elemPoint,const std::vector<felInt>& elemIdPoint,
        int label, felInt numEltPerLabel,
        felInt* ielSupportDof, std::size_t iLumpedModelBC, ElementType& eltType,
        felInt numElement_eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    //!Assembly loop on boundary element of a certain label in implicit LumpedModelBC
    void assembleMatrixImplLumpedModelBC(int rank, const std::size_t iLabel) override;
    // LumpedModelBC: NS model with implicit implementation
    void userChangePattern(int numProc, int rankProc) override;

    //! Functions used in method of characteristics
    //==============================================

    void computeElementArrayCharact(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                    felInt& iel, ElementType& eltType, felInt& ielGeo, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    //similar to functions of std::set value by vectors in ElementField class, but for a field of type QUAD_POINT_FIELD
    //obliged to put here because mesh and some functions of LinearProblem class are needed
    void setValueCharact(PetscVector& v, ElementField& elemFieldCharact, CurrentFiniteElement& fe, felInt iel,
                         int idVariable, const ElementType& eltType, felInt ielGeo, const double dt);
    //backtracking the characteristic line a length dt
    int charactLine(PetscVector& v, int idVariable, int numComp, const double dt, int numStep, int& effStep,
                    Point& pt, Point& localCoord, ElementType& eltType, felInt& ielGeo, Point& vel);
    //find the next point while backtracking the characteristic line a length step from a point by a variant of RK4 scheme
    int nextPt(PetscVector& v, int idVariable, int numComp, const double step,
               Point& pt, Point& localCoord, ElementType& eltType, felInt& ielGeo, Point& vel);
    //backtracking the characteristic line in one element
    int charactLineElem(PetscVector& v, int idVariable, int numComp, double dtTol,
                        Point& pt, Point& localCoord, ElementType& eltType, felInt& ielGeo, Point& vel, double& dtLeft);
    //std::vector interpolation
    Point vecInterp(PetscVector& v, int idVariable, int numComp,
                    const Point& localCoord, ElementType& eltType, felInt ielGeo);
    //===================================

    void gatherVectorBeforeAssembleMatrixRHS() override;
    void initExtrapol(PetscVector& V_1) override;
    void updateExtrapol(PetscVector& V_1) override;
    void addMatrixRHS() override;

    void reviseSolution(PetscVector& alphai, std::vector<PetscVector>& stationnarySolutions);

    // functions non linear solver
    void evalDynamicResidual();
    void addMatrixRHSNl(const FlagMatrixRHS flagMatrixRHS);
    inline PetscVector& previousSolution() {
      return m_solExtrapol;
    }


    /**
     * @brief it computes the P1 normal field on the lateral surface
     * This function compute the P1 outward normal starting from the discontinuos normal.
     * It also computes, for each boundary dof, the measure of the all the boundary elements that share the dof it has to be called when you init the structure
     */
    void computeNormalField(const std::vector<int> & labels, felInt idVecVariable);


    /**
     * @brief Used to compute the P1 outward normal
     */
    void normalFieldComputer(felInt ielSupportDof);

    /**
     * @brief Used to update fe used for the computation of the P1 outward normal
     */
    void updateFeNormVel(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint);

    /**
     * @brief Used to normalized the P1 outward normal right after its computation
     * It does a getValues from the sequential normalField and a setValue on the parallel normalField.
     * It is then necessary to call an assembly on the parallel normalField after finished using it.
     */
    void normalizeComputer(felInt ielSupportDof);

    /**
     * @brief Virtual function to initialize element fields for the computation of the P1 outward normal
     */
    void initPerETNormVel() {};


    // Access functions
    inline const LumpedModelBC* lumpedModelBC() const {
      return m_lumpedModelBC;
    }

    virtual void updateVolume() {};

    // assemble the terms for the face-oriented stabilization
    void assembleFaceOrientedStabilization();

    void computeBoundaryStress(PetscVector& vec);

    inline felInt idVarVel()    const { return m_iVelocity; }
    inline felInt idVarPre()    const { return m_iPressure; }
    inline felInt idVarLag()    const { return m_iLagrange; }
    inline felInt idVarJap()    const { return m_iJapanCon; }
    inline felInt iUnknownVel() const { return m_iUnknownVel; }
    inline felInt iUnknownPre() const { return m_iUnknownPre; }
    inline felInt iUnknownLag() const { return m_iUnknownLag; }
    inline felInt iUnknownJap() const { return m_iUnknownJap; }

    bool m_hasLocalizationChanged = false;
    inline bool hasLocalizationChanged() { return m_hasLocalizationChanged; } 
    void localizeItfQpInBackMesh(std::size_t itfMsh, std::size_t bckMsh, felInt itfVar, std::vector<std::vector<felInt>>& mapItfQpInBackMesh);
    void evaluateFluidStructurePattern();

    void computeSolidFluidMaps();
    void updateStructureVel(CurBaseFiniteElement& fe, std::vector<felInt>& elemIdPoint);
    
    void updateNormal(CurvilinearFiniteElement& fe, int idMsh, std::vector<felInt>& elemIdPoint);

    void computeBHStabLagLag(const double coeff, const int iq);
    void computeBHStabLagJap(const double coeff, const int iq);
    void computeBHStabJapLag(const double coeff, const int iq);
    void computeBHStabJapJap(const double coeff, const int iq);

    void assembleBarbosaHughesStab();
    void assembleJapanConstraintInterface();
    void assembleJapanConstraintBoundary();
    void assembleBrezziPitkarantaStab();

    void computeLagrangeMultiplierOnInterface(FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);

    void setInterfaceExchangedData(std::vector<double>& itfVel, std::vector<double>& itfForc);

    void computeInterfaceStress();
    void computeElementaryInterfaceStress(const std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt iel, ElementVector& elemVec);

    void computeItfDisp(std::vector<double>& itfDisp);
    void moveItfMeshes();

    void computeRealPressure();
    
  protected:
    CurrentFiniteElement* m_feVel;
    CurrentFiniteElement* m_fePres;
    CurvilinearFiniteElement* m_curvFeVel;
    CurvilinearFiniteElement* m_curvFePre;
    CurvilinearFiniteElement* m_curvFeLag;
    CurvilinearFiniteElement* m_curvFeStruc;
    CurrentFiniteElementWithBd* m_feVelWithBd;
    CurrentFiniteElementWithBd* m_fePreWithBd;
    Variable* m_velocity;
    Variable* m_pressure;
    Variable* m_displacement;
    felInt m_iVelocity; //variables
    felInt m_iPressure;
    felInt m_iLagrange;
    felInt m_iJapanCon;
    felInt m_iDisplacement;
    felInt m_iUnknownVel; //unknowns
    felInt m_iUnknownPre;
    felInt m_iUnknownLag;
    felInt m_iUnknownJap;

    int m_velBlock;
    int m_preBlock;
    int m_lagBlock;
    int m_japBlock;

    std::vector<std::vector<felInt> > m_mapLagQpInVelMesh;
    std::vector<std::vector<felInt> > m_mapLagQpInVelMeshSeq;
    std::vector<std::vector<felInt> > m_mapJapQpInVelMesh;
    std::vector<std::vector<felInt> > m_mapJapQpInVelMeshSeq;

    std::vector<double> m_realDisp;
    std::vector<double> m_fictDisp;

    bool m_initializeMapPnt;
    std::vector<felInt> m_mapPntRealToFict;

    double m_viscosity;
    double m_density;
    double m_theta;
    bool m_useSymmetricStress;
    PetscVector m_seqBdfRHS;
    PetscVector m_solExtrapol;

    std::vector<int> m_auxiliaryInts;

    //============== LumpedModelBC============
    LumpedModelBC* m_lumpedModelBC;
    //! Vector of elemField to apply LumpedModelBC boundary conditions.
    std::vector<ElementField> m_elemFieldLumpedModelBC;
    std::vector<double> PlumpedModelBC;

    ElementField m_elemFieldRHS;

    //Reduced steklov
    bool m_forceConvAndStabComputation;

    FlagMeshUpdated m_meshUpdateState = FlagMeshUpdated::PreviousMesh;

    // Fictitious + lagrange multiplier
    ElementField m_structVelQuad;
    bool         m_swapNormals;



    virtual void userComputeItfDisp() {};
    virtual void userMoveFictMesh() {};

  private:
    // private function for face oriented stabilization
    void addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec);
    void updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fevel, CurrentFiniteElementWithBd* fepre, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup);

    //! boolean which indicate if boundary condition for pressure is apply
    ElementField m_elemFieldAdv;
    ElementField m_elemFieldRHSbdf;
    ElementField m_elemFieldCharact;
    ElementField m_elemFieldTotalPressureFormulation;
    ElementField m_elemFieldSistole;
    ElementField m_elemFieldDiastole;

    //! Element field for Newton interation
    ElementField m_elemFieldVelSeq;
    ElementField m_elemFieldPresSeq;
    PetscVector m_betaSeq;

    Bdf* m_bdf;
    CardiacCycle* m_cardiacCycle;
    RISModel* m_ris;

    bool allocateSeqVec;
    bool allocateSeqVecExt;

    // Characteristic Method
    Locator::Pointer m_pLocator = nullptr;

    //ALE
    ElementField m_elemFieldVelMesh;
    PetscVector m_beta;
    int numDofExtensionProblem;

    std::vector<PetscInt> m_petscToGlobal1;
    std::vector<PetscInt> m_petscToGlobal2;
    std::vector<PetscScalar> m_auxvec;

    // Fictitious + lagrange multiplier
    std::vector<double> *m_structVel = nullptr;
    std::vector<double> *m_intfForc = nullptr;
    std::vector<Point> m_ficInterface;

  public:
    ElementField m_structNormalQuad;

  private:
    ElementField m_seqLagDofPts;
    CSRMatrixPattern m_csrNoInterface;
    bool m_isCsrNoInterfaceStored;

    //Theta-Method
    std::vector<ElementMatrix*> m_elMatRHSThetaMethod;
    void computeRHSThetaMethod(felInt& iel);
    ElementField m_elemFieldPres;

  public:

    // distinction in order to add lbg term on current mesh with only_rhs.
    void onPreviousMesh(){ m_meshUpdateState = FlagMeshUpdated::PreviousMesh;}
    void onCurrentMesh(){ m_meshUpdateState = FlagMeshUpdated::CurrentMesh;}
 

    #ifdef FELISCE_WITH_CVGRAPH
    public:
      // Read/Send
      void sendData();
      void readData() override;
    
    private:
      void cvgraphNaturalBC(felInt iel) override;
      ElementField m_robinAux;
      // Functions for mass matrix boundary computation
      void initPerETMass();
      void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&);
      void massMatrixComputer(felInt ielSupportDof);
      void assembleMassBoundaryAndInitKSP( std::size_t iConn ) override;
    #endif
  };
}

#endif
