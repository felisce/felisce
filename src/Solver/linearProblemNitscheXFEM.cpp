//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Fernandez & F.M. Gerosa
//

/*!
 \file  linearProblemNitscheXFEM.cpp
 \authors M. Fernandez & F.M. Gerosa
 \date  04/2018
 \brief Solver for a fictitious domain and Nitsche-XFEM fluid formulation.
 */

// System includes
#include <numeric>

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "Geometry/geo_utilities.hpp"
#include "Geometry/neighFacesOfEdges.hpp"
#include "linearProblemNitscheXFEM.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce 
{
  LinearProblemNitscheXFEM::LinearProblemNitscheXFEM():
    LinearProblem("NXFEM Fluid Solver",1,2),
    m_viscosity(0.),
    m_density(0.),
    m_bdf(NULL) 
  {
    m_isSeqBdfRHSAllocated = false;
    m_isSeqVelExtrapolAllocated = false;

    m_curvFeStruc = nullptr;

    m_seqBdfRHS.resize(FelisceParam::instance().orderBdfNS);
    for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i)
      m_seqBdfRHS[i] = PetscVector::null();

    m_seqVelExtrapol.resize(FelisceParam::instance().orderBdfNS);
    for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i)
      m_seqVelExtrapol[i] = PetscVector::null();

    m_intfVelo = nullptr;
    m_intfForc = nullptr;  
  }

  LinearProblemNitscheXFEM::~LinearProblemNitscheXFEM() 
  {

    m_bdf = nullptr;
    if (m_isSeqBdfRHSAllocated) {
      for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i)
        m_seqBdfRHS[i].destroy();
      m_seqBdfRHS.clear();
    }

    if (m_isSeqVelExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i)
        m_seqVelExtrapol[i].destroy();
      m_seqVelExtrapol.clear();
    }

    for(std::size_t iPt=0; iPt < m_mshPts.size(); ++iPt) {
      delete m_mshPts[iPt];
    }

    for(std::size_t iPt=0; iPt < m_subItfPts.size(); ++iPt) {
      delete m_subItfPts[iPt];
    }

    for(std::size_t i=0; i<m_solutionOld.size(); ++i)
      m_solutionOld[i].destroy();

    for(std::size_t i=0; i<m_sequentialSolutionOld.size(); ++i)
      m_sequentialSolutionOld[i].destroy();
    
    for(std::size_t i=0; i<m_aoOld.size(); ++i)
      AODestroy(&m_aoOld[i]);
  }

  void LinearProblemNitscheXFEM::initialize(std::vector<GeometricMeshRegion::Pointer>& vec_mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) 
  {
    // calling initialize in LinearProblem
    LinearProblem::initialize(vec_mesh, comm, doUseSNES);

    // the problem
    m_fstransient = fstransient;

    // define variable of the linear system
    std::vector<PhysicalVariable> listVariable(2);
    std::vector<std::size_t> listNumComp(2);
    listVariable[0] = velocity;
    listNumComp[0]  = dimension();
    listVariable[1] = pressure;
    listNumComp[1]  = 1;
      
    //define unknown of the linear system.
    m_listUnknown.push_back(velocity);
    m_listUnknown.push_back(pressure);

    //adding Darcy pressure on a boundary
    if ( FelisceParam::instance().contactDarcy > 0 ) {
      listVariable.push_back(pressureDarcy);  // this represents the constant LM
      listNumComp.push_back(1);               // to be used with fusionDof and AllToOne = true
      m_listUnknown.push_back(pressureDarcy);
    }

    // link between variable and unknowns
    definePhysicalVariable(listVariable, listNumComp);

    // get id of the variables
    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    m_iPressure = m_listVariable.getVariableIdList(pressure);

    // get the variables
    m_velocity = &m_listVariable[m_iVelocity];
    m_pressure = &m_listVariable[m_iPressure];

    // get id of the unknowns
    m_iUnknownVel = m_listUnknown.getUnknownIdList(velocity);
    m_iUnknownPre = m_listUnknown.getUnknownIdList(pressure);

    // index block unknown
    m_velBlock = m_listUnknown.getBlockPosition(m_iUnknownVel,0);
    m_preBlock = m_listUnknown.getBlockPosition(m_iUnknownPre,0);

    if ( FelisceParam::instance().contactDarcy > 0 ) {
      m_iPreDarcy   = m_listVariable.getVariableIdList(pressureDarcy);   // get id of the variable
      m_presDarcy   = &m_listVariable[m_iPreDarcy];                      // get the variable
      m_iUnknownDar = m_listUnknown.getUnknownIdList(pressureDarcy);     // get id of the unknown
      m_darBlock    = m_listUnknown.getBlockPosition(m_iUnknownDar,0);   // index block unknown
    }

    // data from the data file
    m_viscosity          = FelisceParam::instance().viscosity;
    m_density            = FelisceParam::instance().density;
    m_nitschePar         = FelisceParam::instance().NitschePenaltyParam;

    m_useSymmetricStress = FelisceParam::instance().useSymmetricStress;
    m_useNSEquation      = FelisceParam::instance().NSequationFlag;
    m_useInterfaceStab   = FelisceParam::instance().useInterfaceFlowStab;
    
    // number of vertes for sub element
    m_numVerPerFluidElt = mesh()->domainDim() + 1;
    m_numVerPerSolidElt = mesh()->domainDim();

    // create the vector of points for the fluid elements
    m_mshPts.resize(m_numVerPerFluidElt);
    for(std::size_t iver=0; iver < m_mshPts.size(); ++iver) {
      m_mshPts[iver] = new Point();
    }

    m_itfPts.resize(m_numVerPerSolidElt);
    m_subItfPts.resize(m_numVerPerSolidElt);
    for(std::size_t iver=0; iver < m_subItfPts.size(); ++iver) {
      m_subItfPts[iver] = new Point();
    }

    m_skipSmallVolume = m_skipSmallVolume && ( dimension() == 3 );
    m_printSkipVolume = m_printSkipVolume && m_skipSmallVolume;
  }

  void LinearProblemNitscheXFEM::initSupportDofDerivedProblem() 
  {

    m_duplicateSupportElements->duplicateIntersectedSupportElements(m_supportDofUnknown);
  }

  void LinearProblemNitscheXFEM::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) 
  {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    // Get the finite elements
    m_feVel  = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];

    // Definition of the ElemField
    m_elemFieldAdv.initialize(   DOF_FIELD, *m_feVel, m_feVel->numCoor());
    m_elemFieldAdvDup.initialize(DOF_FIELD, *m_feVel, m_feVel->numCoor());
    m_elemFieldAdvTmp.initialize(DOF_FIELD, *m_feVel, m_feVel->numCoor());
    m_elemFieldRHSbdf.initialize(DOF_FIELD, *m_feVel, m_velocity->numComponent());
    m_elemFieldNormal.initialize(CONSTANT_FIELD, m_feVel->numCoor());

    // solid
    m_strucVelQuadPts.initialize(QUAD_POINT_FIELD, *m_feVel, m_velocity->numComponent()); // in computeElementArray
    m_strucVelDofPts.initialize(DOF_FIELD, *m_curvFeStruc, m_curvFeStruc->numCoor()); // in computeElementArray
    m_ddotComp.initialize(QUAD_POINT_FIELD, *m_fePres, m_pressure->numComponent()); // in computeElementArray
  }

  void LinearProblemNitscheXFEM::initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) 
  {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    // Get the curvilinear finite elements
    m_curvFeVel   = m_listCurvilinearFiniteElement[m_iVelocity];
    m_curvFePress = m_listCurvilinearFiniteElement[m_iPressure];
  }

  void LinearProblemNitscheXFEM::userChangePattern(int numProc, int rankProc) 
  {
    (void) numProc;
    (void) rankProc;
    // There may be integrals that involve the dof of the two duplicated support elements.
    // The integrals for the ghost penalty modify also the pattern
    // same for the face-oriented stabilization and the DG terms for the tip 
    
    // definition of variables
    felInt currentIelGeo, oppositeIelGeo;
    felInt node1, node2;
    felInt idVar1, idVar2;
    felInt ielSupport, jelSupport;
    std::vector<felInt> vecSupportCurrent, vecSupportOpposite;
    bool isAnInnerFace;

    GeometricMeshRegion::ElementType eltType, eltTypeOpp;
    felInt numEltPerLabel;
    felInt numElement[GeometricMeshRegion::m_numTypesOfElement];
    felInt numFacesPerElement;

    const std::vector<felInt>& iSupportDof = m_supportDofUnknown[0].iSupportDof();
    const std::vector<felInt>& iEle = m_supportDofUnknown[0].iEle();

    // the initial repartition of dof
    const felInt numDof          = dof().numDof();
    const felInt numDofByProc    = numDof/MpiInfo::numProc();
    const felInt beginIdDofLocal = MpiInfo::rankProc()*numDofByProc;
    const felInt numDofProc      = ( MpiInfo::rankProc() == MpiInfo::numProc()-1 ) ? numDof-beginIdDofLocal : numDofByProc;
    const felInt endIdDofLocal   = beginIdDofLocal + numDofProc;

    std::vector<std::set<felInt> > nodesNeighborhood(numDofProc);

    // build the edges or faces depending on the dimension
    // In this function, "faces" refers to either edges or faces.
    if( FelisceParam::instance().useGhostPenalty || FelisceParam::instance().NSStabType == 1 || !FelisceParam::instance().confIntersection ) {
      if(dimension() == 2)
        mesh()->buildEdges();
      else
        mesh()->buildFaces();
    }

    // ------------------------------------------------------------------------- //
    // Complementary pattern duplicated elements                                 //
    // ------------------------------------------------------------------------- //
    const auto& idInterElt = m_duplicateSupportElements->getIntersectedMshElt();
    for(auto ielIt = idInterElt.begin(); ielIt != idInterElt.end(); ++ielIt) {
      // get the geometric id of the element and the ids of its support elements
      currentIelGeo = ielIt->first;

      // loop over the unknowns of the problem 
      for (std::size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
        idVar1 = m_listUnknown.idVariable(iUnknown1);
        // get the support element of the current element
        m_supportDofUnknown[iUnknown1].getIdElementSupport(currentIelGeo, vecSupportCurrent);
        
        // loop over the support elements
        for(std::size_t ielSup = 0; ielSup < vecSupportCurrent.size(); ++ielSup) {
          ielSupport = vecSupportCurrent[ielSup];
          // for all support dof in this support element
          for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
            // for all component of the unknown
            for (std::size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); ++iComp) {
              // get the global id of the support dof
              dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);

              // If this support dof is owned by this process
              if(node1 >= beginIdDofLocal && node1 < endIdDofLocal) {
                const felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);

                // for all unknown in the linear problem
                for (std::size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                  idVar2 = m_listUnknown.idVariable(iUnknown2); 
                  // for all component of the second unknown
                  for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); ++jComp) { 
                    // If the two current components are connected
                    const felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                    if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                      // for all support element different than the ielSup
                      for(std::size_t jelSup=0; jelSup<vecSupportCurrent.size(); ++jelSup) {
                        if(jelSup != ielSup) {
                          jelSupport = vecSupportCurrent[jelSup];
                          // for all support dof in this support element
                          for (felInt jSup = 0; jSup < m_supportDofUnknown[iUnknown2].getNumSupportDof(jelSupport); ++jSup) {
                            // get the global id of the second support dof
                            dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                            if(node1 != node2)
                              nodesNeighborhood[node1-beginIdDofLocal].insert(node2);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    // ----------------------------------------------------- //
    // Complementary pattern for face-oriented stabilization //
    // Complementary pattern for ghost penalty stabilization //
    // ----------------------------------------------------- //
    if( FelisceParam::instance().NSStabType == 1 || FelisceParam::instance().useGhostPenalty ) {

      // first loop on element type
      currentIelGeo = 0;
      for (std::size_t i=0; i<mesh()->bagElementTypeDomain().size(); ++i) {
        eltType =  mesh()->bagElementTypeDomain()[i];
        const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
        numElement[eltType] = 0;
        numFacesPerElement = geoEle->numBdEle();
        // second loop on region of the mesh.
        for(GeometricMeshRegion::IntRefToBegEndIndex_type::const_iterator itRef = mesh()->intRefToBegEndMaps[eltType].begin(); itRef != mesh()->intRefToBegEndMaps[eltType].end(); itRef++) {
          numEltPerLabel = itRef->second.second;
          // third loop on element
          for (felInt iElm = 0; iElm < numEltPerLabel; ++iElm) {
        
            // loop over the boundaries of the element
            for(felInt iFace = 0; iFace < numFacesPerElement; ++iFace) {
              // check if this face is an inner face or a boundary
              oppositeIelGeo = currentIelGeo;
              eltTypeOpp = eltType;
              isAnInnerFace = mesh()->getAdjElement(eltTypeOpp, oppositeIelGeo, iFace);
              if(isAnInnerFace) {

                // loop on the unknown of the linear problem
                for (std::size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
                  idVar1 = m_listUnknown.idVariable(iUnknown1);
                  // get the support element of the current element
                  m_supportDofUnknown[iUnknown1].getIdElementSupport(currentIelGeo, vecSupportCurrent);

                  // get number of dof in support element
                  const std::size_t sizeCurrent = m_supportDofUnknown[0].getNumSupportDof(vecSupportCurrent[0]);
      
                  // loop over the support elements
                  for(std::size_t ielSup = 0; ielSup < vecSupportCurrent.size(); ++ielSup) {
                    ielSupport = vecSupportCurrent[ielSup];

                    // for all support dof in this support element
                    for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
                      // for all component of the unknown
                      for (std::size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); ++iComp) {
                        // get the global id of the support dof
                        dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);
                        
                        // if this node is on this process
                        if(node1 >= beginIdDofLocal && node1 < endIdDofLocal) {
                          const felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);

                          // for all unknown
                          for (std::size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                            idVar2 = m_listUnknown.idVariable(iUnknown2);                            
                            
                            // get the support element of the opposite element
                            m_supportDofUnknown[iUnknown2].getIdElementSupport(oppositeIelGeo, vecSupportOpposite);

                            // get number of dof in support element
                            const std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(vecSupportOpposite[0]);

                            for(std::size_t jelSup=0; jelSup < vecSupportOpposite.size(); ++jelSup) {
                              jelSupport = vecSupportOpposite[jelSup];
                              
                              // compute dof in common
                              std::size_t countSupportDofInCommon = 0;
                              for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                                for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2)
                                  if( iSupportDof[iEle[jelSupport] + iSup2] == iSupportDof[iEle[ielSupport] + iSup1] )
                                    ++countSupportDofInCommon;
                              }
                             
                              // if the two support elements share an edge/face
                              if( countSupportDofInCommon == sizeCurrent-1 ) {
                                // for all component of the second unknown
                                for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {
                                  // If the two current components are connected
                                  const felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                                  if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                                    // for all support dof in this support element
                                    for (felInt jSup = 0; jSup < m_supportDofUnknown[iUnknown2].getNumSupportDof(jelSupport); ++jSup) {
                                      // get the global id of the second support dof
                                      dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                                      if(node1 != node2)
                                        nodesNeighborhood[node1-beginIdDofLocal].insert(node2);
                                    }
                                  }
                                }
                              }
                            }                            
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            ++currentIelGeo;
            ++numElement[eltType];
          }
        }
      }
    }

    // ----------------------------------------------------- //
    // Complementary pattern for tip-DG faces                //
    // ----------------------------------------------------- //
    const felInt nbrTipElt = m_duplicateSupportElements->getNumTipElt();
    if(nbrTipElt > 0 && !FelisceParam::instance().confIntersection ) {

      // get set of tip elements
      const auto& idTipElt = m_duplicateSupportElements->getIntersectedTipElt();

      // copy of idTipElt in temporary structure
      std::unordered_set<felInt> idTipAndNeiElt(idTipElt.begin(),idTipElt.end());

      // get elttype and number of faces
      eltType = mesh()->bagElementTypeDomain()[0];
      numFacesPerElement = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

      // loop on tip elements
      for (auto it = idTipElt.begin(); it != idTipElt.end(); ++it) {
        currentIelGeo = *it;

        // looping on the faces of the element
        for(felInt iFace=0; iFace < numFacesPerElement; ++iFace) {
          // check if this face is an inner face or a boundary
          oppositeIelGeo = currentIelGeo;
          eltTypeOpp = eltType;
          isAnInnerFace = mesh()->getAdjElement(eltTypeOpp, oppositeIelGeo, iFace);

          if( isAnInnerFace )
            idTipAndNeiElt.insert(oppositeIelGeo); 
        }
      }

      // loop on tip elements and neighbors
      for (auto it = idTipAndNeiElt.begin(); it != idTipAndNeiElt.end(); ++it) {
        currentIelGeo = *it;

        bool isCurrentIelGeoATipElm = false;
        if ( idTipElt.find(currentIelGeo) != idTipElt.end() )
          isCurrentIelGeoATipElm = true;

        // looping on the faces of the element
        for(felInt iFace=0; iFace < numFacesPerElement; ++iFace) {
          // check if this face is an inner face or a boundary
          oppositeIelGeo = currentIelGeo;
          eltTypeOpp = eltType;
          isAnInnerFace = mesh()->getAdjElement(eltTypeOpp, oppositeIelGeo, iFace);

          // if boundary face go to next one
          if ( !isAnInnerFace )
            continue;

          // if current element is not a tip elt, check if the opposite is a tip elt
          bool isOppositeIelGeoATipElm = false;
          if ( !isCurrentIelGeoATipElm ) {
            if ( idTipElt.find(oppositeIelGeo) != idTipElt.end() )
              isOppositeIelGeoATipElm = true;
          }

          // if one of the two is a tip elt
          if( isCurrentIelGeoATipElm || isOppositeIelGeoATipElm ) {
            // loop on the unknown of the linear problem
            for (std::size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
              idVar1 = m_listUnknown.idVariable(iUnknown1);
              // get the support element of the current element
              m_supportDofUnknown[iUnknown1].getIdElementSupport(currentIelGeo, vecSupportCurrent);

              // loop over the support elements
              for(std::size_t ielSup = 0; ielSup < vecSupportCurrent.size(); ++ielSup) {
                ielSupport = vecSupportCurrent[ielSup];
      
                // for all support dof in this support element
                for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
                  // for all component of the unknown
                  for (std::size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); ++iComp) {
                    // get the global id of the support dof
                    dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);

                    // if this node is on this process
                    if(node1 >= beginIdDofLocal && node1 < endIdDofLocal) {
                      const felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);

                      // for all unknown
                      for (std::size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                        idVar2 = m_listUnknown.idVariable(iUnknown2);                            
                        // get the support element of the opposite element
                        m_supportDofUnknown[iUnknown2].getIdElementSupport(oppositeIelGeo, vecSupportOpposite);

                        for(std::size_t jelSup=0; jelSup < vecSupportOpposite.size(); ++jelSup) {
                          jelSupport = vecSupportOpposite[jelSup];

                          // for all component of the second unknown
                          for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {
                            // If the two current components are connected
                            const felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                            if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                              // for all support dof in this support element
                              for (felInt jSup = 0; jSup < m_supportDofUnknown[iUnknown2].getNumSupportDof(jelSupport); ++jSup) {
                                // get the global id of the second support dof
                                dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);
                                if(node1 != node2)
                                  nodesNeighborhood[node1-beginIdDofLocal].insert(node2);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    // ----------------------------------------------------------------- //
    // build the complementary pattern and merge it with the current one //
    // ----------------------------------------------------------------- //   
    std::size_t dofSize = 0;
    for(std::size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode)
      dofSize += nodesNeighborhood[iNode].size();
    
    // the complementary pattern
    if(dofSize > 0) {
      std::vector<felInt> iCSR(numDofProc+1, 0);
      std::vector<felInt> jCSR(dofSize     , 0);
      felInt pos;
      
      for(felInt iNode = 0; iNode < numDofProc; ++iNode) {
        iCSR[iNode + 1] = iCSR[iNode] + nodesNeighborhood[iNode].size();
        pos = 0;
        for(auto iCon = nodesNeighborhood[iNode].begin(); iCon != nodesNeighborhood[iNode].end(); ++iCon) {
          jCSR[iCSR[iNode] + pos] = *iCon;
          ++pos;
        }
      }

      // Now, call merge pattern
      dof().mergeGlobalPattern(iCSR, jCSR);
    }
  }

  void LinearProblemNitscheXFEM::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel1, felInt& iel2, ElementType& eltType, felInt& ielGeo, FlagMatrixRHS flagMatrixRHS) 
  {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;

    // get the side of the support element looking at iel2 
    // here iel2 is refering to the solution because it will set the column in the global matrix
    // (see the function setValueMatrixRHS in linearProblem.cpp)
    felInt ielSupportDof;
    m_supportDofUnknown[0].getIdElementSupport(eltType, ielGeo, ielSupportDof); // TODO here ielGeo is ID elt by type in the rest of the function is the global ID
    const sideOfInterface sideOfSupportElm = ielSupportDof == iel2 ? LEFT : RIGHT;
    const double side = sideOfSupportElm == LEFT ? 1. : -1.;

    if( m_skipSmallVolume ){
      m_totalVol = compAreaVolume(elemPoint);
      m_skippedVol = 0;
      m_totalArea = 0;
      m_skippedArea = 0;
      m_tol = m_eps * m_totalVol;
    }

    if( iel1 == iel2 ) {
      //////////////////////////////////
      // VOLUME ELEMENT - SUB ELEMENT //
      //////////////////////////////////
      felInt numSubElement = m_duplicateSupportElements->getNumSubMshEltPerMshElt(ielGeo);

      // Get the support element of the old solutions (id of duplicated/non-dupl old element in the old support dof)
      std::vector<felInt> ielOld(m_solutionOld.size());  
      std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
      for(std::size_t i = 0; i < oldSupportElement.size(); ++i) {
        m_supportDofUnknownOld[i][0].getIdElementSupport(ielGeo, oldSupportElement[i]);
      
        // set the id for the old solution
        if( oldSupportElement[i].size() > 1 ) {
          // this element is also duplicated for the old solution
          ielOld[i] = sideOfSupportElm == LEFT ? oldSupportElement[i][0] : oldSupportElement[i][1];
        } else {
          // this element is NOT duplicated for the old solution
          ielOld[i] = oldSupportElement[i][0];
        }
      }
      
      // This element has been duplicted in the actual configuration
      if( numSubElement > 0 ) {

        // Volume sub elements
        felInt subElemId;
        std::vector<double> tmpPt(3);

        // loop over the sub-elements
        for(felInt iSubElt = 0; iSubElt < numSubElement; ++iSubElt) {
          subElemId = m_duplicateSupportElements->getSubMshEltIdxMsh(ielGeo, iSubElt);

          // if the side of the sub-element is the same as the element
          if( m_duplicateSupportElements->getSubMshEltSide(ielGeo, iSubElt) == sideOfSupportElm ) {

            // fill the vector with the points of the sub-element
            for(std::size_t iPt = 0; iPt < m_numVerPerFluidElt; ++iPt) {
              m_duplicateSupportElements->getSubMshEltVerCrd(subElemId, iPt, tmpPt);
              *m_mshPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
            }

            if( m_skipSmallVolume ){ 
              const double area = compAreaVolume(m_mshPts);
              if( area <= m_tol ){
                m_skippedVol += std::abs(area);
                continue;
              }
            }

            // Update finite element
            m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);
            m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);

            // build classic NS matrix
            computeElementMatrixRHS();
            for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i)
              computeElementMatrixRHSPartDependingOnOldTimeStep(iel1, ielOld[i], i);
          }
        }
      } else {
        // No volume sub elements
        for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {

          if( oldSupportElement[i].size() > 1 ) {
            // old element is duplicated, get the sub elements
            const felInt numSubElementOld = m_duplicateSupportElements->getNumSubMshEltMshOld(ielGeo, i);
            
            felInt subElemIdOld, ielOldTmp;
            std::vector<double> tmpPtOld(3);
            
            for(felInt iSubElt = 0; iSubElt < numSubElementOld; ++iSubElt) {
              subElemIdOld = m_duplicateSupportElements->getSubMshEltIdxMshOld(ielGeo, iSubElt, i);
            
              // fill the vector with the points of the sub-element
              for(std::size_t iPt = 0; iPt < m_numVerPerFluidElt; ++iPt) {
                m_duplicateSupportElements->getSubMshEltVerCrdOld(subElemIdOld, iPt, tmpPtOld, i);
                *m_mshPts[iPt] = Point(tmpPtOld[0], tmpPtOld[1], tmpPtOld[2]);
              }

              if( m_skipSmallVolume ){
                const double area = compAreaVolume(m_mshPts);
                if(area <= m_tol){
                  m_skippedVol += std::abs(area);
                  continue;
                }
              }

              // Update finite element
              m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);
              m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_mshPts);
              
              // Get the id of the support element owning the sub element
              ielOldTmp = m_duplicateSupportElements->getSubMshEltSideOld(subElemIdOld, i) == LEFT ? oldSupportElement[i][0] : oldSupportElement[i][1];

              // build classic NS matrix
              computeElementMatrixRHSPartDependingOnOldTimeStep(iel1, ielOldTmp, i);
            }
          }
        }

        // Update finite element if needed (needed in case we just modified the fem due to integration over subDomains)
        if( !m_feVel->hasOriginalQuadPoint() || !m_fePres->hasOriginalQuadPoint() ) {
          m_feVel->updateBasisAndFirstDeriv(0, elemPoint);
          m_fePres->updateBasisAndFirstDeriv(0, elemPoint);
        } else {
          m_feVel->updateFirstDeriv(0, elemPoint);
          m_fePres->updateFirstDeriv(0, elemPoint);
        }
          
        // build classic NS matrix
        computeElementMatrixRHS();

        // build matrix and rhs depending on old time step if this element is not intersected by the old solutions
        for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {
          if( oldSupportElement[i].size() == 1 )
            computeElementMatrixRHSPartDependingOnOldTimeStep(iel1, ielOld[i], i);
        }
      }


      ////////////////////////////
      // SUB STRUCTURE ELEMENTS //
      ////////////////////////////
      numSubElement = m_duplicateSupportElements->getNumSubItfEltPerMshElt(ielGeo);
      if( numSubElement > 0 ) {
        felInt subElemId;
        felInt elemId;
        std::vector<double> tmpPt(3);
        std::vector<double> itfNormal(dimension(),0);
        std::vector<felInt> strucElemPtsId(m_numVerPerSolidElt);
        GeometricMeshRegion::ElementType eltTypeStruc = m_interfMesh->bagElementTypeDomain()[0]; 

        // new position for  penaltyParam
        m_feVel->updateMeas(0, elemPoint); 
        const double penaltyParam = m_viscosity * m_nitschePar / m_feVel->diameter();

        // for every interface sub-element
        for(felInt iSubElt = 0; iSubElt < numSubElement; ++iSubElt) {
          // get the id of the sub element
          subElemId = m_duplicateSupportElements->getSubItfEltIdxMsh(ielGeo, iSubElt);

          // get the id of the element owing this sub-element
          elemId = m_duplicateSupportElements->getItfEltIdxOfSubItfElt(subElemId);

          // skip this subElement if belongs to the fictitious interface (this is a tip)
          if( m_duplicateSupportElements->getItfEltRef(elemId) < 0 )
            continue;

          // fill the vector with the points of the sub-element
          for(std::size_t iPt = 0; iPt < m_numVerPerSolidElt; ++iPt) {
            m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
            *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
          }

          // get the coordinates of the element owning this sub-element
          m_interfMesh->getOneElement(eltTypeStruc, elemId, strucElemPtsId, m_itfPts, 0);  

          if( m_skipSmallVolume ){ 
            const double area = compAreaVolume(m_subItfPts);
            m_totalArea += area;
            if ( area <= m_eps*compAreaVolume(m_itfPts) ) {
              m_skippedArea += std::abs(area);
              continue;
            }
          }

          // update the finite element
          m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_curvFeStruc);
          m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_curvFeStruc);

          // Inflow stabilization for NS
          if( m_useNSEquation == 1 && m_useInterfaceStab ) {
            
            // get the normal to this sub element
            m_interfMesh->listElementNormal(eltTypeStruc, elemId).getCoor(itfNormal);
            m_elemFieldNormal.setValue(itfNormal);
            
            // compute (\beta \cdot n)_- u_i v_i
            m_elementMat[0]->f_dot_n_phi_i_phi_j(0.5 * side * m_density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
          }

          // compute the elementary matrix and rhs
          // update the value of the solid velocity
          updateStructureVel(elemId);
          updateSubStructureVel(m_itfPts, m_subItfPts);

          // Nitsche's penalty term, \gamma \mu / h u_i . v_i
          m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

          // Nitsche's consistency terms: - \sigma(u_i, p_i)n_i . v_i
          computeNitscheSigma_u_v(side * m_viscosity, -side);  

          // Nitsche's consistency terms, - \sigma(v_i, -q_i)n_i . u_i
          // BE CAREFUL: the equation for q is multiply by minus !!!!!
          computeNitscheSigma_v_u(0., -side);
          
          // Nische's penalty term, \gamma \mu / h \dot{d}^n v_i
          m_elementVector[0]->source(penaltyParam, *m_feVel, m_strucVelQuadPts, m_velBlock, m_velocity->numComponent());

          // ddot \sigma(v_i, -q_i) n_i . ddot //
          computeNitscheDpoint_Sigma_v(0., -side);
        }
      }
    } else {

      if( m_useNSEquation == 1 && m_useInterfaceStab ) {
        
        // Get the support element of the old solutions
        std::vector<felInt> ielOld(m_solutionOld.size());
        std::vector<felInt> ielOldDup(m_solutionOld.size());
        std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
        for(std::size_t i = 0; i < oldSupportElement.size(); ++i) {
          m_supportDofUnknownOld[i][0].getIdElementSupport(ielGeo, oldSupportElement[i]);
        
          // set the id for the old solution
          if( oldSupportElement[i].size() > 1 ) {
            // this element is also duplicated for the old solution
            if( sideOfSupportElm == LEFT ) {
              ielOld[i]    = oldSupportElement[i][0];
              ielOldDup[i] = oldSupportElement[i][1];
            } else {
              ielOld[i]    = oldSupportElement[i][1];
              ielOldDup[i] = oldSupportElement[i][0];
            }
          } else {
            // this element is NOT duplicated for the old solution
            ielOld[i]    = oldSupportElement[i][0];
            ielOldDup[i] = oldSupportElement[i][0];
          }
        }

        ////////////////////////////
        // SUB STRUCTURE ELEMENTS //
        ////////////////////////////
        const felInt numSubElement = m_duplicateSupportElements->getNumSubItfEltPerMshElt(ielGeo);
        if( numSubElement > 0 ) {
          felInt subElemId, elemId;
          std::vector<double> tmpPt(3);
          std::vector<double> itfNormal(dimension());
          std::vector<felInt> strucElemPtsId(m_numVerPerSolidElt);
          GeometricMeshRegion::ElementType eltTypeStruc = m_interfMesh->bagElementTypeDomain()[0]; 

          // set the extrapolation of velocity
          m_elemFieldAdv.val.clear();
          m_elemFieldAdvDup.val.clear();
          if( FelisceParam::instance().useProjectionForNitscheXFEM ) {
            for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel1, m_iVelocity, m_ao, dof());
              m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
              m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
            }
          } else {
            for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
              m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOldDup[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
              m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
            }
          }

          // for every sub-element
          for(felInt iSubElt = 0; iSubElt < numSubElement; ++iSubElt) {
            // get the id of the sub element
            subElemId = m_duplicateSupportElements->getSubItfEltIdxMsh(ielGeo, iSubElt);

            // get the id of the element owing this sub element
            elemId = m_duplicateSupportElements->getItfEltIdxOfSubItfElt(subElemId);

            // skip this subElement if belongs to the fictitious interface (this is a tip)
            if( m_duplicateSupportElements->getItfEltRef(elemId) < 0 )
              continue;

            // fill the vector with the points of the sub-element
            for(std::size_t iPt = 0; iPt < m_numVerPerSolidElt; ++iPt) {
              m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
              *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
            }     

            // get the coordinates of the element owning this sub-element
            m_interfMesh->getOneElement(eltTypeStruc, elemId, strucElemPtsId, m_itfPts, 0); 

            if( m_skipSmallVolume ){
              const double area = compAreaVolume(m_subItfPts);
              m_totalArea += area;
              if ( area <= m_eps*compAreaVolume(m_itfPts) ) {
                m_skippedArea += std::abs(area);
                continue;
              }
            }

            // get the normal to this sub element
            m_interfMesh->listElementNormal(eltTypeStruc, elemId).getCoor(itfNormal);
            m_elemFieldNormal.setValue(itfNormal);

            // update the finite element
            m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_curvFeStruc);
            m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_curvFeStruc);

            // compute Inflow stabilization for NS equation
            m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * m_density, *m_feVel, m_elemFieldAdv,    m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
            m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * m_density, *m_feVel, m_elemFieldAdvDup, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
          }
        }
      }
    }

    if( m_printSkipVolume ) {
      if ( std::abs(m_skippedVol) > m_eps * std::abs(m_totalVol) )
        std::cout << "In computeElementArray, iel1 = " << iel1 << " iel2 = " << iel2 << " Skipped volume is : " << std::fixed << std::setprecision(16) << m_skippedVol / m_totalVol * 100. << " %" << std::defaultfloat << " m_skippedVol = " << m_skippedVol << std::endl;
      
      if ( std::abs(m_skippedArea) > m_eps * std::abs(m_totalArea) )
        std::cout << "In computeElementArray, iel1 = " << iel1 << " iel2 = " << iel2 << " Skipped surface is : " << std::fixed << std::setprecision(16) << m_skippedArea / m_totalArea * 100. << " %" << std::defaultfloat << " m_skippedArea = " << m_skippedArea << std::endl;
    }
  }

  namespace
  {
    template<int DimT>
    void normal(std::vector<Point*>& pnt, std::vector<double>& N) {

      if ( DimT == 2 ){
        N[0] = pnt[0]->y() - pnt[1]->y();  
        N[1] = pnt[1]->x() - pnt[0]->x();

        const double invNorm = 1./ std::sqrt( N[0]*N[0] + N[1]*N[1]);

        N[0] *= invNorm;
        N[1] *= invNorm;
      }
      else if ( DimT == 3 ){

        MathUtilities::CrossProduct(N, (*pnt[1] - *pnt[0]).getCoor(), (*pnt[2] - *pnt[0]).getCoor());

        const double invNorm = 1./ std::sqrt( N[0]*N[0] + N[1]*N[1] + N[2]*N[2]);

        N[0] *= invNorm;
        N[1] *= invNorm;
        N[2] *= invNorm;
      }
      else{
        std::cout << "ERROR in normal()" << std::endl;
      }
    }
  }

  void LinearProblemNitscheXFEM::assembleMatrixFrontPoints() 
  {
    // This is to handle the DG terms for the elements containing the tip of the solid 
    // Assemble the matrix to take the crack point into account
    // Enforce continuity accros the fictitious interface
    const felInt nbrVerFro = m_duplicateSupportElements->getNumFrontPoints();

    if(nbrVerFro > 0) {
      std::vector<felInt> verFro;
      std::vector<felInt> verFro_temp; // generic front vertex vector for checking multi fronts within element
      felInt currentGlobalIelGeo, currentIelGeo;
      GeometricMeshRegion::ElementType eltType = mesh()->bagElementTypeDomain()[0];

      const felInt numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      std::vector<Point*>  elemPoint(numPointPerElt);
      std::vector<felInt>  elemIdPoint(numPointPerElt, 0);

      std::vector<felInt> vectorIdSupport;
      std::vector<double> itfNormal(dimension(), 0.);

      PetscInt naux;

      // Define finite element
      defineFiniteElement(eltType);

      // Element matrix and vector initialisation
      initElementArray();

      // allocate arrays for assembling the matrix
      FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;
      allocateArrayForAssembleMatrixRHS(flag);

      // init variables
      initPerElementType(eltType, flag);

      for(felInt iVer=0; iVer<nbrVerFro; ++iVer) {
        // get the info on the front point
        m_duplicateSupportElements->getFrontPointInfo(iVer, verFro);

        // get the fluid element in which the fictitious interface is
        currentGlobalIelGeo = verFro[1];
        ISGlobalToLocalMappingApply(mappingElem(), IS_GTOLM_MASK, 1, &currentGlobalIelGeo, &naux, &currentIelGeo);
        if ( currentIelGeo == -1 )
          continue;

        // get the support elements
        setElemPoint(eltType, currentIelGeo, elemPoint, elemIdPoint, vectorIdSupport);

        // fill the vector with the points of the fictitious interface
        *m_subItfPts[0] = Point(m_interfMesh->listPoint(verFro[0])); 
        *m_subItfPts[1] = Point(mesh()->listPoint(verFro[2]));  

        // now check if two fronts are in same element--then opposite vertex is other front)
        for(felInt iVer2=0; iVer2<nbrVerFro; ++iVer2) {
          if(iVer2!=iVer){
            m_duplicateSupportElements->getFrontPointInfo(iVer2, verFro_temp);
            if(verFro[1]==verFro_temp[1]){
              *m_subItfPts[1] = Point(m_interfMesh->listPoint(verFro[2]));  
            }
          }
        }
        
        // update the finite element
        m_feVel->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_curvFeStruc);
        m_fePres->updateSubElementFirstDeriv(0, elemPoint, m_subItfPts, m_curvFeStruc);

        // compute the normal
        normal<2>(m_subItfPts, itfNormal);

        // setting normal in element field
        m_elemFieldNormal.setValue(itfNormal);

        // parameters
        const double penaltyParam = m_viscosity * m_nitschePar / m_feVel->diameter();

        // loop over the support elements
        for(std::size_t iSup1=0; iSup1 < vectorIdSupport.size(); ++iSup1) {
          felInt iel1 = vectorIdSupport[iSup1];
          for(std::size_t iSup2=0; iSup2 < vectorIdSupport.size(); ++iSup2) {
            felInt iel2 = vectorIdSupport[iSup2];

            // clear elementary matrix
            for (std::size_t j=0; j<numberOfMatrices(); ++j)
              m_elementMat[j]->zero();
            
            // get the side
            const sideOfInterface sideOfSupportElm = iSup2 == 0 ? LEFT : RIGHT;
            const double side = sideOfSupportElm == LEFT ? 1. : -1.;

            // set the extrapolation of velocity
            if( m_useNSEquation == 1 && m_useInterfaceStab ) {
              // Get the support element of the old solutions
              std::vector<felInt> ielOld(m_solutionOld.size());
              std::vector<felInt> ielOldDup(m_solutionOld.size());
              std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
              for(std::size_t i=0; i<oldSupportElement.size(); ++i) {
                m_supportDofUnknownOld[i][0].getIdElementSupport(currentGlobalIelGeo, oldSupportElement[i]);
                
                // set the id for the old solution
                if(oldSupportElement[i].size() > 1) {
                  // this element is also duplicated for the old solution
                  if(sideOfSupportElm == LEFT) {
                    ielOld[i] = oldSupportElement[i][0];
                    ielOldDup[i] = oldSupportElement[i][1];
                  } else {
                    ielOld[i] = oldSupportElement[i][1];
                    ielOldDup[i] = oldSupportElement[i][0];
                  }
                } else {
                  // this element is NOT duplicated for the old solution
                  ielOld[i] = oldSupportElement[i][0];
                  ielOldDup[i] = oldSupportElement[i][0];
                }
              }
              
              m_elemFieldAdv.val.clear();
              m_elemFieldAdvDup.val.clear();
              if(FelisceParam::instance().useProjectionForNitscheXFEM) {
                for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel1, m_iVelocity, m_ao, dof());
                  m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
                  m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                }
              } else {
                for(felInt i=0; i<FelisceParam::instance().orderBdfNS; ++i) {
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                  m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOldDup[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                  m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                }
              }
            }

            // The normal is defined external to Omega_1. (Omega_1: right and Omega_2: left)
            // side = 1, on the left and -1 on the right. 
            // The unknown is telling us in which subdomain we are and therefore which normal we have to consider 
            if(iSup1 == iSup2) {

              // Nitsche's penalty term, \gamma \nu / h u_i . v_i
              m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

              // Nitsche's consistency terms, -\sigma(u_i, p_i)n_i . v_i - \sigma(v_i, q_i)n_i . u_i
              computeNitscheSigma_u_v(0.5 * side * m_viscosity, -0.5 * side);
              computeNitscheSigma_v_u(0.5 * side * m_viscosity, -0.5 * side);
              
              // stabilize inflow
              if(m_useNSEquation == 1 && m_useInterfaceStab ){
                // (0.5 \rho_f \beta_1 \cdot n_2) (u_1 \cdot v_1)-(0.5 \rho_f \beta_2 \cdot n_2) (u_2 \cdot v_2)
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.5 * side * m_density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
              }
            } else {
              m_elementMat[0]->phi_i_phi_j(-penaltyParam, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              
              computeNitscheSigma_u_v(-0.5 * side * m_viscosity,  0.5 * side);
              computeNitscheSigma_v_u( 0.5 * side * m_viscosity, -0.5 * side);

              // stabilize inflow
              if(m_useNSEquation == 1 && m_useInterfaceStab ) {
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * m_density, *m_feVel, m_elemFieldAdv,    m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * m_density, *m_feVel, m_elemFieldAdvDup, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
              }
            }
            
            // add values of elemMat in the global matrix: matrix(0).
            setValueMatrixRHS(vectorIdSupport[iSup1], vectorIdSupport[iSup2], flag);
          }
        }
      }

      // desallocate array use for assemble with petsc.
      desallocateArrayForAssembleMatrixRHS(flag);
    }
  }

  void LinearProblemNitscheXFEM::assembleMatrixTip() 
  {  
    // This is to handle the DG terms for the elements containing the tip edges/faces of the solid 
    // Enforce continuity accros the fictitious interface

    // get number of fluid tip elements
    const felInt nbrTipElm = m_duplicateSupportElements->getNumTipElt();
    if( nbrTipElm > 0 ) {

      // we assume fluid elements to be only triangles or tetrahedra
      GeometricMeshRegion::ElementType eltTypeFluid = meshLocal()->bagElementTypeDomain()[0];
      std::vector<Point*> fluidElmPoint(m_numVerPerFluidElt, nullptr);
      std::vector<felInt> fluidElmIdPoint(m_numVerPerFluidElt);
      // we assume structure elements to be only segments or triangles
      GeometricMeshRegion::ElementType eltTypeStruc = m_interfMesh->bagElementTypeDomain()[0];  
      std::vector<felInt> strucElmIdPoint(m_numVerPerSolidElt);

      felInt currentIelGeo, currentGlobalIelGeo, numSubElement;
      felInt elemId, subElemId;

      std::vector<felInt> vectorIdSupport;
      std::vector<double> itfNormal(dimension());
      std::vector<double> tmpPt(3);
      PetscInt naux;

      // define finite element
      defineFiniteElement(eltTypeFluid);

      // Element matrix and vector initialisation
      initElementArray();

      // allocate arrays for assembling the matrix
      allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);

      // init variables
      initPerElementType(eltTypeFluid, FlagMatrixRHS::only_matrix);

      // loop over fluid tip elements
      for(felInt iTipElm = 0; iTipElm < nbrTipElm; ++iTipElm) {
        // get fluid tip element global and local id, if element doesn't belong to current proc skip it
        currentGlobalIelGeo = m_duplicateSupportElements->getTipEltIdx(iTipElm);
        ISGlobalToLocalMappingApply(mappingElem(), IS_GTOLM_MASK, 1, &currentGlobalIelGeo, &naux, &currentIelGeo);
        if ( currentIelGeo == -1 )
          continue;

        if( m_skipSmallVolume ){
          m_totalArea = 0;
          m_skippedArea = 0;
        }

        // get the number of structure sub-element in the current fluid element
        numSubElement = m_duplicateSupportElements->getNumSubItfEltPerMshElt(currentGlobalIelGeo);
        for(felInt iSubElt = 0; iSubElt < numSubElement; ++iSubElt) {
          // get the id of the strucure sub-element
          subElemId = m_duplicateSupportElements->getSubItfEltIdxMsh(currentGlobalIelGeo, iSubElt);
          // get the id of the structure element owing this sub-element
          elemId = m_duplicateSupportElements->getItfEltIdxOfSubItfElt(subElemId); 
          // if elemId doesn't belong to fictitious structure skip it
          if( m_duplicateSupportElements->getItfEltRef(elemId) > 0 )
            continue;

          // get the normal to this sub element
          m_interfMesh->listElementNormal(eltTypeStruc, elemId).getCoor(itfNormal);

          // fill the vector with the points of the sub-element
          for(std::size_t iPt = 0; iPt < m_numVerPerSolidElt; ++iPt) {
            m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
            *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
          }

          // get the coordinates of the structure element owning this sub-element
          m_interfMesh->getOneElement(eltTypeStruc, elemId, strucElmIdPoint, m_itfPts, 0);   

          if( m_skipSmallVolume ){
            const double area = compAreaVolume(m_subItfPts);
            m_totalArea += area;
            if ( area <= m_eps*compAreaVolume(m_itfPts) ) {
              m_skippedArea += std::abs(area);
              continue;
            }
          }

          // get the support elements
          setElemPoint(eltTypeFluid, currentIelGeo, fluidElmPoint, fluidElmIdPoint, vectorIdSupport);
          
          // update the finite element
          m_feVel->updateSubElementFirstDeriv(0, fluidElmPoint, m_subItfPts, m_curvFeStruc);
          m_fePres->updateSubElementFirstDeriv(0, fluidElmPoint, m_subItfPts, m_curvFeStruc);

          m_elemFieldNormal.setValue(itfNormal);

          // parameters
          const double penaltyParam = m_viscosity * m_nitschePar / m_feVel->diameter();

          // loop over the support elements
          for(std::size_t iSup1 = 0; iSup1 < vectorIdSupport.size(); ++iSup1) {
            const felInt iel1 = vectorIdSupport[iSup1];
            for(std::size_t iSup2 = 0; iSup2 < vectorIdSupport.size(); ++iSup2) {
              const felInt iel2 = vectorIdSupport[iSup2];

              // clear elementary matrix
              for(std::size_t j = 0; j < numberOfMatrices(); ++j)
                m_elementMat[j]->zero();
              
              // get the side
              const sideOfInterface sideOfSupportElm = iSup2 == 0 ? LEFT : RIGHT;
              const double side = sideOfSupportElm == LEFT ? 1. : -1.;

              // set the extrapolation of velocity
              if( m_useNSEquation == 1 && m_useInterfaceStab ) {
                // Get the support element of the old solutions
                std::vector<felInt> ielOld(m_solutionOld.size());
                std::vector<felInt> ielOldDup(m_solutionOld.size());
                std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
                for(std::size_t i = 0; i < oldSupportElement.size(); ++i) {

                  m_supportDofUnknownOld[i][0].getIdElementSupport(currentGlobalIelGeo, oldSupportElement[i]);
                  
                  // set the id for the old solution
                  if( oldSupportElement[i].size() > 1 ) {
                    // this element is also duplicated for the old solution
                    if( sideOfSupportElm == LEFT ) {
                      ielOld[i] = oldSupportElement[i][0];
                      ielOldDup[i] = oldSupportElement[i][1];
                    } else {
                      ielOld[i] = oldSupportElement[i][1];
                      ielOldDup[i] = oldSupportElement[i][0];
                    }
                  } else {
                    // this element is NOT duplicated for the old solution
                    ielOld[i] = oldSupportElement[i][0];
                    ielOldDup[i] = oldSupportElement[i][0];
                  }
                }
                
                m_elemFieldAdv.val.clear();
                m_elemFieldAdvDup.val.clear();
                if( FelisceParam::instance().useProjectionForNitscheXFEM ) {
                  for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {
                    m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel1, m_iVelocity, m_ao, dof());
                    m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                    m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, iel2, m_iVelocity, m_ao, dof());
                    m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                  }
                } else {
                  for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {
                    m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                    m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                    m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOldDup[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                    m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                  }
                }
              }

              // The normal is defined external to Omega_1. (Omega_1: right and Omega_2: left)
              // side = 1, on the left and -1 on the right. 
              // The unknown is telling us in which subdomain we are and therefore which normal we have to consider               
              if( iSup1 == iSup2 ) {

                // Nitsche's penalty term, \gamma \mu / h u_i . v_i
                m_elementMat[0]->phi_i_phi_j(penaltyParam, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

                // Nitsche's consistency terms, -\sigma(u_i, p_i)n_i . v_i - \sigma(v_i, q_i)n_i . u_i
                computeNitscheSigma_u_v(0.5 * side * m_viscosity, -0.5 * side);
                computeNitscheSigma_v_u(0.5 * side * m_viscosity, -0.5 * side); 
                
                // stabilize inflow
                if( m_useNSEquation == 1 && m_useInterfaceStab ){
                  // (0.5 \rho_f \beta_1 \cdot n_2) (u_1 \cdot v_1)-(0.5 \rho_f \beta_2 \cdot n_2) (u_2 \cdot v_2)
                  m_elementMat[0]->f_dot_n_phi_i_phi_j(0.5 * side * m_density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
                }
              } else {
                m_elementMat[0]->phi_i_phi_j(-penaltyParam, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
                
                computeNitscheSigma_u_v(-0.5 * side * m_viscosity,  0.5 * side);
                computeNitscheSigma_v_u( 0.5 * side * m_viscosity, -0.5 * side);

                // stabilize inflow
                if( m_useNSEquation == 1 && m_useInterfaceStab ) {
                  m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * m_density, *m_feVel, m_elemFieldAdv,    m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
                  m_elementMat[0]->f_dot_n_phi_i_phi_j(0.25 * side * m_density, *m_feVel, m_elemFieldAdvDup, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
                }
              }

              // add values of elemMat in the global matrix: matrix(0).
              setValueMatrixRHS(vectorIdSupport[iSup1], vectorIdSupport[iSup2], FlagMatrixRHS::only_matrix);
            }
          }
        } // end for on the tip triangle

        if( m_printSkipVolume ) {
          if ( std::abs(m_skippedArea) > m_eps * std::abs(m_totalArea) )
            std::cout << "In assembleMatrixTip, Skipped surface is : " << std::fixed << std::setprecision(16) << m_skippedArea / m_totalArea * 100. << " %" << std::defaultfloat << std::endl;
        }
      } 

      // desallocate array use for assemble with petsc.
      desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
    }
  }

  void LinearProblemNitscheXFEM::assembleMatrixTipFaces() 
  {
    // ------------------------------------- //
    // assemble matrix for tip faces element //
    // ------------------------------------- //

    // get number of fluid tip elements
    const felInt nbrTipElm = m_duplicateSupportElements->getNumTipElt();
    if( nbrTipElm > 0 ) {

      // get set of tip elements
      const auto& idTipElt = m_duplicateSupportElements->getIntersectedTipElt();
      bool isOppTipElt;

      // get the types of elements
      GeometricMeshRegion::ElementType eltType = meshLocal()->bagElementTypeDomain()[0];
      GeometricMeshRegion::ElementType eltTypeOpp;
      const std::size_t numPointPerElt  = GeometricMeshRegion::m_numPointsPerElt[eltType];
      const std::size_t numFacesPerType = dimension() == 2 ? 
                                          GeometricMeshRegion::m_numEdgesPerElt[eltType] :
                                          GeometricMeshRegion::m_numFacesPerElt[eltType];

      // define finite element
      defineFiniteElement(eltType);

      // Element matrix and vector initialisation
      initElementArray();

      // allocate arrays for assembling the matrix
      allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);

      // init variables
      initPerElementType(eltType, FlagMatrixRHS::only_matrix);

      // element informations
      std::vector<felInt> ielCurrentSupportElt, ielOppositeSupportElt;
      std::vector<Point*> elemCurrentPoint(numPointPerElt),   elemOppositePoint(numPointPerElt);
      std::vector<felInt> elemCurrentIdPoint(numPointPerElt), elemOppositeIdPoint(numPointPerElt);
      std::vector<Point*> facPts(numPointPerElt-1);
      std::vector<double> faceNorm(dimension()), tmpPt(3);
      bool isAnInnerFace;
      felInt subFaceId;
      felInt currentIelGeo; 
      felInt currentGlobalIelGeo, oppositeGlobalIelGeo;
      felInt idCurrentSupElt, idOppositeSupElt;
      sideOfInterface sideOfSupportElement, sideOfSupportOpposite;
      PetscInt naux;

      // mapping from Felisce to Xfem for local ordering of faces or edges
      auto& mapFELtoXFM = m_duplicateSupportElements->getMapFaceFelisceToIntersector( dimension() );

      std::array< std::array<short int,3>, 4> mapFACtoVER;
      if( dimension() == 2 )
        mapFACtoVER = { {{0,1,-1},{1,2,-1},{2,0,-1},{-1,-1,-1}} }; // TODO D.C. map from element faces to vertices (should be moved in a class for every geometric element) 
      else
        mapFACtoVER = { {{0,1,2},{0,3,1},{1,3,2},{0,2,3}} };

      // finite element for the opposite element
      const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const felInt typeOfFiniteElement = m_velocity->finiteElementType();
      const RefElement *refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *meshLocal());
      CurrentFiniteElement oppositeEltFE(*refEle, *geoEle, m_velocity->degreeOfExactness());

      // loop over fluid tip elements
      for(felInt iTipElm = 0; iTipElm < nbrTipElm; ++iTipElm) {
        // get fluid tip element global and local id, if element doesn't belong to current proc skip it
        currentGlobalIelGeo = m_duplicateSupportElements->getTipEltIdx(iTipElm);
        ISGlobalToLocalMappingApply(mappingElem(), IS_GTOLM_MASK, 1, &currentGlobalIelGeo, &naux, &currentIelGeo);
        if ( currentIelGeo == -1 )
          continue;

        if( m_skipSmallVolume ){
          m_totalArea = 0;
          m_skippedArea = 0;
        }

        // get informations on the current element
        setElemPoint(eltType, currentIelGeo, elemCurrentPoint, elemCurrentIdPoint, ielCurrentSupportElt);

        m_feVel->updateMeas(0, elemCurrentPoint); 

        // compute penalty parameter
        const double penaltyParam = m_viscosity * m_nitschePar / m_feVel->diameter();

        // loop over all the edges or faces of the current support element
        for(std::size_t iFace = 0; iFace < numFacesPerType; ++iFace) {
          // get the neighboor. If it exists, this is an inner edge/face
          oppositeGlobalIelGeo = currentGlobalIelGeo;
          eltTypeOpp = eltType;
          isAnInnerFace = mesh()->getAdjElement(eltTypeOpp, oppositeGlobalIelGeo, iFace);
          if( isAnInnerFace ) {

            // get information on the opposite element
            m_supportDofUnknown[0].getIdElementSupport(oppositeGlobalIelGeo, ielOppositeSupportElt);

            mesh()->getOneElement(eltTypeOpp, oppositeGlobalIelGeo, elemOppositeIdPoint, elemOppositePoint, 0);

            if ( idTipElt.find(oppositeGlobalIelGeo) != idTipElt.end() )
              isOppTipElt = true;
            else
              isOppTipElt = false;

            // compute interior normal
            for (int iPt = 0; iPt < dimension(); ++iPt)
              facPts[iPt] = elemCurrentPoint[mapFACtoVER[iFace][iPt]];

            if( dimension() == 2 )
              normal<2>(facPts, faceNorm);
            else
              normal<3>(facPts, faceNorm);

            // setting the normal on the face
            m_elemFieldNormal.setValue(faceNorm);

            // get number of sub faces 
            const felInt nbrSubFaces = m_duplicateSupportElements->getNumSubFacPerFacTipElt(iTipElm, mapFELtoXFM[iFace]);

            // loop on subfaces
            for(felInt iSubFace = 0; iSubFace < nbrSubFaces; ++iSubFace){
              subFaceId = m_duplicateSupportElements->getSubTipFacIdxMsh(iTipElm, mapFELtoXFM[iFace], iSubFace);

              // get the sign of the subface
              sideOfSupportElement = m_duplicateSupportElements->getSgnSubFace(subFaceId);
              idCurrentSupElt = ielCurrentSupportElt[ sideOfSupportElement == RIGHT ? 1 : 0 ];

              // select opposite element support
              sideOfSupportOpposite = m_duplicateSupportElements->getSgnSubOppFace(subFaceId);
              idOppositeSupElt = ielOppositeSupportElt[ sideOfSupportOpposite == RIGHT ? 1 : 0 ];

              // fill the vector with the points of the sub-element
              for (int iPt = 0; iPt < dimension(); ++iPt) {
                m_duplicateSupportElements->getSubTipFacVerCrd(subFaceId, iPt, tmpPt);
                *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
              }

              if( m_skipSmallVolume ){
                const double area = compAreaVolume(m_subItfPts);
                m_totalArea += area;
                if ( area <= m_eps*compAreaVolume(facPts) ) {
                  m_skippedArea += std::abs(area);
                  continue;
                }
              }

              // update the weight and the test function at quadrature points given the subface
              m_feVel->updateSubElementFirstDeriv(0, elemCurrentPoint, m_subItfPts, m_curvFeStruc);

              // update the weight and the test function at quadrature points given the subface
              oppositeEltFE.updateSubElementFirstDeriv(0, elemOppositePoint, m_subItfPts, m_curvFeStruc);

              // set the extrapolation of velocity
              if( m_useNSEquation == 1 && m_useInterfaceStab ) {
                // Get the support element of the old solutions
                std::vector<felInt> ielOld(m_solutionOld.size());
                std::vector<felInt> ielOldOpp(m_solutionOld.size());
                std::vector< std::vector<felInt> > oldSupportElement(m_solutionOld.size());
                for(std::size_t i = 0; i < oldSupportElement.size(); ++i) {

                  // setting z_1
                  m_supportDofUnknownOld[i][0].getIdElementSupport(currentGlobalIelGeo, oldSupportElement[i]);
                  
                  // set the id for the old solution
                  if( oldSupportElement[i].size() > 1 ) {
                    // this element is also duplicated for the old solution
                    if( sideOfSupportElement == LEFT ) {
                      ielOld[i] = oldSupportElement[i][0];
                    } else {
                      ielOld[i] = oldSupportElement[i][1];
                    }
                  } else {
                    // this element is NOT duplicated for the old solution
                    ielOld[i] = oldSupportElement[i][0];
                  }

                  // setting z_2
                  m_supportDofUnknownOld[i][0].getIdElementSupport(oppositeGlobalIelGeo, oldSupportElement[i]);
                  
                  // set the id for the old solution
                  if( oldSupportElement[i].size() > 1 ) {
                    // this element is also duplicated for the old solution
                    if( sideOfSupportOpposite == LEFT ) {
                      ielOldOpp[i] = oldSupportElement[i][0];
                    } else {
                      ielOldOpp[i] = oldSupportElement[i][1];
                    }
                  } else {
                    // this element is NOT duplicated for the old solution
                    ielOldOpp[i] = oldSupportElement[i][0];
                  }
                }

                m_elemFieldAdv.val.clear();
                m_elemFieldAdvDup.val.clear();
                for(felInt i = 0; i < FelisceParam::instance().orderBdfNS; ++i) {
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOld[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                  m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
                  m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[i], *m_feVel, ielOldOpp[i], m_iVelocity, m_aoOld[i], m_dofOld[i]);
                  m_elemFieldAdvDup.val += m_elemFieldAdvTmp.val;
                }
              }

              // -------------------------------------------------------- //
              // current element for phi_i and psi_j                      //
              // -------------------------------------------------------- //
              m_elementMat[0]->zero();

              // Nitsche's penalty term, \gamma \mu / h u . v
              m_elementMat[0]->psi_j_phi_i(penaltyParam, *m_feVel, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

              //Nitsche's consistency terms, -\sigma(u , p) n . v
              if( m_useSymmetricStress )
                m_elementMat[0]->eps_psi_j_dot_vec_phi_i(m_viscosity, m_elemFieldNormal, *m_feVel, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->grad_psi_j_dot_vec_phi_i(0.5 * m_viscosity, m_elemFieldNormal, *m_feVel, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and the velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(-0.5 * m_elemFieldNormal.val(idim,0), *m_feVel, *m_feVel, m_velBlock+idim, m_preBlock, m_pressure->numComponent());

              // Nitsche's symmetry terms, - u . \sigma(v , - q )n,
              // !! the equation for q is multiplied by - !!
              if( m_useSymmetricStress )
                m_elementMat[0]->psi_j_eps_phi_i_dot_vec(m_viscosity, m_elemFieldNormal, *m_feVel, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->psi_j_grad_phi_i_dot_vec(0.5 * m_viscosity, m_elemFieldNormal, *m_feVel, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(-0.5 * m_elemFieldNormal.val(idim,0), *m_feVel, *m_feVel, m_preBlock, m_velBlock+idim, m_pressure->numComponent());

              // stabilize inflow (0.5 \rho_f \beta_1 \cdot n) (u_1 \cdot v_1)
              if( m_useNSEquation == 1 && m_useInterfaceStab )
                m_elementMat[0]->f_dot_n_phi_i_phi_j(0.5 * m_density, *m_feVel, m_elemFieldAdv, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);

              if ( isOppTipElt )
                *m_elementMat[0] *= 0.5;

              setValueMatrixRHS(idCurrentSupElt, idCurrentSupElt, FlagMatrixRHS::only_matrix);


              // -------------------------------------------------------- //
              // current element for phi_i and opposite element for psi_j //
              // -------------------------------------------------------- //   
              m_elementMat[0]->zero();
              
              // Nitsche's penalty term, \gamma \mu / h u . v
              m_elementMat[0]->psi_j_phi_i(-penaltyParam, *m_feVel, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              
              // Nitsche's consistency terms, -\sigma(u , p) n . v
              if( m_useSymmetricStress )
                m_elementMat[0]->eps_psi_j_dot_vec_phi_i(m_viscosity, m_elemFieldNormal, *m_feVel, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->grad_psi_j_dot_vec_phi_i(0.5 * m_viscosity, m_elemFieldNormal, *m_feVel, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and the velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(-0.5 * m_elemFieldNormal.val(idim,0), *m_feVel, oppositeEltFE, m_velBlock+idim, m_preBlock, m_pressure->numComponent());
              
              // Nitsche's symmetry terms, - u . \sigma(v , - q )n,
              // !! the equation for q is multiplied by - !!
              if( m_useSymmetricStress )
                m_elementMat[0]->psi_j_eps_phi_i_dot_vec(-m_viscosity, m_elemFieldNormal, *m_feVel, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->psi_j_grad_phi_i_dot_vec(-0.5 * m_viscosity, m_elemFieldNormal, *m_feVel, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(0.5 * m_elemFieldNormal.val(idim,0), *m_feVel, oppositeEltFE, m_preBlock, m_velBlock+idim, m_pressure->numComponent());
                
              // stabilize inflow    +(0.25 \rho_f \beta_1 \cdot n) (u_2 \cdot v_1) + (0.25 \rho_f \beta_2 \cdot n) (u_2 \cdot v_1)
              if( m_useNSEquation == 1 && m_useInterfaceStab ) {
                m_elementMat[0]->f_dot_n_psi_j_phi_i(-0.25 * m_density, *m_feVel, oppositeEltFE, m_elemFieldAdv,    m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
                m_elementMat[0]->f_dot_n_psi_j_phi_i(-0.25 * m_density, *m_feVel, oppositeEltFE, m_elemFieldAdvDup, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
              }

              if ( isOppTipElt )
                *m_elementMat[0] *= 0.5;

              setValueMatrixRHS(idCurrentSupElt, idOppositeSupElt, FlagMatrixRHS::only_matrix);


              // -------------------------------------------------------- //
              // opposite element for phi_i and current element for psi_j //
              // -------------------------------------------------------- //                  
              m_elementMat[0]->zero();
              
              // Nitsche's penalty term, \gamma \mu / h u . v
              m_elementMat[0]->psi_j_phi_i(-penaltyParam, oppositeEltFE, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              
              // Nitsche's consistency terms, -\sigma(u , p) n . v
              if( m_useSymmetricStress )
                m_elementMat[0]->eps_psi_j_dot_vec_phi_i(-m_viscosity, m_elemFieldNormal, oppositeEltFE, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->grad_psi_j_dot_vec_phi_i(-0.5 * m_viscosity, m_elemFieldNormal, oppositeEltFE, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and the velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(0.5 * m_elemFieldNormal.val(idim,0), oppositeEltFE, *m_feVel, m_velBlock+idim, m_preBlock, m_pressure->numComponent());
              
              // Nitsche's symmetry terms, - u . \sigma(v , - q )n,
              // !! the equation for q is multiplied by - !!
              if( m_useSymmetricStress )
                m_elementMat[0]->psi_j_eps_phi_i_dot_vec(m_viscosity, m_elemFieldNormal, oppositeEltFE, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->psi_j_grad_phi_i_dot_vec(0.5 * m_viscosity, m_elemFieldNormal, oppositeEltFE, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(-0.5 * m_elemFieldNormal.val(idim,0), oppositeEltFE, *m_feVel, m_preBlock, m_velBlock+idim, m_pressure->numComponent());

              // stabilize inflow.   - (0.25 \rho_f \beta_1 \cdot n) (u_1 \cdot v_2) - (0.25 \rho_f \beta_2 \cdot n) (u_1 \cdot v_2)
              if( m_useNSEquation == 1 && m_useInterfaceStab ) {
                m_elementMat[0]->f_dot_n_psi_j_phi_i(0.25 * m_density, oppositeEltFE, *m_feVel, m_elemFieldAdv,    m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
                m_elementMat[0]->f_dot_n_psi_j_phi_i(0.25 * m_density, oppositeEltFE, *m_feVel, m_elemFieldAdvDup, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);
              }

              if ( isOppTipElt )
                *m_elementMat[0] *= 0.5;

              setValueMatrixRHS(idOppositeSupElt, idCurrentSupElt, FlagMatrixRHS::only_matrix);


              // -------------------------------------------------------- //
              // opposite element for phi_i and psi_j                     //
              // -------------------------------------------------------- //
              m_elementMat[0]->zero();
              
              // Nitsche's penalty term, \gamma \mu / h u . v
              m_elementMat[0]->psi_j_phi_i(penaltyParam, oppositeEltFE, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              
              // Nitsche's consistency terms, -\sigma(u , p) n . v
              if( m_useSymmetricStress )
                m_elementMat[0]->eps_psi_j_dot_vec_phi_i(-m_viscosity, m_elemFieldNormal, oppositeEltFE, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->grad_psi_j_dot_vec_phi_i(-0.5 * m_viscosity, m_elemFieldNormal, oppositeEltFE, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and the velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(0.5 * m_elemFieldNormal.val(idim,0), oppositeEltFE, oppositeEltFE, m_velBlock+idim, m_preBlock, m_pressure->numComponent());
              
              // Nitsche's symmetry terms, - u . \sigma(v , - q )n,
              // !! the equation for q is multiplied by - !!
              if( m_useSymmetricStress )
                m_elementMat[0]->psi_j_eps_phi_i_dot_vec(-m_viscosity, m_elemFieldNormal, oppositeEltFE, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              else
                m_elementMat[0]->psi_j_grad_phi_i_dot_vec(-0.5 * m_viscosity, m_elemFieldNormal, oppositeEltFE, oppositeEltFE, m_velBlock, m_velBlock, m_velocity->numComponent());
              // same finite element for the pressure and velocity
              for(std::size_t idim = 0; idim < m_velocity->numComponent(); ++idim)
                m_elementMat[0]->psi_j_phi_i(0.5 * m_elemFieldNormal.val(idim,0), oppositeEltFE, oppositeEltFE, m_preBlock, m_velBlock+idim, m_pressure->numComponent());
              
              // stabilize inflow    + (0.5 \rho_f \beta_2 \cdot n) (u_2 \cdot v_2)
              if( m_useNSEquation == 1 && m_useInterfaceStab )
                m_elementMat[0]->f_dot_n_phi_i_phi_j(-0.5 * m_density, oppositeEltFE, m_elemFieldAdvDup, m_elemFieldNormal, m_velBlock, m_velBlock, m_velocity->numComponent(), 0);

              if ( isOppTipElt )
                *m_elementMat[0] *= 0.5;

              setValueMatrixRHS(idOppositeSupElt, idOppositeSupElt, FlagMatrixRHS::only_matrix);
            }
          }
        }

        if( m_printSkipVolume ) {
          if ( std::abs(m_skippedArea) > m_eps * std::abs(m_totalArea) )
            std::cout << "In assembleMatrixTipFaces, Skipped surface is : " << std::fixed << std::setprecision(16) << m_skippedArea / m_totalArea * 100. << " %" << std::defaultfloat << std::endl;
        }        
      }

      // desallocate array use for assemble with petsc.
      desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
    }
  }

  void LinearProblemNitscheXFEM::assembleMatrixGhostPenalty() 
  {
    // ------------- //
    // Ghost penalty //
    // ------------- //
    // get the types of elements
    GeometricMeshRegion::ElementType eltTypeOpp, eltType = meshLocal()->bagElementTypeDomain()[0]; // list of domain element types in the mesh
    const felInt numFacePerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numFace(); //if we are in 3d we integrate on the faces
    const felInt numEdgePerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numEdge(); //if we are in 2d we integrate on the edges
    std::vector<felInt> idOfFacesCurrent( dimension() == 2 ? numEdgePerType : numFacePerType, -1);
    std::vector<felInt> idOfFacesOpposite(dimension() == 2 ? numEdgePerType : numFacePerType, -1); 

    // define finite element
    defineFiniteElement(eltType);
    defineCurrentFiniteElementWithBd(eltType);

    // Element matrix and vector initialisation
    initElementArray();

    // allocate arrays for assembling the matrix
    allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);

    // init variables
    initPerElementType(eltType, FlagMatrixRHS::only_matrix);
    m_feVelWithBd = m_listCurrentFiniteElementWithBd[m_iVelocity];

    // get global supportDofMesh informations
    std::vector<felInt>& iEle        = m_supportDofUnknown[0].iEle();
    std::vector<felInt>& iSupportDof = m_supportDofUnknown[0].iSupportDof();

    // element informations
    bool isAnInnerFace;
    std::vector<felInt> ielCurrentSupportElt, ielOppositeSupportElt;
    std::vector<Point*> elemCurrentPoint(m_numVerPerFluidElt),   elemOppositePoint(m_numVerPerFluidElt);
    std::vector<felInt> elemCurrentIdPoint(m_numVerPerFluidElt), elemOppositeIdPoint(m_numVerPerFluidElt);
    felInt currentIelGeo;
    felInt currentGlobalIelGeo, oppositeGlobalIelGeo;
    felInt idCurrentSupElt, idOppositeSupElt;
    PetscInt naux;

    // finite element with boundary for the opposite element
    const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
    const felInt typeOfFiniteElement = m_velocity->finiteElementType();
    const RefElement *refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *meshLocal());
    CurrentFiniteElementWithBd oppositeEltFEWithBd(*refEle, *geoEle, m_velocity->degreeOfExactness(), m_velocity->degreeOfExactness());

    // loop over all the intersected elements
    const auto& idInterElt = m_duplicateSupportElements->getIntersectedMshElt();
    for(auto ielIt = idInterElt.begin(); ielIt != idInterElt.end() && ielIt->first < mesh()->numElements(eltType); ++ielIt) {
      // get the geometric id of the element and the ids of its support elements
      currentGlobalIelGeo = ielIt->first;
      ISGlobalToLocalMappingApply(mappingElem(), IS_GTOLM_MASK, 1, &currentGlobalIelGeo, &naux, &currentIelGeo);
      
      // jump to the next intersected element if this element doesn't belong to this processor
      if( currentIelGeo == -1)
          continue; 
        
      // get all the faces of the element
      if( dimension() == 2 )
        mesh()->getAllEdgeOfElement(currentGlobalIelGeo, idOfFacesCurrent);
      else
        mesh()->getAllFaceOfElement(currentGlobalIelGeo, idOfFacesCurrent);

      // get informations on the current element
      setElemPoint(eltType, currentIelGeo, elemCurrentPoint, elemCurrentIdPoint, ielCurrentSupportElt);

      // loop over all the edges or faces of the current support element
      for(std::size_t iFace = 0; iFace < idOfFacesCurrent.size(); ++iFace) {

        // get the neighboor. If it exists, this is an inner edge
        oppositeGlobalIelGeo = currentGlobalIelGeo;
        eltTypeOpp = eltType;
        isAnInnerFace = mesh()->getAdjElement(eltTypeOpp, oppositeGlobalIelGeo, iFace);
        
        if( isAnInnerFace ) {
          
          // get information on the opposite element
          m_supportDofUnknown[0].getIdElementSupport(oppositeGlobalIelGeo, ielOppositeSupportElt);
          
          mesh()->getOneElement(eltTypeOpp, oppositeGlobalIelGeo, elemOppositeIdPoint, elemOppositePoint, 0);

          // get number of dof in support element
          const std::size_t sizeCurrent  = m_supportDofUnknown[0].getNumSupportDof(ielCurrentSupportElt[0]);
          const std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(ielOppositeSupportElt[0]);

          // for each support elements
          for(std::size_t iSup = 0; iSup < ielCurrentSupportElt.size(); ++iSup) {

            // get current support element
            idCurrentSupElt = ielCurrentSupportElt[iSup];

            // the other element is also duplicated, take the one on the same side
            if( ielOppositeSupportElt.size() > 1 )
              idOppositeSupElt = ielOppositeSupportElt[iSup];
            else
              idOppositeSupElt = ielOppositeSupportElt[0];

            // compute dof in common
            std::size_t countSupportDofInCommon = 0;
            for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
              for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2)
                if( iSupportDof[iEle[idOppositeSupElt] + iSup2] == iSupportDof[iEle[idCurrentSupElt] + iSup1] )
                  ++countSupportDofInCommon;
            }

            // if the two support elements share an edge/face
            bool computeGhostPenalty = false;
            if( countSupportDofInCommon == sizeCurrent-1)
              computeGhostPenalty = true;


            // add the contribution of this edge/face
            if( computeGhostPenalty ) {


              // if opposite element is not duplicated compute stabilization both side
              bool computeBothSide = false;
              if( ielOppositeSupportElt.size() > 1 )
                computeBothSide = true;

              // update the finite elements
              m_feVelWithBd->updateFirstDeriv(0, elemCurrentPoint);
              m_feVelWithBd->updateBdMeasNormal();
              oppositeEltFEWithBd.updateFirstDeriv(0, elemOppositePoint);
              oppositeEltFEWithBd.updateBdMeasNormal();

              // find the local id of the edge in the opposite element
              if( dimension() == 2 )
                mesh()->getAllEdgeOfElement(eltType, oppositeGlobalIelGeo, idOfFacesOpposite);
              else
                mesh()->getAllFaceOfElement(eltType, oppositeGlobalIelGeo, idOfFacesOpposite);
              
              felInt localIdFaceOpposite = -1;
              for(std::size_t jface = 0; jface < idOfFacesOpposite.size(); ++jface) {
                if( idOfFacesCurrent[iFace] == idOfFacesOpposite[jface] ) {
                  localIdFaceOpposite = jface;
                }
              }

              // set coefficient of the ghost penalty
              const double gammag = FelisceParam::instance().coefGhostPenalty * m_viscosity * m_feVelWithBd->bdEle(iFace).diameter();

              // ----------------------------------- //
              // current element for phi_i and phi_j //
              // ----------------------------------- //
              m_elementMat[0]->zero();
              m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, *m_feVelWithBd, *m_feVelWithBd, iFace, iFace, m_velBlock, m_velBlock, m_velocity->numComponent());
              setValueMatrixRHS(idCurrentSupElt, idCurrentSupElt, FlagMatrixRHS::only_matrix);

              // -------------------------------------------------------- //
              // current element for phi_i and opposite element for phi_j //
              // -------------------------------------------------------- //
              m_elementMat[0]->zero();
              m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, oppositeEltFEWithBd, *m_feVelWithBd, localIdFaceOpposite, iFace, m_velBlock, m_velBlock, m_velocity->numComponent());
              setValueMatrixRHS(idCurrentSupElt, idOppositeSupElt, FlagMatrixRHS::only_matrix);

              // if the opposite element is not duplicated, it will not be handle in this loop
              // compute all the term in this case
              if( computeBothSide ) {
                // -------------------------------------------------------- //
                // opposite element for phi_i and current element for phi_j //
                // -------------------------------------------------------- //
                m_elementMat[0]->zero();
                m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, *m_feVelWithBd, oppositeEltFEWithBd, iFace, localIdFaceOpposite, m_velBlock, m_velBlock, m_velocity->numComponent());
                setValueMatrixRHS(idOppositeSupElt, idCurrentSupElt, FlagMatrixRHS::only_matrix);

                // ------------------------------------ //
                // opposite element for phi_i and phi_j //
                // ------------------------------------ //
                m_elementMat[0]->zero();
                m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammag, oppositeEltFEWithBd, oppositeEltFEWithBd, localIdFaceOpposite, localIdFaceOpposite, m_velBlock, m_velBlock, m_velocity->numComponent());
                setValueMatrixRHS(idOppositeSupElt, idOppositeSupElt, FlagMatrixRHS::only_matrix);
              }
            }
          }
        }
      }
    }

    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
  }

  void LinearProblemNitscheXFEM::assembleMatrixFaceOriented() 
  {
    // Use Face-oriented stabilization
    // E. Burman, M. A. Fernandez and P. Hansbo, Continuous interior penalty finite element method for
    // Oseen's equations, 2006.
    std::pair<bool, GeometricMeshRegion::ElementType> adjElt;

    GeometricMeshRegion::ElementType eltType;       // Type of element
    GeometricMeshRegion::ElementType eltTypeOpp;    // Type of element

    bool computeFaceOriented;                       // true if we need to compute the stabilization on this face
    
    felInt ielCurrentLocalGeo = 0;                  // local geometric id of the current element
    felInt ielCurrentGlobalGeo = 0;                 // global geometric id of the current element
    felInt ielOppositeGlobalGeo = 0;                // global geometric id of the opposite element
    felInt numFacesPerType;                         // number of faces of an element of a given type
    felInt numEltPerLabel;                          // number of element by reference
    felInt numPointPerElt;                          // number of vertices by element
    felInt currentSupport;                          // id of the current support element
    felInt oppositeSupport;                         // id of the opposite support element
    std::vector<felInt> ielCurrentGlobalSup;        // local support element id of the current element
    std::vector<felInt> ielOppositeGlobalSup;       // local support element id of the opposite element

    std::vector<felInt> idOfFacesCurrent;           // ids of the current element edges
    std::vector<felInt> idOfFacesOpposite;          // ids of the opposite element edges
    std::vector<felInt> currentElemIdPoint;         // ids of the vertices of the current element
    std::vector<felInt> oppositeElemIdPoint;        // ids of the vertices of the opposite element 
    std::vector<Point*> currentElemPoint;           // point coordinates of the current element vertices
    std::vector<Point*> oppositeElemPoint;          // point coordinates of the opposite element vertices
    std::vector<felInt> countElement;               // count the element by type

    ElementField elemFieldAdvFace;
    ElementField elemFieldAdvFaceTmp;

    const std::vector<felInt>& iSupportDof = m_supportDofUnknown[0].iSupportDof();
    const std::vector<felInt>& iEle = m_supportDofUnknown[0].iEle();

    // resize vector
    countElement.resize(GeometricMeshRegion::m_numTypesOfElement, 0);

    // volume bag
    const std::vector<ElementType>& bagElementTypeDomain = meshLocal()->bagElementTypeDomain();

    // finite element with bd for the opposite element
    std::map<GeometricMeshRegion::ElementType, CurrentFiniteElementWithBd*> oppositeEltFEWithBdPre;
    std::map<GeometricMeshRegion::ElementType, CurrentFiniteElementWithBd*> oppositeEltFEWithBdVel;

    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      eltType = bagElementTypeDomain[i];
      const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;

      felInt typeOfFEVel = m_velocity->finiteElementType();
      const RefElement *refEleVel = geoEle->defineFiniteEle(eltType, typeOfFEVel, *mesh());
      oppositeEltFEWithBdVel[eltType] = new CurrentFiniteElementWithBd(*refEleVel, *geoEle, m_velocity->degreeOfExactness(), m_velocity->degreeOfExactness());

      felInt typeOfFEPre = m_pressure->finiteElementType();
      const RefElement *refElePre = geoEle->defineFiniteEle(eltType, typeOfFEPre, *mesh());
      oppositeEltFEWithBdPre[eltType] = new CurrentFiniteElementWithBd(*refElePre, *geoEle, m_pressure->degreeOfExactness(), m_pressure->degreeOfExactness());
    }

    // First loop on geometric type
    for (std::size_t itype = 0; itype < bagElementTypeDomain.size(); ++itype) {
      // get element type and number of vertices and "faces"
      eltType = bagElementTypeDomain[itype];
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      numFacesPerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

      // resize of vectors
      idOfFacesCurrent.resize(numFacesPerType, -1);
      idOfFacesOpposite.resize(numFacesPerType, -1);
      currentElemIdPoint.resize(numPointPerElt, -1);
      oppositeElemIdPoint.resize(numPointPerElt, -1);
      currentElemPoint.resize(numPointPerElt, nullptr);
      oppositeElemPoint.resize(numPointPerElt, nullptr);

      // define finite element
      defineFiniteElement(eltType);
      defineCurrentFiniteElementWithBd(eltType);

      // Element matrix and vector initialisation
      initElementArray();

      // allocate arrays for assembling the matrix
      allocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);

      // init variables
      initPerElementType(eltType, FlagMatrixRHS::only_matrix);
      m_feVelWithBd = m_listCurrentFiniteElementWithBd[m_iVelocity];
      m_fePreWithBd = m_listCurrentFiniteElementWithBd[m_iPressure];

      // second loop on region of the mesh
      for(auto itRef = meshLocal()->intRefToBegEndMaps[eltType].begin(); itRef != meshLocal()->intRefToBegEndMaps[eltType].end(); itRef++) {
        // get the number of element with this reference
        numEltPerLabel = itRef->second.second;

        // set the current label of LinearProblem to the current one of this loop
        initPerDomain(itRef->first, FlagMatrixRHS::only_matrix);

        // third loop on element
        for(felInt iel = 0; iel < numEltPerLabel; ++iel) {
          // get the global id of the current geometric element
          ISLocalToGlobalMappingApply(mappingElem(), 1, &ielCurrentLocalGeo, &ielCurrentGlobalGeo);

          // get all the faces of the element
          if( dimension() == 2 )
            mesh()->getAllEdgeOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);
          else
            mesh()->getAllFaceOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);

          // get informations on the current element
          setElemPoint(eltType, countElement[eltType], currentElemPoint, currentElemIdPoint, ielCurrentGlobalSup);

          // update the finite elements
          m_feVelWithBd->updateFirstDeriv(0, currentElemPoint);
          m_feVelWithBd->updateBdMeasNormal();
          m_fePreWithBd->updateFirstDeriv(0, currentElemPoint);
          m_fePreWithBd->updateBdMeasNormal();

          // loop over the support element of the current geometric element
          for(std::size_t ielSup = 0; ielSup < ielCurrentGlobalSup.size(); ++ielSup) {
            currentSupport = ielCurrentGlobalSup[ielSup];

            // get the old support dof
            std::vector< std::vector<felInt> > oldSupportElement(FelisceParam::instance().orderBdfNS);
            std::vector<felInt> ielOld(FelisceParam::instance().orderBdfNS);
            
            for(felInt iextrap = 0; iextrap < FelisceParam::instance().orderBdfNS; ++iextrap) {
              // get the old support element
              m_supportDofUnknownOld[iextrap][0].getIdElementSupport(ielCurrentGlobalGeo, oldSupportElement[iextrap]);
              
              // set the id for the old solution
              if( oldSupportElement[iextrap].size() > 1 )
                ielOld[iextrap] = oldSupportElement[iextrap][ielSup];
              else
                ielOld[iextrap] = oldSupportElement[iextrap][0];
            }

            // compute coefficients
            m_elemFieldAdv.val.clear();
            if( FelisceParam::instance().useProjectionForNitscheXFEM ) {
              for(felInt iextrap = 0; iextrap < FelisceParam::instance().orderBdfNS; ++iextrap) {
                m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVel, currentSupport, m_iVelocity, m_ao, dof());
                m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              }
            } else {
              for(felInt iextrap = 0; iextrap < FelisceParam::instance().orderBdfNS; ++iextrap) {
                m_elemFieldAdvTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVel, ielOld[iextrap], m_iVelocity, m_aoOld[iextrap], m_dofOld[iextrap]);
                m_elemFieldAdv.val += m_elemFieldAdvTmp.val;
              }
            }
          
            double normVelExtrap = 0;
            for(std::size_t jcol = 0; jcol < m_elemFieldAdv.val.size2(); ++jcol)
              normVelExtrap = std::max(normVelExtrap, norm_2(column(m_elemFieldAdv.val, jcol)));
            
            const double hK = m_feVelWithBd->diameter();
            const double ReK = m_density * normVelExtrap * hK / m_viscosity;
            const double coeffVel = FelisceParam::instance().stabFOVel * std::min(1., ReK) * hK * hK;

            double gammaP = 0.;
            if( m_useNSEquation == 0 ) {
              gammaP = FelisceParam::instance().stabFOPre * hK * hK * hK / m_viscosity;
            } else {
              double xiPre = ReK < 1 ? m_density * hK * hK * hK / m_viscosity : hK * hK / normVelExtrap;
              gammaP = FelisceParam::instance().stabFOPre * xiPre;
            }
            
            // loop over all the faces of the current geometric element
            for(std::size_t iFace = 0; iFace < idOfFacesCurrent.size(); ++iFace) {
              // check if this face is an inner face or a boundary
              ielOppositeGlobalGeo = ielCurrentGlobalGeo;
              
              // first = true if there is an adjacent element
              // second = the ElementType of the adjacent element if first is true
              adjElt = mesh()->getAdjElement(ielOppositeGlobalGeo, iFace);
              
              // compute the stabilization only on inner faces
              if( adjElt.first ) {
                // get the type of the opposite element
                eltTypeOpp = adjElt.second;
                
                // find the local id of the edge in the opposite element
                if( dimension() == 2 )
                  mesh()->getAllEdgeOfElement(ielOppositeGlobalGeo, idOfFacesOpposite);
                else
                  mesh()->getAllFaceOfElement(ielOppositeGlobalGeo, idOfFacesOpposite);
                
                felInt localIdFaceOpposite = -1;
                for(std::size_t jface = 0; jface < idOfFacesOpposite.size(); ++jface) {
                  if( idOfFacesCurrent[iFace] == idOfFacesOpposite[jface] )
                    localIdFaceOpposite = jface;
                }
                
                // get information on the opposite element
                // same as the function "setElemPoint" but here ielOppositeGlobalGeo is global
                // we assume that the supportDofMesh is the same for all unknown (like in setElemPoint)
                m_supportDofUnknown[0].getIdElementSupport(ielOppositeGlobalGeo, ielOppositeGlobalSup);
                
                mesh()->getOneElement(eltTypeOpp, ielOppositeGlobalGeo, oppositeElemIdPoint, oppositeElemPoint, 0);
                
                // get number of dof in support element
                const std::size_t sizeCurrent  = m_supportDofUnknown[0].getNumSupportDof(ielCurrentGlobalSup[0]);
                const std::size_t sizeOpposite = m_supportDofUnknown[0].getNumSupportDof(ielOppositeGlobalSup[0]);
                
                // compare the number of support element
                computeFaceOriented = false;
                if( ielCurrentGlobalSup.size() == ielOppositeGlobalSup.size() ) {

                  // both are duplicated or both are not. Take the element on the same side
                  oppositeSupport = ielOppositeGlobalSup[ielSup];

                  // compute dof in common
                  std::size_t countSupportDofInCommon = 0;
                  for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                    for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2)
                      if( iSupportDof[iEle[oppositeSupport] + iSup2] == iSupportDof[iEle[currentSupport] + iSup1] )
                        ++countSupportDofInCommon;
                  }

                  // if the two support elements share an edge/face
                  if( countSupportDofInCommon == sizeCurrent-1 )
                    computeFaceOriented = true;
                } 
                else {

                  // One is duplicated, the other is not
                  for(std::size_t jelSup=0; jelSup < ielOppositeGlobalSup.size() && !computeFaceOriented; ++jelSup) {
                    oppositeSupport = ielOppositeGlobalSup[jelSup];
                    
                    // compute dof in common
                    std::size_t countSupportDofInCommon = 0;
                    for(std::size_t iSup1=0; iSup1<sizeCurrent; ++iSup1) {
                      for(std::size_t iSup2=0; iSup2<sizeOpposite; ++iSup2)
                        if( iSupportDof[iEle[oppositeSupport] + iSup2] == iSupportDof[iEle[currentSupport] + iSup1] )
                          ++countSupportDofInCommon;
                    }
                   
                    // if the two support elements share an edge/face
                    if( countSupportDofInCommon == sizeCurrent-1 )
                      computeFaceOriented = true;
                  }
                }

                // if there really is an opposite support element
                // Even for an inner edge, it is possible that the current support element has no opposite
                // support element (if it is duplicated and the opposite element is not).
                if( computeFaceOriented ) {
                  // update the opposite finite elements
                  oppositeEltFEWithBdVel[eltTypeOpp]->updateFirstDeriv(0, oppositeElemPoint);
                  oppositeEltFEWithBdVel[eltTypeOpp]->updateBdMeasNormal();
                  oppositeEltFEWithBdPre[eltTypeOpp]->updateFirstDeriv(0, oppositeElemPoint);
                  oppositeEltFEWithBdPre[eltTypeOpp]->updateBdMeasNormal();

                  // compute coefficient
                  elemFieldAdvFace.initialize(DOF_FIELD, m_feVelWithBd->bdEle(iFace), m_velocity->numComponent());
                  elemFieldAdvFaceTmp.initialize(DOF_FIELD, m_feVelWithBd->bdEle(iFace), m_velocity->numComponent());
                  if( FelisceParam::instance().useProjectionForNitscheXFEM ) {
                    for(felInt iextrap = 0; iextrap < FelisceParam::instance().orderBdfNS; ++iextrap) {
                      elemFieldAdvFaceTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVelWithBd, iFace, currentSupport, m_iVelocity, m_ao, dof());
                      elemFieldAdvFace.val += elemFieldAdvFaceTmp.val;
                    }
                  } else {
                    for(felInt iextrap = 0; iextrap < FelisceParam::instance().orderBdfNS; ++iextrap) {
                      elemFieldAdvFaceTmp.setValue(m_seqVelExtrapol[iextrap], *m_feVelWithBd, iFace, ielOld[iextrap], m_iVelocity, m_aoOld[iextrap], m_dofOld[iextrap]);
                      elemFieldAdvFace.val += elemFieldAdvFaceTmp.val;
                    }
                  }

                  UBlasVector normalVel = prod(trans(elemFieldAdvFace.val), m_feVelWithBd->bdEle(iFace).normal[0]);
                  double normVelBd = norm_inf(normalVel);
                  double gammaU = coeffVel * normVelBd;
                  
                  // We can now compute all the integrals.
                  // There is a minus sign for gammaP because there is one for q div(u).
                  // current element for phi_i and phi_j
                  m_elementMat[0]->zero();
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n( gammaU, *m_feVelWithBd, *m_feVelWithBd, iFace, iFace, m_velBlock, m_velBlock, m_velocity->numComponent());
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *m_fePreWithBd, *m_fePreWithBd, iFace, iFace, m_preBlock, m_preBlock, m_pressure->numComponent());
                  setValueMatrixRHS(currentSupport, currentSupport, FlagMatrixRHS::only_matrix);
                  
                  // current element for phi_i and opposite element for phi_j
                  m_elementMat[0]->zero();
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n( gammaU, *oppositeEltFEWithBdVel[eltTypeOpp], *m_feVelWithBd, localIdFaceOpposite, iFace, m_velBlock, m_velBlock, m_velocity->numComponent());
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *oppositeEltFEWithBdPre[eltTypeOpp], *m_fePreWithBd, localIdFaceOpposite, iFace, m_preBlock, m_preBlock, m_pressure->numComponent());
                  setValueMatrixRHS(currentSupport, oppositeSupport, FlagMatrixRHS::only_matrix);
                  
                  // current element for phi_i and opposite element for phi_j
                  m_elementMat[0]->mat() = trans(m_elementMat[0]->mat());
                  setValueMatrixRHS(oppositeSupport, currentSupport, FlagMatrixRHS::only_matrix);
                  
                  // current element for phi_i and phi_j
                  m_elementMat[0]->zero();
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n( gammaU, *oppositeEltFEWithBdVel[eltTypeOpp], *oppositeEltFEWithBdVel[eltTypeOpp], localIdFaceOpposite, localIdFaceOpposite, m_velBlock, m_velBlock, m_velocity->numComponent());
                  m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *oppositeEltFEWithBdPre[eltTypeOpp], *oppositeEltFEWithBdPre[eltTypeOpp], localIdFaceOpposite, localIdFaceOpposite, m_preBlock, m_preBlock, m_pressure->numComponent());
                  setValueMatrixRHS(oppositeSupport, oppositeSupport, FlagMatrixRHS::only_matrix);          
                }
              }
            }
          }
          ++countElement[eltType];
          ++ielCurrentLocalGeo;
        }
      }
    }

    // desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
    
    // desallocate opposite finite elements
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
      eltType = bagElementTypeDomain[i];
      delete oppositeEltFEWithBdVel[eltType];
      delete oppositeEltFEWithBdPre[eltType];
    }
  }

  void LinearProblemNitscheXFEM::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*ielSupportDof*/, FlagMatrixRHS /*flagMatrixRHS*/) 
  {

    // call to the function in linear problem that updates the curvilinear finite element
    if( m_curvFeVel->hasOriginalQuadPoint() ) {
      m_curvFeVel->updateMeasNormalContra(0, elemPoint);
      m_curvFePress->updateMeasNormalContra(0, elemPoint);
    } else {
      m_curvFeVel->updateBasisAndNormalContra(0, elemPoint);
      m_curvFePress->updateBasisAndNormalContra(0, elemPoint);
    }
  }

  void LinearProblemNitscheXFEM::computeAndApplyElementNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof1, felInt& ielSupportDof2, felInt& ielGeoGlobal, FlagMatrixRHS flagMatrixRHS) 
  {

    if( ielSupportDof1 == ielSupportDof2 ) {
      
      // get the side of the sub elements
      std::vector<sideOfInterface> bndElemSide;
      m_duplicateSupportElements->getSubBndEltSide(ielGeoGlobal, bndElemSide);

      // check if the face belongs to the intersected mesh
      if( bndElemSide.size() > 0 ) {

        // update finite elements
        computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, flagMatrixRHS);

        // detect current side
        felInt ielSupport;
        m_supportDofUnknown[0].getIdElementSupport(ielGeoGlobal, ielSupport);
        const sideOfInterface side = ielSupport == ielSupportDof2 ? LEFT : RIGHT;

        // get sub-faces coordinates
        std::vector< std::vector< Point > > bndElemCrd;
        m_duplicateSupportElements->getSubBndElt(ielGeoGlobal, bndElemCrd);

        // loop over the sub-faces
        for(std::size_t iSubElt = 0; iSubElt < bndElemSide.size(); ++iSubElt) {

          // check if sub-face is the good side
          if( bndElemSide[iSubElt] != side )
            continue;

          // get sub-face coordinates
          for(std::size_t iPt = 0; iPt < m_numVerPerSolidElt; ++iPt)
            *m_subItfPts[iPt] = bndElemCrd[iSubElt][iPt];

          // update finite elements
          m_curvFeVel->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);
          m_curvFePress->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);

          // compute specific term of users
          userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, ielSupportDof2, ielGeoGlobal, m_currentLabel);

          // apply natural boundary condition
          applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, flagMatrixRHS);
        }
      } else {
        computeElementArrayBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, flagMatrixRHS);

        userElementComputeNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, ielSupportDof2, ielGeoGlobal, m_currentLabel);

        applyNaturalBoundaryCondition(elemPoint, elemIdPoint, ielSupportDof1, flagMatrixRHS);
      }
    }
  }

  void LinearProblemNitscheXFEM::assembleMatrixBCDarcy() 
  {

    const std::vector<int>& r_fusionlabel = FelisceParam::instance().FusionLabel;
    int currentLabel;
    FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::only_matrix;
    felInt numEltPerLabel, numElement = 0;
    felInt ielGeoGlobal, ielGeo = 0;

    // contains ids of all the support elements associated to a mesh element
    std::vector<felInt> vectorIdSupport;
    
    // use to get element number in support dof mesh ordering.
    felInt ielSupportDof;

    // first loop on geometric type. (only one...)
    ElementType eltType = meshLocal()->bagElementTypeDomainBoundary()[0];

    // number points per element
    const int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];

    // get number domain elements
    const felInt numDomElt = meshLocal()->getNumDomainElement();
    
    // contains points of the current element.
    std::vector<Point*> elemPoint(numPointPerElt, 0);

    // contains ids of point of the current element.
    std::vector<felInt> elemIdPoint(numPointPerElt, 0);
    
    // define all current finite element use in the problem from data
    // file configuration and allocate elemMat and elemVec
    defineCurvilinearFiniteElement(eltType);

    // Element matrix and vector initialisation
    initElementArrayBD();
    
    // allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);
    
    // initialize the elementField for pressureDarcy
    initPerElementTypeDarcy(eltType, flagMatrixRHS); 
    
    // second loop on region of the mesh.
    for (auto itRef = meshLocal()->intRefToBegEndMaps[eltType].begin(); itRef != meshLocal()->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      auto it_fus = std::find(r_fusionlabel.begin(), r_fusionlabel.end(), currentLabel); 
      if ( it_fus != r_fusionlabel.end() ) {

        // third loop on element in the region with the type: eltType. ("real" loop on elements).
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {

          m_supportDofUnknownLocal[m_iPreDarcy].getIdElementSupport(eltType, numElement, vectorIdSupport);
          ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[m_iPreDarcy], vectorIdSupport.size(), vectorIdSupport.data(), vectorIdSupport.data());

          meshLocal()->getOneElement(eltType, numElement, elemIdPoint, elemPoint, 0);

          ielGeo = numElement + numDomElt;
          ISLocalToGlobalMappingApply(mappingElem(), 1, &ielGeo, &ielGeoGlobal);

          // fourth loop over all the support elements
          for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {

            // get the id of the support
            ielSupportDof = vectorIdSupport[it];

            // clear elementary matrices
            for (std::size_t j=0; j<m_elementMatBD.size(); j++)
              m_elementMatBD[j]->zero();

            // compute Darcy term on element
            computeAndApplyElementDarcy(elemPoint, ielSupportDof, ielSupportDof, ielGeoGlobal, flagMatrixRHS);

            // add values of elemMat in the global matrix: m_matrices[0].
            setValueMatrixRHSBD(ielSupportDof, ielSupportDof, flagMatrixRHS);
          }

          numElement++;
        }
      } else {
        numElement += numEltPerLabel;
      }
    }

    //desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);
  }

  void LinearProblemNitscheXFEM::computeAndApplyElementDarcy(const std::vector<Point*>& elemPoint, felInt ielSupportDof1, felInt ielSupportDof2, felInt ielGeoGlobal, FlagMatrixRHS flagMatrixRHS) 
  {
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
    if(ielSupportDof1 == ielSupportDof2) {

      // get the side of the sub elements
      std::vector<sideOfInterface> bndElemSide;
      m_duplicateSupportElements->getSubBndEltSide(ielGeoGlobal, bndElemSide);

      // check if the face belongs to the intersected mesh
      if( bndElemSide.size() > 0 ) {

        // update finite elements
        computeElementArrayDarcy(elemPoint);

        // detect current side
        felInt ielSupport;
        m_supportDofUnknown[0].getIdElementSupport(ielGeoGlobal, ielSupport);
        const sideOfInterface side = ielSupport == ielSupportDof2 ? LEFT : RIGHT;

        // get sub-faces coordinates
        std::vector< std::vector< Point > > bndElemCrd;
        m_duplicateSupportElements->getSubBndElt(ielGeoGlobal, bndElemCrd);

        // loop over the sub-faces
        for(std::size_t iSubElt=0; iSubElt<bndElemSide.size(); ++iSubElt) {

          // check if sub-face is the good side
          if(bndElemSide[iSubElt] != side)
            continue;

          // get sub-face coordinates
          for(std::size_t iPt=0; iPt < m_numVerPerSolidElt; ++iPt)
            *m_subItfPts[iPt] = bndElemCrd[iSubElt][iPt];

          // update finite elements
          m_curvFeVel->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);
          m_curvFeDarcy->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);

          // compute Darcy term
          elementComputeDarcyOnBoundary(elemPoint);
        }
      } else {
        // update finite elements
        computeElementArrayDarcy(elemPoint);

        // compute Darcy term
        elementComputeDarcyOnBoundary(elemPoint);
      }
    }
  }

  void LinearProblemNitscheXFEM::setStrucFiniteElement(CurvilinearFiniteElement* strucFE) 
  {

    m_curvFeStruc = strucFE;
  }

  void LinearProblemNitscheXFEM::addMatrixRHS() 
  {

    this->vector().axpy(1.0,vector(1));
  }

  void LinearProblemNitscheXFEM::setDuplicateSupportObject(DuplicateSupportDof* object) 
  {

    m_duplicateSupportElements = object;
  }

  void LinearProblemNitscheXFEM::gatherSeqVelExtrapol(std::vector<PetscVector>& parallelVec) 
  {
    for(std::size_t i=0; i<parallelVec.size(); ++i)
      gatherVector(parallelVec[i], m_seqVelExtrapol[i]);
    m_isSeqVelExtrapolAllocated = true;
  }

  void LinearProblemNitscheXFEM::gatherSeqBdfRHS(std::vector<PetscVector>& parallelVec) 
  {
    for(std::size_t i=0; i<parallelVec.size(); ++i)
      gatherVector(parallelVec[i], m_seqBdfRHS[i]);
    m_isSeqBdfRHSAllocated = true;
  }

  void LinearProblemNitscheXFEM::deleteDynamicData() 
  {
    // here, we delete all the matrices, vectors and mapping of the linear problem.
    // everything will be rebuilt at the next iteration

    // sequential solution
    bool& isSeqSolAlloc = isSequentialSolutionAllocated();
    if(isSeqSolAlloc) {      
      sequentialSolution().destroy();
      isSeqSolAlloc = false;
    }

    // element matrices and matrices
    for (unsigned int i = 0; i < numberOfMatrices(); i++)
      matrix(i).destroy();

    m_elementMat.clear();
    m_elementVector.clear();
    m_elementMatBD.clear();
    m_elementVectorBD.clear();

    // solution and rhs
    bool& areSolAndRhsAlloc = areSolutionAndRHSAllocated();
    if (areSolAndRhsAlloc) {
      for (unsigned int i = 0; i < numberOfVectors(); i++)
        vector(i).destroy();
      
      solution().destroy();
      areSolAndRhsAlloc = false;
    }

    // finite elements
    for (std::size_t i = 0; i < m_listCurrentFiniteElement.size(); i++)
      delete m_listCurrentFiniteElement[i];
    m_listCurrentFiniteElement.listCurrentFiniteElement().clear();

    for (std::size_t i = 0; i < m_listCurvilinearFiniteElement.size(); i++)
      delete m_listCurvilinearFiniteElement[i];
    m_listCurvilinearFiniteElement.listCurvilinearFiniteElement().clear();

    // mappings
    bool& isMapElemAlloc = isMappingElemAllocated();
    if(isMapElemAlloc) {
      ISLocalToGlobalMappingDestroy(&mappingElem());
      isMapElemAlloc = false;
    }

    bool& isMapElemSupportAlloc = isMappingElemSupportAllocated();
    if(isMapElemSupportAlloc) {
      for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
        ISLocalToGlobalMappingDestroy(mappingElemSupportPerUnknown(iUnknown));

      isMapElemSupportAlloc = false;
    }

    bool& isMapNodesAlloc = isMappingNodesAllocated();
    if(isMapNodesAlloc) {
      ISLocalToGlobalMappingDestroy(&mappingNodes());
      isMapNodesAlloc = false;
    }

    bool& isMapFelToPetscAlloc = isMappingLocalFelisceToGlobalPetscAllocated();
    if(isMapFelToPetscAlloc) {
      for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
        ISLocalToGlobalMappingDestroy(&mappingIdFelisceToPetsc(iUnknown));

      isMapFelToPetscAlloc = false;
    }

    // AO (copy the AO, it will be destroy with the old one)
    bool& isAOAlloc = isAOAllocated();
    if(isAOAlloc) {
      AODestroy(&ao());
      isAOAlloc = false;
    }

    // dofs (the pattern is useless for the old dof)
    dof().numDof() = 0;
    dof().numDofPerUnknown().clear();
    dof().clearPattern();

    // repartition of the dof
    m_dofPart.clear();
    m_eltPart.clear();

    // supportDofMesh (copy the old one)
    m_supportDofUnknown.clear();
    m_supportDofUnknownLocal.clear();

    // local mesh
    meshLocal()->domainDim() = GeometricMeshRegion::GeoMeshUndefined;
    meshLocal()->deleteElements();

    // extrapolation and time rhs of this class
    if(m_isSeqVelExtrapolAllocated) {
      for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i) {
        m_seqVelExtrapol[i].destroy();
        m_seqVelExtrapol[i] = PetscVector::null();
      }
      m_isSeqVelExtrapolAllocated = false;
    }

    if(m_isSeqBdfRHSAllocated) {
      for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i) {
        m_seqBdfRHS[i].destroy();
        m_seqBdfRHS[i] = PetscVector::null();
      }
      m_isSeqBdfRHSAllocated = false;
    }

    // KSP
    m_KSPInterface->destroy();
  }

  void LinearProblemNitscheXFEM::initOldObjects() 
  {
    // compute how many old solution we need to store
    felInt order = FelisceParam::instance().orderBdfNS;

    // resize vector
    m_sequentialSolutionOld.resize(order);
    m_solutionOld.resize(order);
    m_aoOld.resize(order);
    m_supportDofUnknownOld.resize(order);
    m_dofOld.resize(order);

    // init old solution    
    for(std::size_t i=0; i<m_sequentialSolutionOld.size(); ++i) {
      m_sequentialSolutionOld[i].duplicateFrom(sequentialSolution());
      m_sequentialSolutionOld[i].copyFrom(sequentialSolution());
    }

    for(std::size_t i=0; i<m_solutionOld.size(); ++i) {
      m_solutionOld[i].duplicateFrom(solution());
      m_solutionOld[i].copyFrom(solution());
    }

    // init old AO
    for(std::size_t i=0; i<m_aoOld.size(); ++i) {
      m_aoOld[i] = ao();
      PetscObjectReference((PetscObject)ao());
    }

    // init old support dof
    for(std::size_t i=0; i<m_supportDofUnknownOld.size(); ++i)
      m_supportDofUnknownOld[i] = m_supportDofUnknown;

    // init old dof
    std::vector<SupportDofMesh*> pSupportDofUnknown(m_supportDofUnknown.size(), NULL);
    for(std::size_t i=0; i<m_dofOld.size(); ++i) {
      for(std::size_t iUnknown=0; iUnknown < m_supportDofUnknown.size(); ++iUnknown)
        pSupportDofUnknown[iUnknown] = &m_supportDofUnknownOld[i][iUnknown];

      m_dofOld[i].setDof(m_listUnknown, m_listVariable, pSupportDofUnknown);
      m_dofOld[i].numDof() = dof().numDof();
      m_dofOld[i].numDofPerUnknown() = dof().numDofPerUnknown();
    }
  }

  void LinearProblemNitscheXFEM::updateOldSolution(felInt countInitOld) 
  {

    if(countInitOld > 0) {

      // sequential solution
      m_sequentialSolutionOld[m_sequentialSolutionOld.size() - 1].destroy();
      for(std::size_t i=m_sequentialSolutionOld.size() - 1; i>0; --i)
        m_sequentialSolutionOld[i] = m_sequentialSolutionOld[i-1];
      
      m_sequentialSolutionOld[0].duplicateFrom(sequentialSolution());
      m_sequentialSolutionOld[0].copyFrom(sequentialSolution());
      
      // parallel solution
      m_solutionOld[m_solutionOld.size() - 1].destroy();
      for(std::size_t i=m_solutionOld.size() - 1; i>0; --i)
        m_solutionOld[i] = m_solutionOld[i-1];
      
      m_solutionOld[0].duplicateFrom(solution());
      m_solutionOld[0].copyFrom(solution());

      // AO
      AODestroy(&m_aoOld[m_aoOld.size() - 1]);
      for(std::size_t i=m_aoOld.size() - 1; i>0; --i)
        m_aoOld[i] = m_aoOld[i-1];
      m_aoOld[0] = ao();
      PetscObjectReference((PetscObject)ao());

      // supportDofMesh (copy the old one)
      for(std::size_t i=m_supportDofUnknownOld.size() - 1; i>0; --i)
        m_supportDofUnknownOld[i] = m_supportDofUnknownOld[i-1];
      m_supportDofUnknownOld[0] = m_supportDofUnknown;

      // dofs
      for(std::size_t i=m_dofOld.size() - 1; i>0; --i) {
        m_dofOld[i].numDof() = m_dofOld[i-1].numDof();
        m_dofOld[i].numDofPerUnknown() = m_dofOld[i-1].numDofPerUnknown();
      }
      
      m_dofOld[0].numDof() = dof().numDof();
      m_dofOld[0].numDofPerUnknown() = dof().numDofPerUnknown();

      // delete extrapolations
      for(std::size_t i=0; i<m_seqVelExtrapol.size(); ++i) {
        m_seqVelExtrapol[i].destroy();
        m_seqVelExtrapol[i] = PetscVector::null();
      }
      m_isSeqVelExtrapolAllocated = false;
      
      // delete rhs time
      for(std::size_t i=0; i<m_seqBdfRHS.size(); ++i) {
        m_seqBdfRHS[i].destroy();
        m_seqBdfRHS[i] = PetscVector::null();
      }
      m_isSeqBdfRHSAllocated = false;
    } else {

      // copy the solutions
      for(std::size_t i=m_sequentialSolutionOld.size() - 1; i>0; --i)
        m_sequentialSolutionOld[i].copyFrom(m_sequentialSolutionOld[i-1]);
      m_sequentialSolutionOld[0].copyFrom(sequentialSolution());

      for(std::size_t i=m_solutionOld.size() - 1; i>0; --i)
        m_solutionOld[i].copyFrom(m_solutionOld[i-1]);
      m_solutionOld[0].copyFrom(solution());
    }
  }

  void LinearProblemNitscheXFEM::setInterfaceExchangedData(std::vector<double>& intfVelo, std::vector<double>& intfForc) 
  {
    m_intfVelo = &intfVelo;
    m_intfForc = &intfForc;
  }

  void LinearProblemNitscheXFEM::computeInterfaceStress() 
  {

    FEL_CHECK(m_intfForc != nullptr, "Error: the pointer of the interface force has not been initialized yet.");

    std::fill(m_intfForc->begin(), m_intfForc->end(), 0);

    felInt numEltPerLabel;
    int currentLabel;
    
    // Get fictitious interface labels
    const std::vector<int>& fictitious = FelisceParam::instance().fictitious;
    
    // Get element type
    const ElementType eltType   = m_interfMesh->bagElementTypeDomain()[0];
    const ElementType eltTypeFl = meshLocal()->bagElementTypeDomain()[0];

    // Resize array
    const int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    std::vector<Point*> elemPoint(numPointPerElt, 0);
    std::vector<felInt> elemIdPoint(numPointPerElt, 0);

    // Define finite element
    defineFiniteElement(eltTypeFl);

    // Element matrix and vector initialisation
    initElementArray();

    // Get the finite elements
    m_feVel  = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];

    // Initialize element fields
    m_elemFieldSolidRHS.initialize(QUAD_POINT_FIELD, *m_curvFeStruc, m_curvFeStruc->numCoor());
    m_seqVelDofPts.initialize(DOF_FIELD, *m_feVel,  m_velocity->numComponent());
    m_seqPreDofPts.initialize(DOF_FIELD, *m_fePres, m_pressure->numComponent());

    ElementVector elemVec( {m_curvFeStruc}, {static_cast<std::size_t>(m_curvFeStruc->numCoor())} );

    // Used to define a "global" numbering of element in the mesh.
    felInt numElement = 0;

    // Loop on element in the region with the type: eltType
    for(auto itRef = m_interfMesh->intRefToBegEndMaps[eltType].begin(); itRef != m_interfMesh->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel   = itRef->first;
      numEltPerLabel = itRef->second.second;

      // if fictitious interface skip
      auto it_fic = find (fictitious.begin(), fictitious.end(), currentLabel);
      if ( it_fic == fictitious.end() ) {

        for(felInt iel = 0; iel < numEltPerLabel; iel++) {
          
          // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          m_interfMesh->getOneElement(eltType, numElement, elemIdPoint, elemPoint, 0);

          // clear elementary vector.
          elemVec.zero();

          // compute stress on interface element
          m_computeElementaryInterfaceStress(elemPoint, elemIdPoint, eltType, numElement, elemVec);

          // assembly m_intForc
          for(int ic = 0; ic < m_curvFeStruc->numCoor(); ++ic) {
            for(int ip = 0; ip < m_curvFeStruc->numPoint(); ++ip) { // TODO should be numDof but if P1 --> numPoint ok
              int gdof = elemIdPoint[ip];
              (*m_intfForc)[m_curvFeStruc->numCoor()*gdof+ic] += elemVec.vec()[ip+ic*m_curvFeStruc->numDof()];
            }
          }
          
          numElement++;
        }
      } else {

        numElement += numEltPerLabel;
      }
    }
  }

  void LinearProblemNitscheXFEM::printStructStress(int indexTime, double dt) 
  {

    if ( MpiInfo::rankProc() != 0)
      return;
    
    int nbp = m_interfMesh->numPoints();
   
    // write case 
    FILE * pFile;
    std::string fileName, fileNameDir, fileNameCase;
    
    fileNameDir = FelisceParam::instance().resultDirStresses;
    fileNameCase = fileNameDir+"stresses.case";

    pFile = fopen (fileNameCase.c_str(),"w");
    if (pFile==NULL) {
      std::string command = "mkdir -p " + fileNameDir;
      int ierr = system(command.c_str());
      if( ierr > 0)
        FEL_ERROR("Impossible to write "+fileNameCase+" case file");
      pFile = fopen (fileNameCase.c_str(),"w");
    }
    fprintf( pFile,"FORMAT\n");
    fprintf( pFile,"type: ensight\n");
    fprintf( pFile,"GEOMETRY\n");
    fprintf( pFile,"model: solid.geo\n");
    fprintf( pFile,"VARIABLE\n");
    fprintf( pFile,"vector per node: 1 stresses stresses.*****.vct\n");
    fprintf( pFile,"TIME\n");
    fprintf( pFile, "time set: %d\n", 1);
    fprintf( pFile,"number of steps: %d \n", indexTime+1);
    fprintf( pFile, "filename start number: %d\n", 0);
    fprintf( pFile, "filename increment: %d\n", 1);
    fprintf( pFile, "time values:\n");
    int count=0;
  
    for (int i=0; i < indexTime+1; ++i) {
      fprintf( pFile, "%12.5e",i*dt);
      ++count;
      if ( count == 6) {
        fprintf( pFile, "\n" );
        count=0;
      }
    }
    fclose(pFile);

    // file .vct  
    fileName = fileNameDir+"stresses";
    std::stringstream oss;
    oss << indexTime;
    std::string aux = oss.str();
    if ( indexTime < 10 ) {
      aux = "0000" + aux;
    }
    else if ( indexTime < 100 ) {
      aux = "000" + aux;
    }
    else if ( indexTime < 1000 ) {
      aux = "00" + aux;
    }
    else if ( indexTime < 10000 ) {
      aux = "0" + aux;
    }
    
    fileName = fileName + "." + aux + ".vct";
    
    pFile = fopen (fileName.c_str(),"w");
    
    if (pFile==NULL) {
      FEL_ERROR("Impossible to write " + fileName + " vector ensight solution");
    }
    
    count=0;
    int nDimensions = dimension();
    fprintf( pFile, "Vector per node\n");
    // [u_x_1, u_y_1, u_z_1,u_x_2, u_y_2, u_z_2, ..., u_x_dim, u_y_dim, u_z_dim ]
    for ( int i=0; i < nbp; ++i ) {
      for ( int j=0; j < nDimensions; ++j ) {
        if(indexTime==0){
          fprintf(pFile,"%12.5e", 0.);
          ++count;
        } else {
          fprintf(pFile,"%12.5e", (*m_intfForc)[ i*nDimensions+j ] );
          ++count;
        }
        
        if ( count == 6 ) {
          fprintf(pFile, "\n" );
          count=0;
        }
      }
    }
    fclose( pFile );
  }

  void LinearProblemNitscheXFEM::computeElementMatrixRHS() 
  {
    // mass coefficient
    const double coef = m_density/m_fstransient->timeStep;

    //================================
    //            matrix 
    //================================
    // Laplacian operator
    if (m_useSymmetricStress) {
      // \mu \int \epsilon u^{n+1} : \epsilon v
      m_elementMat[0]->eps_phi_i_eps_phi_j(2*m_viscosity,*m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
    } else {
      // \mu \int \nabla u^{n+1} : \nabla v
      m_elementMat[0]->grad_phi_i_grad_phi_j(m_viscosity, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
    }

    // div and grad operator
    // \int q \nabla  \cdot u^{n+1} and \int p \nabla  \cdot v
    m_elementMat[0]->psi_j_div_phi_i_and_psi_i_div_phi_j(*m_feVel, *m_fePres, m_velBlock, m_preBlock, -1, -1);

    // mass matrix from the time scheme
    // \int u^{n+1} \cdot v
    m_elementMat[0]->phi_i_phi_j(m_bdf->coeffDeriv0()*coef, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
  }

  void LinearProblemNitscheXFEM::computeElementMatrixRHSPartDependingOnOldTimeStep(felInt iel, felInt ielOld, felInt idTimeStep) 
  {
    // --------------- //
    // convective term //
    // --------------- //
    switch( m_useNSEquation ) {
    case 0:
      // Stoke's model, no convective term
      break;

    case 1:
      // classic Navier Stokes convective term : \rho (u \cdot \grad) u
      // Natural condition: \sigma(u,p) \cdot n = 0
      // discretization : \rho (u^{n} \cdot \grad) u^{n+1}
      // DEFAULT METHOD
      if(FelisceParam::instance().useProjectionForNitscheXFEM)
        m_elemFieldAdv.setValue(m_seqVelExtrapol[idTimeStep], *m_feVel, iel, m_iVelocity, m_ao, dof());
      else
        m_elemFieldAdv.setValue(m_seqVelExtrapol[idTimeStep], *m_feVel, ielOld, m_iVelocity, m_aoOld[idTimeStep], m_dofOld[idTimeStep]);
      
      m_elementMat[0]->u_grad_phi_j_phi_i(m_density, m_elemFieldAdv, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

      // Temam's operator to stabilize the convective term
      // Convective term in energy balance :
      // ... + \rho/2 \int_Gamma (u^n.n)|u^n+1|^2 - \rho/2 \int_Omega (\nabla . u^n) |u^n+1|^2 + ...
      // Add \rho/2 \int_Omega (\nabla . u^n) u^n+1 v to delete the second term.
      // it's consistent with the exact solution (divergence = 0)
      m_elementMat[0]->div_u_phi_j_phi_i(0.5*m_density, m_elemFieldAdv, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
      break;

    default:
      FEL_ERROR("Error: Wrong or not implemented convection method, check the param NSequationFlag in the data file");
    }

    //================================
    //              RHS
    //================================
    // bdf
    // \frac{\rho}{\dt} \sum_{i=1}^k \alpha_i u_{n+1-i}
    // 'm_density' and not 'coef' because the time step is integrated into the bdf term
    if(FelisceParam::instance().useProjectionForNitscheXFEM)
      m_elemFieldRHSbdf.setValue(m_seqBdfRHS[idTimeStep], *m_feVel, iel, m_iVelocity, m_ao, dof());
    else
      m_elemFieldRHSbdf.setValue(m_seqBdfRHS[idTimeStep], *m_feVel, ielOld, m_iVelocity, m_aoOld[idTimeStep], m_dofOld[idTimeStep]);
    
    m_elementVector[1]->source(m_density, *m_feVel, m_elemFieldRHSbdf, m_velBlock, m_velocity->numComponent());
  }

  void LinearProblemNitscheXFEM::computeNitscheSigma_u_v(double velCoeff, double preCoeff) 
  {
    // \int_K (velCoeff \grad u vec + preCoeff p vec) \cdot v
    if(m_useSymmetricStress)
      m_elementMat[0]->phi_i_eps_phi_j_dot_vec(2.*velCoeff, m_elemFieldNormal, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
    else
      m_elementMat[0]->phi_i_grad_phi_j_dot_vec(velCoeff, m_elemFieldNormal, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

    // same finite element for the pressure and the velocity
    for(std::size_t idim=0; idim<m_velocity->numComponent(); ++idim)
      m_elementMat[0]->phi_i_phi_j(preCoeff*m_elemFieldNormal.val(idim,0), *m_feVel, m_velBlock+idim, m_preBlock, m_pressure->numComponent());
  }

  void LinearProblemNitscheXFEM::computeNitscheSigma_v_u(double velCoeff, double preCoeff) 
  {
    // \int_K (velCoeff \grad v vec + preCoeff q vec) \cdot u
    if(m_useSymmetricStress)
      m_elementMat[0]->eps_phi_i_dot_vec_phi_j(2.*velCoeff, m_elemFieldNormal, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());
    else
      m_elementMat[0]->grad_phi_i_dot_vec_phi_j(velCoeff, m_elemFieldNormal, *m_feVel, m_velBlock, m_velBlock, m_velocity->numComponent());

    // same finite element for the pressure and velocity
    for(std::size_t idim=0; idim<m_velocity->numComponent(); ++idim)
      m_elementMat[0]->phi_i_phi_j(preCoeff*m_elemFieldNormal.val(idim,0), *m_feVel, m_preBlock, m_velBlock+idim, m_pressure->numComponent());
  }

  void LinearProblemNitscheXFEM::computeNitscheDpoint_Sigma_v(double velCoeff, double preCoeff) 
  {
    // \int_K ddot \cdot (velCoeff \grad v vec - preCoeff q vec)
    if(m_useSymmetricStress)
      m_elementVector[0]->eps_phi_i_dot_vec1_dot_vec2(2.*velCoeff, m_elemFieldNormal, m_strucVelQuadPts, *m_feVel, m_velBlock, m_velocity->numComponent());
    else
      m_elementVector[0]->grad_phi_i_dot_vec1_dot_vec2(velCoeff, m_elemFieldNormal, m_strucVelQuadPts, *m_feVel, m_velBlock, m_velocity->numComponent());

    for(std::size_t idim=0; idim<m_velocity->numComponent(); ++idim) {
      for(felInt ig=0; ig<m_feVel->numQuadraturePoint(); ++ig) {
        m_ddotComp.val(0, ig) = m_strucVelQuadPts.val(idim, ig);
      }
      m_elementVector[0]->source(preCoeff*m_elemFieldNormal.val(idim,0), *m_feVel, m_ddotComp, m_preBlock, m_pressure->numComponent());
    }
  }

  void LinearProblemNitscheXFEM::updateStructureVel(felInt strucId) 
  {
    // update on the structure dof the local structure velocity from the master at the struct element strucId
    FEL_CHECK(m_intfVelo != nullptr, "Error: the pointer of the interface velocity has not been initialized yet.");
    // velocity comming from the FSI master ( The order is not the same! )
    // the local ordering is done by component while the global is by point
    std::vector<felInt> idDof(m_curvFeStruc->numDof());
    m_interfMesh->getOneElement(strucId,idDof);
    for(int ic=0; ic< m_curvFeStruc->numCoor(); ++ic) {
      for(int il=0; il< m_curvFeStruc->numDof(); ++il) {
        m_strucVelDofPts.val(ic, il) = (*m_intfVelo)[m_curvFeStruc->numCoor()*idDof[il] +ic]; 
      }
    } 
  }

  void LinearProblemNitscheXFEM::updateSubStructureVel(const std::vector<Point*>& ptElem, const std::vector<Point*>& ptSubElem) 
  {
    // update on the SubStructure dof the local structure velocity from m_strucVelDofPts
    FEL_ASSERT(ptElem.size() == 2 || ptElem.size() == 3);

    // compute the value of the displacement at the new integration points
    // get the coordinate of the integration point in the sub element
    m_curvFeStruc->update(0, ptSubElem);
    m_curvFeStruc->computeCurrentQuadraturePoint();

    // compute the coordinate of these integration points in the reference element with the mapping of the
    // structure element (not the structure sub element)
    std::vector<Point> intPoints(m_curvFeStruc->numQuadraturePoint()); // Replace with m_computeIntegrationPoints
    if( ptElem.size() == 2){
      double dx, dy;
      dx = abs(ptElem[1]->x() - ptElem[0]->x());
      dy = abs(ptElem[1]->y() - ptElem[0]->y());
      if(dx >= dy) {
        for(int ig=0; ig<m_curvFeStruc->numQuadraturePoint(); ++ig) {
          intPoints[ig][0] = 2.*(m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/(ptElem[1]->x() - ptElem[0]->x()) - 1.;
          intPoints[ig][1] = 0.;
        }
      } else {
        for(int ig=0; ig<m_curvFeStruc->numQuadraturePoint(); ++ig) {
          intPoints[ig][0] = 2.*(m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/(ptElem[1]->y() - ptElem[0]->y()) - 1.;
          intPoints[ig][1] = 0;
        }
      }
    } else {

      double det1 = (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->x() - ptElem[0]->x())
                  - (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->y() - ptElem[0]->y());

      double det2 = (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->z() - ptElem[0]->z())
                  - (ptElem[2]->z() - ptElem[0]->z())*(ptElem[1]->x() - ptElem[0]->x());

      double det3 = (ptElem[2]->z() - ptElem[0]->z())*(ptElem[1]->y() - ptElem[0]->y())
                  - (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->z() - ptElem[0]->z());

      if(abs(det1) >= abs(det2) && abs(det1) >= abs(det3)){
        for(int ig=0; ig<m_curvFeStruc->numQuadraturePoint(); ++ig) {
          intPoints[ig][0] = (ptElem[2]->y() - ptElem[0]->y())*(m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/det1
                           + (ptElem[0]->x() - ptElem[2]->x())*(m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/det1;

          intPoints[ig][1] = (ptElem[0]->y() - ptElem[1]->y())*(m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/det1
                           + (ptElem[1]->x() - ptElem[0]->x())*(m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/det1;

          intPoints[ig][2] = 0.;

        }
      } else if(abs(det2) >= abs(det1) && abs(det2) >= abs(det3)){
        for(int ig=0; ig<m_curvFeStruc->numQuadraturePoint(); ++ig) {
          intPoints[ig][0] = (ptElem[2]->x() - ptElem[0]->x())*(m_curvFeStruc->currentQuadPoint[ig].z() - ptElem[0]->z())/det2
                           + (ptElem[0]->z() - ptElem[2]->z())*(m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/det2;

          intPoints[ig][1] = (ptElem[0]->x() - ptElem[1]->x())*(m_curvFeStruc->currentQuadPoint[ig].z() - ptElem[0]->z())/det2
                           + (ptElem[1]->z() - ptElem[0]->z())*(m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/det2;

          intPoints[ig][2] = 0.;
        }
      } else {
        for(int ig=0; ig<m_curvFeStruc->numQuadraturePoint(); ++ig) {
          intPoints[ig][0] = (ptElem[2]->z() - ptElem[0]->z())*(m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/det3
                           + (ptElem[0]->y() - ptElem[2]->y())*(m_curvFeStruc->currentQuadPoint[ig].z() - ptElem[0]->z())/det3;

          intPoints[ig][1] = (ptElem[0]->z() - ptElem[1]->z())*(m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/det3
                           + (ptElem[1]->y() - ptElem[0]->y())*(m_curvFeStruc->currentQuadPoint[ig].z() - ptElem[0]->z())/det3;

          intPoints[ig][2] = 0.;
        }
      }
    }

    // update the structure finite element with the element (really needed ? we only want to access the basis
    // function directly to evaluate them at the computed integration points).
    m_curvFeStruc->update(0, ptElem);

    // Now we can evaluate the basis function of the reference element at the integration points of
    // the sub element
    for(int icomp=0; icomp<m_feVel->numCoor(); ++icomp) {
      for(int ig=0; ig<m_curvFeStruc->numQuadraturePoint(); ++ig) {
        m_strucVelQuadPts.val(icomp, ig) = 0;
        for(int idof=0; idof<m_curvFeStruc->numDof(); ++idof) {
          m_strucVelQuadPts.val(icomp, ig) += m_strucVelDofPts.val(icomp, idof) * m_curvFeStruc->refEle().basisFunction().phi(idof, intPoints[ig]);
        }
      }
    }
  }

  void LinearProblemNitscheXFEM::initPerElementTypeDarcy(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) 
  {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    // Get the curvilinear finite elements
    m_curvFeVel   = m_listCurvilinearFiniteElement[m_iVelocity];
    m_curvFePress = m_listCurvilinearFiniteElement[m_iPressure];
    m_curvFeDarcy = m_listCurvilinearFiniteElement[m_iPreDarcy];
  }

  void LinearProblemNitscheXFEM::computeElementArrayDarcy(const std::vector<Point*>& elemPoint) 
  {

    // call to the function in linear problem that updates the curvilinear finite element
    if(m_curvFeVel->hasOriginalQuadPoint()) {
      m_curvFeVel->updateMeasNormalContra(0, elemPoint);
      m_curvFeDarcy->updateMeasNormalContra(0, elemPoint);
    } else {
      m_curvFeVel->updateBasisAndNormalContra(0, elemPoint);
      m_curvFeDarcy->updateBasisAndNormalContra(0, elemPoint);
    }

    //if ( FelisceParam::instance().contactDarcy == 2 ) // TODO
    //  m_curvFeVel->updateMeasNormalTangent(0, elemPoint);
  }

  void LinearProblemNitscheXFEM::elementComputeDarcyOnBoundary(const std::vector<Point*>& elemPoint)
  {
    // Check if we are in the labels where we want to consider the porous media
    // No need to reupdate the FEM, already done in computeElementArrayDarcy.
    
    if ( dimension() == 2 ) {
      const double eps = FelisceParam::instance().epsDarcy;
      const double K = FelisceParam::instance().kDarcy;
      const double alpha = 1.;

      // FLUID BOUNDARY TERMS
      // + eps /4K uf_j . n vf_i . n
      m_elementMatBD[0]->phi_i_dot_n_phi_j_dot_n(0.25*eps/K, *m_curvFeVel, m_velBlock, m_velBlock, m_velocity->numComponent());

      // + pl_j vf_i.n
      m_elementMatBD[0]->psi_j_phi_i_dot_n(1, *m_curvFeVel, *m_curvFeDarcy, m_velBlock, m_darBlock, 0);
     
      // BJS condition
      if( FelisceParam::instance().contactDarcy == 2){
        const double coeff = -alpha/sqrt(eps*K);
        std::vector<double> edgeTan(2);

        // tangent vector in 2D
        edgeTan[1] = elemPoint[1]->y() - elemPoint[0]->y();  
        edgeTan[0] = elemPoint[1]->x() - elemPoint[0]->x();

        const double tmpTang = 1/sqrt(edgeTan[0]*edgeTan[0] + edgeTan[1]*edgeTan[1]);  // TODO
        edgeTan[1] *= tmpTang;
        edgeTan[0] *= tmpTang;

        for(std::size_t idim=0; idim<m_velocity->numComponent(); ++idim)
          m_elementMatBD[0]->phi_i_phi_j(coeff*edgeTan[idim], *m_curvFeVel, m_velBlock+idim, m_velBlock+idim, 1);

        // m_elementMatBD[0]->phi_i_dot_t_phi_j_dot_t(coeff, *m_curvFeVel, m_velBlock+idim, m_velBlock+idim, m_velocity->numComponent());
      } 

      // DARCY TERMS
      // - uf_j.n ql_i
      m_elementMatBD[0]->phi_i_psi_j_scalar_n(-1, *m_curvFeDarcy, *m_curvFeVel, m_darBlock, m_velBlock, m_velocity->numComponent());
      
      // + eps * Kt grad_t Pl_j . grad_t ql_i
      m_elementMatBD[0]->grad_phi_i_grad_phi_j( eps*K, *m_curvFeDarcy, m_darBlock, m_darBlock, m_presDarcy->numComponent());

    } else {
      FEL_ERROR("DarcyForContact model is not yet available for 3D");
    }
  }

  void LinearProblemNitscheXFEM::m_computeElementaryInterfaceStress(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, const ElementType eltType, const felInt iel, ElementVector& elemVec) 
  {
  
    // loop over the sub structure to compute the integrals involving the fluid velocity and pressure
    felInt numSubElement = m_duplicateSupportElements->getNumSubItfEltPerItfElt(iel);
    if ( numSubElement > 0 ) {

      // integration points
      m_intPoints.clear();
      m_intPoints.resize(m_curvFeStruc->numQuadraturePoint());

      // Assembling of the terms coming from the coupling with the fluid and the Nitsche-XFEM formulation
      std::vector<felInt> vectorIdSupport;

      felInt subElemId;
      felInt fluidElemId;
      std::vector<double> tmpPt(3);
      std::vector<double> itfNormal(dimension());

      GeometricMeshRegion::ElementType eltTypeFluid = this->mesh()->bagElementTypeDomain()[0];
      const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltTypeFluid];
      std::vector<Point*> fluidElemPts(numPointsPerElt);
      std::vector<felInt> fluidElemPtsId(numPointsPerElt);

      if( m_skipSmallVolume ){
        m_totalArea = 0;
        m_skippedArea = 0;
      }

      updateStructureVel(iel);

      // get the normal to this sub element
      m_interfMesh->listElementNormal(eltType, iel).getCoor(itfNormal);

      for(felInt iSubElt = 0; iSubElt < numSubElement; ++iSubElt) {
        // get the id of the solid sub element
        subElemId = m_duplicateSupportElements->getSubItfEltIdxItf(iel, iSubElt);
        
        // fill the vector with the points of the sub-element
        for(std::size_t iPt=0; iPt < m_numVerPerSolidElt; ++iPt) {
          m_duplicateSupportElements->getSubItfEltVerCrd(subElemId, iPt, tmpPt);
          *m_subItfPts[iPt] = Point(tmpPt[0], tmpPt[1], tmpPt[2]);
        }

        if( m_skipSmallVolume ){
          const double area = compAreaVolume(m_subItfPts);
          m_totalArea += area;
          if ( area <= m_eps*compAreaVolume(elemPoint) ) {
            m_skippedArea += std::abs(area);
            continue;
          }
        }

        // update measure
        m_curvFeStruc->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);
        
        // get the coordinate of the fluid element
        fluidElemId = m_duplicateSupportElements->getMshEltIdxOfSubItfElt(subElemId);
                
        mesh()->getOneElement(eltTypeFluid, fluidElemId, fluidElemPtsId, fluidElemPts, 0);
        
        // update the element for the fluid finite element
        m_feVel->updateFirstDeriv(0, fluidElemPts);
        
        // compute the integration point for this element
        m_computeIntegrationPoints(fluidElemPts);
        
        // get all the support elements for the fluid
        m_supportDofUnknown[0].getIdElementSupport(fluidElemId, vectorIdSupport);
              
        // Nitsche penalty term (derivative of the displacement)
        // This integral is here because the penalty parameter depends of the fluid element
        const double penaltyTerm = m_viscosity * m_nitschePar / m_feVel->diameter();

        // stress coming from the nitsche penalty term
        // - int_\Sigma gamma.mu/h dotd . w
        elemVec.source(-2.*penaltyTerm, *m_curvFeStruc, m_strucVelDofPts, 0, m_curvFeStruc->numCoor());

        for(std::size_t iSup = 0; iSup < vectorIdSupport.size(); ++iSup) { 
          // get the velocity  at the dof of the structure
          m_seqVelDofPts.setValue(sequentialSolution(), *m_feVel, vectorIdSupport[iSup], m_iVelocity, m_ao, dof());
          m_computeFluidVelOnSubStructure();

          // int_\Sgima gamma.mu/h u^n.  w
          elemVec.source(penaltyTerm, *m_curvFeStruc, m_elemFieldSolidRHS, 0, m_curvFeStruc->numCoor());
        }

        // \int sigma(u,p) n . w 
        for(std::size_t iSup = 0; iSup < vectorIdSupport.size(); ++iSup) {
          // get the velocity and pressure at the dof of the structure
          m_seqVelDofPts.setValue(sequentialSolution(), *m_feVel,  vectorIdSupport[iSup], m_iVelocity, m_ao, dof());
          m_seqPreDofPts.setValue(sequentialSolution(), *m_fePres, vectorIdSupport[iSup], m_iPressure, m_ao, dof());
          
          // evaluate stress at quadrature points
          m_computeFluidStress(2.*iSup - 1., itfNormal);
          elemVec.source(-1, *m_curvFeStruc, m_elemFieldSolidRHS, 0, m_curvFeStruc->numCoor());
        }
      }

      if( m_skipSmallVolume && MpiInfo::rankProc() == 0 ) {
        if ( std::abs(m_skippedArea) > m_eps * std::abs(m_totalArea) )
          std::cout << "In computeElementaryInterfaceStress, Skipped surface is : " << std::fixed << std::setprecision(16) << m_skippedArea / m_totalArea * 100. << " %" << std::defaultfloat << std::endl;
      }
    }
  }

  void LinearProblemNitscheXFEM::m_computeIntegrationPoints(std::vector<Point*>& ptElem) 
  { // TODO D.C. this function should be moved to finite element class
    // compute the integration of the substructure in the reference element of the structure element.
    this->m_curvFeStruc->update(0, m_subItfPts);
    this->m_curvFeStruc->computeCurrentQuadraturePoint();
    
    TypeShape fluidCellShape = (TypeShape) m_feVel->geoEle().shape().typeShape();
    
    // mapping from the current fluid element to the fluid reference element
    if(fluidCellShape == Quadrilateral) {
      for(int ig=0; ig<this->m_curvFeStruc->numQuadraturePoint(); ++ig) {
        for(int icomp=0; icomp<m_feVel->numCoor(); ++icomp) {
          m_intPoints[ig][icomp] = 2*(this->m_curvFeStruc->currentQuadPoint[ig][icomp] - (*ptElem[0])[icomp])/((*ptElem[2])[icomp] - (*ptElem[0])[icomp]) - 1.;
        }
      }
    } else if(fluidCellShape == Triangle) {
      double det = (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->x() - ptElem[0]->x())
                 - (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->y() - ptElem[0]->y());

      for(int ig=0; ig<this->m_curvFeStruc->numQuadraturePoint(); ++ig) {
        m_intPoints[ig][0] = (ptElem[2]->y() - ptElem[0]->y())*(this->m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/det + (ptElem[0]->x() - ptElem[2]->x()) * (this->m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/det;
        m_intPoints[ig][1] = (ptElem[0]->y() - ptElem[1]->y())*(this->m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x())/det + (ptElem[1]->x() - ptElem[0]->x()) * (this->m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y())/det;
      }
    } else if(fluidCellShape == Tetrahedron ) {

      UBlasPermutationMatrix permut(m_feVel->numCoor());
      UBlasMatrix FLU(m_feVel->numCoor(),m_feVel->numCoor());
      UBlasMatrix IdM(m_feVel->numCoor(),m_feVel->numCoor());
      UBlasVector refPoint(m_feVel->numCoor());
      UBlasVector tmpRefPoint(m_feVel->numCoor());
      IdM.assign(boost::numeric::ublas::identity_matrix<double>(FLU.size1()));
      //filling the matrix 
      for(int indx = 0; indx < m_feVel->numCoor() ; ++indx){
        FLU(0,indx) = ptElem[indx+1]->x() - ptElem[0]->x();
        FLU(1,indx) = ptElem[indx+1]->y() - ptElem[0]->y();
        FLU(2,indx) = ptElem[indx+1]->z() - ptElem[0]->z();
      }

      lu_factorize(FLU,permut);
      lu_substitute(FLU,permut,IdM);

      for(int ig=0; ig<this->m_curvFeStruc->numQuadraturePoint(); ++ig){

        // filling the rhs
        tmpRefPoint(0) = this->m_curvFeStruc->currentQuadPoint[ig].x() - ptElem[0]->x();
        tmpRefPoint(1) = this->m_curvFeStruc->currentQuadPoint[ig].y() - ptElem[0]->y();
        tmpRefPoint(2) = this->m_curvFeStruc->currentQuadPoint[ig].z() - ptElem[0]->z();

        refPoint = prod(IdM,tmpRefPoint);
        m_intPoints[ig][0] = refPoint[0];
        m_intPoints[ig][1] = refPoint[1];
        m_intPoints[ig][2] = refPoint[2];

        refPoint.clear();
        tmpRefPoint.clear();
      }
    } else {
      FEL_ERROR("This shape type is not implemented yet");
    }
  }

  void LinearProblemNitscheXFEM::m_computeFluidVelOnSubStructure() 
  {
    // Compute the velocity at the integration points of the sub structure
    // These points are computed in the function computeIntegrationPoints
    for(int icomp=0; icomp<m_curvFeStruc->numCoor(); ++icomp) {
      for(int ig=0; ig<this->m_curvFeStruc->numQuadraturePoint(); ++ig) {
        m_elemFieldSolidRHS.val(icomp, ig) = 0;
        for(int idof=0; idof<m_feVel->numDof(); ++idof) {
          m_elemFieldSolidRHS.val(icomp, ig) += m_seqVelDofPts.val(icomp, idof) * m_feVel->refEle().basisFunction().phi(idof, m_intPoints[ig]);
        }
      }
    }
  }

  void LinearProblemNitscheXFEM::m_computeFluidStress(double sideCoeff, const std::vector<double>& strucNormal) 
  {
    // compute the tensor at integration points of the sub structure
    // These points are computed in the function computeIntegrationPoints
    double tmpval1 = 0;
    
    for(int icomp=0; icomp<m_curvFeStruc->numCoor(); ++icomp) {
      for(int ig=0; ig< m_curvFeStruc->numQuadraturePoint(); ++ig) {
        m_elemFieldSolidRHS.val(icomp, ig) = 0;
      
        // velocity
        for(int jcomp=0; jcomp<m_feVel->numCoor(); ++jcomp) {

          tmpval1 = 0;
          for(int idof=0; idof<m_feVel->numDof(); ++idof)
            tmpval1 += m_seqVelDofPts.val(icomp, idof) * m_feVel->dPhi[ig](jcomp, idof);
                
          m_elemFieldSolidRHS.val(icomp, ig) += m_viscosity * tmpval1 * sideCoeff * strucNormal[jcomp];
        }
                
        // pressure
        for(int idof=0; idof<m_feVel->numDof(); ++idof)
          m_elemFieldSolidRHS.val(icomp, ig) -= sideCoeff * m_seqPreDofPts.val(0, idof) * m_fePres->refEle().basisFunction().phi(idof, m_intPoints[ig]) * strucNormal[icomp];
      }
    }
      
    // if we use the symmetric formulation for the stress
    if(m_useSymmetricStress) {
      for(int idof=0; idof<m_feVel->numDof(); ++idof) {

        tmpval1 = 0;
        for(int jcomp=0; jcomp<m_feVel->numCoor(); ++jcomp)
          tmpval1 += m_seqVelDofPts.val(jcomp, idof) * strucNormal[jcomp];

        for(int icomp=0; icomp<m_curvFeStruc->numCoor(); ++icomp)
          for(int ig=0; ig<this->m_curvFeStruc->numQuadraturePoint(); ++ig)
            m_elemFieldSolidRHS.val(icomp, ig) += m_viscosity * tmpval1 * sideCoeff * m_feVel->dPhi[ig](icomp, idof);
      }
    }
  }

  double LinearProblemNitscheXFEM::compAreaVolume(const std::vector<Point*>& elemPoint) const 
  {
    double area;
    Point normal, edge;
    MathUtilities::CrossProduct(normal.getCoor(), (*elemPoint[1] - *elemPoint[0]).getCoor(), (*elemPoint[2] - *elemPoint[0]).getCoor());
    
    if(elemPoint.size() == 3){
      area = 0.5 * normal.norm();          
    }
    else {
      edge = (*elemPoint[3]) - (*elemPoint[0]);   
      area = std::inner_product(normal.getCoor().begin(), normal.getCoor().end(), edge.getCoor().begin(), 0.) / 6.;       
    }
    if(area < 0){
      std::cout << " ERROR: Element with negative area" << std::endl;
    }
    return area;
  }

  void LinearProblemNitscheXFEM::printSkipVolume(bool printSlipVolume)
  {

    m_printSkipVolume = printSlipVolume;
  }

  void LinearProblemNitscheXFEM::printMeshPartition(int indexTime, double dt) const 
  {

    {
      // write case 
      FILE * pFile;
      std::string fileName, fileNameDir, fileNameCase;
      
      fileNameDir = FelisceParam::instance().resultDir;
      fileNameCase = fileNameDir+"intersection.case";

      pFile = fopen (fileNameCase.c_str(),"w");
      if (pFile==NULL) {
        std::string command = "mkdir -p " + fileNameDir;
        int ierr = system(command.c_str());
        if( ierr > 0)
          FEL_ERROR("Impossible to write "+fileNameCase+" case file");
        pFile = fopen (fileNameCase.c_str(),"w");
      }
      fprintf( pFile,"FORMAT\n");
      // fprintf( pFile,"type: ensight\n");
      fprintf( pFile,"type: ensight gold\n");
      fprintf( pFile,"GEOMETRY\n");
      // fprintf( pFile,"model: 1 fluid.geo.*****.geo\n");
      fprintf( pFile,"model: 1 fluid.geo\n");
      fprintf( pFile,"VARIABLE\n");
      fprintf( pFile,"scalar per element: 1 intersection intersection.*****.scl\n");
      fprintf( pFile,"TIME\n");
      fprintf( pFile, "time set: %d\n", 1);
      fprintf( pFile,"number of steps: %d \n", indexTime+1);
      fprintf( pFile, "filename start number: %d\n", 0);
      fprintf( pFile, "filename increment: %d\n", 1);
      fprintf( pFile, "time values:\n");
    
      int count=0;
      for (int i=0; i < indexTime+1; ++i) {
        fprintf( pFile, "%12.5e",i*dt);
        ++count;
        if ( count == 6) {
          fprintf( pFile, "\n" );
          count=0;
        }
      }
      fclose(pFile);
    }

    {
      felInt currentIelGeo;
      std::vector<felInt> vecSupportCurrent;
      GeometricMeshRegion::ElementType eltType;

      std::string fileName = FelisceParam::instance().resultDir + "intersection";
      std::stringstream oss;
      oss << indexTime;
      std::string aux = oss.str();
      if ( indexTime < 10 ) {
        aux = "0000" + aux;
      } else if ( indexTime < 100 ) {
        aux = "000" + aux;
      } else if ( indexTime < 1000 ) {
        aux = "00" + aux;
      } else if ( indexTime < 10000 ) {
        aux = "0" + aux;
      }

      fileName = fileName + "." + aux + ".scl";
      FILE * pFile;
      pFile = fopen (fileName.c_str(),"w");

      if (pFile==nullptr) {
        FEL_ERROR("Impossible to write " + fileName + " scalar ensight solution");
      }

      // int count=0;
      fprintf( pFile, "Scalar per element\n");
      fprintf(pFile, "part\n");
      fprintf(pFile, "%10d\n",0);
      if (mesh()->domainDim() == 2) {
        fprintf(pFile, "tria3\n");
      }
      else if (mesh()->domainDim() == 3) {
        fprintf(pFile, "tetra4\n");
      }

      currentIelGeo = 0;
      for (std::size_t i=0; i<mesh()->bagElementTypeDomain().size(); ++i) {
        eltType = mesh()->bagElementTypeDomain()[i];

        // second loop on element
        for (felInt iElm = 0; iElm < mesh()->numElements(eltType); ++iElm) {

          fprintf(pFile,"%d\n", static_cast<int>(m_eltPart[m_currentMesh][currentIelGeo]));

          ++currentIelGeo;
        }
      }
      fprintf(pFile, "\n");
      fclose(pFile);
    }
  }

  void LinearProblemNitscheXFEM::writeDuplication(int rank, IO& io, double& time, int iteration) const 
  {

    std::vector<felInt> vec_id(dimension()+1);
    double *solution = new double[supportDofUnknown(0).listNode().size()];
    for (std::size_t i = 0; i < supportDofUnknown(0).listNode().size(); ++i)
      solution[i] = 0;    


    for (std::size_t iElm = 0; iElm < supportDofUnknown(0).vectorIdElementSupport().size(); ++iElm) {
      auto& r_vec = supportDofUnknown(0).vectorIdElementSupport()[iElm];

      std::size_t numDpl = r_vec.size();

      if ( numDpl > 1 ) {
        supportDofUnknown(0).getIdPointElementSupport(r_vec[0], vec_id);

        for (auto node_id = vec_id.begin(); node_id < vec_id.end(); ++node_id)
          solution[*node_id] = 1; 

        supportDofUnknown(0).getIdPointElementSupport(r_vec[1], vec_id);

        for (auto node_id = vec_id.begin(); node_id < vec_id.end(); ++node_id)
          solution[*node_id] = -1; 
      }
    }

    io.writeSolution(rank, time, iteration, 0, "duplication", solution, supportDofUnknown(0).listNode().size(), dimension(), true);
  }
}
