//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/steklovBanner.hpp"

namespace felisce {
  steklovBanner::steklovBanner(int numLocDof, int rankProc, MPI_Comm comm, int dimRoSteklov):
    m_numLoc(numLocDof),
    m_numGlob(0),
    m_rankProc(rankProc),
    m_comm(comm),
    m_counter(0),
    m_step(1),
    m_counterStep(0),
    m_dimRoSteklov(dimRoSteklov) {
    MPI_Allreduce(&m_numLoc,&m_numGlob,1,MPI_INT,MPI_SUM,m_comm);
    if ( FelisceParam::verbose() > 1 && numLocDof > 0 ) {
      std::stringstream master;
      master<<"The total number of degrees of freedom on the interface is"<< std::endl;
      PetscPrintf(m_comm,"%s",master.str().c_str());
      MPI_Barrier(m_comm);
      std::cout<<"["<<m_rankProc<<"] numLoc (numGlob): "<<m_numLoc<<" ( "<<m_numGlob<<" )"<< std::endl;
      MPI_Barrier(m_comm);
    }
  }
  void
  steklovBanner::initComputation() {
    if ( FelisceParam::verbose() > 0  && m_numLoc > 0 ) {
      std::stringstream master,master2;
      if ( m_dimRoSteklov <= 0 ) {
        master << "Starting computation of Steklov operator "<< std::endl;
        m_NMax =  std::min(100, m_numGlob );
        m_step = double(m_numGlob) / double(m_NMax) ;
      } else {
        master << "Starting computation of the reduced order Steklov operator "<< std::endl;
        m_NMax =  std::min(100, m_dimRoSteklov);
        m_step = double(m_dimRoSteklov) / double(m_NMax) ;
      }
      PetscPrintf(m_comm, "%s",master.str().c_str() );
      master2<<"<";
      for ( int k(0); k<m_NMax; ++k) {
        master2<<"-";
      }
      master2<<">"<<std::endl;
      PetscPrintf(m_comm,"%s",master2.str().c_str());
      PetscPrintf(m_comm,"<");
      MPI_Barrier(m_comm);
    }
  }
  void
  steklovBanner::operator++() {
    ++m_counter;
    if ( m_counter > m_step * ( m_counterStep + 1 ) ) {
      ++m_counterStep;
      if ( FelisceParam::verbose() > 0  && m_numLoc > 0 ) {
        PetscPrintf(m_comm,"=");
        MPI_Barrier(m_comm);
      }
    }
  }
  void
  steklovBanner::finalizeComputation() {
    if ( FelisceParam::verbose() > 0  && m_numLoc > 0 ) {
      for (int k(0); k < m_NMax-m_counterStep; ++k) {
        PetscPrintf(m_comm,"=");
      }
      PetscPrintf(m_comm,">\n");
      MPI_Barrier(m_comm);
    }
  }
}
