//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin E. Schenone
//

#ifndef _CARDIACFUNCTION_HPP
#define _CARDIACFUNCTION_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce {
  class LinearProblem;

  /*!
  \brief File with CardiacFunction class, AppCurrent and heterogeneous parameters for BidomainProblem
   */
  class CardiacFunction {
  public:
    CardiacFunction();
    virtual ~CardiacFunction();
    virtual void initialize(FelisceTransient::Pointer fstransient);
  protected:
    FelisceTransient::Pointer m_fstransient;
  };

  //A class to impose thorso/lungs/bone conductivity.
  class ThoraxConductivity:
    public CardiacFunction {
  public:
    ThoraxConductivity();
    ~ThoraxConductivity() override;
    double sigmaT(int& label) const ;
    double operator() (int& label) const;
  };

  //A class to impose thorso/lungs/bone conductivity.
  class HeartConductivity:
    public CardiacFunction {
  public:
    HeartConductivity();
    ~HeartConductivity() override;
    double sigmaH(int& label) const ;
    double operator() (int& label) const;
  };

  //A class to impose a non-pathological applied current to the ellibi-geometry.
  class AppCurrent:
    public CardiacFunction {
  public:
    AppCurrent();
    ~AppCurrent() override;
    virtual double Iapp(double x, double y, double z, double t=0., double dist=0.) const ;
    virtual double operator() (double x, double y, double z, double t=0., double dist=0.) const;
  };

  //A class to impose a non-pathological applied current to the zygote-geometry.
  class zygoteIapp:
    public AppCurrent {
  public:
    zygoteIapp();
    ~zygoteIapp() override = default;
    double Iapp(double x, double y, double z, double t, double dist) const override;
    double operator() (double x, double y, double z, double t,double dist) const override;
  };

  //A class to impose a non-pathological applied exterior current to the atria.
  class AppCurrentAtria:
    public CardiacFunction {
  public:
    AppCurrentAtria();
    ~AppCurrentAtria() override;
    virtual double IappAtria(double x, double y, double z, double t) const;
    virtual double operator() (double x, double y, double z, double t) const;
  };


  //A class to impose heterogeneous tau_close for ellibi-geometry.
  class HeteroTauClose:
    public CardiacFunction {
  public:
    HeteroTauClose();
    ~HeteroTauClose() override;
    virtual double TauClose(double x, double y, double z, double dist=0., double t=0.) const;
    virtual double operator() (double x, double y, double z, double dist=0., double t=0.) const;
  };

  //A class to simulate infarction.
  class HeteroTauOut:
    public CardiacFunction {
  public:
    HeteroTauOut();
    ~HeteroTauOut() override;
    virtual double TauOut(double x, double y, double z) const;
    virtual double operator() (double x, double y, double z) const;
  }	;

  //A class to simulate infarction.
  class HeteroTauIn:
    public CardiacFunction {
  public:
    HeteroTauIn();
    ~HeteroTauIn() override;
    double TauIn(double x, double y, double z) const;
    double operator() (double x, double y, double z) const;
  };

  //A class to impose heterogeneous Ito for the Courtemanche-Ramirez-Nattel model.
  class HeteroCourtModelIto:
    public CardiacFunction {
  public:
    HeteroCourtModelIto();
    ~HeteroCourtModelIto() override;
    virtual double CourtCondIto(double x, double y, double z) const;
    virtual double operator() (double x, double y, double z) const;
  };

  //A class to impose heterogeneous ICaL for the Courtemanche-Ramirez-Nattel model.
  class HeteroCourtModelICaL:
    public CardiacFunction {
  public:
    HeteroCourtModelICaL();
    ~HeteroCourtModelICaL() override;
    virtual double CourtCondICaL(double x, double y, double z) const;
    virtual double operator() (double x, double y, double z) const;
  };

  //A class to impose heterogeneous multiplicative coefficient for the Courtemanche-Ramirez-Nattel model.
  class HeteroCourtModelMultCoeff:
    public CardiacFunction {
  public:
    HeteroCourtModelMultCoeff();
    ~HeteroCourtModelMultCoeff() override;
    virtual double CourtCondMultCoeff(double x, double y, double z, double ref=0.) const;
    virtual double operator() (double x, double y, double z, double ref=0.) const;
  };


  // Class to impose heterogeneous "s" parameter in FHN model.
  class HeteroSparFHN:
    public CardiacFunction {
  public:
    HeteroSparFHN();
    ~HeteroSparFHN() override;
    virtual double SparFHN(double x, double y, double z) const;
    virtual double operator() (double x, double y, double z) const;
  };

  //A class to impose heterogeneous tau_close for ellibi-geometry.
  class HeteroMVCoeff:
    public CardiacFunction {
  public:
    HeteroMVCoeff();
    ~HeteroMVCoeff() override;
    virtual int MVCoeff(double x, double y, double z, double dist, double t=0.) const;
    virtual int operator() (double x, double y, double z, double dist, double t=0.) const;
  };

}


#endif
