//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __LINEARPROBLEMPERFECTFLUIDRS_HPP__
#define __LINEARPROBLEMPERFECTFLUIDRS_HPP__

// System includes

// External includes

// Project includes
#include "Solver/linearProblemReducedSteklov.hpp"
#include "Solver/linearProblemPerfectFluid.hpp"

namespace felisce {
  class LinearProblemPerfectFluidRS : public LinearProblemReducedSteklov<LinearProblemPerfectFluid> {
  public:
    void solvePFUsingSteklov(FelisceTransient::Pointer fs,int nfp);
    void initializeDofBoundaryAndBD2VolMaps() override;
    void exportNormalField();
  protected:
    void userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/,felInt& iel, int label) override;
  private:
    void initPerETMASS() override;
    void initPerETLAP() override;
    
    void updateFE(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/) override;    
    
    void massMatrixComputer(felInt ielSupportDof) override;  
    void laplacianMatrixComputer(felInt ielSupportDof) override;

    CurvilinearFiniteElement* m_feIopCurv;
  };
}
#endif
