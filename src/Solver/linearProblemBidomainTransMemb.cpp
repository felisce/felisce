//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone C. Corrado
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemBidomainTransMemb.hpp"
#include "InputOutput/io.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemBidomainTransMemb::LinearProblemBidomainTransMemb():
    LinearProblem("Problem cardiac Bidomain first equation",3),
    m_fePotTransMemb(nullptr),
    m_fePotTransMembCurv(nullptr),
    m_bdf(nullptr),
    m_fiber(nullptr),
    m_angleFiber(nullptr),
    m_endocardiumDistance(nullptr) {
    if(FelisceParam::instance().monodomain)
      this->setNumberOfMatrix(2);

  }

  LinearProblemBidomainTransMemb::~LinearProblemBidomainTransMemb() {
    m_fstransient = nullptr;
    m_fePotTransMemb = nullptr;
    m_fePotTransMembCurv = nullptr;
    m_bdf = nullptr;
    if (m_fiber)
      delete [] m_fiber;
    if (m_angleFiber)
      delete [] m_angleFiber;
    if (m_endocardiumDistance)
      delete [] m_endocardiumDistance;
  }

  void LinearProblemBidomainTransMemb::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    
    listVariable[0] = potTransMemb;
    listNumComp[0] = 1;
  
    //define unknown of the linear system.
    m_listUnknown.push_back(potTransMemb);
  
    definePhysicalVariable(listVariable,listNumComp);
  }

  void LinearProblemBidomainTransMemb::readData(IO& io) {
    // Read fibers file (*.00000.vct)
    m_fiber = new double[m_mesh[m_currentMesh]->numPoints()*3];
    io.readVariable(0, 0.,m_fiber, m_mesh[m_currentMesh]->numPoints()*3);
    if (FelisceParam::instance().hasCoupledAtriaVent) {
      m_angleFiber = new double[m_numDof];
      io.readVariable(2, 0.,m_angleFiber, m_numDof);
    }
    // Read distance from endocardium file for Zygote geometry (*.00000.scl)
    if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "zygoteImproved") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
      m_endocardiumDistance = new double[m_numDof];
      io.readVariable(1, 0.,m_endocardiumDistance, m_numDof);
    }
  }
  void LinearProblemBidomainTransMemb::getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber) {
    int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
    elemFiber.resize( numSupport*3,0.);
    for (int i = 0; i < numSupport; i++) {
      elemFiber[i*3] = m_fiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
      elemFiber[i*3+1] = m_fiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+1];
      elemFiber[i*3+2] = m_fiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+2];
    }
  }

  void LinearProblemBidomainTransMemb::getAngleFiber(felInt iel, int iUnknown, std::vector<double>& elemAngle) {
    int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
    elemAngle.resize(numSupport,0.);
    for (int i = 0; i < numSupport; i++) {
      elemAngle[i] = m_angleFiber[(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
    }
  }

  void LinearProblemBidomainTransMemb::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    //Init pointer on Finite Element, Variable or idVariable
    m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
    m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
  }

  void LinearProblemBidomainTransMemb::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;
    m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
    m_fePotTransMemb->updateFirstDeriv(0, elemPoint);
    std::vector<double> elemFiber;
    getFiberDirection(iel,m_ipotTransMemb, elemFiber);

    //Bdf coeff not initialized at iteration 0, so need to be added to matrix(at)first iteration.
    double coefTime = 1./m_fstransient->timeStep;
    double coefAm = FelisceParam::instance().Am;
    double coefCm = FelisceParam::instance().Cm;
    double coef = m_bdf->coeffDeriv0()*coefTime*coefAm*coefCm;
    //1st equation : Am*C*/dT V_m
    m_elementMat[0]->phi_i_phi_j(coef,*m_fePotTransMemb,0,0,1);

    //1st equation : \sigma_i^t \grad V_m
    m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

    //1st equation : (\sigma_i^l-\sigma_i^t) a vec a \grad V_m
    m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);

    //1st equation RHS1 : mass RHS
    m_elementMat[1]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);

    if(!FelisceParam::instance().monodomain) {
      //1st equation RHS2 : - \sigma_i^t \grad u_e
      m_elementMat[2]->grad_phi_i_grad_phi_j(-1.*(FelisceParam::instance().intraTransvTensor),*m_fePotTransMemb,0,0,1);

      //1st equation RHS2 : - (\sigma_i^l-\sigma_i^t) a vec a \grad u_e
      m_elementMat[2]->tau_grad_phi_i_tau_grad_phi_j(-1.*(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotTransMemb,0,0,1);
    }

  }


  void LinearProblemBidomainTransMemb::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELEM_ID_POINT;;
    m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
    m_fePotTransMembCurv = m_listCurvilinearFiniteElement[m_ipotTransMemb];

    m_fePotTransMembCurv->updateMeasNormalContra(0, elemPoint);


    if ((m_currentLabel >= 30) && (m_currentLabel <= 40)) {
      std::vector<double> elemAngle;
      getAngleFiber(iel,m_ipotTransMemb,elemAngle);

      std::vector<double> elemFiber;
      getFiberDirection(iel,m_ipotTransMemb,elemFiber);

      // Constant angle
      //double angle = acos(0.0)/2.0;

      // Assembling matrix(0)
      double coefTime = 1./m_fstransient->timeStep;
      double coefAm = FelisceParam::instance().Am;
      double coefCm = FelisceParam::instance().Cm;
      double coef = m_bdf->coeffDeriv0()*coefTime*coefAm*coefCm;

      //1st equation : Am*Cm/dt V_m
      m_elementMatBD[0]->phi_i_phi_j(coef,*m_fePotTransMembCurv,0,0,1);

      //1st equation : \sigma_i^t \grad V_m
      if (m_currentLabel < 40) { // regular atrial myocardium
        m_elementMatBD[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensorAtria,*m_fePotTransMembCurv,0,0,1);
        m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j((FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria),elemFiber,elemAngle,*m_fePotTransMembCurv,0,0,1);
      }


      //Assembling matrix(1) = mass (only first quadrant if hasAppliedExteriorCurrent = false)
      //1st equation RHS1 : mass RHS
      m_elementMatBD[1]->phi_i_phi_j(1.,*m_fePotTransMembCurv,0,0,1);

      if(!FelisceParam::instance().monodomain) {
        //1st equation RHS2 : - \sigma_i^t \grad u_e
        if (m_currentLabel < 40) { // regular atrial myocardium
          m_elementMatBD[2]->grad_phi_i_grad_phi_j(-1.0*(FelisceParam::instance().intraTransvTensorAtria),*m_fePotTransMembCurv,0,0,1);
          m_elementMatBD[2]->tau_orthotau_grad_phi_i_grad_phi_j(-1.0*(FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria),elemFiber,elemAngle,*m_fePotTransMembCurv,0,0,1);
        }
      }
    }
  }

  void LinearProblemBidomainTransMemb::addMatrixRHS() {
    // _RHS = m_mass * m_massRHS
    mult(matrix(1),m_massRHS,vector());

    if(!FelisceParam::instance().monodomain) {
      // _RHS = _RHS + _Ksigmai * _KsigmaiRHS
      multAdd(matrix(2),_KsigmaiRHS,vector(),vector());
    }

    m_massRHS.zeroEntries();
    _KsigmaiRHS.zeroEntries();
  }

}
