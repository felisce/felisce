//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNSSimplifiedFSI.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "Core/felisceTools.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce {
  /** \brief  Constructor of the LinearProblemNSSimplifiedFSI class.

     By default m_autoregulated and m_iopCoupling are setted to false.
     Then actual data are read from FelisceParam.
     ___________________________________________________________________
     Several _PetscVectors_ are created (empty and with no structure: just the pointer)
     - _Parallel_: 
       + displacement, 
       + normalField, 
       + measStar, 
       + meanGradUN,
       + meanCurvature,
       + koiterCoefficients,
       + normalVelocity,
       + maxEigVec (if exportPrinciplaDirectionsAndCurvature is true),
       + minEigVec (if exportPrinciplaDirectionsAndCurvature is true),
       + partition (if exportDofPartition is true)
     ___________________________________________________________________
     - _Sequential_: 
       + displacement,
       + displacementOld,
       + normalField,
       + measStar,
       + meanGradUN,
       + meanCurvature,
       + koiterCoefficients,
       + normalVelocity,
       + normalVelocityOld,
       + displacementCurrent,
       + maxEigVec (if exportPrincipalDirectionsAndCurvature is true),
       + minEigVec (if exportPrincipalDirectionsAndCurvature is true),
       + partition (if exportDofPartition is true)
  */
  LinearProblemNSSimplifiedFSI::LinearProblemNSSimplifiedFSI():LinearProblemNS(),
                                                               m_autoregulated(false),
                                                               m_iopCoupling(false)
  {
    m_nonLinear=FelisceParam::instance().nonLinearKoiterModel?1:0;
    if ( FelisceParam::instance().zeroDisplacementAtBoarders )
      FelisceParam::instance().useEssDerivedBoundaryCondition = true;
    if ( FelisceParam::instance().autoregulated )
      this->autoregulated(true);
    
    m_vecs.Init("displacement");
    m_vecs.Init("normalField");
    m_vecs.Init("measStar");
    m_vecs.Init("meanGradUN");
    m_vecs.Init("meanCurvature");
    m_vecs.Init("koiterCoefficients");
    m_vecs.Init("normalVelocity");
    if ( FelisceParam::instance().exportDofPartition )
      m_vecs.Init("partition");

    m_seqVecs.Init("displacement");
    m_seqVecs.Init("displacementOld");
    m_seqVecs.Init("normalField");
    m_seqVecs.Init("measStar");
    m_seqVecs.Init("meanGradUN");
    m_seqVecs.Init("meanCurvature");
    m_seqVecs.Init("koiterCoefficients");
    m_seqVecs.Init("normalVelocity");
    m_seqVecs.Init("normalVelocityOld");
    m_seqVecs.Init("displacementCurrent");

    if ( FelisceParam::instance().exportDofPartition )
      m_seqVecs.Init("partition");

    if ( FelisceParam::instance().exportPrincipalDirectionsAndCurvature ) {
      m_vecs.Init("maxEigVec");
      m_vecs.Init("minEigVec");
      m_seqVecs.Init("maxEigVec");
      m_seqVecs.Init("minEigVec");
    }
  }

  /** \brief initialize LinearProblem::m_dofBD.

      It is important to call this function when we want to use the utilities in DofBoundary.
      This class allows you to define a mapping between the volume numbering and a new indexing on
      the boundary. This also let you define PetscVector only on the boundary.

      TODO: use these class to redefine all the vectors of this class only on the boundary: this will save a lot of memory
   */
  void
  LinearProblemNSSimplifiedFSI::initializeDofBoundaryAndBD2VolMaps() {
    /*! The object LinearProblem::m_dofBD is initialized. */
    m_dofBD[0/*iBD*/].initialize(this);
    /*! The list of dofs on the boundary, for each variable and for each component are computed. */
    for ( felInt iComp(0); iComp < this->dimension(); ++iComp )
      this->dofBD(/*iBD*/0).buildListOfBoundaryPetscDofs(this, FelisceParam::instance().labelNSMesh, this->iUnknownVel()  , iComp);
    this->dofBD(/*iBD*/0).buildListOfBoundaryPetscDofs(this, FelisceParam::instance().labelNSMesh,  this->iUnknownPre()  , /*iComp*/0);
  }

  /*! \brief \f$\vec u \cdot \vec n \f$ are reset to zero
    
    These quantities are used in IOPCouplModel, use this function
    to std::set them to zero.
   */
  void
  LinearProblemNSSimplifiedFSI::resetInterfaceQuantities() {
    m_seqVecs.Get("normalVelocity").set(0.0);
    m_seqVecs.Get("normalVelocityOld").set(0.0);
  }
  /** \brief  It is a call to handleLumpedModel in #m_autoregulation (Autoregulation class)
     
     @param t current time step

     The parameters of the windkessel model will be updated accordingly to
     what is prescribed in the autoregulation class.
   */
  void
  LinearProblemNSSimplifiedFSI::handleWindkessel(const double& t) {
    m_autoregulation.handleLumpedModel( t, this->lumpedModelBC());
  }

  /*! \brief wrapping of #m_autoregulation function Autoregulation::control()
    
    \param[in] t current time step
    \return the value of the active tension at the current time step
   */
  double
  LinearProblemNSSimplifiedFSI::control(const double& t) {
    return m_autoregulation.control(t);
  }

  /*!
    \brief compute flow and mean incoming pressure for the current time step
    \param[in] time the current time step
    \param[in] rank the id of the current processor
    
    This function is used for post processing
   */
  void
  LinearProblemNSSimplifiedFSI::updateControl(const double& time, const int& rank) {
    std::vector<int> labelInlet = m_label["inflow"];
    std::vector<int> labelOutlet = m_label["outflow"];
    double P(0),Q(0);
    std::vector<double> pout(labelOutlet.size(),0.0);
    if (labelInlet.size() > 0)
    {
      std::vector<double> quantity(labelInlet.size(), 0.0);

      //! The flow rate is computed at the inlet
      this->computeMeanQuantity(velocity, labelInlet, quantity);
      Q = -quantity[0]; //TODO to be changed in case of several inputs

      //! The mean incoming pressure is computed at the inlet
      this->computeMeanQuantity(pressure, labelInlet, quantity);
      P = quantity[0] / Autoregulation::m_cycleDuration;  
    } else {
      std::cout<<__FILE__<<":"<<__LINE__<<" inflow(s) label(s) not defined"<<std::endl;
    }
    if (labelOutlet.size() > 0 ) {
        //! The mean Pressure is computed in all the outlets
        this->computeMeanQuantity(pressure, labelOutlet, pout);
    } else {
      std::cout<<__FILE__<<":"<<__LINE__<<" outlet(s) label(s) not defined"<<std::endl;
    }
    if (labelOutlet.size()*labelInlet.size() > 0 ) {
      /*! The values are saved in #m_autoregulation, that stores
        the values and update the mean values over the cardiac cycle
      */
      m_autoregulation.update(time,Q,P,pout,rank);
    }
  }

  /*!
    \brief wrapping of Autoregulation::exportSummary
    \param[in] rank the id of the current processor
   */
  void
  LinearProblemNSSimplifiedFSI::exportControl(const int& rank) {
    m_autoregulation.exportSummary(rank);
  }

  /*! 
    \brief it initializes #m_autoregulation
    \param[in] nc a number of cardiac cycles
   */
  void 
  LinearProblemNSSimplifiedFSI::initializeAutoregulation( const int& nc) {

    if (m_mesh[m_currentMesh]->bagElementTypeDomainBoundary().size() != 1 ){
      std::cout<<"m_mesh.bagElementTypeDomainBoundary().size() = "<<m_mesh[m_currentMesh]->bagElementTypeDomainBoundary().size()<<std::endl;
      FEL_ERROR("bag element type domain boundary is either to big or empty, pay attention to call this function after the initializeLinearProblem of the model");
    }
    //! the initialize method of #m_autoregulation is called
    m_label = m_autoregulation.initialize(GeometricMeshRegion::eltEnumToFelNameGeoEle[m_mesh[m_currentMesh]->bagElementTypeDomainBoundary()[0]].first);
    /*! the difference between the pressure that will be used for this simulation and the reference one 
      is computed for as many cardiac cycles as specified in the input
     */
    m_autoregulation.computeExtraPressure(nc);
    /*! 
      The value of the control function is exported for post-processing reason
     */
    m_autoregulation.exportControlFunctions(nc);
  }
  	  
  void LinearProblemNSSimplifiedFSI::initPerET() {
    m_curvFeVel   = m_listCurvilinearFiniteElement[m_iVelocity];
    m_curvFePre = m_listCurvilinearFiniteElement[m_iPressure];
  }
  void LinearProblemNSSimplifiedFSI::updateFe(const std::vector<Point*>& elemPoint,const std::vector<int>& /*elemIdPoint*/) {
    m_curvFeVel   ->updateMeasNormal(0, elemPoint);
    m_curvFePre ->updateMeasNormal(0,elemPoint);
  }
 
  
  void LinearProblemNSSimplifiedFSI::computeCurvatures() {
    assemblyLoopBoundary(& LinearProblemNSSimplifiedFSI::curvaturesComputer);
    m_vecs.Get("meanCurvature").assembly();
    m_vecs.Get("koiterCoefficients").assembly();
    pointwiseDivide(m_vecs.Get("meanCurvature"), m_vecs.Get("meanCurvature"), m_vecs.Get("measStar"));
    pointwiseDivide(m_vecs.Get("koiterCoefficients"), m_vecs.Get("koiterCoefficients"), m_vecs.Get("measStar"));
    gatherVector( m_vecs.Get("meanCurvature"), m_seqVecs.Get("meanCurvature") );
    gatherVector( m_vecs.Get("koiterCoefficients"), m_seqVecs.Get("koiterCoefficients") );
    if ( FelisceParam::instance().exportPrincipalDirectionsAndCurvature ) {
      m_vecs.Get("maxEigVec").assembly();
      m_vecs.Get("minEigVec").assembly();      
      pointwiseDivide(m_vecs.Get("maxEigVec"), m_vecs.Get("maxEigVec"), m_vecs.Get("measStar"));
      pointwiseDivide(m_vecs.Get("minEigVec"), m_vecs.Get("minEigVec"), m_vecs.Get("measStar"));
      gatherVector( m_vecs.Get("maxEigVec"), m_seqVecs.Get("maxEigVec") );
      gatherVector( m_vecs.Get("minEigVec"), m_seqVecs.Get("minEigVec") );
    }
  }

  //! \brief Initialize data members of the structure
  void LinearProblemNSSimplifiedFSI::initStructure() {
    /*! 
      When the iopCoupling is used, the vectors 
      for the coupling and for the corresponding iterations 
      are initialized.
      
      Depending on the coupling technique:
      - currentIop (sequential and parallel)
      - x1 (only sequential)
      - x0,fx0,fx1 (only sequential and only for Aitken and IronsTuck
    */
    if ( m_iopCoupling ) {
      m_vecs.Init("currentIop");
      m_seqVecs.Init("currentIop");
      
      this->initFixedPointAcceleration();
    }

    this->initPetscVectors();
   
    if ( FelisceParam::instance().exportDofPartition ) {
      PetscVector aus;
      std::vector<double> dofPartInDouble;
      for (std::size_t idDofPart(0); idDofPart< m_dofPart.size(); idDofPart++)
        dofPartInDouble.push_back(m_dofPart[idDofPart]);
      this->fromDoubleStarToVec(&dofPartInDouble[0], &aus, m_dofPart.size());
      m_vecs.Get("partition").assembly();
      m_vecs.Get("partition").copyFrom(aus);
      gatherVector(m_vecs.Get("partition"),m_seqVecs.Get("partition"));
    }
    //! Structure data and other parameters are read from FelisceParam
    m_deltaT     = m_fstransient->timeStep;
    m_widthS     = FelisceParam::instance().widthFiber + FelisceParam::instance().widthShell;
    m_poissonS   = FelisceParam::instance().poissonShell;
    m_youngS     = FelisceParam::instance().youngShell;
    m_betaS      = m_youngS* FelisceParam::instance().widthShell/(1.0 - m_poissonS*m_poissonS);
    m_densityS   = FelisceParam::instance().densityStructure;
    m_viscS      = FelisceParam::instance().viscosityStructure;
    m_referencePressure = FelisceParam::instance().outsidePressure;
    m_alphaS     = m_widthS * m_densityS / m_deltaT;
    m_fiberTensPassive  = FelisceParam::instance().preStressFibers;
    m_fiberYoung = FelisceParam::instance().youngFibers;
    m_fiberDensity = FelisceParam::instance().fiberDensity;
    if ( FelisceParam::verbose() > 1 ) {
      std::stringstream out;
      out << " fibers parameters-->  ";
      out << " tension: "<<m_fiberTensPassive;
      out << " Young:   "<<m_fiberYoung<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",out.str().c_str());
    }

    for (std::size_t iEmbedFSI=0; iEmbedFSI < m_boundaryConditionList.numEmbedFSIBoundaryCondition(); iEmbedFSI++ ) {
      BoundaryCondition* BC = m_boundaryConditionList.EmbedFSI(iEmbedFSI);
      for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
        m_embedLabels.push_back(*it_labelNumber);
      }
    }
    if(m_fiberDensity.size() == 0){
      // No fiberDensity was provided in the data file
      // We create a fiberDensity equal to 1. on all the labels where there is a BC of EmbedFSI type
      m_fiberDensity.resize(m_embedLabels.size());
      for(std::size_t i=0; i<m_fiberDensity.size();i++){
        m_fiberDensity[i] = 1.;
      }
    }
    if(m_fiberDensity.size() != m_embedLabels.size()){
      std::cout << "m_fiberDensity.size()  = " << m_fiberDensity.size() << std::endl;
      std::cout << "m_embedLabels = " << m_embedLabels.size() << std::endl;
      std::cout << "Error in " << __FILE__ << ": the number of values provided in the data file for fiberDensity should be the same as the number of EmbedFSI labels" << std::endl;
      exit(1);
    }
    for(std::size_t itFiberDens=0; itFiberDens<m_fiberDensity.size(); itFiberDens++){
      m_label2embedFSI[m_embedLabels[itFiberDens]] = itFiberDens;
    }

    this->computeNormalField(m_embedLabels, m_iVelocity);
    /*! 
      In a 3D problem the curvatures are computed by calling computeCurvatures() method.\n
      In 2D the #m_radius is std::set from FelisceParam
     */
    switch (this->dimension()) {
    case 1:
      FEL_ERROR("this structural model is not supposed to work on points")
        break;
    case 2:
      m_radius = FelisceParam::instance().radius;
      break;
    case 3:
      computeCurvatures();
      break;
    default:
      FEL_ERROR("strange dimension parameter");
    }
  }
  
  /*! \brief It updates the normalVelocity and displacementCurrent.
   */
  void LinearProblemNSSimplifiedFSI::updateNormalVelocityAndCurrentDisplacement() {
    //! normalVelocityOld (seq) = normalVelocity (seq), normalVelocity is reset to zero.
    m_seqVecs.Get("normalVelocityOld").copyFrom(m_seqVecs.Get("normalVelocity"));
    m_vecs.Get("normalVelocity").set(0.0); //TODO remove!
    //! normalVelocity (para) = solution(para) scalar normalField(para), normalVelocity is gathered
    pointwiseMult(m_vecs.Get("normalVelocity"),this->solution(), m_vecs.Get("normalField"));
    gatherVector(m_vecs.Get("normalVelocity"), m_seqVecs.Get("normalVelocity"));
    /*! The results of this operation is still in the form of a std::vector: its components are sum up and stored in the pressure block
      by using computeScalarNormalVelocity()
     */
    computeScalarNormalVelocity();
    //! displacementCurrent = displacement + deltaT * normalVelocity (all sequential)
    m_seqVecs.Get("displacementCurrent").copyFrom(m_seqVecs.Get("displacement"));
    m_seqVecs.Get("displacementCurrent").axpy(m_deltaT,m_seqVecs.Get("normalVelocity"));
  }
  /*! \brief It updates the displacement
    TODO change the name of this function
   */
  void LinearProblemNSSimplifiedFSI::updateStructure() {
    gatherSolution();
    //! A P1 approximation of gradient of the velocity in the normal direction is computed
    this->computeMeanGradUN();      
    //! displacementOld=displacement (sequential)
    m_seqVecs.Get("displacementOld").copyFrom(m_seqVecs.Get("displacement"));
    //! It updates the normalVelocity and displacementCurrent.
    this->updateNormalVelocityAndCurrentDisplacement();
    /*! 
      The variable displacement is computed:\n
      displacement is defined as vectorial variable, like the velocity, but it is a scalar defined as the sum of its (fake) components, this is done to avoid complications in parallel
      so we first do the pointwise multiplication between the solution and the normal field to obatin the normal velocity then we multiply it by the time step and we add it to the displacement.\n
      After that the function computeScalarDisplacement() is called to store the scalar displacement in the pressure block and also to compute its minimum and its maximum for post-process.
      TODO: it is redundant, I think we should use displacementCurrent.
    */
    m_vecs.Get("displacement").axpy(m_deltaT, m_vecs.Get("normalVelocity"));
    gatherVector( m_vecs.Get("displacement"), m_seqVecs.Get("displacement"));
    double maxD(0),minD(0), gMaxD(0), gMinD(0);
    computeScalarDisplacement(maxD,minD);
    MPI_Reduce(&maxD, &gMaxD, 1,   MPI_DOUBLE, MPI_MAX, 0, MpiInfo::petscComm());
    MPI_Reduce(&minD, &gMinD, 1,   MPI_DOUBLE, MPI_MIN, 0, MpiInfo::petscComm());
    std::stringstream s1,s2;
    s1 << " Maximum displacement [micron]: "<< gMaxD * 1.e4 <<std::endl;
    s2 << " Minimum displacement [micron]: "<< gMinD * 1.e4 <<std::endl;
    PetscPrintf(MpiInfo::petscComm(), "%s",s1.str().c_str());
    PetscPrintf(MpiInfo::petscComm(), "%s",s2.str().c_str());
  }
  
  void LinearProblemNSSimplifiedFSI::userElementInitNaturalBoundaryCondition() {
    if ( this->dimension() == 1 )
      FEL_ERROR("this structural model is not supposed to work on points");
    
    m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
    m_curvFePre = m_listCurvilinearFiniteElement[m_iPressure];
    
    m_displacementEF     .initialize( DOF_FIELD, *m_curvFePre, 1 );        //current displacement
    m_displacementOldEF  .initialize( DOF_FIELD, *m_curvFePre, 1 );        //old     displacement
    m_gradUNEF           .initialize( DOF_FIELD, *m_curvFeVel, this->dimension() );               //gradient of the normal velocity
    m_TgradUNEF          .initialize( DOF_FIELD, *m_curvFeVel, this->dimension() );              //gradient of the normal velocity projected onto the tangent plane
    m_iopEF              .initialize( DOF_FIELD, *m_curvFePre, 1 );
    m_normalFieldEF      .initialize( DOF_FIELD, *m_curvFeVel, this->dimension() );
    
    if ( this->dimension() == 3 ) {
      m_meanCurvEF           .initialize( DOF_FIELD, *m_curvFeVel, 3 ); //1: mean curv; 2: gauss curv; 3: 2*m_meanCurv^2-m_gaussCurv;
      m_koiterCoefficientsEF .initialize( DOF_FIELD, *m_curvFeVel, 3 ); //c_1,c_2,c_3
      m_firstLapCoeffEF      .initialize( DOF_FIELD, *m_curvFeVel, 1 );
      m_secondLapCoeffEF     .initialize( DOF_FIELD, *m_curvFeVel, 1 );
      m_thirdLapCoeffEF      .initialize( DOF_FIELD, *m_curvFeVel, 1 );
      m_minLapFibersCoeffEF  .initialize( DOF_FIELD, *m_curvFeVel, 1 );
      m_maxLapFibersCoeffEF  .initialize( DOF_FIELD, *m_curvFeVel, 1 );
      m_etagradunEF          .initialize(DOF_FIELD, *m_curvFeVel, m_curvFeVel->numCoor() );
    }
  }
  
  void LinearProblemNSSimplifiedFSI::userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/,felInt& iel, int label) {
    BoundaryCondition* BC;
    for (std::size_t iEmbedFSI=0; iEmbedFSI<m_boundaryConditionList.numEmbedFSIBoundaryCondition(); iEmbedFSI++ ) { //loop on all the surface regions that has the EmbedFSI BC
      BC = m_boundaryConditionList.EmbedFSI(iEmbedFSI);//get the the BC
      if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),label) != BC->listLabel().end() ) {
        double dispValue, dispValueOld, springCoeff;
        m_curvFeVel   ->updateMeas(0,elemPoint);
        m_curvFePre ->updateMeas(0,elemPoint);
        m_normalFieldEF    .setValue( m_seqVecs.Get("normalField")     , *m_curvFeVel  , iel, m_iVelocity, m_ao, dof());
        m_displacementEF   .setValue( m_seqVecs.Get("displacement")    , *m_curvFePre, iel, m_iPressure, m_ao, dof());
        m_displacementOldEF.setValue( m_seqVecs.Get("displacementOld") , *m_curvFePre, iel, m_iPressure, m_ao, dof());
        m_gradUNEF         .setValue( m_seqVecs.Get("meanGradUN")      , *m_curvFeVel  , iel, m_iVelocity, m_ao, dof());
        computeTangentComponent(); //m_TgradUNEF
        if ( m_iopCoupling )
          m_iopEF.setValue( m_seqVecs.Get("currentIop"), *m_curvFePre, iel, m_iPressure, m_ao, dof());

        switch ( this->dimension() ) {
        case 2:
          { //======================================================================//
            // in 2D we implemented a much simpler model, 
            // basically a first order transpiration + a linear koiter model with no fibers
            //
            //================================ Variables declaration================//
            /*
              betaS       = E*h/(1 - nu^2);
              alphaS      = rho h  / deltaT;
            */            
            for ( int iLocDof(0); iLocDof < m_curvFeVel->numDof(); iLocDof++) {
              dispValue = m_displacementEF.val(0,iLocDof);
              dispValueOld = m_displacementOldEF.val(0,iLocDof);

              //===================================================================================
              //  \mathcal{J}_1
              springCoeff =  m_betaS / ( std::pow(m_radius,2) );

              m_elemFieldAlphaForEmbedFSI[iEmbedFSI].val(0,iLocDof) = springCoeff * m_deltaT + m_alphaS + m_viscS;
              for ( int iCoor(0); iCoor < m_curvFeVel->numCoor(); iCoor++) {
                m_elemFieldEmbedFSI[iEmbedFSI].val(iCoor, iLocDof) = 
                  m_normalFieldEF.val(iCoor,iLocDof) * (   
                                                    - ( springCoeff - m_alphaS/m_fstransient->timeStep ) * dispValue // (-J_1 + rho h /delta T^2)\eta^k 
                                                    - m_alphaS/m_fstransient->timeStep * dispValueOld 
                                                    - m_referencePressure // -p^{ref}
                                                    - m_iopEF.val(0,iLocDof)
                                                       ) //normal direction: displacement equation
                  - m_elemFieldAlphaForEmbedFSI[iEmbedFSI].val(0,iLocDof) * dispValue * m_TgradUNEF.val(iCoor,iLocDof); //tangent direction: transpiration
              }
            }
            m_elemFieldNormalForEmbedFSI[iEmbedFSI].setValue( m_seqVecs.Get("normalField"), *m_curvFeVel, iel, m_iVelocity, m_ao, dof());
          }
          break;
        case 3:
          {
            ElementField currentNormalVelocity;
            currentNormalVelocity.initialize( DOF_FIELD, *m_curvFePre, 1 );
            currentNormalVelocity.setValue(m_seqVecs.Get("normalVelocity")    , *m_curvFePre, iel, m_iPressure, m_ao, dof());

            if ( FelisceParam::instance().switchSimplFSIOff ) {
              for ( int iLocDof(0); iLocDof < m_curvFeVel->numDof(); iLocDof++) {    
                m_elemFieldAlphaForEmbedFSI[iEmbedFSI].val(0,iLocDof) = .1;
                for ( int iCoor(0); iCoor < m_curvFeVel->numCoor(); iCoor++) {
                  m_elemFieldEmbedFSI[iEmbedFSI].val(iCoor, iLocDof) = m_normalFieldEF.val(iCoor,iLocDof) * (- m_referencePressure
                                                                                                             -m_iopEF.val(0,iLocDof)
                                                                                                             +m_elemFieldAlphaForEmbedFSI[iEmbedFSI].val(0,iLocDof)*currentNormalVelocity.val(0,iLocDof)
                                                                                                             );
                }
              }
              m_elemFieldNormalForEmbedFSI[iEmbedFSI].setValue( m_seqVecs.Get("normalField"), *m_curvFeVel, iel, m_iVelocity, m_ao, dof());        
            } else {              
            //================================ Variables declaration================//
            double normGradDisp, cNormGradDisp, cSquaredNormGradDisp, normGradDispMin, normGradDispMax, extraTension;
            m_curv.update(m_normalFieldEF, m_curvFeVel->covMetric[0], m_curvFeVel->m_covBasis[0]);
            /*
              betaS       = E*h/(1 - nu^2);
              alphaS      = rho h  / deltaT;
              fiberTens   = k_0(S);
              fiberYoung  = k_1;
            */
            // -k_0(\bar S) + k_0(S)
            extraTension =  - ( m_autoregulated ? control(m_fstransient->time): 0. );
            
            
            
            double fiberTensDensity = (m_fiberTensPassive + extraTension) * m_fiberDensity[ m_label2embedFSI[label] ];
              double fiberYoungDensity = m_fiberYoung * m_fiberDensity[m_label2embedFSI[label]];
            for ( int iLocDof(0); iLocDof < m_curvFeVel->numDof(); iLocDof++) {
              for ( int ausComp(0); ausComp < m_curvFeVel->numCoor(); ausComp++) {
                m_etagradunEF.val(ausComp,iLocDof) = m_displacementEF.val(0,iLocDof)*m_gradUNEF.val(ausComp,iLocDof);
              }
            }

            // read the coefficients of the model
            m_koiterCoefficientsEF.setValue( m_seqVecs.Get("koiterCoefficients"), *m_curvFeVel, iel, m_iVelocity, m_ao, dof());
            m_meanCurvEF.setValue( m_seqVecs.Get("meanCurvature"), *m_curvFeVel, iel, m_iVelocity, m_ao, dof());
        
            // compute the squared norm of the gradient
            normGradDisp  = Curvatures::mNormOfGradient(m_displacementEF, m_curv.invMetricTensor() );
            // compute the squared norm of the gradient weigthed by C
            cNormGradDisp = Curvatures::mNormOfGradient(m_displacementEF, m_curv.cMatrix() );
            // compute the squared norm of the gradient weigthed by C^2
            cSquaredNormGradDisp = Curvatures::mNormOfGradient(m_displacementEF, m_curv.cSquaredMatrix() );
            // compute the squared norm of the gradient in the min direction
            normGradDispMin = Curvatures::mNormOfGradient(m_displacementEF, m_curv.minProjTensor() );
            // compute the squared norm of the gradient in the max direction
            normGradDispMax = Curvatures::mNormOfGradient(m_displacementEF, m_curv.maxProjTensor());
            
            for ( int iLocDof(0); iLocDof < m_curvFeVel->numDof(); iLocDof++) {
              dispValue = m_displacementEF.val(0,iLocDof);
              dispValueOld = m_displacementOldEF.val(0,iLocDof);

              //===================================================================================
              //  \mathcal{J}_1
              // cf. Articles_Aletti/SimplifiedFSImodel and autoregulation/Contents/nonlinearkoiter.pdf p.5 (discretization in time)
              springCoeff = 0.5 * m_betaS * (                                                               
                                             2.0 * m_koiterCoefficientsEF.val(0,iLocDof)                                                //  2c_1
                                             - 3.0 * m_koiterCoefficientsEF.val(1,iLocDof) * dispValue * m_nonLinear                    // -3c_2 \eta 
                                             + 4.0 * m_koiterCoefficientsEF.val(2,iLocDof) * std::pow(dispValue,2) * m_nonLinear        //  4c_3 \eta^2
                                             + 2.0 * m_poissonS * m_meanCurvEF.val(2,iLocDof) * normGradDisp * m_nonLinear              //  2\nu(2\rho_1^2-\rho_2)||\nabla\eta||^2
                                             + (1-m_poissonS) * cSquaredNormGradDisp * m_nonLinear                                  //  (1-\nu)||\nabla\eta||^2_{C^2}
                                                                                                            )
                +       (m_curv.coeffFibersLinearMin() + m_curv.coeffFibersLinearMax())*fiberTensDensity               // This two lines should be more different if fibers were not aligned with curv dir
                +       (m_curv.coeffFibersLinearMin() + m_curv.coeffFibersLinearMax())*fiberYoungDensity              // This two lines should be more different if fibers were not aligned with curv dir
                +   0.5*(m_curv.coeffFibersLinearMin() * normGradDispMin + m_curv.coeffFibersLinearMax() * normGradDispMin)*fiberYoungDensity    //ok
                - 3./2.*(m_curv.coeffFibersQuadraticMin() + m_curv.coeffFibersCubicMax())*fiberYoungDensity*dispValue                            // ok
                +   0.5*(m_curv.coeffFibersQuadraticMin() + m_curv.coeffFibersCubicMax())*fiberYoungDensity*std::pow(dispValue,2);               // ok
              //TODO we still do not compute the mean value in the nodes of the mesh for fibers coefficients 

              //===================================================================================
              // all these quantities called LapCoeff refers to $\tens J(\eta,\nabla\eta)$
              // for $\tens I$
              m_firstLapCoeffEF.val(0,iLocDof) =
                - 4.0 * m_poissonS * m_meanCurvEF.val(0,iLocDof) * dispValue
                + 2.0 * m_poissonS * m_meanCurvEF.val(2,iLocDof) * std::pow(dispValue,2)
                + normGradDisp;
          
              // for $\tens C$
              m_secondLapCoeffEF.val(0,iLocDof) = - 2.0 * (1-m_poissonS) * dispValue;

              // for $\tens C^2
              m_thirdLapCoeffEF.val(0,iLocDof) = (1-m_poissonS) * std::pow(dispValue,2);

              // for $\varrho_{min} \tens P_{min}$
              m_minLapFibersCoeffEF.val(0,iLocDof) = 
                fiberTensDensity 
                - m_curv.minCurv() * dispValue * fiberYoungDensity
                + 0.5 * std::pow( m_curv.minCurv() * dispValue, 2) * fiberYoungDensity
                + 0.5 * normGradDispMin * fiberYoungDensity;

              // for $\varrho_{max} \tens P_{max}$          
              m_maxLapFibersCoeffEF.val(0,iLocDof) = 
                fiberTensDensity 
                - m_curv.maxCurv() * dispValue * fiberYoungDensity
                + 0.5 * std::pow( m_curv.maxCurv() * dispValue, 2) * fiberYoungDensity
                + 0.5 * normGradDispMax * fiberYoungDensity;

              m_elemFieldAlphaForEmbedFSI[iEmbedFSI].val(0,iLocDof) = springCoeff * m_deltaT + m_alphaS + m_viscS; // ok
              //===================================================================================
              for ( int iCoor(0); iCoor < m_curvFeVel->numCoor(); iCoor++) {
                //TODO the terms \Delta t \nabla_nn u^k are missing 
                m_elemFieldEmbedFSI[iEmbedFSI].val(iCoor, iLocDof) = //in the paper this is called \vec l^{k,k-1}
                  m_normalFieldEF.val(iCoor,iLocDof) * (   
                                                    - ( springCoeff - m_alphaS/m_fstransient->timeStep ) * dispValue // (-J_1 + rho h /delta T^2)\eta^k 
                                                    - m_alphaS/m_fstransient->timeStep * dispValueOld // ok
                                                    - m_referencePressure // -p^{ref}
                                                    - m_iopEF.val(0,iLocDof)
                                                    + 0.5 * m_betaS * ( 1.0 - m_poissonS ) * cNormGradDisp * m_nonLinear // -j0
                                                    + 0.5 * m_betaS * 2.0 * m_poissonS * m_meanCurvEF.val(0,iLocDof) * normGradDisp * m_nonLinear //-jo 
                                                    + normGradDispMin * ( 0.5 * m_curv.coeffFiberProportionMin() * m_curv.minCurv() ) * fiberYoungDensity //-jo
                                                    + normGradDispMax * ( 0.5 * m_curv.coeffFiberProportionMax() * m_curv.maxCurv() ) * fiberYoungDensity //-jo
                                                    - extraTension * ( m_curv.coeffFiberProportionMin() * m_curv.minCurv() +  m_curv.coeffFiberProportionMax() * m_curv.maxCurv()) // -jo
                                                       ) //normal direction: displacement equation
                  - m_elemFieldAlphaForEmbedFSI[iEmbedFSI].val(0,iLocDof) * dispValue * m_TgradUNEF.val(iCoor,iLocDof); //tangent direction: transpiration
              }


            }
            m_elemFieldNormalForEmbedFSI[iEmbedFSI].setValue( m_seqVecs.Get("normalField"), *m_curvFeVel, iel, m_iVelocity, m_ao, dof());          

        
            if ( FelisceParam::instance().nonLinearKoiterModel ) {
              // first laplacian contribution
              m_elementVectorBD[0]->sGrad_phi_i_tensor_sGrad_f( -FelisceParam::instance().penValueForEmbedFSI * 0.5 * m_betaS, m_firstLapCoeffEF, m_displacementEF, m_normalFieldEF, *m_curvFeVel, m_curv.invMetricTensor() );
              m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i( FelisceParam::instance().penValueForEmbedFSI * 0.5 * m_betaS * m_deltaT, m_firstLapCoeffEF, m_normalFieldEF, *m_curvFeVel, m_curv.invMetricTensor() );

              // second laplacian contribution
              m_elementVectorBD[0]->sGrad_phi_i_tensor_sGrad_f( -FelisceParam::instance().penValueForEmbedFSI * 0.5 * m_betaS, m_secondLapCoeffEF, m_displacementEF, m_normalFieldEF, *m_curvFeVel, m_curv.cMatrix() );
              m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i( FelisceParam::instance().penValueForEmbedFSI * 0.5 * m_betaS * m_deltaT, m_secondLapCoeffEF, m_normalFieldEF, *m_curvFeVel, m_curv.cMatrix() );

              // third laplacian contribution
              m_elementVectorBD[0]->sGrad_phi_i_tensor_sGrad_f( -FelisceParam::instance().penValueForEmbedFSI * 0.5 * m_betaS, m_thirdLapCoeffEF, m_displacementEF, m_normalFieldEF, *m_curvFeVel, m_curv.cSquaredMatrix() );
              m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i( FelisceParam::instance().penValueForEmbedFSI * 0.5 * m_betaS * m_deltaT, m_thirdLapCoeffEF, m_normalFieldEF, *m_curvFeVel, m_curv.cSquaredMatrix() );
            }
        
            // laplacian terms due to fibers
            // min direction
            m_elementVectorBD[0]->sGrad_phi_i_tensor_sGrad_f( -FelisceParam::instance().penValueForEmbedFSI, m_minLapFibersCoeffEF, m_displacementEF, m_normalFieldEF, *m_curvFeVel, m_curv.minFiberTensor() );
            m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i( FelisceParam::instance().penValueForEmbedFSI * m_deltaT, m_minLapFibersCoeffEF, m_normalFieldEF, *m_curvFeVel, m_curv.minFiberTensor());

            // max direction
            m_elementVectorBD[0]->sGrad_phi_i_tensor_sGrad_f( -FelisceParam::instance().penValueForEmbedFSI, m_maxLapFibersCoeffEF, m_displacementEF, m_normalFieldEF, *m_curvFeVel, m_curv.maxFiberTensor() );
            m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i( FelisceParam::instance().penValueForEmbedFSI * m_deltaT, m_maxLapFibersCoeffEF, m_normalFieldEF, *m_curvFeVel, m_curv.maxFiberTensor());

            double stabParam = FelisceParam::instance().tssParam;
            m_elementMatBD[0]->transpirationSurfStab( FelisceParam::instance().penValueForEmbedFSI * stabParam, m_elemFieldAlphaForEmbedFSI[iEmbedFSI], m_normalFieldEF, *m_curvFeVel, m_curv.invMetricTensor());
            if ( FelisceParam::instance().tssConsistent )
              m_elementVectorBD[0]->transpirationSurfStabRHS( - FelisceParam::instance().penValueForEmbedFSI * stabParam, m_elemFieldAlphaForEmbedFSI[iEmbedFSI], m_normalFieldEF, m_etagradunEF, *m_curvFeVel, m_curv.invMetricTensor());       
          }
          }
          break;
        default:
          FEL_ERROR("strange dimension parameter");
        } 
      }
    }
  }

  void LinearProblemNSSimplifiedFSI::computeTangentComponent() {
    for ( int iLocDof(0); iLocDof < m_curvFeVel->numDof(); iLocDof++) {
      for ( int iCoor(0); iCoor < m_curvFeVel->numCoor(); iCoor++) {
        m_TgradUNEF.val(iCoor,iLocDof)=0.0;
        for ( int iCoorAus(0); iCoorAus < m_curvFeVel->numCoor(); iCoorAus++) {
          // kronecker delta
          double delta=0.0;
          if( iCoor==iCoorAus )
            delta=1.0;
          m_TgradUNEF.val(iCoor,iLocDof) += ( delta - m_normalFieldEF.val(iCoor,iLocDof) * m_normalFieldEF.val(iCoorAus,iLocDof) ) * m_gradUNEF.val( iCoorAus, iLocDof );
        }
      }
    }
  }

  void LinearProblemNSSimplifiedFSI::initPerETWBD() {
    m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
    m_curWBDFeVel = m_listCurrentFiniteElementWithBd[m_iVelocity];
  }
  
  void LinearProblemNSSimplifiedFSI::updateFeWBD(const std::vector<Point*>& elemPoint,const std::vector<felInt>& elemIdPoint) {
    std::vector<Point*> elemPointVol;
    std::vector<felInt> elemIdPointVol;
    m_curvFeVel->updateMeasNormal(0, elemPoint);
    m_mesh[m_currentMesh]->getElementFromBdElem(elemIdPoint, elemIdPointVol, /* not used */elemPointVol, /*idLocFace*/m_auxiliaryInts[0], /*idElemVol*/m_auxiliaryInts[1]); 
    m_curWBDFeVel->updateFirstDeriv(0, elemPointVol);
    m_curWBDFeVel->updateBdMeasNormal();
  }
  void LinearProblemNSSimplifiedFSI::meanGradUNComputer(felInt ielSupportDof){
    double value;
    for ( std::size_t iCoor(0); iCoor < ( std::size_t ) m_curvFeVel->numCoor(); iCoor++) {
      //it is a std::vector with dimension = num of quad points on the bd elem
      std::vector<double> aus;
      this->computeGradUN( m_curWBDFeVel, m_auxiliaryInts[1], m_iVelocity, aus, m_auxiliaryInts[0], iCoor);

      double feMeas( m_curvFeVel->measure() );
      value = aus[0] * feMeas; //TODO here you should integrate to be more general (for P1 it is ok..), in the case of P1 the gradient is constant over the element
      for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFeVel->numDof(); iSurfDof++) {
        felInt iDofGlob;
        dof().loc2glob( ielSupportDof, iSurfDof, m_iVelocity, iCoor, iDofGlob);
        AOApplicationToPetsc(m_ao,1,&iDofGlob);
        m_vecs.Get("meanGradUN").setValue(iDofGlob, value, ADD_VALUES);
      }  
    }
  }
  void LinearProblemNSSimplifiedFSI::computeMeanGradUN() {
    m_vecs.Get("meanGradUN").set(0.0);
    m_seqVecs.Get("meanGradUN").set(0.0);
    m_auxiliaryInts.resize(2);
    if ( this->dimension() == 3 )
     m_mesh[m_currentMesh]->buildFaces();
    else
     m_mesh[m_currentMesh]->buildEdges();

    //TODO save this list of labels!
    std::vector<felInt> labels;
    for (std::size_t iEmbedFSI=0; iEmbedFSI < m_boundaryConditionList.numEmbedFSIBoundaryCondition(); iEmbedFSI++ ) {
      BoundaryCondition* BC = m_boundaryConditionList.EmbedFSI(iEmbedFSI);
      for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++) {
        labels.push_back(*it_labelNumber);
      }
    }
    
    this->assemblyLoopBoundaryGeneral(&LinearProblemNSSimplifiedFSI::meanGradUNComputer,labels,&LinearProblemNSSimplifiedFSI::initPerETWBD,&LinearProblemNSSimplifiedFSI::updateFeWBD);
    m_vecs.Get("meanGradUN").assembly();
    pointwiseDivide(m_vecs.Get("meanGradUN"), m_vecs.Get("meanGradUN"), m_vecs.Get("measStar"));
    gatherVector( m_vecs.Get("meanGradUN"), m_seqVecs.Get("meanGradUN") );
  }

  void LinearProblemNSSimplifiedFSI::computeGradUN( CurrentFiniteElementWithBd* feVol, felInt iel, int idVariable, std::vector<double> &grad, int ibdFace, int icomp) {
    grad.clear();
    grad.resize( feVol->numQuadPointBdEle(ibdFace), 0. );

    felInt numCoor = feVol->numCoor(); // dimension of the space, (3).
    felInt numDof  = feVol->numDof(); // number of dof on the volume
    felInt idDof; // the index of the Dof in the global numbering
    std::size_t iQuadVol;

    for( std::size_t ilocg = 0; ilocg < grad.size(); ilocg++) {
      iQuadVol = ilocg + feVol->indexQuadPoint( ibdFace + 1 ); // face ibd starts at ibd+1 (0 for internal quad points)

      for ( int idof = 0; idof < numDof; idof++) {
        double solValue;//the value
        dof().loc2glob( iel, idof, idVariable, icomp, idDof);
        AOApplicationToPetsc( m_ao, 1, &idDof);
        this->sequentialSolution().getValues(1, &idDof, &solValue);
        for ( int iauscomp = 0; iauscomp < numCoor; iauscomp++) {
          grad[ilocg] += solValue * feVol->dPhi[ iQuadVol ]( iauscomp, idof ) * feVol->bdEle( ibdFace ).normal[ ilocg ]( iauscomp );
        }
      }
    }
  }
  
  void LinearProblemNSSimplifiedFSI::applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) {
    if ( FelisceParam::instance().zeroDisplacementAtBoarders ) {
      if (FelisceParam::verbose() > 2)
        std::cout<<"["<<rank<<"]--Applying zero-Dirichlet BCs on the velocity at the interface between inlet outlet and surface."<<std::endl;
      getRings();
      double bigValue=FelisceParam::instance().penValueForEmbedFSI*1e10;
      
      for(auto itDofBC = m_idDofRings.begin(); itDofBC != m_idDofRings.end(); ++itDofBC) { 
        if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix))
          matrix(0).setValue(*itDofBC, *itDofBC, bigValue, INSERT_VALUES);
        if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) 
          vector().setValue(*itDofBC,0.0, INSERT_VALUES);
      }
    }
  }

  void LinearProblemNSSimplifiedFSI::curvaturesComputer(felInt ielSupportDof) {
    ElementField normal;

    normal.initialize( DOF_FIELD, *m_curvFeVel, m_curvFeVel->numCoor() );
    normal.setValue( m_seqVecs.Get("normalField"), *m_curvFeVel, ielSupportDof, m_iVelocity, m_ao, dof());
    m_curv.update(normal, m_curvFeVel->covMetric[0], m_curvFeVel->m_covBasis[0]);

    // The curvatures have been computed in the element, but we want to define them on the nodes, computing a mean over the elements sharing that node
    // We use a std::vector field, where each component stores a different thing
    double ausMeanCurvature,ausKoiterCoeff,currCompMinEigV,currCompMaxEigV;
    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFeVel->numDof(); iSurfDof++) {
      for ( std::size_t iCoor(0); iCoor <  ( std::size_t ) m_curvFeVel->numCoor(); iCoor++) {
        felInt iDofGlob;
        dof().loc2glob( ielSupportDof, iSurfDof, m_iVelocity, iCoor, iDofGlob);
        AOApplicationToPetsc(m_ao,1,&iDofGlob);
        ausMeanCurvature=0.0;
        ausKoiterCoeff=0.0;
        currCompMaxEigV=0;
        currCompMinEigV=0;
        for ( std::size_t iQuadBd(0.0) ; iQuadBd <  ( std::size_t )m_curvFeVel->numQuadraturePoint(); iQuadBd++) {
          if ( FelisceParam::instance().exportPrincipalDirectionsAndCurvature ) {
            currCompMaxEigV += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.maxEigVCartComp(iCoor);
            currCompMinEigV += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.minEigVCartComp(iCoor);
          }
          if ( iCoor == 0 ) {
            ausMeanCurvature += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.meanCurv();// \rho_1
            ausKoiterCoeff += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.coeffKoiterLinear(m_poissonS);// C_1
          } else if ( iCoor == 1 ) {
            ausMeanCurvature += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.gaussCurv();// \rho_2
            ausKoiterCoeff += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.coeffKoiterQuadratic(m_poissonS);// C_2
          } else if ( iCoor == 2 ) {
            ausMeanCurvature += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.coeffKoiterMeanOfSquaredCurvatures();// 2*std::pow(m_meanCurv,2)-m_gaussCurv;
            ausKoiterCoeff += m_curvFeVel->weightMeas( iQuadBd ) * m_curv.coeffKoiterCubic(m_poissonS);// C_3
          }
        }
        m_vecs.Get("meanCurvature").setValue(iDofGlob, ausMeanCurvature, ADD_VALUES);
        m_vecs.Get("koiterCoefficients").setValue(iDofGlob, ausKoiterCoeff, ADD_VALUES);
        if ( FelisceParam::instance().exportPrincipalDirectionsAndCurvature ) {
          m_vecs.Get("maxEigVec").setValue(iDofGlob, currCompMaxEigV, ADD_VALUES);
          m_vecs.Get("minEigVec").setValue(iDofGlob, currCompMinEigV, ADD_VALUES);
        }
      }
    }
  }
  
  /*!
    \brief It compute the displacement as a scalar.
    
    \param[out] max, the maximum of the displacement
    \param[in] min, the minimum of the displacement

    The computation is done via assemblyLoopBoundary() and scalarComputer().

    The displacement is the integral over time of the scalar product of the velocity times the normal.\n
    Therefore it is first stored as std::vector. The i-th component of such std::vector contain \f$ v_i n_i\f$.
    This function sum the components and store them into the pressure block of displacement (both para and seq).\n
    At the same tame also the max and min of the displacement are computed.
   */
  void LinearProblemNSSimplifiedFSI::computeScalarDisplacement(double& max, double& min) {
    m_auxiliaryDoubles.clear();
    m_auxiliaryDoubles.push_back(-1.e30);
    m_auxiliaryDoubles.push_back(1.e30);

    assemblyLoopBoundary(&LinearProblemNSSimplifiedFSI::scalarComputer);
    m_vecs.Get("displacement").assembly();
    
    gatherVector(m_vecs.Get("displacement"),m_seqVecs.Get("displacement"));
    
    max=m_auxiliaryDoubles[0];
    min=m_auxiliaryDoubles[1];
  }

  /*!
    \brief It compute the normalVelocity as a scalar
    
    As the displacement also the normalVelocity is a scalar product of the velocity times the normal.\n
    Therefore it is first stored as std::vector. The i-th component of such std::vector contain \f$ v_i n_i\f$.
    This function sum the components and store them into the pressure block of normalVelocity (both para and seq).
   */
  void LinearProblemNSSimplifiedFSI::computeScalarNormalVelocity() {
    assemblyLoopBoundary(&LinearProblemNSSimplifiedFSI::normalVelocityScalarComputer);
    m_vecs.Get("normalVelocity").assembly();
    gatherVector(m_vecs.Get("normalVelocity"),m_seqVecs.Get("normalVelocity"));    
  }
  
  double LinearProblemNSSimplifiedFSI::computeL2Difference(std::string current, std::string old) {
    if ( current == "zero" )
      return std::sqrt( this->computeL2ScalarProduct(old,old) );
    if ( old == "zero" )
      return std::sqrt( this->computeL2ScalarProduct(current,current) );

    m_auxiliaryStrings.clear();
    m_auxiliaryStrings.push_back(current);
    m_auxiliaryStrings.push_back(old);

    m_auxiliaryDoubles.clear();
    m_auxiliaryDoubles.push_back(0.0);
    assemblyLoopBoundary(&LinearProblemNSSimplifiedFSI::normL2BoundaryDifferenceComputer);
    double result(0);
    MPI_Allreduce(&m_auxiliaryDoubles[0],&result,1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
    return std::sqrt(result);
  }

  std::pair<double,double> LinearProblemNSSimplifiedFSI::computeTestQuantities( bool lumped ) {
    std::pair<double,double> resultP;
    m_auxiliaryDoubles.clear();
    m_auxiliaryDoubles.resize(4,0.0);
    if ( lumped ) 
      assemblyLoopBoundary(&LinearProblemNSSimplifiedFSI::testQuantitiesLumpedComputer);
    else
      assemblyLoopBoundary(&LinearProblemNSSimplifiedFSI::testQuantitiesComputer);
    std::vector<double> result(4,0);
    MPI_Allreduce(m_auxiliaryDoubles.data(),result.data(),4,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
    
    using std::sqrt;
    resultP.first  = std::sqrt(result[0])/( std::sqrt(result[1]) + 1e-12);
    resultP.second = std::sqrt(result[2])/( std::sqrt(result[3]) + 1e-12);
    return resultP;
  }

  double LinearProblemNSSimplifiedFSI::computeL2ScalarProduct(std::string l, std::string r) {
    m_auxiliaryStrings.clear();
    m_auxiliaryStrings.push_back(l);
    m_auxiliaryStrings.push_back(r);

    m_auxiliaryDoubles.clear();
    m_auxiliaryDoubles.push_back(0.0);
    assemblyLoopBoundary(&LinearProblemNSSimplifiedFSI::scalarProductComputer);
    double result(0);
    MPI_Allreduce(&m_auxiliaryDoubles[0],&result,1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
    return result;
  }

  void LinearProblemNSSimplifiedFSI::normL2BoundaryDifferenceComputer(felInt ielSupportDof) {
    double current,old,valuei,valuej;
    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFePre->numDof(); iSurfDof++) {
      felInt iDofGlobScalar;
      dof().loc2glob( ielSupportDof, iSurfDof, m_iPressure, 0, iDofGlobScalar);
      AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      
      m_seqVecs.Get( m_auxiliaryStrings[0] ).getValues(1,&iDofGlobScalar, &current);
      m_seqVecs.Get( m_auxiliaryStrings[1] ).getValues(1,&iDofGlobScalar, &old);

      valuei=current-old;
      for ( std::size_t jSurfDof(0); jSurfDof < ( std::size_t ) m_curvFePre->numDof(); jSurfDof++) {
        felInt jDofGlobScalar;
        dof().loc2glob( ielSupportDof, jSurfDof, m_iPressure, 0, jDofGlobScalar);
        AOApplicationToPetsc(m_ao,1,&jDofGlobScalar);
        m_seqVecs.Get( m_auxiliaryStrings[0] ).getValues(1,&jDofGlobScalar, &current);
        m_seqVecs.Get( m_auxiliaryStrings[1] ).getValues(1,&jDofGlobScalar, &old);

        valuej=current-old;
        for(int ig=0; ig < m_curvFePre->numQuadraturePoint(); ig++) {
          m_auxiliaryDoubles[0] += valuei*valuej * m_curvFePre->phi[ig](iSurfDof) * m_curvFePre->phi[ig](jSurfDof) * m_curvFePre->weightMeas(ig);
        }
      }
    }
  }

  void 
  LinearProblemNSSimplifiedFSI::testQuantitiesComputer(felInt ielSupportDof) {

    double uni,unoldi,ciopi,x1i;
    double value0i,value1i,value2i,value3i;

    double unj,unoldj,ciopj,x1j;
    double value0j,value1j,value2j,value3j;

    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFePre->numDof(); iSurfDof++) {
      felInt iDofGlobScalar;
      dof().loc2glob( ielSupportDof, iSurfDof, m_iPressure, 0, iDofGlobScalar);
      AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      
      m_seqVecs.Get( "normalVelocity" ).getValues(1,&iDofGlobScalar, &uni);
      m_seqVecs.Get( "normalVelocityOld" ).getValues(1,&iDofGlobScalar, &unoldi);
      m_seqVecs.Get( "currentIop" ).getValues(1,&iDofGlobScalar, &ciopi);
      m_seqVecs.Get( "x1" ).getValues(1,&iDofGlobScalar, &x1i);

      value0i = uni-unoldi;
      value1i = uni;
      value2i = ciopi-x1i;
      value3i = ciopi;

      
      for ( std::size_t jSurfDof(0); jSurfDof < ( std::size_t ) m_curvFePre->numDof(); jSurfDof++) {
        felInt jDofGlobScalar;
        dof().loc2glob( ielSupportDof, jSurfDof, m_iPressure, 0, jDofGlobScalar);
        AOApplicationToPetsc(m_ao,1,&jDofGlobScalar);
      
        m_seqVecs.Get( "normalVelocity" ).getValues(1,&jDofGlobScalar, &unj);
        m_seqVecs.Get( "normalVelocityOld" ).getValues(1,&jDofGlobScalar, &unoldj);
        m_seqVecs.Get( "currentIop" ).getValues(1,&jDofGlobScalar, &ciopj);
        m_seqVecs.Get( "x1" ).getValues(1,&jDofGlobScalar, &x1j);

        value0j = unj-unoldj;
        value1j = unj;
        value2j = ciopj-x1j;
        value3j = ciopj;
      
        for(int ig=0; ig < m_curvFePre->numQuadraturePoint(); ig++) {
          double w = m_curvFePre->phi[ig](iSurfDof) * m_curvFePre->phi[ig](jSurfDof) * m_curvFePre->weightMeas(ig);
          m_auxiliaryDoubles[0] += value0i*value0j * w;
          m_auxiliaryDoubles[1] += value1i*value1j * w;
          m_auxiliaryDoubles[2] += value2i*value2j * w;
          m_auxiliaryDoubles[3] += value3i*value3j * w;
        }
      }
    }
  }

  void 
  LinearProblemNSSimplifiedFSI::testQuantitiesLumpedComputer(felInt ielSupportDof) {

    double uni,unoldi,ciopi,x1i;
    double value0i,value1i,value2i,value3i;

    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFePre->numDof(); iSurfDof++) {
      felInt iDofGlobScalar;
      dof().loc2glob( ielSupportDof, iSurfDof, m_iPressure, 0, iDofGlobScalar);
      AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      
      m_seqVecs.Get( "normalVelocity" ).getValues(1,&iDofGlobScalar, &uni);
      m_seqVecs.Get( "normalVelocityOld" ).getValues(1,&iDofGlobScalar, &unoldi);
      m_seqVecs.Get( "currentIop" ).getValues(1,&iDofGlobScalar, &ciopi);
      m_seqVecs.Get( "x1" ).getValues(1,&iDofGlobScalar, &x1i);
      
      using std::pow;
      value0i = std::pow( uni-unoldi ,2);
      value1i = std::pow( uni ,2);
      value2i = std::pow( ciopi-x1i ,2);
      value3i = std::pow( ciopi ,2);

      for(int ig=0; ig < m_curvFePre->numQuadraturePoint(); ig++) {
        double w = std::pow( m_curvFePre->phi[ig](iSurfDof) ,2) * m_curvFePre->weightMeas(ig);
        m_auxiliaryDoubles[0] += value0i * w;
        m_auxiliaryDoubles[1] += value1i * w;
        m_auxiliaryDoubles[2] += value2i * w;
        m_auxiliaryDoubles[3] += value3i * w;
      }
    }
  }

  void LinearProblemNSSimplifiedFSI::scalarProductComputer(felInt ielSupportDof) {
    double l,r;
    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFePre->numDof(); iSurfDof++) {
      felInt iDofGlobScalar;
      dof().loc2glob( ielSupportDof, iSurfDof, m_iPressure, 0, iDofGlobScalar);
      AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      m_seqVecs.Get( m_auxiliaryStrings[0] ).getValues(1,&iDofGlobScalar, &r);
      for ( std::size_t jSurfDof(0); jSurfDof < ( std::size_t ) m_curvFePre->numDof(); jSurfDof++) {
        felInt jDofGlobScalar;
        dof().loc2glob( ielSupportDof, jSurfDof, m_iPressure, 0, jDofGlobScalar);
        AOApplicationToPetsc(m_ao,1,&jDofGlobScalar);
        m_seqVecs.Get( m_auxiliaryStrings[1] ).getValues(1,&iDofGlobScalar, &l);      
        for(int ig=0; ig < m_curvFePre->numQuadraturePoint(); ig++) {
          m_auxiliaryDoubles[0] += l*r * m_curvFePre->phi[ig](iSurfDof) * m_curvFePre->phi[ig](jSurfDof) * m_curvFePre->weightMeas(ig);
        }
      }
    }
  }

  void LinearProblemNSSimplifiedFSI::normalVelocityScalarComputer(felInt ielSupportDof) {
    double value,valueAus;
    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFeVel->numDof(); iSurfDof++) {
      value=0.0;
      felInt iDofGlobScalar;
      dof().loc2glob( ielSupportDof, iSurfDof, m_iPressure, 0, iDofGlobScalar);
      AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      for ( std::size_t iCoor(0); iCoor <  ( std::size_t ) m_curvFeVel->numCoor(); iCoor++) {
        felInt iDofGlob;
        dof().loc2glob( ielSupportDof, iSurfDof, m_iVelocity, iCoor, iDofGlob);
        AOApplicationToPetsc(m_ao,1,&iDofGlob);
        m_seqVecs.Get("normalVelocity").getValues(1,&iDofGlob,&valueAus);
        value += valueAus;
      }
      m_vecs.Get("normalVelocity").setValue(iDofGlobScalar, value, INSERT_VALUES);
    }
  }

  /*! \brief    A function of the loop to sum up the fake components of the displacement and to compute its maximum and minimum.
   */
  void LinearProblemNSSimplifiedFSI::scalarComputer(felInt ielSupportDof) {
    double value,valueAus;
    for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFeVel->numDof(); iSurfDof++) {
      value=0.0;
      felInt iDofGlobScalar;
      dof().loc2glob( ielSupportDof, iSurfDof, m_iPressure, 0, iDofGlobScalar);
      AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      for ( std::size_t iCoor(0); iCoor <  ( std::size_t ) m_curvFeVel->numCoor(); iCoor++) {
        felInt iDofGlob;
        dof().loc2glob( ielSupportDof, iSurfDof, m_iVelocity, iCoor, iDofGlob);
        AOApplicationToPetsc(m_ao,1,&iDofGlob);
        m_seqVecs.Get("displacement").getValues(1,&iDofGlob,&valueAus);
        value += valueAus;
      }
      if( value > m_auxiliaryDoubles[0] )
        m_auxiliaryDoubles[0] = value;
      if( value < m_auxiliaryDoubles[1] )
        m_auxiliaryDoubles[1] = value;
      m_vecs.Get("displacement").setValue(iDofGlobScalar, value, INSERT_VALUES);
    } 
  }
  void
  LinearProblemNSSimplifiedFSI::readIOPInterface(std::map<int,std::map<int,int> > mapIOP, PetscVector& seqCurrentIOP, std::map<int,int> labelMap, felInt rank) {
    if ( FelisceParam::verbose() > 0 ) {
      std::stringstream out;
      out<<"reading data at the interface...";
      PetscPrintf(MpiInfo::petscComm(), "%s",out.str().c_str());
    }
    felInt iDofGlobScalarPressure(0);
    felInt iDofGlobScalarIOP(0);
    felInt iDofGlobScalarPressureAus(0);
    double valueAus(0.0);
    for(auto it_labelNumber = FelisceParam::instance().labelIOPMesh.begin(); it_labelNumber != FelisceParam::instance().labelIOPMesh.end();  it_labelNumber++) {
      felInt labelInThisProblem=labelMap[*it_labelNumber];
      if ( FelisceParam::verbose() > 3 ) {
        std::cout << std::endl;
        std::cout << "label IOP mesh: "<<*it_labelNumber<<" label NS mesh: "<<labelInThisProblem<<std::endl;
      }
      for(auto it_suppDofIop = mapIOP[*it_labelNumber].begin(); it_suppDofIop != mapIOP[*it_labelNumber].end(); it_suppDofIop++) {
        iDofGlobScalarIOP = it_suppDofIop->second;
        iDofGlobScalarPressure = m_dofBD[0/*iBD*/].getPetscDofs( this->dof().getNumGlobComp(m_iUnknownPre,0), labelInThisProblem, it_suppDofIop->first );
        iDofGlobScalarPressureAus = iDofGlobScalarPressure;
        AOPetscToApplication(m_ao,/*counter*/1,&iDofGlobScalarPressureAus);
        if ( m_dofPart[iDofGlobScalarPressureAus] == rank ) {
          seqCurrentIOP.getValues(1,&iDofGlobScalarIOP, &valueAus);
          m_vecs.Get("currentIop").setValue(iDofGlobScalarPressure, valueAus, INSERT_VALUES);
        }
      }
    }
    m_vecs.Get("currentIop").assembly();
    gatherVector(m_vecs.Get("currentIop"), m_seqVecs.Get("currentIop"));
    m_seqVecs.Get("currentIop").assembly();

    if ( FelisceParam::verbose() > 0 ) {
      std::stringstream out;
      out<<"done"<<std::endl;
      PetscPrintf(MpiInfo::petscComm(), "%s",out.str().c_str());
    }    
  }

  /***********************************************************************************/
  /***********************************************************************************/

  void LinearProblemNSSimplifiedFSI::initFixedPointAcceleration()
  {
    m_accMethod = accelerationType(FelisceParam::instance(this->instanceIndex()).accelerationMethod);
    m_omegaAcceleration = FelisceParam::instance(this->instanceIndex()).omegaAcceleration;
    m_seqVecs.Init("x1");
    switch ( m_accMethod ) {
      case FixedPoint:
      case Relaxation:
        break;
      case Aitken:
      case IronsTuck:
        m_seqVecs.Init("x0");
        m_seqVecs.Init("fx0");
        m_seqVecs.Init("fx1");
        break;
    }
  }

  /***********************************************************************************/
  /***********************************************************************************/

  void LinearProblemNSSimplifiedFSI::accelerationPreStep(PetscVector& seqCurrentInput)
  {
    // pre and post with respect to reading the data at the interface

    // method explanation in accelerationPostStep
    switch(m_accMethod) {
      case FixedPoint:
      case Relaxation:
        break;
      case Aitken:
      case IronsTuck:
        // in these two cases we have to move forward x0 and x1
        m_seqVecs.Get("x0" ).copyFrom(m_seqVecs.Get("x1" ));
        m_seqVecs.Get("fx0").copyFrom(m_seqVecs.Get("fx1"));
        break;
    }
    // in any case we need to the save the current value of iop which was the last point
    // where we have have evaluated the functional F.
    m_seqVecs.Get("x1").copyFrom(seqCurrentInput);
    // The following evaluation of the system will
    // be saved in fx1 in the function acceleration
  }

  /***********************************************************************************/
  /***********************************************************************************/

  void LinearProblemNSSimplifiedFSI::accelerationPostStep( PetscVector& seqCurrentInput, int nbOfCurrentIteration ) 
  {
    // We are solving a fixed point problem.

    // here an example for IOPcouplModel:
    // the input to the coupled system is currentiop
    // then you solve NS you compute the displacement
    // than you solve PF given the displacement and the current iop
    // the solution of PF at the boarder is the outcome of function.
    // P_interface = F ( currentIop )
    // an evaluation of F implies the solution of a NS pb + a PF pb.
    // P_interface will be modified by this function and reused for a new iteration.

    // Four (4) different methods:
    // 0. fixed point iterations
    //    default method: we simly iterate F over the same value.
    //    new x1=fx1;
    // 1. relaxation:
    //    new x1= w * fx1 + (1 - w) x1
    //    w=1 ==> back to fixed point iterations
    // 2. Aitken acceleration
    //    new x1= w * fx1 + (1 - w) x1, but w is a funcion of ( x0, fx0, x1, fx1 )
    // 3. Irons-Tuck: the same as Aitken, should be more "stable"
    switch(m_accMethod) {
      case FixedPoint:
      case Relaxation:
        // there is no need to save explicitly fx1 as done in aitken and irons tuck
        // since it is stored in currentIop and it will be overridden with the correct value.
        break;
      case Aitken:
      {
        // at this point we just read the results from the PF problem
        // this has been stored in the petscvec currentIop, since in a
        // fixed point approach it is already the new input (new x1 = currentIop = fx1).
        // in all the other approaches this is just the lates outcome
        // of the F.
        m_seqVecs.Get("fx1").copyFrom( seqCurrentInput );

        if ( nbOfCurrentIteration == 1 /*first iteration: w=1*/ ) {
          m_omegaAcceleration = 1;
        } else {
          // we compute the value for the omega parameter
          m_seqVecs.Init("dx"); // map element initialization
          m_seqVecs.Get("dx").duplicateFrom(m_seqVecs.Get("x1")); // structure of dx copied from x1
          m_seqVecs.Get("dx").copyFrom(m_seqVecs.Get("x1")); // dx = x1
          m_seqVecs.Get("dx").axpy(-1.,m_seqVecs.Get("x0")); // dx = x1 - x0

          m_seqVecs.Init("tmp"); // map element initialization
          m_seqVecs.Get("tmp").duplicateFrom(m_seqVecs.Get("fx0")); // structure of tmp copied from fx0
          m_seqVecs.Get("tmp").copyFrom(m_seqVecs.Get("fx0"));   // tmp = fx0
          m_seqVecs.Get("tmp").axpy(-1.,m_seqVecs.Get("fx1"));   // tmp = fx0 - fx1
          m_seqVecs.Get("tmp").axpy(1.,m_seqVecs.Get("dx"));     // tmp = fx0 - fx1 + x1 - x0

          const double omega  = computeL2ScalarProduct("dx" ,"tmp");
          const double xxnorm = computeL2ScalarProduct("tmp","tmp");
          if ( Tools::equal( xxnorm, 0.0 ) )
            m_omegaAcceleration = 1;
          else
            m_omegaAcceleration = omega / xxnorm;

          // we destroy those auxiliary vectors
          m_seqVecs.Get("tmp").destroy();
          m_seqVecs.erase("tmp");
          m_seqVecs.Get("dx").destroy();
          m_seqVecs.erase("dx");

        }
      }
        break;
      case IronsTuck:
      {
        m_seqVecs.Get("fx1").copyFrom(seqCurrentInput);

        if ( nbOfCurrentIteration == 1 /*first iteration: w=1*/ ) {
          m_omegaAcceleration = 1;
        } else {

          // we compute the value for the omega parameter
          m_seqVecs.Init("d1"); // map element initialization
          m_seqVecs.Get("d1").duplicateFrom(m_seqVecs.Get("x1")); // structure of d1 copied from x1
          m_seqVecs.Get("d1").copyFrom(m_seqVecs.Get("x1"));  // d1 = x1
          m_seqVecs.Get("d1").axpy(-1.,m_seqVecs.Get("fx1")); // d1 = x1 - fx1

          m_seqVecs.Init("tmp"); // map element initialization
          m_seqVecs.Get("tmp").duplicateFrom(m_seqVecs.Get("x0")); // structure of tmp copied from x0
          m_seqVecs.Get("tmp").copyFrom(m_seqVecs.Get("x0"));  // tmp = x0
          m_seqVecs.Get("tmp").axpy(-1.,m_seqVecs.Get("fx0")); // tmp = x0 - fx0
          m_seqVecs.Get("tmp").axpy(-1.,m_seqVecs.Get("d1"));  // tmp = x0 - fx0 - (x1 - fx1 )

          const double xxnorm ( computeL2ScalarProduct("tmp","tmp") );
          const double omega ( computeL2ScalarProduct("d1","tmp") );
          if ( Tools::equal( xxnorm, 0.0 ) ) {
            m_omegaAcceleration = 1;
          } else {
            m_omegaAcceleration = ( 1 + omega / xxnorm ) * m_omegaAcceleration;
          }

          // we destroy those auxiliary vectors
          m_seqVecs.Get("tmp").destroy();
          m_seqVecs.Get("d1").destroy();
          m_seqVecs.erase("d1");
          m_seqVecs.erase("tmp");
        }
      }
        break;
    }

    if ( FelisceParam::verbose() > 1 ) {
      std::stringstream msg;
      msg<<"----> using omega = "<<m_omegaAcceleration<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    }

    //for the fixed point this step is not needed
    if ( m_accMethod != FixedPoint ) {
      std::stringstream msg;
      msg<<" updating fixed point input "<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      // update of currentIop, i.e. the new x1, the input for the next evaluation of F
      seqCurrentInput.axpby( 1. - m_omegaAcceleration, m_omegaAcceleration, m_seqVecs.Get("x1") );
    }
  }

  /***********************************************************************************/
  /***********************************************************************************/

}
