//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Fernandez & F.M. Gerosa
//

/*!
 \file  linearProblemNitscheXFEM.cpp
 \authors M. Fernandez & F.M. Gerosa
 \date  04/2018
 \brief Solver for a fictitious domain and Nitsche-XFEM fluid formulation.
 */

#ifndef _LinearProblemNitscheXFEM_HPP
#define _LinearProblemNitscheXFEM_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "DegreeOfFreedom/duplicateSupportDof.hpp"
#include "FiniteElement/elementField.hpp"
#include "Solver/bdf.hpp"


namespace felisce {

  // Class for the linear problem
  class LinearProblemNitscheXFEM:
    public LinearProblem 
  {

    public:

      // Constructor
      LinearProblemNitscheXFEM();

      // Destructor
      ~LinearProblemNitscheXFEM();

      // Pointer definition of LinearProblemNitscheXFEM
      FELISCE_CLASS_POINTER_DEFINITION(LinearProblemNitscheXFEM);

      // Initialize the linar problem (parameters, unknowns, ...)
      void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

      // Duplicate and initialize the support dof
      void initSupportDofDerivedProblem() override;

      // Initialize the bdf scheme (pointer to the one in the model)
      void initializeTimeScheme(Bdf* bdf) override { m_bdf = bdf; }

      // Get the current finite elements and initialize elemFields for the problem
      void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

      // Get the current finite elements and initialize elemFields for Neumann BD
      void initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) override;

      // Update matrix pattern
      void userChangePattern(int numProc, int rankProc) override;

      // Compute element array
      void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel1, felInt& iel2, ElementType& eltType, felInt& ielGeo, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

      // Assemble front point tip elements
      void assembleMatrixFrontPoints();

      // Assemble DG terms on fictitious interface
      void assembleMatrixTip();

      // Assemble DG terms on tip elements faces
      void assembleMatrixTipFaces();

      // Assemble ghost penalty stabilisation
      void assembleMatrixGhostPenalty();
      
      // Assemble face oriented stabilisation
      void assembleMatrixFaceOriented();

      // Compute element array Neumann BD
      void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
      
      // Compute and apply Neumann BD
      void computeAndApplyElementNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof1, felInt& ielSupportDof2, felInt& ielGeo, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

      // Assemble Darcy BD
      void assembleMatrixBCDarcy();

      // Get the pointer to the curvilinear finite element of the structure
      void setStrucFiniteElement(CurvilinearFiniteElement* strucFE); // TODO D.C. remove this part once merged multimesh in master

      // Add m_Matrix[1] to the current matrix (m_Matrix[0])
      void addMatrixRHS() override;

      // Get the reference to the object in the model duplicating the support dofs
      void setDuplicateSupportObject(DuplicateSupportDof* object);

      // Get a pointer to the structure linear problem
      void setInterfaceMesh(GeometricMeshRegion* interfMesh) { m_interfMesh = interfMesh; } // TODO D.C. remove this part once merged multimesh in master

      // Gather a vector to m_seqVelExtrapol
      void gatherSeqVelExtrapol(std::vector<PetscVector>& parallelVec);

      // Gather bdf->vector() to m_seqBdfRHS
      void gatherSeqBdfRHS(std::vector<PetscVector>& parallelVec);

      // Dynamics, copy original <-> current
      void deleteDynamicData();

      // Initialization objects previous time step
      void initOldObjects();

      // Update solution previous time step
      void updateOldSolution(felInt countInitOld);

      // Get solution previous time step
      PetscVector const & solutionOld(felInt n) const { return m_solutionOld[n]; }

      // Set the pointer to the structure velocity and stresses. (main_pvm)
      void setInterfaceExchangedData(std::vector<double>& intfVelo, std::vector<double>& intfForc);

      // Compute stresses on interface
      void computeInterfaceStress();

      // Print stresses
      void printStructStress(int indexTime, double dt);

      void printMeshPartition(int indexTime, double dt) const; // TODO remove

      void writeDuplication(int rank, IO& io, double& time, int iteration) const;

      void printSkipVolume(bool printSlipVolume);

    protected:

      CurrentFiniteElement* m_feVel;                       // volume velocity finite element
      CurrentFiniteElement* m_fePres;                      // volume pressure finite element
      CurrentFiniteElementWithBd* m_feVelWithBd;           // volume finite element with edges
      CurrentFiniteElementWithBd* m_fePreWithBd;           // volume finite element with edges
      CurvilinearFiniteElement *m_curvFeVel;               // boundary velocity finite element
      CurvilinearFiniteElement *m_curvFePress;             // boundary pressure finite element
      CurvilinearFiniteElement *m_curvFeDarcy;             // finite element for pressure Darcy
      CurvilinearFiniteElement *m_curvFeStruc;             // finite element for the structure
      Variable* m_velocity;                                // velocity
      Variable* m_pressure;                                // pressure
      Variable* m_presDarcy;                               // pressure darcy
      felInt m_iVelocity;                                  // id of the velocity
      felInt m_iPressure;                                  // id of the pressure
      felInt m_iPreDarcy;                                  // id of the pressure
      felInt m_iUnknownVel;                                // id of the velocity unknown
      felInt m_iUnknownPre;                                // id of the pressure unknown
      felInt m_iUnknownDar;                                // id of the pressure darcy unknown
      int m_velBlock;                                      // index velocity block
      int m_preBlock;                                      // index pressure block
      int m_darBlock;                                      // index pressure darcy block
      int m_useNSEquation;                                 // flag to choose implementation method of advective term
      double m_viscosity;                                  // dynamic viscosity of the fluid
      double m_density;                                    // density of the fluid
      double m_nitschePar;                                 // Nitsche penalty parameter
      bool m_useSymmetricStress;                           // expression of the Laplacian (epsilon(u) or grad(u))
      bool m_useInterfaceStab;                             // interface flow stabilization flag

      std::vector<PetscVector> m_seqVelExtrapol;           // extrapolation of the velocity

      // Duplicated support
      DuplicateSupportDof *m_duplicateSupportElements;     // manage the duplication of the support dofs

      // Old supportDofUnknown
      std::vector< std::vector<SupportDofMesh> > m_supportDofUnknownOld;

      // Auxiliary arrays to store elements points
      std::vector<Point*> m_mshPts;
      std::vector<Point*> m_itfPts;
      std::vector<Point*> m_subItfPts;
      std::vector<Point>  m_intPoints;

      // Number of vertices per element
      size_t m_numVerPerFluidElt;
      size_t m_numVerPerSolidElt;

      // Vectors to store previous time step info 
      std::vector<Dof> m_dofOld;
      std::vector<AO>  m_aoOld;

    private:

      // Compute elementary terms
      void computeElementMatrixRHS();
      void computeElementMatrixRHSPartDependingOnOldTimeStep(felInt iel, felInt ielOld, felInt idTimeStep);
      void computeNitscheSigma_u_v(double velCoeff, double preCoeff);
      void computeNitscheSigma_v_u(double velCoeff, double preCoeff);
      void computeNitscheDpoint_Sigma_v(double velCoeff, double preCoeff);

      // It contains all the Darcy-boundary integrals
      void initPerElementTypeDarcy(ElementType& eltType, FlagMatrixRHS flagMatrixRHS);
      void computeElementArrayDarcy(const std::vector<Point*>& elemPoint);
      void computeAndApplyElementDarcy(const std::vector<Point*>& elemPoint, felInt ielSupportDof1, felInt ielSupportDof2, felInt ielGeoGlobal, FlagMatrixRHS flagMatrixRHS);
      void elementComputeDarcyOnBoundary(const std::vector<Point*>& elemPoint);

      // Update structure velocity
      void updateStructureVel(felInt strucId);

      // Update structure velocity on sub interface elements
      void updateSubStructureVel(const std::vector<Point*>& ptElem, const std::vector<Point*>& ptSubElem);

      // Compute elementary stress
      void m_computeElementaryInterfaceStress(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, const ElementType eltType, felInt iel, ElementVector& elemVec);

      // Update quadrature point in substructure
      void m_computeIntegrationPoints(std::vector<Point*>& ptElem); // TODO D.C. move to finite element class

      // Evaluate structure velocity on sub element quadrature points 
      void m_computeFluidVelOnSubStructure();

      // Evaluate the tensor at integration points of the sub structure
      void m_computeFluidStress(double sideCoeff, const std::vector<double>& strucNormal);

      // BDF time scheme
      Bdf* m_bdf;                                          // Bdf scheme, initialized in initializedTimeScheme
      bool m_isSeqBdfRHSAllocated;                         // to know if m_seqBdfRHS has been allocated
      std::vector<PetscVector> m_seqBdfRHS;                // bdf part going to the rhs
      
      // Sequential velocity
      bool m_isSeqVelExtrapolAllocated;                    // to know if m_solExtrapol has been allocated
    
      // Element fields
      ElementField m_elemFieldAdv;                         // the extrapolated velocity
      ElementField m_elemFieldAdvDup;                      // the extrapolated velocity on the duplicate elt
      ElementField m_elemFieldAdvTmp;
      ElementField m_elemFieldRHSbdf;                      // the bdf part that goes in the rhs
      ElementField m_seqVelDofPts;
      ElementField m_seqPreDofPts;
      ElementField m_elemFieldNormal;
      ElementField m_strucVelDofPts;
      ElementField m_strucVelQuadPts;
      ElementField m_ddotComp;
      ElementField m_elemFieldSolidRHS;

      // Interface mehs
      GeometricMeshRegion* m_interfMesh;                   // structure linear problem

      std::vector<PetscVector> m_solutionOld;
      std::vector<PetscVector> m_sequentialSolutionOld;

      // Communication with fluidToMaster
      std::vector<double>* m_intfVelo;
      std::vector<double>* m_intfForc;

      
      double compAreaVolume(const std::vector<Point*>& elemPoint) const; 
      double m_totalVol = 0;
      double m_skippedVol = 0;
      double m_totalArea = 0;
      double m_skippedArea = 0;
      double m_tol = 0; 
      double m_eps = 1.e-8;
      bool   m_skipSmallVolume = true; 
      bool   m_printSkipVolume = true;
  };
}

#endif
