//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _EIGENPROBLEMALPCURV_HPP
#define _EIGENPROBLEMALPCURV_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/eigenProblemALP.hpp"

namespace felisce {
  /*!
   \class EigenProblemALPCurv
   \authors E. Schenone
   \date 31/01/2013
   \brief Manage functions of a ALP eigenvalues problem and ALP-ROM solver.
   */

  class EigenProblemALPCurv:
    public EigenProblemALP {
  public:
    // Constructor
    //============
    EigenProblemALPCurv();
    ~EigenProblemALPCurv() override;

    //Define Physical Variable associate to the problem
    //=================================================
    void initPerElementTypeBD() override;
    void initPerElementType() override {}

    // Assemble Matrix
    //================
    //! Update finite element with element vertices coordinates and compute operators.
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      (void) elemPoint;
      (void) elemIdPoint;
      (void) iel;
      (void) flagMatrixRHS;
    }
    void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel) override;

  protected:
    CurvilinearFiniteElement* m_fePotTransMembCurv;
  };

}

#endif
