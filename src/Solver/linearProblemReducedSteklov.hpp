//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _LINEARPROBLEMREDUCEDSTEKLOV_HPP
#define _LINEARPROBLEMREDUCEDSTEKLOV_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "FiniteElement/elementField.hpp"
#include "Solver/reducedSteklov.hpp"
#include "Geometry/curvatures.hpp"

namespace felisce {
  /*!
   \class LinearProblemReducedSteklov
   \authors M.Aletti
   \date 2015
   \brief Commmon interface for different linear problems to be used for steklov reduction
  */
  template <class volumeProblem>
  class ReducedSteklov;
  
  template<class volumeProblem> class LinearProblemReducedSteklov :
    public volumeProblem {
    
    friend class ReducedSteklov<volumeProblem>;

  public:
    using LinearProblem::sequential;
    using LinearProblem::parallel;
    enum OptionForSteklov {
      LOAD_IT=               0,
      COMPUTE_IT=            1,
      COMPUTE_AND_SAVE_IT=   2
    };
    enum imgType { dirichlet, neumann };
    // ====================================================================
    // Simple constructor/destructor                                      =
    // ====================================================================
    LinearProblemReducedSteklov();
    ~LinearProblemReducedSteklov() override;
    // ====================================================================
    // These functions let the model build the steklov oeprator           =
    // and use it.                                                        =
    // Since the internal mapping towards the interface is complicated    =
    // the idea is to pass a petscVector defined in the volume            =
    // and do all the crazy mapping into the function                     =
    // ====================================================================
    void assembleFullRankSteklov();
    void assembleLowRankSteklov(felInt imesh);
    // ====================================================================
    // function that read a given petscVector coming                      =
    // from the NS problem                                                =
    // and save it in the corresponding dof of                            =
    // the petscVector <whereToSave>                                      =
    // ====================================================================
    // scalar case
    void readerInterface( const std::map<int, std::map<int,int> > & externalMap,
                          PetscVector& seqExternalVec,
                          const std::map<int,int> &mapBetweenLabels,
                          std::string whereToSave,
                          felInt iUnkWTW=0,   /* where to write */
                          felInt iCompWTW=0); /* where to write */
    void readerInterface( const std::map<int, std::map<int,std::map<int,int> > > & externalMap,
                          PetscVector& seqExternalVec,
                          const std::map<int,int> &mapBetweenLabels,
                          std::string whereToSave,
                          felInt iUnkWTW, /*where to write*/
                          felInt iUCR);/*where to read (iUnknownComp of the other linearProblem)*/
    // ===================================================================
    // export functions
    // ===================================================================
    void exportOutputSteklov(FelisceTransient::Pointer fstransient,int iFixedPointIteration);
    void exportSteklovInputOutput( PetscVector& Input, PetscVector& Output, FelisceTransient::Pointer fstransient, int iFixedPointIteration);
    inline void exportAllEig(felInt imesh) {m_reducedSteklov->exportAllEig(imesh); }

    virtual void initializeDofBoundaryAndBD2VolMaps()=0;
    virtual void derivedProblemAssemble( FlagMatrixRHS /*flag*/) {};
    virtual double userInletPressure() {return 1.;};

    inline DofBoundary& dofBD(std::size_t iBD) { return m_dofBD[iBD]; };
    
  protected:
    virtual void computeTheConstantResponse() {};
    
    void readerInterfaceComponentWise( const std::map<int,std::map<int,int> > & externalMap,
                                       PetscVector& seqExternalVec,
                                       const std::map<int,int> & mapBetweenLabels,
                                       std::string whereToSave,
                                       felInt iUnknownWTW,
                                       felInt iCompWTW);

    virtual void userSetNullSpace(PetscVector& v, int k);
    // ====================================================================
    //  This function is used to apply the boundary condition             =
    //  at the interface                                                  =
    // ====================================================================
    void userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/,felInt& /*iel*/, int /*label*/) override{}
    // ====================================================================
    // This function allocate the petsc matrix to store                   =
    // the steklov operator and apply steklov allow you to use it         =
    // ====================================================================
    void applySteklov( PetscVector& in, PetscVector& out, FelisceTransient::Pointer fstransient, int nfp);
    // ====================================================================
    // This flag is std::set to false in the constructor                       =
    // it is used in the boundary condition loop                           =
    // to read data from the correct std::vector in case of the computation    =
    // of the Steklov operator                                            =
    // it is std::set to true in the meanwhile that we are computing S-Operator=
    // ====================================================================
    bool m_computingSteklov;
    ReducedSteklov<volumeProblem>* m_reducedSteklov;
    PetscMatrix m_fullSteklov;              //  - the matrix (parallel and dense)    

    //! This method assembles the stiffness matrix defined with the laplacian operator on the boundary
    PetscMatrix assembleLaplacianBoundary();
    //! This method assembles the mass matrix defined on the boundary
    PetscMatrix assembleMassBoundary();

    // This std::set of virtual functions should be reimplemented by the derived linear problem since they differ from scalar
    // to std::vector, for instance.
    virtual void initPerETMASS(){};
    virtual void initPerETLAP(){};    

    virtual void updateFE(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/){};
    
    virtual void massMatrixComputer(felInt /*ielSupportDof*/){};  
    virtual void laplacianMatrixComputer(felInt /*ielSupportDof*/){};
    
    void setValueMatrixBD(felInt ielSupportDof);
    std::vector<felInt> m_globPosColumn;
    std::vector<felInt> m_globPosRow;
    std::vector<double> m_matrixValues;

    Curvatures m_curv;
    ElementField m_normalField;
        
    void assembleVolumeSystem( FlagMatrixRHS flagMatrixRHS);
    virtual void useSteklovDataBegin();
    virtual void useSteklovDataEnd();
    std::vector<double> m_tmpValues;
    void setSteklovData( PetscVector& bdInput );
    PetscVector getSteklovImg();

    std::vector<int> m_interfaceLabels;

    PetscMatrix m_aux;

    imgType m_imgType;

    bool m_useSteklovData = false;

    ChronoInstance::Pointer m_chronoRS;

    // Dof boundary utilities
    std::vector<DofBoundary> m_dofBD = std::vector<DofBoundary>(1, DofBoundary());
  
  public:
    double comulatedSolveTime(){
      return m_chronoRS->diff_cumul();
    }
    
    ChronoInstance::Pointer chronoRS(){return m_chronoRS;}
    
    void initChronoRS(){
      if (m_chronoRS == nullptr) {
        m_chronoRS = felisce::make_shared<ChronoInstance>(); 
      }
    }
  };
}

#include "linearProblemReducedSteklov.tpp"

#endif
