//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _EIGENPROBLEMALP_HPP
#define _EIGENPROBLEMALP_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/eigenProblem.hpp"

/*
 WARNING !
 EigenProblemALP class files need slepc library to work properly.
*/

namespace felisce 
{
/*!
  \class EigenProblemALP
  \authors E. Schenone
  \date 31/01/2013
  \brief Manage functions of a ALP eigenvalues problem and ALP-ROM solver.
  */

class EigenProblemALP:
  public EigenProblem {
public:
  // Constructor
  //============
  EigenProblemALP();
  ~EigenProblemALP() override;

  virtual void initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm);

  //Define Physical Variable associate to the problem
  //=================================================
  void initPerElementType() override;

  // Assemble Matrix
  //================
  //! Update finite element with element vertices coordinates and compute operators.
  void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

  //! Visualize modes as a variable
  virtual void writeMode(const int iter=0);
  virtual void writeEnsightSolution(const int iter);

  //! Solve eigenproblem to build rom basis
  void buildSolver() override;
  virtual void solve();

  // Reduced model functions
  //========================
  // Initialization
  void readData(IO& io) override;
  void getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber);
  void readBasis();
  virtual void initializeROM();
  virtual void initializeSystemSolver();
  void readEnsightFile(double* vec, std::string varName, int idIter, felInt size);
  void readEigenValueFromFile();
  virtual void computeGamma();
  void computeGammaExtrap();
  void computeMatrixZeta();
  virtual void computeMatrixM();
  virtual void initializeSolution();
  void setIntegrationMethod(const int methodModel) {
    m_method = methodModel;
  }
  virtual void setHeteroTauClose(std::vector<double>& valueTauClose);
  virtual void setFhNf0(std::vector<double>& valuef0);
  virtual void setIapp(std::vector<double>& iapp, int& idS);
  // Compute solution
  int secondOrderGlobalIndex(int& i, int& j);
  int thirdOrderGlobalIndex(int& i, int& j, int& k);
  int fourthOrderGlobalIndex(int& i, int& j, int& k, int& h);
  virtual void computeTensor();
  virtual void checkSolution(PetscVector& solution, PetscVector& refSol, double & error);
  virtual void checkBasis(int size=0);
  //Update functions
  virtual void updateBasis();
  virtual void setPotential();
  void MGS(int size=0);
  void MGS(std::vector<PetscVector>& extraSpace);
  virtual void updateTensor();
  //virtual
  void updateBeta();
  virtual void updateBetaMonolithic();
  void updateBetaEI();
  void updateBetaBdf2();
  void ConjGrad(double* A, double* b, double* x, double tol, int maxIter, int n);
  void solveOrthComp(double* A, double* b, double* x, double tol, int maxIter);
  virtual void updateEigenvalue();

  // Projection functions from FE space to RO one and viceversa
  void projectOnBasis(PetscVector& vectorDof, double* vectorBasis, bool flagMass=true, int size=0);
  void projectOnDof(double* vectorBasis, PetscVector& vectorDof, int size);

  // Write an ECG
  virtual void initializeECG() {}
  virtual void writeECG(int /*iteration*/) {}
  virtual void updateEcgOperator() {}

  virtual void readMeasureEcgFilter() {}

  virtual void getExternalVec(double*){}
  virtual void computeControlForFEM(PetscVector){}
  virtual void readDataControl(){}
  virtual bool applyFilter(){return false;}
    
  // Print&Auxiliary functions
  //==========================
  void viewALP(double* vToPrint, int vSize, std::ofstream outStream);
  void viewALP(double* vToPrint, int vSize, std::string fName);


  // Acces functions
  //================
    
  PetscVector& basisFct(const int i=0) {
    return m_basis[i];
  }
  double eigenValueFirst() {
    return m_eigenValue[0];
  }
  double* eigenValue() {
    return m_eigenValue;
  }
  double* beta() {
    return m_beta;
  }
  double* mu() {
    return m_mu;
  }
  double* gamma() {
    return m_gamma;
  }
  double* eta() {
    return m_eta;
  }
  int dimRomBasis() {
    return m_dimRomBasis;
  }
  int tensor3size() {
    return m_size3;
  }
  int tensor4size() {
    return m_size4;
  }

  template <class Templ_functor> void evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array);
  template <class Templ_functor> void evalFunctionOnDof(const Templ_functor& func,double time, std::vector<double>& array);

protected:
  int m_problem = -1;
  int m_method = -1;
  CurrentFiniteElement* m_fePotTransMemb = nullptr;
  felInt m_ipotTransMemb;
  PetscVector m_U_0;
  PetscVector m_U_0_seq;
  PetscVector m_W_0;
  PetscVector m_W_0_seq;
  PetscVector m_Ue_0;
  PetscVector m_Ue_0_seq;
  PetscVector m_FhNf0;
  ElementField m_elemFieldU0;
  ElementField m_elemFieldFhNf0;
  PetscVector m_initPot;
  double* m_eigenValue = nullptr;
  std::vector<PetscVector> m_basis;
  std::vector<PetscVector> m_orthComp;

  double m_coefChi;
  felInt m_dimRomBasis = 0;
  bool m_useImprovedRec;
  felInt m_dimOrthComp;

  int m_idL = 0;
  int m_idG = 0;
  int m_idK = 0;
  int m_idKie = 0;
  int m_idGs = 0;
  int m_idKs = 0;

  int m_tensorOrder;
  double** m_matrixM = nullptr;
  std::vector<PetscVector> m_zeta;
  double* m_tensorB = nullptr;
  double* m_tensorE = nullptr;
  double* m_tensorEs = nullptr;
  double* m_tensorQ = nullptr;
  double* m_tensorT = nullptr;
  double* m_tensorTs = nullptr;
  double** m_tensorU = nullptr;
  double* m_tensorY = nullptr;
  int m_size2;
  int m_size3;
  int m_size4;
  int m_numSource;
  double* m_beta = nullptr;
  double* m_mu = nullptr;
  double* m_xi = nullptr;
  double* m_beta_n_1 = nullptr;
  double* m_mu_n_1 = nullptr;
  double* m_xi_n_1 = nullptr;
  double* m_gamma = nullptr;
  double* m_eta = nullptr;
  double* m_gamma_extrap = nullptr;
  double* m_eta_extrap = nullptr;
  double** m_source = nullptr;
  double* m_modeMean = nullptr;
  double* m_fiber = nullptr;
  bool m_solutionInitialized = false;

  PetscMatrix m_matrixRom;
  PetscVector m_rhsRom;
  PetscVector m_solutionRom;
  KSPInterface::Pointer m_kspRom = felisce::make_shared<KSPInterface>();
};

template <class Templ_functor> void EigenProblemALP::evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array) {
  Point pt;
  array.resize(m_numDof);
  felInt position;
  for( felInt i = 0; i < m_numDof; i++) {
    findCoordinateWithIdDof(i,pt);
    position = i;
    AOApplicationToPetsc(m_ao,1,&position);
    array[position] = func(pt.x(),pt.y(),pt.z());
  }
}

template <class Templ_functor> void EigenProblemALP::evalFunctionOnDof(const Templ_functor& func,double time, std::vector<double>& array) {
  Point pt;
  array.resize(m_numDof);
  felInt position;
  for( felInt i = 0; i < m_numDof; i++) {
    findCoordinateWithIdDof(i,pt);
    position = i;
    AOApplicationToPetsc(m_ao,1,&position);
    array[position] = func(pt.x(),pt.y(),pt.z(),time);
  }
}

}

#endif
