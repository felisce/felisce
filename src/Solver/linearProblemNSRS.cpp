//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNSRS.hpp"

namespace felisce{

  void
  LinearProblemNSRS::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblemNS::initialize(mesh,fstransient,comm, doUseSNES);
    
    m_vecs.Init("externalVelocity");
    m_seqVecs.Init("externalVelocity");
    m_vecs.Init("normalField");
    m_seqVecs.Init("normalField");
    m_imgType=neumann;
  }
  
  
  void 
  LinearProblemNSRS::initializeDofBoundaryAndBD2VolMaps() {
    /// this function builds all the mappings 
    /// to deal with petsc objects defined at the interface
    m_interfaceLabels = FelisceParam::instance().fsiInterfaceLabel;
    m_dofBD[0/*iBD*/].initialize(this);
    std::vector<int> components(3);
    for( felInt iComp(0); iComp<this->dimension(); ++iComp ) {
      this->dofBD(/*iBD*/0).buildListOfBoundaryPetscDofs(this, m_interfaceLabels, this->iUnknownVel() , iComp);
      components[iComp]=iComp;
    }
    m_dofBD[0/*iBD*/].buildBoundaryVolumeMapping( m_iUnknownVel, components);
  }
  /*! \brief Function to assemble the mass matrix
   *  Function to be called in the assemblyLoopBoundaryGeneral
   */
  void LinearProblemNSRS::massMatrixComputer(felInt ielSupportDof) {
    this->m_elementMatBD[0]->zero();
    this->m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFePre,/*iblock*/0,/*iblock*/0,this->dimension());
    this->setValueMatrixBD(ielSupportDof);
  }
  
  /*! \brief Function to assemble the laplacian matrix
   *  Function to be called in the assemblyLoopBoundaryGeneral
   */
  void  LinearProblemNSRS::laplacianMatrixComputer(felInt ielSupportDof) {
    m_normalField.setValue(this->m_seqVecs.Get("normalField"), *m_curvFePre, ielSupportDof, /*m_idVarVel*/0, this->m_ao, this->dof() );
    m_curv.update(m_normalField, m_curvFePre->covMetric[0], m_curvFePre->m_covBasis[0]);
    this->m_elementMatBD[0]->zero();
    for( felInt iComp(0); iComp<this->dimension(); ++iComp ) {
      this->m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i_for_scalar(/*coef*/ 1, *m_curvFePre, m_curv.invMetricTensor(),/*idVelBlock*/0+iComp );//TODO we could save some computations here.
    }  

    std::vector<felInt> idBd;
    for( felInt iDof(0); iDof<m_curvFePre->numDof(); ++iDof ) {
      felInt id(0);
      this->dof().loc2glob(ielSupportDof, iDof, 0/*idVar*/, 0/*iComp*/, id);
      AOApplicationToPetsc(this->ao(),1,&id);
      if( m_idDofRingsSeq.find(id) != m_idDofRingsSeq.end() ) {
        idBd.push_back(iDof);
      }
    }
    if ( idBd.size() == 2 ) {
      double h=m_curvFePre->measOfSegment(idBd[0],idBd[1]);
      double TGV = 1.e10;
      for ( felInt iComp(0); iComp<this->dimension(); ++iComp ) {
        UBlasMatrixRange matrix = this->m_elementMatBD[0]->matBlock(iComp,iComp);
        matrix(idBd[0],idBd[0]) += TGV*h/3;
        matrix(idBd[1],idBd[1]) += TGV*h/3;
        matrix(idBd[1],idBd[0]) += TGV*h/6;
        matrix(idBd[0],idBd[1]) += TGV*h/6;
      }
    }
    //
    
    this->setValueMatrixBD(ielSupportDof);
  }
  void
  LinearProblemNSRS::initPerETLAP() {
    this->initPerETMASS();
    m_normalField.initialize( DOF_FIELD, *m_curvFePre, m_curvFePre->numCoor() );       
  }
  void
  LinearProblemNSRS::initPerETMASS() {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
    for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    
    m_curvFePre = this->m_listCurvilinearFiniteElement[ m_iPressure ];
  }
  void
  LinearProblemNSRS::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
    m_curvFePre->updateMeasNormal(0, elemPoint);  
  }

  void 
  LinearProblemNSRS::exportNormalField() {
    PetscVector tmpVec = this->dofBD(/*iBD*/0).allocateBoundaryVector(DofBoundary::parallel);
    this->dofBD(/*iBD*/0).restrictOnBoundary(this->m_seqVecs.Get("normalField"), tmpVec);
    if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      tmpVec.saveInBinaryFormat( this->m_dofBD[0/*iBD*/].comm(), "normal", FelisceParam::instance().resultDir );
    }
  }

  
  void LinearProblemNSRS::finalizeEssBCTransientDerivedProblem() {
    if ( m_fstransient->iteration > 0 || m_forceConvAndStabComputation ) {
      for(std::size_t iBC=0; iBC < m_boundaryConditionList.numDirichletBoundaryCondition(); iBC++) {
        BoundaryCondition* BC = m_boundaryConditionList.Dirichlet(iBC);
        if ( m_useSteklovData ) {
          this->setValueBoundaryCondition(BC,m_seqVecs.Get("dataSteklov"));
        } else {
          this->setValueBoundaryCondition(BC,m_seqVecs.Get("externalVelocity"));
        }
      }
    }
  }

  void LinearProblemNSRS::computeTheConstantResponse() {
    // This flag is necessary to change the behavior of computElementArray and to make it compute also the Stabilization terms
    m_forceConvAndStabComputation = true;
    this->clearMatrixRHS( FlagMatrixRHS::matrix_and_rhs); //This clears just the matrix 0
    this->clearMatrix(1); // We want to clear everything.
    this->assembleVolumeSystem(FlagMatrixRHS::matrix_and_rhs);
    this->solve(MpiInfo::rankProc(),MpiInfo::numProc());
    this->gatherSolution();

    std::vector<IO::Pointer> iotmp = {felisce::make_shared<IO>(FelisceParam::instance().inputDirectory, FelisceParam::instance().inputFile, FelisceParam::instance().inputMesh[1],
                                                               FelisceParam::instance().outputMesh[1], FelisceParam::instance().meshDir, FelisceParam::instance().resultDir,
                                                               "constResp")};

    iotmp[0]->writeMesh(*m_mesh[1]);
    iotmp[0]->initializeOutput();
    double time=0;
    int k=0;
    this->writeSolutionFromVec(this->sequentialSolution(), MpiInfo::rankProc(), iotmp, time, k, std::string("constResp"));
    iotmp[0]->postProcess(time/*, !m_sameGeometricMeshForVariable*/);
      
    // The standard behavior is then restored
    m_forceConvAndStabComputation = false;
  }

  void LinearProblemNSRS::derivedProblemAssemble(FlagMatrixRHS flag) {
    if ( flag == FlagMatrixRHS::matrix_and_rhs || flag == FlagMatrixRHS::only_matrix ) {
      this->addMatrixRHS();
    }
  }
  
  void LinearProblemNSRS::useSteklovDataBegin() {
    LinearProblemReducedSteklov<LinearProblemNS>::useSteklovDataBegin();
    m_forceConvAndStabComputation = true;
  }
  void LinearProblemNSRS::useSteklovDataEnd() {
    LinearProblemReducedSteklov<LinearProblemNS>::useSteklovDataEnd();
    m_forceConvAndStabComputation = false;
  }


  void 
  LinearProblemNSRS::solveStokesUsingSteklov(FelisceTransient::Pointer fs, int nfp)
  {
    // initialization and computation
    this->applySteklov(m_seqVecs.Get("externalVelocity"), this->solution(),fs,nfp);
  }
  void
  LinearProblemNSRS::computeResidualRS() {
    if ( FelisceParam::instance().computeSteklov ) {
      this->seqResidual().copyFrom(this->sequentialSolution());
    } else {
      LinearProblemReducedSteklov<LinearProblemNS>::computeResidual();
    }
  }
}
