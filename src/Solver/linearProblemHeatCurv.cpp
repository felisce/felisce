//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHeatCurv.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemHeatCurv::LinearProblemHeatCurv():
    LinearProblem()
  {}

  LinearProblemHeatCurv::~LinearProblemHeatCurv()
  = default;

  void LinearProblemHeatCurv::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = temperature;
    listNumComp[0] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(temperature);
    definePhysicalVariable(listVariable,listNumComp);
  }

  void LinearProblemHeatCurv::initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_iTemperature = m_listVariable.getVariableIdList(temperature);
    m_feTemp = m_listCurvilinearFiniteElement[m_iTemperature];
    m_elemField.initialize(DOF_FIELD,*m_feTemp);
  }

  void LinearProblemHeatCurv::computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_feTemp->updateMeasNormal(0, elemPoint);
    m_elementMatBD[0]->grad_phi_i_grad_phi_j(1.,*m_feTemp,0,0,1);
    double coef = 1./m_fstransient->timeStep;
    m_elementMatBD[0]->phi_i_phi_j(coef,*m_feTemp,0,0,1);
    m_elemField.setValue(this->sequentialSolution(), *m_feTemp, iel, m_iTemperature, m_ao, dof());
    assert(!m_elementVectorBD.empty());
    m_elementVectorBD[0]->source(coef,*m_feTemp,m_elemField,0,1);
  }
}


