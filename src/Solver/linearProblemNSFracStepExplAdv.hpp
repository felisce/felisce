//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Caiazzo
//

#ifndef _LinearProblemNSFracStepExplAdv_HPP
#define _LinearProblemNSFracStepExplAdv_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"


namespace felisce {
  /*!
   \class LinearProblemNSFracStepExplAdv
   \author A. Caiazzo
   \date 14/02/2012
   \brief Explicit (stabilized) Advection step of a fractional step for NSE
   */
  class LinearProblemNSFracStepExplAdv:
    public LinearProblem {
  public:
    LinearProblemNSFracStepExplAdv();
    ~LinearProblemNSFracStepExplAdv() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) override;
    void copyMatrixRHS() override;
    void addMatrixRHS() override;
  protected:
    CurrentFiniteElement* m_feVelAdv;
    CurrentFiniteElement* m_feVel;

    Variable* m_velocityAdvection;
    Variable* m_velocity;
    felInt m_iVelocityAdvection;
    felInt m_iVelocity;
    double m_viscosity;
    double m_density;
    int m_explicitAdvection;
    // bdf:
    PetscVector m_seqBdfRHS;
    PetscVector m_solExtrapol;

    int _RungeKutta;

  private:
    ElementField m_elemFieldAdv;
    ElementField m_elemFieldDiff; // not used now

    // bdf:
    //Bdf* m_bdf;
    ElementField m_elemFieldRHSbdf;

    ElementField m_elemFieldExt;
    ElementField m_elemFieldPres;
    ElementField m_elemFieldRHS;

    // boundary terms
    ElementField m_elemFieldDiffBD;
    PetscMatrix m_matrix;
    bool m_buildTeporaryMatrix;

    // for assembling of integral boundary conditions
    std::vector<felInt> elemIdPointOnBoundary;
    std::vector< Point*> elemPointOnBoundary;

    //bool allocateSeqVec;
    //bool allocateSeqVecExt;


  };
}

#endif
