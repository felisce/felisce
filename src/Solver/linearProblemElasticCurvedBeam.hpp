//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:   
//

#ifndef _LINEARPROBLEMELASTICCURVEDBEAM_HPP
#define _LINEARPROBLEMELASTICCURVEDBEAM_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce
{
  
class LinearProblemElasticCurvedBeam:
  public LinearProblem
{
public:
  LinearProblemElasticCurvedBeam(); 
  
  ~LinearProblemElasticCurvedBeam() override;
  
  void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
  
  
  void initPerElementTypeBD(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  
  void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  
  void computeElementArrayBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,  
                              const std::vector<Point*>& elemNormal, const std::vector< std::vector<Point*> >& elemTangent, 
                              felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  
  void userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel) override;

protected:
  
  void m_reinterpoledPhiGeo(); 
  
  void m_compute_F0_F1_reinterpoled(const std::vector<Point*>& elemNormal, const std::vector<Point*>& elemPoint, double thickness);
  
  void m_compute_E0_E1_E2(bool reinterpolation);
  
  void m_computeStiffnessMatrix(const std::vector< std::vector<Point*> >& elemTangent, bool reinterpolation);
  
  CurvilinearFiniteElement* m_feDisp;
  CurvilinearFiniteElement* m_feRota;    
  felInt m_iDisp;
  felInt m_iRota; 
  const QuadratureRule* m_quadratureRule_z;
  int m_numQuadraturePoint_r, m_numQuadraturePoint_z;
  std::vector <double>  m_weightMeas_z;

                  
  UBlasMatrix Elasticity;
  UBlasMatrix B,C;

  bool m_reinterpolation;
  double m_Grr, m_Girr, m_Grr0, m_Grr1, m_Grr2;
  double m_weightMeas;
  UBlasMatrix m_B, m_B0, m_B1, m_B2; 
  UBlasMatrix m_F0, m_F1;
  int m_numPoint, m_numCoor;
  UBlasMatrix m_F0_reinterpol, m_F1_reinterpol;
  UBlasMatrix m_dPhiGeo; 
  UBlasVector m_phiGeo, m_phiGeo_reinterpol;

  bool m_createNormalTangent = false; // true if normals and tangents must be buit if not false

private:

  ElementMatrix* m_matVel;
  ElementField m_elemField; 
    
};
}

#endif
