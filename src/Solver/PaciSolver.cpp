//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    F. Raphel
//

// System includes

// External includes

// Project includes
#include "Solver/PaciSolver.hpp"


//************************************
//Constants
//************************************

static constexpr double R = 8.314472; // [J/mol/K] Gas constant
static constexpr double F = 96485.3415; // [C/mol] Faraday constant
static constexpr double T = 310.0; // [K] Temperature
static constexpr double Q = 2.3;
static constexpr double Lo = 0.025; //[dimensionless]
static constexpr double Nao = 151.; // [mM]
static constexpr double Ko = 5.4; // [mM]
static constexpr double Cao = 1.8; // [mM]
static constexpr double Ki = 150.; // [mM]//150 doc---- 140 Matlab
static constexpr double gCaL = 8.635702e-5; //[m3/F.s]
static constexpr double gKr = 29.8667; //[S/F]
static constexpr double gKs = 2.041;
static constexpr double gf = 30.10312;
static constexpr double arel = 16.464; //[mM/s]
static constexpr double brel = 0.25;
static constexpr double crel = 8.232;
static constexpr double Vleak = 4.4444e-4;
static constexpr double gpCa = 0.4125; //[A/F]
static constexpr double gbNa = 0.9; //[S/F]
static constexpr double gbCa = 0.69264;
static constexpr double Bufc = 0.25; //[mM]
static constexpr double Bufsr = 10.;
static constexpr double Kbufc = 0.001; //[mM]
static constexpr double Kbufsr = 0.3;
static constexpr double Kup = 0.00025; //[mM]
static constexpr double KpCa = 0.0005;
static constexpr double Pkna = 0.03;
static constexpr double Ksat = 0.1;
static constexpr double KmCa = 1.38; //[mM]
static constexpr double KmNai = 87.5;
static constexpr double alpha = 2.8571432; //[dimensionless]
static constexpr double gamma_value = 0.35;
static constexpr double KmNa = 40.; //[mM]
static constexpr double Kmk = 1.;

#ifdef FELISCE_WITH_SUNDIALS

#define Ith(v,i)    NV_Ith_S(v,i-1)       /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) DENSE_ELEM(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */

static constexpr double Y0 = RCONST(0.0);      /* initial y components */
static constexpr double Y1 = RCONST(1.0);
static constexpr double Y075 = RCONST(0.75);
static constexpr double Y01 = RCONST(0.1);
static constexpr double Ycai = RCONST(0.0002);
static constexpr double Ycasr = RCONST(0.3);
static constexpr double Ynai = RCONST(10.);
static constexpr double Y0bis = RCONST(1e-15);      /* initial y components */

static constexpr double YmORd = RCONST(0.0074621);//minfORd
static constexpr double YjORd = RCONST( 0.692477);//jinfORd
static constexpr double Yhslow = RCONST(0.692574);//hslow
static constexpr double Yhfast = RCONST(0.692591);//hfast
static constexpr double Yhl = RCONST(0.496116);//hl
static constexpr double Yml = RCONST(0.000194015);//ml

//static constexpr double RTOL = RCONST(1.0e-6);  /* scalar relative tolerance     ----- 1e-6: doc Sundials       */
//static constexpr double RTOL = RCONST(1.0e-4); //Nouveau test: avant: spontane

//static constexpr double RTOL = RCONST(1.0e-1); //Non sponta ok!
//static constexpr double RTOL = RCONST(1.0e-4);
static constexpr double RTOL = RCONST(1.0e0); //Non sponta ok!



//static constexpr double ATOL4 = RCONST(1.0e-4); /* std::vector absolute tolerance components */
static constexpr double ATOL4 = RCONST(1.0e-4); /* std::vector absolute tolerance components */
static constexpr double ATOL6 = RCONST(1.0e-6);
static constexpr double ATOL8 = RCONST(1.0e-8);
static constexpr double ATOL10 = RCONST(1.0e-10);
static constexpr double ATOL12 = RCONST(1.0e-12);
static constexpr double ATOL14 = RCONST(1.0e-14);

//static constexpr double RTOL = RCONST(1.0e-4);   /* scalar relative tolerance   1e-4: 0.01%     */
//static constexpr double ATOL = RCONST(1.0e-4);   /* std::vector absolute tolerance components */

#endif

namespace felisce {

  /////////////////
  // Constructor //
  /////////////////

  PaciSolver::PaciSolver(FelisceTransient::Pointer fstransient):
    IonicSolver(fstransient) {
    m_nComp = 42;
  }

  /////////////////
  /////////////////


  ////////////////
  // Destructor //
  ////////////////

  PaciSolver::~PaciSolver() = default;

  ////////////////
  ////////////////


  //////////////////////////////////////////////////////////////
  // ComputeRHS: useless in the non Spontaneous model (cvode) //
  //////////////////////////////////////////////////////////////

  void PaciSolver::computeRHS() {

#ifndef FELISCE_WITH_SUNDIALS
    //Only spontaneous model
    double& dt = m_fstransient->timeStep;

    m_bdf.computeRHSTime(dt, m_vecRHS);


    felInt pos;
    double value_uExtrap,value_Cai,value_df1,value_fCa,value_g;


    std::vector<double> values_RHS;
    std::vector<double> valuem_solEDO;


    values_RHS.resize(42);
    valuem_solEDO.resize(42);


    values_RHS.clear();
    valuem_solEDO.clear();

    int& numIt = m_fstransient->iteration;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);

      //Initialization
      m_vecSolEDO[4].getValues(1,&pos,&value_fCa);
      m_vecSolEDO[13].getValues(1,&pos,&value_g);
      m_vecSolEDO[14].getValues(1,&pos,&value_Cai);
      m_vecSolEDO[17].getValues(1,&pos,&value_df1);

      for(int iEDO=0; iEDO<42; iEDO++) {
        m_vecSolEDO[iEDO].getValues(1,&pos,&valuem_solEDO[iEDO]);//value_RHS = _RHS(i)
      }

      if(numIt == 1) {
        value_fCa = 1.;
        value_g = 1.;
        value_Cai = 0.0002;
        value_df1 = 0.;
      }

      double hinf,tauh;
      PaciSolver::hgate(hinf,tauh,value_uExtrap);

      double jinf,tauj;
      PaciSolver::jgate(jinf,tauj,value_uExtrap);

      double minf,taum;
      PaciSolver::mgate(minf,taum,value_uExtrap);

      double dinf,taud;
      PaciSolver::dgate(dinf,taud,value_uExtrap);

      double fCainf,taufCa,constfCa;
      PaciSolver::fcagate(fCainf,taufCa,constfCa,value_uExtrap,value_Cai,value_fCa);

      double f1inf,tauf1;
      PaciSolver::f1gate(f1inf,tauf1,value_uExtrap,value_df1,value_Cai);

      double f2inf,tauf2;
      PaciSolver::f2gate(f2inf,tauf2,value_uExtrap);

      double rinf,taur;
      PaciSolver::rgate(rinf,taur,value_uExtrap);

      double qinf,tauq;
      PaciSolver::qgate(qinf,tauq,value_uExtrap);

      double xr1inf,tauxr1;
      PaciSolver::xr1gate(xr1inf,tauxr1,value_uExtrap);

      double xr2inf,tauxr2;
      PaciSolver::xr2gate(xr2inf,tauxr2,value_uExtrap);

      double xsinf,tauxs;
      PaciSolver::xsgate(xsinf,tauxs,value_uExtrap);

      double xfinf,tauf;
      PaciSolver::xfgate(xfinf,tauf,value_uExtrap);

      double ginf,taug,constg;
      PaciSolver::ggate(ginf,taug,constg,value_uExtrap,value_Cai,value_g);


      //! For non spontaneous AP
      double minfORd,taumORd;
      PaciSolver::mgateORd(minfORd, taumORd,value_uExtrap);

      double jinfORd,taujORd;
      PaciSolver::jgateORd(jinfORd, taujORd,value_uExtrap);

      double hslowinf,tauhslow;
      PaciSolver::hslowgateORd(hslowinf, tauhslow,value_uExtrap);

      double hfastinf,tauhfast;
      PaciSolver::hfastgateORd(hfastinf, tauhfast,value_uExtrap);

      double hlinf,tauhl;
      PaciSolver::hlgateORd(hlinf, tauhl,value_uExtrap);

      double mlinf,tauml;
      PaciSolver::mlgateORd(mlinf, tauml,value_uExtrap);


      //RHS:
      values_RHS[0]=hinf/tauh;
      values_RHS[1]=jinf/tauj;
      values_RHS[2]=minf/taum;
      values_RHS[3]=dinf/taud;
      values_RHS[4]=constfCa*fCainf/taufCa;
      values_RHS[5]=f1inf/tauf1;
      values_RHS[6]=f2inf/tauf2;
      values_RHS[7]=rinf/taur;
      values_RHS[8]=qinf/tauq;
      values_RHS[9]=xr1inf/tauxr1;
      values_RHS[10]=xr2inf/tauxr2;
      values_RHS[11]=xsinf/tauxs;
      values_RHS[12]=xfinf/tauf;
      values_RHS[13]=constg*ginf/taug;
      values_RHS[14]=0.;
      values_RHS[15]=0.;
      values_RHS[16]=0.;
      values_RHS[17]=0.;
      values_RHS[18]=0.;
      values_RHS[19]=0.;
      values_RHS[20]=0.;
      values_RHS[21]=0.;
      values_RHS[22]=0.;
      values_RHS[23]=0.;
      values_RHS[24]=0.;
      values_RHS[25]=0.;
      values_RHS[26]=0.;
      values_RHS[27]=0.;
      values_RHS[28]=0.;
      values_RHS[29]=0.;
      values_RHS[30]=0.;
      values_RHS[31]=0.;
      values_RHS[32]=0.;
      values_RHS[33]=0.;
      values_RHS[34]=0.;
      values_RHS[35]=0.;


      //! For non spontaneous AP: ORd

      values_RHS[36]=minfORd/taumORd;
      values_RHS[37]=jinfORd/taujORd;
      values_RHS[38]=hslowinf/tauhslow;
      values_RHS[39]=hfastinf/tauhfast;
      values_RHS[40]=hlinf/tauhl;
      values_RHS[41]=mlinf/tauml;

      for (std::size_t index = 0 ; index < 42 ; ++index)
        m_vecRHS[index].setValue(pos,values_RHS[index], ADD_VALUES);


    }


    for(int index =0 ; index < 42 ; ++index) {
      m_vecRHS[index].assembly();
    }

#endif

  }


  void PaciSolver::solveEDO() {


    //################//
    //Ionic parameters//
    //################//

    const double Cm = 98.7109;
    const double Vc = 8800.;
    const double Vsr = 583.73;
    const double gNa = 3.6712302e3;
    const double PNaK = 1.841424;
    const double KNaCa = 4900.;
    const double Vmaxup = 0.56064;


    //##################
    //##################



    double value_uExtrap;
    std::vector<double> values_RHS;
    std::vector<double> valuem_solEDO;

    values_RHS.resize(42);
    valuem_solEDO.resize(42);

    values_RHS.clear();
    valuem_solEDO.clear();

    double& dt = m_fstransient->timeStep;
    double& coeffDeriv = m_bdf.coeffDeriv0();


    double betaEDOh,betaEDOj,betaEDOm,betaEDOd,betaEDOfca,betaEDOf1,betaEDOf2,betaEDOr,betaEDOq,betaEDOxr1,betaEDOxr2,betaEDOxs,betaEDOxf,betaEDOg,betaEDOcai,betaEDOcasr,betaEDOnai;
    double Cai,fca,df1,g,Casr,Nai,f1;

    int& numIt = m_fstransient->iteration;
    felInt pos;

#ifdef FELISCE_WITH_SUNDIALS

    if(numIt == 1) { //resizes
      M_userData.M_u_extrap.resize(m_size);
      M_userData.value_df1.resize(m_size);
      M_userData.vINaCa.resize(m_size);
      M_userData.vIPCa.resize(m_size);
      M_userData.vIbCa.resize(m_size);
      M_userData.vENa.resize(m_size);
      M_userData.vEKs.resize(m_size);
      M_userData.vECa.resize(m_size);
      M_userData.vINa.resize(m_size);
      M_userData.vICaL.resize(m_size);
      M_userData.vINaK.resize(m_size);
      M_userData.vIrel.resize(m_size);
      M_userData.vIup.resize(m_size);
      M_userData.vIleak.resize(m_size);
      M_userData.vIbNa.resize(m_size);
      cvode_mem.resize(m_size);
      initvalue.resize(m_size);
      abstol.resize(m_size);
    }

#endif

    for (felInt i = 0; i < m_size; i++) {

      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)



#ifdef FELISCE_WITH_SUNDIALS

      double time = m_fstransient->time;
      int flag;

      M_userData.M_u_extrap[i] = value_uExtrap;
      M_userData.pos = i;

      if(numIt == 1) { //1st iteration --> initializations for each Dof


        M_userData.value_df1[i] = 0.;

        cvode_mem[i] = NULL;
        initvalue[i] = N_VNew_Serial(23);
        abstol[i] = N_VNew_Serial(23);


        Ith(initvalue[i],1) = Y075;//h
        Ith(initvalue[i],2) = Y075;//j
        Ith(initvalue[i],3) = Y0bis;//m
        Ith(initvalue[i],4) = Y0bis;//d
        Ith(initvalue[i],5) = Y1;//fca
        Ith(initvalue[i],6) = Y1;//f1
        Ith(initvalue[i],7) = Y1;//f2
        Ith(initvalue[i],8) = Y0bis;//r
        Ith(initvalue[i],9) = Y1;//q
        Ith(initvalue[i],10) = Y0bis;//xr1
        Ith(initvalue[i],11) = Y1;//xr2
        Ith(initvalue[i],12) = Y0bis;//xs
        Ith(initvalue[i],13) = Y01;//xf
        Ith(initvalue[i],14) = Y1;//g
        Ith(initvalue[i],15) = Ycai;//Cai
        Ith(initvalue[i],16) = Ycasr;//Casr
        Ith(initvalue[i],17) = Ynai;//Nai


        Ith(initvalue[i],18) = YmORd;//minfORd
        Ith(initvalue[i],19) = YjORd;//jinfORd
        Ith(initvalue[i],20) = Yhslow;//hslow
        Ith(initvalue[i],21) = Yhfast;//hfast
        Ith(initvalue[i],22) = Yhl;//hl
        Ith(initvalue[i],23) = Yml;//ml


        Ith(abstol[i],1) = ATOL4;
        Ith(abstol[i],2) = ATOL4;
        Ith(abstol[i],3) = ATOL4;
        Ith(abstol[i],4) = ATOL4;
        Ith(abstol[i],5) = ATOL4;
        Ith(abstol[i],6) = ATOL4;
        Ith(abstol[i],7) = ATOL4;
        Ith(abstol[i],8) = ATOL4;
        Ith(abstol[i],9) = ATOL4;
        Ith(abstol[i],10) = ATOL4;
        Ith(abstol[i],11) = ATOL4;
        Ith(abstol[i],12) = ATOL4;
        Ith(abstol[i],13) = ATOL4;
        Ith(abstol[i],14) = ATOL4;
        Ith(abstol[i],15) = ATOL4;
        Ith(abstol[i],16) = ATOL4;
        Ith(abstol[i],17) = ATOL4;

        Ith(abstol[i],18) = ATOL4;
        Ith(abstol[i],19) = ATOL4;
        Ith(abstol[i],20) = ATOL4;
        Ith(abstol[i],21) = ATOL4;
        Ith(abstol[i],22) = ATOL4;
        Ith(abstol[i],23) = ATOL4;

        cvode_mem[i] = CVodeCreate(CV_BDF,CV_NEWTON);//CV_BDF or CV_ADAMS and CV_NEWTON or CV_FUNCTIONAL || (CV_BDF,CV_NEWTON) for stiff pb

        datatest = &M_userData;
        flag = CVodeSetUserData(cvode_mem[i],datatest);
        flag = CVodeInit(cvode_mem[i],M_f,0.0,initvalue[i]);


        //Tolerances************
        //
        flag = CVodeSVtolerances(cvode_mem[i],RTOL,abstol[i]);
        //flag = CVodeWFtolerances(cvode_mem, ewt);

        //**********************


        //Optional inputs********************
        //
        //flag = CVSpgmr(cvode_mem, PREC_LEFT, 0);
        //flag = CVSpilsSetGSType(cvode_mem, MODIFIED_GS);
        //flag = CVodeRootInit(cvode_mem,2,g);//Optional
        //
        //************************************

        flag = CVDense(cvode_mem[i],23);//--->if ORd? else 17?


        //Optional inputs******************
        //

        //flag = CVodeSetMaxNumSteps(cvode_mem[i],500);//default: 500
        //flag = CVodeSetMaxNumSteps(cvode_mem[i],1000);//ok!
        flag = CVodeSetMaxNumSteps(cvode_mem[i],1000);//default: 500


        flag = CVodeSetMaxStep(cvode_mem[i],0.1);//if 0: inf

        //---------------
        //
        //For more informations, print flag
        //
        // If flag = 0: SUCCESS
        // If flag = 1: TSTOP_RETURN
        //....see cvode.h


        realtype tout;
        int flag;


        flag = CVode(cvode_mem[i], time, initvalue[i], &tout, CV_NORMAL);


        if (  flag != CV_SUCCESS ) {
          std::cout << "CVODE solver failed " << std::endl;
          exit(1);
        }

      } else {

        double df1;
        m_vecSolEDO[17].getValues(1,&pos,&df1);
        M_userData.value_df1[i] = df1;
        double f1;
        m_vecSolEDO[5].getValues(1,&pos,&f1);

        realtype tout;
        int flag;


        flag = CVode(cvode_mem[i], time, initvalue[i], &tout, CV_NORMAL);

        if (  flag != CV_SUCCESS ) {
          std::cout << "CVODE solver failed " << std::endl;
          exit(1);
        }
      }

      for(int j=0; j<17; j++) {
        valuem_solEDO[j] = Ith (initvalue[i],j+1);
      }

      valuem_solEDO[17] = (valuem_solEDO[5]-f1)/dt;
      valuem_solEDO[18] = M_userData.vINaCa[i];
      valuem_solEDO[19] = M_userData.vIPCa[i];
      valuem_solEDO[20] = M_userData.vIbCa[i];
      valuem_solEDO[21] = M_userData.vENa[i];
      valuem_solEDO[22] = M_userData.vEKs[i];
      valuem_solEDO[23] = M_userData.vECa[i];
      valuem_solEDO[24] = M_userData.vINa[i];
      valuem_solEDO[25] = M_userData.vICaL[i];
      valuem_solEDO[31] = M_userData.vINaK[i];
      valuem_solEDO[32] = M_userData.vIrel[i];
      valuem_solEDO[33] = M_userData.vIup[i];
      valuem_solEDO[34] = M_userData.vIleak[i];
      valuem_solEDO[35] = M_userData.vIbNa[i];

      for(int j=0; j<6; j++) {
        valuem_solEDO[36+j] = Ith (initvalue[i],j+18);
      }



#else

      for(int iEDO=0; iEDO<36; iEDO++)
        m_vecRHS[iEDO].getValues(1,&pos,&values_RHS[iEDO]);//value_RHS


      m_vecSolEDO[4].getValues(1,&pos,&fca);
      m_vecSolEDO[5].getValues(1,&pos,&f1);
      m_vecSolEDO[13].getValues(1,&pos,&g);
      m_vecSolEDO[14].getValues(1,&pos,&Cai);
      m_vecSolEDO[15].getValues(1,&pos,&Casr);
      m_vecSolEDO[16].getValues(1,&pos,&Nai);
      m_vecSolEDO[17].getValues(1,&pos,&df1);


      if(numIt==1) {
        fca = 1.;
        g = 1.;
        Cai = 0.0002;
        Casr = 0.3;
        Nai = 10.;
        f1 = 1.;
        df1 = 0.;
      }

      double hinf,tauh;
      PaciSolver::hgate(hinf,tauh,value_uExtrap);

      double jinf,tauj;
      PaciSolver::jgate(jinf,tauj,value_uExtrap);

      double minf,taum;
      PaciSolver::mgate(minf,taum,value_uExtrap);

      double dinf,taud;
      PaciSolver::dgate(dinf,taud,value_uExtrap);

      double fCainf,taufCa,constfCa;
      PaciSolver::fcagate(fCainf,taufCa,constfCa,value_uExtrap,Cai,fca);
      double f1inf,tauf1;
      PaciSolver::f1gate(f1inf,tauf1,value_uExtrap,df1,Cai);

      double f2inf,tauf2;
      PaciSolver::f2gate(f2inf,tauf2,value_uExtrap);

      double rinf,taur;
      PaciSolver::rgate(rinf,taur,value_uExtrap);

      double qinf,tauq;
      PaciSolver::qgate(qinf,tauq,value_uExtrap);

      double xr1inf,tauxr1;
      PaciSolver::xr1gate(xr1inf,tauxr1,value_uExtrap);

      double xr2inf,tauxr2;
      PaciSolver::xr2gate(xr2inf,tauxr2,value_uExtrap);

      double xsinf,tauxs;
      PaciSolver::xsgate(xsinf,tauxs,value_uExtrap);

      double xfinf,tauf;
      PaciSolver::xfgate(xfinf,tauf,value_uExtrap);

      double ginf,taug,constg;
      PaciSolver::ggate(ginf,taug,constg,value_uExtrap,Cai,g);



      //! For non spontaneous AP
      double minfORd,taumORd;
      PaciSolver::mgateORd(minfORd, taumORd,value_uExtrap);

      double jinfORd,taujORd;
      PaciSolver::jgateORd(jinfORd, taujORd,value_uExtrap);

      double hslowinf,tauhslow;
      PaciSolver::hslowgateORd(hslowinf, tauhslow,value_uExtrap);

      double hfastinf,tauhfast;
      PaciSolver::hfastgateORd(hfastinf, tauhfast,value_uExtrap);

      double hlinf,tauhl;
      PaciSolver::hlgateORd(hlinf, tauhl,value_uExtrap);

      double mlinf,tauml;
      PaciSolver::mlgateORd(mlinf, tauml,value_uExtrap);




      //h
      betaEDOh = coeffDeriv/dt+1./tauh;
      valuem_solEDO[0] = values_RHS[0]/betaEDOh;

      //j
      betaEDOj = coeffDeriv/dt+1./tauj;
      valuem_solEDO[1] = values_RHS[1]/betaEDOj;

      //m
      betaEDOm = coeffDeriv/dt+1./taum;
      valuem_solEDO[2] = values_RHS[2]/betaEDOm;

      //d
      betaEDOd = coeffDeriv/dt+1./taud;
      valuem_solEDO[3] = values_RHS[3]/betaEDOd;

      //fCa
      betaEDOfca = coeffDeriv/dt+constfCa/taufCa;
      valuem_solEDO[4] = values_RHS[4]/betaEDOfca;

      //f1
      betaEDOf1 = coeffDeriv/dt+1./tauf1;
      valuem_solEDO[5] = values_RHS[5]/betaEDOf1;

      //f2
      betaEDOf2 = coeffDeriv/dt+1./tauf2;
      valuem_solEDO[6] = values_RHS[6]/betaEDOf2;

      //r
      betaEDOr = coeffDeriv/dt+1./taur;
      valuem_solEDO[7] = values_RHS[7]/betaEDOr;

      //q
      betaEDOq = coeffDeriv/dt+1./tauq;
      valuem_solEDO[8] = values_RHS[8]/betaEDOq;

      //xr1
      betaEDOxr1 = coeffDeriv/dt+1./tauxr1;
      valuem_solEDO[9] = values_RHS[9]/betaEDOxr1;

      //xr2
      betaEDOxr2 = coeffDeriv/dt+1./tauxr2;
      valuem_solEDO[10] = values_RHS[10]/betaEDOxr2;

      //xs
      betaEDOxs = coeffDeriv/dt+1./tauxs;
      valuem_solEDO[11] = values_RHS[11]/betaEDOxs;

      //xf
      betaEDOxf = coeffDeriv/dt+1./tauf;
      valuem_solEDO[12] = values_RHS[12]/betaEDOxf;

      //g
      betaEDOg = coeffDeriv/dt+constg/taug;
      valuem_solEDO[13] = values_RHS[13]/betaEDOg;



      //! For non spontaneous AP

      //mORd
      const double betaEDOmORd = coeffDeriv/dt+1./taumORd;
      valuem_solEDO[18] = values_RHS[18]/betaEDOmORd;

      //jORd
      const double betaEDOjORd = coeffDeriv/dt+1./taujORd;
      valuem_solEDO[19] = values_RHS[19]/betaEDOjORd;

      //hslow
      const double betaEDOhslow = coeffDeriv/dt+1./tauhslow;
      valuem_solEDO[20] = values_RHS[20]/betaEDOhslow;

      //hfast
      const double betaEDOhfast = coeffDeriv/dt+1./tauhfast;
      valuem_solEDO[21] = values_RHS[21]/betaEDOhfast;

      //hl
      const double betaEDOhl = coeffDeriv/dt+constfCa/tauhl;
      valuem_solEDO[22] = values_RHS[22]/betaEDOhl;

      //ml
      const double betaEDOml = coeffDeriv/dt+1./tauml;
      valuem_solEDO[23] = values_RHS[23]/betaEDOml;



      //Calcul des courants pour les concentrations
      double value_iup = Vmaxup/(1.+std::pow(Kup/Cai,2));
      double value_ileak = Vleak*(Casr-Cai);


      double value_irel = (crel+(arel*std::pow(Casr,2))/(std::pow(brel,2)+std::pow(Casr,2)))*valuem_solEDO[3]*valuem_solEDO[13];
      value_irel = value_irel*0.0411;//ventricular
      //Atrial
      //value_irel = value_irel*0.0556;



      //iNaK
      const double num1 = PNaK*Ko*Nai/(Ko+Kmk);
      const double num2 = num1/(Nai+KmNa);
      double value_iNaK = num2/(1.+0.1245*std::exp(-0.1*value_uExtrap*F/(R*T))+0.0353*std::exp(-value_uExtrap*F/(R*T)));

      //iNaCa
      const double prodnum1 = std::pow(Nai,3)*Cao*std::exp(gamma_value*value_uExtrap*F/(R*T));
      const double prodnum2 = alpha*std::pow(Nao,3)*Cai*std::exp((gamma_value-1.)*value_uExtrap*F/(R*T));
      const double numINaCa = KNaCa*(prodnum1-prodnum2);
      const double prodden1 = std::pow(KmNai,3)+std::pow(Nao,3);
      const double prodden2 = KmCa+Cao;
      const double prodden3 = 1.+Ksat*std::exp((gamma_value-1.)*value_uExtrap*F/(R*T));
      const double denINaCa = prodden1*prodden2*prodden3;
      double value_iNaCa = numINaCa/denINaCa;

      valuem_solEDO[18] = value_iNaCa;

      //iCaL
      const double term1 = (gCaL*4.*value_uExtrap*F*F)/(R*T);
      const double term2 = (Cai*std::exp((2.*value_uExtrap*F)/(R*T))-0.341*Cao)/(std::expm1((2.*value_uExtrap*F)/(R*T)));

      double value_iCaL = term1*term2*valuem_solEDO[3]*valuem_solEDO[5]*valuem_solEDO[6]*valuem_solEDO[4];


      //ipCa
      double value_iPCa = gpCa*Cai/(Cai+KpCa);

      valuem_solEDO[19] = value_iPCa;


      const double ENa = (R*T/F)*std::log(Nao/Nai);
      const double ECa = 0.5*(R*T/F)*std::log(Cao/Cai);



      //iNa
      const double m3 = valuem_solEDO[2]*valuem_solEDO[2]*valuem_solEDO[2];
      double value_iNa=gNa*m3*valuem_solEDO[0]*valuem_solEDO[1]*(value_uExtrap-ENa);

      //ibNa-ibCa
      double value_ibNa=gbNa*(value_uExtrap-ENa);
      double value_ibCa=gbCa*(value_uExtrap-ECa);

      valuem_solEDO[20] = value_ibCa;

      double Caibufc = 1./(1.+(Bufc*Kbufc)/std::pow(Cai+Kbufc,2));
      double Casrbufsr = 1./(1.+(Bufsr*Kbufsr)/std::pow(Casr+Kbufsr,2));


      //Cai
      const double valCai = Caibufc*(value_ileak-value_iup+value_irel-Cm*(value_iCaL+value_ibCa+value_iPCa-2.*value_iNaCa)/(2.*Vc*F));

      //Casr
      const double valCasr = (Casrbufsr*Vc)*(value_iup-(value_irel+value_ileak))/Vsr;

      //Nai
      const double valNai = -Cm*(value_iNa+value_ibNa+3.*value_iNaK+3.*value_iNaCa)/(F*Vc);


      //Cai
      betaEDOcai = coeffDeriv/dt;
      values_RHS[14] += valCai;
      valuem_solEDO[14] = values_RHS[14]/betaEDOcai;

      //Casr
      betaEDOcasr = coeffDeriv/dt;
      values_RHS[15] += valCasr;
      valuem_solEDO[15] = values_RHS[15]/betaEDOcasr;

      //Nai
      betaEDOnai = coeffDeriv/dt;
      values_RHS[16] += valNai;
      valuem_solEDO[16] = values_RHS[16]/betaEDOnai;


      //Calcul de df1
      valuem_solEDO[17] = (valuem_solEDO[5]-f1)/dt;


      //Calcul de ENa EKs et ECa
      valuem_solEDO[21] = (R*T/F)*std::log(Nao/Nai);
      valuem_solEDO[22] = (R*T/F)*std::log((Ko+Pkna*Nao)/(Ki+Pkna*Nai));
      valuem_solEDO[23] = 0.5*(R*T/F)*std::log(Cao/Cai);



#endif


      for(int iEDO=0; iEDO<17; iEDO++) {
        m_vecSolEDO[iEDO].setValue(pos,valuem_solEDO[iEDO], INSERT_VALUES);
      }

      m_vecSolEDO[17].setValue(pos,valuem_solEDO[17], INSERT_VALUES);
      m_vecSolEDO[18].setValue(pos,valuem_solEDO[18], INSERT_VALUES);
      m_vecSolEDO[19].setValue(pos,valuem_solEDO[19], INSERT_VALUES);
      m_vecSolEDO[20].setValue(pos,valuem_solEDO[20], INSERT_VALUES);


      //ENa EKs ECa
      m_vecSolEDO[21].setValue(pos,valuem_solEDO[21], INSERT_VALUES);
      m_vecSolEDO[22].setValue(pos,valuem_solEDO[22], INSERT_VALUES);
      m_vecSolEDO[23].setValue(pos,valuem_solEDO[23], INSERT_VALUES);



      //Currennts Iup,Ileak,Irel,INaK,INaCa,ICaL,IpCa,INa,IbNa,IbCa

#ifdef FELISCE_WITH_SUNDIALS
      m_vecSolEDO[24].setValue(pos,valuem_solEDO[24], INSERT_VALUES);
      m_vecSolEDO[25].setValue(pos,valuem_solEDO[25], INSERT_VALUES);
      m_vecSolEDO[31].setValue(pos,valuem_solEDO[31], INSERT_VALUES);
      m_vecSolEDO[32].setValue(pos,valuem_solEDO[32], INSERT_VALUES);
      m_vecSolEDO[33].setValue(pos,valuem_solEDO[33], INSERT_VALUES);
      m_vecSolEDO[34].setValue(pos,valuem_solEDO[34], INSERT_VALUES);
      m_vecSolEDO[35].setValue(pos,valuem_solEDO[35], INSERT_VALUES);

      //! For non spontaneous AP
      m_vecSolEDO[36].setValue(pos,valuem_solEDO[18], INSERT_VALUES);
      m_vecSolEDO[37].setValue(pos,valuem_solEDO[19], INSERT_VALUES);
      m_vecSolEDO[38].setValue(pos,valuem_solEDO[20], INSERT_VALUES);
      m_vecSolEDO[39].setValue(pos,valuem_solEDO[21], INSERT_VALUES);
      m_vecSolEDO[40].setValue(pos,valuem_solEDO[22], INSERT_VALUES);
      m_vecSolEDO[41].setValue(pos,valuem_solEDO[23], INSERT_VALUES);

#else
      m_vecSolEDO[24].setValue(pos,value_iNa, INSERT_VALUES);
      m_vecSolEDO[25].setValue(pos,value_iCaL, INSERT_VALUES);
      m_vecSolEDO[31].setValue(pos,value_iNaK, INSERT_VALUES);
      m_vecSolEDO[32].setValue(pos,value_irel, INSERT_VALUES);
      m_vecSolEDO[33].setValue(pos,value_iup, INSERT_VALUES);
      m_vecSolEDO[34].setValue(pos,value_ileak, INSERT_VALUES);
      m_vecSolEDO[35].setValue(pos,value_ibNa, INSERT_VALUES);
#endif

    }

    for (std::size_t index = 0 ; index < 42; ++index)
      m_vecSolEDO[index].assembly();


  }





  void PaciSolver::computeIon() {

    //################//
    //Ionic parameters//
    //################//

    const double gNa = 3.6712302e3;
    const double gto = 29.9038;
    const double gK1 = 28.1492;
    const double PNaK = 1.841424;
    const double KNaCa = 4900.;

    //##################
    //##################

    //*******************************************************//
    //Declaration des variables: courants et concentrations
    //*******************************************************//

    double value_ion;
    double value_uExtrap;

    double value_iCaL;
    double value_iPCa;
    double value_iNaCa;
    double value_iNaK;
    double value_ik1;
    double value_ito;
    double value_ikr;
    double value_iks;
    double value_if;
    double value_iNa;

    //gates:
    std::vector<double> valuem_solEDO;
    valuem_solEDO.resize(42);


    valuem_solEDO.clear();

    felInt pos;

    //*****************************************//
    //********  Calculation  ******************//
    //*****************************************//

    for(felInt i = 0 ; i < m_size; i++) {


      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);//value_uExtrap = m_uExtrap(i)

      for(int iEDO=0; iEDO<42; iEDO++) {
        m_vecSolEDO[iEDO].getValues(1,&pos,&valuem_solEDO[iEDO]);
      }


      //******************************************
      //Reversal potential: Nernst equation
      //
      const double Ef = -17e-3; //[mV]
      const double EK = (R*T/F)*std::log(Ko/Ki);
      const double ENa = valuem_solEDO[21];
      const double EKs = valuem_solEDO[22];
      const double ECa = valuem_solEDO[23];

      //******************************************
      //******************************************



      //*************
      //Na+ current
      //
      const double m3sponta = valuem_solEDO[2]*valuem_solEDO[2]*valuem_solEDO[2];
      //-------------------------------------
      //value_iNa=gNa*m3*valuem_solEDO[0]*valuem_solEDO[1]*(value_uExtrap-ENa);
      //-------------------------------------

      //! For non spontaneous AP

      const double hORd = 0.99*valuem_solEDO[39]+0.01*valuem_solEDO[38];
      const double m3 = valuem_solEDO[36]*valuem_solEDO[36]*valuem_solEDO[36];
      const double iNafast = 75.*(value_uExtrap-ENa)*m3*valuem_solEDO[37]*hORd;
      const double iNalate = 0.0075*(value_uExtrap-ENa)*valuem_solEDO[41]*valuem_solEDO[40];

      if(FelisceParam::instance().spontaneous == false) {
        value_iNa = iNafast+iNalate;
      } else {
        value_iNa=gNa*m3sponta*valuem_solEDO[0]*valuem_solEDO[1]*(value_uExtrap-ENa);
      }


      //*********************
      //L-Type Ca2+ current
      //
      const double term1 = (gCaL*4.*value_uExtrap*F*F)/(R*T);
      const double term2 = (valuem_solEDO[14]*std::exp((2.*value_uExtrap*F)/(R*T))-0.341*Cao)/(std::expm1((2.*value_uExtrap*F)/(R*T)));
      //------------------------------------------
      value_iCaL = term1*term2*valuem_solEDO[3]*valuem_solEDO[5]*valuem_solEDO[6]*valuem_solEDO[4];
      //------------------------------------------



      //***************************
      //Transient outward current
      //---------------------------------
      value_ito = gto*valuem_solEDO[7]*valuem_solEDO[8]*(value_uExtrap-EK);
      //---------------------------------

      //************************************
      //Rapid delayed rectifier K+ current
      //--------------------------------------------------
      value_ikr = gKr*std::sqrt(Ko/5.4)*valuem_solEDO[9]*valuem_solEDO[10]*(value_uExtrap-EK);
      //--------------------------------------------------


      //***********************************
      //Slow delayed rectifier K+ current
      //--------------------------------------------------------------------------
      value_iks = gKs*std::pow(valuem_solEDO[11],2)*(1.+0.6/(1.+std::pow(3.8e-5/valuem_solEDO[14],1.4)))*(value_uExtrap-EKs);
      //--------------------------------------------------------------------------


      //*****************************
      //Inward rectifier K+ current
      //
      const double alphak1 = 3.91/(1.+std::exp(1000.*0.5942*(value_uExtrap-EK-200e-3)));
      const double betak1 = (-1.509*std::exp(1000.*0.0002*(value_uExtrap-EK+100e-3))+std::exp(1000.*0.5886*(value_uExtrap-EK-10e-3)))/(1.+std::exp(1000.*0.4547*(value_uExtrap-EK)));
      const double xK1inf = alphak1/(alphak1+betak1);
      //-------------------------------------------------
      value_ik1 = gK1*xK1inf*std::sqrt(Ko/5.4)*(value_uExtrap-EK);
      //-------------------------------------------------


      //*******************************************
      //Hyperpolarization activated funny current
      //------------------------------
      value_if = gf*valuem_solEDO[12]*(value_uExtrap-Ef);
      //------------------------------



      //********************
      //Na+K+ pump current
      //
      const double num1 = PNaK*Ko*valuem_solEDO[16]/(Ko+Kmk);
      const double num2 = num1/(valuem_solEDO[16]+KmNa);
      //--------------------------------------------------------------------------------
      value_iNaK = num2/(1.+0.1245*std::exp(-0.1*value_uExtrap*F/(R*T))+0.0353*std::exp(-value_uExtrap*F/(R*T)));
      //--------------------------------------------------------------------------------



      //***************************
      //Na+Ca2+ exchanger current
      //
      const double prodnum1 = std::pow(valuem_solEDO[16],3)*Cao*std::exp(gamma_value*value_uExtrap*F/(R*T));
      const double prodnum2 = alpha*std::pow(Nao,3)*valuem_solEDO[14]*std::exp((gamma_value-1.)*value_uExtrap*F/(R*T));
      const double numINaCa = KNaCa*(prodnum1-prodnum2);
      const double prodden1 = std::pow(KmNai,3)+std::pow(Nao,3);
      const double prodden2 = KmCa+Cao;
      const double prodden3 = 1.+Ksat*std::exp((gamma_value-1.)*value_uExtrap*F/(R*T));
      const double denINaCa = prodden1*prodden2*prodden3;
      //-------------------------------
      value_iNaCa = numINaCa/denINaCa;
      //-------------------------------



      //*******************
      //Ca2+ pump current
      //--------------------------------------
      value_iPCa = gpCa*valuem_solEDO[14]/(valuem_solEDO[14]+KpCa);
      //--------------------------------------



      //*********************
      //Background currents
      //--------------------------------------
      double value_ibNa=gbNa*(value_uExtrap-ENa);
      double value_ibCa=gbCa*(value_uExtrap-ECa);
      //--------------------------------------


      //********************
      //Membrane potential
      //********************

      value_iNaCa = valuem_solEDO[18];
      value_iPCa = valuem_solEDO[19];
      value_ibCa = valuem_solEDO[20];

      //Currents Ito,IKr,IKs,IK1,If
      m_vecSolEDO[26].setValue(pos,value_ito, INSERT_VALUES);
      m_vecSolEDO[27].setValue(pos,value_ikr, INSERT_VALUES);
      m_vecSolEDO[28].setValue(pos,value_iks, INSERT_VALUES);
      m_vecSolEDO[29].setValue(pos,value_ik1, INSERT_VALUES);
      m_vecSolEDO[30].setValue(pos,value_if, INSERT_VALUES);

      value_ion = -(value_ik1+value_ito+value_ikr+value_iks+value_iCaL+value_iNaK+value_iNa+value_iNaCa+value_iPCa+value_if+value_ibNa+value_ibCa);
      m_ion.setValue(pos,value_ion, INSERT_VALUES);

    }

    m_ion.assembly();

    m_vecSolEDO[26].assembly();
    m_vecSolEDO[27].assembly();
    m_vecSolEDO[28].assembly();
    m_vecSolEDO[29].assembly();
    m_vecSolEDO[30].assembly();


    //********************
    //********************


  }

  //****************************************************************************************************
  //****************************************************************************************************

  //Gates functions

  //****************************************************************************************************
  //****************************************************************************************************


  void PaciSolver::hgate(double& hinf,double& tauh, double u) {

    hinf = 1./std::sqrt(1.+std::exp(1000.*(u+72.1e-3)/5.7));
    double alphah,betah;

    if(u<-40e-3) {
      alphah = 0.057*std::exp(-1000.*(u+80e-3)/6.8);
      betah = 2.7*std::exp(0.079*u*1000.)+(3.1e5)*std::exp(0.3485*u*1000.);
      tauh = 1.5/(alphah+betah);
    } else {
      alphah = 0.;
      betah = 0.77/(0.13*(1.+std::exp(-1000.*(u+10.66e-3)/11.1)));
      tauh = 2.542;
    }
  }

  void PaciSolver::jgate(double& jinf,double& tauj, double u) {

    jinf = 1./std::sqrt(1.+std::exp(1000.*(u+72.1e-3)/5.7));
    double alphaj,betaj;

    if(u<-40e-3) {
      const double numalphaj = 1000.*(u+37.78e-3)*(-25428.*std::exp(1000.*0.2444*u)-(6.948e-6)*std::exp(-0.04391*u*1000.));
      const double denalphaj = 1./(1.+std::exp(0.311*(u+79.23e-3)*1000.));

      alphaj = numalphaj/denalphaj;
      betaj = (0.02424*std::exp(-0.01052*u*1000.))/(1.+std::exp(-0.1378*(u+40.14e-3)*1000.));
    } else {
      alphaj = 0.;
      betaj = (0.6*std::exp(0.057*u*1000.))/(1.+std::exp(-0.1*(u+32.e-3)*1000.));
    }
    tauj = 7./(alphaj+betaj);
  }

  void PaciSolver::mgate(double& minf,double& taum, double u) {

    const double minfradicand = 1.+std::exp(1000.*(-u-34.1e-3)/5.9);
    minf = 1./std::pow(minfradicand,1./3.);
    const double alpham = 1./(1.+std::exp(1000.*(-u-60e-3)/5.));
    const double betam = (0.1/(1.+std::exp(1000.*(u+35e-3)/5.)))+(0.1/(1.+std::exp(1000.*(u-50e-3)/200.)));
    taum = alpham*betam;
  }

  void PaciSolver::dgate(double& dinf,double& taud, double u) {

    dinf = 1./(1.+std::exp(-1000.*(u+9.1e-3)/7.));
    //Atrial
    //dinf = 1./(1.+std::exp(-1000.*(value_uExtrap+5.986e-3)/7.));
    const double alphad = (1.4/(1.+std::exp(1000.*(-u-35e-3)/13.)))+0.25;
    const double betad = 1.4/(1.+std::exp(1000.*(u+5e-3)/5.));
    const double gammad = 1./(1.+std::exp(1000.*(-u+50e-3)/20.));
    taud = (alphad*betad)+gammad;
  }

  void PaciSolver::fcagate(double& fcainf,double& taufca, double& constfca, double u, double Cai, double fCa) {

    const double alphafCa = 1./(1.+std::pow(Cai/0.0006,8));
    const double betafCa = 0.1/(1.+std::exp((Cai-0.0009)/0.0001));
    const double gammafCa = 0.3/(1.+std::exp((Cai-0.00075)/0.0008));
    fcainf = (alphafCa+betafCa+gammafCa)/1.3156;
    taufca = 2.;//[ms]
    constfca = 1.;
    if(fcainf>fCa && u>-60e-3) {
      constfca = 0.;
    }
  }

  void PaciSolver::f1gate(double& f1inf,double& tauf1, double u, double df1, double Cai) {

    f1inf = 1./(1.+std::exp(1000.*(u+26e-3)/3.));//ventricular
    //Atrial
    //f1inf = 1./(1.+std::exp(-1000.*(value_uExtrap+25.266e-3)/3.));

    const double sum1 = 1102.5*std::exp(-std::pow(std::pow(1000.*(u+27e-3),2.)/15.,2.));
    const double sum2 = 200./(1.+std::exp(1000.*(-u+13e-3)/10.));
    const double sum3 = 180./(1.+std::exp(1000.*(u+30e-3)/10.));
    tauf1 = sum1+sum2+sum3+20.;
    if(df1>0.) {
      tauf1 = tauf1*(1.+1433*(Cai-(50e-6)));
    }
  }

  void PaciSolver::f2gate(double& f2inf,double& tauf2, double u) {

    const double sum12 = 600.*std::exp(-std::pow(1000.*(u+25e-3),2.)/170.);
    const double sum22 = 31./(1.+std::exp(1000.*(-u+25e-3)/10.));
    const double sum32 = 16./(1.+std::exp(1000.*(u+30e-3)/10.));
    tauf2 = sum12+sum22+sum32;
    f2inf = (0.67/(1.+std::exp(1000.*(u+35e-3)/4.)))+0.33;
  }

  void PaciSolver::rgate(double& rinf,double& taur, double u) {
    rinf = 1./(1.+std::exp(1000.*(-u+22.3e-3)/18.75));
    taur = (14.40516/(1.037*std::exp(1000.*0.09*(u+30.61e-3))+0.369*std::exp(-0.12*(u+23.84e-3)*1000.)))+2.75352;
  }

  void PaciSolver::qgate(double& qinf,double& tauq, double u) {
    qinf = 1./(1.+std::exp(1000.*(u+53e-3)/13.));
    tauq = (39.102/(0.57*std::exp(-0.08*(u+44e-3)*1000.)+0.065*std::exp(1000.*0.1*(u+45.93e-3))))+6.06;
  }

  void PaciSolver::xr1gate(double& xr1inf,double& tauxr1, double u) {
    const double V05 = (-((R*T)/(F*Q))*std::log(std::pow(1.+Cao/2.6,4)/(Lo*std::pow(1.+Cao/0.58,4)))-0.019);
    xr1inf = 1./(1.+std::exp(1000.*(V05-u)/4.9));
    const double alphaxr1 = 450./(1.+std::exp(1000.*(-u-45e-3)/10.));
    const double betaxr1 = 6./(1.+std::exp(1000.*(u+30e-3)/11.5));
    tauxr1 = alphaxr1*betaxr1;
  }

  void PaciSolver::xr2gate(double& xr2inf,double& tauxr2, double u) {
    xr2inf = 1./(1.+std::exp(1000.*(u+88e-3)/50.));
    const double alphaxr2 = 3./(1.+std::exp(1000.*(-u-60e-3)/20.));
    const double betaxr2 = 1.12/(1.+std::exp(1000.*(u-60e-3)/20.));
    tauxr2 = alphaxr2*betaxr2;
  }

  void PaciSolver::xsgate(double& xsinf,double& tauxs, double u) {
    xsinf = 1./(1.+std::exp(1000.*(-u-20e-3)/16.));
    const double alphaxs = 1100./std::sqrt((1.+std::exp(1000.*(-u-10e-3)/6.)));
    const double betaxs = 1./(1.+std::exp(1000.*(u-60e-3)/20.));
    tauxs = alphaxs*betaxs;
  }

  void PaciSolver::xfgate(double& xfinf,double& tauf, double u) {
    xfinf = 1./(1.+std::exp(1000.*(u+77.85e-3)/5.));
    tauf = 1900./(1.+std::exp(1000.*(u+15e-3)/10.));
  }

  void PaciSolver::ggate(double& ginf,double& taug,double& constg, double u, double Cai, double g) {

    if(Cai<=0.00035) {
      ginf = 1./(1.+std::pow(Cai/0.00035,6));
    } else {
      ginf = 1./(1.+std::pow(Cai/0.00035,16));
    }

    taug = 2.; //[ms]
    constg = 1.;

    if(ginf>g && u>-60e-3) {
      constg = 0.;
    }
  }

  //For non spontaneous AP, only used for INa

  void PaciSolver::mgateORd(double& minf, double& taum, double u) {
    minf = 1./(1.+std::exp(-1000.*(u+39.57e-3)/9.871));
    const double exp1 = 6.765*std::exp(1000.*(u+11.64e-3)/34.77);
    const double exp2 = 8.552*std::exp(-1000.*(u+77.42e-3)/5.955);
    taum = 1./(exp1+exp2);
  }

  void PaciSolver::jgateORd(double& jinf, double& tauj, double u) {
    jinf = 1./(1.+std::exp(1000.*(u+82.9e-3)/6.086));
    const double exp1 = 0.02136*std::exp(-1000.*(u+100.6e-3)/8.281);
    const double exp2 = 0.3052*std::exp(1000.*(u+0.9941e-3)/38.45);
    tauj = 2.038+1./(exp1+exp2);
  }

  void PaciSolver::hslowgateORd(double& hslowinf, double& tauhslow, double u) {
    hslowinf = 1./(1.+std::exp(1000.*(u+82.9e-3)/6.086));
    const double exp1 = 0.009764*std::exp(-1000.*(u+17.95e-3)/28.05);
    const double exp2 = 0.3343*std::exp(1000.*(u+5.730e-3)/56.66);
    tauhslow = 1./(exp1+exp2);
  }

  void PaciSolver::hfastgateORd(double& hfastinf, double& tauhfast, double u) {
    hfastinf = 1./(1.+std::exp(1000.*(u+82.9e-3)/6.086));
    const double exp1 = 1.432e-5*std::exp(-1000.*(u+1.196e-3)/6.285);
    const double exp2 = 6.149*std::exp(1000.*(u+0.5096e-3)/20.27);
    tauhfast = 1./(exp1+exp2);

  }

  void PaciSolver::hlgateORd(double& hlinf, double& tauhl, double u) {
    hlinf = 1./(1.+std::exp(1000.*(u+87.61e-3)/7.488));
    tauhl = 200.;
  }

  void PaciSolver::mlgateORd(double& mlinf, double& tauml, double u) {
    mlinf = 1./(1.+std::exp(-1000.*(u+42.85e-3)/5.264));
    const double exp1 = 6.765*std::exp(1000.*(u+11.64e-3)/34.77);
    const double exp2 = 8.552*std::exp(-1000.*(u+77.42e-3)/5.955);
    tauml = 1./(exp1+exp2);
  }



  //****************************************************************************************************
  //****************************************************************************************************

  // CVODE: y' = f(y)

  //****************************************************************************************************
  //****************************************************************************************************


#ifdef FELISCE_WITH_SUNDIALS

  //for each node
  int PaciSolver::M_f(realtype t, N_Vector y, N_Vector ydot, void *f_data) {

    //-----------------------------------------------------//
    // RHS
    //-----------------------------------------------------//


    UserData* data = static_cast< UserData* >(f_data);
    const int pos = data->pos;
    const double UExtrap_forM_f = data->M_u_extrap[pos];
    const double value_df1 = data->value_df1[pos];

    double hinf,tauh;
    PaciSolver::hgate(hinf,tauh,UExtrap_forM_f);

    double jinf,tauj;
    PaciSolver::jgate(jinf,tauj,UExtrap_forM_f);

    double minf,taum;
    PaciSolver::mgate(minf,taum,UExtrap_forM_f);

    double dinf,taud;
    PaciSolver::dgate(dinf,taud,UExtrap_forM_f);

    double fcainf,taufca,constfca;
    PaciSolver::fcagate(fcainf,taufca,constfca,UExtrap_forM_f,Ith(y,15),Ith(y,5));

    double f1inf,tauf1;
    PaciSolver::f1gate(f1inf,tauf1,UExtrap_forM_f,value_df1,Ith(y,15));

    double f2inf,tauf2;
    PaciSolver::f2gate(f2inf,tauf2,UExtrap_forM_f);

    double rinf,taur;
    PaciSolver::rgate(rinf,taur,UExtrap_forM_f);

    double qinf,tauq;
    PaciSolver::qgate(qinf,tauq,UExtrap_forM_f);

    double xr1inf,tauxr1;
    PaciSolver::xr1gate(xr1inf,tauxr1,UExtrap_forM_f);

    double xr2inf,tauxr2;
    PaciSolver::xr2gate(xr2inf,tauxr2,UExtrap_forM_f);

    double xsinf,tauxs;
    PaciSolver::xsgate(xsinf,tauxs,UExtrap_forM_f);

    double xfinf,tauf;
    PaciSolver::xfgate(xfinf,tauf,UExtrap_forM_f);

    double ginf,taug,constg;
    PaciSolver::ggate(ginf,taug,constg,UExtrap_forM_f,Ith(y,15),Ith(y,14));


    //! Non spontaneous (ORd)
    double minfORd,taumORd;
    PaciSolver::mgateORd(minfORd, taumORd,UExtrap_forM_f);

    double jinfORd,taujORd;
    PaciSolver::jgateORd(jinfORd, taujORd,UExtrap_forM_f);

    double hslowinf,tauhslow;
    PaciSolver::hslowgateORd(hslowinf, tauhslow,UExtrap_forM_f);

    double hfastinf,tauhfast;
    PaciSolver::hfastgateORd(hfastinf, tauhfast,UExtrap_forM_f);

    double hlinf,tauhl;
    PaciSolver::hlgateORd(hlinf, tauhl,UExtrap_forM_f);

    double mlinf,tauml;
    PaciSolver::mlgateORd(mlinf, tauml,UExtrap_forM_f);



    //------------------------------------
    // Calculations for Cai, Casr and Nai
    //------------------------------------

    const double Cm = 98.7109;
    const double Vc = 8800.;
    const double Vsr = 583.73;
    const double gNa = 3.6712302e3;
    const double PNaK = 1.841424;
    const double KNaCa = 4900.;
    const double Vmaxup = 0.56064;


    const double Ileak = Vleak*(Ith(y,16)-Ith(y,15));
    const double Iup =  Vmaxup/(1.+std::pow(Kup/Ith(y,15),2));
    double Irel =  (crel+(arel*std::pow(Ith(y,16),2))/(std::pow(brel,2)+std::pow(Ith(y,16),2)))*Ith(y,4)*Ith(y,14);
    Irel = Irel*0.0411;//ventricular

    //Atrial
    //Irel = value_irel*0.0556;

    //iNaK
    const double num1 = PNaK*Ko*Ith(y,17)/(Ko+Kmk);
    const double num2 = num1/(Ith(y,17)+KmNa);
    const double INaK = num2/(1.+0.1245*std::exp(-0.1*UExtrap_forM_f*F/(R*T))+0.0353*std::exp(-UExtrap_forM_f*F/(R*T)));


    //iNaCa
    const double prodnum1 = std::pow(Ith(y,17),3)*Cao*std::exp(gamma_value*UExtrap_forM_f*F/(R*T));
    const double prodnum2 = alpha*std::pow(Nao,3)*Ith(y,15)*std::exp((gamma_value-1.)*UExtrap_forM_f*F/(R*T));
    const double numINaCa = KNaCa*(prodnum1-prodnum2);
    const double prodden1 = std::pow(KmNai,3)+std::pow(Nao,3);
    const double prodden2 = KmCa+Cao;
    const double prodden3 = 1.+Ksat*std::exp((gamma_value-1.)*UExtrap_forM_f*F/(R*T));
    const double denINaCa = prodden1*prodden2*prodden3;
    const double INaCa = numINaCa/denINaCa;


    //iCaL
    const double term1 = (gCaL*4.*UExtrap_forM_f*F*F)/(R*T);
    const double term2 = (Ith(y,15)*std::exp((2.*UExtrap_forM_f*F)/(R*T))-0.341*Cao)/(std::expm1((2.*UExtrap_forM_f*F)/(R*T)));
    const double ICaL = term1*term2*Ith(y,4)*Ith(y,5)*Ith(y,6)*Ith(y,7);


    //ipCa
    const double IPCa = gpCa*Ith(y,15)/(Ith(y,15)+KpCa);


    const double ENa = (R*T/F)*std::log(Nao/Ith(y,17));
    const double EKs = (R*T/F)*std::log((Ko+Pkna*Nao)/(Ki+Pkna*Ith(y,17)));
    const double ECa = 0.5*(R*T/F)*std::log(Cao/Ith(y,15));


    //iNa
    //const double m3 = std::pow(Ith(y,3),3.);
    //const double INa=gNa*m3*Ith(y,1)*Ith(y,2)*(UExtrap_forM_f-ENa);

    //! For non spontaneous AP
    const double hORd = 0.99*Ith(y,21)+0.01*Ith(y,20);
    const double m3 = std::pow(Ith(y,18),3.);
    const double iNafast = 75.*(UExtrap_forM_f-ENa)*m3*Ith(y,19)*hORd;
    const double iNalate = 0.0075*(UExtrap_forM_f-ENa)*Ith(y,23)*Ith(y,22);

    const double INa = iNafast+iNalate;


    //ibNa-ibCa
    const double IbNa=gbNa*(UExtrap_forM_f-ENa);
    const double IbCa=gbCa*(UExtrap_forM_f-ECa);


    const double Caibufc = 1./(1.+(Bufc*Kbufc)/std::pow(Ith(y,15)+Kbufc,2));
    const double Casrbufsr = 1./(1.+(Bufsr*Kbufsr)/std::pow(Ith(y,16)+Kbufsr,2));

    //Cai-Casr-Nai
    double cstteCai = Caibufc*(Ileak-Iup+Irel-Cm*(ICaL+IbCa+IPCa-2.*INaCa)/(2.*Vc*F));
    double cstteCasr = (Casrbufsr*Vc)*(Iup-(Irel+Ileak))/Vsr;
    double cstteNai = -Cm*(INa+IbNa+3.*INaK+3.*INaCa)/(F*Vc);


    //Add currents in data
    data->vINaCa[pos] = INaCa;
    data->vIPCa[pos] = IPCa;
    data->vIbCa[pos] = IbCa;
    data->vENa[pos] = ENa;
    data->vEKs[pos] = EKs;
    data->vECa[pos] = ECa;
    data->vINa[pos] = INa;
    data->vICaL[pos] = ICaL;
    data->vINaK[pos] = INaK;
    data->vIrel[pos] = Irel;
    data->vIup[pos] = Iup;
    data->vIleak[pos] = Ileak;
    data->vIbNa[pos] = IbNa;


    //-----------------------------------------------------//
    // Derivates
    //-----------------------------------------------------//

    Ith(ydot,1) = (hinf-Ith(y,1))/tauh;//h
    Ith(ydot,2) = (jinf-Ith(y,2))/tauj;//j
    Ith(ydot,3) = (minf-Ith(y,3))/taum;//m
    Ith(ydot,4) = (dinf-Ith(y,4))/taud;//d
    Ith(ydot,5) = constfca*(fcainf-Ith(y,5))/taufca;//fca
    Ith(ydot,6) = (f1inf-Ith(y,6))/tauf1;//f1
    Ith(ydot,7) = (f2inf-Ith(y,7))/tauf2;//f2
    Ith(ydot,8) = (rinf-Ith(y,8))/taur;//r
    Ith(ydot,9) = (qinf-Ith(y,9))/tauq;//q
    Ith(ydot,10) = (xr1inf-Ith(y,10))/tauxr1;//xr1
    Ith(ydot,11) = (xr2inf-Ith(y,11))/tauxr2;//xr2
    Ith(ydot,12) = (xsinf-Ith(y,12))/tauxs;//xs
    Ith(ydot,13) = (xfinf-Ith(y,13))/tauf;//xf
    Ith(ydot,14) = constg*(ginf-Ith(y,14))/taug;//g
    Ith(ydot,15) = cstteCai;//Cai
    Ith(ydot,16) = cstteCasr;//Casr
    Ith(ydot,17) = cstteNai;//Nai

    //! For non spontaneous AP
    Ith(ydot,18) = (minfORd-Ith(y,18))/taumORd;//mORd
    Ith(ydot,19) = (jinfORd-Ith(y,19))/taujORd;//jORd
    Ith(ydot,20) = (hslowinf-Ith(y,20))/tauhslow;//hslow
    Ith(ydot,21) = (hfastinf-Ith(y,21))/tauhfast;//hfast
    Ith(ydot,22) = (hlinf-Ith(y,22))/tauhl;//hl
    Ith(ydot,23) = (mlinf-Ith(y,23))/tauml;//ml

    return(0);
  }





  static int ewt(N_Vector y, N_Vector w, void *user_data) {
    int i;
    realtype yy, ww, rtol, atol[17];

    rtol    = RTOL;
    atol[0] = ATOL8;
    atol[1] = ATOL8;
    atol[2] = ATOL8;
    atol[3] = ATOL8;
    atol[4] = ATOL8;
    atol[5] = ATOL8;
    atol[6] = ATOL8;
    atol[7] = ATOL8;
    atol[8] = ATOL8;
    atol[9] = ATOL8;
    atol[10] = ATOL8;
    atol[11] = ATOL8;
    atol[12] = ATOL8;
    atol[13] = ATOL8;
    atol[14] = ATOL8;
    atol[15] = ATOL8;
    atol[16] = ATOL8;
    atol[17] = ATOL8;


    for (i=1; i<=17; i++) {
      yy = Ith(y,i);
      ww = rtol * ABS(yy) + atol[i-1];
      if (ww <= 0.0) return (-1);
      Ith(w,i) = 1.0/ww;
    }

    return(0);
  }





  void PaciSolver::PrintOutput(realtype t, realtype y1, realtype y2, realtype y3, realtype y4, realtype y5, realtype y6) {
#if defined(SUNDIALS_EXTENDED_PRECISION)
    printf("At t = %0.4Le      y =%14.6Le  %14.6Le  %14.6Le  %14.6Le  %14.6Le  %14.6Le\n", t, y1, y2, y3, y4, y5, y6);
#elif defined(SUNDIALS_DOUBLE_PRECISION)
    printf("At t = %0.4le      y =%14.6le  %14.6le  %14.6le  %14.6le  %14.6le  %14.6le\n", t, y1, y2, y3, y4, y5, y6);
#else
    printf("At t = %0.4e      y =%14.6e  %14.6e  %14.6e  %14.6e  %14.6e  %14.6e\n", t, y1, y2, y3, y4, y5, y6);
#endif

    return;
  }

#endif




}




/*
Stock std::cout and others informations



        if(pos==700){
        std::cout << "Iion: " << value_ion << std::endl;
        }
      */
/*
  if(pos==700){
  std::cout << " " << std::endl;
  std::cout << "ik1: " << value_ik1 << std::endl;
  std::cout << "ito: " << value_ito << std::endl;
  std::cout << "ikr: " << value_ikr << std::endl;
  std::cout << "iks: " << value_iks << std::endl;
  std::cout << "iCaL: " << value_iCaL << std::endl;
  std::cout << "iNaK: " << value_iNaK << std::endl;
  std::cout << "iNa: " << value_iNa << std::endl;
  std::cout << "iNaCa: " << value_iNaCa << std::endl;
  std::cout << "iPCa: " << value_iPCa << std::endl;
  std::cout << "if: " << value_if << std::endl;
  std::cout << "ibNa: " << value_ibNa << std::endl;
  std::cout << "ibCa: " << value_ibCa << std::endl;
  std::cout << " " << std::endl;
  std::cout << "Iion: " << value_ion << " sans Cm: " << value_ion*Cm << std::endl;
  std::cout << "u: " << value_uExtrap << std::endl;
  std::cout << " " << std::endl;
  }
*/

//const double coefCm = FelisceParam::instance().Cm;//Test pour recuperer la valeur Cm du data
/*
  if(coefCm == 98.7109){
  //Ventricular
  Vc = 8800.;
  Vsr = 583.73;
  gNa = 3.6712302e3;
  gto = 29.9038;
  gk1 = 28.1492;
  PNaK = 1.841424;
  KNaCa = 4900.;
  Vmaxup = 0.56064;
  }
  else if(coefCm == 78.6671){
  //Atrial
  Vc = 7012.;
  Vsr = 465.20;
  gNa = 6.646185e3;
  gto = 59.8077;
  gk1 = 19.1925;
  PNaK = 1.4731392;
  KNaCa = 2450.;
  Vmaxup = 0.22;
  }
*/


/*
if(pos==700){
  std::cout << "avant: " << std::endl;
  PaciSolver::PrintOutput(time-dt, Ith(initvalue,1), Ith(initvalue,2), Ith(initvalue,3), Ith(initvalue,4), Ith(initvalue,5), Ith(initvalue,6));
  PaciSolver::PrintOutput(time-dt, Ith(initvalue,7), Ith(initvalue,8), Ith(initvalue,9), Ith(initvalue,10), Ith(initvalue,11), Ith(initvalue,12));
  PaciSolver::PrintOutput(time-dt, Ith(initvalue,13), Ith(initvalue,14), Ith(initvalue,15), Ith(initvalue,16), Ith(initvalue,17), Ith(initvalue,18));
  std::cout << " " << std::endl;
  }
*/


