//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. Lombardi
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemFKPP.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"

namespace felisce {
  LinearProblemFKPP::LinearProblemFKPP():
    LinearProblem("FKPP"),
    m_feTemp(nullptr)
    {}

  LinearProblemFKPP::~LinearProblemFKPP()
  {
    m_fstransient = nullptr;
    m_feTemp = nullptr;
  }

  void LinearProblemFKPP::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = temperature;
    listNumComp[0] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(temperature);
    definePhysicalVariable(listVariable,listNumComp);

  }

  void LinearProblemFKPP::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    m_iTemperature = m_listVariable.getVariableIdList(temperature);
    m_feTemp = m_listCurrentFiniteElement[m_iTemperature];

    // define elemField to put term u_n_1.phi_j in RHS.
    // m_elemField contains u_n_1 values.
    m_elemField.initialize(DOF_FIELD,*m_feTemp);
  }

  void LinearProblemFKPP::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;

    // update finite element with point coordinate.
    m_feTemp->updateFirstDeriv(0, elemPoint);
    double coefTime = 1./m_fstransient->timeStep;

    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix) {
      // matrix operators.
      m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().Am,*m_feTemp,0,0,1);
      m_elementMat[0]->phi_i_phi_j(-FelisceParam::instance().Cm+coefTime,*m_feTemp,0,0,1);
    }

    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs) {
      // std::vector operators.
      m_elemField.setValue(this->sequentialSolution(), *m_feTemp, iel, m_iTemperature, m_ao, dof());
      assert(!m_elementVector.empty());
      
      m_elementVector[0]->source(coefTime,*m_feTemp,m_elemField,0,1);

      double coefRHS = -0.5*FelisceParam::instance().Cm; // why should I put this *0.5? Maybe "quadratic" contains an error ... (Elisa, 11/2015)
      m_elementVector[0]->quadratic(coefRHS,*m_feTemp,m_elemField,0,1);
    }

  }

}
