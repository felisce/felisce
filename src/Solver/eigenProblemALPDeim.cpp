//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes
// Project includes
#include "Solver/eigenProblemALPDeim.hpp"
#include "Core/felisceTools.hpp"

namespace felisce {
  /*! Construtor
   */
  EigenProblemALPDeim::EigenProblemALPDeim():
    EigenProblemALP(),
    m_resPoint(false),
    m_fePotExtraCell(nullptr),
    m_dimCollocationPts(0),
    m_collocationNode(nullptr),
    m_basisDeimColloc(nullptr),
    m_basisDeim(nullptr),
    m_orthCompDeim(nullptr),
    m_improvedRecType(0),
    m_hatU(nullptr),
    m_hatW(nullptr),
    m_hatUe(nullptr),
    m_potential(nullptr),
    m_hatBasis(nullptr),
    m_theta(nullptr),
    m_thetaOrth(nullptr),
    m_massDeim(nullptr),
    m_stiffDeim(nullptr),
    m_invGK(nullptr),
    m_resU(nullptr),
    m_hatF(nullptr),
    m_hatG(nullptr),
    m_matrixA(nullptr),
    m_matrixB(nullptr),
    m_sumG(nullptr),
    m_coefFhNs(nullptr),
    m_coefTauClose(nullptr),
    m_coefTauOut(nullptr)
  {}

  EigenProblemALPDeim::~EigenProblemALPDeim() {
    m_fePotExtraCell = nullptr;
    if (m_collocationNode) {
      delete [] m_collocationNode;
    }
    if (m_hatU) {
      delete[] m_hatU;
    }
    if (m_hatW) {
      delete[] m_hatW;
    }
    if (m_hatUe) {
      delete[] m_hatUe;
    }
    if (m_potential) {
      delete [] m_potential;
    }
    if (m_hatF) {
      delete[] m_hatF;
    }
    if (m_hatG) {
      delete[] m_hatG;
    }
    if (m_matrixA) {
      for (int i=0; i< m_dimRomBasis+1; i++) {
        delete [] m_matrixA[i];
      }
      delete [] m_matrixA;
    }
    if (m_matrixB) {
      for (int i=0; i< m_dimRomBasis; i++) {
        delete [] m_matrixB[i];
      }
      delete [] m_matrixB;
    }
    if (m_sumG) {
      delete [] m_sumG;
    }
    if (m_basisDeim) {
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] m_basisDeim[i];
      }
      delete [] m_basisDeim;
    }
    if (m_basisDeimColloc) {
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] m_basisDeimColloc[i];
      }
      delete [] m_basisDeimColloc;
    }
    if (m_orthCompDeim) {
      for (int i=0; i< m_dimOrthComp; i++) {
        delete [] m_orthCompDeim[i];
      }
      delete [] m_orthCompDeim;
    }
    if (m_hatBasis) {
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] m_hatBasis[i];
      }
      delete[] m_hatBasis;
    }
    if (m_theta) {
      for (int i=0; i< m_dimRomBasis; i++) {
        delete [] m_theta[i];
      }
      delete[] m_theta;
    }
    if (m_thetaOrth) {
      for (int i=0; i< m_dimOrthComp; i++) {
        delete [] m_thetaOrth[i];
      }
      delete[] m_thetaOrth;
    }
    if (m_massDeim) {
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] m_massDeim[i];
      }
      delete[] m_massDeim;
    }
    if (m_stiffDeim) {
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] m_stiffDeim[i];
      }
      delete[] m_stiffDeim;
    }
    
    if (m_invGK) {
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] m_invGK[i];
      }
      delete [] m_invGK;
    }
    
    if (m_resU) {
      delete [] m_resU;
    }

    if (FelisceParam::instance().ROMmethod == "ALP-DEIM") {
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_basis[i].destroy();
      }
    }

    if (m_projectorPiV.size() > 0) {
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_projectorPiV[i].destroy();
      }
    }

    if (m_coefFhNs) {
      delete [] m_coefFhNs;
    }
    if (m_coefTauClose) {
      delete [] m_coefTauClose;
    }
    if (m_coefTauOut) {
      delete [] m_coefTauOut;
    }
  }

  void EigenProblemALPDeim::initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::initialize\n");

    EigenProblem::initialize(mesh, fstransient, comm);

    std::unordered_map<std::string, int> mapOfPbm;
    mapOfPbm["FKPP"] = 0;
    mapOfPbm["Monodomain"] = 1;
    mapOfPbm["Bidomain"] = 2;
    mapOfPbm["BidomainCurv"] = 2;
    mapOfPbm["BidomainCoupled"] = 3;
    m_problem = mapOfPbm[FelisceParam::instance().model];

    m_dimRomBasis = FelisceParam::instance().dimRomBasis;
    m_dimCollocationPts = FelisceParam::instance().dimCollocationPts;
    m_useImprovedRec = FelisceParam::instance().useImprovedRec;
    m_dimOrthComp = FelisceParam::instance().dimOrthComp;

    m_numSource = FelisceParam::instance().numberOfSource;

    int id=0;
    m_idL = id;
    id++;
    m_idG = id;
    id++;
    if (!FelisceParam::instance().solveEigenProblem) {
      if (m_useImprovedRec) {
        m_idK = id;
        id++;
      }
    }
    m_numberOfMatrix = id;

    m_coefChi = FelisceParam::instance().chiSchrodinger;
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;
    switch (m_problem) {
    case 0: {
      nameOfTheProblem = "Problem Fisher-Kolmogorov-Petrovski-Piskunov equation (FKPP)";
      listVariable.push_back(potTransMemb);
      listNumComp.push_back(1);
      //define unknown of the linear system.
      m_listUnknown.push_back(potTransMemb);
      break;
    }
    case 1: {
      nameOfTheProblem = "Problem cardiac Monodomain";
      listVariable.push_back(potTransMemb);
      listNumComp.push_back(1);
      //define unknown of the linear system.
      m_listUnknown.push_back(potTransMemb);
      break;
    }
    case 2: {
      nameOfTheProblem = "Problem cardiac Bidomain";
      listVariable.push_back(potTransMemb);
      listNumComp.push_back(1);
      //define unknown of the linear system.
      m_listUnknown.push_back(potTransMemb);
      break;
    }
    case 3: {
      nameOfTheProblem = "Problem cardiac Bidomain";
      listVariable.push_back(potTransMemb);
      listNumComp.push_back(1);
      listVariable.push_back(potExtraCell);
      listNumComp.push_back(1);
      //define unknown of the linear system.
      m_listUnknown.push_back(potTransMemb);
      m_listUnknown.push_back(potExtraCell);
      break;      
    }
    default:
      FEL_ERROR("Model not defined for ALP solver.");
      break;
    }
    definePhysicalVariable(listVariable,listNumComp);

  }

  // Define Physical Variable associate to the problem
  void EigenProblemALPDeim::initPerElementType() {
    switch(m_problem) {
    case 0: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
      break;
    }
    case 1: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
      break;
    }
    case 2: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
      break;
    }
    case 3: {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_ipotExtraCell = m_listVariable.getVariableIdList(potExtraCell);
      m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
      m_fePotExtraCell = m_fePotTransMemb;
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);
      m_elemFieldUe0.initialize(DOF_FIELD,*m_fePotExtraCell);
      break;
    }
    default:
      FEL_ERROR("Model not defined for ALP solver.");
      break;
    }

    if (FelisceParam::instance().hasInfarct && FelisceParam::instance().typeOfIonicModel == "fhn") {
      m_elemFieldFhNf0.initialize(DOF_FIELD,*m_fePotTransMemb);
    }
  }

  // Assemble Matrix
  void EigenProblemALPDeim::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    if (FelisceParam::verbose() > 50 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeElementArray\n");
    switch(m_problem) {
    case 0: { // FKPP
      m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
      m_fePotTransMemb->updateFirstDeriv(0, elemPoint);

      // m_Matrix[0] = grad(phi_i) * grad(phi_j)
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(1.,*m_fePotTransMemb,0,0,1);

      if (FelisceParam::instance().hasInitialCondition) {
        // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
        m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMemb,0,0,1);
      }

      // Matrix[1] = phi_i * phi_j
      m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);

      if (!FelisceParam::instance().solveEigenProblem) {
        if (m_useImprovedRec) {
          // m_Matrix[m_numberOfMatrix-1] = grad(phi_i) * grad(phi_j)
          m_elementMat[m_idK]->grad_phi_i_grad_phi_j(1.,*m_fePotTransMemb,0,0,1);
        }
      }
      break;
    }
    case 1:
    case 2: { // Monodomain
      m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
      m_fePotTransMemb->updateFirstDeriv(0, elemPoint);

      // m_Matrix[0] = grad(phi_i) * grad(phi_j)
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      if (FelisceParam::instance().testCase == 1) {
        std::vector<double> elemFiber;
        getFiberDirection(iel,m_ipotTransMemb, elemFiber);
        // m_Matrix[0] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
        m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
      }

      if (FelisceParam::instance().hasInitialCondition) {
        // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
        m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMemb,0,0,1);
      }

      // m_Matrix[1] = phi_i * phi_j
      m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);

      if (!FelisceParam::instance().solveEigenProblem) {
        if (m_useImprovedRec) {
          // m_Matrix[2] = E_ij = grad(phi_i) * grad(phi_j)
          m_elementMat[m_idK]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
          if (FelisceParam::instance().testCase == 1) {
            std::vector<double> elemFiber;
            getFiberDirection(iel,m_ipotTransMemb, elemFiber);
            // m_Matrix[2] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
            m_elementMat[m_idK]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
          }
        }
      }
      break;
    }
    case 3: { // Bidomain (coupled)
      m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
      m_fePotTransMemb->updateFirstDeriv(0, elemPoint);
      
      // m_Matrix[0] = [sigma_i grad(phi_i) * grad(phi_j)       sigma_i grad(phi_i) * grad(phi_j)     ]
      //               [sigma_i grad(phi_i) * grad(phi_j)  (sigma_i+sigma_e) grad(phi_i) * grad(phi_j)]
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotExtraCell,0,1,1);
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,1,0,1);
      m_elementMat[m_idL]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotExtraCell,1,1,1);

      if (FelisceParam::instance().testCase == 1) {
        std::vector<double> elemFiber;
        getFiberDirection(iel,m_ipotTransMemb, elemFiber);
        // m_Matrix[0] +=  (\sigma_i^l-\sigma_i^t) a vec a grad(phi_i) * grad(phi_j)
        m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,0,0,1);
        m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotExtraCell,0,1,1);
        m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor,elemFiber,*m_fePotTransMemb,1,0,1);
        m_elementMat[m_idL]->tau_grad_phi_i_tau_grad_phi_j(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraFiberTensor-FelisceParam::instance().extraTransvTensor,elemFiber,*m_fePotExtraCell,1,1,1);
      }
      
      if (FelisceParam::instance().hasInitialCondition) {
        // m_Matrix[0] += - m_coefChi * V * phi_i * phi_j
        m_elemFieldU0.setValue(m_U_0_seq, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldU0,*m_fePotTransMemb,0,0,1);
        m_elemFieldUe0.setValue(m_Ue_0_seq, *m_fePotExtraCell, iel, m_ipotExtraCell, m_ao, m_dof);
        m_elementMat[m_idL]->a_phi_i_phi_j(- m_coefChi,m_elemFieldUe0,*m_fePotExtraCell,1,1,1);
      }
      
      // m_Matrix[1] = phi_i * phi_j
      m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);
      m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotExtraCell,1,1,1);
      
      break;
      }
    default:
      FEL_ERROR("Model not defined for ALP solver.");
      break;
    }
  }

  // Projection functions from FE space to RO one
  void EigenProblemALPDeim::projectOnBasis(double* vectorDof, double* vectorBasis, bool flagMass) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::projectOnBasis\n");

    if (flagMass) {
      for (int iBasis=0; iBasis<m_dimRomBasis; iBasis++) {
        vectorBasis[iBasis] =0.;
        for (int jPts=0; jPts<m_dimCollocationPts; jPts++) {
          double tmpVal=0;
          for (int kPts=0; kPts<m_dimCollocationPts; kPts++) {
            tmpVal += m_massDeim[jPts][kPts]*m_basisDeim[kPts][iBasis];
          }
          vectorBasis[iBasis] += tmpVal*vectorDof[jPts];
        }
      }
    } else {
      for (int iBasis=0; iBasis<m_dimRomBasis; iBasis++) {
        vectorBasis[iBasis] =0.;
        for (int jPts=0; jPts<m_dimCollocationPts; jPts++) {
          vectorBasis[iBasis] += m_basisDeim[jPts][iBasis]*vectorDof[jPts];
        }
      }
    }

  }

  // Projection functions from RO space to FE one
  void EigenProblemALPDeim::projectOnDof(double* vectorBasis, double* vectorDof, int size) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::projectOnDof\n");
    for (int jPts=0; jPts<m_dimCollocationPts; jPts++) {
      vectorDof[jPts] = 0.;
      for (int iBasis=0; iBasis<size; iBasis++) {
        vectorDof[jPts] += m_basisDeim[jPts][iBasis]*vectorBasis[iBasis];
      }
    }
  }

  void EigenProblemALPDeim::readData(IO& io) {
    if (FelisceParam::verbose() > 0) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::readData.\n");
    m_Matrix[m_idG].getVecs(m_U_0,nullPetscVector);
    m_U_0.setFromOptions();
    
    m_U_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
    m_U_0_seq.setFromOptions();
    
    m_W_0.duplicateFrom(m_U_0);
    m_W_0.copyFrom(m_U_0);
    
    m_W_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
    m_W_0_seq.setFromOptions();
    
    m_Ue_0.duplicateFrom(m_U_0);
    m_Ue_0.copyFrom(m_U_0);
    
    m_Ue_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
    m_Ue_0_seq.setFromOptions();
    
    if (FelisceParam::instance().hasInitialCondition) {
      // Read initial solution file (*.00000.scl)
      int idInVar = 0;
      // potTransMemb
      double* initialSolution = nullptr;
      initialSolution = new double[m_mesh->numPoints()];
      io.readVariable(idInVar, 0.,initialSolution, m_mesh->numPoints());
      idInVar ++;
      // ionicVar
      double* ionicSolution = nullptr;
      if ( (m_problem == 1) || (m_problem == 2) ) {
        ionicSolution = new double[m_mesh->numPoints()];
        io.readVariable(idInVar, 0.,ionicSolution, m_mesh->numPoints());
        idInVar ++;
      }
      // potExtraCell
      double* extraCellSolution = nullptr;
      if (m_problem == 2) {
        extraCellSolution = new double[m_mesh->numPoints()];
        io.readVariable(idInVar, 0.,extraCellSolution, m_mesh->numPoints());
        idInVar ++;
      }
      // initial potential
      double* initPot = nullptr;
      if (FelisceParam::instance().optimizePotential) {
        m_initPot.createSeq(PETSC_COMM_SELF,m_numDof);
        m_initPot.setFromOptions();
        initPot = new double[m_mesh->numPoints()];
        io.readVariable(idInVar, 0.,initPot, m_mesh->numPoints());
        idInVar ++;
      }
      // initialize (parallel) initial solution vectors
      fromDoubleStarToVec(initialSolution, m_U_0, m_mesh->numPoints());
      if ( (m_problem == 1) || (m_problem == 2) )
        fromDoubleStarToVec(ionicSolution, m_W_0, m_mesh->numPoints());
      if (m_problem == 2 )
        fromDoubleStarToVec(extraCellSolution, m_Ue_0, m_mesh->numPoints());
      
      // initialize SEQUENTIAL initial solution vectors (in order to use elemField and define collocation pts solution)
      felInt ia;
      for (felInt i=0; i< m_numDof; i++) {
        ia = i;
        AOApplicationToPetsc(m_ao,1,&ia);
        m_U_0_seq.setValues(1,&ia,&initialSolution[i],INSERT_VALUES);
        if ( (m_problem == 1) || (m_problem == 2) )
          m_W_0_seq.setValues(1,&ia,&ionicSolution[i],INSERT_VALUES);
        if (m_problem == 2)
          m_Ue_0_seq.setValues(1,&ia,&extraCellSolution[i],INSERT_VALUES);
        if (FelisceParam::instance().optimizePotential)
          m_initPot.setValues(1,&ia,&initPot[i],INSERT_VALUES);
      }
      m_U_0_seq.assembly();
      if ( (m_problem == 1) || (m_problem == 2) ) {
        m_W_0_seq.assembly();
      }
      if (m_problem == 2) {
        m_Ue_0_seq.assembly();
      }
      if (FelisceParam::instance().optimizePotential) {
        m_initPot.assembly();
      }
      
      if (initialSolution) delete [] initialSolution;
      if (ionicSolution) delete [] ionicSolution;
      if (extraCellSolution) delete [] extraCellSolution;
      if (initPot) delete [] initPot;
      
      // Read fibers file (*.00000.vct)
      if (FelisceParam::instance().testCase == 1) {
        if (m_fiber == nullptr)
          m_fiber = new double[m_mesh->numPoints()*3];
        io.readVariable(idInVar, 0.,m_fiber, m_mesh->numPoints()*3);
        idInVar ++;
      }
    }
    else {
      PetscPrintf(PETSC_COMM_WORLD, "U_0 not read from file (zero vector).");
      m_U_0.set( 0.);
      m_U_0.assembly();
      if ( (m_problem == 1) || (m_problem == 2) ) {
        if (FelisceParam::instance().typeOfIonicModel == "schaf") {
          m_W_0.set( 1.);
          m_W_0_seq.set( 1.);
        }
        else {
          m_W_0.set( 0.);
          m_W_0_seq.set( 0.);
        }
        m_W_0.assembly();
        m_W_0_seq.assembly();
      }
      m_U_0_seq.set( 0.);
      m_U_0_seq.assembly();
      if (m_problem == 2) {
        m_Ue_0.set( 0.);
        m_Ue_0.assembly();
        m_Ue_0_seq.set( 0.);
        m_Ue_0_seq.assembly();
      }
    }
    
  }

  void EigenProblemALPDeim::readCollocationPts() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::readCollocationPts\n");
    std::string collocationPtsFileName;
    collocationPtsFileName = FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().collocationPtsFile;// load the collocation points file

    felInt numCollocationNode;
    std::ifstream collocationPtsFile(collocationPtsFileName.c_str());
    if(!collocationPtsFile) {
      FEL_ERROR("Fatal error: cannot open match file " + collocationPtsFileName );
    }
    collocationPtsFile >> numCollocationNode;

    if (numCollocationNode != m_dimCollocationPts) {
      FEL_WARNING("Wrong number of collocation points in ECGnode file.");
    }

    if (m_collocationNode == nullptr) {
      m_collocationNode = new felInt[m_dimCollocationPts];

      for(felInt j=0; j<m_dimCollocationPts; j++) {
        collocationPtsFile >> m_collocationNode[j];
      }
    }
    collocationPtsFile.close();
  }


  void EigenProblemALPDeim::initializeROM() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::initializeROM()\n");

    // Basis in collocation points
    if (m_basisDeimColloc == nullptr) {
      m_basisDeimColloc = new double* [m_dimCollocationPts];
      for (int i=0 ; i< m_dimCollocationPts; i++) {
        m_basisDeimColloc[i] = new double[m_dimCollocationPts];
      }
    }
    if (m_basisDeim == nullptr) {
      m_basisDeim = new double* [m_dimCollocationPts];
      for (int i=0 ; i< m_dimCollocationPts; i++) {
        m_basisDeim[i] = new double[m_dimRomBasis];
      }
    }


    if (m_useImprovedRec) {
      if (m_orthCompDeim == nullptr){
        if (m_dimRomBasis+m_dimOrthComp > m_dimCollocationPts) {
          FEL_WARNING("Dimension of orthogonal component exceeds");
          m_dimOrthComp = m_dimCollocationPts - m_dimRomBasis;
        }
        m_orthCompDeim = new double* [m_dimCollocationPts];
        for (int i=0 ; i< m_dimCollocationPts; i++) {
          m_orthCompDeim[i] = new double[m_dimOrthComp];
        }
      }
    }

    if (FelisceParam::instance().solveEigenProblem)
    {
      int rankProc;
      MPI_Comm_rank(m_petscComm,&rankProc);
      for (int i=0; i<m_dimCollocationPts; i++) {
        double* vec = new double[m_numDof];
        fromVecToDoubleStar(vec, m_basis[i], rankProc, 1, m_numDof);
        // Collocation of basis : m_basisDeim = P(m_basis)
        for (int j=0; j<m_dimCollocationPts; j++) {
          int k=m_collocationNode[j]-1;
          m_basisDeimColloc[j][i] = vec[k];
          if (i<m_dimRomBasis) {
            m_basisDeim[j][i] = m_basisDeimColloc[j][i];
          }
        }
        delete [] vec;
        if (FelisceParam::verbose() > 40)
          m_basis[i].view();
      }
    }
    else
    {
      // Basis in mesh points
      m_basis.resize(m_dimCollocationPts);
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_Matrix[m_idG].getVecs(m_basis[i],nullPetscVector);
        m_basis[i].setFromOptions();
        double* vec = new double[m_numDof];
        readEnsightFile(vec,"basis",i,m_numDof);
        fromDoubleStarToVec(vec, m_basis[i], m_numDof);
        // Collocation of basis : m_basisDeim = P(m_basis)
        for (int j=0; j<m_dimCollocationPts; j++) {
          int k=m_collocationNode[j]-1;
          m_basisDeimColloc[j][i] = vec[k];
          if (i<m_dimRomBasis) {
            m_basisDeim[j][i] = m_basisDeimColloc[j][i];
          }
        }
        delete [] vec;
        if (FelisceParam::verbose() > 40)
          m_basis[i].view();
      }

      readEigenValueFromFile();
      computeMassDeim();
      initializeSolution();
      
    }

    if (m_useImprovedRec && m_improvedRecType==0) {
      for (int i=0; i<m_dimCollocationPts; i++) {
        for (int j=0; j<m_dimOrthComp; j++) {
          m_orthCompDeim[i][j] = m_basisDeimColloc[i][j+m_dimRomBasis];
        }
      }
    }

  }

  void EigenProblemALPDeim::initializeSolution() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::initializeSolution()\n");

    if ( !m_solutionInitialized) {

      if (m_hatU == nullptr)
        m_hatU = new double[m_dimCollocationPts];
      // m_hatU = P(m_U_0)
      for (int j=0; j<m_dimCollocationPts; j++) {
        felInt iout = m_collocationNode[j]-1;
        AOApplicationToPetsc(m_ao, 1, &iout);
        double valueVec;
        m_U_0_seq.getValues(1, &iout, &valueVec);
        m_hatU[j] = valueVec;
      }

      // m_beta = \Phi*m_hatU
      if (m_beta == nullptr)
        m_beta = new double[m_dimRomBasis];
      //EigenProblemALP::
      projectOnBasis(m_U_0,m_beta,true);

      // Ionic variable W
      if ( (m_problem == 1) || (m_problem == 2) ) {
        if (m_hatW == nullptr)
          m_hatW = new double[m_dimCollocationPts];
        // m_hatW = P(m_W_0)
        for (int j=0; j<m_dimCollocationPts; j++) {
          felInt iout = m_collocationNode[j]-1;
          AOApplicationToPetsc(m_ao, 1, &iout);
          double valueVec;
          m_W_0_seq.getValues(1, &iout, &valueVec);
          m_hatW[j] = valueVec;
        }

      }

      // Extra-cell potential
      if (m_problem == 2) {
        if (m_hatUe == nullptr)
          m_hatUe = new double[m_dimCollocationPts];
        // m_hatUe = P(m_Ue_0)
        for (int j=0; j<m_dimCollocationPts; j++) {
          felInt iout = m_collocationNode[j]-1;
          AOApplicationToPetsc(m_ao, 1, &iout);
          double valueVec;
          m_Ue_0_seq.getValues(1, &iout, &valueVec);
          m_hatUe[j] = valueVec;
        }
        // m_xi = \Phi*m_hatUe
        if (m_xi == nullptr)
          m_xi = new double[m_dimRomBasis];
        //EigenProblemALP::
        projectOnBasis(m_Ue_0,m_xi,true);
      }
      
      if (FelisceParam::instance().optimizePotential) {
        if (m_potential == nullptr) 
          m_potential = new double[m_dimCollocationPts];
        // m_potential = P(m_initPot)
        for (int j=0; j<m_dimCollocationPts; j++) {
          felInt iout = m_collocationNode[j]-1;
          AOApplicationToPetsc(m_ao, 1, &iout);
          double valueVec;
          m_initPot.getValues(1, &iout, &valueVec);
          m_potential[j] = valueVec;
        }
      }

      if (m_resPoint) {
        if (m_resU == nullptr) {
          m_resU = new double[m_dimCollocationPts];
        }
        
        for (int i=0; i<m_dimCollocationPts; i++) {
          m_resU[i] = m_hatU[i];
          for (int j=0; j<m_dimRomBasis; j++) {
            m_resU[i] -= m_basisDeim[i][j]*m_beta[j];
          }
        }
        
        double maxRes = 0.;      
        for (int i=0; i<m_dimCollocationPts; i++) {
          if (m_resU[i] > maxRes) {
            maxRes = m_resU[i];
          }
        }
        std::cout << "maxRes = " << maxRes << std::endl;

      }
      
      m_solutionInitialized = true;
    }

  }

  // Modified Graam-Schmidt: orthonormalization (in the sense of m_massDeim) of A
  void EigenProblemALPDeim::MGS(double** A, double** R, int size) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::MGS\n");
    
    for(int i=0; i<size; i++) {

      // tmp = G phi_i
      double tmp[m_dimCollocationPts];
      for (int j=0; j<m_dimCollocationPts; j++) {
        tmp[j] = 0.;
        for (int k=0; k<m_dimCollocationPts; k++)
          tmp[j] += m_massDeim[j][k]*A[k][i];
      }

      // Diagonal of R
      R[i][i] =0.;
      for (int k=0; k<m_dimCollocationPts; k++)
        R[i][i] += A[k][i] * tmp[k];
      
      R[i][i] = std::sqrt(R[i][i]);
      for (int k=0; k<m_dimCollocationPts; k++)
        A[k][i] = A[k][i]/R[i][i];

      // Extradiag R
      for (int j=i+1; j<size; j++) {
        R[i][j] =0.;
        for (int k=0; k<m_dimCollocationPts; k++)
          R[i][j] += A[k][j]*tmp[k];
        R[j][i] = 0.;
        // A
        for (int k=0; k<m_dimCollocationPts; k++)
          A[k][j] -= R[i][j] * A[k][i];
      }
    }

  }

  void EigenProblemALPDeim::factorizationQR(double** A, double** Q, double** R, int sizeRow, int sizeCol) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::factorizationQR\n");
    // Copy A into W
    double** W = new double* [sizeRow];
    for (int i=0; i<sizeRow; i++) {
      W[i] = new double [sizeCol];
      for (int j=0; j<sizeCol; j++)
        W[i][j] = A[i][j];
    }

    for(int i=0; i<sizeCol; i++) {
      // Diagonal of R
      R[i][i] =0.;
      for (int j=0; j<sizeRow; j++)
        R[i][i] += W[j][i] * W[j][i];
      R[i][i] = std::sqrt(R[i][i]);

      // Q
      for (int j=0; j<sizeRow; j++)
        Q[j][i] = W[j][i] / R[i][i];

      // Extradiag R
      for (int k=i+1; k<sizeCol; k++) {
        R[i][k] =0.;
        for (int j=0; j<sizeRow; j++)
          R[i][k] += Q[j][i] * W[j][k];
        R[k][i] = 0.;
        // W
        for (int j=0; j<sizeRow; j++)
          W[j][k] -= R[i][k] * Q[j][i];
      }
    }

    for (int i=0; i<sizeRow; i++)
      delete [] W[i];
    delete [] W;

  }

  void EigenProblemALPDeim::inverseTriangular(double** A, double** Ainv, int sizeA) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::inverseTriangular\n");
    double det=1.;
    for (int l=0; l<sizeA; l++) {
      det = det*A[l][l];
    }
    if (det < 1.e-7) {
      std::cout << "det(R) = " << det << std::endl;
      FEL_ERROR("W is singular.");
    }

    for (int l=0; l<sizeA; l++) {
      double* tmpX;
      tmpX = new double[l+1];
      tmpX[l] = 1.0/(A[l][l]);
      for (int j=l-1; j>=0; j--) {
        tmpX[j] = 0.0;
        for (int k=j+1; k<=l; k++) {
          tmpX[j] -=A[j][k]*tmpX[k];
        }
        tmpX[j] = tmpX[j]/A[j][j];
      }

      for (int j=0; j<l+1; j++) {
        Ainv[j][l] = tmpX[j];
        if (j != l)
          Ainv[l][j] = 0.;
      }

      delete [] tmpX;
    }

  }

  // Projection on collocation points of mass matrix
  void EigenProblemALPDeim::computeMassDeim() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeMassDeim\n");

    if (m_massDeim == nullptr) {
      m_massDeim = new double* [m_dimCollocationPts];
      for (int i=0; i< m_dimCollocationPts; i++) {
        m_massDeim[i] = new double [m_dimCollocationPts];
      }
    }
    if (m_useImprovedRec || m_resPoint) {
      if (m_stiffDeim == nullptr) {
        m_stiffDeim = new double* [m_dimCollocationPts];
        for (int i=0; i< m_dimCollocationPts; i++) {
          m_stiffDeim[i] = new double [m_dimCollocationPts];
        }
      }
    }

    double** matrixQ;
    matrixQ = new double* [m_dimCollocationPts];
    for (int i=0; i< m_dimCollocationPts; i++) {
      matrixQ[i] = new double [m_dimCollocationPts];
    }
    double** matrixR;
    matrixR = new double* [m_dimCollocationPts];
    for (int i=0; i< m_dimCollocationPts; i++) {
      matrixR[i] = new double [m_dimCollocationPts];
    }
    double** matrixRinv;
    matrixRinv = new double* [m_dimCollocationPts];
    for (int i=0; i< m_dimCollocationPts; i++) {
      matrixRinv[i] = new double [m_dimCollocationPts];
    }
    double** matrixH;
    matrixH = new double* [m_dimCollocationPts];
    for (int i=0; i< m_dimCollocationPts; i++) {
      matrixH[i] = new double [m_dimCollocationPts];
    }

    factorizationQR(m_basisDeimColloc, matrixQ, matrixR, m_dimCollocationPts, m_dimCollocationPts);
#ifndef NDEBUG
    double checkQR = 0.;
    for (int i=0; i<m_dimCollocationPts; i++) {
      for (int j=0; j<m_dimCollocationPts; j++) {
        checkQR = m_basisDeimColloc[i][j];
        for (int k=0; k<m_dimCollocationPts; k++) {
          checkQR -= matrixQ[i][k]*matrixR[k][j];
        }
        if (std::abs(checkQR) > 1.e-3) {
          FEL_WARNING("Error in QR factorization of matrix W.");
        }
      }
    }
#endif
    inverseTriangular(matrixR, matrixRinv, m_dimCollocationPts);
#ifndef NDEBUG
    double checkRinv = 0.;
    for (int i=0; i<m_dimCollocationPts; i++) {
      for (int j=0; j<m_dimCollocationPts; j++) {
        checkRinv = 0.;
        if ( i==j ) checkRinv = 1.;
        for (int k=0; k<m_dimCollocationPts; k++) {
          checkRinv -= matrixR[i][k]*matrixRinv[k][j];
        }
        if (std::abs(checkRinv) > 1.e-3) {
          FEL_WARNING("Error in inverse of triangular matrix R.");
        }
      }
    }
#endif
    // H = R^-1 Q^T
    for (int iG=0; iG< m_dimCollocationPts; iG++) {
      for (int jG=0; jG< m_dimCollocationPts; jG++) {
        matrixH[iG][jG] =0.;
        for (int kSum=0; kSum< m_dimCollocationPts; kSum++)
          matrixH[iG][jG] += matrixRinv[iG][kSum] * matrixQ[jG][kSum];
      }
    }

    // G = H^T H
    for (int iG=0; iG< m_dimCollocationPts; iG++) {
      for (int jG=0; jG< m_dimCollocationPts; jG++) {
        m_massDeim[iG][jG] = 0.;
        for (int kSum=0; kSum< m_dimCollocationPts; kSum++)
          m_massDeim[iG][jG] += matrixH[kSum][iG] * matrixH[kSum][jG] ;
      }
    }

    if (m_useImprovedRec || m_resPoint) {
      // K : W^T K W = V^T K_fem V
      std::vector<PetscVector> matrixX;
      std::vector<PetscVector> matrixY;
      matrixX.resize(m_dimCollocationPts);
      matrixY.resize(m_dimCollocationPts);
      for (int i=0; i< m_dimCollocationPts; i++) {
        matrixX[i].duplicateFrom(m_basis[0]);
        matrixY[i].duplicateFrom(m_basis[0]);
      }
      // X = V H
      // V = m_basis
      for (int i=0; i<m_dimCollocationPts; i++) {
        for (int j=0; j<m_dimCollocationPts; j++) {
          matrixX[i].axpy(matrixH[j][i],m_basis[j]);
        }
      }
      // Y = K_fem X
      // K_fem = m_Matrix[m_idK];
      for (int i=0; i<m_dimCollocationPts; i++) {
        mult(m_Matrix[m_idK],matrixX[i],matrixY[i]);
      }
      // K = X^T K_fem X = X^T Y
      for (int i=0; i<m_dimCollocationPts; i++) {
        for (int j=0; j<m_dimCollocationPts; j++) {
          double valueK;
          dot(matrixX[i],matrixY[j], &valueK);
          m_stiffDeim[i][j] = valueK;
        }
      }
      for (int i=0; i< m_dimCollocationPts; i++) {
        matrixX[i].destroy();
        matrixY[i].destroy();
      }
    }
    
    if (m_resPoint) {
      m_invGK = new double* [m_dimCollocationPts];
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_invGK[i] = new double[m_dimCollocationPts];
      }
      
      double** invG = new double* [m_dimCollocationPts];
      for (int i=0; i<m_dimCollocationPts; i++) {
        invG[i] = new double[m_dimCollocationPts];
      }
      
      for (int i=0; i<m_dimCollocationPts; i++) {
        for (int j=i; j<m_dimCollocationPts; j++) {
          invG[i][j] = 0.;
          for (int k=0; k<m_dimCollocationPts; k++) {
            invG[i][j] += m_basisDeimColloc[i][k]*m_basisDeimColloc[j][k];
          }
          invG[j][i] = invG[i][j];
        }
      }
      
      for (int i=0; i<m_dimCollocationPts; i++) {
        for (int j=i; j<m_dimCollocationPts; j++) {
          m_invGK[i][j] = 0.;
          for (int k=0; k<m_dimCollocationPts; k++) {
            m_invGK[i][j] += invG[i][k]*m_stiffDeim[k][j];
          }
          m_invGK[j][i] = m_invGK[i][j];
        }
      }
      
      for (int i=0; i< m_dimCollocationPts; i++) {
        delete [] invG[i];
      }
      delete [] invG;
      
    }
    

    for (int i=0; i< m_dimCollocationPts; i++) {
      delete [] matrixQ[i];
      delete [] matrixR[i];
      delete [] matrixRinv[i];
      delete [] matrixH[i];
    }
    delete [] matrixQ;
    delete [] matrixR;
    delete [] matrixRinv;
    delete [] matrixH;
  }

  void EigenProblemALPDeim::computeProjectionPiV() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeProjectionPiV\n");

    if (m_projectorPiV.size() < 1) {
      std::vector<PetscVector> projectorPiW;
      projectorPiW.resize(m_dimCollocationPts);
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_Matrix[m_idG].getVecs(projectorPiW[i],nullPetscVector);
        projectorPiW[i].setFromOptions();
        for (int k=0; k<m_dimCollocationPts; k++)
          projectorPiW[i].axpy( m_basisDeimColloc[i][k], m_basis[k]);
      }

      m_projectorPiV.resize(m_dimCollocationPts);
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_Matrix[m_idG].getVecs(m_projectorPiV[i],nullPetscVector);
        m_projectorPiV[i].setFromOptions();
        for (int k=0; k<m_dimCollocationPts; k++)
          m_projectorPiV[i].axpy( m_massDeim[k][i], projectorPiW[k]);
      }
      for (int i=0; i<m_dimCollocationPts; i++) {
        projectorPiW[i].destroy();
      }
    }

  }

  void EigenProblemALPDeim::writeEnsightSolution(const int iter) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::writeEnsightSolution\n");
    PetscVector solU;
    solU.duplicateFrom(m_U_0);
    PetscVector solUe;
    solUe.duplicateFrom(m_U_0);
    PetscVector solW;
    solW.duplicateFrom(m_W_0);

    if (m_projectorPiV.size() < 1)
      computeProjectionPiV();

    if (iter > 0) {
      for (int k=0; k<m_dimCollocationPts; k++) {
        solU.axpy( m_hatU[k], m_projectorPiV[k]);
      }
      if ( (m_problem == 1) || (m_problem == 2)) {
        if (FelisceParam::instance().printIonicVar) {
          for (int k=0; k<m_dimCollocationPts; k++) {
            solW.axpy( m_hatW[k], m_projectorPiV[k]);
          }
        }
      }
      if (m_problem == 2) {
        for (int k=0; k<m_dimCollocationPts; k++) {
          solUe.axpy( m_hatUe[k], m_projectorPiV[k]);
        }
      }
    } else {
      solU.copyFrom(m_U_0);
      if ( (m_problem == 1) || (m_problem == 2)) {
        if (FelisceParam::instance().printIonicVar) {
          solW.copyFrom(m_W_0);
        }
      }
      if (m_problem == 2) {
        solUe.copyFrom(m_Ue_0);
      }
    }

    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);

    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];

    if (m_problem == 0) {
      fromVecToDoubleStar(tmpSolToSave, solU, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "sol");
    }

    if ( (m_problem == 1) || (m_problem == 2)) {
      fromVecToDoubleStar(tmpSolToSave, solU, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "potTransMemb");
      if (FelisceParam::instance().printIonicVar) {
        fromVecToDoubleStar(tmpSolToSave, solW, rankProc, 1);
        writeEnsightVector(tmpSolToSave, iter, "ionicVar");
      }
    }

    if (m_problem == 2) {
      fromVecToDoubleStar(tmpSolToSave, solUe, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "potExtraCell");
    }

    delete [] tmpSolToSave;
    solU.destroy();
    solUe.destroy();
    solW.destroy();

    std::vector<std::string> varNames;
    if (m_problem == 0) {
      varNames.resize(1);
      varNames[0] = "sol";
    } else if (m_problem == 1) {
      if (FelisceParam::instance().printIonicVar) {
        varNames.resize(2);
        varNames[0] = "potTransMemb";
        varNames[1] = "ionicVar";
      } else {
        varNames.resize(1);
        varNames[0] = "potTransMemb";
      }
    } else if (m_problem == 2) {
      if (FelisceParam::instance().printIonicVar) {
        varNames.resize(3);
        varNames[0] = "potTransMemb";
        varNames[1] = "potExtraCell";
        varNames[2] = "ionicVar";
      } else {
        varNames.resize(2);
        varNames[0] = "potTransMemb";
        varNames[1] = "potExtraCell";
      }
    }
    if (rankProc == 0)
      writeEnsightCase(iter+1, FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution,varNames, FelisceParam::instance().time);

  }

  void EigenProblemALPDeim::computeRHSDeim() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeRHSDeim\n");

    if (m_hatF == nullptr) {
      m_hatF = new double[m_dimCollocationPts];
    }

    switch (m_problem) {
    case 0: {
      // FKPP:
      // TO DO
      FEL_ERROR("FKPP RHS NOT DEFINED");
      break;
    }
    case 1:
    case 2: {
      if (m_hatG == nullptr) {
        m_hatG = new double[m_dimCollocationPts];
      }

      // MONODOMAIN or BIDOMAIN:
      double Am = FelisceParam::instance().Am;
      double Cm = FelisceParam::instance().Cm;
      // FhN
      double a = FelisceParam::instance().alpha;
      double s = FelisceParam::instance().f0;
      double eps = FelisceParam::instance().epsilon;
      double gam = FelisceParam::instance().gammaEl;
      // Schaf
      double& tauIn = FelisceParam::instance().tauIn;
      double& tauOut  = FelisceParam::instance().tauOut;
      double& tauOpen  = FelisceParam::instance().tauOpen;
      double& tauClose = FelisceParam::instance().tauClose;
      
      
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_hatF[i] = 0.;
        
        if(m_resPoint) {
          for (int k=0; k<m_dimCollocationPts; k++) {
            m_hatF[i] += m_invGK[i][k]*m_resU[k];
          }
        }
        
        for (int j=0; j<m_dimRomBasis; j++) {
          double coef = m_beta[j];
          if (m_problem == 2)
            coef += m_xi[j];
          if (FelisceParam::instance().optimizePotential) {
            m_hatF[i] += coef * (- m_eigenValue[j] - m_coefChi*m_potential[i]) * m_basisDeim[i][j];
          }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
          else {
            m_hatF[i] += coef * (- m_eigenValue[j] - m_coefChi*m_hatU[i]) * m_basisDeim[i][j];
          }
        }
        if (FelisceParam::instance().typeOfIonicModel == "fhn") {
          if (FelisceParam::instance().hasInfarct)
            s = m_coefFhNs[i];
          m_hatF[i] += s * Am * ( - m_hatU[i]*m_hatU[i]*m_hatU[i] + (1+a) * m_hatU[i]*m_hatU[i] - a * m_hatU[i] ) - Am * m_hatW[i];
          m_hatG[i] = eps * ( gam * m_hatU[i] - m_hatW[i] );
        }
        else if (FelisceParam::instance().typeOfIonicModel == "schaf") {
          if (FelisceParam::instance().hasInfarct)
            tauOut = m_coefTauOut[i];
          m_hatF[i] += Am * ( m_hatW[i] * m_hatU[i] * m_hatU[i] * (1 - m_hatU[i]) / tauIn - m_hatU[i] / tauOut);

          if (m_hatU[i] < FelisceParam::instance().vGate) {
            m_hatG[i] = (1 - m_hatW[i]) / tauOpen;
          }
          else {
            if (FelisceParam::instance().hasHeteroTauClose) {
              tauClose = m_coefTauClose[i];
            }
            m_hatG[i] = - m_hatW[i] / tauClose;
          }
        }

        if (FelisceParam::instance().hasSource) {
          double delay[m_numSource];
          double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
          for (int iS=0; iS<m_numSource; iS++) {
            delay[iS] = FelisceParam::instance().delayStim[iS];
            if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
              m_hatF[i] += Am*m_source[iS][i];
          }
        }

        m_hatF[i] = m_hatF[i] / (Am*Cm);
      }
    }
    }

  }

  void EigenProblemALPDeim::computeTheta() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeTheta\n");
    if (m_theta == nullptr) {
      m_theta = new double* [m_dimRomBasis];
      for (int i=0; i< m_dimRomBasis; i++) {
        m_theta[i] = new double [m_dimRomBasis];
      }
      if(m_useImprovedRec && m_improvedRecType==0) {
        m_thetaOrth = new double* [m_dimOrthComp];
        for (int i=0; i< m_dimOrthComp; i++) {
          m_thetaOrth[i] = new double [m_dimRomBasis];
        }
      }
    }

    double* tmp = new double [m_dimCollocationPts];

    // \theta = ( \Phi^T G ) (F * \Phi)
    
    double* extraF = nullptr;
    if (m_problem == 0) {
      double Cm = FelisceParam::instance().Cm;
      extraF = new double[m_dimCollocationPts];
      for (int k=0; k< m_dimCollocationPts; k++) {
        extraF[k] = Cm * m_hatU[k];
        for (int id=0; id< m_dimRomBasis; id++) {
            extraF[k] += - m_eigenValue[id] * m_beta[id] * m_basisDeim[k][id];
        }
      }
    }
    
    for (int id=0; id< m_dimRomBasis; id++) {
      for (int jd=0; jd< m_dimCollocationPts; jd++) {
        tmp[jd] = 0.;
        for (int k=0; k< m_dimCollocationPts; k++)
          tmp[jd] += m_basisDeim[k][id] * m_massDeim[k][jd];
      }

      for (int jd=0; jd< id+1; jd++) {
        m_theta[id][jd] = 0.;
        for (int k=0; k< m_dimCollocationPts; k++) {
          m_theta[id][jd] += tmp[k] * m_hatF[k] * m_basisDeim[k][jd];
          if (m_problem == 0)
            m_theta[id][jd] += tmp[k] * extraF[k] * m_basisDeim[k][jd];            
        }        
        if (jd != id)
          m_theta[jd][id] = m_theta[id][jd];
      }
    }
    if(m_useImprovedRec && m_improvedRecType==0) {
      // \theta^orth = ( \Psi^T G ) (F * \Phi)
      for (int id=0; id< m_dimOrthComp; id++) {
        for (int jd=0; jd< m_dimCollocationPts; jd++) {
          tmp[jd] = 0.;
          for (int k=0; k< m_dimCollocationPts; k++)
            tmp[jd] += m_orthCompDeim[k][id] * m_massDeim[k][jd];
        }
        
        for (int jd=0; jd< m_dimRomBasis; jd++) {
          m_thetaOrth[id][jd] = 0.;
          for (int k=0; k< m_dimCollocationPts; k++)
            m_thetaOrth[id][jd] += tmp[k] * m_hatF[k] * m_basisDeim[k][jd];
        }
      }
    }
    
    if (extraF) delete [] extraF;
    delete [] tmp;

  }

  void EigenProblemALPDeim::computeMatrixM() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeMatrixM()\n");
    if ( m_matrixM == nullptr ) {
      m_matrixM = new double*[m_dimRomBasis];
      for (int i=0; i<m_dimRomBasis; i++) {
        m_matrixM[i] = new double[m_dimRomBasis];
      }
    }

    double epsilonLambda = 5.e-01; //1.e-01;
    for (int i=0; i<m_dimRomBasis; i++) {
      m_matrixM[i][i] = 0.;
      for (int j=0; j<i; j++) {
        m_matrixM[i][j] = 0.;
        if ( std::abs(m_eigenValue[i] - m_eigenValue[j] ) > epsilonLambda ) {
          m_matrixM[i][j] = m_coefChi / ( m_eigenValue[j] - m_eigenValue[i] ) * m_theta[i][j];
        }
        m_matrixM[j][i] = (-1.0) * m_matrixM[i][j];
      }
    }

  }

  void EigenProblemALPDeim::computeHatU() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeHatU\n");

    projectOnDof(m_beta,m_hatU,m_dimRomBasis);

    if (m_problem == 0) { // FKPP (fix max and min u)
      for (int i=0; i<m_dimCollocationPts; i++) {
        if (m_hatU[i] < 0.)
          m_hatU[i] = 0.;
        else if (m_hatU[i] > 1.)
          m_hatU[i] = 1.;
      }
    }
    
    if (m_resPoint) {
      double maxRes = 0.;      
      for (int i=0; i<m_dimCollocationPts; i++) {
        if (m_resU[i] > maxRes) {
          maxRes = m_resU[i];
        }
        m_hatU[i] += m_resU[i];
      }
      std::cout << "maxRes = " << maxRes << std::endl;
    }
    

    if ( (m_problem == 1) || (m_problem == 2)) {
      double dt = FelisceParam::instance().timeStep;
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_hatW[i] += dt*m_hatG[i];
      }
    }

    if (m_problem == 2)
      projectOnDof(m_xi,m_hatUe,m_dimRomBasis);

    // Potential evolution
    // Hyp: dt(U) = dt(u) = hat(F(u))
    if (FelisceParam::instance().optimizePotential) {
      double dt = FelisceParam::instance().timeStep;
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_potential[i] += dt*m_hatF[i]; // Warning! Not work like that!
      }
    }
    
  }

  void EigenProblemALPDeim::computeTensor() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::computeTensor()\n");

    if (m_problem ==2) {
      // If \sigma_I = \alpha \sigma_E
      // then A \xi + B \beta = 0
      // A_ij = \sum_{h,l}^{N_p} (\alpha + 1)(\chi m_hatU[l] + lambda_i) \phi_j(x_h) G_hl \phi_i(x_h)
      // "+" \int u_e = 0
      if (m_matrixA == nullptr) {
        m_matrixA = new double* [m_dimRomBasis+1];
        for (int i=0; i<m_dimRomBasis+1; i++) {
          m_matrixA[i] = new double[m_dimRomBasis+1];
        }
      }
      // B_ij = \sum_{h,l}^{N_p} (\chi m_hatU[l] + lambda_i) \phi_j(x_h) G_hl \phi_i(x_h)
      if (m_matrixB == nullptr) {
        m_matrixB = new double* [m_dimRomBasis];
        for (int i=0; i<m_dimRomBasis; i++) {
          m_matrixB[i] = new double[m_dimRomBasis];
        }
      }
      // In order to compute \int u_e = 0 :
      // m_sumG = [1 ... 1] * G
      if (m_sumG == nullptr) {
        m_sumG = new double[m_dimCollocationPts];
        for (int i=0; i<m_dimCollocationPts; i++) {
          m_sumG[i] = 0.;
          for (int j=0; j<m_dimCollocationPts; j++) {
            m_sumG[i] += m_massDeim[j][i];
          }
        }
      }
    }
  }

  void EigenProblemALPDeim::setHeteroTauClose(std::vector<double>& valueTauClose) {

    if (m_coefTauClose == nullptr)
      m_coefTauClose = new double[m_dimCollocationPts];
    // m_coefFhNs = P(valuef0)
    for (int j=0; j<m_dimCollocationPts; j++) {
      m_coefTauClose[j] = valueTauClose[m_collocationNode[j]-1];
    }

  }

  void EigenProblemALPDeim::setHeteroTauOut(std::vector<double>& valueTauOut) {

    if (m_coefTauOut == nullptr)
      m_coefTauOut = new double[m_dimCollocationPts];
    // m_coefFhNs = P(valuef0)
    for (int j=0; j<m_dimCollocationPts; j++) {
      m_coefTauOut[j] = valueTauOut[m_collocationNode[j]-1];
    }

  }

  void EigenProblemALPDeim::setFhNf0(std::vector<double>& valuef0) {

    if (m_coefFhNs == nullptr)
      m_coefFhNs = new double[m_dimCollocationPts];
    // m_coefFhNs = P(valuef0)
    for (int j=0; j<m_dimCollocationPts; j++) {
      m_coefFhNs[j] = valuef0[m_collocationNode[j]-1];
    }

  }

  void EigenProblemALPDeim::setIapp(std::vector<double>& iApp, int& idS) {
    if (m_source == nullptr) {
      m_source = new double*[m_numSource];
      for (int i=0; i<m_numSource; i++) {
        m_source[i] = new double[m_dimCollocationPts];
      }
    }

    // m_source = P(iApp)
    for (int j=0; j<m_dimCollocationPts; j++) {
      m_source[idS][j] = iApp[m_collocationNode[j]-1];
    }
  }

  //Update functions
  void EigenProblemALPDeim::updateBasis() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::updateBasis()\n");

    double deltaPhi[m_dimCollocationPts][m_dimRomBasis];
    double dt = FelisceParam::instance().timeStep;

    double** psiC = nullptr;
    double** res = nullptr;
    // if (m_useImprovedRec==true) => d\Phi/dt = \Phi*M + R
    // if improvedRecType==0 => R = \Psi*C
    // if improvedRecType==1 => R = (-L-lambda)^{-1} t
    if (m_useImprovedRec)
    {
      switch (m_improvedRecType) {
        case 0: {
          // 1) Compute \Psi = (I-Phi^T Phi G)W_\orth
          double** phiphiT = new double* [m_dimCollocationPts];
          for (int i=0; i<m_dimCollocationPts; i++)
            phiphiT[i] = new double[m_dimCollocationPts];
          for (int i=0; i<m_dimCollocationPts; i++) {
            for (int j=0; j<m_dimCollocationPts; j++) {
              phiphiT[i][j] = 0.;
              for (int k=0; k<m_dimRomBasis; k++) {
                phiphiT[i][j] += m_basisDeim[i][k] * m_basisDeim[j][k];
              }
            }
          }
          for (int i=0; i<m_dimCollocationPts; i++) {
            double* vec = new double[m_dimCollocationPts];
            for (int k=0; k<m_dimCollocationPts; k++) {
              if (k == i)
                vec[k] = 1.;
              else
                vec[k] = 0.;
              for (int l=0; l<m_dimCollocationPts; l++) {
                vec[k] -= phiphiT[i][l] * m_massDeim[l][k];
              }
            }
            for (int j=0; j<m_dimOrthComp; j++) {
              m_orthCompDeim[i][j] = 0.;
              for (int k=0; k<m_dimCollocationPts; k++) {
                m_orthCompDeim[i][j] += vec[k] * m_basisDeimColloc[k][j+m_dimRomBasis];
              }
            }
            delete [] vec;
          }
          for (int i=0; i<m_dimCollocationPts; i++)
            delete [] phiphiT[i];
          delete [] phiphiT;
          
          // 2) Compute "C" and \Psi*C
          psiC = new double* [m_dimCollocationPts];
          for (int i=0; i<m_dimCollocationPts; i++)
            psiC[i] = new double[m_dimRomBasis];
          double* vectorC = new double [m_dimOrthComp];
          double** matrixHtmp = new double* [m_dimCollocationPts];
          for (int i=0; i<m_dimCollocationPts; i++)
            matrixHtmp[i] = new double[m_dimOrthComp];
          
          // H_tmp = K \Psi - \chi G (hat{u}*\Psi)
          for (int j=0; j<m_dimCollocationPts; j++) {
            for (int k=0; k<m_dimOrthComp; k++) {
              matrixHtmp[j][k] = 0.;
              for (int l=0; l<m_dimCollocationPts; l++) {
                matrixHtmp[j][k] += (m_stiffDeim[j][l] - m_coefChi * m_massDeim[j][l] * m_hatU[l] ) * m_orthCompDeim[l][k];
              }
            }
          }
          
          // solve (H_const+h_i) c_i = \chi \Psi^T G f*\Phi \forall i
          for (int i=0; i<m_dimRomBasis; i++) {
            double checkOrth = 0.;
            double* rhsC = new double[m_dimOrthComp];
            for (int j=0; j<m_dimOrthComp; j++) {
              rhsC[j] = m_coefChi * m_thetaOrth[j][i];
              vectorC[j] = 1.;
              checkOrth += m_thetaOrth[j][i]*m_thetaOrth[j][i];
            }
            
            if (FelisceParam::verbose()) {
              std::cout << "checkOrth = " << checkOrth << std::endl;
            }
            if (checkOrth > 1.e-4)
            {
              double** matrixH = new double* [m_dimOrthComp];
              for (int j=0; j<m_dimOrthComp; j++)
                matrixH[j] = new double[m_dimOrthComp];
              
              // H = \Psi^T ( H_tmp + h_i )
              // h_i = - \lambda_i G \Psi
              
              for (int j=0; j<m_dimOrthComp; j++) {
                double* tmpVec = new double[m_dimCollocationPts];
                for (int k=0; k<m_dimCollocationPts; k++) {
                  tmpVec[k] = 0.;
                  for (int l=0; l<m_dimCollocationPts; l++) {
                    tmpVec[k] += m_eigenValue[i] * m_massDeim[k][l] * m_orthCompDeim[l][j];
                  }
                }
                for (int k=0; k<m_dimOrthComp; k++) {
                  matrixH[k][j] = 0.;
                  for (int l=0; l<m_dimCollocationPts; l++) {
                    matrixH[k][j] += m_orthCompDeim[l][k] * ( matrixHtmp[l][j] - tmpVec[l]);
                  }
                }
                delete [] tmpVec;
              }
              
              ConjGrad(matrixH, rhsC, vectorC, 1.e-7, 1000, m_dimOrthComp, true);
              for (int j=0; j<m_dimCollocationPts; j++) {
                psiC[j][i] = 0.;
                for (int k=0; k<m_dimOrthComp; k++) {
                  psiC[j][i] += m_orthCompDeim[j][k]*vectorC[k];
                }
              }
              for (int j=0; j<m_dimOrthComp; j++)
                delete [] matrixH[j];
              delete [] matrixH;
            }
            else {
              for (int j=0; j<m_dimCollocationPts; j++) {
                psiC[j][i] = 0.;
              }          
            }
            delete [] rhsC;
          }
          
          delete [] vectorC;
          for (int i=0; i<m_dimOrthComp; i++)
            delete [] matrixHtmp[i];
          delete [] matrixHtmp;

          break;
        }
        case 1: {
          res = new double* [m_dimRomBasis];
          for (int i=0; i<m_dimRomBasis; i++) {
            res[i] = new double[m_dimCollocationPts];
          }
          // 1) Compute H = K - \chi diag(\hat{u}^1/2) G diag(\hat{u}^1/2) - G \lambda_i = Htmp - G \lambda_i
          double** matrixH = new double* [m_dimCollocationPts];
          double** matrixHtmp = new double* [m_dimCollocationPts];
          for (int j=0; j<m_dimCollocationPts; j++) {
            matrixH[j] = new double[m_dimCollocationPts];
            matrixHtmp[j] = new double[m_dimCollocationPts];
          }
          // Htmp = K - 1/2 (diag(\hat{U}^{1/2}) G diag(\hat{U}^{1/2})
          for (int i=0; i<m_dimCollocationPts; i++) {
            for (int j=0; j<m_dimCollocationPts; j++) {
              matrixHtmp[i][j] = m_stiffDeim[i][j];
              if (FelisceParam::instance().optimizePotential) {
                matrixHtmp[i][j] -= m_coefChi * (m_potential[i]/std::fabs(m_potential[i])) * std::sqrt(std::fabs(m_potential[i])) * m_massDeim[i][j] * std::sqrt(std::fabs(m_potential[j]));
              }
              else {
              matrixHtmp[i][j] -= m_coefChi * (m_hatU[i]/std::fabs(m_hatU[i])) * std::sqrt(std::fabs(m_hatU[i])) * m_massDeim[i][j] * std::sqrt(std::fabs(m_hatU[j]));
              }
            }
          }
          // 2) Compute t_i = \chi G (\hat{f}*\phi_i - \sum_j \theta_ji \phi_j)
          double* vectorT = new double[m_dimCollocationPts];
          // H*r_i = t_i
          for (int i=0; i<m_dimRomBasis; i++) {
            // H = Htmp - G \lambda_i
            for (int j=0; j<m_dimCollocationPts; j++) {
              for (int k=0; k<m_dimCollocationPts; k++) {
                matrixH[j][k] = matrixHtmp[j][k] - m_eigenValue[i]*m_massDeim[j][k];
              }
            }
            // tmpVec = \sum_j \theta_ji \phi_j
            double tmpVec[m_dimCollocationPts];
            for (int k=0; k<m_dimCollocationPts; k++) {
              tmpVec[k] = 0.;
              for (int j=0; j<m_dimRomBasis; j++) {
                tmpVec[k] += m_theta[j][i]*m_basisDeim[k][j];
              }
            }
            // tmpVec2 = \hat{f} * \phi_i - tmpVec
            double tmpVec2[m_dimCollocationPts];
            for (int k=0; k<m_dimCollocationPts; k++) {
              tmpVec2[k] = m_hatF[k]*m_basisDeim[k][i] - tmpVec[k];
            }
            // t_i = \chi G (\hat{f} * \phi_i - \sum_j \theta_ji \phi_j) = \chi G (\hat{f} * \phi_i - tmpVec) = \chi G tmpVec2
            for (int j=0; j<m_dimCollocationPts; j++) {
              vectorT[j] = 0.;
              for (int k=0; k<m_dimCollocationPts; k++) {
                vectorT[j] += m_coefChi * m_massDeim[j][k] * tmpVec2[k];
              }
            }

            
            double Ht[m_dimCollocationPts];
            double tTHt = 0.;
            double tTt = 0.;
            for (int j=0; j<m_dimCollocationPts; j++) {
              Ht[j] = 0.;
              for (int k=0; k<m_dimCollocationPts; k++) {
                Ht[j] += matrixH[j][k]*vectorT[k];
              }
            }
            for (int k=0; k<m_dimCollocationPts; k++) {
              tTHt += vectorT[k]*Ht[k];
              tTt += vectorT[k]*vectorT[k];
            }
            double alpha;
            alpha = tTt/tTHt;
           // std::cout << "alpha  " << alpha << std::endl;
            // double resT = 0.;
            double resNorm = 0.;
            for (int j=0; j<m_dimCollocationPts; j++) {
              // resT += vectorT[j]*vectorT[j];
              resNorm += (vectorT[j]-alpha*Ht[j])*(vectorT[j]-alpha*Ht[j]);
            }
           // std::cout << "res = " << resT << " - " << resNorm << std::endl;
            if (resNorm > 1.e-3) {
              for (int j=0; j<m_dimCollocationPts; j++) {
                res[i][j] = alpha*vectorT[j];
              }
              ConjGrad(matrixH, vectorT, res[i], 1.e-2, 10, m_dimCollocationPts, true);
            }
            else {
              for (int j=0; j<m_dimCollocationPts; j++) {
                res[i][j] = 0.;
              }
            }
            
          }
          
          delete [] vectorT;
          for (int i=0; i<m_dimCollocationPts; i++) {
            delete [] matrixH[i];
            delete [] matrixHtmp[i];
          }
          delete [] matrixH;
          delete [] matrixHtmp;

          break;
        }
        default:
          break;
      }
    }
    
    for (int i=0; i<m_dimCollocationPts; i++) {
      for (int j=0; j<m_dimRomBasis; j++) {
        deltaPhi[i][j] = 0.;
        for (int k=0; k<m_dimRomBasis; k++) {
          deltaPhi[i][j] += m_basisDeim[i][k]*m_matrixM[j][k];
        }
        // 3) d\Phi += \Psi*C
        if(m_useImprovedRec) {
          if (m_improvedRecType==0) {
            deltaPhi[i][j] += psiC[i][j];
          }
          else if (m_improvedRecType==1) {
            deltaPhi[i][j] += res[j][i];
          }
        }
      }
    }
    if(m_useImprovedRec) {
      if (m_improvedRecType==0) {
        for (int i=0; i<m_dimCollocationPts; i++)
          delete [] psiC[i];
        delete [] psiC;
      }
      else if (m_improvedRecType==1) {
        for (int i=0; i<m_dimRomBasis; i++)
          delete [] res[i];
        delete [] res;        
      }
    }

    for (int i=0; i<m_dimCollocationPts; i++)
      for (int j=0; j<m_dimRomBasis; j++)
        m_basisDeim[i][j] += dt*deltaPhi[i][j];

    
    if (m_resPoint) {
      for (int i=0; i<m_dimCollocationPts; i++) {
        m_resU[i] = dt*m_hatF[i];
        for (int j=0; j<m_dimRomBasis; j++) {
          m_resU[i] -= dt*deltaPhi[i][j]*m_beta[j];
        }
      }
    }
    
    
    double** matrixR;
    matrixR = new double* [m_dimRomBasis];
    for (int i=0; i< m_dimRomBasis; i++) {
      matrixR[i] = new double [m_dimRomBasis];
    }

    MGS(m_basisDeim, matrixR, m_dimRomBasis);

    for (int i=0; i< m_dimRomBasis; i++) {
      delete [] matrixR[i];
    }
    delete [] matrixR;

  }

  void EigenProblemALPDeim::setPotential()
  {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::setPotential()\n");
    int Nm = FelisceParam::instance().numApproxMode;
    int nCutOff = FelisceParam::instance().nCutOff;
    
    m_initPot.duplicateFrom(m_basis[0]);
    m_initPot.copyFrom(m_U_0);
    
    int sizePb = -1;
    std::vector<PetscVector> lagMult;
    std::vector<PetscVector> errorRep;
    if (m_problem < 2)
    { // FKPP or Monodomain
      sizePb = 1;
    }
    else if (m_problem == 2)
    { // Bidomain
      sizePb = 2;
    }
    else
      FEL_ERROR("Problem not found.");
    lagMult.resize(sizePb);
    errorRep.resize(sizePb);
    
    for (int i=0; i<sizePb; i++) {
      lagMult[i].duplicateFrom(m_basis[0]);
      errorRep[i].duplicateFrom(m_basis[0]);
    }
    
    PetscVector tmpVec;
    tmpVec.duplicateFrom(m_basis[0]);
    
    double errorU = 1.;
    double errorUe = 1.;
    
    std::vector<double*> coeffBeta;
    coeffBeta.resize(sizePb);
    
    //EigenProblemALP::
    if (m_beta == nullptr) {
      m_beta = new double[Nm];
      projectOnBasis(m_U_0,m_beta,true,Nm);
    }
    m_solutionInitialized = true;
    coeffBeta[0] = m_beta;
    
    errorRep[0].copyFrom(m_U_0);
    for (int i=0; i<Nm; i++) {
      double coef = -m_beta[i];
      errorRep[0].axpy(coef,m_basis[i]);
    }
    mult(m_Matrix[m_idG],errorRep[0],tmpVec);
    dot(errorRep[0],tmpVec,&errorU);
//    errorU = std::sqrt(errorU)/Nm;
    
    if (m_problem == 2) {
      //EigenProblemALP::
      if (m_xi == nullptr) {
        m_xi = new double[Nm];
        projectOnBasis(m_Ue_0,m_xi,true,Nm);
      }
      coeffBeta[1] = m_xi;
      errorRep[1].copyFrom(m_Ue_0);
      for (int i=0; i<Nm; i++) {
        double coef = -m_xi[i];
        errorRep[1].axpy(coef,m_basis[i]);
      }
      mult(m_Matrix[m_idG],errorRep[1],tmpVec);
      dot(errorRep[1],tmpVec,&errorUe);
//      errorUe = std::sqrt(errorUe)/Nm;
    }
    
    for (int i=0; i<sizePb; i++) {
      lagMult[i].copyFrom(errorRep[i]);
      lagMult[i].scale(-1.);
    }
    
    PetscVector dLdU;
    dLdU.duplicateFrom(m_basis[0]);
    
    PetscVector phi_ij;
    phi_ij.duplicateFrom(m_basis[0]);
    
    PetscVector dUphi;
    dUphi.duplicateFrom(m_basis[0]);
    
    PetscVector dU;
    dU.duplicateFrom(m_basis[0]);
    
    PetscVector dUphiTG;
    dUphiTG.duplicateFrom(m_basis[0]);
    std::vector<PetscVector> lKeps;
    lKeps.resize(sizePb);
    for (int i=0; i<sizePb; i++) {
      lKeps[i].duplicateFrom(m_basis[0]);
    }
    
    int numIter = 0;
    double step = 0.25; // 1.e-1;
    double k[2];
    k[0] = 50.;
    k[1] = 10.;
    
    double** q = new double* [sizePb];
    for (int iPb=0; iPb<sizePb; iPb++) {
      q[iPb] = new double[nCutOff];
    }
    double** theta = new double* [nCutOff];
    for (int i=0; i<nCutOff; i++) {
      theta[i] = new double[nCutOff];
    }
    
    PetscPrintf(PETSC_COMM_WORLD,"numIter = %i \n",numIter);
    PetscPrintf(PETSC_COMM_WORLD,"error = %e, %e \n",errorU,errorUe);
    
    while ( (errorU > 1.e-08) && (errorUe > 1.e-08) && (numIter < 5000) )
    {
      // q[iPb]_j = <lagMult[iPb]+k*eps[iPb], phi_j>
      for (int iPb=0; iPb<sizePb; iPb++) {
        lKeps[iPb].copyFrom(lagMult[iPb]);
        lKeps[iPb].axpy(k[iPb],errorRep[iPb]);
        //EigenProblemALP::
        projectOnBasis(lKeps[iPb], q[iPb], true, nCutOff);
      }
      
      // dL/dU = (U - u0)
      dLdU.copyFrom(m_initPot);
      dLdU.axpy(-1.,m_U_0);
      double reg = 1.;
      dLdU.scale(reg);
      // dL/dU -= \chi * \sum \sum ( sum_pb(beta_i q_j) / (lambda_j - lambda_i) phi_i phi_j
      for (int i=0; i<Nm; i++) {
        for (int j=0; j<nCutOff; j++) {
          if ( std::fabs(m_eigenValue[j] - m_eigenValue[i]) > 1.e-1 ) {
            pointwiseMult(phi_ij, m_basis[i], m_basis[j]);
            double coef = 0.;
            for (int iPb=0; iPb<sizePb; iPb++) {
              coef += coeffBeta[iPb][i]*q[iPb][j];
            }
            coef = - m_coefChi * coef/(m_eigenValue[j] - m_eigenValue[i]);
            dLdU.axpy(coef,phi_ij);
          }
        }
      }
      
      // dU = -s dL/dU
      dU.set(0.);
      dU.axpy(-step,dLdU);
      
      // theta_ij = <dU phi_i, phi_j>
      for (int i=0; i<nCutOff; i++) {
        pointwiseMult(dUphi, dU, m_basis[i]);
        mult(m_Matrix[m_idG], dUphi, dUphiTG);
        for (int j=0; j<=i; j++) {
          dot(dUphiTG, m_basis[j], &theta[i][j]);
          if (i != j)
            theta[j][i] = theta[i][j];
        }
      }
      
      // Update phi
      for (int i=0; i<nCutOff; i++) {
        for (int j=0; j<nCutOff; j++) {
          if ( std::fabs(m_eigenValue[j] - m_eigenValue[i]) > 1.e-1 ) {
            double coef = m_coefChi * theta[i][j]/(m_eigenValue[j] - m_eigenValue[i]);
            m_basis[i].axpy(coef, m_basis[j]);
          }
        }
      }
      //EigenProblemALP::
      MGS(nCutOff);
      
      // Update lambda
      for (int i=0; i<nCutOff; i++) {
        m_eigenValue[i] -= m_coefChi * theta[i][i];
      }
      
      // Update beta
      //EigenProblemALP::
      projectOnBasis(m_U_0,m_beta,true,Nm);
      // Update xi
      if (m_problem == 2) {
        //EigenProblemALP::
        projectOnBasis(m_Ue_0,m_xi,true,Nm);
      }
      
      // Compute error
      errorRep[0].copyFrom(m_U_0);
      for (int i=0; i<Nm; i++) {
        errorRep[0].axpy(-1.*m_beta[i],m_basis[i]);
      }
      mult(m_Matrix[m_idG],errorRep[0],tmpVec);
      dot(errorRep[0],tmpVec,&errorU);
//      errorU = std::sqrt(errorU)/Nm;
      if (m_problem == 2) {
        errorRep[1].copyFrom(m_Ue_0);
        for (int i=0; i<Nm; i++) {
          errorRep[1].axpy(-1.*m_xi[i],m_basis[i]);
        }
        mult(m_Matrix[m_idG],errorRep[1],tmpVec);
        dot(errorRep[1],tmpVec,&errorUe);
//        errorUe = std::sqrt(errorUe)/Nm;
      }
      
      // Update Lagrangian Multiplicator
      for (int iPb=0; iPb<sizePb; iPb++) {
        lagMult[iPb].axpy(step, errorRep[iPb]);
      }
      
      // Update initial potential
      m_initPot.axpy(1.,dU);
      
      numIter++;
      
      PetscPrintf(PETSC_COMM_WORLD,"numIter = %i \n",numIter);
      PetscPrintf(PETSC_COMM_WORLD,"error = %e, %e \n",errorU,errorUe);
    }
    
    for (int iPb=0; iPb<sizePb; iPb++) {
      lagMult[iPb].destroy();
      errorRep[iPb].destroy();
      lKeps[iPb].destroy();
    }
    dLdU.destroy();
    phi_ij.destroy();
    dUphi.destroy();
    dU.destroy();
    dUphiTG.destroy();
    tmpVec.destroy();
    
    for (int i=0; i<sizePb; i++) {
      delete [] q[i];
    }
    delete [] q;
    for (int i=0; i<nCutOff; i++) {
      delete [] theta[i];
    }
    delete [] theta;
    
    // Write initial potential in Ensight file
    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];
    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);
    fromVecToDoubleStar(tmpSolToSave, m_initPot, rankProc, 1);
    writeEnsightVector(tmpSolToSave, 0, "initialPotential");
    delete [] tmpSolToSave;
    
    std::string fileName = FelisceParam::instance().resultDir + "/eigenValue";
    viewALP(m_eigenValue,m_dimRomBasis,fileName);
    
    checkBasis(Nm);
    
  }
  
  void EigenProblemALPDeim::updateBeta() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::updateBeta()\n");

    double dt = FelisceParam::instance().timeStep;
    double deltaBeta[m_dimRomBasis];
    
    double Cm = FelisceParam::instance().Cm;

    // beta^{n+1} = beta^n - dt * M^n * beta^n + dt * (\Phi^T G F^)^n

    double PhiTG[m_dimRomBasis][m_dimCollocationPts];
    for (int i=0; i<m_dimRomBasis; i++) {
      for (int j=0; j<m_dimCollocationPts; j++) {
        PhiTG[i][j] = 0.;
        for (int k=0; k<m_dimCollocationPts; k++) {
          PhiTG[i][j] += m_basisDeim[k][i] * m_massDeim[k][j];
        }
      }
    }

    // delta_beta^n
    for (int i=0; i<m_dimRomBasis; i++) {
      deltaBeta[i] = 0.;

      // M^n * beta^n
      for (int j=0; j<m_dimRomBasis; j++) {
        deltaBeta[i] -= m_beta[j] * m_matrixM[j][i];
      }
      // (\Phi^T G F^)^n
      for (int k=0; k<m_dimCollocationPts; k++) {
        deltaBeta[i] += PhiTG[i][k] * m_hatF[k];
      }
    }
    // beta^{n+1} = beta^n - dt delta_beta^n
    for (int i=0; i<m_dimRomBasis; i++) {
      if (m_problem == 0) { // FKPP -> semi-implicit scheme
        // beta^{n+1} = beta^n - dt * M^n * beta^n + dt * (\Phi^T G F^)^n + dt (Cm - lambda) beta^n, F^ = - (chi + Cm) u^.2
        m_beta[i] = (1. - m_eigenValue[i]/2.*dt + Cm/2.*dt)/(1. + m_eigenValue[i]/2.*dt- Cm/2.*dt) * m_beta[i] + dt/(1. +m_eigenValue[i]/2.*dt - Cm/2.*dt) * deltaBeta[i];
      }
      else { // Explicit Euler
        m_beta[i] += dt * deltaBeta[i];
      }
    }
    
    if (m_resPoint) {
      for (int i=0; i<m_dimCollocationPts; i++) {
        for (int j=0; j<m_dimRomBasis; j++) {
          m_resU[i] -= dt*m_basisDeim[i][j]*deltaBeta[j];
        }
      }
    }

    if (m_problem == 2) {
      // If \sigma_E = \alpha \sigma_I
      // A \xi + B \beta = 0
      // A_ij = \sum_{h,l}^{N_p} (\alpha + 1)(\chi m_hatU[l] + lambda_i) \phi_j(x_h) G_hl \phi_i(x_l)
      // B_ij = \sum_{h,l}^{N_p} (\chi m_hatU[l] + lambda_i) \phi_j(x_h) G_hl \phi_i(x_h)

      double alpha = FelisceParam::instance().extraTransvTensor / FelisceParam::instance().intraTransvTensor;
      if (FelisceParam::instance().testCase == 1) {
        double beta = FelisceParam::instance().extraFiberTensor / FelisceParam::instance().intraFiberTensor;
        if ( !felisce::Tools::almostEqual(alpha,beta,1.e-3) ) {
          FEL_ERROR("Intra and extra-cellular conductivity must have the same anysotropy");
        }
      }

      for (int i=0; i<m_dimRomBasis; i++) {
        for (int j=0; j<m_dimRomBasis; j++) {
          m_matrixA[i][j] = .0;
          m_matrixB[i][j] = .0;
          for (int l=0; l<m_dimCollocationPts; l++) {
            if (FelisceParam::instance().optimizePotential) {
              m_matrixA[i][j] += (alpha+1)*(m_coefChi * m_potential[l] + m_eigenValue[i])*PhiTG[j][l]*m_basisDeim[l][i];
              m_matrixB[i][j] += (m_coefChi * m_potential[l] + m_eigenValue[i])*PhiTG[j][l]*m_basisDeim[l][i];
            }
            else {
              m_matrixA[i][j] += (alpha+1)*(m_coefChi * m_hatU[l] + m_eigenValue[i])*PhiTG[j][l]*m_basisDeim[l][i];
              m_matrixB[i][j] += (m_coefChi * m_hatU[l] + m_eigenValue[i])*PhiTG[j][l]*m_basisDeim[l][i];
            }
          }
        }
        m_matrixA[i][m_dimRomBasis] = 0.;
        for (int l=0; l<m_dimCollocationPts; l++) {
          m_matrixA[i][m_dimRomBasis] += m_sumG[l] * m_basisDeim[l][i];
        }
        m_matrixA[m_dimRomBasis][i] = m_matrixA[i][m_dimRomBasis];
      }
      m_matrixA[m_dimRomBasis][m_dimRomBasis] = 0.;
      double* newXi = new double[m_dimRomBasis+1];
      double* rhsXi = new double[m_dimRomBasis+1];
      for (int i=0; i<m_dimRomBasis; i++) {
        newXi[i] = m_xi[i];
        rhsXi[i] = 0.;
        for (int j=0; j<m_dimRomBasis; j++) {
          rhsXi[i] += - m_matrixB[i][j] * m_beta[j];
        }
      }
      newXi[m_dimRomBasis] = 0.;
      rhsXi[m_dimRomBasis] = 0.;

      ConjGrad(m_matrixA, rhsXi, newXi, 1.e-09, 1000, m_dimRomBasis+1);

      for (int i=0; i<m_dimRomBasis; i++) {
        m_xi[i] = newXi[i];
      }
      delete [] newXi;
      delete [] rhsXi;
    }
    
  }

  void EigenProblemALPDeim::ConjGrad(double** A, double* b, double* x, double tol, int maxIter, int n, bool sym) {

    if (sym) {
      double* res = new double[n];
      double* p = new double[n];
      double* Ax = new double[n];
      
      for (int i=0; i<n; i++) {
        Ax[i] = 0.;
        for (int j=0; j<n; j++) {
          Ax[i] += A[i][j]*x[j];
        }
      }
      for (int i=0; i<n; i++) {
        res[i] = b[i] - Ax[i];
        p[i] = res[i];
      }
      double res2 = 0.;
      double newRes2 = 0.;
      for (int i=0; i<n; i++) {
        res2 += res[i]*res[i];
      }
      
      double* Ap = new double[n];
      double pAp;
      double alpha;
      double beta;
      bool isConv = false;
      int iter = 1;
      
      if (res2 < tol) {
        isConv = true;
        if (FelisceParam::verbose()) {
          PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",0,res2);
        }
      }
      
      while (!isConv) {
        for (int i=0; i<n; i++) {
          Ap[i] = 0.;
          for (int j=0; j<n; j++) {
            Ap[i] += A[i][j]*p[j];
          }
        }
        pAp = 0.;
        for (int i=0; i<n; i++)
          pAp += p[i]*Ap[i];
        alpha = res2/pAp;
        
        for (int i=0; i<n; i++) {
          x[i] += alpha*p[i];
          res[i] -= alpha*Ap[i];
          newRes2 += res[i]*res[i];
        }
        
        if ( (newRes2 < tol) || (iter >= maxIter) ) {
          if (newRes2 < tol) {
            isConv = true;
            if (FelisceParam::verbose()) {
              PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",iter,newRes2);
            }
          } else {
            PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient: %i iterations, residual: %1.10f \n",iter,newRes2);
            FEL_WARNING("Conjugate Gradient did not converge.");
          }
          break;
        }
        else {
          beta = newRes2/res2;
          res2 = newRes2;
          newRes2 = 0.;
          for (int i=0; i<n; i++) {
            p[i] = res[i] + beta*p[i];
          }
          iter++;
        }
      }
      
      delete [] res;
      delete [] p;
      delete [] Ap;
      delete [] Ax;
      
    }
    else
    {
      double** AtA = new double*[n];
      for (int i=0; i<n; i++)
        AtA[i] = new double[n];
      
      double* Atb = new double[n];
      
      for (int i=0; i<n; i++) {
        Atb[i] = 0.;
        for (int j=0; j<n; j++) {
          Atb[i] += A[j][i]*b[j];
          AtA[i][j] = 0.;
          for (int k=0; k<n; k++) {
            AtA[i][j] += A[k][i]*A[k][j];
          }
        }
      }
      
      double* res = new double[n];
      double* p = new double[n];
      double* Ax = new double[n];
      
      for (int i=0; i<n; i++) {
        Ax[i] = 0.;
        for (int j=0; j<n; j++) {
          Ax[i] += AtA[i][j]*x[j];
        }
      }
      for (int i=0; i<n; i++) {
        res[i] = Atb[i] - Ax[i];
        p[i] = res[i];
      }
      double res2 = 0.;
      double newRes2 = 0.;
      for (int i=0; i<n; i++) {
        res2 += res[i]*res[i];
      }
      
      double* Ap = new double[n];
      double pAp;
      double alpha;
      double beta;
      bool isConv = false;
      int iter = 1;
      
      if (res2 < tol) {
        isConv = true;
        if (FelisceParam::verbose()) {
          PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",0,res2);
        }
      }
      
      while (!isConv) {
        for (int i=0; i<n; i++) {
          Ap[i] = 0.;
          for (int j=0; j<n; j++) {
            Ap[i] += AtA[i][j]*p[j];
          }
        }
        pAp = 0.;
        for (int i=0; i<n; i++)
          pAp += p[i]*Ap[i];
        alpha = res2/pAp;
        
        for (int i=0; i<n; i++) {
          x[i] += alpha*p[i];
          res[i] -= alpha*Ap[i];
          newRes2 += res[i]*res[i];
        }
        
        if ( (newRes2 < tol) || (iter >= maxIter) ) {
          if (newRes2 < tol) {
            isConv = true;
            if (FelisceParam::verbose()) {
              PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient converged in %i iterations, residual: %1.10f \n",iter,newRes2);
            }
          } else {
            PetscPrintf(PETSC_COMM_WORLD,"Conjugate Gradient: %i iterations, residual: %1.10f \n",iter,newRes2);
            FEL_ERROR("Conjugate Gradient did not converge.");
          }
          break;
        } else {
          beta = newRes2/res2;
          res2 = newRes2;
          newRes2 = 0.;
          for (int i=0; i<n; i++) {
            p[i] = res[i] + beta*p[i];
          }
          iter++;
        }
      }
      
      delete [] res;
      delete [] p;
      delete [] Ap;
      delete [] Ax;
      
      for (int i=0; i<n; i++)
        delete AtA[i];
      delete [] AtA;
      delete [] Atb;
    }
    
  }

  void EigenProblemALPDeim::updateEigenvalue() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::updateEigenvalue()\n");

    double dt = FelisceParam::instance().timeStep;
    // lambda_i^{n+1} = lambda_i^n - dt * m_coefChi * m_theta_ii
    for (int i=0; i<m_dimRomBasis; i++) {
      m_eigenValue[i] -= dt * m_coefChi * m_theta[i][i];
    }

  }

  void EigenProblemALPDeim::checkBasis(int size) {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::checkBasis()\n");
        
    if (size == 0)
      size = m_dimRomBasis;
    
    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);
    PetscVector tmpSol;
    tmpSol.duplicateFrom(m_U_0);
    
    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];
    
    // Check convergence on m_U_0
    double convergence[size];
    for (int iBasis=0; iBasis < size; iBasis++) {
      double tmpVec[iBasis+1];
      for (int jBasis=0; jBasis<iBasis+1; jBasis++) {
        tmpVec[jBasis] = m_beta[jBasis];
      }
      
      //EigenProblemALP::
      projectOnDof(tmpVec,tmpSol,iBasis+1);
      
      checkSolution(tmpSol,m_U_0,convergence[iBasis]);
      
      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      if (m_problem == 0) {
        writeEnsightVector(tmpSolToSave, iBasis, "uConv");
      }
      if ( (m_problem == 1) || (m_problem == 2) ) {
        writeEnsightVector(tmpSolToSave, iBasis, "potTransMembConv");
      }
      
      tmpSol.axpy(-1.,m_U_0);
      tmpSol.abs();
      double maxU0;
      m_U_0.max(&maxU0);
      double minU0;
      m_U_0.min(&minU0);
      double deltaU0 = maxU0-minU0;
      tmpSol.scale(1./deltaU0);
      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      if (m_problem == 0) {
        writeEnsightVector(tmpSolToSave, iBasis, "uError");
      }
      if ( (m_problem == 1) || (m_problem == 2) ) {
        writeEnsightVector(tmpSolToSave, iBasis, "potTransMembError");
      }
      
      PetscPrintf(PETSC_COMM_WORLD, "convergence[%d] = %e \n", iBasis, convergence[iBasis]);
      
    }
    
    
    std::string fileName = FelisceParam::instance().resultDir + "/convergence";
    viewALP(convergence,size,fileName);
        
    // Check convergence on m_Ue_0
    if (m_problem == 2) {
      double convergenceUe[size];
      for (int iBasis=0; iBasis < size; iBasis++) {
        double tmpVec[iBasis+1];
        for (int jBasis=0; jBasis<iBasis+1; jBasis++) {
          tmpVec[jBasis] = m_xi[jBasis];
        }
        
        //EigenProblemALP::
        projectOnDof(tmpVec,tmpSol,iBasis+1);
        
        checkSolution(tmpSol,m_Ue_0,convergenceUe[iBasis]);
        
        fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
        writeEnsightVector(tmpSolToSave, iBasis, "potExtraCellConv");
        
        tmpSol.axpy(-1.,m_Ue_0);
        tmpSol.abs();
        double maxU0;
        m_Ue_0.max(&maxU0);
        double minU0;
        m_Ue_0.min(&minU0);
        double deltaU0 = maxU0-minU0;
        tmpSol.scale(1./deltaU0);
        fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
        writeEnsightVector(tmpSolToSave, iBasis, "potExtraCellError");
        
        PetscPrintf(PETSC_COMM_WORLD, "convergenceUe[%d] = %e \n", iBasis, convergenceUe[iBasis]);
        
      }
      
      fileName = FelisceParam::instance().resultDir + "/convergenceUe";
      viewALP(convergenceUe,size,fileName);
    }
    
    std::vector<std::string> varNames;
    if (m_problem == 0) {
      varNames.resize(2);
      varNames[0] = "uConv";
      varNames[1] = "uError";
    }
    else if (m_problem == 1) {
      varNames.resize(2);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potTransMembError";
    }
    else if (m_problem == 2) {
      varNames.resize(4);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potExtraCellConv";
      varNames[2] = "potTransMembError";
      varNames[3] = "potExtraCellError";
    }
    if (rankProc == 0)
      writeEnsightCase(size, 1.,varNames);
    
    delete [] tmpSolToSave;
    tmpSol.destroy();
  }
  
  void EigenProblemALPDeim::checkBasisCollocation() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemALPDeim::checkBasisCollocation()\n");

    if (!m_solutionInitialized) {
      readCollocationPts();
      initializeROM();
      computeMassDeim();
      initializeSolution();
      computeProjectionPiV();
    }

    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);
    PetscVector tmpSol;
    tmpSol.duplicateFrom(m_U_0);

    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];

    // Check convergence on m_U_0
    double convergence[m_dimRomBasis];
    for (int iBasis=1; iBasis < m_dimRomBasis; iBasis++) {
      tmpSol.set(0.);
      double* tmpVec = new double[iBasis];
      for (int jBasis=0; jBasis<iBasis; jBasis++) {
        tmpVec[jBasis] = m_beta[jBasis];
      }

      projectOnDof(tmpVec,m_hatU,iBasis);

      for (int k=0; k<m_dimCollocationPts; k++) {
        tmpSol.axpy( m_hatU[k], m_projectorPiV[k]);
      }

      checkSolution(tmpSol,m_U_0,convergence[iBasis]);

      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      if (m_problem == 0) {
        writeEnsightVector(tmpSolToSave, iBasis, "uConv");
      }
      if ( (m_problem == 1) || (m_problem == 2) ) {
        writeEnsightVector(tmpSolToSave, iBasis, "potTransMembConv");
      }

      tmpSol.axpy(-1.,m_U_0);
      tmpSol.abs();
      double maxU0;
      m_U_0.max(&maxU0);
      double minU0;
      m_U_0.min(&minU0);
      double deltaU0 = maxU0-minU0;
      tmpSol.scale(1./deltaU0);
      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      if (m_problem == 0) {
        writeEnsightVector(tmpSolToSave, iBasis, "uError");
      }
      if ( (m_problem == 1) || (m_problem == 2) ) {
        writeEnsightVector(tmpSolToSave, iBasis, "potTransMembError");
      }
      
      PetscPrintf(PETSC_COMM_WORLD, "convergence[%d] = %e \n", iBasis, convergence[iBasis]);

      delete [] tmpVec;
    }

    
    std::string fileName = FelisceParam::instance().resultDir + "/convergence";
    viewALP(convergence,m_dimRomBasis,fileName);

    // Check convergence on m_W_0
//    if ( (m_problem == 1) || (m_problem == 2) ) {
//      double convergenceW[m_dimRomBasis];
//      for (int iBasis=1; iBasis < m_dimRomBasis; iBasis++) {
//        tmpSol.set(0.);
//        double* tmpVec = new double[iBasis];
//        for (int jBasis=0; jBasis<iBasis; jBasis++) {
//          tmpVec[jBasis] = m_mu[jBasis];
//        }
//
//        projectOnDof(tmpVec,m_hatW,iBasis);
//
//        for (int k=0; k<m_dimCollocationPts; k++) {
//          tmpSol.axpy( m_hatW[k], m_projectorPiV[k]);
//        }
//
//        checkSolution(tmpSol,m_W_0,convergenceW[iBasis]);
//
//        fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
//        writeEnsightVector(tmpSolToSave, iBasis, "ionicVarConv");
//
//        PetscPrintf(PETSC_COMM_WORLD, "convergenceW[%d] = %e \n", iBasis, convergenceW[iBasis]);
//
//        delete [] tmpVec;
//      }
//      if (rankProc == 0)
//        writeEnsightCase(m_dimRomBasis, 1., "ionicVarConv");
//      fileName = FelisceParam::instance().resultDir + "/convergenceW";
//      viewALP(convergenceW,m_dimRomBasis,fileName);
//    }

    // Check convergence on m_Ue_0
    if (m_problem == 2) {
      double convergenceUe[m_dimRomBasis];
      for (int iBasis=1; iBasis < m_dimRomBasis; iBasis++) {
        tmpSol.set(0.);
        double* tmpVec = new double[iBasis];
        for (int jBasis=0; jBasis<iBasis; jBasis++) {
          tmpVec[jBasis] = m_xi[jBasis];
        }

        projectOnDof(tmpVec,m_hatUe,iBasis);

        for (int k=0; k<m_dimCollocationPts; k++) {
          tmpSol.axpy( m_hatUe[k], m_projectorPiV[k]);
        }

        checkSolution(tmpSol,m_Ue_0,convergenceUe[iBasis]);

        fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
        writeEnsightVector(tmpSolToSave, iBasis, "potExtraCellConv");

        tmpSol.axpy(-1.,m_Ue_0);
        tmpSol.abs();
        double maxU0;
        m_Ue_0.max(&maxU0);
        double minU0;
        m_Ue_0.min(&minU0);
        double deltaU0 = maxU0-minU0;
        tmpSol.scale(1./deltaU0);
        fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
        writeEnsightVector(tmpSolToSave, iBasis, "potExtraCellError");
        
        PetscPrintf(PETSC_COMM_WORLD, "convergenceUe[%d] = %e \n", iBasis, convergenceUe[iBasis]);

        delete [] tmpVec;
      }
      
      fileName = FelisceParam::instance().resultDir + "/convergenceUe";
      viewALP(convergenceUe,m_dimRomBasis,fileName);
    }

    std::vector<std::string> varNames;
    if (m_problem == 0) {
      varNames.resize(2);
      varNames[0] = "uConv";
      varNames[1] = "uError";
    }
    else if (m_problem == 1) {
      varNames.resize(2);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potTransMembError";
    }
    else if (m_problem == 2) {
      varNames.resize(4);
      varNames[0] = "potTransMembConv";
      varNames[1] = "potExtraCellConv";
      varNames[2] = "potTransMembError";
      varNames[3] = "potExtraCellError";
    }
    if (rankProc == 0)
      writeEnsightCase(m_dimRomBasis, 1.,varNames);
    
    delete [] tmpSolToSave;
    tmpSol.destroy();
  }


}
