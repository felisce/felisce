//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemLinearElasticity.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce {
  LinearProblemLinearElasticity::LinearProblemLinearElasticity():
    LinearProblem("Linear Elasticity",/*numberOfMatrices*/2),m_coupled(false)  {


    std::vector<std::string> listPara; //list

    // The petsc vectors normalField and measStar are necessary for the computation
    // of the P1-normal field.
    listPara.emplace_back("normalField");
    listPara.emplace_back("measStar");
    // Old quantities
    listPara.emplace_back("displacementOld");
    if (FelisceParam::instance().timeScheme == 0 ) {
      listPara.emplace_back("displacementOlder");
    } else {
      listPara.emplace_back("accelerationCurrent");
      listPara.emplace_back("accelerationOld");
      listPara.emplace_back("velocityCurrent");
      listPara.emplace_back("velocityOld");
    }

    // Stress coming from external problem
    listPara.emplace_back("externalStress");

    std::vector<std::string>  listSeq(listPara);

    for( std::size_t k(0); k<listPara.size();++k) {
      m_vecs.Init(listPara[k]);
    }
    for( std::size_t k(0); k<listSeq.size();++k) {
      m_seqVecs.Init(listSeq[k]);
    }
    
    if ( FelisceParam::instance().withCVG ) {
      #ifdef FELISCE_WITH_CVGRAPH
      m_cvgDirichletVariable = "cvgraphDisplacement";
      #endif
      m_seqVecs.Init("cvgraphDisplacementOld");
      m_seqVecs.Init("cvgraphDisplacement");
      m_seqVecs.Init("externalVelocity");
      m_seqVecs.Init("cvgraphSTRESS");
      m_seqVecs.Init("currentVelocity");
    }
    m_interfaceLabels=FelisceParam::instance().fsiInterfaceLabel;
  }

  void LinearProblemLinearElasticity::initPetscVecsForCoupling() {
    m_seqVecs.Init("previousFixedPointVelocity");
    m_vecs.Init("velocityCurrent");
    m_seqVecs.Init("velocityCurrent");
    this->initFixedPointAcceleration();
  }

  void
  LinearProblemLinearElasticity::initializeDofBoundaryAndBD2VolMaps() {
    /// this function builds all the mappings 
    /// to deal with petsc objects defined at the interface
    m_dofBD[0/*iBD*/].initialize(this);
    std::vector<int> components(3);
    for ( std::size_t icomp(0); icomp<(std::size_t)this->dimension(); icomp++) {
      this->dofBD(/*iBD*/0).buildListOfBoundaryPetscDofs(this, m_interfaceLabels, /*iunknown*/ 0 , icomp);
      components[icomp]=icomp;
    }
    m_dofBD[0/*iBD*/].buildBoundaryVolumeMapping(/*iunknown*/0, components);
  }
  void LinearProblemLinearElasticity::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm,doUseSNES);

    m_fstransient = fstransient;
    // you can either specify the lame coefficients or young and poisson, by default I assume to use ( E, \nu)
    if ( FelisceParam::instance().young < 0 || FelisceParam::instance().poisson < 0 ) {
      m_mu = FelisceParam::instance().mu_lame;
      m_lambda = FelisceParam::instance().lambda_lame;
    } else {
      m_mu = FelisceParam::instance().young/2/(1+FelisceParam::instance().poisson);
      m_lambda = FelisceParam::instance().young*FelisceParam::instance().poisson/(1.+FelisceParam::instance().poisson)/(1.-2.*FelisceParam::instance().poisson);
    }
    m_deltaT = FelisceParam::instance().timeStep;
    m_density = FelisceParam::instance().densitySolid;

    m_betaNM  = FelisceParam::instance().gammaNM;
    m_gammaNM = FelisceParam::instance().betaNM;
    
    std::cout<<"E: " << FelisceParam::instance().young << std::endl;
    std::cout<<"nu: "<< FelisceParam::instance().poisson << std::endl;
    std::cout<<"mu: "<< m_mu << std::endl;
    std::cout<<"lambda: "<< m_lambda << std::endl;
    std::cout<<"deltaT: "<< m_deltaT << std::endl;
    std::cout<<"density: "<< m_density << std::endl;
    
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;

    listVariable.push_back(displacement);
    listNumComp.push_back(this->dimension());

    userAddOtherVariables(listVariable,listNumComp);
    
    //define unknowns of the linear system.
    m_listUnknown.push_back(displacement);
    this->definePhysicalVariable(listVariable,listNumComp);
    m_iDisplacement = this->listVariable().getVariableIdList(displacement);
  }

  void LinearProblemLinearElasticity::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    (void) eltType;
    (void) flagMatrixRHS;
    m_fe = m_listCurrentFiniteElement[m_iDisplacement];
    
    m_elemFields["displacementOld"].initialize(DOF_FIELD,*m_fe,this->dimension());
    if (FelisceParam::instance().timeScheme == 0) {
      m_elemFields["displacementOlder"].initialize(DOF_FIELD,*m_fe,this->dimension());
    } else {
      m_elemFields["velocityOld"].initialize(DOF_FIELD,*m_fe,this->dimension());
      m_elemFields["accelerationOld"].initialize(DOF_FIELD,*m_fe,this->dimension());
    }

    
  }
  void LinearProblemLinearElasticity::initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELT_TYPE;

    m_curvFe = m_listCurvilinearFiniteElement[m_iDisplacement];
#ifdef FELISCE_WITH_CVGRAPH
    if (FelisceParam::instance().withCVG ) {
      if ( slave()->thereIsAtLeastOneRobinCondition() ){
        m_robinAux.initialize( DOF_FIELD, *m_curvFe, this->dimension() );
      }
    }
#endif
  }
  
  void LinearProblemLinearElasticity::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS /*flagMatrixRHS*/) {
    
    (void)iel;

    m_curvFe->updateMeas(0,elemPoint);
#ifdef FELISCE_WITH_CVGRAPH
    if (FelisceParam::instance().withCVG ) {
      this->cvgraphNaturalBC(iel);
    }
#endif
  }
  
  void LinearProblemLinearElasticity::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    (void) elemIdPoint;
    (void) elemPoint;

    m_fe->updateFirstDeriv(0, elemPoint);
    
    if ( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix ) {
      if ( m_fstransient->iteration == 0 ) {
        //! Divergence of the stress-tensor
        m_elementMat[1]->eps_phi_i_eps_phi_j( 2*m_mu, *m_fe, 0, 0, this->dimension());
        m_elementMat[1]->div_phi_j_div_phi_i( m_lambda, *m_fe, 0, 0, this->dimension());

        //! Mass term for the time scheme
        if (FelisceParam::instance().timeScheme == 0 ) {
          m_elementMat[1]->phi_i_phi_j(m_density/(m_deltaT*m_deltaT),*m_fe,0,0,this->dimension());
        } else {
          m_elementMat[1]->phi_i_phi_j(m_density/(m_deltaT*m_deltaT*m_betaNM),*m_fe,0,0,this->dimension());
        }
      }
    }
    if ( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) {
      this->computeRHS(iel);
    }
  }
  
  void LinearProblemLinearElasticity::computeRHS(felInt iel) {
    if (FelisceParam::instance().timeScheme == 0 ) {
      //! We read the values we need
      m_elemFields.Get("displacementOld")   .setValue(m_seqVecs.Get("displacementOld")   ,*m_fe,iel,m_iDisplacement,m_ao,dof());
      m_elemFields.Get("displacementOlder") .setValue(m_seqVecs.Get("displacementOlder") ,*m_fe,iel,m_iDisplacement,m_ao,dof());
    
      //! We scale the values with the correct coefficients and we sum them into the first elementField    
      m_elemFields.Get("displacementOld")   .val *= 2./m_deltaT/m_deltaT;
      m_elemFields.Get("displacementOlder") .val *= -1./m_deltaT/m_deltaT;
      m_elemFields.Get("displacementOld")   .val += m_elemFields.Get("displacementOlder").val;

      //! Everything is considered as a source term
      m_elementVector[0]->source(m_density, *m_fe, m_elemFields.Get("displacementOld"), /*iblock*/0,/*numComp*/this->dimension());
      
    } else {
      //! We read the values we need
      m_elemFields.Get("displacementOld") .setValue(m_seqVecs.Get("displacementOld"), *m_fe,iel,m_iDisplacement,m_ao,dof());
      m_elemFields.Get("velocityOld")     .setValue(m_seqVecs.Get("velocityOld"), *m_fe,iel,m_iDisplacement,m_ao,dof());
      m_elemFields.Get("accelerationOld") .setValue(m_seqVecs.Get("accelerationOld"), *m_fe,iel,m_iDisplacement,m_ao,dof());
      
      //! We scale the values with the correct coefficients and we sum them into the first elementField    
      m_elemFields.Get("displacementOld").val += m_deltaT*m_elemFields.Get("velocityOld").val;
      m_elemFields.Get("displacementOld").val += (m_deltaT*m_deltaT*(1-2*m_betaNM)/2)*m_elemFields.Get("accelerationOld").val;
      
      //! Everything is considered as a source term
      m_elementVector[0]->source(m_density/(m_betaNM*m_deltaT*m_deltaT), *m_fe, m_elemFields.Get("displacementOld"), /*iblock*/0,/*numComp*/this->dimension());
    }
  }

  void LinearProblemLinearElasticity::updateCurrentVelocity() {
    if ( FelisceParam::instance().timeScheme == 1) {
      if ( m_coupled ) {
        m_seqVecs.Get("previousFixedPointVelocity").copyFrom(m_seqVecs.Get("velocityCurrent"));
      }
      // The current acceleration:
      m_vecs.Get("accelerationCurrent").copyFrom(m_vecs.Get("accelerationOld"));
      m_vecs.Get("accelerationCurrent").axpbypcz(/*a*/1, /*b*/m_deltaT,/*c*/m_deltaT*m_deltaT/2*(1-2*m_betaNM), /*x*/m_vecs.Get("displacementOld"), /*y*/m_vecs.Get("velocityOld"));
      m_vecs.Get("accelerationCurrent").axpby(/*a*/1./m_betaNM/m_deltaT/m_deltaT, /*b*/ -1./m_betaNM/m_deltaT/m_deltaT, /*x*/this->solution());
      this->gatherVector(m_vecs.Get("accelerationCurrent"),m_seqVecs.Get("accelerationCurrent"));
         
      m_vecs.Get("velocityCurrent").copyFrom(m_vecs.Get("velocityOld"));
      m_vecs.Get("velocityCurrent").axpbypcz(m_deltaT*(1-m_gammaNM), m_deltaT*m_gammaNM,/*c*/1, m_vecs.Get("accelerationOld"),m_vecs.Get("accelerationCurrent"));
      this->gatherVector(m_vecs.Get("velocityCurrent"),m_seqVecs.Get("velocityCurrent"));
    } else {
      // The current displacement is the solution...
      if ( m_coupled ) {
        m_seqVecs.Get("previousFixedPointVelocity").copyFrom(m_seqVecs.Get("velocityCurrent"));
        // The current velocity:
        m_vecs.Get("velocityCurrent").copyFrom(this->solution());// currentVelocity = currentDisplacement
        m_vecs.Get("velocityCurrent").axpby(-1./m_deltaT, 1./m_deltaT, m_vecs.Get("displacementOld")); // currentVelocity = (currentDisplacent - oldDisplacement)/delta T
        this->gatherVector(m_vecs.Get("velocityCurrent"),m_seqVecs.Get("velocityCurrent"));
      }
    }
      

    
  }
  
  void LinearProblemLinearElasticity::updateOldDisplacement() {
    if ( FelisceParam::instance().timeScheme == 0 ) {
      
      m_vecs.Get("displacementOlder").copyFrom(m_vecs.Get("displacementOld"));
      this->gatherVector(m_vecs.Get("displacementOlder"),m_seqVecs.Get("displacementOlder"));
      
    } else {
      
      m_vecs.Get("velocityOld").copyFrom(m_vecs.Get("velocityCurrent"));
      m_vecs.Get("accelerationOld").copyFrom(m_vecs.Get("accelerationCurrent"));

      this->gatherVector(m_vecs.Get("velocityOld"),m_seqVecs.Get("velocityOld"));
      this->gatherVector(m_vecs.Get("accelerationOld"),m_seqVecs.Get("accelerationOld"));
      
    }
    m_vecs.Get("displacementOld").copyFrom(this->solution());
    this->gatherVector(m_vecs.Get("displacementOld"),m_seqVecs.Get("displacementOld"));
#ifdef FELISCE_WITH_CVGRAPH
    if( FelisceParam::instance().withCVG ) { //&& m_readVariable == "VELOCITY" 
      this->gatherSolution();
      m_seqVecs.Get("cvgraphDisplacementOld").copyFrom(this->sequentialSolution());
    }
#endif
  }

  void
  LinearProblemLinearElasticity::readStressInterface(std::map<int, std::map<int,std::map<int,int> > > externalMap, const PetscVector& externalSeqStress, felInt iUC0) {
    if ( FelisceParam::verbose() > 0 ) {
      std::stringstream out;
      out<<"reading data at the interface...";
      PetscPrintf(MpiInfo::petscComm(), "%s",out.str().c_str());
    }
    felInt iDofGlobScalarPressure(0);
    felInt iDofGlobScalarIOP(0);
    felInt iDofGlobScalarPressureAus(0);
    double valueAus(0.0);
    std::vector<felInt> labels = FelisceParam::instance().fsiInterfaceLabel;
    for ( std::size_t iComp(0); iComp< (std::size_t) this->dimension(); ++iComp, ++iUC0) {
      for ( std::size_t iLab(0); iLab < m_interfaceLabels.size(); ++iLab) {
        //Contrary to the application PerfectFluid we assume the labels are the same, therefore we do not need a mapBetweenLabels
        for(auto it_suppDofIop = externalMap.at(iUC0)[m_interfaceLabels[iLab]].begin(); it_suppDofIop != externalMap.at(iUC0)[m_interfaceLabels[iLab]].end(); it_suppDofIop++) {
          iDofGlobScalarIOP = it_suppDofIop->second;
          iDofGlobScalarPressure = m_dofBD[0/*iBD*/].getPetscDofs( this->dof().getNumGlobComp(m_iDisplacement,iComp), m_interfaceLabels[iLab], it_suppDofIop->first );
          iDofGlobScalarPressureAus = iDofGlobScalarPressure;
          AOPetscToApplication(m_ao,/*counter*/1,&iDofGlobScalarPressureAus);
          if ( m_dofPart[iDofGlobScalarPressureAus] == MpiInfo::rankProc() ) {
            externalSeqStress.getValues(1,&iDofGlobScalarIOP, &valueAus);
            m_vecs.Get("externalStress").setValue(iDofGlobScalarPressure, valueAus, INSERT_VALUES);
          }
        }
      }
    }
    m_vecs.Get("externalStress").assembly();
    gatherVector(m_vecs.Get("externalStress"), m_seqVecs.Get("externalStress"));

    if ( FelisceParam::verbose() > 0 ) {
      std::stringstream out;
      out<<"done"<<std::endl;
      PetscPrintf(MpiInfo::petscComm(), "%s",out.str().c_str());
    }    
  }
  double LinearProblemLinearElasticity::computeL2ScalarProduct(std::string l, std::string r) {
    m_auxiliaryStrings.clear();
    m_auxiliaryStrings.push_back(l);
    m_auxiliaryStrings.push_back(r);

    m_auxiliaryDoubles.clear();
    m_auxiliaryDoubles.push_back(0.0);
    assemblyLoopBoundaryGeneral(&LinearProblemLinearElasticity::scalarProductComputer,
                                m_interfaceLabels,
                                &LinearProblemLinearElasticity::initPerET,
                                &LinearProblemLinearElasticity::updateFe);
    double result(0);
    MPI_Allreduce(&m_auxiliaryDoubles[0],&result,1,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
    return result;
  }
  void LinearProblemLinearElasticity::scalarProductComputer(felInt ielSupportDof) {
    double l,r;
    for ( std::size_t iComp(0); iComp < (std::size_t) this->dimension(); ++iComp ) {
      for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFe->numDof(); iSurfDof++) {
        felInt iDofGlobScalar;
        dof().loc2glob( ielSupportDof, iSurfDof, m_iDisplacement, iComp, iDofGlobScalar);
        AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
        m_seqVecs.Get( m_auxiliaryStrings[0] ).getValues(1,&iDofGlobScalar, &r);
        for ( std::size_t jSurfDof(0); jSurfDof < ( std::size_t ) m_curvFe->numDof(); jSurfDof++) {
          felInt jDofGlobScalar;
          dof().loc2glob( ielSupportDof, jSurfDof, m_iDisplacement, iComp, jDofGlobScalar);
          AOApplicationToPetsc(m_ao,1,&jDofGlobScalar);
          m_seqVecs.Get( m_auxiliaryStrings[1] ).getValues(1,&iDofGlobScalar, &l);      
          for(int ig=0; ig < m_curvFe->numQuadraturePoint(); ig++) {
            m_auxiliaryDoubles[0] += l*r * m_curvFe->phi[ig](iSurfDof) * m_curvFe->phi[ig](jSurfDof) * m_curvFe->weightMeas(ig);
          }
        }
      }
    }
  }

  std::pair<double,double>
  LinearProblemLinearElasticity::computeTestQuantities() {
    std::pair<double,double> pairOfTestQuantities;
    m_auxiliaryDoubles.clear();
    m_auxiliaryDoubles.resize(4,0.0);
    assemblyLoopBoundaryGeneral( &LinearProblemLinearElasticity::testQuantitiesComputer,
                                 m_interfaceLabels,
                                 &LinearProblemLinearElasticity::initPerET,
                                 &LinearProblemLinearElasticity::updateFe);
    std::vector<double> result(4,0.0);
    MPI_Allreduce(m_auxiliaryDoubles.data(),result.data(),4,MPI_DOUBLE,MPI_SUM,MpiInfo::petscComm());
    using std::sqrt;
    pairOfTestQuantities.first  = std::sqrt(result[0])/( std::sqrt(result[1]) + 1e-12);
    pairOfTestQuantities.second = std::sqrt(result[2])/( std::sqrt(result[3]) + 1e-12);
    return pairOfTestQuantities;
  }
  void LinearProblemLinearElasticity::initPerET() {
    m_curvFe   = m_listCurvilinearFiniteElement[m_iDisplacement];
  }
  void LinearProblemLinearElasticity::updateFe(const std::vector<Point*>& elemPoint,const std::vector<int>& /*elemIdPoint*/) {
    m_curvFe   ->updateMeasNormal(0, elemPoint);
  }

  void 
  LinearProblemLinearElasticity::testQuantitiesComputer(felInt ielSupportDof) {
    
    double uni,unoldi,ciopi,x1i;
    double value0i,value1i,value2i,value3i;

    double unj,unoldj,ciopj,x1j;
    double value0j,value1j,value2j,value3j;

    felInt iDofGlobScalar;

    for ( std::size_t iComp(0); iComp < ( std::size_t )this->dimension(); iComp++) {
      for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_curvFe->numDof(); iSurfDof++) {
      
        dof().loc2glob( ielSupportDof, iSurfDof, m_iDisplacement, iComp, iDofGlobScalar);
        AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
      
        m_seqVecs.Get( "velocityCurrent" ).getValues(1,&iDofGlobScalar, &uni);
        m_seqVecs.Get( "previousFixedPointVelocity" ).getValues(1,&iDofGlobScalar, &unoldi);
        m_seqVecs.Get( "externalStress" ).getValues(1,&iDofGlobScalar, &ciopi);
        m_seqVecs.Get( "x1" ).getValues(1,&iDofGlobScalar, &x1i);

        value0i = uni-unoldi;
        value1i = uni;
        value2i = ciopi-x1i;
        value3i = ciopi;

      
        for ( std::size_t jSurfDof(0); jSurfDof < ( std::size_t ) m_curvFe->numDof(); jSurfDof++) {
          felInt jDofGlobScalar;
          dof().loc2glob( ielSupportDof, jSurfDof, m_iDisplacement, iComp, jDofGlobScalar);
          AOApplicationToPetsc(m_ao,1,&jDofGlobScalar);
      
          m_seqVecs.Get( "velocityCurrent" ).getValues(1,&jDofGlobScalar, &unj);
          m_seqVecs.Get( "previousFixedPointVelocity" ).getValues(1,&jDofGlobScalar, &unoldj);
          m_seqVecs.Get( "externalStress" ).getValues(1,&jDofGlobScalar, &ciopj);
          m_seqVecs.Get( "x1" ).getValues(1,&jDofGlobScalar, &x1j);

          value0j = unj-unoldj;
          value1j = unj;
          value2j = ciopj-x1j;
          value3j = ciopj;
      
          for(int ig=0; ig < m_curvFe->numQuadraturePoint(); ig++) {
            double w = m_curvFe->phi[ig](iSurfDof) * m_curvFe->phi[ig](jSurfDof) * m_curvFe->weightMeas(ig);
            m_auxiliaryDoubles[0] += value0i*value0j * w;
            m_auxiliaryDoubles[1] += value1i*value1j * w;
            m_auxiliaryDoubles[2] += value2i*value2j * w;
            m_auxiliaryDoubles[3] += value3i*value3j * w;
          }
        }
      }
    }
  }

  void LinearProblemLinearElasticity::applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) {
    if( FelisceParam::instance().useEssDerivedBoundaryCondition ) {
      if ( FelisceParam::verbose() > 2 )
        std::cout<<"["<<rank<<"]--Applying zero-Dirichlet BCs on the velocity at the interface between inlet outlet and surface."<<std::endl;
      getRings();
      double bigValue=1.e20;
      
      for(auto itDofBC = m_idDofRings.begin(); itDofBC != m_idDofRings.end(); ++itDofBC) { 
        if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix))
          matrix(0).setValue(*itDofBC, *itDofBC, bigValue, INSERT_VALUES);
        if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs)) 
          vector().setValue(*itDofBC,0.0, INSERT_VALUES);
      }
    }
  }
  
#ifdef FELISCE_WITH_CVGRAPH
  void LinearProblemLinearElasticity::readData() {
    bool dispUpdateNeeded(false);
    bool dispDirectly(false);
    for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for ( std::size_t cVar(0); cVar<this->slave()->numVarToRead(iConn); ++cVar) {
        std::string varToRead = this->slave()->readVariable(iConn,cVar);
        if ( varToRead  == "STRESS" ) {
          vecs.push_back(m_seqVecs.Get("cvgraphSTRESS"));
        } else if  ( varToRead  == "VELOCITY" ) {
          dispUpdateNeeded = true;
          vecs.push_back(m_seqVecs.Get("externalVelocity"));
        } else if ( varToRead == "DISPLACEMENT" ) {
          dispDirectly = true;
          vecs.push_back(m_seqVecs.Get("cvgraphDisplacement"));
        } else {
          std::cout<<"Connection: "<<iConn<<"--variable to read: "<<varToRead<<std::endl;
          FEL_ERROR("Error in variable to read");
        }
      }
      this->slave()->receiveData(vecs,iConn);
    }
    if ( dispUpdateNeeded && dispDirectly ) {
      FEL_ERROR("TODO: implement a better readData function");
    }
    if ( dispUpdateNeeded ) {
      waxpy(m_seqVecs.Get("cvgraphDisplacement"), m_deltaT, m_seqVecs.Get("externalVelocity"),m_seqVecs.Get("cvgraphDisplacementOld"));
    }
  }
  
  void LinearProblemLinearElasticity::sendData() {
    this->gatherSolution();

    m_seqVecs.Get("currentVelocity").copyFrom(this->sequentialSolution());// currentVelocity = currentDisplacement
    m_seqVecs.Get("currentVelocity").axpby(-1./m_deltaT, 1./m_deltaT, m_seqVecs.Get("displacementOld")); // currentVelocity = (currentDisplacent - oldDisplacement)/delta T

    for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for ( std::size_t cVar(0); cVar<this->slave()->numVarToSend(iConn); ++cVar) {
        std::string varToSend = this->slave()->sendVariable(iConn, cVar);
        if (  varToSend == "STRESS" ) {
          this->prepareResidual(iConn);
          vecs.push_back(m_seqStressOut);
        } else if  ( varToSend == "DISPLACEMENT" ) {
          vecs.push_back(this->sequentialSolution());
        } else if  ( varToSend == "VELOCITY" ) {
          vecs.push_back(m_seqVecs.Get("currentVelocity"));
        } else {
          std::cout<<"Connection: "<<iConn<<"--variable to send: "<<varToSend<<std::endl;
          FEL_ERROR("Error in variable to send");
        }
      }
      this->slave()->sendData(vecs,iConn);
    }
  }
  void LinearProblemLinearElasticity::assembleMassBoundaryAndInitKSP( std::size_t iConn ) {
    LinearProblem::assembleMassBoundaryAndInitKSP(&LinearProblemLinearElasticity::massMatrixComputer,
                                         this->slave()->interfaceLabels(iConn),
                                         &LinearProblemLinearElasticity::initPerETMass,
                                         &LinearProblemLinearElasticity::updateFE,
                                         iConn);
  }

  /*! \brief Function to assemble the mass matrix
   *  Function to be called in the assemblyLoopBoundaryGeneral
   */
  void LinearProblemLinearElasticity::massMatrixComputer(felInt ielSupportDof) {
    this->m_elementMatBD[0]->zero();
    this->m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFe,/*iblock*/0,/*iblock*/0,this->dimension());
    this->setValueMatrixBD(ielSupportDof);
  }
  
  void
  LinearProblemLinearElasticity::initPerETMass() {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
    for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    
    m_curvFe = this->m_listCurvilinearFiniteElement[ m_iDisplacement ];
  }

  void
  LinearProblemLinearElasticity::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
    m_curvFe->updateMeasNormal(0, elemPoint);  
  }
#endif
  
  void LinearProblemLinearElasticity::computeBoundaryStress(const std::vector<int>& labels) {
    if ( m_computedStress.isNull() )
      m_computedStress.duplicateFrom(this->solution());
    if ( m_seqComputedStress.isNull() )
      m_seqComputedStress.duplicateFrom(this->sequentialSolution());
    m_computedStress.zeroEntries();
    this->gatherSolution(); 
    m_grad.resize(this->dimension(),this->dimension());
    m_stress.resize(this->dimension());
    m_previousVolEltType=GeometricMeshRegion::Nod;
    if(m_mesh[m_currentMesh]->statusFaces() == false) {
     m_mesh[m_currentMesh]->buildFaces();
    }
    assemblyLoopBoundaryGeneral(&LinearProblemLinearElasticity::boundaryStressComputer,
                                labels,
                                &LinearProblemLinearElasticity::initPerETBoundaryStress,
                                &LinearProblemLinearElasticity::updateFEBoundaryStress);
    this->desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_rhs);
    m_computedStress.assembly();
    this->gatherVector(m_computedStress,m_seqComputedStress);
  }

  /*! \brief Function to compute boundary stress
   *  Function to be called in the assemblyLoopBoundaryGeneral
   */
  void LinearProblemLinearElasticity::boundaryStressComputer(felInt ielSupportDof) {
    std::vector<felInt> bd2vol;
    m_fe->identifyLocBDDof(*m_curvFe,bd2vol); 
    m_elemFieldStress.val*=0.0;
    for (int hbdDof(0); hbdDof<m_curvFe->numDof(); hbdDof++) {
      m_grad    = prod(m_fe->dPhiOnDofs[bd2vol.at(hbdDof)],trans(m_elemFieldDisplacementVolumeDofs.val));
      m_stress  = m_mu*( prod(m_grad,m_curvFe->normal[0]) + prod(trans(m_grad),m_curvFe->normal[0]) ) + m_lambda*Tools::trace(m_grad)*m_curvFe->normal[0];
      for (int jcomp(0); jcomp<this->dimension(); jcomp++) {
        m_elemFieldStress.val(jcomp,hbdDof) = m_stress(jcomp); 
      }
    }
    m_elementVectorBD[0]->zero();
    m_elementVectorBD[0]->source(-1.0, *m_curvFe, m_elemFieldStress, 0, this->dimension());
    this->setValueCustomVectorBD(ielSupportDof, ADD_VALUES, m_computedStress );
  }
  void
  LinearProblemLinearElasticity::initPerETBoundaryStress() {
    this->desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_rhs);
    this->allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_rhs);
    m_curvFe   = m_listCurvilinearFiniteElement[m_iDisplacement];
    m_elemFieldStress.initialize(DOF_FIELD, *m_curvFe,this->dimension());
  }
  void
  LinearProblemLinearElasticity::updateFEBoundaryStress(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint) {
    std::vector<Point*> elemPointVol; 
    std::vector<felInt> elemIdPointVol;
    int idLocFace = -1;
    felInt idElemVol = -1;
    m_mesh[m_currentMesh]->getElementFromBdElem(elemIdPoint,elemIdPointVol,elemPointVol,idLocFace,idElemVol);
    int dummy;
    m_mesh[m_currentMesh]->getTypeElemFromIdElem(idElemVol, m_volEltType, dummy);
    if (m_volEltType != m_previousVolEltType ) {
      this->defineFiniteElement(m_volEltType);
      this->initElementArray();
      m_previousVolEltType=m_volEltType;
      m_fe = m_listCurrentFiniteElement[m_iDisplacement];
      m_elemFieldDisplacementVolumeDofs.initialize(DOF_FIELD, *m_fe,this->dimension());
      m_fe->allocateOnDofsDataStructures();
    }
    m_fe->updateFirstDerivOnDofs(0, elemPointVol);
    m_curvFe->updateMeasNormal(0,elemPoint);
    m_elemFieldDisplacementVolumeDofs.setValue(this->sequentialSolution(),*m_fe,idElemVol,m_iDisplacement,m_ao,dof());
  }

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::computeNormalField(const std::vector<int> & labels, felInt idVecVariable) 
{
  m_auxiliaryInts.resize(1);
  m_auxiliaryInts[0]=idVecVariable;//for instance m_iVelocity;
  /*!
    For each dof d of the surface the sum: \f$\sum_{K} m(K)\vec n(K)\f$ is computed.
    - K are all the boundary elements that share the dof d.
    - m(K) is the measure of the element
    - n(K) is the P0 normal defined on the element
    */
  this->assemblyLoopBoundaryGeneral(&LinearProblemLinearElasticity::normalFieldComputer,labels,&LinearProblemLinearElasticity::initPerETNormVel,&LinearProblemLinearElasticity::updateFeNormVel);

  m_vecs.Get("normalField").assembly();
  m_vecs.Get("measStar").assembly();

  /// Than the vector is divided by \f$\sum_{K} m(K)\f$ to obtain the P1 field.
  pointwiseDivide(m_vecs.Get("normalField"), m_vecs.Get("normalField"), m_vecs.Get("measStar"));
  gatherVector( m_vecs.Get("normalField"), m_seqVecs.Get("normalField") );

  /// using normalizeComputer we normalize the result.
  this->assemblyLoopBoundaryGeneral(&LinearProblemLinearElasticity::normalizeComputer,labels,&LinearProblemLinearElasticity::initPerETNormVel,&LinearProblemLinearElasticity::updateFeNormVel);

  m_vecs.Get("normalField").assembly();
  gatherVector( m_vecs.Get("normalField"), m_seqVecs.Get("normalField") );
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::normalFieldComputer(felInt ielSupportDof) 
{
  double value;
  for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numDof(); iSurfDof++) {
    double feMeas(m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->measure());
    for ( std::size_t iCoor(0); iCoor <  ( std::size_t ) m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numCoor(); iCoor++) {
      felInt iDofGlob;
      dof().loc2glob( ielSupportDof, iSurfDof, m_auxiliaryInts[0], iCoor, iDofGlob);
      AOApplicationToPetsc(m_ao,1,&iDofGlob);
      value=0.0;
      for ( std::size_t iQuadBd(0.0) ; iQuadBd <  ( std::size_t )m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numQuadraturePoint(); iQuadBd++) {
        value += m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->weightMeas( iQuadBd ) * m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->normal[iQuadBd](iCoor);
      }
      m_vecs.Get("normalField").setValue(iDofGlob, value, ADD_VALUES);
      m_vecs.Get("measStar").setValue(iDofGlob, feMeas, ADD_VALUES);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::updateFeNormVel(const std::vector<Point*>& elemPoint,const std::vector<int>& /*elemIdPoint*/)
{
  m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->updateMeasNormal(0,elemPoint);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::normalizeComputer(felInt ielSupportDof)
{
  const int numComp = m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numCoor( );
  const int numDof  = m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numDof( );

  std::vector< double > norm(numDof,0.0);
  std::vector< std::vector< double > > NN(numDof, std::vector< double >(numComp, 0.0));
  for ( int idof = 0; idof < numDof; idof++ ) {
    for ( int icomp = 0; icomp < numComp; icomp++ ) {
      int iDofGlob;
      double value;
      dof().loc2glob( ielSupportDof, idof, m_auxiliaryInts[0], icomp, iDofGlob);
      AOApplicationToPetsc(m_ao, 1, &iDofGlob);
      m_seqVecs.Get("normalField").getValues(1, &iDofGlob, &value);
      norm[idof] += value*value;
      NN[idof][icomp] = value;
    }
    norm[idof] = std::sqrt( norm[idof] );
    for ( int icomp = 0; icomp < numComp; icomp++ )
      NN[idof][icomp] = NN[idof][icomp]/norm[idof];
  }
  for ( int idof = 0; idof < numDof; idof++ ) {
    for ( int icomp = 0; icomp < numComp; icomp++ ) {
      int iDofGlob;
      dof().loc2glob( ielSupportDof, idof, m_auxiliaryInts[0], icomp, iDofGlob);
      AOApplicationToPetsc(m_ao, 1, &iDofGlob);
      m_vecs.Get("normalField").setValue(iDofGlob, NN[idof][icomp], INSERT_VALUES);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::initFixedPointAcceleration()
{
  m_accMethod = accelerationType(FelisceParam::instance(this->instanceIndex()).accelerationMethod);
  m_omegaAcceleration = FelisceParam::instance(this->instanceIndex()).omegaAcceleration;
  m_seqVecs.Init("x1");
  switch ( m_accMethod ) {
    case FixedPoint:
    case Relaxation:
      break;
    case Aitken:
    case IronsTuck:
      m_seqVecs.Init("x0");
      m_seqVecs.Init("fx0");
      m_seqVecs.Init("fx1");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::accelerationPreStep(PetscVector& seqCurrentInput)
{
  // pre and post with respect to reading the data at the interface

  // method explanation in accelerationPostStep
  switch(m_accMethod) {
    case FixedPoint:
    case Relaxation:
      break;
    case Aitken:
    case IronsTuck:
      // in these two cases we have to move forward x0 and x1
      m_seqVecs.Get("x0" ).copyFrom(m_seqVecs.Get("x1" ));
      m_seqVecs.Get("fx0").copyFrom(m_seqVecs.Get("fx1"));
      break;
  }
  // in any case we need to the save the current value of iop which was the last point
  // where we have have evaluated the functional F.
  m_seqVecs.Get("x1").copyFrom(seqCurrentInput);
  // The following evaluation of the system will
  // be saved in fx1 in the function acceleration
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::accelerationPostStep( PetscVector& seqCurrentInput, int nbOfCurrentIteration ) 
{
  // We are solving a fixed point problem.

  // here an example for IOPcouplModel:
  // the input to the coupled system is currentiop
  // then you solve NS you compute the displacement
  // than you solve PF given the displacement and the current iop
  // the solution of PF at the boarder is the outcome of function.
  // P_interface = F ( currentIop )
  // an evaluation of F implies the solution of a NS pb + a PF pb.
  // P_interface will be modified by this function and reused for a new iteration.

  // Four (4) different methods:
  // 0. fixed point iterations
  //    default method: we simly iterate F over the same value.
  //    new x1=fx1;
  // 1. relaxation:
  //    new x1= w * fx1 + (1 - w) x1
  //    w=1 ==> back to fixed point iterations
  // 2. Aitken acceleration
  //    new x1= w * fx1 + (1 - w) x1, but w is a funcion of ( x0, fx0, x1, fx1 )
  // 3. Irons-Tuck: the same as Aitken, should be more "stable"
  switch(m_accMethod) {
    case FixedPoint:
    case Relaxation:
      // there is no need to save explicitly fx1 as done in aitken and irons tuck
      // since it is stored in currentIop and it will be overridden with the correct value.
      break;
    case Aitken:
    {
      // at this point we just read the results from the PF problem
      // this has been stored in the petscvec currentIop, since in a
      // fixed point approach it is already the new input (new x1 = currentIop = fx1).
      // in all the other approaches this is just the lates outcome
      // of the F.
      m_seqVecs.Get("fx1").copyFrom( seqCurrentInput );

      if ( nbOfCurrentIteration == 1 /*first iteration: w=1*/ ) {
        m_omegaAcceleration = 1;
      } else {
        // we compute the value for the omega parameter
        m_seqVecs.Init("dx"); // map element initialization
        m_seqVecs.Get("dx").duplicateFrom(m_seqVecs.Get("x1")); // structure of dx copied from x1
        m_seqVecs.Get("dx").copyFrom(m_seqVecs.Get("x1")); // dx = x1
        m_seqVecs.Get("dx").axpy(-1.,m_seqVecs.Get("x0")); // dx = x1 - x0

        m_seqVecs.Init("tmp"); // map element initialization
        m_seqVecs.Get("tmp").duplicateFrom(m_seqVecs.Get("fx0")); // structure of tmp copied from fx0
        m_seqVecs.Get("tmp").copyFrom(m_seqVecs.Get("fx0"));   // tmp = fx0
        m_seqVecs.Get("tmp").axpy(-1.,m_seqVecs.Get("fx1"));   // tmp = fx0 - fx1
        m_seqVecs.Get("tmp").axpy(1.,m_seqVecs.Get("dx"));     // tmp = fx0 - fx1 + x1 - x0

        const double omega  = computeL2ScalarProduct("dx" ,"tmp");
        const double xxnorm = computeL2ScalarProduct("tmp","tmp");
        if ( Tools::equal( xxnorm, 0.0 ) )
          m_omegaAcceleration = 1;
        else
          m_omegaAcceleration = omega / xxnorm;

        // we destroy those auxiliary vectors
        m_seqVecs.Get("tmp").destroy();
        m_seqVecs.erase("tmp");
        m_seqVecs.Get("dx").destroy();
        m_seqVecs.erase("dx");

      }
    }
      break;
    case IronsTuck:
    {
      m_seqVecs.Get("fx1").copyFrom(seqCurrentInput);

      if ( nbOfCurrentIteration == 1 /*first iteration: w=1*/ ) {
        m_omegaAcceleration = 1;
      } else {

        // we compute the value for the omega parameter
        m_seqVecs.Init("d1"); // map element initialization
        m_seqVecs.Get("d1").duplicateFrom(m_seqVecs.Get("x1")); // structure of d1 copied from x1
        m_seqVecs.Get("d1").copyFrom(m_seqVecs.Get("x1"));  // d1 = x1
        m_seqVecs.Get("d1").axpy(-1.,m_seqVecs.Get("fx1")); // d1 = x1 - fx1

        m_seqVecs.Init("tmp"); // map element initialization
        m_seqVecs.Get("tmp").duplicateFrom(m_seqVecs.Get("x0")); // structure of tmp copied from x0
        m_seqVecs.Get("tmp").copyFrom(m_seqVecs.Get("x0"));  // tmp = x0
        m_seqVecs.Get("tmp").axpy(-1.,m_seqVecs.Get("fx0")); // tmp = x0 - fx0
        m_seqVecs.Get("tmp").axpy(-1.,m_seqVecs.Get("d1"));  // tmp = x0 - fx0 - (x1 - fx1 )

        const double xxnorm ( computeL2ScalarProduct("tmp","tmp") );
        const double omega ( computeL2ScalarProduct("d1","tmp") );
        if ( Tools::equal( xxnorm, 0.0 ) ) {
          m_omegaAcceleration = 1;
        } else {
          m_omegaAcceleration = ( 1 + omega / xxnorm ) * m_omegaAcceleration;
        }

        // we destroy those auxiliary vectors
        m_seqVecs.Get("tmp").destroy();
        m_seqVecs.Get("d1").destroy();
        m_seqVecs.erase("d1");
        m_seqVecs.erase("tmp");
      }
    }
      break;
  }

  if ( FelisceParam::verbose() > 1 ) {
    std::stringstream msg;
    msg<<"----> using omega = "<<m_omegaAcceleration<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
  }

  //for the fixed point this step is not needed
  if ( m_accMethod != FixedPoint ) {
    std::stringstream msg;
    msg<<" updating fixed point input "<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    // update of currentIop, i.e. the new x1, the input for the next evaluation of F
    seqCurrentInput.axpby( 1. - m_omegaAcceleration, m_omegaAcceleration, m_seqVecs.Get("x1") );
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemLinearElasticity::sumOnBoundary(PetscVector& v, PetscVector& b, const DofBoundary& dofBD) 
{
  felInt numLocalDofInterface = dofBD.numLocalDofInterface();
  std::vector<double> tmp(numLocalDofInterface);
  b.getValues(numLocalDofInterface, dofBD.loc2PetscVolPtr(), tmp.data() );
  v.setValues(numLocalDofInterface, dofBD.loc2PetscVolPtr(), tmp.data(),ADD_VALUES);
  v.assembly();
}

/***********************************************************************************/
/***********************************************************************************/

#ifdef FELISCE_WITH_CVGRAPH           
  void
  LinearProblemLinearElasticity::cvgraphNaturalBC(felInt iel)  {
    BoundaryCondition* BC;
    // Loop over the cvgraph connections
    for ( std::size_t iConn(0); iConn<this->slave()->numConnections(); ++iConn ) {
      // Extract the labels of the current connection
      std::vector<int> labels = this->slave()->interfaceLabels(iConn);
      // Verify if the current label is part of the interface of this connection
      if ( std::find( labels.begin(), labels.end(), m_currentLabel ) != labels.end() ) {
        switch ( this->slave()->numVarToRead(iConn) ) {          
        case 1:  // only one var to read: neumann interface BC
          // Verify that we have to apply the stress in its strong form.
          if ( this->slave()->readVariable(iConn,/*cVar*/0) == "STRESS" && this->slave()->residualInterpolationType(iConn) == 0 ) {
            // Look through the different neumann conditions to find the correct one.
            for (std::size_t iNeumann=0; iNeumann<m_boundaryConditionList.numNeumannBoundaryCondition(); iNeumann++ ) {
              BC = m_boundaryConditionList.Neumann(iNeumann);//get the the BC
              if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),m_currentLabel) != BC->listLabel().end() ) {
                //compute the force term.
                m_elemFieldNeumann[iNeumann].setValue( m_seqVecs.Get("cvgraphSTRESS"), *m_curvFe, iel, m_iDisplacement, m_ao, dof());
              }
            }
          }
          break;
        case 2: //two variables to read: robin interface BC
          // Look through the different robin conditions to find the correct one.
          for (std::size_t iRobin=0; iRobin<m_boundaryConditionList.numRobinBoundaryCondition(); iRobin++ ) {
            BC = m_boundaryConditionList.Robin(iRobin);
            if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),m_currentLabel ) != BC->listLabel().end() ) {
              if ( this->slave()->residualInterpolationType(iConn) == 0 ) {
                m_elemFieldRobin[iRobin].setValue( m_seqVecs.Get("cvgraphSTRESS"), *m_curvFe  , iel, m_iDisplacement, m_ao, dof());
              } else {
                m_elemFieldRobin[iRobin].val *=0 ;
              }
              m_robinAux.setValue(m_seqVecs.Get("cvgraphDisplacement"),*m_curvFe, iel, m_iDisplacement, m_ao, dof());
              m_elemFieldRobin[iRobin].val += FelisceParam::instance().alphaRobin[iRobin]*m_robinAux.val;
            }
          }
          break;
        default:
          FEL_ERROR("Three variables to read not yet implemented for this linear problem.")
            break;
        } 
      }           
    }
  }
#endif
}
