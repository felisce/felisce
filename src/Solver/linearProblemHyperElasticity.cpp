//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHyperElasticity.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce
{

/**
  * \brief This function is given to SNES to compute the function.
  *
  * However as a matter of fact the jacobian will also be computed here, as it is the way Felisce works
  * (it hence avoids duplication or storage of intermediate matrixes)
  *
  * \param[in] snes SNES object. Not explicitly used but required by Petsc prototype.
  * \param[in] evaluationState State at which to evaluate residual.
  * \param[in] residual Vector to put residual. What is used as residual in LinearProblem is #_RHS.
  * \param[in] context_as_void Optional user-defined function context. In our case a #SnesContext object,
  * of which LinearProblem object is an attribute.
  */
PetscErrorCode snesFunctionImpl(SNES snes, Vec evaluationState, Vec residual, void* context_as_void) {
  static_cast<void>(snes);
  //      static_cast<void>(residual); // residual as input argument should in fact be the same as linear_problem.RHS()

  FEL_ASSERT(context_as_void);

  SnesContext* context_ptr = static_cast<SnesContext*>(context_as_void);
  SnesContext& context = *context_ptr;
  LinearProblem* linear_problem = context.linearProblem;

  {
    // DEV TMP: not generic enough! (displacement is really related to my own peculiar problem...)
    PetscInt NdofLocalPerUnknown = linear_problem->numDofLocalPerUnknown(/*PhysicalVariable::*/displacement);

    PetscVector& lp_evaluation_state = linear_problem->evaluationState();
    PetscVector& lp_rhs = linear_problem->vector();

    std::vector<PetscInt> local_indexing(NdofLocalPerUnknown); // std::vector that contains integers from 0 to local size - 1
    {
      // std::iota(local_indexing.begin(), local_indexing.end(), 0); // std::iota only works with C++11!

      for (int i = 0; i < NdofLocalPerUnknown; ++i) // substitute that also works in C++ 98 (arguably more easy to understand anyway)
        local_indexing[i] = i;
    }

    std::vector<PetscInt> global_indexing(NdofLocalPerUnknown);

    ISLocalToGlobalMappingApply(linear_problem->mappingDofLocalToDofGlobal(/*PhysicalVariable::*/displacement),
                                NdofLocalPerUnknown,
                                local_indexing.data(),
                                global_indexing.data());

    {
      PetscScalar * evaluation_state_array;
      VecGetArrayRead(evaluationState,(const PetscScalar**)&evaluation_state_array);

      PetscScalar * residual_array;
      VecGetArray(residual,&residual_array);

      FEL_ASSERT_EQUAL(global_indexing.size(), NdofLocalPerUnknown);

      lp_evaluation_state.setValues(global_indexing.size(),global_indexing.data(),evaluation_state_array,INSERT_VALUES);
      lp_evaluation_state.assembly();

      lp_rhs.setValues(global_indexing.size(),global_indexing.data(),residual_array,INSERT_VALUES);
      lp_rhs.assembly();

      VecRestoreArray(residual,&residual_array);
      VecRestoreArrayRead(evaluationState,(const PetscScalar**)&evaluation_state_array);

    }
  }

  linear_problem->preSNESAssemble();
  linear_problem->assembleMatrixRHS(context.rankProc, context.flag_matrix_rhs);
  linear_problem->computeSystemAfterAssembling();

  //Apply boundary conditions.
  linear_problem->finalizeEssBCTransient();
  {
    enum { idBCforLinCombMethod = 0 };
    bool doPrintBC = false;
    linear_problem->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod,
                            context.rankProc,
                            FlagMatrixRHS::matrix_and_rhs,
                            FlagMatrixRHS::matrix_and_rhs,
                            idBCforLinCombMethod, doPrintBC,
                            context.do_apply_natural);
  }

  VecCopy(linear_problem->vector().toPetsc(),residual);
  //VecView(residual,PETSC_VIEWER_STDOUT_WORLD);
  PetscFunctionReturn(0);
}

/***********************************************************************************/
/***********************************************************************************/

/**
  * \brief This function is supposed to be given to SNES to compute the jacobian.
  *
  * However the jacobian has already been computed within snesFunctionImpl, so this function is mostly a dummy one...
  */
PetscErrorCode snesJacobianImpl(SNES snes, Vec evaluationState, Mat jacobian , Mat preconditioner, void* context_as_void) {
  static_cast<void>(snes);
  static_cast<void>(jacobian);
  static_cast<void>(preconditioner);
  static_cast<void>(evaluationState);
  FEL_ASSERT(context_as_void);

  PetscFunctionReturn(0);
}

/***********************************************************************************/
/***********************************************************************************/

LinearProblemHyperElasticity::LinearProblemHyperElasticity(SpatialForce force)
: LinearProblem("HyperElasticity equation"),
  m_force(force),
  system_(HyperelasticityNS::static_),
  is_first_dynamic_iteration_done_(false)
{
}

/***********************************************************************************/
/***********************************************************************************/

LinearProblemHyperElasticity::~LinearProblemHyperElasticity()
= default;

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES)
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  using namespace HyperelasticityNS;

  LinearProblem::initialize(mesh, comm, doUseSNES);
  m_fstransient  = fstransient;

  m_typeOfForceApplied = Private::HyperElasticity::AssignForceType(FelisceParam::instance().volumicOrSurfacicForce);

  r_instance.hyperElasticLaw.hyperElasticLaws->InitParameters();

  FEL_ASSERT(FelisceParam::instance().volumic_mass > 0.);

  std::vector<PhysicalVariable> listVariable;
  std::vector<std::size_t> listNumComp;

  listVariable.push_back(displacement);
  listNumComp.push_back(this->dimension());

  m_listUnknown.push_back(displacement);

  if (r_instance.hyperElasticLaw.pressureData->IsCompressible()) {
    listVariable.push_back(pressure);
    listNumComp.push_back(1);
    m_listUnknown.push_back(pressure);
  }

  definePhysicalVariable(listVariable,listNumComp);
  setNumberOfMatrix(MatrixNS::Nmatrices);

  switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
    case none:
    case midpoint:
      break;
    case half_sum:
      setNumberOfVector(VectorNS::Nvector);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::InitializeDerivedAttributes()
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  using namespace HyperelasticityNS;

  // Check here there are no Neumann or NeumannNormal boundary conditions in the case of the volumic force
  // (it is not stricto sensu a limitation, so it could be lifted later, but at the moment it is a good way
  // to make sure there is no mishap in the data parameter file, in which we could apply both volumic and
  // surfacic force without being aware of it.)
  if (m_typeOfForceApplied == Private::HyperElasticity::volumic) {
    if (m_boundaryConditionList.numNeumannBoundaryCondition() || m_boundaryConditionList.numNeumannNormalBoundaryCondition()) {
      FEL_ERROR("Neumann condition found along with a volumic force; it is not allowed at the moment. "
                "Check there is no mishap in your data file.");
    }
  }

  current_displacement_.duplicateFrom(this->solution());
  current_velocity_.duplicateFrom(this->solution());
  seq_current_displacement_.duplicateFrom(this->sequentialSolution());

  current_velocity_.zeroEntries();
  current_displacement_.zeroEntries();
  seq_current_displacement_.zeroEntries();

  switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
    case midpoint: {
      midpoint_position_.duplicateFrom(this->solution());
      seq_midpoint_position_.duplicateFrom(this->sequentialSolution());

      midpoint_position_.zeroEntries();
      seq_midpoint_position_.zeroEntries();
      break;
    }
    case half_sum:
    case none:
      break;
  }

  m_snesContext.initialize(*this, MpiInfo::rankProc(), MpiInfo::numProc(), ApplyNaturalBoundaryConditions::no, FlagMatrixRHS::matrix_and_rhs);

  snesInterface().setFunction(vector(), snesFunctionImpl, &m_snesContext);
  snesInterface().setJacobian(matrix(0), matrix(0), snesJacobianImpl, &m_snesContext);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS)
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  m_currentElementType = eltType;
  m_iDisplacement = m_listVariable.getVariableIdList(displacement);
  m_feDisplacement = m_listCurrentFiniteElement[m_iDisplacement];

  if (r_instance.hyperElasticLaw.pressureData->IsCompressible()) {
    r_instance.hyperElasticLaw.pressureData->Init(m_listVariable, m_listCurrentFiniteElement);

    FEL_ASSERT(m_feDisplacement->numQuadraturePoint() == r_instance.hyperElasticLaw.pressureData->FiniteElementPressure()->numQuadraturePoint()
            && "In its current state the code is unable to cope with different quadrature points for both variables.");
    FEL_ASSERT(m_feDisplacement->numDof() == r_instance.hyperElasticLaw.pressureData->FiniteElementPressure()->numDof()
            && "In its current state the code is unable to cope with different degres for both variables.");
  }

  if (flagMatrixRHS == FlagMatrixRHS::only_rhs || flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs)
    m_elemField.initialize(QUAD_POINT_FIELD, *m_feDisplacement, this->dimension());
  }

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::computeElementArray(
  const std::vector<Point*>& elemPoint,
  const std::vector<felInt>& elemIdPoint,
  int& iel, FlagMatrixRHS flagMatrixRHS)
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  IGNORE_UNUSED_ELEM_ID_POINT;
  using namespace HyperelasticityNS;

  FEL_ASSERT(m_feDisplacement);
  CurrentFiniteElement& feDisplacement = *m_feDisplacement; // alias
  FEL_ASSERT(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs);

  feDisplacement.updateFirstDeriv(0, elemPoint);

  if (r_instance.hyperElasticLaw.pressureData->IsCompressible())
    r_instance.hyperElasticLaw.pressureData->FiniteElementPressure()->updateFirstDeriv(0, elemPoint);

  const int NquadraturePoint = feDisplacement.numQuadraturePoint();

  const std::size_t sizeMatrix = std::pow(this->dimension(), 2);
  UBlasMatrix nonLinearPart(sizeMatrix, sizeMatrix);
  ElementVector& elementVector = *m_elementVector[0];
  UBlasVector pressure;

  switch (system_) {
  case HyperelasticityNS::static_: {
    std::vector<UBlasVector> displacement_per_component = extractDisplacementPerComponent(iel, evaluationState(), seqEvaluationState());

    if (r_instance.hyperElasticLaw.pressureData->IsCompressible()) {
      pressure = extractPressure(iel, evaluationState(), seqEvaluationState());
    }

    // DEV NOTE: nonLinearPart size could be defined once and for all. See when profiling whether it is
    // a performance bottleneck.
    ElementMatrix& elementMatrix = *m_elementMat[0];

    // At the moment all variables must share the same integration pattern, so no need to split calculation
    // for displacement and pressure.
    for(int quadraturePointIndex = 0; quadraturePointIndex < NquadraturePoint; ++quadraturePointIndex) {
      UBlasMatrix linearPart;
      UBlasVector rhsPart;

      {
        using namespace Private::HyperElasticity;
        calculateLinearAndNonLinearMatrixAndRhs(r_instance.hyperElasticLaw.hyperElasticLaws->GetHyperElasticLaw(this->m_currentLabel), feDisplacement,
            displacement_per_component,
            pressure,
            quadraturePointIndex, linearPart,
            nonLinearPart, rhsPart, r_instance.hyperElasticLaw.pressureData->IsCompressible());
      }

      UBlasMatrix tangentMatrix = linearPart + nonLinearPart;

      elementMatrix.grad_phi_i_GradientBasedHyperElastTensor_grad_phi_j(1., feDisplacement,
          quadraturePointIndex,
          tangentMatrix, 0, 0);
      elementVector.GradientBasedHyperElasticityVector_grad_phi_i(1., feDisplacement,
          quadraturePointIndex,
          rhsPart, 0);
    }

    // Set the source in the case of a volumic force. Surfacic ones are handled differently (through Neumann boundary condition)
    if (m_typeOfForceApplied == Private::HyperElasticity::volumic) {
      // Vector operators.
      for (int iComp = 0; iComp < this->dimension(); ++iComp)
        m_elemField.setValueVec(m_force, feDisplacement, iComp);

      elementVector.source(1., feDisplacement, m_elemField, 0, this->dimension());
    }

    break;
  }
  case HyperelasticityNS::dynamic_: {

    // Note: the system matrix and rhs are NOT assembled here: they are calculated afterward within method
    // computeSystemAfterAssembling() with the matrix calculated below and others calculated beforehand.

    // Assemble the mass matrix in very first dynamic iteration. As it remains constant, it will be kept
    // unchanged and uncleared throughout the rest of the program lifetime.
    if (!is_first_dynamic_iteration_done_) {
      FEL_ASSERT(m_elementMat[MatrixNS::mass_per_square_time]);
      ElementMatrix& elem_mass_matrix = *m_elementMat[MatrixNS::mass_per_square_time];
      elem_mass_matrix.phi_i_phi_j(2. * FelisceParam::instance().volumic_mass / std::pow(m_fstransient->timeStep, 2),
                                    feDisplacement, 0, 0, this->dimension());
    }


    // Assemble the new stiffness matrix.
    std::vector<UBlasVector> displacement_per_component;
    std::vector<UBlasVector> current_displacement_per_component;

    switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
      case midpoint:
        displacement_per_component = extractDisplacementPerComponent(iel, midpoint_position_, seq_midpoint_position_);
        break;
      case half_sum: {
        displacement_per_component = extractDisplacementPerComponent(iel, evaluationState(), seqEvaluationState());

        if (!is_first_dynamic_iteration_done_)
          current_displacement_per_component = extractDisplacementPerComponent(iel, current_displacement_, seq_current_displacement_);
        break;
      }
      case none:
        break;
    }


    for (int quadraturePointIndex = 0; quadraturePointIndex < NquadraturePoint; ++quadraturePointIndex) {
      UBlasMatrix linearPart;
      UBlasVector rhsPart;

      {
        using namespace Private::HyperElasticity;
        calculateLinearAndNonLinearMatrixAndRhs(r_instance.hyperElasticLaw.hyperElasticLaws->GetHyperElasticLaw(this->m_currentLabel),
            feDisplacement,
            displacement_per_component,
            pressure,
            quadraturePointIndex,
            linearPart,
            nonLinearPart,
            rhsPart,
            r_instance.hyperElasticLaw.pressureData->IsCompressible()
            );
      }

      UBlasMatrix tangentMatrix = linearPart + nonLinearPart;

      FEL_ASSERT(flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs);

      switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
        case midpoint: {
          m_elementMat[MatrixNS::new_stiffness]->grad_phi_i_GradientBasedHyperElastTensor_grad_phi_j(1., feDisplacement,
              quadraturePointIndex,
              tangentMatrix, 0, 0);
          elementVector.GradientBasedHyperElasticityVector_grad_phi_i(1., feDisplacement,
              quadraturePointIndex,
              rhsPart, 0);
          break;
      }
      case half_sum: {
        m_elementMat[MatrixNS::new_stiffness]->grad_phi_i_GradientBasedHyperElastTensor_grad_phi_j(.5, feDisplacement,
            quadraturePointIndex,
            tangentMatrix, 0, 0);
        m_elementVector[VectorNS::new_stiffness]->GradientBasedHyperElasticityVector_grad_phi_i(1., feDisplacement,
            quadraturePointIndex,
            rhsPart, 0);



        if (!is_first_dynamic_iteration_done_) {
          {
            using namespace Private::HyperElasticity;
            calculateLinearAndNonLinearMatrixAndRhs(r_instance.hyperElasticLaw.hyperElasticLaws->GetHyperElasticLaw(this->m_currentLabel), feDisplacement,
                current_displacement_per_component,
                pressure,
                quadraturePointIndex,
                linearPart, nonLinearPart,
                rhsPart,
                r_instance.hyperElasticLaw.pressureData->IsCompressible()
                );
          }

          tangentMatrix = linearPart + nonLinearPart;


          m_elementMat[MatrixNS::current_stiffness]->grad_phi_i_GradientBasedHyperElastTensor_grad_phi_j(.5, feDisplacement,
              quadraturePointIndex,
              tangentMatrix, 0, 0);

          m_elementVector[VectorNS::current_stiffness]->GradientBasedHyperElasticityVector_grad_phi_i(1., feDisplacement,
              quadraturePointIndex,
              rhsPart, 0);
        }
        break;
      }
      case none:
        break;
      }
    }
    break;

  }

  } // switch(system_)
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::preSNESAssembleAdditional()
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  using namespace HyperelasticityNS;

  // clearMatrixRHS(); useless: already performed in preSNESAssemble()
  clearMatrix(MatrixNS::new_stiffness);

  switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
    case midpoint: {

      {
        midpoint_position_.copyFrom(current_displacement_);
        midpoint_position_.axpy(1., evaluationState());
        midpoint_position_.scale(0.5);
      }

      {
        gatherVector(midpoint_position_, seq_midpoint_position_);
      }
      break;
    }
    case half_sum: {
      clearVector(VectorNS::new_stiffness);
      break;
    }
    case none:
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::computeSystemAfterAssembling()
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  if (system_ == HyperelasticityNS::dynamic_) {
    using namespace HyperelasticityNS;

    // In the very first dynamic iteration the mass matrices are assembled once and for all.
    if (!is_first_dynamic_iteration_done_)
      SetFirstDynamicIterationDone();

    // Now all elements are settled to build the system.
    {
      // Tangent
      matrix(MatrixNS::system).copyFrom(matrix(MatrixNS::mass_per_square_time), SAME_NONZERO_PATTERN);

      switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
        case midpoint: {
          matrix(MatrixNS::system).axpy(0.5, matrix(MatrixNS::new_stiffness), SAME_NONZERO_PATTERN);
          break;
        }
        case half_sum: {
          matrix(MatrixNS::system).axpy(1., matrix(MatrixNS::new_stiffness), SAME_NONZERO_PATTERN);
          matrix(MatrixNS::system).axpy(1., matrix(MatrixNS::current_stiffness), SAME_NONZERO_PATTERN);
          break;
        }
        case none:
          break;
      }
    }

    {
      // Residual
      PetscVector vector;
      vector.duplicateFrom(current_displacement_);
      // < the contribution to RHS of current displacement term.

      vector.copyFrom(evaluationState());
      vector.axpy(-1., current_displacement_);
      vector.axpy(-m_fstransient->timeStep, current_velocity_);

      PetscVector dynamic_contribution_to_rhs;
      dynamic_contribution_to_rhs.duplicateFrom(this->vector());
      mult(matrix(MatrixNS::mass_per_square_time), vector, dynamic_contribution_to_rhs);

      this->vector().axpy(1., dynamic_contribution_to_rhs);


      switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
        case midpoint:
          break;
        case half_sum: {
          this->vector().axpy(1., this->vector(VectorNS::new_stiffness));
          this->vector().axpy(1., this->vector(VectorNS::current_stiffness));
          break;
        }
        case none:
          break;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

std::vector<UBlasVector> LinearProblemHyperElasticity::extractDisplacementPerComponent(
  PetscInt iel,
  PetscVector& mpi_vector,
  PetscVector& seq_vector) const
{
  std::vector<UBlasVector> ret(this->dimension());

  for (int iComp = 0; iComp < this->dimension(); ++iComp) {
    ret[iComp] = extractUnknownValue(m_iDisplacement, iComp, m_currentElementType, iel, mpi_vector, seq_vector);
  }

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

inline UBlasVector LinearProblemHyperElasticity::extractPressure(
  PetscInt iel,
  PetscVector& mpi_vector,
  PetscVector& seq_vector) const
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  FEL_ASSERT(r_instance.hyperElasticLaw.pressureData->IsCompressible());

  return extractUnknownValue(r_instance.hyperElasticLaw.pressureData->IndexPressure(), 0, m_currentElementType, iel, mpi_vector, seq_vector);
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector LinearProblemHyperElasticity::extractUnknownValue(std::size_t index_unknown, std::size_t iComp, ElementType eltType,
                                                              std::size_t iel, PetscVector& mpi_vector, PetscVector& seq_vector) const 
{
  const SupportDofMesh& support = m_supportDofUnknown[index_unknown];

  PetscInt id_element_support;
  support.getIdElementSupport(eltType, iel, id_element_support);

  const std::size_t Nlocal_support_dof = static_cast<std::size_t>(support.getNumSupportDof(id_element_support));

  UBlasVector ret(Nlocal_support_dof);
  PetscInt index_global_dof;

  for (std::size_t iSupport = 0; iSupport < Nlocal_support_dof; ++iSupport) {
    m_dof.loc2glob(id_element_support, iSupport, index_unknown, iComp, index_global_dof);

    PetscInt index_local_dof;

    // m_mappingNodes is an empirical guess: it is the one that works not stupidly, ie returns a valid index if the dof is on current processor...
    // DEV I'm not sure it will work with two variables, but the support that seems more logical failed utterly...
    ISGlobalToLocalMappingApply(m_mappingNodes,
                                IS_GTOLM_MASK,
                                1, &index_global_dof,
                                FELISCE_PETSC_NULLPTR, &index_local_dof);

# ifndef NDEBUG
    PetscInt localSize;
    mpi_vector.getLocalSize(&localSize);
    if (index_local_dof >= localSize) {
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
      std::cerr << '[' << rank << "] Dof index = " << index_local_dof
      << " should be lower than local displacement size which "
      "is " << localSize << std::endl;
      FEL_ASSERT(false);
    }
# endif // NDEBUG

    // DEV \todo These indexes should be accessible locally with the use of ghost vertices...
    if (index_local_dof == -1) {
      // Case in which the values aren't stored on the current processor... I must fetch the global value!!!!
      PetscScalar * seq_vector_array;
      seq_vector.getArray(&seq_vector_array);
      AOApplicationToPetsc(m_ao, 1, &index_global_dof);
      ret(iSupport) = seq_vector_array[index_global_dof];
      seq_vector.restoreArray(&seq_vector_array);
    } else {
      PetscScalar* mpi_vector_array;
      mpi_vector.getArray(&mpi_vector_array);
      ret(iSupport) = mpi_vector_array[index_local_dof];
      mpi_vector.restoreArray(&mpi_vector_array);
    }
  }

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::computeElementArrayBoundaryCondition(
  const std::vector<Point*>& elemPoint,
  const std::vector<felInt>& elemIdPoint,
  int& iel, FlagMatrixRHS flagMatrixRHS)
{
  IGNORE_UNUSED_IEL;
  IGNORE_UNUSED_ELEM_ID_POINT;

  switch(flagMatrixRHS) {
  case FlagMatrixRHS::only_rhs:
  case FlagMatrixRHS::matrix_and_rhs: {
    CurvilinearFiniteElement& feCurvDisplacement = *m_listCurvilinearFiniteElement[m_iDisplacement];
    feCurvDisplacement.updateMeasNormal(0, elemPoint);

    // Below: only when spatial function
    if (m_boundaryConditionList.NeumannNormal(0)->typeValueBC() == FunctionS) {
      {
        const int component_index = 0;
        m_elemFieldNeumannNormal[0].setValueVec(m_force, feCurvDisplacement, component_index);

        // Important: in my first approach I added the following line, to mimic what was done in volumic force case:
        //m_elemVecBD->source(1., feCurvDisplacement, m_elemFieldNeumannNormal[0], block, Ncomponent);
        // It was wrong: the source was added more properly in applyNaturalBoundaryCondition()
      }
    }
    break;
  }
  case FlagMatrixRHS::only_matrix:
    break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::GoToDynamic()
{
  system_ = HyperelasticityNS::dynamic_;
  UpdateDisplacementBetweenTimeStep();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemHyperElasticity::endIteration()
{
  // Retrieve the instance
  const auto& r_instance = FelisceParam::instance();

  // First update velocities.
  {
    current_velocity_.scale(-1.);
    PetscVector diff_displ;
    diff_displ.duplicateFrom(this->solution());
    {
      diff_displ.copyFrom(this->solution());
      diff_displ.axpy(-1., current_displacement_);
      diff_displ.scale(2. / m_fstransient->timeStep);
    }
    current_velocity_.axpy(1., diff_displ);
  }

  // Then update displacement.
  UpdateDisplacementBetweenTimeStep();

  using namespace HyperelasticityNS;

  switch(r_instance.hyperElasticLaw.timeSchemeHyperelasticity) {
    case midpoint:
      break;
    case half_sum:
      matrix(MatrixNS::current_stiffness).copyFrom(matrix(MatrixNS::new_stiffness),SAME_NONZERO_PATTERN);
      vector(VectorNS::current_stiffness).copyFrom(vector(VectorNS::new_stiffness));
      break;
    case none:
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

inline void LinearProblemHyperElasticity::SetFirstDynamicIterationDone()
{
  FEL_ASSERT(!is_first_dynamic_iteration_done_ && "Should only be called once!");
  is_first_dynamic_iteration_done_ = true;
}

/***********************************************************************************/
/***********************************************************************************/

inline void LinearProblemHyperElasticity::UpdateDisplacementBetweenTimeStep()
{
  current_displacement_.copyFrom(this->solution());
  gatherSolution();
  seq_current_displacement_.copyFrom(this->sequentialSolution());
}

}
