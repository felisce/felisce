//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone C. Corrado
//

#ifndef _LinearProblemBidomainTransMemb_HPP
#define _LinearProblemBidomainTransMemb_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  /*!
   \class LinearProblemBidomainTransMemb
   \authors E. Schenone C. Corrado
   \date 27/01/201
   \brief ???
   */
  class LinearProblemBidomainTransMemb:
    public LinearProblem {
  public:
    LinearProblemBidomainTransMemb();
    ~LinearProblemBidomainTransMemb() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber);
    void getAngleFiber(felInt iel, int iUnknown, std::vector<double>& elemAngle);
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      m_currentLabel=label;
      IGNORE_UNUSED_FLAG_MATRIX_RHS;
    }
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void initializeTimeScheme(Bdf* bdf) override {
      m_bdf = bdf;
    }
    void readData(IO& io) override;

    void addMatrixRHS() override;

    const PetscVector&  massRHS() const {
      return m_massRHS;
    }
    PetscVector&  massRHS() {
      return m_massRHS;
    }
    const PetscVector&  ksigmaiRHS() const {
      return _KsigmaiRHS;
    }
    PetscVector&  ksigmaiRHS() {
      return _KsigmaiRHS;
    }

    std::vector<double> & EndocardiumDistance() {
      return m_vectorDistance;
    }

  protected:
    CurrentFiniteElement* m_fePotTransMemb;
    CurvilinearFiniteElement* m_fePotTransMembCurv;
    felInt m_ipotTransMemb;
  private:
    Bdf* m_bdf;
    PetscVector m_massRHS;
    PetscVector _KsigmaiRHS;
    double* m_fiber;
    double* m_angleFiber;
    double* m_endocardiumDistance;
    std::vector<double> m_vectorDistance;
  };
}

#endif
