//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef _LINEARPROBLEMFSICONTINUATION_HPP
#define _LINEARPROBLEMFSICONTINUATION_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"
#include "DegreeOfFreedom/dof.hpp"


/*!
 \file linearProblemFSIContinuation.hpp
 \date 22/04/2022
 \brief Continuation method for NS 
*/

namespace felisce {  
  /*!
  \class LinearProblemNSContinuation
  \authors M. Nechita & M. Agbalessi
  \date 22/04/2022
  \brief Manage specific functions for unique continuation for Navier-Stikes equations.
  */
  
  class LinearProblemNSContinuation:
    public LinearProblem {
  public:
    LinearProblemNSContinuation();
    ~LinearProblemNSContinuation(){};

    // usual methods
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override; // set the current finite element 
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override; // compute the elementary arrays for the block system
    
    // methods regarding the cip stabilization
    void userChangePattern(int numProc, int rankProc) override;
    void assembleCIPStabilization();
    void addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec);
    void updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fe, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup);
    
    // method to read the data from a forward problem
    virtual void readVelocityData(IO& io,double iteration);
    
    // methods to use a feature previous velocity 
    virtual void setPreviousVelocity(PetscVector & previousVelocityCandidate, AO & aoPreviousVelocity, Dof & dofPreviousVelocity); 
    inline PetscVector & previousVelocity(){return *m_previousVelocity;}
    inline AO & aoPreviousVelocity(){return *m_aoPreviousVelocity;}
    inline Dof & dofPreviousVelocity(){return *m_dofPreviousVelocity;}
    
    
  protected:
    CurrentFiniteElement* m_feVel;
    CurrentFiniteElement* m_fePres;
    ElementField m_sourceTerm;
    ElementField m_dataTerm;
   
    Variable* m_velocity;
    Variable* m_pressure;
    felInt m_iVelocity;
    felInt m_iPressure;
    felInt m_iDisplacement;
    felInt m_iVelocityDiv; 
    felInt m_iPotThorax; 

    double m_viscosity;
    bool m_useSymmetricStress;

    PetscVector m_velocityData;  
    std::vector<double> m_vectorVelocityData;
    PetscVector m_oldVelocity;

    //ALE
    ElementField m_elemFieldAdv;
    ElementField m_elemFieldVelMesh;
    ElementField m_beta;
    int numDofExtensionProblem;

    std::vector<PetscInt> m_petscToGlobal1;
    std::vector<PetscInt> m_petscToGlobal2;
    std::vector<PetscScalar> m_auxvec;

    // velocity in Rhs and convective terms
    bool m_useDataPreviousVelocity ;  
    PetscVector * m_previousVelocity ; 
    AO  * m_aoPreviousVelocity; 
    Dof * m_dofPreviousVelocity; 

  };
}

#endif
