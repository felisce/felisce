//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef LINEARPROBLEM_HPP
#define LINEARPROBLEM_HPP

// System includes
#include <unordered_set>
#include <vector>
#include <numeric>

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#include <parmetis.h>
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"
#include "Core/NoThirdPartyWarning/Petsc/error.hpp"

// Project includes
#include "PETScInterface/petscVector.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "PETScInterface/KSPInterface.hpp"
#include "PETScInterface/SNESInterface.hpp"
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"
#include "Core/felisceTools.hpp"
#include "Core/mpiInfo.hpp"
#include "Core/felisceTransient.hpp"
#include "DegreeOfFreedom/boundaryConditionList.hpp"
#include "DegreeOfFreedom/listVariable.hpp"
#include "DegreeOfFreedom/listUnknown.hpp"
#include "DegreeOfFreedom/supportDofMesh.hpp"
#include "DegreeOfFreedom/dof.hpp"
#include "DegreeOfFreedom/csrmatrixpattern.hpp"
#include "DegreeOfFreedom/dofBoundary.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/listCurrentFiniteElement.hpp"
#include "FiniteElement/listCurrentFiniteElementWithBd.hpp"
#include "FiniteElement/listCurvilinearFiniteElement.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/point.hpp"
#include "InputOutput/io.hpp"
#include "Solver/CVGraphInterface.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name  Enum's
///@{

enum class FlagMatrixRHS : int 
{
  matrix_and_rhs = 0,
  only_matrix = 1,
  only_rhs = 2
};

namespace DirichletApplicationOptions {
  enum Values {
    nonSymmetricPseudoElimination = 0,
    symmetricPseudoElimination = 1,
    penalization = 2
  };
}

enum class ApplyNaturalBoundaryConditions {
  no,
  yes
};

///@}
///@name Type Definitions
///@{

// Forward declarations.
class Model;
class FelisceParam;
class FelisceTransient;
// class IO;
class Bdf;
class LumpedModelBC;
class CardiacCycle;
class RISModel;
class ElementField;
class BoundaryCondition;
#ifdef FELISCE_WITH_CVGRAPH
class cvgMainSlave;
#endif

typedef ApplyNaturalBoundaryConditions ApplyNaturalBoundaryConditionsType;

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

// SNES staff
class SnesContext {
public:
  SnesContext()= default;;
  void initialize(LinearProblem& linearProblem, int rankProc, int sizeProc,
      ApplyNaturalBoundaryConditionsType do_apply_natural,
      FlagMatrixRHS flag);

  LinearProblem* linearProblem;
  int rankProc;
  int sizeProc;
  ApplyNaturalBoundaryConditionsType do_apply_natural;
  FlagMatrixRHS flag_matrix_rhs;
};

/*!
  \class LinearProblem
  \authors J. Foulon & J-F. Gerbeau & V. Martin
  \date 05/01/2011
  \brief Manage general functions of an EDP problem.
*/
class LinearProblem 
{
  public:
    ///@name Type Definitions
    ///@{

      typedef GeometricMeshRegion::ElementType ElementType;

      /// Two types of PetsVec, to improve readibility
      enum vectorType { sequential, ///< Sequential vectors
                        parallel    ///< Parallel vectors
                      };

      /// Pointer definition of LinearProblem
      FELISCE_CLASS_POINTER_DEFINITION(LinearProblem);

    ///@}
    ///@name Life Cycle
    ///@{

      /*!
        * \brief Constructor
        *
        * \param[in] name Name of the linear problem
        * \param[in] numMat Number of matrices
        * \param[in] numVec Number of rhs
        */
      LinearProblem(const std::string name = "No name", const std::size_t numMat = 1, const std::size_t numVec = 1);

      /*!
       * \brief Destructor
       */
      virtual ~LinearProblem();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

      /*!
        * \brief Initialization steps. Beware: all quantities may not be initialised there.
        *
        * \param[in] mesh The vector of meshes
        * \param[in] fstransient TODO
        * \param[in] comm TODO
        * \param[in] doUseSNES True if you handling a non linear problem and are willing to use Petsc Newton-Raphson method. False in most of the current cases!
        */
      virtual void initialize(std::vector<GeometricMeshRegion::Pointer>& /*mesh*/, FelisceTransient::Pointer /*fstransient*/, MPI_Comm& /*comm*/, bool /*doUseSNES*/) = 0;

      /*!
        * \brief Init few quantities.
        *
        * Init a handful of attributes.
        *
        * This method is usually called in the beginning of the virtual #initialize() defined just above (\todo
        * This design is error prone; it should be reversed: a non virtual initialize which includes a call to
        * a virtual method that can be overloaded. Hence there would be no risk of forgetting the call of present method...)
        *
        * \param[in] doUseSNES True if you handling a non linear problem and are willing to use Petsc Newton-Raphson method.
        */
      void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, MPI_Comm& comm, bool doUseSNES);

      /*!
       * \brief Finalizes the linear problem
       */
      virtual void finalize(std::vector<GeometricMeshRegion::Pointer>& /*mesh*/, FelisceTransient::Pointer /*fstransient*/, MPI_Comm& /*comm*/) {}

      /*!
        * \brief Initialize some attributes at the very end of the #Model::initializeLinearProblem() method.
        *
        * Rationale: for Elasticity and HyperElasticity, I need to set the values of attributes but can't do it
        * in #initialize() because some of the quantities I need (namely the boundary conditions and the dof) aren't
        * defined yet.
        */
      virtual void InitializeDerivedAttributes() {}

      /*!
       * \brief Set the ID of the problem solver
       */
      inline void fixIdOfTheProblemSolver(int const id) { m_identifier_solver = id; };



      // Define Physical Variable associate to the problem
      //==================================================
      void definePhysicalVariable(const std::vector<PhysicalVariable>& listVariable, const std::vector<std::size_t>& listNumComp);
      
      virtual void userAddOtherVariables(std::vector<PhysicalVariable>& /*listVariable*/, std::vector<std::size_t>& /*listNumComp*/) {}

      virtual void userAddOtherUnknowns(std::vector<PhysicalVariable>& /*listVariable*/, std::vector<std::size_t>& /*listNumComp*/) {}
      


      // Compute Dof
      //============

      /**
       * @brief Compute global degrees of freedom with global support dof creation.
       *        Build pattern of the matrix.
       */
      void computeDof(int numProc, int idProc);
      
      /**
       * @brief Change pattern function
       */ 
      virtual void userChangePattern(int /*numProc*/, int /*rankProc*/) {};

      /**
       * @brief Initialize supprtDof in derived problem
       */
      virtual void initSupportDofDerivedProblem() {};

      /// for one element and a local suppordof id, find coordinate of support.
      void getSupportCoordinate(ElementType& eltType, felInt iel, felInt idSupport, int iUnknown, Point& pt);

      /// for one element and a local suppordof id, find coordinate of support.
      void getSupportCoordinate(felInt idElt, felInt idSupport, int iUnknown, Point& pt);
  


      // Partition function
      //===================

      /**
       * @brief After partitionning we obtain element repartition and we create mesh region and "region" support dof.
       */
      void cutMesh();

      /**
       * @brief use ParMetis to partition dof (line of the matrix).
       * The function call parmetis to compute the dof partition which is stored in m_dofPart.
       * Next, the application ordering (AO) used to fo from the global felisce ordering to the global petsc ordering
       * is created.
       * Finaly, the element partition is created and is store in m_eltPart.
       * @param[in] numPart Number of process.
       * @param[in] rankPart Rank of the current process.
       * @param[in] comm MPI communicator.
       */
      void partitionDof(idx_t numPart, int rankPart, MPI_Comm comm);
      // void partitionDof_Petsc(idx_t numPart, int rankPart, MPI_Comm comm);



      // Build Mesh and SupportDofMesh local with partition arrays
      //==========================================================
      
      /**
       * @brief Create the local mesh and local supportDofMesh-> Create the local to global mapping for the elements
       * One element is on rankPart if the majority of its dof is in the process.
       * All points of the mesh are duplicated.
       * @param[in] rankPart Rank of the current process.
       */
      void setLocalMeshAndSupportDofMesh(int rankPart);



      // Allocate Matrix and RHS
      //========================
      
      /**
       * @brief use the pattern defined with dof to allocate memory for the matrix in CSR format.
       * This function is indeed allocating the matrix, but it is also creating the rhs, solution, serial solution
       * vectors. It creates two mappings:
       * - m_mappingNodes which std::unordered_map the local id to the global id of dofs.
       * - m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc which is actually a std::vector of mappings that maps the
       * local id to the global id in the petsc ordering for each unknown.
       */
      void allocateMatrix();
  
      /**
       * @brief Allocate the serial vector that contains the solution of the linear system.
       */
      void allocateSequentialSolution();



      // Assemble Matrix
      //================

      /**
       * @brief elementary loop to build matrix.
       * This function uses specific functions which are defined in derived problem classes.
       */
      void assembleMatrixRHS(int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
      
      /**
       * @brief Initialize the currentFiniteElement list.
       */
      void defineFiniteElement(const ElementType& eltType);

      /**
       * @brief Initialize the currentFiniteElement list.
       */  
      void defineFiniteElement(const std::vector<ElementType>& eltType);
      
      /**
       * @brief Initialize element array
       */ 
      void initElementArray(const bool initTranspose=false);
  
      /**
       * @brief Initialize the element fields
       */ 
      virtual void initPerElementType(ElementType /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**
       * @brief Use by user to add some terms like source term define with elemField.
       */       
      virtual void userElementInit() {};

      /**
       * @brief Initialize some parameter associated to a mesh region.
       */  
      virtual void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /**
       * @brief Get vertices, coordinates and support element of element
       */  
      void setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, std::vector<felInt>& vectorSupport);
      
      /**
       * @brief Get vertices, coordinates and support element of element
       */  
      void setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, felInt* ielSupportDof);

      /**.
       * @brief Update finite element with element vertices coordinates and compute operators.
       */  
      virtual void computeElementArray(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Update finite element with element vertices coordinates and compute operators.
       */  
      virtual void computeElementArray(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel1*/, felInt& /*iel2*/, ElementType& /*eltType*/, felInt& /*ielGeo*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Update finite element with element vertices coordinates and compute operators for characteristics.
       */  
      virtual void computeElementArrayCharact(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, ElementType& /*eltType*/, felInt& /*ielGeo*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Compute user defined operators.
       */  
      virtual void userElementCompute(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Compute user defined operators.
       */        
      virtual void userElementCompute(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel1*/, felInt& /*iel2*/, ElementType& /*eltType*/, felInt& /*ielGeo*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Compute user defined operators.
       */  
      virtual void userElementCompute(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, const std::vector<Point*>& /*elemPointVol*/, std::vector<felInt>& /*elemIdPointVol*/, felInt& /*ielVol*/, std::vector<felInt>& /*elemIdPointLocal*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**
       * @brief use elementary calculus to fill the global matrix.
       */
      void setValueMatrixRHS(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs, bool buildMatT=false);

      /**
       * @brief use elementary calculus to fill the global matrix.
       */
      void setValueMatrixRHS(const felInt ielSupportDof1, const felInt ielSupportDof2, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs, bool buildMatT=false);

      /**
       * @brief use elementary calculus to fill the global matrix.
       */
      void setValueCustomMatrix(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, PetscMatrix& mat, bool buildMatT = false);
            
      /**
       * @brief use elementary calculus to fill the global matrix.
       */
      void setValueCustomMatrix(const felInt ielSupportDof1, const felInt ielSupportDof2, PetscMatrix& mat, bool buildMatT = false);

      /**
       * @brief Function to set the elemVector[0] into a generic vector vec.
       */
      void setValueCustomVector(const std::vector<felInt>& ielSupportDof1, InsertMode mode, PetscVector& vec);

      /**
       * @brief Function to set the elemVector[0] into a generic vector vec.
       */
      void setValueCustomVector(const felInt ielSupportDof1, InsertMode mode, PetscVector& vec);

      /**
       * @brief Allocate memory for assemble matrix and rhs.
       */      
      void allocateArrayForAssembleMatrixRHS(FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
      
      /**
       * @brief Desallocate memory for assemble matrix and rhs.
       */        
      void desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /**
       * @brief Assembly loop on boundary element of a certain label in implicit LumpedModelBC
       */ 
      virtual void assembleMatrixImplLumpedModelBC(int /*rank*/, const std::size_t /*iLabel*/) {};

      /**
       * @brief Assembly loop on crack elements pre-defined
       */ 
      virtual void derivedAssembleMatrixCrackModel(int /*rank*/) {};

      /**
       * @brief elementary loop to build matrix.
       * This function uses specific functions which are define in derived problem classes.
       */
      void assembleMatrixRHSBD(int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
      
      /**
       * @brief Initialize the curvilinearFiniteElement list.
       */  
      void defineCurvilinearFiniteElement(const ElementType& eltType);

      /**
       * @brief Initialize the curvilinearFiniteElement list.
       */    
      void defineCurvilinearFiniteElement(const std::vector<ElementType>& eltType);

      /**
       * @brief Initialize the currentFiniteElementWithBd list.
       */      
      void defineCurrentFiniteElementWithBd(const ElementType& eltType);
      
      /**
       * @brief Initialize the currentFiniteElementWithBd list.
       */    
      void defineCurrentFiniteElementWithBd(const std::vector<ElementType>& eltType);

      /**
       * @brief Initialize element array
       */ 
      void initElementArrayBD(const bool initTranspose=false);

      /**
       * @brief Initialize the element fields
       */ 
      virtual void initPerElementTypeBD(ElementType /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};
  
      /**
       * @brief Use by user to add some terms like source term define with elemField.
       */      
      virtual void userElementInitBD() {};
  
      /**
       * @brief Initialize some parameter associated to a mesh region.
       */  
      virtual void initPerDomainBD(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /**
       * @brief Get vertices, coordinates, normal, tangent and support element of element
       */    
      void setElemPointNormalTangent(ElementType& eltType, felInt iel,std::vector<Point*>& elemPoint, std::vector<Point*>& elemNormal, 
                                     std::vector< std::vector<Point*> >& elemTangent, std::vector<felInt>& elemIdPoint, std::vector<felInt>& vectorSupport);

      /**.
       * @brief Update finite element with element vertices coordinates and compute operators.
       */    
      virtual void computeElementArrayBD(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Update finite element with element vertices coordinates and compute operators.
       */  
      virtual void computeElementArrayBD(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, const std::vector<Point*>& /*elemNormal*/, const std::vector<std::vector<Point*> >& /*elemTangent*/, felInt& /*iel*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Compute user defined operators.
       */    
      virtual void userElementComputeBD(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/) {};
     
      /**
       * @brief use elementary calculus to fill the global matrix.
       */
      void setValueMatrixRHSBD(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs, bool buildMatT=false);
      
      /**
       * @brief use elementary calculus to fill the global matrix.
       */
      void setValueMatrixRHSBD(const felInt ielSupportDof1, const felInt ielSupportDof2, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs, bool buildMatT=false);
      
      /**
       * @brief Place the m_elementMatrixBD[0] into a generic matrix.
       */      
      void setValueCustomMatrixBD(const std::vector<felInt>& ielSupportDof1, const std::vector<felInt>& ielSupportDof2, PetscMatrix& mat, bool buildMatT=false);

      /**
       * @brief Place the m_elementMatrixBD[0] into a generic matrix.
       */ 
      void setValueCustomMatrixBD(const felInt ielSupportDof1, const felInt ielSupportDof2, PetscMatrix& mat, bool buildMatT=false);

      /**
       * @brief Place the m_elementVectorBD[0] into a generic vec.
       */
      void setValueCustomVectorBD(const std::vector<felInt>& ielSupportDof1, InsertMode mode, PetscVector& vec) ;

      /**
       * @brief Place the m_elementVectorBD[0] into a generic vec.
       */
      void setValueCustomVectorBD(const felInt ielSupportDof1, InsertMode mode, PetscVector& vec );
      
      /**
       * @brief Allocate memory for assemble matrix and rhs.
       */      
      void allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /**
       * @brief elementary loop to build matrix for "mixed" domain (volumes+surfaces or surfaces+edges).
       * This function is used when the 3D (resp. 2D) domain has some surfaces (resp. edges) that are not boundaries of any volume (resp. surface), see for instance atria+ventricles.
       * This function uses specific functions which are defined in derived problem classes.
       */
      void assembleMatrixRHSCurrentAndCurvilinearElement(int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /**
       * @brief This a quite general function to have a general assembly loop on the boundary.
       */ 
      template<class theClass>
      void assemblyLoopBoundaryGeneral( void (theClass::*functionOfTheLoop)( felInt ),
                                        const std::vector<felInt> & labels, void (theClass::*initPerElementType)(),
                                        void (theClass::*updateFunction)(const std::vector<Point*>&,const std::vector<int>&));
      

      // Assemble Boundary Conditions
      //=============================

      /*!
        * \brief Apply boundary counditions
        *
        * \param[in] doPrintBC If true (and verbose > 2) print informations about boundary conditions.
        * Rationale: in a Newton-Raphson method, we do not want to print these informations for each iteration.
        * \param[in] do_apply_natural ApplyNaturalBoundaryConditions::yes in most cases.
        * Rationale: in Newmark time scheme, we do not want to apply thee ones in the time iterations.
        *
        */
      void applyBC(const int & applicationOption, int rank, FlagMatrixRHS flagMatrixRHSEss = FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS flagMatrixRHSNat = FlagMatrixRHS::matrix_and_rhs, const int idBCforLinCombMethod=0,
                   bool doPrintBC = true, ApplyNaturalBoundaryConditionsType do_apply_natural = ApplyNaturalBoundaryConditions::yes);

      /**
       * @brief Assembly loop for boundary conditions on boundary element with curvilinear finite element.
       */ 
      void assembleMatrixRHSNaturalBoundaryCondition(int rank, FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs, const int idBCforLinCombMethod=0);

      /**
       * @brief Allocate vector boundary condition for derived problems
       */       
      virtual void allocateVectorBoundaryConditionDerivedLinPb() {};
      
      /**
       * @brief Initialize the element fields
       */ 
      virtual void initPerElementTypeBoundaryCondition(ElementType& /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/) {};
      
      /**
       * @brief Use by user to add some terms
       */     
      virtual void userElementInitNaturalBoundaryCondition() {};

      /**
       * @brief Allocate vector m_elemFieldRobin, m_elemFieldRobinNormal, m_elemFieldNeumann, m_elemFieldNeumannNormal, m_elemFieldBackflowStab.
       */ 
      void allocateElemFieldBoundaryCondition(const int idBCforLinCombMethod = 0);

      /**
       * @brief Overwrite value of m_elemFieldRobin, m_elemFieldRobinNormal, m_elemFieldNeumann and m_elemFieldNeumannNormal in derived linear problem.
       */ 
      virtual void allocateElemFieldBoundaryConditionDerivedLinPb(const int /*idBCforLinCombMethod*/) {};

      /**
       * @brief Initiallize some paramater associate to a mesh region.
       */ 
      virtual void initPerDomainBoundaryCondition(std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, int label, felInt /*numEltPerLabel*/, felInt* /*ielSupportDof*/, ElementType& /*eltType*/, felInt /*numElement_eltType*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) { m_currentLabel = label; };

      /**
       * @brief Update finite element with element vertices coordinates and compute operators (for curvilinear finite element).
       */ 
      virtual void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
  
      /**
       * @brief Compute user defined boundary conditions.
       */ 
      virtual void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, int /*label*/) {};

      /**
       * @brief Compute user defined boundary conditions.
       */ 
      virtual void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*ielSupportDof1*/, felInt& /*ielSupportDof2*/, felInt& /*ielGeoGlobal*/, int /*label*/) {};

      /**
       * @brief Apply boundary condition of type: Neumann, NeumannNormal
       */
      void applyNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::only_rhs);

      /**
       * @brief Compute and apply boundary conditions
       */
      virtual void computeAndApplyElementNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof1, felInt& ielSupportDof2, felInt& ielGeoGlobal, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /**.
       * @brief Compute operators for surface model
       */  
      virtual void computeElementArraySurfaceModel(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, FlagMatrixRHS /*flagMatrixRHS*/ = FlagMatrixRHS::matrix_and_rhs) {};

      /**.
       * @brief Essential boundary condition : Dirichlet, Essential LumpedModelBC
       */  
      void applyEssentialBoundaryCondition(int applicationOption, int rank, FlagMatrixRHS flagMatrixRHS);

      /**.
       * @brief Essential boundary condition derived problem
       */  
      virtual void applyEssentialBoundaryConditionDerivedProblem(int /*rank*/, FlagMatrixRHS /*flagMatrixRHS*/) {};

      /**
       * @brief Define boundary condition and allocate element fields
       */
      virtual void defineBC();

      /**
       * @brief determine the dof for each essential boundary condition
       */
      void determineDofAssociateToLabel();
      
      /**
       * @brief Initialize the array idEltAndIdSupport for the boundary condition BC.
       * @param[in] BC The boundary condition
       */
      void determineElementSuppDofAssociateToBC(BoundaryCondition* BC);

      /**
       * @brief Set the value of the constant BC (in time AND space)
       */
      void finalizeEssBCConstantInT();

      /**
       * @brief Set the value of the constant BC in users (in time AND space)
       */      
      virtual void userFinalizeEssBCConstantInT() {};
      
      /**
       * @brief Call the functions for the essential BC
       */
      void finalizeEssBCTransient();

      /**
       * @brief Call the derived problem functions for the essential BC
       */
      virtual void finalizeEssBCTransientDerivedProblem() {};

      /**
       * @brief Call the user functions for the essential BC
       */
      virtual void userFinalizeEssBCTransient() {};

      /**
       * @brief ???
       */
      void getListDofOfBC(const BoundaryCondition& BC, std::unordered_set<felInt>& idDofBC, std::unordered_map<int, double>& idBCAndValue);

      /**
       * @brief ???
       */
      void getRings();
  
      /**
       * @brief ???
       */
      virtual void userGetBC(BoundaryCondition* &BCIn,BoundaryCondition* &BCOut, BoundaryCondition* &BCLat);
      
      /**
       * @brief ???
       */
      void getListDofOfSurfaceByBc(const BoundaryCondition& BC, std::set<felInt>& idDofBC, int rankProc );
      
      /**
       * @brief ???
       */
      virtual double userComputeValueNeumannNormalTransient(const int /*iNeumannNormalBC*/) { return 0; };

      /**
       * @brief ???
       */
      void setValueBoundaryCondition(BoundaryCondition* bc, PetscVector& vecOfValue);

      /**
       * @brief ???
       */
      virtual void defineRobinBCVector(PetscVector& /*robinVec*/, const int /*iRobin*/) {};
  


      //Solve linear system
      //===================

      /*!
        * \brief Post assemble operations.
        *
        * Performs some operations after assembling is done. For instance, in dynamic systems the assembled matrices
        * are not necessarily those of the system, but rather intermediate ones required to build the matrices of the system.
        * For instance, in dynamic elasticity a new stiffness matrix is assembled and the matrix and RHS of the
        * system are created from this one and others stored elsewhere.
        *
        * \internal I know Model class provides a postAssemblingMatrixRHS() method. However, it is not enough in the
        * case of hyperelasticity: as Petsc's SNES is in charge of the Newton Raphson's loop, I must respect its interface
        * and assembling is done at Petsc's call of the function given to SNESSetFunction().
        * I deliberately chose a very different method name to avoid any ambiguity.
        */
      virtual void computeSystemAfterAssembling() {};

      /*!
        * \brief Pre assemble operations for iterative solve.
        *
        * Perform some operations before assembling the matrices (for instance clearing them out.)
        *
        * \internal I know Model class provides a preAssemblingMatrixRHS() method. However, it is not enough in the
        * case of hyperelasticity: as Petsc's SNES is in charge of the Newton Raphson's loop, I must respect its interface
        * and assembling is done at Petsc's call of the function given to SNESSetFunction().
        * I deliberately chose a very different method name to avoid any ambiguity.
        */
      void preSNESAssemble();

      /*!
       * \brief Additional operations for preSNESAssemble() specific to derived problem.
       */
      virtual void preSNESAssembleAdditional() {};

      /**
       * @brief solve the linear solver with Petsc methods.
       */
      void buildSolver();
      
      /**
       * @brief Solve a ksp linear problem.
       */
      void solve(int rank, int size, bool new_matrix = true);

      /**
       * @brief Solve with ksp a system with the same matrix as in the previous solve
       */
      void solveWithSameMatrix();

      /*!
        * \brief Solve a snes linear system.
        *
        * A SNES linear system is an iterative system that uses Petsc's Newton-Raphson method.
        *
        * \param[in] do_apply_natural Whether natural boundaries conditions are applied.
        * ApplyNaturalBoundaryConditions::yes in most cases, but might be no for instance in dynamic systems where
        * these conditions appear explicitly only on the very first iteration.
        * \param[in] flag_matrix_rhs Specify on which element the assembling is done.
        */
      void iterativeSolve(int rank, int size, ApplyNaturalBoundaryConditionsType do_apply_natural = ApplyNaturalBoundaryConditions::yes, FlagMatrixRHS flag_matrix_rhs = FlagMatrixRHS::matrix_and_rhs);
      
      /**
       * @brief Initializes the solver before the non-linear solve.
       */
      virtual void initNonLinearIteration() {};

      /**
       * @brief Finalizes the solver before the non-linear solve.
       */
      virtual void endNonLinearIteration() {};

      /**
       * @brief Initializes the solver for the current time step
       */
      virtual void initIteration() {};

      /**
       * @brief Finalizes the solver for the current time step
       */
      virtual void endIteration() {};

      /**
       * @brief Create and copy matrix and rhs withou boundary conditions
       */      
      void createAndCopyMatrixRHSWithoutBC(FlagMatrixRHS flag = FlagMatrixRHS::matrix_and_rhs);

      /**
       * @brief Compute the residual
       */
      void computeResidual();



      //Vector and Matrix operators
      //===========================

      /**
       * @brief Add matrix and rhs
       */      
      virtual inline void addMatrixRHS() { matrix(0).axpy(1,matrix(1),SAME_NONZERO_PATTERN); }
      
      /**
       * @brief Scales the LHS matrix by a constant.
       */
      virtual void addScaleMatrix(double /*coef*/ = 1.0) {};

      /**
       * @brief Scales the RHS vector by a constant.
       */
      virtual void addScaleRHS(double /*coef*/ = 1.0) {};

      virtual inline void copyMatrixRHS() {};

      virtual inline void inverseMatrix() {};
      
      virtual inline void solveExplicit() {};

      /*!
        * \brief Clear Matrix and/or RHS
        *
        * \param[in] flagMatrixRHS If 0, clear both matrix and RHS. If 1, only matrix, if 2, only RHS
        */
      void clearMatrixRHS(const FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);

      /*!
        * \brief Clear the ith matrix.
        * \param[in] i Matrix index
        */
      void clearMatrix(std::size_t i = 0);

      /*!
        * \brief Clear the ith std::vector.
        * \param[in] i Vector index
        */
      void clearVector(std::size_t i = 0);

      /**
       * @brief This method provides the solution vector 
       */
      inline void gatherSolution() { gatherVec(m_sol, m_seqSol); }

      /**
       * @brief This method provides the NL iteration evaluation state vector 
       */
      inline void gatherEvaluationState() { gatherVec(m_evaluationState, m_seqEvaluationState); };
      
      // We should stop using this function and prefer directly gatherVec (petscVector.hpp, petscVector_friend.hpp).
      // This function is left to avoid compatibility issues.
      inline void gatherVector(PetscVector& v, PetscVector& seqV) { gatherVec(v,seqV); } // TODO remove
      
      virtual void gatherVectorBeforeAssembleMatrixRHS() {};

      void getSolution(double* & solution, int sizeProc, int numProc);
      
      void setSolution(double* solution, int sizeProc, int numProc);
      
      void getSolutionUnknown(PhysicalVariable unknown, PetscVector& v);

      void getVector(double* & value, PetscVector&  seqVec, int sizeProc, int numProc);

      /**
       * @brief This method gets the coodinate of support dof associate to the dof.
       */      
      void findCoordinateWithIdDof(felInt i, Point& pt);

      /*!
        * \brief Evaluates function f(x,y,z) on dof
        * \param[in] func   Functor to compute the values
        * \param[in] array  Array of values
        */
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Evaluates function f(x,y,z)  on dof
        * \param[in] func       Functor to compute the values
        * \param[in] array      Array of values
        * \param[in] extraData  Extra data
        */            
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array, std::vector<double>& extraData); // TODO is linearProblem a good place for this function?
      
      /*!
        * \brief Evaluates function f(x,y,z)  on dof
        * \param[in] func       Functor to compute the values
        * \param[in] array      Array of values
        * \param[in] extraData  Extra data
        */            
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func, std::vector<int>& array, std::vector<double>& extraData); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Evaluates function f(x,y,z,t)  on dof
        * \param[in] func   Functor to compute the values
        * \param[in] time   Time
        * \param[in] array  Array of values
        */      
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func,double time, std::vector<double>& array); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Evaluates function f(x,y,z,t) on dof
        * \param[in] func       Functor to compute the values
        * \param[in] array      Array of values
        * \param[in] extraData  Extra data
        * \param[in] time       Time
        */    
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array, std::vector<double>& extraData,double time); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Evaluates function f(x,y,z,t) on dof
        * \param[in] func       Functor to compute the values
        * \param[in] time       Time
        * \param[in] array      Array of values
        * \param[in] extraData  Extra data
        */     
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func, double time, std::vector<double>& array, std::vector<double>& extraData); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Evaluates function f(x,y,z) on dof
        * \param[in] func   Functor to compute the values
        * \param[in] iComp  I-th component
        * \param[in] array  Array of values
        */       
      template <class Templ_functor> 
      void evalFunctionOnDof(const Templ_functor& func, int iComp, std::vector<double>& array); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Evaluates function f(x,y,z)  on dof
        * \param[in] func   Functor to compute the values
        * \param[in] array  Array of values
        */       
      template <class Templ_functor> 
      void evalFunctionWithCompOnDof(const Templ_functor& func, std::vector<double>& array); // TODO is linearProblem a good place for this function?

      /*!
        * \brief Set values in m_seqSol and m_sol
        * \param[in] functorXYZ Functor to compute the values
        * \param[in] iVariable  Variable ID
        * \param[in] iComponent Component ID
        */
      template <class FunctorXYZ>
      inline void set_U_0(const FunctorXYZ& functorXYZ, int iVariable, int iComponent);

      /*!
        * \brief Set values in m_seqSol and m_sol
        * \param[in] functorXYZ Functor to compute the values
        * \param[in] iVariable  Variable ID
        * \param[in] iComponent Component ID
        */
      template <class FunctorXYZ>
      inline void set_U_0_parallel(const FunctorXYZ& functorXYZ, int iUnknown, int iComponent);



      // Methods for auxiliary vectors
      //==============================

      /**
       * @brief Initialize vectors
       */        
      void initPetscVectors();



      // Virtual methods widely used in derived linear problems
      //=======================================================
      virtual void updateExtrapol(PetscVector& /*V_1*/) {};
      
      virtual void initExtrapol(PetscVector& /*V_1*/) {};
      
      virtual void initializeTimeScheme(Bdf* /*bdf*/) {};
      
      virtual void readData(IO& /*io*/) {};
      
      virtual void readData(IO& /*io*/, double /*iteration*/) {};

    ///@}
    ///@name Access
    ///@{

      /**
       * @brief Returns the dimension of the problem (constant version)
       */
      inline int dimension() const { return m_dimesion; }

      /**
       * @brief Returns the verbosity level (constant version)
       */
      inline const int& verbosity() const { return m_verbosity; }

      /**
       * @brief Returns the list of variables (constant version)
       */
      inline const ListVariable& listVariable() const { return m_listVariable; }

      /**
       * @brief Returns the list of variables
       */  
      inline ListVariable& listVariable() { return m_listVariable; }
      
      /**
       * @brief Returns the list of unknowns (constant version)
       */
      inline const ListUnknown& listUnknown() const { return m_listUnknown; }

      /**
       * @brief Returns the list of unknowns
       */   
      inline ListUnknown& listUnknown() { return m_listUnknown; }

      /**
       * @brief Returns the i-th linear problem mesh (constant version)
       */
      inline const GeometricMeshRegion::Pointer mesh(int id) const { return m_mesh[id]; }
      
      /**
       * @brief Returns the i-th linear problem mesh
       */
      inline GeometricMeshRegion::Pointer mesh(int id) { return m_mesh[id]; }

      /**
       * @brief Returns the m_currentMesh linear problem mesh (constant version)
       */
      inline const GeometricMeshRegion::Pointer mesh() const { return m_mesh[m_currentMesh]; }
      
      /**
       * @brief Returns the m_currentMesh linear problem mesh
       */
      inline GeometricMeshRegion::Pointer mesh() { return m_mesh[m_currentMesh]; }

      /**
       * @brief Returns the linear problem mesh vector (constant version)
       */
      inline const std::vector<GeometricMeshRegion::Pointer> & listMesh() const { return m_mesh; }
      
      /**
       * @brief Returns the linear problem mesh vector
       */
      inline std::vector<GeometricMeshRegion::Pointer> & listMesh() { return m_mesh; }

      /**
       * @brief Returns the linear problem local mesh (constant version)
       */
      inline const GeometricMeshRegion::Pointer meshLocal(int id) const { return m_meshLocal[id]; }
      
      /**
       * @brief Returns the linear problem local mesh
       */
      inline GeometricMeshRegion::Pointer meshLocal(int id) { return m_meshLocal[id]; }

      /**
       * @brief Returns the m_currentMesh linear problem local mesh (constant version)
       */
      inline const GeometricMeshRegion::Pointer meshLocal() const { return m_meshLocal[m_currentMesh]; }
      
      /**
       * @brief Returns the m_currentMesh linear problem local mesh
       */
      inline GeometricMeshRegion::Pointer meshLocal() { return m_meshLocal[m_currentMesh]; }
      
      /**
       * @brief Returns the linear problem local mesh (constant version)
       */
      inline const std::vector<GeometricMeshRegion::Pointer>& listMeshLocal() const { return m_meshLocal; }

      /**
       * @brief Returns the linear problem local mesh
       */
      inline std::vector<GeometricMeshRegion::Pointer>& listMeshLocal() { return m_meshLocal; }

      /**
       * @brief Returns the DoF object associated to the unknown (constant version).
       */
      inline const Dof& dof() const { return m_dof; }
      
      /**
       * @brief Returns the DoF object associated to the unknown.
       */
      inline Dof& dof() { return m_dof; }
      
      /**
       * @brief Returns the dof partition vector (constant version)
       */
      inline const std::vector<int>& dofPartition() const { return m_dofPart; }
     
      /**
       * @brief Returns the dof partition vector
       */ 
      inline std::vector<int>& dofPartition(){ return m_dofPart; }

      /**
       * @brief It returns the number of dofs(constant version)
       */
      inline const felInt& numDof() const { return m_numDof; }
      
      /**
       * @brief It returns the number of dofs
       */
      inline felInt& numDof() { return m_numDof; }

      /**
       * @brief It returns the number of dofs (local-constant version).
       */
      inline const felInt& numDofLocal() const { return m_numDofLocal; }
      
      /**
       * @brief It returns the number of dofs (local).
       */
      inline felInt& numDofLocal() { return m_numDofLocal; }

      /**
       * @brief It returns the number of dofs associated to the unknown. (constant version)
       * \param[in] i The ID of the unknown
       */
      inline const felInt& numDofPerUnknown(felInt i) const { return m_numDofUnknown[i]; }
      
      /**
       * @brief It returns the number of dofs associated to the unknown.
       * \param[in] i The ID of the unknown
       */
      inline felInt& numDofPerUnknown(felInt i) { return m_numDofUnknown[i]; }

      /**
       * @brief It returns the number of dofs associated to the unknown. (constant version)
       * \param[in] i The ID of the unknown
       */      
      inline felInt numDofPerUnknown(PhysicalVariable unknown) const { return m_numDofUnknown[m_listUnknown.getUnknownIdList(unknown)]; }

      /**
       * @brief It returns the number of dofs associated to the unknown.
       * \param[in] i The ID of the unknown
       */      
      inline felInt& numDofPerUnknown(PhysicalVariable unknown) { return m_numDofUnknown[m_listUnknown.getUnknownIdList(unknown)]; }

      /**
       * @brief It returns the local number of dofs associated to the unknown. (constant version)
       * \param[in] i The ID of the unknown
       */    
      inline felInt numDofLocalPerUnknown(PhysicalVariable unknown) const  { return m_numDofLocalUnknown[m_listUnknown.getUnknownIdList(unknown)]; };

      /**
       * @brief It returns the local number of dofs associated to the unknown. (constant version)
       * \param[in] i The ID of the unknown
       */    
      inline felInt& numDofLocalPerUnknown(PhysicalVariable unknown) { return m_numDofLocalUnknown[m_listUnknown.getUnknownIdList(unknown)]; };

      /**
       * @brief Returns the vector of supportDof (constant version)
       */
      inline const std::vector<SupportDofMesh>& supportDofUnknown() const { return m_supportDofUnknown; }

      /**
       * @brief Returns the vector of supportDof
       */
      inline std::vector<SupportDofMesh>& supportDofUnknown() { return m_supportDofUnknown; }

      /**
       * @brief Returns the supportDof related to the given unknown (constant version)
       * \param[in] iunknown The ID of the unknown
       */  
      inline const SupportDofMesh& supportDofUnknown(int iunknown) const { return m_supportDofUnknown[iunknown]; }

      /**
       * @brief Returns the supportDof related to the given unknown (constant version)
       * \param[in] iunknown The ID of the unknown
       */   
      inline SupportDofMesh& supportDofUnknown(int iunknown) { return m_supportDofUnknown[iunknown]; }

      /**
       * @brief Returns the vector of supportDofLocal (constant version)
       */
      inline const std::vector<SupportDofMesh>& supportDofUnknownLocal() const { return m_supportDofUnknownLocal; }
      
      /**
       * @brief Returns the vector of supportDofLocal
       */
      inline std::vector<SupportDofMesh>& supportDofUnknownLocal() { return m_supportDofUnknownLocal; }

      /**
       * @brief Returns the supportDofLocal related to the given unknown (constant version)
       * \param[in] iunknown The ID of the unknown
       */  
      inline const SupportDofMesh& supportDofUnknownLocal(int iunknown) const { return m_supportDofUnknownLocal[iunknown]; }

      /**
       * @brief Returns the supportDofLocal related to the given unknown (constant version)
       * \param[in] iunknown The ID of the unknown
       */   
      inline SupportDofMesh& supportDofUnknownLocal(int iunknown) { return m_supportDofUnknownLocal[iunknown]; }

      /**
       * @brief Returns the flag to know if m_ao is allocated (constant version)
       */  
      inline bool isAOAllocated() const { return m_initAO; }

      /**
       * @brief Returns the flag to know if m_ao is allocated
       */  
      inline bool& isAOAllocated() { return m_initAO; }

      /**
       * @brief Returns the AO object (constant version)
       */
      inline const AO& ao() const { return m_ao; }

      /**
       * @brief Returns the AO object
       */  
      inline AO& ao() { return m_ao; }
      
      /**
       * @brief Returns the flag to know if m_mappingNodes is allocated (constant version)
       */  
      inline bool isMappingNodesAllocated() const { return m_initMappingNodes; }

      /**
       * @brief Returns the flag to know if m_mappingNodes is allocated
       */  
      inline bool& isMappingNodesAllocated() { return m_initMappingNodes; }

      /**
       * @brief Returns the local to global mapping of the dof (constant version)
       */
      inline const ISLocalToGlobalMapping& mappingNodes() const { return m_mappingNodes; }
      
      /**
       * @brief Returns the local to global mapping of the dof
       */  
      inline ISLocalToGlobalMapping& mappingNodes() { return m_mappingNodes; }
      
      /**
       * @brief Returns the flag to know if m_initMappingElem is allocated (constant version)
       */    
      inline bool isMappingElemAllocated() const { return m_initMappingElem; }

      /**
       * @brief Returns the flag to know if m_initMappingElem is allocated
       */    
      inline bool& isMappingElemAllocated() { return m_initMappingElem; }

      /**
       * @brief Returns the local to global mapping of the elements (constant version)
       */
      inline const ISLocalToGlobalMapping& mappingElem() const { return m_mappingElem[m_currentMesh]; }

      /**
       * @brief Returns the local to global mapping of the elements
       */  
      inline ISLocalToGlobalMapping& mappingElem() { return m_mappingElem[m_currentMesh]; }

      /**
       * @brief Returns the local to global mapping of the elements (constant version)
       */
      inline const ISLocalToGlobalMapping& mappingElem(int id) const { return m_mappingElem[id]; }

      /**
       * @brief Returns the local to global mapping of the elements
       */  
      inline ISLocalToGlobalMapping& mappingElem(int id) { return m_mappingElem[id]; }

      /**
       * @brief Returns the flag to know if m_initMappingElemSupport is allocated (constant version)
       */  
      inline bool isMappingElemSupportAllocated() const { return m_initMappingElemSupport; }

      /**
       * @brief Returns the flag to know if m_initMappingElemSupport is allocated
       */  
      inline bool& isMappingElemSupportAllocated() { return m_initMappingElemSupport; }

      /**
       * @brief Returns the local to global mapping of the supportDof elements for the given unknown (constant version)
       * \param[in] iunknown The ID of the unknown
       */
      inline const ISLocalToGlobalMapping* mappingElemSupportPerUnknown(int iUnknown) const { return m_mappingElemSupportPerUnknown[iUnknown]; }

      /**
       * @brief Returns the local to global mapping of the supportDof elements for the given unknown
       * \param[in] iunknown The ID of the unknown
       */
      inline ISLocalToGlobalMapping* mappingElemSupportPerUnknown(int iUnknown) { return m_mappingElemSupportPerUnknown[iUnknown]; }

      /**
       * @brief Returns the flag to know if m_initMappingLocalFelisceToGlobalPetsc is allocated (constant version)
       */  
      inline bool isMappingLocalFelisceToGlobalPetscAllocated() const { return m_initMappingLocalFelisceToGlobalPetsc; }

      /**
       * @brief Returns the flag to know if m_initMappingLocalFelisceToGlobalPetsc is allocated
       */  
      inline bool& isMappingLocalFelisceToGlobalPetscAllocated() { return m_initMappingLocalFelisceToGlobalPetsc; }

      /**
       * @brief Returns the local id dof per unknown to the global id in PETSc ordering (constant version)
       * \param[in] iunknown The ID of the unknown
       */
      inline const ISLocalToGlobalMapping& mappingIdFelisceToPetsc(int iUnknown) const { return m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown]; }

      /**
       * @brief Returns the local id dof per unknown to the global id in PETSc ordering
       * \param[in] iunknown The ID of the unknown
       */
      inline ISLocalToGlobalMapping& mappingIdFelisceToPetsc(int iUnknown) { return m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown]; }

      /**
       * @brief Returns the local id dof per unknown to the global id in PETSc ordering (constant version)
       * \param[in] iunknown The ID of the unknown
       */
      inline const ISLocalToGlobalMapping& mappingDofLocalToDofGlobal(PhysicalVariable unknown) const { return m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[m_listUnknown.getUnknownIdList(unknown)]; }

      /**
       * @brief Returns the local id dof per unknown to the global id in PETSc ordering
       * \param[in] iunknown The ID of the unknown
       */
      inline ISLocalToGlobalMapping& mappingDofLocalToDofGlobal(PhysicalVariable unknown) { return m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[m_listUnknown.getUnknownIdList(unknown)]; }

      /**
       * @brief Returns the flag to know if the system has been built (constant version)
       */  
      inline bool areSolutionAndRHSAllocated() const { return m_buildSystem; } // TODO change name

      /**
       * @brief Returns the flag to know if the system has been built
       */  
      inline bool& areSolutionAndRHSAllocated() { return m_buildSystem; } // TODO change name

      /**
       * @brief Returns the number of matrices allocated
       */  
      inline std::size_t numberOfMatrices() const { return m_matrices.size(); }

      /**
       * @brief Returns the vector of matrices
       */  
      inline std::vector<PetscMatrix> & matrices() { return m_matrices; }

      /**
       * @brief Returns the i-th matrix (constant version)
       * \param[in] i Matrix index number
       */
      inline PetscMatrix const& matrix(const std::size_t index = 0) const { return m_matrices[index]; }

      /**
       * @brief Returns the i-th matrix
       * \param[in] i Matrix index number
       */
      inline PetscMatrix& matrix(const std::size_t index = 0) { return m_matrices[index]; }
      
      /**
       * @brief Returns the number of rhs allocated
       */  
      inline std::size_t numberOfVectors() const { return m_vectors.size(); }

      /**
       * @brief Returns the vector of rhs
       */  
      inline std::vector<PetscVector> & vectors() { return m_vectors; }

      /**
       * @brief Returns the i-th rhs (constant version)
       * \param[in] i Rhs index number
       */
      inline const PetscVector& vector(const std::size_t index = 0) const { return m_vectors[index]; }

      /**
       * @brief Returns the i-th rhs
       * \param[in] i Rhs index number
       */  
      inline PetscVector& vector(const std::size_t index = 0) { return m_vectors[index]; }

      /**
       * @brief Returns the local solution (constant version)
       */
      inline const PetscVector& solution() const { return m_sol; }

      /**
       * @brief Returns the local solution
       */ 
      inline PetscVector& solution() { return m_sol; }
     
      /**
       * @brief Returns the flag to know if the sequential solution is allocated
       */ 
      inline bool isSequentialSolutionAllocated() const { return m_seqSolAllocated; } 

      /**
       * @brief Returns the flag to know if the sequential solution is allocated
       */ 
      inline bool& isSequentialSolutionAllocated() { return m_seqSolAllocated; }

      /**
       * @brief Returns the sequential solution (constant version)
       */
      inline const PetscVector& sequentialSolution() const { return m_seqSol; }

      /**
       * @brief Returns the sequential solution
       */ 
      inline PetscVector & sequentialSolution() { return m_seqSol; }

      /**
       * @brief Returns the linear problem local mesh (constant version)
       */
      inline std::size_t currentMesh() const { return m_currentMesh; }

      /**
       * @brief Returns the linear problem local mesh
       */
      inline std::size_t& currentMesh() { return m_currentMesh; }

      /**
       * @brief Returns the list of boundary conditions (constant version)
       */
      inline const BoundaryConditionList& getBoundaryConditionList() const { return m_boundaryConditionList; }

      /**
       * @brief Returns the list of boundary conditions
       */  
      inline BoundaryConditionList& getBoundaryConditionList() { return m_boundaryConditionList; }

      /**
       * @brief Returns the KSPInterface object
       */  
      inline KSPInterface& kspInterface() { return *m_KSPInterface; }

      /**
       * @brief Returns the SNESInterface object
       */  
      inline SNESInterface& snesInterface() {return *m_pSNESInterface;}

      /**
       * @brief Returns m_evaluationState (constant version)
       */
      inline const PetscVector& evaluationState() const { return m_evaluationState; }

      /**
       * @brief Returns m_evaluationState
       */
      inline PetscVector& evaluationState() { return m_evaluationState; }

      /**
       * @brief Returns m_seqEvaluationState (constant version)
       */
      inline const PetscVector & seqEvaluationState() const { return m_seqEvaluationState; }

      /**
       * @brief Returns m_seqEvaluationState
       */  
      inline PetscVector & seqEvaluationState() { return m_seqEvaluationState; }

      /**
       * @brief Returns the linearized flag (constant version)
       */ 
      inline bool linearizedFlag() const { return m_linearizedFlag; }

      /**
       * @brief Returns the linearized flag
       */ 
      inline bool& linearizedFlag() { return m_linearizedFlag; }

      /**
       * @brief Returns the matrix without boundary condition
       */
      inline PetscMatrix& matrixWithoutBC() { return m_matrixWithoutBC; }
      
      /**
       * @brief Returns the rhs without boundary condition
       */
      inline PetscVector& rhsWithoutBC() { return m_rhsWithoutBC; }
      
      /**
       * @brief Returns the residual
       */
      inline const PetscVector& residual() const { return m_residual; }

      /**
       * @brief Returns the sequential residual
       */
      inline const PetscVector& seqResidual() const { return m_seqResidual; }

      /**
       * @brief Returns the static term flag (constant version)
       */ 
      inline bool computeStaticTerm() const { return m_computeStaticTerm; }

      /**
       * @brief Returns the static term flag
       */ 
      inline bool& computeStaticTerm() { return m_computeStaticTerm; }

      /**
       * @brief Returns the i-th extenal vector
       * \param[in] i External vector index
       */ 
      inline PetscVector& externalVec(std::size_t i = 0) { return *m_externalVec[i]; }

      /**
       * @brief Returns the i-th extenal vector ordering
       * \param[in] i External vector index
       */ 
      inline AO& externalAO(std::size_t i = 0) { return m_externalAO[i]; }

      /**
       * @brief Push back external vector
       * \param[in] extVec The vector
       */ 
      inline void pushBackExternalVec(PetscVector& extVec) { m_externalVec.push_back(&extVec); }

      /**
       * @brief Push back external AO object
       * \param[in] extAO The vector
       */ 
      inline void pushBackExternalAO(AO extAO) { m_externalAO.push_back(extAO); }

      /**
       * @brief Push back external Dof object 
       * \param[in] extDof The vector
       */ 
      inline void pushBackExternalDof(Dof& extDof) { m_externalDof.push_back(&extDof); }

      /**
       * @brief Gather auxiliary vectors by name 
       * \param[in] name The vector name
       */ 
      inline void gather(std::string name) { gatherVec( m_vecs.Get(name), m_seqVecs.Get(name)); }
    
      /*! \brief Get an auxiliary vector by name 
       * \param[in] type Sequential or parallel
       * \param[in] name String defining the name of the vector
       * \return the petscVector
        */
      inline PetscVector& get(vectorType type, std::string name)
      {
        if      ( type == sequential ) { if ( m_seqVecs.count(name) > 0 ) return m_seqVecs[name]; } 
        else if ( type == parallel   ) { if ( m_vecs.count(name) > 0    ) return m_vecs[name]; }
            
        FEL_ERROR( "The std::vector " + name + " that you are looking for was not initialized." );
        return m_vecs[name];
      }

      /**
       * @brief CVGraphInterface (constant version)
       */ 
      inline const CVGraphInterface::Pointer cvgraphInterf() const { return m_cvgraphInterf; }

      /**
       * @brief CVGraphInterface
       */ 
      inline CVGraphInterface::Pointer cvgraphInterf() { return m_cvgraphInterf; }

      /**
       * @brief Returns the name of the linear problem
       */ 
      inline std::string name() const { return m_name; }

      /**
       * @brief Returns the pointer the to model owning this linear problem (constant version)
       */ 
      inline Model* model() const { return mOwnerModel; }

      /**
       * @brief Returns the pointer the to model owning this linear problem
       */ 
      inline Model*& model() { return mOwnerModel; }

      /**
       * @brief Returns the index if the parameter class attached to the model owning this linear problem (constant version)
       */       
      std::size_t instanceIndex() const; // { return mOwnerModel->instanceIndex(); }

      /**
       * @brief Returns the index if the parameter class attached to the model owning this linear problem
       */ 
      std::size_t& instanceIndex(); // { return mOwnerModel->instanceIndex(); }

      /**
       * @brief Returns the ID of the linear problem(constant version)
       */ 
      inline std::size_t identifierProblem() const { return m_identifier_problem; }

      /**
       * @brief Returns the ID of the linear problem
       */ 
      inline std::size_t& identifierProblem() { return m_identifier_problem; }

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{


      //Write on files
      //==============

      /// Print solution of the system solve.
      void printSolution(int verbose = 0, std::ostream& outstr = std::cout) const; // TODO move in a more suitable class

      /// Write in file matrix.m, matrix in matlab format.
      void writeMatrixForMatlab(int verbose = 0, const std::string& filename = "matrix") const; // TODO move in a more suitable class

      /// Write in file RHS.m, RHS in matlab format.
      void writeRHSForMatlab(int verbose = 0, const std::string& filename = "rhs") const; // TODO move in a more suitable class

      // Write in file sol.m, solution in matlab format.
      void writeSolForMatlab(int verbose = 0, const std::string& filename = "solution") const; // TODO move in a more suitable class

      /// Write in file filename, matrix in matlab format.
      void writeMatrixForMatlab(std::string const& fileName, PetscMatrix& matrix) const; // TODO move in a more suitable class

      /// Write in file filenam, std::vector in matlab format.
      void writeVectorForMatlab(std::string const& fileName, PetscVector& vector) const; // TODO move in a more suitable class

      /// Write in file matrix.mb, matrix in matlab format.
      void writeMatrixForMatlabBinary(int verbose = 0, const std::string& filename = "matrix") const; // TODO move in a more suitable class

      /// Write in file RHS.mb, RHS in matlab format.
      void writeRHSForMatlabBinary(int verbose = 0, const std::string& filename = "rhs") const; // TODO move in a more suitable class

      /// Write in file sol.mb, solution in matlab format.
      void writeSolForMatlabBinary(int verbose = 0, const std::string& filename = "solution") const; // TODO move in a more suitable class

      /// Write in file filename, matrix in matlab format.
      void writeMatrixForMatlabBinary(std::string const& fileName, PetscMatrix& matrix) const; // TODO move in a more suitable class

      /// Write in file filenam, std::vector in matlab format.
      void writeVectorForMatlabBinary(std::string const& fileName, PetscVector& vector) const; // TODO move in a more suitable class

      /// Write solution from vector.
      void writeSolutionFromVec(PetscVector& v, int rank, std::vector<IO::Pointer>& io, double& time, int iteration, std::string prefix); // TODO move in a more suitable class


      //Write solution
      //==============

      virtual void writeSolution(int rank, std::vector<IO::Pointer>& io, double& time, int iteration);
      
      void writeSolutionBackup(const ListVariable& listVar, int rank, std::vector<IO::Pointer>& io, double& time, int iteration);
      
      void fromVecToDoubleStar(double* solution, const PetscVector& sol, int rank, int dimension, felInt size=0);
      
      void fromDoubleStarToVec(double* solution, PetscVector* sol, felInt size);
      
      virtual void writeSupportVariableBackup(std::string name, double* supportVariable, int size, int dimension, int rank, IO::Pointer io, double& time, int iteration, bool manageMemory=false);


      //Write mesh to read solution with specific mesh (P2, Q2,...)
      //===========================================================

      void writeGeoForUnknown(int iUnknown, std::string nameOfGeoFile="");
      
      void writeGeoForUnknownEnsightGold(int iUnknown, std::map<felInt, std::vector<felInt> >& refToListOfIds, std::string nameOfGeoFile="");

    ///@}
    ///@name Operation that should be declared as friends function
    ///@{

      /// Remove average on solution.
      void removeAverageFromSolution(PhysicalVariable unknown, int rankProc);

      /// Remove average on solution. Every processors do the same work at the moment (in progress).
      void removeAverageFromSolutionCurv(PhysicalVariable unknown, int rankProc);
      
      void removeMaxSol(PhysicalVariable unknown, double valMax, int rankProc);
      
      //Compute mean quantities or flux on a given label lists.
      // If PhysicalVariable has more than ncoor components -> compute Flux (typically for the velocity)
      // If PhysicalVariable is a scalar (num component = 1) -> compute Mean (typically for the Pressure)
      // --Version specific for the solution
      void computeMeanQuantity(PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& MeanQuantity);
      
      //Compute mean quantities or flux on a given label lists.
      // If PhysicalVariable has more than ncoor components -> compute Flux (typically for the velocity)
      // If PhysicalVariable is a scalar (num component = 1) -> compute Mean (typically for the Pressure)
      // --Generic version for any petscVec
      void computeMeanQuantity(PetscVector& seqVec, PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& meanQuantity);
      
      //Compute mean quantities or flux on a given label lists.
      // If PhysicalVariable has more than ncoor components -> compute Flux (typically for the velocity)
      // If PhysicalVariable is a scalar (num component = 1) -> compute Mean (typically for the Pressure)
      // --Generic version for any petscVec
      void computeMeanExternalQuantity(PetscVector& seqVec, AO externalAO, PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& meanQuantity);
      
      //Compute mean quantities for each component on a given label lists.
      void computeMeanQuantityDomain(PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& MeanQuantity, std::vector<double>& measure);
      
      //Compute the measure of a boundary part
      void computeMeasure(const std::vector<int>& label, std::vector<double>& measure);
      
      //Compute the measure and the averaged normal of a boundary part
      void computeMeasureNormal(const std::vector<int>& label, std::vector<double>& measure, std::vector<double>& normal);
      
      // Compute L2 error between present solution unknown (u or p) and the reference solution fctExactSolOnDof, where the latter
      // is analytically computed on dof or read from a file (in this case, please sure that numDof(unknown) = size of fctExactSolOnDof - Cuc 10/10/13)
      void errorL2(PhysicalVariable unknown, std::vector<double>& fctExactSolOnDof, double& resultL2Norm);
      
      // Computing \int_\Omega u \dot u i.e. norm(u,2)^2 (unknown = u) - Gregory 25/09/12
      void computel2NormSquared(PhysicalVariable unknown, int rankProc, double& l2NormSquared);
      
      // Computing \int_\Omega \nabla u : \nabla u  where u = unknown
      // It's semi-norm(u,H1)^2 - Gregory 25/09/12
      void computeSemiNormH1Squared(PhysicalVariable unknown, int rankProc, double& semiNormH1Squared);
      
      // Computing \int_\Gamma \rho*0.5 (u \dot u) (u \dot n) where u = unknown - Gregory 25/09/12
      void computePowerBDu(PhysicalVariable unknown, int rankProc, const std::vector<int>& label, double density, std::vector<double>& PowerBDu);
      
      // Computing \int_\Gamma  (grad u \dot n) \cdot w, where u = unknown and w is given
      void computeConvBoundary(PhysicalVariable unknown, PetscVector& testFunction, int rankProc, const std::vector<int>& label, std::vector<double>& convBD);
      
      /* Given two std::vector functions, u and w, compute \int_{Gamma} \grad_u \cdot n \cdot w */
      double computeViscBoundary(int label, PetscVector& W);
      
      // Computing \int_\Gamma p (u \dot n) - Gregory 25/09/12
      // Typically unknow1 = p and unknow2 = u
      void computePowerBDpu(PhysicalVariable unknown1, PhysicalVariable unknown2, int rankProc, const std::vector<int>& label, std::vector<double>& PowerBDpu);
      
      // Computing \int_\Omega u(n) \dot u(n-1) - Gregory 25/09/12
      void computeProdUn_Un_1(PhysicalVariable unknown, int rankProc, Bdf& bdf, double& prodUn_Un_1);
      
      // Computing \int_\Omega (u(n)-u(n-1))^2 - Gregory 25/09/12
      void computeDiffSquaredUn_Un_1(PhysicalVariable unknown, int rankProc, Bdf& bdf, double& diffSquareUn_Un_1);
      
      // Computing numerical Temam's trick : \int_\Omega div(u(n-1)) \dot u(n) v
      void computeTemamTerm(PhysicalVariable unknown, int rankProc, Bdf& bdf, double& temamTerm);
      
      void computeBoundaryStress(int coeff, int label, std::vector<double>& stress);
      
      void computeDomainIntegralOfDivergence(PhysicalVariable unknown, const std::vector<int>& label, std::vector<double>& integralOfDivergence);

    ///@}
  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

      /// Spatial dimesion of the problem
      int m_dimesion = -1; 

      /// Verbosity level considered 
      int m_verbosity = 0;   

      /// Object to manage time splitting
      FelisceTransient::Pointer m_fstransient = nullptr;
      
      /// Object which contains variables of the linear system.
      ListVariable m_listVariable;

      /// Object which contains unknown of the linear system.
      ListUnknown m_listUnknown;

    ///@}
    ///@name Protected member Variables related to the meshes
    ///@{

      /// Set of meshes used by unknowns
      std::vector<std::size_t> m_meshUnknown;

      /// Vector of input meshes defining the domains of work.
      std::vector<GeometricMeshRegion::Pointer> m_mesh;

      /// Vector of the partitioned meshes
      std::vector<GeometricMeshRegion::Pointer> m_meshLocal;

    ///@}
    ///@name Protected member Variables related to Dof, SupportDof and their partition
    ///@{

      /// Object which manage degrees of freedom
      Dof m_dof; 

      /// Array which contains degree of freedom repartition (size = numDof, value = rank of the processor which keep in memory the dof).
      std::vector<int> m_dofPart; // TODO if it is realted to the dof. why is it here and not in the dof class?
      
      /// Array which contains element repartition (size = numDof, value = rank of the processor which keep in memory the element). //size = numElem..
      std::vector<std::vector<int> > m_eltPart; // TODO if it is realted to the dof. why is it here and not in the dof class?

      /// Number of dof in the problem
      felInt m_numDof = 0;

      /// Number of dof contained by the current processor
      felInt m_numDofLocal = 0;
      
      /// Number of dof for unknown
      std::vector<felInt> m_numDofUnknown;
      
      /// Number of dof local for unknown
      std::vector<felInt> m_numDofLocalUnknown;

      /// Global information about support of the degrees of freedom
      std::vector<SupportDofMesh>  m_supportDofUnknown;

      /// Boolean to know if m_supportDofUnknownOriginal is allocated
      bool m_areOriginalSupportDofMeshAllocated = false;

      /// Global information about the original support of the degrees of freedom
      std::vector<SupportDofMesh*> m_supportDofUnknownOriginal;

      /// Local information about support of the degrees of freedom
      std::vector<SupportDofMesh> m_supportDofUnknownLocal;
      
      /// Boolean to know if m_ao is allocated
      bool m_initAO = false;

      /// Mapping between global problem numbering and matrix dof numbering (specific to Petsc utilisation).
      AO m_ao; // TODO if it is realted to the dof why is here and not in the dof class?
      
      /// Boolean to know if m_mappingNodes is allocated
      bool m_initMappingNodes = false;

      /// Mapping between local to global ordering of support dof.
      ISLocalToGlobalMapping m_mappingNodes; // TODO  do we really need it? // TODO if it is realted to the dof. why is it here and not in the dof class?

      /// Boolean to know if m_mappingElem is allocated
      bool m_initMappingElem = false;

      /// Mapping between local to global ordering of element.
      std::vector<ISLocalToGlobalMapping> m_mappingElem; // TODO if it is realted to the mesh. why is it here and not in the mesh class?

      /// Boolean to know if m_mappingElemSupportPerUnknown is allocated
      bool m_initMappingElemSupport = false;

      /// Mapping between local to global ordering of element support
      std::vector<ISLocalToGlobalMapping*> m_mappingElemSupportPerUnknown; // TODO if it is realted to the supportDof. why is it here and not in the supportDof class?

      /// Boolean to know if m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc is allocated
      bool m_initMappingLocalFelisceToGlobalPetsc = false;

      /// Mapping between local to global ordering of element.
      std::vector<ISLocalToGlobalMapping> m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc; // TODO  do we really need it?

    ///@}
    ///@name Protected member Variables used for the linear system
    ///@{

      /// Boolean to know if the system (m_matrices, m_vectors...) is allocated
      bool m_buildSystem = false;

      /// Matrices used in formulation: the first one is the matrix used to compute the solution
      std::vector<PetscMatrix> m_matrices;

      /// Rhs used in formulation: the first one is the rhs used to compute the solution
      std::vector<PetscVector> m_vectors;
    
      /// Solution of the problem
      PetscVector m_sol;

      /// Boolean to know if the sequential solution is allocated
      bool m_seqSolAllocated = false;

      /// Sequential solution of the problem
      PetscVector m_seqSol;
    
    ///@}
    ///@name Protected member Variables for assembling the matrix
    ///@{

      /// ID of the current mesh used for assembling
      std::size_t m_currentMesh = 0;

      /// Label of the current element
      int m_currentLabel;

      /// List of finite element in the problem.
      ListCurrentFiniteElement m_listCurrentFiniteElement;

      /// List of curvilinear finite element in the problem.
      ListCurvilinearFiniteElement m_listCurvilinearFiniteElement;

      /// List of finite element with Bd in the problem.
      ListCurrentFiniteElementWithBd m_listCurrentFiniteElementWithBd;

      /// Element matrix
      std::vector<ElementMatrix::Pointer> m_elementMat;

      /// Element matrix transpose
      std::vector<ElementMatrix::Pointer> m_elementMatT;

      /// Element vector
      std::vector<ElementVector::Pointer> m_elementVector;

      /// Element matrix boundary element
      std::vector<ElementMatrix::Pointer> m_elementMatBD;

      /// Element matrix boundary element transpose
      std::vector<ElementMatrix::Pointer> m_elementMatTBD;

      /// Element vector boundary element
      std::vector<ElementVector::Pointer> m_elementVectorBD;

      /// Temporary array: mapping from local to global position of matrix/vector lines
      felInt* m_GposLine   = nullptr;

      /// Temporary array: mapping from local to global position of matrix columns
      felInt* m_GposColumn = nullptr;

    ///@}
    ///@name Protected member Variables for assembling the boundary condition
    ///@{

      /*    
        what if we use
        std::unordered_map<typeOfBC,std::vector<ElementField> > m_mapElemFieldBC
        such that m_mapElemFieldBC[ neumann ] ~ m_elemFieldNeumann?
        in this way the code would be more readable and we can loop over them and generalize functions that, 
        for now are huge cut and paste.
      */

      /// Object which contains all objects boundaryCondition to define boundaries conditions of the problem.
      BoundaryConditionList m_boundaryConditionList;

      /// Vector of elemField to apply Neumann boundary condition.
      std::vector<ElementField> m_elemFieldNeumann;

      /// Vector of elemField to apply Neumann Normal boundary condition.
      std::vector<ElementField> m_elemFieldNeumannNormal;

      /// Vector of elemField to apply Robin boundary condition.
      std::vector<ElementField> m_elemFieldRobin;

      /// Vector of elemField to apply Robin Normal boundary condition.
      std::vector<ElementField> m_elemFieldRobinNormal;

      /// Vector of elemField to apply EmbedFSI boundary condition.
      std::vector<ElementField> m_elemFieldEmbedFSI;

      /// Vector of elemFieldNormal to apply EmbedFSI boundary condition.
      std::vector<ElementField> m_elemFieldNormalForEmbedFSI;

      /// Vector of elemFieldAlpha to apply EmbedFSI boundary condition.
      std::vector<ElementField> m_elemFieldAlphaForEmbedFSI;

      /// Vector of elemField to apply backflow stabilization boundary condition.
      std::vector<ElementField> m_elemFieldBackflowStab;

      /// Boolean to know if m_idDofRings and m_idDofRingsSeq have been already computed
      bool m_ringsComputed = false;

      /// Id of the dofs belonging to the rings
      std::unordered_set<int> m_idDofRings;

      // Id of the dofs belonging to the rings seq vector
      std::unordered_set<int> m_idDofRingsSeq;

    ///@}
    ///@name Protected member Variables for solving the system of equation 
    ///@{

      /// Interface to PETSc linear solver (direct or iterative)
      KSPInterface::Pointer m_KSPInterface = felisce::make_shared<KSPInterface>();

      /// Interface to PETSc non linear solver (direct or iterative)
      SNESInterface::Pointer m_pSNESInterface = felisce::make_shared<SNESInterface>();

      /// Id of the solver if multiple solvers (default value = 0).
      int m_identifier_solver = 0; 

      /// Evaluation state of the residual of the problem (only useful in SNES method)
      PetscVector m_evaluationState;

      /// Sequential evaluation state of the residual of the problem (only useful in SNES method)
      PetscVector m_seqEvaluationState;

      /// Context for SNES solver 
      SnesContext m_snesContext;

      /// The flag that determines if the problem is linearized (used for FSI)
      bool m_linearizedFlag = false;

      /// Matrix without boundary condition
      PetscMatrix m_matrixWithoutBC;

      /// RHS without boundary condition
      PetscVector m_rhsWithoutBC;

      /// Parallel residual
      PetscVector m_residual;
      
      /// Sequential residual
      PetscVector m_seqResidual;

      /// The flag that determines whether compute or not the static term
      bool m_computeStaticTerm = false;
 
    ///@}
    ///@name Protected member Variables that point to external vectors (e.g. vectors defined in another linearProblem)
    ///@{   

      /// Pointer to external vectors 
      std::vector<PetscVector*> m_externalVec;

      /// Pointer to external vectors ordering 
      std::vector<AO> m_externalAO;

      /// Pointer to external vectors of dof objects 
      std::vector<Dof*> m_externalDof;

    ///@}
    ///@name Protected member Variables auxiliary maps of petscVector
    ///@{ 

      /// Map of petscVector
      Tools::mapHolder<std::string,PetscVector> m_vecs;

      /// Map of sequential petscVector
      Tools::mapHolder<std::string,PetscVector> m_seqVecs;

    ///@}
    ///@name Protected member Variables CVGraphInterface
    ///@{ 

      /// CVGraphInterface
      CVGraphInterface::Pointer m_cvgraphInterf;

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

      /**
        * \brief Set the number of matrixes
        */
      inline void setNumberOfMatrix(const std::size_t number) { m_matrices.resize(number); }

      /**
        * \brief Set the number of vectors.
        */
      inline void setNumberOfVector(const std::size_t number) { m_vectors.resize(number); }

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}
  private:
    ///@name Private static Member Variables
    ///@{

    ///@}
    ///@name Private member Variables
    ///@{

      /// Name of the current linear problem
      std::string m_name; 

      /// The Model owner of this linear problem
      Model* mOwnerModel;

      /// Id of the problem if multiple problem (default value = 0).
      std::size_t m_identifier_problem = 0;

    ///@}
    ///@name Private Operations
    ///@{

      /**
       * @brief Assembly loop when some support dof are duplicated
       * This function is called when some support dof are duplicated. It's only the loop over all the support
       * element.
       */
      void m_duplicateSupportDofAssemblyLoop(int rank, ElementType& eltType, felInt ielGeo, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, FlagMatrixRHS flagMatrixRHS);

      /**
       * @brief Assembly loop for boundary condition when some support dof are duplicated
       * This function is called when some support dof are duplicated. It's only the loop over all the support
       * element.
       */
      void m_duplicateSupportDofAssemblyLoopBoundaryCondition(int rank, ElementType& eltType, felInt ielGeoByType, std::vector<Point*>& elemPoint, std::vector<felInt>& elemIdPoint, FlagMatrixRHS flagMatrixRHS);

      /**
       * @brief Helper function for imposing essential boundary condition
       */
      template<DirichletApplicationOptions::Values>
      void applyEssentialBoundaryConditionHelper(FlagMatrixRHS flagMatrixRHS);
 
       /**
       * @brief Apply essential boundary condition via penalization
       */ 
      void applyEssentialBoundaryConditionViaPenalizationOnBC(FlagMatrixRHS flagMatrixRHS, const BoundaryCondition* const BC);

       /**
       * @brief Apply essential boundary condition via symmetric pseudo elimination
       */  
      void applyEssentialBoundaryConditionViaSymmetricPseudoEliminationOnBC(FlagMatrixRHS flagMatrixRHS, const BoundaryCondition* const BC, std::vector<felInt>& rows);

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
#ifdef FELISCE_WITH_CVGRAPH
  // Many functions and variables used with CVGraph that should be moved into CVGraphInterface
  public:
    ///@name Operations
    ///@{

      void setValueMatrixBD(felInt ielSupportDof); 
      void computeResidualOnDofs( PetscVector& residual, PetscVector& stressOut, PetscVector& seqStressOut, std::size_t iConn);
      void sendInitialCondition (std::size_t iConn);
      void applyOnlyCVGBoundaryCondition();
      void assembleRHSOnlyCVGraph();
      void prepareResidual(std::size_t iConn);
      void sumOnBoundaryCVGraph(PetscVector& v, PetscVector& b, const DofBoundary& dofBD);

      virtual void cvgAddExternalResidualToRHS();
      virtual void cvgraphFinalizeEssBC();
      
      virtual void readData() {};
      virtual void assembleMassBoundaryAndInitKSP( std::size_t /*iConn*/ ) { FEL_ERROR("You need to implement this function in this linear problem"); }
      virtual void cvgraphNaturalBC(felInt /*iel*/) {};

      template<class theClass>
      void assembleMassBoundaryAndInitKSP(  void (theClass::*functionOfTheLoop)( felInt ),
                                            const std::vector<felInt> & labels,
                                            void (theClass::*initPerElementType)(),
                                            void (theClass::*updateFunction)(const std::vector<Point*>&,const std::vector<int>&),
                                            std::size_t iBD);

      template<class theClass>
      void assembleMassBoundaryOnly(  void (theClass::*functionOfTheLoop)( felInt ),
                                      const std::vector<felInt> & labels,
                                      void (theClass::*initPerElementType)(),
                                      void (theClass::*updateFunction)(const std::vector<Point*>&,const std::vector<int>&),
                                      DofBoundary &dofBD, PetscMatrix&massMatrix);

    ///@}
    ///@name Access
    ///@{

      inline std::vector<DofBoundary>&  dofBDVec()     { return m_dofBD;     }
      inline std::vector<PetscMatrix>&  massBDVec()    { return m_massBD;    }
      inline std::vector<KSPInterface>& kspMassBDVec() { return m_kspMassBD; }
      inline DofBoundary& dofBD(std::size_t iBD) { return m_dofBD[iBD]; };
      inline cvgMainSlave*& slave() { return m_slave; }

    ///@}
  protected:
    ///@name Protected member Variables
    ///@{

      std::vector<felInt> m_globPosColumn;
      std::vector<felInt> m_globPosRow;
      std::vector<double> m_matrixValues;
      DofBoundary* m_auxiliaryDofBD = nullptr; ///< an auxiliary pointer to a DofBoundary to be used in setValueMatrixBD()

      std::vector<KSPInterface> m_kspMassBD;
      std::vector<PetscMatrix>  m_massBD;
      PetscVector m_stressOut, m_seqStressOut;
      PetscMatrix m_auxiliaryMatrix;

      std::string m_cvgDirichletVariable;

      std::vector<DofBoundary> m_dofBD = std::vector<DofBoundary>(1, DofBoundary());

    ///@}
  private:
    ///@name Private member Variables
    ///@{
    
      cvgMainSlave* m_slave = nullptr;

    ///@}
    ///@name Operations
    ///@{
    
      void sendZero(std::size_t iConn);

    ///@}
#endif
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/


#include "linearProblem_template.hpp"

#endif // LINEAR_PROBLEM_HPP
