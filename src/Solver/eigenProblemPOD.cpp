//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E.Schenone D. Lombardi
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes

// Project includes
#include "Solver/eigenProblemPOD.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementField.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTools.hpp"

namespace felisce 
{
  /*! Constructor
   */
  EigenProblemPOD::EigenProblemPOD():
    EigenProblem(),
    m_fePotTransMemb(nullptr),
    m_eigenValue(nullptr),
    m_dimRomBasis(0),
    m_beta(nullptr),
    m_mu(nullptr),
    m_gamma(nullptr),
    m_eta(nullptr),
    m_modeMean(nullptr),
    m_fiber(nullptr),
    m_source(nullptr),
    m_solutionInitialized(false),
    m_tensorCs(nullptr),
    m_tensorE(nullptr),
    m_tensorT(nullptr),
    m_tensorTs(nullptr),
    m_tensorY(nullptr)
  {}

  EigenProblemPOD::~EigenProblemPOD() {
    delete [] m_fePotTransMemb;
    delete [] m_eigenValue;
    for (int i=0; i<m_dimRomBasis; i++) {
      m_basis[i].destroy();
    }

    m_U_0.destroy();
    m_U_0_seq.destroy();
    m_W_0.destroy();
    if (m_solutionInitialized) {
      delete [] m_beta;
      delete [] m_mu;
      delete [] m_gamma;
      delete [] m_eta;
      delete [] m_modeMean;
    }
    if (m_fiber) {
      delete [] m_fiber;
    }
    m_autocorr.destroy();
  }

  void EigenProblemPOD::initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm) {
    EigenProblem::initialize(mesh, fstransient, comm);

    std::unordered_map<std::string, int> mapOfPbm;
    mapOfPbm["FKPP"] = 0;
    mapOfPbm["Monodomain"] = 1;
    mapOfPbm["Bidomain"] = 2;
    m_problem = mapOfPbm[FelisceParam::instance().model];

    m_dimRomBasis = FelisceParam::instance().dimRomBasis;
    m_numSource = FelisceParam::instance().numberOfSource;

    //    m_numSnapshot = FelisceParam::instance().numberOfSnapshot;
    //    m_samplingFreq = FelisceParam::instance().samplingFrequency;

    int id=0;
    m_idG = id;
    id++;
    m_idK = id;
    id++;
    if (FelisceParam::instance().hasInfarct) {
      m_idGs = id;
      id++;
    }
    m_numberOfMatrix = id;


    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;
    if (m_problem==1) {
      nameOfTheProblem = "Problem cardiac Monodomain";
      listVariable.push_back(potTransMemb);
      listNumComp.push_back(1);
      m_listUnknown.push_back(potTransMemb);
    }
    definePhysicalVariable(listVariable,listNumComp);

  }

  // Define Physical Variable associate to the problem
  void EigenProblemPOD::initPerElementType() {
    if (m_problem==1) {
      //Init pointer on Finite Element, Variable or idVariable
      m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
      m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
      m_elemFieldU0.initialize(DOF_FIELD,*m_fePotTransMemb);

      if (FelisceParam::instance().hasInfarct) {
        m_elemFieldFhNf0.initialize(DOF_FIELD,*m_fePotTransMemb);
      }

    }
  }



  // Assemble Matrix

  // mass matrix
  void EigenProblemPOD::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    if (m_problem==1) {
      m_fePotTransMemb->updateMeasQuadPt(0, elemPoint);
      m_fePotTransMemb->updateFirstDeriv(0, elemPoint);

      // Matrix[0] = phi_i * phi_j
      m_elementMat[m_idG]->phi_i_phi_j(1.,*m_fePotTransMemb,0,0,1);
      m_elementMat[m_idK]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor,*m_fePotTransMemb,0,0,1);

      if (FelisceParam::instance().hasInfarct) {
        // m_Matrix[] = s * phi_i * phi_j
        m_elemFieldFhNf0.setValue(m_FhNf0, *m_fePotTransMemb, iel, m_ipotTransMemb, m_ao, m_dof);
        m_elementMat[m_idGs]->a_phi_i_phi_j(1.,m_elemFieldFhNf0,*m_fePotTransMemb,0,0,1);
      }
    }

  }



  void EigenProblemPOD::readSnapshot(IO& io) {
    m_snapshot.resize(m_numSnapshot);

    for(int i=0; i<m_numSnapshot; i++) {

      double* snap = nullptr;
      snap = new double[m_mesh->numPoints()];
      double timeVar = 0.;

      m_Matrix[m_idG].getVecs(m_snapshot[i],nullPetscVector);
      m_snapshot[i].setFromOptions();

      PetscVector tmpSeq;
      tmpSeq.createSeq(PETSC_COMM_SELF,m_numDof);
      tmpSeq.setFromOptions();


      timeVar = i*m_samplingFreq*FelisceParam::instance().timeStep;
      io.readVariable(0, timeVar,snap, m_mesh->numPoints());
      felInt ia;
      for (felInt j=0; j< m_numDof; j++) {
        ia = j;
        AOApplicationToPetsc(m_ao,1,&ia);
        tmpSeq.setValues(1,&ia,&snap[j],INSERT_VALUES);
      }
      tmpSeq.assembly();
      fromDoubleStarToVec(snap,m_snapshot[i] , m_mesh->numPoints());

      tmpSeq.destroy();
      delete [] snap;
    }

  }



  void EigenProblemPOD::computeAutocorr() {
    m_autocorr.createAIJ(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, m_numSnapshot, m_numSnapshot,PETSC_DECIDE, nullptr, PETSC_DECIDE, nullptr);
    for(int i=0; i<m_numSnapshot; i++) {
      for(int j=0; j<=i; j++) {
        double prod = 0.0;
        scalarProduct(m_snapshot[i],m_snapshot[j],prod,true);
        m_autocorr.setValues(1,&i,1,&j,&prod,INSERT_VALUES);
        m_autocorr.setValues(1,&j,1,&i,&prod,INSERT_VALUES);
      }
    }

    m_autocorr.assembly(MAT_FINAL_ASSEMBLY);
  }



  // Visualize modes as a variable
  void EigenProblemPOD::writeMode(const int iter) {
    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);

    std::string fileName;
    if (iter > 0)
      fileName = "basis" + std::to_string(iter);
    else
      fileName = "basis";

    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];

    for (int iBasis=0; iBasis<m_dimRomBasis; iBasis++) {
      fromVecToDoubleStar(tmpSolToSave, m_basisPOD[iBasis], rankProc, 1);
      writeEnsightVector(tmpSolToSave, iBasis, fileName);
    }
    delete [] tmpSolToSave;

    if (rankProc == 0)
      writeEnsightCase(m_dimRomBasis, 1.,fileName);

  }





  void EigenProblemPOD::writeEnsightSolution(const int iter) {
    PetscVector solUe;
    solUe.duplicateFrom(m_U_0);

    if (iter > 0) {
      projectOnDof(m_beta,m_sol,m_dimRomBasis);
      if (FelisceParam::instance().model == "Bidomain") {
        std::cout << "Bidomain still to be defined with POD"<<std::endl;
      }
    } else {
      m_sol.copyFrom(m_U_0);
      if (FelisceParam::instance().model == "Bidomain") {
        std::cout << "Bidomain still to be defined with POD"<<std::endl;
      }
    }

    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);

    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];


    if ( (FelisceParam::instance().model == "FKPP") ) {
      fromVecToDoubleStar(tmpSolToSave, m_sol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "sol");
    }

    if ( (FelisceParam::instance().model == "Monodomain") || (FelisceParam::instance().model == "Bidomain") ) {
      fromVecToDoubleStar(tmpSolToSave, m_sol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "potTransMemb");
    }

    if (FelisceParam::instance().model == "Bidomain") {
      fromVecToDoubleStar(tmpSolToSave, solUe, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iter, "potExtraCell");
    }

    delete [] tmpSolToSave;

    if ( (FelisceParam::instance().model == "FKPP") ) {
      if (rankProc == 0)
        writeEnsightCase(iter+1, FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution,"sol", FelisceParam::instance().time);
    }

    if ( (FelisceParam::instance().model == "Monodomain") || (FelisceParam::instance().model == "Bidomain") ) {
      if (rankProc == 0)
        writeEnsightCase(iter+1, FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution,"potTransMemb", FelisceParam::instance().time);
    }

    if (FelisceParam::instance().model == "Bidomain") {
      std::vector<std::string> varNames;
      varNames.resize(2);
      varNames[0] = "potTransMemb";
      varNames[1] = "potExtraCell";
      if (rankProc == 0)
        writeEnsightCase(iter+1, FelisceParam::instance().timeStep * FelisceParam::instance().frequencyWriteSolution,varNames, FelisceParam::instance().time);
    }

  }




  // Solve eigenproblem to build rom basis
  void EigenProblemPOD::buildSolver() {
    if (FelisceParam::verbose()) PetscPrintf(PETSC_COMM_WORLD,"\nBuilding Slepc Solver.\n");

#ifdef FELISCE_WITH_SLEPC
    EPSSetOperators(m_eps,m_autocorr.toPetsc(),nullptr);
    EPSSetDimensions(m_eps,m_numSnapshot,PETSC_DECIDE,PETSC_DECIDE);
    EPSSetProblemType(m_eps,EPS_HEP);
    EPSSetType(m_eps, EPSKRYLOVSCHUR); //EPSARNOLDI);
    EPSSetTolerances(m_eps,1.0e-5,200);
    EPSKrylovSchurSetRestart(m_eps,0.6);
    EPSSetFromOptions(m_eps);
#endif
  }


  void EigenProblemPOD::solve() {
    if (FelisceParam::verbose()) PetscPrintf(PETSC_COMM_WORLD,"\nSolve eigenvalue problem.\n");
#ifdef FELISCE_WITH_SLEPC
    PetscInt nconv;
    //First request a singular value from one end of the spectrum
    EPSSetWhichEigenpairs(m_eps,EPS_LARGEST_REAL);
    if (m_verbose >= 10) {
      m_autocorr.view();
      EPSView(m_eps,PETSC_VIEWER_STDOUT_WORLD);
    }

    EPSSolve(m_eps);

    //Get number of converged singular values
    EPSGetConverged(m_eps,&nconv);
    PetscPrintf(PETSC_COMM_WORLD," Number of converged values = %d.\n", nconv);

    if ( m_dimRomBasis > m_numDof ) {
      PetscPrintf(PETSC_COMM_WORLD," Warning! Number of function basis bigger than number of dof.\n");
      m_dimRomBasis = m_numDof;
    }

    if ( m_dimRomBasis > nconv ) {
      PetscPrintf(PETSC_COMM_WORLD," Warning! Number of function basis bigger than number of converged values.\n");
      m_dimRomBasis = nconv;
    }

    double sigma;
    //Get converged singular values: largest singular value is stored in sigma.
    if (nconv > 0) {
      for (int i=0; i < m_dimRomBasis; i++) {
        EPSGetEigenpair(m_eps,i,&sigma,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR);
      }
    } else {
      m_dimRomBasis = 0;
      PetscPrintf(PETSC_COMM_WORLD," Unable to compute smallest singular value!\n\n");
      exit(1);
    }
    PetscPrintf(PETSC_COMM_WORLD, "m_dimRomBasis = %d \n", m_dimRomBasis);

    if (m_eigenValue == nullptr)
      m_eigenValue = new double[m_dimRomBasis];

    //m_basis.clear();
    //m_basis.resize(m_dimRomBasis);
    m_basisPOD.resize(m_dimRomBasis);
    m_eigenvector.resize(m_dimRomBasis);
    double norm2;

    double imgpart = 0.;
    for (int i=0; i < m_dimRomBasis; i++) {
      m_autocorr.getVecs(nullPetscVector,m_eigenvector[i]);
      EPSGetEigenpair(m_eps,i,&m_eigenValue[i],&imgpart,m_eigenvector[i].toPetsc(),FELISCE_PETSC_NULLPTR);
      if (Tools::notEqual(imgpart,0.0)) {
        PetscPrintf(PETSC_COMM_WORLD, "Warning! Im(m_eigenValue[%d]) = %e \n", i, imgpart);
      }
      m_eigenvector[i].norm(NORM_2,&norm2);

      if (m_verbose >= 1) {
        PetscPrintf(PETSC_COMM_WORLD, "m_eigenValue[%d] = %e \n", i, m_eigenValue[i]);
        PetscPrintf(PETSC_COMM_WORLD, "norm2[%d] = %e \n", i, norm2);
      }
    }

    felInt ia;
    for (int i=0; i<m_dimRomBasis; i++) {
      m_Matrix[0].getVecs(nullPetscVector,m_basisPOD[i]);
      for(int h=0; h<m_numSnapshot; h++) {
        ia = h;
        AOApplicationToPetsc(m_ao,1,&ia);
        double tmpCoeff = 0.0;
        m_eigenvector[i].getValues(1,&ia,&tmpCoeff);
        m_basisPOD[i].axpy( tmpCoeff, m_snapshot[h]);
      };
      double tmpRescale = m_eigenValue[i];
      tmpRescale = std::sqrt(tmpRescale);
      tmpRescale = 1.0/tmpRescale;
      m_basisPOD[i].scale(tmpRescale);
    };

    std::string fileName = FelisceParam::instance().resultDir + "/eigenValue";
    viewPOD(m_eigenValue,m_dimRomBasis,fileName);
#endif
  }




  // Reduced model functions
  //========================
  // Initialization
  void EigenProblemPOD::readData(IO& io) {
    m_Matrix[0].getVecs(m_U_0,nullPetscVector);
    m_U_0.setFromOptions();

    m_W_0.duplicateFrom(m_U_0);
    m_W_0.copyFrom(m_U_0);

    m_U_0_seq.createSeq(PETSC_COMM_SELF,m_numDof);
    m_U_0_seq.setFromOptions();
    if (FelisceParam::instance().hasInitialCondition) {
      // Read initial solution file (*.00000.scl)
      double* initialSolution = nullptr;
      initialSolution = new double[m_mesh->numPoints()];
      double* ionicSolution = nullptr;
      ionicSolution = new double[m_mesh->numPoints()];
      // potTransMemb
      io.readVariable(0, 0.,initialSolution, m_mesh->numPoints());
      // ionicVar
      io.readVariable(1, 0.,ionicSolution, m_mesh->numPoints());
      felInt ia;
      for (felInt i=0; i< m_numDof; i++) {
        ia = i;
        AOApplicationToPetsc(m_ao,1,&ia);
        m_U_0_seq.setValues(1,&ia,&initialSolution[i],INSERT_VALUES);
      }
      m_U_0_seq.assembly();
      fromDoubleStarToVec(initialSolution, m_U_0, m_mesh->numPoints());
      fromDoubleStarToVec(ionicSolution, m_W_0, m_mesh->numPoints());
      delete [] initialSolution;
      delete [] ionicSolution;
      // Read fibers file (*.00000.vct)
      if (FelisceParam::instance().testCase == 1) {
        if (m_fiber == nullptr)
          m_fiber = new double[m_mesh->numPoints()*3];
        io.readVariable(2, 0.,m_fiber, m_mesh->numPoints()*3);
      }
    } else {
      PetscPrintf(PETSC_COMM_WORLD, "U_0 not read from file (zero vector).");
      m_U_0.set( 0.);
      m_U_0.assembly();
      m_W_0.set( 0.);
      m_W_0.assembly();
      m_U_0_seq.set( 0.);
      m_U_0_seq.assembly();
    }
  }

  void EigenProblemPOD::getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber) {
    int numSupport = static_cast<int>(m_supportDofUnknown[iUnknown].iEle()[iel+1] - m_supportDofUnknown[iUnknown].iEle()[iel]);
    elemFiber.resize( numSupport*3,0.);
    for (int i = 0; i < numSupport; i++) {
      elemFiber[i*3] = m_fiber[3*(m_supportDofUnknown[iUnknown].iSupportDof()[m_supportDofUnknown[iUnknown].iEle()[iel]+i])];
      elemFiber[i*3+1] = m_fiber[3*(m_supportDofUnknown[iUnknown].iSupportDof()[m_supportDofUnknown[iUnknown].iEle()[iel]+i])+1];
      elemFiber[i*3+2] = m_fiber[3*(m_supportDofUnknown[iUnknown].iSupportDof()[m_supportDofUnknown[iUnknown].iEle()[iel]+i])+2];
    }
  }

  void EigenProblemPOD::initializeROM() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemPOD::initializeROM()\n");

    m_basisPOD.resize(m_dimRomBasis);
    for (int i=0; i<m_dimRomBasis; i++) {
      m_Matrix[m_idG].getVecs(m_basisPOD[i],nullPetscVector);
      m_basisPOD[i].setFromOptions();
      double* vec = new double[m_numDof];
      readEnsightFile(vec,"basis",i,m_numDof);
      fromDoubleStarToVec(vec, m_basisPOD[i], m_numDof);
      delete [] vec;
      if (FelisceParam::verbose() > 40) m_basisPOD[i].view();
    }

    readEigenValueFromFile();

    initializeSolution();
  }

  void EigenProblemPOD::readEnsightFile(double* vec, std::string varName, int idIter, felInt size) {
    std::string iteration;
    if (idIter < 10)
      iteration = "0000" + std::to_string(idIter);
    else if (idIter < 100)
      iteration = "000" + std::to_string(idIter);
    else if (idIter < 1000)
      iteration = "00" + std::to_string(idIter);
    else if (idIter < 10000)
      iteration = "0" + std::to_string(idIter);
    else if (idIter < 100000)
      iteration = std::to_string(idIter);

    std::string fileName = FelisceParam::instance().inputDirectory + "/" + varName + "." + iteration + ".scl";
    if (FelisceParam::verbose() > 1) PetscPrintf(PETSC_COMM_WORLD, "Read file %s\n", fileName.c_str());
    FILE* pFile = fopen (fileName.c_str(),"r");
    if (pFile==nullptr) {
      FEL_ERROR("Impossible to read "+fileName +".");
    }
    char str[80];
    std::string display;
    if ( fscanf(pFile,"%s", str) !=1 ) {
      FEL_WARNING("Failed ot read str");
    }
    display = str;
    if ( fscanf(pFile,"%s", str) !=1 ) {
      FEL_WARNING("Failed ot read str");
    }
    display = str;
    if ( fscanf(pFile,"%s", str) !=1) {
      FEL_WARNING("Failed ot read str");
    }
    display = str;
    //Read values
    int count = 0;
    for ( felInt i = 0; i < size; i++) {
      if ( fscanf(pFile,"%lf", &vec[i]) !=1 ) {
        FEL_WARNING("Failed ot read vec");
      }
      if ( count == 6 )  {
        if ( fscanf( pFile, "\n" ) !=0 ) {
          FEL_WARNING("Failed ot read");
        }
        count=0;
      }
      count++;
    }
    fclose(pFile);
  }

  void EigenProblemPOD::readEigenValueFromFile() {
    std::string fileName = FelisceParam::instance().inputDirectory + "/eigenValue";
    if (FelisceParam::verbose() > 1) PetscPrintf(PETSC_COMM_WORLD, "Read file %s\n", fileName.c_str());
    FILE* pFile = fopen (fileName.c_str(),"r");
    if (pFile==nullptr) {
      FEL_ERROR("Impossible to read "+fileName +".");
    }
    //Read values
    m_eigenValue = new double[m_dimRomBasis];
    for ( felInt i = 0; i < m_dimRomBasis; i++) {
      if ( fscanf(pFile,"%lf", &m_eigenValue[i]) !=1 ) {
        FEL_WARNING("Failed ot read vec");
      }
      if ( fscanf( pFile, "\n" ) !=0 ) {
        FEL_WARNING("Failed ot read");
      }
    }
    fclose(pFile);

    if (FelisceParam::verbose() > 10) {
      PetscPrintf(PETSC_COMM_WORLD, "EigenValues:\n");
      for ( felInt i = 0; i < m_dimRomBasis; i++) {
        PetscPrintf(PETSC_COMM_WORLD, "[%d] %e \n", i, m_eigenValue[i]);
      }
    }

  }


  void EigenProblemPOD::computeGamma() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemPOD::computeGamma()\n");

    // Warning: for Monodomain heart potential problem coupled with FhN
    // todo To be generalized

    double Am = FelisceParam::instance().Am;
    double Cm = FelisceParam::instance().Cm;
    double s = FelisceParam::instance().f0;
    double a = FelisceParam::instance().alpha;
    double eps = FelisceParam::instance().epsilon;
    double gam = FelisceParam::instance().gammaEl;

    if (m_gamma == nullptr)
      m_gamma = new double[m_dimRomBasis];
    if (m_eta == nullptr)
      m_eta = new double[m_dimRomBasis];

    for (int p=0; p<m_dimRomBasis; p++) {
      m_gamma[p] = 0.0;
    };

    if ( !FelisceParam::instance().hasInfarct ) {
      for (int p=0; p<m_dimRomBasis; p++) {
        m_gamma[p] = - ( s*a*Am ) * m_beta[p];
        m_gamma[p] += - Am * m_mu[p];
        for (int i=0; i<m_dimRomBasis; i++) {
          m_gamma[p] -= m_beta[i]*m_tensorE[secondOrderGlobalIndex(p,i)];
          for (int j=0; j<m_dimRomBasis; j++) {
            m_gamma[p] += ( s*(a+1)*Am) * m_beta[i] * m_beta[j] * m_tensorT[thirdOrderGlobalIndex(i,j,p)];
            for (int k=0; k<m_dimRomBasis; k++) {
              m_gamma[p] += - s*Am * m_beta[i] * m_beta[j] * m_beta[k] * m_tensorY[fourthOrderGlobalIndex(i,j,k,p)];
            }
          }
        }

        if (FelisceParam::instance().hasSource) {
          double delay[m_numSource];
          for (int iS=0; iS<m_numSource; iS++) {
            delay[iS] = FelisceParam::instance().delayStim[iS];
          }
          double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
          for (int iS=0; iS<m_numSource; iS++) {
            if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
              m_gamma[p] += Am*m_source[iS][p];
          }
        }

        m_gamma[p] = m_gamma[p] / (Am*Cm);

        m_eta[p] = eps * ( gam * m_beta[p] - m_mu[p] );

      }
    } else if (FelisceParam::instance().hasInfarct) {
      for (int p=0; p<m_dimRomBasis; p++) {
        m_gamma[p] += - Am * m_mu[p];
        for (int i=0; i<m_dimRomBasis; i++) {
          m_gamma[p] -= m_beta[i]*m_tensorE[secondOrderGlobalIndex(p,i)];
          m_gamma[p] += - a*Am * m_beta[i] * m_tensorCs[secondOrderGlobalIndex(i,p)];
          for (int j=0; j<m_dimRomBasis; j++) {
            m_gamma[p] += (a+1)*Am * m_beta[i] * m_beta[j] * m_tensorTs[thirdOrderGlobalIndex(i,j,p)];
            for (int k=0; k<m_dimRomBasis; k++) {
              m_gamma[p] += - Am * m_beta[i] * m_beta[j] * m_beta[k] * m_tensorY[fourthOrderGlobalIndex(i,j,k,p)];
            }
          }
        }

        if (FelisceParam::instance().hasSource) {
          double delay[m_numSource];
          for (int iS=0; iS<m_numSource; iS++) {
            delay[iS] = FelisceParam::instance().delayStim[iS];
          }
          double tPeriod=fmod(m_fstransient->time,FelisceParam::instance().timePeriod);
          for (int iS=0; iS<m_numSource; iS++) {
            if ((tPeriod>=delay[iS])&&(tPeriod<=(delay[iS]+FelisceParam::instance().stimTime)))
              m_gamma[p] += Am*m_source[iS][p];
          }
        }

        m_gamma[p] = m_gamma[p] / (Am*Cm);

        m_eta[p] = eps * ( gam * m_beta[p] - m_mu[p] );

      }
    }

  }





  void EigenProblemPOD::initializeSolution() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemPOD::initializeSolution()\n");

    if ( !m_solutionInitialized) {
      if (m_beta == nullptr)
        m_beta = new double[m_dimRomBasis];

      projectOnBasis(m_U_0,m_beta,true);

      if (m_mu == nullptr)
        m_mu = new double[m_dimRomBasis];

      projectOnBasis(m_W_0,m_mu,true);

      m_solutionInitialized = true;
    }
  }


  void EigenProblemPOD::setFhNf0(std::vector<double>& valuef0) {
    m_FhNf0.createSeq(PETSC_COMM_SELF,m_numDof);
    m_FhNf0.setFromOptions();

    // initialize SEQUENTIAL vectors (in order to use elemField)
    felInt ia;
    for (felInt i=0; i< m_numDof; i++) {
      ia = i;
      AOApplicationToPetsc(m_ao,1,&ia);
      m_FhNf0.setValues(1,&ia,&valuef0[i],INSERT_VALUES);
    }
    m_FhNf0.assembly();

  }

  void EigenProblemPOD::setIapp(std::vector<double>& iApp, int& idS) {
    if (m_source == nullptr) {
      m_source = new double*[m_numSource];
      for (int i=0; i<m_numSource; i++) {
        m_source[i] = new double[m_dimRomBasis];
      }
    }

    PetscVector RHS;
    RHS.createSeq(PETSC_COMM_SELF,m_numDof);
    RHS.setFromOptions();
    // initialize SEQUENTIAL vectors (in order to use elemField)
    felInt ia;
    for (felInt i=0; i< m_numDof; i++) {
      ia = i;
      AOApplicationToPetsc(m_ao,1,&ia);
      RHS.setValues(1,&ia,&iApp[i],INSERT_VALUES);
    }
    RHS.assembly();

    projectOnBasis(RHS, m_source[idS], true);

    RHS.destroy();
  }






  void EigenProblemPOD::checkSolution(PetscVector& solution, PetscVector& refSol, double & error) {

    PetscVector tmpVec1;
    tmpVec1.duplicateFrom(refSol);

    PetscVector tmpVec2;
    tmpVec2.duplicateFrom(refSol);

    waxpy(tmpVec1,-1.0,refSol,solution);

    mult(m_Matrix[m_idG],tmpVec1,tmpVec2);

    dot(tmpVec1,tmpVec2,&error);

    tmpVec1.destroy();
    tmpVec2.destroy();
  }



  void EigenProblemPOD::checkBasis() {

    if (!m_solutionInitialized) {
      initializeSolution();
    }

    double convergence[m_dimRomBasis];
    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);
    PetscVector tmpSol;
    tmpSol.duplicateFrom(m_snapshot[m_numSnapshot-1]);

    // Check convergence on the last snapshot
    double* tmpSolToSave = nullptr;
    tmpSolToSave = new double[m_numDof];
    for (int iBasis=0; iBasis < m_dimRomBasis; iBasis++) {
      double tmpCoeff[iBasis];
      for (int jBasis=0; jBasis<iBasis+1; jBasis++) {
        tmpCoeff[jBasis] = 0.0;
        scalarProduct(m_snapshot[m_numSnapshot-1], m_basisPOD[jBasis], tmpCoeff[jBasis],true);
      }

      projectOnDof(tmpCoeff,tmpSol,iBasis);

      checkSolution(tmpSol,m_snapshot[m_numSnapshot-1],convergence[iBasis]);

      fromVecToDoubleStar(tmpSolToSave, tmpSol, rankProc, 1);
      writeEnsightVector(tmpSolToSave, iBasis, "potTransMembConv");

      PetscPrintf(PETSC_COMM_WORLD, "convergence[%d] = %e \n", iBasis, convergence[iBasis]);
    }
    if (rankProc == 0)
      writeEnsightCase(m_dimRomBasis, 1., "potTransMembConv");
    std::string fileName = FelisceParam::instance().resultDir + "/convergence";
    viewPOD(convergence,m_dimRomBasis,fileName);

    delete [] tmpSolToSave;
    tmpSol.destroy();
  }


  void EigenProblemPOD::checkOrthonormality() {
    std::ofstream fOrth;
    fOrth.open("Orthonormality.log");

    std::cout << "Checking basis orthonormality: " <<std::endl;
    for(int i=0; i<m_dimRomBasis; i++) {
      for(int j=0; j<i+1; j++) {
        double prod = 0.0;
        scalarProduct(m_basisPOD[i], m_basisPOD[j], prod, true);
        fOrth << "Mass("<<i<<","<<j<<") = " << prod <<std::endl;
      };
    };
    fOrth.close();
  }



  void EigenProblemPOD::scalarProduct(PetscVector& firstVec, PetscVector& secondVec, double& prod, bool flagMass) {
    PetscVector tmpVec;
    tmpVec.duplicateFrom(firstVec);

    if (flagMass) {
      mult(m_Matrix[m_idG], firstVec, tmpVec);
      dot(tmpVec,secondVec,&prod);
    } else {
      dot( firstVec,secondVec,&prod);
    }

    tmpVec.destroy();
  };



  // Modified Gram-Schmidt (see Golub & Van Loan)
  void EigenProblemPOD::MGS(std::vector<PetscVector>& subspace) {
    PetscVector tmpVec;
    tmpVec.duplicateFrom(subspace[0]);
    int dimSubSpace = subspace.size();

    for(int iBasis=0; iBasis<dimSubSpace; iBasis++) {
      mult(m_Matrix[0],subspace[iBasis],tmpVec);
      double rii = 0.0;
      dot(tmpVec,subspace[iBasis],&rii);
      rii = 1.0/std::sqrt(rii);
      subspace[iBasis].scale(rii);

      mult(m_Matrix[0],subspace[iBasis],tmpVec);

      for(int j=iBasis+1; j<m_dimRomBasis; j++) {
        double rij = 0.0;
        dot(tmpVec,subspace[j],&rij);
        rij = -1.0 * rij;
        subspace[j].axpy(rij,subspace[iBasis]);
      }
    }
    tmpVec.destroy();
  }



  void EigenProblemPOD::computeMatrixZeta() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemPOD::computeMatrixZeta()\n");
    if ( m_zeta.size() == 0 ) {
      m_size2 = static_cast<int>(m_dimRomBasis*(m_dimRomBasis+1)/2);
      m_zeta.resize(m_size2);
      for (int i=0; i<m_size2; i++)
        m_zeta[i].duplicateFrom(m_basisPOD[0]);
    }

    for (int i=0; i<m_dimRomBasis; i++) { // row
      for (int j=0; j<i+1; j++) { // column
        // secondOrderGlobalIndex(i,j) = i*(i+1)/2 + j
        pointwiseMult(m_zeta[secondOrderGlobalIndex(i,j)],m_basisPOD[i],m_basisPOD[j]);
      }
    }

  }



  int EigenProblemPOD::secondOrderGlobalIndex(int& i, int& j) {
    int id;

    if (i >= j) {
      id = static_cast<int>(i*(i+1)/2+j);
    } else {
      id = static_cast<int>(j*(j+1)/2+i);
    }

    return id;
  }

  int EigenProblemPOD::thirdOrderGlobalIndex(int& i, int& j, int& k) {
    int id;
    int s[3];
    s[0] = i;
    s[1] = j;
    s[2] = k;
    int temp;
    for (int ii=0; ii<3; ii++) {
      for (int jj=0; jj<2; jj++) {
        if (s[jj] < s[jj+1]) {
          temp = s[jj];
          s[jj] = s[jj+1];
          s[jj+1] = temp;
        }
      }
    }
    id = static_cast<int>(s[0]*(s[0]+1)*(s[0]+2)/6);
    id += static_cast<int>(s[1]*(s[1]+1)/2);
    id += s[2];
    return id;
  }

  int EigenProblemPOD::fourthOrderGlobalIndex(int& i, int& j, int& k, int& h) {
    int id;
    int s[4];
    s[0] = i;
    s[1] = j;
    s[2] = k;
    s[3] = h;
    int temp;
    for (int ii=0; ii<4; ii++) {
      for (int jj=0; jj<3; jj++) {
        if (s[jj] < s[jj+1]) {
          temp = s[jj];
          s[jj] = s[jj+1];
          s[jj+1] = temp;
        }
      }
    }
    id = static_cast<int>(s[0]*(s[0]+1)*(s[0]*s[0]+5*s[0]+6)/24);
    id += static_cast<int>(s[1]*(s[1]+1)*(s[1]+2)/6);
    id += static_cast<int>(s[2]*(s[2]+1)/2);
    id += s[3];
    return id;
  }


  void EigenProblemPOD::computeTensor() {
    int n = m_dimRomBasis;

    if ( !FelisceParam::instance().hasInfarct) {
      // T_ijk = < phi_i, zeta_jk >
      m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
      if (m_tensorT == nullptr)
        m_tensorT = new double [m_size3];
      // Y_ijkh = < zeta_ij, zeta_kh >
      m_size4 = static_cast<int>(n*(n+1)*(n*n+5*n+6)/24);
      if (m_tensorY == nullptr)
        m_tensorY = new double [m_size4];
      if (m_tensorE == nullptr)
        m_tensorE = new double [m_size2];


      double valueT;
      double valueY;
      double valueE;
      PetscVector phiG;
      phiG.duplicateFrom(m_basisPOD[0]);
      PetscVector phiK;
      phiK.duplicateFrom(m_basisPOD[0]);

      for (int i=0; i<m_dimRomBasis; i++) {
        // phiK = phi_i^T * K
        mult(m_Matrix[m_idK],m_basisPOD[i],phiK);
        for (int j=0; j<i+1; j++) {
          // phiG = zeta_ij^T * G
          mult(m_Matrix[m_idG],m_zeta[secondOrderGlobalIndex(i,j)],phiG);
          // valueE
          dot(m_basisPOD[j],phiK,&valueE);
          m_tensorE[secondOrderGlobalIndex(i,j)] = valueE;

          for (int k=0; k<j+1; k++) {
            // valueT = zeta_ij^T * G * phi_k
            dot(m_basisPOD[k],phiG,&valueT);
            m_tensorT[thirdOrderGlobalIndex(i,j,k)] = valueT;
            for (int h=0; h<k+1; h++) {
              // valueY = zeta_ij^T * G * zeta_kh
              dot(m_zeta[secondOrderGlobalIndex(k,h)],phiG,&valueY);
              m_tensorY[fourthOrderGlobalIndex(i,j,k,h)] = valueY;
            }
          }
        }
      }
      phiG.destroy();
    } else if (FelisceParam::instance().hasInfarct) {
      // Cs_ij = < s phi_i, phi_j >
      if (m_tensorCs == nullptr)
        m_tensorCs = new double [m_size2];
      // T_ijk = < phi_i, zeta_jk >
      m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
      if (m_tensorT == nullptr)
        m_tensorT = new double [m_size3];
      // Ts_ijk = < s phi_i, zeta_jk >
      m_size3 = static_cast<int>(n*(n+1)*(n+2)/6);
      if (m_tensorTs == nullptr)
        m_tensorTs = new double [m_size3];
      // Y_ijkh = < zeta_ij, zeta_kh >
      m_size4 = static_cast<int>(n*(n+1)*(n*n+5*n+6)/24);
      if (m_tensorY == nullptr)
        m_tensorY = new double [m_size4];
      if (m_tensorE == nullptr)
        m_tensorE = new double [m_size2];

      double valueCs;
      double valueT;
      double valueTs;
      double valueY;
      double valueE;
      PetscVector phiGs;
      PetscVector zetaG;
      PetscVector zetaGs;
      PetscVector phiK;
      phiGs.duplicateFrom(m_basisPOD[0]);
      zetaG.duplicateFrom(m_basisPOD[0]);
      zetaGs.duplicateFrom(m_basisPOD[0]);
      phiK.duplicateFrom(m_basisPOD[0]);
      for (int i=0; i<m_dimRomBasis; i++) {
        // phiGs = phi_i^T * Gs
        mult(m_Matrix[m_idGs],m_basisPOD[i],phiGs);
        mult(m_Matrix[m_idK],m_basisPOD[i],phiK);
        for (int j=0; j<i+1; j++) {
          // valueCs = phi_i^T * Gs * phi_j
          dot(m_basisPOD[j],phiGs,&valueCs);
          m_tensorCs[secondOrderGlobalIndex(i,j)] = valueCs;

          //valueE
          dot(m_basisPOD[j],phiK,&valueE);
          m_tensorE[secondOrderGlobalIndex(i,j)] = valueE;

          // zetaG = zeta_ij^T * G
          mult(m_Matrix[m_idG],m_zeta[secondOrderGlobalIndex(i,j)],zetaG);
          // zetaGs = zeta_i^T * Gs
          mult(m_Matrix[m_idGs],m_zeta[secondOrderGlobalIndex(i,j)],zetaGs);
          for (int k=0; k<j+1; k++) {
            // valueT = zeta_ij^T * G * phi_k
            dot(m_basisPOD[k],zetaG,&valueT);
            m_tensorT[thirdOrderGlobalIndex(i,j,k)] = valueT;
            // valueTs = zeta_ij^T * Gs * phi_k
            dot(m_basisPOD[k],zetaGs,&valueTs);
            m_tensorTs[thirdOrderGlobalIndex(i,j,k)] = valueTs;
            for (int h=0; h<k+1; h++) {
              // valueY = zeta_ij^T * Gs * zeta_kh
              dot(m_zeta[secondOrderGlobalIndex(k,h)],zetaGs,&valueY);
              m_tensorY[fourthOrderGlobalIndex(i,j,k,h)] = valueY;
            }
          }
        }
      }
      phiGs.destroy();
      zetaG.destroy();
      zetaGs.destroy();
    }
  };



  void EigenProblemPOD::updateBeta() {
    if (FelisceParam::verbose() > 10 ) PetscPrintf(PETSC_COMM_WORLD,"\nEigenProblemPOD::updateBeta()\n");

    double dt = FelisceParam::instance().timeStep;
    double newBeta[m_dimRomBasis];
    // beta^{n+1} = beta^n + dt * gamma^n
    for (int i=0; i<m_dimRomBasis; i++) {
      newBeta[i] = m_gamma[i];
    }
    for (int i=0; i<m_dimRomBasis; i++) {
      m_beta[i] += dt * newBeta[i];
    }

    double newMu[m_dimRomBasis];
    // mu^{n+1} = mu^n + dt * eta^n
    for (int i=0; i<m_dimRomBasis; i++) {
      newMu[i] = m_eta[i];
    }
    for (int i=0; i<m_dimRomBasis; i++) {
      m_mu[i] += dt * newMu[i];
    }
  }



  // Projection functions from FE space to RO one
  void EigenProblemPOD::projectOnBasis(PetscVector& vectorDof, double* vectorBasis, bool flagMass) {
    PetscVector tmpVec;
    tmpVec.duplicateFrom(vectorDof);

    if (flagMass) {
      for (int iBasis=0; iBasis<m_dimRomBasis; iBasis++) {
        mult(m_Matrix[m_idG], m_basisPOD[iBasis], tmpVec);
        dot(tmpVec,vectorDof,&vectorBasis[iBasis]);
      }
    } else {
      for (int iBasis=0; iBasis<m_dimRomBasis; iBasis++) {
        dot( m_basisPOD[iBasis],vectorDof,&vectorBasis[iBasis]);
      }
    }

    tmpVec.destroy();
  }

  // Projection functions from RO space to FE one
  void EigenProblemPOD::projectOnDof(double* vectorBasis, PetscVector& vectorDof, int size) {
    vectorDof.set(0.);
    for (int iBasis=0; iBasis<size; iBasis++) {
      vectorDof.axpy(vectorBasis[iBasis],m_basisPOD[iBasis]);
    }
  }


  // Auxiliary function to debug purposes.
  void EigenProblemPOD::viewPOD(double* vToPrint, int vSize, std::ofstream outStream) {
    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);
    if (rankProc == 0) {
      for(int i=0; i<vSize; i++) {
        outStream << vToPrint[i] << std::endl;
      }
      outStream.close();
    }
  }


  void EigenProblemPOD::viewPOD(double* vToPrint, int vSize, std::string fName) {
    int rankProc;
    MPI_Comm_rank(m_petscComm,&rankProc);
    if (rankProc == 0) {
      std::ofstream outStream(fName.c_str());
      for(int i=0; i<vSize; i++) {
        outStream << vToPrint[i] << std::endl;
      }
      outStream.close();
    }
  }

}
