//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E.Schenone C.Corrado
//

// System includes

// External includes

// Project includes
#include "Solver/ionicSolver.hpp"

namespace felisce {
  IonicSolver::IonicSolver(FelisceTransient::Pointer fstransient):
    m_size(0),
    m_fstransient(fstransient),
    m_nComp(1),
    m_stabInit(false) {
  }


  IonicSolver::~IonicSolver() {
    m_uExtrap.destroy();
    m_ion.destroy();
    if (FelisceParam::instance().stateFilter &&m_stabInit ) {
      m_stabTerm.destroy();
      m_stabInit=false;
    }
    if (m_nComp>1) {
      for (std::size_t i=0; i<m_nComp; i++) {
        m_vecRHS[i].destroy();
        m_vecSolEDO[i].destroy();
      }
    } else { //nComp==1
      m_RHS.destroy();
      m_solEDO.destroy();
    }
  }


  void IonicSolver::defineOrderBdf(int order, int nComp) {
    m_bdf.defineOrder(order, nComp);
  }

  //Size of equation not necessarily V_0 size.
  void IonicSolver::defineSizeAndMappingOfIonicProblem(felInt size, ISLocalToGlobalMapping& mapping, AO ao) {
    m_size = size;
    m_localDofToGlobalDof = mapping;
    m_ao = ao;
    //Print the mapping.
    //ISLocalToGlobalMappingView(m_localDofToGlobalDof, PETSC_VIEWER_STDOUT_WORLD);
  }



  void IonicSolver::initializeExtrap(PetscVector& V_0) {
    //Initialize u_extrapolate.
    m_uExtrap.duplicateFrom(V_0);
    m_uExtrap.copyFrom(V_0);

    if (m_nComp ==1) {
      //Initialize m_RHS of Schaf EDO.
      m_RHS.duplicateFrom(V_0);
      m_RHS.set(0.);

      //Initialize solution of Schaf EDO.
      m_solEDO.duplicateFrom(V_0);
      m_solEDO.set(0.);

    } else {
      m_vecRHS.resize(m_nComp);//for Vec: MV, Paci
      m_vecSolEDO.resize(m_nComp);

      //Initialize m_RHS of Schaf EDO.
      for (std::size_t i=0; i<m_nComp; i++) {
        m_vecRHS[i].duplicateFrom(V_0);
        m_vecRHS[i].set(0.);
      }

      //Initialize solution of Schaf EDO.
      for (std::size_t i=0; i<m_nComp; i++) {
        m_vecSolEDO[i].duplicateFrom(V_0);
        m_vecSolEDO[i].set(0.);
      }
    }

    //Initialize value of ionic current m_ion
    m_ion.duplicateFrom(V_0);
    m_ion.set(0.);

  }

  //Initialize solution of Schaf EDO in case of bdf order == 1.
  void IonicSolver::initialize(PetscVector& sol_0) {
    m_bdf.initialize(sol_0);
  }

  void IonicSolver::initialize(std::vector<PetscVector>& sol_0) {
    if (m_nComp ==1) {
      m_bdf.initialize(sol_0[0]);
    } else {
      m_bdf.initialize(sol_0);

    }
  }

  //Initialize solution of Schaf EDO in case of bdf order == 2.
  void IonicSolver::initialize(PetscVector& sol_0, PetscVector& sol_1) {
    m_bdf.initialize(sol_0,sol_1);
  }

  void IonicSolver::initialize(std::vector<PetscVector>& sol_0, std::vector<PetscVector>& sol_1) {
    if (m_nComp == 1) {
      m_bdf.initialize(sol_0[0],sol_1[0]);
    } else {
      m_bdf.initialize(sol_0,sol_1);
    }
  }

  //Initialize solution of Schaf EDO in case of bdf order == 3.
  void IonicSolver::initialize(PetscVector& sol_0, PetscVector& sol_1, PetscVector& sol_2) {
    m_bdf.initialize(sol_0,sol_1,sol_2);
  }

  void IonicSolver::initialize(std::vector<PetscVector>& sol_0, std::vector<PetscVector>& sol_1, std::vector<PetscVector>& sol_2) {
    if (m_nComp == 1) {
      m_bdf.initialize(sol_0[0],sol_1[0],sol_2[0]);
    } else {
      m_bdf.initialize(sol_0,sol_1,sol_2);
    }
  }

  void IonicSolver::updateBdf() {
    if (m_nComp ==1) {
      m_bdf.update(m_solEDO);
    } else {
      m_bdf.update(m_vecSolEDO);
    }
  }

  void IonicSolver::update(PetscVector& V_1) {
    m_uExtrap.copyFrom(V_1);
  }
}



