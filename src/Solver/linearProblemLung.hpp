//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef _LINEARPROBLEMLUNG_HPP
#define _LINEARPROBLEMLUNG_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
// #include "FiniteElement/elementField.hpp"
#include "Solver/linearProblem.hpp"

namespace felisce {

  class LinearProblemLung:
    public LinearProblem {

    public:
      LinearProblemLung();

      ~LinearProblemLung() override = default;;

    protected:

      ///lung case (TODO: much code duplicated, is this really necessary ?)
      /**
       * @brief elementary loop to build matrix. lung case : enables access to previous iteration states for element field actualization
       * This function uses specific functions which are defined in derived problem classes.
       */
      void assembleMatrixRHSlung(Vec & divVel,Mat & divBasis,PetscVector&  rhsMinus,PetscVector&  prevVel, PetscVector&  prevDis, double currPre, int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
      
      void assembleMatrixRHSlungDisplacement(PetscVector&  emphysemaCoeff, PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis,PetscVector&  rhsMinus,PetscVector&  localExpansion,PetscVector&  pCouplingB,PetscVector&  volumeB,PetscVector&  prevVel, PetscVector&  prevDis, PetscVector&  displReal, double currPre, int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
      
      void assembleMatrixRHSlungPressure(PetscVector&  emphysemaCoeff,PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis,PetscVector&  rhsMinus,PetscVector&  localExpansion,PetscVector&  pCouplingB,PetscVector&  volumeB,PetscVector&  prevVel, PetscVector&  prevDis, double currPre, int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs);
      
      void computeVolumeLungRegions(PetscVector&  volReg, PetscVector&  displ, int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::only_rhs);
      
      void computeVolumeLungRegions2(PetscVector&  volReg, PetscVector&  displ, int rank, FlagMatrixRHS flagMatrixRH = FlagMatrixRHS::only_rhs);
      
      void normalLungMesh(PetscVector& modelVec, std::string modeLung, PetscVector&  normalVec, felInt idVecVariable);

      virtual void computeElementArrayLung(Vec & divVel,Mat & divBasis,PetscVector&  rhsMinus,int currentLabel,PetscVector&  prevVel, PetscVector&  prevDis, double currPre,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                        felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs );

      virtual void computeElementArrayLungDisplacement(PetscVector&  emphysemaCoeff,PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis,PetscVector&  rhsMinus,PetscVector&  localExpansion,PetscVector&  pCouplingB,PetscVector&  volumeB,int currentLabel,PetscVector&  prevVel, PetscVector&  prevDis, PetscVector&  displReal, double currPre,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                        felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs );

      virtual void computeElementArrayLungPressure(PetscVector&  emphysemaCoeff,PetscVector&  volReg,PetscVector&  volEvol,PetscVector&  divVel,PetscMatrix& divBasis,PetscVector&  rhsMinus,PetscVector&  localExpansion,PetscVector&  pCouplingB,PetscVector&  volumeB,int currentLabel,PetscVector&  prevVel, PetscVector&  prevDis, double currPre,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                        felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs );

      virtual void computeElementLungVolume(int currentLabel, PetscVector&  volReg,PetscVector&  displ,const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                        felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs );
  
      ///apply boundary condition for the mechanically induce lung ventilation (lung model)
      void applyBoundaryConditionMecha(double ratioM, PetscVector& volCurr,PetscVector& volFRC,PetscVector& volTLC, double dt, double coeffReaction, const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS=FlagMatrixRHS::only_rhs);
    
      ///Assembly loop on boundary element with curvilinear finite element. This is used in the lung model for mechanically induced ventilation
      void assembleMatrixRHSBoundaryConditionMecha(double ratioM, PetscVector& volCurr,PetscVector& volFRC,PetscVector& volTLC , double dt, double coeffReaction, int rank, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs, const int idBCforLinCombMethod=0);

      void applyBCMecha(double ratioM, PetscVector& volCurr,PetscVector& volFRC,PetscVector& volTLC, double dt, double coeffReaction, const int & applicationOption, int rank, FlagMatrixRHS flagMatrixRHSEss = FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS flagMatrixRHSNat = FlagMatrixRHS::matrix_and_rhs, const int idBCforLinCombMethod=0,
                        bool doPrintBC = true, ApplyNaturalBoundaryConditionsType do_apply_natural = ApplyNaturalBoundaryConditions::yes);
  
      
      /** 
       * @brief it computes the P1 normal field on the lateral surface
       * This function compute the P1 outward normal starting from the discontinuos normal.
       * It also computes, for each boundary dof, the measure of the all the boundary elements that share the dof it has to be called when you init the structure
       */
      void computeNormalField(const std::vector<int> & labels, felInt idVecVariable);


      /** 
       * @brief Used to compute the P1 outward normal
       */
      void normalFieldComputer(felInt ielSupportDof);

      /** 
       * @brief Used to update fe used for the computation of the P1 outward normal
       */
      void updateFeNormVel(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint);

      /** 
       * @brief Used to normalized the P1 outward normal right after its computation
       * It does a getValues from the sequential normalField and a setValue on the parallel normalField.
       * It is then necessary to call an assembly on the parallel normalField after finished using it.
       */
      void normalizeComputer(felInt ielSupportDof);

      /** 
       * @brief Virtual function to initialize element fields for the computation of the P1 outward normal
       */
      void initPerETNormVel() {};


      std::vector<int> m_auxiliaryInts;
  };
}

#endif