//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

// System includes

// External includes

// Project includes
#include "Solver/courtemancheSolver.hpp"

namespace felisce {
  CourtemancheSolver::CourtemancheSolver(FelisceTransient::Pointer fstransient):
    m_fstransient(fstransient),
    m_size(0),
    m_aoPetsc(FELISCE_PETSC_NULLPTR){
  }


  CourtemancheSolver::~CourtemancheSolver() {
    m_courtCondIto.clear();
    m_uExtrap.destroy();
    m_ion.destroy();
    m_m.destroy();
    m_h.destroy();
    m_j.destroy();
    m_ao.destroy();
    m_io.destroy();
    m_ua.destroy();
    m_ui.destroy();
    m_xr.destroy();
    m_xs.destroy();
    m_d.destroy();
    m_f.destroy();
    m_fca.destroy();
    m_urel.destroy();
    m_vrel.destroy();
    m_wrel.destroy();
    m_nai.destroy();
    m_nao.destroy();
    m_cao.destroy();
    m_ki.destroy();
    m_ko.destroy();
    m_cai.destroy();
    m_naiont.destroy();
    m_kiont.destroy();
    m_caiont.destroy();
    m_ileak.destroy();
    m_iup.destroy();
    m_itr.destroy();
    m_irel.destroy();
    m_cmdn.destroy();
    m_trpn.destroy();
    m_nsr.destroy();
    m_jsr.destroy();
    m_csqn.destroy();
  }


  //Size of equation not necessarily V_0 size.
  void CourtemancheSolver::defineSizeAndMappingOfIonicProblem(felInt size, ISLocalToGlobalMapping& mapping, AO ao) {
    m_size = size;
    m_localDofToGlobalDof = mapping;

    m_aoPetsc = ao;
    //Print the mapping.
    //ISLocalToGlobalMappingView(m_localDofToGlobalDof, PETSC_VIEWER_STDOUT_WORLD);
  }


  void CourtemancheSolver::initializeExtrap(PetscVector& V_0) {
    //Initialize u_extrapolate.
    m_uExtrap.duplicateFrom(V_0);
    m_uExtrap.copyFrom(V_0);

    //Initialize ion.
    m_ion.duplicateFrom(V_0);
    m_ion.zeroEntries();

  }


  void CourtemancheSolver::updateExtrap(PetscVector& V_1) {
    //Update u_extrapolate.
    swap(V_1,m_uExtrap);
  }

  void CourtemancheSolver::initializeConcentrationsAndGateConditions(PetscVector& m,PetscVector& h, PetscVector& j, PetscVector& ao, PetscVector& io, PetscVector& ua, PetscVector& ui, PetscVector& xr, PetscVector& xs, PetscVector& d, PetscVector& f, PetscVector& fca, PetscVector& urel, PetscVector& vrel, PetscVector& wrel, PetscVector& nai, PetscVector& nao, PetscVector& cao, PetscVector& ki, PetscVector& ko, PetscVector& cai, PetscVector& naiont, PetscVector& kiont, PetscVector& caiont, PetscVector& ileak, PetscVector& iup, PetscVector& itr, PetscVector& irel, PetscVector& cmdn, PetscVector& trpn, PetscVector& nsr, PetscVector& jsr, PetscVector& csqn) {
    m_m.duplicateFrom(m);
    m_m.copyFrom(m);
    m_h.duplicateFrom(h);
    m_h.copyFrom(h);
    m_j.duplicateFrom(j);
    m_j.copyFrom(j);
    m_ao.duplicateFrom(ao);
    m_ao.copyFrom(ao);
    m_io.duplicateFrom(io);
    m_io.copyFrom(io);
    m_ua.duplicateFrom(ua);
    m_ua.copyFrom(ua);
    m_ui.duplicateFrom(ui);
    m_ui.copyFrom(ui);
    m_xr.duplicateFrom(xr);
    m_xr.copyFrom(xr);
    m_xs.duplicateFrom(xs);
    m_xs.copyFrom(xs);
    m_d.duplicateFrom(d);
    m_d.copyFrom(d);
    m_f.duplicateFrom(f);
    m_f.copyFrom(f);
    m_fca.duplicateFrom(fca);
    m_fca.copyFrom(fca);
    m_urel.duplicateFrom(urel);
    m_urel.copyFrom(urel);
    m_vrel.duplicateFrom(vrel);
    m_vrel.copyFrom(vrel);
    m_wrel.duplicateFrom(wrel);
    m_wrel.copyFrom(wrel);
    m_nai.duplicateFrom(nai);
    m_nai.copyFrom(nai);
    m_nao.duplicateFrom(nao);
    m_nao.copyFrom(nao);
    m_cao.duplicateFrom(cao);
    m_cao.copyFrom(cao);
    m_ki.duplicateFrom(ki);
    m_ki.copyFrom(ki);
    m_ko.duplicateFrom(ko);
    m_ko.copyFrom(ko);
    m_cai.duplicateFrom(cai);
    m_cai.copyFrom(cai);
    m_naiont.duplicateFrom(naiont);
    m_naiont.copyFrom(naiont);
    m_kiont.duplicateFrom(kiont);
    m_kiont.copyFrom(kiont);
    m_caiont.duplicateFrom(caiont);
    m_caiont.copyFrom(caiont);
    m_ileak.duplicateFrom(ileak);
    m_ileak.copyFrom(ileak);
    m_iup.duplicateFrom(iup);
    m_iup.copyFrom(iup);
    m_itr.duplicateFrom(itr);
    m_itr.copyFrom(itr);
    m_irel.duplicateFrom(irel);
    m_irel.copyFrom(irel);
    m_cmdn.duplicateFrom(cmdn);
    m_cmdn.copyFrom(cmdn);
    m_trpn.duplicateFrom(trpn);
    m_trpn.copyFrom(trpn);
    m_nsr.duplicateFrom(nsr);
    m_nsr.copyFrom(nsr);
    m_jsr.duplicateFrom(jsr);
    m_jsr.copyFrom(jsr);
    m_csqn.duplicateFrom(csqn);
    m_csqn.copyFrom(csqn);
  }



  double CourtemancheSolver::iIonTotal(double v, felInt pos) {

    double iIonTot = 0.0;
    double R = 8.3143;
    double temp = 310.0;
    double frdy = 96.4867;
    double dt = m_fstransient->timeStep;


    // Compute INa: ina
    double ina = 0.0;
    double gna = 7.8;
    double ena = ((R*temp)/frdy)*std::log(m_value_nao/m_value_nai);
    double am = 3.2;

    if (std::abs(v - 47.13) < 1.0e-7) {
      am = 0.32*(v+47.13)/(1.0-exp(-0.1*(v+47.13)));
    }

    double bm = 0.08*exp(-v/11.0);
    double ah, bh, aj, bj;

    if (v < -40.0) {
      ah = 0.135*exp((80.0+v)/-6.8);
      bh = 3.56*exp(0.079*v)+310000.0*exp(0.35*v);
      aj = (-127140.0*exp(0.2444*v)-0.00003474*exp(-0.04391*v))*((v+37.78)/(1.0+exp(0.311*(v+79.23))));
      bj = (0.1212*exp(-0.01052*v))/(1.0+exp(-0.1378*(v+40.14)));
    } else {
      ah = 0.0;
      bh = 1.0/(0.13*(1.0+exp((v+10.66)/-11.1)));
      aj = 0.0;
      bj = (0.3*exp(-0.0000002535*v))/(1.0+exp(-0.1*(v+32.0)));
    }

    m_value_h = ah/(ah+bh)-((ah/(ah+bh))-m_value_h)*exp(-dt/(1.0/(ah+bh)));
    m_value_j = aj/(aj+bj)-((aj/(aj+bj))-m_value_j)*exp(-dt/(1.0/(aj+bj)));
    m_value_m = am/(am+bm)-((am/(am+bm))-m_value_m)*exp(-dt/(1.0/(am+bm)));

    if (!FelisceParam::instance().hasHeteroCondAtria) {
      ina = gna*m_value_m*m_value_m*m_value_m*m_value_h*m_value_j*(v-ena)*m_courtCondMultCoeff[pos];
    } else {
      ina = gna*m_value_m*m_value_m*m_value_m*m_value_h*m_value_j*(v-ena);
    }

    // Compute ICaL: ical
    double ical = 0.0;
    double dss = 1.0/(1.0+exp(-(v+10.0)/8.0));
    double taud = (1.0-exp((v+10.0)/-6.24))/(0.035*(v+10.0)*(1.0+exp((v+10.0)/-6.24)));
    double fss = 1.0/(1.0+exp((v+28.0)/6.9));
    double tauf = 9.0/(0.0197*exp(-std::pow((0.0337*(v+10.0)),2))+0.02);
    double fcass = 1.0/(1.0+m_value_cai/0.00035);
    double taufca = 2.0;

    m_value_d = dss-(dss-m_value_d)*exp(-dt/taud);
    m_value_f = fss-(fss-m_value_f)*exp(-dt/tauf);
    m_value_fca = fcass-(fcass-m_value_fca)*exp(-dt/taufca);

    double gcalbar = 0.1238;
    if (FelisceParam::instance().hasHeteroCourtPar) {
      gcalbar = m_courtCondICaL[pos];
    }

    ical = gcalbar*m_value_d*m_value_f*m_value_fca*(v-65.0);


    // Compute IKr: ikr
    double ikr = 0.0;

    double gkr = 0.0294;//*std::sqrt(ko/5.4);
    double ekr = ((R*temp)/frdy)*std::log(m_value_ko/m_value_ki);

    double xrss = 1.0/(1.0+exp(-(v+14.1)/6.5));
    double tauxr = 1.0/(0.0003*(v+14.1)/(1.0-exp(-(v+14.1)/5.0))+0.000073898*(v-3.3328)/(exp((v-3.3328)/5.1237)-1.0));

    m_value_xr = xrss-(xrss-m_value_xr)*exp(-dt/tauxr);

    double r = 1.0/(1.0+exp((v+15.0)/22.4));

    ikr = gkr*m_value_xr*r*(v-ekr);


    // Compute IKs: iks
    double iks = 0.0;

    double gks = 0.129;
    double eks = ((R*temp)/frdy)*std::log(m_value_ko/m_value_ki);
    double axs = 4.0e-5*(v-19.9)/(1.0-exp(-(v-19.9)/17.0));
    double bxs = 3.5e-5*(v-19.9)/(exp((v-19.9)/9.0)-1.0);
    double tauxs = 1.0/(2.0*(axs+bxs));
    double xsss = 1.0/std::pow((1.0+exp(-(v-19.9)/12.7)),0.5);
    m_value_xs = xsss-(xsss-m_value_xs)*exp(-dt/tauxs);

    //Warning ! The parameter of the CRN model gks becomes 5.0*gks in order to accelerate the atrial repolarization.
    if (FelisceParam::instance().torsade) {
      if ( (m_fstransient->time > FelisceParam::instance().torsadeTimeBegin) && (m_fstransient->time < FelisceParam::instance().torsadeTimeEnd) ) {
        iks = gks*m_value_xs*m_value_xs*(v-eks);
      }
    } else {
      iks = 5./*3.0*/*gks*m_value_xs*m_value_xs*(v-eks);
    }

    // Compute IKi: iki
    double iki = 0.0;

    double gki = 0.09;//*std::pow(ko/5.4,0.4);
    double eki = ((R*temp)/frdy)*std::log(m_value_ko/m_value_ki);

    double kin = 1.0/(1.0+exp(0.07*(v+80.0)));

    iki = gki*kin*(v-eki);


    // Compute Ikur: ikur
    double ikur = 0.0;

    double gkur = 0.005+0.05/(1+exp(-(v-15.0)/13.0));
    double ekur = ((R*temp)/frdy)*std::log(m_value_ko/m_value_ki);
    double alphauakur = 0.65/(exp(-(v+10.0)/8.5)+exp(-(v-30.0)/59.0));
    double betauakur = 0.65/(2.5+exp((v+82.0)/17.0));
    double tauuakur = 1.0/(3.0*(alphauakur+betauakur));
    double uakurss = 1.0/(1.0+exp(-(v+30.3)/9.6));
    double alphauikur = 1.0/(21.0+exp(-(v-185.0)/28.0));
    double betauikur = exp((v-158.0)/16.0);
    double tauuikur = 1.0/(3.0*(alphauikur+betauikur));
    double uikurss = 1.0/(1.0+exp((v-99.45)/27.48));

    m_value_ua = uakurss-(uakurss-m_value_ua)*exp(-dt/tauuakur);
    m_value_ui = uikurss-(uikurss-m_value_ui)*exp(-dt/tauuikur);

    ikur = gkur*m_value_ua*m_value_ua*m_value_ua*m_value_ui*(v-ekur);


    // Compute ITo: ito
    double ito = 0.0;
    double gito = 0.1652;
    if (FelisceParam::instance().hasHeteroCourtPar) {
      gito = m_courtCondIto[pos];
    }

    double erevto = ((R*temp)/frdy)*std::log(m_value_ko/m_value_ki);

    double alphaato = 0.65/(exp(-(v+10.0)/8.5)+exp(-(v-30.0)/59.0));
    double betaato = 0.65/(2.5+exp((v+82.0)/17.0));
    double tauato = 1.0/(3.0*(alphaato+betaato));
    double atoss = 1.0/(1.0+exp(-(v+20.47)/17.54));
    m_valuem_ao = atoss-(atoss-m_valuem_ao)*exp(-dt/tauato);

    double alphaiito = 1.0/(18.53+exp((v+113.7)/10.95));
    double betaiito = 1.0/(35.56+exp(-(v+1.26)/7.44));
    double tauiito = 1.0/(3.0*(alphaiito+betaiito));
    double iitoss = 1.0/(1.0+exp((v+43.1)/5.3));
    m_value_io = iitoss-(iitoss-m_value_io)*exp(-dt/tauiito);

    ito = gito*m_valuem_ao*m_valuem_ao*m_valuem_ao*m_value_io*(v-erevto);


    // Compute INaCa: inaca
    double inaca = 0.0;

    double gammas = 0.35;
    double kmnancx = 87.5;
    double kmcancx = 1.38;
    double ksatncx = 0.1;

    inaca = 1600.0*(exp(gammas*frdy*v/(R*temp))*m_value_nai*m_value_nai*m_value_nai*m_value_cao-exp((gammas-1.0)*frdy*v/(R*temp))*m_value_nao*m_value_nao*m_value_nao*m_value_cai)/((std::pow(kmnancx,3)+std::pow(m_value_nao,3))*(kmcancx+m_value_cao)*(1.0+ksatncx*exp((gammas-1.0)*frdy*v/(R*temp))));


    // Compute INaK: inak
    double inak = 0.0;
    double sigma = (exp(m_value_nao/67.3)-1.0)/7.0;
    double ibarnak = 0.6;
    double kmko = 1.5;
    double kmnai = 10.0;

    double fnak = 1.0/(1.0+0.1245*exp((-0.1*v*frdy)/(R*temp))+0.0365*sigma*exp((-v*frdy)/(R*temp)));

    inak = ibarnak*fnak*(1.0/(1.0+std::pow((kmnai/m_value_nai),1.5)))*(m_value_ko/(m_value_ko+kmko));


    // Compute IPCa: ipca
    double ipca = 0.0;
    double ibarpca = 0.275;
    double kmpca = 0.0005;

    ipca = (ibarpca*m_value_cai)/(kmpca+m_value_cai);


    // Compute IbCa: ibca
    double ibca = 0.0;
    double gcab = 0.00113;
    double ecan = ((R*temp)/frdy/2.0)*std::log(m_value_cao/m_value_cai);

    ibca = gcab*(v-ecan);


    // Compute IbNa: ibna
    double ibna = 0.0;
    double gnab = 0.000674;
    double enan = ((R*temp)/frdy)*std::log(m_value_nao/m_value_nai);

    ibna = gnab*(v-enan);


    // Compute ILeak: ileak
    m_value_ileak = 0.0;
    double caupmax = 15.0;
    double iupmax = 0.005;

    m_value_ileak = m_value_nsr*iupmax/caupmax;


    // Compute Iup: iup
    m_value_iup = 0.0;
    double kup = 0.00092;
    m_value_iup = iupmax/(1.0+(kup/m_value_cai)) ;


    // Compute Itr: itr
    m_value_itr = 0.0;
    double tautr = 180.0;
    m_value_itr = (m_value_nsr-m_value_jsr)/tautr;


    // Compute Irel: irel
    m_value_irel = 0.0;
    double krel = 30.0;
    double constvrel = 96.48;
    double fn = constvrel*(1.0e-12)*m_value_irel-(5.0e-13)*(ical/2.0-inaca/5.0)/frdy;

    double tauurel = 8.0;
    double urelss = 1.0/(1.0+exp(-(fn-3.4175e-13)/13.67e-16));

    double tauvrel = 1.91+2.09/(1.0+exp(-(fn-3.4175e-13)/13.67e-16));
    double vrelss = 1.0-1.0/(1.0+exp(-(fn-6.835e-14)/13.67e-16));

    double tauwrel = 6.0*(1.0-exp(-(v-7.9)/5.0))/((1.0+0.3*exp(-(v-7.9)/5.0))*(v-7.9));
    double wrelss = 1.0-1.0/(1.0+exp(-(v-40.0)/17.0));

    m_value_urel = urelss-(urelss-m_value_urel)*exp(-dt/tauurel);
    m_value_vrel = vrelss-(vrelss-m_value_vrel)*exp(-dt/tauvrel);
    m_value_wrel = wrelss-(wrelss-m_value_wrel)*exp(-dt/tauwrel);

    m_value_irel = krel*m_value_urel*m_value_urel*m_value_vrel*m_value_wrel*(m_value_jsr-m_value_cai);

    // Compute total current for Na:
    m_value_naiont = ina+ibna+3.0*inak+3.0*inaca;
    // Compute total current for K:
    m_value_kiont = ikr+iks+iki-2.0*inak+ito+ikur;
    // Compute total current for Ca:
    m_value_caiont = ical+ibca+ipca-2.0*inaca;

    iIonTot = ical + ibca + ibna + inaca + ipca + inak + ikr + iks + ikur + ito + iki + ina;

    return iIonTot;
  }

  void CourtemancheSolver::updateConcentrations() {
    double dt = m_fstransient->timeStep;
    double frdy = 96.4867;
    double vi = 13668.0;


    // Update Na concentration
    double dnai = -dt*m_value_naiont/(vi*frdy);
    m_value_nai = dnai + m_value_nai;

    // Update K concentration
    double dki = -dt*m_value_kiont/(vi*frdy);
    m_value_ki = dki + m_value_ki;

    // Update Ca concentration
    double vup = 1109.52;
    double constvrel = 96.48;

    double b1cai = -m_value_caiont/(2.0*frdy*vi)+(vup*(m_value_ileak-m_value_iup)+constvrel*m_value_irel)/vi;

    double cmdnmax = 0.05;
    double trpnmax = 0.07;
    double kmcmdn = 0.00238;
    double kmtrpn = 0.0005;
    double b2cai = 1.0+trpnmax*kmtrpn/std::pow((m_value_cai+kmtrpn),2)+cmdnmax*kmcmdn/std::pow((m_value_cai+kmcmdn),2);

    double dcai = dt*b1cai/b2cai;

    m_value_cai = dcai+m_value_cai;
  }

  PetscVector& CourtemancheSolver::ionicVariable(int iVar) {
    switch (iVar+1) {
    case 1:
      return m_m;
    case 2:
      return m_h;
    case 3:
      return m_j;
    case 4:
      return m_ao;
    case 5:
      return m_io;
    case 6:
      return m_ua;
    case 7:
      return m_ui;
    case 8:
      return m_xr;
    case 9:
      return m_xs;
    case 10:
      return m_d;
    case 11:
      return m_f;
    case 12:
      return m_fca;
    case 13:
      return m_urel;
    case 14:
      return m_vrel;
    case 15:
      return m_wrel;
    case 16:
      return m_nai;
    case 17:
      return m_nao;
    case 18:
      return m_cao;
    case 19:
      return m_ki;
    case 20:
      return m_ko;
    case 21:
      return m_cai;
    case 22:
      return m_naiont;
    case 23:
      return m_kiont;
    case 24:
      return m_caiont;
    case 25:
      return m_cmdn;
    case 26:
      return m_trpn;
    case 27:
      return m_nsr;
    case 28:
      return m_jsr;
    case 29:
      return m_csqn;
    default:
      FEL_ERROR("Courtemanche ionic variable not defined");
      return m_csqn;
    }
  }



  void CourtemancheSolver::computeIon() {

    felInt pos;
    double value_uExtrap;
    double value_ion;

    for (felInt i = 0; i < m_size; i++) {
      ISLocalToGlobalMappingApply(m_localDofToGlobalDof,1,&i,&pos);
      // value_uExtrap = m_uExtrap(i);
      m_uExtrap.getValues(1,&pos,&value_uExtrap);

      m_m.getValues(1,&pos,&m_value_m);
      m_h.getValues(1,&pos,&m_value_h);
      m_j.getValues(1,&pos,&m_value_j);
      m_ao.getValues(1,&pos,&m_valuem_ao);
      m_io.getValues(1,&pos,&m_value_io);
      m_ua.getValues(1,&pos,&m_value_ua);
      m_ui.getValues(1,&pos,&m_value_ui);
      m_xr.getValues(1,&pos,&m_value_xr);
      m_xs.getValues(1,&pos,&m_value_xs);
      m_d.getValues(1,&pos,&m_value_d);
      m_f.getValues(1,&pos,&m_value_f);
      m_fca.getValues(1,&pos,&m_value_fca);
      m_urel.getValues(1,&pos,&m_value_urel);
      m_vrel.getValues(1,&pos,&m_value_vrel);
      m_wrel.getValues(1,&pos,&m_value_wrel);
      m_nai.getValues(1,&pos,&m_value_nai);
      m_nao.getValues(1,&pos,&m_value_nao);
      m_cao.getValues(1,&pos,&m_value_cao);
      m_ki.getValues(1,&pos,&m_value_ki);
      m_ko.getValues(1,&pos,&m_value_ko);
      m_cai.getValues(1,&pos,&m_value_cai);
      m_naiont.getValues(1,&pos,&m_value_naiont);
      m_kiont.getValues(1,&pos,&m_value_kiont);
      m_caiont.getValues(1,&pos,&m_value_caiont);
      m_ileak.getValues(1,&pos,&m_value_ileak);
      m_iup.getValues(1,&pos,&m_value_iup);
      m_itr.getValues(1,&pos,&m_value_itr);
      m_irel.getValues(1,&pos,&m_value_irel);
      m_cmdn.getValues(1,&pos,&m_value_cmdn);
      m_trpn.getValues(1,&pos,&m_value_trpn);
      m_nsr.getValues(1,&pos,&m_value_nsr);
      m_jsr.getValues(1,&pos,&m_value_jsr);
      m_csqn.getValues(1,&pos,&m_value_csqn);

      value_ion = iIonTotal(value_uExtrap,pos);

      felInt meshId = pos;
      AOPetscToApplication(m_aoPetsc,1,&meshId);
      felInt sizeVent = 45580;
      if (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart")
        sizeVent = 32320;
      if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        if (meshId < sizeVent)
          value_ion = 0.0;
      }

      updateConcentrations();


      m_ion.setValue(pos,value_ion, INSERT_VALUES);
      m_m.setValue(pos,m_value_m, INSERT_VALUES);
      m_h.setValue(pos,m_value_h, INSERT_VALUES);
      m_j.setValue(pos,m_value_j, INSERT_VALUES);
      m_ao.setValue(pos,m_valuem_ao, INSERT_VALUES);
      m_io.setValue(pos,m_value_io, INSERT_VALUES);
      m_ua.setValue(pos,m_value_ua, INSERT_VALUES);
      m_ui.setValue(pos,m_value_ui, INSERT_VALUES);
      m_xr.setValue(pos,m_value_xr, INSERT_VALUES);
      m_xs.setValue(pos,m_value_xs, INSERT_VALUES);
      m_d.setValue(pos,m_value_d, INSERT_VALUES);
      m_f.setValue(pos,m_value_f, INSERT_VALUES);
      m_fca.setValue(pos,m_value_fca, INSERT_VALUES);
      m_urel.setValue(pos,m_value_urel, INSERT_VALUES);
      m_vrel.setValue(pos,m_value_vrel, INSERT_VALUES);
      m_wrel.setValue(pos,m_value_wrel, INSERT_VALUES);
      m_nai.setValue(pos,m_value_nai, INSERT_VALUES);
      m_nao.setValue(pos,m_value_nao, INSERT_VALUES);
      m_cao.setValue(pos,m_value_cao, INSERT_VALUES);
      m_ki.setValue(pos,m_value_ki, INSERT_VALUES);
      m_ko.setValue(pos,m_value_ko, INSERT_VALUES);
      m_cai.setValue(pos,m_value_cai, INSERT_VALUES);
      m_naiont.setValue(pos,m_value_naiont, INSERT_VALUES);
      m_kiont.setValue(pos,m_value_kiont, INSERT_VALUES);
      m_caiont.setValue(pos,m_value_caiont, INSERT_VALUES);
      m_ileak.setValue(pos,m_value_ileak, INSERT_VALUES);
      m_iup.setValue(pos,m_value_iup, INSERT_VALUES);
      m_itr.setValue(pos,m_value_itr, INSERT_VALUES);
      m_irel.setValue(pos,m_value_irel, INSERT_VALUES);
      m_cmdn.setValue(pos,m_value_cmdn, INSERT_VALUES);
      m_trpn.setValue(pos,m_value_trpn, INSERT_VALUES);
      m_nsr.setValue(pos,m_value_nsr, INSERT_VALUES);
      m_jsr.setValue(pos,m_value_jsr, INSERT_VALUES);
      m_csqn.setValue(pos,m_value_csqn, INSERT_VALUES);


    }

    m_ion.assembly();
    m_m.assembly();
    m_h.assembly();
    m_j.assembly();
    m_ao.assembly();
    m_io.assembly();
    m_ua.assembly();
    m_ui.assembly();
    m_xr.assembly();
    m_xs.assembly();
    m_d.assembly();
    m_f.assembly();
    m_fca.assembly();
    m_urel.assembly();
    m_vrel.assembly();
    m_wrel.assembly();
    m_nai.assembly();
    m_nao.assembly();
    m_cao.assembly();
    m_ki.assembly();
    m_ko.assembly();
    m_cai.assembly();
    m_naiont.assembly();
    m_kiont.assembly();
    m_caiont.assembly();
    m_ileak.assembly();
    m_iup.assembly();
    m_itr.assembly();
    m_irel.assembly();
    m_cmdn.assembly();
    m_trpn.assembly();
    m_nsr.assembly();
    m_jsr.assembly();
    m_csqn.assembly();

  }


}
