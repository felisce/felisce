//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E.Schenone D. Lombardi
//

#ifndef _EIGENPROBLEMPOD_HPP
#define _EIGENPROBLEMPOD_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/eigenProblem.hpp"

namespace felisce {
  /*!
   \class EigenProblemPOD
   \authors E.Schenone D.Lombardi
   \date 20/09/2013
   \brief Manage functions of a POD eigenvalues problem.
   */

  class EigenProblemPOD:
    public EigenProblem {
  public:
    // Constructor
    //============
    EigenProblemPOD();
    ~EigenProblemPOD() override;

    void initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm);

    //Define Physical Variable associate to the problem
    //=================================================
    void initPerElementType() override;

    // Assemble Matrix
    //================
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void readSnapshot(IO& io);
    void computeAutocorr();

    //! Visualize modes as a variable
    void writeMode(const int iter=0);
    void writeEnsightSolution(const int iter);

    //! Solve eigenproblem to build rom basis
    void buildSolver() override;
    virtual void solve();

    // Reduced model functions
    //========================
    // Initialization
    void readData(IO& io) override;
    void getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber);
    void initializeROM();

    void readEnsightFile(double* vec, std::string varName, int idIter, felInt size);
    void readEigenValueFromFile();
    void computeGamma();
    void computeMatrixZeta();

    void computeTensor();
    int secondOrderGlobalIndex(int& i, int& j);
    int thirdOrderGlobalIndex(int& i, int& j, int& k);
    int fourthOrderGlobalIndex(int& i, int& j, int& k, int& h);

    void initializeSolution();
    void setFhNf0(std::vector<double>& valuef0);
    void setIapp(std::vector<double>& iApp, int& idS);

    // Compute solution
    void checkSolution(PetscVector& solution, PetscVector& refSol, double & error);
    void checkBasis();
    void checkOrthonormality();


    //Update functions
    void MGS(std::vector<PetscVector>&);
    void updateBeta();


    // Projection functions from FE space to RO one and viceversa
    void projectOnBasis(PetscVector& vectorDof, double* vectorBasis, bool flagMass);
    void projectOnDof(double* vectorBasis, PetscVector& vectorDof, int size);
    void scalarProduct(PetscVector& firstVec, PetscVector& secondVec, double& prod,bool flagMass);


    // Print&Auxiliary functions
    //==========================
    void viewPOD(double* vToPrint, int vSize, std::ofstream outStream);
    void viewPOD(double* vToPrint, int vSize, std::string fName);


    // Acces functions
    //================
    double eigenValueFirst() {
      return m_eigenValue[0];
    }
    double* eigenValue() {
      return m_eigenValue;
    }
    double* beta() {
      return m_beta;
    }
    double* mu() {
      return m_mu;
    }
    double* gamma() {
      return m_gamma;
    }
    double* eta() {
      return m_eta;
    }
    int dimRomBasis() {
      return m_dimRomBasis;
    }

    template <class Templ_functor> void evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array);

    // writing ECG
    virtual void initializeECG() {}
    virtual void writeECG(int /*iteration*/) {}
    virtual void updateEcgOperator() {}

  protected:
    CurrentFiniteElement* m_fePotTransMemb;
    felInt m_ipotTransMemb;
    felInt m_ipotExtraCell;

    std::vector<PetscVector> m_snapshot;
    PetscMatrix m_autocorr;

    PetscVector m_U_0;
    PetscVector m_U_0_seq;
    PetscVector m_W_0;
    PetscVector m_FhNf0;
    ElementField m_elemFieldU0;
    double* m_eigenValue;
    std::vector<PetscVector> m_basis;
    std::vector<PetscVector> m_basisPOD;
    std::vector<PetscVector> m_eigenvector;
    std::vector<PetscVector> m_zeta;

    felInt m_dimRomBasis;
    felInt m_numSnapshot;
    felInt m_samplingFreq;

    double* m_beta;
    double* m_mu;
    double* m_gamma;
    double* m_eta;
    double* m_modeMean;
    double* m_fiber;
    double** m_source;
    bool m_solutionInitialized;

    int m_idK;
    int m_idG;
    int m_idGs;


    double* m_tensorCs;
    double* m_tensorE;
    double* m_tensorT;
    double* m_tensorTs;
    double* m_tensorY;
    int m_size2;
    int m_size3;
    int m_size4;
    int m_numSource;
    int m_problem;


    ElementField m_elemFieldFhNf0;

  };


  template <class Templ_functor> void EigenProblemPOD::evalFunctionOnDof(const Templ_functor& func, std::vector<double>& array) {
    Point pt;
    array.resize(m_numDof);
    felInt position;
    for( felInt i = 0; i < m_numDof; i++) {
      findCoordinateWithIdDof(i,pt);
      position = i;
      AOApplicationToPetsc(m_ao,1,&position);
      array[position] = func(pt.x(),pt.y(),pt.z());
    }
  }


}

#endif
