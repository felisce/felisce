//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Solver/eigenProblem.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementField.hpp"
#include "Core/felisceParam.hpp"

namespace felisce 
{
/*! Constructor
  */
EigenProblem::EigenProblem()
{
  nullPetscVector = PetscVector::null();
}

/***********************************************************************************/
/***********************************************************************************/

EigenProblem::~EigenProblem() 
{
  if(m_seqSolAllocated) {
    m_seqSol.zeroEntries();
    m_seqSol.destroy();
  }
  if (m_initNumDofUnknown) {
    delete [] m_numDofUnknown;
    delete [] m_numDofLocalUnknown;
  }

  if (m_buildSystem ) {
    m_sol.destroy();
  }

#ifdef FELISCE_WITH_SLEPC
  EPSDestroy(&m_eps);
#endif

}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::initialize(const GeometricMeshRegion::Pointer& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm) 
{
  m_fstransient = fstransient;
  m_petscComm = comm;
  m_verbose = FelisceParam::verbose();
  m_mesh = mesh;

#ifdef FELISCE_WITH_SLEPC
  EPSCreate(comm, &m_eps);
#endif
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::definePhysicalVariable(const std::vector<PhysicalVariable>& listVariable, const std::vector<std::size_t>& listNumComp) 
{
  for (std::size_t iVar = 0; iVar < listVariable.size(); iVar++) {
    m_listVariable.addVariable(listVariable[iVar],listNumComp[iVar]);
  }
  if (listVariable.size() != m_listVariable.size()) {
    FEL_ERROR("Problem with your input file. Not Enought physical variable defined for this problem!");
  }

  //link between unknown of the linear system and variable define in the problem.
  Variable variable;
  bool unknown = false;
  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
    for (std::size_t iVar = 0; iVar < listVariable.size() && unknown == false; iVar++) {
      variable = m_listVariable[iVar];
      if ( m_listUnknown[iUnknown] == variable.physicalVariable()) {
        m_listUnknown.idVariable(iUnknown) = iVar;
        m_listVariable.listIdUnknownOfVariable(iVar) = static_cast<int>(iUnknown);
        unknown = true;
      }
    }
    unknown = false;
  }
  if (m_verbose) {
    PetscPrintf(m_petscComm, "\n/========== Information about the linear system ==========/\n");
    PetscPrintf(m_petscComm, "<%s>.\n", nameOfTheProblem.c_str());
    PetscPrintf(m_petscComm, "Unknonwns are: ");
    for (std::size_t iUnkonwn = 0; iUnkonwn < m_listUnknown.size(); iUnkonwn++) {
      PetscPrintf(m_petscComm, "<%s> ", m_listVariable[m_listUnknown.idVariable(iUnkonwn)].name().c_str());
    }
    PetscPrintf(m_petscComm, "\nPhysical variables associate: ");
  }
  m_listVariable.print(m_verbose);
  m_listUnknown.setDefaultMask(m_listVariable);
  m_listUnknown.print(1);
  if (m_numDofUnknown == nullptr)
    m_numDofUnknown = new felInt[m_listUnknown.size()];
  if (m_numDofLocalUnknown == nullptr)
    m_numDofLocalUnknown = new felInt[m_listUnknown.size()];
  m_initNumDofUnknown = true;

  //todo Jeremie 01 02 2012: assemble multiple matrix
  m_Matrix.resize(m_numberOfMatrix);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::computeDof(int numProc, int idProc) 
{
  //    felInt numElement = m_mesh->getNumDomainElement() + m_mesh->getNumBoundaryElement();
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    SupportDofMesh supportDofMesh(m_listVariable[m_listUnknown.idVariable(iUnknown)], m_mesh);
    m_supportDofUnknown.push_back(supportDofMesh);

    if (m_verbose) {
      PetscPrintf(m_petscComm, "\n/=======Global support dof mesh associate to the unknown: <%s>.======/\n",
                  m_listVariable[m_listUnknown.idVariable(iUnknown)].name().c_str());
      supportDofMesh.print(m_verbose);
    }
  }

  std::vector<SupportDofMesh*> pSupportDofUnknown(m_supportDofUnknown.size(), nullptr);
  for(std::size_t iUnknown=0; iUnknown < m_supportDofUnknown.size(); ++iUnknown)
    pSupportDofUnknown[iUnknown] = &m_supportDofUnknown[iUnknown];

  m_dof.setDof(m_listUnknown, m_listVariable, pSupportDofUnknown);
  //m_dof.buildPattern();
  //Initialize pattern, random repartition on processors before call ParMetis.
  m_dof.initializePattern(numProc, idProc);

  m_numDof = m_dof.numDof();
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    m_numDofUnknown[iUnknown] = m_dof.numDofPerUnknown()[iUnknown];
  }

  if (m_verbose) {
    PetscPrintf(m_petscComm, "\n/========== Information about dof ==========/\n");
  }
  m_dof.print(m_verbose);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::partitionDof( idx_t numPart, int rankPart, MPI_Comm comm) 
{
  /*!
    Partitioning of the dof.
  */

  /*
    Declaration of the parameters for ParMetis.
    idx_t define the type for ParMetis int.
  */

  // Input parameters
  idx_t   num_flag = 0;                        // C-style array index.
  idx_t   ncon = 1;                            // number of weight per vertices of the graph.
  idx_t   wgtFlag = 0;                         // No weight in the graph.
  idx_t   option[3];                           // verbose level and random seed for parmetis.
  idx_t   vertexdist[numPart + 1];             // Repartition of dof between the processes.

  real_t  ubvec = static_cast<real_t>(1.05);   // Imbalance tolerance

  // Output parameters
  idx_t   edgecut;                             // Number of edge in the graph that are cut by the partitioning.
  int numRows =  m_dof.pattern().numRows();
  std::vector<idx_t> dofPartitionning(m_dof.pattern().numRows(), 0);  // The partition of the dof.

  /*
      Initialisation of the parameters.
  */

  // vertexdist
  // vertexdist[j] = sum(size in process i), for i=0,..,j-1.
  // initialize the first value (always 0)
  vertexdist[0] = 0;

  // Gather all the size in vertexdist, starting at index 1 (because vertexdist[0] is 0)
  MPI_Allgather(&numRows, 1, MPI_INT, &vertexdist[1], 1, MPI_INT, comm);

  // Compute the partial sum
  for (int i = 1; i < numPart; i ++)
    vertexdist[i + 1] += vertexdist[i];

  // option
  option[0] = 1;     // 0 = default values, 1 = user defined.
  option[1] = 100;   // verbose level.
  option[2] = 100;   // random seed.

  {
    // tpwgts
    real_t  tpwgts[numPart*ncon];                // See parmetis manual for description
    for ( felInt i= 0; i < numPart*ncon; i++)
      tpwgts[i] = static_cast<real_t>(1.)/numPart;

    /*
      Parmetis call
    */

    MPI_Comm comm_bis = PETSC_COMM_WORLD;
    ParMETIS_V3_PartKway(vertexdist, &m_dof.pattern().rowPointer(0), &m_dof.pattern().columnIndex(0), nullptr, nullptr, &wgtFlag, &num_flag, &ncon, &numPart, tpwgts, &ubvec, option, &edgecut, dofPartitionning.data(), &comm_bis);
  }

  /*
    Gather the partitioning to all process
  */

  m_dofPart.resize(m_dof.numDof(), 0);

  {
    felInt recvcount[numPart];
    for(int i=0; i<numPart; i++)
      recvcount[i] = vertexdist[i+1] - vertexdist[i];

    MPI_Allgatherv(dofPartitionning.data(), dofPartitionning.size(), MPI_INT, m_dofPart.data(), recvcount, vertexdist, MPI_INT, comm);
  }

  // Count the number of local dofs
  for (felInt i = 0; i < m_dof.numDof(); i++) {
    if (m_dofPart[i] == rankPart)
      m_numDofLocal++;
  }

  /*
    Build the matrix pattern
  */

  m_dof.buildPattern(rankPart, m_dofPart.data());

  // Pattern operations
  if(m_dof.patternIsChanged()) {
    m_dof.mergeLocalCompPattern(m_petscComm, numPart, rankPart, m_dofPart);

    //free memory
    m_dof.clearPatternC();
  }

  /*!
    Global ordering: AO is an application between global mesh ordering and global ordering of Petsc
    AO: globalordering to globalordering
    the action is:
    - Take the global index associated to the i-th proc
    - Create an application to a new global index in order to have contiguous global indexes on the processor

    both global ordering are on input (loc2glob and pordering)
    AOCreateBasic(comm, m_numDofLocal, loc2Glob, pordering, &m_ao);

    Example:
    Proc 0:
    glob2loc = [0 1 2 3 8]
    pordering =[0 1 2 3 4]

    Proc 1:
    glob2loc = [4 5 6 7]
    pordering =[5 6 7 8]
  */
  {
    felInt rstart;
    felInt cptDof = 0;

    felInt loc2Glob[m_numDofLocal];
    for (felInt i = 0; i < m_numDofLocal; i++)
      loc2Glob[i] = 0;

    for (felInt i = 0; i < m_dof.numDof(); i++) {
      if (m_dofPart[i] == rankPart) {
        // cptDof (local) -> i (global)
        loc2Glob[cptDof] = i;
        cptDof++;
      }
    }

    // inclusive sum (rank_0 = numdoflocal_0, rank_i = sum_j=0^rank_i numdoflocal_i)
    MPI_Scan(&m_numDofLocal, &rstart, 1, MPIU_INT, MPI_SUM, comm);
    rstart -= m_numDofLocal;

    felInt pordering[m_numDofLocal];
    for (felInt i= 0; i < m_numDofLocal; i++) {
      // pordering: so global indexes are contiguous on a processor; pordering: global->global
      pordering[i] = rstart + i;
    }

    AOCreateBasic(comm, m_numDofLocal, loc2Glob, pordering, &m_ao);
    // AOView(m_ao,PETSC_VIEWER_STDOUT_WORLD);
  }

  /*!
    Compute the element repartition.
    An element is owned by a process k if most of its dof are on process k.
    In case of a draw, the element belongs to the process with the smallest id.
  */

  // Declaration of some variables
  felInt max = 0;                                   // used to compute indice_max.
  felInt indice_max = 0;                            // id of the process that owns the element.
  felInt idDof;                                     // Local number of a dof.
  felInt ielSupportDof;                             // Global number of a dof.
  felInt numElemsPerRef = 0;                        // Number of elements per label.
  int    idVar;                                     // id of a physical variable.
  felInt numEltTot = 0;                             // Total number of element (to be computed).
  felInt numEltPerType[GeometricMeshRegion::m_numTypesOfElement]; // Total number of element per type (to be computed).
  felInt numDofPerProcs[numPart];                    // Number of dofs per process.
  GeometricMeshRegion::ElementType eltType;

  // Initialization
  for ( int i = 0; i < numPart; i++)
    numDofPerProcs[i] = 0;

  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++)
    numEltPerType[ityp] = 0;

  m_eltPart.resize(m_mesh->getNumElement(), 0);

  // bag containing all the domain element type.
  const std::vector<ElementType>& bagElementTypeDomain =m_mesh->bagElementTypeDomain();

  // for each domain element type
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    // for each label where the current element type can be found
    for(auto itRef =m_mesh->intRefToBegEndMaps[eltType].begin(); itRef !=m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
      numElemsPerRef = itRef->second.second;

      // for each local element with the current label
      for (felInt iel = 0; iel < numElemsPerRef; iel++) {

        // for each unknown of the linear system
        for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
          // get the global id of the current element
          idVar = m_listUnknown.idVariable(iUnknown);
          m_supportDofUnknown[iUnknown].getIdElementSupport(eltType, numEltPerType[eltType], ielSupportDof);

          // for each support dof of the current element
          for (felInt iSupport = 0; iSupport <  m_supportDofUnknown[iUnknown].getNumSupportDof(ielSupportDof); iSupport++) {

            // for each components of the current unknown
            for (std::size_t iComp = 0; iComp < m_listVariable[idVar].numComponent(); iComp++) {
              // get the global id of the dof and increased numDofPerProcs
              m_dof.loc2glob(ielSupportDof, iSupport, idVar, iComp, idDof);
              ++numDofPerProcs[m_dofPart[idDof]];
            }
          }
        }

        // Find the rank of the process that owns the most dof in the current element.
        for (int j = 0; j < numPart; j++) {
          if (max < numDofPerProcs[j]) {
            max = numDofPerProcs[j];
            indice_max = j;
          }
        }

        // Fill m_eltPart
        m_eltPart[numEltTot] = indice_max;

        // Increment the counter
        numEltPerType[eltType]++;
        numEltTot++;

        // Clear local variables
        max = 0;
        indice_max = 0;
        for ( int j = 0; j < numPart; j++)
          numDofPerProcs[j] = 0;
      }
    }
  }

  // bag containing all the "boundary" element type.
  const std::vector<ElementType>& bagElementTypeDomainBoundary =m_mesh->bagElementTypeDomainBoundary();

  // Do the same thing but with the "boundaries" element this time
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    eltType =  bagElementTypeDomainBoundary[i];

    for(auto itRef =m_mesh->intRefToBegEndMaps[eltType].begin(); itRef !=m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
      numElemsPerRef = itRef->second.second;

      for (felInt iel = 0; iel < numElemsPerRef; iel++) {

        for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
          idVar = m_listUnknown.idVariable(iUnknown);
          m_supportDofUnknown[iUnknown].getIdElementSupport(eltType, numEltPerType[eltType], ielSupportDof);

          for (int iSupport = 0; iSupport <  m_supportDofUnknown[iUnknown].getNumSupportDof(ielSupportDof); iSupport++) {

            for (std::size_t iComp = 0; iComp < m_listVariable[idVar].numComponent(); iComp++) {
              m_dof.loc2glob(ielSupportDof,iSupport,idVar,iComp,idDof);
              numDofPerProcs[ m_dofPart[idDof] ]++;
            }
          }
        }

        for (int j = 0; j < numPart; j++) {
          if (max < numDofPerProcs[j] ) {
            max = numDofPerProcs[j];
            indice_max = j;
          }
        }

        m_eltPart[numEltTot] = indice_max;
        numEltPerType[eltType]++;
        numEltTot++;

        max = 0;
        indice_max = 0;
        for (int j = 0; j < numPart; j++)
          numDofPerProcs[j] = 0;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::getSupportCoordinate(felInt idElt,
                                        felInt idSupport,
                                        int iUnknown,
                                        Point& pt) 
{
  const felInt id = m_supportDofUnknownLocal[iUnknown].iSupportDof()[m_supportDofUnknownLocal[iUnknown].iEle()[idElt]
              + idSupport];
  pt = m_supportDofUnknownLocal[iUnknown].listNode()[id];
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::cutMesh() 
{
  partitionDof(static_cast<idx_t>(MpiInfo::numProc()), MpiInfo::rankProc(), MpiInfo::petscComm());
  setLocalMeshAndSupportDofMesh(MpiInfo::rankProc());
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::setLocalMeshAndSupportDofMesh(int rankPart) {
  int idVar = -1;

  // Set the local mesh and fill the local to global mapping for the support elements (loc2GlobSupportElem)
  std::vector<felInt> loc2GlobElem;
  m_meshLocal = felisce::make_shared<GeometricMeshRegion>();
  m_meshLocal->setLocalMesh(*m_mesh, m_eltPart, rankPart, loc2GlobElem);


  // for each unknown, create the support dof and put them in m_supportDofUnknownLocal
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    idVar = m_listUnknown.idVariable(iUnknown);
    SupportDofMesh supportDofMesh(m_listVariable[idVar], m_meshLocal, loc2GlobElem, m_supportDofUnknown[iUnknown]);

    m_supportDofUnknownLocal.push_back(supportDofMesh);
  }

  // Create the local to global mapping for the elements
  IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(m_petscComm, 1, loc2GlobElem.size(), loc2GlobElem.data(), PETSC_COPY_VALUES, &m_mappingElem);

  // print info on the local mesh and supportDofMeshes
  if (m_verbose) {
    // print the local mesh
    PetscPrintf(m_petscComm, "\n/================== Local mesh ================/\n");
    m_meshLocal->print(m_verbose);

    // print the local supportDofMesh for each unknown
    for(std::size_t iUnknown=0; iUnknown<m_listUnknown.size(); iUnknown++) {
      PetscPrintf(m_petscComm,
                  "\n/=========== Local support dof mesh associate to the unknown: <%s> ===========/\n",
                  m_listVariable[idVar].name().c_str());
      m_supportDofUnknownLocal[iUnknown].print(m_verbose);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::allocateMatrix() 
{
  // Create the local to global mapping for support dofs.
  felInt loc2Glob[m_numDofLocal];
  felInt cptDof= 0;
  for ( felInt iLDof = 0; iLDof < m_numDofLocal; iLDof++)
    loc2Glob[iLDof] = 0;

  for ( felInt iDof = 0; iDof < m_dof.numDof(); iDof++) {
    if ( m_dofPart[iDof] == MpiInfo::rankProc() ) {
      loc2Glob[cptDof] = iDof;
      cptDof++;
    }
  }

  // global to local mapping on nodes
  IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(m_petscComm, 1, m_numDofLocal, loc2Glob, PETSC_COPY_VALUES, &m_mappingNodes);
  // ISLocalToGlobalMappingView( m_mappingNodes, PETSC_VIEWER_STDOUT_WORLD);

  if ( MpiInfo::numProc() > 1 ) {
    felInt iCSR[m_dof.pattern().numRows()+1];
    felInt jCSR[m_dof.pattern().numNonzeros()];

    iCSR[0] = 0;
    std::set<felInt> jCSRSorted;
    felInt connect;
    felInt globValue;
    for ( felInt ii = 0; ii < m_numDofLocal; ii++) {
      connect = m_dof.pattern().numNonzerosInRow(ii);
      iCSR[ii+1] = iCSR[ii] + connect;
      for ( felInt jj = 0; jj < connect; jj++) {
        globValue = m_dof.pattern().columnIndex( m_dof.pattern().rowPointer(ii) + jj );
        AOApplicationToPetsc(m_ao, 1, &globValue);
        jCSRSorted.insert( globValue );
      }
      felInt kk = 0;
      for(auto  it_jCSR = jCSRSorted.begin(); it_jCSR != jCSRSorted.end(); it_jCSR++) {
        jCSR[ iCSR[ii] + kk] = *it_jCSR;
        kk++;
      }
      jCSRSorted.clear();
    }
    for (std::size_t ii = 0; ii < m_Matrix.size(); ii++) {
      m_Matrix[ii].createAIJ(m_petscComm, m_numDofLocal, m_numDofLocal, m_dof.numDof(), m_dof.numDof(), 0, FELISCE_PETSC_NULLPTR, 0, FELISCE_PETSC_NULLPTR);
      m_Matrix[ii].setFromOptions();
      m_Matrix[ii].mpiAIJSetPreallocationCSR( iCSR, jCSR, nullptr);
      m_Matrix[ii].setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    }
  } else {
    const std::size_t numRows = m_dof.pattern().numRows();
    felInt nnz[ numRows ];
    for ( felInt idof = 0; idof < static_cast<felInt>(numRows) ; idof++) {
      nnz[idof] = m_dof.pattern().numNonzerosInRow(idof);
    }

    for (std::size_t ii = 0; ii < m_Matrix.size(); ii++) {
      m_Matrix[ii].createSeqAIJ(m_petscComm, numRows, numRows, 0, nnz);
      m_Matrix[ii].setFromOptions();
      m_Matrix[ii].setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
    }
  }

  m_numDof = m_dof.numDof();

  // Create the std::vector solution
  m_sol.createMPI(m_petscComm, m_numDofLocal, m_numDof);
  m_sol.set( 0.);
  allocateSequentialSolution();
  m_buildSystem = true;

  // Get the local size of all process
  m_localSizeOfVector.resize(MpiInfo::numProc());
  for (int ii = 0; ii < MpiInfo::numProc(); ii++)
    m_localSizeOfVector[ii] = 0;

  felInt sizeLocal = 0;
  m_sol.getLocalSize(&sizeLocal);

  MPI_Allgather(&sizeLocal, 1, MPI_INT, &m_localSizeOfVector[0], 1, MPI_INT, m_petscComm);

  // For each unknown, create a local to global mapping to std::unordered_map the local id of dof to their global id in the petsc ordering.
  felInt idGlobalDof = 0;
  felInt sumPreviousSizeLocal = 0;
  std::vector<felInt> loc2GlobPerUnknown;
  felInt sizeIdDof;
  m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc.resize(m_listUnknown.size());

  for ( int iProc = 0; iProc < MpiInfo::rankProc(); iProc++)
    sumPreviousSizeLocal += m_localSizeOfVector[iProc];

  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ )
    m_numDofLocalUnknown[iUnknown] = 0;

  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    for ( felInt iDof = 0; iDof < m_numDofUnknown[iUnknown]; iDof++) {
      AOApplicationToPetsc(m_ao, 1, &idGlobalDof);
      if ( (sumPreviousSizeLocal <= idGlobalDof) && (idGlobalDof < m_localSizeOfVector[MpiInfo::rankProc()]+sumPreviousSizeLocal) ) {
        m_numDofLocalUnknown[iUnknown]++;
        loc2GlobPerUnknown.push_back(idGlobalDof);
      }
      idGlobalDof++;
    }
    sizeIdDof = loc2GlobPerUnknown.size();

    IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(m_petscComm, 1, sizeIdDof, loc2GlobPerUnknown.data(), PETSC_COPY_VALUES,
                                  &m_mappingIdLocalDofPerUnknownToIdGlobalDofPetsc[iUnknown] );
    loc2GlobPerUnknown.clear();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::allocateSequentialSolution() 
{
  m_seqSol.createSeq(PETSC_COMM_SELF,m_numDof);
  m_seqSol.set(0.0);
  m_seqSolAllocated=true;
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::assembleMatrix() 
{
  ElementType eltType;           // geometric element type in the mesh.
  int numPointPerElt = 0;        // number of points per geometric element.
  int currentLabel = 0;          // number of label domain.
  felInt numEltPerLabel = 0;     // number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;
  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;
  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  //Assembly loop.
  //First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];
    //resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);
    //define all current finite element use in the problem from data
    //file cnfiguration and allocate    1 and elemVec (question: virtual ?).
    defineFiniteElement(eltType);
    //allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrix();
    //virtual function use in derived problem to allocate elemenField necessary.
    initPerElementType();
    //second loop on region of the mesh.
    for(auto itRef = m_meshLocal->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;
      //By default this virtual defnie variable m_currentLabel: (m_currentLabel=label).
      //We can switch on label region with that and define some parameters.
      initPerDomain(currentLabel);
      //Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, &ielSupportDof);
        //todo Jeremie 01 02 2012: assemble multiple matrix
        for (std::size_t j = 0; j < m_Matrix.size(); j++)
          m_elementMat[j]->zero();
        computeElementArray(elemPoint, elemIdPoint, ielSupportDof);
        // add values of elemMat in the global matrix: m_Matrix[0].
        setValueMatrix(&ielSupportDof);
        numElement[eltType]++;
      }
    }
    //desallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrix();
  }

  //real assembling of the matrix manage by PETsC.
  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    m_Matrix[i].assembly(MAT_FINAL_ASSEMBLY);
  }

  //call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbose);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::assembleMatrixBD() 
{
  ElementType eltType;           //geometric element type in the mesh.
  int numPointPerElt = 0;        //number of points per geometric element.
  int currentLabel = 0;          //number of label domain.
  felInt numEltPerLabel = 0;        //number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // contains the ids of the support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDof;

  //Assembly loop.
  //First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType =  bagElementTypeDomain[i];

    //resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    //define all current finite element use in the problem from data
    //file configuration and allocate elemMat and elemVec (question: virtual ?).
    defineCurvilinearFiniteElement(eltType);

    //allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixBD();

    //virtual function use in derived problem to allocate elemenField necessary.
    initPerElementTypeBD();

    // use by user to add specific term (term source for example with elemField).
    userElementInitBD();

    //second loop on region of the mesh.
    for(auto itRef = m_meshLocal->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      //By default this virtual defnie variable m_currentLabel: (m_currentLabel=label).
      //We can switch on label region with that and define some parameters.
      initPerDomainBD(currentLabel);

      //Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDof = vectorIdSupport[it];

          //todo Jeremie 01 02 2012: assemble multiple matrix
          for (std::size_t j = 0; j < m_Matrix.size(); j++)
            m_elementMatBD[j]->zero();

          // function call in derived problem to compute specific operators of the  problem (Heat, N-S,...).
          computeElementArrayBD(elemPoint, elemIdPoint, ielSupportDof);

          // add values of elemMat in the global matrix: m_Matrix[0].
          setValueMatrixBD(&ielSupportDof);
        }
        numElement[eltType]++;
      }
    }
    //allocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrix();
  }

  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    m_Matrix[i].assembly(MAT_FINAL_ASSEMBLY);
  }
  //call with high level of verbose to print matrix in matlab format.
  writeMatrixForMatlab(m_verbose);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::initElementArray() 
{
  int idVar;

  std::vector<std::size_t>           numberCmp(m_listUnknown.size());
  std::vector<const CurBaseFiniteElement*> finiteElt(m_listUnknown.size());

  for (std::size_t n=0; n<m_listUnknown.size(); n++){
    idVar = m_listUnknown.idVariable(n);
    numberCmp[n] = m_listVariable[idVar].numComponent();
    finiteElt[n] = m_listCurrentFiniteElement[idVar];
  }

  for (std::size_t i = 0; i < m_Matrix.size(); i++)
    m_elementMat.push_back(felisce::make_shared<ElementMatrix>(finiteElt, numberCmp, numberCmp));
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::initElementArrayBD() 
{
  int idVar;

  std::vector<std::size_t>           numberCmp(m_listUnknown.size());
  std::vector<const CurBaseFiniteElement*> finiteElt(m_listUnknown.size());
  for (std::size_t n=0; n<m_listUnknown.size(); n++){
    idVar = m_listUnknown.idVariable(n);
    numberCmp[n] = m_listVariable[idVar].numComponent();
    finiteElt[n] = m_listCurvilinearFiniteElement[idVar];
  }

  for (std::size_t i = 0; i < m_Matrix.size(); i++)
    m_elementMatBD.push_back(felisce::make_shared<ElementMatrix>(finiteElt, numberCmp, numberCmp));
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::defineFiniteElement(const ElementType& eltType) 
{
  if (m_listCurrentFiniteElement.size() > 0) {
    m_listCurrentFiniteElement.clear();
    //todo Jeremie 01 02 2012: assemble multiple matrix
    m_elementMat.clear();
  }
  const RefElement *refEle;

  int typeOfFiniteElement = 0;
  CurrentFiniteElement* fe;
  const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  for ( std::size_t iVar = 0, size = m_listVariable.size(); iVar < size; iVar++) {
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType(); // 0 = linear, 1 = quadratic, 2 = bubble
    refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_meshLocal);
    fe = new CurrentFiniteElement(*refEle,*geoEle,m_listVariable[iVar].getDegreeOfExactness());
    m_listCurrentFiniteElement.add(fe);
  }
  //Element matrix and std::vector initialisation
  initElementArray();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::defineCurvilinearFiniteElement(const ElementType& eltType) 
{
  if (m_listCurvilinearFiniteElement.size() > 0) {
    m_listCurvilinearFiniteElement.clear();
    std::cout << m_Matrix.size();
    m_elementMatBD.clear();
  }
  //Initialisation of Finite Element curvlinear problem
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurvilinearFiniteElement* fe;
  geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  for ( std::size_t iVar = 0; iVar < m_listVariable.size(); iVar++) {
    typeOfFiniteElement = m_listVariable[iVar].finiteElementType();
    refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_meshLocal);
    fe = new CurvilinearFiniteElement(*refEle,*geoEle,m_listVariable[iVar].degreeOfExactness());
    FEL_ASSERT(fe);
    m_listCurvilinearFiniteElement.add(fe);
  }
  //Element matrix and std::vector initialisation
  initElementArrayBD();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint,
                                std::vector<felInt>& elemIdPoint, felInt* ielSupportDof)
{
  //    int iVar = 0; // id of the variable. ielSupport always the same with all variable.
  int iUnknown = 0;
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(eltType, iel, *ielSupportDof);
  ISLocalToGlobalMappingApply(m_mappingElem,1,ielSupportDof,ielSupportDof);

  int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  m_meshLocal->getOneElement(eltType,iel,elemIdPoint,0);
  for (int iPoint = 0; iPoint < numPointsPerElt; iPoint++) {
    elemPoint[iPoint] = &m_meshLocal->listPoints()[elemIdPoint[iPoint]]; // => m_meshLocal not const
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::setElemPoint(ElementType& eltType, felInt iel, std::vector<Point*>& elemPoint,
                                std::vector<felInt>& elemIdPoint, std::vector<felInt>& vectorSupport) 
{
  //    int iVar = 0; // id of the variable. ielSupport always the same with all variable.
  // We assume that the support elements are the same for all the unknown.
  int iUnknown = 0;
  m_supportDofUnknownLocal[iUnknown].getIdElementSupport(eltType, iel, vectorSupport);
  ISLocalToGlobalMappingApply(m_mappingElem, vectorSupport.size(), vectorSupport.data(), vectorSupport.data());
  int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  m_meshLocal->getOneElement(eltType, iel, elemIdPoint, 0);
  for (int iPoint = 0; iPoint < numPointsPerElt; iPoint++) {
    elemPoint[iPoint] = &m_meshLocal->listPoints()[elemIdPoint[iPoint]]; // => m_meshLocal not const
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::setValueMatrix(const felInt* ielSupportDof) 
{
  // \todo: works (a priori) only for the square matrix
  const felInt numDofTotal = m_elementMat[0]->mat().size1();
  felInt node1 = 0;
  felInt node2 = 0;
  felInt cptGposLine = 0;
  felInt cptGposLine2 = 0;
  felInt sizeSupport = 0;
  felInt sizeSupport2 = 0;
  felInt cptBlock = 0;
  int idVar = -1;
  int idVar2 = -1;
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(iUnknown);
    sizeSupport = m_listCurrentFiniteElement[idVar]->numDof();
    // Warning: in elemMat the order is e.g. ( ux, uy, p)
    // in the matrix the order is (ux1,uy1,ux2,uy2,...,p1,p2,..)
    for (std::size_t iComp = 0; iComp < m_listVariable[idVar].numComponent(); iComp++) {
      cptBlock++;
      for ( int iSupport = 0; iSupport < sizeSupport; iSupport++) {
        m_dof.loc2glob(*ielSupportDof, iSupport, idVar, iComp, node1);
        m_GposLine[cptGposLine]= node1;
        cptGposLine2 = 0;
        for ( std::size_t iUnknown2 = 0; iUnknown2 <m_listUnknown.size(); iUnknown2++) {
          idVar2 = m_listUnknown.idVariable(iUnknown2);
          sizeSupport2 = m_listCurrentFiniteElement[idVar2]->numDof();
          // order matters: comp before support to manage ( ux, uy, p) order in  mat elem to (ux1,uy1,ux2,uy2,...,p1,p2,..)!!!!
          for (std::size_t jComp = 0; jComp <m_listVariable[idVar2].numComponent(); jComp++) {
            for (felInt  jSupport = 0; jSupport < sizeSupport2; jSupport++) {
              int iConnect = m_dof.getNumGlobComp( iUnknown, iComp);
              int jConnect = m_dof.getNumGlobComp( iUnknown2, jComp);
              if ( m_listUnknown.mask()(iConnect, jConnect) > 0) {
                m_dof.loc2glob(*ielSupportDof,jSupport,idVar2,jComp,node2);
                m_GposColumn[cptGposLine2] = node2;
                for (std::size_t i = 0; i < m_Matrix.size(); i++) {
                  m_valueMat[i][cptGposLine2 + cptGposLine*numDofTotal] = m_elementMat[i]->mat()(cptGposLine, cptGposLine2);
                }
                cptGposLine2++;
              }
            }
          }
        }
        cptGposLine++;
      }
    }
  }

  AOApplicationToPetsc(m_ao,cptGposLine,m_GposLine);
  AOApplicationToPetsc(m_ao,cptGposLine2,m_GposColumn);
  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    m_Matrix[i].setValues(numDofTotal,m_GposLine,numDofTotal,m_GposColumn,m_valueMat[i],ADD_VALUES);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::allocateArrayForAssembleMatrix() 
{
  felInt numDofTotal = 0;
  int idFE = -1; //(idFE = idVar)
  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
    idFE = m_listUnknown.idVariable(iUnknown);
    numDofTotal += m_listCurrentFiniteElement[idFE]->numDof()*m_listVariable[idFE].numComponent();
  }

  if (m_GposColumn == nullptr)
    m_GposColumn = new felInt[numDofTotal];
  if (m_GposLine == nullptr)
    m_GposLine = new felInt[numDofTotal];
  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    m_valueMat.push_back(new double[numDofTotal*numDofTotal]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::setValueMatrixBD(const felInt* ielSupportDof) 
{
  const felInt numDofTotal = m_elementMatBD[0]->mat().size1();

  felInt node1 = 0;
  felInt node2 = 0;
  felInt cptGposLine = 0;
  felInt cptGposLine2 = 0;
  felInt sizeSupport = 0;
  felInt sizeSupport2 = 0;
  felInt cptBlock = 0;
  int idVar = -1;
  int idVar2 = -1;

  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
    idVar = m_listUnknown.idVariable(iUnknown);
    sizeSupport = m_listCurvilinearFiniteElement[idVar]->numDof();
    // order matters: comp before support to manage ( ux, uy, p) order in  mat elem to (ux1,uy1,ux2,uy2,...,p1,p2,..)!!!!
    for (std::size_t iComp = 0; iComp < m_listVariable[idVar].numComponent(); iComp++) {
      cptBlock++;
      for ( int iSupport = 0; iSupport < sizeSupport; iSupport++) {
        m_dof.loc2glob(*ielSupportDof, iSupport, idVar, iComp, node1);
        m_GposLine[cptGposLine]= node1;
        cptGposLine2 = 0;
        for ( std::size_t iUnknown2 = 0; iUnknown2 <m_listUnknown.size(); iUnknown2++) {
          idVar2 = m_listUnknown.idVariable(iUnknown2);
          sizeSupport2 = m_listCurvilinearFiniteElement[idVar2]->numDof();
          // order matters: comp before support to manage ( ux, uy, p) order in  mat elem to (ux1,uy1,ux2,uy2,...,p1,p2,..)!!!!
          for (std::size_t jComp = 0; jComp <m_listVariable[idVar2].numComponent(); jComp++) {
            for ( int jSupport = 0; jSupport < sizeSupport2; jSupport++) {
              int iConnect = m_dof.getNumGlobComp( iUnknown, iComp);
              int jConnect = m_dof.getNumGlobComp( iUnknown2, jComp);
              if ( m_listUnknown.mask()(iConnect, jConnect) > 0) {
                m_dof.loc2glob(*ielSupportDof,jSupport,idVar2,jComp,node2);
                m_GposColumn[cptGposLine2] = node2;
                for (std::size_t i = 0; i < m_Matrix.size(); i++) {

                  m_valueMat[i][cptGposLine2 + cptGposLine*numDofTotal] = m_elementMatBD[i]->mat()(cptGposLine, cptGposLine2);
                }
                cptGposLine2++;
              }
            }
          }
        }
        cptGposLine++;
      }
    }
  }

  AOApplicationToPetsc(m_ao,cptGposLine,m_GposLine);
  AOApplicationToPetsc(m_ao,cptGposLine2,m_GposColumn);
  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    m_Matrix[i].setValues(numDofTotal,m_GposLine,numDofTotal,m_GposColumn,m_valueMat[i],ADD_VALUES);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::allocateArrayForAssembleMatrixBD() 
{
  felInt numDofTotal = 0;
  for (std::size_t iFe = 0; iFe < m_listCurvilinearFiniteElement.size(); iFe++) {
    numDofTotal += m_listCurvilinearFiniteElement[iFe]->numDof()*m_listVariable[iFe].numComponent();
  }
  m_GposColumn = new felInt[numDofTotal];
  m_GposLine = new felInt[numDofTotal];

  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    m_valueMat.push_back(new double[numDofTotal*numDofTotal]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::desallocateArrayForAssembleMatrix() 
{
  for (std::size_t i = 0; i < m_Matrix.size(); i++) {
    delete [] m_valueMat[i];
  }
  m_valueMat.clear();
  delete [] m_GposLine;
  m_GposLine = nullptr;
  delete [] m_GposColumn;
  m_GposColumn = nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::scaleMatrix(felReal coef, int i) 
{
  m_Matrix[i].scale(coef);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::scaleMatrix(felReal coef, PetscMatrix& M)
{
  M.scale(coef);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::printSolution(int verbose) const 
{
  if (verbose > 19) {
    PetscPrintf(m_petscComm,"\n/================Solution of the assembly system Ax=b================/\n");
    m_sol.view();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::printMatrix(PetscMatrix& M) 
{
  M.view();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::printVector(PetscVector& V) 
{
  V.view();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeMatrixForMatlab(int verbose) const 
{
  if (verbose > 20) {
    PetscPrintf(m_petscComm,"\n/===========Matrix write in Matlab file matrix.m=============/\n");
    PetscViewer viewer;
    PetscViewerCreate(m_petscComm,&viewer);
    PetscViewerASCIIOpen(m_petscComm,"matrix.m",&viewer);
    PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
    m_Matrix[0].view(viewer);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeMatrixForMatlab(std::string const& fileName, PetscMatrix& matrix) const 
{
  PetscPrintf(m_petscComm,"\n/===========Matrix write in Matlab file fileName=============/\n");
  PetscViewer viewer;
  PetscViewerCreate(m_petscComm,&viewer);
  PetscViewerASCIIOpen(m_petscComm,fileName.c_str(),&viewer);
  PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
  matrix.view(viewer);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeVectorForMatlab(std::string const& fileName, PetscVector& vector) const
{
  PetscPrintf(m_petscComm,"\n/===========std::vector write in Matlab file std::vector.m=============/\n");
  PetscViewer viewer;
  PetscViewerCreate(m_petscComm,&viewer);
  PetscViewerASCIIOpen(m_petscComm,fileName.c_str(),&viewer);
  PetscViewerPushFormat(viewer,PETSC_VIEWER_ASCII_MATLAB);
  vector.view(viewer);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::buildSolver() 
{
#ifdef FELISCE_WITH_SLEPC
  EPSSetOperators(m_eps,m_Matrix[0].toPetsc(),FELISCE_PETSC_NULLPTR);
  EPSSetDimensions(m_eps,1,PETSC_DECIDE,PETSC_DECIDE);
  EPSSetFromOptions(m_eps);
#endif
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::buildSVD()
{
#ifdef FELISCE_WITH_SLEPC
  // TODO: Replace with this when SLEPc has SVDSetOperators in all versions
  // SVDSetOperators(m_svd,m_Matrix[0].toPetsc(), FELISCE_PETSC_NULLPTR);
#if PETSC_VERSION_LT(3, 19, 0)
  SVDSetOperator(m_svd,m_Matrix[0].toPetsc());
#else
  // TODO: Replace with this when SLEPc has SVDSetOperators in all versions
  SVDSetOperators(m_svd,m_Matrix[0].toPetsc(), FELISCE_PETSC_NULLPTR);
#endif
  SVDSetType(m_svd,SVDCROSS);
  SVDSetFromOptions(m_svd);
#endif
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::solve() 
{
#ifdef FELISCE_WITH_SLEPC
  PetscInt nconv1;
  PetscInt nconv2;
  PetscReal sigma_1 = 0.;
  PetscReal sigma_n = 0.;
  //First request a singular value from one end of the spectrum
  EPSSetWhichEigenpairs(m_eps,EPS_LARGEST_MAGNITUDE);
  if (m_verbose >= 10) {
    m_Matrix[0].view();
    EPSView(m_eps,PETSC_VIEWER_STDOUT_WORLD);
  }

  EPSSolve(m_eps);

  //Get number of converged singular values
  EPSGetConverged(m_eps,&nconv1);
  //Get converged singular values: largest singular value is stored in sigma_1.
  if (nconv1 > 0) {
    EPSGetEigenpair(m_eps,0,&sigma_1,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR);
  } else {
    PetscPrintf(PETSC_COMM_WORLD," Unable to compute large singular value!\n\n");
  }

  //Request a singular value from the other end of the spectrum
  EPSSetWhichEigenpairs(m_eps,EPS_SMALLEST_MAGNITUDE);
  EPSSolve(m_eps);
  //Get number of converged eigenpairs
  EPSGetConverged(m_eps,&nconv2);
  //Get converged singular values: smallest singular value is stored in sigma_n.
  if (nconv2 > 0) {
    EPSGetEigenpair(m_eps,0,&sigma_n,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR,FELISCE_PETSC_NULLPTR);
  } else {
    PetscPrintf(PETSC_COMM_WORLD," Unable to compute small singular value!\n\n");
  }

  /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                        Display solution
    - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  if (nconv1 > 0 && nconv2 > 0) {
    PetscPrintf(PETSC_COMM_WORLD," Computed singular values: sigma_1=%6F, sigma_n=%6F\n",sigma_1,sigma_n);
    PetscPrintf(PETSC_COMM_WORLD," Estimated condition number: sigma_1/sigma_n=%6F\n\n",sigma_1/sigma_n);
  }
#endif
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::solveSVD() 
{
#ifdef FELISCE_WITH_SLEPC
  SVDSetDimensions(m_svd,1,PETSC_DECIDE,PETSC_DECIDE);
  PetscInt nconv1;
  PetscReal sigma_1 = 0.;
  SVDSetWhichSingularTriplets(m_svd,SVD_LARGEST);
  SVDSolve(m_svd);
  SVDGetConverged(m_svd,&nconv1);
  if (nconv1 > 0) {
    SVDGetSingularTriplet(m_svd,0,&sigma_1,nullptr,nullptr);
  } else {
    PetscPrintf(PETSC_COMM_WORLD," Unable to compute large singular value!\n\n");
  }

  PetscInt nconv2;
  PetscReal sigma_n = 0.;
  SVDSetWhichSingularTriplets(m_svd,SVD_SMALLEST);
  SVDSolve(m_svd);
  SVDGetConverged(m_svd,&nconv2);
  if (nconv2 > 0) {
    SVDGetSingularTriplet(m_svd,0,&sigma_n,nullptr,nullptr);
  } else {
    PetscPrintf(PETSC_COMM_WORLD," Unable to compute small singular value!\n\n");
  }

#endif
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::clearMatrix(const int i) {
  m_Matrix[i].setUnfactored();
  m_Matrix[i].zeroEntries();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::gatherSolution() {
  PetscVector vout;
  m_sol.scatterToAll(vout,INSERT_VALUES,SCATTER_FORWARD);
  m_seqSol.copyFrom(vout);
  vout.destroy();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::fromVecToDoubleStar(double* solution, PetscVector& sol, int rank, int dimension, felInt size) {
  IGNORE_UNUSED_ARGUMENT(dimension);

  if (FelisceParam::verbose() >= 40 )
    sol.view();
  felInt sizeVec;
  sol.getSize(&sizeVec);

  // Check if the demanded double* output has the same dimension of the input Vec
  if (size == 0)
    size = sizeVec;

  PetscVector xsol;
  sol.scatterToZero(xsol, INSERT_VALUES, SCATTER_FORWARD);

  felInt iout[size];
  for (felInt i=0; i<size; i++)
    iout[i] = i;

  AOApplicationToPetsc(m_ao, size, iout);

  if(rank == 0) {
    xsol.getValues( size, iout, solution);
  }
  xsol.destroy();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::fromDoubleStarToVec(double* solution, PetscVector& sol, felInt size) {
  felInt ia;
  for (felInt i=0; i< size; i++) {
    ia = i;
    AOApplicationToPetsc(m_ao,1,&ia);
    sol.setValues(1,&ia,&solution[i],INSERT_VALUES);
  }

  sol.assembly();

  if (FelisceParam::verbose() >= 40 )
    sol.view();
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeSolution(int rank, IO& io, double& time, int iteration) 
{
  // to be sure that m_seqSol is up to date
  gatherSolution();

  felInt iout[m_numDof];
  felReal valueVec[m_numDof];
  for (felInt i=0; i<m_numDof; i++)
    iout[i] = i;
  AOApplicationToPetsc(m_ao,m_numDof,iout);
  m_seqSol.getValues(m_numDof,iout,valueVec);

  int idVar = -1;
  int idVar2 = -1;
  // Write solution in ensight format
  if ( rank == 0) {
    double* solution;
    for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++) {
      felInt numValueStart = 0;
      idVar = m_listUnknown.idVariable(iUnknown);
      for ( std::size_t iUnknown2 = 0; iUnknown2 < iUnknown; iUnknown2++) {
        idVar2 = m_listUnknown.idVariable(iUnknown2);
        numValueStart += m_supportDofUnknown[iUnknown2].listNode().size()*m_listVariable[idVar2].numComponent();
      }
      switch (m_listVariable[idVar].numComponent()) {
      case 1:
        solution = new double[m_supportDofUnknown[iUnknown].listNode().size()];
        for (felInt i = 0; i < static_cast<felInt>(m_supportDofUnknown[iUnknown].listNode().size()); i++) {
          solution[i] = valueVec[i+numValueStart];
        }
        io.writeSolution(rank, time, iteration, 0, m_listVariable[idVar].name(), solution, m_supportDofUnknown[iUnknown].listNode().size(), m_mesh->domainDim(),true);
        break;
      case 2:
        solution = new double[m_supportDofUnknown[iUnknown].listNode().size()*3];
        for (felInt i = 0; i < static_cast<felInt>(m_supportDofUnknown[iUnknown].listNode().size()); i++) {
          solution[i*3] = valueVec[i*2+numValueStart];
          solution[i*3+1] = valueVec[i*2+1+numValueStart];
          solution[i*3+2] = 0.;
        }
        io.writeSolution(rank, time, iteration, 1, m_listVariable[idVar].name(), solution, m_supportDofUnknown[iUnknown].listNode().size(), m_mesh->domainDim(),true);
        break;
      case 3:
        solution = new double[m_supportDofUnknown[iUnknown].listNode().size()*3];
        for (felInt i = 0; i < static_cast<felInt>(m_supportDofUnknown[iUnknown].listNode().size()); i++) {
          solution[i*3] = valueVec[i*3+numValueStart];
          solution[i*3+1] = valueVec[i*3+1+numValueStart];
          solution[i*3+2] = valueVec[i*3+2+numValueStart];
        }
        io.writeSolution(rank, time, iteration, 1, m_listVariable[idVar].name(), solution, m_supportDofUnknown[iUnknown].listNode().size(), m_mesh->domainDim(),true);
        break;
      default:
        FEL_ERROR("Problem with the dimension of your solution to write it.")
        break;
      }
      //delete [] solution;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeEnsightVector(double* solValue, int idIter,
                                      std::string varName) 
{
  int rankProc;
  MPI_Comm_rank(m_petscComm,&rankProc);

  std::string iteration;
  if (idIter < 10)
    iteration = "0000" + std::to_string(idIter);
  else if (idIter < 100)
    iteration = "000" + std::to_string(idIter);
  else if (idIter < 1000)
    iteration = "00" + std::to_string(idIter);
  else if (idIter < 10000)
    iteration = "0" + std::to_string(idIter);
  else if (idIter < 100000)
    iteration = std::to_string(idIter);

  std::string fileName = FelisceParam::instance().resultDir + "/" + varName + "." + iteration + ".scl";
  FILE * solFile;
  if (rankProc == 0) {
    solFile = fopen(fileName.c_str(),"w");
    fprintf(solFile, "Scalar per node\n");
    int count = 0;
    for(int j=0; j < m_numDof; j++) {
      fprintf(solFile,"%12.5e", solValue[j]);
      count++;
      if(count == 6) {
        fprintf(solFile,"\n");
        count = 0;
      }
    }
    fprintf(solFile,"\n");
    fclose(solFile);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeEnsightCase(const int numIt, 
              const double dt, std::string varName, double time0) 
{
  FILE * pFile;
  std::string caseName = FelisceParam::instance().resultDir + "/" + varName + ".case";
  pFile = fopen(caseName.c_str(),"w");
  fprintf(pFile,"FORMAT\n");
  fprintf(pFile,"type: ensight\n");
  fprintf(pFile, "GEOMETRY\n");
  std::string nameGeo = "model: 1 " + FelisceParam::instance().outputMesh[0] + "\n";
  fprintf(pFile,"%s", nameGeo.c_str());
  fprintf(pFile, "VARIABLE\n");
  std::string secName = "scalar per node: 1 " + varName + " " + varName +".*****.scl\n";

  fprintf(pFile, "%s", secName.c_str());
  fprintf(pFile, "TIME\n");
  fprintf(pFile, "time std::set: %d \n", 1);
  fprintf(pFile, "number of steps: %d \n", numIt);
  fprintf(pFile, "filename start number: %d\n", 0);
  fprintf(pFile, "filename increment: %d\n", 1);
  fprintf(pFile, "time values:\n");

  int count=0;
  for(int j=0; j < numIt; j++) {
    double valT = time0 + j*dt;
    fprintf(pFile,"%lf ",valT);
    count++;
    if(count == 6) {
      fprintf(pFile,"\n");
      count=0;
    }
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeEnsightCase(const int numIt, 
                                    const double dt,
                                    std::vector<std::string> varName, 
                                    double time0) 
{
  FILE * pFile;
  std::string caseName = FelisceParam::instance().resultDir + "/" 
                                      + varName[0] + ".case";
  pFile = fopen(caseName.c_str(),"w");
  fprintf(pFile,"FORMAT\n");
  fprintf(pFile,"type: ensight\n");
  fprintf(pFile, "GEOMETRY\n");
  std::string nameGeo = "model: 1 " +	FelisceParam::instance().outputMesh[0] + "\n";
  fprintf(pFile,"%s", nameGeo.c_str());
  fprintf(pFile, "VARIABLE\n");

  std::string secName;
  for (std::size_t i=0; i<varName.size(); i++) {
    secName = "scalar per node: 1 " + varName[i] + " " + varName[i] +".*****.scl\n";
    fprintf(pFile, "%s", secName.c_str());
  }
  fprintf(pFile, "TIME\n");
  fprintf(pFile, "time std::set: %d \n", 1);
  fprintf(pFile, "number of steps: %d \n", numIt);
  fprintf(pFile, "filename start number: %d\n", 0);
  fprintf(pFile, "filename increment: %d\n", 1);
  fprintf(pFile, "time values:\n");

  int count=0;
  for(int j=0; j < numIt; j++) {
    double valT = time0 + j*dt;
    fprintf(pFile,"%lf ",valT);
    count++;
    if(count == 6) {
      fprintf(pFile,"\n");
      count=0;
    }
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::writeGeoForUnknown(int iUnknown) 
{
  // Intialization
  // =============
  std::string fileNameGeo, fileNameRoot, fileName;
  ElementType eltType;
  Ensight ens(FelisceParam::instance().inputDirectory,FelisceParam::instance().inputFile,FelisceParam::instance().inputMesh[0],FelisceParam::instance().outputMesh[0],FelisceParam::instance().meshDir,FelisceParam::instance().resultDir);
  std::string keyWord;
  int the_ref;
  short verbose = 0;
  FILE * pFile;
  //    int  nVpEl = 0;
  //    felInt startindex = 0;
  felInt numElemsPerRef = 0;
  felInt ielSupportDof = 0;
  std::vector<felInt> elem;
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }
  const int idVar = m_listUnknown.idVariable(iUnknown);
  fileNameGeo = listVariable().listVariable()[idVar].name()+".geo";
  fileNameRoot = FelisceParam::instance().resultDir;
  fileName = fileNameRoot + fileNameGeo;
  if (verbose>0) std::cout << "Writing file "<< fileName << std::endl;

  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    std::string command = "mkdir -p " + FelisceParam::instance().resultDir;
    int ierr = system(command.c_str());
    if (ierr>0) {
      FEL_ERROR("Impossible to write "+fileName+"  geo mesh");
    }
    pFile = fopen (fileName.c_str(),"w");
  }
  //! Descriptions lines (All Geometry files must contain these first six lines)
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"node id assign\n");
  fprintf(pFile,"element id assign\n");
  fprintf(pFile,"coordinates\n");
  fprintf(pFile,"%8d\n",(int)m_supportDofUnknown[iUnknown].listNode().size());

  //! write the vertices
  if ( m_mesh->numCoor() == 3 ) {
    for(felInt iv=0; iv< static_cast<felInt>(m_supportDofUnknown[iUnknown].listNode().size()); iv++) {
      fprintf( pFile,"%12.5e%12.5e%12.5e\n", m_supportDofUnknown[iUnknown].listNode()[iv].x(),
                m_supportDofUnknown[iUnknown].listNode()[iv].y(),m_supportDofUnknown[iUnknown].listNode()[iv].z() );
    }
  } else if ( m_mesh->numCoor() == 2 ) {
    double zcoor = 0.;
    for(felInt iv=0; iv< static_cast<felInt>(m_supportDofUnknown[iUnknown].listNode().size()); iv++) {
      fprintf( pFile, "%12.5e%12.5e%12.5e\n", m_supportDofUnknown[iUnknown].listNode()[iv].x(), m_supportDofUnknown[iUnknown].listNode()[iv].y(), zcoor);
    }
  } else {
    std::ostringstream msg;
    msg << "Error in writing geo File " << fileNameGeo << ". "
        << "Expecting mesh with 2 or 3 coordinates (it is " << m_mesh->numCoor() << ")."
        << std::endl;
    FEL_ERROR(msg.str());
  }

  //!write the elements per references.
  //------------
  felInt cptpart = 0;
  std::string DescriptionLine,felName;
  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  m_mesh->intRefToEnum();
  for(auto it_ref = intRefToEnum.begin();
        it_ref != intRefToEnum.end(); ++it_ref) {
    the_ref = (*it_ref).first;

    for(auto it_elem = it_ref->second.begin();
          it_elem != it_ref->second.end(); ++it_elem) {
      eltType = *it_elem;
      if (m_mesh->flagFormatMesh() == GeometricMeshRegion::FormatMedit) {
        cptpart++;
        fprintf ( pFile, "part%8d\n",cptpart );
        if ( m_listVariable[idVar].finiteElementType() == 0 )
          felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
        else if ( m_listVariable[idVar].finiteElementType() == 1 )
          felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltQuad[eltType]].first;
        else {
          if(m_mesh->domainDim() == GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numCoor())
            felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltBubble[eltType]].first;
          else
            felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
        }

        auto it_felName = FelisceParam::instance().felName2MapintRef2DescLine.find(felName);
        if ( it_felName == FelisceParam::instance().felName2MapintRef2DescLine.end() ) {
          //std::cout << "Warning: ----------->trying to access an unexisting element: " << felName << " in input file !!!!!!!" << std::endl;
          DescriptionLine = felName + "m_ref_" + std::to_string(the_ref);
        } else {
          auto it_ref2 = it_felName->second.find(the_ref);
          if ( it_ref2 == 	it_felName->second.end() ) {
            /*std::cout << "Warning: trying to access an unexisting reference: " << the_ref << "for element : "
              << felName << " in input file !!!!! " << std::endl << std::flush;*/
            DescriptionLine = felName + "m_ref_" + std::to_string(the_ref);
          } else {
            DescriptionLine = it_ref2->second;
          }
        }
        fprintf (pFile, "%s", DescriptionLine.c_str() );
      }
      fprintf(pFile,"\n");
      keyWord = ens.eltFelNameToEns6Pair()[felName].second;
      fprintf( pFile, "%s", keyWord.c_str() );
      fprintf(pFile,"\n");
      numElemsPerRef =m_mesh->intRefToBegEndMaps[eltType][the_ref].second;
      //        startindex =m_mesh->intRefToBegEndMaps[eltType][the_ref].first;
      fprintf(pFile,"%8d\n", numElemsPerRef);
      //        nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
      for ( felInt iel = 0; iel < numElemsPerRef; iel++) {
        m_supportDofUnknown[iUnknown].getIdElementSupport(eltType, numElement[eltType], ielSupportDof);
        elem.resize(m_supportDofUnknown[iUnknown].getNumSupportDof(ielSupportDof),0);
        for ( int iSupport = 0; iSupport < m_supportDofUnknown[iUnknown].getNumSupportDof(ielSupportDof); iSupport++) {
          elem[iSupport] = m_supportDofUnknown[iUnknown].iSupportDof()[ m_supportDofUnknown[iUnknown].iEle()[ielSupportDof] + iSupport];
        }
        for (std::size_t ivert = 0; ivert < elem.size(); ivert++) {
          fprintf( pFile, "%8d", elem[ivert]+1);
        }
        fprintf(pFile,"\n");
        numElement[eltType]++;
      }
    }
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void EigenProblem::findCoordinateWithIdDof(felInt i, Point& pt) 
{
  felInt inf = 0;
  felInt sup = 0;
  int idUnknown = -1;
  int idVar = -1;
  bool findUnknown = false;
  for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size() && findUnknown == false; iUnknown++) {
    if (iUnknown == 0) {
      inf = 0;
      sup = m_numDofUnknown[iUnknown];
    } else {
      inf += m_numDofUnknown[iUnknown-1];
      if (iUnknown == m_listUnknown.size()-1)
        sup = m_numDof;
      else
        sup += m_numDofUnknown[iUnknown];
    }

    if (inf <= i && i < sup) {
      idUnknown = iUnknown;
      findUnknown = true;
    }
  }
  const felInt idDofPerUnknown = i - inf;
  felInt idSupportDof = -1;
  idVar = m_listUnknown.idVariable(idUnknown);
  idSupportDof = idDofPerUnknown/m_listVariable[idVar].numComponent();
  pt = m_supportDofUnknown[idUnknown].listNode()[idSupportDof];
}

}
