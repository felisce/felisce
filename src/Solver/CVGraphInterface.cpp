//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: J. Foulon, S. Smaldone
//

// System includes

// External includes

// Project includes
#include "Solver/CVGraphInterface.hpp"


namespace felisce 
{
CVGraphInterface::CVGraphInterface()
{
}

/***********************************************************************************/
/***********************************************************************************/

CVGraphInterface::~CVGraphInterface() 
{
  if (m_listVariable)
    delete [] m_listVariable;
  if (m_listOfNodeInInterface)
    delete [] m_listOfNodeInInterface;
  if (m_TotSolToImpose)
    delete [] m_TotSolToImpose;
  if (m_StoredSolution)
    delete [] m_StoredSolution;
  if (m_SolutionToImpose)
    delete [] m_SolutionToImpose;
  if (m_ComputedVariableToImpose)
    delete [] m_ComputedVariableToImpose;
  if (m_listOfDofInInterface)
    delete [] m_listOfDofInInterface;
  if (m_listOfValuesInInterface)
    delete [] m_listOfValuesInInterface;
  if (m_listOfDofForVariable)
    delete [] m_listOfDofForVariable;
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::initialize(ListVariable* listVariable, const std::string& fileName) 
{
  m_listVariable = listVariable;
  //Read interface file.
  FILE *pFile;
  pFile = fopen(fileName.c_str(),"r");
  if (pFile==nullptr) {
    FEL_WARNING("Impossible to read interface file: "+fileName+".");
  } else {
    //Read supportDof in interface.
    if (! fscanf(pFile, "%d", &m_totalNumberOfNode) )
      FEL_ERROR("totalNumberOfNode in interface not scanned in file.");
    if (!m_totalNumberOfNode) {
      FEL_ERROR("Your interface is empty.\n");
    }

    if (m_listOfNodeInInterface == nullptr)
      m_listOfNodeInInterface = new felInt[m_totalNumberOfNode];

    felInt id = 0;
    for (felInt indc = 0; indc<m_totalNumberOfNode; indc++) {
      if (! fscanf(pFile, "%d", &id) )
        FEL_ERROR("id of interface supportDof not scanned in file.");
      m_listOfNodeInInterface[indc] = id - 1;
    }

    // We count how many dof a single node is supporting.
    // For instance for NS in 3D we have 3+1 -> m_numberOfDofBySupport = 4
    // What about the case of P2-P1? Some of the node will support only the velocity -> 3
    // and some others also the pressure -> 4
    for (unsigned int iVar = 0; iVar < m_listVariable->listVariable().size(); iVar++) {
      m_numberOfDofBySupport += m_listVariable->listVariable()[iVar].numComponent();
    }

    // Number of total dof at the interface
    m_TotNumDofOnInterface = m_numberOfDofBySupport*m_totalNumberOfNode;
    // Arrays allocation
    if (m_listOfValuesInInterface == nullptr)
      m_listOfValuesInInterface = new double[m_TotNumDofOnInterface];
    if (m_StoredSolution == nullptr)
      m_StoredSolution = new double[m_TotNumDofOnInterface];
    // Arrays initialization (to zero)
    for(felInt il=0; il<m_TotNumDofOnInterface; il++) {
      m_listOfValuesInInterface[il]=0;
      m_StoredSolution[il]=0;
    }
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::identifyIdDof(Dof& dof) 
{
  // Alocation of vectors
  // This vector will contain all the id of the dofs, component wise.
  if (m_listOfDofInInterface == nullptr)
    m_listOfDofInInterface = new felInt[m_numberOfDofBySupport*m_totalNumberOfNode];
  std::vector<felInt> idDofBySupport;
  felInt cptComponent = 0;

  // Each variable dofs start in a different position
  felInt indiceStart[m_listVariable->listVariable().size()];
  indiceStart[0] = 0;
  for (unsigned int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++) {
    indiceStart[iVar] = indiceStart[iVar-1] + (m_totalNumberOfNode-1)*m_listVariable->listVariable()[iVar-1].numComponent(); //why -1?
  }

  // for each node (or supportDof)
  for (felInt isd = 0; isd<m_totalNumberOfNode; isd++) {
    // Get the list of dofs (var1comp1,var1comp2,var1comp3,..,var2Comp1,..)
    dof.supportDofToDof(m_listOfNodeInInterface[isd],idDofBySupport);
    for (unsigned int iVar = 0; iVar < m_listVariable->listVariable().size(); iVar++) {
      for (std::size_t iComp = 0; iComp < m_listVariable->listVariable()[iVar].numComponent(); iComp++) {
        m_listOfDofInInterface[indiceStart[iVar]+m_listVariable->listVariable()[iVar].numComponent()*isd+cptComponent] = idDofBySupport[cptComponent];
        cptComponent++;
      }
    }
    idDofBySupport.clear();
    cptComponent = 0;
  }
}

/***********************************************************************************/
/***********************************************************************************/

felInt* CVGraphInterface::listOfDofInInterfaceForVariable(int iVariable) 
{
  if (m_listOfDofForVariable == nullptr) {
    identifyIdDofForVariable();
  }
  return m_listOfDofForVariable[iVariable];
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::identifyIdDofForVariable() 
{
  if (m_listOfDofForVariable == nullptr) {
    m_listOfDofForVariable = new felInt*[m_listVariable->listVariable().size()];
    for (unsigned int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++)
      m_listOfDofForVariable[iVar] = nullptr;
  }

  int  indiceStart[m_listVariable->listVariable().size()];
  indiceStart[0] = 0;
  for (unsigned  int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++) {
    indiceStart[iVar] = indiceStart[iVar-1] + m_totalNumberOfNode*m_listVariable->listVariable()[iVar-1].numComponent();
  }

  for (unsigned int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++) {

    felInt VarNumComp = m_listVariable->listVariable()[iVar].numComponent();
    m_SizeListOfDofForVariable = VarNumComp*m_totalNumberOfNode;

    if (m_listOfDofForVariable[iVar] == nullptr)
      m_listOfDofForVariable[iVar] = new felInt[m_SizeListOfDofForVariable];

    for (felInt iv = 0; iv < m_SizeListOfDofForVariable; iv++) {
      m_listOfDofForVariable[iVar][iv] = m_listOfDofInInterface[iv+indiceStart[iVar]];
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::ImposeSolution(double* ImpValue) 
{
  felInt VarNumComp = m_listVariable->listVariable()[0].numComponent();
  m_SizeListOfDofForVariable = VarNumComp*m_totalNumberOfNode;
  if (m_TotSolToImpose == nullptr) {
    m_TotSolToImpose = new double[m_SizeListOfDofForVariable];
  }
  m_TotSolToImpose=ImpValue;
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::StoreSolution(double* StoringValue) 
{
  m_StoredSolution=StoringValue;
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::ImposeSolutionAndComputedVariable(double* ImpValue, int idVariable)
{
  felInt VarNumComp = m_listVariable->listVariable()[idVariable].numComponent();
  m_SizeListOfDofForVariable = VarNumComp*m_totalNumberOfNode;

  // Vector with the solution at interface
  if (m_SolutionToImpose == nullptr)
    m_SolutionToImpose = new double[m_SizeListOfDofForVariable];
  //Vector with the computed variable at interface
  if (m_ComputedVariableToImpose == nullptr)
    m_ComputedVariableToImpose = new double[m_SizeListOfDofForVariable];

  for(int is=0; is<m_SizeListOfDofForVariable; is++) {
    m_SolutionToImpose[is]=ImpValue[is];
  }

  for(int ik=0; ik<m_SizeListOfDofForVariable; ik++) {
    m_ComputedVariableToImpose[ik]=ImpValue[m_SizeListOfDofForVariable+ik];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::ExtractSolution(PetscVector& VecOfSol, double* valueDofInterface) 
{
  VecOfSol.getValues( m_numberOfDofBySupport * m_totalNumberOfNode,
                      m_listOfDofInInterface, valueDofInterface);
  m_listOfValuesInInterface = valueDofInterface;
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::ExtractSolution( PetscVector& VecOfSol, double*  valueDofInterface, int idVariable) 
{
  int  indiceStart[m_listVariable->listVariable().size()];
  indiceStart[0] = 0;
  for (unsigned  int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++) {
    indiceStart[iVar] = indiceStart[iVar-1] +
                        m_totalNumberOfNode*m_listVariable->listVariable()[iVar-1].numComponent();
  }

  m_SizeListOfDofForVariable = m_listVariable->listVariable()[idVariable].numComponent()*m_totalNumberOfNode;
  felInt listOfDofForVariable[m_SizeListOfDofForVariable];

  for (felInt iv = 0; iv < m_SizeListOfDofForVariable; iv++) {
    listOfDofForVariable[iv] = m_listOfDofInInterface[iv+indiceStart[idVariable]];
  }

  VecOfSol.getValues(m_SizeListOfDofForVariable,listOfDofForVariable,valueDofInterface);
  m_listOfValuesInInterface = valueDofInterface;
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::ExtractSolutionAndComputedVariable( PetscVector& VecOfSol, std::vector<double>& VecComputedVar,
    double* valueDofInterface, int idVariable) 
{
  felInt VarNumComp = m_listVariable->listVariable()[idVariable].numComponent();
  m_SizeListOfDofForVariable = VarNumComp*m_totalNumberOfNode;
  double VariableSolution[m_SizeListOfDofForVariable];
  felInt listOfDofForVariable[m_SizeListOfDofForVariable];
  std::vector<double> ComputedVariable(m_SizeListOfDofForVariable,0);

  //----take only the the solution with IdVaraible VecOfSol
  int indiceStart[m_listVariable->listVariable().size()];
  indiceStart[0] = 0;
  for (unsigned  int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++) {
    indiceStart[iVar] = indiceStart[iVar-1] +
                        m_totalNumberOfNode*m_listVariable->listVariable()[iVar-1].numComponent();
  }
  for (felInt iz = 0; iz < m_SizeListOfDofForVariable; iz++) {
    listOfDofForVariable[iz] = m_listOfDofInInterface[iz+indiceStart[idVariable]];
  }
  VecOfSol.getValues(m_SizeListOfDofForVariable,listOfDofForVariable,VariableSolution);
  m_listOfValuesInInterface=VariableSolution;

  //----------put the computed varaible in the sencond part of the std::vector------
  for(int ia=0; ia<m_SizeListOfDofForVariable; ia++) {
    ComputedVariable[ia] = VecComputedVar[listOfDofForVariable[ia]];
  }

  for(int ib=0; ib<m_SizeListOfDofForVariable; ib++) {
    valueDofInterface[ib] = VariableSolution[ib];
  }
  for(int ic=0; ic<m_SizeListOfDofForVariable; ic++) {
    valueDofInterface[m_SizeListOfDofForVariable + ic] = ComputedVariable[ic];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CVGraphInterface::print(int verbose, std::ostream& c) const 
{
  verbose = 1;
  if (verbose) {
    c << "Welcome in the interface.\n";
    if (m_listOfNodeInInterface) {
      c << "Your list of nodes on the interface ( Total number of nodes = "<<m_totalNumberOfNode<< " ): ";
      for (felInt ie = 0; ie < m_totalNumberOfNode; ie++) {
        c << m_listOfNodeInInterface[ie]<< " ";
      }
      c << std::endl;
    }

    int sizeVar = 0;
    felInt  indiceStart[m_listVariable->listVariable().size()];
    indiceStart[0] = 0;
    for (unsigned  int iVar = 1; iVar < m_listVariable->listVariable().size(); iVar++) {
      indiceStart[iVar] = indiceStart[iVar-1] +
                          m_totalNumberOfNode*m_listVariable->listVariable()[iVar-1].numComponent();
    }

    if (m_listOfDofInInterface) {
      for (unsigned int iVar = 0; iVar < m_listVariable->listVariable().size(); iVar++) {
        sizeVar = m_totalNumberOfNode*m_listVariable->listVariable()[iVar].numComponent();
        c << "Dof in interface for " << m_listVariable->listVariable()[iVar].name() << ": \n";
        for (int il = 0; il < sizeVar; il++) {
          c << m_listOfDofInInterface[il+indiceStart[iVar]] << "  ";
        }
        c << std::endl;
        c<<"With values: "<<std::endl;
        for (int im = 0; im < sizeVar; im++) {
          c <<  m_listOfValuesInInterface[im+indiceStart[iVar]] << "  ";
        }
        c << std::endl;
      }
    }
  }
}
}
