//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef _LINEARPROBLEMELASTICITY_DYNAMIC_HPP
#define _LINEARPROBLEMELASTICITY_DYNAMIC_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "FiniteElement/elementField.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

// Forward declaration.
class FelisceTransient;

///@}
///@name  Enum's
///@{

namespace ElasticityNS
{

  enum MatrixIndex {
    current_displacement = 1,
    current_velocity = 2
  };

  enum ForceType {
    volumic = 1,
    surfacic = 2
  };

  /*!
    * \brief Tells which part of the system is being solved.
    *
    * There is first a static solve, and then we go to the dynamic resolution.
    */
  enum System {
    static_,
    dynamic_
  };

} // namespace ElasticityNS

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

class LinearProblemElasticityDynamic
  : public LinearProblem
{
public:
  ///@name Type Definitions
  ///@{

  /*!
    * \brief Convenient typedef over the type of a spatial function
    *
    * Arguments are:
    *   first -> index of the component considered (0 for x, 1 for y, 2 for z)
    *   second-> x
    *   third -> y
    *   fourth-> z
    */
  typedef std::function<double(int, double, double, double)> SpatialForce;

  ///@}
  ///@name Life Cycle
  ///@{

  /*!
    * \brief Constructor
    *
    * \param[in] force Spatial force to apply
    */
  LinearProblemElasticityDynamic(SpatialForce force);

  /// Destructor
  ~LinearProblemElasticityDynamic() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /// Overload of Felisce namesake method.
  void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES = true) override;

  /// Overload of Felisce namesake method.
  void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

  /// Overload of Felisce namesake method.
  void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, int& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

  /// Overload of Felisce namesake method.
  void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, int& iel, FlagMatrixRHS flagMatrixRHS) override;

  /// Overload of Felisce namesake method
  void InitializeDerivedAttributes() override;

  /// Update for next time step. (not called after each dynamic iteration).
  void endIteration() override;

  /// Update current displacement. Already called in endIteration().
  void UpdateCurrentDisplacement();

  /*!
    * \brief Compute the RHS part in Newmark scheme.
    *
    * Only useful to solve dynamic problems.
    *
    * In Newmark scheme matrices are assembled once and for all; at each iteration basically there is:
    * . Calculation of the new RHS, which involves simple matrix algebra. THis is the role fo current method.
    * . Apply boundary conditions. RHS might still be modified here!
    * . Solve the system.
    */
  void ComputeRHS();

  /// Tells the problem the static resolution is done. Useful only when solving dynamic problems.
  void GoToDynamic();

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations

  /// Assign the type of force applied from the integer read in the data file
  void setTypeOfForceApplied();

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  /// Index related to displacement.
  int m_iDisplacement;

  /// Finite element related to displacement.
  CurrentFiniteElement* m_feDisplacement;

  /// Field element. Used to give the force to Felisce in case of a volumic force.
  ElementField m_elemField;

  /// As its name says...
  UBlasMatrix m_gradientBasedElastTensor;

  /// Whether the applied force is volumic or surfacic.
  ElasticityNS::ForceType m_typeOfForceApplied;

  /// The force applied, when it is a spatial one (constant ones are handled differently in Felisce).
  SpatialForce m_force;

  /// Felisce transient parameters.
  FelisceTransient::Pointer transient_parameters_;

  /// Velocity from previous iteration.
  PetscVector current_velocity_;

  /// Displacement from previous iteration.
  PetscVector current_displacement_;

  /// Whether we are in static or dynamic part of the resolution.
  ElasticityNS::System system_;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif
