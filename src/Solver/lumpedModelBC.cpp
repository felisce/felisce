//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Solver/lumpedModelBC.hpp"

namespace felisce {

  LumpedModelBC::LumpedModelBC(FelisceTransient::Pointer fstransient):
    m_fstransient(fstransient) 
  {
    m_size = FelisceParam::instance().lumpedModelBCLabel.size();
    m_Rd.resize(m_size);
    m_Rp.resize(m_size);
    m_C.resize(m_size);
    m_Pv.resize(m_size);
    m_Pd0.resize(m_size);
    m_Q.resize(m_size);
    m_Pd.resize(m_size);
    m_PdOld.resize(m_size);
    m_Pp.resize(m_size);
    m_PpOld.resize(m_size);
    m_name.resize(m_size);

    for(std::size_t i=0; i<m_size; i++) {
      m_Rp[i] = FelisceParam::instance().lumpedModelBC_Rprox[i];
      m_C[i] = FelisceParam::instance().lumpedModelBC_C[i];
      m_Pv[i] = FelisceParam::instance().lumpedModelBC_Pvenous[i];
      m_Pd0[i] = FelisceParam::instance().lumpedModelBC_Pdist_init[i];
      m_Q[i] = 0.;
      m_Pd[i] = m_Pd0[i];
      m_PdOld[i] = 0.;
      m_Pp[i] = m_Pd0[i];
      switch ( FelisceParam::instance().lumpedModelBCType[i] ) {
      case 1: // RCR
        m_Rd[i] = FelisceParam::instance().lumpedModelBC_Rdist[i];
        break;
      case 2: // RC
        if ( Tools::notEqual(FelisceParam::instance().lumpedModelBC_Rdist[i],0.) )
          FEL_WARNING("RC lumpedModelBC bc used but Rd is not equal to 0 for all the outlet\n");
        break;
      }
      m_name[i] = FelisceParam::instance().lumpedModelBCName[i];
    }

    if ( FelisceParam::instance().lumpedModelBC_C_type == 2 ) { // non linear compliance : C=C(V)
      m_volume_n_1 = FelisceParam::instance().lumpedModelBC_volume0 ;
      m_volumeMax = FelisceParam::instance().lumpedModelBC_volumeMax ;
      m_volumeMin = FelisceParam::instance().lumpedModelBC_volumeMin ;
      m_volume0 = FelisceParam::instance().lumpedModelBC_volume0 ;
      m_volume0ref = FelisceParam::instance().lumpedModelBC_referenceVolume0 ;
      m_powerNonLinearCompliance = FelisceParam::instance().powerNonLinearCompliance ;
    }
  }

  

  void LumpedModelBC::iterate() {
    //
    // update the m_Q[i] before
    //
    double dt = m_fstransient->timeStep;
    double tau;
    if ( FelisceParam::instance().lumpedModelBC_C_type == 2 ) { // non linear compliance : C=C(V)
      for( std::size_t idBCwindk=0 ; idBCwindk<m_size ; idBCwindk++ ) {
        m_C[idBCwindk] = computeNonLinearCompliance(idBCwindk);
      }
    }
    
    for(std::size_t i=0; i<m_size; i++) {
      m_PdOld[i] = m_Pd[i];
      m_PpOld[i] = m_Pp[i];
      switch ( FelisceParam::instance().lumpedModelBCType[i] ) {
      case 1: // RCR
        tau = m_Rd[i]*m_C[i];
        m_Pd[i]= ((tau/dt) * m_PdOld[i] +  ( m_Pv[i] + m_Rd[i] * m_Q[i]) ) / (1+tau/dt);
        break;
      case 2: // RC
        m_Pd[i]= m_PdOld[i] +  (dt/m_C[i]) * m_Q[i] ;
        break;
      }
      m_Pp[i] = m_Pd[i] + m_Rp[i]*m_Q[i];
    }
  }

  void LumpedModelBC::print(int verbose,std::ostream& c) const {
    IGNORE_UNUSED_VERBOSE;
    for(std::size_t i=0; i<m_size; i++) {
      c << "LumpedModelBC " << i << std::endl;
      c << "  Type lumpedModelBC = " ;
      switch ( FelisceParam::instance().lumpedModelBCType[i] ) {
      case 1: // RCR
        c << "RCR or R" << std::endl;
        break;
      case 2: // RC
        c << "RC" << std::endl;
        break;
      }
      c << "  Algo lumpedModelBC = " ;
      switch ( FelisceParam::instance().lumpedModelBCAlgo[i]) {
      case 1:
        c << "explicit" << std::endl;
        break;
      case 2:
        c << "implicit" << std::endl;
        break;
      }

      c << "LumpedModelBC " << i << std::endl;
      c << "    Rd = " << m_Rd[i] << std::endl;
      c << "    Rp = " << m_Rp[i] << std::endl;
      c << "     C = " << m_C[i] << std::endl;
      c << "    Pv = " << m_Pv[i] << std::endl;
      c << "   Pd0 = " << m_Pd0[i] << std::endl;
    }

  }

  // non linear compliance
  //======================

  double LumpedModelBC::computeNonLinearCompliance(std::size_t& idBCwindk) {
    m_volumeMaxTilde = 2*m_volume0ref - m_volumeMin;
    m_volumeMinTilde = 2*m_volume0ref - m_volumeMax;
    if ( ( m_volumeMin <= m_volume_n_1 ) && ( m_volume_n_1  <= m_volume0ref ) ) {
      return FelisceParam::instance().lumpedModelBC_C[idBCwindk] * std::pow( ((m_volumeMaxTilde - m_volume_n_1 )*( m_volume_n_1 - m_volumeMin )) / ( ( m_volumeMaxTilde - m_volume0ref )*( m_volume0ref - m_volumeMin ) ) ,m_powerNonLinearCompliance);
    } else if ( (m_volumeMax > m_volume_n_1) && (m_volume_n_1  > m_volume0ref) ) {
      return FelisceParam::instance().lumpedModelBC_C[idBCwindk] * std::pow( ((m_volumeMax - m_volume_n_1 )* (m_volume_n_1 - m_volumeMinTilde )) / ( ( m_volumeMax - m_volume0ref )*( m_volume0ref - m_volumeMinTilde ) ) , m_powerNonLinearCompliance);
    } else {
      return 0.;
    }
  }

  void LumpedModelBC::write(std::string filename) const {
    static int init = 1;
    std::cout << "Write lumped parameter model on '" << filename << "'"<< std::endl;
    std::ofstream outputfile;
    if(init) {
      outputfile.open((filename.c_str()));
      outputfile << "# 1:time ";
      int j=2;
      for(std::size_t i=0; i<m_size; i++) {
        outputfile << j << ":Q_" << m_name[i] << " ";
        j++;
        outputfile << j << ":Pp_" << m_name[i] << " ";
        j++;
        outputfile << j << ":Pd_" << m_name[i] << " ";
        j++;
      }
      outputfile << std::endl;
    } else {
      outputfile.open((filename.c_str()),std::ios::app);
    }
    outputfile << m_fstransient->time << " ";
    for(std::size_t i=0; i<m_size; i++) {
      outputfile << m_Q[i] << " " << m_Pp[i] << " " << m_Pd[i] << " ";
    }
    outputfile << std::endl;
    init = 0;
  }
}

