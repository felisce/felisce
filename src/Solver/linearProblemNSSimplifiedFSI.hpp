//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _LinearProblemNSSimplifiedFSI_HPP
#define _LinearProblemNSSimplifiedFSI_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNS.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTools.hpp"
#include "FiniteElement/elementField.hpp"
#include "Solver/autoregulation.hpp"
#include "Geometry/curvatures.hpp"

namespace felisce {

  class LinearProblemNSSimplifiedFSI:
    public LinearProblemNS {
    /*! 
      @{\name Generic 
    */
  public:
    LinearProblemNSSimplifiedFSI();
    /*! \brief  destructor
      Do nothing-destructor
     */
    ~LinearProblemNSSimplifiedFSI() override= default;;
    /*!@}*/
    /*!
      @{ \name Boundary condition
     */
  public:
    void userElementInitNaturalBoundaryCondition() override;
    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;
    void applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) override;
    //!\brief getter of #m_lumpedModelBC
    inline LumpedModelBC* lumpedModelBC() { return m_lumpedModelBC; }
  protected:
    std::unordered_map<std::string,std::vector<int> > m_label; //< ids of inlet,outlet,lateral surface
    std::vector<felInt> m_embedLabels;
    /*!@}*/
    /*!
      @{ \name Structure methods
    */
  public:
    void initStructure();
    void computeCurvatures();
    void updateStructure();
    void updateNormalVelocityAndCurrentDisplacement();
    void computeScalarDisplacement(double& max, double& min);
  private:
    void computeGradUN(CurrentFiniteElementWithBd* fe ,felInt iel,felInt idVariable, std::vector<double> &gradUdotNdotN, int ibd, int icomp);
    void computeMeanGradUN();
    void computeTangentComponent();
    void computeScalarNormalVelocity();
    /*!@}*/
    /*!
      @{ \name Structure properties and parameters
     */
  public:
    double m_betaS;  //!<
    double m_deltaT; //!<  \brief  time step
    double m_fiberTensPassive; //!< \brief  \f$k_{0,ref}\f$ the reference part of the fiber pre-stress
    double m_fiberYoung; //!<  \brief \f$k_{1,ref}\f$ the reference part of the fiber elastic modulus
    std::vector<double> m_fiberDensity; //!< \brief Density of fibers by label (coefficient multiplying (m_fiberTensPassive+extraTension) and m_fiberYoung). The list is ordered as the labels defining the EmbedFSI type of BC.
    std::map<int,int> m_label2embedFSI; //!< \brief gives the rank in the EmbedFSI list of a given label
  private:
    double m_densityS; //!< \brief density of the entire structure
    double m_viscS; //!< \brief viscosity of the structure (not tested)
    double m_widthS; //!< \brief entire thickness of the structure TODO change name
    double m_youngS; //!< \brief young modulus of the koiter shell
    double m_poissonS; //!< \brief poisson ratio of the entire structure
    double m_alphaS;  //!< 
    int m_nonLinear; //!< \brief 1: non-linear model, 0: linear model
  protected:
    double m_radius; //!< \brief radius of the structure (only in 2D)
    /*!@}*/
    /*!
      @{ \name Autoregulation
    */
  public:
    void handleWindkessel( const double& t);
    double control( const double& t);
    void updateControl (const double& time, const int& rank);
    void exportControl (const int& rank);
    inline Autoregulation* getAutoregulation() { return &m_autoregulation; }
    void initializeAutoregulation( const int& nc );
    inline void autoregulated( const bool& autoreg ){ m_autoregulated = autoreg; }
    inline bool const& autoregulated() { return m_autoregulated;}
  private:
    double m_referencePressure; //!< \brief the pressure associated with no control
    bool m_autoregulated; //!< \brief wether we are using autoregulation or not
  protected:
    Autoregulation m_autoregulation; //!< \brief class to handle autoregulation
    /*!@}*/
    /*!
      @{ \name all the available computers: the functionOfTheLoop that can be called with function assemblyLoopBoundary()
     */
    void scalarComputer(felInt ielSupportDof);
    void meanGradUNComputer(felInt ielSupportDof);
    void normalVelocityScalarComputer(felInt ielSupportDof);
    void curvaturesComputer(felInt ielSupportDof);
    void normL2BoundaryDifferenceComputer(felInt ielSupportDof);
    void scalarProductComputer(felInt ielSupportDof);
    double computeL2ScalarProduct(std::string l, std::string r);
    void testQuantitiesComputer(felInt ielSupportDof);
    void testQuantitiesLumpedComputer(felInt ielSupportDof);
    
    /*!@}*/
    /*! 
      @{ \name Coupling with IOP
    */
  public:
    void initializeDofBoundaryAndBD2VolMaps();
    inline void coupled( const bool& coupled ){ m_iopCoupling = coupled; }
    inline bool const& coupled() { return m_iopCoupling; }
    void readIOPInterface(std::map<int,std::map<int, int> >  mapIOP, PetscVector& seqCurrentIOP, std::map<int,int> labelMap, felInt rank);
    double computeL2Difference(std::string current, std::string old = "zero");
    void resetInterfaceQuantities();
    std::pair<double,double> computeTestQuantities( bool lumped );
  private:
    bool m_iopCoupling; //!< \brief wether we are using the iop coupling or not
    /*@}*/

    /*!
      @{ \name Element field for boundary condition and the curvature object
    */
  private:
    ElementField m_normalFieldEF,
      m_displacementEF,
      m_displacementOldEF,
      m_gradUNEF,
      m_TgradUNEF,
      m_iopEF,
      m_koiterCoefficientsEF,
      m_meanCurvEF,
      m_firstLapCoeffEF,
      m_secondLapCoeffEF,
      m_thirdLapCoeffEF,
      m_minLapFibersCoeffEF,
      m_maxLapFibersCoeffEF,
      m_etagradunEF;
    Curvatures m_curv;
    std::vector<double> m_auxiliaryDoubles;
    std::vector<std::string> m_auxiliaryStrings;
    /*@}*/

    /*!
      @{ \name small functions and some variables to use the assemblyLoopBoundaryGeneral
    */
  private:
    template<class theClass> void assemblyLoopBoundary( void (theClass::*functionOfTheLoop)(felInt));
    void initPerET();
    void initPerETWBD();
    void updateFe   (const std::vector<Point*>& elemPoint,const std::vector<felInt>& elemIdPoint);
    void updateFeWBD(const std::vector<Point*>& elemPoint,const std::vector<felInt>& elemIdPoint);

    CurrentFiniteElementWithBd* m_curWBDFeVel;
    /*@}*/
  public:
    inline DofBoundary& dofBD(std::size_t iBD) { return m_dofBD[iBD]; };

  protected:
    // Dof boundary utilities
    std::vector<DofBoundary> m_dofBD = std::vector<DofBoundary>(1, DofBoundary());

  public:
    void initFixedPointAcceleration();
    /// This two methods implement aitken acceleration algorithm to
    /// speed up fixed point iteration.
    /// First you call accelerationPreStep
    /// and you pass the input that you are about to use
    /// Than you use it and you obtain a new input
    /// you pass this input to accelerationPostStep
    /// To obtain a smarter input based on that.
    void accelerationPreStep(PetscVector& seqCurrentInput);
    void accelerationPostStep(PetscVector& seqCurrentInput,felInt cIteration);
  private:
    /// Different available techniques
    enum accelerationType {
        FixedPoint = 0,  /*!< Standard fixed point iterations. We need only x1 and only for computing norm of update */
        Relaxation = 1,  /*!< Relaxed fixed point iterations. We need only x1 */
        Aitken     = 2,  /*!< Aitken acceleration. We need only x0,x1,fx0,fx1 */
        IronsTuck  = 3   /*!< Variant of Aitken acceleration. We need only x0,x1,fx0,fx1 */
        };

    accelerationType m_accMethod; ///< The acceleration method in use
    double m_omegaAcceleration;   ///< Accelaration parameter, computed during acceleration or setted
  };
  
  /*!
    \brief this function it is a generic assembly loop on the boundary elements where EmbedFSI conditions are imposed.
   */
  template<class theClass>
  inline void LinearProblemNSSimplifiedFSI::assemblyLoopBoundary( void (theClass::*functionOfTheLoop)( felInt ) ) {
    this->assemblyLoopBoundaryGeneral(functionOfTheLoop,m_embedLabels,&LinearProblemNSSimplifiedFSI::initPerET,&LinearProblemNSSimplifiedFSI::updateFe);
  }
}

#endif
