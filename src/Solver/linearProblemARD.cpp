//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemARD.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce {
  LinearProblemARD::LinearProblemARD():
    LinearProblem("Advection-Reaction-Diffusion"),
    m_density(0.),
    m_rhsDynamicValue(nullptr),
    m_advDynamicValue(nullptr),
    m_difDynamicValue(nullptr),
    m_reaDynamicValue(nullptr)
  {}

  LinearProblemARD::~LinearProblemARD() = default;


  void LinearProblemARD::userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel,FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_IEL;
    IGNORE_UNUSED_ELEM_POINT;
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
    if (m_rhsDynamicValue  != nullptr) {
      m_elemFieldRHS.setValue(*m_feTemp, *m_rhsDynamicValue, m_fstransient->time);
      assert(!m_elementVector.empty());
      m_elementVector[0]->source(1.,*m_feTemp,m_elemFieldRHS,0,1);
    }

    if (m_advDynamicValue  != nullptr) {
      m_elemFieldAdv.setValue(*m_feTemp, *m_advDynamicValue, m_fstransient->time);
      m_elementMat[0]->u_grad_phi_j_phi_i(1.,m_elemFieldAdv,*m_feTemp,0,0,1);
    }

    if (m_difDynamicValue  != nullptr) {
      m_elemFieldDif.setValue(*m_feTemp, *m_difDynamicValue, m_fstransient->time);
      m_elementMat[0]->a_grad_phi_i_grad_phi_j(1.,m_elemFieldDif,*m_feTemp,0,0,1);
    }

    if (m_reaDynamicValue  != nullptr) {
      m_elemFieldRea.setValue(*m_feTemp, *m_reaDynamicValue, m_fstransient->time);
      m_elementMat[0]->a_phi_i_phi_j(1.,m_elemFieldRea,*m_feTemp,0,0,1);
    }
  }


  void LinearProblemARD::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh, comm, doUseSNES);
    m_fstransient = fstransient;

    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = temperature;
    listNumComp[0]  = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(temperature);
    definePhysicalVariable(listVariable,listNumComp);
    m_density = FelisceParam::instance().density;

    m_rhsDynamicValue = FelisceParam::instance().elementFieldDynamicValue("ARDSourceTerm");
    m_advDynamicValue = FelisceParam::instance().elementFieldDynamicValue("ARDAdvectionTerm");
    m_difDynamicValue = FelisceParam::instance().elementFieldDynamicValue("ARDDiffusionTerm");
    m_reaDynamicValue = FelisceParam::instance().elementFieldDynamicValue("ARDReactionTerm");
  }


  void LinearProblemARD::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_iTemp = m_listVariable.getVariableIdList(temperature);
    m_feTemp = m_listCurrentFiniteElement[m_iTemp];

    m_elemFieldAdv.initialize(DOF_FIELD,*m_feTemp,this->dimension());
    m_elemFieldDif.initialize(DOF_FIELD,*m_feTemp);
    m_elemFieldRea.initialize(DOF_FIELD,*m_feTemp);
    m_elemFieldRHS.initialize(DOF_FIELD,*m_feTemp);
    m_elemField.initialize(DOF_FIELD,*m_feTemp);
  }

  void LinearProblemARD::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_feTemp->updateFirstDeriv(0, elemPoint);
    double coef = m_density/m_fstransient->timeStep;
    int id=0;

    //================================
    //            matrix
    //================================
    if (m_advDynamicValue  == nullptr) {
      m_elemFieldAdv.setValue(externalVec(id),*m_feTemp, iel, m_iTemp, m_externalAO[id], *m_externalDof[id]);
      m_elementMat[0]->u_grad_phi_j_phi_i(1.,m_elemFieldAdv,*m_feTemp,0,0,1);
      id++;
    }

    if (m_difDynamicValue  == nullptr) {
      m_elemFieldDif.setValue(externalVec(id),*m_feTemp, iel, m_iTemp, m_externalAO[id], *m_externalDof[id]);
      m_elementMat[0]->a_grad_phi_i_grad_phi_j(1.,m_elemFieldDif,*m_feTemp,0,0,1);
      id++;
    }

    if (m_reaDynamicValue  == nullptr) {
      m_elemFieldRea.setValue(externalVec(id),*m_feTemp, iel, m_iTemp, m_externalAO[id], *m_externalDof[id]);
      m_elementMat[0]->a_phi_i_phi_j(1.,m_elemFieldRea,*m_feTemp,0,0,1);
      id++;
    }

    m_elementMat[0]->phi_i_phi_j(coef,*m_feTemp,0,0,1);


    //================================
    //              RHS
    //================================
    if (m_rhsDynamicValue  == nullptr) {
      m_elemFieldRHS.setValue(externalVec(id), *m_feTemp, iel, m_iTemp, m_externalAO[id], *m_externalDof[id]);
      assert(!m_elementVector.empty());
      m_elementVector[0]->source(1.,*m_feTemp,m_elemFieldRHS,0,1);
    }

    m_elemField.setValue(this->sequentialSolution(), *m_feTemp, iel, m_iTemp, m_ao, dof());
    assert(!m_elementVector.empty());
    m_elementVector[0]->source(coef,*m_feTemp,m_elemField,0,1);
  }
}
