//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _LINEARPROBLEMPOISSONCONTINUATION_HPP
#define _LINEARPROBLEMPOISSONCONTINUATION_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"

/*!
 \file linearProblemPoissonContinuation.hpp
 \date 17/02/2022
 \brief Continuation method for Poisson's equation
*/

namespace felisce {  
  class LinearProblemPoissonContinuation:
    public LinearProblem {
  public:
    LinearProblemPoissonContinuation();
    ~LinearProblemPoissonContinuation(){};

    // usual methods
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override; // initialization of the class
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override; // set the current finite element 
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override; // compute the elementary arrays for the block system
    
    // methods regarding the cip stabilization
    void userChangePattern(int numProc, int rankProc) override;
    void assembleFaceOrientedStabilization();
    void addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec);
    void updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fe, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup);
    
    void readData(IO& io,double iteration) override; // this should be used to read the data from the forward problem
    
  protected:
    CurrentFiniteElement* m_feTemp;
    ElementField m_elemField;
    ElementField m_sourceTerm;
    felInt m_iPotThorax;
    felInt m_iTemperature;
    Variable* m_potThorax;
    Variable* m_temperature;
    PetscVector m_potData;
  };
}

#endif
