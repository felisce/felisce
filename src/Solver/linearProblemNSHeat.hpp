//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __LINEARPROBLEMNSHEAT_HPP__
#define __LINEARPROBLEMNSHEAT_HPP__

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNS.hpp"

namespace felisce {
  class LinearProblemNSHeat : public LinearProblemNS {
  public:
    LinearProblemNSHeat();
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    virtual void userInitialize(){};
    void userAddOtherUnknowns(std::vector<PhysicalVariable>& listVariable, std::vector<std::size_t>& listNumComp) override;
    std::size_t iTemp() {return m_iTemp;};
    std::size_t iUnkTemp() {return m_iUnknownTemp;};
    virtual void userPostProcessing(){};
  private:
    std::size_t m_iTemp,m_iUnknownTemp;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) override;
    void userElementInit() override;
    CurrentFiniteElement* m_feTemp;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) override;
    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& ielSupportDof,int label) override;
    void userElementInitNaturalBoundaryCondition() override;

    ElementField m_elemFieldOldTemp,m_elemFieldAdvection,m_stevino;
    double m_referenceTemperature,m_coeffOfThermalExpansion,m_thermalDiffusion,m_t0;
  protected:
    std::vector<double> m_gravity;
    std::vector<double> m_zedZeroStevino;
  };
}
#endif
