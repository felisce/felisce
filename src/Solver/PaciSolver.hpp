//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    F. Raphel
//

#ifndef _PACISOLVER_HPP
#define _PACISOLVER_HPP

// System includes

// External includes
#ifdef FELISCE_WITH_SUNDIALS
#include "cvode/cvode.h"          /* prototypes for CVODE fcts. and consts. */
#include "nvector/nvector_serial.h" /* serial N_Vector types, fcts., and macros */
#include "cvode/cvode_dense.h"    /* prototype for CVDense */
#include "sundials/sundials_dense.h" /* definitions DenseMat DENSE_ELEM */
#include "sundials/sundials_types.h" /* definition of type realtype */
#include "cvode/cvode_spgmr.h"//tests        /* prototypes & constants for CVSPGMR */
#include "cvode/cvode_band.h"        /* prototype for CVBand */
#include "sundials/sundials_band.h"  /* definitions of type DlsMat and macros */
#include "cvode/cvode_bandpre.h"     /* prototypes & constants for CVBANDPRE module */
#include "sundials/sundials_math.h"  /* definition of ABS */
#endif

// Project includes
#include "Solver/ionicSolver.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce 
{
#ifdef FELISCE_WITH_SUNDIALS
  class UserData {
  public:
    std::vector<double> M_u_extrap;
    std::vector<double> value_df1;
    std::vector<double> vINaCa;
    std::vector<double> vIPCa;
    std::vector<double> vIbCa;
    std::vector<double> vENa;
    std::vector<double> vEKs;
    std::vector<double> vECa;
    std::vector<double> vINa;
    std::vector<double> vICaL;
    std::vector<double> vINaK;
    std::vector<double> vIrel;
    std::vector<double> vIup;
    std::vector<double> vIleak;
    std::vector<double> vIbNa;
    int pos;

  };
#endif


  class PaciSolver:
    public IonicSolver {
  public:
    //!Constructor.
    PaciSolver(FelisceTransient::Pointer fstransient);
    //!Destructor.
    ~PaciSolver() override;
    //!Compute RHS in EDO.
    void computeRHS() override;
    //!Solve the EDO.
    void solveEDO() override;
    //!Computation of ionic current from EDO solution.
    void computeIon() override;


    //gates
    static void hgate(double& hinf, double& tauh, double u);
    static void jgate(double& jinf, double& tauj, double u);
    static void mgate(double& minf, double& taum, double u);
    static void dgate(double& dinf, double& taud, double u);
    static void fcagate(double& fcainf, double& taufca, double& constfca, double u, double Cai, double fCa);
    static void f1gate(double& f1inf, double& tauf1, double u, double df1, double Cai);
    static void f2gate(double& f2inf, double& tauf2, double u);
    static void rgate(double& rinf, double& taur, double u);
    static void qgate(double& qinf, double& tauq, double u);
    static void xr1gate(double& xr1inf, double& tauxr1, double u);
    static void xr2gate(double& xr2inf, double& tauxr2, double u);
    static void xsgate(double& xsinf, double& tauxs, double u);
    static void xfgate(double& xfinf, double& tauf, double u);
    static void ggate(double& ginf, double& taug, double& constg, double u, double Cai, double g);

    //gates for ORd--> non spontaneous AP
    static void mgateORd(double& minf, double& taum, double u);
    static void jgateORd(double& jinf, double& tauj, double u);
    static void hslowgateORd(double& hslowinf, double& tauhslow, double u);
    static void hfastgateORd(double& hfastinf, double& tauhfast, double u);
    static void hlgateORd(double& hlinf, double& tauhl, double u);
    static void mlgateORd(double& mlinf, double& tauml, double u);
    //


#ifdef FELISCE_WITH_SUNDIALS
    static int M_f(realtype t, N_Vector y, N_Vector ydot, void *f_data);
    static void PrintOutput(realtype t, realtype y1, realtype y2, realtype y3, realtype y4, realtype y5, realtype y6);


    //for tolerances
    static int ewt(N_Vector y, N_Vector w, void *user_data);

    UserData M_userData;
    void *datatest;


    std::vector<void *> cvode_mem;
    std::vector<N_Vector> initvalue;
    std::vector<N_Vector> abstol;


#endif

  protected:


  private:

  };

}


#endif
