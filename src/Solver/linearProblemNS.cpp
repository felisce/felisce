//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes
#include <stack>
#include <functional>

// External includes

// Project includes
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "Core/felisceTools.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "FiniteElement/elementVector.hpp"
#include "InputOutput/io.hpp"
#include "Solver/linearProblemNS.hpp"

/*!
  \file LinearProblemNS.cpp

  \date 05/01/2011
  \brief Monolithic algorithm for the Navier-Stokes equations
*/

namespace felisce {

LinearProblemNS::LinearProblemNS():
  LinearProblem("Navier-Stokes monolithic",FelisceParam::instance().nonLinearFluid == true ? 3 : 2), // TODO FelisceParam::instance(???) which one??
  m_initializeMapPnt(true),
  m_viscosity(0.),
  m_density(0.),
  m_theta(0.),
  m_lumpedModelBC(nullptr),
  m_forceConvAndStabComputation(false),
  m_bdf(nullptr),
  m_cardiacCycle(nullptr),
  m_ris(nullptr),
  allocateSeqVec(false),
  allocateSeqVecExt(false),
  m_isCsrNoInterfaceStored(false)
{
  // When using cvgraph, the following vectors are needed.
  // If we use a D2N or a N2D some of them may be unused
  if ( FelisceParam::instance().withCVG ) { // TODO FelisceParam::instance(???) which one??
    #ifdef FELISCE_WITH_CVGRAPH
      m_cvgDirichletVariable = "cvgraphVelocity";
    #endif
    m_seqVecs.Init("cvgraphSTRESS");
    m_seqVecs.Init("externalDisplacementOld");
    m_seqVecs.Init("externalDisplacement");
    m_seqVecs.Init("cvgraphVelocity");
  }
}

/***********************************************************************************/
/***********************************************************************************/

LinearProblemNS::~LinearProblemNS() 
{
  m_elemFieldLumpedModelBC.clear();
  m_bdf = nullptr;
  if (allocateSeqVec) {
    m_seqBdfRHS.destroy();
  }
  if (allocateSeqVecExt) {
    m_solExtrapol.destroy();
  }
}

/***********************************************************************************/
/***********************************************************************************/

PetscErrorCode NSJacobian(SNES snes, Vec evaluationState, Mat jacobian , Mat preconditioner, void* context_as_void) 
{
  (void) snes;
  (void) evaluationState;
  (void) jacobian;
  (void) preconditioner;
  (void) context_as_void;
  PetscFunctionBegin;

  SnesContext* context_ptr = static_cast<SnesContext*>(context_as_void);
  SnesContext& context = *context_ptr;
  LinearProblemNS& lpb = *static_cast<LinearProblemNS*>(context.linearProblem);

  // Retrieve instance
  auto& r_instance = FelisceParam::instance(lpb.instanceIndex());

  // add matrices linear and non linear terms
  lpb.addMatrixRHSNl(FlagMatrixRHS::only_matrix);

  // remove rows of nl matrices that have prescribed velocities (Dirichlet)
  lpb.applyBC(r_instance.essentialBoundaryConditionsMethod,
              MpiInfo::rankProc(), FlagMatrixRHS::only_matrix, FlagMatrixRHS::only_matrix,
              0, true, ApplyNaturalBoundaryConditions::no);

  PetscFunctionReturn(0);
}

/***********************************************************************************/
/***********************************************************************************/

PetscErrorCode NSFunction(SNES snes, Vec evaluationState, Vec residual, void* context_as_void)
{
  PetscFunctionBegin;
  SnesContext* context_ptr = static_cast<SnesContext*>(context_as_void);
  SnesContext& context = *context_ptr;
  LinearProblemNS& lpb = *static_cast<LinearProblemNS*>(context.linearProblem);
  
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(lpb.instanceIndex());

  PetscInt nbEval;
  SNESGetNumberFunctionEvals(snes,&nbEval);

  // transfer evaluation state U^(n, k-1) to computeElementArray
  VecCopy(evaluationState,lpb.evaluationState().toPetsc());
  lpb.gatherVector(lpb.evaluationState(), lpb.seqEvaluationState());

  // add solution at previous time step
  if (nbEval == 0 && r_instance.useALEformulation)
    lpb.addMatrixRHSNl(FlagMatrixRHS::only_rhs);

  // clear non linear matrix
  lpb.matrix(2).zeroEntries();

  if (nbEval > 0 ) {
    // assembly after first inner iteration
    lpb.clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
    lpb.assembleMatrixRHS(context.rankProc);

    // add static and dynamic matrices
    lpb.addMatrixRHS();

    // add solution at previous time step
    if (r_instance.useALEformulation)
      lpb.addMatrixRHSNl(FlagMatrixRHS::only_rhs);

    // apply boundary conditions.
    lpb.finalizeEssBCTransient();
    lpb.applyBC(r_instance.essentialBoundaryConditionsMethod,context.rankProc);
  }

  // compute residual
  lpb.evalDynamicResidual();

  // send residual to petsc
  VecCopy(lpb.vector().toPetsc(),residual);

  PetscFunctionReturn(0);

}

void LinearProblemNS::evalDynamicResidual()
{
  // compute residual
  multAdd(matrix(0),evaluationState(),vector(),vector());
}

void LinearProblemNS::InitializeDerivedAttributes()
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if (r_instance.nonLinearFluid) {
    m_snesContext.initialize(*this, MpiInfo::rankProc(), MpiInfo::numProc(), ApplyNaturalBoundaryConditions::no, FlagMatrixRHS::matrix_and_rhs);
    snesInterface().setFunction(vector(), NSFunction, &m_snesContext);
    snesInterface().setJacobian(matrix(0), matrix(0), NSJacobian, &m_snesContext);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  LinearProblem::initialize(mesh, comm, doUseSNES);
  m_fstransient = fstransient;
  std::vector<PhysicalVariable> listVariable;
  std::vector<std::size_t> listNumComp;

  listVariable.push_back(velocity);
  listNumComp.push_back(dimension());

  listVariable.push_back(pressure);
  listNumComp.push_back(1);

  if ( r_instance.useALEformulation ) {
    listVariable.push_back(displacement);
    listNumComp.push_back(dimension());
  }

  // define unknown of the linear system.
  m_listUnknown.push_back(velocity);
  m_listUnknown.push_back(pressure);

  if ( r_instance.useFDlagrangeMult ) {
    listVariable.push_back(lagMultiplier); 
    listNumComp.push_back(dimension());
    m_listUnknown.push_back(lagMultiplier);

    if ( r_instance.massConstraint ) {
      listVariable.push_back(temperature);
      listNumComp.push_back(1);
      m_listUnknown.push_back(temperature);
    }
  }

  userAddOtherUnknowns(listVariable,listNumComp);
  userAddOtherVariables(listVariable,listNumComp);
  definePhysicalVariable(listVariable,listNumComp);

  m_iVelocity = m_listVariable.getVariableIdList(velocity);
  m_iPressure = m_listVariable.getVariableIdList(pressure);

  m_velocity = &m_listVariable[m_iVelocity];
  m_pressure = &m_listVariable[m_iPressure];

  m_iUnknownVel = m_listUnknown.getUnknownIdList(velocity);
  m_iUnknownPre = m_listUnknown.getUnknownIdList(pressure);

  m_velBlock = m_listUnknown.getBlockPosition(m_iUnknownVel,0);
  m_preBlock = m_listUnknown.getBlockPosition(m_iUnknownPre,0);

  if (r_instance.useFDlagrangeMult) {

    if ( r_instance.massConstraint ) {
      if (r_instance.BHstab) {

        if ( dimension() == 2 ) {
          std::vector<int> mask{  1, 1, /**/ 1, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 1, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 1, /**/ 0, 0, /**/ 0,
                                  1, 1, /**/ 0, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 0, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 0, /**/ 1, 1, /**/ 1};
          m_listUnknown.setMask(mask);
        } 
        else if ( dimension() == 3 ) {
          std::vector<int> mask{  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 0, 0, 0, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 1};
          m_listUnknown.setMask(mask);
        }            
      } 
      else if (r_instance.BPstab) {

        if ( dimension() == 2 ) {
          std::vector<int> mask{  1, 1, /**/ 1, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 1, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 1, /**/ 0, 0, /**/ 0,
                                  1, 1, /**/ 0, /**/ 1, 1, /**/ 0,
                                  1, 1, /**/ 0, /**/ 1, 1, /**/ 0,
                                  1, 1, /**/ 0, /**/ 0, 0, /**/ 1};
          m_listUnknown.setMask(mask);
        } 
        else if ( dimension() == 3 ) {
          std::vector<int> mask{  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 0, 0, 0, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0, /**/ 1};
          m_listUnknown.setMask(mask);
        }   
      } 
      else {

        if ( dimension() == 2 ) {
          std::vector<int> mask{  1, 1, /**/ 1, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 1, /**/ 1, 1, /**/ 1,
                                  1, 1, /**/ 1, /**/ 0, 0, /**/ 0,
                                  1, 1, /**/ 0, /**/ 0, 0, /**/ 0,
                                  1, 1, /**/ 0, /**/ 0, 0, /**/ 0,
                                  1, 1, /**/ 0, /**/ 0, 0, /**/ 0};
          m_listUnknown.setMask(mask);
        } 
        else if ( dimension() == 3 ) {
          std::vector<int> mask{  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1, /**/ 1,
                                  1, 1, 1, /**/ 1, /**/ 0, 0, 0, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0, /**/ 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0, /**/ 0}; 
          m_listUnknown.setMask(mask);
        }
      }      
    }
    else {
      if (r_instance.BHstab) {

        if ( dimension() == 2 ) {
          std::vector<int> mask{  1, 1, /**/ 1, /**/ 1, 1,
                                  1, 1, /**/ 1, /**/ 1, 1,
                                  1, 1, /**/ 1, /**/ 0, 0,
                                  1, 1, /**/ 0, /**/ 1, 1,
                                  1, 1, /**/ 0, /**/ 1, 1};
          m_listUnknown.setMask(mask);
        } 
        else if ( dimension() == 3 ) {
          std::vector<int> mask{  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 0, 0, 0,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1};
          m_listUnknown.setMask(mask);
        }            
      } 
      else if (r_instance.BPstab) {

        if ( dimension() == 2 ) {
          std::vector<int> mask{  1, 1, /**/ 1, /**/ 1, 1,
                                  1, 1, /**/ 1, /**/ 1, 1,
                                  1, 1, /**/ 1, /**/ 0, 0,
                                  1, 1, /**/ 0, /**/ 1, 1,
                                  1, 1, /**/ 0, /**/ 1, 1};
          m_listUnknown.setMask(mask);
        } 
        else if ( dimension() == 3 ) {
          std::vector<int> mask{  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 0, 0, 0,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 0, /**/ 1, 1, 1};
          m_listUnknown.setMask(mask);
        }   
      } 
      else {

        if ( dimension() == 2 ) {
          std::vector<int> mask{  1, 1, /**/ 1, /**/ 1, 1,
                                  1, 1, /**/ 1, /**/ 1, 1,
                                  1, 1, /**/ 1, /**/ 0, 0,
                                  1, 1, /**/ 0, /**/ 0, 0,
                                  1, 1, /**/ 0, /**/ 0, 0};
          m_listUnknown.setMask(mask);
        } 
        else if ( dimension() == 3 ) {
          std::vector<int> mask{  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 1, 1, 1,
                                  1, 1, 1, /**/ 1, /**/ 0, 0, 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0,
                                  1, 1, 1, /**/ 0, /**/ 0, 0, 0}; 
          m_listUnknown.setMask(mask);
        }
      }
    }

    m_iLagrange   = m_listVariable.getVariableIdList(lagMultiplier);
    m_iUnknownLag = m_listUnknown.getUnknownIdList(lagMultiplier);
    m_lagBlock    = m_listUnknown.getBlockPosition(m_iUnknownLag,0);

    if ( r_instance.massConstraint ) {
      m_iJapanCon   = m_listVariable.getVariableIdList(temperature);
      m_iUnknownJap = m_listUnknown.getUnknownIdList(temperature);
      m_japBlock    = m_listUnknown.getBlockPosition(m_iUnknownJap,0);
    }

    m_listUnknown.setUnknownsRows(std::vector<int>{m_iUnknownVel,m_iUnknownPre});
    m_listUnknown.setUnknownsCols(std::vector<int>{m_iUnknownVel,m_iUnknownPre});

    m_swapNormals = r_instance.flipItfNormal;
  }

  m_viscosity = r_instance.viscosity;
  m_density = r_instance.density;
  m_theta = r_instance.theta;
  m_useSymmetricStress = r_instance.useSymmetricStress;

  const int verbose = FelisceParam::verbose();
  const double tolrescaling = r_instance.Geo.tolrescaling;
  
  if ( r_instance.characteristicMethod )
    m_pLocator = felisce::make_shared<Locator>(mesh[m_currentMesh].get(), false, tolrescaling, verbose);

  if ( r_instance.useFDlagrangeMult )
    m_pLocator = felisce::make_shared<Locator>(mesh[m_currentMesh].get(), true, tolrescaling, verbose);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initPerElementType(ElementType /* eltType */, FlagMatrixRHS /* flagMatrixRHS */) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( m_currentMesh == m_listVariable[m_iVelocity].idMesh() ) {
    m_feVel = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];

    m_elemFieldAdv.initialize(DOF_FIELD,*m_feVel,dimension());
    m_elemFieldRHS.initialize(DOF_FIELD,*m_feVel,dimension());
    m_elemFieldRHSbdf.initialize(DOF_FIELD,*m_feVel,dimension());
    m_elemFieldTotalPressureFormulation.initialize(QUAD_POINT_FIELD,*m_feVel,dimension());
    m_elemFieldCharact.initialize(QUAD_POINT_FIELD,*m_feVel,dimension());

    if (r_instance.nonLinearFluid) {
      m_elemFieldVelSeq.initialize(DOF_FIELD,*m_feVel,dimension());
      m_elemFieldPresSeq.initialize(DOF_FIELD, *m_fePres, 1);
    }

    // theta-method RHS
    if ( m_theta < 1 ) {
      m_elemFieldPres.initialize(DOF_FIELD,*m_fePres,1);
      std::size_t SizeElMatRHSThetaMethod = 2;
      if(r_instance.useALEformulation)
        SizeElMatRHSThetaMethod = 1;

      const std::vector<const CurBaseFiniteElement*> finiteElt({m_feVel, m_fePres});
      std::vector<std::size_t>           numberCmp({m_listVariable[m_iVelocity].numComponent(),m_listVariable[m_iPressure].numComponent()});
      for (std::size_t i = 0; i < SizeElMatRHSThetaMethod; i++)
        m_elMatRHSThetaMethod.push_back(new ElementMatrix(finiteElt,numberCmp, numberCmp));
    }

    //=========
    //   ALE
    //=========
    if(r_instance.useALEformulation) {

      m_iDisplacement = m_listVariable.getVariableIdList(displacement);
      m_displacement = &m_listVariable[m_iDisplacement];

      m_elemFieldVelMesh.initialize(DOF_FIELD,*m_feVel,dimension());

      numDofExtensionProblem = m_externalDof[0]->numDof();

      m_petscToGlobal1.resize(numDofExtensionProblem);
      m_petscToGlobal2.resize(numDofExtensionProblem);
      m_auxvec.resize(numDofExtensionProblem);

      for (int i=0; i<numDofExtensionProblem; i++) {
        m_petscToGlobal1[i]=i;
        m_petscToGlobal2[i]=i;
      }
      AOApplicationToPetsc(m_externalAO[0],numDofExtensionProblem,m_petscToGlobal1.data());
      AOApplicationToPetsc(m_ao,numDofExtensionProblem,m_petscToGlobal2.data());

      if ( m_fstransient->iteration > 0) {
        if ( m_fstransient->iteration == 1) {
          m_beta.duplicateFrom(m_solExtrapol);
          if (r_instance.nonLinearFluid)
            m_betaSeq.duplicateFrom(seqEvaluationState());
        }
        //=========
        //   ALE
        //=========
        // Construction of m_beta = u^{n-1} - w^{n}
        // Recall that externalVec(0) = - w^{n}
        m_beta.copyFrom(m_solExtrapol);
        externalVec(0).getValues(numDofExtensionProblem,m_petscToGlobal1.data(), m_auxvec.data());
        m_beta.setValues(numDofExtensionProblem, m_petscToGlobal2.data() ,m_auxvec.data(), ADD_VALUES);
        if (r_instance.nonLinearFluid){
          m_auxvec.resize(numDofExtensionProblem);
          externalVec(0).getValues(numDofExtensionProblem,m_petscToGlobal1.data(), m_auxvec.data());
          m_betaSeq.copyFrom(seqEvaluationState());
          m_betaSeq.setValues(numDofExtensionProblem, m_petscToGlobal2.data() ,m_auxvec.data(), ADD_VALUES);
        }      
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initializeRISModel(RISModel* risModel) 
{
  m_ris = risModel;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initializeCardiacCycle(CardiacCycle* cardiacCycle) 
{
  m_cardiacCycle = cardiacCycle;
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initializeLumpedModelBC(LumpedModelBC* lumpedModelBC)
{
  m_lumpedModelBC = lumpedModelBC;
  m_elemFieldLumpedModelBC.clear();
  m_elemFieldLumpedModelBC.resize(m_lumpedModelBC->size());
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initPerElementTypeBoundaryCondition(ElementType& /* eltType */, FlagMatrixRHS /* flagMatrixRHS */) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if (r_instance.CardiacCycle) {
    m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
    m_elemFieldSistole.initialize(CONSTANT_FIELD, *m_curvFeVel, dimension());
    m_elemFieldDiastole.initialize(CONSTANT_FIELD, *m_curvFeVel);
  }

  if (m_lumpedModelBC) {
    // allocate m_elemFieldLumpedModelBC
    for (std::size_t iLumpedModelBC=0; iLumpedModelBC <m_lumpedModelBC->size() ; iLumpedModelBC++) {
      if(r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 1 || (r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 2 && r_instance.model == "NS"))
        m_elemFieldLumpedModelBC[iLumpedModelBC].initialize(CONSTANT_FIELD,1);
    }
  }

  #ifdef FELISCE_WITH_CVGRAPH
    if (r_instance.withCVG ) {
      m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
      if ( slave()->thereIsAtLeastOneRobinCondition() ){
        m_robinAux.initialize( DOF_FIELD, *m_listCurvilinearFiniteElement[m_iVelocity], dimension() );
      }
    }
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::initPerDomainBoundaryCondition(std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
    int label, felInt numEltPerLabel,felInt* ielSupportDof,
    ElementType& eltType, felInt numElement_eltType, FlagMatrixRHS flagMatrixRHS) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  m_currentLabel = label;

  //================================
  // Explicit/implicit lumpedModel boundary conditions with model NS
  //================================
  if (m_lumpedModelBC && (r_instance.model == "NS") ) {
    for (std::size_t iLumpedModelBC=0; iLumpedModelBC <m_lumpedModelBC->size() ; iLumpedModelBC++) {
      if(r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 1 || r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 2) {
        if (r_instance.lumpedModelBCLabel[iLumpedModelBC] == label) {
          applyNaturalBoundaryConditionLumpedModelBC(elemPoint,elemIdPoint,label,numEltPerLabel, ielSupportDof,iLumpedModelBC,eltType, numElement_eltType, flagMatrixRHS);
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /* elemIdPoint */, felInt& iel, FlagMatrixRHS /* flagMatrixRHS */) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());
  
  // General updates
  for (std::size_t iFe = 0; iFe < m_listCurvilinearFiniteElement.size(); iFe++) {
    m_listCurvilinearFiniteElement[iFe]->updateMeasNormal(0, elemPoint);
  }

  // Saving ptr to velocity curvilinear finite element
  CurvilinearFiniteElement* curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
  // Position of the velocity block
  int iblockVel = m_listUnknown.getBlockPosition(m_iUnknownVel,0);

  // Windkessels model!
  if (m_lumpedModelBC && r_instance.model == "NS") {
    for (std::size_t iLumpedModelBC=0; iLumpedModelBC <m_lumpedModelBC->size() ; iLumpedModelBC++) {
      if ( r_instance.lumpedModelBCLabel[iLumpedModelBC] == m_currentLabel) {
        if(r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 1 || r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 2) {

          m_elementVectorBD[0]->f_phi_i_scalar_n(1.,*curvFeVel,m_elemFieldLumpedModelBC[iLumpedModelBC],iblockVel);
        }
        // to be continued // Justine 7/11/13
        //if ( r_instance.lumpedModelBCAlgoImprovedExplicit == 1 ) {
        // we add \int_Gamma (u^{n+1} \cdot n) (v \cdot n)
        // and \int_Gamma (u^n \cdot n) (v \cdot n)
        //}
      }
    }
  }

  if(r_instance.CardiacCycleLabel == m_currentLabel) {
    double penalisationParam = 0.;
    penalisationParam = m_cardiacCycle->penalisationParam(); //= 0 if valve closed, = TGV if valve open
    m_elemFieldSistole.setValue(m_cardiacCycle->inflow());
    m_elemFieldDiastole.setValue(m_cardiacCycle->Pressure_in());

    m_elementMatBD[0]->phi_i_phi_j(penalisationParam, *curvFeVel, iblockVel, iblockVel, dimension()); // epsilon * int_{Gamma_in} [ u^{n+1} * v ]
    m_elementVectorBD[0]->source(penalisationParam, *curvFeVel, m_elemFieldSistole, iblockVel, dimension()); // epsilon * int_{Gamma_in} [ u_{in} * v ]
    m_elementVectorBD[0]->f_phi_i_scalar_n(-1., *curvFeVel, m_elemFieldDiastole, iblockVel); // int_{Gamma_in} [-p * N* v ] curvFeVel referred to test function v
  }

  if (r_instance.RISModels) {
    if (r_instance.PenalizationMethod)
      FEL_ERROR("RIS and penalization models are activated at the same time!");
    double  Res = 0.;
    Res =  m_ris->resistance(m_currentLabel);
    if( Tools::notEqual(Res,0.) ) {
      //LHS term of the penalisation
      if ( std::find(r_instance.forbiddenLabelsForCorrection.begin(), r_instance.forbiddenLabelsForCorrection.end(), m_currentLabel) == r_instance.forbiddenLabelsForCorrection.end() ) {
        m_elementMatBD[0]->phi_i_phi_j(Res,*curvFeVel, iblockVel, iblockVel, dimension());
      }
      //RHS term of the penalisation
      //Correct the constraint with the velocity of the mesh (R*[u-w] = 0)
      //This term is not added at the first iteration as the first iteration is not physical in our case (A.This, M. Fernandez, J-F. Gerbeau : Zygot case with full deformation).
      if(m_fstransient->iteration>1){
        if ( std::find(r_instance.forbiddenLabelsForCorrection.begin(), r_instance.forbiddenLabelsForCorrection.end(), m_currentLabel) == r_instance.forbiddenLabelsForCorrection.end() && r_instance.useALEformulation ) {
          m_elementVectorBD[0]->source(-Res, *curvFeVel, m_elemFieldVelMesh, iblockVel, dimension());
        }
      }
    }
  }

  if (r_instance.PenalizationMethod) {
    if (r_instance.RISModels)
      FEL_ERROR("RIS and penalization models are activated at the same time!");
    double Pen = r_instance.Gamma_penalization;
    if ( r_instance.closed_penalization[0] == m_currentLabel || r_instance.closed_penalization[1] == m_currentLabel ){
      if (r_instance.useALEformulation && m_fstransient->iteration>1 )
        m_elemFieldVelSeq.setValue(m_betaSeq, *m_feVel, iel, m_iVelocity, m_ao, dof());
      else
        m_elemFieldVelSeq.setValue(seqEvaluationState(), *m_feVel, iel, m_iVelocity, m_ao, dof());
      //LHS term of penalization
      m_elementMatBD[2]->penalty_phi_i_scalar_n_phi_j_scalar_n(Pen, *curvFeVel, m_elemFieldVelSeq, iblockVel, iblockVel);
      //RHS term of penalization
      m_elementVectorBD[0]->penalty_phi_i_scalar_n(-Pen, *curvFeVel, m_elemFieldVelSeq, iblockVel);
    }
  }

  #ifdef FELISCE_WITH_CVGRAPH
  if (r_instance.withCVG )
    cvgraphNaturalBC(iel);
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /* elemIdPoint */, felInt& iel, FlagMatrixRHS flagMatrixRHS) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( m_currentMesh == m_listVariable[m_iVelocity].idMesh() ) {
    m_feVel->updateFirstDeriv(0, elemPoint);
    m_fePres->updateFirstDeriv(0, elemPoint);

    PetscScalar coef = m_density/m_fstransient->timeStep;

    if ( m_feVel->measure() < 0. ) {
      int iglo;
      ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh],1,&iel,&iglo);
      std::cout << " id global = " << iglo << std::endl;
      FEL_ERROR("Error: Element of negative measure!");
    }

    std::size_t indexMatrix;
    if( r_instance.useALEformulation )
      indexMatrix = 0; // The matrix actually used in the solver, which is not constant (because of advection, ALE,...)
    else
      indexMatrix = 1; // The "static" part of the matrix, when there is no ALE it is basically everything, but the advection.

    // Static matrix
    if ( ( (m_fstransient->iteration == 0) && (r_instance.useALEformulation != 1) ) || 
         ( (m_fstransient->iteration  > 0) &&  r_instance.useALEformulation       ) ) {
      /*! In this function we compute the term of the matrix that, without ALE does not change in time, so they should go to the matrix 1.
        *
        *  We enter in the following cases:
        *
        *  - Before the first iteration, when ALE is not 1 ( which means either no ALE or ALE = 2 )
        *     indexMatrix = 1 if ALE=0,
        *     indexMatrix = 0 if ALE>=2
        *
        *  - For each iteration, but not before when ALE>0
        *     indexMatrix = 0
        *
        *  When ALE=0
        *      you enter only before the first iteration.
        *  When ALE = 1
        *      you do not enter before the first iteration.
        *  When ALE = 2
        *      you enter the first iteration and the following
        */
      if ( flagMatrixRHS != FlagMatrixRHS::only_rhs ) {

        if (m_useSymmetricStress) {
          // \eta \int \epsilon u^{n+1} : \epsilon v
          m_elementMat[indexMatrix]->eps_phi_i_eps_phi_j(m_theta*2*m_viscosity, *m_feVel, m_velBlock, m_velBlock, m_listVariable[m_iVelocity].numComponent());
        } else {
          // \eta \int \nabla u^{n+1} \nabla v
          m_elementMat[indexMatrix]->grad_phi_i_grad_phi_j(m_theta*m_viscosity, *m_feVel, m_velBlock, m_velBlock, m_listVariable[m_iVelocity].numComponent());
        }

        // \int q \nabla  \cdot u^{n+1} and \int p \nabla  \cdot v
        m_elementMat[indexMatrix]->psi_j_div_phi_i_and_psi_i_div_phi_j (*m_feVel, *m_fePres, m_velBlock, m_preBlock, -1, -1);

        if ( !r_instance.quasistatic ) {
          // \dfrac{\rho}{\dt} u^{n+1} v
          if (  r_instance.useALEformulation == 2 ) {
            m_elementMat[indexMatrix+1]->phi_i_phi_j(0.5*m_bdf->coeffDeriv0()*coef, *m_feVel, 0, 0, dimension()); // indexMatrix + 1 = 1
            m_elementMat[indexMatrix  ]->phi_i_phi_j(0.5*m_bdf->coeffDeriv0()*coef, *m_feVel, 0, 0, dimension()); // indexMatrix     = 0
          } else {
            m_elementMat[indexMatrix]->phi_i_phi_j(m_bdf->coeffDeriv0()*coef, *m_feVel, 0, 0, dimension());
          }
        }
      }
    }

    // Dynamic matrix
    if ( m_fstransient->iteration > 0 || m_forceConvAndStabComputation ) {

      // Matrix : convective term and stabilization
      if ( flagMatrixRHS != FlagMatrixRHS::only_rhs ) {
        //================================
        //          Convective Term
        //================================
        switch ( r_instance.NSequationFlag ) {
          case 0:
            //nothing to do: Stokes' model
            break;

          case 1:
            // Method 1: \rho u \grad(u)
            // Natural condition: \sigma(u,p) \cdot n = 0
            // DEFAULT METHOD

            // Temam's stabilization trick
            m_elemFieldAdv.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
            m_elementMat[0]->div_u_phi_j_phi_i(0.5*m_density, m_elemFieldAdv, *m_feVel, m_velBlock, m_velBlock, m_listVariable[m_iVelocity].numComponent());

            if ( r_instance.useALEformulation ) {
              //=========
              //   ALE
              //=========
              // m_elemFieldAdv -> m_beta   where m_beta = u^{n-1} - w^{n} is defined in userElementInit()
              // m_elemFielVelMesh -> -w^{n}
              m_elemFieldAdv.setValue(m_beta, *m_feVel, iel, m_iVelocity, m_ao, dof());
              m_elemFieldVelMesh.setValue(externalVec(0), *m_feVel, iel, m_iDisplacement, m_externalAO[0], *m_externalDof[0]);

              if (  r_instance.useALEformulation == 2 )
                m_elementMat[0]->div_u_phi_j_phi_i(0.5*m_density,m_elemFieldVelMesh,*m_feVel,0,0,dimension());
              else
                m_elementMat[0]->div_u_phi_j_phi_i(m_density,m_elemFieldVelMesh,*m_feVel,0,0,dimension());
            }

            m_elementMat[0]->u_grad_phi_j_phi_i(m_theta*m_density, m_elemFieldAdv, *m_feVel, m_velBlock, m_velBlock, m_listVariable[m_iVelocity].numComponent());
            break;

          case 2:
            m_elemFieldAdv.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
            // Method 2: rotational formulation
            m_elemFieldTotalPressureFormulation.setValue(sequentialSolution(), *m_feVel, iel, m_iVelocity, m_ao, dof());

            // Natural condition: \sigma(u,p) \cdot n + \rho * 0.5 * |u|^2 \cdot n = 0 (total pressure)
            m_elementMat[0]->convective_rot(m_density, m_elemFieldTotalPressureFormulation, *m_feVel, m_velBlock, m_velBlock, m_listVariable[m_iVelocity].numComponent());
            break;

          case 3:
            // Method 3: Total pressure skew-symmetric formulation
            m_elemFieldAdv.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
            // \int_{Sigma}  u^{n+1} Grad u^{n} \dot v - v Grad u^{n} \dot u^{n+1}
            m_elementMat[0]->phi_grad_u_minus_grad_u_phi(m_density, m_elemFieldAdv, *m_feVel,  m_velBlock, m_velBlock, m_listVariable[m_iVelocity].numComponent());
            break;

          default:
            FEL_ERROR("Error: No Convection Method found for Navier-Stokes equations");
        }

        //================================
        //         FE stabilization
        //================================
        if ( r_instance.NSStabType == 0 ) { // SUPG (1: face-oriented)
          if ( m_velocity->finiteElementType() == m_pressure->finiteElementType()) {

            double stab1 = r_instance.stabSUPG;
            double stab2 = r_instance.stabdiv;
            int type = r_instance.typeSUPG;
            double Rem_elem, tau;
	          double delta;

            if ( r_instance.hasImmersedData && r_instance.hasDivDivStab ) {

              // Boilevin-Kayl 02/2018: for the moment, delta is the first external vector which has been pushed back in NSModel for the std::set of methods (hasImmersedData + hasDivDivStab + !useALEformulation)
              // This should be updated when additional methods will be considered
              // Boilevin-Kayl 11/2018: the following has been updated in order to handle the case where hasImmersedData and useALEformulation are used at the same time

              // here iel is already relative to the global numbering, so no need to use an additional mapping or a global index like ielG
              if ( r_instance.useALEformulation ) {
                externalVec(1).getValues(1, &iel, &delta);
              }
              else {
                externalVec(0).getValues(1, &iel, &delta);
              }

              stab1 *= delta;
              stab2 *= 1.0/delta;
            }

            // Beware: if elemFieldRHS is modified in userNS, it wont be considered here  (usually elemFieldRHS = 0 (no body force))...
            // if the matrix and the rhs are not assembled together

            m_elementMat[0]->stab_supg(m_fstransient->timeStep, stab1, stab2, m_density, m_viscosity, *m_feVel, m_elemFieldAdv, m_elemFieldRHS, *m_elementVector[0], Rem_elem, tau, type);
          }
        }
      }

      // TODO D.C. why only rhs??? 
      if ( ( r_instance.NSStabType == 0 ) && ( flagMatrixRHS == FlagMatrixRHS::only_rhs ) && ( m_meshUpdateState == FlagMeshUpdated::PreviousMesh ) && !m_forceConvAndStabComputation ) {
        if ( m_velocity->finiteElementType() == m_pressure->finiteElementType() ) {
          m_elemFieldAdv.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
          double stab1 = r_instance.stabSUPG;
          int type = r_instance.typeSUPG;
          double Rem_elem, tau;

          // TODO D.C. what about the if ( r_instance.hasImmersedData && r_instance.hasDivDivStab ) case???
          
          m_elementVector[0]->stab_supg(m_fstransient->timeStep, stab1, m_density, m_viscosity, *m_feVel, m_elemFieldAdv, m_elemFieldRHS, Rem_elem, tau, type);
        }
      }

      if ( ( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs ) || ( ( flagMatrixRHS == FlagMatrixRHS::only_rhs ) && ( m_meshUpdateState == FlagMeshUpdated::PreviousMesh ) ) ) {
        //================================
        //              RHS
        //================================
        // 1/ bdf (with order k from data)
        // \frac{\rho}{\dt} \sum_{i=1}^k \alpha_i u_{n+1-i}
        if ( !r_instance.quasistatic ) {
          m_elemFieldRHSbdf.setValue(m_seqBdfRHS, *m_feVel, iel, m_iVelocity, m_ao, dof());
          if ( r_instance.nonLinearFluid ){
            if ( (r_instance.useALEformulation && m_meshUpdateState==FlagMeshUpdated::PreviousMesh ) || ( !r_instance.useALEformulation ) )
              m_elementVector[0]->source(-1.*m_density, *m_feVel, m_elemFieldRHSbdf, m_velBlock, m_listVariable[m_iVelocity].numComponent());
          }
          else {
            m_elementVector[0]->source(m_density, *m_feVel, m_elemFieldRHSbdf, m_velBlock, m_listVariable[m_iVelocity].numComponent());
          }
        }
        // 'm_density' and not 'coef' because the time step is integrated into the bdf term

        // 2/ no bdf method
        // \dfrac{\rho}{\dt} u^{n} v
        //m_elemFieldAdv.setValue(m_seqSol, *m_feVel, iel, m_iVelocity, m_ao, dof());
        //m_elementVector[0]->source(coef,*m_feVel,m_elemFieldAdv,0,dimension());

        // theta-method RHS
        if ( m_theta < 1 ) {
          computeRHSThetaMethod(iel);
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::computeElementArrayCharact(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /* elemIdPoint */, felInt& iel, ElementType& eltType, felInt& ielGeo, FlagMatrixRHS /* flagMatrixRHS */) 
{

  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  m_feVel->updateFirstDeriv(0, elemPoint);
  m_fePres->updateFirstDeriv(0, elemPoint);
  PetscScalar coef = m_density/m_fstransient->timeStep;

  if ( m_fstransient->iteration == 0) {
    //================================
    //            matrix
    //================================

    if (m_useSymmetricStress) {
      // \eta \int \epsilon u^{n+1} : \epsilon v
      m_elementMat[1]->eps_phi_i_eps_phi_j(2*m_viscosity,*m_feVel, 0, 0, dimension());
    } else {
      // \eta \int \nabla u^{n+1} \nabla v
      m_elementMat[1]->grad_phi_i_grad_phi_j(m_viscosity, *m_feVel, 0, 0, dimension());
    }

    // \int q \nabla  \cdot u^{n+1} and \int p \nabla  \cdot v
    m_elementMat[1]->psi_j_div_phi_i_and_psi_i_div_phi_j (*m_feVel,*m_fePres,0,m_velocity->numComponent(), -1, -1);

    // \dfrac{\rho}{\dt} u^{n+1} v
    FEL_CHECK(r_instance.orderBdfNS == 1, "Error: Higher bdf order not yet implemented for method of characteristics");
    if (r_instance.orderBdfNS == 1)
      m_elementMat[1]->phi_i_phi_j(coef,*m_feVel,0,0,dimension());
  } else {
    //================================
    //              RHS
    //================================

    //method of characteristics: \dfrac{\rho}{\dt} u^n o X^n v
    if (r_instance.orderBdfNS == 1) {
      setValueCharact(sequentialSolution(),m_elemFieldCharact, *m_feVel, iel, m_iVelocity, eltType, ielGeo, m_fstransient->timeStep);
      assert(!m_elementVector.empty()); // TODO D.C. FELASSERT?
      m_elementVector[0]->source(coef, *m_feVel, m_elemFieldCharact, 0, dimension());
    }

    //================================
    //         FE stabilization
    //================================

    if ( m_velocity->finiteElementType() == m_pressure->finiteElementType()) {
      double stab1 = r_instance.stabSUPG;
      double stab2 = r_instance.stabdiv;
      int type = r_instance.typeSUPG;
      double Rem_elem,tau;

      // Beware: if elemFieldRHS is modified in userNS, it wont be considered here...
      // (usually elemFieldRHS = 0 (no body force))

      //todo Jeremie 01 02 2012: assemble multiple matrix
      m_elementMat[0]->stab_supg(m_fstransient->timeStep,
                                 stab1,stab2, m_density, m_viscosity, *m_feVel,
                                 m_elemFieldAdv, m_elemFieldRHS,
                                 *m_elementVector[0], Rem_elem, tau, type);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::setValueCharact(PetscVector& v, ElementField& elemFieldCharact, CurrentFiniteElement& fe, felInt iel,
                                      int idVariable, const ElementType& eltType, felInt ielGeo, const double dt) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  Point pt, localCoord, vel;
  ElementField elemFieldVelOnDep;
  ElementType eltTypeArr;
  felInt ielGeoArr;
  double dtLeft, dtTol = r_instance.timeStepCharact;
  int numComp = elemFieldCharact.numComp(), numStep = 1 + static_cast<int>(dt/dtTol), effStep;

  switch (elemFieldCharact.type()) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    //velocity field in the departure element, in the ordering of felisce
    elemFieldVelOnDep.initialize(DOF_FIELD, fe, numComp);
    elemFieldVelOnDep.setValue(v, fe, iel, idVariable, m_ao, dof());

    //loop on quadrature points of the departure element
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      //coordinates and local coordinates of each quadrature point pt
      pt.setCoor( fe.currentQuadPoint[ig].coor() );
      localCoord = fe.quadratureRule().quadraturePoint(ig);

      //velocity at pt: v(pt) = \sum v(x_iDof) phi_iDof(localCoord)
      vel.x() = vel.y() = vel.z() = 0.;
      for(int iComp=0; iComp < numComp; iComp++) {
        for (int iDof=0; iDof < fe.numDof(); iDof++) {
          vel[iComp] += elemFieldVelOnDep.val(iComp, iDof) * fe.phi[ig](iDof);
        }
      }

      //backtrack the characteristic line a length dt
      eltTypeArr = eltType;
      ielGeoArr = ielGeo;

      if (r_instance.characteristicMethod == 1) {
        //RK4 scheme
        if ( !(charactLine(v, idVariable, numComp, dt, numStep, effStep, pt, localCoord, eltTypeArr, ielGeoArr, vel)) ) {
          dtLeft = dt - effStep * (dt/numStep);
          while ( charactLineElem(v, idVariable, numComp, dtTol, pt, localCoord, eltTypeArr, ielGeoArr, vel, dtLeft) );
        }
      } else {
        //Euler scheme
        dtLeft = dt;
        while ( charactLineElem(v, idVariable, numComp, dtTol, pt, localCoord, eltTypeArr, ielGeoArr, vel, dtLeft) );
      }

      //compute elemFieldCharact
      for(int iComp=0; iComp < numComp; iComp++) {
        elemFieldCharact.val(iComp, ig) = vel[iComp];
      }
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("setValueCharact from Petsc std::vector is not yet implemented for this type of element field");
    break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

int LinearProblemNS::charactLine(PetscVector& v, int idVariable, int numComp, const double dt, int numStep, int& effStep,
                                 Point& pt, Point& localCoord, ElementType& eltType, felInt& ielGeo, Point& vel)
{
  //given a point pt, backtrack the characteristic line a length dt
  double step = - dt / numStep;
  for(int iStep=1; iStep <= numStep; iStep++) {
    if ( !nextPt(v, idVariable, numComp, step, pt, localCoord, eltType, ielGeo, vel) ) {
      /* This case occurs when for example the point pt got at iStep is outside the domain,
         then the number of effectively done steps before stopping is effStep = iStep-1 and
         the output pt, localCoord... are those at (iStep-1)-th step.
      */
      effStep = iStep-1;
      return(0);
    }
  }
  effStep = numStep;

  return(1);
}

/***********************************************************************************/
/***********************************************************************************/

int LinearProblemNS::nextPt(PetscVector& v, int idVariable, int numComp, const double step,
                            Point& pt, Point& localCoord, ElementType& eltType, felInt& ielGeo, Point& vel) 
{
  //given a point pt and initial velocity vel, backtrack the characteristic line a length step (<0) by a variant of RK4 scheme
  //input: point pt, its local coordinates, element containing pt, velocity at pt
  //output: next point pt with local coordinates localCoord, element containing pt, vel is velocity at pt
  Point ptTmp, localCoordTmp;
  ElementType eltTypeTmp;
  felInt ielGeoTmp;
  GeometricMeshRegion::seedElement seedElt, seedEltTmp;
  double s1 = step / 6.;
  int base;

  std::array<Point, 4> veltmp;
  seedElt = std::make_pair(eltType, ielGeo);

  // first, second and third integration point, element containing this point, velocity at this point
  veltmp[0] = vel;
  for (int intPnt = 0; intPnt < 3; ++intPnt) {

    for(int iComp = 0; iComp < numComp; iComp++)
      ptTmp[iComp] = pt[iComp] + 0.5 * step * veltmp[intPnt][iComp];

    base = ++m_pLocator->getMark();

    seedEltTmp = m_pLocator->localizePoint(seedElt, ptTmp, localCoordTmp, base);

    if ( seedEltTmp.first != GeometricMeshRegion::ELEMENT_TYPE_COUNTER && seedEltTmp.second >= 0 ) {
      eltTypeTmp = seedEltTmp.first;
      ielGeoTmp  = seedEltTmp.second;

      seedElt = seedEltTmp;
    } else {

      return 0;
    }

    veltmp[intPnt+1] = vecInterp(v, idVariable, numComp, localCoordTmp, eltTypeTmp, ielGeoTmp);
  }

  // final point
  for(int iComp=0; iComp < numComp; iComp++)
    ptTmp[iComp] = pt[iComp] + s1 * ( veltmp[0][iComp] + 2*(veltmp[1][iComp] + veltmp[2][iComp]) + veltmp[3][iComp] );

  base = ++m_pLocator->getMark();

  seedEltTmp = m_pLocator->localizePoint(seedElt, ptTmp, localCoordTmp, base);

  if ( seedEltTmp.first != GeometricMeshRegion::ELEMENT_TYPE_COUNTER && seedEltTmp.second >= 0 ) {
    eltTypeTmp = seedEltTmp.first;
    ielGeoTmp  = seedEltTmp.second;

    seedElt = seedEltTmp;
  } else {

    return 0;
  }

  // if succeed, update pt, localCoord, vel
  pt         = ptTmp;
  localCoord = localCoordTmp;

  vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);

  return 1;
}

/***********************************************************************************/
/***********************************************************************************/

int LinearProblemNS::charactLineElem(PetscVector& v, int idVariable, int numComp, double dtTol, Point& pt, Point& localCoord, ElementType& eltType, felInt& ielGeo, Point& vel, double& dtLeft) 
{
  //given a point pt, backtrack the characteristic line in one element a length dtTol at most
  //update pt, localCoord, eltType, ielGeo, vel and the time remaining dtLeft
  //return 0 if dtLeft = 0 or reach to the boundary of the domain, else return 1
  const GeoElement *geoEle;
  geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  TypeShape eltShape = (TypeShape)(geoEle->shape().typeShape());

  Point pt1, localCoord1;
  double barCoord[4], vecBarCoord[4];
  double dtAdvec = 0.;
  ElementType eltTypeTmp = eltType;
  felInt ielGeoTmp = ielGeo;
  int i0;//, i1, i2;

  if (vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2] < 1e-20) //almost null velocity, no advection
    return(0);

  switch (eltShape) {

    case Triangle: {
      barCoord[2] = localCoord[0];
      barCoord[0] = localCoord[1];
      barCoord[1] = 1. - localCoord[0] - localCoord[1];
      //endpoint of velocity std::vector starting from pt
      for(int iComp=0; iComp < numComp; iComp++) {
        pt1[iComp] = pt[iComp] + vel[iComp];
      }
      //local coordinates of pt1, barycentric coordinates of vel
      m_mesh[m_currentMesh]->findLocalCoord(eltTypeTmp, ielGeoTmp, pt1, localCoord1);
      vecBarCoord[2] = localCoord1[0] - localCoord[0];
      vecBarCoord[0] = localCoord1[1] - localCoord[1];
      vecBarCoord[1] = - (vecBarCoord[0] + vecBarCoord[2]);

      //dtAdvec = time length from pt to the first intersection of [pt pt1] with an edge
      // = min{barCoord[i]/vecBarCoord[i]} s.t. vecBarCoord[i]>0 and not too small
      dtAdvec = dtLeft * 1e+6;
      i0 = 0;
      for (int ind=0; ind < 3; ind++) {
        if (vecBarCoord[ind] > 1e-10) {
          if (dtAdvec > barCoord[ind]/vecBarCoord[ind]) {
            dtAdvec = barCoord[ind]/vecBarCoord[ind];
            i0  = ind;
          }
        }
      }

      //case when advection stays in the current element, update only pt, localCoord, vel, dtLeft
      if ( (dtLeft <= dtAdvec) && (dtLeft <= dtTol) ) {
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtLeft * vel[iComp];
          localCoord[iComp] = localCoord[iComp] - dtLeft * vecBarCoord[(iComp+2)%3];
        }
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft = 0.;
        return(0);
      }

      //case when advection stays in the current element but dtLeft > dtTol
      if ( (dtLeft <= dtAdvec) && (dtLeft > dtTol) ) {
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtTol * vel[iComp];
          localCoord[iComp] = localCoord[iComp] - dtTol * vecBarCoord[(iComp+2)%3];
        }
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft -= dtTol;
        return(1);
      }

      //case when advection stays in the current element but dtAdvec > dtTol
      if ( (dtLeft > dtAdvec) && (dtAdvec >= dtTol) ) {
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtTol * vel[iComp];
          localCoord[iComp] = localCoord[iComp] - dtTol * vecBarCoord[(iComp+2)%3];
        }
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft -= dtTol;
        return(1);
      }

      //case when advection goes out of the current element through the edge i0
      if ( (dtLeft > dtAdvec) && (dtAdvec < dtTol) ) {
        //update pt, vel, dtLeft (not yet localCoord)
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtAdvec * vel[iComp]; //this point is on the edge i0 of the current element
          localCoord[iComp] = localCoord[iComp] - dtAdvec * vecBarCoord[(iComp+2)%3];
        }
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft -= dtAdvec;
        //update element = edge i0 -> adjacency and localCoord in updated element
        if ( m_mesh[m_currentMesh]->getAdjElement(eltType, ielGeo, i0) ) { //element is already updated to the adjacent one
          eltTypeTmp = eltType;
          ielGeoTmp = ielGeo;
          m_mesh[m_currentMesh]->findLocalCoord(eltTypeTmp, ielGeoTmp, pt, localCoord);
          return(1);
        } else //reach to the boundary of the domain
          return(0);
      }
      break;
    }

    case Tetrahedron: {
      barCoord[3] = localCoord[0];
      barCoord[1] = localCoord[1];
      barCoord[0] = localCoord[2];
      barCoord[2] = 1. - localCoord[0] - localCoord[1] - localCoord[2];
      //endpoint of velocity std::vector starting from pt
      for(int iComp=0; iComp < numComp; iComp++) {
        pt1[iComp] = pt[iComp] + vel[iComp];
      }
      //local coordinates of pt1, barycentric coordinates of vel
      m_mesh[m_currentMesh]->findLocalCoord(eltTypeTmp, ielGeoTmp, pt1, localCoord1);
      vecBarCoord[3] = localCoord1[0] - localCoord[0];
      vecBarCoord[1] = localCoord1[1] - localCoord[1];
      vecBarCoord[0] = localCoord1[2] - localCoord[2];
      vecBarCoord[2] = - (vecBarCoord[0] + vecBarCoord[1] + vecBarCoord[3]);

      //dtAdvec = time length from pt to the first intersection of [pt pt1] with a face
      // = min{barCoord[i]/vecBarCoord[i]} s.t. vecBarCoord[i]>0 and not too small
      dtAdvec = dtLeft * 1e+6;
      i0 = 0;
      for (int ind=0; ind < 4; ind++) {
        if (vecBarCoord[ind] > 1e-10) {
          if (dtAdvec > barCoord[ind]/vecBarCoord[ind]) {
            dtAdvec = barCoord[ind]/vecBarCoord[ind];
            i0  = ind;
          }
        }
      }

      //case when advection stays in the current element, update only pt, localCoord, vel, dtLeft
      if ( (dtLeft <= dtAdvec) && (dtLeft <= dtTol) ) {
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtLeft * vel[iComp];
        }
        localCoord[0] = localCoord[0] - dtLeft * vecBarCoord[3];
        localCoord[1] = localCoord[1] - dtLeft * vecBarCoord[1];
        localCoord[2] = localCoord[2] - dtLeft * vecBarCoord[0];
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft = 0.;
        return(0);
      }

      //case when advection stays in the current element but dtLeft > dtTol
      if ( (dtLeft <= dtAdvec) && (dtLeft > dtTol) ) {
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtTol * vel[iComp];
        }
        localCoord[0] = localCoord[0] - dtTol * vecBarCoord[3];
        localCoord[1] = localCoord[1] - dtTol * vecBarCoord[1];
        localCoord[2] = localCoord[2] - dtTol * vecBarCoord[0];
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft -= dtTol;
        return(1);
      }

      //case when advection stays in the current element but dtAdvec > dtTol
      if ( (dtLeft > dtAdvec) && (dtAdvec >= dtTol) ) {
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtTol * vel[iComp];
        }
        localCoord[0] = localCoord[0] - dtTol * vecBarCoord[3];
        localCoord[1] = localCoord[1] - dtTol * vecBarCoord[1];
        localCoord[2] = localCoord[2] - dtTol * vecBarCoord[0];
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft -= dtTol;
        return(1);
      }

      //case when advection goes out of the current element through the face i0
      if ( (dtLeft > dtAdvec) && (dtAdvec < dtTol) ) {
        //update pt, vel, dtLeft (not yet localCoord)
        for(int iComp=0; iComp < numComp; iComp++) {
          pt[iComp] = pt[iComp] - dtAdvec * vel[iComp]; //this point is on the face i0 of the current element
        }
        localCoord[0] = localCoord[0] - dtAdvec * vecBarCoord[3];
        localCoord[1] = localCoord[1] - dtAdvec * vecBarCoord[1];
        localCoord[2] = localCoord[2] - dtAdvec * vecBarCoord[0];
        vel = vecInterp(v, idVariable, numComp, localCoord, eltType, ielGeo);
        dtLeft -= dtAdvec;
        //update element = face i0 -> adjacency and localCoord in updated element
        if ( m_mesh[m_currentMesh]->getAdjElement(eltType, ielGeo, i0) ) { //element is already updated to the adjacent one
          eltTypeTmp = eltType;
          ielGeoTmp = ielGeo;
          m_mesh[m_currentMesh]->findLocalCoord(eltTypeTmp, ielGeoTmp, pt, localCoord);
          return(1);
        } else //reach to the boundary of the domain
          return(0);
      }
      break;
    }

    case NullShape:
    case Node:
    case Segment:
    case Quadrilateral:
    case Hexahedron:
    case Prism:
    case Pyramid: {
      FEL_ERROR("Unknown (or not implemented) element Shape");
      break;
    }
  }

  return(0);
}


/***********************************************************************************/
/***********************************************************************************/

Point LinearProblemNS::vecInterp(PetscVector& v, int idVariable, int numComp,
                                 const Point& localCoord, ElementType& eltType, felInt ielGeo) 
{
  //given a point in an element with its local coordinates, compute the interpolated std::vector at this point
  Point vecInt(0.);
  ElementField elemField;

  //define finite element
  const RefElement *refEle;
  const GeoElement *geoEle;
  int typeOfFiniteElement = 0;
  CurrentFiniteElement* fe;
  int numPointPerElt = 0;
  std::vector<Point*> elemPoint;
  std::vector<felInt> elemIdPoint;
  felInt ielSupportDof;

  geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  typeOfFiniteElement = m_listVariable[idVariable].finiteElementType();
  refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_mesh[m_currentMesh]);
  fe = new CurrentFiniteElement(*refEle,*geoEle,m_listVariable[idVariable].degreeOfExactness());

  //resize array
  numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  elemPoint.resize(numPointPerElt, nullptr);
  elemIdPoint.resize(numPointPerElt, 0);
  setElemPoint(eltType, ielGeo, elemPoint, elemIdPoint, &ielSupportDof);

  //interpolation
  elemField.initialize(DOF_FIELD, *fe, numComp);
  elemField.setValue(v, *fe, ielSupportDof, idVariable, m_ao, dof());
  for(int iComp=0; iComp < numComp; iComp++) {
    for (int iDof=0; iDof < fe->numDof(); iDof++) {
      vecInt[iComp] += elemField.val(iComp, iDof) * refEle->basisFunction().phi(iDof, localCoord);
    }
  }

  delete fe;
  elemPoint.clear();
  elemIdPoint.clear();
  return(vecInt);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::allocateElemFieldBoundaryConditionDerivedLinPb(const int idBCforLinCombMethod) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // implicit lumpedModelBC but applied as Neumann Normal BC
  if(m_lumpedModelBC && r_instance.model == "NSlinComb") {
    bool isFullImplicit = true;
    bool isFullExplicit = true;
    for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
      if(r_instance.lumpedModelBCAlgo[i] != 2)
        isFullImplicit = false;
      if(r_instance.lumpedModelBCAlgo[i] != 1)
        isFullImplicit = false;
    }
    if(!isFullImplicit && !isFullExplicit)
      FEL_WARNING("NSlinComb not implemented with mixed explicit and implicit algorithm for the windkessel");

    if (isFullImplicit) {
      BoundaryCondition* BC;
      std::vector <double> valueOfBC(1);
      valueOfBC[0] = 0.;
      // 1st step: std::set std::vector m_elemFieldNeumannNormal = 0
      for (std::size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); i++) {
        BC = m_boundaryConditionList.NeumannNormal(i);
        switch (BC->typeValueBC()) {
        case Constant:
          m_elemFieldNeumannNormal[i].setValue(valueOfBC);
          break;
        case FunctionT:
          m_elemFieldNeumannNormal[i].setValue(valueOfBC);
          break;
        case Vector:
        case FunctionS:
        case FunctionTS:
        case EnsightFile:
          FEL_ERROR("This kind of boundary conditions are not available.");
          break;
        }
      }

      // 2nd step: std::set m_elemFieldNeumannNormal[idBCforLinCombMethod] = -1 alternatively while preProcessing
      if ( m_fstransient->iteration == 0 ) {
        valueOfBC[0] = -1.;
        BC = m_boundaryConditionList.NeumannNormal(idBCforLinCombMethod);
        switch (BC->typeValueBC()) {
        case Constant:
          m_elemFieldNeumannNormal[idBCforLinCombMethod].setValue(valueOfBC);
          break;
        case FunctionT:
          m_elemFieldNeumannNormal[idBCforLinCombMethod].setValue(valueOfBC);
          break;
        case FunctionS:
        case Vector:
        case FunctionTS:
        case EnsightFile:
          FEL_ERROR("This kind of boundary conditions are not available.");
          break;
        }
      }
      valueOfBC.clear();
    }
  }
}

void LinearProblemNS::initExtrapol(PetscVector& V_1) 
{
  if (allocateSeqVecExt == false) {
    m_solExtrapol.create(PETSC_COMM_SELF);
    m_solExtrapol.setType(VECSEQ);
    m_solExtrapol.setSizes(PETSC_DECIDE,m_numDof);
    allocateSeqVecExt = true;
  }
  gatherVector(V_1, m_solExtrapol);
}

void LinearProblemNS::updateExtrapol(PetscVector& V_1) 
{
  gatherVector(V_1, m_solExtrapol);
}

void LinearProblemNS::gatherVectorBeforeAssembleMatrixRHS() 
{
  if (allocateSeqVec == false) {
    m_seqBdfRHS.create(PETSC_COMM_SELF);
    m_seqBdfRHS.setType(VECSEQ);
    m_seqBdfRHS.setSizes(PETSC_DECIDE,m_numDof);
    allocateSeqVec = true;
  }
  gatherVector(m_bdf->vector(), m_seqBdfRHS);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::addMatrixRHS() 
{

  //! The not constant matrix (0) is summed with the constant matrix (1).
  //! The result is stored into the not constant matrix (0)
  //! When using ALE there is not a constant matrix (1) and therefore is useless to do it.
  
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( r_instance.useFDlagrangeMult )
    matrix(0).axpy(1,matrix(1),SUBSET_NONZERO_PATTERN);
  else if( r_instance.useALEformulation == 0 )
    matrix(0).axpy(1,matrix(1),SAME_NONZERO_PATTERN);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::addMatrixRHSNl(const FlagMatrixRHS flagMatrixRHS) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  switch (flagMatrixRHS) {
    case FlagMatrixRHS::only_matrix:
      matrix(0).axpy(1, matrix(2), SAME_NONZERO_PATTERN);
      break;
    case FlagMatrixRHS::only_rhs:
      if (r_instance.useALEformulation)
        vector().axpy(1, externalVec(1));
      break;
    default :
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( r_instance.zeroDisplacementAtBoarders ) {
    if (FelisceParam::verbose() > 2)
      std::cout<<"["<<rank<<"]--Applying zero-Dirichlet BCs on the velocity at the interface between inlet outlet and surface."<<std::endl;
    getRings();
    double bigValue=r_instance.penValueForEmbedFSI*1e10;

    for(auto itDofBC = m_idDofRings.begin(); itDofBC != m_idDofRings.end(); ++itDofBC) {
      if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix))
        matrix(0).setValue(*itDofBC, *itDofBC, bigValue, INSERT_VALUES);
      if((flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) ||( (flagMatrixRHS == FlagMatrixRHS::only_rhs)
          &&(m_meshUpdateState==FlagMeshUpdated::PreviousMesh) ) )
        vector().setValue(*itDofBC,0.0, INSERT_VALUES);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::applyNaturalBoundaryConditionLumpedModelBC(std::vector<Point*>& /* elemPoint */, const std::vector<felInt>& /* elemIdPoint */, int /* label */, felInt /* numEltPerLabel */, felInt* /* ielSupportDof */, std::size_t iLumpedModelBC, ElementType& /* eltType */, felInt /* numElement_eltType */, FlagMatrixRHS /* flagMatrixRHS */) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // to apply "NeumannNormal" BC with -p0
  if (r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 1)
    PlumpedModelBC.push_back(-(m_lumpedModelBC->Pp(iLumpedModelBC)));
  else if (r_instance.lumpedModelBCAlgo[iLumpedModelBC] == 2) {
    // Add the explicit part on the RHS
    if(r_instance.lumpedModelBCType[iLumpedModelBC] == 1) {
      // RCR or R type
      double alpha = r_instance.lumpedModelBC_Rdist[iLumpedModelBC] * r_instance.lumpedModelBC_C[iLumpedModelBC];
      double beta = r_instance.timeStep;
      double Pv = r_instance.lumpedModelBC_Pvenous[iLumpedModelBC];
      double Pd = m_lumpedModelBC->Pd(iLumpedModelBC);
      PlumpedModelBC.push_back(-(alpha*Pd + beta*Pv)/(alpha + beta));
    } else if(r_instance.lumpedModelBCType[iLumpedModelBC] == 2) {
      // RC type
      PlumpedModelBC.push_back(-(m_lumpedModelBC->Pd(iLumpedModelBC)));
    }
  }
  m_elemFieldLumpedModelBC[iLumpedModelBC].setValue(PlumpedModelBC);

  if (verbosity()>2) {
    std::cout << "LumpedModelBC on label " << m_currentLabel << std::endl;
    std::cout << "Applied PlumpedModelBC = " <<  -PlumpedModelBC[0] << std::endl;
  }

  PlumpedModelBC.clear();
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::assembleMatrixImplLumpedModelBC(int rank, const std::size_t iLabel) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if (m_fstransient->iteration == 0) {
    int label = r_instance.lumpedModelBCLabel[iLabel];
    PetscScalar coef = 0.;
    if ( r_instance.lumpedModelBCType[iLabel] == 1 ) {
      // R lumpedModel bc, or RCR lumpedModel bc
      coef = r_instance.lumpedModelBC_Rdist[iLabel]/(1.0 + r_instance.lumpedModelBC_Rdist[iLabel] * r_instance.lumpedModelBC_C[iLabel]/r_instance.timeStep) + r_instance.lumpedModelBC_Rprox[iLabel];
    } else if ( r_instance.lumpedModelBCType[iLabel] == 2 ) {
      // RC lumpedModel bc
      coef = r_instance.lumpedModelBC_Rprox[iLabel] + r_instance.timeStep / r_instance.lumpedModelBC_C[iLabel];
    }

    double *tmp_phi_i_dot_n = new double [m_numDof];
    double *phi_i_dot_n = new double [m_numDof]; // to store globally \int_Gamma phi_i n
    int *tmp_ind = new int [m_numDof];
    int *ind = new int [m_numDof];  // to mark the position of matrix assembling
    felInt node = 0;
    int idVar = m_listVariable.getVariableIdList(velocity);
    //      int iUnknown = m_listVariable.listIdUnknownOfVariable(idVar);
    int numComp = m_listVariable[idVar].numComponent();
    ElementType eltType;
    felInt ielGeom;
    int numPointPerElt = 0;
    int currentLabel = 0;
    felInt numEltPerLabel = 0;
    std::vector<Point*> elemPoint;
    std::vector<felInt> elemIdPoint;
    felInt ielSupportDof;

    // initialize tmp_phi_i_dot_n and phi_i_dot_n to 0
    for (int iDof = 0; iDof < m_numDof; iDof++) {
      tmp_phi_i_dot_n[iDof] = 0.;
      phi_i_dot_n[iDof] = 0.;
      tmp_ind[iDof] = 0;
      ind[iDof] = 0;
    }
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
    for (std::size_t iTyp = 0; iTyp < bagElementTypeDomainBoundary.size(); ++iTyp) {
      eltType =  bagElementTypeDomainBoundary[iTyp];
      ielGeom = 0;
      defineCurvilinearFiniteElement(eltType);
      initElementArrayBD();
      CurvilinearFiniteElement& fe = *m_listCurvilinearFiniteElement[idVar];
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);

      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin();
            itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        if (currentLabel == label) {
          // compute tmp_phi_i_dot_n = local \int_Gamma phi_i n = local \sum \int_{element of Gamma} phi_i n
          for (felInt iel1 = 0; iel1 < numEltPerLabel; iel1++) {
            setElemPoint(eltType, ielGeom, elemPoint, elemIdPoint, &ielSupportDof);
            fe.updateMeasNormal(0, elemPoint);
            for (int iDof = 0; iDof < fe.numDof(); iDof++) {
              for (int iComp = 0; iComp < numComp; iComp++) {
                dof().loc2glob(ielSupportDof,iDof,idVar,iComp,node);
                tmp_ind[node] = 1;
                for (int ig=0; ig < fe.numQuadraturePoint(); ig++) {
                  tmp_phi_i_dot_n[node] += fe.phi[ig](iDof) * fe.normal[ig](iComp) * fe.weightMeas(ig);
                }
              }
            }
            ielGeom++;
          } // end of loop on iel1 of each label
        } // end of if currentLabel == label
        else
          ielGeom += numEltPerLabel;
      } //end of loop on itRef
    } // end of loop on iTyp

    // synchronise value of tmp_phi_i_dot_n and tmp_ind
    MPI_Reduce(&tmp_phi_i_dot_n[0], &phi_i_dot_n[0], m_numDof, MPI_DOUBLE, MPI_SUM,0,MpiInfo::petscComm());
    MPI_Reduce(&tmp_ind[0], &ind[0], m_numDof, MPI_INT, MPI_SUM,0,MpiInfo::petscComm());

    if (rank == 0) {
      felInt cptDof = 0;
      for (int iDof = 0; iDof < m_numDof; iDof++) {
        if (ind[iDof] > 0) {
          tmp_ind[cptDof] = iDof;
          tmp_phi_i_dot_n[cptDof] = phi_i_dot_n[iDof];
          cptDof++;
        }
      }

      std::vector<double*> valueMat;
      for (unsigned int i = 0; i < numberOfMatrices(); i++) {
        valueMat.push_back(new double[cptDof*cptDof]);
      }
      for (int iRow = 0; iRow < cptDof; iRow++) {
        for (int iCol = 0; iCol < cptDof; iCol++) {
          valueMat[0][iCol + iRow*cptDof] = 0.;
          valueMat[1][iCol + iRow*cptDof] = coef * tmp_phi_i_dot_n[iRow] * tmp_phi_i_dot_n[iCol];
        }
      }
      AOApplicationToPetsc(m_ao,cptDof,tmp_ind);
      for (unsigned int i = 0; i < numberOfMatrices(); i++) {
        matrix(i).setValues(cptDof,tmp_ind,cptDof,tmp_ind,valueMat[i],ADD_VALUES);
        delete [] valueMat[i];
      }
      valueMat.clear();
    }

    delete [] tmp_phi_i_dot_n;
    delete [] phi_i_dot_n;
    delete [] tmp_ind;
    delete [] ind;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::userChangePattern(int /* numProc */, int /* rankProc */) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  bool isImplicitLumpedModelBCused = false;
  for(std::size_t i=0; i<r_instance.lumpedModelBCAlgo.size(); i++) {
    if(r_instance.lumpedModelBCAlgo[i] == 2)
      isImplicitLumpedModelBCused = true;
  }

  // ------------------------------------------- //
  // monolithic model and implicit LumpedModelBC //
  // ------------------------------------------- //
  if (r_instance.lumpedModelBCLabel.size() > 0 && isImplicitLumpedModelBCused && r_instance.model == "NS") {
    // re-build random repartition of dof on processes
    felInt numDofByProc = m_numDof/MpiInfo::numProc();
    felInt *numDofPerProc = new felInt [MpiInfo::numProc()];
    for (int i = 0; i < MpiInfo::numProc(); i++) {
      if (i == MpiInfo::numProc() - 1)
        numDofPerProc[i] = m_numDof - i*numDofByProc;
      else
        numDofPerProc[i] = numDofByProc;
    }
    felInt scaleDof = 0;
    for (int i = 0; i < MpiInfo::rankProc(); i++) {
      scaleDof += numDofPerProc[i];
    }
    felInt *rankDof = new felInt[m_numDof];
    for (int i = 0; i < m_numDof; i++) {
      rankDof[i] = -1;
    }
    for (felInt i = 0; i < numDofPerProc[MpiInfo::rankProc()]; i++) {
      rankDof[i+scaleDof] = MpiInfo::rankProc();
    }

    // loop to connect all added dof
    std::vector< std::set<felInt> > nodesNeighborhood(m_numDof);
    felInt node1 = 0;
    felInt node2 = 0;
    const int idVar    = m_listVariable.getVariableIdList(velocity);
    const int iUnknown = m_listVariable.listIdUnknownOfVariable(idVar);
    const int numComp  = m_listVariable[idVar].numComponent();
    const std::size_t iMesh = m_listVariable[idVar].idMesh();
    int searchLabel = -1;
    ElementType eltType;
    int currentLabel = 0;
    int numEltPerLabel = 0;
    felInt ielSupportDof1, ielSupportDof2;
    felInt ielGeom1, ielGeom2, ielGeomTmp;

    for (std::size_t iLabel = 0; iLabel < r_instance.lumpedModelBCLabel.size(); iLabel++) {
      if(r_instance.lumpedModelBCAlgo[iLabel] == 2) {
        searchLabel = r_instance.lumpedModelBCLabel[iLabel];
        const std::vector<ElementType>& bagElementTypeDomainBoundary = m_mesh[iMesh]->bagElementTypeDomainBoundary();
        for (std::size_t iTyp = 0; iTyp < bagElementTypeDomainBoundary.size(); ++iTyp) {
          eltType =  bagElementTypeDomainBoundary[iTyp];
          ielGeom1 = 0;

          for(auto itRef = m_mesh[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_mesh[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
            currentLabel = itRef->first;
            numEltPerLabel = itRef->second.second;

            if (searchLabel == currentLabel) {
              ielGeomTmp = ielGeom1;
              for (felInt iel1 = 0; iel1 < numEltPerLabel; iel1++) {
                // find ielSupportDof1
                supportDofUnknown(iUnknown).getIdElementSupport(eltType, ielGeom1, ielSupportDof1);
                for (int iSupport = 0; iSupport < supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof1); iSupport++) {
                  for (int iComp = 0; iComp < numComp; iComp++) {
                    dof().loc2glob(ielSupportDof1,iSupport,idVar,iComp,node1);
                    if (rankDof[node1] == MpiInfo::rankProc()) {
                      ielGeom2 = ielGeomTmp;
                      for (felInt iel2 = 0; iel2 < numEltPerLabel; iel2++) {
                        supportDofUnknown(iUnknown).getIdElementSupport(eltType, ielGeom2, ielSupportDof2);
                        for (int jSupport = 0; jSupport < supportDofUnknown(iUnknown).getNumSupportDof(ielSupportDof2); jSupport++) {
                          for (int jComp = 0; jComp < numComp; jComp++) {
                            dof().loc2glob(ielSupportDof2,jSupport,idVar,jComp,node2);
                            if ( node1 != node2)
                              nodesNeighborhood[node1].insert(node2);
                          }
                        }
                        ielGeom2++;
                      } // end of loop on iel2 of each label
                    } // end of if rankDof[node1] == MpiInfo::rankProc()
                  } // end of loop on iComp
                } // end of loop on iSupport
                ielGeom1++;
              } // end of loop on iel1 of each label
            } // end of if searchLabel == currentLabel
            else
              ielGeom1 += numEltPerLabel;
          } //end of loop on itRef
        } //end of loop on bagElementTypeDomainBoundary
      } // end of if lumpedModelBCAlgo[iLabel] == 2
    } //end of loop on lumpedModelBCLabel

    // Storage the added dof in CSR format
    felInt dofSize = 0;
    for (unsigned int iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
      dofSize += nodesNeighborhood[iNode].size();
    }

    std::vector<felInt> m_iCSR1(dof().pattern().numRows()+1, 0);
    std::vector<felInt> m_jCSR1(dofSize, 0);
    felInt pos = 0;
    felInt cptDof = 0;
    for (unsigned int iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
      if (rankDof[iNode] == MpiInfo::rankProc()) {
        m_iCSR1[cptDof+1 ] = m_iCSR1[cptDof] + nodesNeighborhood[iNode].size();
        pos = 0;
        for(auto iCon = nodesNeighborhood[iNode].begin(); iCon != nodesNeighborhood[iNode].end(); iCon++) {
          m_jCSR1[m_iCSR1[cptDof] + pos] = *iCon;
          pos++;
        }
        cptDof++;
      }
    }

    // merge pattern
    dof().mergeGlobalPattern(m_iCSR1, m_jCSR1);

    delete [] numDofPerProc;
    delete [] rankDof;
    nodesNeighborhood.clear();
    m_iCSR1.clear();
    m_jCSR1.clear();
  }

  // ---------------------------------------------- //
  // Modify pattern for face-oriented stabilization //
  // ---------------------------------------------- //
  if (r_instance.NSStabType == 1) {
    // If the face-oriented stabilization is used, the dofs of an element an all its neighbor
    // are linked together.

    // compute initial (uniform) repartition of dof on processes
    felInt numDofByProc = m_numDof/MpiInfo::numProc();
    std::vector<felInt> numDofPerProc(MpiInfo::numProc());
    for(felInt i=0; i<MpiInfo::numProc(); ++i) {
      if(i == MpiInfo::numProc() - 1)
        numDofPerProc[i] = m_numDof - i*numDofByProc;
      else
        numDofPerProc[i] = numDofByProc;
    }

    felInt shiftDof = 0;
    for(felInt i=0; i<MpiInfo::rankProc(); ++i)
      shiftDof += numDofPerProc[i];

    std::vector<felInt> rankDof(m_numDof, -1);
    for(felInt i=0; i<numDofPerProc[MpiInfo::rankProc()]; ++i)
      rankDof[i + shiftDof] = MpiInfo::rankProc();

    // variables
    GeometricMeshRegion::ElementType eltType, eltTypeOpp;
    const int idVar = m_listVariable.getVariableIdList(velocity);
    const std::size_t iMesh = m_listVariable[idVar].idMesh();
    felInt idVar1, idVar2;
    felInt node1, node2;
    felInt numFacesPerElement, numEltPerLabel;
    felInt ielSupport, jelSupport;
    felInt ielCurrentGeo, ielOppositeGeo;
    std::vector<felInt> vecSupport, vecSupportOpposite;
    std::vector<felInt> numElement(GeometricMeshRegion::m_numTypesOfElement, 0);
    std::vector< std::set<felInt> > nodesNeighborhood(m_numDof);

    // zeroth loop on the unknown of the linear problem
    for (std::size_t iUnknown1 = 0; iUnknown1 < m_listUnknown.size(); ++iUnknown1) {
      idVar1 = m_listUnknown.idVariable(iUnknown1);

      if ( iMesh != m_listVariable[idVar1].idMesh() )
        continue;

      // build the edges or faces depending on the dimension (for the global mesh)
      // In this function, "faces" refers to either edges or faces.
      if(dimension() == 2)
        m_mesh[iMesh]->buildEdges();
      else
        m_mesh[iMesh]->buildFaces();

      // first loop on element type
      ielCurrentGeo = 0;
      for (std::size_t i=0; i<m_mesh[iMesh]->bagElementTypeDomain().size(); ++i) {
        eltType =  m_mesh[iMesh]->bagElementTypeDomain()[i];
        const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
        numElement[eltType] = 0;
        numFacesPerElement  = geoEle->numBdEle();

        // second loop on region of the mesh.
        for(auto itRef = m_mesh[iMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_mesh[iMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
          numEltPerLabel = itRef->second.second;

          // Third loop on element
          for (felInt iel = 0; iel < numEltPerLabel; iel++) {
            m_supportDofUnknown[iUnknown1].getIdElementSupport(eltType, numElement[eltType], vecSupport);

            for(std::size_t ielSup=0; ielSup<vecSupport.size(); ++ielSup) {
              ielSupport = vecSupport[ielSup];

              // for all support dof in this support element
              for (felInt iSup = 0; iSup < m_supportDofUnknown[iUnknown1].getNumSupportDof(ielSupport); ++iSup) {
                // for all component of the unknown
                for (std::size_t iComp = 0; iComp < m_listVariable[idVar1].numComponent(); iComp++) {
                  // get the global id of the support dof
                  dof().loc2glob(ielSupport, iSup, idVar1, iComp, node1);

                  // if this node is on this process
                  if(rankDof[node1] == MpiInfo::rankProc()) {
                    // loop over the boundaries of the element
                    for(felInt iface=0; iface < numFacesPerElement; ++iface) {
                      // check if this face is an inner face or a boundary
                      ielOppositeGeo = ielCurrentGeo;
                      eltTypeOpp = eltType;
                      bool isAnInnerFace = m_mesh[iMesh]->getAdjElement(eltTypeOpp, ielOppositeGeo, iface);

                      if(isAnInnerFace) {
                        // for all unknown
                        for (std::size_t iUnknown2 = 0; iUnknown2 < m_listUnknown.size(); ++iUnknown2) {
                          idVar2 = m_listUnknown.idVariable(iUnknown2);

                          if ( iMesh != m_listVariable[idVar2].idMesh() )
                            continue;

                          // get the support element of the opposite element
                          supportDofUnknown(iUnknown2).getIdElementSupport(ielOppositeGeo, vecSupportOpposite);

                          // for all support element
                          for(std::size_t jelSup=0; jelSup<vecSupportOpposite.size(); ++jelSup) {
                            jelSupport = vecSupportOpposite[jelSup];

                            // for all support dof in this support element
                            for (felInt jSup = 0; jSup < supportDofUnknown(iUnknown2).getNumSupportDof(jelSupport); ++jSup) {

                              // for all component of the second unknown
                              for (std::size_t jComp = 0; jComp < m_listVariable[idVar2].numComponent(); jComp++) {

                                // If the two current components are connected
                                const felInt iConnect = dof().getNumGlobComp(iUnknown1, iComp);
                                const felInt jConnect = dof().getNumGlobComp(iUnknown2, jComp);
                                if (m_listUnknown.mask()(iConnect, jConnect) > 0) {
                                  // get the global id of the second support dof
                                  dof().loc2glob(jelSupport, jSup, idVar2, jComp, node2);

                                  // remove diagonal term to define pattern and use it in Parmetis.
                                  // if the two global ids are different, they are neighboors
                                  if(node1 != node2)
                                    nodesNeighborhood[node1].insert(node2);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            ++ielCurrentGeo;
            ++numElement[eltType];
          }
        }
      }
    }

    // Store the pattern in CSR style
    std::vector<felInt> iCSR, jCSR;
    felInt dofSize = 0;
    felInt cptDof = 0;
    felInt pos;

    iCSR.resize(dof().pattern().numRows() + 1, 0);
    for(std::size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode)
      dofSize += nodesNeighborhood[iNode].size();

    jCSR.resize(dofSize, 0);
    for(std::size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode) {
      if(rankDof[iNode] == MpiInfo::rankProc()) {
        iCSR[cptDof + 1] = iCSR[cptDof] + nodesNeighborhood[iNode].size();
        pos = 0;
        for (auto it=nodesNeighborhood[iNode].begin(); it != nodesNeighborhood[iNode].end(); ++it) {
          jCSR[iCSR[cptDof] + pos] = *it;
          ++pos;
        }
        ++cptDof;
      }
    }

    // Now, call merge pattern
    dof().mergeGlobalPattern(iCSR, jCSR);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::reviseSolution(PetscVector& alphai, std::vector<PetscVector>& stationnarySolutions) 
{
  // m_sol = u^n = \tilde{u}^n + \sum_i alpha_i^n u_i

  // alpha_i^n = m_coefProblem->solution()[i]
  // u_i = m_stationnarySolutionsVec[i]
  // \tilde{u}^n = m_linearProblem[0]->solution() = m_sol already

  PetscScalar coef ;
  for (PetscInt jj = 0 ; jj < (PetscInt) stationnarySolutions.size() ; jj++ ) {
    alphai.getValues(1, &jj, &coef);
    // m_sol = coef * statSol + m_sol
    solution().axpy(coef, stationnarySolutions[jj]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::computeRHSThetaMethod(felInt& iel) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  //==============================================
  //  Provisional implementation
  //==============================================

  m_elMatRHSThetaMethod[0]->mat().clear();

  if ( ((m_fstransient->iteration == 1) && (r_instance.useALEformulation == 0))
       || ((m_fstransient->iteration > 0) && (r_instance.useALEformulation)) ) {
    std::size_t indexMatrix;
    if( r_instance.useALEformulation )
      indexMatrix = 0;
    else
      indexMatrix = 1;
    m_elMatRHSThetaMethod[indexMatrix]->mat().clear();
    if (m_useSymmetricStress) {
      m_elMatRHSThetaMethod[indexMatrix]->eps_phi_i_eps_phi_j((m_theta-1)*2*m_viscosity, *m_feVel, 0, 0, dimension());
    } else {
      m_elMatRHSThetaMethod[indexMatrix]->grad_phi_i_grad_phi_j((m_theta-1)*m_viscosity, *m_feVel, 0, 0, dimension());
    }
  }

  if ( m_fstransient->iteration > 0) {
    //=======================
    //   Convective Term
    //=======================
    switch(r_instance.NSequationFlag) {
    case 0:
      break;
    case 1:
      if(r_instance.useALEformulation) {
        m_elemFieldAdv.setValue(m_beta, *m_feVel, iel, m_iVelocity, m_ao, dof());
      } else {
        m_elemFieldAdv.setValue(m_solExtrapol, *m_feVel, iel, m_iVelocity, m_ao, dof());
      }
      m_elMatRHSThetaMethod[0]->u_grad_phi_j_phi_i((m_theta-1)*m_density,m_elemFieldAdv,*m_feVel,0,0,dimension());
      break;
    default:
      FEL_ERROR("Error: No theta-method for this kind of Convection Method ");
    }
  }

  if( r_instance.useALEformulation == 0)
    m_elMatRHSThetaMethod[0]->mat() = m_elMatRHSThetaMethod[0]->mat() + m_elMatRHSThetaMethod[1]->mat();

  m_elemFieldAdv.setValue(sequentialSolution(), *m_feVel, iel, m_iVelocity, m_ao, dof());
  m_elemFieldPres.setValue(sequentialSolution(), *m_fePres, iel, m_iPressure, m_ao, dof());

  UBlasVector auxVec(m_elementVector[0]->vec().size());
  std::size_t entry=0;

  for (std::size_t velComp=0; velComp <m_elemFieldAdv.get_val().size1() ; velComp++) {
    for (std::size_t velDof=0; velDof <m_elemFieldAdv.get_val().size2() ; velDof++) {
      auxVec(entry) = row(m_elemFieldAdv.get_val(),velComp)(velDof);
      entry++;
    }
  }
  for (std::size_t pressDof=0; pressDof <m_elemFieldPres.get_val().size2() ; pressDof++) {
    auxVec(entry) = row(m_elemFieldPres.get_val(),0)(pressDof);
    entry++;
  }

  axpy_prod(m_elMatRHSThetaMethod[0]->mat(),auxVec,m_elementVector[0]->vec(), false);

}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::assembleFaceOrientedStabilization() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  /*!
  * Assembly loop to compute the face-oriented stabilization
  * E. Burman, M. A. Fernandez and P. Hansbo, Continuous interior penalty finite element method
  * for Oseen's equations, SIAM J. Numer. Anal., 44(3), 1248-1274, 2006.
  *
  * This function works if the mesh has only one type of element !!!
  */
  // definition of variables
  std::pair<bool, GeometricMeshRegion::ElementType> adjElt;
  GeometricMeshRegion::ElementType eltType;     // Type of element

  felInt ielCurrentLocalGeo = 0;             // local geometric id of the current element
  felInt ielCurrentGlobalGeo = 0;            // global geometric id of the current element
  felInt ielOppositeGlobalGeo = 0;           // global geometric id of the opposite element
  felInt numFacesPerType;                    // number of faces of an element of a given type
  felInt numPointPerElt;                     // number of vertices by element
  felInt ielCurrentGlobalSup;                // global support element id of the current element
  felInt ielOppositeGlobalSup;               // global support element id of the opposite element

  std::vector<felInt> idOfFacesCurrent;           // ids of the current element edges
  std::vector<felInt> idOfFacesOpposite;          // ids of the opposite element edges
  std::vector<felInt> currentElemIdPoint;         // ids of the vertices of the current element
  std::vector<felInt> oppositeElemIdPoint;        // ids of the vertices of the opposite element
  std::vector<Point*> currentElemPoint;           // point coordinates of the current element vertices
  std::vector<Point*> oppositeElemPoint;          // point coordinates of the opposite element vertices

  FlagMatrixRHS flag = FlagMatrixRHS::only_matrix;     // flag to only assemble the matrix
  felInt idFaceToDo;
  bool allDone = false;

  ElementField elemFieldAdvFace;

  felInt rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  const int iVel  = m_listVariable.getVariableIdList(velocity); // TODO D.C. ALREADY DEFINED IN CALSS???
  const int iPres = m_listVariable.getVariableIdList(pressure);
  m_velocity = &m_listVariable[iVel ];
  m_pressure = &m_listVariable[iPres];

  if ( m_velocity->idMesh() != m_pressure->idMesh() && m_pressure->idMesh() != static_cast<std::size_t>(m_currentMesh) )
    FEL_ERROR("Velocity and pressure defined on different meshes, or not defined on m_currentMesh");

  // bag element type vector
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[m_currentMesh]->bagElementTypeDomain();
  eltType = bagElementTypeDomain[0];

  // Finite Element With Bd for the opposite element
  CurrentFiniteElementWithBd* ptmp;
  CurrentFiniteElementWithBd* oppositeFEWithBdVel;
  CurrentFiniteElementWithBd* oppositeFEWithBdPre;

  const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;

  felInt typeOfFEVel = m_velocity->finiteElementType();
  const RefElement *refEleVel = geoEle->defineFiniteEle(eltType, typeOfFEVel, *m_mesh[m_currentMesh]);
  oppositeFEWithBdVel = new CurrentFiniteElementWithBd(*refEleVel, *geoEle, m_velocity->degreeOfExactness(), m_velocity->degreeOfExactness());

  felInt typeOfFEPre = m_pressure->finiteElementType();
  const RefElement *refElePre = geoEle->defineFiniteEle(eltType, typeOfFEPre, *m_mesh[m_currentMesh]);
  oppositeFEWithBdPre = new CurrentFiniteElementWithBd(*refElePre, *geoEle, m_pressure->degreeOfExactness(), m_pressure->degreeOfExactness());


  // initializing variables
  numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  numFacesPerType = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->numBdEle();

  // resize of vectors
  idOfFacesCurrent.resize(numFacesPerType, -1);
  idOfFacesOpposite.resize(numFacesPerType, -1);
  currentElemIdPoint.resize(numPointPerElt, -1);
  oppositeElemIdPoint.resize(numPointPerElt, -1);
  currentElemPoint.resize(numPointPerElt, nullptr);
  oppositeElemPoint.resize(numPointPerElt, nullptr);

  // define finite element
  defineFiniteElement(eltType);
  initElementArray();
  defineCurrentFiniteElementWithBd(eltType);

  // allocate arrays for assembling the matrix
  allocateArrayForAssembleMatrixRHS(flag);

  // init variables
  initPerElementType(eltType, flag);
  m_feVelWithBd = m_listCurrentFiniteElementWithBd[m_iVelocity];
  m_fePreWithBd = m_listCurrentFiniteElementWithBd[m_iPressure];


  CurrentFiniteElementWithBd* firstCurrentVel = m_feVelWithBd;
  CurrentFiniteElementWithBd* firstCurrentPre = m_fePreWithBd;
  CurrentFiniteElementWithBd* firstOppositeVel = oppositeFEWithBdVel;
  CurrentFiniteElementWithBd* firstOppositePre = oppositeFEWithBdPre;

  // get informations on the current element
  setElemPoint(eltType, 0, currentElemPoint, currentElemIdPoint, &ielCurrentGlobalSup);

  // update the finite elements
  m_feVelWithBd->updateFirstDeriv(0, currentElemPoint);
  m_feVelWithBd->updateBdMeasNormal();
  m_fePreWithBd->updateFirstDeriv(0, currentElemPoint);
  m_fePreWithBd->updateBdMeasNormal();

  // get the global id of the first geometric element
  ISLocalToGlobalMappingApply(m_mappingElem[m_currentMesh], 1, &ielCurrentLocalGeo, &ielCurrentGlobalGeo);

  // the std::unordered_map to remember what contribution have already been computed
  std::map<felInt, std::vector<bool> > contribDone;
  addNewFaceOrientedContributor(numFacesPerType, ielCurrentGlobalGeo, contribDone[ielCurrentGlobalGeo]);

  // build the stack to know what is the next element
  std::stack<felInt> nextInStack;
  nextInStack.push(ielCurrentGlobalGeo);

  // get all the faces of the element
  if(dimension() == 2)
    m_mesh[m_currentMesh]->getAllEdgeOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);
  else
    m_mesh[m_currentMesh]->getAllFaceOfElement(ielCurrentGlobalGeo, idOfFacesCurrent);

  // loop over all the element (snake style)
  while(!nextInStack.empty()) {
    // check the faces and use the first one that is an inner face and that has not been done yet.
    idFaceToDo = -1;
    allDone = true;
    for(std::size_t iface=0; iface<contribDone[ielCurrentGlobalGeo].size(); ++iface) {
      if(!contribDone[ielCurrentGlobalGeo][iface]) {
        // This is an inner face, check if the contribution has already been computed or not
        if(idFaceToDo == -1) {
          idFaceToDo = iface;
        } else {
          allDone = false;
        }
      }
    }

    // update the stack
    if(!allDone)
      nextInStack.push(ielCurrentGlobalGeo);

    if(nextInStack.top() == ielCurrentGlobalGeo && allDone)
      nextInStack.pop();


    // assemble terms
    if(idFaceToDo != -1) {
      // get the opposite id of the element
      ielOppositeGlobalGeo = ielCurrentGlobalGeo;
      adjElt = m_mesh[m_currentMesh]->getAdjElement(ielOppositeGlobalGeo, idFaceToDo);

      // get the type of the opposite element
      // eltTypeOpp = adjElt.second;

      // update the opposite finite element and std::set ielOppositeGlobalSup
      updateFaceOrientedFEWithBd(oppositeFEWithBdVel, oppositeFEWithBdPre, idOfFacesOpposite, numPointPerElt, ielOppositeGlobalGeo, ielOppositeGlobalSup);

      // find the local id of the edge in the opposite element
      felInt localIdFaceOpposite = -1;
      for(std::size_t jface=0; jface<idOfFacesOpposite.size(); ++jface) {
        if(idOfFacesCurrent[idFaceToDo] == idOfFacesOpposite[jface]) {
          localIdFaceOpposite = jface;
        }
      }


      // compute coefficients
      elemFieldAdvFace.initialize(DOF_FIELD, m_feVelWithBd->bdEle(idFaceToDo), dimension());
      elemFieldAdvFace.setValue(m_solExtrapol, *m_feVelWithBd, idFaceToDo, ielCurrentGlobalSup, m_iVelocity, m_ao, dof());
      UBlasVector tmpval = prod(trans(elemFieldAdvFace.val), m_feVelWithBd->bdEle(idFaceToDo).normal[0]);
      double normVelNormalBd = norm_inf(tmpval);
      double normVelBd = 0.;
      for(std::size_t jcol=0; jcol<elemFieldAdvFace.val.size2(); ++jcol)
        normVelBd = std::max(normVelBd, norm_2(column(elemFieldAdvFace.val, jcol)));

      double hK = 0.5 * (m_feVelWithBd->diameter() + oppositeFEWithBdVel->diameter());
      double ReK = m_density * normVelBd * hK / m_viscosity;

      double coeffVel = r_instance.stabFOVel * std::min(1., ReK) * hK * hK;
      double gammaU = coeffVel * normVelNormalBd;
      double gammaP = 0;
      if(r_instance.NSequationFlag == 0) {
        gammaP   = r_instance.stabFOPre * hK * hK * hK / m_viscosity;
      } else {
        double xiPre = ReK < 1 ? m_density * hK * hK * hK / m_viscosity : hK * hK / normVelBd;
        gammaP   = r_instance.stabFOPre * xiPre;
      }

      // We can now compute all the integrals.
      // There is a minus sign for gammaP because there is one for q div(u).
      // current element for phi_i and phi_j
      m_elementMat[0]->zero();
      m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammaU, *m_feVelWithBd, *m_feVelWithBd, idFaceToDo, idFaceToDo, 0, 0, dimension());
      m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *m_fePreWithBd, *m_fePreWithBd, idFaceToDo, idFaceToDo, dimension(), dimension(), 1);
      setValueMatrixRHS(ielCurrentGlobalSup, ielCurrentGlobalSup, flag);

      // current element for phi_i and opposite element for phi_j
      m_elementMat[0]->zero();
      m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammaU, *oppositeFEWithBdVel, *m_feVelWithBd, localIdFaceOpposite, idFaceToDo, 0, 0, dimension());
      m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *oppositeFEWithBdPre, *m_fePreWithBd, localIdFaceOpposite, idFaceToDo, dimension(), dimension(), 1);
      setValueMatrixRHS(ielCurrentGlobalSup, ielOppositeGlobalSup, flag);

      // opposite element for phi_i and current element for phi_j
      m_elementMat[0]->mat() = trans(m_elementMat[0]->mat());
      setValueMatrixRHS(ielOppositeGlobalSup, ielCurrentGlobalSup, flag);

      // opposite element for phi_i and phi_j
      m_elementMat[0]->zero();
      m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(gammaU, *oppositeFEWithBdVel, *oppositeFEWithBdVel, localIdFaceOpposite, localIdFaceOpposite, 0, 0, dimension());
      m_elementMat[0]->grad_phi_j_dot_n_grad_psi_i_dot_n(-gammaP, *oppositeFEWithBdPre, *oppositeFEWithBdPre, localIdFaceOpposite, localIdFaceOpposite, dimension(), dimension(), 1);
      setValueMatrixRHS(ielOppositeGlobalSup, ielOppositeGlobalSup, flag);

      // update the std::unordered_map to say that this face is done
      contribDone[ielCurrentGlobalGeo][idFaceToDo] = true;

      // check if the opposite element is on this proc
      if(m_eltPart[m_currentMesh][ielOppositeGlobalGeo] == rank) {
        if(contribDone.find(ielOppositeGlobalGeo) == contribDone.end()) {
          // not found in the std::unordered_map, add it and initialize it.
          addNewFaceOrientedContributor(numFacesPerType, ielOppositeGlobalGeo, contribDone[ielOppositeGlobalGeo]);
        }
        contribDone[ielOppositeGlobalGeo][localIdFaceOpposite] = true;

        // update the finite element as the opposite finite element.
        ptmp = m_feVelWithBd;
        m_feVelWithBd = oppositeFEWithBdVel;
        oppositeFEWithBdVel = ptmp;

        ptmp = m_fePreWithBd;
        m_fePreWithBd = oppositeFEWithBdPre;
        oppositeFEWithBdPre = ptmp;

        ielCurrentGlobalGeo = ielOppositeGlobalGeo;
        ielCurrentGlobalSup = ielOppositeGlobalSup;
        idOfFacesCurrent = idOfFacesOpposite;
      } else {
        // not on this rank, take the next one in the stack
        if(!nextInStack.empty()) {
          ielCurrentGlobalGeo = nextInStack.top();
          updateFaceOrientedFEWithBd(m_feVelWithBd, m_fePreWithBd, idOfFacesCurrent, numPointPerElt, ielCurrentGlobalGeo, ielCurrentGlobalSup);
        }
      }
    } else {
      // All contribution have been already computed on this element
      // take the next element in the stack
      if(!nextInStack.empty()) {
        ielCurrentGlobalGeo = nextInStack.top();
        updateFaceOrientedFEWithBd(m_feVelWithBd, m_fePreWithBd, idOfFacesCurrent, numPointPerElt, ielCurrentGlobalGeo, ielCurrentGlobalSup);
      }
    }
  }

  // desallocate array use for assemble with petsc.
  desallocateArrayForAssembleMatrixRHS(flag);

  // desallocate opposite finite elements
  m_feVelWithBd = firstCurrentVel;
  m_fePreWithBd = firstCurrentPre;
  delete firstOppositeVel;
  delete firstOppositePre;
}

void LinearProblemNS::addNewFaceOrientedContributor(felInt size, felInt idElt, std::vector<bool>& vec) 
{
  felInt ielTmp;
  std::pair<bool, GeometricMeshRegion::ElementType> adjElt;

  vec.resize(size, false);
  for(felInt iface=0; iface<size; ++iface) {
    // check if this face is an inner face or a boundary
    ielTmp = idElt;
    adjElt = m_mesh[m_currentMesh]->getAdjElement(ielTmp, iface);

    if(!adjElt.first) {
      // not an inner face, set the contribution to done.
      vec[iface] = true;
    }
  }
}

void LinearProblemNS::updateFaceOrientedFEWithBd(CurrentFiniteElementWithBd* fevel, CurrentFiniteElementWithBd* fepre, std::vector<felInt>& idOfFaces, felInt numPoints, felInt idElt, felInt& idSup) 
{
  std::vector<felInt> elemIdPoint(numPoints, -1);
  std::vector<Point*> elemPoint(numPoints, nullptr);

  // get information on the opposite element
  // same as the function "setElemPoint" but here ielOppositeGlobalGeo is global
  // we assume that the supportDofMesh is the same for all unknown (like in setElemPoint)
  m_supportDofUnknown[0].getIdElementSupport(idElt, idSup);
  m_mesh[m_currentMesh]->getOneElement(idElt, elemIdPoint);
  for (felInt iPoint=0; iPoint<numPoints; ++iPoint)
    elemPoint[iPoint] = &(m_mesh[m_currentMesh]->listPoints()[elemIdPoint[iPoint]]);

  // update the finite elements
  fevel->updateFirstDeriv(0, elemPoint);
  fevel->updateBdMeasNormal();
  fepre->updateFirstDeriv(0, elemPoint);
  fepre->updateBdMeasNormal();

  // get all the faces of the element
  if(dimension() == 2)
    m_mesh[m_currentMesh]->getAllEdgeOfElement(idElt, idOfFaces);
  else
    m_mesh[m_currentMesh]->getAllFaceOfElement(idElt, idOfFaces);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::computeNormalField(const std::vector<int> & labels, felInt idVecVariable)
{
  m_auxiliaryInts.resize(1);
  m_auxiliaryInts[0]=idVecVariable;//for instance m_iVelocity;
  /*!
    For each dof d of the surface the sum: \f$\sum_{K} m(K)\vec n(K)\f$ is computed.
    - K are all the boundary elements that share the dof d.
    - m(K) is the measure of the element
    - n(K) is the P0 normal defined on the element
    */
  assemblyLoopBoundaryGeneral(&LinearProblemNS::normalFieldComputer,labels,&LinearProblemNS::initPerETNormVel,&LinearProblemNS::updateFeNormVel);

  m_vecs.Get("normalField").assembly();
  m_vecs.Get("measStar").assembly();

  /// Than the vector is divided by \f$\sum_{K} m(K)\f$ to obtain the P1 field.
  pointwiseDivide(m_vecs.Get("normalField"), m_vecs.Get("normalField"), m_vecs.Get("measStar"));
  gatherVector( m_vecs.Get("normalField"), m_seqVecs.Get("normalField") );

  /// using normalizeComputer we normalize the result.
  assemblyLoopBoundaryGeneral(&LinearProblemNS::normalizeComputer,labels,&LinearProblemNS::initPerETNormVel,&LinearProblemNS::updateFeNormVel);

  m_vecs.Get("normalField").assembly();
  gatherVector( m_vecs.Get("normalField"), m_seqVecs.Get("normalField") );
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::normalFieldComputer(felInt ielSupportDof)
{
  double value;
  for ( std::size_t iSurfDof(0); iSurfDof < ( std::size_t ) m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numDof(); iSurfDof++) {
    double feMeas(m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->measure());
    for ( std::size_t iCoor(0); iCoor <  ( std::size_t ) m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numCoor(); iCoor++) {
      felInt iDofGlob;
      dof().loc2glob( ielSupportDof, iSurfDof, m_auxiliaryInts[0], iCoor, iDofGlob);
      AOApplicationToPetsc(m_ao,1,&iDofGlob);
      value=0.0;
      for ( std::size_t iQuadBd(0.0) ; iQuadBd <  ( std::size_t )m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numQuadraturePoint(); iQuadBd++) {
        value += m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->weightMeas( iQuadBd ) * m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->normal[iQuadBd](iCoor);
      }
      m_vecs.Get("normalField").setValue(iDofGlob, value, ADD_VALUES);
      m_vecs.Get("measStar").setValue(iDofGlob, feMeas, ADD_VALUES);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::updateFeNormVel(const std::vector<Point*>& elemPoint,const std::vector<int>& /*elemIdPoint*/)
{
  m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->updateMeasNormal(0,elemPoint);
}

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::normalizeComputer(felInt ielSupportDof)
{
  const int numComp = m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numCoor( );
  const int numDof  = m_listCurvilinearFiniteElement[m_auxiliaryInts[0]]->numDof( );

  std::vector< double > norm(numDof,0.0);
  std::vector< std::vector< double > > NN(numDof, std::vector< double >(numComp, 0.0));
  for ( int idof = 0; idof < numDof; idof++ ) {
    for ( int icomp = 0; icomp < numComp; icomp++ ) {
      int iDofGlob;
      double value;
      dof().loc2glob( ielSupportDof, idof, m_auxiliaryInts[0], icomp, iDofGlob);
      AOApplicationToPetsc(m_ao, 1, &iDofGlob);
      m_seqVecs.Get("normalField").getValues(1, &iDofGlob, &value);
      norm[idof] += value*value;
      NN[idof][icomp] = value;
    }
    norm[idof] = std::sqrt( norm[idof] );
    for ( int icomp = 0; icomp < numComp; icomp++ )
      NN[idof][icomp] = NN[idof][icomp]/norm[idof];
  }
  for ( int idof = 0; idof < numDof; idof++ ) {
    for ( int icomp = 0; icomp < numComp; icomp++ ) {
      int iDofGlob;
      dof().loc2glob( ielSupportDof, idof, m_auxiliaryInts[0], icomp, iDofGlob);
      AOApplicationToPetsc(m_ao, 1, &iDofGlob);
      m_vecs.Get("normalField").setValue(iDofGlob, NN[idof][icomp], INSERT_VALUES);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

#ifdef FELISCE_WITH_CVGRAPH
  void LinearProblemNS::assembleMassBoundaryAndInitKSP( std::size_t iConn ) 
  {
    LinearProblem::assembleMassBoundaryAndInitKSP(&LinearProblemNS::massMatrixComputer,
                                          slave()->interfaceLabels(iConn),
                                          &LinearProblemNS::initPerETMass,
                                          &LinearProblemNS::updateFE,
                                          iConn);
  }

  void LinearProblemNS::sendData() 
  {
    gatherSolution();
    for ( std::size_t iConn(0); iConn<slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for (std::size_t cVar(0); cVar<slave()->numVarToSend(iConn); ++cVar) {
        std::string varToSend=slave()->sendVariable(iConn,cVar);
        if (  varToSend== "STRESS" ) {
          prepareResidual(iConn);
          vecs.push_back(m_seqStressOut);
        } else if  ( varToSend == "VELOCITY" ) {
          vecs.push_back(sequentialSolution());
        } else if  ( varToSend == "DISPLACEMENT" ) {
          //TODO curDisp = oldDisp+dt*sequentialSolution();
          //vecs.push_back(curDisp);
          FEL_ERROR("TODO: implement it!");
        } else {
          std::cout<<"variable to send: "<<varToSend<<std::endl;
          FEL_ERROR("Error in variable to read");
        }
      }
      slave()->sendData(vecs,iConn);
    }
  }

  void LinearProblemNS::readData() 
  {
    bool usingDisp(false);
    bool usingVel(false);
    for ( std::size_t iConn(0); iConn<slave()->numConnections(); ++iConn ) {
      std::vector<PetscVector> vecs;
      for (std::size_t cVar(0); cVar<slave()->numVarToRead(iConn); ++cVar) {
        std::string varToRead=slave()->readVariable(iConn,cVar);
        if ( varToRead == "STRESS" ) {
          vecs.push_back(m_seqVecs.Get("cvgraphSTRESS"));
        } else if  ( varToRead == "DISPLACEMENT" ) {
          usingDisp=true;
          vecs.push_back(m_seqVecs.Get("externalDisplacement"));
        } else if  ( varToRead == "VELOCITY" ) {
          usingVel=true;
          vecs.push_back(m_seqVecs.Get("cvgraphVelocity"));
        } else {
          std::cout<<"Connection: "<<iConn<<"variable to read: "<<varToRead<<std::endl;
          FEL_ERROR("Error in variable to read");
        }
      }
      slave()->receiveData(vecs,iConn);
    }
    if ( usingDisp && usingVel ) {
      // It is not safe to mix velocity vars and displacement vars, even from different connections.
      // It is possible to do it of course, but the implementation should be checked carefully.
      FEL_ERROR("You can not mix velocity and displacement.");
    }
    if ( usingDisp ) {
      m_seqVecs.Get("cvgraphVelocity").axpbypcz(1./m_fstransient->timeStep, -1./m_fstransient->timeStep, 0, m_seqVecs.Get("externalDisplacement"), m_seqVecs.Get("externalDisplacementOld"));
    }
  }

  /*! \brief Function to assemble the mass matrix
    *  Function to be called in the assemblyLoopBoundaryGeneral
    */
  void LinearProblemNS::initPerETMass() 
  {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected since it counts for all the VARIABLES
    for (std::size_t iFe = 0; iFe < m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += m_listCurvilinearFiniteElement[iFe]->numDof()*m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    m_curvFeVel = m_listCurvilinearFiniteElement[ m_iVelocity ];
  }
  
  void LinearProblemNS::massMatrixComputer(felInt ielSupportDof) 
  {
    m_elementMatBD[0]->zero();
    m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_curvFeVel,/*iblock*/0,/*iblock*/0,dimension());
    setValueMatrixBD(ielSupportDof);
  }
  
  void LinearProblemNS::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) 
  {
    m_curvFeVel->updateMeasNormal(0, elemPoint);
  }
  
  void LinearProblemNS::computeBoundaryStress(PetscVector& vec) 
  {
    // Retrieve instance
    auto& r_instance = FelisceParam::instance(instanceIndex());

    // This function replaces computeBoundaryStress implemented in linearProblem.cpp
    // I think this function is safer and more general (should work in parallel as well)

    ElementField velocityDofValuesVol;
    ElementField pressureDofValuesBD;
    ElementField tractionVel; // velocity part of \sigma \vect n
    ElementField tractionPre; // pressure part of \sigma \vect n: -p\vect n

    /*===========================================*/
    // initializations for the loop
    ElementType eltType,volEltType,previousVolEltType=GeometricMeshRegion::Nod;
    int numPointPerElt = 0;
    int currentLabel = 0;
    felInt numEltPerLabel = 0;
    felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
    for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
      eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }
    if(m_mesh[m_currentMesh]->statusFaces() == false) {
      m_mesh[m_currentMesh]->buildFaces();
    }
    std::vector<Point*> elemPoint;
    std::vector<felInt> elemIdPoint;
    std::vector<felInt> vectorIdSupport;
    felInt ielSupportDof;
    allocateVectorBoundaryConditionDerivedLinPb();
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
    /*===========================================*/

    for (std::size_t i=0; i < bagElementTypeDomainBoundary.size(); ++i) {// loop over the boundary element type
      eltType =  bagElementTypeDomainBoundary[i];
      numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);
      defineCurvilinearFiniteElement(eltType); // m_listCurvilinearFiniteElement update
      initElementArrayBD();
      allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_rhs); // works in combo with setValueCustomVectorBD

      m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];
      m_curvFePre = m_listCurvilinearFiniteElement[m_iPressure];
      // dof values
      pressureDofValuesBD.initialize(DOF_FIELD, *m_curvFePre,1/*numComp*/);
      // for the results
      tractionVel.initialize(DOF_FIELD, *m_curvFeVel, dimension());
      tractionPre.initialize(DOF_FIELD, *m_curvFePre, dimension());

      for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin();
            itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {// loop over the boundary labels that have this ref
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;
        initPerDomainBoundaryCondition(elemPoint, elemIdPoint, currentLabel, numEltPerLabel, &ielSupportDof, eltType, numElement[eltType], FlagMatrixRHS::only_rhs); //it updates label
        for ( felInt iel = 0; iel < numEltPerLabel; iel++) {// loop over the elements of this label
          // for each element:
          m_elementVectorBD[0]->zero();
          // - get the points and their ids and vectorIdSupport
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);
          ielSupportDof=vectorIdSupport[0];// We assume we are not working with duplicated support dof: TODO
          // - get the volume points and their ids
          std::vector<Point*> elemPointVol;   // Points of the current domain element.
          std::vector<felInt> elemIdPointVol; // Id of points of the domain element.
          std::vector<felInt> bd2vol;
          int idLocFace = -1; // Local identity of the face of a domain element
          felInt idElemVol = -1; // ID of the domain element associated to the face with identity idLocFace
          m_mesh[m_currentMesh]->getElementFromBdElem(elemIdPoint,elemIdPointVol,elemPointVol,idLocFace,idElemVol);
          int dummy;
          m_mesh[m_currentMesh]->getTypeElemFromIdElem(idElemVol, volEltType, dummy);
          if (volEltType != previousVolEltType ) {
            defineFiniteElement(volEltType);
            initElementArray();
            previousVolEltType=volEltType;
            m_feVel = m_listCurrentFiniteElement[m_iVelocity];
            velocityDofValuesVol.initialize(DOF_FIELD, *m_feVel,dimension());
            m_feVel->allocateOnDofsDataStructures();
          }
          m_feVel->updateFirstDerivOnDofs(0, elemPointVol);
          m_curvFeVel->updateMeasNormal(0,elemPoint);
          m_curvFePre->updateMeasNormal(0,elemPoint);
          m_feVel->identifyLocBDDof(*m_curvFeVel,bd2vol);

          // Extract the corresponding values of both pressure and velocity from the sequentialSolution.
          velocityDofValuesVol.setValue(sequentialSolution(),*m_feVel,idElemVol,m_iVelocity,m_ao,dof());
          pressureDofValuesBD.setValue(sequentialSolution(),*m_curvFePre,ielSupportDof,m_iPressure,m_ao,dof());

          // Create an element field (dof) to store the traction \vect t = \sigma \vect n
          // compute \vect t = -p \vect n

          //Attention: we have assumed the normal is constant on the element, and we have taken it on the first quadrature node.
          // if we want to generalize this we have to transform tractionPres into an element field defined on quadrature nodes
          // and we have to evalu
          tractionPre.val = -outer_prod(m_curvFePre->normal[0],pressureDofValuesBD.valAsVec()); // t_{comp,dof) = - n_comp * p_dof


          // compute \vect t = \sigma \vect n (only vel part) (depending on the flag about symmetric stres?)
          UBlasVector tmp(dimension()),tmp2(m_feVel->numDof());

          //\nu \nabla u^T \cdot n
          for (int hbdDof(0); hbdDof<m_curvFeVel->numDof(); hbdDof++) {
            tmp2 = prod(trans(velocityDofValuesVol.val),m_curvFePre->normal[0]);
            tmp  = prod(m_feVel->dPhiOnDofs[bd2vol.at(hbdDof)], tmp2);
            for (int jcomp(0); jcomp<dimension(); jcomp++) {
              tractionVel.val(jcomp,hbdDof) = r_instance.viscosity*tmp(jcomp);
            }
          }
          //\nu \nabla u \cdot n
          for (int hbdDof(0); hbdDof<m_curvFeVel->numDof(); hbdDof++) {
            tmp2  = prod(trans(m_feVel->dPhiOnDofs[bd2vol.at(hbdDof)]),m_curvFePre->normal[0]);
            tmp = prod(velocityDofValuesVol.val,tmp2);
            for (int jcomp(0); jcomp<dimension(); jcomp++) {
              tractionVel.val(jcomp,hbdDof) += r_instance.viscosity*tmp(jcomp);
            }
          }

          m_elementVectorBD[0]->source( /* coef*/-1.0, *m_curvFeVel, tractionVel, /*iBlock*/0, dimension());
          m_elementVectorBD[0]->source( /* coef*/-1.0, *m_curvFePre, tractionPre, /*iBlock*/0, dimension());

          // Place m_elementVectorBD[0] in the correct PetscVector
          setValueCustomVectorBD( /* bd element id */ielSupportDof, ADD_VALUES, vec );
          numElement[eltType]++;
        }
      }
      desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_rhs);
    }
    vec.assembly();
  }

  void LinearProblemNS::cvgraphNaturalBC(felInt iel) 
  {
    // Retrieve instance
    auto& r_instance = FelisceParam::instance(instanceIndex());

    BoundaryCondition* BC;
    // Loop over the cvgraph connections
    for ( std::size_t iConn(0); iConn<slave()->numConnections(); ++iConn ) {
      // Extract the labels of the current connection
      std::vector<int> labels = slave()->interfaceLabels(iConn);
      // Verify if the current label is part of the interface of this connection
      if ( std::find( labels.begin(), labels.end(), m_currentLabel ) != labels.end() ) {
        switch ( slave()->numVarToRead(iConn) ) {

        case 1:  // only one var to read: neumann interface BC
          // Verify that we have to apply the stress in its strong form.
          if ( slave()->readVariable(iConn,/*cVar*/0) == "STRESS" && slave()->residualInterpolationType(iConn) == 0 ) {
            // Look through the different neumann conditions to find the correct one.
            for (std::size_t iNeumann=0; iNeumann<m_boundaryConditionList.numNeumannBoundaryCondition(); iNeumann++ ) {
              BC = m_boundaryConditionList.Neumann(iNeumann);//get the the BC
              if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),m_currentLabel) != BC->listLabel().end() ) {
                //compute the force term.
                m_elemFieldNeumann[iNeumann].setValue( m_seqVecs.Get("cvgraphSTRESS"), *m_curvFeVel  , iel, m_iVelocity, m_ao, dof());
              }
            }
          }
          break;
        case 2: //two variables to read: robin interface BC
          // Look through the different robin conditions to find the correct one.
          for (std::size_t iRobin=0; iRobin<m_boundaryConditionList.numRobinBoundaryCondition(); iRobin++ ) {
            BC = m_boundaryConditionList.Robin(iRobin);
            if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),m_currentLabel ) != BC->listLabel().end() ) {
              if ( slave()->residualInterpolationType(iConn) == 0 ) {
                m_elemFieldRobin[iRobin].setValue( m_seqVecs.Get("cvgraphSTRESS"), *m_curvFeVel  , iel, m_iVelocity, m_ao, dof());
              } else {
                m_elemFieldRobin[iRobin].val *=0 ;
              }
              m_robinAux.setValue(m_seqVecs.Get("cvgraphVelocity"),*m_curvFeVel  , iel, m_iVelocity, m_ao, dof());
              m_elemFieldRobin[iRobin].val += r_instance.alphaRobin[iRobin]*m_robinAux.val;
            }
          }
          break;
        default:
          FEL_ERROR("Three variables to read not yet implemented for this linear problem.")
            break;
        }
      }
    }
  }
#endif

/***********************************************************************************/
/***********************************************************************************/

void LinearProblemNS::localizeItfQpInBackMesh(std::size_t itfMsh, std::size_t /* bckMsh*/, felInt itfVar, std::vector<std::vector<felInt>>& mapItfQpInBackMesh) 
{
  const ElementType eltType = m_mesh[itfMsh]->bagElementTypeDomain()[0];
  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  std::vector<Point*> elemPoint(numPointsPerElt, nullptr);
  std::vector<felInt> elemIdPoint(numPointsPerElt, 0);

  const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
  int  typeOfFiniteElement = m_listVariable[itfVar].finiteElementType();
  const RefElement *refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *m_mesh[itfMsh]);
  CurvilinearFiniteElement feStruc(*refEle, *geoEle, m_listVariable[itfVar].degreeOfExactness());

  const felInt nbrQuaPnt     = feStruc.numQuadraturePoint();
  const felInt numEltPerType = m_mesh[itfMsh]->numElements(eltType);

  if ( mapItfQpInBackMesh.size() != static_cast<std::size_t>(numEltPerType) )
    mapItfQpInBackMesh.resize(numEltPerType, std::vector<felInt>(nbrQuaPnt,-1));


  // Parallel localization ( i prefer this way since the local mesh is usually owned by only one proc)
  const felInt numEltByProc    = numEltPerType/MpiInfo::numProc();
  const felInt beginIdEltLocal = MpiInfo::rankProc()*numEltByProc;
  const felInt numEltProc      = ( MpiInfo::rankProc() == MpiInfo::numProc()-1 ) ? numEltPerType-beginIdEltLocal : numEltByProc;

  std::vector<felInt> recvcn(MpiInfo::numProc(), nbrQuaPnt*numEltByProc);
  recvcn.back() = nbrQuaPnt * ( numEltPerType - numEltByProc * ( MpiInfo::numProc() - 1 ) );

  std::vector<felInt> displs(MpiInfo::numProc(), 0);
  std::transform( recvcn.begin(), recvcn.end()-1, displs.begin(), displs.begin()+1, [](auto i, auto j ){ return i + j; } );

  std::vector<Point> listQuadPoint(nbrQuaPnt*numEltProc);

  // Loop on element in the region with the type: eltType
  for (felInt iel=0; iel<numEltProc; ++iel) {

    // Get elemPoint and elemIdPoint.
    m_mesh[itfMsh]->getOneElement(eltType, beginIdEltLocal+iel, elemIdPoint, elemPoint, 0);

    // Update structure finite element
    feStruc.updatePoint(elemPoint);
    feStruc.computeCurrentQuadraturePoint();

    // Add quadrature point to list
    for (int ig=0; ig<nbrQuaPnt; ig++)
      listQuadPoint[iel*nbrQuaPnt+ig] = feStruc.currentQuadPoint[ig];
  }

  std::vector<felInt>      indElt(listQuadPoint.size());
  std::vector<ElementType> typElt(listQuadPoint.size());

  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;

  r_timer.Start("Locator" + std::to_string(identifierProblem()) + "::findListPointsInMesh", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
  m_pLocator->findListPointsInMesh(indElt, typElt, listQuadPoint);
  r_timer.Stop("Locator" + std::to_string(identifierProblem()) + "::findListPointsInMesh", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  // Now i need to build the global indElt vector
  std::vector<felInt> indEltGlb(nbrQuaPnt*numEltPerType);
  MPI_Allgatherv(indElt.data(), recvcn[MpiInfo::rankProc()], MPI_INT, indEltGlb.data(), recvcn.data(), displs.data(), MPI_INT, MpiInfo::petscComm() );

  for (felInt iel=0; iel<numEltPerType; iel++){
    for (felInt ig=0; ig<nbrQuaPnt; ig++)
      mapItfQpInBackMesh[iel][ig] = indEltGlb[iel*nbrQuaPnt+ig];
  }
}

void LinearProblemNS::computeSolidFluidMaps()
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  const std::size_t velMesh = m_listVariable[m_iVelocity].idMesh();
  const std::size_t lagMesh = m_listVariable[m_iLagrange].idMesh();

  const ElementType eltType = m_mesh[lagMesh]->bagElementTypeDomain()[0];

  // Save previous localization map
  std::vector<std::vector<felInt> > mapLagQpInVelMeshOld(m_mapLagQpInVelMesh.begin(), m_mapLagQpInVelMesh.end());

  // Localize structure mesh in fluid mesh
  localizeItfQpInBackMesh(lagMesh, velMesh, m_iLagrange, m_mapLagQpInVelMeshSeq);

  // Resize m_mapLagQpInVelMesh if needed
  m_mapLagQpInVelMesh.resize(m_meshLocal[lagMesh]->numElements(eltType));

  // Initialize parallel map
  const auto rank = MpiInfo::rankProc();
  auto it = m_eltPart[lagMesh].begin();
  std::copy_if(m_mapLagQpInVelMeshSeq.begin(), m_mapLagQpInVelMeshSeq.end(), m_mapLagQpInVelMesh.begin(), [&](auto&) { return rank == *it++; } );

  // Check if localization has changed
  m_hasLocalizationChanged = false;
  if ( m_mapLagQpInVelMesh != mapLagQpInVelMeshOld )
    m_hasLocalizationChanged = true;

  MPI_Allreduce(MPI_IN_PLACE, &m_hasLocalizationChanged, 1, MPI_CXX_BOOL, MPI_LOR, MpiInfo::petscComm());

  if ( r_instance.massConstraint && r_instance.useFicItf ) {
  
    const std::size_t japMesh = 2; //m_listVariable[m_iLagrange].idMesh(); TODO D.C.
  
    const ElementType eltType = m_mesh[japMesh]->bagElementTypeDomain()[0];

    // Localize structure mesh in fluid mesh
    localizeItfQpInBackMesh(japMesh, velMesh, m_iLagrange, m_mapJapQpInVelMeshSeq);

    // Resize m_mapJapQpInVelMesh if needed
    m_mapJapQpInVelMesh.resize(m_meshLocal[japMesh]->numElements(eltType));

    // Initialize parallel map
    auto it = m_eltPart[japMesh].begin();
    std::copy_if(m_mapJapQpInVelMeshSeq.begin(), m_mapJapQpInVelMeshSeq.end(), m_mapJapQpInVelMesh.begin(), [&](auto&) { return rank == *it++; } );
  }
}

void LinearProblemNS::evaluateFluidStructurePattern()
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(identifierProblem()) + "::evaluateFluidStructurePattern", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  if ( !m_isCsrNoInterfaceStored ){
    m_csrNoInterface.set(dof().pattern().rowPointer(),dof().pattern().columnIndices());
    m_isCsrNoInterfaceStored = true;
  }

  if ( m_hasLocalizationChanged ) {

    // Define variable
    std::vector< std::set<felInt> > nodesNeighborhood(m_numDof);
    felInt ielSupportDofVel, ielSupportDofLag;
    felInt node1, node2, idElt;

    const std::size_t velMesh = m_listVariable[m_iVelocity].idMesh();
    const std::size_t lagMesh = m_listVariable[m_iLagrange].idMesh();

    // Get element type
    const ElementType eltType   = m_mesh[lagMesh]->bagElementTypeDomain()[0];
    const ElementType eltTypeFl = m_mesh[velMesh]->bagElementTypeDomain()[0];

    // Set static pattern
    dof().pattern().set(m_csrNoInterface.rowPointer(),m_csrNoInterface.columnIndices());

    // Loop on element in the region with the type: eltType
    const felInt numEltPerType = m_mesh[lagMesh]->numElements(eltType);
    for (felInt iel = 0; iel < numEltPerType; ++iel) {
      m_supportDofUnknown[m_iUnknownLag].getIdElementSupport(eltType, iel, ielSupportDofLag);

      // For every LM component
      for (std::size_t iComp = 0; iComp < m_listVariable[m_iLagrange].numComponent(); ++iComp) {
        const int iConnect = dof().getNumGlobComp(m_iUnknownLag, iComp);

        // For every dof in support element 
        for (int iSup = 0; iSup < m_supportDofUnknown[m_iUnknownLag].getNumSupportDof(ielSupportDofLag); ++iSup) {

          // get the global id of the support dof
          dof().loc2glob(ielSupportDofLag, iSup, m_iLagrange, iComp, node1);

          // Loop over qpoint
          for (std::size_t iq = 0; iq < m_mapLagQpInVelMeshSeq[iel].size(); ++iq){

            for (std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); ++iUnknown) {
              const int iVariable = m_listUnknown.idVariable(iUnknown);
              if ( m_listVariable[iVariable].idMesh() != velMesh )
                continue;

              // get fluid element id
              idElt = m_mapLagQpInVelMeshSeq[iel][iq];
                
              // get id support dof
              m_supportDofUnknown[iUnknown].getIdElementSupport(eltTypeFl, idElt, ielSupportDofVel);

              // For every velocity component
              for (std::size_t jComp = 0; jComp < m_listVariable[iVariable].numComponent(); ++jComp) {
                const int jConnect = dof().getNumGlobComp(iUnknown, jComp);

                // For every dof in support element 
                for (int jSup = 0; jSup < m_supportDofUnknown[iUnknown].getNumSupportDof(ielSupportDofVel); ++jSup) {

                  // get the global id of the support dof
                  dof().loc2glob(ielSupportDofVel, jSup, iVariable, jComp, node2);

                  // if this node is on this process
                  if ( m_listUnknown.mask()(iConnect, jConnect) ) // TODO D.C. not smart put this check here
                    nodesNeighborhood[node1].insert(node2);
                  
                  if ( m_listUnknown.mask()(jConnect, iConnect) ) // TODO D.C. not smart put this check here
                    nodesNeighborhood[node2].insert(node1);
                }
              }
            }
          }
        }
      }
    }

    // Store the pattern in CSR style
    felInt dofSize = 0;
    felInt cptDof = 0;
    felInt pos;

    for(std::size_t iNode=0; iNode<nodesNeighborhood.size(); ++iNode)
      dofSize += nodesNeighborhood[iNode].size();

    CSRMatrixPattern csr(dof().pattern().numRows(), dofSize);
    for(std::size_t iNode = 0; iNode < nodesNeighborhood.size(); ++iNode) {
      if(m_dofPart[iNode] == MpiInfo::rankProc()) {
        csr.rowPointer(cptDof + 1) = csr.rowPointer(cptDof) + nodesNeighborhood[iNode].size();
        pos = 0;
        for (auto it = nodesNeighborhood[iNode].begin(); it != nodesNeighborhood[iNode].end(); ++it) {
          csr.columnIndex(csr.rowPointer(cptDof) + pos) = *it;
          ++pos;
        }
        ++cptDof;
      }
    }

    dof().mergeGlobalPattern(csr.rowPointer(),csr.columnIndices());

    ///////////////////////////////////////////////////////////////
    // Matrix 0 reconstruction with new pattern
    const std::size_t numRows = dof().pattern().numRows();
    std::vector<felInt> nnz(numRows);
    for (size_t idof = 0; idof < numRows; idof++)
      nnz[idof] = dof().pattern().numNonzerosInRow(idof);
    
    if ( MpiInfo::numProc() > 1 ) {    
      std::vector<felInt> jCSR = dof().pattern().columnIndices();
      AOApplicationToPetsc(ao(), dof().pattern().numNonzeros(), jCSR.data());

      m_matrices[0].destroy();
      m_matrices[0].createAIJ(MpiInfo::petscComm(), m_numDofLocal, m_numDofLocal, dof().numDof(), dof().numDof(), 0, FELISCE_PETSC_NULLPTR, 0, FELISCE_PETSC_NULLPTR);
      m_matrices[0].mpiAIJSetPreallocationCSR(dof().pattern().rowPointer().data(), jCSR.data(), NULL);
      m_matrices[0].setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      m_matrices[0].setFromOptions();
    } else {
      m_matrices[0].destroy();
      m_matrices[0].createSeqAIJ(MpiInfo::petscComm(), numRows, numRows, 0, nnz.data());
      m_matrices[0].setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      m_matrices[0].setFromOptions();
    }
  }

  r_timer.Stop("LinearProblem" + std::to_string(identifierProblem()) + "::evaluateFluidStructurePattern", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

void LinearProblemNS::computeLagrangeMultiplierOnInterface(FlagMatrixRHS flagMatrixRHS) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(identifierProblem()) + "::computeLagrangeMultiplierOnInterface", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDofVel, ielSupportDofLag;

  double val;

  const felInt velMesh = m_listVariable[m_iVelocity].idMesh();
  const felInt lagMesh = m_listVariable[m_iLagrange].idMesh();

  // Get element type
  std::vector<ElementType> eltTypeVec(m_listVariable.size());
  for (std::size_t iVar = 0; iVar < m_listVariable.size(); ++iVar)
    eltTypeVec[iVar] = m_mesh[m_listVariable[iVar].idMesh()]->bagElementTypeDomain()[0];

  const ElementType eltType   = m_mesh[lagMesh]->bagElementTypeDomain()[0];
  const ElementType eltTypeFl = m_mesh[velMesh]->bagElementTypeDomain()[0];

  // Resize array.
  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  std::vector<Point*> elemPoint(numPointsPerElt, nullptr);
  std::vector<felInt> elemIdPoint(numPointsPerElt, 0);

  const int numPointsPerEltFl = GeometricMeshRegion::m_numPointsPerElt[eltTypeFl];
  std::vector<Point*> elemPointFl(numPointsPerEltFl, nullptr);
  std::vector<felInt> elemIdPointFl(numPointsPerEltFl, 0);

  // Define all current finite element use in the problem from data
  defineFiniteElement(eltTypeVec);
  defineCurvilinearFiniteElement(eltTypeVec);
  m_feVel     = m_listCurrentFiniteElement[m_iVelocity];
  m_curvFeLag = m_listCurvilinearFiniteElement[m_iLagrange];

  ///////////////////////////////////////////////////////////////
  // Normal constraint
  m_listUnknown.setUnknownsRows({m_iUnknownLag});
  m_listUnknown.setUnknownsCols({m_iUnknownVel});

  m_velBlock = 0;
  m_lagBlock = 0;

  // Element matrix and vector initialisation
  initElementArrayBD(true);

  // Allocate array use for assemble with petsc.
  allocateArrayForAssembleMatrixRHSBD(flagMatrixRHS);

  // Initialize element field
  m_structVelQuad.initialize(QUAD_POINT_FIELD, *m_curvFeLag, m_curvFeLag->numCoor());
  m_structNormalQuad.initialize(QUAD_POINT_FIELD, *m_curvFeLag, m_curvFeLag->numCoor());

  // Loop on element in the region with the type: eltType
  const felInt numEltPerType = m_meshLocal[lagMesh]->numElements(eltType);
  for (felInt iel = 0; iel < numEltPerType; ++iel) {

    m_supportDofUnknownLocal[m_iUnknownLag].getIdElementSupport(eltType, iel, ielSupportDofLag);
    ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[m_iUnknownLag], 1, &ielSupportDofLag, &ielSupportDofLag);

    // Get structure element points
    m_meshLocal[lagMesh]->getOneElement(eltType, iel, elemIdPoint, elemPoint, 0);

    // Update structure finite element
    m_curvFeLag->updateMeasQuadPt(0, elemPoint);

    // **** Matrix ****************
    if ( flagMatrixRHS != FlagMatrixRHS::only_rhs ) {
      // Sum over quadrature points
      for (int ig = 0; ig < m_curvFeLag->numQuadraturePoint(); ++ig) {

        // Quadrature point localization in fluid mesh
        felInt idElt = m_mapLagQpInVelMesh[iel][ig];

        // Get id support dof
        m_supportDofUnknown[m_iUnknownVel].getIdElementSupport(eltTypeFl, idElt, ielSupportDofVel);

        // Get fluid element points
        m_mesh[velMesh]->getOneElement(eltTypeFl, idElt, elemIdPointFl, elemPointFl, 0);

        // Map structure quadrature point in fluid reference element
        Point qRefPoint;
        m_feVel->mapPointInReferenceElement(0, elemPointFl, m_curvFeLag->currentQuadPoint[ig], qRefPoint);

        // Clean matrices
        m_elementMatBD[0]->zero();
        m_elementMatTBD[0]->zero();

        // Sum over components
        for (std::size_t ic = 0; ic <m_listVariable[m_iLagrange].numComponent(); ++ic){
          UBlasMatrixRange matrix  = m_elementMatBD[0]->matBlock(m_lagBlock+ic,m_velBlock+ic);
          UBlasMatrixRange matrixT = m_elementMatTBD[0]->matBlock(m_velBlock+ic,m_lagBlock+ic);

          // Sum over structure element nodes
          for(int idof = 0; idof < m_curvFeLag->numDof(); ++idof) {

            // Sum over fluid element nodes
            for(int jdof = 0; jdof < m_feVel->numDof(); ++jdof) {

              // Compute integral
              val = m_curvFeLag->weightMeas(ig) * m_curvFeLag->phi[ig](idof) * m_feVel->refEle().basisFunction().phi(jdof,qRefPoint);

              matrix(idof,jdof)  -= val;
              matrixT(jdof,idof) -= val;
            }
          }
        }

        // Add values of elemMat in the global matrix: m_matrices[0].
        setValueCustomMatrixBD(ielSupportDofLag, ielSupportDofVel, m_matrices[0], true);
      }
    }
    // ****************************

    // **** Vector ****************
    if ( flagMatrixRHS != FlagMatrixRHS::only_matrix ) {
      // Update structure velocity
      updateStructureVel(*m_curvFeLag, elemIdPoint);

      // Clean vector
      m_elementVectorBD[0]->zero();

      // Compute
      m_elementVectorBD[0]->source(-1., *m_curvFeLag, m_structVelQuad, m_lagBlock, m_listVariable[m_iLagrange].numComponent());

      // Add values of elemVec in the global vector: m_vectors[0].
      setValueCustomVectorBD(ielSupportDofLag, ADD_VALUES, m_vectors[0]);
    }
    // ****************************
  }

  // Deallocate array use for assemble with petsc.
  desallocateArrayForAssembleMatrixRHS(flagMatrixRHS);

  m_listUnknown.setUnknownsRows({m_iUnknownVel,m_iUnknownPre});
  m_listUnknown.setUnknownsCols({m_iUnknownVel,m_iUnknownPre});

  m_velBlock = m_listUnknown.getBlockPosition(m_iUnknownVel,0);
  m_lagBlock = m_listUnknown.getBlockPosition(m_iUnknownLag,0);

  if( flagMatrixRHS == FlagMatrixRHS::only_rhs ) {
    // Real assembling of the right hand side (RHS).
    // for (std::size_t i = 0; i < m_vectors.size(); i++)
      m_vectors[0].assembly();
  }

  r_timer.Stop("LinearProblem" + std::to_string(identifierProblem()) + "::computeLagrangeMultiplierOnInterface", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

void LinearProblemNS::computeBHStabLagLag(const double coeff, const int iq) 
{
  // \int_{\Sigma} \lambda \cdot \cxi
  UBlasMatrix tmpMat = coeff * m_curvFeLag->weightMeas(iq) * outer_prod(m_curvFeLag->phi[iq], m_curvFeLag->phi[iq]);
  for (std::size_t ic = 0; ic < m_listVariable[m_iLagrange].numComponent(); ++ic){
    UBlasMatrixRange matrix = m_elementMatBD[0]->matBlock(m_lagBlock+ic,m_lagBlock+ic);
    matrix -= tmpMat;
  }
}

void LinearProblemNS::computeBHStabLagJap(const double coeff, const int iq) 
{
  double tmp;

  // \int_{\Sigma} \lambda_j n \cdot \cxi
  for (std::size_t ic = 0; ic < m_listVariable[m_iLagrange].numComponent(); ++ic){
    UBlasMatrixRange matrix = m_elementMatBD[0]->matBlock(m_lagBlock+ic,m_japBlock);

    for (int idof = 0; idof < m_curvFeLag->numDof(); ++idof){
      tmp = coeff * m_curvFeLag->weightMeas(iq) * m_curvFeLag->phi[iq](idof) * m_structNormalQuad.val(ic,iq);
      matrix(idof,0) -= tmp;
    }
  }
}

void LinearProblemNS::computeBHStabJapLag(const double coeff, const int iq) 
{
  double tmp;

  // \int_{\Sigma} \lambda \cdot \cxi_j n
  for (std::size_t ic = 0; ic < m_listVariable[m_iLagrange].numComponent(); ++ic){
    UBlasMatrixRange matrix = m_elementMatBD[0]->matBlock(m_japBlock,m_lagBlock+ic);

    for (int idof = 0; idof < m_curvFeLag->numDof(); ++idof){
      tmp = coeff * m_curvFeLag->weightMeas(iq) * m_curvFeLag->phi[iq](idof) * m_structNormalQuad.val(ic,iq);
      matrix(0,idof) -= tmp;
    }
  }
}

void LinearProblemNS::computeBHStabJapJap(const double coeff, const int iq) 
{
  // \int_{\Sigma} \lambda_j n \cdot \cxi_j n
  UBlasMatrixRange matrix = m_elementMatBD[0]->matBlock(m_japBlock,m_japBlock);
  matrix(0,0) -= coeff * m_curvFeLag->weightMeas(iq);
}

void LinearProblemNS::assembleBarbosaHughesStab() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(identifierProblem()) + "::assembleBarbosaHughesStab", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  std::vector<felInt> ielSupportDof;
  double coeff;
  const felInt velMesh = m_listVariable[m_iVelocity].idMesh();
  const felInt lagMesh = m_listVariable[m_iLagrange].idMesh();

  // Get element type
  std::vector<ElementType> eltTypeVec(m_listVariable.size());
  for (std::size_t iVar = 0; iVar < m_listVariable.size(); ++iVar)
    eltTypeVec[iVar] = m_mesh[m_listVariable[iVar].idMesh()]->bagElementTypeDomain()[0];

  const ElementType eltType   = m_mesh[lagMesh]->bagElementTypeDomain()[0];
  const ElementType eltTypeFl = m_mesh[velMesh]->bagElementTypeDomain()[0];
  
  // Resize array.
  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  std::vector<Point*> elemPoint(numPointsPerElt, nullptr);
  std::vector<felInt> elemIdPoint(numPointsPerElt, 0);

  const int numPointsPerEltFl = GeometricMeshRegion::m_numPointsPerElt[eltTypeFl];
  std::vector<Point*> elemPointFl(numPointsPerEltFl, nullptr);
  std::vector<felInt> elemIdPointFl(numPointsPerEltFl, 0);

  // Define all current finite element use in the problem from data
  defineFiniteElement(eltTypeVec);
  defineCurvilinearFiniteElement(eltTypeVec);
  m_feVel     = m_listCurrentFiniteElement[m_iVelocity];
  m_curvFeLag = m_listCurvilinearFiniteElement[m_iLagrange];

  const double inv_coeff = 1. / ( r_instance.stabCoeffLag * r_instance.viscosity );
  // FEL_WARNING("Using the modified version for the stabilization coeff");
  // const double inv_coeff = 1. / ( r_instance.stabCoeffLag * ( r_instance.viscosity > 1 ? r_instance.viscosity : 1 ) );

  if ( r_instance.massConstraint ){
    ielSupportDof.resize(2);

    m_listUnknown.setUnknownsRows({m_iUnknownLag, m_iUnknownJap});
    m_listUnknown.setUnknownsCols({m_iUnknownLag, m_iUnknownJap});    
    m_lagBlock = 0;
    m_japBlock = m_listVariable[m_iLagrange].numComponent();
  }
  else {
    ielSupportDof.resize(1);

    m_listUnknown.setUnknownsRows({m_iUnknownLag});
    m_listUnknown.setUnknownsCols({m_iUnknownLag}); 
    m_lagBlock = 0;
  }

  // Initialize element field
  m_structNormalQuad.initialize(QUAD_POINT_FIELD, *m_curvFeLag, m_curvFeLag->numCoor());

  // Element matrix and vector initialisation
  initElementArrayBD();

  // Allocate array use for assemble with petsc.
  allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_matrix);

  // Loop on element in the region with the type: eltType
  const felInt numEltPerType = m_meshLocal[lagMesh]->numElements(eltType);
  for (felInt iel=0; iel<numEltPerType; iel++) {

    m_supportDofUnknownLocal[m_iUnknownLag].getIdElementSupport(eltType, iel, ielSupportDof[0]);
    ISLocalToGlobalMappingApply(*m_mappingElemSupportPerUnknown[m_iUnknownLag], 1, &ielSupportDof[0], &ielSupportDof[0]);

    // Get structure element points
    m_meshLocal[lagMesh]->getOneElement(eltType, iel, elemIdPoint, elemPoint, 0);

    // Update structure finite element
    m_curvFeLag->updateMeas(0, elemPoint);

    // Update normals
    updateNormal(*m_curvFeLag, lagMesh, elemIdPoint);

    // Sum over quadrature points
    for (int ig=0; ig<m_curvFeLag->numQuadraturePoint(); ig++) {

      // Quadrature point localization in fluid mesh
      felInt idElt = m_mapLagQpInVelMesh[iel][ig];

      // Get fluid element points
      m_mesh[velMesh]->getOneElement(eltTypeFl, idElt, elemIdPointFl, elemPointFl, 0);

      // Update fluid fe
      m_feVel->updateMeas(0, elemPointFl);

      // Compute coefficient
      coeff = m_feVel->diameter() * inv_coeff;

      // Clear elementary matrix.
      m_elementMatBD[0]->zero();
      
      // Assemble local matrix
      computeBHStabLagLag(coeff, ig);

      if ( r_instance.massConstraint ){

        // Get id support dof
        m_supportDofUnknown[m_iUnknownJap].getIdElementSupport(eltTypeFl, idElt, ielSupportDof[1]);

        // Assemble local matrix
        computeBHStabLagJap(coeff, ig);

        if ( r_instance.BHstabSymm ) { 
          computeBHStabJapLag(coeff, ig);
          computeBHStabJapJap(coeff, ig);
        }
      }

      // Add values of elemMat in the global matrix: m_matrices[0].
      setValueCustomMatrixBD(ielSupportDof, ielSupportDof, m_matrices[0]);
    }
  }

  // Desallocate array use for assemble with petsc.
  desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);

  // Reset
  m_listUnknown.setUnknownsRows({m_iUnknownVel,m_iUnknownPre}); // TODO write a method to reset to default
  m_listUnknown.setUnknownsCols({m_iUnknownVel,m_iUnknownPre});

  m_lagBlock = m_listUnknown.getBlockPosition(m_iUnknownLag,0);
  if ( r_instance.massConstraint )
    m_japBlock = m_listUnknown.getBlockPosition(m_iUnknownJap,0);

  r_timer.Stop("LinearProblem" + std::to_string(identifierProblem()) + "::assembleBarbosaHughesStab", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

void LinearProblemNS::assembleBrezziPitkarantaStab() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  ElementType eltType;
  int numPointPerElt;
  felInt ielSupportDofLag;

  std::vector<Point*> elemPoint;
  std::vector<felInt> elemIdPoint;
  std::vector<felInt> vectorIdSupport;

  // Lagrange multiplier mesh
  const felInt lagMesh = m_listVariable[m_iLagrange].idMesh();

  const double inv_coeff = 1. / ( r_instance.stabCoeffLag * r_instance.viscosity );

  m_listUnknown.setUnknownsRows({m_iUnknownLag});
  m_listUnknown.setUnknownsCols({m_iUnknownLag});

  m_lagBlock = 0;

  // Assembly loop.
  // First loop on geometric type.
  const std::vector<ElementType>& bagElementTypeDomain = m_meshLocal[lagMesh]->bagElementTypeDomain();
  for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
    eltType = bagElementTypeDomain[i];

    // Resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    // define all current finite element use in the problem from data
    defineCurvilinearFiniteElement(eltType);
    m_curvFeLag = m_listCurvilinearFiniteElement[m_iLagrange];

    // Element matrix and std::vector initialisation
    initElementArrayBD();

    // Allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_matrix);

    // Loop on element in the region with the type: eltType
    const felInt numEltPerType = m_meshLocal[lagMesh]->numElements(eltType);
    for (felInt iel = 0; iel < numEltPerType; ++iel) {

      // Get element info
      setElemPoint(eltType, iel, elemPoint, elemIdPoint, vectorIdSupport);

      // Update structure finite element
      m_curvFeLag->updateMeasNormalContra(0, elemPoint);

      // Set coefficient
      const double coeff = std::pow(m_curvFeLag->diameter(),3)*inv_coeff;

      // Loop over all the support elements
      for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
        
        // get the id of the support
        ielSupportDofLag = vectorIdSupport[it];

        // clear elementary matrix.
        m_elementMatBD[0]->zero();

        // Compute Brezzi-Pitkaranta stabilization
        m_elementMatBD[0]->grad_phi_i_grad_phi_j(-coeff, *m_curvFeLag, m_lagBlock, m_lagBlock, m_listVariable[m_iLagrange].numComponent());

        // Add values of elemMat in the global matrix: m_matrices[0].
        setValueCustomMatrixBD(ielSupportDofLag, ielSupportDofLag, m_matrices[0]);
      }
    }
    // Deallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
  }

  // Reset
  m_listUnknown.setUnknownsRows({m_iUnknownVel,m_iUnknownPre}); // TODO write a method to reset to default
  m_listUnknown.setUnknownsCols({m_iUnknownVel,m_iUnknownPre});

  m_lagBlock = m_listUnknown.getBlockPosition(m_iUnknownLag,0);

  // if ( r_instance.massConstraint ) {
  //   felInt numGlbDof;
  //   const double value = -1.e-7;
  //   m_dof.loc2glob(0, 0, m_iJapanCon, 0, numGlbDof);
  //   AOApplicationToPetsc(m_ao, 1, &numGlbDof);
  //   m_matrices[0].setValues(1, &numGlbDof, 1, &numGlbDof, &value, ADD_VALUES);

  //   // m_elementMat[0]->phi_i_phi_j(-1.e-7, *m_feVel, m_lagBlock, m_lagBlock, m_listVariable[m_iLagrange].numComponent());
  // }
}

void LinearProblemNS::assembleJapanConstraintBoundary() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( r_instance.japanLabels.size() == 0 )
    return;

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(identifierProblem()) + "::assembleJapanConstraintBoundary", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  m_listUnknown.setUnknownsRows({m_iUnknownVel});
  m_listUnknown.setUnknownsCols({m_iUnknownJap});

  m_velBlock = 0;
  m_japBlock = 0;

  ElementType eltType;           //geometric element type in the mesh.
  int numPointPerElt = 0;        //number of points per geometric element.
  int currentLabel = 0;          //number of label domain.
  felInt numEltPerLabel = 0;     //number of element for one label and one eltType.

  // use to define a "global" numbering of element in the mesh.
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int ityp=0; ityp< GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    eltType = (ElementType)ityp;
    numElement[eltType] = 0;
  }

  // contains points of the current element.
  std::vector<Point*> elemPoint;

  // contains ids of point of the current element.
  std::vector<felInt> elemIdPoint;

  // contains ids of support elements of a mesh element
  std::vector<felInt> vectorIdSupport;

  // use to get element number in support dof mesh ordering.
  felInt ielSupportDofCurv;
  
  //Assembly loop curvilinear.
  const std::vector<ElementType>& bagElementTypeDomainBoundary = m_meshLocal[m_currentMesh]->bagElementTypeDomainBoundary();
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    eltType =  bagElementTypeDomainBoundary[i];

    //resize array.
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    //define all current finite element use in the problem from data
    //file configuration and allocate elemMat and elemVec (question: virtual ?).
    defineCurvilinearFiniteElement(eltType);

    // Element matrix and vector initialisation
    initElementArrayBD(true);

    //allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_matrix);

    // get velocity curvilinear fe
    m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];

    // Second loop on region of the mesh.
    for(auto itRef = m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].begin(); itRef != m_meshLocal[m_currentMesh]->intRefToBegEndMaps[eltType].end(); itRef++) {
      currentLabel = itRef->first;
      numEltPerLabel = itRef->second.second;

      auto bndRefIt = find(r_instance.japanLabels.begin(), r_instance.japanLabels.end(), currentLabel);
      if(bndRefIt == r_instance.japanLabels.end()){
        numElement[eltType] += numEltPerLabel;
        continue;
      }

      // Third loop on element in the region with the type: eltType. ("real" loop on elements).
      for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
        // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
        setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

        // update velcity fe
        m_curvFeVel->updateMeasNormal(0, elemPoint);

        // loop over all the support elements
        for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
          // get the id of the support
          ielSupportDofCurv = vectorIdSupport[it];

          // clear elementary matrix.
          m_elementMatBD[0]->zero();
          m_elementMatTBD[0]->zero();

          m_elementMatBD[0]->const_phi_i_dot_n(-1., *m_curvFeVel, m_velBlock, m_japBlock);
          m_elementMatTBD[0]->const_phi_j_dot_n(-1., *m_curvFeVel, m_japBlock, m_velBlock);

          setValueCustomMatrixBD(ielSupportDofCurv, ielSupportDofCurv, m_matrices[0], true);
        }

        numElement[eltType]++;
      }
    }

    //allocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
  }

  m_listUnknown.setUnknownsRows({m_iUnknownVel,m_iUnknownPre});
  m_listUnknown.setUnknownsCols({m_iUnknownVel,m_iUnknownPre});

  m_velBlock = m_listUnknown.getBlockPosition(m_iUnknownVel,0);
  m_japBlock = m_listUnknown.getBlockPosition(m_iUnknownJap,0);

  r_timer.Stop("LinearProblem" + std::to_string(identifierProblem()) + "::assembleJapanConstraintBoundary", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

void LinearProblemNS::assembleJapanConstraintInterface() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(identifierProblem()) + "::assembleJapanConstraintInterface", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  felInt ielSupportDofVel;

  double val;

  const felInt lagMesh = m_listVariable[m_iLagrange].idMesh();
  const felInt velMesh = m_listVariable[m_iVelocity].idMesh();

  std::vector<felInt> listMeshes;
  std::vector< std::vector<std::vector<felInt> >* > listMapQuadPt;
  if ( r_instance.useFicItf ){

    if ( r_instance.M1G.enable || r_instance.usePhyAndFict ){
      listMeshes = {lagMesh, 2}; // TODO D.C.
      listMapQuadPt = {&m_mapLagQpInVelMesh, &m_mapJapQpInVelMesh};
    }
    else {
      listMeshes = {2}; // TODO D.C.
      listMapQuadPt = {&m_mapJapQpInVelMesh};
    }
  } else {
    listMeshes = {lagMesh};
    listMapQuadPt = {&m_mapLagQpInVelMesh};
  }

  m_listUnknown.setUnknownsRows({m_iUnknownVel});
  m_listUnknown.setUnknownsCols({m_iUnknownJap});

  m_velBlock = 0;
  m_japBlock = 0;

  for (auto iMsh = 0u; iMsh < listMeshes.size(); ++iMsh) {
    const felInt id_mesh = listMeshes[iMsh];
    const std::vector<std::vector<felInt> >& mapQuadPt = *listMapQuadPt[iMsh];
    
    // Get element type
    std::vector<ElementType> eltTypeVec(m_listVariable.size());
    for (std::size_t iVar = 0; iVar < m_listVariable.size(); ++iVar)
      eltTypeVec[iVar] = m_mesh[m_listVariable[iVar].idMesh()]->bagElementTypeDomain()[0];
    
    // Get element type
    const ElementType eltType   = m_mesh[id_mesh]->bagElementTypeDomain()[0];
    const ElementType eltTypeFl = m_mesh[velMesh]->bagElementTypeDomain()[0];

    // Resize array.
    const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    std::vector<Point*> elemPoint(numPointsPerElt, nullptr);
    std::vector<felInt> elemIdPoint(numPointsPerElt, 0);

    const int numPointsPerEltFl = GeometricMeshRegion::m_numPointsPerElt[eltTypeFl];
    std::vector<Point*> elemPointFl(numPointsPerEltFl, nullptr);
    std::vector<felInt> elemIdPointFl(numPointsPerEltFl, 0);

    // Define all current finite element use in the problem from data
    defineFiniteElement(eltTypeVec);
    defineCurvilinearFiniteElement(eltTypeVec);
    m_feVel     = m_listCurrentFiniteElement[m_iVelocity];
    m_curvFeLag = m_listCurvilinearFiniteElement[m_iLagrange];

    // Element matrix and vector initialisation
    initElementArrayBD(true);

    // Allocate array use for assemble with petsc.
    allocateArrayForAssembleMatrixRHSBD(FlagMatrixRHS::only_matrix);

    // Initialize element field
    m_structNormalQuad.initialize(QUAD_POINT_FIELD, *m_curvFeLag, m_curvFeLag->numCoor());

    // Loop on element in the region with the type: eltType
    const felInt numEltPerType = m_meshLocal[id_mesh]->numElements(eltType);
    for (felInt iel = 0; iel < numEltPerType; ++iel) {

      // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
      m_meshLocal[id_mesh]->getOneElement(eltType, iel, elemIdPoint, elemPoint, 0);

      // Update structure finite element
      m_curvFeLag->updateMeasQuadPt(0, elemPoint);

      // Update normals
      updateNormal(*m_curvFeLag, id_mesh, elemIdPoint);

      // Sum over quadrature points
      for (int ig = 0; ig < m_curvFeLag->numQuadraturePoint(); ++ig) {

        // Quadrature point localization in fluid mesh
        felInt idElt = mapQuadPt[iel][ig];

        // Get id support dof
        m_supportDofUnknown[m_iUnknownVel].getIdElementSupport(eltTypeFl, idElt, ielSupportDofVel);

        // Get fluid element points
        m_mesh[velMesh]->getOneElement(eltTypeFl, idElt, elemIdPointFl, elemPointFl, 0);

        // Map structure quadrature point in fluid reference element
        Point qRefPoint;
        m_feVel->mapPointInReferenceElement(0, elemPointFl, m_curvFeLag->currentQuadPoint[ig], qRefPoint);

        // Clear elementary matrices
        m_elementMatBD[0]->zero();
        m_elementMatTBD[0]->zero();

        // Sum over components
        for (std::size_t ic = 0; ic < m_listVariable[m_iVelocity].numComponent(); ++ic){

          // Get matrix block
          UBlasMatrixRange matrix  = m_elementMatBD[0]->matBlock(m_velBlock+ic,m_japBlock);
          UBlasMatrixRange matrixT = m_elementMatTBD[0]->matBlock(m_japBlock,m_velBlock+ic);

          // Sum over element nodes
          for(int idof = 0; idof < m_feVel->numDof(); ++idof) {

            // Compute integral
            val = m_curvFeLag->weightMeas(ig) * m_structNormalQuad.val(ic, ig) * m_feVel->refEle().basisFunction().phi(idof,qRefPoint);

            // Fill matrix block
            matrix(idof,0)  -= val;
            matrixT(0,idof) -= val;
          }
        }

        // add values of elemMat in the global matrix: m_matrices[0].
        setValueCustomMatrixBD(ielSupportDofVel, ielSupportDofVel, m_matrices[0], true);
      }
    }

    // Deallocate array use for assemble with petsc.
    desallocateArrayForAssembleMatrixRHS(FlagMatrixRHS::only_matrix);
  }

  m_listUnknown.setUnknownsRows({m_iUnknownVel,m_iUnknownPre});
  m_listUnknown.setUnknownsCols({m_iUnknownVel,m_iUnknownPre});

  m_velBlock = m_listUnknown.getBlockPosition(m_iUnknownVel,0);
  m_japBlock = m_listUnknown.getBlockPosition(m_iUnknownJap,0);

  r_timer.Stop("LinearProblem" + std::to_string(identifierProblem()) + "::assembleJapanConstraintInterface", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);    
}

void LinearProblemNS::updateStructureVel(CurBaseFiniteElement& fe, std::vector<felInt>& elemIdPoint) 
{
  // update on the structure dof the local structure velocity from the master at the struct element strucId
  // FEL_CHECK(m_structVel != 0, "Error: the pointer of the interface velocity has not been initialized yet.");
  // velocity comming from the FSI master ( The order is not the same! ) 
  // the local ordering is done by component while the global is by point
  for(int iq = 0; iq < fe.numQuadraturePoint(); ++iq) {
    for(int ic = 0; ic < fe.numCoor(); ++ic) {
      m_structVelQuad.val(ic, iq) = 0;
      for(int il = 0; il < fe.numDof(); ++il)
        m_structVelQuad.val(ic, iq) += (*m_structVel)[fe.numCoor()*elemIdPoint[il]+ic] * fe.phi[iq](il); 
    }
  }
}

void LinearProblemNS::computeItfDisp(std::vector<double>& itfDisp) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get number coor
  const int nbrCrd = dimension();

  // Get id mesh
  const int idMshLag = m_listVariable[m_iLagrange].idMesh();

  // Get number of real interface points
  const int nbrPntReal = m_mesh[idMshLag]->numPoints();

  // Compute displacement real structure
  m_realDisp.assign( itfDisp.begin(), itfDisp.begin()+nbrCrd*nbrPntReal );

  if ( r_instance.useFicItf ) {

    // Get number of fictitious interface points
    const felInt nbrPntFict = m_mesh[2]->listPoints().size(); // TODO D.C.

    // Resize fictitious displacement vector
    m_fictDisp.assign(nbrPntFict*nbrCrd,0.);
  
    // User defined displacement for fictitious interface
    userComputeItfDisp();
  }
}

void LinearProblemNS::moveItfMeshes()
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  const int idMshLag = listVariable()[m_iLagrange].idMesh();

  mesh(idMshLag)->moveMesh(m_realDisp,1.0);
  meshLocal(idMshLag)->moveMesh(m_realDisp,1.0);

  // Compute mesh normals
  mesh(idMshLag)->computeNormalTangent();
  auto& listNormals = mesh(idMshLag)->listNormals();
  if ( m_swapNormals )
    std::for_each(listNormals.begin(), listNormals.end(), [](auto& p) { p *= -1.; } );

  if ( r_instance.useFicItf ) {
    
    // Move the fictitous mesh
    userMoveFictMesh();

    // Compute mesh normals
    mesh(2)->computeNormalTangent();
    auto& listNormals = mesh(2)->listNormals();
    if ( m_swapNormals )
      std::for_each(listNormals.begin(), listNormals.end(), [](auto& p) { p *= -1.; } );
  }
}

void LinearProblemNS::updateNormal(CurvilinearFiniteElement& fe, int idMsh, std::vector<felInt>& elemIdPoint)  
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( r_instance.useAverageNormal ) {
    double norm;
    for(int iq=0; iq<fe.numQuadraturePoint(); ++iq) {
      for(int ic=0; ic<fe.numCoor(); ++ic) {
        m_structNormalQuad.val(ic, iq) = 0;
        for(int idof=0; idof<fe.numDof(); ++idof)
          m_structNormalQuad.val(ic, iq) += fe.phi[iq](idof) * m_mesh[idMsh]->listNormal(elemIdPoint[idof])[ic];
      }

      norm = 0;
      for(int ic=0; ic<fe.numCoor(); ++ic)
        norm += m_structNormalQuad.val(ic, iq) * m_structNormalQuad.val(ic, iq);

      norm = 1./std::sqrt(norm); 
      for(int ic=0; ic<fe.numCoor(); ++ic)
        m_structNormalQuad.val(ic, iq) *= norm;
    }
  } else {
    const double sign = m_swapNormals == true ? -1. : 1;

    fe.computeNormal();

    for(int ic=0; ic<fe.numCoor(); ++ic)
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq)
        m_structNormalQuad.val(ic, iq) = sign * fe.normal[iq](ic);
  }
}

void LinearProblemNS::setInterfaceExchangedData(std::vector<double>& itfVel, std::vector<double>& itfForc) 
{ 
  m_structVel = &itfVel; 
  m_intfForc = &itfForc;
}

void LinearProblemNS::computeInterfaceStress() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Get the timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("LinearProblem" + std::to_string(identifierProblem()) + "::computeInterfaceStress", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);

  FEL_CHECK(m_intfForc != nullptr, "Error: the pointer of the interface force has not been initialized yet.");

  std::fill(m_intfForc->begin(), m_intfForc->end(), 0);

  // const felInt velMesh = m_listVariable[m_iVelocity].idMesh();
  const felInt lagMesh = m_listVariable[m_iLagrange].idMesh();

  // Get element type
  std::vector<ElementType> eltTypeVec(m_listVariable.size());
  for (std::size_t iVar = 0; iVar < m_listVariable.size(); ++iVar)
    eltTypeVec[iVar] = m_mesh[m_listVariable[iVar].idMesh()]->bagElementTypeDomain()[0];

  const ElementType eltType   = m_mesh[lagMesh]->bagElementTypeDomain()[0];
  // const ElementType eltTypeFl = m_mesh[velMesh]->bagElementTypeDomain()[0];

  // Resize array
  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  std::vector<Point*> elemPoint(numPointsPerElt, nullptr);
  std::vector<felInt> elemIdPoint(numPointsPerElt, 0);

  // Define structure finite element
  const GeoElement *geoEleStruc = m_mesh[lagMesh]->eltEnumToFelNameGeoEle[eltType].second;
  const RefElement *refEleStruc = geoEleStruc->defineFiniteEle(eltType, 0, *m_mesh[lagMesh]);
  m_curvFeStruc = new CurvilinearFiniteElement(*refEleStruc, *geoEleStruc, DegreeOfExactness_2); // TODO D.C.: Provide the value with the fluid or by data file 

  // Define all current finite element use in the problem from data
  defineCurvilinearFiniteElement(eltTypeVec);
  m_curvFeLag = m_listCurvilinearFiniteElement[m_iLagrange];

  // Initialize element fields
  m_structNormalQuad.initialize( QUAD_POINT_FIELD, *m_curvFeStruc, m_curvFeStruc->numCoor());
  m_seqLagDofPts.initialize(DOF_FIELD, *m_curvFeLag, m_curvFeLag->numCoor());

  ElementVector elemVec({m_curvFeStruc}, {static_cast<std::size_t>(m_curvFeStruc->numCoor())});

  // Loop on element in the region with the type: eltType
  const felInt numEltPerType = m_meshLocal[lagMesh]->numElements(eltType);
  for (felInt iel = 0; iel < numEltPerType; ++iel) {

    // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
    m_meshLocal[lagMesh]->getOneElement(eltType, iel, elemIdPoint, elemPoint, 0);

    // clear elementary vector.
    elemVec.zero();

    // compute stress on interface element
    computeElementaryInterfaceStress(elemPoint, elemIdPoint, iel, elemVec);

    // assembly m_intForc
    for (int ic = 0; ic < m_curvFeStruc->numCoor(); ++ic) {
      for (int ip = 0; ip < m_curvFeStruc->numPoint(); ++ip) {
        int gdof = elemIdPoint[ip]; // <- // TODO D.C.: don't like this, works only for P1
        (*m_intfForc)[m_curvFeStruc->numCoor()*gdof+ic] -= elemVec[ip+ic*m_curvFeStruc->numDof()];
      }
    }
  }

  MPI_Allreduce(MPI_IN_PLACE, m_intfForc->data(), m_intfForc->size(), MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());

  // if ( MpiInfo::rankProc() == 0 ){
  //   std::cout << "Stress: ";
  //   for (auto it = m_intfForc->begin(); it != m_intfForc->end(); ++it)
  //     std::cout << *it << " ";
  //   std::cout << std::endl;
  // }

  delete m_curvFeStruc;

  r_timer.Stop("LinearProblem" + std::to_string(identifierProblem()) + "::computeInterfaceStress", m_fstransient ? (m_fstransient->total_number_iterations > 0 ? m_fstransient->total_number_iterations : m_fstransient->iteration ) : 0);
}

void LinearProblemNS::computeElementaryInterfaceStress(const std::vector<Point*>& elemPoint, std::vector<felInt>& /*elemIdPoint*/, felInt iel, ElementVector& elemVec) 
{
  double lag_ic;
  felInt ielGlb;
  const felInt lagMesh = m_listVariable[m_iLagrange].idMesh();

  // update structure finite element
  m_curvFeStruc->updateMeasQuadPt(0, elemPoint);


  //--- Constraint on velocity
  ISLocalToGlobalMappingApply(mappingElem(lagMesh), 1, &iel, &ielGlb);
  m_seqLagDofPts.setValue(sequentialSolution(), *m_curvFeLag, ielGlb, m_iLagrange, m_ao, dof());

  // for every component of the struct test function
  for (int ic = 0; ic < m_curvFeStruc->numCoor(); ++ic) {
    UBlasVectorRange vec = elemVec.vecBlock(ic);
    
    // for every quadrature point
    for (int ig = 0; ig < m_curvFeStruc->numQuadraturePoint(); ig++) {

      const Point& qRefPoint = m_curvFeStruc->quadratureRule().quadraturePoint(ig);

      // evaluate \lambda_{ic}(x_{iq})
      lag_ic = 0;
      for(int idof = 0; idof < m_curvFeLag->numDof(); idof++)
        lag_ic += m_curvFeLag->refEle().basisFunction().phi(idof,qRefPoint) * m_seqLagDofPts.val(ic,idof);

      // for every node
      for(int idof = 0; idof < m_curvFeStruc->numDof(); idof++)
        vec(idof) += m_curvFeStruc->weightMeas(ig) * lag_ic * m_curvFeStruc->phi[ig](idof);
    }
  }
}

void LinearProblemNS::computeRealPressure()
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( !r_instance.massConstraint )
    return;

  if ( !r_instance.compPressure )
    return;

  gatherSolution();
  if ( MpiInfo::rankProc() == 0 ) {

    // Set limit of the region
    std::set<GeometricMeshRegion::seedElement> set_elt;

    // Get id of the mesh
    const std::size_t vel_mesh = m_listVariable[m_iVelocity].idMesh();

    // Get element type
    const ElementType elt_type = m_mesh[vel_mesh]->bagElementTypeDomain()[0];

    // Initialize list of elements in region (Boundary of the region)
    for ( const auto& map : {&m_mapLagQpInVelMeshSeq, &m_mapJapQpInVelMeshSeq} )
      for ( const auto& elt : *map ) 
        for ( const auto& id : elt )
          set_elt.insert( {elt_type, id} );

    // Intialize list of intersected elements
    std::set<GeometricMeshRegion::seedElement> set_itf_elt = set_elt;

    // Set germ element
    GeometricMeshRegion::seedElement germ_elt{elt_type, r_instance.compPresGerm};

    // Detect the region
    m_mesh[vel_mesh]->getElementsInRegion(germ_elt, set_elt);

    // Remove set_itf_elt from set_elt
    for (const auto& elt : set_itf_elt)
      set_elt.erase(elt);

    // Retrieve list of dof to modify
    std::vector<felInt> sup_elt_vec;
    std::vector<felInt> elt_idx_node;
    std::set<felInt>    set_idx_node;
    for (const auto& elt : set_elt) {
      m_supportDofUnknown[m_iUnknownPre].getIdElementSupport(elt.first, elt.second, sup_elt_vec);
      for (const auto& sup_elt : sup_elt_vec) {
        m_supportDofUnknown[m_iUnknownPre].getIndexesElementSupport(sup_elt, elt_idx_node);
        for (const auto& idx : elt_idx_node)
          set_idx_node.insert(idx);
      }
    }

    // Retrieve dof id from nodes
    std::vector<felInt> list_idx_node{set_idx_node.begin(), set_idx_node.end()}, list_idx_dof;
    dof().identifyDofBySupport(m_iUnknownPre, list_idx_node, list_idx_dof);

    // Retrieve petsc ordering
    AOApplicationToPetsc(m_ao, list_idx_dof.size(), list_idx_dof.data());

    // Get value of the pressure jump
    double pre_jump_val;
    felInt idx_dof_jap = 0;
    for (felInt iUnk = 0; iUnk < m_iUnknownJap; ++iUnk)
      idx_dof_jap += dof().numDofPerUnknown()[iUnk];
    
    AOApplicationToPetsc(m_ao, 1, &idx_dof_jap);
    sequentialSolution().getValue(idx_dof_jap, pre_jump_val);
  
    // Modify solution
    std::vector<double> list_val_dof(list_idx_dof.size(), pre_jump_val);
    solution().setValues(list_idx_dof.size(), list_idx_dof.data(), list_val_dof.data(), ADD_VALUES);
  }

  solution().assembly();
}

}
