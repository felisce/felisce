//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone C. Corrado
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemBidomainExtraCell.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "InputOutput/io.hpp"

namespace felisce {
  LinearProblemBidomainExtraCell::LinearProblemBidomainExtraCell():
    LinearProblem("Problem cardiac Bidomain second equation",2),
    m_fePotExtraCell(nullptr),
    m_fePotExtraCellCurv(nullptr),
    m_sortedSol(nullptr),
    m_fiber(nullptr),
    m_angleFiber(nullptr),
    m_endocardiumDistance(nullptr)
  {}

  LinearProblemBidomainExtraCell::~LinearProblemBidomainExtraCell() {
    m_fstransient = nullptr;
    m_fePotExtraCell = nullptr;
    m_fePotExtraCellCurv = nullptr;
    if (m_fiber)
      delete [] m_fiber;
    if (m_angleFiber)
      delete [] m_angleFiber;
    if (m_endocardiumDistance)
      delete [] m_endocardiumDistance;
    delete [] m_sortedSol;
  }

  void LinearProblemBidomainExtraCell::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable(1);
    std::vector<std::size_t> listNumComp(1);
    listVariable[0] = potExtraCell;
    listNumComp[0] = 1;
    //define unknown of the linear system.
    m_listUnknown.push_back(potExtraCell);
    definePhysicalVariable(listVariable,listNumComp);
  }

  void LinearProblemBidomainExtraCell::readData(IO& io) {
    // Read fibers file (*.00000.vct)
    m_fiber = new double[m_mesh[m_currentMesh]->numPoints()*3];
    io.readVariable(0, 0.,m_fiber, m_mesh[m_currentMesh]->numPoints()*3);
    if (FelisceParam::instance().hasCoupledAtriaVent) {
      m_angleFiber = new double[m_numDof];
      io.readVariable(2, 0.,m_angleFiber, m_numDof);
    }
  }

  void LinearProblemBidomainExtraCell::getFiberDirection(felInt iel, int iUnknown, std::vector<double>& elemFiber) {
    int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
    elemFiber.resize( numSupport*3,0.);
    for (int i = 0; i < numSupport; i++) {
      elemFiber[i*3] = m_fiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
      elemFiber[i*3+1] = m_fiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+1];
      elemFiber[i*3+2] = m_fiber[3*(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])+2];
    }
  }

  void LinearProblemBidomainExtraCell::getAngleFiber(felInt iel, int iUnknown, std::vector<double>& elemAngle) {
    int numSupport = static_cast<int>(supportDofUnknown(iUnknown).iEle()[iel+1] - supportDofUnknown(iUnknown).iEle()[iel]);
    elemAngle.resize(numSupport,0.);
    for (int i = 0; i < numSupport; i++) {
      elemAngle[i] = m_angleFiber[(supportDofUnknown(iUnknown).iSupportDof()[supportDofUnknown(iUnknown).iEle()[iel]+i])];
    }
  }

  void LinearProblemBidomainExtraCell::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_ELT_TYPE;
    //Init pointer on Finite Element, Variable or idVariable
    //    m_ipotTransMemb = m_listVariable.getVariableIdList(potTransMemb);
    m_ipotExtraCell = m_listVariable.getVariableIdList(potExtraCell);
    //    m_fePotTransMemb = m_listCurrentFiniteElement[m_ipotTransMemb];
    m_fePotExtraCell = m_listCurrentFiniteElement[m_ipotExtraCell];

  }

  void LinearProblemBidomainExtraCell::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;

    m_fePotExtraCell->updateFirstDeriv(0, elemPoint);
    std::vector<double> elemFiber;
    getFiberDirection(iel,m_ipotExtraCell, elemFiber);


    double eps = 1.0e-6;
    m_elementMat[0]->phi_i_phi_j(eps,*m_fePotExtraCell,0,0,1);

    //2nd equation : (\sigma_i^t+\sigma_e^t)) \grad u_e
    m_elementMat[0]->grad_phi_i_grad_phi_j(FelisceParam::instance().intraTransvTensor+FelisceParam::instance().extraTransvTensor,*m_fePotExtraCell,0,0,1);

    //2nd equation : ((\sigma_i^l-\sigma_i^t)+(\sigma_e^l-\sigma_e^t)) a vec a \grad u_e
    m_elementMat[0]->tau_grad_phi_i_tau_grad_phi_j((FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor)+(FelisceParam::instance().extraFiberTensor-FelisceParam::instance().extraTransvTensor),elemFiber,*m_fePotExtraCell,0,0,1);

    //2nd equation RHS : -(\sigma_i^t) \grad V_m
    m_elementMat[1]->grad_phi_i_grad_phi_j(-1.*(FelisceParam::instance().intraTransvTensor),*m_fePotExtraCell,0,0,1);

    //2nd equation RHS : -(\sigma_i^l-\sigma_i^t) a vec a \grad V_m
    m_elementMat[1]->tau_grad_phi_i_tau_grad_phi_j(-1.*(FelisceParam::instance().intraFiberTensor-FelisceParam::instance().intraTransvTensor),elemFiber,*m_fePotExtraCell,0,0,1);

  }


  void LinearProblemBidomainExtraCell::computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    (void) elemIdPoint;
    (void) flagMatrixRHS;
    m_ipotExtraCell = m_listVariable.getVariableIdList(potExtraCell);
    m_fePotExtraCellCurv = m_listCurvilinearFiniteElement[m_ipotExtraCell];

    m_fePotExtraCellCurv->updateMeasNormalContra(0, elemPoint);


    if ((m_currentLabel >= 30) && (m_currentLabel <= 40)) {
      std::vector<double> elemAngle;
      getAngleFiber(iel,m_ipotExtraCell,elemAngle);

      std::vector<double> elemFiber;
      getFiberDirection(iel,m_ipotExtraCell,elemFiber);

      // Constant angle
      //double angle = acos(0.0)/2.0;


      //2nd equation : (\sigma_i^t+\sigma_e^t) \grad u_e
      double eps = 1.0e-6;
      m_elementMatBD[0]->phi_i_phi_j(eps,*m_fePotExtraCellCurv,0,0,1);

      if (m_currentLabel < 40) { // regular atrial myocardium
        m_elementMatBD[0]->grad_phi_i_grad_phi_j((FelisceParam::instance().intraTransvTensorAtria+FelisceParam::instance().extraTransvTensorAtria),*m_fePotExtraCellCurv,0,0,1);
        m_elementMatBD[0]->tau_orthotau_grad_phi_i_grad_phi_j((FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria + FelisceParam::instance().extraFiberTensorAtria - FelisceParam::instance().extraTransvTensorAtria),elemFiber,elemAngle,*m_fePotExtraCellCurv,0,0,1);
      } else if (m_currentLabel == 40) { // connexion aera
        m_elementMatBD[0]->grad_phi_i_grad_phi_j((FelisceParam::instance().intraTransvTensorAtria+FelisceParam::instance().extraTransvTensorAtria)*0.005,*m_fePotExtraCellCurv,0,0,1);
      }


      //2nd equation RHS : -(\sigma_i^t) \grad V_m
      if (m_currentLabel < 40) { // regular atrial myocardium
        m_elementMatBD[1]->grad_phi_i_grad_phi_j(-1.0*(FelisceParam::instance().intraTransvTensorAtria),*m_fePotExtraCellCurv,0,0,1);
        m_elementMatBD[1]->tau_orthotau_grad_phi_i_grad_phi_j(-1.0*(FelisceParam::instance().intraFiberTensorAtria - FelisceParam::instance().intraTransvTensorAtria),elemFiber,elemAngle,*m_fePotExtraCellCurv,0,0,1);
      } else if (m_currentLabel == 40) { // connexion aera
        m_elementMatBD[1]->grad_phi_i_grad_phi_j(-0.005*(FelisceParam::instance().intraTransvTensorAtria),*m_fePotExtraCellCurv,0,0,1);
      }

    }
  }

  void LinearProblemBidomainExtraCell::addMatrixRHS() {
    // _RHS = _Ksigmai * _RHS
    PetscVector tmpB;
    tmpB.duplicateFrom(vector());
    tmpB.copyFrom(vector());
    mult(matrix(1),tmpB,vector());
    tmpB.destroy();

  }

}
