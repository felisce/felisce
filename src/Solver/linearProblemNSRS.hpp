//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __LINEARPROBLEMNSRS_HPP__
#define __LINEARPROBLEMNSRS_HPP__

// System includes

// External includes

// Project includes
#include "Solver/linearProblemReducedSteklov.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {
  class LinearProblemNSRS : public LinearProblemReducedSteklov<LinearProblemNS> {
  public:
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initializeDofBoundaryAndBD2VolMaps() override;
    void exportNormalField();
    void finalizeEssBCTransientDerivedProblem() override;
    void derivedProblemAssemble(FlagMatrixRHS flag) override;
    void solveStokesUsingSteklov(FelisceTransient::Pointer fs, int nfp);
    void computeResidualRS();
  private:
    void massMatrixComputer(felInt ielSupportDof) override;
    void laplacianMatrixComputer(felInt ielSupportDof) override;
    void initPerETLAP() override;
    void initPerETMASS() override;
    void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) override;
    void userApplyDirichletCondition();
    void computeTheConstantResponse() override;
    void useSteklovDataBegin() override;
    void useSteklovDataEnd() override;
  };
}
#endif
