//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "FiniteElement/elementMatrix.hpp"
#include "Core/felisceTransient.hpp"
#include "Geometry/curvatures.hpp"
#include "Solver/steklovBanner.hpp"

namespace felisce {
  
  // ======================================================================
  //                      the public methods
  // ======================================================================
  /* \brief Constructor
   */
  template <class volumeProblem>
  LinearProblemReducedSteklov<volumeProblem>::LinearProblemReducedSteklov():
    volumeProblem(),
    m_reducedSteklov(nullptr),
    m_chronoRS(nullptr)
  {
    m_imgType=dirichlet; // Default value is Dirichlet, but it can be changed in derived classes.
  }

  /*! \brief Destructor
   *
   *  We destroy the reducedSteklov class
   */
  template <class volumeProblem>
  LinearProblemReducedSteklov<volumeProblem>::~LinearProblemReducedSteklov(){
    if ( m_reducedSteklov )
      delete m_reducedSteklov;
  }

  /* \brief Function to assemble the system of the volume problem
   *  Matrix and Boundary Conditions
   */
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::assembleVolumeSystem( FlagMatrixRHS flag ) {
    this->assembleMatrixRHS(MpiInfo::rankProc(), flag );
    this->derivedProblemAssemble(flag);
    if ( m_imgType == neumann && !( flag == FlagMatrixRHS::only_rhs ) ) {
      this->createAndCopyMatrixRHSWithoutBC();
    }
    this->finalizeEssBCTransient();
    this->applyBC( FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), flag, flag );
  }
  /*! \brief Computation/Loading of the real (Full rank) Steklov operator
   *
   * this function compute the Steklov-Poincare operator
   * the idea is to solve N times the volume problem
   * where N is the dimension of the interface
   * each time the data is computed via dirac delta in a different dof
  */
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::assembleFullRankSteklov() {
    this->initChronoRS();
    //! We allocate the (dense) matrix for the Steklov operator
    if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      m_fullSteklov.createDense(this->m_dofBD[0/*iBD*/].comm(),
                                /*local row size*/  this->m_dofBD[0/*iBD*/].numLocalDofInterface(),
                                /*local col size*/  this->m_dofBD[0/*iBD*/].numLocalDofInterface(),
                                /*global row size*/ this->m_dofBD[0/*iBD*/].numGlobalDofInterface(),
                                /*global col size*/ this->m_dofBD[0/*iBD*/].numGlobalDofInterface(),
                                /*where to allocate the matrix*/FELISCE_PETSC_NULLPTR);
      m_fullSteklov.setFromOptions();
    }

    //! The operator is computed via the solution of the volume problem.
    //! this operator is obviously skipped when the operator is loaded from file.
    switch ( FelisceParam::instance().optionForSteklov ) {
    case LOAD_IT:
      break;
    case COMPUTE_IT:
    case COMPUTE_AND_SAVE_IT:
      {
        // we raise this flag to use correctly the boundary conditions
        // function
        this->useSteklovDataBegin();

        //! We use a banner object to handle the comunications on the screen of this function
        //! and to improve readibility
        steklovBanner banner(this->m_dofBD[0/*iBD*/].numLocalDofInterface(), MpiInfo::rankProc(), this->m_dofBD[0/*iBD*/].comm());


        // We switch off the verbosity of the KSP
        FelisceParam::linearSolverVerboseOFF();

        // We assemble the matrix of the linearProblem ( the volume matrix )
        // including the informations needed for the BC
        // the flag is std::set to only_matrix since we want to build just the matrix
        this->clearMatrixRHS( FlagMatrixRHS::only_matrix );
        this->assembleVolumeSystem( FlagMatrixRHS::only_matrix );

        banner.initComputation();
        std::size_t idDofGlobalApplication, idDofGlobalVolumePetsc;
        for ( idDofGlobalApplication=0; idDofGlobalApplication < this->m_dofBD[0/*iBD*/].numGlobalDofInterface(); ++idDofGlobalApplication, ++banner) {
          this->clearMatrixRHS( FlagMatrixRHS::only_rhs );
          // retrieving the correct volume indeces
          idDofGlobalVolumePetsc=this->m_dofBD[0/*iBD*/].globBD2PetscVol(idDofGlobalApplication);
          felInt idDofGlobalVolumeApplication = idDofGlobalVolumePetsc;
          felInt idDofGlobalPetsc = idDofGlobalApplication;
          AOPetscToApplication(this->m_ao,1,&idDofGlobalVolumeApplication);
          AOApplicationToPetsc(this->m_dofBD[0/*iBD*/].ao(), 1, &idDofGlobalPetsc);

          // creating the correct data steklov
          this->m_vecs.Get("dataSteklov").set(0.0);
          if ( this->m_dofPart[idDofGlobalVolumeApplication] == MpiInfo::rankProc() ) {
            this->m_vecs.Get("dataSteklov").setValue(idDofGlobalVolumePetsc, /* value */ 1., INSERT_VALUES);
          }
          this->m_vecs.Get("dataSteklov").assembly();
          this->gatherVector(this->m_vecs.Get("dataSteklov"),this->m_seqVecs.Get("dataSteklov"));

          //Problem solution
          this->assembleVolumeSystem( FlagMatrixRHS::only_rhs );

          m_chronoRS->start();
          this->solve( MpiInfo::rankProc(), MpiInfo::numProc() );
          m_chronoRS->stop();

          if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
            this->solution().getValues(this->m_dofBD[0/*iBD*/].numLocalDofInterface(), this->m_dofBD[0/*iBD*/].loc2PetscVolPtr(), m_tmpValues.data());
            m_fullSteklov.setValues(/*nb rows*/this->m_dofBD[0/*iBD*/].numLocalDofInterface(),/*id rows*/this->m_dofBD[0/*iBD*/].loc2PetscBDPtr(), /*nb cols*/ 1,/*id cols*/ &idDofGlobalPetsc , m_tmpValues.data(), INSERT_VALUES);
          }
        }
        if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
          m_fullSteklov.assembly(MAT_FINAL_ASSEMBLY);
        }
        banner.finalizeComputation();

        // restoring the normal state
        FelisceParam::linearSolverVerboseON();
        this->useSteklovDataEnd();
      }
      break;
    default:
      FEL_ERROR("unrecognized option for Steklov operator");
    }

    if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() )  {
      switch ( FelisceParam::instance().optionForSteklov ) {
      case LOAD_IT:
        {
          std::stringstream filename;
          if ( FelisceParam::instance().steklovMatrixName.size() == 0 )
            filename << "steklov_" << MpiInfo::numProc();
          else
            filename << FelisceParam::instance().steklovMatrixName << "_" << MpiInfo::numProc();
          m_fullSteklov.loadFromBinaryFormat(this->m_dofBD[0/*iBD*/].comm(),filename.str(),FelisceParam::instance().steklovDataDir);
        }
        break;
      case COMPUTE_IT:
        break;
      case COMPUTE_AND_SAVE_IT:
        {
          std::stringstream filename;
          filename << "steklov_" << MpiInfo::numProc();
          m_fullSteklov.saveInBinaryFormat(this->m_dofBD[0/*iBD*/].comm(),filename.str(),FelisceParam::instance().steklovDataDir);
        }
        break;
      }
    }
    if ( FelisceParam::instance().exportSteklovMatrix && this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      m_fullSteklov.saveInBinaryFormat(this->m_dofBD[0/*iBD*/].comm(),"Steklov",FelisceParam::instance().resultDir);
    }
  }
  /*! \brief Computation of the Reduced Order (Ro) Steklov operator.
   *
   *  The function provides an approximation of the Steklov operator. The matrix rapresentation of the operator
   *  is never computated. The action of the operator on a new input std::vector is approximated via a low rank expansion.
   */
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::assembleLowRankSteklov(felInt imesh){
    this->initChronoRS();
    //! First the mass and the laplacian matrix on the boundary are computed.
    PetscMatrix mass = assembleMassBoundary();
    PetscMatrix laplacian = assembleLaplacianBoundary();
    //! Then the object handling the steklov reduction techniques is created.
    m_reducedSteklov = new ReducedSteklov<volumeProblem>(mass, laplacian, this->m_dofBD[0/*iBD*/].comm(), this, m_imgType,m_chronoRS);

    this->computeTheConstantResponse();
    m_reducedSteklov->addConstantResponse(this->getSteklovImg());

    if ( m_reducedSteklov->loadedFromFile() ) {
      std::stringstream msg;
      msg<<"Off-line basis correctly loaded."<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    } else {
     if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        //! The laplacian eigen-problem on the surface is solved.
        m_reducedSteklov->solveLaplacianEP();
      }
      //! The image through the steklov operator of the laplacian eigen-std::vector is computed.
      m_reducedSteklov->applySteklovOperatorOnLEV(imesh);
      if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        //! The steklov eigenValues and eigenVectors are approximated.
        m_reducedSteklov->steklovEVecEstimate();
      }
    }
  }
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::setSteklovData( PetscVector& bdInput ) {
    this->m_vecs.Get("dataSteklov").set(0.0);
    this->dofBD(/*iBD*/0).extendOnVolume( this->m_vecs.Get("dataSteklov"), bdInput );
    this->gatherVector( this->m_vecs.Get("dataSteklov"), this->m_seqVecs.Get("dataSteklov"));
  }

  template <class volumeProblem>
  PetscVector LinearProblemReducedSteklov<volumeProblem>::getSteklovImg() {

    //! Initialization
    PetscVector bdOutput;
    if ( m_tmpValues.size() != this->m_dofBD[0/*iBD*/].numLocalDofInterface() ) {
      m_tmpValues.resize( this->m_dofBD[0/*iBD*/].numLocalDofInterface() );
    }

    if ( m_imgType == dirichlet ) {
      //! Just read the data
      this->solution().getValues( this->m_dofBD[0/*iBD*/].numLocalDofInterface(), this->m_dofBD[0/*iBD*/].loc2PetscVolPtr(), m_tmpValues.data());
    } else if (m_imgType == neumann) {
      //! Compute the residual
      this->computeResidual();
      //! And read the data inside
      this->m_residual.scale(-1); //We change the sign to be consistent with the reduce steklov class
      this->m_residual.getValues( this->m_dofBD[0/*iBD*/].numLocalDofInterface(), this->m_dofBD[0/*iBD*/].loc2PetscVolPtr(), m_tmpValues.data());
      this->m_residual.scale(-1);
    }
    if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      //allocation
      bdOutput = this->dofBD(/*iBD*/0).allocateBoundaryVector(DofBoundary::parallel);
      //std::set to zero
      bdOutput.set(0.0);
      //std::set of the real values
      bdOutput.setValues( this->m_dofBD[0/*iBD*/].numLocalDofInterface(), this->m_dofBD[0/*iBD*/].loc2PetscBDPtr(), m_tmpValues.data(), INSERT_VALUES);
      //assembly
      bdOutput.assembly();
    }
    return bdOutput;
  }

  // ======================================================================
  //                   now the privates methods
  // ======================================================================
  //  reader of vectors coming from different linearProblems
  //  the std::vector are stored in sequential std::vector "whereToSave"
  // For scalar
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::readerInterface( const std::map<int,std::map<int,int> > & externalMap,
                                                               PetscVector& seqExternalVec,
                                                               const std::map<int,int> &mapBetweenLabels,
                                                               std::string whereToSave,
                                                               felInt iUnkWTW, // std::set to zero by default
                                                               felInt iCompWTW // std::set to zero by default
                                                               ) {
    this->readerInterfaceComponentWise(externalMap,seqExternalVec,mapBetweenLabels,whereToSave,iUnkWTW,iCompWTW);
    this->m_vecs[whereToSave].assembly();
    this->gatherVector(this->m_vecs[whereToSave],this->m_seqVecs[whereToSave]);
    if ( FelisceParam::verbose() > 0 ) {
      PetscPrintf(MpiInfo::petscComm(), "done\n");
    }
  }
  // For std::vector
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::readerInterface( const std::map<int, std::map<int,std::map<int,int> > > & externalMap,
                                                               PetscVector& seqExternalVec,
                                                               const std::map<int,int> &mapBetweenLabels,
                                                               std::string whereToSave,
                                                               felInt iUnkWTW,
                                                               felInt iUCR
                                                               ) {
    for ( std::size_t iComp(0); iComp < (std::size_t) this->dimension(); ++iComp,++iUCR) {
      this->readerInterfaceComponentWise( externalMap.at(iUCR),seqExternalVec, mapBetweenLabels, whereToSave, iUnkWTW, iComp);
    }
    this->m_vecs[whereToSave].assembly();
    this->gatherVector(this->m_vecs[whereToSave],this->m_seqVecs[whereToSave]);
    if ( FelisceParam::verbose() > 0 ) {
      PetscPrintf(MpiInfo::petscComm(), "done\n");
    }
  }
  // Reader of vectors coming from different linearProblems
  // the std::vector are stored in sequential std::vector "whereToSave"
  // TODO: This function fill a volume std::vector, can this be avoided?
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::readerInterfaceComponentWise( const std::map<int,std::map<int,int> > & externalMap,
                                                                            PetscVector& seqExternalVec,
                                                                            const std::map<int,int> &mapBetweenLabels,
                                                                            std::string whereToSave,
                                                                            felInt iUnkWTW,
                                                                            felInt iCompWTW) {
    // some couts to help debugging
    if ( FelisceParam::verbose() > 2 )
      PetscPrintf(MpiInfo::petscComm(),"%s", std::string("[linearProblem--Reduced Steklov] Reading "+ whereToSave +"...").c_str() );

    felInt iDofGlobThisLPB;     // petsc       index of the current dof in this linear problem
    felInt iDofGlobThisLPBApp;  // application index of the current dof in this linear problem
    felInt iDofGlobOtherLPB;    // petsc       index of the current dof in the other linear problem

    int iUnknComp = this->dof().getNumGlobComp(iUnkWTW, iCompWTW);
    double tmpValue;

    // for each label of the interface in the mesh of this problem
    for(auto currentLabel = m_interfaceLabels.begin(); currentLabel != m_interfaceLabels.end();  currentLabel++) {
      // we get the corresponding label in the other mesh
      felInt labelOtherLPBMesh=mapBetweenLabels.find(*currentLabel)->second;
      // some couts to help debugging
      if ( FelisceParam::verbose() > 2 ) {
        std::cout << std::endl;
        std::cout << "Interface label from reduced steklov mesh: "<<*currentLabel<<" , label from the other mesh: "<<labelOtherLPBMesh<<std::endl;
      }
      // for each dof on this portion of the interface
      for(auto currentPetscOtherLPBDOF = externalMap.find(labelOtherLPBMesh)->second.begin(); //begin of the std::unordered_map
            currentPetscOtherLPBDOF !=  externalMap.find(labelOtherLPBMesh)->second.end(); //end of the std::unordered_map
            ++currentPetscOtherLPBDOF) {
        iDofGlobOtherLPB = currentPetscOtherLPBDOF->second; //global dof
        iDofGlobThisLPB = this->m_dofBD[0/*iBD*/].getPetscDofs(/*iUnknown,iComp where to write*/iUnknComp, *currentLabel, currentPetscOtherLPBDOF->first) ;

        iDofGlobThisLPBApp = iDofGlobThisLPB;
        AOPetscToApplication(this->m_ao,1,&iDofGlobThisLPBApp);
        // we check if the dof belongs to this proc
        if ( this->m_dofPart[iDofGlobThisLPBApp] == MpiInfo::rankProc() ) {
          //we get the value
          seqExternalVec.getValues(1,&iDofGlobOtherLPB, &tmpValue);
          // and we std::set it
          this->m_vecs.Get(whereToSave).setValue(iDofGlobThisLPB, tmpValue, INSERT_VALUES);
        }
      }
    }
  }




  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::setValueMatrixBD(felInt ielSupportDof) {
    // Elementary matrices are placed into the bigger one defined on the boundary.
    felInt numDofTotal = this->m_elementMatBD[0]->mat().size1();

    felInt node1 = 0;
    felInt node2 = 0;
    felInt cptGposLine = 0;
    felInt cptGposLine2 = 0;
    felInt sizeSupport = 0;
    felInt sizeSupport2 = 0;
    felInt cptBlock = 0;
    int idVar = -1;
    int idVar2 = -1;
    //! for all the unknowns of the problems
    for ( std::size_t iUnknown = 0; iUnknown < this->m_listUnknown.size(); iUnknown++) {
      idVar = this->m_listUnknown.idVariable(iUnknown);
      sizeSupport = this->m_listCurvilinearFiniteElement[idVar]->numDof();
      //! for all its components
      for (std::size_t iComp = 0; iComp < this->m_listVariable[idVar].numComponent(); iComp++) {
        cptBlock++;
        //! for all the dof on the element
        for ( int iSupport = 0; iSupport < sizeSupport; iSupport++) {

          //! get the volume application index
          this->dof().loc2glob(ielSupportDof, iSupport, idVar, iComp, node1);
          //! save it
          m_globPosRow[cptGposLine]= node1;
          cptGposLine2 = 0;
          //! then we do the same with the columns:
          //! for each unknown
          for ( std::size_t iUnknown2 = 0; iUnknown2 <this->m_listUnknown.size(); iUnknown2++) {
            idVar2 = this->m_listUnknown.idVariable(iUnknown2);
            sizeSupport2 = this->m_listCurvilinearFiniteElement[idVar2]->numDof();
            //! for all its components
            for (std::size_t jComp = 0; jComp <this->m_listVariable[idVar2].numComponent(); jComp++) {
              //! for all the dofs on the element
              for ( int jSupport = 0; jSupport < sizeSupport2; jSupport++) {
                int iConnect = this->dof().getNumGlobComp( iUnknown, iComp);
                int jConnect = this->dof().getNumGlobComp( iUnknown2, jComp);
                if ( this->m_listUnknown.mask()(iConnect, jConnect) > 0) {
                  //! the global index
                  this->dof().loc2glob(ielSupportDof,jSupport,idVar2,jComp,node2);
                  //! save it
                  m_globPosColumn[cptGposLine2] = node2;
                  //! store in this small matrix the values just computed
                  m_matrixValues[cptGposLine2 + cptGposLine*numDofTotal] = this->m_elementMatBD[0]->mat()(cptGposLine, cptGposLine2);
                  cptGposLine2++;
                }
              }
            }
          }
          cptGposLine++;
        }
      }
    }
    //! Now we have to compute where this numbers have to be put into the boundary matrix
    //! volume application -> petsc volume
    AOApplicationToPetsc(this->m_ao,cptGposLine,m_globPosRow.data());
    AOApplicationToPetsc(this->m_ao,cptGposLine2,m_globPosColumn.data());

    std::vector<felInt> row;
    std::vector<felInt> col;
    std::vector<felInt> tmpCol;
    std::vector<double> val;
    //! petsc volume -> boundary application
    for ( int k(0); k < cptGposLine; ++k) {
      tmpCol.clear();
      try {
        // petscVol2ApplicationBD can throw an out_of_range while m_globPosRow[k] can not
        // The idea is that if this fails it means that this particular dof is not in the mapping
        // and that it can be discarded (e.g. pressure nodes when dealing with Stokes problem)
        row.push_back(this->m_dofBD[0/*iBD*/].petscVol2ApplicationBD(m_globPosRow[k]));
        for ( int k2(0); k2 < cptGposLine2; ++k2) {
          try {
            tmpCol.push_back(this->m_dofBD[0/*iBD*/].petscVol2ApplicationBD(m_globPosColumn[k2]));
            val.push_back(m_matrixValues[k2+k*numDofTotal]);
          } catch ( const std::out_of_range& oor ){}
        }
      } catch ( const std::out_of_range& oor ){}
      if (tmpCol.size()>0) {
        col=tmpCol;
      }
    }
    //! boundary application -> petsc application
    AOApplicationToPetsc(this->m_dofBD[0/*iBD*/].ao(),row.size(),row.data());
    AOApplicationToPetsc(this->m_dofBD[0/*iBD*/].ao(),col.size(),col.data());
    // std::set value
    m_aux.setValues(row.size(),row.data(),col.size(),col.data(),val.data(),ADD_VALUES);
  }
  

  //! \brief It assembles the laplacian matrix on the boundary.
  template <class volumeProblem>
  PetscMatrix
  LinearProblemReducedSteklov<volumeProblem>::assembleLaplacianBoundary() {
    PetscMatrix laplacian;
    if ( this->dimension() == 3 ) {
      this->m_dofBD[0/*iBD*/].allocateMatrixOnBoundary( laplacian );
      m_aux=laplacian;
      if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        this->assemblyLoopBoundaryGeneral( &LinearProblemReducedSteklov<volumeProblem>::laplacianMatrixComputer,
                                           m_interfaceLabels,
                                           &LinearProblemReducedSteklov<volumeProblem>::initPerETLAP,
                                           &LinearProblemReducedSteklov<volumeProblem>::updateFE);// we do not need the derivatives for surface laplacian: they are computed in m_curv
        m_globPosColumn.clear();m_globPosRow.clear();m_matrixValues.clear();
        laplacian.assembly(MAT_FINAL_ASSEMBLY);
      }
      if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        if ( FelisceParam::instance().exportLaplacianMatrix ) {
          laplacian.saveInBinaryFormat( this->m_dofBD[0/*iBD*/].comm(), "Laplacian" , FelisceParam::instance().resultDir);
        }
      }
      // Better to put it since m_aux will stay alive as long as the linear problem,
      // while we would like to deallocate this matrix when the laplacian matrix gets out of scope
      m_aux.destroy();
    } else {
      FEL_ERROR("We can not yet assemble the laplacian operator on the boundary of a 2D-domain.");
    }
    return laplacian;
  }
  /* ! \brief this function assemble the mass matrix at the boundary.
   *  It saves such matrix in binary format and then it destroies the matrix.
   */
  template <class volumeProblem>
  PetscMatrix
  LinearProblemReducedSteklov<volumeProblem>::assembleMassBoundary( ) {
    PetscMatrix mass;
    this->m_dofBD[0/*iBD*/].allocateMatrixOnBoundary( mass );
    m_aux=mass;
    if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      this->assemblyLoopBoundaryGeneral( &LinearProblemReducedSteklov<volumeProblem>::massMatrixComputer,
                                         m_interfaceLabels,
                                         &LinearProblemReducedSteklov<volumeProblem>::initPerETMASS,
                                         &LinearProblemReducedSteklov<volumeProblem>::updateFE);
      m_globPosColumn.clear();m_globPosRow.clear();m_matrixValues.clear();

      mass.assembly(MAT_FINAL_ASSEMBLY);
      if ( FelisceParam::instance().exportMassMatrix ) {
        mass.saveInBinaryFormat( this->m_dofBD[0/*iBD*/].comm(), "Mass", FelisceParam::instance().resultDir);
      }
    }
    // Better to put it since m_aux will stay alive as long as the linear problem,
    // while we would like to deallocate this matrix when the mass matrix gets out of scope
    m_aux.destroy();
    return mass;
  }
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::exportOutputSteklov(FelisceTransient::Pointer fstransient,int iFixedPointIteration) {
    PetscVector restrictedSolution = this->dofBD(/*iBD*/0).allocateBoundaryVector(DofBoundary::parallel);
    this->dofBD(/*iBD*/0).restrictOnBoundary(this->solution(),restrictedSolution);
    std::stringstream iFixedPointIterationString;
    if ( iFixedPointIteration < 10 )     iFixedPointIterationString<<"00"<<iFixedPointIteration;
    else if (iFixedPointIteration < 100) iFixedPointIterationString<<"0" <<iFixedPointIteration;
    else                                 iFixedPointIterationString      <<iFixedPointIteration;
    if( FelisceParam::instance().exportOutputOfSteklov ) {
      // binary export
      if ( FelisceParam::instance().exportOnlyLastOutput ) {
        iFixedPointIterationString.str("last");
      }
      std::stringstream filename2;
      filename2<<"outputSteklov_"<<fstransient->itStr()<<"_"<<iFixedPointIterationString.str().c_str();
      if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
        restrictedSolution.saveInBinaryFormat(this->m_dofBD[0/*iBD*/].comm(), filename2.str(), FelisceParam::instance().resultDir);
      }
    }
  }

  // the data is given as a volume std::vector: first you restrict this volume std::vector on
  // the surface you apply the Steklov operator and you save the result in a volume
  // std::vector
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::applySteklov( PetscVector& volumeInput, PetscVector& volumeOutput, FelisceTransient::Pointer fstransient, int iFixedPointIteration) {
    // reset this to zero
    volumeOutput.set(0.0);
    PetscVector Input  = this->dofBD(/*iBD*/0).allocateBoundaryVector(DofBoundary::parallel);
    PetscVector Output = this->dofBD(/*iBD*/0).allocateBoundaryVector(DofBoundary::parallel);
    this->dofBD(/*iBD*/0).restrictOnBoundary( volumeInput, Input);

    if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() ) {
      if ( FelisceParam::instance().useRoSteklov ) {
        m_reducedSteklov->applyLowRankSteklov(Input,Output);
      } else {
        mult(m_fullSteklov, Input, Output);
      }
      this->exportSteklovInputOutput(Input,Output,fstransient,iFixedPointIteration);
    }
    this->dofBD(/*iBD*/0).extendOnVolume(volumeOutput,Output);
  }
  template <class volumeProblem>
  void LinearProblemReducedSteklov<volumeProblem>::exportSteklovInputOutput( PetscVector& Input, PetscVector& Output, FelisceTransient::Pointer fstransient, int iFixedPointIteration) {
    if ( FelisceParam::instance().exportSteklovData ) {
      std::stringstream iFixedPointIterationString;
      if ( iFixedPointIteration < 10 )     iFixedPointIterationString<<"00"<<iFixedPointIteration;
      else if (iFixedPointIteration < 100) iFixedPointIterationString<<"0" <<iFixedPointIteration;
      else                                 iFixedPointIterationString      <<iFixedPointIteration;
      if( FelisceParam::instance().exportInputOfSteklov ) {
        if ( FelisceParam::instance().exportOnlyLastInput ) { iFixedPointIterationString.str("last"); }
        std::stringstream filename;
        filename<<"inputSteklov_"<<fstransient->itStr()<<"_"<<iFixedPointIterationString.str().c_str();
        Input.saveInBinaryFormat(this->m_dofBD[0/*iBD*/].comm(), filename.str(), FelisceParam::instance().resultDir);
      }
      if( FelisceParam::instance().exportOutputOfSteklov ) {
        if ( FelisceParam::instance().exportOnlyLastOutput ) { iFixedPointIterationString.str("last"); }
        std::stringstream filename2;
        filename2<<"outputSteklov_"<<fstransient->itStr()<<"_"<<iFixedPointIterationString.str().c_str();
        Output.saveInBinaryFormat(this->m_dofBD[0/*iBD*/].comm(), filename2.str(), FelisceParam::instance().resultDir);
      }
    }
  }
  template <class volumeProblem>
  void LinearProblemReducedSteklov<volumeProblem>::userSetNullSpace(PetscVector& v, int k) {
    //TODO move this in a user file or do something better
    if ( FelisceParam::instance().notConnectedCylinders ) {
      v.set(0.0);
      std::size_t labelCount = 0;
      double aus(1.0);
      for(auto itLabel = this->m_dofBD[0/*iBD*/].listOfBoundaryPetscDofs(/*iUnkown*/0,/*iComp*/0).begin(); itLabel != this->m_dofBD[0/*iBD*/].listOfBoundaryPetscDofs(/*iUnkown*/0,/*iComp*/0).end(); ++itLabel, ++labelCount ) {
        if ( labelCount == std::size_t(k) ) {
          for(auto itDof = itLabel->second.begin(); itDof != itLabel->second.end(); ++itDof) {
            felInt dof=this->m_dofBD[0/*iBD*/].petscVol2ApplicationBD(itDof->second);
            AOApplicationToPetsc(this->m_dofBD[0/*iBD*/].ao(),1,&dof);
            v.setValues(1,&dof,&aus, INSERT_VALUES);
          }
        }
      }
      v.assembly();
    } else {
      if ( k > 0 ) {
        FEL_ERROR("There are too many zero eigenvalues: implement your own userSetNullSpace function");
      } else {
        v.set(1.0);
      }
    }
  }
  //! \brief it modifies the state of the class
  //! In particular it raises the flag m_useSteklovData, and it allocates the dataSteklov vectors
  template <class volumeProblem>
  void LinearProblemReducedSteklov<volumeProblem>::useSteklovDataBegin() {
    this->m_useSteklovData = true;
    this->m_vecs["dataSteklov"].duplicateFrom( this->solution() );
    this->m_vecs.Get("dataSteklov").set(0.0);
    this->m_seqVecs["dataSteklov"].duplicateFrom( this->sequentialSolution() );
    this->m_seqVecs.Get("dataSteklov").set(0.0);
    m_tmpValues.resize( this->m_dofBD[0/*iBD*/].numLocalDofInterface() );
    this->m_boundaryConditionList.temporarySetBCValuesToZero();
  }

  //! \brief it restores the previous state of the class
  //! Call this function only after having used useSteklovBegin.
  //! The flag: m_useSteklovData is reset to false and the dataSteklov vectors are destroyed
  template <class volumeProblem>
  void
  LinearProblemReducedSteklov<volumeProblem>::useSteklovDataEnd() {
    this->m_useSteklovData = false;
    this->m_vecs.erase("dataSteklov");
    this->m_seqVecs.erase("dataSteklov");
    this->m_boundaryConditionList.restoreBCValues();
  }
}
