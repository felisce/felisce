//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:   S.Smaldone 
//

#ifndef _RISModel_HPP
#define _RISModel_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "Solver/linearProblem.hpp"

namespace felisce {
  class RISModel {

  public:
    RISModel(FelisceTransient::Pointer fstransient , std::vector<LinearProblem*> linearProblem); // constructor
    ~RISModel() = default; // destructor

    // two main methods called with Navier-Stokes in this order: UpdateResistances, forward and CheckValveStatus. If admissibility of the model is 0, Navier-Stokes is computed again
    void UpdateResistances(); // updates resistances of closed and open surfaces and print them
    void CheckValveStatus(); // check the admissibility status of each valve depending on their current configuration and update the admissibility status of the whole model

    // getters
    inline felInt admissibleStatus() {
      return m_ModelAdmissibleStatus;
    }
    inline const double & resistance(felInt label) const {
      return m_resistances[label];
    }
    inline double & resistance(felInt label) {
      return m_resistances[label];
    }
    inline int valve_is_closed() {
      return m_valveClosedFlag[0];  // used in CardiacCycle class and applied to 1 valve case. Returns 1 is the first RIS couple is in closed configuration, 0 if not
    }

    int areAllValvesClosed() const; // return 1 if all valves are closed, 0 if not
    void write(std::string) const; // write resistance values of closed and open surfaces in a file whose name is std::string
 
    //TODO : fuse those two ?
    std::vector<felInt> mitralRegurgitationElements;
    std::map<felInt, double*> mitralRegMap;

  private:
    // methods
    void variables_initialization(); // initialisation of attributes for the general RIS model
    void RIS_models_initialization(int rs); // initialisation of the resistance values of all open, closed and fake surfaces

    void check_opening_condition(int rs, std::vector<double> pressure_surf); // resistances of closed valves std::set to R_inactive if opening conditions are satisfied + save opening_time
    void check_closing_condition(int rs, std::vector<double> flux_surf); // resistances of closed valves std::set to R_active if closing conditions are satisfied + save closing_time
    bool preventValvesFromOpeningTooFast(int rs); // return true if a valve has recently closed whereas another valve would meet the conditions to open

    void two_valve_case(int rs);

    void update_open_surf(int rs); // std::set resistance of open surfaces to R_active if valve is open, to R_inactive if valve is closed
    void update_closed_surf(int rs); // std::set resistance of closed valve to R_inactive if valve is open, to R_active if valve is closed

    void check_closed_status(int rs); // check if the closed surfaces are in adequation with the fluid and open them if not
    void check_open_status(int rs); // check if the open surfaces are in adequation with the fluid and close them if not

    void print() const; // print status and resistance values of each closed and open surfaces

    // attributes
    FelisceTransient::Pointer m_fstransient; // FelisceTransient
    std::vector<LinearProblem*> m_linearProblem; // linearProblem

    felInt m_ModelAdmissibleStatus; // admissibility of the model = product of valve admissible status (0 if not correct, 1 if correct)
    std::vector<felInt> m_ValveAdmissibleStatus; // admissibility values of the valve (0 if not correct, 1 if correct)
    std::vector<double> m_resistances; // resistance values of all the surfaces (closed + fake + open)
    std::vector<felInt> m_closedLabels; // labels of the current considered couple of closed labels
    std::vector<int> m_closed_surf; // labels of the closed surfaces
    std::vector<int> m_open_surf; // labels of the open surfaces
    std::vector<int> m_fake_surf; // labels of the fake surfaces
    std::vector<double> m_flow_ref; // size = m_RISModelNum. If size == 1 -> 1e-2. If size == 2 -> -2 and 0
    felInt m_RISModelNum; // number of couple of closed surfaces
    felInt m_cycl_RIS; // number of RIS cycles
    felInt m_verboseRIS; // verbose for the RIS model

    std::vector<int> m_valveClosedFlag; // status of the valve. If 0: open, if 1: closed
    std::vector<double> m_closingTime; // time of closing of each RISModel
    std::vector<double> m_openingTime; // time of opening of each RISModel

    felInt m_num_closed_surfs; // number of closed surfaces
    felInt m_num_open_surfs; // number of open surfaces
    felInt m_num_fake_surfs; // number of fake surfaces

    double m_R_active_surf; // resistance of closed valve
    double m_R_inactive_surf; // resistance of open valve
    double m_R_activeClosed_TwoValves_Intermediate; // resistance of closed valve just after the closing - only used for two valves
    double m_R_activeClosed_TwoValves_Final; // final resistance of closed valve after several iterations after the closing - only used for two valves
    double m_R_FirstValve_ClosingSpeed; // rate of speed for the evolution of the resistance of the closed surface of the first valve from R_Intermediate to R_Final - only used for two valves
    double m_R_SecondValve_ClosingSpeed; // rate of speed for the evolution of the resistance of the closed surface of the second valve from R_Intermediate to R_Final - only used for two valves
    double m_tps; // current time
    int m_nbTimeSteps_refractory_time; // number of time steps used for defining the refractory time
    double m_refractory_time; // refractory time: time lap during the valve can not change its configuration
    double m_rate_linear_transition; // rate of linear evolution
    double m_rate_linear_transition_firstValve; // rate of linear evolution first valve
    double m_rate_linear_transition_secondValve; // rate of linear evolution second valve
    bool m_linear_evolution_valves; // linear evolution in the value of the resistances of the valves

  };
}

#endif
