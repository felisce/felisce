//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemPerfectFluidRS.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce{
  /* \brief Function to assemble the mass matrix
   * Function to be called in the assemblyLoopBoundaryGeneral
   */
  void LinearProblemPerfectFluidRS::massMatrixComputer(felInt ielSupportDof) {
    this->m_elementMatBD[0]->zero();
    this->m_elementMatBD[0]->phi_i_phi_j(/*coef*/1.,*m_feIopCurv,/*iblock*/0,/*iblock*/0,/*numComponent*/1.);
    this->setValueMatrixBD(ielSupportDof);
  }
  
  /* \brief Function to assemble the laplacian matrix
   * Function to be called in the assemblyLoopBoundaryGeneral
   */
  void  LinearProblemPerfectFluidRS::laplacianMatrixComputer(felInt ielSupportDof) {
    for ( felInt efComp(0); efComp < this->dimension(); ++efComp ) {
      std::stringstream name;
      name<<"normalVector" << efComp;
      m_normalField.setValue(this->m_seqVecs.Get(name.str()), *m_feIopCurv , ielSupportDof, /*m_idVarIop*/0, this->m_ao, this->dof(),
                             /*component where to read in inputvector*/0, /*where to save in elemfield*/efComp);
    }
    m_curv.update(m_normalField, m_feIopCurv->covMetric[0], m_feIopCurv->m_covBasis[0]);
    this->m_elementMatBD[0]->zero();
    this->m_elementMatBD[0]->sGrad_psi_j_tensor_sGrad_phi_i_for_scalar(/*coef*/ 1, *m_feIopCurv, m_curv.invMetricTensor(),/*idBlock*/0 );
    this->setValueMatrixBD(ielSupportDof);
  }
  void
  LinearProblemPerfectFluidRS::initPerETLAP() {
    this->initPerETMASS();
    m_normalField.initialize( DOF_FIELD, *m_feIopCurv, m_feIopCurv->numCoor() );       
  }
  void
  LinearProblemPerfectFluidRS::initPerETMASS() {
    felInt numDofTotal = 0;
    //pay attention this numDofTotal is bigger than expected
    for (std::size_t iFe = 0; iFe < this->m_listCurvilinearFiniteElement.size(); iFe++) {//this loop is on variables while it should be on unknown
      numDofTotal += this->m_listCurvilinearFiniteElement[iFe]->numDof()*this->m_listVariable[iFe].numComponent();
    }
    m_globPosColumn.resize(numDofTotal); m_globPosRow.resize(numDofTotal); m_matrixValues.resize(numDofTotal*numDofTotal);
    
    m_feIopCurv = this->m_listCurvilinearFiniteElement[ 0 /* m_idVarIop */ ];
  }
  void
  LinearProblemPerfectFluidRS::updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&) {
    m_feIopCurv->updateMeasNormal(0, elemPoint);  
  }
  
  /*! \brief DofBoundary class is built
   *  This function calls the methods to correctly initialize the DofBoundary class.
   */
  void 
  LinearProblemPerfectFluidRS::initializeDofBoundaryAndBD2VolMaps() {
    /// this function builds all the mappings 
    /// to deal with petsc objects defined at the interface
    m_dofBD[0/*iBD*/].initialize(this);
    this->dofBD(/*iBD*/0).buildListOfBoundaryPetscDofs(this, FelisceParam::instance().labelIOPMesh, this->idUnknownIop() , /*iComp*/0); 
    m_dofBD[0/*iBD*/].buildBoundaryVolumeMapping( m_iUnknownIop, 0);
    m_interfaceLabels=FelisceParam::instance().labelIOPMesh;
  }
  // this function is supposed to be called in the model
  // and it solves the problem using the S-P operator
  // we assume that the steklov is allocated
  // and that velocity interface has been already read
  // from NS
  void 
  LinearProblemPerfectFluidRS::solvePFUsingSteklov(FelisceTransient::Pointer fs, int nfp)
  {
    // initialization and computation
    m_seqVecs.Init("velPlusAlphaIop");
    m_seqVecs.Get ("velPlusAlphaIop").duplicateFrom(m_seqVecs.Get("velocityInterface"));
    m_seqVecs.Get ("velPlusAlphaIop").copyFrom(m_seqVecs.Get("velocityInterface"));
    m_seqVecs.Get ("velPlusAlphaIop").axpy(m_relaxParam,m_seqVecs.Get("currentIop")); // this iop is the last available approximation of the iop
    if ( FelisceParam::instance().porousParam < 0 ) {
      m_seqVecs.Get ("velPlusAlphaIop").axpy(-1,m_seqVecs.Get("velocityInterfaceOld"));
    }
    // We could have read it directly from the solution(), however this is true only if you do fixed point iterations, we have implemented also more sophisticated techniques that further update
    // the current iop

    this->applySteklov(m_seqVecs.Get("velPlusAlphaIop"), this->solution(),fs,nfp);
    
    // destruction
    m_seqVecs.erase("velPlusAlphaIop");
  }
  
  /*! \brief Function to apply boundary conditions
   * Very important function when working with the Steklov operator.
   *
   * Here the bounadry conditions are detailed, depending on the
   * datafile, we use different couplings.
   */
  void 
  LinearProblemPerfectFluidRS::userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/,felInt& iel, int label) {
    // declaration of a pointer to BC
    BoundaryCondition* BC;
    //! We check among the Neumann boundary conditions if the labels in FelisceParam.labelIOPMesh are here.
    for (std::size_t iInterface=0; iInterface<m_boundaryConditionList.numNeumannBoundaryCondition(); iInterface++ ) { 
      BC = m_boundaryConditionList.Neumann(iInterface);
      // if the current label is from neumann and if it is in the interface
      // and if it is of type EnsightFile
      if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),label) != BC->listLabel().end() &&
           std::find( FelisceParam::instance().labelIOPMesh.begin(), FelisceParam::instance().labelIOPMesh.end(),label) != FelisceParam::instance().labelIOPMesh.end() &&
           BC->typeValueBC() == EnsightFile
           ) {
        CurvilinearFiniteElement *cfeiop = m_listCurvilinearFiniteElement[m_idVarIop];
        if ( m_useSteklovData ) {
          m_elemFieldNeumann[iInterface].setValue( m_seqVecs.Get("dataSteklov"), *cfeiop , iel, m_idVarIop, m_ao, dof());
        }
        else {
          //! We add the coupling condition (same for Neumann and Robin-coupling
          m_elemFieldNeumann[iInterface].setValue( m_seqVecs.Get("velocityInterface"), *cfeiop, iel, m_idVarIop, m_ao, dof());

          //! In the case of a porous media the above line is enough
          //! With a perfect fluid we need to subtract the old value: they are already divided by the timeStep: we are approximating the acceleration.
          if ( FelisceParam::instance().porousParam < 0 ) {
            ElementField oldVelInterface;
            oldVelInterface.initialize(DOF_FIELD,*cfeiop,1);
            oldVelInterface.setValue( m_seqVecs.Get("velocityInterfaceOld"), *cfeiop, iel, m_idVarIop, m_ao, dof());
            for ( int iLocDof(0); iLocDof<cfeiop->numDof(); ++iLocDof) {
              m_elemFieldNeumann[iInterface].val(0,iLocDof) =  m_elemFieldNeumann[iInterface].val(0,iLocDof) - oldVelInterface.val( 0, iLocDof );
            }
          }
        }
      }
    }
    //! Then we check among the Robin boundary conditions
    for (std::size_t iInterface=0; iInterface<m_boundaryConditionList.numRobinBoundaryCondition(); iInterface++ ) { 
      BC = m_boundaryConditionList.Robin(iInterface);
      // if the current label is from robin and if it is in the interface
      // and if it is and EnsightFile type
      if ( std::find( BC->listLabel().begin(), BC->listLabel().end(),label) != BC->listLabel().end() &&
           std::find( FelisceParam::instance().labelIOPMesh.begin(), FelisceParam::instance().labelIOPMesh.end(),label) != FelisceParam::instance().labelIOPMesh.end() &&
           BC->typeValueBC() == EnsightFile
           ) {
        //! the FP.alphaRobin is overwritten with the relaxation parameter.
        FelisceParam::instance().alphaRobin[iInterface] = m_relaxParam;

        CurvilinearFiniteElement *cfeiop = m_listCurvilinearFiniteElement[m_idVarIop];
        if ( m_useSteklovData ) {
          m_elemFieldRobin[iInterface].setValue( m_seqVecs.Get("dataSteklov") , *cfeiop , iel, m_idVarIop, m_ao, dof());
        }
        else {
          //! We add the coupling condition (same for Neumann and Robin-coupling
          m_elemFieldRobin[iInterface].setValue( m_seqVecs.Get("velocityInterface") , *cfeiop , iel, m_idVarIop, m_ao, dof());

          //! In the case of a porous media the above line is enough
          //! With a perfect fluid we need to subtract the old value: they are already divided by the timeStep: we are approximating the acceleration.
          if ( FelisceParam::instance().porousParam < 0 ) {
            ElementField oldVelInterface;
            oldVelInterface.initialize(DOF_FIELD,*cfeiop,1);
            oldVelInterface.setValue( m_seqVecs.Get("velocityInterfaceOld"), *cfeiop, iel, m_idVarIop, m_ao, dof());
            for ( int iLocDof(0); iLocDof<cfeiop->numDof(); ++iLocDof) {
              m_elemFieldRobin[iInterface].val(0,iLocDof) += - oldVelInterface.val( 0, iLocDof );
            }
          }
          
          //! This part differ from the Neumann coupling: the rhs contribution due to the relaxation is added
          ElementField estimatedIOP;
          estimatedIOP.initialize(DOF_FIELD,*cfeiop,1);
          estimatedIOP.setValue(m_seqVecs.Get("currentIop"),*cfeiop,iel,m_idVarIop,m_ao,dof());
          for ( int iLocDof(0); iLocDof<cfeiop->numDof(); ++iLocDof) {
            m_elemFieldRobin[iInterface].val(0,iLocDof) += m_relaxParam * estimatedIOP.val( 0, iLocDof );
          }
        } 
      }
    }
  }
  void 
  LinearProblemPerfectFluidRS::exportNormalField() {
    PetscVector tmpVec = this->dofBD(/*iBD*/0).allocateBoundaryVector(DofBoundary::parallel);
    for ( felInt iComp(0); iComp < this->dimension(); ++iComp ) {
      std::stringstream filenameBin,namevector;
      filenameBin<< "normal_"<<iComp;
      namevector << "normalVector" << iComp;
      this->dofBD(/*iBD*/0).restrictOnBoundary(this->m_seqVecs.Get(namevector.str()), tmpVec);
      if ( this->dofBD(/*iBD*/0).hasDofsOnBoundary() )
        tmpVec.saveInBinaryFormat( this->m_dofBD[0/*iBD*/].comm(), filenameBin.str().c_str(), FelisceParam::instance().resultDir );
    }
  }
}
