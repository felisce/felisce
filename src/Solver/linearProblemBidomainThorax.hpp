//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _LinearProblemBidomainThorax_HPP
#define _LinearProblemBidomainThorax_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Solver/cardiacFunction.hpp"
#include "Core/felisceParam.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "FiniteElement/elementMatrix.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce {
  /*!
   \class LinearProblemBidomainThorax
   \authors E. Schenone
   \date 05/03/2012
   \brief Manage the diffusion probleme in the thorax : useful to calculate the heart-torso (transpose) transfer matrix
   */
  class LinearProblemBidomainThorax:
    public LinearProblem {
  public:
    LinearProblemBidomainThorax();
    ~LinearProblemBidomainThorax() override;
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      m_currentLabel=label;
      IGNORE_UNUSED_FLAG_MATRIX_RHS;
    }
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void readData(IO& io) override;
    void writeSolution(int rank, std::vector<IO::Pointer>& io, double& time, int iteration) override;

    void finalizeEssBCTransientDerivedProblem() override;
    void allocateVectorBoundaryConditionDerivedLinPb() override;
    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;
    void readElectrodesNode(bool flagTranspos=true);
    void calculateRHS();
    felInt readMatch(std::vector<felInt>& matchNode, std::string matchFileName);
    void readHeartValue(std::vector<double>& scalarValue);

    virtual void calculateRes();
    inline const PetscVector&  residual() const {
      return m_thoraxRes;
    }
    inline PetscVector&  residual() {
      return m_thoraxRes;
    }
    inline const PetscVector&  robinVec() const {
      return m_robinVec;
    }
    inline PetscVector&  robinVec() {
      return m_robinVec;
    }
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& iel, FlagMatrixRHS /*flagMatrixRHS*/) override;
#ifdef FELISCE_WITH_CVGRAPH
    void initMassBoundaryForCVG();
    void massMatrixComputer(felInt ielSupportDof);
    void initPerETMass();
    void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&);
#endif
  public:
#ifdef FELISCE_WITH_CVGRAPH
    void readData() override;
    void sendData();
    void initMatrix();
    void addCurrentToRHS();
    virtual void cvgAddRobinToRHS();
#endif


    template <class Templ_functor> void evalFunctionOnElem(const Templ_functor& func, double& value, int& label);
  private:
    CurrentFiniteElement* m_fePotThorax;
    felInt m_ipotThorax;
    // PetscMatrix _RomBasisMatrix;
  protected:
    double* m_potThoraxRes;
    int m_numBcNodeThorax;
    felInt* _ECGnode;
    felInt m_numSourceNode;
    std::vector<felInt> m_thoraxMatchNode;
    std::vector<felInt> m_heartMatchNode;
    PetscVector m_thoraxRes;
    PetscVector m_robinVec;
    CurvilinearFiniteElement* m_curvFe;
  };

  template <class Templ_functor> void LinearProblemBidomainThorax::evalFunctionOnElem(const Templ_functor& func, double& value, int& label) {
    value = func(label);
  }
}

#endif
