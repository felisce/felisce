//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemPerfectFluid.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "Core/felisceTransient.hpp"
#include "Geometry/curvatures.hpp"

namespace felisce {
  

  // ======================================================================
  //                      the public methods
  // ======================================================================
  LinearProblemPerfectFluid::LinearProblemPerfectFluid():LinearProblem("Perfect fluid equation"),
                                                         m_relaxParam(0)
  {}
  /** \brief Initializer of the class.
   *
   * This function overrides LinearProblem::initialize
   * we define variable/unknowns and ids and we std::set the
   * relaxation parameter from the data file.
   *
   * \param[in] mesh a pointer to the GeometricMeshRegion mesh, to be passed to the LinearProblem::initialize()
   * \param[in] comm mpi communicator also to be passed to the mother class
   * \param[in] doUseSNES also to be passed to the mother class
   */
  void 
  LinearProblemPerfectFluid::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    IGNORE_UNUSED_ARGUMENT(fstransient);
    ///  First: mother class function is called.
    LinearProblem::initialize(mesh,comm,doUseSNES);
    ///  Then iop is added as an unknown and as a variable with one component (scalar)
    m_listUnknown.push_back(iop);
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;
    listVariable.push_back(iop);
    listNumComp.push_back(/*numOfComponents*/1);
    ///  Then the other variables possibly coming from the model are added
    this->userAddOtherVariables(listVariable,listNumComp);
    
    // we finalize the variable declaration calling this function
    this->definePhysicalVariable(listVariable,listNumComp);
    
    // we save the id variable and id unknown for the iop
    m_idVarIop = m_listVariable.getVariableIdList(iop);
    m_iUnknownIop = m_listUnknown.getUnknownIdList(iop);

    // we store the relaxation parameter for the interface boundary condition
    m_relaxParam = FelisceParam::instance().relaxationParam;
  }
  // This function initialize the petscVectors and take their structure
  // from the solution and from the sequential solution
  void 
  LinearProblemPerfectFluid::initializePetscVectors() {

    // we call the default constructor of the petscVector
    m_vecs.Init("velocityInterface");
    m_vecs.Init("velocityInterfaceOld");
    m_vecs.Init("currentIop");
    m_seqVecs.Init("velocityInterface");
    m_seqVecs.Init("velocityInterfaceOld");
    m_seqVecs.Init("currentIop");
    for ( felInt iComp(0); iComp < this->dimension(); ++iComp ) {
      std::stringstream namevector;
      namevector << "normalVector" << iComp;
      m_vecs.Init( namevector.str() );
      m_seqVecs.Init( namevector.str() );
    }

    //we take the structure from solution and std::set the value to zero
    for (  auto vecPtr = m_vecs.begin(); vecPtr != m_vecs.end(); vecPtr++) {
      vecPtr->second.duplicateFrom(this->solution());
      vecPtr->second.set(0.0);
    }

    //we take the structure from sequential solution and std::set the value to zero
    for ( auto vecPtr = m_seqVecs.begin(); vecPtr != m_seqVecs.end(); vecPtr++) {
      vecPtr->second.duplicateFrom(this->sequentialSolution());
      vecPtr->second.set(0.0);
    }
    // Why the normalVectors are std::set to -1 ?
    for ( felInt iComp(0); iComp < this->dimension(); ++iComp ) {
      std::stringstream namevector;
      namevector << "normalVector" << iComp;
      m_seqVecs.Get( namevector.str() ).set(-1);
    }
  }
  ///  Some of the vectors containings quantities at the interface are reset to
  // zero
  // TODO change the name
  void
  LinearProblemPerfectFluid::resetInterfaceQuantities() {
    ///  We are advancing in time and therfore we update the old values
    m_vecs.Get("velocityInterfaceOld").copyFrom(m_vecs.Get("velocityInterface"));
    m_seqVecs.Get("velocityInterfaceOld").copyFrom(m_seqVecs.Get("velocityInterface"));
    ///  At the same time we std::set the old ones to zero.
    m_vecs.Get("velocityInterface").set(0.0);
    m_seqVecs.Get("velocityInterface").set(0.0);
  }
  // ======================================================================
  //                   now the privates methods
  // ======================================================================

  // Function the initialize the feiop, it is called in
  // the assembly loop of the matrix in linearProblem
  void 
  LinearProblemPerfectFluid::initPerElementType(ElementType /*eltType*/, FlagMatrixRHS /*flagMatrixRHS*/) {
    felInt iIop = m_listVariable.getVariableIdList(iop);
    m_feIop=m_listCurrentFiniteElement[iIop];
  }
  // Function the compute the elementary matrix for the laplacian
  // called in the assembly loop of the matrix in linearProblem
  void 
  LinearProblemPerfectFluid::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& /*elemIdPoint*/, felInt& /*iel*/, FlagMatrixRHS flagMatrixRHS) {
    m_feIop->updateFirstDeriv(0, elemPoint);
    if ( ( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs ) || ( flagMatrixRHS == FlagMatrixRHS::only_matrix ) ) {
      m_elementMat[0]->grad_phi_i_grad_phi_j( /*coeff*/1.,*m_feIop,/*iblock*/0,/*jblock*/0,/*numOfComponents*/1);
    }
  }
}
