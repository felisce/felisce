//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _SCHAFREVISEDSOLVER_HPP
#define _SCHAFREVISEDSOLVER_HPP

// System includes

// External includes

// Project includes
#include "Solver/schafSolver.hpp"

namespace felisce {
  class SchafRevisedSolver:
    public SchafSolver {
  public:
    ///Constructor.
    SchafRevisedSolver(FelisceTransient::Pointer fstransient);
    ///Destructor.
    ~SchafRevisedSolver() override;

    ///Compute RHS in EDO.
    void computeRHS() override;
    ///Solve the EDO.
    void solveEDO() override;
    ///Computation of ionic current from EDO solution.
    void computeIon() override;
    ///Computation of function f(v) of the ODE.
    void computeTanhFunction(double* u);
    ///Computation of function a(v) of the ODE.
    void computeAFunction();
    ///Computation of function b(v) of the ODE.
    void computeBFunction();

  private:
    double* m_fTanh;
    double* m_fA;
    double* m_fB;
  };
}

#endif
