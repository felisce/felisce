//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __LINEARPROBLEMPOROELASTICITY_HPP__
#define __LINEARPROBLEMPOROELASTICITY_HPP__

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "FiniteElement/elementField.hpp"

// Linear Problem for poroelasticity system.
namespace felisce {

  class LinearProblemPoroElasticity:
    public LinearProblem {
  public:
    LinearProblemPoroElasticity();
    ~LinearProblemPoroElasticity() override= default;;
    virtual void advanceInTime();
    
    virtual void userPostProcessing(){};
    virtual void userInitialize(){};
    void exportInitialFiltrationVelocity(std::vector<IO::Pointer>& io);
    void exportFiltrationVelocity(std::vector<IO::Pointer>& io);
 
  protected:
    felInt m_iDisplacement;  // Solid displacement
    felInt m_iPressure;      // Interstitial pressure (fluid pressure)
    
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    virtual void updateAnisotropicDirection(const std::vector<Point*>& elemPoint);
    CurrentFiniteElement* m_feDisp;
    CurrentFiniteElement* m_fePre;
    
    ElementField m_elemFieldPressure, m_elemFieldDisplacement;
    /// Material properties
    double m_mu,m_lambda,m_M,m_b,m_k0,m_k1; //[M] = [Pascal], [b] = [], [k0]=[k1]=[ m^3 kg^-1 s]=[m^2 Pa^-1 s^-1]
  private:
    void displayPoroElasticProperties() const;
  protected:
    std::vector<double> m_anisotropicDir;
    double m_deltaT;
    
    virtual double userEvaluatePorosity(double /*x*/,double /*y*/,double /*z*/) {
      // Just a default value!
      return 0.1;
    };
    double m_pInit;
  protected:
    // Members and method for Filtration velocity computation
    bool m_matricesForFiltrationVelocityBuilt;
    void assembleMatricesForFiltrationVelocity();
    
    #ifdef FELISCE_WITH_CVGRAPH
    void assembleMassBoundaryAndInitKSP( std::size_t iBD = 0 ) override;

    void massMatrixComputer(felInt ielSupportDof);
    void initPerETMass();
    void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&);
    #endif
    CurvilinearFiniteElement* m_curvFeDisp;
  private:
    void computeFiltrationVelocity();
    
    PetscMatrix m_mass;
    PetscMatrix m_derivative;
  protected:
    KSPInterface::Pointer m_kspForFiltrationVelocity = felisce::make_shared<KSPInterface>();;
    PC m_pcForFiltrationVelocity;
  };
}
#endif
