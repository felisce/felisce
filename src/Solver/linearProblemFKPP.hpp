//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _LINEARPROBLEMFKPP_HPP
#define _LINEARPROBLEMFKPP_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
  /*!
   \class LinearProblemFKPP
   \author D.Lombardi
   \date 17/09/2013
   \brief FKPP
   */
  class LinearProblemFKPP:
    public LinearProblem {
  public:
    LinearProblemFKPP();
    ~LinearProblemFKPP() override;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerDomain(int label, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override {
      m_currentLabel=label;
      IGNORE_UNUSED_FLAG_MATRIX_RHS;
    }
    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

  protected:
    CurrentFiniteElement* m_feTemp;
    felInt m_iTemperature;
  private:
    ElementField m_elemField;
  };
}

#endif
