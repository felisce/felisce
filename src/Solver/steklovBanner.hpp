//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __STEKLOVBANNER_HPP__
#define __STEKLOVBANNER_HPP__

// System includes

// External includes

// Project includes

namespace felisce {
  class steklovBanner {
  public:
    steklovBanner(int numLocDof, int rankProc, MPI_Comm comm, int dimRoSteklov = -1 );
    void initComputation();
    void finalizeComputation();
    void operator++();
  private:
    felInt m_numLoc;
    felInt m_numGlob;
    felInt m_rankProc;
    MPI_Comm m_comm;
    felInt m_counter;
    double m_step;
    felInt m_counterStep;
    felInt m_NMax;
    felInt m_dimRoSteklov;
  };
}
#endif
