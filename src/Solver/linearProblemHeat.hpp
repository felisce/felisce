//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:     J. Foulon & J-F. Gerbeau & V. Martin
//

/*!
 \file linearProblemHeat.hpp
 \authors J. Foulon & J-F. Gerbeau & V. Martin
 \date 05/01/2011
 \brief File where is defined specificity to solve Heat problem.
 */

#ifndef _LINEARPROBLEMHEAT_HPP
#define _LINEARPROBLEMHEAT_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
  /*!
   \class LinearProblemHeat
   \authors J. Foulon & J-F. Gerbeau & V. Martin
   \date 05/01/2011
   \brief Manage specific functions for laplacian problem.
   */
  class LinearProblemHeat:
    public LinearProblem {
  public:
    LinearProblemHeat();
    ~LinearProblemHeat() override= default;;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

  private:
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) override;

    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) override;
  public:
    void readDataDisplacement(std::vector<IO::Pointer>& io, double iteration);
    void updateOldQuantities();

    inline std::vector<double> & Displacement() { return m_vectorDisp; }

  protected:
    CurrentFiniteElement* m_feTemp;
    CurvilinearFiniteElement* m_curvFeTemp;
    felInt m_iTemperature;
    ElementField m_sourceTerm;
  private:
    ElementField m_elemField;
    ElementFieldDynamicValue *m_sourceTermDynamicValue;
    std::vector<double> m_vectorDisp;

#ifdef FELISCE_WITH_CVGRAPH
  public:
    void sendData();
    void readData() override;
    ElementField m_robinAux;
  private:
    void cvgraphNaturalBC(felInt iel) override;
    void initPerETMass();
    void updateFE(const std::vector<Point*>& elemPoint, const std::vector<int>&);
    void massMatrixComputer(felInt ielSupportDof);
    void assembleMassBoundaryAndInitKSP( std::size_t iConn ) override;
#endif
  };
}

#endif
