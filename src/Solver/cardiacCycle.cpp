//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Smaldone
//

// System includes

// External includes

// Project includes
#include "Solver/cardiacCycle.hpp"

namespace felisce 
{
  CardiacCycle::CardiacCycle(FelisceTransient::Pointer fstransient,
                             RISModel* risModel, std::vector<LinearProblem*> linearProblem):
    m_fstransient(fstransient),
    m_ris(risModel),
    m_linearProblem(linearProblem) {

    if (! FelisceParam::instance().RISModels) {
      FEL_ERROR("Valve model not initialized");
    }

    m_inflow.resize(3,0.);
    _CardiacCycleLabel.resize(1,0);
    m_closed_surf.resize(FelisceParam::instance().closed_surf.size(),0);
    m_p0_isovol_relax.resize(1, 0.);
    m_flag_valve_was_open = false;

    m_dt = m_fstransient->timeStep ;
    _CardiacCycleLabel[0] = FelisceParam::instance().CardiacCycleLabel;
    m_duration_ejection = FelisceParam::instance(). duration_ejection;
    m_duration_isovol_relax = FelisceParam::instance().duration_isovol_relax;
    m_duration_filling = FelisceParam::instance().duration_filling;
    m_duration_isovol_contract = FelisceParam::instance().duration_isovol_contract;
    m_t0_ejection = 0.;

    FEL_ASSERT(FelisceParam::instance().closed_surf.size() > 1);
    for(std::size_t ic = 0; ic < FelisceParam::instance().closed_surf.size()/2; ic++) {
      m_closed_surf[2*ic] = FelisceParam::instance().closed_surf[2*ic];
      m_closed_surf[2*ic+1] = FelisceParam::instance().closed_surf[2*ic+1];
    }

  }

  


  void CardiacCycle::ApplyCardiacCycleInput() {
    InflowCondition();
    PressureInput();
  }


  void CardiacCycle::InflowCondition() {

    double tps = 0.;
    tps = m_fstransient->time;

    if(m_ris->valve_is_closed() == 1) { // Valve is closed: Neumann BC.
      // the last time instant when the valve was closed:
      m_t0_ejection = tps;
    }

    if(m_ris->valve_is_closed() == 0 ) { // Valve is open

      PetscPrintf(PETSC_COMM_WORLD,"####### SYSTOLE\n");
      PetscPrintf(PETSC_COMM_WORLD, "******* Cardiac phase: Ejection\n\n" );

      // the inlet is supposed to be oriented along y-axis : to change ! when some one will have more time than me. Sv
      m_inflow[1] = 110* std::sin( (tps - m_t0_ejection) * M_PI / m_duration_ejection ) ;
      m_penalisationParam = 10e30;
    }
  }


  void CardiacCycle::PressureInput() {

    std::vector<double> pres_distal_valve(m_closed_surf.size(), 0.);
    double tphase = 0.;
    m_pressure_in.resize(1, 0.);

    if(m_ris->valve_is_closed() == 0) { // Valve is open: Dirichlet BC.
      m_flag_valve_was_open = true; // to remember that the valve was open
      m_t0_isovol_relax = m_fstransient->time; //t0_isovol_relax will remember the last time instant when the valve was open
    }

    if (m_ris->valve_is_closed() == 1 ) { // Valve is closed
      m_penalisationParam = 0.;
      tphase = m_fstransient->time - m_t0_isovol_relax;

      if(m_flag_valve_was_open) {
        // we enter here only the first time step after the valve closes
        // the inlet mean pressure at the first time instant the valve close
        m_linearProblem[0]->computeMeanQuantity(pressure, _CardiacCycleLabel, m_p0_isovol_relax);
        m_flag_valve_was_open = false;
      }

      if(tphase < m_duration_isovol_relax +m_dt ) {
        PetscPrintf(PETSC_COMM_WORLD, "####### DIASTOLE \n");
        PetscPrintf(PETSC_COMM_WORLD, "******* Cardiac phase: Isovolumetric relaxation\n\n");
        m_pressure_in[0] = m_p0_isovol_relax[0] * exp( -tphase / (m_duration_isovol_relax/4.) );
      }

      if (tphase >= (m_duration_isovol_relax + m_dt) && tphase < (m_duration_isovol_relax + m_duration_filling +m_dt)) {
        PetscPrintf(PETSC_COMM_WORLD, "####### DIASTOLE \n");
        PetscPrintf(PETSC_COMM_WORLD, "******* Cardiac phase: Filling\n\n");
      }

      if(tphase > (m_duration_isovol_relax + m_duration_filling)) {
        PetscPrintf(PETSC_COMM_WORLD, "####### SYSTOLE \n");
        PetscPrintf(PETSC_COMM_WORLD, "******* Cardiac phase: Isovolumetric contraction\n\n");

        m_linearProblem[0]->computeMeanQuantity(pressure, m_closed_surf, pres_distal_valve);
        m_pressure_in[0] = 1.2 * pres_distal_valve[1] * std::pow( tphase - (m_duration_isovol_relax+m_duration_filling), 6) / std::pow(m_duration_isovol_contract, 6) + pres_distal_valve[0] ;
      }

      // PetscPrintf(PETSC_COMM_WORLD,"******* Inlet BC : pressure\n");
      //  PetscPrintf(PETSC_COMM_WORLD, "     p_in = %e\n", m_pressure_in[0]);

    }

  }


}

