//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNSFracStepProj.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce {
  LinearProblemNSFracStepProj::LinearProblemNSFracStepProj():
    LinearProblem("Navier-Stokes Fractional Step: Projection"),
    m_viscosity(0.),
    m_density(0.),
    m_lumpedModelBC(nullptr),
    m_buildTeporaryMatrix(false)
  {}

  LinearProblemNSFracStepProj::~LinearProblemNSFracStepProj() {
    if(m_buildTeporaryMatrix)
      m_matrix.destroy();
  }

  void LinearProblemNSFracStepProj::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblem::initialize(mesh,comm, doUseSNES);
    m_fstransient = fstransient;
    std::vector<PhysicalVariable> listVariable;
    std::vector<std::size_t> listNumComp;
      
    m_explicitAdvection = FelisceParam::instance().explicitAdvection;
    if (m_explicitAdvection) {
      listVariable.push_back(velocityAdvection);
      listNumComp.push_back(this->dimension());
    }
    listVariable.push_back(velocity);
    listNumComp.push_back(this->dimension());
    listVariable.push_back(pressure);
    listNumComp.push_back(1);

    if(FelisceParam::instance().useALEformulation) {
      listVariable.push_back(displacement);
      listNumComp.push_back(this->dimension());
    }

    //define unknown of the linear system.
    m_listUnknown.push_back(pressure);
    definePhysicalVariable(listVariable,listNumComp);
    m_viscosity = FelisceParam::instance().viscosity;
    m_density = FelisceParam::instance().density;
  }

  void LinearProblemNSFracStepProj::initializeLumpedModelBC(LumpedModelBC* lumpedModelBC) {
    m_lumpedModelBC = lumpedModelBC;
  }

  void LinearProblemNSFracStepProj::initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ELT_TYPE;
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_iVelocity = m_listVariable.getVariableIdList(velocity);
    m_iPressure = m_listVariable.getVariableIdList(pressure);
    m_feVel = m_listCurrentFiniteElement[m_iVelocity];
    m_fePres = m_listCurrentFiniteElement[m_iPressure];
    m_velocity = &m_listVariable[m_iVelocity];
    m_pressure = &m_listVariable[m_iPressure];

    m_elemFieldAdv.initialize(DOF_FIELD,*m_feVel,this->dimension());
    m_elemFieldRHS.initialize(DOF_FIELD,*m_feVel,this->dimension());
    m_elemFieldDOF.initialize(DOF_FIELD,*m_feVel,this->dimension());
    m_elemFieldPress.initialize(DOF_FIELD,*m_fePres,1);
  }

  void LinearProblemNSFracStepProj::computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    IGNORE_UNUSED_IEL;
    IGNORE_UNUSED_ELEM_ID_POINT;

    m_feVel->updateFirstDeriv(0, elemPoint);
    m_fePres->updateFirstDeriv(0, elemPoint);
    double coef = m_fstransient->timeStep/m_density;



    if ( m_feVel->measure() < 0. ) {
      int iglo;
      ISLocalToGlobalMappingApply(this->m_mappingElem[m_currentMesh],1,&iel,&iglo);
      std::cout << " iglo = " << iglo << std::endl;
      FEL_ERROR("Error: Element of negative measure.");
    }


    //================================
    //            matrix
    //================================
    if ( (m_fstransient->iteration == 0 &&  FelisceParam::instance().useALEformulation == 0 ) ||      
	       (m_fstransient->iteration > 0  &&  FelisceParam::instance().useALEformulation > 0 ) ) {
      if( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix ) 
	      // - \int \frac{dt}{rhof} \nabla p^{n} \nabla q
	      m_elementMat[0]->grad_phi_i_grad_phi_j(coef,*m_fePres,0,0,1);
    }
 
    if ( m_fstransient->iteration > 0 ) {
      //SUPG/PSPG Stabilization 
      if( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix ) {
	      m_elemFieldAdv.setValue(externalVec(1), *m_feVel, iel, m_iVelocity, m_externalAO[0], *m_externalDof[0]);
        m_elementMat[0]->stab_supgProjCT(m_fstransient->timeStep, FelisceParam::instance().stabSUPG, m_density,  m_viscosity, *m_fePres, m_elemFieldAdv);
      }
    }

    //================================
    //              RHS
    //================================
    if  (m_fstransient->iteration > 0) {
      if( flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_rhs ) {
	      // - \int div \tilde{u}^n
	      m_elemFieldDOF.setValue(externalVec(0), *m_feVel, iel, m_iVelocity, m_externalAO[0], *m_externalDof[0]);
	      assert(!m_elementVector.empty());
	      m_elementVector[0]->div_f_phi_i(-1.,*m_feVel,*m_fePres,m_elemFieldDOF,0);
           
	      // SUPG/PSPG Stabilization 
	      m_elemFieldAdv.setValue(externalVec(1), *m_feVel, iel, m_iVelocity, m_externalAO[0], *m_externalDof[0]);
	      m_elemFieldPress.setValue(this->sequentialSolution(), *m_fePres, iel, m_iPressure, m_ao, dof());
	      m_elementVector[0]->stab_supgProjCT(m_fstransient->timeStep, FelisceParam::instance().stabSUPG, m_density, m_viscosity, *m_fePres,  m_elemFieldAdv,  m_elemFieldDOF, m_elemFieldPress, m_elemFieldRHS);
      }	
    }
  } 
  

  void LinearProblemNSFracStepProj::finalizeEssBCTransientDerivedProblem() {
    if (m_lumpedModelBC) {
      if(FelisceParam::instance().model == "NSFracStep") {
        BoundaryCondition* bc;
        std::vector<double> PlumpedModelBC;
        for(std::size_t iLumpedModelBC=0; iLumpedModelBC<m_lumpedModelBC->size(); iLumpedModelBC++) {
          if(FelisceParam::instance().lumpedModelBCAlgo[iLumpedModelBC] == 1) {
            // get the boundary condition we want to std::set the value
            bc = getBoundaryConditionList().EssentialLumpedModelBC(iLumpedModelBC);
	    if ( m_linearizedFlag ) 
	      PlumpedModelBC.push_back(0.0);
	    else {
	      if ( FelisceParam::instance().orderPressureExtrapolation > 0 )
		PlumpedModelBC.push_back(this->m_lumpedModelBC->Pp(iLumpedModelBC)-this->m_lumpedModelBC->PpOld(iLumpedModelBC));
	      else
		PlumpedModelBC.push_back(this->m_lumpedModelBC->Pp(iLumpedModelBC));
	    }

            if (this->verbosity()>2) {
              std::cout << "LumpedModelBC :" << std::endl;
              std::cout << "Label = " << m_currentLabel << std::endl;
              std::cout << "Applied PlumpedModelBC = " <<  PlumpedModelBC[0] << std::endl;
            }

            // std::set boundary condition
            bc->setValue(PlumpedModelBC);
            PlumpedModelBC.clear();
          }
        }
      }
    }
  }

  void LinearProblemNSFracStepProj::applyEssentialBoundaryConditionDerivedProblem(int rank, FlagMatrixRHS flagMatrixRHS) {
    if (m_lumpedModelBC) {
      if(FelisceParam::instance().model == "NSFracStep") {
        BoundaryCondition* BC;
        felInt ia;
        std::unordered_set<felInt> idDofBC;
        std::unordered_map<int, double> idBCAndValue;
        std::unordered_set<felInt> idDofOfAllEssLumpedModelBC;

        // non symmetric pseudo-elimination, cf. LinearProblem::applyEssentialBoundaryConditionHelper<DirichletApplicationOptions::nonSymmetricPseudoElimination>
        for (std::size_t i=0; i < m_boundaryConditionList.numEssentialLumpedModelBoundaryCondition(); i++) {
          if(FelisceParam::instance().lumpedModelBCAlgo[i] == 1) {
            BC = m_boundaryConditionList.EssentialLumpedModelBC(i);
            idBCAndValue.clear();
            idDofBC.clear();
            getListDofOfBC(*BC, idDofBC, idBCAndValue);
            for(auto itDofBC = idDofBC.begin(); itDofBC != idDofBC.end(); itDofBC++) {
              idDofOfAllEssLumpedModelBC.insert(*itDofBC);
              ia = *itDofBC;
              if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_rhs))
                vector().setValue( ia, idBCAndValue[*itDofBC], INSERT_VALUES);
            }
          }
        }

        // get the number of dof in each process for all EssentialLumpedModel BC
        int numProc;
        MPI_Comm_size(MpiInfo::petscComm(), &numProc);
        int *sizeTmp = new int [numProc];
        int *sizeDofBC = new int [numProc];
        for (int rankProc = 0; rankProc < numProc; rankProc++) {
          sizeTmp[rankProc] = 0;
        }
        sizeTmp[rank] = idDofOfAllEssLumpedModelBC.size();
        MPI_Allreduce(&sizeTmp[0], &sizeDofBC[0], numProc, MPI_INT, MPI_SUM, MpiInfo::petscComm());

        int startDof = 0;
        for (int rankProc = 0; rankProc < rank; rankProc++) {
          startDof += sizeDofBC[rankProc];
        }
        // get the number of dof in all processes = sum of sizeDofBC[] (some dof are counted several times)
        int numDofBCTotal = 0;
        for (int rankProc = 0; rankProc < numProc; rankProc++) {
          numDofBCTotal += sizeDofBC[rankProc];
        }

        // put all the id dof in an array (some dof are counted several times)
        int *dofBCTmp = new int [numDofBCTotal];
        int *dofBCArray = new int [numDofBCTotal];
        for (int iDof = 0; iDof < numDofBCTotal; iDof++) {
          dofBCTmp[iDof] = 0;
        }
        int iDof = 0;
        for(auto itDofBC = idDofOfAllEssLumpedModelBC.begin(); itDofBC != idDofOfAllEssLumpedModelBC.end(); ++itDofBC) {
          dofBCTmp[startDof + iDof] = *itDofBC;
          iDof++;
        }
        MPI_Allreduce(&dofBCTmp[0], &dofBCArray[0], numDofBCTotal, MPI_INT, MPI_SUM, MpiInfo::petscComm());

        // rearrange the id dof in the increasing order (each dof is counted only once)
        idDofOfAllEssLumpedModelBC.clear();
        for (iDof = 0; iDof < numDofBCTotal; iDof++) {
          idDofOfAllEssLumpedModelBC.insert(dofBCArray[iDof]);
        }
        numDofBCTotal = idDofOfAllEssLumpedModelBC.size();
        iDof = 0;
        for(auto itDofBC = idDofOfAllEssLumpedModelBC.begin(); itDofBC != idDofOfAllEssLumpedModelBC.end(); ++itDofBC) {
          dofBCArray[iDof] = *itDofBC;
          iDof++;
        }

        if( (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs) || (flagMatrixRHS == FlagMatrixRHS::only_matrix)) {
          matrix(0).setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
          matrix(0).zeroRows(numDofBCTotal, &dofBCArray[0], 1.);
        }

        delete [] sizeTmp;
        delete [] sizeDofBC;
        delete [] dofBCTmp;
        delete [] dofBCArray;
      }
    }
  }

  void LinearProblemNSFracStepProj::copyMatrixRHS() {
    m_matrix.duplicateFrom(matrix(0),MAT_COPY_VALUES);
    m_matrix.assembly(MAT_FINAL_ASSEMBLY);
    m_buildTeporaryMatrix = true;
  }

  void LinearProblemNSFracStepProj::addMatrixRHS() {
    matrix(0).axpy(1,m_matrix,SAME_NONZERO_PATTERN);
  }

  //Embedded interface  
  void LinearProblemNSFracStepProj::userAssembleMatrixRHSEmbeddedInterface() {}

}
