//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges and M. Fernandez
//

#ifndef _LinearProblemFSINitscheXFEMFluid_HPP
#define _LinearProblemFSINitscheXFEMFluid_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"
#include "FiniteElement/elementField.hpp"
#include "DegreeOfFreedom/duplicateSupportDof.hpp"
#include "Core/felisceTransient.hpp"

namespace felisce 
{  
  /*!
    \brief Monolithic algorithm for fsi with fictitious domain and Nitsche's formulation
  */
  class LinearProblemFSINitscheXFEMFluid:
    public LinearProblem {
  public:

    // Constructor
    LinearProblemFSINitscheXFEMFluid();

    // Destructor
    ~LinearProblemFSINitscheXFEMFluid() override;

    // Initialize the linar problem (parameters, unknowns, ...)
    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

    // add solid variable 
    void addSolidVariable();

    // Duplicate and initialize the support dof
    void initSupportDofDerivedProblem() override;

    // Initialize the bdf scheme (pointer to the one in the model)
    void initializeTimeScheme(Bdf* bdf) override {
      m_bdf = bdf;
    }

    // For Dirichlet BC changing at each time step
    void finalizeEssBCTransientDerivedProblem() override;

    // Get the current finite elements and initialize elemFields for the problem
    void initPerElementType(ElementType eltType, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;


    void computeElementArray(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel1, felInt& iel2, ElementType& eltType, felInt& ielGeo, FlagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    void assembleMatrixFrontPoints();
    void assembleMatrixGhostPenalty();
    void assembleMatrixFaceOriented();


    // LumpedModelBC: NS model with implicit implementation
    void userChangePattern(int numProc, int rankProc) override;

    // gather bdf->vector() to m_seqBdfRHS
    void gatherSeqBdfRHS(std::vector<PetscVector>& parallelVec);

    // gather a std::vector to m_seqVelExtrapol
    void gatherSeqVelExtrapol(std::vector<PetscVector>& parallelVec);

    // get the sequential std::vector of the extrapolation of the velocity (used in the structure problem with
    // stabilized explicit methods)
    PetscVector& getVelExtrapol(felInt n);

    // add m_Matrix[1] to the current matrix (m_Matrix[0])
    void addMatrixRHS() override;

    // get the reference to the object in the model duplicating the support dofs
    void setDuplicateSupportObject(DuplicateSupportDof* object);

    // get the pointer to the curvilinear finite element of the structure
    void setStrucFiniteElement(CurvilinearFiniteElement* strucFE);

    // get a pointer to the structure linear problem
    void setStrucLinPrb(LinearProblem* pLinPrb);


    // Dynamics, copy original <-> current
    void initOriginalBdf(felInt timeScheme, felInt extrapolOrder);
    void copyOriginalBdfToCurrentBdf(felInt timeScheme, felInt extrapolOrder);
    void copyOriginalSolToCurrentSol();
    void copyCurrentSolToOriginalSol();
    void deleteDynamicData();
    void copyOriginalToCurrent(PetscVector& vecSeqOriginal, PetscVector& vecSeqCurrent, PetscVector& vecParCurrent);

    void initOldObjects(felInt stabExplicitExtrapolOrder);
    void updateOldSolution(felInt countInitOld);

    const PetscVector& solutionOld(felInt n) const { return m_solutionOld[n]; }
    const PetscVector& sequentialSolutionOld(felInt n) const { return m_sequentialSolutionOld[n]; }
    AO const & aoOld(felInt n) const { return m_aoOld[n]; }
    Dof const & dofOld(felInt n) const { return m_dofOld[n]; }
    SupportDofMesh const & supportDofUnknownOld(felInt n, felInt i) const { return m_supportDofUnknownOld[n][i]; }

    // virtual function of LinearProblem
    void initPerElementTypeBoundaryCondition(ElementType& eltType, FlagMatrixRHS flagMatrixRHS) override;
    void computeElementArrayBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& ielSupportDof, FlagMatrixRHS flagMatrixRHS) override;


    void setUseCurrentFluidSolution(bool useCurFluidSol) { m_useCurrentFluidSolution = useCurFluidSol;}

    // Manage the extrapolations in the Robin - Neumann schemes
    void gatherRNFluidExtrapol(std::vector<PetscVector>& parFluidExtrapol);
    void gatherRNFluidExtrapol(PetscVector& parFluidExtrapol);
    void setRNFluidExtrapol(PetscVector& seqFluidExtrapol);
    void initRNFluidExtrapol(felInt order);

  protected:
    CurrentFiniteElement* m_feVel;                       // volume velocity finite element
    CurrentFiniteElement* m_fePres;                      // volume pressure finite element
    CurrentFiniteElementWithBd* m_feVelWithBd;           // volume finite element with edges
    CurrentFiniteElementWithBd* m_fePreWithBd;           // volume finite element with edges
    CurvilinearFiniteElement *m_curvFeVel;               // boundary velocity finite element
    CurvilinearFiniteElement *m_curvFePress;             // boundary pressure finite element
    CurvilinearFiniteElement *m_feStruc;                 // finite element for the structure
    Variable* m_velocity;                                // velocity
    Variable* m_pressure;                                // pressure
    felInt m_iVelocity;                                  // id of the velocity
    felInt m_iPressure;                                  // id of the pressure
    double m_viscosity;                                  // dynamic viscosity of the fluid
    double m_density;                                    // density of the fluid
    bool m_useSymmetricStress;                           // expression of the Laplacian (epsilon(u) or grad(u))
    std::vector<PetscVector> m_seqBdfRHS;                // bdf part going to the rhs
    std::vector<PetscVector> m_seqVelExtrapol;           // extrapolation of the velocity
    ElementField m_sourceTerm;

  private:
    void computeElementMatrixRHS();
    void computeElementMatrixRHSPartDependingOnOldTimeStep(felInt iel, felInt ielOld, felInt idTimeStep);
    void computeNitscheSigma_u_v(double velCoeff, double preCoeff);
    void computeNitscheSigma_v_u(double velCoeff, double preCoeff);
    void computeNitscheSigma_u_Sigma_v(double coeff);
    void computeNitscheSigma_uold_Sigma_v(double coeff);
    void computeNitscheDpoint_Sigma_v(double velCoeff, double preCoeff);
    void computeStenbergSigma_u_Sigma_v(double velCoeff, double preCoeff); 
    void updateStructureVel(felInt strucId, PetscVector& strucVel);
    void updateSubStructureVel(const std::vector<Point*>& ptElem, const std::vector<Point*>& ptSubElem);


    ElementField m_elemFieldAdv;                         // the extrapolated velocity
    ElementField m_elemFieldAdvDup;                         // the extrapolated velocity on the duplicate elt
    ElementField m_elemFieldAdvTmp;
    ElementField m_elemFieldRHS;                         // rhs use for supg stabilization (not use at the moment)
    ElementField m_elemFieldRHSbdf;                      // the bdf part that goes in the rhs
    ElementField m_elemFieldTotalPressureFormulation;    // old velocity for total pressure formulation

    Bdf* m_bdf;                                          // Bdf scheme, initialized in initializedTimeScheme
    std::vector<ElementField> m_seqVelDofPts;
    std::vector<ElementField> m_seqPreDofPts;
    ElementField m_elemFieldNormal;
    ElementField m_elemFieldNormalInit;
    ElementField m_tmpSourceTerm;
    ElementField m_strucVelDofPts;
    ElementField m_strucVelQuadPts;
    ElementField m_ddotComp;
    ElementField m_elemFieldAux;

    bool m_isSeqBdfRHSAllocated;                         // to know if m_seqBdfRHS has been allocated
    bool m_isSeqVelExtrapolAllocated;                    // to know if m_solExtrapol has been allocated

    DuplicateSupportDof *m_duplicateSupportElements;     // manage the duplication of the support dofs

    LinearProblem* m_pLinStruc;       // structure linear problem

    ElementFieldDynamicValue *m_elemFieldTimeSchemeType;
    felInt m_timeSchemeType;

    std::vector<Point*> m_mshPts;
    std::vector<Point*> m_itfPts;
    std::vector<Point*> m_subItfPts;

    std::size_t m_numVerPerFluidElt;
    std::size_t m_numVerPerSolidElt;

    felInt m_useCurrentFluidSolution;

    bool m_isSeqRNFluidExtrapolAllocated;
    std::vector<PetscVector> m_seqRNFluidExtrapol;

    // if one choose to do a projection when the intersection is changing
    felInt m_numOriginalDof;
    Bdf m_seqOriginalSolutions;
    PetscVector m_tmpSeqOriginalSol;
    bool m_isSeqOriginalSolAllocated;

    // if one choose to NOT do a projection when the intersection is changing
    std::vector<Dof> m_dofOld;
    std::vector<AO> m_aoOld;
    std::vector<PetscVector> m_solutionOld;
    std::vector<PetscVector> m_sequentialSolutionOld;
    std::vector< std::vector<SupportDofMesh> > m_supportDofUnknownOld;

    //Robin - Neumann extrapolation coefficients
    std::vector<double> m_betaRN;
  };
}

#endif
