//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Boulakia
//

#ifndef _MVSOLVER_HPP
#define _MVSOLVER_HPP

// System includes

// External includes

// Project includes
#include "Solver/ionicSolver.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  class MVSolver:
    public IonicSolver {
  public:
    ///Constructor.
    MVSolver(FelisceTransient::Pointer fstransient);
    ///Destructor.
    ~MVSolver() override;

    ///Compute RHS in EDO.
    void computeRHS() override;
    ///Solve the EDO.
    void solveEDO() override;
    ///Computation of ionic current from EDO solution.
    void computeIon() override;


    ///For Verdandi, if assimilation or not.
    void setAssimilation(bool);

    ///For Verdandi, std::set parameters.
    void setParameters(std::vector<double>);

    ///For Verdandi, get parameters.
    void getParameters(std::vector<double>&);

    ///For Verdandi, get EDOs.
    void getEDOvalues(std::vector<double>&,std::vector<double>&,std::vector<double>&);
    void getEDOpvalues(std::vector<double>&,std::vector<double>&,std::vector<double>&);

    ///For Verdandi, std::set EDOs.
    void setEDOvalues(std::vector<double>,std::vector<double>,std::vector<double>);

    ///For Verdandi, std::set posparam.
    void setPosParam(std::vector<int>);

    ///For Verdandi, get posparam.
    void getPosParam(std::vector<int>&);

    ///For Verdandi, Vm.
    void getVmvalues(std::vector<double>&);
    void setVmvalues(std::vector<double>);
    void getVmpvalues(std::vector<double>&);

    // Infarct
    virtual inline const HeteroTauOut& fctTauOut() const {
      return m_heteroTauOut;
    }
    virtual inline HeteroTauOut& fctTauOut() {
      return m_heteroTauOut;
    }
    inline const std::vector<double>& tauOut() const {
      return m_tauOut;
    }
    inline std::vector<double>& tauOut() {
      return m_tauOut;
    }

    // functor and acces to variable properties fhn

    inline const std::vector<int>& cellType() const {
      return m_cellType;
    }
    inline std::vector<int>& cellType() {
      return m_cellType;
    }

    inline const HeteroMVCoeff& MVCoeff() const {
      return m_heteroCoeff;
    }
    inline HeteroMVCoeff& MVCoeff() {
      return m_heteroCoeff;
    }

  protected:
    HeteroMVCoeff m_heteroCoeff;
    std::vector<int> m_cellType;
    ///To simulate infarction.
    HeteroTauOut m_heteroTauOut;
    std::vector<double> m_tauOut;
  private:
    std::vector<double> m_uo;       // u_o
    std::vector<double> m_uu;       // u_u
    std::vector<double> m_thetav;   // theta_v
    std::vector<double> m_thetaw;   // theta_w
    std::vector<double> m_thetavm;  // theta_v^-
    std::vector<double> m_thetao;   // theta_o
    std::vector<double> m_tauvm1;   // tau_v1^-
    std::vector<double> m_tauvm2;   // tau_v2^-
    std::vector<double> m_tauvp;    // tau_v^+
    std::vector<double> m_tauwm1;   // tau_w1^-
    std::vector<double> m_tauwm2;   // tau_w2^+
    std::vector<double> m_kwm;	    // k_w^-
    std::vector<double> m_uwm;      // u_w^-
    std::vector<double> m_tauwm;    // tau_w^+
    std::vector<double> m_taufi;    // tau_fi
    std::vector<double> m_tauo1;    // tau_o1
    std::vector<double> m_tauo2;    // tau_o2
    std::vector<double> m_tauso1;   // tau_so1
    std::vector<double> m_tauso2;   // tau_so2
    std::vector<double> m_kso;      // k_so
    std::vector<double> m_uso;      // u_so
    std::vector<double> m_taus1;    // tau_s1
    std::vector<double> m_taus2;    // tau_s2
    std::vector<double> m_ks;       // k_s
    std::vector<double> m_us;       // u_s
    std::vector<double> m_tausi;    // tau_si
    std::vector<double> m_tauwinf;  // tau_{w inf}
    std::vector<double> m_winfstar; // w^*_{inf}
    std::vector<double> m_gfi;      // g_{fi}
    std::vector<double> m_gso;      // g_{so}
    std::vector<double> m_gsi;      // g_{si}

    /// Verdandi variables
    std::vector<int> m_posparam;
    bool m_assimilation; //if assimilation or not
    std::vector<double> m_vEDO,m_wEDO,m_sEDO;
    std::vector<double> p_vEDO,p_wEDO,p_sEDO;
    std::vector<double> m_Vm,p_Vm;



  };
}

#endif
