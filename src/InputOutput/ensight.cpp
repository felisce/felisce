//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

/*!
 \file ensight.cpp
 \authors J.Castelneau & J.Foulon
 \date 04/05/2010
 \brief File containing implementation of object to use Ensight software
 */

// System includes

// External includes

// Project includes
#include "InputOutput/ensight.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Core/util_string.hpp"
#include "Core/felisceTools.hpp"

namespace felisce
{
// Ensight Class
//==============
Ensight::StringToEns6Pair_type Ensight::m_eltFelNameToEns6Pair;
Ensight::StringToElementType_type Ensight::m_eltEns6NameToEnum;

/*! Initialisation of static std::unordered_map useful to manage mesh name */
void Ensight::initMap()
{
  //std::unordered_map for ensight6 mesh:
  //felisce type name -> < enum, ensight6 name >
  m_eltFelNameToEns6Pair["Nodes"]        = std::make_pair( GeometricMeshRegion::Nod,    "point"     );
  m_eltFelNameToEns6Pair["Segments2"]    = std::make_pair( GeometricMeshRegion::Seg2,   "bar2"      );
  m_eltFelNameToEns6Pair["Segments3b"]   = std::make_pair( GeometricMeshRegion::Seg3b,  "null1"     );
  m_eltFelNameToEns6Pair["Segments3"]    = std::make_pair( GeometricMeshRegion::Seg3,   "bar3"      );
  m_eltFelNameToEns6Pair["Triangles3"]   = std::make_pair( GeometricMeshRegion::Tria3,  "tria3"     );
  m_eltFelNameToEns6Pair["Triangles4"]   = std::make_pair( GeometricMeshRegion::Tria4,  "null2"     );
  m_eltFelNameToEns6Pair["Triangles6"]   = std::make_pair( GeometricMeshRegion::Tria6,  "tria6"     );
  m_eltFelNameToEns6Pair["Quadrangles4"] = std::make_pair( GeometricMeshRegion::Quad4,  "quad4"     );
  m_eltFelNameToEns6Pair["Quadrangles5"] = std::make_pair( GeometricMeshRegion::Quad5,  "null3"     );
  m_eltFelNameToEns6Pair["Quadrangles6"] = std::make_pair( GeometricMeshRegion::Quad6,  "null4"     );
  m_eltFelNameToEns6Pair["Quadrangles8"] = std::make_pair( GeometricMeshRegion::Quad8,  "quad8"     );
  m_eltFelNameToEns6Pair["Quadrangles9"] = std::make_pair( GeometricMeshRegion::Quad9,  "null5"     );
  m_eltFelNameToEns6Pair["Tetrahedra4"]  = std::make_pair( GeometricMeshRegion::Tetra4, "tetra4"    );
  m_eltFelNameToEns6Pair["Tetrahedra5"]  = std::make_pair( GeometricMeshRegion::Tetra5, "null6"     );
  m_eltFelNameToEns6Pair["Tetrahedra10"] = std::make_pair( GeometricMeshRegion::Tetra10,"tetra10"   );
  m_eltFelNameToEns6Pair["Pyramids5"]    = std::make_pair( GeometricMeshRegion::Pyram5, "pyramid5"  );
  m_eltFelNameToEns6Pair["Pyramids13"]   = std::make_pair( GeometricMeshRegion::Pyram13,"pyramid13" );
  m_eltFelNameToEns6Pair["Prisms6"]      = std::make_pair( GeometricMeshRegion::Prism6, "penta6"    );
  m_eltFelNameToEns6Pair["Prisms9"]      = std::make_pair( GeometricMeshRegion::Prism9, "null7"     );
  m_eltFelNameToEns6Pair["Prisms15"]     = std::make_pair( GeometricMeshRegion::Prism15,"penta15"   );
  m_eltFelNameToEns6Pair["Hexahedra8"]   = std::make_pair( GeometricMeshRegion::Hexa8,  "hexa8"     );
  m_eltFelNameToEns6Pair["Hexahedra9"]   = std::make_pair( GeometricMeshRegion::Hexa9,  "null8"     );
  m_eltFelNameToEns6Pair["Hexahedra12"]  = std::make_pair( GeometricMeshRegion::Hexa12, "null9"     );
  m_eltFelNameToEns6Pair["Hexahedra20"]  = std::make_pair( GeometricMeshRegion::Hexa20, "hexa20"    );
  m_eltFelNameToEns6Pair["Hexahedra26"]  = std::make_pair( GeometricMeshRegion::Hexa26, "null10"    );
  m_eltFelNameToEns6Pair["Hexahedra27"]  = std::make_pair( GeometricMeshRegion::Hexa27, "null11"    );

  FEL_CHECK((unsigned int)GeometricMeshRegion::m_numTypesOfElement == m_eltFelNameToEns6Pair.size(), "Error: incompatibility between ElementType enum and ensight std::unordered_map.");

  ElementType eltType;
  //reverse the std::unordered_map
  //ensight6 name -> enum
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = (ElementType)ityp;
    m_eltEns6NameToEnum[ m_eltFelNameToEns6Pair[ GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first ].second ] = eltType;
  }
}

/***********************************************************************************/
/***********************************************************************************/

/*! Ensigt object build mesh information  \todo and Variable  */
Ensight::Ensight(const std::string& inputDirectory,const std::string& inputFile,const std::string& inputMesh,const std::string& outputMesh,const std::string& meshDir,const std::string& resultDir) :
  m_indexStep(0),
  m_inputDirectory(inputDirectory),
  m_inputFile(inputFile),
  m_inputMesh(inputMesh),
  m_outputMesh(outputMesh),
  m_meshDir(meshDir),
  m_resultDir(resultDir)
{
  initMap();
  m_domainDim = 0;
  m_createNormalTangent = false;
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::initialize(const std::string& inputDirectory,const std::string& inputFile,const std::string& inputMesh,const std::string& outputMesh,const std::string& meshDir,const std::string& resultDir)
{
  m_indexStep = 0;
  m_inputDirectory = inputDirectory;
  m_inputFile = inputFile;
  m_inputMesh = inputMesh;
  m_outputMesh = outputMesh;
  m_meshDir = meshDir;
  m_resultDir = resultDir;
  initMap();
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::initializeOutput(const std::string& nameGeoFile,int time_set,
                                int num_steps,int filename_start_number,
                                int filename_increment )
{
  m_caseOutput.initialize(nameGeoFile,time_set,num_steps,filename_start_number,
                          filename_increment);
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::setResultDir(const std::string resultDir)
{
  m_resultDir=resultDir;
}

/***********************************************************************************/
/***********************************************************************************/

//! It reads ENSIGHT geo file in directory
//! \todo manage verbose
void Ensight::readerGeo(GeometricMeshRegion& mesh, double spaceUnit)
{
  std::string fileNameGeo, fileNameRoot, fileName;
  fileNameGeo = m_inputMesh;                            //name of the geo file
  fileNameRoot = m_meshDir;                             //directory where is the geo file
  fileName = fileNameRoot + fileNameGeo;                      //localize the geo file

  std::string idKeyVert;
  std::string idKeyEle;
  short verbose = FelisceParam::verbose();

  if (verbose>0) std::cout << "Reading from file "<< fileName << std::endl;

  // first scan of the mesh vertices
  FILE *pFile;
  pFile = fopen( fileName.c_str(),"r" );
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to read file: "+fileName+" geo mesh");
  }

  int tmp;
  char tmpIdKey[128];
  int cpt_line = 0;
  fpos_t startEltPos;
  //skip description lines.
  while (cpt_line < 2 ) {
    tmp = fgetc(pFile);
    if ( tmp == '\n' )
      cpt_line ++;
  }
  // get idkeys <off/given/assign/ignore> to describe nodes & elements ids
  if ( fscanf( pFile, "%s %s %s",tmpIdKey, tmpIdKey, tmpIdKey ) != 3 ) {
    FEL_WARNING("Failed to read tmpIdKey");
  }
  idKeyVert = tmpIdKey;
  if ( fscanf( pFile, "%s %s %s",tmpIdKey, tmpIdKey, tmpIdKey ) != 3) {
    FEL_WARNING("Failed to read tmpIdKey");
  }
  idKeyEle = tmpIdKey;
  tmp =  fgetc(pFile);
  cpt_line = 0;
  //skip coordinates.
  while (cpt_line < 1 ) {
    tmp = fgetc(pFile);
    if ( tmp == '\n' )
      cpt_line ++;
  }
  felInt number_of_points = 0;
  if ( fscanf( pFile, "%d", &number_of_points ) != 1) {
    FEL_WARNING("Failed to read numPoints");
  }
  if( verbose > 0) std::cout << "numPoints : "<< number_of_points <<std::endl;
  mesh.listPoints().resize( number_of_points, Point( 0. ) );
  if(FelisceParam::instance().effective2dGEOmesh) {
    mesh.numCoor() = 2;
  } else {
    mesh.numCoor() = 3; //Always dimension 3 in format ensight6
  }

  double BBmin[3] = {Infinity,
                      Infinity,
                      Infinity
                    };
  double BBmax[3] = {- Infinity,
                      - Infinity,
                      - Infinity
                    };

  //------------------------
  // read the vertices
  //! \todo treatment for ignore & off keyWord
  int idOld;
  std::unordered_map<int, int> oldId2OurId;
  for(int iv=0; iv< mesh.numPoints(); iv++) {
    if ( idKeyVert == "given" ) {
      if ( fscanf( pFile, "%d", & idOld ) != 1) {
        FEL_WARNING("Failed to read idOld");
      }
      oldId2OurId[idOld] = iv+1 ;
    }
    if ( fscanf ( pFile,"%lf%lf%lf\n",
                  & mesh.listPoint(iv).x(),
                  & mesh.listPoint(iv).y(),
                  & mesh.listPoint(iv).z() ) != 3) {
      FEL_WARNING("Failed to read listPoint");
    }
    mesh.listPoint(iv).x() *= spaceUnit;
    mesh.listPoint(iv).y() *= spaceUnit;
    if(FelisceParam::instance().effective2dGEOmesh) {
      mesh.listPoint(iv).z() = 0.;
    } else {
      mesh.listPoint(iv).z() *= spaceUnit;
    }
    for (int icoor = 0; icoor < mesh.numCoor(); icoor ++) {
      BBmin[icoor] = std::min( BBmin[icoor], mesh.listPoint(iv)[icoor] );
      BBmax[icoor] = std::max( BBmax[icoor], mesh.listPoint(iv)[icoor] );
    }
  }

  //------------------------
  // first scan of parts to do memory allocation
  char part[5];
  char keyWord[10];
  int tmpNumEle;
  int numPart = 0;
  int partNumber;
  ElementType eltType;
  int begElement [GeometricMeshRegion::m_numTypesOfElement ]; //temporary array: first element & number
  int nbElements [GeometricMeshRegion::m_numTypesOfElement ]; // of elements for each ref by element.
  for (int iele = 0 ; iele < GeometricMeshRegion::m_numTypesOfElement; iele++) {
    begElement [ iele ] = 0;
    nbElements [ iele ] = 0;
  }

  if ( fscanf( pFile, "%s", part ) != 1) {
    FEL_WARNING("Failed to read part");
  }
  fgetpos( pFile, & startEltPos );
  while ( feof( pFile ) == 0) {
    numPart ++;
    if ( fscanf( pFile, "%d", &partNumber) != 1 ) {
      FEL_WARNING("Failed to read partNumber");
    }
    cpt_line = 0;
    tmp =  fgetc(pFile);
    //skip description line of part
    while (cpt_line < 1 ) {
      tmp = fgetc(pFile);
      if ( tmp == '\n' )
        cpt_line ++;
    }
    if ( fscanf( pFile, "%s", keyWord ) != 1 ) {
      FEL_WARNING("Failed to read keyWord");
    }
    eltType = m_eltEns6NameToEnum[keyWord];
    begElement[eltType] += nbElements[eltType] * GeometricMeshRegion::m_numPointsPerElt[eltType];
    nbElements[eltType] = 0;
    if ( strcmp(keyWord,"block") == 0 ) {
      //! \todo manage ERROR messages
      std::ostringstream msg;
      msg << "Try to read Element type block." << std::endl;
      FEL_ERROR(msg.str());
    }
    do {
      eltType = m_eltEns6NameToEnum[keyWord];
      if( verbose > 2) std::cout<<"keyWord "<<keyWord<< " & eltType "<<eltType <<std::endl;
      if ( fscanf( pFile, "%d", &tmpNumEle ) != 1 ) {
        FEL_WARNING("Failed to read tmpNumEle");
      }
      nbElements[eltType] += tmpNumEle;
      mesh.intRefToBegEndMaps[eltType][partNumber] = std::make_pair(begElement[eltType], nbElements[eltType]);
      mesh.numElements(eltType) += tmpNumEle;
      tmp =  fgetc(pFile);
      cpt_line = 0;
      //skip coordinates.
      while (cpt_line < tmpNumEle ) {
        tmp = fgetc(pFile);
        if ( tmp == '\n' )
          cpt_line ++;
      }
      if ( fscanf( pFile, "%s", keyWord ) != 1 && feof( pFile ) == 0) {
        FEL_WARNING("Failed to read keyWord");
      }
    } while ( strcmp(keyWord,part) != 0 && feof( pFile ) == 0 );
  }

  ElementType eltType2;
  for(auto it_eltype = m_eltFelNameToEns6Pair.begin(); it_eltype != m_eltFelNameToEns6Pair.end(); ++it_eltype) {
    eltType2 = it_eltype->second.first;
    // Allocate memory
    if ( mesh.numElements(eltType2) != 0 ) {
      mesh.allocateElements(eltType2);
    }
  }

  //-----------------------------
  // 2nd scan of file
  // Fill the array of elements
  std::string DescripLine="";
  char tmpDescLine[80];
  fsetpos( pFile, &startEltPos );
  int nVpEl;
  std::vector<felInt> elem;
  bool decr_vert_id = true;
  felInt start_index[ GeometricMeshRegion::m_numTypesOfElement ];
  for (int iele = 0 ; iele < GeometricMeshRegion::m_numTypesOfElement; iele++) {
    start_index [ iele ] = 0;
  }
  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  mesh.intRefToEnum();

  m_partIdToDescLine.resize(numPart);
  while ( feof( pFile ) == 0) {
    if ( fscanf( pFile, "%d", &partNumber ) != 1) {
      FEL_WARNING("Failed to read partNumber");
    }
    //scan description line
    cpt_line = 0;
    do {
      if ( fscanf( pFile, "%s", tmpDescLine) != 1 ) {
        FEL_WARNING("Failed to read tmpDescLine");
      }
      tmp = fgetc(pFile);
      DescripLine = DescripLine + tmpDescLine;
    } while (tmp != '\n');
    m_partIdToDescLine[ partNumber-1 ] = DescripLine; // vector begin to 0
    GeometricMeshRegion::descriptionLineEnsightToListLabel[DescripLine].insert(partNumber);
    DescripLine = "";
    if ( fscanf(pFile, "%s", keyWord ) != 1) {
      FEL_WARNING("Failed to read keyWork");
    }
    do {
      if ( fscanf( pFile, "%d", & tmpNumEle ) !=1 ) {
        FEL_WARNING("Failed to read tmpNumEle");
      }
      eltType = m_eltEns6NameToEnum[keyWord];
      intRefToEnum[partNumber].insert( eltType );
      nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
      for ( felInt iel = 0; iel < tmpNumEle; iel++) {
        elem.resize(nVpEl, 0);
        if ( idKeyEle == "given" ) {
          if ( fscanf( pFile, "%d", & idOld ) !=1 ) { //unused idOld (id of element)
            FEL_WARNING("Failed to read idOld");
          }
        }
        for ( int ivert = 0; ivert < nVpEl; ivert++) {
          if ( fscanf( pFile, "%d", &idOld) !=1 ) {
            FEL_WARNING("Failed to read idOld");
          }
          if ( idKeyEle == "given" )
            elem[ivert] = oldId2OurId[idOld];
          else
            elem[ivert] = idOld;
        }
        mesh.setOneElement(eltType, iel+start_index[ eltType ], elem, decr_vert_id);
      }
      start_index[ eltType ] += tmpNumEle;
      if ( fscanf( pFile, "%s", keyWord ) !=1 && feof( pFile ) == 0) {
        FEL_WARNING("Failed to read keyWord");
      }
    } while ( strcmp(keyWord,part) != 0 && feof( pFile ) == 0 );
  }
  mesh.flagFormatMesh() = GeometricMeshRegion::FormatEnsight6;
  mesh.setDomainDim();
  m_domainDim = mesh.domainDim();
  m_createNormalTangent = mesh.createNormalTangent();
  mesh.setBagElementTypeDomain();
  mesh.setBagElementTypeDomainBoundary();
  oldId2OurId.clear();

  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

// It writes ENSIGHT6 geo file
void Ensight::writerGeo(GeometricMeshRegion& mesh, std::string nameOfGeoFile)
{
  // Getting instance
  const auto& r_instance = FelisceParam::instance();
  
  if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatUndefined) {
    felisce_error("Empty Data Structure!!! Have you read mesh file before?",__FILE__,__LINE__);
  }

  // Initialization
  std::string fileNameGeo, fileNameRoot, fileName;
  ElementType eltType;
  std::string keyWord;
  int the_ref;
  short verbose = 0;
  FILE * pFile;
  felInt startindex = 0, numElemsPerRef = 0;
  std::vector<std::vector<felInt>> elem(1);
  bool incr_vert_id = true;
  if(nameOfGeoFile.empty()) {
    fileNameGeo = m_outputMesh;
    fileNameRoot = m_resultDir;
    fileName = fileNameRoot + fileNameGeo; //where is write the geo file
  } else {
    fileName = nameOfGeoFile;
  }
  if (verbose > 0) {
    std::cout << "Writing file "<< fileNameGeo << std::endl;
  }
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    std::string command = "mkdir -p " + m_resultDir;
    int ierr = system(command.c_str());
    if (ierr>0) {
      FEL_ERROR("Impossible to write "+fileName+"  geo mesh");
    }
    pFile = fopen (fileName.c_str(),"w");
  }

  // Descriptions lines (All Geometry files must contain these first six lines)
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"node id assign\n");
  fprintf(pFile,"element id assign\n");
  fprintf(pFile,"coordinates\n");
  fprintf(pFile,"%8d\n",mesh.numPoints());

  // Write the vertices
  const auto dimension = mesh.numCoor();
  if (dimension == 3) {
    for(int iv=0; iv< mesh.numPoints(); iv++) {
      fprintf( pFile,"%12.5e%12.5e%12.5e\n", mesh.listPoint(iv).x(),
                mesh.listPoint(iv).y(), mesh.listPoint(iv).z() );
    }
  } else if (dimension == 2) {
    double zcoor = 0.;
    for(int iv=0; iv< mesh.numPoints(); iv++) {
      fprintf( pFile, "%12.5e%12.5e%12.5e\n", mesh.listPoint(iv).x(), mesh.listPoint(iv).y(), zcoor);
    }
  } else {
    std::ostringstream msg;
    msg << "Error in writing geo File " << fileNameGeo << ". "
        << "Expecting mesh with 2 or 3 coordinates (it is " <<dimension << ")."
        << std::endl;
    FEL_ERROR(msg.str());
  }

  // Write the elements per references
  int cptpart = 0;
  std::string DescriptionLine,felName;
  bool toMITC9 = false;
  auto& intRefToEnum = mesh.intRefToEnum();
  for(auto it_ref = intRefToEnum.begin(); it_ref != intRefToEnum.end(); ++it_ref) {
    the_ref = (*it_ref).first;

    if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatEnsight6) {
      cptpart++;
      fprintf ( pFile, "part%8d\n",cptpart );
      DescriptionLine = m_partIdToDescLine[ cptpart - 1 ];
      fprintf (pFile, "%s", DescriptionLine.c_str() );
    }
    for(auto it_elem = it_ref->second.begin(); it_elem != it_ref->second.end(); ++it_elem) {
      eltType = *it_elem;
      numElemsPerRef = mesh.intRefToBegEndMaps[eltType][the_ref].second;

      if (numElemsPerRef > 0) {
        if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatMedit) {
          cptpart++;
          fprintf ( pFile, "part%8d\n",cptpart );
          felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
          const auto& r_map = r_instance.felName2MapintRef2DescLine;
          auto it_felName = r_map.find(felName);
          if ( it_felName == r_map.end() ) {
            DescriptionLine = felName + "_ref_" + std::to_string(the_ref);
          } else {
            auto it_ref2 = it_felName->second.find(the_ref);
            if ( it_ref2 == it_felName->second.end() ) {
              DescriptionLine = felName + "_ref_" + std::to_string(the_ref);
            } else {
              DescriptionLine = it_ref2->second;
            }
          }
          fprintf (pFile, "%s", DescriptionLine.c_str() );
        }
        fprintf(pFile,"\n");
        keyWord = getKeyword(eltType);

        // Here the keyword is modified for quad9
        if (eltType == 11) { //TODO:  Move this to the auxiliary methods
          keyWord = "quad4";
          toMITC9 = true;
        } else if (eltType == 3 && toMITC9) {
          keyWord = "bar2";
        }

        fprintf( pFile, "%s", keyWord.c_str() );
        fprintf(pFile,"\n");

        // Num elements Q1 = 4 Q9
        startindex = mesh.intRefToBegEndMaps[eltType][the_ref].first;
        if (eltType == 11) {
          fprintf(pFile,"%8d\n", 4*numElemsPerRef);
        } else if (eltType == 3 && toMITC9) { 
          fprintf(pFile,"%8d\n", 2*numElemsPerRef);
        } else {
          fprintf(pFile,"%8d\n", getNumberOfElements(mesh, eltType, the_ref));
        }

        prepareElemIndexes(elem, eltType);
        for ( felInt iel = 0; iel < numElemsPerRef; iel++) {
          getElemIndexes(mesh, eltType, iel, elem, startindex, incr_vert_id);

          // Here is created the news connectivities between 1 quad9 and 4 quad4
          if (eltType == 11) { // Quad9
            std::vector<felInt> listVertQ1Elem(16);
            convertQuad9To4Quad1(listVertQ1Elem);
            for (int k=0 ; k<4 ; k++) {
              for ( int ivert = 0; ivert < 4; ivert++) {
                fprintf( pFile, "%8d", elem[0][listVertQ1Elem[4*k+ivert]]);
              }
              fprintf(pFile,"\n");
            }
          } else if (eltType == 3 && toMITC9) {
            // bar3 (0----2----1) = 2 bar2  (0----2)   (2----1) to corresponding with the quad9 = 4 quad1
            fprintf( pFile, "%8d" "%8d", elem[0][0], elem[0][2] );
            fprintf(pFile,"\n");
            fprintf( pFile, "%8d" "%8d", elem[0][2], elem[0][1] );
            fprintf(pFile,"\n");
          } else {
            for (auto& r_elem : elem) {
              for (std::size_t ivert = 0; ivert < r_elem.size(); ivert++) {
                fprintf( pFile, "%8d", r_elem[ivert]);
              }
              fprintf(pFile,"\n");
            }
          }
        }
      }
    }
  }

  fclose(pFile);
  if (mesh.createNormalTangent()){
    m_caseOutput.SetDomainDim_case(mesh.domainDim());
    m_caseOutput.SetCreateNormalTangent_case(mesh.createNormalTangent());
    std::string nameTangent;
    for (int i=0 ; i<mesh.domainDim() ; i++) {
      nameTangent = "tangentVector" + std::to_string(i+1);
      writeNormalTangent(nameTangent, mesh.listTangents(i));
    }
    writeNormalTangent("normalVector", mesh.listNormals());
  }
}

/***********************************************************************************/
/***********************************************************************************/

//to create the news connectivities between quad9 and 4 quad1
// connectivities to quad9 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }
// 4 quad1 with the following connectivities { 1, 5, 9, 8 } { 2, 6, 9, 5 } { 3, 7, 9, 6 } { 4, 8, 9, 7 }
void Ensight::convertQuad9To4Quad1(std::vector <felInt>& listVertQ1Elem)
{
  for (int i=0 ; i<4 ; i++) {
    listVertQ1Elem[4*i] = i;
    listVertQ1Elem[4*i+1] = 4+i;
    listVertQ1Elem[4*i+2] = 8;
    listVertQ1Elem[4*i+3] = 3+i;
  }
  listVertQ1Elem[3] = 7;
}

/***********************************************************************************/
/***********************************************************************************/

//to get normal and tangent vectors in Ensight
void Ensight::writeNormalTangent(const std::string outPutFileVec, const std::vector<Point>& vect)
{
  std::string fileNameRoot, fileName = outPutFileVec;
  fileNameRoot = m_resultDir;
  fileName = fileNameRoot + fileName + ".vct";

  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to write  "+fileName+" vectorial ensight solution");
  }

  if(FelisceParam::instance().outputFileFormat == ENSIGHT6) {
    fprintf( pFile, "Vector per node\n");
    int count=0;
    for ( unsigned int i=0; i < vect.size(); i++ ) {
      fprintf(pFile, "%12.5e%12.5e%12.5e"  ,vect[i].x(), vect[i].y(), vect[i].z());
      count=count+3;
      if ( count == 6 ) {
        fprintf(pFile, "\n" );
        count=0;
      }
    }
  } else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD) {
    std::size_t nDim = 3;
    int label = 0;
    fprintf(pFile, "Ensight Gold vector per node\n");

    // loop over references
    for (auto refIt = m_refToListOfIds.begin(); refIt != m_refToListOfIds.end(); ++refIt) {
      label = refIt->first;
      fprintf(pFile, "part\n");
      fprintf(pFile, "%10d\n", label + 1);
      fprintf(pFile, "coordinates\n");

      // [u_x_1, u_x_2, u_x_3, ..., u_y_1, u_y_2, u_y_3, ..., u_z_1, u_z_2, u_z_3, ...]
      for(std::size_t idim=0; idim<nDim; ++idim)
        for(std::size_t inode=0; inode < refIt->second.size(); ++inode)
          fprintf(pFile, "%12.5e\n", vect[refIt->second[inode]][idim]);
    }
  } else if(FelisceParam::instance().outputFileFormat == ENSIGHT6BIN) {
    char buffer[80];
    local_strncpy(buffer, "Vector per node\n",80);
    fwrite(buffer, sizeof(char), 80, pFile);
    std::vector<float> array(3*vect.size());
    for(std::size_t i=0 ; i< vect.size() ; i++) {
      for (std::size_t j=0 ; j<3 ; j++)
          array[3*i+j] = static_cast<float>(vect[i].coor(j));
      }
    fwrite(array.data(),sizeof(float),3*vect.size(),pFile);
  }

    else {
    FEL_ERROR("Unknown output file format");
  }

  fclose( pFile );
}

/***********************************************************************************/
/***********************************************************************************/

// It writes ENSIGHT GOLD geo file
//! \todo manage verbose
void Ensight::writerGeoGold(GeometricMeshRegion& mesh, std::string nameOfGeoFile)
{
  m_caseOutput.SetDomainDim_case(mesh.domainDim());
  m_caseOutput.SetCreateNormalTangent_case(mesh.createNormalTangent());

  if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatUndefined) {
    felisce_error("Empty Data Structure!!! Have you read mesh file before?",__FILE__,__LINE__);
  }

  // clear std::unordered_map
  m_refToListOfIds.clear();

  // Initialization
  // =============
  std::string fileNameGeo, fileNameRoot, fileName;
  std::string keyWord;
  FILE * pFile;

  if(nameOfGeoFile.empty()) {
    fileNameGeo = m_outputMesh;
    fileNameRoot = m_resultDir;
    fileName = fileNameRoot + fileNameGeo; //where is write the geo file
  } else {
    fileName = nameOfGeoFile;
  }

  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    std::string command = "mkdir -p " + m_resultDir;
    int ierr = system(command.c_str());
    if (ierr>0) {
      FEL_ERROR("Impossible to write "+fileName+"  geo mesh");
    }
    pFile = fopen (fileName.c_str(),"w");
  }

  // Descriptions lines (All Geometry files must contain these first six lines)
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"node id given\n");
  fprintf(pFile,"element id assign\n");

  // list of type of element by labels
  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  mesh.intRefToEnum();

  felInt label;
  bool(*comparison)(std::pair<felInt, Point >, std::pair<felInt, Point >) = Tools::pairComparison;

  // loop over references
  for (auto refIt = intRefToEnum.begin(); refIt != intRefToEnum.end(); ++refIt) {
    label = refIt->first;
    fprintf(pFile, "part\n");
    fprintf(pFile, "%10d\n", label + 1);

    std::string descriptionLine = "";
    for (auto typeIt = refIt->second.begin(); typeIt != refIt->second.end(); ++typeIt)
      descriptionLine += GeometricMeshRegion::eltEnumToFelNameGeoEle[*typeIt].first + "_";

    descriptionLine += "ref_";
    fprintf(pFile, "%s%d\n", descriptionLine.c_str(), label);
    fprintf(pFile, "coordinates\n");

    std::vector<felInt> numElementOfType(GeometricMeshRegion::m_numTypesOfElement, 0);
    std::map<felInt, felInt> local2GlobalMapping;
    std::set<std::pair<felInt, Point >, bool(*)(std::pair<felInt, Point >, std::pair<felInt, Point >)> listOfNodes(comparison);

    // loop over the type of element having this reference
    for (auto typeIt = refIt->second.begin(); typeIt != refIt->second.end(); ++typeIt) {
      // fill the mappings
      felInt numPointPerElt = mesh.numPtsPerElement(*typeIt);
      auto it_ref = mesh.intRefToBegEndMaps[*typeIt].find(label);
      felInt startindex = it_ref->second.first;
      felInt numElt = it_ref->second.second;

      felInt idPoint;
      Point pt;

      for (felInt iel = 0 ; iel < numElt; iel++) {
        for (int jvert = 0; jvert < numPointPerElt; jvert++) {
          idPoint = mesh.listElements(*typeIt)[startindex + iel*numPointPerElt + jvert];
          pt = mesh.listPoint(idPoint);
          listOfNodes.insert(std::make_pair(idPoint, pt));
        }
      }

      numElementOfType[*typeIt] = numElt;
    }

    fprintf(pFile, "%10d\n", static_cast<int>(listOfNodes.size()));
    felInt localCounter = 1;
    for (auto nodeIt = listOfNodes.begin(); nodeIt != listOfNodes.end(); ++nodeIt) {
      m_refToListOfIds[label].push_back(nodeIt->first);

      fprintf(pFile, "%10d\n", nodeIt->first + 1);
      local2GlobalMapping[nodeIt->first] = localCounter;
      ++localCounter;
    }

    for(felInt icoor=0; icoor<3; ++icoor) {
      for (auto nodeIt = listOfNodes.begin(); nodeIt != listOfNodes.end(); ++nodeIt) {
        fprintf(pFile, "%12.5e\n", nodeIt->second[icoor]);
      }
    }

    for (auto typeIt = refIt->second.begin(); typeIt != refIt->second.end(); ++typeIt) {
      keyWord = m_eltFelNameToEns6Pair[GeometricMeshRegion::eltEnumToFelNameGeoEle[*typeIt].first].second;
      fprintf(pFile, "%s\n", keyWord.c_str());
      fprintf(pFile, "%10d\n", numElementOfType[*typeIt]);

      felInt numPointPerElt = mesh.numPtsPerElement(*typeIt);
      auto it_ref = mesh.intRefToBegEndMaps[*typeIt].find(label);
      felInt startindex = it_ref->second.first;
      felInt numElt = it_ref->second.second;

      for (felInt iel = 0 ; iel < numElt; iel++) {
        for (int jvert = 0; jvert < numPointPerElt; jvert++) {
          fprintf(pFile, "%10d", local2GlobalMapping[mesh.listElements(*typeIt)[startindex + iel*numPointPerElt + jvert]]);
        }
        fprintf(pFile, "\n");
      }
    }
  }

  fclose(pFile);

  if (mesh.createNormalTangent()){
    std::string nameTangent;
    for (int i=0 ; i<mesh.domainDim() ; i++) {
      nameTangent = "tangentVector" + std::to_string(i+1);
      writeNormalTangent(nameTangent, mesh.listTangents(i)); 
    }
    writeNormalTangent("normalVector", mesh.listNormals());
  }
}

/***********************************************************************************/
/***********************************************************************************/

// It writes ENSIGHT6 geo file of rank-th
void Ensight::writerGeo(GeometricMeshRegion& mesh,int rank)
{
  if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatUndefined) {
    felisce_error("Empty Data Structure!!! Have you read mesh file before?",__FILE__,__LINE__);
  }
  // Initialization
  // =============
  std::string fileNameGeo, fileNameRoot, fileName;
  ElementType eltType = static_cast<ElementType>(0);
  std::string keyWord;
  int the_ref;
  short verbose = 1;
  FILE * pFile;
  felInt startindex = 0, numElemsPerRef = 0;
  std::vector<std::vector<felInt>> elem(1);
  bool incr_vert_id = true;
  fileNameGeo = m_outputMesh;
  fileNameRoot = m_resultDir;
  const std::string rankName = rank == -1 ? "" : std::to_string( rank );
  fileName = fileNameRoot + rankName + fileNameGeo;
  if (verbose>0) std::cout << "Writing file "<< fileNameGeo << std::endl;

  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    std::string command = "mkdir -p " + m_resultDir;
    int ierr = system(command.c_str());
    if (ierr>0)
      FEL_ERROR("Impossible to write "+fileName+"  geo mesh");
    pFile = fopen (fileName.c_str(),"w");
  }
  // Descriptions lines (All Geometry files must contain these first six lines)
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"node id assign\n");
  fprintf(pFile,"element id assign\n");
  fprintf(pFile,"coordinates\n");
  fprintf(pFile,"%8d\n",mesh.numPoints());

  // Write the vertices
  if ( mesh.numCoor() == 3 ) {
    for(int iv=0; iv< mesh.numPoints(); iv++) {
      fprintf( pFile,"%12.5e%12.5e%12.5e\n", mesh.listPoint(iv).x(),
                mesh.listPoint(iv).y(), mesh.listPoint(iv).z() );
    }
  } else if ( mesh.numCoor() == 2 ) {
    double zcoor = 0.;
    for(int iv=0; iv< mesh.numPoints(); iv++) {
      fprintf( pFile, "%12.5e%12.5e%12.5e\n", mesh.listPoint(iv).x(), mesh.listPoint(iv).y(), zcoor);
    }
  } else {
    //! manage errors
    std::ostringstream msg;
    msg << "Error in writing geo File " << fileNameGeo << ". "
        << "Expecting mesh with 2 or 3 coordinates (it is " << mesh.numCoor() << ")."
        << std::endl;
    FEL_ERROR(msg.str());
  }

  // Write the elements per references.
  //------------
  int cptpart = 0;
  std::string DescriptionLine,felName;
  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  mesh.intRefToEnum();
  for(auto it_ref = intRefToEnum.begin(); it_ref != intRefToEnum.end(); ++it_ref) {
    the_ref = it_ref->first;
    numElemsPerRef = mesh.intRefToBegEndMaps[eltType][the_ref].second;
    if (numElemsPerRef > 0) {
      if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatEnsight6) {
        cptpart++;
        fprintf ( pFile, "part%8d\n",cptpart );
        DescriptionLine = m_partIdToDescLine[ cptpart - 1 ];
        fprintf (pFile, "%s", DescriptionLine.c_str() );
      }
      for(auto it_elem = it_ref->second.begin(); it_elem != it_ref->second.end(); ++it_elem) {
        eltType = *it_elem;
        if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatMedit) {
          cptpart++;
          fprintf ( pFile, "part%8d\n",cptpart );
          felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
          auto it_felName = FelisceParam::instance().felName2MapintRef2DescLine.find(felName);
          if ( it_felName == FelisceParam::instance().felName2MapintRef2DescLine.end() ) {
            DescriptionLine = felName + "_ref_" + std::to_string(the_ref);
          } else {
            auto it_ref2 = it_felName->second.find(the_ref);
            if ( it_ref2 == it_felName->second.end() ) {
              DescriptionLine = felName + "_ref_" + std::to_string(the_ref);
            } else {
              DescriptionLine = it_ref2->second;
            }
          }
          fprintf (pFile, "%s", DescriptionLine.c_str() );
        }
        fprintf(pFile,"\n");
        keyWord = getKeyword(eltType);
        fprintf( pFile, "%s", keyWord.c_str() );
        fprintf(pFile,"\n");
        startindex = mesh.intRefToBegEndMaps[eltType][the_ref].first;
        fprintf(pFile,"%8d\n", getNumberOfElements(mesh, eltType, the_ref));
        prepareElemIndexes(elem, eltType);
        for ( felInt iel = 0; iel < numElemsPerRef; iel++) {
          getElemIndexes(mesh, eltType, iel, elem, startindex, incr_vert_id);
          for (auto& r_elem : elem) {
            for (std::size_t ivert = 0; ivert < r_elem.size(); ivert++) {
              fprintf( pFile, "%8d", r_elem[ivert]);
            }
            fprintf(pFile,"\n");
          }
        }
      }
    }
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::writerGeoBin(GeometricMeshRegion& mesh, std::string nameOfGeoFile)
{
  if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatUndefined) {
    felisce_error("Empty Data Structure!!! Have you read mesh file before?",__FILE__,__LINE__);
  }
  short verbose = 2;
  // Initialization
  // =============
  std::string fileNameGeo, fileNameRoot, fileName;
  FILE *pFile;
  if(nameOfGeoFile.empty()) {
    fileNameGeo = m_outputMesh;
    fileNameRoot = m_resultDir;
    fileName = fileNameRoot + fileNameGeo; //where is write the geo file
  } else {
    fileName = nameOfGeoFile;
  }
  if (verbose>0) std::cout << "Writing binary file "<< fileNameGeo << std::endl;
  pFile = fopen (fileName.c_str(),"wb");
  if (pFile==nullptr) {
    std::string command = "mkdir -p " + m_resultDir;
    int ierr = system(command.c_str());
    if (ierr>0)
      FEL_ERROR("Impossible to write "+fileName+"  binary geo mesh");
    pFile = fopen (fileName.c_str(),"w");
  }

  char buffer[80];
  local_strncpy(buffer,"C Binary",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  local_strncpy(buffer, "Description line 1",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  local_strncpy(buffer, "Description line 2",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  local_strncpy(buffer, "node id assign",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  local_strncpy(buffer, "element id assign",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  local_strncpy(buffer, "coordinates",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  felInt number_of_points = mesh.numPoints();
  fwrite(&number_of_points, sizeof(int), 1, pFile);
  std::vector<float> coordinates(3*number_of_points);
  for(felInt iv=0; iv< number_of_points; iv++) {
    coordinates[iv*3] = static_cast<float>(mesh.listPoint(iv).x());
    coordinates[iv*3 + 1] = static_cast<float>(mesh.listPoint(iv).y());
    coordinates[iv*3 + 2] = static_cast<float>(mesh.listPoint(iv).z());
  }
  fwrite(coordinates.data(), sizeof(float), 3*number_of_points, pFile);

  int cptpart = 0;
  ElementType eltType;
  int the_ref;
  std::string part;
  std::string DescriptionLine, felName;
  std::string keyWord;
  int  nVpEl = 0;
  felInt startindex = 0, numElemsPerRef = 0;
  std::vector<int> tabElem;
  std::vector<felInt> elem;
  bool incr_vert_id = true;
  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  mesh.intRefToEnum();
  for(auto it_ref = intRefToEnum.begin(); it_ref != intRefToEnum.end(); ++it_ref) {

    the_ref = (*it_ref).first;
    if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatEnsight6) {
      cptpart++;
      part = "part " + std::to_string(cptpart);
      local_strncpy(buffer, part.c_str(),80);
      fwrite(buffer, sizeof(char), 80, pFile);
      DescriptionLine = m_partIdToDescLine[ cptpart - 1 ];
      local_strncpy(buffer, DescriptionLine.c_str(),80);
      fwrite(buffer, sizeof(char), 80, pFile);
    }
    for(auto it_elem = it_ref->second.begin(); it_elem != it_ref->second.end(); ++it_elem) {
      eltType = *it_elem;
      if (mesh.flagFormatMesh() == GeometricMeshRegion::FormatMedit) {
        cptpart++;
        part = "part " + std::to_string(cptpart);
        local_strncpy(buffer, part.c_str(),80);
        fwrite(buffer, sizeof(char), 80, pFile);
        felName = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first;
        auto it_felName = FelisceParam::instance().felName2MapintRef2DescLine.find(felName);
        if ( it_felName == FelisceParam::instance().felName2MapintRef2DescLine.end() ) {
          DescriptionLine = felName + "_ref_" + std::to_string(the_ref);
        } else {
          auto it_ref2 = it_felName->second.find(the_ref);
          if ( it_ref2 == 	it_felName->second.end() ) {
            DescriptionLine = felName + "_ref_" + std::to_string(the_ref);
          } else {
            DescriptionLine = it_ref2->second;
          }
        }
        local_strncpy(buffer, DescriptionLine.c_str(),80);
        fwrite(buffer, sizeof(char), 80, pFile);
      }
      keyWord = m_eltFelNameToEns6Pair[  GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first ].second;
      local_strncpy(buffer, keyWord.c_str(),80);
      fwrite(buffer, sizeof(char), 80, pFile);
      numElemsPerRef = mesh.intRefToBegEndMaps[eltType][the_ref].second;
      startindex = mesh.intRefToBegEndMaps[eltType][the_ref].first;
      fwrite(&numElemsPerRef, sizeof(int), 1, pFile);
      nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
      tabElem.resize(nVpEl*numElemsPerRef);
      for ( felInt iel = 0; iel < numElemsPerRef; iel++) {
        elem.resize(nVpEl, 0);
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        for (int ivert = 0; ivert < nVpEl; ivert++) {
          tabElem[iel*nVpEl + ivert] = elem[ivert];
        }
      }
      fwrite(tabElem.data(), sizeof(int), nVpEl*numElemsPerRef, pFile);
    }
  }
  fclose(pFile);
  if (mesh.createNormalTangent()){
    m_caseOutput.SetDomainDim_case(mesh.domainDim());
    m_caseOutput.SetCreateNormalTangent_case(mesh.createNormalTangent());
    std::string nameTangent;
    for (int i=0 ; i<mesh.domainDim() ; i++) {
      nameTangent = "tangentVector" + std::to_string(i+1);
      writeNormalTangent(nameTangent, mesh.listTangents(i));
    }
    writeNormalTangent("normalVector", mesh.listNormals());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::postProcess(const std::string& outputDirectory,
                          const std::string& nameCase,
                          int optionMultiCase,
                          double& time)
{
  if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD) {
    for(std::size_t i=0; i<m_caseOutput.numVariables(); ++i) {
      m_caseOutput.variable(i).setRefToListOfIds(m_refToListOfIds);
    }
  }
  // m_caseOutput.domainDim() = m_domainDim;
  m_caseOutput.postProcess(outputDirectory,nameCase,optionMultiCase,time);
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::readCaseFile()
{
  m_caseInput.read(m_inputDirectory, m_inputFile);
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::readVariable(int idVariable, double time, double* valueOfVariable, felInt size)
{
  int indexTime = m_caseInput.timeIndex(time);
  m_caseInput.readVariable(idVariable, indexTime, valueOfVariable, size);
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::addVariable(int typeVariable, const std::string nameVariable,
                          const double* solution, const felInt size, bool manageMemory)
{
  m_caseOutput.addVariable(static_cast<EnsightVariable::TypeVar>(typeVariable), nameVariable, solution, size, manageMemory);
}

/***********************************************************************************/
/***********************************************************************************/

EnsightCase::EnsightCase():
  m_verbose(0),
  m_domainDim(0),
  m_createNormalTangent(false),
  m_directory("none"),
  m_filename("none"),
  m_sectionFormat(0),
  m_sectionGeometry(0),
  m_sectionVariable(0),
  m_sectionTime(0),
  m_model_time_set(0),
  m_model_file_set(0),
  m_change_coords_only(false),
  m_change_coords_only_cstep(false),
  m_geo_file("none"),
  m_geo_file_wildcard("none"),
  m_num_wildcard(0),
  m_num_variable(0)
{

}

/***********************************************************************************/
/***********************************************************************************/

EnsightCase::EnsightCase(int verbose):m_verbose(verbose)
{}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::initialize(const std::string& nameGeoFile,int time_set,
                              int num_steps,int filename_start_number,
                              int filename_increment )
{
  m_geo_file = nameGeoFile;
  m_var.clear();
  m_time.clear();
  EnsightTimeBis time(time_set,num_steps,filename_start_number,filename_increment);
  m_time.push_back(time);
}

/***********************************************************************************/
/***********************************************************************************/

EnsightVariable const& EnsightCase::variable(int ivar) const
{
  return m_var[ivar];
}

/***********************************************************************************/
/***********************************************************************************/

EnsightVariable& EnsightCase::variable(int ivar)
{
  return m_var[ivar];
}

/***********************************************************************************/
/***********************************************************************************/

EnsightTimeBis& EnsightCase::timeSet(int iset)
{
  return m_time[iset];
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::read(const std::string& directory,const std::string& case_file)
{
  m_sectionFormat=-1;
  m_sectionGeometry=-1;
  m_sectionVariable=-1;
  m_sectionTime=-1;
  m_line.resize(0);
  m_directory = directory;
  m_filename = case_file;
  const std::string casefilename = m_directory+"/"+case_file;
  std::ifstream casefile(casefilename.c_str(), std::ios_base::in);
  if(m_verbose) std::cout << "EnsightCase::read case file " << casefilename << std::endl;
  if( !casefile) {
    if (m_verbose)
      std::cout << "Cannot open " << casefilename << std::endl;
  } else {
    std::size_t found;
    std::string dummy;
    int i=0;
    while ( getline( casefile, dummy ) ) {
      m_line.push_back(dummy);
      found=m_line[i].find("FORMAT");
      if(found!=std::string::npos) m_sectionFormat=i;
      found=m_line[i].find("GEOMETRY");
      if(found!=std::string::npos) m_sectionGeometry=i;
      found=m_line[i].find("VARIABLE");
      if(found!=std::string::npos) m_sectionVariable=i;
      found=m_line[i].find("TIME");
      if(found!=std::string::npos) m_sectionTime=i;
      i++;
    }
    casefile.close();

    if(m_sectionFormat==-1) {
      FEL_ERROR("Error reading ensight case file: FORMAT section is required")
    }
    if(m_sectionGeometry==-1) {
      FEL_ERROR("Error reading ensight case file: GEOMETRY section is required")
    }
    if(m_sectionVariable==-1) {
      FEL_ERROR("Warning: case file contains no VARIABLE section")
    }
    if(m_sectionTime==-1) {
      std::cout << "Warning: case file contains no TIME section" << std::endl;
    }
#undef DEBUGFORMAT
#ifdef DEBUGFORMAT
    std::cout << "format : " << m_sectionFormat << std::endl;
    std::cout << "geometry : " << m_sectionGeometry<< std::endl;
    std::cout << "variable : " << m_sectionVariable << std::endl;
    std::cout << "time : " << m_sectionTime << std::endl;
#endif

    //====================
    // Read FORMAT section
    //====================
    /*
      This section is required; it specifies the data type.
      FORMAT
      type: ensight gold
      */

    //======================
    // Read GEOMETRY section
    //======================
    /*
      This is a required section. It describes the geometry of the data. Furthermore it can describe periodic match file if used,
      a boundary file if used, rigid body file if used, and vector glyph file if used.
      GEOMETRY
      model:      [ts] [fs]      filename          [change_coords_only [cstep]]
      measured:   [ts] [fs]      filename          [change_coords_only]
      match:                     filename
      boundary:                  filename
      rigid_body:                filename
      Vector_glyphs:             filename

      where:
      ts = a time set number as specified in TIME section, ( optional ).
      fs = a corresponding file set number; specified in the FILE section.
      If an fs number is specified, then a ts is required.
      filename = The declared filename.
      - Wildcard symbols, * , are NOT USED for model or measured files that are static, ( or if written in the Single File Format).
      Furthermore, wildcard symbols are not used for match, boundary, or rigid_body filenames.
      - Wildcard symbols ARE USED for model or measured changing geometry written to multiple files.
      - Model filenames with structured block continuation options will contain % wildcards.
      change_coords_only = This option specifies that the changing geometry is for coordinates only; else the connectivity will
      also assumed to be changing and need to be specified as well.
      cstep = Used with change_coords_only option for the zero-based time step containing the connectivity, ( Optional ).
      If all time steps have connectivity, it can be omitted. If used, the remaining time steps no not need to repeat the connectivity.
      */
    std::vector<std::string> contents;
    split(m_line[m_sectionGeometry+1],contents," ");
    bool has_a_cstep=false;
    if(contents[0] == "model:") {
      m_change_coords_only = false;
      m_change_coords_only_cstep = 0;
      const std::size_t dim = contents.size();
      for(std::size_t j=0; j<dim; j++) {
        found= contents[j].find("change_coords_only");
        if(found!=std::string::npos) {
          m_change_coords_only = true;
          if(j==dim-2) {
            has_a_cstep = true;
            m_change_coords_only_cstep = std::atoi(contents[dim-1].c_str());
          }
        }
      }
      if(m_change_coords_only) {
        // change_coords_only
        if(has_a_cstep) {
          // cstep
          if(dim == 4) {
            m_model_time_set = 0;
            m_model_file_set = 0;
            m_geo_file = contents[1];
          } else if(dim == 5) {
            m_model_time_set = std::atoi(contents[1].c_str());
            m_model_file_set = 0;
            m_geo_file = contents[2];
          } else if(dim == 6) {
            m_model_time_set = std::atoi(contents[1].c_str());
            m_model_file_set = std::atoi(contents[2].c_str());
            m_geo_file = contents[3];
          }
        } else {
          // no cstep
          if(dim == 3) {
            m_model_time_set = 0;
            m_model_file_set = 0;
            m_geo_file = contents[1];
          } else if(dim == 4) {
            m_model_time_set = std::atoi(contents[1].c_str());
            m_model_file_set = 0;
            m_geo_file = contents[2];
          } else if(dim == 5) {
            m_model_time_set = std::atoi(contents[1].c_str());
            m_model_file_set = std::atoi(contents[2].c_str());
            m_geo_file = contents[3];
          }
        }
      } else {
        // no change_coords_only
        if(dim == 2) {
          m_model_time_set = 0;
          m_model_file_set = 0;
          m_geo_file = contents[1];
        } else if(dim == 3) {
          m_model_time_set = std::atoi(contents[1].c_str());
          m_model_file_set = 0;
          m_geo_file = contents[2];
        } else if(dim == 4) {
          m_model_time_set = std::atoi(contents[1].c_str());
          m_model_file_set = std::atoi(contents[2].c_str());
          m_geo_file = contents[3];
        }
      }
    } else {
      std::ostringstream msg;
      msg << contents[0] << " not yet implemented";
      FEL_ERROR(msg.str())
    }
    m_num_wildcard = CountOccurenceChar(m_geo_file,'*');
    if(m_num_wildcard) m_geo_file_wildcard = m_geo_file;
    else m_geo_file_wildcard = "";
    if(CountOccurenceChar(m_geo_file,'%')) {
      FEL_ERROR("Wildcard % not yet implemented ")
    }
    if(m_geo_file_wildcard != "") m_geo_file = ReplaceWilcardByInt(m_geo_file_wildcard, 198, '*');

#undef DEBUGGEO
#ifdef DEBUGGEO
    std::cout << "GEOMETRY" << std::endl;
    std::cout << "\t model:" << std::endl;
    std::cout << "\t model_time_set = " << m_model_time_set << std::endl;
    std::cout << "\t mode_file_set = " << m_model_file_set << std::endl;
    std::cout << "\t geo_file = " << m_geo_file << std::endl;
    std::cout << "\t geo_file_wildcard = " << m_geo_file_wildcard << std::endl;
    std::cout << "\t change_coords_only = " << m_change_coords_only << std::endl;
    std::cout << "\t change_coords_only_cstep = " << m_change_coords_only_cstep << std::endl;
#endif
    //======================
    // Read VARIABLE section
    //======================
    /*
      This section of the ASCII case file is optional. If declared, it specifies the filenames and variables of the data.
      The EnSight Gold Casefile Format allows for 300 variables to be declared.
      Here's an example

      VARIABLE
      constant per case:           [ts]              description const_value(s)
      constant per case file:      [ts]              description cvfilename
      scalar per node:             [ts] [fs]         description filename
      vector per node:             [ts] [fs]         description filename
      tensor symm per node:        [ts] [fs]         description filename
      tensor asym per node:        [ts] [fs]         description filename
      scalar per element:          [ts] [fs]         description filename
      vector per element:          [ts] [fs]         description filename
      tensor symm per element:     [ts] [fs]         description filename
      tensor asym per element:     [ts] [fs]         description filename
      scalar per measured node:    [ts] [fs]         description filename
      vector per measured node:    [ts] [fs]         description filename
      complex scalar per node:     [ts] [fs]         description Re_fn Im_fn freq
      complex vector per node:     [ts] [fs]         description Re_fn Im_fn freq
      complex scalar per element:  [ts] [fs]         description Re_fn Im_fn freq
      complex vector per element:  [ts] [fs]         description Re_fn Im_fn freq


      where:
      ts = The corresponding time set number ( or index ) as specified in the TIME section.
      fs = The corresponding file set number ( Or index ) as specified in the FILE section.
      description = This will be the variable name presented by the GUI.
      const_value(s) = If constants change over time then ns in TIME section below; else ts for constant,
      cvfilename = The constant values filename, that contains the actual constant values.
      Re_fn = The real values filename, that contains the real parts of a complex variable.
      Im_fn = The imaginary values filename, that contains the imaginary parts of a complex variable.
      freq = The corresponding harmonic frequency of the complex variable. UNDEFINED may be used as an acceptable
      value if it's undefined.
      Notes:
      As many variable lines as needed may be used.
      Variable descriptions have the following restrictions:
      - 19 characters
      - duplicates not allowed
      - leading or trailing whitespace is disguarded
      - must not start with a digit
      - must not contain any of the following reserved characters
      ( [ + @ ! * $ ) ] - space # ^ /
      */
    m_num_variable = m_sectionTime - m_sectionVariable - 1;
    m_var.resize(m_num_variable);
    std::string typevar,restofline,description,filename;
    int time_set = 0;
    int file_set = 0;
    for(int j=0; j<m_num_variable; j++) {
      found = m_line[m_sectionVariable+1+j].find(':');
      typevar = m_line[m_sectionVariable+1+j].substr(0,found);
      restofline = m_line[m_sectionVariable+1+j].substr(found+1,m_line[m_sectionVariable+1+j].size()-found-1);
      split(restofline,contents," ");

      const std::size_t contents_size = contents.size();
      if(contents_size == 4) {
        time_set = std::atoi(contents[0].c_str());
        file_set = std::atoi(contents[1].c_str());
        description = contents[2];
        filename = contents[3];
      } else if(contents_size == 3) {
        time_set = std::atoi(contents[0].c_str());
        file_set = -1;
        description = contents[1];
        filename = contents[2];
      } else if(contents_size == 2) {
        time_set = -1;
        file_set = -1;
        description = contents[0];
        filename = contents[1];
      }
      m_var[j] = EnsightVariable(typevar,time_set,file_set,description,filename);
    }
    //======================
    // Read TIME section
    //======================
    /*
      This is an optional section for steady state cases. For transient cases, it is required. It describes time set information.
      Below are three example usages:

      TIME
      time set:               ts [description]
      number of steps:        ns
      filename start number:  fs
      filename increment:     fi
      time values:            time_1 time_2 .... time_ns
      TIME
      time set:               ts [description]
      number of steps:        ns
      filename numbers:       fn
      time values:            time_1 time_2 .... time_ns
      TIME
      time set:               ts [description]
      number of steps:        ns
      filename numbers file:  fnfilename
      time values file:       tvfilename

      where:
      ts = timeset number. This is the number referenced in the GEOMETRY and VARIABLE sections. description = optional timeset
      description which will be shown in userinterface.
      ns = number of transient steps
      fs = the number to replace the “*” wildcards in the filenames, for the first step
      fi = the increment to fs for subsequent steps
      time = the actual time values for each step, each of which must be separated by a white space and which may continue on
      the next line if needed
      fn = a list of numbers or indices, to replace the “*” wildcards in the filenames.
      fnfilename = name of file containing ns filename numbers (fn).
      tvfilename = name of file containing the time values(time_1 ... time_ns).
      */

    //!\todo Several time sets

    m_time.resize(1);
    int ilgn,num_steps = 0;
    int filename_start_number = 0;
    int filename_increment = 0;
    //
    ilgn = m_sectionTime+1;
    found = m_line[ilgn].find(':');
    typevar = m_line[ilgn].substr(0,found);
    restofline = m_line[ilgn].substr(found+1,m_line[ilgn].size()-found-1);
    split(restofline,contents," ");
    if(typevar == "time set") {
      time_set = std::atoi(contents[0].c_str());
    } else {
      std::ostringstream msg;
      msg << "Read: " << typevar << ", Expected: time set";
      FEL_ERROR(msg.str())
    }
    ilgn = m_sectionTime+2;
    found = m_line[ilgn].find(':');
    typevar = m_line[ilgn].substr(0,found);
    restofline = m_line[ilgn].substr(found+1,m_line[ilgn].size()-found-1);
    split(restofline,contents," ");
    if(typevar == "number of steps"){
      num_steps = std::atoi(contents[0].c_str());
    } else {
      std::ostringstream msg;
      msg << "Read: " << typevar << ", Expected: number of steps ";
      FEL_ERROR(msg.str())
    }
    ilgn = m_sectionTime+3;
    found = m_line[ilgn].find(':');
    typevar = m_line[ilgn].substr(0,found);
    restofline = m_line[ilgn].substr(found+1,m_line[ilgn].size()-found-1);
    //StringSplit(restofline," \t",contents);// delimiter is either a space or a tab

    split(restofline,contents," ");
    if(typevar == "filename start number") {
      filename_start_number = std::atoi(contents[0].c_str());
    } else {
      std::ostringstream msg;
      msg << "Read: " << typevar << ", Expected: file name start number";
      FEL_ERROR(msg.str())
    }
    ilgn = m_sectionTime+4;
    found = m_line[ilgn].find(':');
    typevar = m_line[ilgn].substr(0,found);
    restofline = m_line[ilgn].substr(found+1,m_line[ilgn].size()-found-1);
    split(restofline,contents," ");
    if(typevar == "filename increment")
      filename_increment = std::atoi(contents[0].c_str());
    else {
      std::ostringstream msg;
      msg << "Read: " << typevar << ", Expected: file name increment";
      FEL_ERROR(msg.str())
    }
    //
    m_time[0] = EnsightTimeBis(time_set,num_steps,filename_start_number,filename_increment);
    //
    ilgn = m_sectionTime+5;
    found = m_line[ilgn].find(':');
    typevar = m_line[ilgn].substr(0,found);
    restofline = m_line[ilgn].substr(found+1,m_line[ilgn].size()-found-1);
    split(restofline,contents," ");
    if(typevar == "time values") {
      std::stringstream buftime;
      for(unsigned int iline = m_sectionTime+6; iline< m_line.size(); iline++) {
        buftime << m_line[iline];
      }
      //	std::cout	<< buftime.str()<< std::endl;
      for(int j=0; j<num_steps; j++) {
        buftime >> m_time[0].timeValue(j);
      }
    } else {
      std::ostringstream msg;
      msg << "Read: " << typevar << ", Expected: time values";
      FEL_ERROR(msg.str())
    }

    //======================
    // Read FILE section
    //======================
    /*
      This section is optional. It's used for multiple time step data written with the single-file option. This section
      will specify the number of time steps for each file, for each data entity ( meaning for each geometry and each
      variable, model and/or measured).
      If the operation system imposed a maximum file size limit, and the data exceeds this limit, then a data entity
      will use as many continuation files as needed. Each file set corresponds to one and only one time set, but a time
      set may be referenced by many file sets.
      Usage:
      FILE
      file set:                    fs
      filename index:              fi # Note: only used when data continues in other files
      number of steps:             ns

      where:
      fs = file set number. This is the same number referenced in the GEOMETRY and VARIABLE sections.
      ns = number of transient steps.
      fi = file index number in the file name ( This replaces "*" in the filenames ).
      Material Section
      */

    //======================
    // Read MATERIAL section
    //======================
    /*
      Material Section

      This is an optional section. It provides information for the material interface part case. Currently, as of version 8.2, EnSight supports a single material set.
      Usage:
      MATERIAL
      material set number:          ms [description]
      material id count:            nm
      material id numbers:          matno_1 matno_2 ... matno_nm
      material id names:            matdesc_1 mat_2 ... mat_nm
      material id per element:      [ts] [fs] filename
      material mixed ids:           [ts] [fs] filename
      material mixed values:        [ts] [fs] filename
      # optional species parameters
      species id count:             ns
      species id numbers:           spno_1 spno_2 … spno_ns
      species id names:             spdesc_1 spdesc_2 … spdesc_ns
      species per material counts:  sp_1 sp_2 … sp_nm
      species per material lists:   matno_1_sp_1 matno_1_sp_2 … matno_1_sp_sp_1
      matno_2_sp_1 matno_2_sp_2 … matno_2_sp_sp_2
      …
      matno_nm_sp_1 matno_nm_sp2 … matno_nm_sp_sp_nm
      (Note: above concatenated lists do not have to be on
      separate lines.)

      species element values:       [ts] [fs] sp_filename

      where:
      ts = The time set number, the same as declared in the TIME section. Only required for transient materials.
      fs = The file set number, the same as declared in the FILE section. ( Note: if fs is specified, then ts becomes mandatory vs optional.
      ms = The material set number, ( although at this time only one material set is supported ).
      nm = The number of materials for this set.
      matno = The material number used from the material and mixed-material id file. There is a total of nm of these. Non-positive materials are all grouped together into a null-material.
      matdesc = The GUI's material description or the nm matno above.
      filename = The material filename.
      ns = The number of species for the set.
      spno = The "species per material list:" specification. There should be ns of these positive integers.
      spdesc = The GUI description corresponding to the ns spno's.
      spm = The "number of species per material number" (matno); an entry of "0" is valid if no species exist for a given material
      matno_#m_sp = Specie id number ( spno) list member for this material number id ( matno ). If no species for this material, then proceed to next material that has species.
      sp_filename = The filename of the appropriate "species element values:" file.
      */

    //======================
    // Read BLOCK_CONTINUATION section
    //======================
    /*
      Block Continuation Section

      This is an optional section, used for grouping structured blocks together. The purpose of this is to allow the capability to be able to read N number of files using M number of cluster notes, where N > M. The filenames of the geometry and variables must comtain "%" wildcards if this option is used.

      Usage:

      BLOCK_CONTINUATION
      number of sets:                 ns
      filename start number:          fs
      filename increment:             fi

      where:
      ns = The number of contiguous partioned structured files.
      fs = the number to replace the "%" wildcard in the geometry and variable filename, for the first set.
      fi = the increment to fs for subsequent sets.


      */
  }
}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightCase::geometryFile() const
{
  if(m_geo_file_wildcard!="") {
    std::ostringstream msg;
    msg << "Geometry file contains wildcard characters " << m_geo_file_wildcard
        << "Use EnsightCase::geometryFile(int i) to specify the time step";
    FEL_ERROR(msg.str())
  }
  return m_geo_file;
}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightCase::geometryFile(int i) const
{
  if(m_geo_file_wildcard == "") {
    return m_geo_file;
  } else {
    return ReplaceWilcardByInt(m_geo_file_wildcard, i, '*');
  }
}

/***********************************************************************************/
/***********************************************************************************/

int EnsightCase::variableIndex(std::string description) const
{
  for(unsigned int i=0; i<m_var.size(); i++) {
    if(m_var[i].description()==description) return i;
  }
  std::ostringstream msg;
  msg << "Variable " << description << " is not in the case file";
  FEL_ERROR(msg.str())
  return 0;
}

/***********************************************************************************/
/***********************************************************************************/

int EnsightCase::timeIndex(double time) const
{
  return m_time[0].timeIndex(time);
}

/***********************************************************************************/
/***********************************************************************************/

int EnsightCase::getNumNodeFromGeoFile(std::string dir) const
{
  char line[1024];
  int numNode;
  std::string geofilepath = dir + "/" + geometryFile();
  std::ifstream geofile(geofilepath.c_str());
  if(!geofile) {
    std::ostringstream msg;
    msg << "Cannot open " << geometryFile();
    FEL_ERROR(msg.str())
  }
  for(int i=0; i<5; i++) geofile.getline(line,1024);
  geofile >> numNode;
  return numNode;
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::SetDomainDim_case(int domainDim)
{
  m_domainDim = domainDim;
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::SetCreateNormalTangent_case(bool createNormalTangent)
{
  m_createNormalTangent = createNormalTangent;
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::readScalar(const std::string& fileName, double* vec,
                              felInt size)
{
  FILE* pFile = fopen (fileName.c_str(),"r");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to read "+fileName +".");
  }
  char str[80];
  std::string display;
  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_ERROR("Failed to read str");
  }
  display = str;//scalar

  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_ERROR("Failed to read str");
  }
  display = str;//per

  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_ERROR("Failed to read str");
  }
  display = str;//node

  //Read value
  int count = 0;
  for ( felInt i = 0; i < size; i++) {
    if ( fscanf(pFile,"%lf", &vec[i]) !=1 ) {
      FEL_ERROR("Failed to read scalar");
    }
    //      std::cout << vec[i] << std::endl;
    if ( count == 6 ) {
      if ( fscanf( pFile, "\n" ) != 0) {
        FEL_ERROR("Failed to read");
      }
      count=0;
    }
    count++;
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::readVector(const std::string& fileName, double* vec,
                              felInt size)
{
  FILE* pFile = fopen (fileName.c_str(),"r");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to read "+fileName +".");
  }
  char str[80];
  std::string display;
  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_WARNING("Failed to read str");
  }
  display = str;
  if ( fscanf(pFile,"%s", str) !=1 ) {
    FEL_WARNING("Failed to read str");
  }
  display = str;
  if ( fscanf(pFile,"%s", str) !=1) {
    FEL_WARNING("Failed to read str");
  }
  display = str;
  //Read value
  int count = 0;
  for ( felInt i = 0; i < size; i++) {
    if ( fscanf(pFile,"%lf", &vec[i]) !=1 ) {
      FEL_WARNING("Failed to read vec");
    }
    if ( count == 6 )  {
      if ( fscanf( pFile, "\n" ) !=0 ) {
        FEL_WARNING("Failed to read");
      }
      count=0;
    }
    count++;
  }
  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::readVariable(int idVariable,int indexTime, double* vec, felInt size)
{
  if (m_directory.at(m_directory.length()-1) != '/') m_directory += '/';
  const std::string fileName = m_directory + m_var[idVariable].variableFileInput(indexTime);
  const std::string fileNameExt = fileName.substr( fileName.find_last_of( '.' ) +1);
  if (fileNameExt == "scl") {
    readScalar(fileName, vec, size);
  } else if (fileNameExt == "vct") {
    readVector(fileName, vec, size);
  } else {
    FEL_ERROR("This type of file is not readable at the moment.");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::addVariable(const EnsightVariable::TypeVar& type, const std::string& nameVariable,
                              const double* solution, felInt size, bool manageMemory)
{
  EnsightVariable var(type,nameVariable, solution,size,manageMemory);
  m_var.push_back(var);
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::writeVariable(const std::string& outputDirectory, const std::string& nameVariable, int indexTime)
{
  for (std::size_t iVar = 0; iVar < m_var.size(); iVar++) {
    if (strcmp(m_var[iVar].description().c_str(),nameVariable.c_str()) == 0) {
      m_var[iVar].write(outputDirectory, indexTime);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::writeCaseFile(const std::string& outputDirectory,
                                const std::string& nameCase,
                                int optionMultiCase)
{
  // int domainDim = domainDim();
  if (optionMultiCase == 1) {
    for (std::size_t iVar = 0; iVar < m_var.size(); iVar++) {
      FILE * pFile;
      const std::string fileName = outputDirectory + "/" + m_var[iVar].description() +".case";
      pFile = fopen (fileName.c_str(),"w");
      if (pFile==nullptr) {
        FEL_ERROR("Impossible to write "+fileName+" case file");
      }

      fprintf( pFile,"FORMAT\n");
      if(FelisceParam::instance().outputFileFormat == ENSIGHT6)
        fprintf( pFile,"type: ensight\n");
      else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD)
        fprintf( pFile,"type: ensight gold\n");
      else
        FEL_ERROR("Unknown output file format");

      case_mesh_by_variable(pFile, iVar);
      case_variable_by_variable( pFile, iVar);
      if (m_createNormalTangent) case_geometricVec(pFile);
      case_time_section( pFile);
      fclose( pFile);
    }
  } else if(optionMultiCase == 2) {
    FILE * pFile;
    const std::string fileName = outputDirectory + "/" + nameCase +".case";
    pFile = fopen (fileName.c_str(),"w");
    if (pFile==nullptr) {
      const std::string command = "mkdir -p " + outputDirectory;
      const int ierr = system(command.c_str());
      if (ierr>0) {
        FEL_ERROR("Impossible to write "+fileName+"  medit mesh");
      }
      pFile = fopen (fileName.c_str(),"w");
    }

    fprintf( pFile,"FORMAT\n");
    if(FelisceParam::instance().outputFileFormat == ENSIGHT6)
      fprintf( pFile,"type: ensight\n");
    else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD)
      fprintf( pFile,"type: ensight gold\n");
    else
      FEL_ERROR("Unknown output file format");

    case_mesh_multiple_section(pFile);
    case_variable_section( pFile );;
    if (m_createNormalTangent) case_geometricVec(pFile);
    case_time_section( pFile);
    fclose( pFile);
  } else {
    FILE * pFile;
    const std::string fileName = outputDirectory + "/" + nameCase +".case";
    pFile = fopen (fileName.c_str(),"w");
    if (pFile==nullptr) {
      const std::string command = "mkdir -p " + outputDirectory;
      const int ierr = system(command.c_str());
      if (ierr>0) {
        FEL_ERROR("Impossible to write "+fileName+"  medit mesh");
      }
      pFile = fopen (fileName.c_str(),"w");
    }

    fprintf( pFile,"FORMAT\n");
    if( (FelisceParam::instance().outputFileFormat == ENSIGHT6) ||  (FelisceParam::instance().outputFileFormat == ENSIGHT6BIN)) {
      fprintf( pFile,"type: ensight\n");
    } else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD) {
      fprintf( pFile,"type: ensight gold\n");
    } else {
      FEL_ERROR("Unknown output file format");
    }

    case_mesh_section(pFile);
    case_variable_section( pFile );
    if (m_createNormalTangent) case_geometricVec(pFile);
    case_time_section( pFile);
    fclose( pFile);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_mesh_section(FILE* pFile)
{
  fprintf( pFile, "GEOMETRY\n");
  const std::string name = "model: 1 "+ m_geo_file +"\n";
  fprintf( pFile, "%s", name.c_str());
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_mesh_multiple_section(FILE* pFile)
{
  fprintf( pFile, "GEOMETRY\n");
  const std::string name = "model: 1 "+ m_geo_file +".*****.geo\n";
  fprintf( pFile, "%s", name.c_str());
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_mesh_by_variable(FILE* pFile, int iVar)
{
  fprintf( pFile, "GEOMETRY\n");
  const std::string name = "model: 1 "+ m_var[iVar].description() +".geo\n";
  fprintf( pFile, "%s", name.c_str());
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_variable_section(FILE* pFile)
{
  std::string name;
  fprintf( pFile, "VARIABLE\n");

  for (std::size_t iVar = 0; iVar < m_var.size(); iVar++) {
    name = m_var[iVar].description();
    switch( m_var[iVar].type() ) {
      case EnsightVariable::ScalarPerNode:
        name = "scalar per node: 1 "+name+" "+name+".*****.scl\n";
        fprintf( pFile, "%s", name.c_str());
        break;
      case EnsightVariable::VectorPerNode:
        name = "vector per node: 1 "+name+" "+name+".*****.vct\n";
        fprintf( pFile, "%s", name.c_str());
        break;
      case EnsightVariable::Unknown:
        break;
        // Default case should appear with a warning at compile time instead of an error in runtime
        // (that's truly the point of using enums as switch cases)
        //  default:
        //          FEL_ERROR("It is not a valid type of variable for Ensight.");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_variable_by_variable(FILE* pFile, int iVar)
{
  std::string name;
  fprintf( pFile, "VARIABLE\n");
  name = m_var[iVar].description();
  switch( m_var[iVar].type() ) {
    case EnsightVariable::ScalarPerNode:
      name = "scalar per node: 1 "+name+" "+name+".*****.scl\n";
      fprintf( pFile, "%s", name.c_str());
      break;
    case EnsightVariable::VectorPerNode:
      name = "vector per node: 1 "+name+" "+name+".*****.vct\n";
      fprintf( pFile, "%s", name.c_str());
      break;
    case EnsightVariable::Unknown:
      break;
      //    default:
      //      FEL_ERROR("It is not a valid type of variable for Ensight.");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_geometricVec(FILE* pFile)
{
  std::string  name;
  for (int i=0 ; i<m_domainDim ; i++) {
    name = "tangentVector" + std::to_string(i+1);
    name = "vector per node: 1 "+name+" "+name+".vct\n";
    fprintf( pFile, "%s", name.c_str());
  }
  name = "normalVector";
  name = "vector per node: 1 "+name+" "+name+".vct\n";
  fprintf( pFile, "%s", name.c_str());
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::case_time_section(FILE* pFile)
{
  fprintf( pFile,"TIME\n");
  fprintf( pFile, "time set: %d\n", 1);
  fprintf( pFile,"number of steps: %d \n", m_time[0].nbSteps());
  fprintf( pFile, "filename start number: %d\n", m_time[0].fileNameStartNumber());
  fprintf( pFile, "filename increment: %d\n", m_time[0].fileNameIncrement());
  fprintf( pFile, "time values:\n");

  int count=0;

  for (int i=0; i < m_time[0].nbSteps(); ++i) {
    fprintf( pFile, "%12.5e",m_time[0].timeValue(i));
    ++count;
    if ( count == 6) {
      fprintf( pFile, "\n" );
      count=0;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightCase::postProcess(const std::string& outputDirectory,
                              const std::string& nameCase,
                              int optionMultiCase,
                              double& time)
{
  m_time[0].addTimeValue(time);
  writeCaseFile(outputDirectory,nameCase, optionMultiCase);
  const int indexTime = m_time[0].timeIndex(time);

  // Write variables.
  for (std::size_t iVar = 0; iVar < m_var.size(); iVar++)
    m_var[iVar].write(outputDirectory, indexTime);

  for (std::size_t iVar = 0; iVar < m_var.size(); iVar++) {
    m_var[iVar].clear();
  }
  m_var.clear();
}

/***********************************************************************************/
/***********************************************************************************/

EnsightVariable::EnsightVariable(const std::string& aType,int time_set,int file_set,
                                  const std::string& aDescription, const std::string& filename):
  m_type(Unknown),
  m_description(aDescription),
  m_filename(filename)
{
  IGNORE_UNUSED_ARGUMENT(time_set);
  IGNORE_UNUSED_ARGUMENT(file_set);
  //	if(m_verbose) std::cout << "EnsightVariable::  " << m_description << " on " << m_filename << std::endl;
  if(CountOccurenceChar(filename,'*')) {
    m_filename_wildcard = filename;
  } else {
    m_filename_wildcard = "";
    m_filename = filename;
  }
  if(aType == "scalar per node") m_type = ScalarPerNode;
  if(aType == "vector per node") m_type = VectorPerNode;
  if(m_type == Unknown) {
    std::ostringstream msg;
    msg << aType << "EnsightVariable:: Unknown type of variable";
    FEL_ERROR(msg.str())
  }
#undef DEBUGVAR
#ifdef DEBUGVAR
  std::cout << "type: "        << aType << std::endl;
  std::cout << "time set: "    << time_set << std::endl;
  std::cout << "file set: "    << file_set << std::endl;
  std::cout << "description: " << aDescription << std::endl;
  std::cout << "filename: "    << filename << std::endl;
#endif
}

/***********************************************************************************/
/***********************************************************************************/

EnsightVariable::EnsightVariable(const EnsightVariable::TypeVar& aType, const std::string& nameVariable, const double* solution, felInt aSize, bool manageMemory):
  m_type(aType),
  m_size(aSize),
  m_description(nameVariable),
  m_filename("none"),
  m_filename_wildcard("none"),
  m_solution(solution),
  m_manageMemory(manageMemory)
{

}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightVariable::variableFile() const 
{
  if(m_filename_wildcard!="") {
    std::ostringstream msg;
    msg << "Variable file name contains wildcard characters " << m_filename_wildcard
        << "Use EnsightVariable::variableFile(int i) to specify the time step";
    FEL_ERROR(msg.str())
  }
  return m_filename;
}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightVariable::variableFileOutput(const std::string& outputDirectory, int istep) 
{
  const std::string FileRootName = outputDirectory;
  const std::string fileName = FileRootName + "/" + m_description + "." + wildCard(istep);
  return fileName;
}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightVariable::variableFileInput(int istep) 
{
  const std::string string_step = std::to_string(istep);
  const std::string FileRootName = m_filename.substr( 0, m_filename.find_first_of( '.' ) );
  const std::string FileExt = m_filename.substr( m_filename.find_last_of( '.' ) +1);
  const std::string fileName = FileRootName + "." + wildCard(istep) + string_step + "." + FileExt;
  return fileName;
}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightVariable::description() const 
{
  return m_description;
}

/***********************************************************************************/
/***********************************************************************************/

std::string EnsightVariable::wildCard(int indexTime) 
{
  if (indexTime == 0)
    return "0000";
  else if (indexTime < 10)
    return "0000";
  else if (indexTime < 100)
    return "000";
  else if (indexTime < 1000)
    return "00";
  else if (indexTime < 10000)
    return "0";
  else
    return "";
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::write(const std::string& outputDirectory, int indexTime) 
{
  switch (m_type) {
    case ScalarPerNode:
      if(FelisceParam::instance().outputFileFormat == ENSIGHT6)
        writeScalarEnsight6(outputDirectory, indexTime);
      else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD)
        writeScalarEnsightGold(outputDirectory, indexTime);
      else if(FelisceParam::instance().outputFileFormat == ENSIGHT6BIN)
        writeScalarEnsight6Bin(outputDirectory, indexTime);
      else
        FEL_ERROR("Unknown file format");
      break;
    case  VectorPerNode :
      if(FelisceParam::instance().outputFileFormat == ENSIGHT6)
        writeVectorEnsight6(outputDirectory, indexTime);
      else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD)
        writeVectorEnsightGold(outputDirectory, indexTime);
      else if(FelisceParam::instance().outputFileFormat == ENSIGHT6BIN)
        writeVectorEnsight6Bin(outputDirectory, indexTime);
      else
        FEL_ERROR("Unknown file format");
      break;
    case Unknown:
      FEL_ERROR("Impossible to write this type of variable at the moment.");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::writeScalarEnsight6(const std::string& outputDirectory, int indexTime) 
{
  const std::string fileName = variableFileOutput(outputDirectory, indexTime) + std::to_string(indexTime) + ".scl";
  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to write "+fileName+"  scalar ensight solution");
  }

  int count=0;
  fprintf( pFile, "Scalar per node\n");
  for (felInt i=0; i < m_size; ++i) {
    fprintf(pFile,"%12.5e", m_solution[i] );
    ++count;
    if ( count == 6 ) {
      fprintf(pFile, "\n" );
      count=0;
    }
  }
  fprintf( pFile, "\n" );
  fclose( pFile );
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::writeScalarEnsight6Bin(const std::string& outputDirectory, int indexTime) 
{
  const std::string fileName = variableFileOutput(outputDirectory, indexTime) + std::to_string(indexTime) + ".scl";
  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to write "+fileName+"  scalar ensight solution");
  }

  char buffer[80];
  local_strncpy(buffer, "Scalar per node",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  float *scalar = new float[m_size];
  for(int i=0; i< m_size; i++)
    scalar[i] = static_cast<float>(m_solution[i]);

  fwrite(scalar,sizeof(float),m_size,pFile);
  fclose( pFile );
  delete[] scalar;
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::writeScalarEnsightGold(const std::string& outputDirectory, int indexTime) 
{
  const std::string fileName = variableFileOutput(outputDirectory, indexTime) + std::to_string(indexTime) + ".scl";
  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to write "+fileName+"  scalar ensight solution");
  }

  int label=0;
  fprintf( pFile, "Ensight Gold scalar per node\n");

  // loop over references
  for (auto refIt = m_ref2list.begin(); refIt != m_ref2list.end(); ++refIt) {
    label = refIt->first;
    fprintf(pFile, "part\n");
    fprintf(pFile, "%10d\n", label + 1);
    fprintf(pFile, "coordinates\n");

    for(std::size_t inode=0; inode < refIt->second.size(); ++inode) {
      fprintf(pFile,"%12.5e\n", m_solution[refIt->second[inode]]);
    }
  }

  fclose( pFile );
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::writeVectorEnsight6(const std::string& outputDirectory, int indexTime) 
{
  const std::string fileName = variableFileOutput(outputDirectory, indexTime) + std::to_string(indexTime) +".vct";
  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    FEL_ERROR("Impossible to write  "+fileName+" vectorial ensight solution");
  }
  int count=0;
  int nDimensions = 3;
  fprintf( pFile, "Vector per node\n");
  // [u_x_1, u_y_1, u_z_1,u_x_2, u_y_2, u_z_2, ..., u_x_dim, u_y_dim, u_z_dim ]
  for ( felInt i=0; i < m_size; ++i ) {
    for ( int j=0; j < nDimensions; ++j ) {
  //std::cout << m_solution[ i*nDimensions+j ] << std::endl;
      fprintf(pFile,"%12.5e", (float)m_solution[ i*nDimensions+j ] );
      ++count;

      if ( count == 6 ) {
        fprintf(pFile, "\n" );
        count=0;
      }
    }
  }
  fprintf( pFile, "\n" );
  fclose( pFile );
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::writeVectorEnsight6Bin(const std::string& outputDirectory, int indexTime) 
{
  const std::string fileName = variableFileOutput(outputDirectory, indexTime) + std::to_string(indexTime) +".vct";
  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr)
    FEL_ERROR("Impossible to write  "+fileName+" vectorial ensight solution");

  char buffer[80];
  local_strncpy(buffer, "Vector per node\n",80);
  fwrite(buffer, sizeof(char), 80, pFile);
  float* vector = new float[3*m_size];
  for(int i=0; i< 3*m_size; ++i)
    vector[i] = static_cast<float>(m_solution[i]);
  fwrite(vector,sizeof(float),3*m_size,pFile);
  fclose( pFile );
  delete [] vector;
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightVariable::writeVectorEnsightGold(const std::string& outputDirectory, int indexTime) 
{
  const std::string fileName = variableFileOutput(outputDirectory, indexTime) + std::to_string(indexTime) +".vct";
  FILE * pFile;
  pFile = fopen (fileName.c_str(),"w");
  if(pFile==nullptr)
    FEL_ERROR("Impossible to write  "+fileName+" vectorial ensight solution");

  std::size_t nDim = 3;
  int label = 0;
  fprintf(pFile, "Ensight Gold vector per node\n");

  // loop over references
  for (auto refIt = m_ref2list.begin(); refIt != m_ref2list.end(); ++refIt) {
    label = refIt->first;
    fprintf(pFile, "part\n");
    fprintf(pFile, "%10d\n", label + 1);
    fprintf(pFile, "coordinates\n");

    // [u_x_1, u_x_2, u_x_3, ..., u_y_1, u_y_2, u_y_3, ..., u_z_1, u_z_2, u_z_3, ...]
    for(std::size_t idim=0; idim<nDim; ++idim)
      for(std::size_t inode=0; inode < refIt->second.size(); ++inode)
        fprintf(pFile, "%12.5e\n", m_solution[refIt->second[inode]*nDim + idim]);
  }

  fclose(pFile);
}

/***********************************************************************************/
/***********************************************************************************/

EnsightTimeBis::EnsightTimeBis(int time_set,int num_steps,int filename_start_number,int filename_increment):
  m_num_steps(num_steps),
  m_filename_start_number(filename_start_number),
  m_filename_increment(filename_increment) 
{
  IGNORE_UNUSED_ARGUMENT(time_set);
  m_time_values.resize(m_num_steps);
}

/***********************************************************************************/
/***********************************************************************************/

int EnsightTimeBis::timeIndex(double time) const 
{
  for(int i=0; i<m_num_steps; i++) {
    if( std::fabs(time - m_time_values[i]) <= 150*DBL_EPSILON*( std::fabs(time)+std::fabs(m_time_values[i]) )) {
      return (i+m_filename_start_number);
    }
  }
  
  const std::string error_message = "EnsightTime::timeIndex : time " + std::to_string(time) + " is not in the time set";
  FEL_ERROR(error_message)
  return 0;
}

/***********************************************************************************/
/***********************************************************************************/

void EnsightTimeBis::addTimeValue(double& time) 
{
  m_time_values.push_back(time);
  m_num_steps++;
}

/***********************************************************************************/
/***********************************************************************************/

std::string Ensight::getKeyword(const ElementType eltType)
{
  if (eltType == GeometricMeshRegion::Prism9 ) { // Prism9
    return "penta6";
  } else if ( eltType == GeometricMeshRegion::Quad6 ) { // Quad6
    return "quad4";
  } else {
    return m_eltFelNameToEns6Pair[GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first].second;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::prepareElemIndexes(std::vector<std::vector<felInt>>& elem, const ElementType eltType)
{
  const int nVpEl = getNumberPointsElement(eltType);
  const std::size_t number_of_sub_elem = (eltType == GeometricMeshRegion::Prism9 || eltType == GeometricMeshRegion::Quad6) ? 2 : 1;
  if (elem.size() != number_of_sub_elem)
    elem.resize(number_of_sub_elem);
  for (std::size_t  i = 0; i < number_of_sub_elem; ++i)
    elem[i].resize(nVpEl, 0);
}

/***********************************************************************************/
/***********************************************************************************/

void Ensight::getElemIndexes(GeometricMeshRegion& mesh, const ElementType eltType, const felInt iel, std::vector<std::vector<felInt>>& elem, const felInt startindex, const bool incr_vert_id)
{
  if (eltType == GeometricMeshRegion::Prism9 ) { // Prism9
    std::vector<felInt> aux_elem(9, 0);
    mesh.getOneElement(eltType, iel, aux_elem, startindex, incr_vert_id);
    elem[0][0] = aux_elem[0];
    elem[0][1] = aux_elem[1];
    elem[0][2] = aux_elem[2];
    elem[0][3] = aux_elem[6];
    elem[0][4] = aux_elem[7];
    elem[0][5] = aux_elem[8];
    elem[1][0] = aux_elem[6];
    elem[1][1] = aux_elem[7];
    elem[1][2] = aux_elem[8];
    elem[1][3] = aux_elem[3];
    elem[1][4] = aux_elem[4];
    elem[1][5] = aux_elem[5];
  } else if ( eltType == GeometricMeshRegion::Quad6 ) { // Quad6
    std::vector<felInt> aux_elem(6, 0);
    mesh.getOneElement(eltType, iel, aux_elem, startindex, incr_vert_id);
    elem[0][0] = aux_elem[0];
    elem[0][1] = aux_elem[1];
    elem[0][2] = aux_elem[4];
    elem[0][3] = aux_elem[5];
    elem[1][0] = aux_elem[4];
    elem[1][1] = aux_elem[5];
    elem[1][2] = aux_elem[3];
    elem[1][3] = aux_elem[2];
  } else {
    mesh.getOneElement(eltType, iel, elem[0], startindex, incr_vert_id);
  }
}

/***********************************************************************************/
/***********************************************************************************/

int Ensight::getNumberPointsElement(const ElementType eltType)
{
  if (eltType == GeometricMeshRegion::Prism9 ) { // Prism9
    return 6;
  } else if ( eltType == GeometricMeshRegion::Quad6 ) { // Quad6
    return 4;
  } else {
    return GeometricMeshRegion::m_numPointsPerElt[eltType];
  }
}

/***********************************************************************************/
/***********************************************************************************/

felInt Ensight::getNumberOfElements(GeometricMeshRegion& mesh, const ElementType eltType, const int the_ref)
{
  if (eltType == GeometricMeshRegion::Prism9 || eltType == GeometricMeshRegion::Quad6 ) { // Prism9/Quad6
    return 2 * mesh.intRefToBegEndMaps[eltType][the_ref].second;
  } else {
    return mesh.intRefToBegEndMaps[eltType][the_ref].second;
  }
}

}
