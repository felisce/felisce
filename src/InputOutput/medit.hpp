//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

#ifndef _MEDIT_HPP
#define _MEDIT_HPP

// System includes
#include <limits>
#include <string>
#include <unordered_map>

// External includes
extern "C" {
#include <libmeshb7.h>
#undef call
  // call conflicts with boost/regex.hpp and will be remove from libmesh5.h
  // in future versions.

  extern int64_t GmfOpenMesh(const char *, int, ...);
  extern int GmfCloseMesh(int64_t);
  extern int64_t GmfStatKwd(int64_t, int, ...);
  extern int GmfGotoKwd(int64_t, int);
  extern int GmfSetKwd(int64_t, int, int64_t, ...);
  extern int GmfGetLin(int64_t, int, ...);
  extern int GmfSetLin(int64_t, int, ...);

}

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Core/util_string.hpp"

namespace felisce {

  /*!
   * \class Medit
   * \authors J. Castelneau, J. Foulon
   * \brief manage input/output with Medit Software
   * \date 12/05/2010
   *
   * Use libmesh5 which is library developped by Loic Marechal (equipe-projet GAMMA, INRIA)
   */

  class Medit
  {
    public:
      typedef GeometricMeshRegion::ElementType ElementType;
      typedef std::map<int, std::vector<felInt>> RefToElements_type;
      typedef std::unordered_map<std::string, std::pair<ElementType, GmfKwdCod> > StringToMeditPair_type;
      static constexpr double Infinity = std::numeric_limits<double>::infinity();
    private:
      static StringToMeditPair_type   m_eltFelNameToMeditPair; //!<std::unordered_map to link element geometric name in felisce to medit format
      std::string m_inputMesh;
      std::string m_outputMesh;
      std::string m_meshDir;
      std::string m_resultDir;
      int m_meshVersion; //!<value of the mesh version only in medit format
      void initMap();//! Function to initialize m_eltFelNameToMeditPair
    public:
      Medit() = default;
      Medit(const std::string& inputMesh, const std::string& outputMesh,
            const std::string& meshDir, const std::string& resultDir);
      void initialize(const std::string& inputMesh, const std::string& outputMesh,
                      const std::string& meshDir, const std::string& resultDir);

    /**
     * @brief This is an auxiliary method to read the vertices of a mesh
     * @param rMesh The mesh to fill the vertices
     * @param spaceUnit The rescale factor
     * @param InpMsh The type of input mesh
     * @ttparam TType To consider float or double
     * @ttparam TReadRefNodes If we read the references of the nodes
     */
    template<class TType, bool TReadRefNodes>
    void ReadVertices(
      GeometricMeshRegion& rMesh,
      const double spaceUnit,
      int64_t& InpMsh,
      RefToElements_type* RefToElementsMaps
      );

    std::string& outputMesh() {
      return m_outputMesh;
    }

    ~Medit() = default;

    //! Copy constructor, to avoid warning due to user-declared destructor.
    Medit(const Medit&) = default;

      void readerMedit(GeometricMeshRegion& mesh,double spaceUnit, const std::size_t verbose = 0);//!<It reads INRIA MESH file using Loic Marechal's libmesh5
      void writerMedit(GeometricMeshRegion& mesh);//!<It writes INRIA MESH file using Loic Marechal's libmesh5
      void writeSolution(double& time,int iteration, const int & typeVariable, const std::string nameVariable, const double* solution, felInt size, int dimension);
  };

}

#endif
