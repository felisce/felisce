//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "InputOutput/medit.hpp"

namespace felisce
{

Medit::StringToMeditPair_type Medit::m_eltFelNameToMeditPair;

Medit::Medit(const std::string& inputMesh,const std::string& outputMesh,const std::string& meshDir,const std::string& resultDir):
  m_inputMesh(inputMesh),
  m_outputMesh(outputMesh),
  m_meshDir(meshDir),
  m_resultDir(resultDir),
  m_meshVersion(0) {
  initMap();
}

/***********************************************************************************/
/***********************************************************************************/

void Medit::initialize(const std::string& inputMesh,const std::string& outputMesh,const std::string& meshDir,const std::string& resultDir) {
  m_inputMesh=inputMesh;
  m_outputMesh=outputMesh;
  m_meshDir=meshDir;
  m_resultDir=resultDir;
  m_meshVersion=0;
  initMap();
}

/***********************************************************************************/
/***********************************************************************************/

void Medit::initMap() {
  //! std::unordered_map for medit mesh
  //! felisce type name -> < enum, medit keyword >
  if (FelisceParam::instance().readNodesReferences) {
    m_eltFelNameToMeditPair["Vertices"]   = std::make_pair( GeometricMeshRegion::Nod,    GmfVertices         );
  }
  m_eltFelNameToMeditPair["Segments2"]    = std::make_pair( GeometricMeshRegion::Seg2,   GmfEdges            );
  m_eltFelNameToMeditPair["Segments3"]    = std::make_pair( GeometricMeshRegion::Seg3,   GmfEdgesP2          );
  m_eltFelNameToMeditPair["Triangles3"]   = std::make_pair( GeometricMeshRegion::Tria3,  GmfTriangles        );
  m_eltFelNameToMeditPair["Triangles6"]   = std::make_pair( GeometricMeshRegion::Tria6,  GmfTrianglesP2      );
  m_eltFelNameToMeditPair["Quadrangles4"] = std::make_pair( GeometricMeshRegion::Quad4,  GmfQuadrilaterals   );
  m_eltFelNameToMeditPair["Quadrangles6"] = std::make_pair( GeometricMeshRegion::Quad6,  GmfQuadrilaterals6  );
  m_eltFelNameToMeditPair["Quadrangles9"] = std::make_pair( GeometricMeshRegion::Quad9,  GmfQuadrilateralsQ2 );
  m_eltFelNameToMeditPair["Tetrahedra4"]  = std::make_pair( GeometricMeshRegion::Tetra4, GmfTetrahedra       );
  m_eltFelNameToMeditPair["Tetrahedra10"] = std::make_pair( GeometricMeshRegion::Tetra10,GmfTetrahedraP2     );
  m_eltFelNameToMeditPair["Prisms6"]      = std::make_pair( GeometricMeshRegion::Prism6, GmfPrisms           );
  m_eltFelNameToMeditPair["Prisms9"]      = std::make_pair( GeometricMeshRegion::Prism9, GmfPrisms9          );
  m_eltFelNameToMeditPair["Hexahedra8"]   = std::make_pair( GeometricMeshRegion::Hexa8,  GmfHexahedra        );
  m_eltFelNameToMeditPair["Hexahedra12"]  = std::make_pair( GeometricMeshRegion::Hexa12, GmfHexahedra12      );
  m_eltFelNameToMeditPair["Hexahedra27"]  = std::make_pair( GeometricMeshRegion::Hexa27, GmfHexahedraQ2      );
}

/***********************************************************************************/
/***********************************************************************************/

namespace { // anonymous
  // This function writes on \a stream a warning when mesh version is not supported by architecture
  // Compilation error when \a SizeOfLongT is neither 4 (32 bits) nor 8 (64 bits)
  template<int SizeOfLongT>
  void warnInvalidArchitecture(std::ostream& stream) {
    switch(SizeOfLongT) {
    case 4:
      stream << " You are trying to use MeshVersion = 3 with a 32 bit version of Arch.";
      break;
    case 8:
      // Do nothing
      break;
    default:
      felisce_error("Unexpected size of long!", __FILE__, __LINE__);
    }
  }

} // namespace anonymous


/***********************************************************************************/
/***********************************************************************************/

template<class TType, bool TReadRefNodes>
void Medit::ReadVertices(
  GeometricMeshRegion& rMesh,
  const double spaceUnit,
  int64_t& InpMsh,
  RefToElements_type* RefToElementsMaps
  )
{
  // Dimension
  const auto dimension = rMesh.numCoor();

  // Node reference
  int node_ref;

  // Coordinates
  TType xcoor, ycoor, zcoor = 0.0;

  // Reading coordinates
  GmfGotoKwd(InpMsh, GmfVertices);

  // Retrive the correct dimension point
  const std::function<void(int64_t&, TType&, TType&, TType&, int& )> retrieve_3d_vertex =
  [](int64_t& InpMsh, TType& xcoor, TType& ycoor, TType& zcoor, int& node_ref) {GmfGetLin(InpMsh, GmfVertices, &xcoor, &ycoor, &zcoor, &node_ref );};
  const std::function<void(int64_t&, TType&, TType&, TType&, int& )> retrieve_2d_vertex =
  [](int64_t& InpMsh, TType& xcoor, TType& ycoor, TType& zcoor, int& node_ref) {
     IGNORE_UNUSED_ARGUMENT(zcoor);
    GmfGetLin(InpMsh, GmfVertices, &xcoor, &ycoor, &node_ref );
    };

  const auto* p_retrieve_vertex = dimension == 3 ? &retrieve_3d_vertex : &retrieve_2d_vertex;

  // Retrieve coordinates
  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  rMesh.intRefToEnum();
  std::vector<felInt> elem(1);
  for(felInt iv=0; iv< rMesh.numPoints(); iv++) {
    auto& r_point = rMesh.listPoint(iv);
    (*p_retrieve_vertex)(InpMsh, xcoor, ycoor, zcoor, node_ref);
    r_point.x() = xcoor*spaceUnit;
    r_point.y() = ycoor*spaceUnit;
    r_point.z() = zcoor*spaceUnit;

    // We add the node to the mesh
    if constexpr(TReadRefNodes) {
      elem[0] = iv + 1;
      rMesh.setOneElement(GeometricMeshRegion::Nod, iv, elem, true);
      RefToElementsMaps[ GeometricMeshRegion::Nod ][ node_ref ].push_back( iv );
      intRefToEnum[ node_ref ].insert( GeometricMeshRegion::Nod );
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

// Explicit instantation
template void Medit::ReadVertices<float,  true >(GeometricMeshRegion& rMesh, const double spaceUnit, int64_t& InpMsh, RefToElements_type* RefToElementsMaps);
template void Medit::ReadVertices<double, true >(GeometricMeshRegion& rMesh, const double spaceUnit, int64_t& InpMsh, RefToElements_type* RefToElementsMaps);
template void Medit::ReadVertices<float,  false>(GeometricMeshRegion& rMesh, const double spaceUnit, int64_t& InpMsh, RefToElements_type* RefToElementsMaps);
template void Medit::ReadVertices<double, false>(GeometricMeshRegion& rMesh, const double spaceUnit, int64_t& InpMsh, RefToElements_type* RefToElementsMaps);

/***********************************************************************************/
/***********************************************************************************/

// It Reads INRIA MESH file using Loic Marechal's libmesh5
//! \todo manage verbose and errors
void Medit::readerMedit(GeometricMeshRegion& mesh, double spaceUnit, const std::size_t verbose) 
{
  std::string fileNameMedit, fileNameRoot, fileName;
  fileNameMedit = m_inputMesh;
  fileNameRoot  = m_meshDir;
  fileName = fileNameRoot + fileNameMedit;

  int64_t InpMsh = -1;
  int the_ref = 0;

  if (verbose>0) std::cout << "Reading from file "<< fileName << std::endl;

  // one needs to convert the std::string into char* (not const char*)
  char *filenameChar = c_string( fileName );
  if( !( InpMsh = GmfOpenMesh(filenameChar, GmfRead, &m_meshVersion, &mesh.numCoor()) ) ) {
    std::ostringstream msg;
    msg << "Error in reading mesh: File " << fileName << " not found or locked.";

    // Display something only if invalid architecture
    if( m_meshVersion == 3)
      warnInvalidArchitecture<sizeof(void*)>(msg);

    msg << std::endl;
    delete [] filenameChar;
    FEL_ERROR(msg.str());
  }

  if (verbose>1) {
    std::cout << "LIBMESH5: Reading mesh file" << std::endl;
    std::cout << "InputMesh : idx = " << InpMsh << ", version = " << m_meshVersion <<", numCoor = " << mesh.numCoor() << "." << std::endl;
  }

  if (mesh.numCoor() != 2 && mesh.numCoor() != 3) {
    std::ostringstream msg;
    msg << "Error in reading mesh: File " << fileName << ". "
        << "Expecting mesh with 2 or 3 coordinates (it is " << mesh.numCoor() << ")."
        << std::endl;
    delete [] filenameChar;
    FEL_ERROR(msg.str());
  }

  //------------------------
  // read the sizes for memory allocation
  const felInt number_of_nodes = GmfStatKwd(InpMsh, GmfVertices);
  if ( GmfVertices > 10000000 ) {
    FEL_ERROR(" implicite conversion, GmfVertice is stocked with int type in felisce, whereas libmesh use int64_t type " );
  }

  // Prepare mesh
  mesh.listPoints().resize( number_of_nodes, Point( 0. ) );
  for ( auto it_eltype = m_eltFelNameToMeditPair.begin(); it_eltype != m_eltFelNameToMeditPair.end(); ++it_eltype) {

    ElementType eltType = it_eltype->second.first;
    GmfKwdCod   keyWord = it_eltype->second.second;
    mesh.numElements(eltType) = GmfStatKwd(InpMsh, keyWord);

    // Allocate memory
    if ( mesh.numElements(eltType) != 0 ) {
      mesh.allocateElements(eltType);
    }
  }

  RefToElements_type RefToElementsMaps[ GeometricMeshRegion::m_numTypesOfElement ];

  //------------------------
  // Read the vertices
  if (FelisceParam::instance().readNodesReferences) {
    if ( m_meshVersion == 1 ) {
      ReadVertices<float, true>(mesh, spaceUnit, InpMsh, RefToElementsMaps);
    } else {
      ReadVertices<double, true>(mesh, spaceUnit, InpMsh, RefToElementsMaps);
    }
  } else {
    if ( m_meshVersion == 1 ) {
      ReadVertices<float, false>(mesh, spaceUnit, InpMsh, RefToElementsMaps);
    } else {
      ReadVertices<double, false>(mesh, spaceUnit, InpMsh, RefToElementsMaps);
    }
  }

  //------------------------
  // read the elements:
  // do it separately for each type as the nb of arguments changes.
  // we put the reference right after the element. thus a triangle needs 3+1 int.
  ElementType eltType;
  GmfKwdCod   keyWord;
  felInt numElems;
  int nVpEl;
  std::vector<felInt> elem;
  const bool decr_vert_id = true;

  GeometricMeshRegion::IntRefToElementType_type& intRefToEnum =  mesh.intRefToEnum();

  //------------
  // Hexahedra27:
  eltType = m_eltFelNameToMeditPair["Hexahedra27"].first;
  keyWord = m_eltFelNameToMeditPair["Hexahedra27"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Hexahedra27 (27 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);

    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0],  &elem[1],  &elem[2],  &elem[3],
                &elem[4],  &elem[5],  &elem[6],  &elem[7],
                &elem[8],  &elem[9],  &elem[10], &elem[11],
                &elem[12], &elem[13], &elem[14], &elem[15],
                &elem[16], &elem[17], &elem[18], &elem[19],
                &elem[20], &elem[21], &elem[22], &elem[23],
                &elem[24], &elem[25], &elem[26],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Hexahedra12:
  eltType = m_eltFelNameToMeditPair["Hexahedra12"].first;
  keyWord = m_eltFelNameToMeditPair["Hexahedra12"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Hexahedra12 (12 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[ 2], &elem[ 3],
                &elem[4], &elem[5], &elem[ 6], &elem[ 7],
                &elem[8], &elem[9], &elem[10], &elem[11],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Hexahedra8:
  eltType = m_eltFelNameToMeditPair["Hexahedra8"].first;
  keyWord = m_eltFelNameToMeditPair["Hexahedra8"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Hexahedra8 (8 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5], &elem[6], &elem[7],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Prisms9:
  eltType = m_eltFelNameToMeditPair["Prisms9"].first;
  keyWord = m_eltFelNameToMeditPair["Prisms9"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Prisms9 (9 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5], &elem[6], &elem[7],
                &elem[8],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Prisms6:
  eltType = m_eltFelNameToMeditPair["Prisms6"].first;
  keyWord = m_eltFelNameToMeditPair["Prisms6"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Prisms6 (6 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Tetrahedra10:
  eltType = m_eltFelNameToMeditPair["Tetrahedra10"].first;
  keyWord = m_eltFelNameToMeditPair["Tetrahedra10"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Tetrahedra10 (10 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5], &elem[6], &elem[7],
                &elem[8], &elem[9],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Tetrahedra4:
  eltType = m_eltFelNameToMeditPair["Tetrahedra4"].first;
  keyWord = m_eltFelNameToMeditPair["Tetrahedra4"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Tetrahedra4 (4 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }


  //------------
  // Quadrangles9:
  eltType = m_eltFelNameToMeditPair["Quadrangles9"].first;
  keyWord = m_eltFelNameToMeditPair["Quadrangles9"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Quadrangles9 (9 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5], &elem[6], &elem[7],
                &elem[8],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Quadrangles6:
  eltType = m_eltFelNameToMeditPair["Quadrangles6"].first;
  keyWord = m_eltFelNameToMeditPair["Quadrangles6"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // read the Quadrangles6 (6 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Quadrangles4:
  eltType = m_eltFelNameToMeditPair["Quadrangles4"].first;
  keyWord = m_eltFelNameToMeditPair["Quadrangles4"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Quadrangles4 (4 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Triangles6:
  eltType = m_eltFelNameToMeditPair["Triangles6"].first;
  keyWord = m_eltFelNameToMeditPair["Triangles6"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Triangles6 (6 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2], &elem[3],
                &elem[4], &elem[5],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Triangles3:
  eltType = m_eltFelNameToMeditPair["Triangles3"].first;
  keyWord = m_eltFelNameToMeditPair["Triangles3"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Triangles3 (3 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //------------
  // Segments3:
  eltType = m_eltFelNameToMeditPair["Segments3"].first;
  keyWord = m_eltFelNameToMeditPair["Segments3"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Segments3 (3 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1], &elem[2],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }
  //------------
  // Segments2:
  eltType = m_eltFelNameToMeditPair["Segments2"].first;
  keyWord = m_eltFelNameToMeditPair["Segments2"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // read the Segments2 (2 points)
    GmfGotoKwd(InpMsh, keyWord);
    elem.resize(nVpEl, 0);
    for(felInt iel = 0; iel < numElems; iel++) {
      GmfGetLin(InpMsh, keyWord,
                &elem[0], &elem[1],
                &the_ref);
      mesh.setOneElement(eltType, iel, elem, decr_vert_id);
      RefToElementsMaps[ eltType ][ the_ref ].push_back( iel );
      intRefToEnum[ the_ref ].insert( eltType );
    }
  }

  //-------------------------------
  // Close the mesh
  GmfCloseMesh(InpMsh);
  delete [] filenameChar;
  for (int ityp=0; ityp<GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    eltType = static_cast<ElementType>(ityp);
    mesh.reorderListElePerRef(RefToElementsMaps[eltType], eltType);
  }
  mesh.flagFormatMesh() = GeometricMeshRegion::FormatMedit;
  mesh.setDomainDim();
  mesh.setBagElementTypeDomain();
  mesh.setBagElementTypeDomainBoundary();
}

/***********************************************************************************/
/***********************************************************************************/

namespace { // anonymous

  enum { arch32 = 4, arch64 = 8 };

  template<int SizePointerT>
  void checkArchitectureVersion(int mesh_version) {
    switch(SizePointerT) {
    case arch32:
      if (mesh_version == 3)
        felisce_error("Error: You are trying to use MeshVersionFormatted = 3 with a 32 bit version of Arch!", __FILE__,__LINE__);
      break;
    case arch64:
      // Do nothing: all Medit versions are supported by 64 bits architecture.
      break;
    default:
      felisce_error("Error: architecture should be 32 or 64 bits!",__FILE__,__LINE__);
    }
  }

  //! Check whether the Medit version is supported by present architecture.
  void checkMeditVersionSupportedByArchitecture(int mesh_version) {
    checkArchitectureVersion<sizeof(void*)>(mesh_version);
  }

} // namespace anonymous

/***********************************************************************************/
/***********************************************************************************/

// It writes INRIA MESH file using Loic Marechal's libmesh5
//! \todo manage verbose and errors
void Medit::writerMedit(GeometricMeshRegion& mesh)
{
  std::string fileNameMesh, fileNameRoot, fileName;
  fileNameMesh = m_outputMesh;
  fileNameRoot = m_resultDir;
  fileName = fileNameRoot + fileNameMesh;
  int64_t OutMsh = -1;
  short verbose = 0;
  m_meshVersion = 2; //! \todo switch to meshVersion when read Geo File
  //< Why is mesh version hardcoded here? The tests just below are threfore all irrelevant; and it would be
  // nice anyway to let the user choose its Medit version.

  checkMeditVersionSupportedByArchitecture(m_meshVersion);

  if( m_meshVersion == 1 ) {
    felisce_error("Error: we do not write meshes in MeshVersionFormatted 1 (no floats)!",__FILE__,__LINE__);
  }
  // one needs to convert the std::string into char* (not const char*)
  char *fileNameMeshChar = c_string( fileName );
  FILE* pFile;
  pFile = fopen (fileName.c_str(),"w");
  if (pFile==nullptr) {
    std::string command = "mkdir -p " + m_resultDir;
    int ierr = system(command.c_str());
    if (ierr>0) {
      FEL_ERROR("Impossible to write "+fileName+"  medit mesh");
    }
    fclose(pFile);
  }

  const auto dimension = mesh.numCoor();
  if( !( OutMsh = GmfOpenMesh(fileNameMeshChar, GmfWrite, m_meshVersion, dimension) ) ) {
    std::string error="Error: mesh file "+fileNameMesh+" cannot be opened. Several reasons are possible... (See libmesh5!)";
    felisce_error(error.c_str(),__FILE__,__LINE__);
  }
  delete [] fileNameMeshChar;
  if (verbose>1) {
    std::cout << "LIBMESH5: Reading mesh file" << std::endl;
    std::cout << "OutputMesh : idx = " << OutMsh << ", version = " << m_meshVersion
          <<", numCoor = " << dimension << "." << std::endl;
  }

  // Dimension check
  if (dimension != 2 && dimension != 3) {
    std::string error =  "Error in writing mesh: File " + fileNameMesh + "Expecting mesh with 2 or 3 coordinates (it is "+std::to_string(dimension)+").";
    felisce_error(error.c_str(),__FILE__,__LINE__);
  }

  //------------------------
  ElementType eltType;
  GmfKwdCod   keyWord;
  std::vector<felInt> elem;
  felInt numElems = 0;
  int nVpEl = 0;
  felInt startindex = 0, numElemsPerRef = 0;
  int the_ref = 0;
  bool incr_vert_id = true;
  //------------------------

  //------------------------
  // Write the vertices
  const std::function<void(int64_t&, Point&, const int)> write_3d_vertex =
  [](int64_t& OutMsh, Point& point, const int node_ref) {GmfSetLin(OutMsh, GmfVertices, point.x(), point.y(), point.z(), node_ref);};
  const std::function<void(int64_t&, Point&, const int)> write_2d_vertex =
  [](int64_t& OutMsh, Point& point, const int node_ref) {GmfSetLin(OutMsh, GmfVertices, point.x(), point.y(), node_ref);};

  const auto* p_write_vertex = dimension == 3 ? &write_3d_vertex : &write_2d_vertex;
  GmfSetKwd(OutMsh, GmfVertices, mesh.numPoints() );
  elem.resize(1, 0);
  eltType = GeometricMeshRegion::Nod;
  std::size_t counter = 0;
  for ( auto& r_ref : mesh.intRefToBegEndMaps[eltType]) counter += r_ref.first + 1;
  if (counter > 0) {
    for ( auto it_ref = mesh.intRefToBegEndMaps[eltType].begin();it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
        the_ref = it_ref->first;
        startindex     =  it_ref->second.first;
        numElemsPerRef =  it_ref->second.second;
        for(felInt iv = 0; iv < numElemsPerRef; iv++) {
          mesh.getOneElement(eltType, iv, elem, startindex, incr_vert_id);
          (*p_write_vertex)(OutMsh, mesh.listPoint(elem[0] - 1), the_ref);
        }
    }
  } else {
    for(felInt iv = 0; iv < mesh.numPoints(); iv++) {
      (*p_write_vertex)(OutMsh, mesh.listPoint(iv), the_ref);
    }
  }

  //------------------------
  // write the elements:
  // do it separately for each type as the nb of arguments changes.
  // we put the reference right after the element. thus a triangle needs 3+1 int.

  //------------
  // Hexahedra27:
  eltType = m_eltFelNameToMeditPair["Hexahedra27"].first;
  keyWord = m_eltFelNameToMeditPair["Hexahedra27"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Hexahedra27 (27 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for ( auto it_ref = mesh.intRefToBegEndMaps[eltType].begin();it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0],  elem[1],  elem[2],  elem[3],
                  elem[4],  elem[5],  elem[6],  elem[7],
                  elem[8],  elem[9],  elem[10], elem[11],
                  elem[12], elem[13], elem[14], elem[15],
                  elem[16], elem[17], elem[18], elem[19],
                  elem[20], elem[21], elem[22], elem[23],
                  elem[24], elem[25], elem[26],
                  the_ref);
      }
    }
  }
  
  //------------
  // Hexahedra12:
  eltType = m_eltFelNameToMeditPair["Hexahedra12"].first;
  keyWord = m_eltFelNameToMeditPair["Hexahedra12"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Hexahedra12 (12 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {

      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[ 2], elem[ 3],
                  elem[4], elem[5], elem[ 6], elem[ 7],
                  elem[8], elem[9], elem[10], elem[11],
                  the_ref);
      }
    }
  }

  //------------
  // Hexahedra8:
  eltType = m_eltFelNameToMeditPair["Hexahedra8"].first;
  keyWord = m_eltFelNameToMeditPair["Hexahedra8"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Hexahedra8 (8 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {

      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5], elem[6], elem[7],
                  the_ref);
      }
    }
  }

  //------------
  // Prisms9:
  eltType = m_eltFelNameToMeditPair["Prisms9"].first;
  keyWord = m_eltFelNameToMeditPair["Prisms9"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Prisms9 (9 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5], elem[6], elem[7],
                  elem[8],
                  the_ref);
      }
    }
  }

  //------------
  // Prisms6:
  eltType = m_eltFelNameToMeditPair["Prisms6"].first;
  keyWord = m_eltFelNameToMeditPair["Prisms6"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Prisms6 (6 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5],
                  the_ref);
      }
    }
  }

  //------------
  // Tetrahedra10:
  eltType = m_eltFelNameToMeditPair["Tetrahedra10"].first;
  keyWord = m_eltFelNameToMeditPair["Tetrahedra10"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Tetrahedra10 (10 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5], elem[6], elem[7],
                  elem[8], elem[9],
                  the_ref);
      }
    }
  }

  //------------
  // Tetrahedra4:
  eltType = m_eltFelNameToMeditPair["Tetrahedra4"].first;
  keyWord = m_eltFelNameToMeditPair["Tetrahedra4"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Tetrahedra4 (4 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  the_ref);
      }
    }
  }

  //------------
  // Quadrangles9:
  eltType = m_eltFelNameToMeditPair["Quadrangles9"].first;
  keyWord = m_eltFelNameToMeditPair["Quadrangles9"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // write the Quadrangles9 (9 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5], elem[6], elem[7],
                  elem[8],
                  the_ref);
      }
    }
  }

  //------------
  // Quadrangles6:
  eltType = m_eltFelNameToMeditPair["Quadrangles6"].first;
  keyWord = m_eltFelNameToMeditPair["Quadrangles6"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];

  if ( numElems != 0 ) {
    // write the Quadrangles6 (6 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5],
                  the_ref);
      }
    }
  }

  //------------
  // Quadrangles4:
  eltType = m_eltFelNameToMeditPair["Quadrangles4"].first;
  keyWord = m_eltFelNameToMeditPair["Quadrangles4"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Quadrangles4 (4 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  the_ref);
      }
    }
  }

  //------------
  // Triangles6:
  eltType = m_eltFelNameToMeditPair["Triangles6"].first;
  keyWord = m_eltFelNameToMeditPair["Triangles6"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Triangles6 (6 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2], elem[3],
                  elem[4], elem[5],
                  the_ref);
      }
    }
  }

  //------------
  // Triangles3:
  eltType = m_eltFelNameToMeditPair["Triangles3"].first;
  keyWord = m_eltFelNameToMeditPair["Triangles3"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Triangles3 (3 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1], elem[2],
                  the_ref);
      }
    }
  }

  //------------
  // Segments2:
  eltType = m_eltFelNameToMeditPair["Segments2"].first;
  keyWord = m_eltFelNameToMeditPair["Segments2"].second;
  numElems = mesh.numElements(eltType);
  nVpEl = GeometricMeshRegion::m_numPointsPerElt[eltType];
  if ( numElems != 0 ) {
    // write the Segments2 (2 points)
    GmfSetKwd(OutMsh, keyWord, numElems );
    elem.resize(nVpEl, 0);
    for(auto it_ref = mesh.intRefToBegEndMaps[eltType].begin(); it_ref != mesh.intRefToBegEndMaps[eltType].end(); ++it_ref ) {
      the_ref = it_ref->first;
      startindex     =  it_ref->second.first;
      numElemsPerRef =  it_ref->second.second;
      for(felInt iel = 0; iel < numElemsPerRef; iel++) {
        mesh.getOneElement(eltType, iel, elem, startindex, incr_vert_id);
        GmfSetLin(OutMsh, keyWord,
                  elem[0], elem[1],
                  the_ref);
      }
    }
  }

  //-------------------------------
  // Close the mesh
  GmfCloseMesh(OutMsh);

}

/***********************************************************************************/
/***********************************************************************************/

void Medit::writeSolution(double& time,int iteration, const int & typeVariable, const std::string nameVariable, const double* solution,
                          felInt size, int dimension) {

  std::string fileName = m_resultDir + "/" + nameVariable + "." + std::to_string(iteration) + ".sol";

  FILE* pFile;
  pFile = fopen( fileName.c_str(),"w" );
  fprintf(pFile,"MeshVersionFormatted \n 2 \n");
  fprintf(pFile,"Dimension \n %d \n", dimension);
  fprintf(pFile,"SolAtVertices \n %d \n", size);
  switch (typeVariable) {
  case 0:
    fprintf(pFile,"1 1\n");
    for ( int i = 0; i < size; i++)
      fprintf(pFile, "%le \n", solution[i]);
    break;
  case 1:
    fprintf(pFile,"1 2\n");
    for ( int i = 0; i < size; i++) {
      for (int j = 0; j < dimension; j++)
        fprintf(pFile, "%le ", solution[i*3+j]);
      fprintf(pFile, "\n");
    }
    break;
  default:
    break;
  }
  fprintf(pFile,"Iterations \n %d \n Time \n %lf \n", iteration, time);
  fprintf(pFile,"End");
  fclose(pFile);
}
}
