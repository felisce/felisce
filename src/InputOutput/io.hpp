//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

#ifndef IO_HPP
#define IO_HPP

// System includes

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "InputOutput/medit.hpp"
#include "InputOutput/ensight.hpp"

namespace felisce
{

  enum class MeditVersion {DEV = 0, STABLE = 1};

/**
 * @class IO
 * @authors J.Castelneau & J.Foulon
 * @date 13/05/2010
 * @brief Class managing Input and output format (Ensight, Medit, ..)
 */
class IO
{
  private:
    std::string m_inputDirectory;
    std::string m_inputFile; //! input data file
    std::string m_inputMesh;
    std::string m_outputMesh;
    std::string m_meshDir;
    std::string m_resultDir;
    std::string m_prefixName;
    //!<Ensight object to read/write mesh with ensight format
    Ensight m_ens;
    //!<Medit object to read/write mesh with medit format
    Medit m_med;
  public:
    /// Pointer definition of IO
    FELISCE_CLASS_POINTER_DEFINITION(IO);

    /// Assigning stable version
    static constexpr MeditVersion MEDIT_VERSION = MeditVersion::STABLE;

    int typeInput;  // 0 : medit, 1: ensight
    int typeOutput; // 0 : medit 1: ensight
    IO();
    IO(const std::string& inputDirectory, const std::string& inputFile, const std::string& inputMesh,
       const std::string& outputMesh, const std::string& meshDir, const std::string& resultDir, const std::string& prefixName);
    ~IO() = default;

    //! Copy constructor, to avoid warning due to user-declared destructor.
    IO(const IO&) = default;

    void initialize(const std::string& inputDirectory, const std::string& inputFile, const std::string& inputMesh,
                    const std::string& outputMesh, const std::string& meshDir, const std::string& resultDir, const std::string& prefixName);
    void initializeOutput();
    //!<read mesh
    void readMesh(GeometricMeshRegion& mesh, double spaceUnit);
    //!<write mesh
    void writeMesh(GeometricMeshRegion& mesh, std::string nameOfGeoFile="");
    //!<write local mesh localize in the rank-th processor
    void writeOutput(GeometricMeshRegion& mesh, int rank);

    void addVariable(const EnsightVariable::TypeVar& type, const std::string nameVariable,
                     const double* solution, const felInt size, bool manageMemory=false);

    Ensight& ensight() {
      return m_ens;
    }
    Medit& medit() {
      return m_med;
    }

    void readInputFile();
    void readVariable(int idVariable, double time,
                      double* valueOfVariable, felInt size);
    void postProcess(double& time, int flagMultipleCase=false);

    // write a file which contains solution in Medit or Ensight format (actually), Jeremie 03/08/2012
    void writeSolution(int rankProc, double& time, int iteration, const int & typeVariable, const std::string& nameVariable, const double* solution,
                       felInt size, int dimension, bool manageMemory=false);


  };
}

#endif
