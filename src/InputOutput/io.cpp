//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "InputOutput/io.hpp"

namespace felisce
{

IO::IO():typeInput(-1),typeOutput(-1)
{}

/***********************************************************************************/
/***********************************************************************************/

/*! IO object built from information read on mesh*/
IO::IO(const std::string& inputDirectory, const std::string& inputFile, const std::string& inputMesh,
        const std::string& outputMesh, const std::string& meshDir, const std::string& resultDir, const std::string& prefixName)
{
  this->initialize(inputDirectory, inputFile, inputMesh, outputMesh, meshDir, resultDir, prefixName);
}

/***********************************************************************************/
/***********************************************************************************/

void IO::initialize(const std::string& inputDirectory, const std::string& inputFile, const std::string& inputMesh,
                    const std::string& outputMesh, const std::string& meshDir, const std::string& resultDir, const std::string& prefixName)
{
  m_inputDirectory = inputDirectory;
  m_inputFile = inputFile;
  m_inputMesh = inputMesh;
  m_outputMesh = outputMesh;
  m_meshDir = meshDir;
  m_resultDir = resultDir;
  m_prefixName = prefixName;
  m_ens.initialize(m_inputDirectory,m_inputFile,m_inputMesh,m_outputMesh,m_meshDir,m_resultDir);
  m_med.initialize(m_inputMesh,m_outputMesh,m_meshDir,m_resultDir);

  // Switch over the extension
  // =========================
  if ( m_inputMesh.find(".mesh") != std::string::npos ) {
    typeInput = 0;
  } else if ( m_inputMesh.find(".geo") != std::string::npos ) {
    typeInput = 1;
  } else {
    const std::string meshFileExt = m_inputMesh.substr( m_inputMesh.find_last_of( '.' ) +1);
    const std::string error = "No read mesh for such extension: " + meshFileExt;
    felisce_error( error.c_str() , __FILE__, __LINE__);
  }

  // Switch over the extension
  // =========================
  if ( m_outputMesh.find(".mesh") != std::string::npos ) {
    typeOutput = 0;
  } else if ( m_outputMesh.find(".geo") != std::string::npos ) {
    typeOutput = 1;
  } else {
    const std::string meshFileExt = m_outputMesh.substr( m_outputMesh.find_last_of( '.' ) +1);
    const std::string error = "No writer mesh for such extension: " + meshFileExt;
    felisce_error( error.c_str() , __FILE__, __LINE__);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void IO::initializeOutput()
{
  m_ens.initializeOutput(m_outputMesh,1, 0 , 0, 1);
}

/***********************************************************************************/
/***********************************************************************************/

/*! reading of the mesh file according to its extension */
void IO::readMesh(GeometricMeshRegion& mesh, double spaceUnit)
{
  if ( typeInput == 0 ) {
    m_med.readerMedit(mesh, spaceUnit);
  } else if ( typeInput == 1 ) {
    m_ens.readerGeo(mesh, spaceUnit);
  }
}

/***********************************************************************************/
/***********************************************************************************/

/*! writing of the mesh file according to output extension contains in mesh */
void IO::writeMesh(GeometricMeshRegion& mesh, std::string nameOfGeoFile)
{
  if ( typeOutput == 0 ) {
    m_med.writerMedit(mesh);
  } else {
    if (  typeOutput == 1 ) {
      if(FelisceParam::instance().outputFileFormat == ENSIGHT6)
        m_ens.writerGeo(mesh, nameOfGeoFile);
      else if(FelisceParam::instance().outputFileFormat == ENSIGHTGOLD)
        m_ens.writerGeoGold(mesh, nameOfGeoFile);
      else if (FelisceParam::instance().outputFileFormat == ENSIGHT6BIN)
        m_ens.writerGeoBin(mesh, nameOfGeoFile);
      else
        FEL_ERROR("Unknown output file format type");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void IO::writeOutput(GeometricMeshRegion& mesh, int rank)
{
  if ( typeOutput == 0 ) {
    m_med.writerMedit(mesh);
  } else {
    if (  typeOutput == 1 ) {
      m_ens.writerGeo(mesh,rank);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void IO::readInputFile() {
  m_ens.readCaseFile();
}

/***********************************************************************************/
/***********************************************************************************/

void IO::readVariable(int idVariable, double time,
                      double* valueOfVariable, felInt size)
{
  m_ens.readVariable(idVariable, time, valueOfVariable, size);
}

/***********************************************************************************/
/***********************************************************************************/

void IO::addVariable(const EnsightVariable::TypeVar& type, const std::string nameVariable,
                      const double* solution, const felInt size, bool manageMemory)
{
  m_ens.addVariable(type, nameVariable, solution, size, manageMemory);
}

/***********************************************************************************/
/***********************************************************************************/

void IO::postProcess(double& time, int flagMultipleCase)
{
  m_ens.postProcess(m_resultDir, m_prefixName, flagMultipleCase, time);
}

/***********************************************************************************/
/***********************************************************************************/

void IO::writeSolution(int rankProc, double& time, int iteration, const int & typeVariable, const std::string& nameVariable, const double* solution,
                        felInt size, int dimension, bool manageMemory)
{
  IGNORE_UNUSED_RANK_PROC;
  if ( typeOutput == 0 ) {
    m_med.writeSolution(time, iteration, typeVariable, nameVariable, solution, size, dimension);
  } else if ( typeOutput == 1 ) {
    m_ens.addVariable(typeVariable, nameVariable, solution, size, manageMemory);
  }
}

}
