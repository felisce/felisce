//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

/*!
 \file ensight.hpp
 \authors J.Castelneau & J.Foulon
 \date 04/05/2010
 \brief File containing object to use Ensight software
*/

#ifndef _ENSIGHT_HPP
#define _ENSIGHT_HPP

// System includes
#include <cfloat>
#include <list>
#include <unordered_map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce {
  /*!
   \class EnsightTime

   \brief class to handle a time set (in ensight format).

   \author  Jean-Frederic Gerbeau
   */

  static constexpr double Infinity = std::numeric_limits<double>::infinity();

  enum OutputFileFormat {ENSIGHT6=0, ENSIGHTGOLD=1, ENSIGHT6BIN=2, ENSIGHTGOLDBIN=3};

  class EnsightTimeBis {
    /// This class consists of a std::vector<double> m_time_values containig all the time instants.
    /// There is a method to compute the index of a given time instant,
    /// such index contains m_filename_start_number ( see int timeIndex(  double time ) const; )
  private:
    int m_num_steps; ///< The current number of time steps, i.e. the size of m_time_values.
    int m_filename_start_number; ///< set into the constructor is used in the function timeIndex.
    int m_filename_increment; ///< I am not sure it is used anywhere (matteo 10/15)
    std::vector<double> m_time_values; ///< The std::vector containing all the values of the time instants
  public:
    EnsightTimeBis() = default;
    EnsightTimeBis(int time_set, int num_steps, int filename_start_number, int filename_increment); /// The constructor
    /// Getter const
    double timeValue(int i) const {
      return m_time_values[i];
    }
    /// Getter non const
    double& timeValue(int i) {
      return m_time_values[i];
    }
    /// Given a time instant it returns ( the correct index in the std::vector m_time_values ) + m_filename_start_number
    int timeIndex(double time) const;
    int nbSteps() const {
      return m_num_steps;
    }
    int nbSteps() { /// I think it should return a reference (matteo)
      return m_num_steps;
    }
    /// getter
    int fileNameStartNumber() const {
      return m_filename_start_number;
    }
    /// getter
    int fileNameIncrement() const {
      return m_filename_increment;
    }
    /// push_back into the std::vector
    void addTimeValue(double& time);
  };


  //=======================================================================================
  /*!
   \class EnsightVariable

   \brief class to handle a solution (in ensight format).

   \author  Jean-Frederic Gerbeau
   */

  class EnsightVariable {
  public:
    enum TypeVar {Unknown = -1, ScalarPerNode = 0, VectorPerNode = 1};
  private:
    TypeVar m_type; // vector or scalar
    /*! number of solution values ( with previous example size=n ) */
    felInt m_size;
    std::string m_description;
    std::string m_filename;
    std::string m_filename_wildcard;
    /*! pointer on solution values ( [u_x_1, u_x_2, u_x_3, ..., u_x_n,
     u_y_1, u_y_2, u_y_3,...,u_y_n, u_z_1, u_z_2, u_z_3, ...,u_z_n,] ). */
    const double* m_solution;
    /// define if class manage desallocation or not after post-processing.
    bool m_manageMemory;
    std::map<felInt, std::vector<felInt> > m_ref2list;

    char* local_strncpy(char* destination,const char* source, std::size_t num)
    {
    #if defined(__GNUC__) && (__GNUC___ > 8)
      #pragma GCC diagnostic push
      #pragma GCC diagnostic ignored "-Wstringop-truncation"
        return strncpy(destination, source, num);
      #pragma GCC diagnostic pop
    #else
      return strncpy(destination, source, num);
    #endif
    }

  public:
    EnsightVariable() = default;
    EnsightVariable(const std::string& type, int time_set, int file_set,
                    const std::string& description, const std::string& filename);

    EnsightVariable(const EnsightVariable::TypeVar& type, const std::string& nameVariable,
                    const double* solution, felInt size, bool manageMemory=false);

    TypeVar type() {
      return m_type;
    }
    felInt size() {
      return m_size;
    }
    inline void setSize(felInt size) {
      m_size = size;
    }
    inline void setSolution(double* sol) {
      m_solution = sol;
    }

    std::string variableFile() const;
    std::string variableFileInput(int istep);
    std::string variableFileOutput(const std::string& outputDirectory, int istep);
    std::string description() const;
    std::string wildCard(int indexTime);

    void write(const std::string& outputDirectory, int indexTime);
    void writeScalarEnsight6(const std::string& outputDirectory, int indexTime);
    void writeScalarEnsight6Bin(const std::string& outputDirectory, int indexTime);
    void writeScalarEnsightGold(const std::string& outputDirectory, int indexTime);
    void writeVectorEnsight6(const std::string& outputDirectory, int indexTime);
    void writeVectorEnsight6Bin(const std::string& outputDirectory, int indexTime);
    void writeVectorEnsightGold(const std::string& outputDirectory, int indexTime);
    void clear() {
      if (m_manageMemory) delete [] m_solution;
    }

    inline void setRefToListOfIds(std::map<felInt, std::vector<felInt> > ref2list) {
      m_ref2list = ref2list;
    }
  };

  //=======================================================================================
  /*!
   \class EnsightCase

   \brief class to handle a ensight casef

   \author  Jean-Frederic Gerbeau
   */

  class EnsightCase {
    int m_verbose;
    int m_domainDim;
    bool m_createNormalTangent;
    /// Directory where the case file is
    std::string m_directory;
    /// Case file name (including the postfix '.case')
    std::string m_filename;
    /// Lines of the case file
    std::vector<std::string> m_line;
    /// Index of the line where the format section starts
    int m_sectionFormat;
    /// Index of the line where the geometry section starts
    int m_sectionGeometry;
    /// Index of the line where the variable section starts
    int m_sectionVariable;
    /// Index of the line where the time section starts
    int m_sectionTime;
    /// Time set in the 'model' subsection of Geometry section (-1 if not provided)
    int m_model_time_set;
    /// File set int the 'model' subsection of Geometry section (-1 if not provided)
    int m_model_file_set;
    /// Optional field in the 'model' subsection of Geometry section (false if not provided)
    bool m_change_coords_only;
    /// cstep in change_coords_only item (0 if not provided)
    int  m_change_coords_only_cstep;
    /// name of the geometry file (ex crosse.0010.geo)
    std::string m_geo_file;
    /// name of the geometry file with wildcard (ex crosse.****.geo)
    std::string m_geo_file_wildcard;
    /// number of wildcard characters (i.e. character '*')
    int m_num_wildcard;
    /// number of variables
    int m_num_variable;
    /// variable
    std::vector<EnsightVariable> m_var;
    std::vector<EnsightTimeBis> m_time;

  public:
    EnsightCase();
    EnsightCase(int verbose);
    void initialize(const std::string& nameGeoFile, int time_set,
                    int num_steps, int filename_start_number,
                    int filename_increment );
    void read(const std::string& directory, const std::string& case_file);
    std::string geometryFile() const;
    std::string geometryFile(int istep) const;
    EnsightVariable const& variable(int ivar) const;
    EnsightVariable& variable(int ivar);
    EnsightTimeBis& timeSet(int iset = 0);

    int timeIndex(double time) const;
    int variableIndex(std::string description) const;

    /// dim is the number of nodes for a scalar and 3*nb of nodes for a std::vector
    void readVariable(int idVariable,int indexTime, double* vec,felInt size);
    void readScalar(const std::string& fileName, double* vec, felInt size);
    void readVector(const std::string& fileName, double* vec, felInt size);

    ///Output functions.
    void addVariable(const EnsightVariable::TypeVar& type, const std::string& nameVariable,
                     const double* solution, felInt size, bool manageMemory=false);
    void postProcess(const std::string& outputDirectory,
                     const std::string& nameCase,
                     int optionMultiCase,
                     double& time);
    void writeVariable(const std::string& outputDirectory, const std::string& nameVariable, int indexTime);
    void writeCaseFile(const std::string& outputDirectory, const std::string& nameCase, int optionMultiCase);
    void case_mesh_section(FILE* pFile);
    void case_mesh_multiple_section(FILE* pFile);
    void case_mesh_by_variable(FILE* pFile, int iVar);
    void case_geometricVec(FILE* pFile);
    void case_variable_section(FILE* pFile);
    void case_variable_by_variable(FILE* pFile, int iVar);
    void case_time_section(FILE* pFile);
    int getNumNodeFromGeoFile(std::string dir) const;
    void SetDomainDim_case(int domainDim);
    void SetCreateNormalTangent_case(bool createNormalTangent);
    inline std::size_t numVariables() {
      return m_var.size();
    }
    //   inline int domainDim() {
    // return m_domainDim;
    //}
  };


  /**
   * \class Ensight
   * \authors J. Castelneau, J. Foulon
   * \brief Class managing input and output for using Ensight6
   * \date 04/05/2010
   *
   * An Ensight object contains a prefix name, a name directory and a list of EnsightData
   *
   */

  class Ensight {
  public:
    typedef GeometricMeshRegion::ElementType ElementType;
    typedef std::unordered_map<std::string, std::pair<ElementType, std::string> > StringToEns6Pair_type;
    typedef std::unordered_map<std::string, ElementType > StringToElementType_type;
    typedef std::vector< std::string > PartNumberToDescriptionLine_type;
  private:
    /*!< \todo: build a dataSolution to manage all solution information
     ( prefix names, solution directory, ... ) */
    int m_indexStep;
    int m_domainDim;
    bool m_createNormalTangent;
    /*!<std::unordered_map to link element geometric name in felisce to ensight format */
    static StringToEns6Pair_type    m_eltFelNameToEns6Pair;
    /*!<std::unordered_mapping on ensight element name and geometric elementType enum */
    static StringToElementType_type m_eltEns6NameToEnum;
    /*!<Contains a short description of the part*/
    PartNumberToDescriptionLine_type m_partIdToDescLine;
    /*! Function to initialize m_eltFelNameToEns6Pair & m_eltEns6NameToEnum std::unordered_maps */
    void initMap();

    EnsightCase m_caseInput;
    EnsightCase m_caseOutput;

    std::string m_inputDirectory;
    std::string m_inputFile;
    std::string m_inputMesh;
    std::string m_outputMesh;
    std::string m_meshDir;
    std::string m_resultDir;

    std::map<felInt, std::vector<felInt> > m_refToListOfIds;

    char* local_strncpy(char* destination,const char* source, std::size_t num)
    {
    #if defined(__GNUC__) && (__GNUC___ > 8)
      #pragma GCC diagnostic push
      #pragma GCC diagnostic ignored "-Wstringop-truncation"
        return strncpy(destination, source, num);
      #pragma GCC diagnostic pop
    #else
      return strncpy(destination, source, num);
    #endif
    }

    std::string getKeyword(const ElementType eltType);

    void prepareElemIndexes(std::vector<std::vector<felInt>>& elem, const ElementType eltType);

    void getElemIndexes(GeometricMeshRegion& mesh, const ElementType eltType, const felInt iel, std::vector<std::vector<felInt>>& elem, const felInt startindex, const bool incr_vert_id);

    int getNumberPointsElement(const ElementType eltType);

    felInt getNumberOfElements(GeometricMeshRegion& mesh, const ElementType eltType, const int the_ref);

  public:
    //Construtor
    Ensight() = default;
    Ensight(const std::string& inputDirectory,const std::string& inputFile,const std::string& inputMesh,const std::string& outputMesh,const std::string& meshDir,const std::string& resultDir);
    void initialize(const std::string& inputDirectory,const std::string& inputFile,const std::string& inputMesh,const std::string& outputMesh,const std::string& meshDir,const std::string& resultDir);
    ~Ensight() = default;

    /// Copy constructor, to avoid warning due to user-declared destructor.
    Ensight(const Ensight&) = default;

    void initializeOutput(const std::string& nameGeoFile,int time_set,
                          int num_steps,int filename_start_number,
                          int filename_increment );
    // Set manually the directory to write results
    void setResultDir(const std::string resultDir);
    //read input case file.
    void readCaseFile();
    //read data of a variable store in file.
    void readVariable(int idVariable, double time, double* valueOfVariable, felInt size);

    // Variable functions
    /*!< add Variable/Solution which is manage by ensight*/
    void addVariable(/*const EnsightVariable::TypeVar& type*/int typeVariable, const std::string nameVariable,
        const double* solution, const felInt size, bool manageMemory=false);


    // Mesh functions
    /*!< read ensight geoFile to create geometricMeshRegion object */
    void readerGeo(GeometricMeshRegion& mesh, double spaceUnit);
    /*!< write ensight geoFile from geometricMeshRegion object */
    void writerGeo(GeometricMeshRegion& mesh, std::string nameOfGeoFile);
    /*!< write ensight binary geoFile from geometricMeshRegion object */
    void writerGeoBin(GeometricMeshRegion& mesh, std::string nameOfGeoFile);
    /*!< to create the news connectivities between quad9 and 4 quad1 */
    void convertQuad9To4Quad1(std::vector  <felInt>& listVertQ1Elem);
    //to visualize in ensight normal and tangent std::vector
    void writeNormalTangent(const std::string outPutFileVec, const std::vector<Point>& vect);
    /*!< write ensight gold geoFile */
    void writerGeoGold(GeometricMeshRegion& mesh, std::string nameOfGeoFile);
    /*!< write ensight geoFile from geometricMeshRegion object from processor rank-th*/
    void writerGeo(GeometricMeshRegion& mesh,int rank);


    // Post-processing function
    /*!< write all output (variable files, case file,...) */
    void postProcess(const std::string& outputDirectory,
                     const std::string& nameCase,
                     int optionMultiCase,
                     double& time);
    //Access functions
    inline StringToEns6Pair_type  & eltFelNameToEns6Pair() {
      return m_eltFelNameToEns6Pair;
    }

    inline void setRefToListOfIds(std::map<felInt, std::vector<felInt> > ref2list) {
      m_refToListOfIds = ref2list;
    }

    inline int domainDim() {
      return m_domainDim;
    }
    inline EnsightCase& caseInput(){return m_caseInput;}
    inline EnsightCase& caseOutput(){return m_caseOutput;}
  };
}

#endif
