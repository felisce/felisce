//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau
//                Dominique Chapelle
//                Miguel Fernandez
//                Vicente Mataix Ferrandiz
//

#ifndef PETSCVECTOR_HPP
#define PETSCVECTOR_HPP

// System includes
#include <string>
#include <vector>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/sys.hpp" // Jacques 13/07/22: added due to definition of FELISCE_PETSC_NULLPTR
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"

// Project includes
#include "PETScInterface/petscMatrix_fwd.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce 
{

//! \brief Wrapper to Petsc Vector type (Vec)
class PetscVector 
{
  //===============
  // FRIEND CLASSES
  //===============

  friend class PetscMatrix;
  friend class PetscVectorScatter;

  //=================
  // FRIEND FUNCTIONS
  //=================

  // TODO: Define more operators using this friend functions
  friend PetscErrorCode dot(const PetscVector& x,const PetscVector& y,PetscScalar* val);
  friend PetscErrorCode mult(const PetscMatrix& mat,const PetscVector& x,PetscVector&  y);
  friend PetscErrorCode multAdd(const PetscMatrix& mat,const PetscVector& v1,const PetscVector& v2,PetscVector&  v3);
  friend PetscErrorCode multTranspose(const PetscMatrix& mat,const PetscVector& x,PetscVector&  y);
  friend PetscErrorCode pointwiseDivide(const PetscVector& w, const PetscVector& x,const PetscVector& y);
  friend PetscErrorCode pointwiseMult(const PetscVector& w, const PetscVector& x,const PetscVector& y);
  friend PetscErrorCode swap(const PetscVector& x,const PetscVector& y);
  friend PetscErrorCode waxpy(const PetscVector& w,PetscScalar const & alpha,const PetscVector& x,const PetscVector& y);
  friend PetscErrorCode maxPointwiseDivide(const PetscVector& x, const PetscVector& y, PetscScalar* max); 
  friend PetscErrorCode gatherVec(PetscVector& v, PetscVector& seqV);
public:

  /// Pointer definition of PetscVector
  FELISCE_CLASS_POINTER_DEFINITION(PetscVector);

  //=======================
  // CONSTRUCTOR/DESTRUCTOR
  //=======================

  /**
   * @brief Default constructor
   */
  PetscVector();
  
  /**
   * @brief Default destructor
   */
  ~PetscVector();

  /**
   * @brief Copy constructor
   * @param b The vector to be copied
   */
  PetscVector(const PetscVector& b);
  
  /**
   * @brief Assign operator
   * @param b The vector to be assigned
   * @return The current instance
   */
  PetscVector& operator=(const PetscVector& b);

  /**
   * @brief Multiply (*) operator
   * @param scalar The scalar to be multiplied
   * @return The multiplication of the current vector and scalar
   */
  PetscVector operator*(const PetscInt scalar);

  /**
   * @brief Multiply (*) operator (constant version)
   * @param scalar The scalar to be multiplied
   * @return The multiplication of the current vector and scalar
   */
  PetscVector operator*(const PetscInt scalar) const;

  /**
   * @brief Multiply (*) operator
   * @param scalar The scalar to be multiplied
   * @return The multiplication of the current vector and scalar
   */
  PetscVector operator*(const PetscScalar scalar);

  /**
   * @brief Multiply (*) operator (constant version)
   * @param scalar The scalar to be multiplied
   * @return The multiplication of the current vector and scalar
   */
  PetscVector operator*(const PetscScalar scalar) const;

  /**
   * @brief Multiply (*) operator (dot product)
   * @param b The vector to be multiplied
   * @return The dot product of the current vector and b
   */
  PetscScalar operator*(const PetscVector& b);

  /**
   * @brief Multiply (*) operator (dot product) (constant version)
   * @param b The vector to be multiplied
   * @return The dot product of the current vector and b
   */
  PetscScalar operator*(const PetscVector& b) const;

  /**
   * @brief Multiply (*=) operator
   * @param scalar The scalar to be multiplied
   * @return The current vector multiplied by the scalar
   */
  PetscVector& operator*=(const PetscInt scalar);

  /**
   * @brief Multiply (*=) operator
   * @param scalar The scalar to be multiplied
   * @return The current vector multiplied by the scalar
   */
  PetscVector& operator*=(const PetscScalar scalar);

  /**
   * @brief Division (/) operator
   * @param scalar The scalar to be divided
   * @return The division of the current vector and scalar
   */
  PetscVector operator/(const PetscInt scalar);

  /**
   * @brief Division (/) operator (constant version)
   * @param scalar The scalar to be divided
   * @return The division of the current vector and scalar
   */
  PetscVector operator/(const PetscInt scalar) const;

  /**
   * @brief Division (/) operator
   * @param scalar The scalar to be divided
   * @return The division of the current vector and scalar
   */
  PetscVector operator/(const PetscScalar scalar);

  /**
   * @brief Division (/) operator (constant version)
   * @param scalar The scalar to be divided
   * @return The division of the current vector and scalar
   */
  PetscVector operator/(const PetscScalar scalar) const;

  /**
   * @brief Division (/=) operator
   * @param scalar The scalar to be divided
   * @return The current vector divided by the scalar
   */
  PetscVector& operator/=(const PetscInt scalar);

  /**
   * @brief Division (/=) operator
   * @param scalar The scalar to be divided
   * @return The current vector divided by the scalar
   */
  PetscVector& operator/=(const PetscScalar scalar);

  /**
   * @brief Sum (+) operator
   * @param b The vector to be summed
   * @return The sum product of the current vector and b
   */
  PetscVector operator+(const PetscVector& b);

  /**
   * @brief Sum (+) operator (constant version)
   * @param b The vector to be summed
   * @return The sum product of the current vector and b
   */
  PetscVector operator+(const PetscVector& b) const;

  /**
   * @brief Sum (+=) operator
   * @param b The vector to be summed
   * @return The current vector plus b
   */
  PetscVector& operator+=(const PetscVector& b);

  /**
   * @brief Difference (-) operator
   * @param b The vector to be differenced
   * @return The difference product of the current vector and b
   */
  PetscVector operator-(const PetscVector& b);

  /**
   * @brief Difference (-) operator (constant version)
   * @param b The vector to be differenced
   * @return The difference product of the current vector and b
   */
  PetscVector operator-(const PetscVector& b) const;

  /**
   * @brief Difference (-=) operator
   * @param b The vector to be differenced
   * @return The current vector minus b
   */
  PetscVector& operator-=(const PetscVector& b);

  /**
   * @brief [] operator
   * @param Index The index of the value
   * @note Cannot be reference as uses getValue method
   */
  PetscScalar operator[] (const PetscInt Index);

  /**
   * @brief [] operator (const version)
   * @param Index The index of the value
   * @note Cannot be reference as uses getValue method
   */
  PetscScalar operator[] (const PetscInt Index) const;

  //=================
  // STATIC FUNCTIONS
  //=================

  static PetscVector null();

  //===================
  // CREATION FUNCTIONS
  //===================

  /**
   * @brief This is the method that must be called to create the PETSc vector
   * @return The corresponding error code
   */
  PetscErrorCode create(MPI_Comm comm = PETSC_COMM_WORLD);
  
  /**
   * @brief This is the method that must be called to create the PETSc vector (in sequantial)
   * @param comm The communicator
   * @param n The size of the vector
   * @return The corresponding error code
   */
  PetscErrorCode createSeq(MPI_Comm comm,PetscInt n);

  /**
   * @brief This is the method that must be called to create the PETSc vector (in sequantial)
   * @note Arguments reordered to consider PETSC_COMM_WORLD as default
   * @param n The size of the vector
   * @param comm The communicator
   * @return The corresponding error code
   */
  PetscErrorCode createSeqInstance(PetscInt n, MPI_Comm comm = PETSC_COMM_WORLD) {return createSeq(comm, n);}

  /**
   * @brief This is the method that must be called to create the PETSc vector (in MPI)
   * @param comm The communicator
   * @param n The size per partition
   * @param N The size of the vector
   * @return The corresponding error code
   */
  PetscErrorCode createMPI(MPI_Comm comm,PetscInt n,PetscInt N);

  /**
   * @brief This is the method that must be called to create the PETSc vector (in MPI)
   * @note Arguments reordered to consider PETSC_COMM_WORLD as default
   * @param N The size of the vector
   * @param n The size per partition
   * @param comm The communicator
   * @return The corresponding error code
   */
  PetscErrorCode createMPIInstance(PetscInt N, PetscInt n = PETSC_DECIDE, MPI_Comm comm = PETSC_COMM_WORLD) {return createMPI(comm, n, N);}

  //======================
  // DESTRUCTION FUNCTIONS
  //======================

  /**
   * @brief Destroys the current vector (to be called as "destructor")
   * @return The corresponding error code
   */
  PetscErrorCode destroy();

  //===============
  // COPY FUNCTIONS
  //===============

  /**
   * @brief Copies a vector. 
   * @param vector	A vector to copy
   * @return The corresponding error code
   */
  PetscErrorCode copyFrom(const PetscVector& vector) const;
  
  /**
   * @brief Creates a new vector of the same type as an existing vector.
   * @param vector	A vector to mimic
   * @return The corresponding error code
   */
  PetscErrorCode duplicateFrom(const PetscVector& vector);

  //=======================
  // MODIFICATION FUNCTIONS
  //=======================

  /**
   * @brief This routine should be called after completing all calls to VecSetValues()
   * @return The corresponding error code
   */
  PetscErrorCode assembly() const;
  
  PetscErrorCode restoreArray(PetscScalar **a) const;
  
  PetscErrorCode set(const PetscScalar& value) const;
  
  PetscErrorCode setFromOptions() const;

  /**
   * @brief Sets an option for controling a vector's behavior
   * @param op  The option
   * @param flg Flag turn the option on/off
   * @return The corresponding error code
   */
  PetscErrorCode setOption(VecOption op, PetscBool flg) const;  

  /**
   * @brief Sets the local and global sizes, and checks to determine compatibility
   * @param n	The local size (or PETSC_DECIDE to have it set)
   * @param N	The global size (or PETSC_DECIDE)
   * @return The corresponding error code
   */
  PetscErrorCode setSizes(PetscInt n, PetscInt N) const;

  /**
   * @brief Sets the local and global sizes, and checks to determine compatibility
   * @note Version with size across partitions automatic
   * @param N	The global size 
   * @return The corresponding error code
   */
  PetscErrorCode setSizes(PetscInt N) const {return setSizes(PETSC_DECIDE, N);}
  
  /**
   * @brief Builds a vector, for a particular vector implementation.
   * @details See "petsc/include/petscvec.h" for available vector types (for instance, VECSEQ, VECMPI, or VECSHARED).
   * @param method The name of the vector type
   * @return The corresponding error code
   */
  PetscErrorCode setType(VecType method = VECSEQ) const;
  
  /**
   * @brief Set a single entry into a vector
   * @param row	The row location of the entry
   * @param value The value to insert
   * @param mode Either INSERT_VALUES or ADD_VALUES
   * @return The corresponding error code
   */
  PetscErrorCode setValue(PetscInt row, PetscScalar value, InsertMode mode = INSERT_VALUES) const;

  /**
   * @brief Set a single entry into a vector. Only in the rank 0
   * @param row	The row location of the entry
   * @param value The value to insert
   * @param mode Either INSERT_VALUES or ADD_VALUES
   * @return The corresponding error code
   */
  PetscErrorCode setValueOnce(PetscInt row, PetscScalar value, InsertMode mode = INSERT_VALUES) const;

  /**
   * @brief Inserts or adds values into certain locations of a vector.
	 * @param ni Number of elements to add
   * @param ix Indices where to add
   * @param y	Array of values
   * @param mode Either INSERT_VALUES or ADD_VALUES, where ADD_VALUES adds values to any existing entries, and INSERT_VALUES replaces existing entries with new values
   * @return The corresponding error code
   */
  PetscErrorCode setValues(PetscInt ni,const PetscInt ix[],const PetscScalar y[],InsertMode iora = INSERT_VALUES) const;

  /**
   * @brief Inserts or adds values into certain locations of a vector. Only in the rank 0
	 * @param ni Number of elements to add
   * @param ix Indices where to add
   * @param y	Array of values
   * @param mode Either INSERT_VALUES or ADD_VALUES, where ADD_VALUES adds values to any existing entries, and INSERT_VALUES replaces existing entries with new values
   * @return The corresponding error code
   */
  PetscErrorCode setValuesOnce(PetscInt ni,const PetscInt ix[],const PetscScalar y[],InsertMode iora = INSERT_VALUES) const;
  
  /**
   * @brief Set to zero all the values of the vector
   * @return The corresponding error code
   */
  PetscErrorCode zeroEntries() const;

  //=================
  // ACCESS FUNCTIONS
  //=================

  /**
   * @brief Retrieves the C array contained in the vector 
   * @param array The array containing the values
   * @return The corresponding error code
   */
  PetscErrorCode getArray(PetscScalar** array) const;

  /**
   * @brief Retrieves the C array contained in the vector 
   * @note Direct version
   * @return The array containing the values
   */
  PetscScalar* getArray() const;

  /**
   * @brief Retrieves the local size of the vector
   * @param localSize The local size
   * @return The corresponding error code
   */
  PetscErrorCode getLocalSize(PetscInt* localSize) const;
 
  /**
   * @brief Retrieves the local size of the vector
   * @note Direct version
   * @return The local size
   */
  PetscInt getLocalSize() const;

  /**
   * @brief Retrieves the total size of the vector
   * @param size The total size
   * @return The corresponding error code
   */
  PetscErrorCode getSize(PetscInt* size) const;
  
  /**
   * @brief Retrieves the total size of the vector
   * @note Direct version
   * @return The total size
   */
  PetscInt getSize() const;

  /**
   * @brief Gets values from certain locations of a vector. Currently can only get values on the same processor
   * @param ni Number of elements to get
   * @param ix Indices where to get them from (in global 1d numbering)
   * @param y	Array of values
   * @return The corresponding error code
   */
  PetscErrorCode getValues(PetscInt ni,const PetscInt ix[],PetscScalar y[]) const;

  /**
   * @brief Gets values from certain locations of a vector. Currently can only get values on the same processor
   * @param ni Number of elements to get
   * @param ix Index where to get them from (in global 1d numbering)
   * @param y	Value
   * @return The corresponding error code
   */
  PetscErrorCode getValue(const PetscInt ix,PetscScalar& y) const;
  
  PetscErrorCode getAllValuesInAppOrdering(const AO& ao, std::vector<double>& array) const;

  //==================
  // BOOLEAN FUNCTIONS
  //==================

  /**
   * @brief This returns if the vector is null
   * @return If the vector is null
   */
  bool isNull();

  /**
   * @brief This returns if the vector is null (constant version)
   * @return If the vector is null
   */
  bool isNull() const;

  /**
   * @brief This returns if the vector is not null
   * @return If the vector is not null
   */
  bool isNotNull();

  /**
   * @brief This returns if the vector is not null (constant version)
   * @return If the vector is not null
   */
  bool isNotNull() const;

  //=======================
  // MATHEMATICAL FUNCTIONS
  //=======================

  PetscErrorCode axpbypcz(PetscScalar alpha, PetscScalar beta, PetscScalar gamma, const PetscVector& x, const PetscVector& y) const;
  
  /**
   * @brief Compute linear combination of two vectors (y = alpha * x + beta * y)
   * @details x and y MUST be different vectors The implementation is optimized for alpha and/or beta values of 0.0 and 1.0
   * @param alpha The first scalar coefficient
   * @param beta The second scalar coefficient
   * @param x The first vector
   * @return The corresponding error code
   */
  PetscErrorCode axpby(PetscScalar alpha, PetscScalar beta, const PetscVector& x) const;
  
  /**
   * @brief Compute linear combination of two vectors (y = alpha * x + beta * y)
   * @details x and y MUST be different vectors The implementation is optimized for alpha and/or beta values of 0.0 and 1.0
   * @note The order is different so defaults arguments of 1.0 can be considered
   * @param alpha The first scalar coefficient
   * @param beta The second scalar coefficient
   * @param x The first vector
   * @return The corresponding error code
   */
  PetscErrorCode axpby(const PetscVector& x, const PetscScalar alpha = 1.0, const PetscScalar beta = 1.0) const {return axpby(alpha, beta, x);}

  /**
   * @brief Compute linear combination of two vectors (y = alpha x + y)
   * @details x and y MUST be different vectors This routine is optimized for alpha of 0.0
   * @param alpha The scalar coefficient
   * @param x The first vector
   * @return The corresponding error code
   */
  PetscErrorCode axpy(PetscScalar alpha, const PetscVector& x) const;

  /**
   * @brief Compute linear combination of two vectors (y = alpha x + y)
   * @details x and y MUST be different vectors This routine is optimized for alpha of 0.0
   * @note The order is different so defaults arguments of 1.0 can be considered
   * @param alpha The scalar coefficient
   * @param x The first vector
   * @return The corresponding error code
   */
  PetscErrorCode axpy(const PetscVector& x, const PetscScalar alpha = 1.0) const {return axpy(alpha, x);}
    
  /**
   * @brief Computes the maximum value of the current vector
   * @param val The maximum value
   * @param p The position in the vector (null by default)
   * @return The corresponding error code
   */
  PetscErrorCode max(PetscReal* val, PetscInt* p = FELISCE_PETSC_NULLPTR) const;
    
  /**
   * @brief Computes the maximum value of the current vector
   * @param p The position in the vector
   * @param val The maximum value
   * @return The corresponding error code
   */
  PetscErrorCode max(PetscInt* p, PetscReal* val) const;
      
  /**
   * @brief Computes the maximum value of the current vector
   * @details This version has default values for easier computation
   * @return The maximum value
   */
  PetscReal max() const;

  /**
   * @brief Computes the minimum value of the current vector
   * @param val The minimum value
   * @param p The position in the vector (null by default)
   * @return The corresponding error code
   */
  PetscErrorCode min(PetscReal* val, PetscInt* p = FELISCE_PETSC_NULLPTR) const;
    
  /**
   * @brief Computes the minimum value of the current vector
   * @param p The position in the vector
   * @param val The minimum value
   * @return The corresponding error code
   */
  PetscErrorCode min(PetscInt* p, PetscReal * val) const;

  /**
   * @brief Computes the minimum value of the current vector
   * @details This version has default values for easier computation
   * @return The minimum value
   */
  PetscReal min() const;

  /**
   * @brief Computes the norm of the current vector
   * @param type The type of norm considered
   * @param val The normal value
   * @return The corresponding error code
   */
  PetscErrorCode norm(NormType type, PetscReal* val) const;
  
  /**
   * @brief Computes the norm of the current vector
   * @details This version has default values for easier computation
   * @param type The type of norm considered
   * @return The normal value
   */
  PetscReal norm(const NormType type = NORM_2) const;

  /**
   * @brief Scale the vector a given value
   * @param scalar The value that scales the vector
   * @return The corresponding error code
   */
  PetscErrorCode scale(PetscScalar scalar) const;
  
  /**
   * @brief Shifts the vector a given value
   * @param scalar The value that shifts the vector
   * @return The corresponding error code
   */
  PetscErrorCode shift(PetscScalar scalar) const;
  
  /**
   * @brief Computes the sum of the current vector
   * @param sum The sum of the vector
   * @return The corresponding error code
   */
  PetscErrorCode sum(PetscScalar* sum) const;
  
  /**
   * @brief Computes the sum of the current vector
   * @details This version has default values for easier computation
   * @return The sum of the vector
   */
  PetscScalar sum() const;

  /**
   * @brief Computes absolute value of the vector
   * @return The corresponding error code
   */
  PetscErrorCode abs() const;

  //========================
  // COMMUNICATION FUNCTIONS
  //========================

  PetscErrorCode scatterToAll(PetscVector& y, InsertMode addv, ScatterMode mode) const;
  
  PetscErrorCode scatterToAllNotCreatingVector(PetscVector& y, InsertMode addv, ScatterMode mode) const;
  
  PetscErrorCode scatterToZero(PetscVector& y, InsertMode addv, ScatterMode mode) const;
  
  PetscErrorCode scatterToZeroNotCreatingVector(PetscVector& y, InsertMode addv, ScatterMode mode) const;
  
  PetscErrorCode broadCastSequentialVector(MPI_Comm comm, int master ) const;
  
  //=================
  // OUTPUT FUNCTIONS
  //=================
  
  PetscErrorCode view(const PetscViewer& viewer = PETSC_VIEWER_STDOUT_WORLD) const;
  
  PetscErrorCode saveInBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder) const;
  
  PetscErrorCode saveInMatlabFormat(MPI_Comm comm, const std::string filename, const std::string folder) const;

  //================
  // INPUT FUNCTIONS
  //================
  
  PetscErrorCode load(PetscViewer viewer);
  
  PetscErrorCode loadFromBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder);

  PetscErrorCode loadFromMatlabFormat(MPI_Comm comm, const std::string filename, const std::string folder);
  
  PetscErrorCode loadFromEnsight(const std::string fileName);

  //=================
  // TO BE DEPRECATED
  //=================

  const Vec& toPetsc() const;
  
  Vec& toPetsc();
private:

  //=============
  // DATA MEMBERS
  //=============

  Vec m_self = nullptr; /// The PETSc vector 
};

}

#include "petscVector_friend.hpp"
#include "petscVector_inline.hpp"

#endif // PETSCVECTOR_HPP
