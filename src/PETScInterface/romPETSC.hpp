//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _ROMPETSC_HPP
#define _ROMPETSC_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"
#include "Core/NoThirdPartyWarning/Petsc/mat.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"
#include "Core/NoThirdPartyWarning/Petsc/ksp.hpp"
#include "Core/NoThirdPartyWarning/Petsc/pc.hpp"
#include "Core/NoThirdPartyWarning/Petsc/error.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "PETScInterface/petscVector.hpp"

namespace felisce 
{
  // class for handling reduced order models (POD) using Petsc vectors and matrices

  // HELP:

  // Set the parameter m_useInverse = true in RomPETSC::initializeRom  if want to use inverse matrix to solve the reduced system and DO NOT call RomPETSC::setSolver in your LinearProblem.
  // Otherwise, call setSolver, set m_useInverse = false, and set option solver in class FelisceParam (data file) as :
  // solver = gmres
  // preconditioner = lu

  // The pod basis can be read from file using the function RomPETSC::initBasis() or with a pre-allocated PETSC matrix (Mat) with the function RomPETSC::initializeRomBasis(const PetscMatrix& basis). In case of file, name and directory have to be specified in FelisceParam:
  // inputDirectory = path/
  // podBasisFile = file # (without extension, first line need size of matrix)
  class RomPETSC {
  public:
    /// Constructor.
    RomPETSC();
    /// Destructor.
    ~RomPETSC();
    void initializeRom(FelisceTransient::Pointer fstransient,MPI_Comm& comm, AO& ao);
    void initializeRom(FelisceTransient::Pointer fstransient,MPI_Comm& comm);

    // Initialization of the basis
    void initializeRomBasis(const PetscMatrix& basis); // with a pre-allocated matrix
    void initializeRomBasis(const std::vector<PetscVector*>& basis); // with a pre-allocated matrix
    void initializeRomBasis(); // from file

    // Linear system reduction
    void fullToReducedRHS(PetscVector& fullVector);
    void fullToReducedMatrix(PetscMatrix& fullMatrix);
    void solveALP();
    void solveROM(PetscVector& solution);

    // Access function
    felInt dimRomBasis() {
      return m_dimRomBasis;
    }
    felInt dimFullRomBasis() {
      return m_dimFullRomBasis;
    }
    PetscVector& romSolution() {
      return m_romSolution;
    }

    // Solution of the reduced system
    void setSolver(KSP ksp, PC pc, MatStructure setPreconditionerOption);

    void calculateInverse();

    void readCase(int& precisionSol, std::vector<std::string>& variable, felInt& snapSize);
    void readEnsight(double* scalarValue, std::string sclFile, felInt snapSize);

    void gatherVector(PetscVector& parVector, PetscVector& seqVector);
  private:
    /// Number of lumpedModelBC outlets
    FelisceTransient::Pointer m_fstransient;

    std::string m_filenameBasis; // the file containing the POD elements (read from param)
    int m_verbose; // read from param

    int m_dimRomBasis; // the basis used for reduction (read from param)
    felInt m_dimFullRomBasis; // the full POD basis available (read from file)

    PetscMatrix m_romBasis; // basis elements
    PetscMatrix m_mass; // mass matrix of FE space

    PetscMatrix m_romMatrix; // reduced matrix
    PetscMatrix m_inverseMatrix;
    PetscVector m_romRHS; // reduced RHS
    PetscVector m_romSolution; // reduced solution

    bool m_buildBasis;
    bool m_buildMass;
    bool m_useInverse;
    bool m_buildInverse;
    bool m_isFirstStep;

    KSP m_ksp;
    PC m_pc;
    MatStructure m_setPreconditionerOption;
    MPI_Comm m_MPIcomm;
    AO m_ao;
  };
}

#endif

