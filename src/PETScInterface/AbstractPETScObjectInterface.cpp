//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "PETScInterface/AbstractPETScObjectInterface.hpp"

namespace felisce 
{

namespace AOInterface
{

void RetrieveDofPositionInPetscOrdering(
  int* pGlobalIndexes,
  const CurBaseFiniteElement& rFE, 
  const felInt elementIndex, 
  const int idVariable, 
  const AO& rAO, 
  const Dof& rDof,
  const int numComp
  )
{
  const int size = rFE.numDof()*numComp;
  int cpt = 0;
  felInt idDof;
  for(int iComp = 0; iComp<numComp; iComp++) {
    for(int iDof=0; iDof<rFE.numDof(); iDof++) {
      rDof.loc2glob(elementIndex,iDof,idVariable,iComp,idDof);
      pGlobalIndexes[cpt] = idDof;
      cpt++;
    }
  }
  AOApplicationToPetsc(rAO,size,pGlobalIndexes);
}

/***********************************************************************************/
/***********************************************************************************/

void RetrieveDofPositionInPetscOrderingSingleComponent(
  int* pGlobalIndexes,
  const CurBaseFiniteElement& rFE, 
  const felInt elementIndex, 
  const int idVariable, 
  const AO& rAO, 
  const Dof& rDof,
  const int iComp
  )
{
  const int size = rFE.numDof();
  int cpt = 0;
  felInt idDof;
  for(int iDof=0; iDof<rFE.numDof(); iDof++) {
    rDof.loc2glob(elementIndex,iDof,idVariable,iComp,idDof);
    pGlobalIndexes[cpt] = idDof;
    cpt++;
  }
  AOApplicationToPetsc(rAO,size,pGlobalIndexes);
}

/***********************************************************************************/
/***********************************************************************************/

int SearchGlobalIndexNode(
  GeometricMeshRegion&  rMesh,
  const ListUnknown& rListUnknown,
  const AO& rAO, 
  const Dof& rDof,
  const PhysicalVariable& rPhysicalVariable,
  const int iNode,
  const int iComp,
  const GeometricMeshRegion::ElementType& rEltType 
  )
{
  int iel = -1;
  int vertex_id = 0;

  const auto number_of_elements = rMesh.numElements(rEltType);

  const int number_nodes_elem = GeometricMeshRegion::m_numPointsPerElt[rEltType];
  std::vector<felInt> elem(number_nodes_elem, 0);
  for (int i_elem = 0; i_elem < number_of_elements; ++i_elem) {
    rMesh.getOneElement(rEltType, i_elem, elem, false);
    for (vertex_id = 0; vertex_id < number_nodes_elem; vertex_id++) {
      if (elem[vertex_id] + 1 == iNode) {
        iel = i_elem;
        break;
      }
    }
    if (iel > 0) {
      break;
    }
  }

  const int i_unkown_disp = rListUnknown.getUnknownIdList(rPhysicalVariable);
  const int id_var_disp = rListUnknown.idVariable(i_unkown_disp);
  int index = 0;
  rDof.loc2glob(iel,vertex_id, id_var_disp, iComp, index);
  AOApplicationToPetsc(rAO,1,&index);

  return index;
}

}

}