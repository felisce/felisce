//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Vicente Mataix Ferrandiz
//

#ifndef ABSTRACT_PETSC_OBJECT_INTERFACE_HPP
#define ABSTRACT_PETSC_OBJECT_INTERFACE_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"
#include "DegreeOfFreedom/variable.hpp"
#include "DegreeOfFreedom/dof.hpp"

namespace felisce 
{

/**
 * @brief Interface for the Abstract PETSc object (AO) that manages mapping between different global numbering 
 */
namespace AOInterface
{
  /**
   * @brief This method is an auxiliary method which provides the global PETSc ordering of the DoF of a given FE
   * @param pGlobalIndexes The Global indexes of the DoFs of the FE
   * @param rFE The Finite Element 
   * @param elementIndex The index of the current FE
   * @param idVariable The ID of the current variable
   * @param rAO Abstract PETSc object
   * @param rDof The rDof object
   * @param numComp The number of components of the variable
   */
  void RetrieveDofPositionInPetscOrdering(
    int* pGlobalIndexes,
    const CurBaseFiniteElement& rFE, 
    const felInt elementIndex, 
    const int idVariable, 
    const AO& rAO, 
    const Dof& rDof,
    const int numComp
    );

  /**
   * @brief This method is an auxiliary method which provides the global PETSc ordering of the DoF of a given FE in a given component
   * @param pGlobalIndexes The Global indexes of the DoFs of the FE
   * @param rFE The Finite Element 
   * @param elementIndex The index of the current FE
   * @param idVariable The ID of the current variable
   * @param rAO Abstract PETSc object
   * @param rDof The rDof object
   * @param iComp The component ID of the variable
   */
  void RetrieveDofPositionInPetscOrderingSingleComponent(
    int* pGlobalIndexes,
    const CurBaseFiniteElement& rFE, 
    const felInt elementIndex, 
    const int idVariable, 
    const AO& rAO, 
    const Dof& rDof,
    const int iComp
    );

  /**
   * @brief This method searchs the global index of a given node for a given variable and component
   * @param rMesh The mesh considered
   * @param rListUnknown The list of unknown
   * @param rAO Abstract PETSc object
   * @param rDof The rDof object
   * @param rPhysicalVariable The physical variable considered
   * @param iNode The ID of the node
   * @param iComp The component ID of the variable
   * @param rEltType Element type where to search the node
   * @return The global index
   */
  int SearchGlobalIndexNode(
    GeometricMeshRegion& rMesh,
    const ListUnknown& rListUnknown,
    const AO& rAO, 
    const Dof& rDof,
    const PhysicalVariable& rPhysicalVariable,
    const int iNode,
    const int iComp,
    const GeometricMeshRegion::ElementType& rEltType 
    );
}

}

#endif