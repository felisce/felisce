//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef PETSCVECTOR_HPP
#error Include petscVector.hpp instead of petscVector_friend.hpp
#endif

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce 
{

inline PetscErrorCode waxpy(const PetscVector& w,PetscScalar const & alpha,const PetscVector& x,const PetscVector& y) 
{
  const PetscErrorCode code = VecWAXPY(w.m_self,alpha,x.m_self,y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode pointwiseMult(const PetscVector& w, const PetscVector& x,const PetscVector& y) 
{
  const PetscErrorCode code = VecPointwiseMult(w.m_self, x.m_self, y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode pointwiseDivide(const PetscVector& w, const PetscVector& x,const PetscVector& y) 
{
  const PetscErrorCode code = VecPointwiseDivide(w.m_self, x.m_self, y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode swap(const PetscVector& x,const PetscVector& y) 
{
  const PetscErrorCode code = VecSwap(x.m_self,y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode dot(const PetscVector& x,const PetscVector& y,PetscScalar* val) 
{
  const PetscErrorCode code = VecDot(x.m_self,y.m_self,val);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode maxPointwiseDivide(const PetscVector& x, const PetscVector& y, PetscScalar* max) 
{
  const PetscErrorCode code = VecMaxPointwiseDivide(x.m_self, y.m_self, max);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode gatherVec(PetscVector& v, PetscVector& seqV) 
{
  PetscErrorCode code;
  if(seqV.isNotNull())
    code= v.scatterToAllNotCreatingVector(seqV, INSERT_VALUES, SCATTER_FORWARD);
  else
    code = v.scatterToAll(seqV,INSERT_VALUES,SCATTER_FORWARD);
  CHKERRQ(code);
  return code;
}
}
