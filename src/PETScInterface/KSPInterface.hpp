//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Vicente Mataix Ferrandiz
//

#ifndef KSP_INTERFACE_HPP
#define KSP_INTERFACE_HPP

// System includes

// External includes:
#include "Core/NoThirdPartyWarning/Petsc/ksp.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "PETScInterface/petscVector.hpp"
#include "PETScInterface/SNESInterface.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @brief Interface for the abstract PETSc object that manages all Krylov methods. This is the object that manages the linear solves in PETSc (even those such as direct solvers that do no use Krylov accelerators).
 */
class KSPInterface
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of KSPInterface
  FELISCE_CLASS_POINTER_DEFINITION(KSPInterface);

  ///@}
  ///@name Life Cycle
  ///@{

  /**
   * @brief Default constructor
   */
  KSPInterface();

  /**
   * @brief Destructor
   */
  ~KSPInterface();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{
  
  /**
   * @brief Initializes the KSP object
   */
  void init();

  /**
   * @brief Initializes the KSP object with custom MPI_Comm
   */
  void init(MPI_Comm comm);

  /**
   * @brief Initializes the KSP object from a SNED object
   */
  void initFromSNES(SNESInterface::Pointer snes);

  /**
   * @brief This method sets the type of solver, preconditioner with the given values
   * @param[in] solver Name of the choosen solver
   * @param[in] preconditioner Name of the choosen preconditioner
   */
  void setKSPandPCType(const std::string solver, const std::string preconditioner);

  /**
   * @brief This method sets the tolerances with the given values
   * @param[in] relativeTolerance Relative convergence tolerance
   * @param[in] absoluteTolerance Absolute convergence tolerance
   * @param[in] maxIteration Maximum number of iterations
   * @param[in] gmresRestart Sets the number of search directions for GMRES and FGMRES before restart
   */
  void setTolerances(
    const double relativeTolerance,
    const double absoluteTolerance,
    const int    maxIteration,
    const int    gmresRestart
    );

  /**
   * @brief This method sets some options with the given values
   * @param[in] solver Name of the choosen solver
   * @param[in] usePrevSol Flag to use previous solution as initial guess
   */
  void setKSPOptions(const std::string solver, const bool usePrevSol);

  /**
   * @brief This method sets the matrix and some preconditioner options with the given values
   * @param[in] mat Petsc matrix
   * @param[in] preconditionerOption Preconditioner options
   */
  void setKSPOperator(PetscMatrix& mat, const std::string preconditionerOption);

  /**
   * @brief This method sets the initial guess to be zero
   */
  void setInitialGuessZero()
  {
    KSPSetInitialGuessNonzero(m_KSP, PETSC_FALSE);
  }
 
  /**
   * @brief This method sets the initial guess to be non-zero
   */ 
  void setInitialGuessNonzero()
  {
    KSPSetInitialGuessNonzero(m_KSP, PETSC_TRUE);
  }

  /**
   * @brief This method sets the diagonal scale
   */ 
  void setDiagonalScale()
  {
    KSPSetDiagonalScale(m_KSP, PETSC_TRUE);
  }

  /**
   * @brief This method sets the diagonal scale back after solve
   */ 
  void setDiagonalScaleFix()
  {
    KSPSetDiagonalScaleFix(m_KSP, PETSC_TRUE);
  }

  /**
   * @brief This function solves using the KSP object
   * @param[in] rRhs The rhs vector
   * @param[out] rSol The solution vector
   * @param[in] verbose Verbose level
   */
  void solve(PetscVector& rRhs, PetscVector& rSol, const int verbose);

  /**
   * @brief Destroy the KSP object
   */
  void destroy();

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief This function returns if the KSP object is initialized
   * @return true if the KSP object is initialized, false otherwise
   */
  bool& initialized()
  {
    return m_Initialized;
  }

  /**
   * @brief This function returns if the KSP object is initialized or not
   * @return true if the KSP object is initialized, false otherwise
   */
  bool initialized() const
  {
    return m_Initialized;
  }

  /**
   * @brief This function returns the KSP object
   * @return The KSP object
   */
  KSP& getKSP()
  {
    return m_KSP;
  }

  /**
   * @brief This function returns the KSP object (const version)
   * @return The KSP object
   */
  const KSP& getKSP() const
  {
    return m_KSP;
  }

  /**
   * @brief This function returns the PC object
   * @return The PC object
   */
  PC& getPC()
  {
    return m_PC;
  }

  /**
   * @brief This function returns the PC object (const version)
   * @return The PC object
   */
  const PC& getPC() const
  {
    return m_PC;
  }

  /**
   * @brief This function returns the number of interation
   * @return The number of iteration
   */
  PetscInt getNumOfIteration()
  {
    PetscInt its;
    KSPGetIterationNumber(m_KSP, &its);
    return its;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{
  
  bool m_Initialized = false; /// If the KSP class has been m_Initialized

  KSP m_KSP;                  /// The abstract PETSc object that manages all Krylov methods. This is the object that manages the linear solves in PETSc (even those such as direct solvers that do no use Krylov accelerators).

  PC m_PC;                    /// The preconditioner

  MPI_Comm m_comm = MPI_COMM_NULL;  /// The MPI communicator

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

///@}

}

#endif // KSP_INTERFACE_HPP
