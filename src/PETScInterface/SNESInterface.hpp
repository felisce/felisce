//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Vicente Mataix Ferrandiz
//

#ifndef SNES_INTERFACE_HPP
#define SNES_INTERFACE_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/snes.hpp"

// Project includes
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"
#include "PETScInterface/petscVector.hpp"
#include "PETScInterface/petscMatrix.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

// Forward declarations.
class LinearProblem;

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @brief Interface for the SNES solver
 */
class SNESInterface
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of SNESInterface
  FELISCE_CLASS_POINTER_DEFINITION(SNESInterface);

  ///@}
  ///@name Life Cycle
  ///@{

  /**
   * @brief Default constructor
   */
  SNESInterface();

  /**
   * @brief Destructor
   */
  ~SNESInterface();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Initializes the SNES solver
   */
  void init();

  /**
   * @brief Initializes the SNES solver with custom MPI_Comm
   */
  void init(MPI_Comm comm);

  /**
   * @brief Sets the function evaluation routine and function vector for use by the SNES routines in solving systems of nonlinear equations. 
   * @details The Newton-like methods typically solve linear systems of the form
   *     f'(x) x = -f(x),
   * where f'(x) denotes the Jacobian matrix and f(x) is the function. 
   * @param[in] r Vector to store function values
	 * @param[in] f Function evaluation routine; see SNESFunction for calling sequence details
	 * @param[in] ctx [optional] User-defined context for private data for the function evaluation routine (may be NULL) 
   */
  void setFunction(
    PetscVector& r, 
    PetscErrorCode (*f)(SNES,Vec,Vec,void*), 
    void* ctx = nullptr
    );
	
  /**
   * @brief Sets the function to compute Jacobian as well as the location to store the matrix. 
   * @param[in] Amat The matrix that defines the (approximate) Jacobian
   * @param[in] Pmat The matrix to be used in constructing the preconditioner, usually the same as Amat.
	 * @param[in] J Jacobian evaluation routine (if NULL then SNES retains any previously set value), see SNESJacobianFunction for details
	 * @param[in] ctx [optional] User-defined context for private data for the Jacobian evaluation routine (may be NULL) (if NULL then SNES retains any previously set value) 
   */
  void setJacobian(
    PetscMatrix& Amat,
    PetscMatrix& Pmat,
    PetscErrorCode (*J)(SNES,Vec,Mat,Mat,void*),
    void* ctx = nullptr
    );

  /**
   * @brief Gets the function to compute Jacobian as well as the location to store the matrix. 
   * @param[in] Amat The matrix that defines the (approximate) Jacobian
   * @param[in] Pmat The matrix to be used in constructing the preconditioner, usually the same as Amat.
	 * @param[in] J Jacobian evaluation routine (if NULL then SNES retains any previously set value), see SNESJacobianFunction for details
	 * @param[in] ctx [optional] User-defined context for private data for the Jacobian evaluation routine (may be NULL) (if NULL then SNES retains any previously set value) 
   */
  void getJacobian(
    PetscMatrix& Amat,
    PetscMatrix& Pmat,
    PetscErrorCode (*J)(SNES,Vec,Mat,Mat,void*),
    void* ctx = nullptr
    );

  /**
   * @brief Returns the vector where the solution update is stored. 
   * @param[in,out] rDeltaX Vector to store the solution increment
   */
  void getSolutionUpdate(PetscVector& rDeltaX);

  /**
   * @brief Sets various SNES and KSP parameters from user options
   */
  void setFromOptions()
  {
    SNESSetFromOptions(m_SNES);
  }

  /**
   * @brief Returns the KSP context for a SNES solver.  
   * @note Krylov method: https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/KSP/index.html
   * @param[in,out] pKSP The KSP context 
   */
  void getKSP(KSP* pKSP);

  /**
   * @brief Sets an ADDITIONAL function that is to be used at every iteration of the nonlinear solver to display the iteration's progress. 
   * @param[in] pLinearProblem The linear problem to be solved
   */
  void setMonitorSetLinearProblem(LinearProblem* pLinearProblem);

  /**
   * @brief This method sets the tolerances for the SNES solver with the given values
   * @note https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESSetTolerances.html
   * @param[in] absoluteTolerance Absolute convergence tolerance
   * @param[in] relativeTolerance Relative convergence tolerance
   * @param[in] solutionTolerance Convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x ||
   * @param[in] maxIterationSNES Maximum number of iterations
   * @param[in] maxFunctionEvaluatedSNES Maximum number of function evaluations (-1 indicates no limit) 
   * @param[in] divergenceTolerance Divergence tolerance
   */
  void setTolerances(
    const double absoluteTolerance,
    const double relativeTolerance,
    const double solutionTolerance,
    const int maxIterationSNES,
    const int maxFunctionEvaluatedSNES,
    const double divergenceTolerance
    );

  /**
   * @brief This function solves using the SNES object
   * @param[in,out] rX The vector to be solved
   * @return The number of SNES iterations
   */
  PetscInt solve(PetscVector& rX);
  
  ///@}
  ///@name Access
  ///@{

  /**
   * @brief This function returns if the SNES object must be used or not 
   * @return true if the SNES object must be used, false otherwise
   */
  bool& doUseSNES()
  {
    return m_doUseSNES;
  }

  /**
   * @brief This function returns if the SNES object must be used or not 
   * @return true if the SNES object must be used, false otherwise
   */
  bool doUseSNES() const
  {
    return m_doUseSNES;
  }

  /**
   * @brief This function returns the SNES object
   * @return The SNES object
   */
  SNES& getSNES()
  {
    return m_SNES;
  }

  /**
   * @brief This function returns the SNES object (const version)
   * @return The SNES object
   */
  const SNES& getSNES() const
  {
    return m_SNES;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  friend class KSPInterface;

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{
  
  bool m_doUseSNES = false;  /// Flag to use the SNES solver (auxiliary)

  SNES m_SNES;               /// Nonlinear solver 

  MPI_Comm m_comm = MPI_COMM_NULL;  /// The MPI communicator

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

///@}

}

#endif // SNES_INTERFACE_HPP
