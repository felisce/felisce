//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau
//                Dominique Chapelle
//                Miguel Fernandez
//                Vicente Mataix Ferrandiz
//

#ifndef PETSCVECTOR_HPP
#error Include petscVector.hpp instead of petscVector_inline.hpp
#endif

// System includes
#include <cstdlib>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisceParam.hpp"
#include "PETScInterface/petscMatrix.hpp"

namespace felisce 
{

//=======================
// CONSTRUCTOR/DESTRUCTOR
//=======================
inline PetscVector::PetscVector() 
{
  m_self = nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector::PetscVector(const PetscVector& b) 
{
  if ( b.isNotNull() ) {
    duplicateFrom(b);
    copyFrom(b);
  }
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator=(const PetscVector& b) 
{
  this->destroy();
  PetscObjectReference((PetscObject)b.m_self);
  m_self = b.m_self;
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator*(const PetscInt scalar)
{
  PetscVector a(*this);
  a.scale(PetscScalar(scalar));
  return a;
}
/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator*(const PetscInt scalar) const
{
  PetscVector a(*this);
  a.scale(PetscScalar(scalar));
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator*(const PetscScalar scalar)
{
  PetscVector a(*this);
  a.scale(scalar);
  return a;
}
/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator*(const PetscScalar scalar) const
{
  PetscVector a(*this);
  a.scale(scalar);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscScalar PetscVector::operator*(const PetscVector& b)
{
  PetscScalar a;
  dot(*this, b, &a);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscScalar PetscVector::operator*(const PetscVector& b) const
{
  PetscScalar a;
  dot(*this, b, &a);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator*=(const PetscInt scalar)
{
  scale(PetscScalar(scalar));
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator*=(const PetscScalar scalar)
{
  scale(scalar);
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator/(const PetscInt scalar)
{
  PetscVector a(*this);
  a.scale(1.0/PetscScalar(scalar));
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator/(const PetscInt scalar) const
{
  PetscVector a(*this);
  a.scale(1.0/PetscScalar(scalar));
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator/(const PetscScalar scalar)
{
  PetscVector a(*this);
  a.scale(1.0/scalar);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator/(const PetscScalar scalar) const
{
  PetscVector a(*this);
  a.scale(1.0/scalar);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator/=(const PetscInt scalar)
{
  scale(1.0/PetscScalar(scalar));
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator/=(const PetscScalar scalar)
{
  scale(1.0/scalar);
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator+(const PetscVector& b) 
{
  PetscVector a(*this);
  a.axpy(b);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator+(const PetscVector& b) const
{
  PetscVector a(*this);
  a.axpy(b);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator+=(const PetscVector& b) 
{
  axpy(b);
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator-(const PetscVector& b) 
{
  PetscVector a(*this);
  a.axpy(b, -1.0);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector PetscVector::operator-(const PetscVector& b) const
{
  PetscVector a(*this);
  a.axpby(b, -1.0);
  return a;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector& PetscVector::operator-=(const PetscVector& b) 
{
  axpy(b, -1.0);
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscScalar PetscVector::operator[] (const PetscInt Index) 
{
  double value; 
  PetscErrorCode code = getValue(Index, value);
  CHKERRQ(code);
  return value;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscScalar PetscVector::operator[] (const PetscInt Index) const
{
  double value; 
  PetscErrorCode code = getValue(Index, value);
  CHKERRQ(code);
  return value;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscVector::~PetscVector() 
{
  // In PETSc, Vec are handled like shared pointers.
  this->destroy();
}

//=================
// STATIC FUNCTIONS
//=================

inline PetscVector PetscVector::null() 
{
  PetscVector nullVector;
  return nullVector;
}

//===================
// CREATION FUNCTIONS
//===================

inline PetscErrorCode PetscVector::create(MPI_Comm comm) 
{
  
  PetscErrorCode code = VecCreate(comm,&m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::createSeq(MPI_Comm comm,PetscInt n) 
{
  PetscErrorCode code = VecCreateSeq(comm,n,&m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::createMPI(MPI_Comm comm,PetscInt n,PetscInt N) 
{
  PetscErrorCode code = VecCreateMPI(comm,n,N,&m_self);
  CHKERRQ(code);
  return code;
}

//======================
// DESTRUCTION FUNCTIONS
//======================

inline PetscErrorCode PetscVector::destroy() 
{
  PetscErrorCode code = VecDestroy(&m_self);
  CHKERRQ(code);
  return code;
}

//===============
// COPY FUNCTIONS
//===============

inline PetscErrorCode PetscVector::copyFrom(const PetscVector& vector) const 
{
  const PetscErrorCode code = VecCopy(vector.m_self,m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::duplicateFrom(const PetscVector& vector) 
{
  if ( this->isNotNull() )
    this->destroy();
  const PetscErrorCode code = VecDuplicate(vector.m_self,&m_self);
  CHKERRQ(code);
  return code;
}

//=========================
// MODIFICATION FUNCTIONS
//=========================

inline PetscErrorCode PetscVector::assembly() const 
{
  PetscErrorCode code = VecAssemblyBegin(m_self);
  CHKERRQ(code);
  code = VecAssemblyEnd(m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::restoreArray(PetscScalar **a) const 
{
  const PetscErrorCode code = VecRestoreArray(m_self,a);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::set(PetscScalar const & value) const 
{
  const PetscErrorCode code = VecSet(m_self,value);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setFromOptions() const 
{
  const PetscErrorCode code = VecSetFromOptions(m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setOption(VecOption op, PetscBool flg) const 
{
  const PetscErrorCode code = VecSetOption(m_self,op,flg);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setSizes(PetscInt n,PetscInt N) const 
{
  const PetscErrorCode code = VecSetSizes(m_self,n,N);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setType(VecType method) const 
{
  const PetscErrorCode code = VecSetType(m_self,method);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setValue(PetscInt row,PetscScalar value, InsertMode mode) const 
{
  const PetscErrorCode code = VecSetValue(m_self,row,value,mode);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setValueOnce(PetscInt row,PetscScalar value, InsertMode mode) const 
{
  PetscErrorCode code = 0;
  // Get rank
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  // Set value
  if (rank == 0) {
    code = VecSetValue(m_self,row,value,mode);
    CHKERRQ(code);
  }
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setValues(PetscInt ni,const PetscInt ix[],const PetscScalar y[],InsertMode iora) const 
{
  const PetscErrorCode code = VecSetValues(m_self,ni,ix,y,iora);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::setValuesOnce(PetscInt ni,const PetscInt ix[],const PetscScalar y[],InsertMode iora) const 
{
  PetscErrorCode code = 0;
  // Get rank
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  // Set value
  if (rank == 0) {
    code = VecSetValues(m_self,ni,ix,y,iora);
    CHKERRQ(code);
  }
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::zeroEntries() const 
{
  const PetscErrorCode code = VecZeroEntries(m_self);
  CHKERRQ(code);
  return code;
}

//=================
// ACCESS FUNCTIONS
//=================

inline PetscErrorCode PetscVector::getArray(PetscScalar** array) const 
{
  const PetscErrorCode code = VecGetArray(m_self,array);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscScalar* PetscVector::getArray() const
{
  PetscScalar* array;
  getArray(&array);
  return array;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::getAllValuesInAppOrdering(const AO& ao, std::vector<double>& array) const 
{
  PetscErrorCode code;
  felInt numDof;
  code = this->getSize(&numDof);
  CHKERRQ(code);
  array.resize(numDof);
  felInt identityMap[numDof];
  for (felInt i=0; i<numDof; i++)
    identityMap[i] = i;
  AOApplicationToPetsc(ao,numDof,identityMap);
  code = this->getValues(numDof,identityMap,array.data());
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::getLocalSize(PetscInt* localSize) const 
{
  const PetscErrorCode code = VecGetLocalSize(m_self,localSize);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscInt PetscVector::getLocalSize() const
{
  PetscInt local_size;
  getLocalSize(&local_size);
  return local_size;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::getSize(PetscInt* size) const 
{
  const PetscErrorCode code = VecGetSize(m_self,size);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscInt PetscVector::getSize() const
{
  PetscInt size;
  getSize(&size);
  return size;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::getValues(PetscInt ni,const PetscInt ix[],PetscScalar y[]) const 
{
  const PetscErrorCode code = VecGetValues(m_self,ni,ix,y);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::getValue(const PetscInt ix,PetscScalar& y) const
{
  return getValues(1, &ix, &y);
}

//==================
// BOOLEAN FUNCTIONS
//==================

inline bool PetscVector::isNull() 
{
  return m_self == nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline bool PetscVector::isNull() const 
{
  return m_self == nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline bool PetscVector::isNotNull() 
{
  return m_self != nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline bool PetscVector::isNotNull() const 
{
  return m_self != nullptr;
}

//=======================
// MATHEMATICAL FUNCTIONS
//=======================
inline PetscErrorCode PetscVector::axpbypcz(PetscScalar alpha, PetscScalar beta, PetscScalar gamma, const PetscVector& x, const PetscVector& y) const 
{
  const PetscErrorCode code = VecAXPBYPCZ(m_self,alpha,beta,gamma,x.m_self, y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::axpby(PetscScalar alpha, PetscScalar beta, const PetscVector& x) const 
{
  const PetscErrorCode code = VecAXPBY(m_self,alpha,beta,x.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::axpy(PetscScalar alpha, const PetscVector& x) const 
{
  // m_self = m_self + alpha * x.m_self
  const PetscErrorCode code = VecAXPY(m_self,alpha,x.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::max(PetscReal * val, PetscInt* p) const 
{
  const PetscErrorCode code = VecMax(m_self,p,val);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::max(PetscInt * p,PetscReal * val) const 
{
  const PetscErrorCode code = VecMax(m_self,p,val);
  CHKERRQ(code);
  return code;
}
/***********************************************************************************/
/***********************************************************************************/

inline PetscReal PetscVector::max() const 
{
  PetscReal max_value; 
  max(&max_value); 
  return max_value;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::min(PetscReal * val, PetscInt* p) const 
{
  const PetscErrorCode code = VecMin(m_self,p,val);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::min(PetscInt * p,PetscReal * val) const 
{
  const PetscErrorCode code = VecMin(m_self,p,val);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscReal PetscVector::min() const 
{
  PetscReal min_value; 
  min(&min_value); 
  return min_value;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::norm(NormType type, PetscReal* val) const 
{
  const PetscErrorCode code = VecNorm(m_self,type,val);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscReal PetscVector::norm(const NormType type) const 
{
  PetscReal norm_value = 0.0;
  norm(type, &norm_value);
  return norm_value;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::scale(PetscScalar scalar) const 
{
  const PetscErrorCode code = VecScale(m_self,scalar);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::shift(PetscScalar scalar) const 
{
  const PetscErrorCode code = VecShift(m_self,scalar);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::sum(PetscScalar *sum) const 
{
  const PetscErrorCode code = VecSum(m_self,sum);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscScalar PetscVector::sum() const 
{
  PetscReal sum_value = 0.0;
  sum(&sum_value);
  return sum_value;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::abs() const 
{
  const PetscErrorCode code = VecAbs(m_self);
  CHKERRQ(code);
  return code;
}

//========================
// COMMUNICATION FUNCTIONS
//========================

inline PetscErrorCode PetscVector::scatterToAll(PetscVector& y, InsertMode addv, ScatterMode mode) const 
{
  VecScatter inctx;
  PetscErrorCode code = VecScatterCreateToAll(m_self,&inctx,&y.m_self);
  CHKERRQ(code);
  code = VecScatterBegin(inctx,m_self,y.m_self,addv,mode);
  CHKERRQ(code);
  code = VecScatterEnd(inctx,m_self,y.m_self,addv,mode);
  CHKERRQ(code);
  code = VecScatterDestroy(&inctx);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::broadCastSequentialVector(MPI_Comm comm, int master ) const 
{
  double * array;
  int size;
  PetscErrorCode code = this->getSize(&size);
  CHKERRQ(code);
  code = this->getArray(&array);
  CHKERRQ(code);
  MPI_Bcast( array, size, MPI_DOUBLE, master, comm);
  code = this->restoreArray(&array);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::scatterToAllNotCreatingVector(PetscVector& y,InsertMode addv,ScatterMode mode) const 
{
  VecScatter inctx;
  PetscErrorCode code;
  code = VecScatterCreateToAll(m_self,&inctx,nullptr);     CHKERRQ(code);
  code = VecScatterBegin(inctx,m_self,y.m_self,addv,mode); CHKERRQ(code);
  code = VecScatterEnd(inctx,m_self,y.m_self,addv,mode);   CHKERRQ(code);
  code = VecScatterDestroy(&inctx);                        CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::scatterToZero(PetscVector& y,InsertMode addv,ScatterMode mode) const 
{
  VecScatter inctx;
  PetscErrorCode code = VecScatterCreateToZero(m_self,&inctx,&y.m_self);
  CHKERRQ(code);
  code = VecScatterBegin(inctx,m_self,y.m_self,addv,mode);
  CHKERRQ(code);
  code = VecScatterEnd(inctx,m_self,y.m_self,addv,mode);
  CHKERRQ(code);
  code = VecScatterDestroy(&inctx);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::scatterToZeroNotCreatingVector(PetscVector&  y,InsertMode addv,ScatterMode mode) const 
{
  VecScatter inctx;
  PetscErrorCode code;
  code = VecScatterCreateToZero(m_self,&inctx,nullptr);    CHKERRQ(code);
  code = VecScatterBegin(inctx,m_self,y.m_self,addv,mode); CHKERRQ(code);
  code = VecScatterEnd(inctx,m_self,y.m_self,addv,mode);   CHKERRQ(code);
  code = VecScatterDestroy(&inctx);                        CHKERRQ(code);
  return code;
}

//=================
// OUTPUT FUNCTIONS
//=================

inline PetscErrorCode PetscVector::view(const PetscViewer& viewer) const 
{
  PetscErrorCode code = VecView(m_self,viewer);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

// TODO: https://www.mcs.anl.gov/petsc/petsc-3.12/docs/manualpages/Viewer/PetscViewerFormat.html

inline PetscErrorCode PetscVector::saveInBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder) const 
{
  const std::string filenameBin = folder + filename + ".mb";
  PetscViewer binaryViewer;
  PetscErrorCode code;
  code = PetscViewerBinaryOpen(comm, filenameBin.c_str(), FILE_MODE_WRITE, &binaryViewer);     CHKERRQ(code);
  code = PetscViewerPushFormat(binaryViewer,PETSC_VIEWER_NATIVE);                              CHKERRQ(code);
  code = this->view(binaryViewer);                                                             CHKERRQ(code);
  code = PetscViewerDestroy(&binaryViewer);                                                    CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    const std::string msg = "Petsc Vector saved in " + filename + ".mb in folder " + folder + "\n";
    PetscPrintf(comm,"%s",msg.c_str());
  }
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::saveInMatlabFormat(MPI_Comm comm, const std::string filename, const std::string folder) const 
{
  const std::string filenameMatlab = folder + filename + ".m";
  PetscViewer matlabViewer;
  PetscErrorCode code;
  code = PetscViewerCreate(comm,&matlabViewer);
  CHKERRQ(code);
  code = PetscViewerASCIIOpen(comm,filenameMatlab.c_str(),&matlabViewer);
  CHKERRQ(code);
  code = PetscViewerPushFormat(matlabViewer,PETSC_VIEWER_ASCII_MATLAB);
  CHKERRQ(code);
  code = this->view(matlabViewer);
  CHKERRQ(code);
  code = PetscViewerDestroy(&matlabViewer);
  CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    const std::string msg = "Petsc Vector saved in " + filename + ".m in folder " + folder + "\n";
    PetscPrintf(comm,"%s",msg.c_str());
  }
  return code;
}

//================
// INPUT FUNCTIONS
//================

inline PetscErrorCode PetscVector::load(PetscViewer viewer) {
  const PetscErrorCode code = VecLoad(m_self,viewer);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::loadFromBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder) 
{
  const std::string filenameBin = folder + filename + ".mb";
  PetscViewer binaryViewer;
  PetscErrorCode code;
  code = PetscViewerBinaryOpen(comm, filenameBin.c_str(), FILE_MODE_READ, &binaryViewer);      CHKERRQ(code);
  code = PetscViewerPushFormat(binaryViewer,PETSC_VIEWER_NATIVE);                                    CHKERRQ(code);
  code = this->load(binaryViewer);                                                                   CHKERRQ(code);
  code = PetscViewerDestroy(&binaryViewer);                                                          CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    const std::string msg = "Petsc Vector loaded from " + filename + ".mb in folder " + folder + "\n";
    PetscPrintf(comm,"%s",msg.c_str());
  }
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscVector::loadFromMatlabFormat(MPI_Comm comm, const std::string filename, const std::string folder) {
  std::stringstream filenameBin;
  filenameBin<< folder << filename << ".m";
  PetscViewer matlabViewer;
  PetscErrorCode code;
  // code = 
  PetscViewerMatlabOpen(comm, filenameBin.str().c_str(), FILE_MODE_READ, &matlabViewer);      
  // CHKERRQ(code);
  code = PetscViewerPushFormat(matlabViewer,PETSC_VIEWER_ASCII_MATLAB);
  CHKERRQ(code);
  code = this->load(matlabViewer);
  CHKERRQ(code);
  code = PetscViewerDestroy(&matlabViewer);
  CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    std::stringstream msg;
    msg<<"Petsc Vector loaded from "<<filename<<".m in folder "<<folder<<std::endl;
    PetscPrintf(comm,"%s",msg.str().c_str());
  }
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

/* Load .vct file */
inline PetscErrorCode PetscVector::loadFromEnsight(const std::string fileName)
{
  PetscErrorCode code;

  felInt numDof;
  code = this->getSize(&numDof);
  PetscReal array[numDof];

  std::ifstream ensightFile ((fileName).c_str());
  std::string line; 

  getline(ensightFile,line); /* discard first line */
  PetscInt arrIndex = 0;
  if (ensightFile.is_open()){
    while (getline(ensightFile,line)){ 
      if (line.length() == 72){
        for (int i = 0; i < 6; i++){
          array[arrIndex] = atof(line.substr(0, 12).c_str()); arrIndex++;
          line = line.substr(12, line.length());
        }
      } else {
        for (int i = 0; i < 3; i++){
          array[arrIndex] = atof(line.substr(0, 12).c_str()); arrIndex++;
          line = line.substr(12, line.length());
        }
      }
    }
  } else {
      std::cout << "The file " + fileName + " cannot be opened \n";
  }

  PetscInt indexes[arrIndex + 1];  
  for (PetscInt i = 0; i < arrIndex + 1; i++)
    indexes[i] = i;

  code = this->setValues(arrIndex + 1, indexes, array, INSERT_VALUES); 
  CHKERRQ(code);

  return code;
}

//=================
// TO BE DEPRECATED
//=================

inline const Vec& PetscVector::toPetsc() const 
{
  return m_self;
}

/***********************************************************************************/
/***********************************************************************************/

inline Vec& PetscVector::toPetsc() {
  return m_self;
}

/**
 * @brief Multiply (*) operator
 * @param scalar The value that multiplies the vector
 * @param b The vector to be multiplied
 * @return The dot product of the current vector and b
 */
inline PetscVector operator*(
  const PetscInt scalar,
  const PetscVector& b
  )
{
  PetscVector a(b);
  a.scale(PetscScalar(scalar));
  return a;
}

/**
 * @brief Multiply (*) operator
 * @param b The vector to be multiplied
 * @param scalar The value that multiplies the vector
 * @return The dot product of the current vector and b
 */
inline PetscVector operator*(
  const PetscScalar scalar,
  const PetscVector& b
  )
{
  PetscVector a(b);
  a.scale(scalar);
  return a;
}

}
