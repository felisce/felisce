//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef PETSCMATRIX_HPP
#error Include petscMatrix.hpp instead of petscMatrix_friend.hpp
#endif

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "PETScInterface/petscVector.hpp"

namespace felisce 
{

inline PetscErrorCode multAdd(const PetscMatrix& mat,const PetscVector& v1,const PetscVector& v2,PetscVector& v3) 
{
  const PetscErrorCode code = MatMultAdd(mat.m_self,v1.m_self,v2.m_self,v3.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode mult(const PetscMatrix& mat,const PetscVector& x,PetscVector&  y) 
{
  // y = mat * x
  const PetscErrorCode code = MatMult(mat.m_self,x.m_self,y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode matMult(const PetscMatrix& A,const PetscMatrix& B,MatReuse scall,PetscReal fill,PetscMatrix& C) 
{
  const PetscErrorCode code = MatMatMult(A.m_self,B.m_self,scall,fill,&C.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode multTranspose(const PetscMatrix& mat,const PetscVector& x,PetscVector& y)
{
  const PetscErrorCode code = MatMultTranspose(mat.m_self,x.m_self,y.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode transposeMatMult(const PetscMatrix& A,const PetscMatrix& B,MatReuse scall,PetscReal fill,PetscMatrix& C) 
{
  const PetscErrorCode code = MatTransposeMatMult(A.m_self,B.m_self,scall,fill,&C.m_self);
  CHKERRQ(code);
  return code;
}


}
