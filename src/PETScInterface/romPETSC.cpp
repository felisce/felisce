//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "PETScInterface/romPETSC.hpp"

namespace felisce {

  RomPETSC::RomPETSC():
    m_fstransient(nullptr),
    m_buildBasis(false),
    m_buildMass(false),
    m_buildInverse(false),
    m_isFirstStep(false)
  {
    m_setPreconditionerOption = SAME_NONZERO_PATTERN;
  }

  RomPETSC::~RomPETSC() {
    if(m_buildBasis) {
      m_romBasis.destroy();
      m_romMatrix.destroy();
    }
    if(m_buildMass) {
      m_mass.destroy();
    }
    if(m_buildInverse) {
      m_inverseMatrix.destroy();
    }
    if (m_isFirstStep) {
      m_romRHS.destroy();
      m_romSolution.destroy();
    }
  }

  // initialize the pod basis parameters (passing AO& ao too - useful to find petsc dof indeces mapping)
  void RomPETSC::initializeRom(FelisceTransient::Pointer fstransient,MPI_Comm& comm, AO& ao) {
    m_fstransient = fstransient;
    m_verbose = FelisceParam::verbose();
    m_MPIcomm = comm;
    m_ao = ao;
    m_filenameBasis = FelisceParam::instance().podBasisFile;
    m_dimRomBasis = FelisceParam::instance().dimRomBasis;
    m_dimFullRomBasis = m_dimRomBasis;
    m_useInverse = true;
  }

  // initialize the pod basis parameters
  void RomPETSC::initializeRom(FelisceTransient::Pointer fstransient,MPI_Comm& comm) {
    m_fstransient = fstransient;
    m_verbose = FelisceParam::verbose();
    m_MPIcomm = comm;
    m_filenameBasis = FelisceParam::instance().podBasisFile;
    m_dimRomBasis = FelisceParam::instance().dimRomBasis;
    m_dimFullRomBasis = m_dimRomBasis;
    m_useInverse = true;
  }

  // Intialize basis matrix from matrix
  void RomPETSC::initializeRomBasis(const PetscMatrix& basis) {
    felInt lengthBasis;
    basis.getSize(&lengthBasis,&m_dimRomBasis);

    m_romBasis.duplicateFrom(basis,MAT_COPY_VALUES);
    m_romBasis.assembly(MAT_FINAL_ASSEMBLY);

    m_buildBasis = true;

  }

  // Intialize basis matrix from matrix
  void RomPETSC::initializeRomBasis(const std::vector<PetscVector*>& basis) {
    m_dimRomBasis = basis.size();
    felInt lengthBasis;
    (*basis[0]).getSize(&lengthBasis);
    m_romBasis.createDense(m_MPIcomm,PETSC_DECIDE,PETSC_DECIDE,lengthBasis,m_dimRomBasis,FELISCE_PETSC_NULLPTR);
    m_romBasis.setFromOptions();

    double tmpValue[lengthBasis];
    felInt ia[lengthBasis];
    for (felInt i=0; i< lengthBasis; i++) {
      ia[i] = i;
    }
    AOApplicationToPetsc(m_ao,lengthBasis,ia);
    int ja;
    for (int j=0; j<m_dimRomBasis; j++) {
      (*basis[j]).getValues(lengthBasis,ia,tmpValue);
      ja = j;
      AOApplicationToPetsc(m_ao,1,&ja);
      m_romBasis.setValues(lengthBasis,ia,1,&ja,tmpValue,INSERT_VALUES);
    }
    m_romBasis.assembly(MAT_FINAL_ASSEMBLY);

    m_buildBasis = true;

  }

  // read POD basis from file
  // format:
  // # of elements
  // # of dof
  // element1 || ...||  elementK
  // element1 || ...||  elementK
  // ...
  // Intialize basis matrix from file
  void RomPETSC::initializeRomBasis() {
    m_filenameBasis = FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().podBasisFile;
    if (m_verbose) {
      std::cout << " Rom::initBasis: Reading from file "<< m_filenameBasis << std::endl;
    }
    std::ifstream basisFile(m_filenameBasis.c_str(),std::ios_base::in);
    if (!basisFile) {
      FEL_ERROR(" Rom::initBasis(): Impossible to read file: "+m_filenameBasis+" (POD basis)");
    }

    felInt lengthBasis;
    basisFile >> lengthBasis; // read n. of rows
    basisFile >> m_dimFullRomBasis; // read n. of columns (total)
    if (m_verbose) {
      std::cout << "Read matrix of " << lengthBasis << " rows and " << m_dimFullRomBasis << " columns.\n";
    }

    if (m_dimFullRomBasis<m_dimRomBasis) {
      std::cout << " requested dimBasis = " << m_dimRomBasis << ", available elements = "<< m_dimFullRomBasis << std::endl;
      FEL_ERROR(" Rom::initBasis: not enough basis element available !");
    }

    m_romBasis.createDense(m_MPIcomm, PETSC_DECIDE, PETSC_DECIDE, lengthBasis, m_dimRomBasis, FELISCE_PETSC_NULLPTR);
    m_romBasis.setFromOptions();

    double tmpValue[m_dimRomBasis];
    felInt ia[m_dimRomBasis];
    felInt ja;
    char line[1024];
    double k = 1.0;
    for (felInt j=0; j<lengthBasis; j++) { //rows
      ja = j;
      if ((j+1.0)>=(k*0.1*lengthBasis)) {
        std::cout << "... " << 10*k << "% ";
        k=k+1.0;
      }
      for (felInt i=0; i< m_dimRomBasis; i++) { // columns
        ia[i] = i;
        basisFile >> tmpValue[i];
      }
      basisFile.getline(line, 1024, '\n');
      AOApplicationToPetsc(m_ao,1,&ja);
      m_romBasis.setValues(1,&ja,m_dimRomBasis,ia,tmpValue,INSERT_VALUES);
    }
    m_romBasis.assembly(MAT_FINAL_ASSEMBLY);

    m_buildBasis = true;
  }

  void RomPETSC::gatherVector(PetscVector& parVector, PetscVector& seqVector) {
    PetscVector vout;
    parVector.scatterToAll(vout,INSERT_VALUES,SCATTER_FORWARD);
    seqVector.duplicateFrom(vout);
    seqVector.copyFrom(vout);
    vout.destroy();
  }

  // given a full std::vector f, compute \hat f = Phi^t f
  void RomPETSC::fullToReducedRHS(PetscVector& fullVector) {
    if (m_fstransient->iteration == 1) {
      m_romRHS.create(m_MPIcomm);
      m_romRHS.setSizes(PETSC_DECIDE, m_dimRomBasis);
      m_romRHS.setFromOptions();
      m_romRHS.zeroEntries();
    }

    multTranspose(m_romBasis,fullVector,m_romRHS);

    m_isFirstStep = true;
  }

  // given a full matrix A, compute \hat A = Phi^t A \Phi
  void RomPETSC::fullToReducedMatrix(PetscMatrix& fullMatrix) {
    PetscMatrix tmpMat;
    // m_tmpMat = fullMatrix * m_romBasis
    matMult(fullMatrix, m_romBasis, MAT_INITIAL_MATRIX, PETSC_DEFAULT, tmpMat);
    // m_romMatrix = m_romBasis^T * tmpMat
    transposeMatMult(m_romBasis, tmpMat, MAT_INITIAL_MATRIX, PETSC_DEFAULT, m_romMatrix);

    tmpMat.destroy();

    if (m_useInverse)
      calculateInverse();

  }

  // solve the reduced linear system
  void RomPETSC::solveALP() {
    if (m_fstransient->iteration == 1) {
      m_romSolution.duplicateFrom(m_romRHS);
    }
    m_romSolution.zeroEntries();

    if (m_verbose) {
      std::cout << " Rom:: solveROM() - solve reduced model "<< std::endl;
    }

    mult(m_inverseMatrix, m_romRHS, m_romSolution);

  }


  // solve the reduced linear system
  void RomPETSC::solveROM(PetscVector& solution) {
    if (m_fstransient->iteration == 1) {
      m_romSolution.duplicateFrom(m_romRHS);
    }
    m_romSolution.zeroEntries();

    if (m_verbose) {
      std::cout << " Rom:: solveROM() - solve reduced model "<< std::endl;
    }

    if (!m_useInverse) {

      PCFactorSetReuseFill(m_pc, PETSC_TRUE);

      KSPSetOperators(m_ksp,m_romMatrix.toPetsc(),m_romMatrix.toPetsc());
      if(strcmp(FelisceParam::instance().setPreconditionerOption[0].c_str(), "SAME_PRECONDITIONER") == 0)
        KSPSetReusePreconditioner(m_ksp, PETSC_TRUE);
      else
        KSPSetReusePreconditioner(m_ksp, PETSC_FALSE);

      KSPSetFromOptions(m_ksp);
      KSPSolve(m_ksp,m_romRHS.toPetsc(),m_romSolution.toPetsc());

      PetscInt its;
      KSPGetIterationNumber(m_ksp,&its);
      if (m_verbose) {
        PetscPrintf(m_MPIcomm, "\n/================Information about solver==============/\n");
        PetscPrintf(m_MPIcomm, "Solver use: <%s>.\n",FelisceParam::instance().solver[0].c_str());
        PetscPrintf(m_MPIcomm, "Preconditioner use: <%s>.\n",FelisceParam::instance().preconditioner[0].c_str());
        PetscPrintf(m_MPIcomm, "Preconditioner re-use option: <%s>.\n",FelisceParam::instance().setPreconditionerOption[0].c_str());
        if (m_verbose > 1 ) {
          PetscPrintf(m_MPIcomm, "Relative tolerance: %lf.\n",FelisceParam::instance().relativeTolerance[0]);
          PetscPrintf(m_MPIcomm, "Absolute tolerance: %lf.\n",FelisceParam::instance().absoluteTolerance[0]);
          PetscPrintf(m_MPIcomm, "Maximum of iterations: %d.\n",FelisceParam::instance().maxIteration[0]);
        }
        PetscPrintf(m_MPIcomm, "Number of iterations to inverse matrix: %d\n", its);
      }
    } else {
      mult(m_inverseMatrix, m_romRHS, m_romSolution);
    }

    if (m_verbose) {
      std::cout << " Rom:: solveROM() - get full solution "<< std::endl;
    }
    mult(m_romBasis,m_romSolution,solution);
  }

  void RomPETSC::setSolver(KSP ksp, PC pc, MatStructure setPreconditionerOption)
  {
    IGNORE_UNUSED_ARGUMENT(ksp);
    IGNORE_UNUSED_ARGUMENT(pc);
    IGNORE_UNUSED_ARGUMENT(setPreconditionerOption);

    const auto& r_instance = FelisceParam::instance();
    KSPCreate(m_MPIcomm,&m_ksp);
    PCCreate(m_MPIcomm,&m_pc);
    const auto& solver = r_instance.solver[0];
    if ( solver == "preonly" || solver == "mumps" || solver == "superlu_dist" )
      KSPSetType(m_ksp, KSPPREONLY);
    else
      KSPSetType(m_ksp, solver.c_str());
    PCSetFromOptions(m_pc);
    KSPGetPC(m_ksp,&m_pc);
    PCSetType(m_pc,r_instance.preconditioner[0].c_str());
    KSPSetTolerances(m_ksp,r_instance.relativeTolerance[0],r_instance.absoluteTolerance[0],PETSC_DEFAULT,r_instance.maxIteration[0]);

      // TODO: MATSOLVERPASTIX once it works
      if ( solver == "preonly" || solver == "mumps" ) {
        PCFactorSetMatSolverType(m_pc, MATSOLVERMUMPS);
      } else
    #ifdef FELISCE_WITH_SUPERLU
      if ( solver == "superlu_dist" ) {
        PCFactorSetMatSolverType(m_pc, MATSOLVERSUPERLU_DIST);
      } else
    #else
      if ( solver == "superlu_dist" ) {
        FEL_WARNING("SuperLUDist not available, we proceed using MUMPS")
        PCFactorSetMatSolverType(m_pc, MATSOLVERMUMPS);
      } else
    #endif
      {
        if ( r_instance.initSolverWithPreviousSolution[0] ) {
          KSPSetInitialGuessNonzero(m_ksp, PETSC_TRUE);
        }
      }

    {
      // TODO: to be removed
      PetscOptionsSetValue(NULL, "-mat_mumps_icntl_20", "0");    
    }
  }

  void RomPETSC::calculateInverse() {
    //Create the identityMatrix
    PetscMatrix identityMatrix;
    identityMatrix.createDense(m_MPIcomm, PETSC_DECIDE, PETSC_DECIDE, m_dimRomBasis, m_dimRomBasis, FELISCE_PETSC_NULLPTR);
    identityMatrix.setFromOptions();
    identityMatrix.zeroEntries();
    m_inverseMatrix.duplicateFrom(identityMatrix,MAT_COPY_VALUES);
    identityMatrix.shift(1.0);

    // ILU factorization of_romMatrix
    IS perm;
    ISCreateStride(m_MPIcomm, m_dimRomBasis, 0, 1, &perm);
    MatFactorInfo iluinfo;
    iluinfo.fill = 1.0;
    m_romMatrix.luFactor(perm, perm, &iluinfo);

    // Solve m_romMatrix * m_inverseMatrix = identityMatrix
    MatMatSolve(m_romMatrix.toPetsc(), identityMatrix.toPetsc(), m_inverseMatrix.toPetsc());

    identityMatrix.destroy();

    m_buildInverse = true;

  }


  void RomPETSC::readCase(int& precisionSol,std::vector<std::string>& variable, felInt& snapSize) {
    // Solution files.
    std::string caseFileName = FelisceParam::instance().resultDir + "/" + FelisceParam::instance().prefixName[0] + ".case";

    std::ifstream casefile(caseFileName.c_str(), std::ios_base::in);
    char line[1024];
    std::string aux;
    if ( !casefile ) {
      std::cout << " ERROR: Can not open file "+caseFileName+"."<< std::endl;
      exit(1);
    } else {
      if (FelisceParam::verbose() > 0) {
        std::cout << "Reading " << caseFileName << std::endl;
      }
    }

    casefile.getline(line, 1024, ':');
    casefile.getline(line, 1024, ':');
    casefile.getline(line, 1024, ' ');
    casefile.getline(line, 1024, ' ');
    casefile.getline(line, 1024, '\n');
    std::string geoFileName = FelisceParam::instance().resultDir + "/" + line;
    std::ifstream geoFile(geoFileName.c_str(),std::ios_base::in);
    for (int i=0; i<5; i++) {
      geoFile.getline(line,1024,'\n');
    }
    geoFile >> snapSize;
    geoFile.close();

    bool endCase = false;
    while (!endCase) {
      casefile.getline(line, 1024, ':');
      casefile.getline(line, 1024, ' ');
      casefile.getline(line, 1024, ' ');
      casefile.getline(line, 1024, ' ');
      casefile.getline(line, 1024, '.');
      variable.emplace_back(line);
      casefile.getline(line, 1024, '.');
      aux = line;
      precisionSol = aux.length();
      casefile.getline(line, 1024, '\n');
      casefile.getline(line, 1024, ' ');
      aux = line;
      if (aux == "TIME\ntime") {
        endCase = true;
      }
    }
    casefile.close();

    if (m_verbose) {
      std::cout << "Variables:\n";
      for (std::size_t k=0; k < variable.size(); k++) {
        std::cout << variable[k] << std::endl;
      }
      std::cout << "Numbers of nodes per variable : " << snapSize << std::endl;
    }

  }

  void RomPETSC::readEnsight(double* scalarValue,std::string sclFile, felInt snapSize) {
    std::string snapFileName = FelisceParam::instance().resultDir + sclFile + ".scl";
    std::ifstream snap(snapFileName.c_str(), std::ios_base::in);

    if ( !snap ) {
      std::cout << " ERROR: Can not open file "+snapFileName+"."<< std::endl;
    } else {
      if (FelisceParam::verbose() > 1) {
        std::cout << "Reading " << snapFileName << std::endl;
      }
    }
    char newline[1024];
    snap.getline(newline, 1024, '\n');
    for (felInt j=0; j < snapSize; j++) {
      snap >> scalarValue[j];
    }
    snap.close();
  }


}
