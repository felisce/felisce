//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef PETSCMATRIX_HPP
#define PETSCMATRIX_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/mat.hpp"

// Project includes
#include "PETScInterface/petscVector_fwd.hpp"

namespace felisce 
{
//! \brief Wrapper to Petsc Matrix type (Mat)
class PetscMatrix 
{
  //=================
  // FRIEND FUNCTIONS
  //=================

  friend PetscErrorCode matMult(const PetscMatrix& A,const PetscMatrix& B,MatReuse scall,PetscReal fill, PetscMatrix& C);
  friend PetscErrorCode mult(const PetscMatrix& mat,const PetscVector& x,PetscVector&  y);
  friend PetscErrorCode multAdd(const PetscMatrix& mat, const PetscVector& v1, const PetscVector& v2, PetscVector& v3);
  friend PetscErrorCode multTranspose(const PetscMatrix& mat,const PetscVector& x,PetscVector&  y);
  friend PetscErrorCode transposeMatMult(const PetscMatrix& A,const PetscMatrix& B,MatReuse scall,PetscReal fill,PetscMatrix& C);

public:

  //===================
  // CREATION FUNCTIONS
  //===================

  PetscErrorCode createAIJ(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt M,PetscInt N,PetscInt d_nz,const PetscInt d_nnz[],PetscInt o_nz,const PetscInt o_nnz[]);
  
  PetscErrorCode createDense(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt M,PetscInt N,PetscScalar *data);
  
  PetscErrorCode createSeqDense(MPI_Comm comm,PetscInt m,PetscInt n,PetscScalar *data);
  
  PetscErrorCode createMPIAdj(MPI_Comm comm,PetscInt m,PetscInt N,PetscInt *i,PetscInt *j,PetscInt *values);
  
  PetscErrorCode createSeqAIJ(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt nz,const PetscInt nnz[]);

  //=======================
  // CONSTRUCTOR/DESTRUCTOR
  //=======================
  PetscMatrix();
  
  PetscMatrix(const PetscMatrix& b);
  
  PetscMatrix& operator=(const PetscMatrix& b);
  
  ~PetscMatrix();
  
  //======================
  // DESTRUCTION FUNCTIONS
  //======================

  PetscErrorCode destroy();

  //===============
  // COPY FUNCTIONS
  //===============

  PetscErrorCode copyFrom(const PetscMatrix& mat,MatStructure str = SAME_NONZERO_PATTERN );
  
  PetscErrorCode duplicateFrom(const PetscMatrix& mat,MatDuplicateOption op = MAT_DO_NOT_COPY_VALUES );

  //=======================
  // MODIFICATION FUNCTIONS
  //=======================

  PetscErrorCode assembly(MatAssemblyType type);
  
  PetscErrorCode diagonalSet(const PetscVector& diag,InsertMode is);
  
  PetscErrorCode mpiAIJSetPreallocationCSR(const PetscInt i[],const PetscInt j[],const PetscScalar v[]);
  
  PetscErrorCode seqAIJSetPreallocationCSR(const PetscInt i[],const PetscInt j[],const PetscScalar v[]);
  
  PetscErrorCode setFromOptions();
  
  PetscErrorCode setOption(MatOption op,PetscBool flg);
  
  PetscErrorCode setValue(PetscInt row,PetscInt col,PetscScalar value,InsertMode mode);
  
  PetscErrorCode setValues(PetscInt m, const PetscInt idxm[], PetscInt n, const PetscInt idxn[],const PetscScalar v[], InsertMode addv);
  
  PetscErrorCode zeroEntries();
  
  PetscErrorCode zeroRows(PetscInt numRows,const PetscInt rows[], const PetscScalar diag = 1.0);
  
  PetscErrorCode zeroRowsColumns(PetscInt numRows,const PetscInt rows[], const PetscScalar diag = 1.0);

  //=================
  // ACCESS FUNCTIONS
  //=================

  PetscErrorCode getSize(PetscInt *m,PetscInt *n) const;

  PetscErrorCode getVecs(Vec * ptr, PetscVector& left) const;
  
  PetscErrorCode getVecs(PetscVector& right, Vec * ptr) const;
  
  PetscErrorCode getVecs(PetscVector& right, PetscVector& left) const ;

  //==================
  // BOOLEAN FUNCTIONS
  //==================

  /**
   * @brief This returns if the vector is null
   * @return If the vector is null
   */
  bool isNull();

  /**
   * @brief This returns if the vector is null (constant version)
   * @return If the vector is null
   */
  bool isNull() const;

  /**
   * @brief This returns if the vector is not null
   * @return If the vector is not null
   */
  bool isNotNull();

  /**
   * @brief This returns if the vector is not null (constant version)
   * @return If the vector is not null
   */
  bool isNotNull() const;

  //=======================
  // MATHEMATICAL FUNCTIONS
  //=======================

  PetscErrorCode axpy(PetscScalar a,PetscMatrix& X,MatStructure str);
  
  PetscErrorCode luFactor(IS row,IS col,const MatFactorInfo *info);
  
  PetscErrorCode diagonalScale(const PetscVector& leftDiagonalMatrix, const PetscVector& rightDiagonalMatrix);
  
  PetscErrorCode scale(PetscScalar scalar);
  
  PetscErrorCode setUnfactored();
  
  PetscErrorCode shift(const PetscScalar a);
  
  PetscErrorCode norm(NormType type, PetscReal* val) const;

  //=================
  // OUTPUT FUNCTIONS
  //=================

  PetscErrorCode view(PetscViewer viewer = PETSC_VIEWER_STDOUT_WORLD) const;
  
  PetscErrorCode saveInBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder ) const;
  
  PetscErrorCode saveInMatlabFormat(MPI_Comm comm, const std::string filename, const std::string folder ) const;

  //================
  // INPUT FUNCTIONS
  //================
  
  PetscErrorCode load(PetscViewer viewer);
  
  PetscErrorCode loadFromBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder );
  
  //===============
  // TEST FUNCTIONS
  //===============
  
  bool isTransposeOf(PetscMatrix& A, double tol);

  //=================
  // TO BE DEPRECATED
  //=================

  const Mat& toPetsc() const;
  Mat& toPetsc();
  
  //=============
  // DATA MEMBERS
  //=============

private:

  Mat m_self;

};

}

#include "petscMatrix_inline.hpp"
#include "petscMatrix_friend.hpp"

#endif // PETSCMATRIX_HPP
