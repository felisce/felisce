//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/mpiInfo.hpp"
#include "Solver/linearProblem.hpp"
#include "PETScInterface/SNESInterface.hpp"

namespace felisce
{
namespace { // anonymous
/**
 * @brief This function is used to monitor evolution of the convergence.
 * Is is intended to be passed to SNESMonitorSet()
 * @param[in] snes SNES object. Not explicitly used but required by Petsc prototype.
 * @param[in] its Index of iteration.
 * @param[in] norm Current L^2 norm.
 * @param[in] contextAsVoid Optional user-defined function context. In our case is is a pointer to LinearProblem object.
 */
PetscErrorCode snesViewerLinearProblem(
  SNES snes, 
  PetscInt its, 
  PetscReal norm, 
  void* contextAsVoid
  ) 
{
  PetscFunctionBegin;

  FEL_ASSERT(contextAsVoid);

  const LinearProblem* p_linear_problem = static_cast<const LinearProblem*>(contextAsVoid);
  FEL_ASSERT(p_linear_problem);

  PetscReal evaluation_state_Min, evaluation_state_Max;

  const PetscVector& r_vector = p_linear_problem->evaluationState();
  r_vector.min(&evaluation_state_Min);
  r_vector.max(&evaluation_state_Max);

  PetscObject snes_object = reinterpret_cast<PetscObject>(snes);

  PetscReal residual_min, residual_max;
  p_linear_problem->vector().min(&residual_min);
  p_linear_problem->vector().max(&residual_max);

  int tablevel;
  PetscObjectGetTabLevel(snes_object,&tablevel);
  PetscViewerASCIIAddTab(PETSC_VIEWER_STDOUT_WORLD, tablevel);
  PetscViewerASCIIPrintf(PETSC_VIEWER_STDOUT_WORLD,
                          "%d SNES Function Norm is %14.12e and displacement extrema are %8.6e and %8.6e. Residual between %8.6e and %8.6e\n",
                          its,
                          norm,
                          evaluation_state_Min, evaluation_state_Max,
                          residual_min, residual_max
                          );

  PetscViewerASCIISubtractTab(PETSC_VIEWER_STDOUT_WORLD, tablevel);

  PetscFunctionReturn(0);
}
} // namespace anonymous

/***********************************************************************************/
/***********************************************************************************/

SNESInterface::SNESInterface()
{
}

/***********************************************************************************/
/***********************************************************************************/

SNESInterface::~SNESInterface()
{
  if (m_doUseSNES) {
    SNESDestroy(&m_SNES);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::init()
{
  init( MpiInfo::petscComm() );
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::init(MPI_Comm comm)
{
  m_comm = comm;

  // First case of Newton-Raphson method such as implemented by Petsc.
  // By extracting the KSP and PC contexts from the SNES context, we can then
  // directly call any KSP, and PC routines to set various options.
  SNESCreate(m_comm, &m_SNES);

  // We deactivate line-search in order to use standard Newton-Raphson
  SNESSetType(m_SNES,SNESNEWTONLS);
  SNESLineSearch ls;
  SNESGetLineSearch(m_SNES, &ls);
  SNESLineSearchSetType(ls,SNESLINESEARCHBASIC);
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::getSolutionUpdate(PetscVector& rDeltaX)
{
  Vec increment;
  // Warning: SNESGetSolutionUpdate returns -delta U
  SNESGetSolutionUpdate(m_SNES, &increment);

  // Copy to our vector
  VecCopy(increment,rDeltaX.toPetsc());
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::setFunction(PetscVector& r, PetscErrorCode (*f)(SNES,Vec,Vec,void*), void* ctx)
{
  SNESSetFunction(m_SNES, r.toPetsc(), f, ctx);
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::setJacobian(
  PetscMatrix& Amat,
  PetscMatrix& Pmat,
  PetscErrorCode (*J)(SNES,Vec,Mat,Mat,void*),
  void* ctx
  )
{
  SNESSetJacobian(m_SNES, Amat.toPetsc(), Pmat.toPetsc(), J, ctx);
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::getJacobian(
  PetscMatrix& Amat,
  PetscMatrix& Pmat,
  PetscErrorCode (*J)(SNES,Vec,Mat,Mat,void*),
  void* ctx
  )
{
  SNESGetJacobian(m_SNES, &Amat.toPetsc(), &Pmat.toPetsc(), &J, &ctx);
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::getKSP(KSP* pKSP)
{
  SNESGetKSP(m_SNES, pKSP);
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::setMonitorSetLinearProblem(LinearProblem* pLinearProblem)
{
  SNESMonitorSet(m_SNES, snesViewerLinearProblem, pLinearProblem, FELISCE_PETSC_NULLPTR);
}

/***********************************************************************************/
/***********************************************************************************/

void SNESInterface::setTolerances(
  const double absoluteTolerance,
  const double relativeTolerance,
  const double solutionTolerance,
  const int maxIterationSNES,
  const int maxFunctionEvaluatedSNES,
  const double divergenceTolerance
  )
{
  SNESSetTolerances(m_SNES,
                    absoluteTolerance,
                    relativeTolerance,
                    solutionTolerance,
                    maxIterationSNES,
                    maxFunctionEvaluatedSNES
                    );
  if (divergenceTolerance > 0.0) {
    SNESSetDivergenceTolerance(m_SNES, divergenceTolerance);
  }
}

/***********************************************************************************/
/***********************************************************************************/

PetscInt SNESInterface::solve(PetscVector& rX)
{
  SNESSolve(m_SNES, FELISCE_PETSC_NULLPTR, rX.toPetsc());

  SNESConvergedReason cvg_reason;
  SNESGetConvergedReason(m_SNES,&cvg_reason);
  PetscInt its;
  SNESGetIterationNumber(m_SNES,&its);
  if (cvg_reason > 0 || cvg_reason == SNES_DIVERGED_MAX_IT) { // NOTE: See https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/SNES/SNESConvergedReason.html
    switch (cvg_reason) {
        case SNES_CONVERGED_FNORM_ABS:      PetscPrintf(m_comm, "SNES converged in %d iterations due to ||F|| < atol \n", its); break;
        case SNES_CONVERGED_FNORM_RELATIVE: PetscPrintf(m_comm, "SNES converged in %d iterations due to ||F|| < rtol*||F_initial|| \n", its); break;
        case SNES_CONVERGED_SNORM_RELATIVE: PetscPrintf(m_comm, "SNES converged in %d iterations due to Newton computed step size small; || delta x || < stol || x || \n", its); break;
        case SNES_CONVERGED_ITS:            PetscPrintf(m_comm, "SNES converged in %d iterations due to maximum iterations reached  \n", its); break;
        case SNES_DIVERGED_MAX_IT:          PetscPrintf(m_comm, "SNES diverged due to SNESSolve() has reached the maximum number of iterations requested: %d. Calculations continues as normal \n", its); break;
        default:                            PetscPrintf(m_comm, "SNES converged in %d iterations \n", its);
    }
  } else {
    std::string error_message = "Convergence failed in SNES solver. ";
    switch (cvg_reason) {
      // case SNES_CONVERGED_ITERATING:      error_message +=  "SNES diverged due to maximum iterations reached"; break;
      case SNES_DIVERGED_FUNCTION_DOMAIN: error_message +=  "SNES diverged due to the new x location passed the function is not in the domain of F"; break;
      case SNES_DIVERGED_FUNCTION_COUNT:  error_message +=  "SNES diverged due to the user provided function has been called more times then the final argument to SNESSetTolerances() "; break;
      case SNES_DIVERGED_LINEAR_SOLVE:    error_message +=  "SNES diverged due to the linear solve failed"; break;
      case SNES_DIVERGED_FNORM_NAN:       error_message +=  "SNES diverged due to the 2-norm of the current function evaluation is not-a-number (NaN), this is usually caused by a division of 0 by 0"; break;
      // case SNES_DIVERGED_MAX_IT:          error_message +=  "SNES diverged due to SNESSolve() has reached the maximum number of iterations requested"; break;
      case SNES_DIVERGED_LINE_SEARCH:     error_message +=  "SNES diverged due to the line search failed "; break;
      case SNES_DIVERGED_INNER:           error_message +=  "SNES diverged due to inner solve failed"; break;
      case SNES_DIVERGED_LOCAL_MIN:       error_message +=  "SNES diverged due to || J^T b || is small, implies converged to local minimum of F()"; break;
      case SNES_DIVERGED_DTOL:            error_message +=  "SNES diverged due to || F || > divtol*||F_initial||"; break;
      case SNES_DIVERGED_JACOBIAN_DOMAIN: error_message +=  "SNES diverged due to Jacobian calculation does not make sense"; break;
      case SNES_DIVERGED_TR_DELTA:        error_message +=  "SNES diverged due to SNES_DIVERGED_TR_DELTA"; break;
      default:                            error_message +=  "";
    }
    FEL_ERROR(error_message);
  }

  return its;
}

}
