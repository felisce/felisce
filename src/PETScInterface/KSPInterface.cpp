//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/mpiInfo.hpp"
#include "PETScInterface/KSPInterface.hpp"

namespace felisce
{

KSPInterface::KSPInterface()
{
}

/***********************************************************************************/
/***********************************************************************************/

KSPInterface::~KSPInterface()
{
  if (m_Initialized) {
    KSPDestroy(&m_KSP);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::init()
{
  init( MpiInfo::petscComm() );
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::init(MPI_Comm comm)
{
  m_comm = comm;
  KSPCreate(m_comm, &m_KSP);
  m_Initialized = true;
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::initFromSNES(SNESInterface::Pointer snes)
{
  m_comm = snes->m_comm;
  snes->getKSP(&m_KSP);
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::setKSPandPCType(
  const std::string solver,
  const std::string preconditioner
  )
{
  if ( solver == "preonly" || solver == "mumps" || solver == "superlu_dist" )
    KSPSetType(m_KSP, KSPPREONLY);
  else
    KSPSetType(m_KSP, solver.c_str());

  KSPGetPC(m_KSP,&m_PC);
  PCSetType(m_PC, preconditioner.c_str());

  PCSetFromOptions(m_PC);
  KSPSetFromOptions(m_KSP);
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::setTolerances(
  const double relativeTolerance,
  const double absoluteTolerance,
  const int    maxIteration,
  const int    gmresRestart
  )
{
  KSPSetTolerances(m_KSP,relativeTolerance, absoluteTolerance, PETSC_DEFAULT, maxIteration);
  KSPGMRESSetRestart(m_KSP, gmresRestart); // ignored by petsc if solver != gmres
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::setKSPOptions(const std::string solver, const bool usePrevSol)
{
    if ( solver == "preonly" || solver == "mumps" ) {
      PCFactorSetMatSolverType(m_PC, MATSOLVERMUMPS);
    } else
  #ifdef FELISCE_WITH_SUPERLU
    if ( solver == "superlu_dist" ) {
      PCFactorSetMatSolverType(m_PC, MATSOLVERSUPERLU_DIST);
    } else
  #else
    if ( solver == "superlu_dist" ) {
      FEL_WARNING("SuperLUDist not available, we proceed using MUMPS")
      PCFactorSetMatSolverType(m_PC, MATSOLVERMUMPS);
    } else
  #endif
    {
      if ( usePrevSol ) {
        KSPSetInitialGuessNonzero(m_KSP, PETSC_TRUE);
      }
    }

  {
    // TODO: to be removed
    PetscOptionsSetValue(NULL, "-mat_mumps_icntl_20", "0");
  }

  PCSetFromOptions(m_PC);
  KSPSetFromOptions(m_KSP);
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::setKSPOperator(PetscMatrix& mat, const std::string preconditionerOption)
{
  PCFactorSetReuseFill(m_PC, PETSC_TRUE);

  if ( preconditionerOption == "SAME_PRECONDITIONER" ) {
    KSPSetReusePreconditioner(m_KSP, PETSC_TRUE);
    PCSetReusePreconditioner(m_PC, PETSC_TRUE);
  } else {
    KSPSetReusePreconditioner(m_KSP, PETSC_FALSE);
    PCSetReusePreconditioner(m_PC, PETSC_FALSE);
  }

  KSPSetOperators(m_KSP, mat.toPetsc(), mat.toPetsc());
  KSPSetFromOptions(m_KSP);
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::solve(PetscVector& rRhs, PetscVector& rSol, const int verbose)
{
  KSPSolve(m_KSP, rRhs.toPetsc(), rSol.toPetsc());

  KSPConvergedReason cvg_reason;
  KSPGetConvergedReason(m_KSP, &cvg_reason);
  PetscInt its;
  KSPGetIterationNumber(m_KSP, &its);

  if (cvg_reason > 0) {

    if (verbose > 0) {
      switch (cvg_reason) {
          case KSP_CONVERGED_RTOL_NORMAL:     PetscPrintf(m_comm, "KSP converged in %d iterations due to ||R|| < rtol*||R_initial|| (NORMAL)\n", its); break;
          case KSP_CONVERGED_ATOL_NORMAL:     PetscPrintf(m_comm, "KSP converged in %d iterations due to ||R|| < atol (NORMAL)\n", its); break;
          case KSP_CONVERGED_RTOL:            PetscPrintf(m_comm, "KSP converged in %d iterations due to ||R|| < rtol*||R_initial|| \n", its); break;
          case KSP_CONVERGED_ATOL:            PetscPrintf(m_comm, "KSP converged in %d iterations due to ||R|| < atol \n", its); break;
          case KSP_CONVERGED_ITS:             PetscPrintf(m_comm, "KSP converged in %d iteration, direct solver \n", its); break;
#if PETSC_VERSION_LT(3, 19, 0)
          case KSP_CONVERGED_CG_NEG_CURVE:    PetscPrintf(m_comm, "KSP converged in %d iterations, due to KSP_CONVERGED_CG_NEG_CURVE, returned only by KSPNASH used in SNES solver \n", its); break;
          case KSP_CONVERGED_CG_CONSTRAINED:  PetscPrintf(m_comm, "KSP converged in %d iterations, due to KSP_CONVERGED_CG_CONSTRAINED, returned only by KSPSTCG used in SNES solver \n", its); break;
          case KSP_CONVERGED_STEP_LENGTH:     PetscPrintf(m_comm, "KSP converged in %d iterations, due to KSP_CONVERGED_STEP_LENGTH, returned only by KSPLGTR used in SNES solver \n", its); break;
#else
          case KSP_CONVERGED_NEG_CURVE:    PetscPrintf(m_comm, "KSP converged in %d iterations, due to KSP_CONVERGED_NEG_CURVE, returned only by KSPNASH, KSPSTCG and KSPLGTR used in SNES solver \n", its); break;
          case KSP_CONVERGED_STEP_LENGTH:     PetscPrintf(m_comm, "KSP converged in %d iterations, due to KSP_CONVERGED_STEP_LENGTH, returned only by KSPNASH, KSPSTCG and KSPLGTR used in SNES solver \n", its); break;
#endif
          case KSP_CONVERGED_HAPPY_BREAKDOWN: PetscPrintf(m_comm, "KSP converged in %d iterations due to happy breakdown \n", its); break;
          default:                            PetscPrintf(m_comm, "KSP converged in %d iterations \n", its);
      }
    }

    KSPType ksptype;
    KSPGetType(m_KSP, &ksptype);
    PCType pctype;
    PCGetType(m_PC, &pctype);
    PetscReal rnorm;
    KSPGetResidualNorm(m_KSP, &rnorm);

    PetscPrintf(m_comm, "KSP %s-%s converged in %d iterations (residual = %e)\n", ksptype, pctype, its, rnorm);
  } else {
    std::string error_message = "Convergence failed in KSP solver. ";
    switch (cvg_reason) {
        case KSP_CONVERGED_ITERATING:       PetscPrintf(m_comm, "KSP failed due to a call to KSPGetConvergedReason() while KSPSolve() is still running \n"); break;
        case KSP_DIVERGED_NULL:             PetscPrintf(m_comm, "KSP failed due to KSP_DIVERGED_NULL \n"); break;
        case KSP_DIVERGED_ITS:              PetscPrintf(m_comm, "KSP diverged due to KSPSolve() has reached the maximum number of iterations %d \n", its); break;
        case KSP_DIVERGED_DTOL:             PetscPrintf(m_comm, "KSP diverged due to ||R|| > divtol*||R_initial|| \n"); break;
        case KSP_DIVERGED_BREAKDOWN:        PetscPrintf(m_comm, "KSP failed due to a generic breakdown in method. See KSP_DIVERGED_BREAKDOWN reason \n"); break;
        case KSP_DIVERGED_BREAKDOWN_BICG:   PetscPrintf(m_comm, "KSP failed due to a generic breakdown in BICG method. See KSP_DIVERGED_BREAKDOWN_BICG reason \n"); break;
        case KSP_DIVERGED_NONSYMMETRIC:     PetscPrintf(m_comm, "KSP failed due to matrix or preconditioner not symmetric while the Krylov method requires symmetry \n"); break;
        case KSP_DIVERGED_INDEFINITE_PC:    PetscPrintf(m_comm, "KSP failed due to indefinite preconditioner (with both positive and negative eigenvalues) while the Krylov method requires it be positive definite \n"); break;
        case KSP_DIVERGED_NANORINF:         PetscPrintf(m_comm, "KSP failed due to residual norm equal to Not-a-number or Inf \n"); break;
        case KSP_DIVERGED_INDEFINITE_MAT:   PetscPrintf(m_comm, "KSP failed due to indefinite matrix (with both positive and negative eigenvalues) while the Krylov method requires it be positive definite \n"); break;
        case KSP_DIVERGED_PC_FAILED:        PetscPrintf(m_comm, "KSP failed due to was not possible to build or use the requested preconditioner (usually due to zero pivot in a factorization) \n"); break;
        default:                            PetscPrintf(m_comm, "\n");
    }
    FEL_ERROR(error_message);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void KSPInterface::destroy()
{
  if (m_Initialized) {
    KSPDestroy(&m_KSP);
    m_Initialized = false;
  }
}

}
