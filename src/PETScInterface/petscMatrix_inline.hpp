//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef PETSCMATRIX_HPP
#error Include petscMatrix.hpp instead of petscMatrix_inline.hpp
#endif

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisce_error.hpp"
#include "PETScInterface/petscVector.hpp"
#include "Core/felisceParam.hpp"
#include "Core/filesystemUtil.hpp"

namespace felisce {

//=======================
// CONSTRUCTOR/DESTRUCTOR
//=======================

inline PetscMatrix::PetscMatrix() 
{
  m_self = nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscMatrix::PetscMatrix(const PetscMatrix& b) 
{
  if ( b.isNotNull() ) {
    duplicateFrom(b);
    copyFrom(b);
  }
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscMatrix& PetscMatrix::operator=(const PetscMatrix& b) 
{
  this->destroy();
  PetscObjectReference((PetscObject)b.m_self);
  m_self = b.m_self;
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscMatrix::~PetscMatrix() 
{
  // In PETSc, Mat are handled like shared pointers.
  this->destroy();
}

//===================
// CREATION FUNCTIONS
//===================

inline PetscErrorCode PetscMatrix::createDense(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt M,PetscInt N,PetscScalar *data) 
{
  const PetscErrorCode code = MatCreateDense(comm,m,n,M,N,data,&m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::createSeqDense(MPI_Comm comm,PetscInt m,PetscInt n,PetscScalar *data) 
{
  const PetscErrorCode code = MatCreateSeqDense(comm,m,n,data,&m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::createMPIAdj(MPI_Comm comm,PetscInt m,PetscInt N,PetscInt *i,PetscInt *j,PetscInt *values) 
{
  const PetscErrorCode code = MatCreateMPIAdj(comm,m,N,i,j,values,&m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::createSeqAIJ(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt nz,const PetscInt nnz[])
{
  const PetscErrorCode code = MatCreateSeqAIJ(comm,m,n,nz,nnz,&m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::createAIJ(MPI_Comm comm,PetscInt m,PetscInt n,PetscInt M,PetscInt N,PetscInt d_nz,const PetscInt d_nnz[],PetscInt o_nz,const PetscInt o_nnz[]) 
{
  const PetscErrorCode code = MatCreateAIJ(comm,m,n,M,N,d_nz,d_nnz,o_nz,o_nnz,&m_self);
  CHKERRQ(code);
  return code;
}

//======================
// DESTRUCTION FUNCTIONS
//======================

inline PetscErrorCode PetscMatrix::destroy() 
{
  const PetscErrorCode code = MatDestroy(&m_self);
  CHKERRQ(code);
  return code;
}

//===============
// COPY FUNCTIONS
//===============

inline PetscErrorCode PetscMatrix::copyFrom(const PetscMatrix& mat,MatStructure str) 
{
  const PetscErrorCode code = MatCopy(mat.m_self,m_self,str);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::duplicateFrom(const PetscMatrix& mat,MatDuplicateOption op) 
{
  const PetscErrorCode code = MatDuplicate(mat.m_self,op,&m_self);
  CHKERRQ(code);
  return code;
}

//=======================
// MODIFICATION FUNCTIONS
//=======================

inline PetscErrorCode PetscMatrix::assembly(MatAssemblyType type) 
{
  PetscErrorCode code = MatAssemblyBegin(m_self,type);
  CHKERRQ(code);
  code = MatAssemblyEnd(m_self,type);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::diagonalSet(const PetscVector& diag,InsertMode is) 
{
  const PetscErrorCode code = MatDiagonalSet(m_self,diag.m_self,is);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::mpiAIJSetPreallocationCSR(const PetscInt i[],const PetscInt j[],const PetscScalar v[]) 
{
  const PetscErrorCode code = MatMPIAIJSetPreallocationCSR(m_self,i,j,v);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::seqAIJSetPreallocationCSR(const PetscInt i[],const PetscInt j[],const PetscScalar v[]) 
{
  const PetscErrorCode code = MatSeqAIJSetPreallocationCSR(m_self,i,j,v);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::setFromOptions() 
{
  const PetscErrorCode code = MatSetFromOptions(m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::setOption(MatOption op,PetscBool flg) 
{
  const PetscErrorCode code = MatSetOption(m_self,op,flg);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::setValue(PetscInt row,PetscInt col,PetscScalar value,InsertMode mode) 
{
  const PetscErrorCode code = MatSetValue(m_self,row,col,value,mode);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::setValues(PetscInt m,const PetscInt idxm[],PetscInt n,const PetscInt idxn[],const PetscScalar v[],InsertMode addv) 
{
  const PetscErrorCode code = MatSetValues(m_self,m,idxm,n,idxn,v,addv);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::zeroEntries() 
{
  const PetscErrorCode code = MatZeroEntries(m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::zeroRows(PetscInt numRows,const PetscInt rows[], const PetscScalar diag) 
{
  const PetscErrorCode code = MatZeroRows(m_self,numRows,rows,diag,nullptr,nullptr);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::zeroRowsColumns(PetscInt numRows,const PetscInt rows[],const PetscScalar diag) 
{
  const PetscErrorCode code = MatZeroRowsColumns(m_self,numRows,rows,diag,nullptr,nullptr);
  CHKERRQ(code);
  return code;
}

//=================
// ACCESS FUNCTIONS
//=================

inline PetscErrorCode PetscMatrix::getSize(PetscInt *m,PetscInt *n) const 
{
  const PetscErrorCode code = MatGetSize(m_self,m,n);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::getVecs(Vec */*FELISCE_PETSC_NULLPTR*/, PetscVector& left) const 
{
  const PetscErrorCode code = MatCreateVecs(m_self, FELISCE_PETSC_NULLPTR, &left.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::getVecs(PetscVector& right, Vec */*FELISCE_PETSC_NULLPTR*/) const 
{
  const PetscErrorCode code = MatCreateVecs(m_self,&right.m_self, FELISCE_PETSC_NULLPTR);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::getVecs(PetscVector& right,PetscVector& left) const 
{
  const PetscErrorCode code = MatCreateVecs(m_self,&right.m_self,&left.m_self);
  CHKERRQ(code);
  return code; 
}

//==================
// BOOLEAN FUNCTIONS
//==================

inline bool PetscMatrix::isNull() 
{
  return m_self == nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline bool PetscMatrix::isNull() const 
{
  return m_self == nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline bool PetscMatrix::isNotNull() 
{
  return m_self != nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

inline bool PetscMatrix::isNotNull() const 
{
  return m_self != nullptr;
}

//=======================
// MATHEMATICAL FUNCTIONS
//=======================

inline PetscErrorCode PetscMatrix::axpy(PetscScalar a,PetscMatrix& X,MatStructure str) 
{
  const PetscErrorCode code = MatAXPY(m_self,a,X.m_self,str);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::luFactor(IS row,IS col,const MatFactorInfo *info) 
{
  const PetscErrorCode code = MatLUFactor(m_self,row,col,info);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::diagonalScale(const PetscVector& leftDiagonalMatrix, const PetscVector& rightDiagonalMatrix) 
{
  const PetscErrorCode code = MatDiagonalScale(m_self,leftDiagonalMatrix.m_self,rightDiagonalMatrix.m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::scale(PetscScalar scalar) 
{
  const PetscErrorCode code = MatScale(m_self,scalar);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::setUnfactored() 
{
  const PetscErrorCode code = MatSetUnfactored(m_self);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::shift(const PetscScalar a) 
{
  const PetscErrorCode code = MatShift(m_self,a);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::norm(NormType type, PetscReal* val) const 
{
  const PetscErrorCode code = MatNorm(m_self,type,val);
  CHKERRQ(code);
  return code;
}

//=================
// OUTPUT FUNCTIONS
//=================

inline PetscErrorCode PetscMatrix::view(PetscViewer viewer) const 
{
  const PetscErrorCode code = MatView(m_self,viewer);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

//  std::ostream& operator<<(std::ostream& out, PetscMatrix& mm){
//   
//    Mat m = mm.toPetsc();
//
//    PetscInt nCols, nRows;
//
//    MatGetSize(m, &nRows, &nCols);
//    const PetscScalar *row;
//
//    out << "[\n";
//    for (PetscInt i=0; i<nRows; i++){
//      MatGetRow(m, i, NULL, NULL, &row);
//      for (PetscInt j=0; j<nCols; j++)
//        out << row[j] << " ";
//      out << std::endl;
//    }
//    out << " ]\n";  
//
//    return out;
//  }

/***********************************************************************************/
/***********************************************************************************/

// TODO: https://www.mcs.anl.gov/petsc/petsc-3.12/docs/manualpages/Viewer/PetscViewerFormat.html

inline PetscErrorCode PetscMatrix::saveInBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder) const 
{
  std::string foldername = folder + "/";
  // TODO: Replace std::filesystem
  std::string command = "mkdir -p " + foldername;
  int ierr=system(command.c_str());
  if (ierr>0)
    FEL_ERROR(std::string("Impossible to save "+filename+"matrix.\nProblem in creating folder: "+foldername+"\n").c_str());
  std::stringstream filenameBin;
  filenameBin<< foldername << filename << ".mb";
  PetscViewer binaryViewer;
  PetscErrorCode code;
  code = PetscViewerBinaryOpen(comm, filenameBin.str().c_str(), FILE_MODE_WRITE, &binaryViewer);     
  CHKERRQ(code);
  code = PetscViewerPushFormat(binaryViewer,PETSC_VIEWER_NATIVE);                                    
  CHKERRQ(code);
  code = this->view(binaryViewer);                                                                   
  CHKERRQ(code);
  code = PetscViewerDestroy(&binaryViewer);                                                          
  CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    std::stringstream msg;
    msg<<"Petsc Matrix saved in "<<filename<<".mb in folder "<<foldername<<std::endl;
    PetscPrintf(comm, "%s",msg.str().c_str());
  }
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::saveInMatlabFormat(MPI_Comm comm, const std::string filename, const std::string folder) const 
{
  std::stringstream filenameMatlab;
  filenameMatlab<< folder << filename << ".m";
  PetscViewer matlabViewer;
  PetscErrorCode code;
  code = PetscViewerCreate(comm,&matlabViewer);
  CHKERRQ(code);
  code = PetscViewerASCIIOpen(comm,filenameMatlab.str().c_str(),&matlabViewer);
  CHKERRQ(code);
  code = PetscViewerPushFormat(matlabViewer,PETSC_VIEWER_ASCII_MATLAB);
  CHKERRQ(code);
  code = this->view(matlabViewer);
  CHKERRQ(code);
  code = PetscViewerDestroy(&matlabViewer);
  CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    std::stringstream msg;
    msg<<"Petsc Matrix saved in "<<filename<<".m in folder "<<folder<<std::endl;
    PetscPrintf(comm,"%s",msg.str().c_str());
  }
  return code;
}

//================
// INPUT FUNCTIONS
//================

inline PetscErrorCode PetscMatrix::load(PetscViewer viewer) 
{
  const PetscErrorCode code = MatLoad(m_self,viewer);
  CHKERRQ(code);
  return code;
}

/***********************************************************************************/
/***********************************************************************************/

inline PetscErrorCode PetscMatrix::loadFromBinaryFormat(MPI_Comm comm, const std::string filename, const std::string folder) {
  std::stringstream filenameBin;
  filenameBin<< folder << filename << ".mb";
  if ( ! filesystemUtil::fileExists( filenameBin.str() ) )
    FEL_ERROR("Requested file "+ filenameBin.str()+" does not exists");
  PetscViewer binaryViewer;
  PetscErrorCode code;
  code = PetscViewerBinaryOpen(comm, filenameBin.str().c_str(), FILE_MODE_READ, &binaryViewer);      
  CHKERRQ(code);
  code = PetscViewerPushFormat(binaryViewer,PETSC_VIEWER_NATIVE);                                     
  CHKERRQ(code);
  code = this->load(binaryViewer);                                                                   
  CHKERRQ(code);
  code = PetscViewerDestroy(&binaryViewer);                                                          
  CHKERRQ(code);
  if ( FelisceParam::verbose() > 1 ) {
    std::stringstream msg;
    msg<<"Petsc Matrix loaded from "<<filename<<".mb in folder "<<folder<<std::endl;
    PetscPrintf(comm,"%s",msg.str().c_str());
  }
  return code;
}

//===============
// TEST FUNCTIONS
//===============

inline bool PetscMatrix::isTransposeOf(PetscMatrix& A, double tol)
{
  PetscBool isTranspose;
  const PetscErrorCode code = MatIsTranspose(m_self, A.m_self, tol, &isTranspose);
  CHKERRQ(code);
  return isTranspose;
}

//=================
// TO BE DEPRECATED
//=================

inline const Mat& PetscMatrix::toPetsc() const 
{
  return m_self;
}

/***********************************************************************************/
/***********************************************************************************/

inline Mat& PetscMatrix::toPetsc() 
{
  return m_self;
}

}
