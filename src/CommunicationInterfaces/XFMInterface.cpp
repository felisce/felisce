//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. C. Corti
//

/*!
  \file     XFMInterface.hpp
  \authors  D. C. Corti
  \date     00/00/2022
  \brief    interface for XFM module in libXfm
*/

// System includes
#include <numeric>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "CommunicationInterfaces/XFMInterface.hpp"


namespace felisce {

XFMInterface::XFMInterface() 
{
  #ifdef FELISCE_WITH_LIBXFM

    m_xfm             = nullptr;
    m_isXFMInit       = false;
    m_writeXFEMmeshes = false;

    m_IntMethod = None;

    m_fluidMeshName = "fluid";
    m_strucMeshName = "struc";

    m_dim       = -1;
    m_numVerMsh = 0;
    m_numVerItf = 0;
    m_numEltMsh = 0;
    m_numEltItf = 0;
    m_numBndMsh = 0;
    m_iteration = 0;

    m_listStrucVertCoor        = nullptr;
    m_listStrucVertCoorInitial = nullptr;
    m_listStrucRef             = nullptr;

    m_sizLstIntEltMsh = 0;
    m_ptrIntEltMsh    = nullptr;
    m_lstIntEltMsh    = nullptr;
    m_sizLstItfEltMsh = 0;
    m_ptrItfEltMsh    = nullptr;
    m_lstItfEltMsh    = nullptr;
    m_verSgn          = nullptr;

    m_sizLstIntEltItf = 0;
    m_ptrIntEltItf    = nullptr;
    m_lstIntEltItf    = nullptr;

    m_nbrIntVerMsh    = 0;
    m_crdMsh          = nullptr;
    m_intVerIdx       = nullptr;
    m_intVerSgn       = nullptr;
    m_nbrIntEltMsh    = 0;
    m_eltMsh          = nullptr;
    m_elt2MshEltMsh   = nullptr;
    m_eltSgn          = nullptr;

    m_nbrIntVerItf    = 0;
    m_crdItf          = nullptr;
    m_nbrIntEltItf    = 0;
    m_elt2DItf        = nullptr;
    m_elt3DItf        = nullptr;
    m_elt2MshEltItf   = nullptr;
    m_elt2ItfElt      = nullptr;

    m_sizLstIntBndMsh = 0;
    m_ptrIntBndMsh    = nullptr;
    m_lstIntBndMsh    = nullptr;
    m_nbrIntEltBnd    = 0;
    m_elt2DBnd        = nullptr;
    m_elt3DBnd        = nullptr;

    m_sizLstTipSrf    = 0;
    m_ptrTipSrf       = nullptr;
    m_lstTipSrf       = nullptr;
    m_nbrIntTipElt    = 0;
    m_tipElt2MshElt   = nullptr;
    m_nbrIntTipSrf    = 0;
    m_tipSrf3D        = nullptr;
    m_tipSrf2D        = nullptr;
    m_tipSrfSgn       = nullptr;

    m_nbrIntVerFro    = 0;
    m_verFro          = nullptr;
    m_vecFro          = nullptr;

    m_mshItfIdx       = nullptr;
    m_bndItfIdx       = nullptr;

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

XFMInterface::~XFMInterface() 
{
  #ifdef FELISCE_WITH_LIBXFM
      
    if( MpiInfo::rankProc() == 0 ) {
      if( m_isXFMInit )
        m_xfm = Xfm_FreeLibrary(m_xfm);
    }
  
    if( m_listStrucVertCoor != nullptr )
      delete [] m_listStrucVertCoor;

    if( m_listStrucVertCoorInitial != nullptr )
      delete [] m_listStrucVertCoorInitial;

    if( m_listStrucRef != nullptr )
      delete [] m_listStrucRef;

    if( MpiInfo::rankProc() != 0 ) {

      if( m_ptrIntEltMsh != nullptr ) 
        delete [] m_ptrIntEltMsh;
      if( m_lstIntEltMsh != nullptr )
        delete [] m_lstIntEltMsh;
      if( m_ptrItfEltMsh != nullptr )
        delete [] m_ptrItfEltMsh;
      if( m_lstItfEltMsh != nullptr )
        delete [] m_lstItfEltMsh;
      if( m_verSgn != nullptr )
        delete [] m_verSgn;

      if( m_ptrIntEltItf != nullptr )
        delete [] m_ptrIntEltItf;
      if( m_lstIntEltItf != nullptr )
        delete [] m_lstIntEltItf;

      if( m_crdMsh != nullptr )
        delete [] m_crdMsh;
      if( m_intVerIdx != nullptr )
        delete [] m_intVerIdx;
      if( m_intVerSgn != nullptr )
        delete [] m_intVerSgn;
      if( m_eltMsh != nullptr )
        delete [] m_eltMsh;
      if( m_elt2MshEltMsh != nullptr )
        delete [] m_elt2MshEltMsh;
      if( m_eltSgn != nullptr )
        delete [] m_eltSgn;

      if( m_crdItf != nullptr )
        delete [] m_crdItf;
      if( m_elt2DItf != nullptr )
        delete [] m_elt2DItf;
      if( m_elt3DItf != nullptr )
        delete [] m_elt3DItf;
      if( m_elt2MshEltItf != nullptr )
        delete [] m_elt2MshEltItf;
      if( m_elt2ItfElt != nullptr )
        delete [] m_elt2ItfElt;

      if( m_ptrIntBndMsh != nullptr )
        delete [] m_ptrIntBndMsh;
      if( m_lstIntBndMsh != nullptr )
        delete [] m_lstIntBndMsh;
      if( m_elt2DBnd != nullptr )
        delete [] m_elt2DBnd;
      if( m_elt3DBnd != nullptr )
        delete [] m_elt3DBnd;

      if( m_ptrTipSrf != nullptr )
        delete [] m_ptrTipSrf;
      if( m_lstTipSrf != nullptr )
        delete [] m_lstTipSrf;
      if( m_tipElt2MshElt != nullptr )
        delete [] m_tipElt2MshElt;
      if( m_tipSrf3D != nullptr )
        delete [] m_tipSrf3D;
      if( m_tipSrf2D != nullptr )
        delete [] m_tipSrf2D;
      if( m_tipSrfSgn != nullptr )
        delete [] m_tipSrfSgn;

      if( m_verFro != nullptr )
        delete [] m_verFro;
      if( m_vecFro != nullptr )
        delete [] m_vecFro;

      if( m_mshItfIdx != nullptr )
        delete [] m_mshItfIdx;
      if( m_bndItfIdx != nullptr )
        delete [] m_bndItfIdx;
    }

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::initialize(const GeometricMeshRegion& fluidMesh, const GeometricMeshRegion& strucMesh)
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Check if class already initialized
    if( m_isXFMInit ) 
      FEL_ERROR("XFMInterface already initialized");

    //- Get the dimension of the fluid mesh
    m_dim = fluidMesh.domainDim();

    //- Intersection method
    if( FelisceParam::instance().confIntersection )
      m_IntMethod = ConfCutByItfMesh;
    else
      m_IntMethod = NonConfCutByItfMesh;


    //--- Initialize interface
    if ( MpiInfo::rankProc() == 0 )
      m_xfm = Xfm_NewLibrary(m_dim, m_strucMeshName.data(), m_fluidMeshName.data(), m_IntMethod, 0, 0);


    //--- Initialize meshes dimensions
    const GeometricMeshRegion::ElementType fluidType = fluidMesh.bagElementTypeDomain()[0];
    const GeometricMeshRegion::ElementType strucType = strucMesh.bagElementTypeDomain()[0];
    const GeometricMeshRegion::ElementType bndryType = fluidMesh.bagElementTypeDomainBoundary()[0];
    
    m_numVerMsh = fluidMesh.numPoints();
    m_numEltMsh = fluidMesh.numElements(fluidType);
    m_numBndMsh = fluidMesh.numElements(bndryType);
    m_numVerItf = strucMesh.numPoints();
    m_numEltItf = strucMesh.numElements(strucType);

    //- Build fictitious structures to physical structures map
    std::unordered_map<int,int> ref2XFMlabel;
    buildFictitiousToStructureMap(strucMesh, ref2XFMlabel);


    //--- Initialize meshes in XFM library: points and elements
    if ( MpiInfo::rankProc() == 0 ) {

      const felInt numFluidPtPerElt = GeometricMeshRegion::m_numVerticesPerElt[fluidType];
      const felInt numStrucPtPerElt = GeometricMeshRegion::m_numVerticesPerElt[strucType];
      const felInt numBndryPtPerElt = GeometricMeshRegion::m_numVerticesPerElt[bndryType];

      //- List of vertices coordinates of the fluid
      double3d*listFluidVertCoor = new double3d[m_numVerMsh + 1];
      for(felInt iVert=0; iVert<m_numVerMsh; ++iVert) {
        for(std::size_t iCoor=0; iCoor<3; ++iCoor) {
          listFluidVertCoor[iVert+1][iCoor] = fluidMesh.listPoint(iVert)[iCoor];
        }
      }

      //- List of element of the fluid
      int4d* listFluidElt = new int4d[m_numEltMsh + 1];
      for(felInt iel=0; iel<m_numEltMsh; ++iel) {
        for(felInt iPt=0; iPt<numFluidPtPerElt; ++iPt) {
          listFluidElt[iel + 1][iPt] = fluidMesh.listElements(fluidType)[numFluidPtPerElt*iel + iPt] + 1;
        }
      }

      //- List of vertices coordinates (initial and the one that will be updated in the future)
      m_listStrucVertCoor        = new double3d[m_numVerItf + 1];
      m_listStrucVertCoorInitial = new double3d[m_numVerItf + 1];
      for(felInt iVert=0; iVert<m_numVerItf; ++iVert) {
        for(std::size_t iCoor=0; iCoor<3; ++iCoor) {
          m_listStrucVertCoor[iVert + 1][iCoor]        = strucMesh.listPoint(iVert)[iCoor];
          m_listStrucVertCoorInitial[iVert + 1][iCoor] = strucMesh.listPoint(iVert)[iCoor];
        }
      }

      //- Count number not boundary surface elements
      std::vector<felInt>& notBndLabel = FelisceParam::instance().notBoundarySurfaceLabels;
      felInt numIntBndMsh = 0;
      for(auto itRef = fluidMesh.intRefToBegEndMaps[bndryType].begin(); itRef != fluidMesh.intRefToBegEndMaps[bndryType].end(); itRef++)
        if( find(notBndLabel.begin(), notBndLabel.end(), itRef->first) != notBndLabel.end() )
          numIntBndMsh += itRef->second.second;

      int3d *listFluidBndElt2d = nullptr, *listStrucElt2d = nullptr;
      int4d *listFluidBndElt3d = nullptr, *listStrucElt3d = nullptr;

      if ( m_dim == 2 ){

        //- List of bundary element of the fluid
        listFluidBndElt2d = new int3d[m_numBndMsh + 1];
        for(felInt iel=0; iel<m_numBndMsh; ++iel) {
          for(felInt iPt=0; iPt<numBndryPtPerElt; ++iPt)
            listFluidBndElt2d[iel + 1][iPt] = fluidMesh.listElements(bndryType)[numBndryPtPerElt*iel + iPt] + 1;
          listFluidBndElt2d[iel + 1][numBndryPtPerElt] = 0;
        }

        //- list of element of the structure
        listStrucElt2d = new int3d[m_numEltItf + 1];
        m_listStrucRef = new int1d[m_numEltItf + 1];
        felInt startIdx, nbrElm;

         //- List of element of the structure
        for (auto it = strucMesh.intRefToBegEndMaps[strucType].begin(); it != strucMesh.intRefToBegEndMaps[strucType].end(); ++it){
          strucMesh.getElementsIDPerRef(strucType, it->first, &startIdx, &nbrElm);

          for(felInt iel=startIdx; iel < startIdx + nbrElm; ++iel) {
            for(felInt iPt=0; iPt<numStrucPtPerElt; ++iPt)
              listStrucElt2d[iel + 1][iPt] = strucMesh.listElements(strucType)[numStrucPtPerElt*iel + iPt] + 1;
            listStrucElt2d[iel + 1][numStrucPtPerElt] = 0;
            m_listStrucRef[iel + 1] = ref2XFMlabel.find(it->first)->second;
          }
        }

        //- Initialisation of the library (allocation)
        if( MpiInfo::rankProc() == 0 )   
          Xfm_LibraryInitialization_Array(m_xfm, m_numVerMsh, listFluidVertCoor, 0, nullptr, 0, m_numEltMsh, listFluidElt, numIntBndMsh, m_numBndMsh, listFluidBndElt2d,
                                                 m_numVerItf, m_listStrucVertCoor, m_numEltItf, nullptr, listStrucElt2d, m_listStrucRef);
      } else {

        //- List of bundary element of the fluid
        listFluidBndElt3d = new int4d[m_numBndMsh + 1];
        for(felInt iel=0; iel<m_numBndMsh; ++iel) {
          for(felInt iPt=0; iPt<numBndryPtPerElt; ++iPt)
            listFluidBndElt3d[iel + 1][iPt] = fluidMesh.listElements(bndryType)[numBndryPtPerElt*iel + iPt] + 1;
          listFluidBndElt3d[iel + 1][numBndryPtPerElt] = 0;
        }

        //- List of element of the structure
        listStrucElt3d = new int4d[m_numEltItf + 1];
        m_listStrucRef = new int1d[m_numEltItf + 1];
        felInt startIdx, nbrElm;

        //- list of element of the structure
        for (auto it = strucMesh.intRefToBegEndMaps[strucType].begin(); it != strucMesh.intRefToBegEndMaps[strucType].end(); ++it){
          strucMesh.getElementsIDPerRef(strucType, it->first, &startIdx, &nbrElm);

          for(felInt iel=startIdx; iel < startIdx + nbrElm; ++iel) {
            for(felInt iPt=0; iPt<numStrucPtPerElt; ++iPt)
              listStrucElt3d[iel + 1][iPt] = strucMesh.listElements(strucType)[numStrucPtPerElt*iel + iPt] + 1;
            listStrucElt3d[iel + 1][numStrucPtPerElt] = 0;
            m_listStrucRef[iel + 1] = ref2XFMlabel.find(it->first)->second;
          }
        }

        //- Initialisation of the library (allocation)
        if( MpiInfo::rankProc() == 0)
          Xfm_LibraryInitialization_Array(m_xfm, m_numVerMsh, listFluidVertCoor, m_numEltMsh, listFluidElt, numIntBndMsh, m_numBndMsh, listFluidBndElt3d, 0, 0, nullptr,
                                                 m_numVerItf, m_listStrucVertCoor, m_numEltItf, listStrucElt3d, nullptr, m_listStrucRef);
      }


      //--- Clean memory
      if( listFluidVertCoor != nullptr ) delete [] listFluidVertCoor;
      if( listFluidElt      != nullptr ) delete [] listFluidElt;
      if( listFluidBndElt2d != nullptr ) delete [] listFluidBndElt2d;
      if( listStrucElt2d    != nullptr ) delete [] listStrucElt2d;
      if( listFluidBndElt3d != nullptr ) delete [] listFluidBndElt3d;
      if( listStrucElt3d    != nullptr ) delete [] listStrucElt3d;
    }
    else {

      m_listStrucRef = new int1d[m_numEltItf + 1];
      felInt startIdx, nbrElm;

      if ( m_dim == 2 ) {
         //- List of element of the structure
        for (auto it = strucMesh.intRefToBegEndMaps[strucType].begin(); it != strucMesh.intRefToBegEndMaps[strucType].end(); ++it){
          strucMesh.getElementsIDPerRef(strucType, it->first, &startIdx, &nbrElm);

          for(felInt iel=startIdx; iel < startIdx + nbrElm; ++iel)
            m_listStrucRef[iel + 1] = ref2XFMlabel.find(it->first)->second;
        }
      }
      else {
        //- List of element of the structure
        for (auto it = strucMesh.intRefToBegEndMaps[strucType].begin(); it != strucMesh.intRefToBegEndMaps[strucType].end(); ++it){
          strucMesh.getElementsIDPerRef(strucType, it->first, &startIdx, &nbrElm);

          for(felInt iel=startIdx; iel < startIdx + nbrElm; ++iel)
            m_listStrucRef[iel + 1] = ref2XFMlabel.find(it->first)->second;
        }
      }

      m_ptrIntEltMsh = (int1d*)malloc(sizeof(int1d)*(m_numEltMsh+2));
      m_ptrItfEltMsh = (int1d*)malloc(sizeof(int1d)*(m_numEltMsh+2));
      m_ptrIntEltItf = (int1d*)malloc(sizeof(int1d)*(m_numEltItf+2));
      m_ptrIntBndMsh = (int1d*)malloc(sizeof(int1d)*(m_numBndMsh+2));
      // m_verSgn       = (int1d*)malloc(sizeof(int1d)*(m_numVerMsh+1));
      m_mshItfIdx    = (int1d*)malloc(sizeof(int1d)*(m_numEltMsh+1));
      m_bndItfIdx    = (int1d*)malloc(sizeof(int1d)*(m_numBndMsh+1));;
    }


    //--- Set option writeXFMmeshes 
    m_writeXFEMmeshes = FelisceParam::instance().writeXFEMmeshes;

    //- Set flag to true
    m_isXFMInit = true;

  #else

    IGNORE_UNUSED_ARGUMENT(fluidMesh);
    IGNORE_UNUSED_ARGUMENT(strucMesh);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::computeIntersection()
{
  #ifdef FELISCE_WITH_LIBXFM 

    //- Check if class already initialized
    FEL_ASSERT(m_isXFMInit);


    //--- Compute intersection
    intersectMeshes();


    //--- Get the data about the intersection
    //- Data of the mesh
    returnAndBcastVolumeMeshData();

    //- Data of the interface mesh
    returnAndBcastInterfaceMeshData();

    //- Data of the intersected mesh
    returnAndBcastIntersectedVolumeMeshData();

    //- Data of the intersected interface
    returnAndBcastIntersectedInterfaceMeshData();

    //- Data of the intersected boundary mesh
    returnAndBcastIntersectedBoundaryMeshData();

    //- Data of the elements containing a tip
    if( m_IntMethod == NonConfCutByItfMesh )
      returnAndBcastIntersectedTipMeshData();
    else
      returnAndBcastIntersectedFrontMeshData();

    //- Data for multiple interfaces
    returnAndBcastMultipleInterfacesData();


    //--- Write intersected meshes
    if( m_writeXFEMmeshes )
      writeIntersectedMeshes();

  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::updatePosition(std::vector<double>& dispArray)
{
  #ifdef FELISCE_WITH_LIBXFM 

    //- Check if class already initialized
    FEL_ASSERT(m_isXFMInit);

    //- Update the coordinates of the interface
    if ( MpiInfo::rankProc() == 0 ) {

      for(felInt i = 0; i < m_numVerItf; ++i) {
        for(felInt j = 0; j < m_dim; ++j) {
          m_listStrucVertCoor[i+1][j] = m_listStrucVertCoorInitial[i+1][j] + dispArray[j+m_dim*i];
        }
      }

      Xfm_LibraryUpdateInterfaceCoordinates(m_xfm, m_numVerItf, m_listStrucVertCoor);
    }

    //- Write interface mesh
    if( m_writeXFEMmeshes )
      writeInterfaceMesh();

  #else

    IGNORE_UNUSED_ARGUMENT(dispArray);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::markElementsForDuplication(  GeometricMeshRegion& mesh,
                                                std::map<felInt, felInt>& intersectedMshEltIdx,
                                                std::map<felInt, std::vector<felInt> >& intersectedEltVerSgn,
                                                std::map<felInt, std::map<felInt, felInt> >& idVerMsh
                                              )
{
  #ifdef FELISCE_WITH_LIBXFM 

    //- Check if class already initialized
    FEL_ASSERT(m_isXFMInit);

    felInt iSubVer, idVer, idPnt, iSubElt;

    const GeometricMeshRegion::ElementType fluidType = mesh.bagElementTypeDomain()[0];
    felInt numFluidPtPerElt = GeometricMeshRegion::m_numVerticesPerElt[fluidType];

    // loop over the fluid elements
    for(felInt iMshElt = 1; iMshElt <= m_numEltMsh; ++iMshElt) {

      if ( m_ptrIntEltMsh[iMshElt+1] > m_ptrIntEltMsh[iMshElt] ) {

        intersectedMshEltIdx[iMshElt-1] = static_cast<felInt>(m_mshItfIdx[iMshElt]); // for multiple itf in same element intersectedMshEltIdx should be a map<felInt, set<felInt>>
        intersectedEltVerSgn[iMshElt-1] = std::vector<felInt>(numFluidPtPerElt,0);

        for(felInt isubelt = m_ptrIntEltMsh[iMshElt]; isubelt < m_ptrIntEltMsh[iMshElt+1]; ++isubelt) {
          
          // Get the sub element id
          iSubElt = m_lstIntEltMsh[isubelt];

          for(felInt iPt = 0; iPt < numFluidPtPerElt; ++iPt) {
            // Get sub elt vertex id
            iSubVer = m_eltMsh[iSubElt][iPt];

            // Get corresponding id in mesh
            idVer = m_intVerIdx[iSubVer];

            if ( idVer != 0 ) {
              for(felInt jPt = 0; jPt < numFluidPtPerElt; ++jPt) {

                idPnt = mesh.listElements(fluidType)[numFluidPtPerElt*(iMshElt-1)+jPt];
                if ( idPnt+1 == std::abs(idVer) ) {
  
                  if ( idVer > 0 )
                    intersectedEltVerSgn[iMshElt-1][jPt] = static_cast<felInt>(m_eltSgn[iSubElt]);
                  else
                    intersectedEltVerSgn[iMshElt-1][jPt] = 0;

                  break;
                }
              }
            }
          }
        }

        for(felInt iPt = 0; iPt < numFluidPtPerElt; ++iPt){
          idPnt = mesh.listElements(fluidType)[numFluidPtPerElt*(iMshElt-1)+iPt];
          idVerMsh[idPnt][static_cast<felInt>(m_mshItfIdx[iMshElt])] = -1;
        }
      }
    }

  #else

    IGNORE_UNUSED_ARGUMENT(mesh);
    IGNORE_UNUSED_ARGUMENT(intersectedMshEltIdx);
    IGNORE_UNUSED_ARGUMENT(intersectedEltVerSgn);
    IGNORE_UNUSED_ARGUMENT(idVerMsh);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::markBoundaryForDuplication(  GeometricMeshRegion& mesh,
                                                std::map<felInt, felInt>& intersectedMshEltIdx,
                                                std::map<felInt, std::vector<felInt> >& intersectedEltVerSgn,
                                                std::map<felInt, std::vector< std::vector<Point> > >& subBndEltCrd,
                                                std::map<felInt, std::vector<sideOfInterface> >& subBndEltSide
                                              )
{
  #ifdef FELISCE_WITH_LIBXFM 

    //- Check if class already initialized
    FEL_ASSERT(m_isXFMInit);

    felInt idSubVer, idVer, idPnt, idSubBnd, idSubElt;
    felInt cptIdBndElt = m_numEltMsh;

    const GeometricMeshRegion::ElementType bndryType = mesh.bagElementTypeDomainBoundary()[0];
    const felInt numBndryPtPerElt = GeometricMeshRegion::m_numVerticesPerElt[bndryType];

    std::vector<Point> tmp(numBndryPtPerElt);

    for(felInt iBndElt = 1; iBndElt <= m_numBndMsh; ++iBndElt) {

      // If the boundary face belongs to the intersected mesh
      if ( m_ptrIntBndMsh[iBndElt+1] > m_ptrIntBndMsh[iBndElt] ){

        // Mark the boundary element for duplication
        intersectedMshEltIdx[cptIdBndElt] = static_cast<felInt>(m_bndItfIdx[iBndElt]); // for multiple itf in same element intersectedMshEltIdx should be a map<felInt, set<felInt>>
        intersectedEltVerSgn[cptIdBndElt] = std::vector<felInt>(numBndryPtPerElt,0);

        // For every boundary sub-face of the selected face store coordinates and sign
        for(felInt isubfac = m_ptrIntBndMsh[iBndElt]; isubfac < m_ptrIntBndMsh[iBndElt+1]; ++isubfac){

          // Get the sub face id
          idSubBnd = m_lstIntBndMsh[isubfac];

          // Get the id of the element close to the boundary element
          if( m_dim == 2 )
            idSubElt = m_elt2DBnd[idSubBnd][2];
          else
            idSubElt = m_elt3DBnd[idSubBnd][3];

          // Store coordinates
          for (int iPt = 0; iPt < numBndryPtPerElt; ++iPt) {
            // Get sub elt vertex id
            if( m_dim == 2 )
              idSubVer = m_elt2DBnd[idSubBnd][iPt];
            else
              idSubVer = m_elt3DBnd[idSubBnd][iPt];

            // Get corresponding id in mesh
            idVer = m_intVerIdx[idSubVer];

            // Set local signs
            if ( idVer != 0 ) {
              for(felInt jPt = 0; jPt < numBndryPtPerElt; ++jPt) {

                idPnt = mesh.listElements(bndryType)[numBndryPtPerElt*(iBndElt-1)+jPt];
                if ( idPnt+1 == std::abs(idVer) ) {

                  if ( idVer > 0 )
                    intersectedEltVerSgn[cptIdBndElt][jPt] = static_cast<felInt>(m_eltSgn[idSubElt]);
                  else
                    intersectedEltVerSgn[cptIdBndElt][jPt] = 0;

                  break;
                }
              }
            }

            // get point coordinates
            tmp[iPt] = Point(m_crdMsh[idSubVer][0], m_crdMsh[idSubVer][1], m_crdMsh[idSubVer][2]);
          }

          // store coordinates
          subBndEltCrd[cptIdBndElt].push_back(tmp);

          // store sign
          if(m_eltSgn[idSubElt] > 0)
            subBndEltSide[cptIdBndElt].push_back(LEFT);
          else if (m_eltSgn[idSubElt] < 0)
            subBndEltSide[cptIdBndElt].push_back(RIGHT);
          else
            FEL_ERROR("Error while giving the sign to the boundary element. Check markBoundaryForDuplication3D");
        }
      }

      ++cptIdBndElt;
    }

  #else

    IGNORE_UNUSED_ARGUMENT(mesh);
    IGNORE_UNUSED_ARGUMENT(intersectedMshEltIdx);
    IGNORE_UNUSED_ARGUMENT(intersectedEltVerSgn);
    IGNORE_UNUSED_ARGUMENT(subBndEltCrd);
    IGNORE_UNUSED_ARGUMENT(subBndEltSide);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::postProcessTipElements(  std::set<felInt>& intersectedTipEltIdx,
                                            std::map<felInt, std::map<felInt, felInt> >& idVerMsh
                                         )
{
  #ifdef FELISCE_WITH_LIBXFM 

    //- Check if class already initialized
    FEL_ASSERT(m_isXFMInit);

    // tip
    if( m_IntMethod == NonConfCutByItfMesh ) {

      // loop over the fluid tip elements
      for(felInt iElt = 1; iElt <= m_nbrIntTipElt; ++iElt)
        intersectedTipEltIdx.insert(m_tipElt2MshElt[iElt] - 1);


      // std::vector<felInt> elemIdPoint(numFluidPtPerElt);
      // GeometricMeshRegion::BallMap ball;
      // std::map<felInt, felInt> map;
      
      // // for every tip element fill map: idx point ---> element (full mesh felisce format)
      // for (auto elt_idx : intersectedTipEltIdx){
      //   fluidMesh.getOneElement(elt_idx, elemIdPoint);

      //   for (auto pnt_idx : elemIdPoint)
      //     map[pnt_idx] = elt_idx;
      // }

      // // for every point in the map we get the ball of elements
      // GeometricMeshRegion::ElementType elt_typ;
      // felInt elt_idx, ver_pos, ver_sign, pre_ver_sign;
      // std::vector<felInt>::iterator elt_int_idx;
      // for (auto r_map : map) {

      //   // if (MpiInfo::rankProc() == 0 ) std::cout << "Point = " << r_map.first << std::endl;

      //   fluidMesh.GetVertexBall(r_map.first, fluidType, r_map.second, ball);

      //   // extract info
      //   std::tie(elt_typ, elt_idx, ver_pos) = ball[0]; // id element full mesh felisce format

      //   // check if is intersected
      //   elt_int_idx = std::find(m_idIntersectedElement.begin(), m_idIntersectedElement.end(), elt_idx);

      //   // get sign
      //   pre_ver_sign = m_intersectedEltVerSgn[ std::distance(m_idIntersectedElement.begin(), elt_int_idx) ][ver_pos];
        
      //   // what's happen for degenerated verteces???
      //   if ( m_intVerIdx[std::abs(pre_ver_sign)] < 0 ) {
      //     if (MpiInfo::rankProc() == 0 ) std::cout << "WARNING: degenerated vertex " << m_intVerIdx[std::abs(pre_ver_sign)] << std::endl;
      //     continue;
      //   }

      //   // we check if the sign is the same for every vertex
      //   for (auto r_elt = ball.begin()+1; r_elt != ball.end(); ++r_elt) {

      //     // extract info
      //     std::tie(elt_typ, elt_idx, ver_pos) = *r_elt; // id element full mesh felisce format

      //     // check if is intersected
      //     elt_int_idx = std::find(m_idIntersectedElement.begin(), m_idIntersectedElement.end(), elt_idx);
      //     if ( elt_int_idx != m_idIntersectedElement.end() ){

      //       // get sign
      //       ver_sign = m_intersectedEltVerSgn[ std::distance(m_idIntersectedElement.begin(), elt_int_idx) ][ver_pos];

      //       // if (MpiInfo::rankProc() == 0 ) std::cout << "ver_sign = " << ver_sign << " and pre_ver_sign = " << pre_ver_sign << std::endl;

      //       // sign are different
      //       if ( (ver_sign ^ pre_ver_sign) < 0 ){

      //         idVerMsh.erase(r_map.first);
              

      //         // idVerMshMulItf[r_map.first].erase(m_idItfIntersectingElt[std::distance(m_idIntersectedElement.begin(), elt_int_idx)]);
      //         // if ( idVerMshMulItf[r_map.first].empty() )
      //         //   idVerMshMulItf.erase(r_map.first);


      //         // if (MpiInfo::rankProc() == 0 ) std::cout << "Point = " << r_map.first << " erased from idVerMsh" << std::endl;


      //         break;
      //       }
      //     }
      //   }
      // }      
    } else {

      // Change verFro to get the global indices instead of the intersected ones.
      // Now do in two stages because of [i][2] not necessarily global conversion if in same element
      for(felInt i = 1; i <= m_nbrIntVerFro; ++i)
        m_verFro[i][1] = m_elt2MshEltMsh[m_verFro[i][1]];

      for(felInt i = 1; i <= m_nbrIntVerFro; ++i) {

        // again check to see if two fronts in same element; if so, set the corresponding flag
        int front_flag = 0;
        for(felInt j = 1; j <= m_nbrIntVerFro; ++j)
          if( j!=i && m_verFro[i][1] == m_verFro[j][1] )
            front_flag = 1;

        // and again, the special case for the crack when the initial crack point is a duplicated point (one technical front initially)
        if( FelisceParam::instance().hasCrack && m_nbrIntVerFro == 1 )
          front_flag=1;

        //now change verfro to global
        if( !front_flag )
          m_verFro[i][2] = std::abs(m_intVerIdx[m_verFro[i][2]]);
      }

      // All vertices of the elements intersected by the solid are in idVerMsh
      // If an element has a front point, we remove the corresponding vertex
      for(felInt iVer = 1; iVer <= m_nbrIntVerFro; ++iVer)
        idVerMsh.erase(m_verFro[iVer][2] - 1);
    }

  #else

    IGNORE_UNUSED_ARGUMENT(intersectedTipEltIdx);
    IGNORE_UNUSED_ARGUMENT(idVerMsh);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::prepareNextIntersection() const
{
  #ifdef FELISCE_WITH_LIBXFM 

    //- Check if class already initialized
    FEL_ASSERT(m_isXFMInit);
  
    //--- Reinitialize library meshes and structures
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReinitializeMeshesAndDataStructures(m_xfm);

  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

const std::vector<short unsigned int>& XFMInterface::getMapFaceFelisceToIntersector(const felInt dim) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    if ( dim == 2 )
      return mapEdg;

    return mapFac;

  #else

    IGNORE_UNUSED_ARGUMENT(dim);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return mapFac;

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getPtrSubMshEltPerMshElt(std::vector<felInt>& ptrSubMshEltPerMshElt) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    ptrSubMshEltPerMshElt.resize(m_numEltMsh+1);
    for (felInt iel = 0; iel <= m_numEltMsh; ++iel)
      ptrSubMshEltPerMshElt[iel] = static_cast<felInt>(m_ptrIntEltMsh[iel+1]);

  #else

    IGNORE_UNUSED_ARGUMENT(ptrSubMshEltPerMshElt);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getLstSubMshPerEltMsh(std::vector<felInt>& lstSubMshEltPerMshElt) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    lstSubMshEltPerMshElt.resize(m_sizLstIntEltMsh);
    for (felInt iel = 0; iel < m_sizLstIntEltMsh; ++iel)
      lstSubMshEltPerMshElt[iel] = static_cast<felInt>(m_lstIntEltMsh[iel]) - 1;

  #else

    IGNORE_UNUSED_ARGUMENT(lstSubMshEltPerMshElt);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getLstIntMshVerCrd(std::vector<std::array<double,3>>& lstIntMshVerCrd) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    lstIntMshVerCrd.resize(m_nbrIntVerMsh, std::array<double,3>{});
    for (felInt ive = 0; ive < m_nbrIntVerMsh; ++ive)
      for (std::size_t i = 0; i < 3; ++i)
        lstIntMshVerCrd[ive][i] = static_cast<double>(m_crdMsh[ive+1][i]);

  #else

    IGNORE_UNUSED_ARGUMENT(lstIntMshVerCrd);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getLstIntMshElt(std::vector<std::array<felInt,4>>& lstIntMshElt) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    lstIntMshElt.resize(m_nbrIntEltMsh, std::array<felInt,4>{});
    for (felInt iel = 0; iel < m_nbrIntEltMsh; ++iel)
      for (std::size_t i = 0; i < 4; ++i)
        lstIntMshElt[iel][i] = static_cast<felInt>(m_eltMsh[iel+1][i]) -1;

  #else

    IGNORE_UNUSED_ARGUMENT(lstIntMshElt);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getLstIntMshEltSgn(std::vector<felInt>& lstIntMshEltSgn) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    lstIntMshEltSgn.resize(m_nbrIntEltMsh);
    for (felInt iel = 0; iel < m_nbrIntEltMsh; ++iel)
      lstIntMshEltSgn[iel] = static_cast<felInt>(m_eltSgn[iel+1]);

  #else

    IGNORE_UNUSED_ARGUMENT(lstIntMshEltSgn);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getNumSubMshEltPerMshElt(const felInt iel) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_ptrIntEltMsh[iel+2] - m_ptrIntEltMsh[iel+1]);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSubMshEltIdxMsh(const felInt iel, const felInt ielSub) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_lstIntEltMsh[m_ptrIntEltMsh[iel+1] + ielSub] - 1);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(ielSub);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getSubMshEltVerCrd(const felInt ielSub, const felInt iPt, std::vector<double>& coor) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    for(std::size_t i = 0; i < 3; ++i)
      coor[i] = static_cast<double>( m_crdMsh[ m_eltMsh[ielSub+1][iPt]][i] );

  #else

    IGNORE_UNUSED_ARGUMENT(ielSub);
    IGNORE_UNUSED_ARGUMENT(iPt);
    IGNORE_UNUSED_ARGUMENT(coor);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSubMshEltSide(const felInt iel, const felInt ielSub) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_eltSgn[getSubMshEltIdxMsh(iel, ielSub) + 1]);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(ielSub);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getNumSubItfEltPerMshElt(const felInt iel) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_ptrItfEltMsh[iel+2] - m_ptrItfEltMsh[iel+1]);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSubItfEltIdxMsh(const felInt iel, const felInt ielSub) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_lstItfEltMsh[m_ptrItfEltMsh[iel+1] + ielSub] - 1);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(ielSub);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getSubItfEltVerCrd(const felInt ielSub, const felInt iPt, std::vector<double>& coor) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    if( m_dim == 2 ) {

      for(std::size_t i = 0; i < 3; ++i) 
        coor[i] = static_cast<double>( m_crdItf[ m_elt2DItf[ielSub+1][iPt]][i] );
    } else {

      for(std::size_t i = 0; i < 3; ++i)
        coor[i] = static_cast<double>( m_crdItf[ m_elt3DItf[ielSub+1][iPt]][i] );
    }

  #else

    IGNORE_UNUSED_ARGUMENT(ielSub);
    IGNORE_UNUSED_ARGUMENT(iPt);
    IGNORE_UNUSED_ARGUMENT(coor);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getNumSubItfEltPerItfElt(const felInt iel) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_ptrIntEltItf[iel+2] - m_ptrIntEltItf[iel+1]);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSubItfEltIdxItf(const felInt iel, const felInt ielSub) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_lstIntEltItf[m_ptrIntEltItf[iel+1] + ielSub] - 1);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(ielSub);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getMshEltIdxOfSubItfElt(const felInt ielSub) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_elt2MshEltItf[ielSub+1] - 1);

  #else

    IGNORE_UNUSED_ARGUMENT(ielSub);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getItfEltIdxOfSubItfElt(const felInt ielSub) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_elt2ItfElt[ielSub+1] - 1);

  #else

    IGNORE_UNUSED_ARGUMENT(ielSub);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getNumTipElt() const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_nbrIntTipElt);

  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getTipEltIdx(const felInt ielTip) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_tipElt2MshElt[ielTip+1]-1);

  #else

    IGNORE_UNUSED_ARGUMENT(ielTip);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getItfEltRef(const felInt iel) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_listStrucRef[iel+1]);

  #else

    IGNORE_UNUSED_ARGUMENT(iel);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getNumSubFacPerFacTipElt(const felInt ielTip, const felInt ifa) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    if ( ifa == m_dim )
      return static_cast<felInt>(m_ptrTipSrf[ielTip+2][0] - m_ptrTipSrf[ielTip+1][ifa]);

    return static_cast<felInt>(m_ptrTipSrf[ielTip+1][ifa+1] - m_ptrTipSrf[ielTip+1][ifa]);

  #else

    IGNORE_UNUSED_ARGUMENT(ielTip);
    IGNORE_UNUSED_ARGUMENT(ifa);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif     
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSubTipFacIdxMsh(const felInt ielTip, const felInt ifa, felInt isubFac) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_lstTipSrf[m_ptrTipSrf[ielTip+1][ifa]+isubFac] - 1);

  #else

    IGNORE_UNUSED_ARGUMENT(ielTip);
    IGNORE_UNUSED_ARGUMENT(ifa);
    IGNORE_UNUSED_ARGUMENT(isubFac);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getSubTipFacVerCrd(const felInt iSubFac, const felInt iPt, std::vector<double>& coor) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    if ( m_dim == 2 ) {
      for(std::size_t i = 0; i < 3; ++i)
        coor[i] = static_cast<double>(m_crdMsh[m_tipSrf2D[iSubFac+1][iPt]][i]);
    }
    else {
      for(std::size_t i = 0; i < 3; ++i)
        coor[i] = static_cast<double>(m_crdMsh[m_tipSrf3D[iSubFac+1][iPt]][i]);
    }

  #else

    IGNORE_UNUSED_ARGUMENT(iSubFac);
    IGNORE_UNUSED_ARGUMENT(iPt);
    IGNORE_UNUSED_ARGUMENT(coor);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif    
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSgnSubFace(const felInt iSubFac) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_tipSrfSgn[iSubFac+1][0]);
  
  #else

    IGNORE_UNUSED_ARGUMENT(iSubFac);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif    
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getSgnSubOppFace(const felInt iSubFac) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return static_cast<felInt>(m_tipSrfSgn[iSubFac+1][1]);
  
  #else

    IGNORE_UNUSED_ARGUMENT(iSubFac);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif   
}

/***********************************************************************************/
/***********************************************************************************/

felInt XFMInterface::getNumFrontPoints() const
{
  #ifdef FELISCE_WITH_LIBXFM 

    return m_nbrIntVerFro;
  
  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return 0;

  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::getFrontPointInfo(const felInt iVer, std::vector<felInt>& verFro) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    verFro.resize(3);
    verFro[0] = m_verFro[iVer+1][0] - 1;
    verFro[1] = m_verFro[iVer+1][1] - 1;
    verFro[2] = m_verFro[iVer+1][2] - 1;
  
  #else

    IGNORE_UNUSED_ARGUMENT(iVer);
    IGNORE_UNUSED_ARGUMENT(verFro);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

bool XFMInterface::checkFrontPoint(const felInt iVer) const
{
  #ifdef FELISCE_WITH_LIBXFM 

    for(felInt i = 1; i <= m_nbrIntVerFro; ++i)
      if( m_verFro[i][0] == iVer+1 )
        return true;

    return false;
  
  #else

    IGNORE_UNUSED_ARGUMENT(iVer);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

    return false;

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::setIteration(const felInt iteration) 
{
  #ifdef FELISCE_WITH_LIBXFM 

    m_iteration = iteration;
  
  #else

    IGNORE_UNUSED_ARGUMENT(iteration);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif 
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::setMeshesName(const std::string fluidMesh, const std::string strucMesh) 
{
  #ifdef FELISCE_WITH_LIBXFM
    
    m_fluidMeshName = fluidMesh;
    m_strucMeshName = strucMesh;

  #else

    IGNORE_UNUSED_ARGUMENT(fluidMesh);
    IGNORE_UNUSED_ARGUMENT(strucMesh);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
    
  #endif  
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::buildFictitiousToStructureMap(const GeometricMeshRegion& strucMesh, std::unordered_map<int,int>& ref2XFMlabel) const
{
  #ifdef FELISCE_WITH_LIBXFM

    const std::vector<int>& r_structures = FelisceParam::instance().structures;
    const std::vector<int>& r_numficxstr = FelisceParam::instance().numficxstr;
    const std::vector<int>& r_fictitious = FelisceParam::instance().fictitious;

    // loop over felisce label
    int startIndex, pos;
    int xfmLabel = 1;
    GeometricMeshRegion::ElementType strucType = strucMesh.bagElementTypeDomain()[0];
    for (auto it = strucMesh.intRefToBegEndMaps[strucType].begin(); it != strucMesh.intRefToBegEndMaps[strucType].end(); ++it){

      auto it_fic = std::find(r_fictitious.begin(), r_fictitious.end(), it->first); 
      if ( it_fic == r_fictitious.end() ) {

        ref2XFMlabel.insert( {it->first, xfmLabel} );

        auto it_r2f = std::find(r_structures.begin(), r_structures.end(), it->first); 
        if ( it_r2f != r_structures.end() ) {

          pos = std::distance(r_structures.begin(), it_r2f);

          startIndex = std::accumulate(r_numficxstr.begin(), r_numficxstr.begin()+pos, 0);
          for (auto ific=startIndex; ific<startIndex+r_numficxstr[pos]; ++ific)
            ref2XFMlabel.insert( {r_fictitious[ific], -xfmLabel} );
        }

        xfmLabel++;
      }
    }

  #else

    IGNORE_UNUSED_ARGUMENT(strucMesh);
    IGNORE_UNUSED_ARGUMENT(ref2XFMlabel);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
    
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::intersectMeshes()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Intersect meshes
    if ( MpiInfo::rankProc() == 0 )      
      Xfm_LibraryIntersection(m_xfm, 0);

  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastVolumeMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the mesh
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnMeshData(m_xfm, &m_sizLstIntEltMsh, &m_ptrIntEltMsh, &m_lstIntEltMsh, &m_sizLstItfEltMsh, &m_ptrItfEltMsh, &m_lstItfEltMsh, &m_verSgn);

    MPI_Bcast(&m_sizLstIntEltMsh, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_sizLstItfEltMsh, 1, MPI_INT, 0, MpiInfo::petscComm());
    if( MpiInfo::rankProc() != 0 ){
      m_lstIntEltMsh = (int1d*)realloc(m_lstIntEltMsh, sizeof(int1d)*m_sizLstIntEltMsh);
      m_lstItfEltMsh = (int1d*)realloc(m_lstItfEltMsh, sizeof(int1d)*m_sizLstItfEltMsh);
    }
    MPI_Bcast(m_ptrIntEltMsh, m_numEltMsh+2,     MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_lstIntEltMsh, m_sizLstIntEltMsh, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_ptrItfEltMsh, m_numEltMsh+2,     MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_lstItfEltMsh, m_sizLstItfEltMsh, MPI_INT, 0, MpiInfo::petscComm());
    // MPI_Bcast(m_verSgn,       m_numVerMsh+1,     MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastInterfaceMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the interface mesh
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnInterfaceMeshData(m_xfm, &m_sizLstIntEltItf, &m_ptrIntEltItf, &m_lstIntEltItf);
    
    MPI_Bcast(&m_sizLstIntEltItf, 1, MPI_INT, 0, MpiInfo::petscComm());
    if( MpiInfo::rankProc() != 0 ){
      m_lstIntEltItf = (int1d*)realloc(m_lstIntEltItf, sizeof(int1d)*m_sizLstIntEltItf);
    }
    MPI_Bcast(m_ptrIntEltItf, m_numEltItf+2,     MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_lstIntEltItf, m_sizLstIntEltItf, MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastIntersectedVolumeMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the intersected mesh
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnIntersectMeshData(m_xfm, &m_nbrIntVerMsh, &m_crdMsh, &m_intVerIdx, &m_intVerSgn, &m_nbrIntEltMsh, &m_eltMsh, &m_elt2MshEltMsh, &m_eltSgn);

    MPI_Bcast(&m_nbrIntVerMsh, 1, MPI_INT, 0, MpiInfo::petscComm());  // control mpi-type Long Int
    MPI_Bcast(&m_nbrIntEltMsh, 1, MPI_INT, 0, MpiInfo::petscComm());
    if( MpiInfo::rankProc() != 0 ){
      m_crdMsh        = (double3d*)realloc(m_crdMsh,    sizeof(double3d)*(m_nbrIntVerMsh+1));
      m_intVerIdx     = (int1d*   )realloc(m_intVerIdx, sizeof(int1d   )*(m_nbrIntVerMsh+1));
      m_intVerSgn     = (int1d*   )realloc(m_intVerSgn, sizeof(int1d   )*(m_nbrIntVerMsh+1));

      m_eltMsh        = (int4d*)realloc(m_eltMsh,        sizeof(int4d)*(m_nbrIntEltMsh+1));
      m_elt2MshEltMsh = (int1d*)realloc(m_elt2MshEltMsh, sizeof(int1d)*(m_nbrIntEltMsh+1));
      m_eltSgn        = (int1d*)realloc(m_eltSgn,        sizeof(int1d)*(m_nbrIntEltMsh+1));
    }
    MPI_Bcast(m_crdMsh,     3*(m_nbrIntVerMsh+1), MPI_DOUBLE, 0, MpiInfo::petscComm());
    MPI_Bcast(m_intVerIdx,     m_nbrIntVerMsh+1 , MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_intVerSgn,     m_nbrIntVerMsh+1 , MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_eltMsh,     4*(m_nbrIntEltMsh+1), MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_elt2MshEltMsh, m_nbrIntEltMsh+1 , MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_eltSgn,        m_nbrIntEltMsh+1 , MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastIntersectedInterfaceMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the intersected interface
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnIntersectInterfaceMeshData(m_xfm, &m_nbrIntVerItf, &m_crdItf, &m_nbrIntEltItf, &m_elt2DItf, &m_elt3DItf, &m_elt2MshEltItf, &m_elt2ItfElt);

    MPI_Bcast(&m_nbrIntVerItf, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_nbrIntEltItf, 1, MPI_INT, 0, MpiInfo::petscComm());
    if( MpiInfo::rankProc() != 0 ){
      m_crdItf = (double3d*)realloc(m_crdItf, sizeof(double3d)*3*(m_nbrIntVerItf+1));
      if(m_dim == 2)
        m_elt2DItf = (int3d*)realloc(m_elt2DItf, sizeof(int3d)*(m_nbrIntEltItf+1));
      else
        m_elt3DItf = (int4d*)realloc(m_elt3DItf, sizeof(int4d)*(m_nbrIntEltItf+1));
      m_elt2MshEltItf = (int1d*)realloc(m_elt2MshEltItf, sizeof(int1d)*(m_nbrIntEltItf+1));
      m_elt2ItfElt    = (int1d*)realloc(m_elt2ItfElt,    sizeof(int1d)*(m_nbrIntEltItf+1));
    }
    MPI_Bcast(m_crdItf, 3*(m_nbrIntVerItf+1), MPI_DOUBLE, 0, MpiInfo::petscComm());
    if(m_dim == 2)
      MPI_Bcast(m_elt2DItf, 3*(m_nbrIntEltItf+1), MPI_INT, 0, MpiInfo::petscComm());
    else
      MPI_Bcast(m_elt3DItf, 4*(m_nbrIntEltItf+1), MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_elt2MshEltItf, m_nbrIntEltItf+1 , MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_elt2ItfElt,    m_nbrIntEltItf+1 , MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastIntersectedBoundaryMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the intersected boundary mesh
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnBndMeshData(m_xfm, &m_sizLstIntBndMsh, &m_ptrIntBndMsh, &m_lstIntBndMsh, &m_nbrIntEltBnd, &m_elt2DBnd, &m_elt3DBnd);

    MPI_Bcast(&m_sizLstIntBndMsh, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_nbrIntEltBnd,    1, MPI_INT, 0, MpiInfo::petscComm());
    if( MpiInfo::rankProc() != 0 ){
      m_lstIntBndMsh = (int1d*)realloc(m_lstIntBndMsh, sizeof(int1d)*m_sizLstIntBndMsh);
      if(m_dim == 2)
        m_elt2DBnd = (int3d*)realloc(m_elt2DBnd, sizeof(int3d)*(m_nbrIntEltBnd+1));
      else
        m_elt3DBnd = (int4d*)realloc(m_elt3DBnd, sizeof(int4d)*(m_nbrIntEltBnd+1));
    }
    MPI_Bcast(m_ptrIntBndMsh, m_numBndMsh+2, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_lstIntBndMsh, m_sizLstIntBndMsh, MPI_INT, 0, MpiInfo::petscComm());
    if(m_dim == 2)
      MPI_Bcast(m_elt2DBnd, 3*(m_nbrIntEltBnd+1), MPI_INT, 0, MpiInfo::petscComm());
    else
      MPI_Bcast(m_elt3DBnd, 4*(m_nbrIntEltBnd+1), MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastIntersectedTipMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the elements containing a tip
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnTipMeshData(m_xfm, &m_sizLstTipSrf, &m_ptrTipSrf, &m_lstTipSrf, &m_nbrIntTipElt, &m_tipElt2MshElt, &m_nbrIntTipSrf, &m_tipSrf3D , &m_tipSrf2D, &m_tipSrfSgn);

    MPI_Bcast(&m_sizLstTipSrf, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_nbrIntTipElt, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_nbrIntTipSrf, 1, MPI_INT, 0, MpiInfo::petscComm());

    if( MpiInfo::rankProc() != 0 ){
      m_ptrTipSrf = (int4d*)realloc(m_ptrTipSrf, sizeof(int4d)*(m_nbrIntTipElt+2));
      m_lstTipSrf = (int1d*)realloc(m_lstTipSrf, sizeof(int1d)*m_sizLstTipSrf);
      if(m_dim == 2)
        m_tipSrf2D = (int2d*)realloc(m_tipSrf2D, sizeof(int2d)*(m_nbrIntTipSrf+1));
      else
        m_tipSrf3D = (int4d*)realloc(m_tipSrf3D, sizeof(int4d)*(m_nbrIntTipSrf+1));
      m_tipElt2MshElt = (int1d*)realloc(m_tipElt2MshElt, sizeof(int1d)*(m_nbrIntTipElt+1));
      m_tipSrfSgn     = (int2d*)realloc(m_tipSrfSgn,     sizeof(int2d)*(m_nbrIntTipSrf+1));
    }
    MPI_Bcast(m_ptrTipSrf, 4*(m_nbrIntTipElt+2), MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_lstTipSrf,    m_sizLstTipSrf, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_tipElt2MshElt, m_nbrIntTipElt+1, MPI_INT, 0, MpiInfo::petscComm());
    if(m_dim == 2) {
      MPI_Bcast(m_tipSrf2D, 2*(m_nbrIntTipSrf+1), MPI_INT, 0, MpiInfo::petscComm());
    } else {
      MPI_Bcast(m_tipSrf3D, 4*(m_nbrIntTipSrf+1), MPI_INT, 0, MpiInfo::petscComm());
    }
    MPI_Bcast(m_tipSrfSgn,  2*(m_nbrIntTipSrf+1), MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastIntersectedFrontMeshData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data of the elements containing a front point
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnFrontPointsData(m_xfm, &m_nbrIntVerFro, &m_verFro, &m_vecFro);

    MPI_Bcast(&m_nbrIntVerFro, 1, MPI_INT, 0, MpiInfo::petscComm());

    if( MpiInfo::rankProc() != 0 ){
      m_verFro = (int3d*)realloc(m_verFro,    sizeof(int3d)*(m_nbrIntVerFro+1));
      // m_vecFro = (double6d*)realloc(m_vecFro, sizeof(double6d)*(???????????+1));
    }
    MPI_Bcast(m_verFro,     3*(m_nbrIntVerFro+1), MPI_INT, 0, MpiInfo::petscComm());
    // MPI_Bcast(&m_vecFro, 6*(....), MPI_DOUBLE, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::returnAndBcastMultipleInterfacesData()
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Data for multiple interfaces
    if ( MpiInfo::rankProc() == 0 )
      Xfm_LibraryReturnIntersectingItfIdx(m_xfm, &m_mshItfIdx, &m_bndItfIdx);

    MPI_Bcast(m_mshItfIdx, m_numEltMsh+1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(m_bndItfIdx, m_numBndMsh+1, MPI_INT, 0, MpiInfo::petscComm());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::writeInterfaceMesh() const
{

  #ifdef FELISCE_WITH_LIBXFM 

    if ( MpiInfo::rankProc() != 0 )
      return;

    std::string strucName = FelisceParam::instance().resultDir+m_strucMeshName+std::to_string(m_iteration);

    Xfm_LibraryWriteInterfaceMesh(m_xfm, strucName.data());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void XFMInterface::writeIntersectedMeshes() const
{

  #ifdef FELISCE_WITH_LIBXFM 

    if ( MpiInfo::rankProc() != 0 )
      return;

    std::string fluidName = FelisceParam::instance().resultDir+m_fluidMeshName+std::to_string(m_iteration);
    std::string strucName = FelisceParam::instance().resultDir+m_strucMeshName+std::to_string(m_iteration);
    std::string flTipName = FelisceParam::instance().resultDir+m_fluidMeshName+"Tip"+std::to_string(m_iteration);

    Xfm_LibraryWriteIntersectMesh(      0, m_xfm, fluidName.data());
    Xfm_LibraryWriteIntersectInterfaceMesh(m_xfm, strucName.data());
    if( m_IntMethod == NonConfCutByItfMesh && getNumTipElt() > 0 )
      Xfm_LibraryWriteTipElementMesh(m_xfm, flTipName.data());

  #else
    
    FEL_ERROR("You need to compile felisce with libXfm to use this function");

  #endif
}

}


