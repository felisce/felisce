//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. C. Corti
//

/*!
  \file     M1gInterface.hpp
  \authors  D. C. Corti
  \date     00/00/2022
  \brief    interface for M1G module in libXfm
*/

#ifndef M1GINTERFACE_HPP
#define M1GINTERFACE_HPP

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Geometry/geometricMeshRegion.hpp"


#ifdef FELISCE_WITH_LIBXFM
  #ifndef XFM_LIBRARY_HPP
    #define XFM_LIBRARY_HPP    
    extern "C" {
      #include "xfm_library.h"
    }
  #endif
#endif


namespace felisce {

  /*!
   * Class to manage the intersection of the two meshes and the duplication of the support elements
   */
  class M1gInterface {
    
    public:

      using M1G = UniqueFelisceParam::S_M1G;

      // Constructor
      M1gInterface();

      // Destructor
      ~M1gInterface();

      // Initialization
      void initialize(const GeometricMeshRegion& mesh);

      /***** intersection of the meshes *****/
      // Library of Frederic Alauzet (libXfm)
      void mesh123Gen();
      
      // update the position of the mesh
      void updateMeshPosition(const std::vector<double>& dispArray);

      // **** getters / setters ****
      void setMeshName(const std::string name);
      void setData(M1G* m1gStruct);
      void setIteration(const felInt iteration);

      felInt getNbrPoints() const ;
      felInt getNbrElements() const ;
      felInt getElementTag(const felInt iEl) const;

      void   getPointCrd(const felInt iPt, Point& point) const;
      void   getElementPt(const felInt iEl, std::vector<felInt>& element) const;

    private:

      #ifdef FELISCE_WITH_LIBXFM

      // Pointer to data
      M1G* m_M1gData;

      // Mesh names
      std::string m_meshName;

      // iteration
      felInt m_iteration;

      // dimension of the fluid mesh
      felInt m_dim;

      // data
      int1d m_numVerMsh;
      int1d m_numEltMsh;
      
      // Object of libXfm of Frederic Alauzet
      void *m_m1g;
      bool m_isM1GInit;
      bool m_writeM1Gmeshes;

      double3d* m_listVertices;
      double3d* m_listVerticesInitial;
      int3d*    m_listElements;

      /////////////////////////////////////////////
      // WARNING : these arrays start at index 1 //

      int1d m_nbrVerMsh;
      double3d *m_crdMsh;

      int1d m_nbrEltMsh;
      int4d *m_eltMsh;

      // END OF WARNING //
      ////////////////////
      #endif
  };
}

#endif
