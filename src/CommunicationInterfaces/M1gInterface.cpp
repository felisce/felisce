//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. C. Corti
//

/*!
  \file     M1gInterface.hpp
  \authors  D. C. Corti
  \date     00/00/2022
  \brief    interface for M1G module in libXfm
*/

// System includes

// External includes

// Project includes
#include "CommunicationInterfaces/M1gInterface.hpp"

namespace felisce {

M1gInterface::M1gInterface() 
{
  #ifdef FELISCE_WITH_LIBXFM

    m_M1gData = nullptr;

    m_iteration = 0;
    m_dim       = 3;

    m_numVerMsh = 0;
    m_numEltMsh = 0;

    m_m1g = nullptr;

    m_isM1GInit      = false;
    m_writeM1Gmeshes = false;

    m_listVertices        = nullptr;
    m_listVerticesInitial = nullptr;
    m_listElements        = nullptr;

    m_nbrVerMsh = 0;
    m_crdMsh    = nullptr;

    m_nbrEltMsh = 0;
    m_eltMsh    = nullptr;

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

M1gInterface::~M1gInterface() 
{
  #ifdef FELISCE_WITH_LIBXFM
  
    if(m_isM1GInit)
      m_m1g = M1G_FreeLibrary(m_m1g);

    if( MpiInfo::rankProc() != 0 ){

      if(m_crdMsh != nullptr)
        delete [] m_crdMsh;

      if(m_eltMsh != nullptr)
        delete [] m_eltMsh;
    }

    if(m_listVertices != nullptr)
      delete [] m_listVertices;

    if(m_listVerticesInitial != nullptr)
      delete [] m_listVerticesInitial;

    if(m_listElements != nullptr)
      delete [] m_listElements;

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::initialize(const GeometricMeshRegion& mesh)
{
  #ifdef FELISCE_WITH_LIBXFM

    //- Only proc 0 can manage this library
    if ( MpiInfo::rankProc() != 0 )
      return;
    
    if( m_isM1GInit ) 
      FEL_ERROR("M1gInterface already initialized");
    

    //--- Initialize interface
    m_m1g = M1G_NewLibrary(m_dim, m_meshName.data(), m_M1gData->verbose, 0);
    m_isM1GInit = true;


    //--- Set scaling factor
    M1G_LibrarySetHScaleFactor(m_m1g, m_M1gData->Hscaling);


    //--- Set flag for checking orientation
    M1G_LibrarySetCheckOrientation(m_m1g, m_M1gData->CheckOrientation == true ? static_cast<int1d>(1) : static_cast<int1d>(0) );


    //--- Initialize mesh in M1G library: points and elements
    const GeometricMeshRegion::ElementType eltType = GeometricMeshRegion::Seg2;
    const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];

    m_listVertices        = new double3d[mesh.numPoints() + 1];
    m_listVerticesInitial = new double3d[mesh.numPoints() + 1];
    m_listElements        = new int3d[mesh.numElements(eltType) + 1];
    
    m_listVertices[0][0] = m_listVertices[0][1] = m_listVertices[0][2] = 0;
    m_listVerticesInitial[0][0] = m_listVerticesInitial[0][1] = m_listVerticesInitial[0][2] = 0;
    m_listElements[0][0] = m_listElements[0][1] = m_listElements[0][2] = 0;

    // Copy points
    auto& listPnt = mesh.listPoints();
    m_numVerMsh   = static_cast<int1d>(listPnt.size());
    for (std::size_t iPnt = 0; iPnt < listPnt.size(); ++iPnt) {
      for (std::size_t iCoor = 0; iCoor < 3; ++iCoor) {
        m_listVertices[iPnt+1][iCoor]        = listPnt[iPnt][iCoor];
        m_listVerticesInitial[iPnt+1][iCoor] = listPnt[iPnt][iCoor];
      }
    } 

    // Copy elements
    auto* listElt = mesh.listElements(eltType);
    m_numEltMsh   = static_cast<int1d>(mesh.numElements(eltType));
    for (felInt iElt = 0; iElt < mesh.numElements(eltType); ++iElt) {
      for (int iPnt = 0; iPnt < numPointsPerElt; ++iPnt) {
        m_listElements[iElt+1][iPnt] = static_cast<int1d>(listElt[numPointsPerElt*iElt+iPnt] + 1);
      }
      m_listElements[iElt+1][numPointsPerElt] = static_cast<int1d>(0);
    }

    M1G_LibraryInitialization_Array_Mesh(m_m1g, m_numVerMsh, m_listVertices, m_numEltMsh, m_listElements);


    //--- Set germs for triangles and for 123 algorithm
    auto& triLst = m_M1gData->triLst; 
    auto& verLst = m_M1gData->verLst; 
    int1d tmp_nbrTri = static_cast<int1d>(triLst.size()/3);
    int1d tmp_nbrVer = static_cast<int1d>(verLst.size());

    std::vector<int1d> tmp_triLst(triLst.size());
    std::transform(triLst.begin(),triLst.end(), tmp_triLst.begin(), [&](auto ive){ return static_cast<int1d>(ive); } );

    std::vector<int1d> tmp_verLst(verLst.size()); 
    std::transform(verLst.begin(),verLst.end(), tmp_verLst.begin(), [&](auto ive){ return static_cast<int1d>(ive); } );

    M1G_LibrarySetGermVertices(m_m1g, tmp_nbrTri, tmp_triLst.data(), tmp_nbrVer, tmp_verLst.data());


    //--- Set option writeM1Gmeshes 
    m_writeM1Gmeshes = m_M1gData->writeM1Gmeshes;
  
  #else
  
    IGNORE_UNUSED_ARGUMENT(mesh);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::updateMeshPosition(const std::vector<double>& dispArray) 
{
  #ifdef FELISCE_WITH_LIBXFM

    if (MpiInfo::rankProc() == 0){
      for(int1d i = 1; i <= m_numVerMsh; ++i) {
        for(std::size_t j = 0; j < 3; ++j)
          m_listVertices[i][j] = m_listVerticesInitial[i][j] + dispArray[j + 3*(i-1)];
      }

      M1G_LibraryUpdateCoordinates(m_m1g, m_numVerMsh, m_listVertices);
    }

  #else

    IGNORE_UNUSED_ARGUMENT(dispArray);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::mesh123Gen()
{
  #ifdef FELISCE_WITH_LIBXFM

    if ( MpiInfo::rankProc() == 0 ) {

      // Mesh hole
      M1G_LibraryMesh123Generator(m_m1g);

      // Get new mesh 
      M1G_LibraryReturnMeshData(m_m1g, &m_nbrVerMsh, &m_crdMsh, &m_nbrEltMsh, &m_eltMsh);

      if ( m_writeM1Gmeshes ){
        std::string name = m_M1gData->outdir+m_meshName+std::to_string(m_iteration);

        M1G_LibraryWriteMesh(m_m1g, name.data());
      }
    }

    MPI_Bcast(&m_nbrVerMsh, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_nbrEltMsh, 1, MPI_INT, 0, MpiInfo::petscComm());
    if( MpiInfo::rankProc() != 0 ) {
      m_crdMsh = (double3d*)realloc(m_crdMsh, sizeof(double3d)*(m_nbrVerMsh+1));
      m_eltMsh = (int4d*)   realloc(m_eltMsh, sizeof(int4d)   *(m_nbrEltMsh+1));
    } 
    MPI_Bcast(m_crdMsh, 3*(m_nbrVerMsh+1), MPI_DOUBLE, 0, MpiInfo::petscComm());
    MPI_Bcast(m_eltMsh, 4*(m_nbrEltMsh+1), MPI_INT,    0, MpiInfo::petscComm());
  
  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

felInt M1gInterface::getNbrPoints() const
{
  #ifdef FELISCE_WITH_LIBXFM

    return static_cast<felInt>(m_nbrVerMsh);

  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");
    return static_cast<felInt>(1);

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

felInt M1gInterface::getNbrElements() const
{
  #ifdef FELISCE_WITH_LIBXFM

    return static_cast<felInt>(m_nbrEltMsh);

  #else

    FEL_ERROR("You need to compile felisce with libXfm to use this function");
    return static_cast<felInt>(1);

  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::getPointCrd(const felInt iPt, Point& point) const
{
  #ifdef FELISCE_WITH_LIBXFM

    for(std::size_t i = 0; i < 3; ++i)
      point[i] = m_crdMsh[iPt+1][i];
  
  #else

    IGNORE_UNUSED_ARGUMENT(iPt);
    IGNORE_UNUSED_ARGUMENT(point);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::getElementPt(const felInt iEl, std::vector<felInt>& element) const
{
  #ifdef FELISCE_WITH_LIBXFM

    for(std::size_t i = 0; i < 3; ++i)
      element[i] = static_cast<felInt>(m_eltMsh[iEl+1][i]-1);
    
  #else

    IGNORE_UNUSED_ARGUMENT(iEl);
    IGNORE_UNUSED_ARGUMENT(element);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

felInt M1gInterface::getElementTag(const felInt iEl) const
{
  #ifdef FELISCE_WITH_LIBXFM

    return static_cast<felInt>(m_eltMsh[iEl+1][3]);
    
  #else

    IGNORE_UNUSED_ARGUMENT(iEl);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
    return static_cast<felInt>(1);
    
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::setMeshName(const std::string name) 
{
  #ifdef FELISCE_WITH_LIBXFM

    m_meshName = name;
    
  #else

    IGNORE_UNUSED_ARGUMENT(name);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::setData(M1G* m1gStruct)
{
  #ifdef FELISCE_WITH_LIBXFM

    m_M1gData = m1gStruct;
    
  #else

    IGNORE_UNUSED_ARGUMENT(m1gStruct);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

/***********************************************************************************/
/***********************************************************************************/

void M1gInterface::setIteration(const felInt iteration) 
{
  #ifdef FELISCE_WITH_LIBXFM

    m_iteration = iteration;
    
  #else

    IGNORE_UNUSED_ARGUMENT(iteration);
    FEL_ERROR("You need to compile felisce with libXfm to use this function");
  
  #endif
}

}


