//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. C. Corti
//

/*!
  \file     XFMInterface.hpp
  \authors  D. C. Corti
  \date     00/00/2022
  \brief    interface for XFM module in libXfm
*/

#ifndef XFMINTERFACE_HPP
#define XFMINTERFACE_HPP

// System includes
#include <vector>

// Project-specific includes
#include "Core/configure.hpp"

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"

#ifdef FELISCE_WITH_LIBXFM
  #ifndef XFM_LIBRARY_HPP
    #define XFM_LIBRARY_HPP    
    extern "C" {
      #include "xfm_library.h"
    }
  #endif
#endif


namespace felisce {

  class DuplicateSupportDof;
  /*!
   * Class to manage the intersection of the two meshes and the duplication of the support elements
   */
  class XFMInterface 
  {   

    public:

      // Constructor
      XFMInterface();

      // Destructor
      ~XFMInterface();

      // Initialization
      void initialize(const GeometricMeshRegion& fluidMesh, const GeometricMeshRegion& strucMesh);

      // Compute intersection of two meshes
      void computeIntersection();
      
      // Update interface position
      void updatePosition(std::vector<double>& dispArray);

      // Update the arrays for the duplication of the support volume elements
      void markElementsForDuplication(  GeometricMeshRegion& mesh,
                                        std::map<felInt, felInt>& intersectedMshEltIdx,
                                        std::map<felInt, std::vector<felInt> >& intersectedEltVerSgn,
                                        std::map<felInt, std::map<felInt, felInt> >& idVerMsh
                                      );

      // Update the arrays for the duplication of the support boundary elements
      void markBoundaryForDuplication(  GeometricMeshRegion& mesh,
                                        std::map<felInt, felInt>& intersectedMshEltIdx,
                                        std::map<felInt, std::vector<felInt> >& intersectedEltVerSgn,
                                        std::map<felInt, std::vector< std::vector<Point> > >& subBndEltCrd,
                                        std::map<felInt, std::vector<sideOfInterface> >& subBndEltSide
                                      );

      // Post processing for tipe elements and front point elements
      void postProcessTipElements(  std::set<felInt>& intersectedTipEltIdx,
                                    std::map<felInt, std::map<felInt, felInt> >& idVerMsh
                                 );

      // Reset library meshes and data structures for new intersection
      void prepareNextIntersection() const;

      // Get map Edg/Fac
      const std::vector<short unsigned int>& getMapFaceFelisceToIntersector(felInt dim) const;

      // Get the pointers to the list of sub-elements
      void getPtrSubMshEltPerMshElt(std::vector<felInt>& ptrSubMshEltPerMshElt) const;

      // Get the list of sub-elements
      void getLstSubMshPerEltMsh(std::vector<felInt>& lstSubMshEltPerMshElt) const;

      // Get the list of intersected mesh coordinates
      void getLstIntMshVerCrd(std::vector<std::array<double,3>>& lstIntMshVerCrd) const;

      // Get the list of fluid sub-elements
      void getLstIntMshElt(std::vector<std::array<felInt,4>>& lstIntMshElt) const;

      // Get the list of fluid sub-elements signs
      void getLstIntMshEltSgn(std::vector<felInt>& lstIntMshEltSgn) const;

      // Get the number of fluid sub-element for an element of the fluid mesh
      felInt getNumSubMshEltPerMshElt(const felInt iel) const;

      // Get the id of the ielSub-th fluid sub-element of fluid element iel
      felInt getSubMshEltIdxMsh(const felInt iel, const felInt ielSub) const;

      // Get the coordinates of the iPt-th vertex of the sub-element ielSub for the fluid
      void getSubMshEltVerCrd(const felInt ielSub, const felInt iPt, std::vector<double>& coor) const;

      // Get the side of the sub-element
      felInt getSubMshEltSide(const felInt iel, const felInt ielSub) const;

      // Get the number of solid sub-element for an element of the fluid mesh
      felInt getNumSubItfEltPerMshElt(const felInt iel) const;

      // Get the id of the ielSub-th solid sub-element of fluid element iel
      felInt getSubItfEltIdxMsh(const felInt iel, const felInt ielSub) const;

      // Get the coordinates of the iPt-th vertex of the sub-element ielSub for the solid
      void getSubItfEltVerCrd(const felInt ielSub, const felInt iPt, std::vector<double>& coor) const;

      // Get the number of solid sub-element for an element of the solid mesh
      felInt getNumSubItfEltPerItfElt(const felInt iel) const;

      // Get the id of the ielSub-th solid sub-element of solid element iel
      felInt getSubItfEltIdxItf(const felInt iel, const felInt ielSub) const;

      // Get the id of the fluid element owning the sub-solid element ielSub
      felInt getMshEltIdxOfSubItfElt(const felInt ielSub) const;

      // Get the id of the solid element owning the sub-solid element ielSub
      felInt getItfEltIdxOfSubItfElt(const felInt ielSub) const;

      // Get the number of fluid Tip element
      felInt getNumTipElt() const;
      
      // Get the Id of fluid Tip element iTipElt
      felInt getTipEltIdx(const felInt ielTip) const;
      
      // Get the reference of iterface element iel
      felInt getItfEltRef(const felInt iel) const;
      
      // Get the number of subfaces of the ifa^th face of ielTip
      felInt getNumSubFacPerFacTipElt(const felInt ielTip, const felInt ifa) const;

      // Get the id of the isubFac-th fluid sub-face of face ifa of fluid tip element ielTip
      felInt getSubTipFacIdxMsh(const felInt ielTip, const felInt ifa, const felInt isubFac) const;
      
      // Get the coordinates of the iPt point of the tip isubFac
      void getSubTipFacVerCrd(const felInt iSubFac, const felInt iPt, std::vector<double>& coor) const;
      
      // Get the side of the subFace
      felInt getSgnSubFace(const felInt iSubFac) const;
      
      // Get the side of the opposite subFace
      felInt getSgnSubOppFace(const felInt iSubFac) const;

      // Get the number of front points
      felInt getNumFrontPoints() const;

      // Get the information on the iVer-th front point
      void getFrontPointInfo(const felInt iVer, std::vector<felInt>& verFro) const;

      // Check if the vertex iVer is a front point
      bool checkFrontPoint(const felInt iVer) const;

      // Set number iteration
      void setIteration(const felInt iteration);

      // Set meshes names
      void setMeshesName(const std::string fluidMesh, const std::string strucMesh);


    private:

      // Build the map between fictitious strucures and physical structures
      void buildFictitiousToStructureMap(const GeometricMeshRegion& strucMesh, std::unordered_map<int,int>& ref2XFMlabel) const;

      // Compute intersection 
      void intersectMeshes();

      // Return from libXfm volume mesh data
      void returnAndBcastVolumeMeshData();

      // Return from libXfm interface mesh data
      void returnAndBcastInterfaceMeshData();

      // Return from libXfm intersected volume mesh data
      void returnAndBcastIntersectedVolumeMeshData();

      // Return from libXfm intersected interface mesh data
      void returnAndBcastIntersectedInterfaceMeshData();

      // Return from libXfm intersected boundary mesh data
      void returnAndBcastIntersectedBoundaryMeshData();

      // Return from libXfm volume tip mesh elements data
      void returnAndBcastIntersectedTipMeshData();

      // Return from libXfm volume front mesh elements data
      void returnAndBcastIntersectedFrontMeshData();

      // Return from libXfm multiple interfaces data
      void returnAndBcastMultipleInterfacesData();

      // write interface mesh
      void writeInterfaceMesh() const;

      // write intersected meshes
      void writeIntersectedMeshes() const;

      // map triangle edges Felisce -> Xfm
      static inline const std::vector<short unsigned int> mapEdg = {2,0,1};

      // map tetrahedron faces Felisce -> Xfm
      static inline const std::vector<short unsigned int> mapFac = {3,2,0,1};

      #ifdef FELISCE_WITH_LIBXFM
      // Object of libXfm of Frederic Alauzet
      void *m_xfm;

      // Initialisation state of Xfm library
      bool m_isXFMInit;

      // Enable write meshes 
      bool m_writeXFEMmeshes;

      // Intersection method
      Operation m_IntMethod;

      // Meshes names
      std::string m_fluidMeshName;
      std::string m_strucMeshName;

      // Dimension of the fluid mesh
      felInt m_dim;

      // Meshes data
      felInt m_numVerMsh;
      felInt m_numVerItf;
      felInt m_numEltMsh;
      felInt m_numEltItf;
      felInt m_numBndMsh;

      // Iteration number
      felInt m_iteration;

      double3d *m_listStrucVertCoor;
      double3d *m_listStrucVertCoorInitial;
      int1d    *m_listStrucRef;

      ///////////////////////////////////////////////////////////////
      // WARNING : these arrays start at index 1 except for m_lst* //
      // -------------------------------------------------------------
      // mapping from fluid mesh to intersected fluid and solid meshes
      // -------------------------------------------------------------
      int1d m_sizLstIntEltMsh;    // size of m_lstIntEltMsh
      int1d *m_ptrIntEltMsh;      // [i] = index in m_lstIntEltMsh of the first fluid sub-element of the i-th fluid element
      int1d *m_lstIntEltMsh;      // list of fluid sub-element (link with m_ptrIntEltMsh)

      int1d m_sizLstItfEltMsh;    // size of m_lstItfEltMsh
      int1d *m_ptrItfEltMsh;      // [i] = index in m_lstItfEltMsh of the first interface sub-element of the i-th fluid element
      int1d *m_lstItfEltMsh;      // list of solid sub-element (link with m_ptrItfEltMsh)
      
      int1d *m_verSgn;            // tells on which side of the interface a mesh vertex is

      // -------------------------------------------------
      // mapping from solid mesh to intersected solid mesh
      // -------------------------------------------------
      int1d m_sizLstIntEltItf;    // size of m_lstIntEltItf
      int1d *m_ptrIntEltItf;      // [i] = index in m_lstIntEltItf of the first solid sub-element of the i-th solid element
      int1d *m_lstIntEltItf;      // list of solid sub-element (link with m_ptrIntEltItf)

      // ------------------------------------------------------
      // data of INTERSECTED fluid mesh + mapping to fluid mesh
      // ------------------------------------------------------
      int1d m_nbrIntVerMsh;       // number of vertices of the intersected fluid mesh
      double3d *m_crdMsh;         // coordinates of the vertices
      int1d *m_intVerIdx;         // corresponding index of the vertex in the mesh. =0 if not a mesh vertex. <0 if it is a degenerate case.
      int1d *m_intVerSgn;         // tells on which side of the interface the vertices of the intersected mesh are.

      int1d m_nbrIntEltMsh;       // number of element of the intersected fluid mesh
      int4d *m_eltMsh;            // list of the elements
      int1d *m_elt2MshEltMsh;     // [i] = id of the element of the fluid mesh to which this i-th element belongs
      int1d *m_eltSgn;            // [i] = side of the solid from which the i-th element is

      // -----------------------------------------------------------------
      // data of INTERSECTED solid + mapping to the solid and fluid meshes
      // -----------------------------------------------------------------
      int1d m_nbrIntVerItf;       // number of vertices of the intersected solid mesh
      double3d *m_crdItf;         // coordinates of the vertices

      int1d m_nbrIntEltItf;       // number of element of the intersected fluid mesh
      int3d *m_elt2DItf;          // list of the 1D elements
      int4d *m_elt3DItf;          // list of the 2D elements

      int1d *m_elt2MshEltItf;     // [i] = id of the element of the fluid mesh to which this i-th element belongs
      int1d *m_elt2ItfElt;        // [i] = id of the element of the solid mesh to which this i-th element belongs

      // -----------------------------------------------------------------------------------------------------
      // mapping from boundary fluid mesh to boundary intersected fluid mesh + INTERSECTED boundary fluid mesh
      // -----------------------------------------------------------------------------------------------------
      int1d m_sizLstIntBndMsh;
      int1d *m_ptrIntBndMsh;
      int1d *m_lstIntBndMsh;
      int1d m_nbrIntEltBnd;       // number of boundary elements in the intersected fluid esh
      int3d *m_elt2DBnd;          // list of the 1D boundary sub-elements
      int4d *m_elt3DBnd;          // list of the 2D boundary sub-elements

      // ----------------------------------------------------------------------------------------------------------
      // mapping from intersected tip fluid mesh element to fluid mesh + INTERSECTED tip fluid mesh element surface
      // ----------------------------------------------------------------------------------------------------------
      int1d m_sizLstTipSrf;
      int4d *m_ptrTipSrf;
      int1d *m_lstTipSrf;
      int1d m_nbrIntTipElt;       // number of fluid elements containing a Tip
      int1d *m_tipElt2MshElt;     // corresponding element in the fluid mesh
      int1d m_nbrIntTipSrf;       // number of tip surface elements
      int4d *m_tipSrf3D;          // list of 3D tip surface elements
      int2d *m_tipSrf2D;          // list of 2D tip surface elements
      int2d *m_tipSrfSgn;         // sign of tip surface elements

      // ---------------------------------------------------------------
      // mapping from intersected front fluid mesh element to fluid mesh 
      // ---------------------------------------------------------------
      int1d m_nbrIntVerFro;       // number of front points
      int3d *m_verFro;            // data of the front points
      double6d *m_vecFro;         // don't really know what that is at the moment

      // --------------------------------------------------------------
      // mapping from intersected element to intersecting interface ref 
      // --------------------------------------------------------------
      int1d *m_mshItfIdx;
      int1d *m_bndItfIdx;
      // END OF WARNING //
      ////////////////////
      #endif
  };
}

#endif
