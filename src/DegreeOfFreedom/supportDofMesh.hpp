//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon and others
//

/*!
 \file supportDofMesh.hpp
 \author J. Foulon and others
 \date 07/10/2010
 \brief SupportDofMesh class declaration
 */

#ifndef SUPPORTDOFMESH_HPP
#define SUPPORTDOFMESH_HPP

// System includes
#include <vector>
#include <unordered_map>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "DegreeOfFreedom/variable.hpp"
#include "FiniteElement/refElement.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce
{
  class GeoElement;
  /*!
   \class SupportDofMesh
   \date 07/10/2010
   \brief Object SupportDofMesh links the mesh and the finite element used to solve the problem.
   We build one SupportDofMesh by variable and etablish a link from GeometricMeshRegion to SupportDofMesh.
   The supports of dof for a variable are stored in CSR format with 2 arrays m_iEle which contains
   the first index of the supports in the elements, that are stored in array m_iSupportDof.
   Ex: iel = 5. Support dof in element number iel are stored in m_iSupport from m_iEle[iel] to (m_iEle[iel+1]-1).
   */
  class SupportDofMesh {
  public:
    //! simplify ElementType enum use.
    typedef GeometricMeshRegion::ElementType ElementType;
    //! typedef to simplify use of the std::map<finite element name --> finite element reference>.
    typedef std::unordered_map<std::string, const RefElement*> StringToReferenceElement;
    //! Map to link name of the finite element reference to the finite element reference object.
    static StringToReferenceElement m_eltRefNameToRefEle;

    // Constructors
    //=============
    SupportDofMesh( const Variable& variable, const GeometricMeshRegion::Pointer& mesh);
    SupportDofMesh( const Variable& variable, const GeometricMeshRegion::Pointer& meshLocal, std::vector<felInt>& loc2GlobElemSupport, SupportDofMesh& supportDofMesh);

    // Link geometry and support dof mesh.
    //====================================
    //void setSizeOfIdElementSupport(felInt numElement);

    //! \brief get the id of the element from its type and its id with respect to its type
    void getIdElementSupport(const ElementType& eltType, felInt ielGeom, std::vector<felInt>& vectorSupport) const;
    void getIdElementSupport(const ElementType& eltType, felInt ielGeom, felInt& ielSupportDof) const;

    void getIdElementSupport(felInt idEle, std::vector<felInt>& vecSupport) const;
    void getIdElementSupport(felInt idEle, felInt& ielSupportDof) const;

    void printIdElementSupport(std::ostream& outstr = std::cout) const;

    void getIdPointElementSupport(felInt idEle, std::vector<felInt>& vecIdPoint) const;

    /*!
     * \brief Computes the list of all element supports of element iel.
     *
     * \param[in] iel Index of finite element considered.
     * \param[out] indexes All indexes of support dof related to the \a iel finite element.
     */
    void getIndexesElementSupport(int iel, std::vector<int>& indexes) const;

    void fusionSupportDof(const Variable& variable);

    void matchingElemConnectivity(std::size_t labelIN, std::size_t labelOUT, std::set<std::size_t>& listSuppDofIN, std::set<std::size_t>& listSuppDofOUT);

    void getElemConnectivitySingleLabel(std::size_t label, std::set<std::size_t>& listSuppDof);

    void GetPermutationList(std::vector<std::pair<std::size_t, std::size_t> >& listSuppDofMatched);


    // Embedded Interface
    void matchEmbeddedLabelPairs(const Variable& variable);

    // Cracks
    void matchCrackLabelPairs(const Variable& variable);

    // For both cracks and embedded interface
    void extractIdElemWithConnectivityFromSides(std::size_t labelSide1, std::size_t labelSide2,
                                                std::vector< std::pair<std::size_t, std::set<std::size_t> > >& listSuppElemSide1,
                                                std::vector< std::pair<std::size_t, std::set<std::size_t> > >& listSuppElemSide2);

    /*!
     * \brief Duplicate the support dof intersected by the structure
     *
     * This function duplicates the marked support dof. It modify four vectors:
     * - m_listNode
     * - m_vectorIdElementSupport
     * - m_iEle
     * - m_iSupportDof
     *
     * In the variable m_listNode , the duplicated support dof are insert at the end.
     * In the other vectors, the element on the "left" side (the one where the normal is pointing) is
     * always the first. The element on the "right"side is the second one.
     * This function assumes that you are using \f$ P^1 \f$ finite element.
     */
    void duplicateSupportElements( std::map<felInt, felInt>&                    listIntersectedEltIdx,
                                   std::map<felInt, std::vector<felInt> >&      listIntersectedVerSgn,
                                   std::map<felInt, std::map<felInt, felInt> >& mapVerticesIdxToDupl); 


    //! Print function
    //================
    void print(int verbose = 0, std::ostream& outstr = std::cout) const;

    // Access functions
    //=================
    inline int getNumSupportDof(felInt iel) const {
      return (static_cast<int>(m_iEle[iel+1] - m_iEle[iel]));
    }

    inline const std::vector<Point> & listNode() const {
      return m_listNode;
    }
    inline std::vector<Point> & listNode() {
      return m_listNode;
    }

    inline const std::vector<felInt> & iSupportDof() const {
      return m_iSupportDof;
    }
    inline std::vector<felInt> & iSupportDof() {
      return m_iSupportDof;
    }

    inline const std::vector<felInt> & iEle() const {
      return m_iEle;
    }
    inline std::vector<felInt> & iEle() {
      return m_iEle;
    }

    inline const std::vector<std::vector<felInt> > & vectorIdElementSupport() const {
      return m_vectorIdElementSupport;
    }

    inline std::vector<std::vector<felInt> > & vectorIdElementSupport() {
      return m_vectorIdElementSupport;
    }

    inline const std::map<felInt, felInt> & mapSuppElemMatched() const {
      return m_mapSuppElemMatched;
    }

    inline const std::map<felInt, felInt> & mapSuppDofMatched0() const {
      return m_mapSuppDofMatched0;
    }

    inline const std::map<felInt, felInt> & mapSuppDofMatched1() const {
      return m_mapSuppDofMatched1;
    }

    inline const std::map<felInt, felInt> & mapSuppElemMatchedCrack() const {
      return m_mapSuppElemMatchedCrack;
    }

    inline const std::map<felInt, felInt> & mapSuppDofMatchedCrack0() const {
      return m_mapSuppDofMatchedCrack0;
    }

    inline const std::map<felInt, felInt> & mapSuppDofMatchedCrack1() const {
      return m_mapSuppDofMatchedCrack1;
    }

    inline const std::vector<felInt> & listPerm() const {
      return m_listPerm;
    }

    inline std::vector<felInt> & listPerm() {
      return m_listPerm;
    }

    inline const felInt & numSupportDof() const  {
      return m_numSupportDof;
    }
    
    inline const felInt & getIdFusionDarcy() const  {
      return m_idFusionDarcy;
    }

    // Set functions
    // =============
    void setNumSupportDof(felInt newNum) {
      m_numSupportDof = newNum;
    }

  private:

    //! Reference to mesh
    GeometricMeshRegion::Pointer m_mesh;

    //! Variable associate to these supports of dof
    Variable m_variable;
    //! List of support dof coordinate (similar to list of point in the mesh format)
    std::vector<Point> m_listNode;

    //! total number of support Dof
    //! if NOT FusionDof m_numSupportDof == listeNode.size()
    //! if DO FusionDof m_numSupportDof != listeNode.size()
    felInt m_numSupportDof;
    felInt m_idFusionDarcy;

    std::vector<felInt> m_listPerm;

    //! Pointer to the supports of dof of elements (CSR format: array1)
    std::vector <felInt> m_iEle;

    //! Store all supports of dof (element by element) (CSR format: array2).
    std::vector <felInt> m_iSupportDof;

    //! Link between the id of element in the mesh and the associated support element
    std::vector <std::vector< felInt > > m_vectorIdElementSupport;

    //! Link support elements from both sides of an embedded interface problem (Side 0, Side 1)
    std::map<felInt,felInt> m_mapSuppElemMatched;
    //! Link support dofs from both sides of an embedded interface problem
    std::map<felInt,felInt> m_mapSuppDofMatched0; // from Side 0 to Side 1
    std::map<felInt,felInt> m_mapSuppDofMatched1; // form Side 1 to Side 0

    //~ Link support elements and dofs from both sides of a CRACK problem
    std::map<felInt,felInt> m_mapSuppElemMatchedCrack;
    //! Link support dofs from both sides of an embedded interface problem
    std::map<felInt,felInt> m_mapSuppDofMatchedCrack0; // from Side 0 to Side 1
    std::map<felInt,felInt> m_mapSuppDofMatchedCrack1; // form Side 1 to Side 0


    //! mesh points are already copied.
    bool m_copiedMeshPoint;

    //! edge nodes are allocated
    felInt m_numDofSupportedByEdge;
    bool m_resizedEdgeNode;

    //! face nodes are allocated
    bool m_resizedFaceNode;

    //! volume nodes are allocated
    bool m_resizedVolumeNode;

    //! embedded interface related maps are created
    bool m_createdEmbeddedInterfaceMaps;

    //!  crack related maps are created
    bool m_createdCrackInterfaceMaps;


    //===============================
    //! internal functions:
    //! Initialisation of the std::unordered_map m_eltRefNameToRefEle
    void m_initMap();

    //! resize: m_iSupportDof and m_iEle
    void m_resizeSupportVectors();

    //! copy the mesh points into m_listNode
    void m_copyNodes();

    //! resize lists
    void m_resizeEdgeNodes();
    void m_resizeFaceNodes(const RefElement& refElement);
    void m_resizeVolumeNodes(const RefElement& refElement);

    //! make the link between the nodes of the (reference) finite element, and
    //!    the nodes of the geometric element of the mesh (useful for P1 geoEle and P2 refEle...)
    void m_linkNodesGeoAndRefEle(const CurBaseFiniteElement& fe, std::vector<int>& feIdLocalDof) const;

    //! build the nodes that live on middle edges or middle faces
    //! used to use P2/Q2 finite elements on P1/Q1 mesh
    //! (all edges or faces must have been built in the mesh previously)
    void m_buildNodesOfEdgeFaceVolumePerBag(const std::vector<ElementType>& theBagElt, bool boundary_flag);

    //! return:
    //!   elem : the element.
    //!   elemConnectivity: the ID of the nodes of the element. Ex. P2: [V0, V1, V2, Mid0, Mid1, Mid2]
    void m_getElementSupportConnectivity(const RefElement& refEle, ElementType eltType, felInt iel, const std::vector<int>& feIdLocalDof,
                                         std::vector<felInt>& the_elem, std::vector<felInt>& elemConnectivity) const;

    //! build the arrays in CSR format: m_iEle, m_iSupportDof :
    void m_buildSupportCSR(const std::vector<ElementType>& theBagElt, felInt& cptElt);

    //! to switch from geoEle P1 to refEle P2 for instance
    const RefElement& m_chooseRefEleFromFEType(const GeoElement& geoEle, const ElementType eltType) const;

  };
}

#endif
