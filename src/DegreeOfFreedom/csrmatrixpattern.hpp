//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Cedric Doucet
//

#ifndef CSRMATRIXPATTERN_HPP
#define CSRMATRIXPATTERN_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce {

  /// \class CSRMatrixPattern
  /// \brief Sparse matrix pattern in Compressed Sparse Row (CSR) storage
  class CSRMatrixPattern {

  public:

    /**
     * @brief Default constructor
     */
    CSRMatrixPattern() = default;

    /**
     * @brief Constructor
     * @param[in] numRows number of rows
     * @param[in] numNonzeros number of nonzeros
     */
    CSRMatrixPattern(
      std::size_t numRows,
      std::size_t numNonzeros
      ): m_rowptr(numRows+1,0),
         m_colind(numNonzeros,0)
    {
    }

    /// \brief Index of first nonzero in a row
    /// \param[in] rowIndex index of a row
    /// \return index of first nonzero in row of index rowIndex
    inline felInt & rowPointer(felInt rowIndex) {
#ifdef NDEBUG
      return m_rowptr[rowIndex];
#else
      return m_rowptr.at(rowIndex);
#endif
    }

    /// \brief Index of first nonzero in a row
    /// \param[in] rowIndex index of a row
    /// \return index of first nonzero in row of index rowIndex
    inline felInt const & rowPointer(felInt rowIndex) const {
#ifdef NDEBUG
      return m_rowptr[rowIndex];
#else
      return m_rowptr.at(rowIndex);
#endif
    }

    /// \brief Number of rows
    /// \return number of rows
    inline std::size_t numRows() const {
      return m_rowptr.size()-1;
    }

    /// \brief Number of nonzeros
    /// \return number of nonzeros
    inline std::size_t numNonzeros() const {
      return m_colind.size();
    }

    /// \brief Number of nonzeros in a row
    /// \param[in] rowIndex index of a row
    /// \return number of nonzeros in row of index rowIndex
    inline felInt numNonzerosInRow(std::size_t rowIndex) const {
#ifdef NDEBUG
      return m_rowptr[rowIndex+1] - m_rowptr[rowIndex];
#else
      return m_rowptr.at(rowIndex+1) - m_rowptr.at(rowIndex);
#endif
    }

    /// \brief Column index of a nonzero
    /// \param[in] nonzeroIndex index of a nonzero
    inline felInt const & columnIndex(std::size_t nonzeroIndex) const {
#ifdef NDEBUG
      return m_colind[nonzeroIndex];
#else
      return m_colind.at(nonzeroIndex);
#endif
    }

    /// \brief Column index of a nonzero
    /// \param[in] nonzeroIndex index of a nonzero
    inline felInt & columnIndex(std::size_t nonzeroIndex) {
#ifdef NDEBUG
      return m_colind[nonzeroIndex];
#else
      return m_colind.at(nonzeroIndex);
#endif
    }

    /// \brief Resizing
    /// \param[in] numRows number of rows
    /// \param[in] numNonzeros number of nonzeros
    inline void resize(std::size_t numRows,std::size_t numNonzeros) {
      m_rowptr.resize(numRows+1,0);
      m_colind.resize(numNonzeros,0);
    }

    /// \brief clear
    inline void clear() {
      m_rowptr.clear();
      m_colind.clear();
    }

    /// \param[in] rowPointers This std::vector contains row pointers.
    ///                        Its size equals numRows+1: the number of rows (numRows) plus one.
    ///                        The numRows first entries contain indices of first nonzero in each row.
    ///                        The last entry contains the number of nonzeros.
    //
    /// \param[in] columnIndices This std::vector contains column indices of nonzeros.
    ///                          Is size equals the number of nonzeros.
    ///                          Each entry contain the column index of the corresponding nonzero.
    /// \brief Modify pattern's content
    void set(std::vector<felInt> const & rowPointers,std::vector<felInt> const & columnIndices) {
      m_rowptr = rowPointers;
      m_colind = columnIndices;
    }

    /// \brief Accessor to column indices of nonzeros.
    /// \return Vector of column indices of nonzeros
    std::vector<felInt> const & columnIndices() const {
      return m_colind;
    }

    
    /// \brief Accessor to rowPointer.
    /// \return Vector rowPointer
    std::vector<felInt> const & rowPointer() const {
      return m_rowptr;
    }

    
    /// \brief print the pattern
    void print(int verbose, std::ostream& outstr) const {
      IGNORE_UNUSED_ARGUMENT(verbose);
      outstr << "\nidNode | iCSR[idNode] | jCSR[iCSR[idNode]], ..., jCSR[iCSR[idNode+1]-1]\n";
      for (unsigned int iNode = 0; iNode < m_rowptr.size()-1; ++iNode) {
        outstr << iNode << " | " << m_rowptr[iNode] << " | ";
        for(felInt iDof=m_rowptr[iNode]; iDof<m_rowptr[iNode+1]; ++iDof)
          outstr << m_colind[iDof] << " ";
        outstr << "\n";
      }
      outstr << m_rowptr.size()-1 << " | " << m_rowptr[m_rowptr.size()-1];
      outstr << std::endl << std::endl;
    }

  private:

    /// \brief sparse row pointers (index of first nonzero in each row)
    std::vector<felInt> m_rowptr;

    /// \brief column indices of nonzeros
    std::vector<felInt> m_colind;
  };

}

#endif // CSRMATRIXPATTERN_HPP
