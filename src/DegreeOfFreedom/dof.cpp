//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M.A. Fernandez & J.Foulon
//

/*!
 \file    dof.cpp
 \authors M.A. Fernandez & J.Foulon
 \date    07/10/2010
 \brief   Implementation of the class Dof.
 */

// System includes
#include <cmath>
#include <unordered_set>

// External includes

// Project includes
#include "DegreeOfFreedom/dof.hpp"

namespace felisce
{
  /*!
    \brief Constructor of the Dof class
  */
  Dof::Dof():
    m_numDof(0),
    m_patternIsChanged(false)
  {}

  /*!
    \brief Set m_unknown, m_variable and m_pSupportDofUnknown members.
    \param[in] unknown complete list of unkowns.
    \param[in] variable complete list of variables.
    \param[in] SupportDofUnknown supports dof associate to the unknowns.
  */
  void Dof::setDof(const ListUnknown& unknown, const ListVariable& variable,
                   const std::vector<SupportDofMesh*>& pSupportDofUnknown) {
    m_unknown = unknown;
    m_variable = variable;
    m_pSupportDofUnknown = pSupportDofUnknown;
  }

  /*!
    \brief Function to get global number of dof.
    \param[in] iEle Local number of the element.
    \param[in] iLocDof Local number of the support dof.
    \param[in] iVar Number of the variable
    \param[in] iComp Number of the component of the unknown
    \param[out] numGlobalDof Global number of the support dof.
  */
  void Dof::loc2glob(felInt iEle, int iLocDof, int iVar, std::size_t iComp, felInt& numGlobalDof) const {
    const int iUnknown = m_variable.listIdUnknownOfVariable(iVar);
    int iVar2 = -1;

    #ifndef NDEBUG
      if ( iUnknown == -1) {
        std::cout << "iVar: " << iVar << std::endl;
        FEL_ERROR("This variable isn't an unknown in this problem !\n");
      }

      if ((unsigned) iVar > m_variable.size() )
        FEL_ERROR("Not Enough variable!");

      if ( iComp >= m_variable[iVar].numComponent()) {
        std::ostringstream oconv;
        oconv << "Not enough components for this variable! ";
        oconv << "At least " << m_variable[iVar].numComponent() + 1u << " were expected but " << iComp << " were found.";
        FEL_ERROR(oconv.str());
      }
    #endif

    felInt numDofExisting = 0;
    felInt numSupportOfDof = 0;
    numGlobalDof = 0;
    // e.g. for Navier Stokes model, we have two unknowns. In m_pSupportDofUnknown, there are first the velocity dof support and then pressure one.
    // So if iUnknown==0 (velocity), we directly compute the global dof number. Else if iUnknown==1 (pressure), we have to skip all the velocity global dof numbers (numDofExisting).
    for ( int i = 0; i < iUnknown; i++) {
      iVar2 = m_unknown.idVariable(i);
      numSupportOfDof = m_pSupportDofUnknown[i]->numSupportDof();
      numDofExisting += numSupportOfDof * m_variable[iVar2].numComponent() ;
    }

    const auto& r_support_unknowns = m_pSupportDofUnknown[iUnknown];
    numGlobalDof = numDofExisting + r_support_unknowns->iSupportDof()[r_support_unknowns->iEle()[iEle] + iLocDof ] * m_variable[iVar].numComponent() + iComp;
  }

  /*!
    \brief Function to get global number of dof.
  */
  void Dof::loc2glob(felInt iEle, int numLocDof, int iVar, std::size_t numComp, std::vector<felInt>& numGlobalDof) const {
    std::vector<std::size_t> loc2globComp(numComp);
    std::vector<int> loc2globDof(numLocDof);

    for (std::size_t iComp = 0; iComp < numComp; iComp++)
      loc2globComp[iComp] = iComp;

    for (int iSupport = 0; iSupport < numLocDof; iSupport++)
      loc2globDof[iSupport] = iSupport;

    this->loc2glob(iEle, loc2globDof, iVar, loc2globComp, numGlobalDof);
  }

  /*!
    \brief Function to get global number of dof.
  */
  void Dof::loc2glob(felInt iEle, std::vector<int>& iLocDof, int iVar, std::vector<std::size_t>& iComp, std::vector<felInt>& numGlobalDof) const {
    int iUnknown = m_variable.listIdUnknownOfVariable(iVar);
    int iVar2 = -1;
    std::vector<felInt> locdofid(iLocDof.size());
    numGlobalDof.resize(iComp.size() * iLocDof.size());

    #ifndef NDEBUG
      if ( iUnknown == -1) {
        std::cout << "iVar: " << iVar << std::endl;
        FEL_ERROR("This variable isn't an unknown in this problem !\n");
      }

      if ((unsigned) iVar > m_variable.size() )
        FEL_ERROR("Not Enough variable!");

      for(std::size_t i=0; i<iComp.size(); ++i) {
        if ( iComp[i] >= m_variable[iVar].numComponent()) {
          std::ostringstream oconv;
          oconv << "Not enough components for this variable! ";
          oconv << "At least " << m_variable[iVar].numComponent() + 1u << " were expected but "
                << iComp[i] << " were found.";
          FEL_ERROR(oconv.str());
        }
      }
    #endif

    felInt numDofExisting = 0;
    felInt numSupportOfDof = 0;
    // e.g. for Navier Stokes model, we have two unknowns. In m_pSupportDofUnknown, there are first the velocity dof support and then pressure one.
    // So if iUnknown==0 (velocity), we directly compute the global dof number. Else if iUnknown==1 (pressure), we have to skip all the velocity global dof numbers (numDofExisting).
    // TODO : rename "numDofExisting"
    for ( int i = 0; i < iUnknown; i++) {
      iVar2 = m_unknown.idVariable(i);
      numSupportOfDof = m_pSupportDofUnknown[i]->numSupportDof();
      numDofExisting += numSupportOfDof * m_variable[iVar2].numComponent() ;
    }

    // dof are arrange in a 1d std::vector like this: iLocDof[0] - iComp[0], iLocDof[1] - iComp[0], ....
    std::size_t cnt = 0;
    for(std::size_t i=0; i<iLocDof.size(); ++i)
      locdofid[i] = numDofExisting + m_pSupportDofUnknown[iUnknown]->iSupportDof()[m_pSupportDofUnknown[iUnknown]->iEle()[iEle] + iLocDof[i] ]*m_variable[iVar].numComponent();

    for(std::size_t j=0; j<iComp.size(); ++j) {
      for(std::size_t i=0; i<iLocDof.size(); ++i) {
        numGlobalDof[cnt] = locdofid[i] + iComp[j];
        cnt += 1;
      }
    }
  }

  /*!
    \brief Function to get global number of dof from one number of support dof.
    \param[in] idSupportDof number of the support dof.
    \param[out] idDof std::vector of dofs.
  */
  void Dof::supportDofToDof(felInt idSupportDof, std::vector<felInt>& idDof) const {
    int idVar;
    int idVariable;
    felInt numDofExisting = 0;
    felInt numSupportOfDof = 0;
    for (unsigned int iUnknown = 0; iUnknown < m_unknown.size(); iUnknown++) {
      idVar = m_unknown.idVariable(iUnknown);
      numDofExisting = 0;
      numSupportOfDof = 0;

      if(FelisceParam::instance().FusionDof) {
        idSupportDof = m_pSupportDofUnknown[iUnknown]->listPerm()[idSupportDof];
      }

      for (unsigned int i = 0; i < iUnknown; i++) {
        numSupportOfDof = m_pSupportDofUnknown[i]->numSupportDof();
        idVariable = m_unknown.idVariable(i);
        numDofExisting += numSupportOfDof * m_variable[idVariable].numComponent();
      }

      for (std::size_t iComp = 0; iComp < m_variable[idVar].numComponent(); iComp++)
        idDof.push_back(numDofExisting + idSupportDof*m_variable[idVar].numComponent() + iComp);
    }
  }

  /*!
    \brief Find the global number of the component in the mask array.
    Useful to identify interaction between variable.
    \param[in] iUnknown Number of the unknown
    \param[in] iComp Number of the research component for this unknown
    \return The global number of the component.
  */
  int Dof::getNumGlobComp(std::size_t iUnknown, int iComp) const {
    int result = 0;
    int idVar = -1;
    FEL_ASSERT( iUnknown < m_unknown.size() && "Not enough unknown in the system.");
    for ( std::size_t iUnk = 0; iUnk < iUnknown; iUnk++) {
      idVar = m_unknown.idVariable(iUnk);
      result += m_variable[idVar].numComponent();
    }
    result +=iComp;
    return result;
  }

  /*!
    \brief Find the global number of the dof associated to the support dof of a given variable.
    \param[in] iUnknown id of the Unknown
    \param[in] listSuppDof support of Dofs
    \param[out] listDofAssociated the global number of the dofs .
  */
  void Dof::identifyDofBySupport(felInt iUnknown, std::vector<felInt>& listSuppDof, std::vector<felInt>& listDofAssociated) const 
  {
    // Compute number of dof placed before the current unknown
    felInt numDofExisting = 0;
    for (felInt i = 0; i < iUnknown; ++i)
      numDofExisting += m_numDofPerUnknown[i];

    // Get the dof idx for the current unknown
    felInt idSupportDof;
    std::size_t counter = 0;
    const int idVar = m_unknown.idVariable(iUnknown);
    listDofAssociated.resize(m_variable[idVar].numComponent()*listSuppDof.size(), -1);
    for (std::size_t i = 0; i < listSuppDof.size(); ++i) {
      idSupportDof = listSuppDof[i];
      for (std::size_t iComp = 0; iComp < m_variable[idVar].numComponent(); ++iComp)
        listDofAssociated[counter++] = numDofExisting + idSupportDof*m_variable[idVar].numComponent() + iComp;
    }
  }

  // /*!
  //   \brief Method to evaluate the pattern of the matrix.
  //   We realize an "assembly" loop to browse all dof defined (with all unknowns and components)
  //   and we connect dof each others.
  //   After, we store all this information in 2 arrays ( m_iCSR, m_jCSR) as CSR format.
  //   We are using STL unordered_set to sort dof number and eliminate copies.
  //   \todo Optimize loop organisation.
  //   \todo Benoit, 29/10/13 : remove this function ? It seems to not be used anywhere.
  // */
  // void Dof::buildPattern() {
  //   felInt numSupportOfDof = 0;
  //   int idVar1 = -1;
  //   int idVar2 = -1;

  //   // Eval number total of dof and store total of dof by unknown in STL vector
  //   m_numDofPerUnknown.resize(m_unknown.size());
  //   for (std::size_t iUnknown = 0; iUnknown < m_unknown.size(); iUnknown++) {
  //     idVar1 = m_unknown.idVariable(iUnknown);
  //     numSupportOfDof = m_pSupportDofUnknown[iUnknown]->numSupportDof();
  //     m_numDofPerUnknown[iUnknown] = numSupportOfDof * m_variable[idVar1].numComponent();
  //     m_numDof += m_numDofPerUnknown[iUnknown];
  //   }

  //   // loop to connect all dof each others.
  //   std::vector< std::unordered_set<felInt> > nodesNeighborhood(m_numDof);
  //   felInt node1 = 0;
  //   felInt node2 = 0;
  //   for ( std::size_t iUnknown = 0; iUnknown < m_unknown.size(); iUnknown++) {
  //     idVar1 = m_unknown.idVariable(iUnknown);
  //     for (felInt iel = 0; iel < static_cast<felInt>(m_pSupportDofUnknown[iUnknown]->iEle().size()) - 1; iel++) {
  //       for ( int iSupport = 0; iSupport <  m_pSupportDofUnknown[iUnknown]->getNumSupportDof(iel); iSupport++) {
  //         for (std::size_t iComp = 0; iComp < m_variable[idVar1].numComponent(); iComp++) {
  //           this->loc2glob(iel,iSupport,idVar1,iComp,node1);
  //           for ( std::size_t iUnknown2 = 0; iUnknown2 < m_unknown.size(); iUnknown2++) {
  //             idVar2 = m_unknown.idVariable(iUnknown2);
  //             for ( int jSupport = 0; jSupport < m_pSupportDofUnknown[iUnknown2]->getNumSupportDof(iel); jSupport++) {
  //               for (std::size_t jComp = 0; jComp < m_variable[idVar2].numComponent(); jComp++) {
  //                 const int iConnect = getNumGlobComp( iUnknown, iComp);
  //                 const int jConnect = getNumGlobComp( iUnknown2, jComp);
  //                 if ( m_unknown.mask()(iConnect, jConnect) > 0) {
  //                   this->loc2glob(iel,jSupport,idVar2,jComp,node2);
  //                   nodesNeighborhood[node1].insert(node2);
  //                   nodesNeighborhood[node2].insert(node1);
  //                 }
  //               }
  //             }
  //           }
  //         }
  //       }
  //     }
  //   }

  //   //Storage in CSR format
  //   felInt dofSize = 0;
  //   for (unsigned int iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
  //     dofSize += nodesNeighborhood[iNode].size();
  //   }
  //   m_pattern.resize(m_numDof,dofSize);
  //   felInt pos = 0;
  //   for (unsigned int iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
  //     m_pattern.rowPointer(iNode+1) = m_pattern.rowPointer(iNode) + nodesNeighborhood[iNode].size();
  //     pos = 0;
  //     for(auto iCon = nodesNeighborhood[iNode].begin(); iCon != nodesNeighborhood[iNode].end(); iCon++) {
  //       m_pattern.columnIndex(m_pattern.rowPointer(iNode) + pos) = *iCon;
  //       pos++;
  //     }
  //   }
  // }

  /*!
    \brief Method to evaluate the pattern of the matrix.
    We realize an "assembly" loop to browse all dof defined (with all unknowns and components)
    and we connect dof each others.
    After, we store all this information in 2 arrays ( m_iCSR, m_jCSR) as CSR format.
    We are using STL unordered_set to sort dof number and eliminate copies.
    \todo Optimize loop organisation.
  */
  void Dof::buildPattern(int rankProc, const felInt* dofRepartition) {
    m_pattern.clear();

    felInt numDofPerProc = 0;
    for (felInt iDof = 0; iDof < m_numDof; iDof++) {
      if (dofRepartition[iDof] == rankProc)
        numDofPerProc++;
    }

    int idVar1, idVar2;
    int iMehs1, iMesh2;
    std::vector< std::unordered_set<felInt> > nodesNeighborhood(m_numDof);
    felInt node1 = 0;
    felInt node2 = 0;

    // Check
    const auto size_unknown = m_unknown.size();
    FEL_ASSERT(m_pSupportDofUnknown.size() == size_unknown && "Inconsistent size of the system");

    // Loop to connect all dof each others.
    // For all unknown of the linear system
    for ( std::size_t iUnknown1 = 0; iUnknown1 < size_unknown; iUnknown1++) {
      idVar1 = m_unknown.idVariable(iUnknown1);
      iMehs1 = m_variable[idVar1].idMesh();

      // For all support element of the current unknown
      const auto& r_support_1 = m_pSupportDofUnknown[iUnknown1];
      const auto& r_variable_1 = m_variable[idVar1];
      for (felInt iel = 0; iel < static_cast<felInt>(r_support_1->iEle().size()) - 1; iel++) {
        // For all support dof of the current support element
        for ( int iSupport = 0; iSupport < r_support_1->getNumSupportDof(iel); iSupport++) {

          // For all component of the unknown
          for (std::size_t iComp = 0; iComp < r_variable_1.numComponent(); iComp++) {

            // Get the global number of the support dof
            this->loc2glob(iel, iSupport, idVar1, iComp, node1);

            // FEL_ASSERT(m_numDof > node1 && "Inconsistent size of the system");

            // If the support dof is owned by the process
            if (dofRepartition[node1] == rankProc) {

              // for all unknown of the linear system
              for ( std::size_t iUnknown2 = 0; iUnknown2 < size_unknown; iUnknown2++) {
                idVar2 = m_unknown.idVariable(iUnknown2);
                iMesh2 = m_variable[idVar2].idMesh();
                if ( iMehs1 != iMesh2 )
                  continue;

                // for all support dof of the current support element
                const auto& r_support_2 = m_pSupportDofUnknown[iUnknown2];
                const auto& r_variable_2 = m_variable[idVar2];
                for ( int jSupport = 0; jSupport < r_support_2->getNumSupportDof(iel); jSupport++) {

                  // for all component of the second unknown
                  for (std::size_t jComp = 0; jComp < r_variable_2.numComponent(); jComp++) {

                    const int iConnect = getNumGlobComp( iUnknown1, iComp);
                    const int jConnect = getNumGlobComp( iUnknown2, jComp);

                    // FEL_ASSERT(int(m_unknown.mask().size1()) > iConnect && "Inconsistent size of the system");
                    // FEL_ASSERT(int(m_unknown.mask().size2()) > jConnect && "Inconsistent size of the system");

                    // if the two components are connected to each other
                    if ( m_unknown.mask()(iConnect, jConnect) > 0) {
                      // get the global number of the second support dof
                      this->loc2glob(iel,jSupport,idVar2,jComp,node2);

                      // FEL_ASSERT(m_numDof > node2 && "Inconsistent size of the system");

                      // and insert it in nodesNeighboorhood if it's not in yet.
                      nodesNeighborhood[node1].insert(node2); // TODO D.C. why we don't check if ( node1 != node2) as everywhere else???
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    //Storage in CSR format
    felInt dofSize = 0;
    for (unsigned int iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
      dofSize += nodesNeighborhood[iNode].size();
    }
    m_pattern.resize(numDofPerProc,dofSize);
    felInt pos = 0;
    felInt cptDof = 0;
    for (unsigned int iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
      if (dofRepartition[iNode] == rankProc) {
        m_pattern.rowPointer(cptDof+1) = m_pattern.rowPointer(cptDof) + nodesNeighborhood[iNode].size();
        pos = 0;
        for(auto iCon = nodesNeighborhood[iNode].begin(); iCon != nodesNeighborhood[iNode].end(); iCon++) {
          m_pattern.columnIndex(m_pattern.rowPointer(cptDof) + pos) = *iCon;
          pos++;
        }
        cptDof++;
      }
    }
  }

  /*!
    \brief Method to initialize the pattern with a uniform repartition of the dof on the processes.
    We realize an "assembly" loop to browse all dof defined (with all unknowns and components)
    and we connect dof each others.
    After, we store all this information in 2 arrays ( m_iCSR, m_jCSR) as CSR format.
    We are using STL set to sort dof number and eliminate copies.
    \todo Optimize loop organisation.
  */
  void Dof::initializePattern(int sizeProc, int rankProc) {
    felInt numSupportOfDof = 0;
    int idVar1, idVar2;
    int iMesh1, iMesh2;
    // Eval number total of dof and store total of dof by unknown in STL vector
    const auto size_unknown = m_unknown.size();
    m_numDofPerUnknown.resize(size_unknown);

    for (std::size_t iUnknown = 0; iUnknown < size_unknown; iUnknown++) {
      idVar1 = m_unknown.idVariable(iUnknown);
      numSupportOfDof = m_pSupportDofUnknown[iUnknown]->numSupportDof();
      m_numDofPerUnknown[iUnknown] = numSupportOfDof * m_variable[idVar1].numComponent();
      m_numDof += m_numDofPerUnknown[iUnknown];
    }

    // number of dof in this proc
    felInt numDofByProc = m_numDof/sizeProc;
    felInt beginIdDofLocal = rankProc*numDofByProc;
    felInt numDofProc;
    felInt endIdDofLocal;
    if(rankProc == sizeProc-1) {
      numDofProc = m_numDof - beginIdDofLocal;
      endIdDofLocal = m_numDof;
    } else {
      numDofProc = numDofByProc;
      endIdDofLocal = beginIdDofLocal + numDofProc;
    }

    // Loop to connect all dof each others.
    std::vector< std::unordered_set<felInt> > nodesNeighborhood(numDofProc);
    felInt node1 = 0;
    felInt node2 = 0;

    // For all unknowns
    for ( std::size_t iUnknown1 = 0; iUnknown1 < size_unknown; iUnknown1++) {
      idVar1 = m_unknown.idVariable(iUnknown1);
      iMesh1 = m_variable[idVar1].idMesh();

      // For all global support element of this unknown
      const auto& r_support_1 = m_pSupportDofUnknown[iUnknown1];
      const auto& r_variable_1 = m_variable[idVar1];
      for (felInt iel = 0; iel < static_cast<felInt>(r_support_1->iEle().size()) - 1; iel++) {

        // For all support dof in this element
        for ( int iSupport = 0; iSupport < r_support_1->getNumSupportDof(iel); iSupport++) {

          // For all component of the unknown
          for (std::size_t iComp = 0; iComp < r_variable_1.numComponent(); iComp++) {
            // Get the global id of the support dof
            loc2glob(iel, iSupport, idVar1, iComp, node1);

            // If this support dof is owned by this process
            if(node1 >= beginIdDofLocal && node1 < endIdDofLocal) {
              // For all unknown in the linear problem
              for ( std::size_t iUnknown2 = 0; iUnknown2 < size_unknown; iUnknown2++) {
                idVar2 = m_unknown.idVariable(iUnknown2);
                iMesh2 = m_variable[idVar2].idMesh();
                if ( iMesh1 != iMesh2 )
                  continue;

                // For all support dof in the current element
                const auto& r_support_2 = m_pSupportDofUnknown[iUnknown2];
                const auto& r_variable_2 = m_variable[idVar2];
                for (int jSupport = 0; jSupport < r_support_2->getNumSupportDof(iel); jSupport++) {

                  // For all component of the second unknown
                  for (std::size_t jComp = 0; jComp < r_variable_2.numComponent(); jComp++) {
                    // If the two current components are connected
                    const int iConnect = getNumGlobComp( iUnknown1, iComp);
                    const int jConnect = getNumGlobComp( iUnknown2, jComp);
                    if ( m_unknown.mask()(iConnect, jConnect) > 0) {
                      // get the global id of the second support dof
                      loc2glob(iel, jSupport, idVar2, jComp, node2);

                      // remove diagonal term to define pattern and use it in Parmetis.
                      // if the two global ids are different, they are neighboors
                      if ( node1 != node2)
                        nodesNeighborhood[node1-beginIdDofLocal].insert(node2);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    // Storage in CSR format
    std::size_t dofSize = 0;
    for (std::size_t iNode = 0; iNode < nodesNeighborhood.size(); iNode++) {
      dofSize += nodesNeighborhood[iNode].size();
    }
    m_pattern.resize(numDofProc,dofSize);
    felInt pos = 0;
    for (felInt iNode = 0; iNode < numDofProc; iNode++) {
      m_pattern.rowPointer(iNode+1) = m_pattern.rowPointer(iNode) + nodesNeighborhood[iNode].size();
      pos = 0;
      for(auto iCon = nodesNeighborhood[iNode].begin(); iCon != nodesNeighborhood[iNode].end(); iCon++) {
        m_pattern.columnIndex(m_pattern.rowPointer(iNode) + pos) = *iCon;
        pos++;
      }
    }
  }

  void Dof::mergeGlobalPattern(const std::vector<felInt>& iCSRc_new, const std::vector<felInt>& jCSRc_new) {
    // Size of iCSR (number of dof + 1)
    std::size_t dimICSR = m_pattern.numRows() + 1;

    // Check that the given complementary pattern has the right size
    FEL_ASSERT(iCSRc_new.size() == dimICSR);

    // if m_patternC is not created already, initialize it to nothing
    if(m_patternIsChanged == 0)
      m_patternC.resize(dimICSR, 0);

    // temporary set to add the new non zero element contains in the new complementary pattern
    std::set<felInt> tmpset, tmpsetc;
    std::pair<std::set<felInt>::iterator, bool> ret;

    // New jCSR and iCSR for m_pattern and m_patternC
    std::vector<felInt> iCSR(dimICSR), iCSRc(dimICSR);
    std::vector<felInt> jCSR, jCSRc;

    // Initialize first entry of iCSR and iCSRc
    iCSR[0] = 0;
    iCSRc[0] = 0;

    // Reserve space for jCSR and jCSRc
    jCSR.reserve(m_pattern.numNonzeros() + jCSRc_new.size());
    jCSRc.reserve(m_patternC.numNonzeros() + jCSRc_new.size());

    // loop over each dof
    for(std::size_t n=0; n<dimICSR-1; ++n) {
      // fill tmpset with the current indices of column for this row
      tmpset.clear();
      for(felInt i=m_pattern.rowPointer(n); i<m_pattern.rowPointer(n+1); ++i)
        tmpset.insert(m_pattern.columnIndex(i));

      // fill tmpsetc with the current indices of column for this row
      tmpsetc.clear();
      for(felInt i=m_patternC.rowPointer(n); i<m_patternC.rowPointer(n+1); ++i)
        tmpsetc.insert(m_patternC.columnIndex(i));

      // insert the new nonzero of jCSRc_new in the sets
      for(felInt i=iCSRc_new[n]; i<iCSRc_new[n+1]; ++i) {
        ret = tmpset.insert(jCSRc_new[i]);
        if(ret.second)
          tmpsetc.insert(jCSRc_new[i]);
      }

      // set the iCSR component of the patterns
      iCSR[n+1] = iCSR[n] + tmpset.size();
      iCSRc[n+1] = iCSRc[n] + tmpsetc.size();

      // set the jCSR component of the patterns
      for (auto it = tmpset.begin(); it != tmpset.end(); ++it)
        jCSR.push_back(*it);

      for (auto it = tmpsetc.begin(); it != tmpsetc.end(); ++it)
        jCSRc.push_back(*it);
    }

    // Resize the jCSR componenent of the patterns (remove the not needed reserve space)
    jCSR.resize(iCSR[dimICSR-1]);
    jCSRc.resize(iCSRc[dimICSR-1]);

    // Set the new patterns
    m_pattern.set(iCSR, jCSR);
    m_patternC.set(iCSRc, jCSRc);

    // Set the flag indicating that there is a non empty complementary pattern
    m_patternIsChanged = true;
  }

  /*!
    \brief Build complementary pattern with the new repartition of the dof
    \param[in] mpiComm MPI communicator
    \param[in] numProc Total number of process.
    \param[in] rankProc Rank of the current process.
    \param[in] dofRepartition New repartition of the dof among the process.
  */
  void Dof::mergeLocalCompPattern(MPI_Comm mpiComm, felInt numProc, felInt rankProc, const std::vector<felInt>& dofRepartition) {
    // build the uniform initial repartition
    felInt numDofByProc = m_numDof/numProc;
    felInt beginIdDofLocal = rankProc*numDofByProc;
    felInt endIdDofLocal;
    felInt numDofProc;
    if(rankProc == numProc-1) {
      numDofProc = m_numDof - beginIdDofLocal;
      endIdDofLocal = m_numDof;
    } else {
      numDofProc = numDofByProc;
      endIdDofLocal = beginIdDofLocal + numDofProc;
    }

    // Complementary pattern on the new repartition of dof
    std::vector<felInt> iCSRc(m_pattern.numRows() + 1), jCSRc;

    // New pattern
    std::vector<felInt> iCSR, jCSR;

    // Initialize first value of iCSRc and reserve space for jCSRc
    iCSRc[0] = 0;
    jCSRc.reserve(m_patternC.numNonzeros());

    // Count the dof own by this process
    felInt countDof = 0;
    felInt countDofInit = 0;
    felInt numNonZeros = 0;

    // Temporary vector
    std::vector<felInt> tmpvec;

    // MPI status
    MPI_Status mpiStatus;

    // build new complementary pattern
    for(felInt idof=0; idof<m_numDof; ++idof) {
      // if the dof is own by this process
      if(dofRepartition[idof] == rankProc) {
        // if the dof was also on this process with the initial repartition
        if((idof >= beginIdDofLocal) && (idof < endIdDofLocal)) {
          // take directly from the current complementary pattern
          numNonZeros = m_patternC.numNonzerosInRow(countDofInit);
          iCSRc[countDof + 1] = iCSRc[countDof] + numNonZeros;
          for(felInt i=m_patternC.rowPointer(countDofInit); i<m_patternC.rowPointer(countDofInit+1); ++i)
            jCSRc.push_back(m_patternC.columnIndex(i));

          ++countDofInit;
          ++countDof;
        } else {
          // ask the process owning it in the initial repartition
          felInt senderRank = 1;
          while(idof >= senderRank * numDofByProc)
            ++senderRank;
          senderRank = std::min(senderRank - 1, numProc - 1);

          MPI_Probe(senderRank, idof, mpiComm, &mpiStatus);
          MPI_Get_count(&mpiStatus, MPI_INT, &numNonZeros);
          tmpvec.resize(numNonZeros);
          MPI_Recv(tmpvec.data(), numNonZeros, MPI_INT, senderRank, idof, mpiComm, &mpiStatus);
          for(std::size_t i=0; i<tmpvec.size(); ++i)
            jCSRc.push_back(tmpvec[i]);

          iCSRc[countDof + 1] = iCSRc[countDof] + numNonZeros;
          ++countDof;
        }
      } else {
        // if this dof is own by this process with the initial repartition
        if((idof >= beginIdDofLocal) && (idof < endIdDofLocal)) {
          // send to the new process
          felInt localPos = idof - beginIdDofLocal;
          numNonZeros = m_patternC.numNonzerosInRow(localPos);

          // We, Benoit and Matteo, added this check. (27 may 2015)
          // The reason behind is that, even when sending zero integers,
          // the address of "m_patternC.columnIndex(m_patternC.rowPointer(localPos))" was
          // evaluated and led to a segmentation fault when accessing to m_colind into the complementary pattern
          // ( at least in debug mode, in release it was not clear)
          // This happened, for instance, when the complementary pattern was completely empty on one processor (think about implicit windkessel for instance).
          // It was not possible to avoid at all the send when sending zero integers since the other processor would have waited in any case:
          if ( numNonZeros > 0 ) {
            MPI_Send(&m_patternC.columnIndex(m_patternC.rowPointer(localPos)), numNonZeros, MPI_INT, dofRepartition[idof], idof, mpiComm);
          }
          else {
            int trash(-1);
            MPI_Send(&trash, numNonZeros, MPI_INT, dofRepartition[idof], idof, mpiComm);
          }
          ++countDofInit;
        }
      }
    }

    // Add the complementary pattern to the pattern
    jCSRc.resize(iCSRc[iCSRc.size() - 1]);
    iCSR.resize(iCSRc.size());
    jCSR.resize(m_pattern.numNonzeros() + jCSRc.size());

    iCSR[0] = 0;
    felInt count = 0;
    for(std::size_t i=0; i<iCSR.size()-1; ++i) {
      // iCSR
      iCSR[i+1] = iCSR[i] + m_pattern.numNonzerosInRow(i) + (iCSRc[i+1] - iCSRc[i]);

      // jCSR
      for(felInt j=m_pattern.rowPointer(i); j<m_pattern.rowPointer(i+1); ++j) {
        jCSR[count] = m_pattern.columnIndex(j);
        ++count;
      }

      for(felInt j=iCSRc[i]; j<iCSRc[i+1]; ++j) {
        jCSR[count] = jCSRc[j];
        ++count;
      }
    }

    // set the patterns
    m_patternC.set(iCSRc, jCSRc);
    m_pattern.set(iCSR, jCSR);
  }

  void Dof::getNeighbourhood( felInt node, std::vector<felInt> & neighbourhood ) const{
    neighbourhood.clear();
    const std::size_t start = m_pattern.rowPointer( node );
    const std::size_t size = m_pattern.rowPointer( node + 1 ) - start;
    for ( std::size_t k(0); k < size; ++k)
      neighbourhood.push_back( m_pattern.columnIndex( start + k ) );
  }

  /*! 
   * Print pattern of the matrix and number of dofs associate at each variable.
   */
  void Dof::print(int verbose, std::ostream& outstr) const {
    if (verbose) {
      outstr << "Dof: " << m_numDof << std::endl;
      outstr << "Number of dof per unknown:\n";
      for (unsigned int iNumDof = 0; iNumDof < m_numDofPerUnknown.size(); iNumDof++)
        outstr << "unknown " << iNumDof << ": " << m_numDofPerUnknown[iNumDof] << "\n";
      outstr << std::endl;

      if(verbose > 25) {
        outstr << "PATTERN" << std::endl;
        m_pattern.print(verbose, outstr);

        if(m_patternIsChanged == 1) {
          outstr << "COMPLEMENTARY PATTERN" << std::endl;
          m_patternC.print(verbose, outstr);
        }
      }
    }
  }
}

