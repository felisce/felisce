//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau
//

#ifndef __DOFBOUNDARY_HPP__
#define __DOFBOUNDARY_HPP__

// System includes

// External includes

// Project includes
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"
#include "DegreeOfFreedom/dof.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "PETScInterface/petscVector.hpp"

namespace felisce 
{
//forward declaration
class LinearProblem;
// class PetscMatrix;
/*! \class DofBoundary
  \author M.Aletti
  \date 2015

  DofBoundary contains several tools to:
  - list the dofs at the interface in order to create a one to one mapping within two different linear problems that share a common interface.
  - create a new ordering (local, application and petsc) to describe dofs at the interface in order to define matrices and vectors therein.
  - mappings to associate dofs on the boundary to the corresponding ones on the volume.
  - it also creates a subcommunicator where procs are divided into two separate groups: those who have at least one dof on the boundary and those who don't.
*/
class DofBoundary {
  
public:
  /// Two types of PetsVec, to improve readibility
  enum vectorType { sequential, ///< Sequential vectors
                    parallel    ///< Parallel vectors
                  };

  //===============
  // INITIALIZATION METHODS AND DATA MEMBERS
  //==============
public:
  DofBoundary();
  void initialize( LinearProblem* lpb_ptr );
  //! it fills the interface mappings in.
  void buildBoundaryVolumeMapping(int iUnknown, int iComponent );
  void buildBoundaryVolumeMapping(int iUnknown, std::vector<int> components );
  /*!
    @{ \name General information taken from LinearProblem
  */
private:
  Dof* m_dofPtr; //!< Pointer to m_dof of LinearProblem.
  AO m_ao; //!< The ApplicationOrdering of LinearProblem.
  std::vector < int > *m_dofPartPtr; //!< 
  /*!@}*/
public:
  //! it fills m_boundaryPattern in, taking the pattern from the volume matrix pattern.
  void buildBoundaryPattern();
  //! it allocates a matrix on the boundary using the boundary pattern.
  void allocateMatrixOnBoundary( PetscMatrix& theMatrix );

  /** 
   * @brief it allocates a vector on the boundary
   * Note that it is necessary to pass v as a reference since the allocation is done at this time and therefore even the pointer is changed.
   * The vector is allocated only if you actually have some dofs on the boundary.
   */
  PetscVector allocateBoundaryVector(vectorType type);

  /** 
   * @brief It restricts a volume std::vector to a PARALLEL boundary vector
   * @param[in] volumeVector, parallel or sequential vector defined on the volume
   * @param[out] parallelBoundaryVector, a std::vector defined on the boundary
   *  Given a vector on the volume the value on the boundary of the domain are read and written
   *  on a smaller vector defined only on the boundary
   */
  void restrictOnBoundary(PetscVector& volume, PetscVector& boundary);
  
  /** 
   * @brief It provides the extension of a boundary vector to a PARALLEL volume vector
   * @param[in]  boundaryVector parallel or serial vector defined on the boundary.
   * @param[out] parallelVolumeVector a parallel vector defined on the volume.
   * Given a (sequential or parallel) vector defined on the boundary.
   * The function extends it to a PARALLEL volume vector. The function leaves untouched the value corresponing to the volume dofs.
   * Then outside the function you can do a gather, no need to call assembly on the volume vector out of this function.
   */
  void extendOnVolume(PetscVector& volume, PetscVector& boundary);

  // it builds some maps:
  // These maps are global with respect to the different processors.
  //
  //    m_listOfBoundaryPetscDofs: it depends on iVar and iComp
  //      l = label of a region of the interface  |-->   map_l:
  //                                                        i \in (0, numDofOfThisLabel) |--> idDofInPetscOrdering_i
  //    m_mapLab2FirstPoint: /* it does not really depend on nothing, but the geometry */
  //      l = label of a region of the interface  |-->   point, 3D coordinates of the first point
  //
  //    m_mapLab2AllPoints: /* no matter which iVar and which iComp, the ordering will be the same! */
  //      l = label of a region of the interface  |-->   std::vector containing all the points in the same order as in m_listOfBoundaryPetscDofs
  //
  void buildListOfBoundaryPetscDofs(LinearProblem* lpb_ptr, std::vector<int> labelOfInterface, int iUnknown, int iComponent);

  /*!
    @{ \name Getters
  */
  // ===================
  // GETTERS
  // ===================
public:
  //! Number of local dofs on the interface.
  inline std::size_t numLocalDofInterface() const  { return m_numLocalDofInterface;  }
  //! Number of local dofs on the interface.
  inline std::size_t numGlobalDofInterface() const { return m_numGlobalDofInterface; }
  //! Application ordering std::unordered_map for the interface.
  inline AO ao() const { return m_aoInterface; }
  //! Boundary communicator.
  inline MPI_Comm comm() const { return m_boundaryComm; }
  //! true if the current processor has at least one of its dofs on the boundary
  inline bool hasDofsOnBoundary() const { return ( m_numLocalDofInterface > 0 ); }
  /*! \brief given the idUnkComp, the label and the id, it returns the dof id in the petsc volume ordering
    
    \param[in] idUnkComp the composed id that denotes both the unknown and the component
    \param[in] bdLabel the label of the portion of the boundary we are looking at
    \param[in] id the id of the dofTODO: in which ordering??
    \return id of the dof in the petsc volume ordering
  */
  inline felInt getPetscDofs( felInt idUnkComp, felInt bdLabel, felInt id ) const { return m_listOfBoundaryPetscDofs.at(idUnkComp).at(bdLabel).at(id); }
  /*! \brief given the unknown index and the component it returns a mapping that, for each label, contains the list of ids in petsc volume ordering
    \param[in] iUnknown id of the unknown
    \param[in] iComponent number of the component
    \return a std::unordered_map from a label of a portion of the boundary to a std::unordered_map which in fact is a list of ids petsc volume ordering
  */
  inline const std::map<int,std::map<int,int> >& listOfBoundaryPetscDofs( int iUnknown, int iComponent ) const {   return m_listOfBoundaryPetscDofs.at(m_dofPtr->getNumGlobComp(iUnknown,iComponent));  }
  inline const std::map<int, std::map<int, std::map<int, int > > > listOfBoundaryPetscDofs() const { return m_listOfBoundaryPetscDofs;}
  //! function returning a pointer to the starting point of #m_loc2PetscVolume
  inline const felInt* loc2PetscVolPtr() const  { return &m_loc2PetscVolume[0]; }
  //! function returning a pointer to the starting point of #m_loc2PetscInterface
  inline const felInt* loc2PetscBDPtr() const   { return &m_loc2PetscInterface[0]; }
  //! function returning a pointer to the starting point of #m_glob2PetscVolume
  inline const felInt* glob2PetscVolPtr() const { return &m_glob2PetscVolume[0]; }
  //! given an index in the petsc volume ordering it returns the correspoding index in the application boundary ordering
  inline felInt petscVol2ApplicationBD( int id ) const { return  m_petscVolume2Glob.at(id) ; }
  //! given an index in the application boundary ordering it returns the coresponding index in the petsc volume ordering
  inline felInt globBD2PetscVol( int id ) const { return m_glob2PetscVolume[id]; }
  //! it returns the mapping associating each label its first point
  inline const std::map<int, Point >& getMapLab2FirstPoint() const {  return m_mapLab2FirstPoint;  }
  //! it returns the mapping associating each label all its points
  inline const std::map<int, std::vector<Point> > & lab2AllPoints() const {  return m_mapLab2AllPoints;   }  
  // It returns the std::vector of std::set of pairs of (PetscVolDof,Point)
  inline const std::vector<setOfPairOfIntAndPoint> & localVecOfSet() const { return m_localVecOfSet; } 
  /*!@}*/
public:    
  // ===================
  // DEBUG AND PRINTING FUNCTIONS
  // ===================
  void displayBoundaryVolumeMapping () const;
  void exportInterfacePoints( int iUnknown, int iComp ) const;
  void BoundaryVolumeMappingForExample() const;
private:
  // ====================================================================
  // Number of dofs at interface: local and global
  // ====================================================================
  felInt m_numLocalDofInterface;              //!< The local number of interface dofs on the interface.
  felInt m_numGlobalDofInterface;             //!< The local number of interface dofs on the interface.
  // ====================================================================
  // mappings from labels to first and all points
  // ====================================================================
  std::map<int, Point > m_mapLab2FirstPoint;
  std::map<int, std::vector<Point> > m_mapLab2AllPoints;
  //! For each label, std::set of (PetscVolDof,Point)
  std::vector<setOfPairOfIntAndPoint> m_localVecOfSet;
  //! mapping from iUnkComp to a mapping from labels to the petscdofs
  std::map<int, std::map<int, std::map<int, int > > > m_listOfBoundaryPetscDofs;
  // ====================================================================
  // mappings for the interface
  // ====================================================================
  AO m_aoInterface;                           //!  The AO mapping between application ordering and petsc ordering on the interface.
  std::vector<felInt> m_loc2GlobInterface ;   //!  Local ids  (0 ,..., #m_numLocalDofInterface - 1) --> global ids in application ordering.
  std::vector<felInt> m_loc2PetscInterface;   //!  Local ids  (0 ,..., #m_numLocalDofInterface - 1) --> global ids in interface petsc ordering.
  std::vector<felInt> m_glob2PetscVolume;     //!  Global ids (0 ,..., #m_numGlobDofInterface - 1) --> global ids in the volume in petsc ordering.
  std::map<felInt,felInt> m_petscVolume2Glob; //!  Inverse of #m_glob2PetscVolume.
  std::vector<felInt> m_loc2PetscVolume;      /*!  Composition of #m_loc2GlobInterface and #m_glob2PetscVolume: #m_glob2PetscVolume[#m_loc2GlobInterface[i]] = #m_loc2PetscVolume.
                                                it lists all the local dofs of the interface in the petsc volume ordering */
  // ====================================================================
  // MPI BOUNDARY COMMUNICATOR
  // ====================================================================
  MPI_Comm m_boundaryComm; //!<communicator that contains all the procs in the boarder and the others in two saparete groups.
  // ====================================================================
  // PATTERN INFORMATION
  // ====================================================================
  CSRMatrixPattern m_boundaryPattern; //!< The patter of a matrix defined on the boundary.
  // ==========================================
  // BOOLEAN VALUES TO CHECK ERRORS
  // ==========================================
  bool m_boundaryPatternAlreadyBuilt;
  bool m_dofBoundaryHasBeenInitialized;
  bool m_volumeInterfaceMappingBuilt;
  std::vector<bool> m_dofsHaveBeenComputed;
public:
  bool isOnSurface( felInt iUnkComp, felInt petscDof );
private:
  void unRoll(felInt iUnkComp);
  std::vector<std::set<felInt> > m_unRolledList;
public:
  // ====================================================================
  // SURFACE INTERPOLATOR
  // ====================================================================
  void writeSurfaceInterpolatorFile(int iUnknown, int numComp, std::string filename, std::string folder) const;
};
}//namespace
#endif
