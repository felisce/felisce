//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//
//

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "DegreeOfFreedom/boundaryConditionList.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce 
{
BoundaryConditionList::BoundaryConditionList():
  m_numberOfBoundaryCondition(0),
  m_naturalBoundaryCondition(false),
  m_essentialBoundaryCondition(false)
{

}

/***********************************************************************************/
/***********************************************************************************/

BoundaryConditionList::~BoundaryConditionList()
{
  for (std::size_t i = 0; i < m_boundaryConditionList.size(); i++) {
    delete m_boundaryConditionList[i];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryConditionList::add(BoundaryCondition* bc) {
  
  m_boundaryConditionList.push_back(bc);
  
  // here is important to write, e.g., felisce::neumann to avoid name crash with BoundaryConditionList::neumann() the function Teo 29/01
  switch (bc->typeBC()) {
    case felisce::Dirichlet:
      m_idOfDirichletBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_essentialBoundaryCondition = true;
      break;
    case felisce::Neumann:
      m_idOfNeumannBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
    case felisce::NeumannNormal:
      m_idOfNeumannNormalBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
    case felisce::Robin:
      m_idOfRobinBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
    case felisce::RobinNormal:
      m_idOfRobinNormalBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
    case felisce::EmbedFSI:
      m_idOfEmbedFSIBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
    case felisce::EssentialLumpedModelBC:
      m_idOfEssentialLumpedModelBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_essentialBoundaryCondition = true;
      break;
    case felisce::NaturalLumpedModelBC:
      m_idOfNaturalLumpedModelBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
    case felisce::BackflowStab:
      m_idOfBackflowStabBoundaryCondition.push_back(m_numberOfBoundaryCondition);
      m_naturalBoundaryCondition = true;
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("This type of condition limit doesn't manage.");
      //       break;
  }
  m_numberOfBoundaryCondition++;
  
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryConditionList::fixUnknownIdForEachBC(const ListVariable& listVariable) {
  for ( std::size_t iBoundaryCondition = 0; iBoundaryCondition <
        m_boundaryConditionList.size(); iBoundaryCondition++) {
    for ( std::size_t iVar = 0; iVar < listVariable.size(); iVar++) {
      if (!strcmp(listVariable[iVar].name().c_str(),m_boundaryConditionList[iBoundaryCondition]->getVariable().name().c_str()))
        m_boundaryConditionList[iBoundaryCondition]->getUnknown() = static_cast<int>(iVar);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryConditionList::print(int verbose, std::ostream& outstr) const {
  if (verbose) {
    outstr << "Number of BC: " << m_numberOfBoundaryCondition << std::endl;
    outstr << "Number of Dirichlet BC: " << m_idOfDirichletBoundaryCondition.size() << std::endl;
    outstr << "Number of Neumann BC: " << m_idOfNeumannBoundaryCondition.size() << std::endl;
    outstr << "Number of Neumann Normal BC: " << m_idOfNeumannNormalBoundaryCondition.size() << std::endl;
    outstr << "Number of Robin BC: " << m_idOfRobinBoundaryCondition.size() << std::endl;
    outstr << "Number of RobinNormal BC: " << m_idOfRobinNormalBoundaryCondition.size() << std::endl;
    outstr << "Number of EmbedFSI BC: " << m_idOfEmbedFSIBoundaryCondition.size() << std::endl;
    outstr << "Number of Essential LumpedModelBC: " << m_idOfEssentialLumpedModelBoundaryCondition.size() << std::endl;
    outstr << "Number of Natural LumpedModelBC: " << m_idOfNaturalLumpedModelBoundaryCondition.size() << std::endl;
    outstr << "Number of Backflow stabilization: " << m_idOfBackflowStabBoundaryCondition.size() << std::endl;
    for ( std::size_t iBoundaryCondition = 0; iBoundaryCondition <
          m_boundaryConditionList.size(); iBoundaryCondition++)
      m_boundaryConditionList[iBoundaryCondition]->print(verbose);
    outstr << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

std::size_t BoundaryConditionList::size() {
  return m_boundaryConditionList.size();
}
}
