//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J. Foulon
//

/*!
 \file    variable.hpp
 \author  J. Foulon
 \date    07/10/2010
 \brief   File where is define an physical Felisce's variable
 */

#ifndef VARIABLE_H
#define VARIABLE_H

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/quadratureRule.hpp"

namespace felisce {

  class Variable {
  
  private:
    bool m_boolInitialize;
    /// name of the variable ( Velocity, Pressure, Temperature, potExtraCell, potTransMemb ...)
    std::string m_name;
    /// PhysicalVariable is an enum (velocity, pressure, etc.) defined in Core/felisce.hpp
    PhysicalVariable m_physicalVariable;
    /// number of components ( scalar, vectorial variable)
    std::size_t m_numComponent;
    /// id of the mesh associated to this variable
    std::size_t m_idMesh;
    /// type of element use with this variable (linear = 0, quadratic = 1 (P1/P2, Q1/Q2, ...) )
    int m_finiteElementType;
    /// degree of exactness of the integration formula
    std::vector<DegreeOfExactness> m_degreeOfExactness = std::vector<DegreeOfExactness>(1);
  
  public:
    // Constructor
    //============
    Variable();

    /*!
    \brief Method initialize read data file and create variable number iVar.
    \param[in] iVar number of the variable
    */
    void initialize(const std::size_t iVar, std::size_t instanceIndex=0);
    
    // Printer
    //========
    void print(int verbose = 0, std::ostream& outstr = std::cout) const;

    //Set functions
    //=============
    void setNumComponent(const std::size_t nComp);
    void setMeshId(const std::size_t meshId);
    
    // Access Functions
    //=================
    inline std::string name() const {
      return m_name;
    }

    inline const PhysicalVariable & physicalVariable() const {
      return m_physicalVariable;
    }

    inline const std::size_t& numComponent() const {
      return m_numComponent;
    }

    inline const std::size_t& idMesh() const {
      return m_idMesh;
    }
   
    inline const int& finiteElementType() const {
      return m_finiteElementType;
    }

    inline const std::vector<DegreeOfExactness>& getDegreeOfExactness() const {
      return m_degreeOfExactness;
    }

    inline const DegreeOfExactness& degreeOfExactness(const std::size_t Index = 0) const {
      return m_degreeOfExactness[Index];
    }
  };
}

#endif
