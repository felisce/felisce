//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/listVariable.hpp"

namespace felisce {

  ListVariable::ListVariable()
  = default;  

  void ListVariable::addVariable( const PhysicalVariable& variableName, std::size_t nComp, std::size_t instanceIndex) {
    Variable v;
    auto& r_instance = FelisceParam::instance(instanceIndex);

    // number of variables = size of nameVariable vector.
    for (std::size_t iVar = 0; iVar < r_instance.nameVariable.size(); iVar++ ) 
      if ( variableName == r_instance.physicalVariable[iVar] ) {
        v.initialize(iVar, instanceIndex);
        v.setNumComponent(nComp);
        v.setMeshId(r_instance.idMesh[iVar]);
        m_listVariable.push_back(v);
        m_listIdUnknownOfVariable.push_back(-1); 
      }
  }
  

  void ListVariable::addVariable( const Variable& var) {
    m_listVariable.push_back(var);
  }

  //! Print function
  void ListVariable::print(int verbose, std::ostream& outstr) const {
    if (verbose > 0 ) {
      outstr << "\n/======list of variables========/\n";
      for ( unsigned int iVar = 0; iVar < m_listVariable.size(); iVar++)
        m_listVariable[iVar].print(verbose,outstr);
      outstr << std::endl;
    }
  }

  /*!
   \brief Find the position of one variable in the vector with its name.
   Useful to generalize Felisce Library utilisation.
   \param[in] variable Variable considered
   */
  int ListVariable::getVariableIdList( PhysicalVariable variable ) const {
    for ( unsigned int iVar = 0; iVar < m_listVariable.size(); iVar++)
      if (m_listVariable[iVar].physicalVariable() == variable)
        return static_cast<int>(iVar);

    return -1;
  }

  /*!
   \brief Find the position of one variable in the vector with its std::string name.
   Useful to generalize Felisce Library utilisation.
   \param[in] nameVariable name of the variable research
   */
  int ListVariable::getVariableIdList( std::string nameVariable ) const {
    for ( unsigned int iVar = 0; iVar < m_listVariable.size(); iVar++)
      if ( !strcmp(m_listVariable[iVar].name().c_str(), nameVariable.c_str()))
        return static_cast<int>(iVar);
  
    return -1;
  }

  /*!
   \brief Find one variable in the vector with its name.
   Useful to generalize Felisce Library utilisation.
   \param[in] variable Variable considered
   */
  Variable* ListVariable::getVariable( PhysicalVariable variable ) {
    int id = getVariableIdList(variable);
    return &m_listVariable[id];
  }

  /*!
   \brief Find one variable in the vector with its std::string name.
   Useful to generalize Felisce Library utilisation.
   \param[in] nameVariable name of the variable research
   */
  Variable* ListVariable::getVariable( std::string nameVariable ) {
    int id = getVariableIdList(nameVariable);
    return &m_listVariable[id];
  }
}
