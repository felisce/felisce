//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//
//

#ifndef _BOUNDARYCONDITIONLIST_HPP
#define _BOUNDARYCONDITIONLIST_HPP

// System includes
#include <vector>
#include <ostream>

// External includes

// Project includes
#include "DegreeOfFreedom/listVariable.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class BoundaryCondition;

class BoundaryConditionList 
{
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /// Defaut constructor
  BoundaryConditionList();

  /// Default destructor
  ~BoundaryConditionList();

  ///@}
  ///@name Operators
  ///@{

  inline BoundaryCondition* operator[](const int i) {
    return m_boundaryConditionList[i];
  }
  
  ///@}
  ///@name Operations
  ///@{

  void add(BoundaryCondition* bc);

  std::size_t size();

  void fixUnknownIdForEachBC(const ListVariable& listVariable);

  // two functions to, temporary, set to zero the constant values
  inline void temporarySetBCValuesToZero() {
    m_tmpValues = FelisceParam::instance().value;
    FelisceParam::instance().value = std::vector<double>(m_tmpValues.size(),0.0);
  }
  inline void restoreBCValues() {
    FelisceParam::instance().value = m_tmpValues;
  };

  ///@}
  ///@name Access
  ///@{

  inline std::size_t numDirichletBoundaryCondition() const {
    return m_idOfDirichletBoundaryCondition.size();
  }

  inline std::size_t numNeumannBoundaryCondition() const {
    return m_idOfNeumannBoundaryCondition.size();
  }

  inline std::size_t numNeumannNormalBoundaryCondition() const {
    return m_idOfNeumannNormalBoundaryCondition.size();
  }

  inline std::size_t numRobinBoundaryCondition() const {
    return m_idOfRobinBoundaryCondition.size();
  }

  inline std::size_t numRobinNormalBoundaryCondition() const {
    return m_idOfRobinNormalBoundaryCondition.size();
  }

  inline std::size_t numEmbedFSIBoundaryCondition() const {
    return m_idOfEmbedFSIBoundaryCondition.size();
  }

  inline std::size_t numEssentialLumpedModelBoundaryCondition() const {
    return m_idOfEssentialLumpedModelBoundaryCondition.size();
  }

  inline std::size_t numNaturalLumpedModelBoundaryCondition() const {
    return m_idOfNaturalLumpedModelBoundaryCondition.size();
  }

  inline std::size_t numBackflowStabBoundaryCondition() const {
    return m_idOfBackflowStabBoundaryCondition.size();
  }

  inline int idBCOfDirichlet(const int i) const {
    return m_idOfDirichletBoundaryCondition[i];
  }

  inline int idBCOfNeumann(const int i) const {
    return m_idOfNeumannBoundaryCondition[i];
  }

  inline int idBCOfNeumannNormal(const int i) const {
    return m_idOfNeumannNormalBoundaryCondition[i];
  }

  inline int idBCOfRobin(const int i) const {
    return m_idOfRobinBoundaryCondition[i];
  }

  inline int idBCOfRobinNormal(const int i) const {
    return m_idOfRobinNormalBoundaryCondition[i];
  }

  inline int idBCOfEmbedFSI(const int i) const {
    return m_idOfEmbedFSIBoundaryCondition[i];
  }

  inline int idBCOfEssentialLumpedModel(const int i) const {
    return m_idOfEssentialLumpedModelBoundaryCondition[i];
  }

  inline int idBCOfNaturalLumpedModel(const int i) const {
    return m_idOfNaturalLumpedModelBoundaryCondition[i];
  }

  inline int idBCOfBackflowStab(const int i) const {
    return m_idOfBackflowStabBoundaryCondition[i];
  }

  inline const BoundaryCondition* Dirichlet(const int i) const {
    return m_boundaryConditionList[m_idOfDirichletBoundaryCondition[i]];
  }
  inline BoundaryCondition* Dirichlet(const int i) {
    return m_boundaryConditionList[m_idOfDirichletBoundaryCondition[i]];
  }

  inline const BoundaryCondition* Neumann(const int i) const {
    return m_boundaryConditionList[m_idOfNeumannBoundaryCondition[i]];
  }
  inline BoundaryCondition* Neumann(const int i) {
    return m_boundaryConditionList[m_idOfNeumannBoundaryCondition[i]];
  }

  inline const BoundaryCondition* NeumannNormal(const int i) const {
    return m_boundaryConditionList[m_idOfNeumannNormalBoundaryCondition[i]];
  }
  inline BoundaryCondition* NeumannNormal(const int i) {
    return m_boundaryConditionList[m_idOfNeumannNormalBoundaryCondition[i]];
  }

  inline const BoundaryCondition* Robin(const int i) const {
    return m_boundaryConditionList[m_idOfRobinBoundaryCondition[i]];
  }
  inline BoundaryCondition* Robin(const int i) {
    return m_boundaryConditionList[m_idOfRobinBoundaryCondition[i]];
  }

  inline const BoundaryCondition* RobinNormal(const int i) const {
    return m_boundaryConditionList[m_idOfRobinNormalBoundaryCondition[i]];
  }
  inline BoundaryCondition* RobinNormal(const int i) {
    return m_boundaryConditionList[m_idOfRobinNormalBoundaryCondition[i]];
  }

  inline const BoundaryCondition* EmbedFSI(const int i) const {
    return m_boundaryConditionList[m_idOfEmbedFSIBoundaryCondition[i]];
  }
  inline BoundaryCondition* EmbedFSI(const int i) {
    return m_boundaryConditionList[m_idOfEmbedFSIBoundaryCondition[i]];
  }

  inline const BoundaryCondition* EssentialLumpedModelBC(const int i) const {
    return m_boundaryConditionList[m_idOfEssentialLumpedModelBoundaryCondition[i]];
  }
  inline BoundaryCondition* EssentialLumpedModelBC(const int i) {
    return m_boundaryConditionList[m_idOfEssentialLumpedModelBoundaryCondition[i]];
  }

  inline const BoundaryCondition* NaturalLumpedModelBC(const int i) const {
    return m_boundaryConditionList[m_idOfNaturalLumpedModelBoundaryCondition[i]];
  }
  inline BoundaryCondition* NaturalLumpedModelBC(const int i) {
    return m_boundaryConditionList[m_idOfNaturalLumpedModelBoundaryCondition[i]];
  }

  inline const BoundaryCondition* BackflowStabBC(const int i) const {
    return m_boundaryConditionList[m_idOfBackflowStabBoundaryCondition[i]];
  }
  
  inline BoundaryCondition* BackflowStabBC(const int i) {
    return m_boundaryConditionList[m_idOfBackflowStabBoundaryCondition[i]];
  }
  
  inline int startIndiceOfValue(const int i) const {
    return m_startIndiceOfValue[i];
  }
  inline std::vector<int>& startIndiceOfValue() {
    return m_startIndiceOfValue;
  }

  inline bool NaturalBoundaryCondition() const {
    return m_naturalBoundaryCondition;
  }
  inline bool NaturalBoundaryCondition() {
    return m_naturalBoundaryCondition;
  }

  inline bool EssentialBoundaryCondition() const {
    return m_essentialBoundaryCondition;
  }
  inline bool EssentialBoundaryCondition() {
    return m_essentialBoundaryCondition;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose = 0, std::ostream& outstr = std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  std::vector<BoundaryCondition*> m_boundaryConditionList;
  std::vector<int> m_idOfDirichletBoundaryCondition;
  std::vector<int> m_idOfNeumannBoundaryCondition;
  std::vector<int> m_idOfNeumannNormalBoundaryCondition;
  std::vector<int> m_idOfRobinBoundaryCondition;
  std::vector<int> m_idOfRobinNormalBoundaryCondition;
  std::vector<int> m_idOfEmbedFSIBoundaryCondition;
  std::vector<int> m_idOfEssentialLumpedModelBoundaryCondition;
  std::vector<int> m_idOfNaturalLumpedModelBoundaryCondition;
  std::vector<int> m_idOfBackflowStabBoundaryCondition;

  std::vector<int> m_startIndiceOfValue;
  int m_numberOfBoundaryCondition;
  bool m_naturalBoundaryCondition;
  bool m_essentialBoundaryCondition;

  std::vector<double> m_tmpValues;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif
