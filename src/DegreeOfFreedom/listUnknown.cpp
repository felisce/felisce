//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/listUnknown.hpp"

namespace felisce 
{
ListUnknown::ListUnknown() 
{
  m_sizeOfMask = 0;
}

/***********************************************************************************/
/***********************************************************************************/

void ListUnknown::push_back(PhysicalVariable physicalVariable) 
{
  m_listUnknown.push_back(physicalVariable);
  m_listIdVarOfUnknown.resize(m_listUnknown.size(),-1);
}

/***********************************************************************************/
/***********************************************************************************/

void ListUnknown::setDefaultMask(const ListVariable& listVariable)
{
  int idVar;
  const Variable* v;

  m_iBlockOfUnknown.push_back(0);
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++ ) {
    idVar = listVariable.getVariableIdList(m_listUnknown[iUnknown]);
    m_listIdVarOfUnknown.push_back(idVar);
    v = &listVariable[idVar];
    m_sizeOfMask += v->numComponent();

    m_iBlockOfUnknown.push_back(v->numComponent()+m_iBlockOfUnknown[iUnknown]);

    m_listUnknownsRows.push_back(iUnknown);
    m_listUnknownsCols.push_back(iUnknown);
  }

  m_listUnknownsRowsDefault = m_listUnknownsRows;
  m_listUnknownsColsDefault = m_listUnknownsCols;

  // resizing of the mask
  m_mask.resize(m_sizeOfMask, m_sizeOfMask);

  // default values in the mask
  for ( int i = 0; i < m_sizeOfMask; i++)
    for ( int j = 0; j < m_sizeOfMask; j++)
      m_mask(i,j) = 1;
}

/***********************************************************************************/
/***********************************************************************************/

void ListUnknown::setMask( const std::vector<int> & array ) 
{
  for ( int i = 0; i < m_sizeOfMask; i++)
      for ( int j = 0; j < m_sizeOfMask; j++)
        m_mask(i,j) = array[ i*m_sizeOfMask + j ];
}

/***********************************************************************************/
/***********************************************************************************/

int ListUnknown::getUnknownIdList( PhysicalVariable unknown ) 
{
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++)
    if ( m_listUnknown[iUnknown] == unknown )
      return iUnknown;

  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

int ListUnknown::getUnknownIdList( PhysicalVariable unknown ) const
{
  for ( std::size_t iUnknown = 0; iUnknown < m_listUnknown.size(); iUnknown++)
    if ( m_listUnknown[iUnknown] == unknown )
      return iUnknown;

  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

void ListUnknown::print(int verbose, std::ostream& outstr) const 
{
  if (verbose > 1 ) {
    outstr << "/====Mask (define connection on unknown. Use to compute matrix skeleton.)======/\n";
    for ( int i = 0; i < m_sizeOfMask; i++) {
      for ( int j = 0; j < m_sizeOfMask; j++)
        outstr << m_mask(i,j) << " ";
      outstr << std::endl;
    }
  }
}
}

