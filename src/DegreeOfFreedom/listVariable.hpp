//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

#ifndef LISTVARIABLE_H
#define LISTVARIABLE_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "DegreeOfFreedom/variable.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  /*!
   \class ListVariable
   \author J. Foulon
   \date 07/10/2010
   \brief Class containing a list of variables in the problem.
   These variables are strore with an STL std::vector.
   */

  class ListVariable {
    
  private:
    //! vector STL which contains Felisce's variables.
    std::vector<Variable> m_listVariable;
    // Contains Idunknown associate to the variable (== -1 if no unknown associate).
    std::vector<int> m_listIdUnknownOfVariable;

  public:
    //Constructor
    //===========
    ListVariable();

    //Set functions
    //=============
    void addVariable( const PhysicalVariable& variableName, std::size_t nComp, std::size_t instanceIndex=0);
    void addVariable( const Variable& var);
    void print(int verbose = 0, std::ostream& outstr = std::cout) const;
    int getVariableIdList( PhysicalVariable variable ) const;
    int getVariableIdList( std::string nameVariable ) const;
    Variable* getVariable( PhysicalVariable variable );
    Variable* getVariable( std::string nameVariable );
    std::size_t size() const {
      return m_listVariable.size();
    }

    //Access Functions
    //================
    inline const std::vector<Variable> & listVariable() const {
      return m_listVariable;
    }
    inline std::vector<Variable> & listVariable() {
      return m_listVariable;
    }

    inline const int & listIdUnknownOfVariable(int i) const {
      return m_listIdUnknownOfVariable[i];
    }
    inline int & listIdUnknownOfVariable(int i) {
      return m_listIdUnknownOfVariable[i];
    }

    //Operators
    //=========
    Variable& operator[](int i) {
      return m_listVariable[i];
    }
    const Variable& operator[](int i) const {
      return m_listVariable[i];
    }
  };
}
#endif
