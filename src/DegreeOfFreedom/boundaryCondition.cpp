//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau
//

// System includes
#include <cstring>
#include <iostream>

// External includes

// Project includes
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{
BoundaryCondition::BoundaryCondition(
  const TypeOfBoundaryCondition& typeBoundaryCondition,
  const TypeValueOfBoundaryCondition& typeValueBoundaryCondition,
  const int label,
  const Variable& variable,
  const Component& component
  ):
  m_typeBC(typeBoundaryCondition),
  m_typeValueBC(typeValueBoundaryCondition),
  m_nameLabel("none"),
  m_variable(variable),
  m_idUnknown(-1),
  m_component(component) 
{
  determineComponent();
  m_listLabel.insert(label);
}

/***********************************************************************************/
/***********************************************************************************/

BoundaryCondition::BoundaryCondition(
  const TypeOfBoundaryCondition& typeBoundaryCondition,
  const TypeValueOfBoundaryCondition& typeValueBoundaryCondition,
  const std::string& aNameLabel,
  const Variable & variable,
  const Component& component
  ):
  m_typeBC(typeBoundaryCondition),
  m_typeValueBC(typeValueBoundaryCondition),
  m_nameLabel(aNameLabel),
  m_variable(variable),
  m_idUnknown(-1),
  m_component(component) 
{
  determineComponent();
}

/***********************************************************************************/
/***********************************************************************************/

BoundaryCondition::BoundaryCondition(
  const std::string& typeBoundaryCondition,
  const std::string& typeValueBoundaryCondition,
  const int label,
  const Variable& variable,
  const std::string& component
  ):
  m_nameLabel("none"),
  m_variable(variable),
  m_idUnknown(-1) 
{
  initMap();

  try {
    m_typeBC = nameTypeBCToEnumTypeBC.at(typeBoundaryCondition);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition type '"+typeBoundaryCondition+"'");
  }

  try {
    m_typeValueBC = nameValueTypeBCToEnumValueTypeBC.at(typeValueBoundaryCondition);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition value type '"+typeValueBoundaryCondition+"'");
  }

  try {
    m_component = nameComponentBCToEnumComponentBC.at(component);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition component '"+component+"'");
  }
  determineComponent();
  m_listLabel.insert(label);
}

/***********************************************************************************/
/***********************************************************************************/

BoundaryCondition::BoundaryCondition(
  const std::string& typeBoundaryCondition,
  const std::string& typeValueBoundaryCondition,
  const std::vector<int>& label,
  const Variable& variable,
  const std::string& component
  ):
  m_nameLabel("none"),
  m_variable(variable),
  m_idUnknown(-1) 
{
  initMap();

  try {
    m_typeBC = nameTypeBCToEnumTypeBC.at(typeBoundaryCondition);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition type '"+typeBoundaryCondition+"'");
  }

  try {
    m_typeValueBC = nameValueTypeBCToEnumValueTypeBC.at(typeValueBoundaryCondition);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition value type '"+typeValueBoundaryCondition+"'");
  }

  try {
    m_component = nameComponentBCToEnumComponentBC.at(component);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition component '"+component+"'");
  }
  determineComponent();
  for (unsigned int i = 0; i < label.size(); i++)
    m_listLabel.insert(label[i]);
}

/***********************************************************************************/
/***********************************************************************************/

BoundaryCondition::BoundaryCondition(
  const std::string& typeBoundaryCondition,
  const std::string& typeValueBoundaryCondition,
  const std::string& aNameLabel,
  const Variable& variable,
  const std::string& component
  ):
  m_nameLabel(aNameLabel),
  m_variable(variable),
  m_idUnknown(-1) 
{
  initMap();

  try {
    m_typeBC = nameTypeBCToEnumTypeBC.at(typeBoundaryCondition);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition type '"+typeBoundaryCondition+"'");
  }

  try {
    m_typeValueBC = nameValueTypeBCToEnumValueTypeBC.at(typeValueBoundaryCondition);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition value type '"+typeValueBoundaryCondition+"'");
  }

  try {
    m_component = nameComponentBCToEnumComponentBC.at(component);
  } catch (const std::out_of_range& ) {
    FEL_ERROR("Unknown boundary condition component '"+component+"'");
  }
  determineComponent();
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryCondition::initMap() 
{
  nameTypeBCToEnumTypeBC["Dirichlet"] = Dirichlet;
  nameTypeBCToEnumTypeBC["Neumann"] = Neumann;
  nameTypeBCToEnumTypeBC["NeumannNormal"] = NeumannNormal;
  nameTypeBCToEnumTypeBC["Robin"] = Robin;
  nameTypeBCToEnumTypeBC["RobinNormal"] = RobinNormal;
  nameTypeBCToEnumTypeBC["EmbedFSI"] = EmbedFSI;
  nameTypeBCToEnumTypeBC["BackflowStab"] = BackflowStab;

  nameValueTypeBCToEnumValueTypeBC["Constant"] = Constant;
  nameValueTypeBCToEnumValueTypeBC["Vector"] = Vector;
  nameValueTypeBCToEnumValueTypeBC["FunctionS"] = FunctionS;
  nameValueTypeBCToEnumValueTypeBC["FunctionT"] = FunctionT;
  nameValueTypeBCToEnumValueTypeBC["FunctionTS"] = FunctionTS;
  nameValueTypeBCToEnumValueTypeBC["EnsightFile"] = EnsightFile;

  nameComponentBCToEnumComponentBC["CompNA"] = CompNA;
  nameComponentBCToEnumComponentBC["Comp1"] = Comp1;
  nameComponentBCToEnumComponentBC["Comp2"] = Comp2;
  nameComponentBCToEnumComponentBC["Comp3"] = Comp3;
  nameComponentBCToEnumComponentBC["Comp12"] = Comp12;
  nameComponentBCToEnumComponentBC["Comp13"] = Comp13;
  nameComponentBCToEnumComponentBC["Comp23"] = Comp23;
  nameComponentBCToEnumComponentBC["Comp123"] = Comp123;
}

/***********************************************************************************/
/***********************************************************************************/

int BoundaryCondition::determineNumComponent(const std::string& cp) 
{
  std::unordered_map<std::string,Component> tmpMap = {
    std::make_pair("CompNA",  CompNA ),
    std::make_pair("Comp1",   Comp1  ),
    std::make_pair("Comp2",   Comp2  ),
    std::make_pair("Comp3",   Comp3  ),
    std::make_pair("Comp12",  Comp12 ),
    std::make_pair("Comp13",  Comp13 ),
    std::make_pair("Comp23",  Comp23 ),
    std::make_pair("Comp123", Comp123)
  };

  switch(tmpMap[cp]) {
    case CompNA:
    case Comp1:
    case Comp2:
    case Comp3:
      return 1;
    case Comp12:
    case Comp13:
    case Comp23:
      return 2;
    case Comp123:
      return 3;
  }
  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryCondition::determineComponent() {
  switch(m_component) {
    case CompNA:
      m_numComp.insert(0);
      break;
    case Comp1:
      m_numComp.insert(0);
      break;
    case Comp2:
      m_numComp.insert(1);
      break;
    case Comp3:
      m_numComp.insert(2);
      break;
    case Comp12:
      m_numComp.insert(0);
      m_numComp.insert(1);
      break;
    case Comp13:
      m_numComp.insert(0);
      m_numComp.insert(2);
      break;
    case Comp23:
      m_numComp.insert(1);
      m_numComp.insert(2);
      break;
    case Comp123:
      m_numComp.insert(0);
      m_numComp.insert(1);
      m_numComp.insert(2);
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("problem with components.");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryCondition::setValue(const std::vector<double>& value)
{
  std::size_t size_BC = 0;
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++) {
    for(auto it_comp = m_numComp.begin(); it_comp != m_numComp.end(); it_comp++) {
      ++size_BC;
    }
  }

  m_valueBCInSupportDof.resize(size_BC);
  int cptComp = 0;
  std::size_t index = 0;
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++) {
    cptComp =0;
    for(auto it_comp = m_numComp.begin(); it_comp != m_numComp.end(); it_comp++) {
      m_valueBCInSupportDof[index] = value[cptComp];
      ++cptComp;
      ++index;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void BoundaryCondition::print(int verbose, std::ostream& outstr) const {
  if (verbose) {
    // \todo Refactor very clumsy code below (if and switch kind of redundant)
    if (m_typeBC==EssentialLumpedModelBC || m_typeBC==NaturalLumpedModelBC) {
      switch (m_typeBC) {
        case EssentialLumpedModelBC:
          outstr <<"BC with type: Essential LumpedModelBC for labels: ( ";
          break;
        case NaturalLumpedModelBC:
          outstr <<"BC with type: Natural LumpedModelBC for labels: ( ";
          break;
        case Dirichlet:
        case Neumann:
        case NeumannNormal:
        case Robin:
        case RobinNormal:
        case EmbedFSI:
        case BackflowStab:
          break;
      }
      for(auto it_labelNumber = m_listLabel.begin();
            it_labelNumber != m_listLabel.end(); it_labelNumber++) {
        outstr << *it_labelNumber << " ";
      }
      outstr << ") associate to variable <" << m_variable.name() << "> " << std::endl;
    } else {
      outstr <<"BC with type: " ;
      switch (m_typeBC) {
        case Dirichlet:
          std::cout << "Dirichlet" ;
          break;
        case Neumann:
          std::cout << "Neumann" ;
          break;
        case NeumannNormal:
          std::cout << "Neumann Normal" ;
          break;
        case Robin:
          std::cout << "Robin" ;
          break;
        case RobinNormal:
          std::cout << "RobinNormal" ;
          break;
          case EmbedFSI:
            std::cout << "EmbedFSI" ;
            break;
          case BackflowStab:
            std::cout << "BackflowStab" ;
            break;
        case EssentialLumpedModelBC:
        case NaturalLumpedModelBC:
          break;
      }
      std::cout << " for labels: ( ";
      for(auto it_labelNumber = m_listLabel.begin();
            it_labelNumber != m_listLabel.end(); it_labelNumber++) {
        outstr << *it_labelNumber << " ";
      }
      outstr << ") associate to variable <" << m_variable.name() << "> and with unknown number: "<< m_idUnknown << " on components: ";

      switch(m_component) {
        case Comp1:
          outstr << "x";
          break;
        case Comp2:
          outstr << "y";
          break;
        case Comp3:
          outstr << "z";
          break;
        case Comp12:
          outstr << "x, y";
          break;
        case Comp13:
          outstr << "x, z";
          break;
        case Comp23:
          outstr << "y, z";
          break;
        case Comp123:
          outstr << "x, y, z";
          break;
        case CompNA:
          outstr << "NA";
          break;
          // Default case should appear with a warning at compile time instead of an error in runtime
          // (that's truly the point of using enums as switch cases)
          //  default:
          //            FEL_ERROR("problem with components.");
      }
      outstr << " with value Type: " ;
      switch (m_typeValueBC) {
        case Constant:
          std::cout << "Constant" << std::endl;
          break;
        case Vector:
          std::cout << "Vector" << std::endl;
          break;
        case FunctionS:
          std::cout << "FunctionS" << std::endl;
          break;
        case FunctionT:
          std::cout << "FunctionT" << std::endl;
          break;
        case FunctionTS:
          std::cout << "FunctionTS" << std::endl;
          break;
        case EnsightFile:
          std::cout << "EnsightFile" << std::endl;
          break;
      }
    }
  }
}

}
