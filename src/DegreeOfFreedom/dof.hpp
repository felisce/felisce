//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M.A. Fernandez & J.Foulon
//

/*!
 \file    dof.hpp
 \authors M.A. Fernandez & J.Foulon
 \date    07/10/2010
 \brief   File where is define the class to manage degrees of freedom (dof) of the mathematical problem.
 */

#ifndef DOF_HPP
#define DOF_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "DegreeOfFreedom/csrmatrixpattern.hpp"
#include "DegreeOfFreedom/listUnknown.hpp"
#include "DegreeOfFreedom/listVariable.hpp"
#include "DegreeOfFreedom/supportDofMesh.hpp"
#include "FiniteElement/currentFiniteElement.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce {
  /*!
   \class Dof
   \authors M.A. Fernandez & J.Foulon
   \date 07/10/2010
   \brief Manage degrees of freedom of the finite element problem.
   */
  class Dof {
  public:
    // Constructor
    // ============
    Dof();
    void setDof(const ListUnknown& unknown, const ListVariable& variable,
                const std::vector<SupportDofMesh*>& pSupportDofUnknown);
    
    void loc2glob(felInt iEle, int iLocDof, int iVar, std::size_t iComp, felInt& numGlobalDof) const;
    
    void loc2glob(felInt iEle, int numLocDof, int iVar, std::size_t numComp, std::vector<felInt>& numGlobalDof) const;
    
    void loc2glob(felInt iEle, std::vector<int>& iLocDof, int iVar, std::vector<std::size_t>& iComp, std::vector<felInt>& numGlobalDof) const;
    
    void supportDofToDof(felInt idSupportDof, std::vector<felInt>& idDof) const;

    void identifyDofBySupport(felInt iUnknown, std::vector<felInt>& listSuppDof, std::vector<felInt>& listDofAssociated) const;

    int getNumGlobComp(std::size_t iUnknown, int iComp) const ;
    // void buildPattern();
    void initializePattern(int sizeProc, int rankProc);
    void buildPattern(int rankProc,const felInt* dofRepartition);
    void print(int verbose = 0, std::ostream& outstr = std::cout) const;
    void mergeGlobalPattern(const std::vector<felInt>& iCSRc_new, const std::vector<felInt>& jCSRc_new);
    void mergeLocalCompPattern(MPI_Comm mpiComm, felInt numProc, felInt rankProc, const std::vector<felInt>& dofRepartition);

    // Access Function
    //================

    inline void clearPattern() {
      m_pattern.clear();
    }

    inline void clearPatternC() {
      m_patternC.clear();
      m_patternIsChanged = false;
    }

    inline void resizePattern(std::size_t numRows,std::size_t numNonzeros) {
      m_pattern.resize(numRows,numNonzeros);
    }

    inline CSRMatrixPattern const & pattern() const {
      return m_pattern;
    }

    inline CSRMatrixPattern & pattern() {
      return m_pattern;
    }

    inline CSRMatrixPattern const & patternC() const {
      return m_patternC;
    }

    inline CSRMatrixPattern & patternC() {
      return m_patternC;
    }

    inline const ListUnknown & listUnknown() const {
      return m_unknown;
    }
    inline const ListVariable & listVariable() const {
      return m_variable;
    }

    // inline const std::vector<SupportDofMesh>& supportDofUnknown() const {
    //   return m_supportDofUnknown;
    // }

    inline const  felInt & numDof() const {
      return m_numDof;
    }
    inline  felInt & numDof() {
      return m_numDof;
    }

    inline const  std::vector<felInt> & numDofPerUnknown() const {
      return m_numDofPerUnknown;
    }
    inline  std::vector<felInt> & numDofPerUnknown() {
      return m_numDofPerUnknown;
    }
    inline bool patternIsChanged() {
      return m_patternIsChanged;
    }
    void getNeighbourhood( felInt node, std::vector<felInt> & neighbourhood ) const;
  private:
    //! list of unknowns associated to the problem.
    ListUnknown m_unknown;
    //! list of variables associated to the problem.
    ListVariable m_variable;
    //! all support of dof object for every variable.
    std::vector<SupportDofMesh*> m_pSupportDofUnknown;

    // matrix pattern (CSR format)
    CSRMatrixPattern m_pattern;

    // matrix complementary pattern
    CSRMatrixPattern m_patternC;

    //! indicate the number of dof
    felInt m_numDof;
    //! indicate the number of dof by variable for the problem.
    std::vector<felInt> m_numDofPerUnknown;
    //! tells if pattern is changed due to merge operation
    bool m_patternIsChanged;
  };
}

#endif

