//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon and others
//

/*!
 \file supportDofMesh->cpp
 \author J. Foulon V. Martin and others
 \date 07/10/2010
 \brief File contains SupportDofMesh implementation
 */

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/supportDofMesh.hpp"
#include "FiniteElement/refElement.hpp"
#include "FiniteElement/curvilinearFiniteElement.hpp"
#include "FiniteElement/currentFiniteElement.hpp"

namespace felisce
{
  SupportDofMesh::StringToReferenceElement SupportDofMesh::m_eltRefNameToRefEle;

  void SupportDofMesh::m_initMap() 
  {
    m_eltRefNameToRefEle[ "refElementNode"  ]            = & refElementNode;
    m_eltRefNameToRefEle[ "refElementSegmentP1"  ]       = & refElementSegmentP1;
    m_eltRefNameToRefEle[ "refElementSegmentP1b"  ]      = & refElementSegmentP1b;
    m_eltRefNameToRefEle[ "refElementSegmentP2"  ]       = & refElementSegmentP2;
    m_eltRefNameToRefEle[ "refElementTriangleP1"  ]      = & refElementTriangleP1;
    m_eltRefNameToRefEle[ "refElementTriangleP1b"  ]     = & refElementTriangleP1b;
    m_eltRefNameToRefEle[ "refElementTriangleP2"  ]      = & refElementTriangleP2;
    m_eltRefNameToRefEle[ "refElementQuadrangleQ1"  ]    = & refElementQuadrangleQ1;
    m_eltRefNameToRefEle[ "refElementQuadrangleP1xP2"  ] = & refElementQuadrangleP1xP2;
    m_eltRefNameToRefEle[ "refElementQuadrangleQ1b"  ]   = & refElementQuadrangleQ1b;
    m_eltRefNameToRefEle[ "refElementQuadrangleQ2"  ]    = & refElementQuadrangleQ2;
    m_eltRefNameToRefEle[ "refElementQuadrangleQ2c"  ]   = & refElementQuadrangleQ2c;
    m_eltRefNameToRefEle[ "refElementTetrahedronP1"  ]   = & refElementTetrahedronP1;
    m_eltRefNameToRefEle[ "refElementTetrahedronP1b"  ]  = & refElementTetrahedronP1b;
    m_eltRefNameToRefEle[ "refElementTetrahedronP2"  ]   = & refElementTetrahedronP2;
    m_eltRefNameToRefEle[ "refElementHexahedronQ1"  ]    = & refElementHexahedronQ1;
    m_eltRefNameToRefEle[ "refElementHexahedronQ1b"  ]   = & refElementHexahedronQ1b;
    m_eltRefNameToRefEle[ "refElementHexahedronQ2"  ]    = & refElementHexahedronQ2;
    m_eltRefNameToRefEle[ "refElementHexahedronQ2c"  ]   = & refElementHexahedronQ2c;
    m_eltRefNameToRefEle[ "refElementPrismR1"  ]         = & refElementPrismR1;
    m_eltRefNameToRefEle[ "refElementPrismP1xP2"  ]      = & refElementPrismP1xP2;
    m_eltRefNameToRefEle[ "refElementPrismR2"  ]         = & refElementPrismR2;
    m_eltRefNameToRefEle[ "refElementTetrahedronRT0"  ]  = & refElementTetrahedronRT0;
  }

  /*!
    \brief Constructor for the global SupportDofMesh
    \param[in] variable Variable associated to this SupportDofMesh
    \param[in] mesh Reference to the mesh to etablish a connexion with finite element
    geometric and finite element reference
  */
  SupportDofMesh::SupportDofMesh(const Variable& variable, const GeometricMeshRegion::Pointer& mesh):
    m_mesh(mesh),
    m_variable(variable),
    m_copiedMeshPoint(false),
    m_numDofSupportedByEdge(0),
    m_resizedEdgeNode(false),
    m_resizedFaceNode(false),
    m_resizedVolumeNode(false),
    m_createdEmbeddedInterfaceMaps(false),
    m_createdCrackInterfaceMaps(false) 
  {
    m_initMap();
    //=============================
    // initialize the mesh/supportDof : copy points, resize m_listNode for internal edges and faces if necessary
    const std::vector<ElementType>& bagElementTypeDomain = m_mesh->bagElementTypeDomain();
    for (std::size_t ieltype = 0; ieltype < bagElementTypeDomain.size(); ++ieltype) {
      const ElementType eltType =  bagElementTypeDomain[ieltype];

      //! definition of the finite element type ( geoEle, RefEle )
      //! allow to pass from geoEleP1 to refEleP2
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);

      const int numDOFNodeVertex  =  refEle.numDOFNodeVertex();
      const int numDOFEdge        =  refEle.numDOFEdge();
      const int numDOFFace        =  refEle.numDOFFace();
      const int numDOFVolume      =  refEle.numDOFVolume();

      //! if at least one element is vertex based: copy the nodes:
      if ( numDOFNodeVertex > 0 )
        m_copyNodes();

      if ( numDOFEdge > 0) {
        if ( refEle.numDOF() == geoEle.numPoint() ) m_mesh->statusEdges() = true;

        if ( m_mesh->statusEdges() == false ) {
          std::stringstream msg;
          msg << "-->building internal edges" << std::endl;
          m_mesh->buildEdges();
          msg << "<--edges built" << std::endl;
          PetscPrintf(PETSC_COMM_WORLD,"%s",msg.str().c_str());
        }
        if (refEle.edgesCanHaveDifferentNumberOfDof())
          m_numDofSupportedByEdge = m_mesh->listEdges().countAndComputeIdEdgesSupportingDofs(refEle);
        else
          m_numDofSupportedByEdge = m_mesh->numEdges() * refEle.numDOFPerEdge();
        m_resizeEdgeNodes();
      }

      if ( numDOFFace > 0 ) {
        if ( refEle.numDOF() == geoEle.numPoint() ) m_mesh->statusFaces() = true;

        if ( m_mesh->statusFaces() == false ) {
          std::cout << "-->building internal faces" << std::endl;
          m_mesh->buildFaces();
          std::cout << "<--faces built" << std::endl;
        }
        m_resizeFaceNodes(refEle);
      }

      //! if at least one element is volume based: build the volumes
      if( numDOFVolume > 0)
        m_resizeVolumeNodes(refEle);
    }

    felInt cptElt = 0;

    //! build the edge and face nodes
    //! Domain
    m_buildNodesOfEdgeFaceVolumePerBag(m_mesh->bagElementTypeDomain(), false);

    //! Boundary (it should not be useful to build the edge/face nodes: already done via the domain!!...?)
    m_buildNodesOfEdgeFaceVolumePerBag(m_mesh->bagElementTypeDomainBoundary(), true);
    // Do it for all bags (3d, 2d, 1d??)

    // Set m_numSupportDof
    m_numSupportDof = m_listNode.size();

    if(FelisceParam::instance().FusionDof) { //For periodic boundary conditions
      m_listPerm.resize(m_listNode.size(), 0);
      for (std::size_t lp = 0; lp<m_listNode.size(); lp++ ) {
        m_listPerm[lp] = lp ;//this is just the list of node if variable not fusioned
      }
      fusionSupportDof(variable);
    }

    //! Compute the size of support vectors:
    //!   m_iSupportDof (sum of the number of Dof in each elements)
    //!   m_iEle (Total number of elements + 1)
    //!   m_vectorIdElementSupport (Total number of elements)
    m_resizeSupportVectors();

    //! compute support vectors in CSR format: m_iSupportDof, m_iEle, and compute m_vectorIdElementSupport
    m_buildSupportCSR(m_mesh->bagElementTypeDomain(), cptElt);
    m_buildSupportCSR(m_mesh->bagElementTypeDomainBoundary(), cptElt);

    // Embedded Interface
    if(FelisceParam::instance().EmbeddedInterface)
      matchEmbeddedLabelPairs(variable);

    // Cracks
    if(FelisceParam::instance().hasCrack){
       matchCrackLabelPairs(variable);
    }
  }

  /*!
    \brief Constructor of local SupportDofMesh (partitionning of SupportDofMesh in n locals SupportDofMesh).
    \param[in] variable Variable associate to this SupportDofmesh
    \param[in] meshLocal Reference to the local mesh.
    \param[in,out] loc2GlobElemSupport Local to global mapping of element/element support ids.
    \param[in] globSupportDofMesh Global SupportDofmesh
    \param[in] loc2GlobSupport Mapping on global and local numbering of Support.
    \param[in] rank Rank of the working process.
  */
  SupportDofMesh::SupportDofMesh(const Variable& variable, const GeometricMeshRegion::Pointer& meshLocal, std::vector<felInt>& loc2GlobElemSupport, SupportDofMesh& globSupportDofMesh):
    m_mesh(meshLocal),
    m_variable(variable),
    m_numDofSupportedByEdge(0),
    m_createdEmbeddedInterfaceMaps(false),
    m_createdCrackInterfaceMaps(false)
  {
    // Copy all the support dof from the global mesh
    m_listNode = globSupportDofMesh.listNode();

    // Initialize variables
    ElementType eltType;                // Element type
    std::size_t cptElt = 0;             // Number of local support element
    felInt cptSupportElem = 0;          // Count the number of support element
    felInt cptSupport = 0;              // Number of local support dof
    felInt numSupportDofElem = 0;       // Number of support dof on an element
    felInt idGlobalElementSupport = 0;  // Global id of a support element

    // Local bags of element type
    const std::vector<ElementType>& bagElementTypeDomain = m_mesh->bagElementTypeDomain();
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_mesh->bagElementTypeDomainBoundary();

    // Total number of local elements
    cptElt = loc2GlobElemSupport.size();

    // m_link between mesh element and element support
    m_vectorIdElementSupport.resize(cptElt);

    // Total number of support dof per local elements
    for(std::size_t iel=0; iel<cptElt; iel++) {
      globSupportDofMesh.getIdElementSupport(loc2GlobElemSupport[iel], m_vectorIdElementSupport[iel]);
      for(std::size_t iSup=0; iSup < m_vectorIdElementSupport[iel].size(); ++iSup) {
        cptSupport += globSupportDofMesh.getNumSupportDof(m_vectorIdElementSupport[iel][iSup]);
        ++cptSupportElem;
      }
    }

    // Allocate the arrays
    m_iEle.resize(cptSupportElem+1, 0);     // m_iEle array of the CSR format: the support elements
    m_iSupportDof.resize(cptSupport, 0);    // m_iSupportDof of the CSR format: the support dof
    loc2GlobElemSupport.resize(cptSupportElem, 0);

    // Filling of m_iEle and m_iSupportDof and m_vectorIdElementSupport arrays
    m_iEle[0] = 0;
    cptElt = 0;
    cptSupportElem = 0;

    // for each element type in the domain
    for (std::size_t i=0; i < bagElementTypeDomain.size(); ++i) {
      eltType = bagElementTypeDomain[i];

      // for each element of the current element type
      for ( felInt iel = 0; iel < m_mesh->numElements(eltType); iel++ ) {
        for( std::size_t iSup=0; iSup < m_vectorIdElementSupport[cptElt].size(); ++iSup) {
          idGlobalElementSupport = m_vectorIdElementSupport[cptElt][iSup];
          numSupportDofElem = globSupportDofMesh.getNumSupportDof(idGlobalElementSupport);

          // fill m_iEle for this element (CSR format)
          m_iEle[cptSupportElem+1] = m_iEle[cptSupportElem] + numSupportDofElem;

          // fill m_iSupportDof (add the support dof of this element) (CSR format)
          for ( int iSupport = 0; iSupport < numSupportDofElem; iSupport++)
            m_iSupportDof[m_iEle[cptSupportElem] + iSupport] = globSupportDofMesh.iSupportDof()[globSupportDofMesh.iEle()[idGlobalElementSupport] + iSupport];

          // fill m_vectorIdElementSupport
          m_vectorIdElementSupport[cptElt][iSup] = cptSupportElem;

          // fill loc2GlobElemSupport
          loc2GlobElemSupport[cptSupportElem] = idGlobalElementSupport;

          // increment the count of local support elements
          ++cptSupportElem;
        }

        // increment the count of local elements
        ++cptElt;
      }
    }

    // same for the boundary domain elements
    for (std::size_t i=0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType = bagElementTypeDomainBoundary[i];

      // for each element of the current element type
      for (felInt iel = 0; iel < m_mesh->numElements(eltType); iel++) {
        for(std::size_t iSup=0; iSup < m_vectorIdElementSupport[cptElt].size(); ++iSup) {
          idGlobalElementSupport = m_vectorIdElementSupport[cptElt][iSup];
          numSupportDofElem = globSupportDofMesh.getNumSupportDof(idGlobalElementSupport);

          // fill m_iEle for this element (CSR format)
          m_iEle[cptSupportElem+1] = m_iEle[cptSupportElem] + numSupportDofElem;

          // fill m_iSupportDof (add the support dof of this element) (CSR format)
          for ( int iSupport = 0; iSupport < numSupportDofElem; iSupport++)
            m_iSupportDof[m_iEle[cptSupportElem] + iSupport] = globSupportDofMesh.iSupportDof()[globSupportDofMesh.iEle()[idGlobalElementSupport] + iSupport];

          // fill m_vectorIdElementSupport
          m_vectorIdElementSupport[cptElt][iSup] = cptSupportElem;

          // fill loc2GlobElemSupport
          loc2GlobElemSupport[cptSupportElem] = idGlobalElementSupport;

          // increment the count of local support elements
          ++cptSupportElem;
        }

        // increment the count of local elements
        ++cptElt;
      }
    }

    // Set m_numSupportDof
    m_numSupportDof = std::set<felInt>(m_iSupportDof.begin(), m_iSupportDof.end()).size();
  }

  /*!
    \brief Resize the following vectors :
    m_iSupportDof (sum of the number of Dof in each elements),
    m_iEle (Total number of elements + 1)
  */
  void SupportDofMesh::m_resizeSupportVectors() 
  {
    felInt numDofTotal = 0;
    felInt numElemTotal = 0;
    ElementType eltType;
    const std::vector<ElementType>& bagEleDomain = m_mesh->bagElementTypeDomain();
    const std::vector<ElementType>& bagEleDomainBoundary = m_mesh->bagElementTypeDomainBoundary();

    for (std::size_t ieltype=0; ieltype < bagEleDomain.size(); ++ieltype) {
      eltType = bagEleDomain[ieltype];
      numElemTotal += m_mesh->numElements(eltType);

      //! definition of the finite element type ( geoEle, RefEle )
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);

      numDofTotal += refEle.numDOF()*m_mesh->numElements(eltType);
    }

    for (std::size_t ieltype=0; ieltype < bagEleDomainBoundary.size(); ++ieltype) {
      eltType = bagEleDomainBoundary[ieltype];
      numElemTotal += m_mesh->numElements(eltType);

      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);

      numDofTotal += refEle.numDOF()*m_mesh->numElements(eltType);
    }

    m_iSupportDof.resize(numDofTotal, 0);
    m_iEle.resize(numElemTotal+1, 0);
    m_vectorIdElementSupport.resize(numElemTotal);
  }

  /*!
   \brief make the link between the nodes of the (reference) finite element, and
               the nodes of the geometric element of the mesh.
   For instance, identify nodes between linear geometric element and quadratic treatment in the problem.
   \param[in] fe finite element reference
   \param[out] feIdLocalDof return the ID of the local Dof in the element, -1 if not linked.
               for instance if GeoEle=TriaP1 and RefEle=TriaP2: feIdLocalDof=[0,1,2,-1,-1,-1].
   */
  void SupportDofMesh::m_linkNodesGeoAndRefEle(const CurBaseFiniteElement& fe, std::vector<int>& feIdLocalDof) const 
  {
    // The vertex dof in the definition of the refElement have to be the first ones!
    int numDOFNodeVertex       = fe.refEle().numDOFNodeVertex();
    int numDOF                 = fe.refEle().numDOF();
    const int*  idSupportOfDOF = fe.refEle().idSupportOfDOF();

    feIdLocalDof.resize(numDOF, -1);

    for ( int iDOFNode = 0; iDOFNode < numDOFNodeVertex; iDOFNode++)
      feIdLocalDof[iDOFNode] = idSupportOfDOF[iDOFNode];
  }

  /*
  void SupportDofMesh::m_linkNodesGeoAndRefEle(const CurvilinearFiniteElement& fe, std::vector<int>& feIdLocalDof) const
  {
    int numDOFNode        =  fe.refEle().numDOFNode();
    int numDOF            =  fe.refEle().numDOF();

    feIdLocalDof.clear();
    feIdLocalDof.resize( numDOF, -1 );

    const int*  idSupportOfDOF = fe.refEle().idSupportOfDOF();
    double dist = 0.;

    // TODO
    // - Change the norm (use infinite or L1 norm).
    // NB fe is initialized here such that the refEle and the geoEle
    // have the same vertices (reference element).
    // -> No need to use relative norm in the comparison.
    for ( int iDOFNode = 0; iDOFNode < numDOFNode; iDOFNode++) {
      for ( int i = 0; i < fe.geoEle().numPoint(); i++) {
        dist = std::sqrt( (fe.refEle().node()[iDOFNode].x() - fe.geoEle().pointCoor(i, 0))*(fe.refEle().node()[iDOFNode].x() - fe.geoEle().pointCoor(i, 0)) +
                    (fe.refEle().node()[iDOFNode].y() - fe.geoEle().pointCoor(i, 1))*(fe.refEle().node()[iDOFNode].y() - fe.geoEle().pointCoor(i, 1)) +
                    (fe.refEle().node()[iDOFNode].z() - fe.geoEle().pointCoor(i, 2))*(fe.refEle().node()[iDOFNode].z() - fe.geoEle().pointCoor(i, 2)) );
        if ( dist < 0.0000000001 ) {
          feIdLocalDof[iDOFNode] = idSupportOfDOF[iDOFNode];
        }
      }
    }
  }
  */

  //! \brief copy the mesh points (all points in mesh) into m_listNode
  void SupportDofMesh::m_copyNodes() 
  {
    if ( m_copiedMeshPoint  ) {
      return;
    } else {
      // copy nodes only once!
      m_listNode = m_mesh->listPoints();
      m_copiedMeshPoint = true;
    }
  }

  //! \brief create edge nodes if necessary
  void SupportDofMesh::m_resizeEdgeNodes() 
  {
    if ( m_resizedEdgeNode  ) {
      return;
    } else { // resize only once!
      felInt size = m_listNode.size() + m_numDofSupportedByEdge;
      m_listNode.resize(size); // expand the std::vector
      m_resizedEdgeNode = true;
    }
  }

  //! \brief create internal faces if necessary
  void SupportDofMesh::m_resizeFaceNodes(const RefElement& refEle) 
  {
    if ( m_resizedFaceNode  ) {
      return;
    } else { // resize only once!
      int numDOFPerFace = refEle.numDOFPerFace();
      felInt size = m_listNode.size() + m_mesh->numFaces() * numDOFPerFace;
      m_listNode.resize(size); // expand the std::vector
      m_resizedFaceNode = true;
    }
  }

  //! \brief create internal volumes if necessary
  void SupportDofMesh::m_resizeVolumeNodes(const RefElement& refEle) 
  {
    if ( m_resizedVolumeNode  ) {
      return;
    } else { // resize only once!
      int numDOFPerVolume = refEle.numDOFVolume();
      felInt size = m_listNode.size() + m_mesh->getNumElement3D() * numDOFPerVolume;
      m_listNode.resize(size); // expand the std::vector
      m_resizedVolumeNode = true;
    }
  }

  //! build and add the edge and face NODES to m_listNode
  //! (typically build the edge midpoints for constructing P2/Q2 from P1/Q1 mesh).
  void SupportDofMesh::m_buildNodesOfEdgeFaceVolumePerBag(const std::vector<ElementType>& theBagElt, bool boundary_flag) 
  {
    std::vector<felInt> elem;
    std::vector<Point*> elePoint;
    std::vector<felInt> elemConnectivity;
    std::vector<int> feIdLocalDof;
    felInt countEltPerType;

    for (std::size_t ieltype = 0; ieltype < theBagElt.size(); ++ieltype) {
      ElementType eltType =  theBagElt[ieltype];
      felInt numPtsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];

      //! definition of the finite element type ( geoEle, RefEle )
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);

      CurBaseFiniteElement::Pointer fe = nullptr;

      if ( boundary_flag ) { // curvFE necessary for the boundary elements to have
        // A correct coorMap for ALL coordinates (because numCoor(Bndry) = numCoor(Domain)-1)
        fe = felisce::make_shared<CurvilinearFiniteElement>(refEle, geoEle, DegreeOfExactness_0);
      } else {
        fe = felisce::make_shared<CurrentFiniteElement>(refEle, geoEle, DegreeOfExactness_0);
      }

      const int numDOFNodeVertex  =  refEle.numDOFNodeVertex();
      const int numDOFNodeEdge    =  refEle.numDOFNodeEdge(); // numDOFNodeEdge or numDOFEdge here ??? VM
      const int numDOFNodeFace    =  refEle.numDOFNodeFace();
      const int numDOFNodeVolume  =  refEle.numDOFNodeVolume();

      // check that m_listNode is resized before inserting the nodes:
      FEL_ASSERT( m_resizedEdgeNode || numDOFNodeEdge == 0 ); //check: numDOFNodeEdge > 0 => m_resizedEdgeNode = true.
      FEL_ASSERT( m_resizedFaceNode || numDOFNodeFace == 0 );
      if ( numDOFNodeEdge > 0 && m_mesh->statusEdges() == false ) {
        FEL_ERROR("Error: you must build the Edges BEFORE the build of Edge nodes!");
      }
      if ( numDOFNodeFace > 0 && m_mesh->statusFaces() == false ) {
        FEL_ERROR("Error: you must build the Faces BEFORE the build of Face nodes!");
      }

      // Link geoEle and refElement nodes (P1->P2 for instance)
      m_linkNodesGeoAndRefEle(*fe, feIdLocalDof);

      elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0);
      elePoint.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], nullptr);
      elemConnectivity.resize(refEle.numDOF(),0);

      countEltPerType = 0;
      for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin();itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
        const felInt numElemsPerRef = itRef->second.second;

        // Build and add the edge middle points to the mesh.

        //??? Question: pourquoi ne pas travailler par liste de faces/d'aretes au lieu de passer par les elements???  VM
        for ( felInt iel = 0; iel < numElemsPerRef; iel++ ) {
          //! get the elem and its connectivity
          m_getElementSupportConnectivity(refEle, eltType, countEltPerType, feIdLocalDof, elem, elemConnectivity);
          for ( int ivert = 0; ivert < numPtsPerElt; ivert++) {
            elePoint[ivert] = &m_mesh->listPoint( elem[ivert] );
          }
          fe->update(iel, elePoint); // geometric update only

          // TODO : CHECK THAT IT WORKS FOR MULTIPLE DOF PER EDGE/FACE! VM

          //! computing edge nodes
          if ( numDOFNodeEdge > 0 ) {
            Point middleEdge;
            for ( int iDof = 0; iDof < numDOFNodeEdge; iDof++) { // numDOFNodeEdge or numDOFEdge here ??? VM
              if ( feIdLocalDof[iDof + numDOFNodeVertex] == -1 ) { //add the midpoint if not yet created

                fe->coorMap(middleEdge, refEle.node()[numDOFNodeVertex + iDof]);
                m_listNode[ elemConnectivity[numDOFNodeVertex + iDof] ] = middleEdge;
              }
            }
          }

          //! computing face nodes
          if ( numDOFNodeFace > 0 ) {
            Point middleFace;
            for ( int iDof = 0; iDof < numDOFNodeFace; iDof++) {
              if ( feIdLocalDof[iDof + numDOFNodeVertex + numDOFNodeEdge] == -1 ) {
                fe->coorMap(middleFace, refEle.node()[numDOFNodeVertex + numDOFNodeEdge + iDof]);
                m_listNode[ elemConnectivity[numDOFNodeVertex + numDOFNodeEdge + iDof] ] = middleFace;
              }
            }
          }

          //! computing volume nodes
          if ( numDOFNodeVolume > 0 ) {
            Point middleVolume;
            int shift = numDOFNodeVertex + numDOFNodeEdge + numDOFNodeFace;
            for ( int iDof = 0; iDof < numDOFNodeVolume; iDof++) {
              if ( feIdLocalDof[shift + iDof] == -1 ) {
                fe->coorMap(middleVolume, refEle.node()[shift + iDof]);
                m_listNode[ elemConnectivity[shift + iDof] ] = middleVolume;
              }
            }
          }

          countEltPerType++;
        }
      }
    }
  }

  //! return:
  //!   elem : the element.
  //!   elemConnectivity: the ID of the nodes of the element. Ex. P2: [V0, V1, V2, Mid0, Mid1, Mid2]
  void SupportDofMesh::m_getElementSupportConnectivity(const RefElement& refEle, ElementType eltType, felInt iel, const std::vector<int>& feIdLocalDof,
                                                       std::vector<felInt>& the_elem, std::vector<felInt>& elemConnectivity) const
  {
    //const int numDOFNode        =  refEle.numDOFNode();
    const int numDOFNodeVertex  = refEle.numDOFNodeVertex();
    const int numDOFNodeEdge    = refEle.numDOFNodeEdge();
    const int numDOFNodeFace    = refEle.numDOFNodeFace();
    const int numDOFNodeVolume  = refEle.numDOFNodeVolume();
    const int numDOFEdge        = refEle.numDOFEdge();
    const int numDOFFace        = refEle.numDOFFace();
    const int numDOFVolume      = refEle.numDOFVolume();
    const int numDOF            = refEle.numDOF();
    const int numDOFPerFace     = refEle.numDOFPerFace();

    const GeoElement& geoEle    = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
    const int numPoint          =  geoEle.numPoint();

    FEL_ASSERT( the_elem.size() == static_cast<std::size_t>(m_mesh->numPtsPerElement(eltType)) );
    FEL_ASSERT( elemConnectivity.size() == static_cast<std::size_t>(numDOF) );

    //! We assume that either the edge/face dofs are fully nodal, or fully non-nodal.
    FEL_ASSERT(numDOFNodeEdge == numDOFEdge || numDOFNodeEdge == 0);
    FEL_ASSERT(numDOFNodeFace == numDOFFace || numDOFNodeFace == 0);

    m_mesh->getOneElement(eltType, iel, the_elem, 0);

    //! IDs of the edges (or faces) of the element (as provided by listEdges, given edges first).
    std::vector<felInt> elem2Edge(GeometricMeshRegion::m_numEdgesPerElt[eltType], -1);
    std::vector<felInt> elem2Face(GeometricMeshRegion::m_numFacesPerElt[eltType], -1);

    int shiftLocalDOF     = 0;
    felInt shiftGlobalDOF = 0;

    //! 1/ VERTEX nodes
    if ( numDOFNodeVertex != 0 ) {
      // Is this correct for non nodal dof, such as RT0 or P0: ??? VM
      // start with vertex nodes.
      for (std::size_t iv = 0; iv < the_elem.size(); iv++)
        elemConnectivity[iv] = the_elem[iv];

      shiftLocalDOF  += numDOFNodeVertex;
      shiftGlobalDOF += m_mesh->numPoints();
    }

    //! 2/ EDGE dofs
    //! It assumes that for all elements, there is the same number of dof per edges.
    if ( numDOFEdge != 0 && numDOF != numPoint) {
      // look only at the 1rst DOFNodeEdge
      int idof = 0;

      //! if there is a node dof supported by an edge (ex: refEle=P2, built from geoEle=P1)
      //! or a non nodal dof on an edge
      if ( (numDOFNodeEdge > 0 && feIdLocalDof[idof + shiftLocalDOF] == -1) || (numDOFNodeEdge == 0) ) {
        // get all the edges of the element
        m_mesh->getAllEdgeOfElement(eltType, iel, elem2Edge);
        // elem2Edge contains the ids of the edges,
        // i.e. m_listEdge[elem2Edge[i]] returns the pointer to the i-th edge of the element

        // add the edge dofs after the vertex node dofs
        int localCount(0);
        for ( std::size_t iedg = 0; iedg < elem2Edge.size(); iedg++ ) {
          for ( int jdof = 0; jdof < refEle.numDOFPerEdge(iedg); jdof++ ) {
            if (refEle.edgesCanHaveDifferentNumberOfDof()) {
              FEL_ASSERT( refEle.numDOFPerEdge(iedg) == 1 );
              elemConnectivity[shiftLocalDOF + localCount] = shiftGlobalDOF + m_mesh->listEdges()[elem2Edge[iedg]]->idOnlySupporting() + jdof;
              localCount++;
            } else {
              elemConnectivity[shiftLocalDOF + localCount] = jdof + elem2Edge[iedg]*refEle.numDOFPerEdge() + shiftGlobalDOF;
              localCount++;
            }
          }
        }
      }
      shiftLocalDOF  += numDOFEdge;
      shiftGlobalDOF += m_mesh->listEdges().numEdgesSupportingADof();
    }

    //! 2/ FACE dofs
    //! It assumes that for all elements, there is the same number of dof per faces
    if ( numDOFFace != 0 && numDOF != numPoint) {
      // look only at the 1rst DOFNodeFace
      int idof = 0;

      //! If there is a node dof supported by a face or a non nodal dof on a face
      if ( (numDOFNodeFace > 0 && feIdLocalDof[idof + shiftLocalDOF] == -1) || (numDOFNodeFace == 0) ) {
        // get all the faces of the element
        m_mesh->getAllFaceOfElement(eltType,iel,elem2Face);

        // add the face dofs after the edge node dofs
        for ( std::size_t ifac = 0; ifac < elem2Face.size(); ifac++ ) {
          for ( int jdof = 0; jdof < numDOFPerFace; jdof++ ) {
            elemConnectivity[shiftLocalDOF + ifac*numDOFPerFace + jdof] = jdof + elem2Face[ifac]*numDOFPerFace + shiftGlobalDOF;
          }
        }
      }
      shiftLocalDOF  += numDOFFace; //or numDOFNodeFace; ???
      shiftGlobalDOF += m_mesh->numFaces() * numDOFPerFace;
    }

    //! 3/ VOLUME dofs
    //! It assumes that there is always the same number of volume dof for all elements
    if ( numDOFVolume != 0 ) {
      // look only at the 1rst DOFNodeVolume
      int idof = 0;

      int elem2Volume = 0;
      if ( numDOFNodeVolume > 0 && feIdLocalDof[idof + shiftLocalDOF] == -1 ) {
        // compute elem2Volume
        auto& bagElementType3D = GeometricMeshRegion::bagElementType3D;
        for (auto it_elttype = bagElementType3D.begin(); it_elttype != bagElementType3D.end(); ++it_elttype) {
          ElementType elementType = (ElementType)*it_elttype;
          if(elementType == eltType) {
            elem2Volume += iel;
          } else {
            elem2Volume += m_mesh->numElements(elementType);
          }
        }

        // add the volume dofs after the face dofs.
        for ( int jdof = 0; jdof < numDOFVolume; jdof++ )
          elemConnectivity[shiftLocalDOF + jdof] = shiftGlobalDOF + elem2Volume*numDOFVolume + jdof;
      }
    }
  }

  //! helper to build the support arrays in CSR format
  //! m_iEle, m_iSupportDof :
  void SupportDofMesh::m_buildSupportCSR(const std::vector<ElementType>& theBagElt, felInt& cptElt) 
  {
    std::vector<felInt> elem;
    std::vector<felInt> elemConnectivity;
    std::vector<int> feIdLocalDof;
    felInt countEltPerType;

    for (std::size_t ieltype = 0; ieltype < theBagElt.size(); ++ieltype) {
      ElementType eltType = theBagElt[ieltype];

      //! definition of the finite element type ( geoEle, RefEle )
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);

      CurrentFiniteElement fe(refEle, geoEle, DegreeOfExactness_0);
      // Link geo and ref nodes
      m_linkNodesGeoAndRefEle(fe, feIdLocalDof);

      elemConnectivity.resize(refEle.numDOF(),0);
      elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0);

      countEltPerType = 0;
      for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin(); itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
        // felInt currentLabel = itRef->first;
        felInt numElemsPerRef = itRef->second.second;

        for ( felInt iel = 0; iel < numElemsPerRef; iel++ ) {
          //! get the elem and its connectivity
          m_getElementSupportConnectivity(refEle, eltType, countEltPerType, feIdLocalDof, elem, elemConnectivity);

          if(FelisceParam::instance().FusionDof) {
            for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++) {
              m_iSupportDof[m_iEle[cptElt] + iDof ] = m_listPerm[elemConnectivity[iDof]];
            }
          } else {
            // Link from geometric element to supportdofmesh: CSR format
            for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++) {
              m_iSupportDof[m_iEle[cptElt] + iDof ] = elemConnectivity[iDof];
            }
          }
          m_iEle[cptElt + 1] = m_iEle[cptElt] + elemConnectivity.size();
          m_vectorIdElementSupport[cptElt].push_back(cptElt);

          cptElt++;
          countEltPerType++;
        }
      }
    }
  }

  //! to switch from geoEle P1 to refEle P2 for instance
  const RefElement& SupportDofMesh::m_chooseRefEleFromFEType(const GeoElement& geoEle, const ElementType eltType) const 
  {
    // Choice of the reference finite element (0: default, 1: quadratic)
    switch ( m_variable.finiteElementType() ) {
      case 0: { //! default
        return geoEle.defaultFiniteEle();
      }
      case 1: { //! quadratic
        const GeoElement& geoEleBis = *GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltQuad[eltType]].second;
        return geoEleBis.defaultFiniteEle();
      }
      case 2: { //! bubble
        if(m_mesh->domainDim() == geoEle.numCoor()) {
          const GeoElement& geoEleBis = *GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltBubble[eltType]].second;
          return geoEleBis.defaultFiniteEle();
        } else {
          // This looks ugly, I know, but the alternative would be to add another case after bubble finite element and add a lot of useless mappings.
          // If someone ever tries to use P1bubble in prisms this has to be changed!!
          if (m_mesh->bagElementTypeDomain().size()==1 && m_mesh->bagElementTypeDomain()[0] == GeometricMeshRegion::Prism6 && eltType == GeometricMeshRegion::Quad4)
            return refElementQuadrangleP1xP2;
          return geoEle.defaultFiniteEle();
        }
      }
      default:
        FEL_ERROR("Error: the finite Element type (linear, quadratic, ...) is not set properly.");
    }
    return refElementNode;
  }

  /*!
    \brief Get the id of the element from its type and id with respect to its type
    \param[in] mesh The mesh where to get the id from
    \param[in] eltType The type of element
    \param[in] ielGeom The id of the element
    \param[out] ielSupportDof The id of the element
  */
  void SupportDofMesh::getIdElementSupport(const ElementType& eltType, felInt ielGeom, std::vector<felInt>& vectorSupport) const 
  {
    felInt position = 0;
    m_mesh->getIdElemFromTypeElemAndIdByType(eltType, ielGeom, position);
    vectorSupport = m_vectorIdElementSupport[position];
  }

  /*!
    \brief Get the id of the element from its type and id with respect to its type
    \param[in] mesh The mesh where to get the id from
    \param[in] eltType The type of element
    \param[in] ielGeom The id of the element
    \param[out] ielSupportDof The id of the element
  */
  void SupportDofMesh::getIdElementSupport(const ElementType& eltType, felInt ielGeom, felInt& ielSupportDof) const 
  {
    felInt position = 0;
    m_mesh->getIdElemFromTypeElemAndIdByType(eltType, ielGeom, position);
    ielSupportDof = m_vectorIdElementSupport[position][0];
  }

  void SupportDofMesh::getIdElementSupport(felInt idEle, std::vector<felInt>& vecSupport) const 
  {
    vecSupport = m_vectorIdElementSupport[idEle];
  }

  void SupportDofMesh::getIdElementSupport(felInt idEle, felInt& ielSupportDof) const 
  {
    ielSupportDof = m_vectorIdElementSupport[idEle][0];
  }

  void SupportDofMesh::getIdPointElementSupport(felInt idEle, std::vector<felInt>& vecIdPoint) const
  {
    for ( int i = 0; i < getNumSupportDof(idEle); ++i)
      vecIdPoint[i] = m_iSupportDof[m_iEle[idEle]+i];
  }

  void SupportDofMesh::fusionSupportDof(const Variable& variable) 
  {
    std::size_t cpt = 0;
    std::size_t label, labelIN, labelOUT;
    std::size_t FusionNumLabPerVariable;
    std::vector< std::pair<std::size_t, std::size_t> > listSuppDofMatched;
    std::set<std::size_t> listSuppDofIN;
    std::set<std::size_t> listSuppDofOUT;
    std::set<std::size_t> listSuppDof;
    std::set<std::size_t> listSuppDofToMatched;
    double distNodes;
    std::vector<int> elem, elemConnectivity, feIdLocalDof;
    felInt countEltPerType;

    for(std::size_t fvar = 0; fvar < FelisceParam::instance().FusionVariable.size(); fvar++) { // loop on fusion variables
      FusionNumLabPerVariable = FelisceParam::instance().FusionNumLabel[fvar];
      if ( FelisceParam::instance().FusionVariable[fvar] == variable.name().c_str()) {

        if (  FelisceParam::instance().AllToOne[fvar]   ) { // all nodes fusioned into node 0
          for ( std::size_t i= 1; i <  m_listNode.size(); ++i )
		        listSuppDofMatched.emplace_back(0,i);
        }
        else if ( FelisceParam::instance().AllInLabelToOne ) { // all nodes of a label fusioned into a single node 
          // get the nodes 
          PetscPrintf(PETSC_COMM_WORLD,"##### For variable %s \n",variable.name().c_str() );
	        for(std::size_t flab = 0; flab < FusionNumLabPerVariable; flab++) { // loop on labels
		        label = FelisceParam::instance().FusionLabel[cpt + flab];
		        PetscPrintf(PETSC_COMM_WORLD,"Fusion dof for label %ld \n", static_cast<unsigned long>(label) );
		        getElemConnectivitySingleLabel(label, listSuppDof); // get list of all nodes with label = labelDarcy
		        for (auto it = listSuppDof.begin(); it!=listSuppDof.end(); ++it) {
	           if ( it !=  listSuppDof.begin() )
                listSuppDofMatched.emplace_back(*listSuppDof.begin(),*it);
		        } 
	        }
        }
        else if ( FelisceParam::instance().contactDarcy > 0 ) {
	        // =================================================
	        // MF : This part has to be reworked, it is to specifiy to the Darcy test case 
	        //==================================================
          // WARNING: done for only one type of label per variable
          // get the list of darcy global nodes
          PetscPrintf(PETSC_COMM_WORLD,"##### For variable %s \n",variable.name().c_str() );
	        for(std::size_t flab = 0; flab < FusionNumLabPerVariable; flab++) { // loop on labels
		        label = FelisceParam::instance().FusionLabel[cpt + flab];
		        PetscPrintf(PETSC_COMM_WORLD,"Fusion dof for label %ld \n", static_cast<unsigned long>(label) );
            getElemConnectivitySingleLabel(label, listSuppDof); // get list of all nodes with label = labelDarcy
	        }
	  
          // std::set the nodeId for the fusion in m_idFusionDarcy
          m_idFusionDarcy = *listSuppDof.begin();

          // loop on the mesh nodes and match the nodes not on the list, with the first darcy node
          const std::vector<ElementType>& bagElementTypeDomain = m_mesh->bagElementTypeDomain();
          for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) {
            ElementType eltType  =  bagElementTypeDomain[i];
            const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
            const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);
            CurrentFiniteElement fe(refEle, geoEle, DegreeOfExactness_0);
            m_linkNodesGeoAndRefEle(fe, feIdLocalDof);
            elemConnectivity.resize(refEle.numDOF(),0);
            elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0);


            // second loop on label of the mesh.
            countEltPerType = 0;
            for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin();
              itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
              const std::size_t numEltPerLabel = itRef->second.second;
              for (std::size_t iel = 0; iel < numEltPerLabel; iel++ ) {
                m_getElementSupportConnectivity(refEle, eltType, countEltPerType, feIdLocalDof, elem, elemConnectivity);

                for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++)  {
	                auto kelin = std::find(listSuppDof.begin(), listSuppDof.end(),elemConnectivity[iDof]);
	                if ( kelin == listSuppDof.end() ) // if the node is not inside darcy_nodes_list we add it to the std::set of all nodes to be matched
                    listSuppDofToMatched.insert(elemConnectivity[iDof]); // we first save inside a std::set all the nodes to be matched after
	        	        //listSuppDofMatched.push_back(std::make_pair(*listSuppDof.begin(),elemConnectivity[iDof])); // the pair is not unique!
                }

                countEltPerType++;
              }
            }
          }
          // creation of std::vector<pair> listSuppDofMatched from the std::set listSuppDofToMatched
          for (auto itMatch=listSuppDofToMatched.begin(); itMatch!=listSuppDofToMatched.end(); ++itMatch)
            listSuppDofMatched.emplace_back(*listSuppDof.begin(),*itMatch);
        }
        else {
	        PetscPrintf(PETSC_COMM_WORLD,"##### For variable %s \n",variable.name().c_str() );
	        for(std::size_t flab = 0; flab < FusionNumLabPerVariable; flab++) { // loop on labels
	          labelIN = FelisceParam::instance().FusionLabel[cpt + 2*flab];
	          labelOUT = FelisceParam::instance().FusionLabel[cpt + 2*flab +1];
	          PetscPrintf(PETSC_COMM_WORLD,"Fusion label %ld in label %ld \n", static_cast<unsigned long>(labelIN), static_cast<unsigned long>(labelOUT) );
            matchingElemConnectivity(labelIN, labelOUT, listSuppDofIN, listSuppDofOUT);
	        }
          
	        //loop to delete the supporDofIN in common with the supporDofOUT
	        for (auto kelout=listSuppDofOUT.begin(); kelout !=  listSuppDofOUT.end() ; ++kelout )  {
	          auto kelin = std::find(listSuppDofIN.begin(), listSuppDofIN.end(), *kelout);
	          if ( kelin !=  listSuppDofIN.end() )
      		    listSuppDofIN.erase( kelin );
	        }
		
	        //loop to match the SuppDofIN with the SuppDofOUT
	        for (auto kelout=listSuppDofOUT.begin(); kelout !=  listSuppDofOUT.end() ; ++kelout ) {
	          Point& pout = listNode()[*kelout];
	          for (auto kelin=listSuppDofIN.begin(); kelin !=  listSuppDofIN.end() ; ++kelin ) {
      		    Point& pin = listNode()[*kelin];
      		    distNodes = std::sqrt( ( pin.x() - pout.x() ) * ( pin.x() - pout.x() ) + ( pin.y() - pout.y() ) * ( pin.y() - pout.y() ) + ( pin.z() - pout.z() ) * ( pin.z() - pout.z() ) );
      		    if ( distNodes < FelisceParam::instance().FusionTolerance )
      		      listSuppDofMatched.emplace_back(*kelin,*kelout);
	          }
	        }
      	}
        m_numSupportDof = m_listNode.size() - listSuppDofMatched.size();
        GetPermutationList(listSuppDofMatched);
      }
      if ( FelisceParam::instance().AllInLabelToOne )
        cpt++;
      else
        cpt += FusionNumLabPerVariable*2;
    }
  }

  void SupportDofMesh::getElemConnectivitySingleLabel(std::size_t label, std::set<std::size_t>& listSuppDof) 
  {
    std::vector<int> elem, elemConnectivity, feIdLocalDof;
    felInt countEltPerType;

    for (std::size_t ieltype = 0; ieltype < m_mesh->bagElementTypeDomainBoundary().size(); ++ieltype) {
      ElementType eltType =  m_mesh->bagElementTypeDomainBoundary()[ieltype];
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);
      CurrentFiniteElement fe(refEle, geoEle, DegreeOfExactness_0);
      m_linkNodesGeoAndRefEle(fe, feIdLocalDof);
      elemConnectivity.resize(refEle.numDOF(),0); // contains the supportDofs points
      elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0); // contains the nodes' points (equal for P1)

      countEltPerType = 0;
      for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin();
            itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
        std::size_t currentLabel = itRef->first;
        std::size_t numElemsPerRef = itRef->second.second;
        for (std::size_t iel = 0; iel < numElemsPerRef; iel++ ) {
          m_getElementSupportConnectivity(refEle, eltType, countEltPerType, feIdLocalDof, elem, elemConnectivity);
          for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++)  {
            if(currentLabel == label )
              listSuppDof.insert(elemConnectivity[iDof]);
          }

          countEltPerType++;
        }
      }
    }
  }

  void SupportDofMesh::matchingElemConnectivity(std::size_t labelIN, std::size_t labelOUT, std::set<std::size_t>& listSuppDofIN, std::set<std::size_t>& listSuppDofOUT) 
  {
    std::vector<int> elem, elemConnectivity, feIdLocalDof;
    felInt countEltPerType;

    for (std::size_t ieltype = 0; ieltype < m_mesh->bagElementTypeDomainBoundary().size(); ++ieltype) {
      ElementType eltType =  m_mesh->bagElementTypeDomainBoundary()[ieltype];
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);
      CurrentFiniteElement fe(refEle, geoEle, DegreeOfExactness_0);
      m_linkNodesGeoAndRefEle(fe, feIdLocalDof);
      elemConnectivity.resize(refEle.numDOF(),0);
      elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0);

      countEltPerType = 0;
      for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin();
            itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
        std::size_t currentLabel = itRef->first;
        std::size_t numElemsPerRef = itRef->second.second;
        for (std::size_t iel = 0; iel < numElemsPerRef; iel++ ) {
          m_getElementSupportConnectivity(refEle, eltType, countEltPerType, feIdLocalDof, elem, elemConnectivity);
          for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++)  {
            if(currentLabel == labelIN )
              listSuppDofIN.insert(elemConnectivity[iDof]);
            if(currentLabel == labelOUT )
              listSuppDofOUT.insert(elemConnectivity[iDof]);
          }

          countEltPerType++;
        }
      }
    }
  }

  void SupportDofMesh::GetPermutationList(std::vector <std::pair< std::size_t,  std::size_t> >& listSuppDofMatched) 
  {
    std::size_t iin, iio, iaux;

    for (std::size_t ip = 0; ip < listSuppDofMatched.size(); ++ip) {

      // retrieve the SuppDof to be linked
      iin = listSuppDofMatched[ip].first; //  SuppDofs to be kept
      iio = listSuppDofMatched[ip].second; // SuppDofs to be removed

      // 1st step - remove iioSuppDof in m_listPerm
      for(std::size_t jp = iio+1; jp < m_listPerm.size(); ++jp)
        m_listPerm[jp]-=1;
      for (std::size_t jp = 0; jp < ip; ++jp) {
        iaux = listSuppDofMatched[jp].second;
        if (  m_listPerm[iaux] > m_listPerm[iio] )
          m_listPerm[iaux]-=1;
      }

      // 2nd step - assign new link to SuppDof
      m_listPerm[iio]=m_listPerm[iin];
    }
  }

  // Embedded interface
  void SupportDofMesh::matchEmbeddedLabelPairs(const Variable& variable) 
  {
    std::size_t cpt = 0;
    std::size_t MatchNumLabPerVariable;
    double distNodes;
    std::vector< std::size_t > checkboard;

    std::size_t labelSide0, labelSide1;
    std::size_t idElemSide0, idElemSide1;
    std::set<std::size_t> listSuppDofSide0, listSuppDofSide1;
    std::vector< std::pair<std::size_t, std::set<std::size_t> > > listSuppElemSide0, listSuppElemSide1;

    for(std::size_t mvar = 0; mvar < FelisceParam::instance().EmbeddedVariable.size(); mvar++) {
      MatchNumLabPerVariable = FelisceParam::instance().EmbeddedNumInterface[mvar];
      if ( FelisceParam::instance().EmbeddedVariable[mvar]  == variable.name().c_str()) {
        PetscPrintf(PETSC_COMM_WORLD,"##### For variable %s \n",variable.name().c_str() );

        for(std::size_t mlab = 0; mlab < MatchNumLabPerVariable; mlab++) {
          labelSide0 = FelisceParam::instance().EmbeddedLabelPairs[cpt + 2*mlab];
          labelSide1 = FelisceParam::instance().EmbeddedLabelPairs[cpt + 2*mlab +1];
          PetscPrintf(PETSC_COMM_WORLD,"Match label %ld (the computations are perfomed on this label) and label %ld \n", static_cast<unsigned long>(labelSide0), static_cast<unsigned long>(labelSide1) );
          matchingElemConnectivity(labelSide0, labelSide1, listSuppDofSide0, listSuppDofSide1);
          extractIdElemWithConnectivityFromSides(labelSide0, labelSide1, listSuppElemSide0, listSuppElemSide1);
        }

        // loop to match the listSuppDofSide0 with the listSuppDofSide1
        for (auto kDofSide1=listSuppDofSide1.begin(); kDofSide1 !=  listSuppDofSide1.end() ; ++kDofSide1 ) {
          Point& pSide1 = listNode()[*kDofSide1];
          for (auto kDofSide0=listSuppDofSide0.begin(); kDofSide0 !=  listSuppDofSide0.end() ; ++kDofSide0 ) {
            Point& pSide0 = listNode()[*kDofSide0];
            distNodes = std::sqrt( ( pSide0.x() - pSide1.x() ) * ( pSide0.x() - pSide1.x() ) +
                              ( pSide0.y() - pSide1.y() ) * ( pSide0.y() - pSide1.y() ) +
                              ( pSide0.z() - pSide1.z() ) * ( pSide0.z() - pSide1.z() ) );
            if ( distNodes < FelisceParam::instance().FusionTolerance ){
              m_mapSuppDofMatched0.insert(std::make_pair(*kDofSide0,*kDofSide1));
	            m_mapSuppDofMatched1.insert(std::make_pair(*kDofSide1,*kDofSide0));
	          }
          }
        }

        // loop to match listSuppElemSide0 with the listSuppElemSide1
        for (std::size_t kElemSide0= 0; kElemSide0 < listSuppElemSide0.size(); kElemSide0++ ){
          idElemSide0 = listSuppElemSide0[kElemSide0].first;
          for (std::size_t kElemSide1= 0; kElemSide1 < listSuppElemSide1.size(); kElemSide1++ ){
            idElemSide1 = listSuppElemSide1[kElemSide1].first;
            // check wether dofs of the elements shared the same support point
            checkboard.clear();
            checkboard.resize((listSuppElemSide0[kElemSide0].second).size());
            int i = 0;
	          for (auto kDofSide0=(listSuppElemSide0[kElemSide0].second).begin(); kDofSide0 !=  (listSuppElemSide0[kElemSide0].second).end() ; ++kDofSide0 ) {
	            if(  (listSuppElemSide1[kElemSide1].second).count(m_mapSuppDofMatched0[*kDofSide0]) )
		            checkboard[i] = 1;
	            else
		            checkboard[i] = 0;
	            ++i;
            }
	          std::size_t count = 0;
	          for(std::size_t check= 0; check < checkboard.size(); check++ )
	            count += checkboard[check];
	          if ( count == (listSuppElemSide0[kElemSide0].second).size() )
	            m_mapSuppElemMatched.insert(std::make_pair(idElemSide0,idElemSide1));
	        }
	      }
	      cpt +=  MatchNumLabPerVariable*2;
      }
    }
    m_createdEmbeddedInterfaceMaps = true;
  }

  /// CRACK INTERFACE
  void SupportDofMesh::matchCrackLabelPairs(const Variable& variable) 
  {
    std::size_t cpt = 0;
    std::size_t MatchNumLabPerVariable;
   // double distNodes;
    std::vector< std::size_t > checkboard;

    std::size_t labelSide0, labelSide1;
    std::size_t idElemSide0, idElemSide1;
    std::set<std::size_t> listSuppDofSide0, listSuppDofSide1;
    std::vector< std::pair<std::size_t, std::set<std::size_t> > > listSuppElemSide0, listSuppElemSide1;

    for(std::size_t mvar = 0; mvar < FelisceParam::instance().CrackVariable.size(); mvar++) {
      MatchNumLabPerVariable = FelisceParam::instance().CrackNum[mvar];
      if ( FelisceParam::instance().CrackVariable[mvar]  == variable.name().c_str()) {
        PetscPrintf(PETSC_COMM_WORLD,"##### For variable %s \n",variable.name().c_str() );

        for(std::size_t mlab = 0; mlab < MatchNumLabPerVariable; mlab++) {
          labelSide0 = FelisceParam::instance().CrackLabelPairs[cpt + 2*mlab];
          labelSide1 = FelisceParam::instance().CrackLabelPairs[cpt + 2*mlab +1];
          PetscPrintf(PETSC_COMM_WORLD,"Match label %ld (the computations are perfomed on this label) and label %ld \n", static_cast<unsigned long>(labelSide0), static_cast<unsigned long>(labelSide1) );
          matchingElemConnectivity(labelSide0, labelSide1, listSuppDofSide0, listSuppDofSide1);
          extractIdElemWithConnectivityFromSides(labelSide0, labelSide1, listSuppElemSide0, listSuppElemSide1);
        }

        // loop to match the listSuppDofSide0 with the listSuppDofSide1
        for (auto kDofSide1=listSuppDofSide1.begin(); kDofSide1 !=  listSuppDofSide1.end() ; ++kDofSide1 ) {
         // Point& pSide1 = listNode()[*kDofSide1];
          for (auto kDofSide0=listSuppDofSide0.begin(); kDofSide0 !=  listSuppDofSide0.end() ; ++kDofSide0 ) {
          //  Point& pSide0 = listNode()[*kDofSide0];
            m_mapSuppDofMatchedCrack0.insert(std::make_pair(*kDofSide0,*kDofSide1));
            m_mapSuppDofMatchedCrack1.insert(std::make_pair(*kDofSide1,*kDofSide0));
          }
        }

        // loop to match listSuppElemSide0 with the listSuppElemSide1
        for (std::size_t kElemSide0= 0; kElemSide0 < listSuppElemSide0.size(); kElemSide0++ ){
          idElemSide0 = listSuppElemSide0[kElemSide0].first;
          for (std::size_t kElemSide1= 0; kElemSide1 < listSuppElemSide1.size(); kElemSide1++ ){
            idElemSide1 = listSuppElemSide1[kElemSide1].first;
            // check wether dofs of the elements shared the same support point
            checkboard.clear();
            checkboard.resize((listSuppElemSide0[kElemSide0].second).size());
            int i = 0;
            for (auto kDofSide0=(listSuppElemSide0[kElemSide0].second).begin(); kDofSide0 !=  (listSuppElemSide0[kElemSide0].second).end() ; ++kDofSide0 ) {
              if(  (listSuppElemSide1[kElemSide1].second).count(m_mapSuppDofMatchedCrack0[*kDofSide0]) )
                checkboard[i] = 1;
              else
                checkboard[i] = 0;
              ++i;
            }
            std::size_t count = 0;
            for(std::size_t check= 0; check < checkboard.size(); check++ )
              count += checkboard[check];
            if ( count == (listSuppElemSide0[kElemSide0].second).size() )
              m_mapSuppElemMatchedCrack.insert(std::make_pair(idElemSide0,idElemSide1));
          }
        }
        cpt +=  MatchNumLabPerVariable*2;
      }
    }
    m_createdCrackInterfaceMaps = true;
  }

  // both CRACK AND EMBEDDED
  void SupportDofMesh::extractIdElemWithConnectivityFromSides(std::size_t labelSide0, std::size_t labelSide1,
                                                              std::vector< std::pair<std::size_t, std::set<std::size_t> > >& listSuppElemSide0,
                                                              std::vector< std::pair<std::size_t, std::set<std::size_t> > >& listSuppElemSide1) 
  {
    std::vector<int> elem, elemConnectivity, feIdLocalDof;
    felInt countEltPerType;
    std::vector<felInt> vecSupport;
    std::set<std::size_t> listSuppDof;

    for (std::size_t ieltype = 0; ieltype < m_mesh->bagElementTypeDomainBoundary().size(); ++ieltype) {
      ElementType eltType =  m_mesh->bagElementTypeDomainBoundary()[ieltype];
      const GeoElement& geoEle = *GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement& refEle = m_chooseRefEleFromFEType(geoEle, eltType);
      CurrentFiniteElement fe(refEle, geoEle, DegreeOfExactness_0);
      m_linkNodesGeoAndRefEle(fe, feIdLocalDof);
      elemConnectivity.resize(refEle.numDOF(),0);
      elem.resize(GeometricMeshRegion::m_numPointsPerElt[eltType], 0);

      countEltPerType = 0;
      for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin(); itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
        std::size_t currentLabel   = itRef->first;
        std::size_t numElemsPerRef = itRef->second.second;

        for (std::size_t iel = 0; iel < numElemsPerRef; iel++ ) {

          m_getElementSupportConnectivity(refEle, eltType, countEltPerType, feIdLocalDof, elem, elemConnectivity);
          getIdElementSupport(eltType, countEltPerType, vecSupport);

	        for (std::size_t iSupportEl = 0; iSupportEl < vecSupport.size(); iSupportEl++)  {
            if(currentLabel == labelSide0 ){
	            listSuppDof.clear();
              for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++)
                listSuppDof.insert(elemConnectivity[iDof]);
              listSuppElemSide0.emplace_back(vecSupport[iSupportEl],listSuppDof);
            }
            if(currentLabel == labelSide1 ){
	            listSuppDof.clear();
              for (std::size_t iDof = 0; iDof < elemConnectivity.size(); iDof++)
                listSuppDof.insert(elemConnectivity[iDof]);
              listSuppElemSide1.emplace_back(vecSupport[iSupportEl],listSuppDof);
            }
          }
          countEltPerType++;
        }
      }
    }
  }

  void SupportDofMesh::duplicateSupportElements( std::map<felInt, felInt>&                    listIntersectedEltIdx,
                                                 std::map<felInt, std::vector<felInt> >&      listIntersectedVerSgn,
                                                 std::map<felInt, std::map<felInt, felInt> >& mapVerticesIdxToDupl
                                               ) 
  {

    // add new point to listNode and listPerm and set the id of each node marked for duplication
    // TODO in case of fusionDof we do it here, but it should be done calling fusionSupportDof after the duplication...
    felInt curSupport    = 0;
    auto& r_instance     = FelisceParam::instance();
    auto& fusionVariable = r_instance.FusionVariable;
    auto  doFusionDof    = std::find(fusionVariable.begin(), fusionVariable.end(), m_variable.name().c_str());

    if( doFusionDof != fusionVariable.end() ) {

      if ( r_instance.contactDarcy > 0 ) {
        // TODO D.C. 
        // -------------
        // If we are modifying the supportDofUnknow for the darcyPressure we fusion also the new duplicated node
        // on the first node with label = darcyLabel
        for(auto it_map = mapVerticesIdxToDupl.begin(); it_map != mapVerticesIdxToDupl.end(); ++it_map) {

          auto& nest_map = it_map->second;
          for(auto it_nest_map = nest_map.begin(); it_nest_map != nest_map.end(); ++it_nest_map) {

            m_listNode.push_back(m_listNode[it_map->first]);
            m_listPerm.push_back(0); // because we do fusionDof with the first Darcy-label node

            it_nest_map->second = getIdFusionDarcy();
          }
        }

        // we don't need to change the number of supportDof for the Darcy Pressure in case of fusionDof for Darcy,
        // TODO D.C.
      } else {

        FEL_ERROR("Not implemented yet");
      }
    } else {

      // add the duplicated support dof at the end of listNode and update listPerm
      for(auto it_map = mapVerticesIdxToDupl.begin(); it_map != mapVerticesIdxToDupl.end(); ++it_map) {

        auto& nest_map = it_map->second;
        for(auto it_nest_map = nest_map.begin(); it_nest_map != nest_map.end(); ++it_nest_map){

          m_listNode.push_back(m_listNode[it_map->first]);
          m_listPerm.push_back(m_listNode.size());

          it_nest_map->second = m_numSupportDof + curSupport;
          ++curSupport;
        }
      }

      // change m_numSupportDof
      m_numSupportDof = m_listNode.size();
    }

    // for each intersected element
    for(auto itListInt = listIntersectedEltIdx.begin(); itListInt != listIntersectedEltIdx.end(); ++itListInt) {

      // get the id of the element
      const felInt idElement = itListInt->first;

      // get the id of the interface
      const felInt idInterface = itListInt->second;

      // get the id of the support element
      const felInt idSupportElt = m_vectorIdElementSupport[idElement][0];

      // number of support dof of the element
      const felInt numSupportDofOfElem = getNumSupportDof(idSupportElt);

      // insert new support dof (duplicate them) in m_iEle
      m_iEle.insert(m_iEle.begin()+idSupportElt+2, m_iEle[idSupportElt] + numSupportDofOfElem);
      for(std::size_t jel = idSupportElt+2; jel < m_iEle.size(); ++jel)
        m_iEle[jel] += numSupportDofOfElem;

      // add the new support element in vectorIdElementSupport
      for(std::size_t jel = idElement+1; jel < m_vectorIdElementSupport.size(); ++jel)
        for(std::size_t idSupport = 0; idSupport < m_vectorIdElementSupport[jel].size(); ++idSupport)
          m_vectorIdElementSupport[jel][idSupport]++;
      m_vectorIdElementSupport[idElement].push_back(idSupportElt+1);

      // update m_iSupportDof, duplicate the support element and fill it with -1
      m_iSupportDof.insert(m_iSupportDof.begin()+m_iEle[idSupportElt+1], numSupportDofOfElem, -1);

      // for each support dof
      for(felInt iSupport = 0; iSupport < numSupportDofOfElem; ++iSupport) {

        // look for the current dof in the list of duplicated vertices
        const auto ver_2_map = mapVerticesIdxToDupl.find( m_iSupportDof[m_iEle[idSupportElt]+iSupport] );

        // duplicate the support if needed
        if( ver_2_map != mapVerticesIdxToDupl.end() ) {

          // get the duplicated vertex
          const auto itf_2_dup = ver_2_map->second.find( idInterface );

          if ( itf_2_dup != ver_2_map->second.end() ) {

            // get the side 
            const auto side = listIntersectedVerSgn[idElement][iSupport];

            if( side > 0 ) {
              // Point on the left part of the structure
              // set only the duplicated support dof
              m_iSupportDof[m_iEle[idSupportElt+1]+iSupport] = itf_2_dup->second;
            } else if ( side < 0 ) {
              // Point on the right part of the structure
              // first set the duplicated support dof with the original one and second change the original.
              m_iSupportDof[m_iEle[idSupportElt+1]+iSupport] = m_iSupportDof[m_iEle[idSupportElt]+iSupport];
              m_iSupportDof[m_iEle[idSupportElt]+iSupport]   = itf_2_dup->second;
            } else {
              // degenerated case, the mesh vertex is on the interface
              m_iSupportDof[m_iEle[idSupportElt+1]+iSupport] = itf_2_dup->second;
            }
          } else {

            FEL_ERROR("Not really clear how the duplication should be in this case");
          }
        } else {
          // copy the one in the original support element
          m_iSupportDof[m_iEle[idSupportElt+1]+iSupport] = m_iSupportDof[m_iEle[idSupportElt]+iSupport];
        }
      }
    }
  }

  //--------------------------------------
  void SupportDofMesh::getIndexesElementSupport(int iel, std::vector<int>& indexes) const 
  {
    FEL_ASSERT_LT(iel, m_iEle.size() - 1);

    felInt first_index = m_iEle[iel];
    felInt last_index  = m_iEle[iel + 1];

    FEL_ASSERT(last_index <= static_cast<felInt>(m_iSupportDof.size()));

    indexes.resize(last_index - first_index);
    for (std::size_t i = 0; i < indexes.size(); ++i) 
      indexes[i] = m_iSupportDof[first_index+i];
  }
  //--------------------------------------

  void SupportDofMesh::printIdElementSupport(std::ostream& outstr) const 
  {
    ElementType eltType;
    felInt numElemsPerRef;
    felInt countEltPerType[ GeometricMeshRegion::m_numTypesOfElement ];
    std::vector<felInt> vecSupport;

    for (int ityp = 0; ityp < GeometricMeshRegion::m_numTypesOfElement; ityp++) {
      eltType = (ElementType)ityp;
      countEltPerType[eltType] = 0;
    }
    const std::vector<ElementType>& bagElementTypeDomainBoundary = m_mesh->bagElementTypeDomainBoundary();
    const std::vector<ElementType>& bagElementTypeDomain = m_mesh->bagElementTypeDomain();

    outstr << "Mapping eltGeom to eltSupport in supportDof" << std::endl;

    for (std::size_t ityp = 0; ityp < bagElementTypeDomain.size() ; ++ityp) {
      eltType = bagElementTypeDomain[ityp];
      if (m_mesh->numElements(eltType) != 0 ) {
        for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin();
              itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
          numElemsPerRef = itRef->second.second;
          for ( felInt iel = 0; iel < numElemsPerRef; iel++ ) {
            getIdElementSupport(eltType, countEltPerType[eltType], vecSupport);
            outstr << "( " << GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first << ", " << iel << ")--->";
            for(std::size_t idSupport=0; idSupport<vecSupport.size(); ++idSupport)
              outstr << " " << vecSupport[idSupport];
            outstr << std::endl;
            countEltPerType[eltType]++;
          }
        }
      }
    }

    for (std::size_t ityp = 0; ityp < bagElementTypeDomainBoundary.size() ; ++ityp) {
      eltType = bagElementTypeDomainBoundary[ityp];
      if (m_mesh->numElements(eltType) != 0 ) {
        for(auto itRef = m_mesh->intRefToBegEndMaps[eltType].begin();
              itRef != m_mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
          numElemsPerRef = itRef->second.second;
          for ( felInt iel = 0; iel < numElemsPerRef; iel++ ) {
            getIdElementSupport(eltType, countEltPerType[eltType], vecSupport);
            outstr << "( " << GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].first << ", " << iel << ")--->";
            for(std::size_t idSupport=0; idSupport<vecSupport.size(); ++idSupport)
              outstr << " " << vecSupport[idSupport];
            outstr << std::endl;
            countEltPerType[eltType]++;
          }
        }
      }
    }
  }

  /*!
    \brief Print the support of dof in CSR format
    \param[in] verbose Level of verbose
    \param[in] outstr The out stream
  */
  void SupportDofMesh::print(int verbose, std::ostream& outstr) const 
  {
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    if (verbose) {
      PetscPrintf(PETSC_COMM_WORLD,"[%d] Number of DOF supports: %d\n", rank, numSupportDof());
    }

    if  (verbose > 24) {
      outstr << "\n [" << rank << "] Nodes coordinates (size = " <<  m_listNode.size() <<"): \n";
      for (felInt iv = 0; iv < static_cast<felInt>(m_listNode.size()); iv++) {
        outstr << "[" << rank << "] ( " << m_listNode[iv].x() << ", " <<  m_listNode[iv].y() << ", " <<  m_listNode[iv].z() << ") "<< std::endl;
      }
      outstr << std::endl;
      outstr << "[" << rank << "] Supports of dof associated to elements (CSR format):\n";
      outstr << "[" << rank << "] The i th element have iEle[i+1] - iEle[i] supports of dof.\n";
      outstr << "[" << rank << "] iEle: ";
      for (felInt iel = 0; iel < static_cast<felInt>(m_iEle.size()); iel++) {
        outstr << m_iEle[iel] << " ";
      }
      outstr << std::endl << std::endl;

      outstr << "[" << rank << "] Supports of dof of the i th element are stored from iSupportDof[ iEle[i] to (iSupportDof[ iEle[i+1] - 1):"<< std::endl;
      outstr << "[" << rank << "] iSupportDof: ";
      for (unsigned int iSupDof = 0; iSupDof < m_iSupportDof.size(); iSupDof++) {
        outstr << m_iSupportDof[iSupDof] << " ";
      }
      outstr << std::endl;

      outstr << " m_vectorIdElementSupport is : " << std::endl;
      for (unsigned int iSupDof = 0; iSupDof < m_vectorIdElementSupport.size(); iSupDof++) {
        for (unsigned int jSupDof = 0; jSupDof < m_vectorIdElementSupport[iSupDof].size(); jSupDof++)
          outstr << m_vectorIdElementSupport[iSupDof][jSupDof] << " ";
        outstr << std::endl;
      }
      outstr << std::endl;

      /* if (FelisceParam::instance().FusionDof) // to better arrange
      {
      std::cout<< "\n/======= Periodic boundary conditions. ======/\n"<< std::endl;
      std::cout<<"\nLabel IN:  "<<std::endl;
      std::cout<<"Support Dof:"<<std::endl;
      for(int iin=0; iin<m_listSuppDofIN.size(); iin++){
      std::cout<<m_listSuppDofIN[iin]<< " ";
      }
      std::cout<<std::endl;


      std::cout<<"\nLabel OUT:  " <<std::endl;
      std::cout<<"Support Dof:"<<std::endl;
      for(int iout=0; iout<m_listSuppDofOUT.size(); iout++){
      std::cout<<m_listSuppDofOUT[iout]<< " ";
      }
      std::cout<<std::endl;
      }*/


      if (FelisceParam::instance().EmbeddedInterface && m_createdEmbeddedInterfaceMaps){
        std::cout << "m_mapSuppElemMatched: {";
        for(auto idElem = m_mapSuppElemMatched.begin(); idElem != m_mapSuppElemMatched.end(); ++idElem)
          std::cout << " {" << idElem->first << " " << idElem->second  << "}";
        std::cout << " }" << std::endl;

        std::cout << "m_mapSuppDofMatched0: {";
        for(auto idDof = m_mapSuppDofMatched0.begin(); idDof != m_mapSuppDofMatched0.end(); ++idDof)
            std::cout << " {" << idDof->first << " " << idDof->second  << "}";
        std::cout << " }" << std::endl;

        std::cout << "m_mapSuppDofMatched1: {";
        for(auto idDof = m_mapSuppDofMatched1.begin(); idDof != m_mapSuppDofMatched1.end(); ++idDof)
            std::cout << " {" << idDof->first << " " << idDof->second  << "}";
        std::cout << " }" << std::endl;
      }

    }
  }
}
