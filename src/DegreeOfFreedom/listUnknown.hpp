//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau
//

#ifndef LISTUNKNOWN_H
#define LISTUNKNOWN_H

// System includes
#include <ostream>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "DegreeOfFreedom/listVariable.hpp"

namespace felisce 
{
/*!
  \class ListUnknown
  \author J. Foulon & J-F Gerbeau
  \date 26/11/2011
  \brief Class containing a list of unknown of a linear system.
  */

class ListUnknown 
{
  public:
    typedef boost::numeric::ublas::matrix<int> interactionsOnUnknown;

    //!Constructor.
    ListUnknown();

    //! Add physicalVariable to list of unknown
    void push_back(PhysicalVariable physicalVariable);

    //! Return number of unknowns.
    std::size_t size() const {
      return m_listUnknown.size();
    }

    //! Determine sizeOdMask.
    void setDefaultMask(const ListVariable& listVariable);
  
    //! Function to define specific connection on unknown.
    /*!
      \brief array contains all value of the mask by line
      ex: \f$ \left (
      \begin{array}{cccc}
      & Vx & Vy & P \\
      Vx & 1  & 0  & 1 \\
      Vy & 0  & 1  & 1 \\
      P  & 1  & 1  & 0 \\
      \end{array}
      \right ) \f$
      array = [ 1, 0, 1, 0, 1, 1, 1, 1, 0 ]
      */

    /*! Fill the mask with 0 or 1 value to define interaction on variable's conponents.
      \param[in] array contains value for the mask (0 or 1) if variable are connected.
    */
    void setMask( const std::vector<int> & array );

    //! Get start of the block in elementary matrix associate to the unknown.
    int getBlockPosition(int iUnknown, int iComp) {
      FEL_ASSERT( iUnknown >= 0 && iUnknown < int(m_iBlockOfUnknown.size()) );
      return m_iBlockOfUnknown[iUnknown]+iComp;
    }

    void print(int verbose = 0, std::ostream& outstr = std::cout) const;
    
    //! Access function
    //=================
    //! Return id of the variable associate to the unknown.
    inline const int & idVariable(const std::size_t iUnknown) const {
      return m_listIdVarOfUnknown[iUnknown];
    }
    
    inline int & idVariable(const std::size_t iUnknown) {
      return m_listIdVarOfUnknown[iUnknown];
    }

    inline const interactionsOnUnknown & mask() const {
      return m_mask;
    }

    inline interactionsOnUnknown & mask() {
      return m_mask;
    }

    inline const std::vector<int>& getUnknownsRows() const {
      return m_listUnknownsRows;
    }

    inline const std::vector<int>& getUnknownsCols() const {
      return m_listUnknownsCols;
    }

    inline std::vector<int>& getUnknownsRows() {
      return m_listUnknownsRows;
    }

    inline std::vector<int>& getUnknownsCols() {
      return m_listUnknownsCols;
    }

    inline void setUnknownsRows(const std::vector<int>& listUnknown) {
      m_listUnknownsRows = listUnknown;
    }

    inline void setUnknownsCols(const std::vector<int>& listUnknown) {
      m_listUnknownsCols = listUnknown;
    }

    inline void setUnknownsRows() {
      m_listUnknownsRows = m_listUnknownsRowsDefault;
    }

    inline void setUnknownsCols() {
      m_listUnknownsCols = m_listUnknownsColsDefault;
    }


    /*!
    \brief Find the position of one unknown in the std::vector with its name.
    Useful to generalize Felisce Library utilisation.
    \param[in] unknown name of the unknown research
    */
    int getUnknownIdList( PhysicalVariable unknown );
  
    /*!
    \brief Find the position of one unknown in the std::vector with its name.
    Useful to generalize Felisce Library utilisation.
    \param[in] unknown name of the unknown research
    */
    int getUnknownIdList( PhysicalVariable unknown ) const;

    //Operators.
    //==========
    PhysicalVariable& operator[](int i) {
      return m_listUnknown[i];
    }

  private:
    std::vector<PhysicalVariable> m_listUnknown;
    std::vector<int> m_listIdVarOfUnknown;
    //! List f unknown for rows and columns
    std::vector<int> m_listUnknownsRows;
    std::vector<int> m_listUnknownsCols;
    std::vector<int> m_listUnknownsRowsDefault;
    std::vector<int> m_listUnknownsColsDefault;
    //! Locate start of the block in elementary matrix associate to the unknown.
    std::vector<int> m_iBlockOfUnknown;
    //! small matrice to define interaction on variables. Useful to define matrix's patern.
    interactionsOnUnknown m_mask;
    //! sum of variable components. ex: Variable velocity with 2 components and pressure with 1 component so sizeOfMask equal 3.
    int m_sizeOfMask;
  };
}

#endif

