//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau
//

// System includes
#include <unordered_map>

// External includes

// Project includes
#include "DegreeOfFreedom/dofBoundary.hpp"
#include "Solver/linearProblem.hpp"
#include "PETScInterface/petscMatrix.hpp"

namespace felisce 
{
/*! \brief Class constructor.
  It sets all flags to false
*/
DofBoundary::DofBoundary():
  m_boundaryPatternAlreadyBuilt(false),
  m_dofBoundaryHasBeenInitialized(false),
  m_volumeInterfaceMappingBuilt(false)
{}

/*! \brief Initializer of the entire class

  It has to be called in linear problem.

  \param[in] lpb_ptr a pointer to the linear problem class.

  The flag #m_dofBoundaryHasBeenInitialized is std::set to false.
*/
void DofBoundary::initialize(LinearProblem* lpb_ptr) {
  m_dofPtr     = &lpb_ptr->dof();
  m_ao         = lpb_ptr->ao();
  m_dofPartPtr = &lpb_ptr->dofPartition();
  m_dofBoundaryHasBeenInitialized=true;
}

/*! \brief It creates the pattern for a sparse matrix defined on the boundary.

  The pattern is taken directly from the volume pattern.
  It implies:
  - #m_dofBoundaryHasBeenInitialized = true
  - #m_volumeInterfaceMappingBuilt = true.
  In case it had already been called before it does not do anyting.
  It raises the flag  #m_boundaryPatternAlreadyBuilt to true.

  #m_boundaryPattern is created.
*/
void DofBoundary::buildBoundaryPattern() {
  if ( m_boundaryPatternAlreadyBuilt )
    return;
  if ( !m_dofBoundaryHasBeenInitialized )
    FEL_ERROR("dof boundary not yet initialized");
  if ( !m_volumeInterfaceMappingBuilt)
    FEL_ERROR("volume interface mapping not yet build");
  // if the pattern is already built than exit
  m_boundaryPatternAlreadyBuilt = true;

  // create a mapping to get local numbering of a dof given in global application numbering
  // TODO check if the mapping is present in linearProblem
  felInt idLocal = 0;
  std::unordered_map<felInt,felInt> glob2loc;
  for (felInt i = 0; i < m_dofPtr->numDof(); i++) {
    if ( (*m_dofPartPtr)[i] == MpiInfo::rankProc()) {
      glob2loc[i] = idLocal;
      idLocal++;
    }
  }

  // this structure will contain, for each node on the Boundary, the std::vector of the neighbours which belong to the boundary.
  // it is local to the proc.
  std::vector< std::vector< felInt > > nodesNeighborhood( m_numLocalDofInterface );
  // the number of nnz in the local pattern
  std::size_t nnz = 0;
  // building of nodesNeighborhood:
  // for each surface node in this proc..
  for ( int iNode = 0; iNode < m_numLocalDofInterface; iNode++ ) {
    // get all the neighbours!
    // they can be both on surface and in the volume
    // and in this proc or in another
    std::vector<felInt> currentVecOfNeighbour;
    felInt iGlobAppli = m_loc2PetscVolume[iNode];
    AOPetscToApplication(m_ao,1,&iGlobAppli);
    m_dofPtr->getNeighbourhood( glob2loc[iGlobAppli], currentVecOfNeighbour ); //currentVec is in global application numbering
    // we save only the neighbours that are on the surface
    // we take also the neighbours on different proc
    for ( std::size_t iNeigh(0); iNeigh < currentVecOfNeighbour.size(); ++iNeigh ) {
      felInt iPetsc = currentVecOfNeighbour[iNeigh];
      AOApplicationToPetsc(m_ao,1,&iPetsc);
      if ( std::find( m_glob2PetscVolume.begin(), m_glob2PetscVolume.end(), iPetsc) != m_glob2PetscVolume.end() ) {
        nodesNeighborhood[ iNode ].push_back(currentVecOfNeighbour[iNeigh]);
      }
    }
    // we update nnz
    nnz = nnz + nodesNeighborhood[ iNode ].size();
  }
  // resize of the pattern!
  // nb of rows: m_numLocalDofInterface
  // total number of non zeros
  m_boundaryPattern.resize(m_numLocalDofInterface,nnz);
  // we fill it!
  for ( int iNode = 0; iNode < m_numLocalDofInterface; iNode++) {
    m_boundaryPattern.rowPointer(iNode+1) = m_boundaryPattern.rowPointer(iNode) + nodesNeighborhood[iNode].size();
    felInt pos = 0;
    for(auto iCon = nodesNeighborhood[iNode].begin(); iCon != nodesNeighborhood[iNode].end(); iCon++) {
      // here we apply the std::unordered_map petscVolume2Glob to move the index *iCon from the global application volume ordering to
      // the global application interface ordering
      felInt iPetscVol = *iCon;
      AOApplicationToPetsc( m_ao, 1, &iPetscVol );
      m_boundaryPattern.columnIndex( m_boundaryPattern.rowPointer(iNode) + pos ) = m_petscVolume2Glob[ iPetscVol ];
      pos++;
    }
  }
}

/*! \brief It allocates a sparse petsc matrix using the boundary pattern.

  \param[in,out] theMatrix an empty matrix that will be allocated.

  It is usefull, for instance, to allocate a mass matrix on the boarder.
  It is necessary to pass a completely empty matrix.

  If the boundary pattern was not built it builds it.
*/
void DofBoundary::allocateMatrixOnBoundary( PetscMatrix& theMatrix ) {
  if ( !m_boundaryPatternAlreadyBuilt )
    this->buildBoundaryPattern();
  // only processors who actually own some nodes on the boundary
  if ( m_numLocalDofInterface > 0 ) {
    // we get the nb of procs that share the boundary nodes
    felInt size;
    MPI_Comm_size( m_boundaryComm, &size );

    // the matrix is now allocate in the same way as it is done for the system matrix
    // parallel case
    if ( size > 1 ) {

      std::vector<felInt> jCSR = m_boundaryPattern.columnIndices();
      AOApplicationToPetsc(m_aoInterface, m_boundaryPattern.numNonzeros(), jCSR.data());

      theMatrix.createAIJ(m_boundaryComm, m_numLocalDofInterface, m_numLocalDofInterface,
                                m_numGlobalDofInterface, m_numGlobalDofInterface, 0,
                                FELISCE_PETSC_NULLPTR, 0, FELISCE_PETSC_NULLPTR);
      theMatrix.mpiAIJSetPreallocationCSR(m_boundaryPattern.rowPointer().data(), jCSR.data(), nullptr);
      theMatrix.setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      theMatrix.setFromOptions();
    } // serial case
    else {

      std::size_t numRows = m_boundaryPattern.numRows();
      std::vector<felInt> nnz( numRows );
      for ( std::size_t idof = 0; idof < numRows; idof++ ) {
        nnz[idof] = m_boundaryPattern.numNonzerosInRow(idof);
      }

      theMatrix.createSeqAIJ(m_boundaryComm, numRows, numRows, 0, nnz.data());
      theMatrix.setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      theMatrix.setFromOptions();
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

PetscVector DofBoundary::allocateBoundaryVector(vectorType type)
{
  // To use this function you have to firt initialize correctly dofBoundary
  // look for instance into linearProblemNSRS.cpp
  PetscVector v;
  if ( hasDofsOnBoundary() ) {
    int numProcOnBoundary;
    MPI_Comm_size( comm(), &numProcOnBoundary);
    if ( numProcOnBoundary == 1 || type == sequential ) {
      v.createSeq( comm(), numGlobalDofInterface() );
    } else {
      v.createMPI( comm(), numLocalDofInterface(), numGlobalDofInterface() );
    }
    v.setFromOptions();
  }
  return v;
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::restrictOnBoundary(PetscVector& volumeVector, PetscVector& parallelBoundaryVector) 
{
  if ( hasDofsOnBoundary() ) {
    std::vector<double> tmpValuesBD( numLocalDofInterface(), 0.0);
    volumeVector.getValues(numLocalDofInterface(), loc2PetscVolPtr(), tmpValuesBD.data());
    parallelBoundaryVector.setValues(numLocalDofInterface(), loc2PetscBDPtr(), tmpValuesBD.data(), INSERT_VALUES);
    parallelBoundaryVector.assembly();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::extendOnVolume(PetscVector& parallelVolumeVector, PetscVector& boundaryVector) 
{
  if ( hasDofsOnBoundary() ) {
    std::vector<double> tmpValuesBD( numLocalDofInterface(), 0.0);
    boundaryVector.getValues( numLocalDofInterface(), loc2PetscBDPtr(), tmpValuesBD.data()); // Values are read from the boundary std::vector.
    parallelVolumeVector.setValues( numLocalDofInterface(), loc2PetscVolPtr(), tmpValuesBD.data(), INSERT_VALUES); // Values are written in a PARALLEL volume std::vector.
  }
  parallelVolumeVector.assembly();
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::buildListOfBoundaryPetscDofs(LinearProblem* lpb_ptr, std::vector<int> labelOfInterface, int iUnknown, int iComponent) 
{
  if ( ! m_dofBoundaryHasBeenInitialized )
    FEL_ERROR("dof boundary not yet initialized");
  std::size_t iUC(m_dofPtr->getNumGlobComp(iUnknown,iComponent));
  if ( iUC >= m_dofsHaveBeenComputed.size() ) {
    m_dofsHaveBeenComputed.resize( iUC+1, false);
  } else if ( m_dofsHaveBeenComputed[iUC] ) {
    std::stringstream msg;
    msg<<"list of boundary petsc dofs already computed for iUnknown: "<<iUnknown<<" iComp: "<<iComponent<<std::endl;
    FEL_ERROR(msg.str().c_str());
  }
  m_dofsHaveBeenComputed[iUC] = true;

  const int idVar = lpb_ptr->listUnknown().idVariable(iUnknown);
  const int iMesh = lpb_ptr->listVariable()[idVar].idMesh();
  if (FelisceParam::verbose() > 2 )
    std::cout<<"["<<MpiInfo::rankProc()<<"]starting std::unordered_map interface creation in "<<lpb_ptr->name()<<" unknown id: "<<iUnknown<<", component: "<<iComponent<<std::endl;

  std::vector<setOfPairOfIntAndPoint> globalVecOfSet;

  // some useful std::vector
  std::vector<Point*> elemPoint;
  std::vector<felInt> elemIdPoint;
  std::vector<felInt> vectorIdSupport;

  //to improve readability
  GeometricMeshRegion::Pointer mesh = lpb_ptr->meshLocal(iMesh);
  const std::vector<GeometricMeshRegion::ElementType>& bagElementTypeDomainBoundary = mesh->bagElementTypeDomainBoundary();

  //initialization of the counter numElement
  felInt numElement[ GeometricMeshRegion::m_numTypesOfElement ]; //std::vector of size 23 number of the different type of elements why we do not use a std::unordered_map
  for (int ityp=0; ityp < GeometricMeshRegion::m_numTypesOfElement; ityp++ ) {
    numElement[ityp]=0;
  }

  //========loop on the element type of boundary (e.g. triangles..not a real loop)
  for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
    GeometricMeshRegion::ElementType eltType = bagElementTypeDomainBoundary[i];

    int  typeOfFiniteElement = lpb_ptr->listVariable()[idVar].finiteElementType();
    const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
    const RefElement *refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *mesh);
    CurvilinearFiniteElement* fe = new CurvilinearFiniteElement(*refEle, *geoEle, lpb_ptr->listVariable()[idVar].degreeOfExactness());
    FEL_ASSERT(fe);
    if( FelisceParam::instance(lpb_ptr->instanceIndex()).flipNormal )
      fe->m_sign = -1;

    const int numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
    elemPoint.resize(numPointPerElt, nullptr);
    elemIdPoint.resize(numPointPerElt, 0);

    //=======loop on subregion of the surface, with different labels.
    for(auto itRef = mesh->intRefToBegEndMaps[eltType].begin(); itRef != mesh->intRefToBegEndMaps[eltType].end(); itRef++) {
      const int currentLabel = itRef->first;
      const int numEltPerLabel = itRef->second.second;

      bool Found( false ); //flag to select the correct label
      for(auto it_labelNumber = labelOfInterface.begin(); it_labelNumber != labelOfInterface.end() && !Found; ++it_labelNumber) {
        if(*it_labelNumber == currentLabel) {
          Found=true;

          setOfPairOfIntAndPoint localSetOfPointsAndIds(&Tools::lgPairComparison);

          for ( felInt iel = 0; iel < numEltPerLabel; iel++) {
            // return each id of point of the element and coordinate in two arrays:
            // elemPoint and elemIdPoint.
            lpb_ptr->setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

            // loop over all the support elements
            for (std::size_t it = 0; it < vectorIdSupport.size(); it++) {
              // get the id of the support
              felInt ielSupportDof = vectorIdSupport[it];
              for ( std::size_t iSurfDof(0); iSurfDof < (std::size_t) fe->numDof(); iSurfDof++ ) {

                felInt iDofGlobScalar;
                lpb_ptr->dof().loc2glob( ielSupportDof, iSurfDof, idVar, iComponent, iDofGlobScalar);
                AOApplicationToPetsc(m_ao,1,&iDofGlobScalar);
                Point P;
                lpb_ptr->getSupportCoordinate(eltType, numElement[eltType], iSurfDof, iUnknown, P);
                localSetOfPointsAndIds.insert(std::make_pair(iDofGlobScalar,P) );
              }
            }
            numElement[eltType]++;
          }

          if ( int(m_localVecOfSet.size()) <= currentLabel ) {
            m_localVecOfSet.resize(currentLabel+1, setOfPairOfIntAndPoint(&Tools::lgPairComparison) );
          }
          m_localVecOfSet[currentLabel] = localSetOfPointsAndIds;
        }
      }
      if (!Found)
        numElement[eltType] += numEltPerLabel;
    }
  
    delete fe;
  }//end of the loop

  const int idUnknComp = lpb_ptr->dof().getNumGlobComp(iUnknown,iComponent);
  for(auto it_labelNumber = labelOfInterface.begin(); it_labelNumber != labelOfInterface.end(); ++it_labelNumber) {

    if ( FelisceParam::verbose() > 3 )
      std::cout<<"["<<MpiInfo::rankProc()<<"] starting gathering phase"<<std::endl;

    if ( int( m_localVecOfSet.size() ) <= *it_labelNumber )
      m_localVecOfSet.resize(*it_labelNumber+1, setOfPairOfIntAndPoint(&Tools::lgPairComparison) );

    if ( int( globalVecOfSet.size() ) <= *it_labelNumber )
      globalVecOfSet.resize(*it_labelNumber+1, setOfPairOfIntAndPoint(&Tools::lgPairComparison) );

    Tools::allGatherSetPair(m_localVecOfSet[*it_labelNumber],globalVecOfSet[*it_labelNumber]);
    const auto& r_global_vec = globalVecOfSet[*it_labelNumber];

    if ( FelisceParam::verbose() > 3 )
      std::cout<<"["<<MpiInfo::rankProc()<<"] ending gathering phase"<<std::endl;

    int cPoint=0;

    for(auto it= r_global_vec.begin(); it!= r_global_vec.end(); ++it) {
      m_listOfBoundaryPetscDofs[ idUnknComp ][*it_labelNumber][cPoint] = it->first;
      cPoint++;
    }
    cPoint=0;
    if ( m_mapLab2AllPoints[*it_labelNumber].size() == 0 ) {
      for(auto it= r_global_vec.begin(); it!= r_global_vec.end(); ++it) {
        if (cPoint==0) {
          m_mapLab2FirstPoint[*it_labelNumber] = it->second;
        }
        m_mapLab2AllPoints[*it_labelNumber].push_back(it->second);
      }
      cPoint++;
    }

  }
  if ( FelisceParam::verbose() > 3 )
    std::cout<<"["<<MpiInfo::rankProc()<<"] ended"<<std::endl;
  if ( FelisceParam::verbose() > 3 )
    std::cout<<"["<<MpiInfo::rankProc()<<"] "<<m_listOfBoundaryPetscDofs[idUnknComp].size()<<" labels, for "<<idUnknComp<<" id unknown and component"<<std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::writeSurfaceInterpolatorFile(int iUnknown, int numComp, std::string filename, std::string folder) const {
  // iUnknwon: index of the unknown we want to interpolate.
  // numComp: number of components to distinguish between scalars (1) and vectors (2 or 3).

  // Goal of the function is to write a list of the points, belonging to the surface,
  // that support a degree of freedom (or more) for this unknown.

  // ids: for each point we store the id (in globalApplicationOrderingBD) or the ids in the case of a std::vector(that's why a std::set is used.)
  std::vector<std::vector<int> > ids;
  // labels: for each point we save the surfaceLabel.
  // if he belongs to more than one label we store the first one found.
  std::vector<felInt> labels;
  // points: for each point we actually store the Point.
  std::vector<Point> points;

  // Checking that everything was allocated
  for ( felInt iComp(0); iComp<numComp; ++iComp) {
    std::size_t iUC=m_dofPtr->getNumGlobComp(iUnknown, iComp);
    if ( iUC >= m_dofsHaveBeenComputed.size() || !m_dofsHaveBeenComputed[iUC] ) {
      std::stringstream msg;
      msg<<"Error in writing surface interpolator file for iUnknown: "<<iUnknown<<" and iComp: "<<iComp<<std::endl;
      FEL_ERROR(msg.str().c_str());
    }
  }

  // We do a first loop on all the points considering only the first component.
  // The idea is that we need to build a sort of index for the points and it is not easy to do it
  // since the point class does not work very well as key-value for a std::unordered_map.

  felInt iUC0 = m_dofPtr->getNumGlobComp(iUnknown, /*iComp*/0);

  // The idea is that for all the components we will loop over the labels and their points in the same way.
  // When looping for the first component we associate an incremental index (cPointUnique) to the points
  // that we find. If passing to a different label we find the same point twice we are able to tell it since,
  // for the same component the point will be associated to the same degreeOfFreedom.
  // The list of already found deegreesOfFreedom is saved in a std::set (alreadyFound).
  // For each point we come across either we put the cPointUnique index in the std::vector idForiUC0
  // or we put (-1) if the point was already found in a different label.
  int cPointUnique(0);
  std::set<int> alreadyFound;
  std::vector<int> idForiUC0;

  // For each label
  for ( auto itLabel = m_listOfBoundaryPetscDofs.at(iUC0).begin(); itLabel != m_listOfBoundaryPetscDofs.at(iUC0).end(); ++itLabel) {
    // Point index label-based
    int cpoint(0);
    // for each point
    for ( auto itDof =  itLabel->second.begin(); itDof != itLabel->second.end(); ++itDof) {
      FEL_ASSERT(cpoint==itDof->first);
      // We insert the dof id in the list
      auto ret = alreadyFound.insert(m_petscVolume2Glob.at(itDof->second));
      // We check if we already found this dof.
      if ( !ret.second ) {
        // In this case we do nothing
        // and we add a minus one to idForiUC0
        idForiUC0.push_back(-1);
      } else {
        // In case of a new point
        // we add the point index
        idForiUC0.push_back(cPointUnique);
        cPointUnique++;
        // we save the point
        points.push_back(m_mapLab2AllPoints.find(itLabel->first)->second[cpoint]);
        // ...the label
        labels.push_back(itLabel->first);
        // ...and the dof id
        std::vector<int> tmp;
        tmp.push_back(m_petscVolume2Glob.at(itDof->second));
        ids.push_back(tmp);
      }
      //increase the index label-based
      cpoint++;
    }
  }

  // Now we are ready for a second round:
  // if there are more components to interpolate
  // we loop again in the same way as we just did for the first component

  for ( felInt iComp(1); iComp<numComp; ++iComp) {
    // This counter will be used to acces idForiUC0 and either extract -1
    // or extract the cPointUnique index.
    int count(0);
    std::size_t iUC=m_dofPtr->getNumGlobComp(iUnknown, iComp);
    // for each label
    for ( auto itLabel = m_listOfBoundaryPetscDofs.at(iUC).begin(); itLabel != m_listOfBoundaryPetscDofs.at(iUC).end(); ++itLabel) {
      // counter label-based
      int cpoint=0;
      // loop over the points of this label
      for ( auto itDof =  itLabel->second.begin(); itDof != itLabel->second.end(); ++itDof) {
        // verify if we have to skip this point
        if ( idForiUC0[count] >= 0 ) {
          // insert the dof id of the current component
          ids[idForiUC0[count]].push_back(m_petscVolume2Glob.at(itDof->second));
          FEL_ASSERT(Tools::equalTol(points[idForiUC0[count]].x(),m_mapLab2AllPoints.find(itLabel->first)->second[cpoint].x(),1e-16));
          FEL_ASSERT(Tools::equalTol(points[idForiUC0[count]].y(),m_mapLab2AllPoints.find(itLabel->first)->second[cpoint].y(),1e-16));
          FEL_ASSERT(Tools::equalTol(points[idForiUC0[count]].z(),m_mapLab2AllPoints.find(itLabel->first)->second[cpoint].z(),1e-16));
        }
        count++;
        cpoint++;
      }
    }
  }
  // Opening the file
  std::stringstream filenameFull,cmd;
  cmd<<"mkdir -p "<<folder;
  int ierr = system(cmd.str().c_str());
  if (ierr>0) FEL_ERROR("error in execution of " + cmd.str() );
  filenameFull<<folder <<"/"<< filename;
  std::ofstream surfaceInterpolatorFile;
  surfaceInterpolatorFile.open(filenameFull.str().c_str());
  if ( ! surfaceInterpolatorFile.is_open() ) FEL_ERROR("not able to open the file");

  // Writing the file
  surfaceInterpolatorFile<<points.size()<<std::endl;
  for ( std::size_t curPoint(0); curPoint<points.size(); ++curPoint) {
    surfaceInterpolatorFile<<numComp<<" "<<points[curPoint].x()<<" "<<points[curPoint].y()<<" "<<points[curPoint].z()<<" ";
    surfaceInterpolatorFile<<labels[curPoint]<<" ";
    FEL_ASSERT((std::size_t)numComp==ids[curPoint].size());
    for(auto it=ids[curPoint].begin(); it!= ids[curPoint].end(); it++) {
      surfaceInterpolatorFile<<*it<<" ";
    }
    surfaceInterpolatorFile<<std::endl;
  }
  surfaceInterpolatorFile.close();
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::exportInterfacePoints(int iUnknown, int iComp) const {
  std::size_t iUC=m_dofPtr->getNumGlobComp(iUnknown, iComp);
  if ( iUC >= m_dofsHaveBeenComputed.size() || !m_dofsHaveBeenComputed[iUC] ) {
    std::stringstream msg;
    msg<<"trying to export points for iUnknown: "<<iUnknown<<" and iComp: "<<iComp<<std::endl;
    FEL_ERROR(msg.str().c_str());
  }
  /*! a matlab file "interface.m" is written into the result folder
    to import it is enough to run such file in a matlab shell
    it will create a matrix called "interfacePoints" with
    three columns and a row for each point
    the order of the point is the order of the application
  */
  std::unordered_map<int,Point> id2Point;
  for(auto itLabel = m_listOfBoundaryPetscDofs.at(iUC).begin(); itLabel != m_listOfBoundaryPetscDofs.at(iUC).end(); ++itLabel) {
    int cpoint(0);
    for(auto itDof =  itLabel->second.begin(); itDof != itLabel->second.end(); ++itDof) {
      id2Point[itDof->second]=m_mapLab2AllPoints.find(itLabel->first)->second[cpoint];
      if ( FelisceParam::verbose() > 2 )
        std::cout<<" id: "<<itDof->second<<" point "<<m_mapLab2AllPoints.find(itLabel->first)->second[cpoint]<<std::endl;
      cpoint++;
    }
  }
  std::stringstream filename,cmd;
  cmd<<"mkdir -p "<<FelisceParam::instance().resultDir;
  int ierr = system(cmd.str().c_str());
  if (ierr>0) {
    FEL_ERROR("error in execution of " + cmd.str() );
  }

  filename<< FelisceParam::instance().resultDir << "interface.m";
  std::ofstream interfaceFile;
  interfaceFile.open(filename.str().c_str());
  if ( ! interfaceFile.is_open() )
    FEL_ERROR("not able to open the file");
  interfaceFile<<"interfacePoints = [ ";
  for ( int i(0); i<m_numGlobalDofInterface; ++i) {
    if ( id2Point.count(m_glob2PetscVolume[i]) > 0 ) {
      if ( FelisceParam::verbose() > 2 )
        std::cout<<" id2: "<<m_glob2PetscVolume[i]<<" point "<<id2Point[m_glob2PetscVolume[i]]<<std::endl;
      Point P=id2Point[m_glob2PetscVolume[i]];
      interfaceFile<<P.x()<<" , "<<P.y()<<" , "<<P.z()<<" ; ";
    }
  }
  interfaceFile<<"];"<<std::endl;
  interfaceFile.close();
}

// for compatibility with previous definition of buildBoundaryVolumeMapping, but also
// to have a better code in the linear problems:
void DofBoundary::buildBoundaryVolumeMapping( int iUnknown, int iComponent ) {
  this->buildBoundaryVolumeMapping( iUnknown, std::vector<int>(1,iComponent) );
}

// One mapping at the time is allowed
// it means that you choose an unknown and a (std::set of) component and you create a mapping!
// for instance in Navier Stokes you can build mapping between the ?whole? volume problem ( vel x 3 + pre ) and
// one component of the velocity OR the pressure on the interface.
void DofBoundary::buildBoundaryVolumeMapping( int iUnknown, std::vector<int> components ) {

  if ( m_volumeInterfaceMappingBuilt ) {
    #ifndef NDEBUG
    std::stringstream msg;
    msg<<"trying to call builBoundaryVolumeMapping again with iUnknwon: "<<iUnknown<<" and component(s): ";
    for(auto itComponent=components.begin(); itComponent!=components.end(); ++itComponent) {
      msg<<*itComponent<<" ";
    }
    msg<<std::endl;
    FEL_WARNING(msg.str().c_str());
    #endif

    return;
  }
  if ( ! m_dofBoundaryHasBeenInitialized )
    FEL_ERROR("dof boundary not yet initialized");
  m_volumeInterfaceMappingBuilt=true;

  std::set<int> setGlobVolPetscIds;
  for (std::size_t idComponent(0); idComponent<components.size(); ++idComponent) {
    int iComponent = components[idComponent];
    int iUnknComp = m_dofPtr->getNumGlobComp(iUnknown, iComponent);

    // we check some basic assumptions
    FEL_CHECK( m_listOfBoundaryPetscDofs[iUnknComp].size() > 0, "not even one label stored in m_listOfBoundaryPetscDofs" );
    FEL_CHECK( m_listOfBoundaryPetscDofs[iUnknComp].begin()->second.size() > 0, "the first label stored in the std::unordered_map is empty" );
    if ( FelisceParam::verbose() > 2) {
      PetscPrintf( MpiInfo::petscComm(), "Starting creation of all the mappings to define petsc objects in the interface\n");
    }

    // ===========================================================
    // the global volume petsc ids are stored in the m_listOfBoundaryPetscDofs
    // container depending on their label, therefore some of those
    // ids could be repeted if they are at the interface
    // between different labels
    // we "gather" all the ids in one std::set to destroy
    // duplicates and to sort them
    for(auto itLabel = m_listOfBoundaryPetscDofs[iUnknComp].begin(); itLabel != m_listOfBoundaryPetscDofs[iUnknComp].end(); ++itLabel) {
      for(auto itDof =  itLabel->second.begin(); itDof != itLabel->second.end(); ++itDof) {
        setGlobVolPetscIds.insert(itDof->second);
      }
    }
  }
  // ===========================================================
  //                          m_glob2PetscVolume               =
  // ===========================================================
  // TODO: this is simply a copy of the std::set into a std::vector, there is for sure a better way of doing it.
  // we allocate the std::unordered_map
  // applicationInterface2PetscVol (m_glob2PetscVolume), actually it is a std::vector of integers and
  // it maps:
  //           ( 0 , m_numGlobalDofInterface - 1) --> (???) \in global volume petsc ids
  // so that from the global interface application id you can get the global volume petsc id
  for(auto itDof =  setGlobVolPetscIds.begin(); itDof != setGlobVolPetscIds.end(); ++itDof) {
    m_glob2PetscVolume.push_back(*itDof);              // global petsc volume id            = m_glob2PetscVolume[global application interface id]
  }
  setGlobVolPetscIds.clear();
  m_numGlobalDofInterface = m_glob2PetscVolume.size();


  // ===========================================================
  //                          m_petscVolume2Glob               =
  // ===========================================================
  int cpoint=0;
  for(auto petscVolId=m_glob2PetscVolume.begin(); petscVolId != m_glob2PetscVolume.end(); ++petscVolId, ++cpoint) {
    int aus( *petscVolId );
    m_petscVolume2Glob[ aus  ] = cpoint;
  }

  // ==============================================================
  //           m_loc2GlobInterface & m_loc2PetscVolume           =
  // ==============================================================
  //  Here we create the std::vector,
  //  local to the proc, that contains the global application
  //  ids of the dof at the interface, i.e,
  //  m_loc2GlobInterface:
  //            ( 0 , m_numLocalDofInterface - 1 ) --> ( 0 , m_numGlobalDofInterface - 1 )
  // we create an auxiliary std::vector initialized with the global
  // volume petsc ids and then we use m_ao
  // to replace those values with global colume application ids
  std::vector<felInt> applicationIdsVolume(m_glob2PetscVolume);
  AOPetscToApplication(m_ao,m_numGlobalDofInterface,&applicationIdsVolume[0]);
  // this is done because we want to check if those dofs belong
  // to this proc or not and the std::vector m_dofPart
  // is defined on the global volume application ids not the petsc ids
  // at the same time we define a variable startCounter
  // which is equal to the number of dofs in the processors
  // with a lower rank than the current
  // it is zero for the proc 0
  // and it should be numGlob-numLoc-1 for the last proc
  felInt startCounter(0);
  for ( felInt iGlobDof(0); iGlobDof < m_numGlobalDofInterface; ++iGlobDof ) {
    felInt dofProc( (*m_dofPartPtr)[ applicationIdsVolume[iGlobDof] ]);
    if (  dofProc  == MpiInfo::rankProc() ) {
      m_loc2GlobInterface.push_back(iGlobDof);
    }
    if ( dofProc < MpiInfo::rankProc() ) {
      ++startCounter;
    }
  }
  applicationIdsVolume.clear();
  m_numLocalDofInterface = m_loc2GlobInterface.size();
  for(auto iLocDof = m_loc2GlobInterface.begin(); iLocDof != m_loc2GlobInterface.end(); ++iLocDof ) {
    m_loc2PetscVolume.push_back(m_glob2PetscVolume[*iLocDof]);
  }

  MPI_Comm_split(MpiInfo::petscComm(), ( m_numLocalDofInterface > 0 ) ? 1 : 0, MpiInfo::rankProc(), &m_boundaryComm);
  int rankB,sizeB;
  MPI_Comm_rank(m_boundaryComm, &rankB);
  MPI_Comm_size(m_boundaryComm, &sizeB);
  if (FelisceParam::verbose()>2)
    std::cout<<"["<<MpiInfo::rankProc()<<"] "<<"rank on the boarder "<< rankB << " size of this group " << sizeB << std::endl;

  // ==============================================================
  //                    m_aoInterface                             =
  // ==============================================================
  // we create the std::vector petscIds
  // which contains ( startCounter, startCounter + 1, ..., startCounter + numLocDof -1 )
  std::vector<felInt> petscIds;
  petscIds.reserve(m_numLocalDofInterface);

  for ( felInt iLocDof(0); iLocDof<m_numLocalDofInterface; ++iLocDof ) {
    petscIds.push_back(startCounter + iLocDof);
  }
  // we build m_aoInterface
  // this will allow you to move from the global petsc ordering defined in petscIds and
  // the global application ordering.
  // consider that in serial this is the identity mapping
  // but in parallel nodes are contiguous in petsc ordering
  AOCreateBasic(m_boundaryComm, m_numLocalDofInterface, &m_loc2GlobInterface[0], &petscIds[0], &m_aoInterface);
  petscIds.clear();


  // ==============================================================
  //                         m_loc2PetscInterface                 =
  // ==============================================================
  // from local index, directly to the petsc index of the interface
  for ( felInt iDofInterface(0); iDofInterface < m_numLocalDofInterface; ++iDofInterface) {
    m_loc2PetscInterface.push_back(m_loc2GlobInterface[iDofInterface]);
  }
  AOApplicationToPetsc(m_aoInterface,m_numLocalDofInterface,&m_loc2PetscInterface[0]);

//     bool m_exportInterface=true;
//     if ( m_exportInterface ) {
    if ( MpiInfo::rankProc() == 0 ) {
      this->exportInterfacePoints( iUnknown, components[0] );//we export the points only for the first component.
    }
//     }
  if ( FelisceParam::verbose() > 2 ) {
    displayBoundaryVolumeMapping();
  }
}

/*! \brief post-process function used in the examples
  It writes a readable file to be plot in python.
  */
void DofBoundary::BoundaryVolumeMappingForExample() const {
  if ( ! m_volumeInterfaceMappingBuilt )
    FEL_ERROR("volume interface mapping not yet build");

  //! The master processor start to write:
  if ( MpiInfo::rankProc() == 0 ) {
    //! a file BVMapping.dat is opened
    std::stringstream cmd;
    cmd<<"mkdir -p "<<FelisceParam::instance().resultDir;
    int ierr = system( cmd.str().c_str() );
    if (ierr>0) {
      FEL_ERROR("error in execution of " + cmd.str() );
    }
    std::stringstream filename;
    filename << FelisceParam::instance().resultDir << "BVMapping.dat";

    std::ofstream outFile;
    outFile.open(filename.str().c_str());
    //! we write the header of the first part
    outFile<<"# Total Number of Processors"<<std::endl;
    outFile<<MpiInfo::numProc()<<std::endl;
    outFile<<"# Number of boundary dofs"<<std::endl;
    outFile<<m_numGlobalDofInterface<<std::endl;

    // for each id we get the corresponding point  //TODO create a function for that.
    const std::size_t iUC=m_dofPtr->getNumGlobComp(1, 0);//TODO
    std::unordered_map<int,Point> id2Point;
    for(auto itLabel = m_listOfBoundaryPetscDofs.at(iUC).begin(); itLabel != m_listOfBoundaryPetscDofs.at(iUC).end(); ++itLabel) {
      int cpoint(0);
      for(auto itDof =  itLabel->second.begin(); itDof != itLabel->second.end(); ++itDof) {
        id2Point[itDof->second]=m_mapLab2AllPoints.find(itLabel->first)->second[cpoint];
        cpoint++;
      }
    }

    // a second line for humans
    //! We write a table like this:
    //! PointsCoordinate, boundaryApplicationOrdering (BAO), boundaryPetscOrdering (BPO), volumePetscOrdering (VPO), volumeApplicationOrdering (VAO)
    outFile<<"# PointsCoordinate, boundaryApplicationOrdering (BAO), boundaryPetscOrdering (BPO), volumePetscOrdering (VPO), volumeApplicationOrdering (VAO)"<<std::endl;
    for ( int cPoint(0); cPoint < m_numGlobalDofInterface; ++cPoint ) {
      Point P=id2Point[m_glob2PetscVolume[cPoint]];
      felInt BPO=cPoint;                      AOApplicationToPetsc( m_aoInterface, 1, &BPO );
      felInt VAO=m_glob2PetscVolume[cPoint];  AOPetscToApplication( m_ao, 1, &VAO);
      outFile
        << P.x()                      <<" , "
        << P.y()                      <<" , "
        << P.z()                      <<" , "
        << cPoint                     <<" , "
        << BPO                        <<" , "
        << m_glob2PetscVolume[cPoint] <<" , "
        << VAO                        << std::endl;
    }
    outFile.close();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::displayBoundaryVolumeMapping() const {
  if ( ! m_volumeInterfaceMappingBuilt )
    FEL_ERROR("volume interface mapping not yet build");
  const int N(25); // used only for printing information in debug
  typedef std::stringstream aStream;
  aStream master;
  master << "m_glob2PetscVolume maps the global indeces in the application ordering (0,...," << m_numGlobalDofInterface-1 <<")" << std::endl;
  master << " to the corresponding indeces of the volume in the petsc ordering (" << m_glob2PetscVolume[0]<<",...,"<<m_glob2PetscVolume[m_numGlobalDofInterface-1]<<")"<<std::endl;
  PetscPrintf( MpiInfo::petscComm(), "%s",master.str().c_str() );
  MPI_Barrier(MpiInfo::petscComm());
  if ( FelisceParam::verbose() > 3) {
    aStream lineOne, lineTwo;
    lineOne<<"The first "<<N<<" mapped dofs being: "<<std::endl;
    for ( int ii(0); ii < N && ii < m_numGlobalDofInterface; ++ii) {
      lineOne << ii                     << '\t' ;
      lineTwo << m_glob2PetscVolume[ii] << '\t' ;
    }
    lineOne << std::endl;
    lineTwo << std::endl;
    PetscPrintf( MpiInfo::petscComm(), "%s",lineOne.str().c_str() );
    MPI_Barrier(MpiInfo::petscComm());
    PetscPrintf( MpiInfo::petscComm(), "%s",lineTwo.str().c_str() );
    MPI_Barrier(MpiInfo::petscComm());
  }
  master.str("");
  master.clear();
  master<<"Number of local dof (for each proc) on the interface is ... "<<std::endl;
  PetscPrintf(MpiInfo::petscComm(),"%s", master.str().c_str());
  MPI_Barrier(MpiInfo::petscComm());
  std::cout<<"["<<MpiInfo::rankProc()<<"] numLoc ("<<m_numLocalDofInterface<<"/"<<m_numGlobalDofInterface<<") (local/global)"<<std::endl;
  MPI_Barrier(MpiInfo::petscComm());
  master.str("");
  master.clear();
  master<<"m_loc2GlobInterface let you go from local indexing to global ids in the application ordering"<<std::endl;
  master<<"---------info are displayed only for the last proc------------"<<std::endl;
  PetscPrintf( MpiInfo::petscComm(),"%s", master.str().c_str() );
  MPI_Barrier(MpiInfo::petscComm());
  if ( MpiInfo::rankProc() == MpiInfo::numProc() - 1) {
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] mapping from local (0,...,"<<m_numLocalDofInterface-1<<")"<<std::endl;
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] The first "<< ( (N<m_numLocalDofInterface) ? N : m_numLocalDofInterface ) <<" being: "<<std::endl;
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] ";
    for ( int ii(0); ii<N && ii<m_numLocalDofInterface; ++ii ) {
      std::cout<<ii<<'\t';
    }
    std::cout<<std::endl;
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] ";
    for ( int ii(0); ii<N && ii<m_numLocalDofInterface; ++ii ) {
      std::cout<<m_loc2GlobInterface[ii]<<'\t';
    }
    std::cout<<std::endl;
  }
  MPI_Barrier(MpiInfo::petscComm());
  master.str("");
  master.clear();
  master<<"m_loc2PetscVolume let you go from local indexing to global ids in the volume petsc ordering"<<std::endl;
  master<<"---------info are displayed only for the last proc------------"<<std::endl;
  PetscPrintf( MpiInfo::petscComm(),"%s", master.str().c_str() );
  MPI_Barrier(MpiInfo::petscComm());
  if ( MpiInfo::rankProc() == MpiInfo::numProc() -1 ) {
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] mapping from local (0,...,"<<m_numLocalDofInterface-1<<")"<<std::endl;
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] The first "<<( ( N < m_numLocalDofInterface )?N:m_numLocalDofInterface)<<" being: "<<std::endl;
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] ";
    for ( int ii(0); ii<N && ii<m_numLocalDofInterface; ++ii ) {
      std::cout<<ii<<'\t';
    }
    std::cout<<std::endl;
    std::cout<<"-----------["<<MpiInfo::rankProc()<<"] ";
    for ( int ii(0); ii<N && ii<m_numLocalDofInterface; ++ii ) {
      std::cout<<m_loc2PetscVolume[ii]<<'\t';
    }
    std::cout<<std::endl;
  }
  MPI_Barrier(MpiInfo::petscComm());
  if ( FelisceParam::verbose() > 2 ) {
    master.str("");
    master.clear();
    master<<" m_aoInterface "<<std::endl;
    PetscPrintf(MpiInfo::petscComm(), "%s",master.str().c_str());
    MPI_Barrier(MpiInfo::petscComm());
    AOView(m_aoInterface,PETSC_VIEWER_STDOUT_WORLD);
  }
  master.str("");
  master.clear();
  master<<"m_loc2PetscInterface let you go from local indexing to global indexing petsc ordering"<<std::endl;
  PetscPrintf( MpiInfo::petscComm(), "%s",master.str().c_str() );
  MPI_Barrier(MpiInfo::petscComm());
  if ( FelisceParam::verbose() > 2 ) {
    master.str("");
    master.clear();
    master<<"----------info are displayed only for the last proc"<<std::endl;
    PetscPrintf( MpiInfo::petscComm(), "%s",master.str().c_str() );
    MPI_Barrier(MpiInfo::petscComm());
    if ( MpiInfo::rankProc() == MpiInfo::numProc() - 1 ) {
      std::cout<<"-----------["<<MpiInfo::rankProc()<<"] mapping from local (0,...,"<<m_numLocalDofInterface-1<<")"<<std::endl;
      std::cout<<"-----------["<<MpiInfo::rankProc()<<"] The first "<<( ( N < m_numLocalDofInterface )?N:m_numLocalDofInterface )<<" being: "<<std::endl;
      std::cout<<"-----------["<<MpiInfo::rankProc()<<"] ";
      for ( int ii(0); ii<N && ii<m_numLocalDofInterface; ++ii ) {
        std::cout<<ii<<'\t';
      }
      std::cout<<std::endl;
      std::cout<<"-----------["<<MpiInfo::rankProc()<<"] ";
      for ( int ii(0); ii<N && ii<m_numLocalDofInterface; ++ii ) {
        std::cout<<m_loc2PetscInterface[ii]<<'\t';
      }
      std::cout<<std::endl;
    }
    MPI_Barrier(MpiInfo::petscComm());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void DofBoundary::unRoll( felInt iUnkComp ) {
  if ( ! m_dofsHaveBeenComputed[iUnkComp] )
    FEL_ERROR("need to calculate those dofs");
  if ( m_unRolledList.size() <= std::size_t( iUnkComp ) ) {
    m_unRolledList.resize(iUnkComp+1);
  }
  if ( m_unRolledList[iUnkComp].size() > 0 )
    return;
  else {
    for(auto it_label = m_listOfBoundaryPetscDofs[iUnkComp].begin(); it_label != m_listOfBoundaryPetscDofs[iUnkComp].end(); ++it_label ) {
      for(auto it_point = it_label->second.begin(); it_point != it_label->second.end(); ++it_point) {
        m_unRolledList[iUnkComp].insert( it_point->second );
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool DofBoundary::isOnSurface( felInt iUnkComp, felInt petscDof ) {
  if ( m_unRolledList.size() <= std::size_t( iUnkComp ) ) {
    this->unRoll(iUnkComp);
  }
  if ( m_unRolledList[iUnkComp].size() == std::size_t(0) ) {
    this->unRoll(iUnkComp);
  }
  auto it = m_unRolledList[iUnkComp].find( petscDof );
  return it != m_unRolledList[iUnkComp].end();
}

}
