//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J. Foulon
//

/*!
 \file    variable.cpp
 \author  J. Foulon
 \date    07/10/2010
 \brief   File contains Variable implementation
 */

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/variable.hpp"

namespace felisce {

  Variable::Variable():
    m_boolInitialize(false)
  {}

  /***********************************************************************************/
  /***********************************************************************************/

  void Variable::initialize(const std::size_t iVar, std::size_t instanceIndex)
  {
    const auto& r_instance = FelisceParam::instance(instanceIndex);
    m_name = r_instance.nameVariable[iVar];
    m_physicalVariable = r_instance.physicalVariable[iVar];
    m_numComponent = 0;
    m_idMesh = -1;
    m_finiteElementType = r_instance.typeOfFiniteElement[iVar];
    if (r_instance.decomposePlaneTransverse[iVar]) {
      m_degreeOfExactness.resize(3);
      m_degreeOfExactness[1] = static_cast<DegreeOfExactness>(r_instance.planeDegreeOfExactness[iVar]);
      m_degreeOfExactness[2] = static_cast<DegreeOfExactness>(r_instance.transverseDegreeOfExactness[iVar]);
    }
    m_degreeOfExactness[0] = static_cast<DegreeOfExactness>(r_instance.degreeOfExactness[iVar]);
    m_boolInitialize = true;
  }

  /***********************************************************************************/
  /***********************************************************************************/

  void Variable::setNumComponent(const std::size_t nComp) 
  {
    if (!m_boolInitialize) {
      FEL_ERROR("Your variable is not initialized.");
    }
    m_numComponent = nComp;
  }

  /***********************************************************************************/
  /***********************************************************************************/

  void Variable::setMeshId(const std::size_t idMesh)
  {
    if (!m_boolInitialize) {
      FEL_ERROR("Your variable is not initialized.");
    }
    m_idMesh = idMesh;
  }

  /***********************************************************************************/
  /***********************************************************************************/

  /// print function.
  void Variable::print(int verbose, std::ostream& outstr) const 
  {
    if (!m_boolInitialize) {
      FEL_ERROR("Your variable is not initialized.");
    }

    if (verbose > 0) {
      outstr << "<" << m_name << ">";
      if (verbose > 1) {
        outstr << " with " << m_numComponent << " components, on a finite element of type " << m_finiteElementType
                << ", with degree of exactness: " << m_degreeOfExactness[0];
      }
      outstr << std::endl;
    }
  }
}
