//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau
//

#ifndef _BOUNDARYCONDITION_HPP
#define _BOUNDARYCONDITION_HPP

// System includes
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <utility>

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "DegreeOfFreedom/variable.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Solver/linearProblem.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class BoundaryCondition 
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Point
  FELISCE_CLASS_POINTER_DEFINITION(BoundaryCondition);

  /// Function definition
  typedef double (* BoundaryConditionfunction_pointer)(double x, double y, double z, double t);

  /// The element type definition
  typedef GeometricMeshRegion::ElementType ElementType;
  
  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor
  BoundaryCondition(
    const TypeOfBoundaryCondition& typeBoundaryCondition,
    const TypeValueOfBoundaryCondition& typeValueBoundaryCondition,
    const int label,
    const Variable& variable,
    const Component& component
    );

  /// Constructor name label
  BoundaryCondition(
    const TypeOfBoundaryCondition& typeBoundaryCondition,
    const TypeValueOfBoundaryCondition& typeValueBoundaryCondition,
    const std::string& nameLabel,
    const Variable& variable,
    const Component& component
    );

  /// Constructor label
  BoundaryCondition(
    const std::string& typeBoundaryCondition,
    const std::string& typeValueBoundaryCondition,
    const int label,
    const Variable& variable,
    const std::string& component
    );

  /// Constructor label vector
  BoundaryCondition(
    const std::string& typeBoundaryCondition,
    const std::string& typeValueBoundaryCondition,
    const std::vector<int>& label,
    const Variable& variable,
    const std::string& component
    );

  /// Constructor with name label
  BoundaryCondition(
    const std::string& typeBoundaryCondition,
    const std::string& typeValueBoundaryCondition,
    const std::string& nameLabel,
    const Variable& variable,
    const std::string& component
    );

  /// default destructor
  ~BoundaryCondition() = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  void determineComponent();

  static int determineNumComponent(const std::string& cp);

  void initMap();

  /**
   * @brief Set the value for constant (in time AND space) BC.
   * @param[in] value The array with the constant value for each component of the BC
   */
  void setValue(const std::vector<double>& value);

  template <class FunctorXYZ>
  void setValue(LinearProblem* linearProblem, const FunctorXYZ& functorXYZ);

  template <class FunctorXYZ>
  void setValueVec(LinearProblem* linearProblem, const FunctorXYZ& functorXYZ);

  template <class FunctorXYZ>
  void setValueVec(LinearProblem* linearProblem, const FunctorXYZ& functorXYZ, double param);

  template <class FunctorXYZT>
  void setValue(LinearProblem* linearProblem, const FunctorXYZT& functorXYZT, double t);

  template <class FunctorT>
  void setValueT(LinearProblem* linearProblem, const FunctorT& functorT, double t);

  void setUnknown(const int iUnknown) {
    m_idUnknown = iUnknown;
  }

  ///@}
  ///@name Access
  ///@{

  inline const TypeOfBoundaryCondition& typeBC() const {
    return m_typeBC;
  }

  inline const TypeValueOfBoundaryCondition& typeValueBC() const {
    return m_typeValueBC;
  }

  inline const Variable& getVariable() const {
    return m_variable;
  }

  inline const int& getUnknown() const {
    return m_idUnknown;
  }

  inline int& getUnknown() {
    return m_idUnknown;
  }

  inline const std::string& nameLabel() const {
    return m_nameLabel;
  }

  inline const std::unordered_set<int>& listLabel() const {
    return m_listLabel;
  }

  inline std::unordered_set<int>& listLabel() {
    return m_listLabel;
  }

  inline const std::set<int>& getComp() const {
    return m_numComp;
  }
  
  inline const std::vector<double>& ValueBCInSupportDof() const {
    return m_valueBCInSupportDof;
  }

  inline std::vector<double>& ValueBCInSupportDof() {
    return m_valueBCInSupportDof;
  }

  inline const std::vector< std::pair<felInt,felInt> >& idEltAndIdSupport() const {
    return m_idEltAndIdSupport;
  }
  inline std::vector< std::pair<felInt,felInt> >& idEltAndIdSupport() {
    return m_idEltAndIdSupport;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose = 0, std::ostream& outstr = std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  std::unordered_map<std::string, TypeOfBoundaryCondition > nameTypeBCToEnumTypeBC;                /// The name type to enum
  std::unordered_map<std::string, TypeValueOfBoundaryCondition > nameValueTypeBCToEnumValueTypeBC; /// Value type to enum
  std::unordered_map<std::string, Component > nameComponentBCToEnumComponentBC;                    /// Name component type to enum 
  
  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  TypeOfBoundaryCondition m_typeBC;                          /// Dirichlet, Neumann, NeumannNormal, Robin, RobinNormal, EmbedFSI, EssentialLumpedModelBC, NaturalLumpedModelBC, BackflowStab
  TypeValueOfBoundaryCondition m_typeValueBC;                /// Constant, Vector, FunctionS, FunctionT, FunctionTS, EnsightFile
  std::unordered_set<int> m_listLabel;                       /// List of labels
  std::string m_nameLabel;                                   /// Label name
  Variable m_variable;                                       /// Variable concern by this BoundaryCondition
  int m_idUnknown;                                           /// Id of the unknown concern by the variable
  Component m_component;                                     /// Component
  std::set<int> m_numComp;                                   /// List of components concern for this variable
  std::vector<std::pair<felInt,felInt>> m_idEltAndIdSupport; /// Support list
  std::vector<double> m_valueBCInSupportDof;                 /// Value in support DoF
};
///@}
///@name Type Definitions
///@{

template <class FunctorXYZ>
void BoundaryCondition::setValue(LinearProblem* linearProblem, const FunctorXYZ& functorXYZ) {
  FEL_CHECK(m_typeValueBC == FunctionS, "Error: the type of this BC should be a FunctionS")
  Point pt;
  m_valueBCInSupportDof.resize(m_idEltAndIdSupport.size(), 0.);
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++) {
    linearProblem->getSupportCoordinate(m_idEltAndIdSupport[iSupportDofBC].first,
                                        m_idEltAndIdSupport[iSupportDofBC].second,
                                        m_idUnknown, pt);
    m_valueBCInSupportDof[iSupportDofBC] = functorXYZ(pt.x(), pt.y(), pt.z());
  }
}

/***********************************************************************************/
/***********************************************************************************/

template <class FunctorXYZ>
void BoundaryCondition::setValueVec(LinearProblem* linearProblem, const FunctorXYZ& functorXYZ) {
  FEL_CHECK(m_typeValueBC == FunctionS, "Error: the type of this BC should be a FunctionS")
  Point pt;
  m_valueBCInSupportDof.clear();
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++) {
    linearProblem->getSupportCoordinate(m_idEltAndIdSupport[iSupportDofBC].first,
                                        m_idEltAndIdSupport[iSupportDofBC].second,
                                        m_idUnknown, pt);
    for(auto it_comp = m_numComp.begin(); it_comp != m_numComp.end(); it_comp++) {
      m_valueBCInSupportDof.push_back(functorXYZ(*it_comp,pt.x(), pt.y(), pt.z()));
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

template <class FunctorXYZ>
void BoundaryCondition::setValueVec(LinearProblem* linearProblem, const FunctorXYZ& functorXYZ, double param) {
  // If one uses param as the time, this template can be use as a setValueVec for FunctorXYZT aswell
  FEL_CHECK(m_typeValueBC == FunctionS || m_typeValueBC == FunctionTS , "Error: the type of this BC should be a FunctionS or a FunctionTS")
  Point pt;
  m_valueBCInSupportDof.clear();
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++) {
    linearProblem->getSupportCoordinate(m_idEltAndIdSupport[iSupportDofBC].first,
                                        m_idEltAndIdSupport[iSupportDofBC].second,
                                        m_idUnknown, pt);
    for(auto it_comp = m_numComp.begin(); it_comp != m_numComp.end(); it_comp++) {
      m_valueBCInSupportDof.push_back(functorXYZ(*it_comp,pt.x(), pt.y(), pt.z(), param));
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

template <class FunctorXYZT>
void BoundaryCondition::setValue(LinearProblem* linearProblem, const FunctorXYZT& functorXYZT, double t) {
  FEL_CHECK(m_typeValueBC == FunctionS || m_typeValueBC == FunctionTS , "Error: the type of this BC should be a FunctionS or a FunctionTS")
  Point pt;
  m_valueBCInSupportDof.resize(m_idEltAndIdSupport.size(), 0.);
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++) {
    linearProblem->getSupportCoordinate(m_idEltAndIdSupport[iSupportDofBC].first,
                                        m_idEltAndIdSupport[iSupportDofBC].second,
                                        m_idUnknown, pt);
    m_valueBCInSupportDof[iSupportDofBC] = functorXYZT(pt.x(), pt.y(), pt.z(), t);
  }
}

/***********************************************************************************/
/***********************************************************************************/

template <class FunctorT>
void BoundaryCondition::setValueT(LinearProblem* linearProblem, const FunctorT& functorT, double t) {
  IGNORE_UNUSED_ARGUMENT(linearProblem);
  FEL_CHECK( m_typeValueBC == FunctionT , "Error: the type of this BC should be a FunctionT")
  const double value = functorT(t) ;
  m_valueBCInSupportDof.resize(m_idEltAndIdSupport.size(), 0.);
  for (std::size_t iSupportDofBC = 0; iSupportDofBC < m_idEltAndIdSupport.size(); iSupportDofBC++)
    m_valueBCInSupportDof[iSupportDofBC] = value ;
}

///@}
} /* namespace felisce.*/

#endif
