//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges, M. Fernandez and D. Corti
//

/*!
 \file duplicateSupportDof.cpp
 \authors B. Fabreges, M. Fernandez and D. Corti
 \date 23/01/2014
 \brief Class to manage the duplication of the support dof
*/

// System includes
#include <algorithm>
#include <numeric>

// External includes

// Project includes
#include "DegreeOfFreedom/duplicateSupportDof.hpp"


namespace felisce {

DuplicateSupportDof::DuplicateSupportDof() 
{
  m_hasIntersectionChange = false;
  m_isDSPinit             = false;

  m_verbose   =  0;
  m_dim       = -1;

  m_oldPtrIntEltMsh.resize(FelisceParam::instance().orderBdfNS+FelisceParam::instance().orderPressureExtrapolation);
  m_oldLstIntEltMsh.resize(FelisceParam::instance().orderBdfNS+FelisceParam::instance().orderPressureExtrapolation);
  m_oldCrdMsh.resize(      FelisceParam::instance().orderBdfNS+FelisceParam::instance().orderPressureExtrapolation);
  m_oldEltMsh.resize(      FelisceParam::instance().orderBdfNS+FelisceParam::instance().orderPressureExtrapolation);
  m_oldEltSgn.resize(      FelisceParam::instance().orderBdfNS+FelisceParam::instance().orderPressureExtrapolation);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::duplicateIntersectedSupportElements(std::vector<SupportDofMesh>& supportDofUnknown) 
{
  //- Check if class already initialized
  FEL_ASSERT(m_isDSPinit);

  for(std::size_t iUnknown=0; iUnknown < supportDofUnknown.size(); ++iUnknown)
    supportDofUnknown[iUnknown].duplicateSupportElements(m_intersectedMshEltIdx, m_intersectedEltVerSgn, m_intersectedVerMshIdx);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::intersectMeshes(GeometricMeshRegion& fluidMesh, GeometricMeshRegion& /*strucMesh*/) 
{
  //- Check if class already initialized
  FEL_ASSERT(m_isDSPinit);

  //- Update old arrays
  updateOldArrays();

  //- Prepare for new intersection
  m_XFMInterface.prepareNextIntersection();


  //--- Compute intersection
  m_XFMInterface.computeIntersection();


  //--- Copy old maps
  std::map<felInt, felInt>               oldIntersectedMshEltIdx(m_intersectedMshEltIdx);
  std::map<felInt, std::vector<felInt> > oldIntersectedEltVerSgn(m_intersectedEltVerSgn);
  std::size_t oldIdVerMshSize = m_intersectedVerMshIdx.size();


  //--- Clean everything
  m_intersectedMshEltIdx.clear();
  m_intersectedEltVerSgn.clear();
  m_intersectedTipEltIdx.clear();
  m_intersectedVerMshIdx.clear();
  m_subBndEltCrd.clear();
  m_subBndEltSide.clear();


  //--- Fill data strutures for duplication
  //- Volume elements
  m_XFMInterface.markElementsForDuplication(fluidMesh, m_intersectedMshEltIdx, m_intersectedEltVerSgn, m_intersectedVerMshIdx);

  //- Boundary elements
  m_XFMInterface.markBoundaryForDuplication(fluidMesh, m_intersectedMshEltIdx, m_intersectedEltVerSgn, m_subBndEltCrd, m_subBndEltSide);


  //--- Post processing for tip elements
  m_XFMInterface.postProcessTipElements(m_intersectedTipEltIdx, m_intersectedVerMshIdx);


  //--- Check if new intersection have been made (comparing with the previous time step)
  m_hasIntersectionChange = false;

  if ( m_intersectedMshEltIdx != oldIntersectedMshEltIdx ) {

    m_hasIntersectionChange = true;
  } else if( oldIdVerMshSize != m_intersectedVerMshIdx.size() ) {
    // check the duplicated vertex.
    // It is possible that the same elements are intersected but not the nodes. For a tip that
    // disappears because of contact for example (the number will be different)
    m_hasIntersectionChange = true;
  } else if ( m_intersectedEltVerSgn != oldIntersectedEltVerSgn ) {
    // check if the sign of the original vertex has changed
    m_hasIntersectionChange = true;
  }

  //- Output
  print();
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::initAndIntersectMeshes(GeometricMeshRegion& fluidMesh, GeometricMeshRegion& strucMesh)
{

  //- Check if class is not already initialized
  FEL_ASSERT(!m_isDSPinit);

  //- Set dimension
  m_dim = fluidMesh.domainDim();


  //--- Initialize the interface to the library
  m_XFMInterface.initialize(fluidMesh, strucMesh);


  //--- Compute intersection
  m_XFMInterface.computeIntersection();

  //- Initialize the old arrays
  initializeOldArrays();


  //--- Clean everything
  m_intersectedMshEltIdx.clear();
  m_intersectedEltVerSgn.clear();
  m_intersectedTipEltIdx.clear();
  m_intersectedVerMshIdx.clear();
  m_subBndEltCrd.clear();
  m_subBndEltSide.clear();


  //--- Fill data strutures for duplication
  //- Volume elements
  m_XFMInterface.markElementsForDuplication(fluidMesh, m_intersectedMshEltIdx, m_intersectedEltVerSgn, m_intersectedVerMshIdx);

  //- Boundary elements
  m_XFMInterface.markBoundaryForDuplication(fluidMesh, m_intersectedMshEltIdx, m_intersectedEltVerSgn, m_subBndEltCrd, m_subBndEltSide);


  //--- Post processing for tip elements
  m_XFMInterface.postProcessTipElements(m_intersectedTipEltIdx, m_intersectedVerMshIdx);

  //- Output
  print();


  //--- Initialisation is done
  m_isDSPinit = true;
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::updateInterfacePosition(std::vector<double>& dispArray) 
{
  //- Check if class already initialized
  FEL_ASSERT(m_isDSPinit);

  m_XFMInterface.updatePosition(dispArray);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumSubMshEltMshOld(felInt iel, felInt idTimeStep) const 
{

  return m_oldPtrIntEltMsh[idTimeStep][iel+1] - m_oldPtrIntEltMsh[idTimeStep][iel];
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getSubMshEltIdxMshOld(felInt iel, felInt ielSub, felInt idTimeStep) const
{

  return m_oldLstIntEltMsh[idTimeStep][m_oldPtrIntEltMsh[idTimeStep][iel]+ielSub];
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getSubMshEltVerCrdOld(felInt ielSub, felInt iPt, std::vector<double>& coor, felInt idTimeStep) const
{
  for(std::size_t i = 0; i < 3; ++i) {
    coor[i] = m_oldCrdMsh[idTimeStep][m_oldEltMsh[idTimeStep][ielSub][iPt]][i];
  }
}

/***********************************************************************************/
/***********************************************************************************/

sideOfInterface DuplicateSupportDof::getSubMshEltSideOld(felInt ielSub, felInt idTimeStep) const
{

  return static_cast<sideOfInterface>(m_oldEltSgn[idTimeStep][ielSub]);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumSubMshEltPerMshElt(felInt iel) const
{
  return m_XFMInterface.getNumSubMshEltPerMshElt(iel);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getSubMshEltIdxMsh(felInt iel, felInt ielSub) const
{

  return m_XFMInterface.getSubMshEltIdxMsh(iel, ielSub);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getSubMshEltVerCrd(felInt ielSub, felInt iPt, std::vector<double>& coor) const
{

  m_XFMInterface.getSubMshEltVerCrd(ielSub,iPt,coor);
}

/***********************************************************************************/
/***********************************************************************************/

sideOfInterface DuplicateSupportDof::getSubMshEltSide(felInt iel, felInt ielSub) const
{

  return static_cast<sideOfInterface>(m_XFMInterface.getSubMshEltSide(iel, ielSub));
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumSubItfEltPerMshElt(felInt iel) const
{

  return m_XFMInterface.getNumSubItfEltPerMshElt(iel);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getSubItfEltIdxMsh(felInt iel, felInt ielSub) const
{

  return m_XFMInterface.getSubItfEltIdxMsh(iel, ielSub);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getSubItfEltVerCrd(felInt ielSub, felInt iPt, std::vector<double>& coor) const
{

  m_XFMInterface.getSubItfEltVerCrd(ielSub,iPt,coor);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumSubItfEltPerItfElt(felInt iel) const
{

  return m_XFMInterface.getNumSubItfEltPerItfElt(iel);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getSubItfEltIdxItf(felInt iel, felInt ielSub) const
{

  return m_XFMInterface.getSubItfEltIdxItf(iel, ielSub);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getMshEltIdxOfSubItfElt(felInt ielSub) const
{

  return m_XFMInterface.getMshEltIdxOfSubItfElt(ielSub);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getItfEltIdxOfSubItfElt(felInt ielSub) const
{

  return m_XFMInterface.getItfEltIdxOfSubItfElt(ielSub);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumTipElt() const
{

  return m_XFMInterface.getNumTipElt();
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getTipEltIdx(felInt ielTip) const
{
  return m_XFMInterface.getTipEltIdx(ielTip);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getItfEltRef(felInt iel) const
{

  return m_XFMInterface.getItfEltRef(iel);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumSubFacPerFacTipElt(felInt ielTip, felInt ifa) const
{

  return m_XFMInterface.getNumSubFacPerFacTipElt(ielTip, ifa);
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getSubTipFacIdxMsh(felInt ielTip, felInt ifa, felInt isubFac) const
{

  return m_XFMInterface.getSubTipFacIdxMsh(ielTip, ifa, isubFac);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getSubTipFacVerCrd(felInt iSubFac, felInt iPt, std::vector<double>& coor) const
{

  m_XFMInterface.getSubTipFacVerCrd(iSubFac, iPt, coor);
}

/***********************************************************************************/
/***********************************************************************************/

sideOfInterface DuplicateSupportDof::getSgnSubFace(felInt iSubFac) const
{

  return static_cast<sideOfInterface>(m_XFMInterface.getSgnSubFace(iSubFac));
}

/***********************************************************************************/
/***********************************************************************************/

sideOfInterface DuplicateSupportDof::getSgnSubOppFace(felInt iSubFac) const
{

  return static_cast<sideOfInterface>(m_XFMInterface.getSgnSubOppFace(iSubFac));
}

/***********************************************************************************/
/***********************************************************************************/

felInt DuplicateSupportDof::getNumFrontPoints() const
{
  
  return m_XFMInterface.getNumFrontPoints();
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getFrontPointInfo(felInt iVer, std::vector<felInt>& verFro) const
{

  m_XFMInterface.getFrontPointInfo(iVer, verFro);
}

/***********************************************************************************/
/***********************************************************************************/

bool DuplicateSupportDof::checkFrontPoint(felInt iVer) const
{

  return m_XFMInterface.checkFrontPoint(iVer);
}

/***********************************************************************************/
/***********************************************************************************/

bool DuplicateSupportDof::hasIntersectionChange() const
{

  return m_hasIntersectionChange;
}

/***********************************************************************************/
/***********************************************************************************/

const std::map<felInt, std::map<felInt, felInt> >& DuplicateSupportDof::getIntersectedVerMsh() const
{

  return m_intersectedVerMshIdx;
}

/***********************************************************************************/
/***********************************************************************************/
        
const std::map<felInt, felInt>& DuplicateSupportDof::getIntersectedMshElt() const
{
  
  return m_intersectedMshEltIdx;
}

/***********************************************************************************/
/***********************************************************************************/

const std::set<felInt>& DuplicateSupportDof::getIntersectedTipElt() const
{
  
  return m_intersectedTipEltIdx;
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getSubBndElt(felInt iel, std::vector< std::vector<Point> >& bndElem) const
{
  bndElem.clear();
  auto it = m_subBndEltCrd.find(iel);
  if(it != m_subBndEltCrd.end())
    bndElem = it->second;
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::getSubBndEltSide(felInt iel, std::vector<sideOfInterface>& bndElemSide) const
{  
  bndElemSide.clear();
  auto it = m_subBndEltSide.find(iel);
  if( it != m_subBndEltSide.end() )
    bndElemSide = it->second;
}

/***********************************************************************************/
/***********************************************************************************/

const std::vector<short unsigned int>& DuplicateSupportDof::getMapFaceFelisceToIntersector(felInt dim) const
{

  return m_XFMInterface.getMapFaceFelisceToIntersector(dim);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::setVerbose(felInt verbose) 
{

  m_XFMInterface.setIteration(verbose);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::setMeshesName(std::string fluidMesh, std::string strucMesh)
{

  m_XFMInterface.setMeshesName(fluidMesh, strucMesh);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::setIteration(felInt iteration)
{

  m_XFMInterface.setIteration(iteration);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::initializeOldArrays() 
{

  //--- Initialize old arrays
  for(std::size_t j = 0; j < m_oldPtrIntEltMsh.size(); ++j)
    m_XFMInterface.getPtrSubMshEltPerMshElt(m_oldPtrIntEltMsh[j]);

  for(std::size_t j = 0; j < m_oldLstIntEltMsh.size(); ++j)
    m_XFMInterface.getLstSubMshPerEltMsh(m_oldLstIntEltMsh[j]);

  for(std::size_t j = 0; j < m_oldCrdMsh.size(); ++j)
    m_XFMInterface.getLstIntMshVerCrd(m_oldCrdMsh[j]);

  for(std::size_t j = 0; j < m_oldEltMsh.size(); ++j)
    m_XFMInterface.getLstIntMshElt(m_oldEltMsh[j]);

  for(std::size_t j = 0; j < m_oldEltSgn.size(); ++j)
    m_XFMInterface.getLstIntMshEltSgn(m_oldEltSgn[j]);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::updateOldArrays() 
{

  //--- Update old arrays
  for(std::size_t j = m_oldPtrIntEltMsh.size() - 1; j > 0; --j)
    m_oldPtrIntEltMsh[j] = std::move(m_oldPtrIntEltMsh[j-1]);
  m_XFMInterface.getPtrSubMshEltPerMshElt(m_oldPtrIntEltMsh[0]);

  for(std::size_t i = m_oldLstIntEltMsh.size() - 1; i > 0; --i)
    m_oldLstIntEltMsh[i] = std::move(m_oldLstIntEltMsh[i-1]);
  m_XFMInterface.getLstSubMshPerEltMsh(m_oldLstIntEltMsh[0]);

  for(std::size_t i = m_oldCrdMsh.size() - 1; i > 0; --i)
    m_oldCrdMsh[i] = std::move(m_oldCrdMsh[i-1]);
  m_XFMInterface.getLstIntMshVerCrd(m_oldCrdMsh[0]);

  for(std::size_t i = m_oldEltMsh.size() - 1; i > 0; --i)
    m_oldEltMsh[i] = std::move(m_oldEltMsh[i-1]);
  m_XFMInterface.getLstIntMshElt(m_oldEltMsh[0]);

  for(std::size_t i = m_oldEltSgn.size() - 1; i > 0; --i)
    m_oldEltSgn[i] = std::move(m_oldEltSgn[i-1]);
  m_XFMInterface.getLstIntMshEltSgn(m_oldEltSgn[0]);
}

/***********************************************************************************/
/***********************************************************************************/

void DuplicateSupportDof::print() const 
{
  if( m_verbose < 20 )
    return;

  if ( MpiInfo::rankProc() == 0 ){

    PetscPrintf(PETSC_COMM_WORLD, "\n###### Intersected elements ( volume and boundary elements ) #####\n");
    PetscPrintf(PETSC_COMM_WORLD, "Number of intersected elements : %ld\n", static_cast<unsigned long>(m_intersectedMshEltIdx.size()));
    for(auto it_elt = m_intersectedMshEltIdx.begin(); it_elt != m_intersectedMshEltIdx.end(); ++it_elt) {
      PetscPrintf(PETSC_COMM_WORLD, "Element %d intersected by interface %d: \n", it_elt->first, it_elt->second);
      auto it_sgn = m_intersectedEltVerSgn.find(it_elt->first);
      for(std::size_t ive = 0; ive < it_sgn->second.size(); ++ive)
        PetscPrintf(PETSC_COMM_WORLD, "--- Local vertex %ld, and sign %+d \n", static_cast<unsigned long>(ive), it_sgn->second[ive]);
    }
    PetscPrintf(PETSC_COMM_WORLD, "\n");

    PetscPrintf(PETSC_COMM_WORLD, "\n###### Intersected boundary elements ######\n");
    PetscPrintf(PETSC_COMM_WORLD, "Number of intersected boundaries : %ld \n", static_cast<unsigned long>(m_subBndEltCrd.size()));
    auto it_elt_crd = m_subBndEltCrd.begin();
    auto it_elt_sid = m_subBndEltSide.begin();
    while( it_elt_crd != m_subBndEltCrd.end() && it_elt_sid != m_subBndEltSide.end() ) {
      PetscPrintf(PETSC_COMM_WORLD, "Element %d :\n", it_elt_crd->first);
      auto& it_sub_elt_crd = it_elt_crd->second;
      auto& it_sub_elt_sid = it_elt_sid->second;
      for(std::size_t iel = 0; iel < it_sub_elt_crd.size(); ++iel) {
        PetscPrintf(PETSC_COMM_WORLD, "Sub element %ld : side = %+d \n", static_cast<unsigned long>(iel), it_sub_elt_sid[iel]);
        for(std::size_t ipt = 0; ipt < it_sub_elt_crd[iel].size(); ++ipt)
          PetscPrintf(PETSC_COMM_WORLD, "\t\t %12.8f %12.8f %12.8f \n", it_sub_elt_crd[iel][ipt].x(), it_sub_elt_crd[iel][ipt].y(), it_sub_elt_crd[iel][ipt].z());
      }
      it_elt_crd++;
      it_elt_sid++;
    }
    PetscPrintf(PETSC_COMM_WORLD, "\n");
  }
}

}
