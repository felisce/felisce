//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges, M. Fernandez and D. Corti
//

/*!
  \file duplicateSupportDof.hpp
  \authors B. Fabreges, M. Fernandez and D. Corti
  \date 23/01/2014
  \brief hpp file for the class DuplicateSupportDof
*/

#ifndef _DuplicateSupportDof_HPP
#define _DuplicateSupportDof_HPP

// System includes

// External includes

// Project includes
#include "CommunicationInterfaces/XFMInterface.hpp"
#include "DegreeOfFreedom/supportDofMesh.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "PETScInterface/petscVector_fwd.hpp"


namespace felisce {
  /*!
   * Class to manage the intersection of the two meshes and the duplication of the support elements
   */
  class DuplicateSupportDof 
  {
    
    public:
      
      // Constructor
      DuplicateSupportDof();

      // Destructor
      ~DuplicateSupportDof() = default;

      // Duplicate the support dofs
      void duplicateIntersectedSupportElements(std::vector<SupportDofMesh>& supportDofUnknown);

      /*!
       * \brief Intersect two meshes
       * \param[in] fluidMesh The fluid mesh.
       * \param[in] strucMesh The solid mesh.
       *
       * After the update, the following steps are executed:
       * - The intersection is computed and the information are extracted.
       * - The intersected elements (volume and boundary) are marked for duplication aswell as their vertices.
       */
      void intersectMeshes(GeometricMeshRegion& fluidMesh, GeometricMeshRegion& strucMesh);

      // As intersectMeshes + class initialization
      void initAndIntersectMeshes(GeometricMeshRegion& fluidMesh, GeometricMeshRegion& strucMesh);

      // Update the position of the solid mesh
      void updateInterfacePosition(std::vector<double>& dispArray);

      // Get the number of fluid sub-element for an element of the fluid mesh
      felInt getNumSubMshEltMshOld(felInt iel, felInt idTimeStep) const;

      // Get the id of the ielSub-th fluid sub-element of fluid element iel
      felInt getSubMshEltIdxMshOld(felInt iel, felInt ielSub, felInt idTimeStep) const;

      // Get the coordinates of the iPt-th vertex of the sub-element ielSub for the fluid
      void getSubMshEltVerCrdOld(felInt ielSub, felInt iPt, std::vector<double>& coor, felInt idTimeStep) const;

      // Get the side of the sub-element
      sideOfInterface getSubMshEltSideOld(felInt ielSub, felInt idTimeStep) const;

      // Get the number of fluid sub-element for an element of the fluid mesh
      felInt getNumSubMshEltPerMshElt(felInt iel) const;

      // Get the id of the ielSub-th fluid sub-element of fluid element iel
      felInt getSubMshEltIdxMsh(felInt iel, felInt ielSub) const;

      // Get the coordinates of the iPt-th vertex of the sub-element ielSub for the fluid
      void getSubMshEltVerCrd(felInt ielSub, felInt iPt, std::vector<double>& coor) const;
      
      // Get the side of the sub-element
      sideOfInterface getSubMshEltSide(felInt iel, felInt ielSub) const;
      
      // Get the number of solid sub-element for an element of the fluid mesh
      felInt getNumSubItfEltPerMshElt(felInt iel) const;
      
      // Get the id of the ielSub-th solid sub-element of fluid element iel
      felInt getSubItfEltIdxMsh(felInt iel, felInt ielSub) const;
      
      // Get the coordinates of the iPt-th vertex of the sub-element ielSub for the solid
      void getSubItfEltVerCrd(felInt ielSub, felInt iPt, std::vector<double>& coor) const;
      
      // Get the number of solid sub-element for an element of the solid mesh
      felInt getNumSubItfEltPerItfElt(felInt iel) const;
      
      // Get the id of the ielSub-th solid sub-element of solid element iel
      felInt getSubItfEltIdxItf(felInt iel, felInt ielSub) const;
      
      // Get the id of the fluid element owning the sub-solid element ielSub
      felInt getMshEltIdxOfSubItfElt(felInt ielSub) const;
      
      // Get the id of the solid element owning the sub-solid element ielSub
      felInt getItfEltIdxOfSubItfElt(felInt ielSub) const;

      // Get the number of fluid Tip element
      felInt getNumTipElt() const;

      // Get the Id of fluid Tip element iTipElt
      felInt getTipEltIdx(felInt ielTip) const;

      // Get the reference of iterface element iel
      felInt getItfEltRef(felInt iel) const;

      // Get the number of subfaces of the ifa^th face of ielTip
      felInt getNumSubFacPerFacTipElt(felInt ielTip, felInt ifa) const;

      // Get the idx of the isubFac-th sub-face of the ifa-th face of ielTip
      felInt getSubTipFacIdxMsh(felInt ielTip, felInt ifa, felInt isubFac) const;

      // Get the coordinates of the iPt point of the tip isubFac
      void getSubTipFacVerCrd(felInt iSubFac, felInt iPt, std::vector<double>& coor) const;

      // Get the side of the subFace
      sideOfInterface getSgnSubFace(felInt SubFac) const;

      // Get the side of the opposite subFace
      sideOfInterface getSgnSubOppFace(felInt SubFac) const;

      // Get the number of front points
      felInt getNumFrontPoints() const;

      // Get the information on the iVer-th front point
      void getFrontPointInfo(felInt iVer, std::vector<felInt>& verFro) const;

      // Check if the vertex iVer is a front point
      bool checkFrontPoint(felInt iVer) const;

      // Check if the intersection has change after moving the interface
      bool hasIntersectionChange() const;

      // Get a reference to the map m_idVerMsh
      const std::map<felInt, std::map<felInt, felInt> >& getIntersectedVerMsh() const;

      // Get a reference to the map m_intersectedMshEltIdx
      const std::map<felInt, felInt>& getIntersectedMshElt() const;

      // Get a reference to the map m_idTipElement
      const std::set<felInt>& getIntersectedTipElt() const;

      // Get the sub element of a boundary element
      void getSubBndElt(felInt iel, std::vector< std::vector <Point> >& bndElem) const;

      // Get the side of a boundary sub element
      void getSubBndEltSide(felInt iel, std::vector<sideOfInterface>& bndElemSide) const;

      // Get map edge/face
      const std::vector<short unsigned int>& getMapFaceFelisceToIntersector(felInt dim) const;

      // Set the verbose level
      void setVerbose(felInt verbose);

      // Set meshes name
      void setMeshesName(std::string fluidMesh, std::string strucMesh);

      // Set iteration
      void setIteration(felInt iteration);


    private:

      // Initialize old arrays to store information about previous intersections
      void initializeOldArrays();

      // Update old arrays with current intersection info
      void updateOldArrays();

      // Print
      void print() const;

      // Interface to Xfm library
      XFMInterface m_XFMInterface;

      // Dynamic update
      bool m_hasIntersectionChange;

      // Initialization
      bool m_isDSPinit;

      // Verbosity
      felInt m_verbose;

      // Dimension of the fluid mesh
      felInt m_dim;

      // map of the original id of the fluid mesh paired with the iterface Idx to its duplicated id
      std::map<felInt, std::map<felInt, felInt> > m_intersectedVerMshIdx;

      // Idx of the intersected element to Idx interface intersecting the element
      std::map<felInt, felInt> m_intersectedMshEltIdx;

      // Vertices local signs for every intersected element
      std::map<felInt, std::vector<felInt> > m_intersectedEltVerSgn;

      // Idx of the intersected tip element
      std::set<felInt> m_intersectedTipEltIdx;

      // sub element of the intersected boundary elements
      std::map<felInt, std::vector< std::vector<Point> > > m_subBndEltCrd;

      // side of the sub boundary elements
      std::map<felInt, std::vector<sideOfInterface> > m_subBndEltSide;

      // Old arrays 
      std::vector<std::vector<felInt>>               m_oldPtrIntEltMsh;
      std::vector<std::vector<felInt>>               m_oldLstIntEltMsh;
      std::vector<std::vector<std::array<double,3>>> m_oldCrdMsh;
      std::vector<std::vector<std::array<felInt,4>>> m_oldEltMsh;
      std::vector<std::vector<felInt>>               m_oldEltSgn;
  };
}

#endif
