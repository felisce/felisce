//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include "NSContinuationModel.hpp"


namespace felisce {
  NSContinuationModel::NSContinuationModel():Model() {
    m_name = "NSContinuation";
  }

  // Called in Model::initializeLinearProblem, just after the mesh partitioning.
  void NSContinuationModel::initializeDerivedModel() {
    // get the linearProblem for continuation; m_linearProblem[0] = continuation and m_linearProblem[1] = harmonic extension
    m_lpb = static_cast<LinearProblemNSContinuation*>(m_linearProblem[0]);
    
    m_tau = 1./(m_fstransient->timeStep);

    // Initializing the PetscVectors
    m_lpb->initPetscVectors();

    // Initialize the displacement vector
    if( m_lpb->dimension()==2)
    {
      m_disp.duplicateFrom(m_lpb->vector());
      m_disp.zeroEntries();
    }
   

    if( FelisceParam::instance(instanceIndex()).useALEformulation ) 
    {
      // ALE
      m_numDofExtensionProblem = m_linearProblem[1]->numDof();
      m_dispMeshVector.resize( m_linearProblem[1]->supportDofUnknown(0).listNode().size() * m_linearProblem[1]->meshLocal()->numCoor() );
      m_petscToGlobal.resize(m_numDofExtensionProblem);
      for (int ipg=0; ipg< m_numDofExtensionProblem; ipg++)
        m_petscToGlobal[ipg] = ipg;

      AOApplicationToPetsc(m_linearProblem[1]->ao(),m_numDofExtensionProblem,m_petscToGlobal.data());
    }

    
  }

  void NSContinuationModel::setExternalVec() {

    // ALE
    if( FelisceParam::instance(instanceIndex()).useALEformulation ) 
    {
      // Initialization of the std::vector containing the displacement of the mesh d^{n}, sequential std::vector
      m_dispMesh.duplicateFrom(m_linearProblem[1]->sequentialSolution());
      m_dispMesh.zeroEntries();

      // Initialization of the std::vector containing the velocity of the mesh w, sequential std::vector
      m_velMesh.duplicateFrom(m_linearProblem[1]->sequentialSolution());
      m_velMesh.zeroEntries();

      // Passing the ao and dof
      m_lpb->pushBackExternalAO(m_linearProblem[1]->ao());
      m_lpb->pushBackExternalDof(m_linearProblem[1]->dof());
      // Passing the velocity of the mesh to the NS problem
      m_lpb->pushBackExternalVec(m_velMesh);  //externalVec(0) in m_lpb
      // Resizing the auxiliary vectors needed in updateMesh()
      m_tmpDisp.resize(m_numDofExtensionProblem);

      m_displTimeStep = 0.;

    }
    
    if(m_lpb->dimension()==2)
    {
      m_dispSeq.createSeq(PETSC_COMM_SELF, m_lpb->numDof());
      m_dispSeq.set(0.);
      m_lpb->pushBackExternalVec(m_dispSeq); //externalVec(1) if in 2D  
    }
    
  }


  void NSContinuationModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    if(iProblem == 0) {
      m_linearProblem[0]->solution().zeroEntries();
    }

    //=========
    //   ALE
    //=========
    if( FelisceParam::instance(instanceIndex()).useALEformulation ) {
      if ( ( iProblem == 1 ) && ( !FelisceParam::instance(instanceIndex()).useElasticExtension ) ) {
        // First assembly loop in iteration 0 to build static matrix of the extension problem
        m_linearProblem[1]->assembleMatrixRHS(MpiInfo::rankProc());
        // Save the static part of the extension problem in the matrix _A
        m_linearProblem[1]->copyMatrixRHS();
      }
    }
  }


  //! The main purpose of this function is to sum the constant and the non constant matrices
  void NSContinuationModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    // matrix(0) = matrix(0) + matrix(1) (add static matrix to the dynamic matrix to build complete matrix of the system )
    m_linearProblem[iProblem]->addMatrixRHS();
  }


  void NSContinuationModel::prepareForward() {

    writeSolution();
    
    updateTime(); //Here the matrix(0), the non constant one, is cleared and the rhs also.

    printNewTimeIterationBanner();

    m_lpb->gatherVectorBeforeAssembleMatrixRHS();

    m_lpb->readVelocityData(*io(), m_fstransient->time);

    computeALEterms(); // if no ALE, it does not compute the displacement 
  }


  void NSContinuationModel::solveContinuation() {
    const auto& r_felisce_param_instance = FelisceParam::instance(instanceIndex());

    m_lpb->solution().zeroEntries();

    //Steps corresponding to solveNS()
    if( FelisceParam::instance(instanceIndex()).useALEformulation )
      m_lpb->meshLocal()->moveMesh(m_dispMeshVector,0.0);

    //Apply boundary conditions for the continuation problem in the reference mesh configuration
    m_lpb->finalizeEssBCTransient();
    m_lpb->applyBC(r_felisce_param_instance.essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    if( FelisceParam::instance(instanceIndex()).useALEformulation )
      m_lpb->meshLocal()->moveMesh(m_dispMeshVector,1.0);
    
    // Solve the linear system
    m_lpb->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    
    /*if(m_lpb->dimension() == 2)
    {
       m_disp.axpy(m_fstransient->timeStep, m_lpb->solution());
       m_lpb->gatherVector(m_disp, m_dispSeq);
    } */

    // Gather the velocity
    m_lpb->gatherSolution(); 
  }

  void NSContinuationModel::forward() {
    prepareForward();
    solveContinuation();
  }

  // *********************** ALE *************************************************
  void NSContinuationModel::computeALEterms() {
    // Assemble the RHS in the previous mesh configuration
    m_lpb->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);
    
    if( FelisceParam::instance(instanceIndex()).useALEformulation )
      UpdateMeshByDisplacement();
    
    m_lpb->assembleCIPStabilization();
      
    // Assemble the matrix in the current mesh configuration
    m_lpb->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
  }

  void NSContinuationModel::UpdateMeshByDisplacement() {
    // Get the previous displacement
    m_velMesh.copyFrom(m_dispMesh);

    if( FelisceParam::instance(instanceIndex()).useALEformulation ) {
      // Solve the harmonic extension problem
      solveHarmExt();
      m_linearProblem[1]->gatherSolution(); 
    }
    updateMesh(); // current displacement m_dispMesh
    
    // Update the mesh velocity; VecAXPBY(Vec y,PetscScalar a,PetscScalar b,Vec x);  -> y=a∗x+b∗y
    m_velMesh.axpby(-m_tau,m_tau,m_dispMesh);
    // Result: m_velMesh =  -w^{n}  =  \dfrac{1}{\dt} (-d^{n} + d^{n-1})
  }

  void NSContinuationModel::solveHarmExt() {
    PetscPrintf(MpiInfo::petscComm(),"Solve harmonic extension\n");
    //Assemble matrix and RHS
    m_linearProblem[1]->assembleMatrixRHS(MpiInfo::rankProc());

    //Specific operations before solving the system
    postAssemblingMatrixRHS(1);

    //Apply essential transient boundary conditions
    m_linearProblem[1]->finalizeEssBCTransient();
    m_linearProblem[1]->applyBC(FelisceParam::instance(instanceIndex()).essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    //Solve the linear system
    m_linearProblem[1]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
 
  }

  void NSContinuationModel::updateMesh() {
    PetscPrintf(MpiInfo::petscComm(),"Update mesh\n");
    
    // Get the displacement from the harmonic extension
    m_dispMesh.copyFrom(m_linearProblem[1]->sequentialSolution());
      
    // Extract the values of m_dispMesh and put them in the std::vector m_dispMeshVector according to the application ordering of the nodes
    m_dispMesh.getValues(m_numDofExtensionProblem, m_petscToGlobal.data(), m_tmpDisp.data());   
    

    m_dispMeshVector = m_tmpDisp;

    //Move the mesh in the fluid problem
    m_lpb->meshLocal()->moveMesh(m_dispMeshVector,1.0);
  }

  // Compute the L2-error and append to file [Temporary implementation]
  
  
}
