//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Fernandez & L. Boilevin-Kayl
//

#ifndef _Immersed_HPP
#define _Immersed_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Geometry/Tools/interpolator.hpp"
#include "Geometry/Tools/intersector.hpp"
#include "Solver/linearProblem.hpp"

namespace felisce {
  /*!
  \brief Class for the penalty terms of the fictitious domain method.
  */
  class Immersed {
  public:
    Immersed();
    ~Immersed();
 
    void initialize(GeometricMeshRegion::Pointer backMesh);
    void setInterfaceExchangedData(std::vector<double>* intfDisp, std::vector<double>* intfVelo, std::vector<double>* intfForc, std::vector<double>* velFluid);
    void computePenaltyForce(AO& ao, Dof& dof, int idVarVel, PetscVector& seqVel);
    void updateInterface(GeometricMeshRegion& mesh);
    void updateIntersection(GeometricMeshRegion& mesh, PetscVector& delta, const MPI_Comm& comm);
    void applyPenaltyToMatrixAndRHS(AO& ao, Dof& dof, int idVarVel, PetscMatrix& matrix, PetscVector& rhs, bool only_rhs = false);
    
  private:

    void m_readInterfaceMesh(); // read the immersed structure and put it into m_interfMesh
    void m_buildLumpedMassMatrixOfStructure(); // build the lumped mass matrix of the immersed structure
    void m_divDivStabPostProcess(std::vector<felInt>& elementID); // print intersected elements

    GeometricMeshRegion::Pointer m_backMesh;
    GeometricMeshRegion::Pointer m_interfMesh;
    int m_allowProjection;
    std::vector<Point> m_listPointsRef;
    std::vector<Point> m_listPoints;
    int m_nbPoints;
    std::vector<double> m_intOp_val; 
    std::vector<felInt> m_indElt; 
    int m_nbVertices;
    int m_nbCoor;

    std::vector<double> *m_intfDisp;
    std::vector<double> *m_intfVelo;
    std::vector<double> *m_intfForc;
    std::vector<double> *m_velFluid;

    std::vector<double> m_intfForcOld; // std::vector to store the previous force
    std::vector<double> m_lumpedMassMatrix; // std::vector to store the lumped mass matrix of the structure

    int m_indexTime;

    Interpolator::Pointer m_interpolator;
    Intersector::Pointer  m_intersector;
  };
}

#endif
