//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _HEATPARAMETRICMODEL_HPP
#define _HEATPARAMETRICMODEL_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  class HeatParametricModel:
    public Model {
  public:
    /// Construtor.
    HeatParametricModel();
    /// Destructor.
    ~HeatParametricModel() override;
    void initializeDerivedModel() override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    ///  Manage time iteration.
    void forward() override;
    ///  Function to get size of the state std::vector.
    int getNstate() const override;
    ///  Function to get state std::vector.
    void getState(double* & state) override;
    ///  Function to std::set state std::vector.
    void setState(double* & state) override;

    //RHS coefficient
    double Kparameter;
    double RHSparameter;
  };
}

#endif
