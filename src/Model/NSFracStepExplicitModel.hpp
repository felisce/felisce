//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _NSFracStepExplicitModel_HPP
#define _NSFracStepExplicitModel_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Solver/bdf.hpp"

namespace felisce {
  class NSFracStepExplicitModel:
    public Model {
  public:
    /// Construtor.
    NSFracStepExplicitModel();
    /// Destructor.
    ~NSFracStepExplicitModel() override;
    void initializeDerivedModel() override;
    void setExternalVec() override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;

    ///  Manage time iteration.
    void forward() override;

    ///  Function to get size of the state std::vector.
    int getNstate() const override;
    ///  Function to get state std::vector.
    void getState(double* & state) override;
    felInt numDof_swig();
    void getState_swig(double* data, felInt numDof);
    ///  Function to std::set state std::vector.
    void setState(double* & state) override;

    ///  Access functions:
    inline const Bdf & bdf() const {
      return m_bdfFS;
    }
    inline Bdf & bdf() {
      return m_bdfFS;
    }

  private:
    ///  Lumped model:
    LumpedModelBC* m_lumpedModelBC;
    std::vector<int> m_labelFluxListLumpedModelBC;
    std::vector<double> m_fluxLumpedModelBC;

    ///  Time scheme.
    Bdf m_bdfFS;
    PetscVector  m_solExtrapolate;
  };
}

#endif
