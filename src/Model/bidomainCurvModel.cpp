//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

// System includes

// External includes

// Project includes
#include "Model/bidomainCurvModel.hpp"

namespace felisce {
  BidomainCurvModel::BidomainCurvModel():
    Model(),
    m_ionic(nullptr),
    m_schaf(nullptr),
    m_fhn(nullptr),
    m_court(nullptr),
    m_buildIonic(false),
    m_buildCourt(false),
    m_verbose(0) {
    for(std::size_t i=0; i<m_linearProblemBidomainCurv.size(); i++) {
      m_linearProblemBidomainCurv[i] = nullptr;
    }
    m_name = "BidomainCurv";
  }

  BidomainCurvModel::~BidomainCurvModel() {
    for(std::size_t i=0; i<m_linearProblemBidomainCurv.size(); i++) {
      m_linearProblemBidomainCurv[i] = nullptr;
    }
    if (m_buildIonic) {
      delete m_ionic;
      m_ionic = nullptr;
    }
    if (m_buildCourt) {
      delete m_court;
      m_court = nullptr;
    }
    m_solExtraCell.destroy();
    m_extrapolate.destroy();
    if (m_buildCourt) {
      m_m.destroy();
      m_h.destroy();
      m_j.destroy();
      m_ao.destroy();
      m_io.destroy();
      m_ua.destroy();
      m_ui.destroy();
      m_xr.destroy();
      m_xs.destroy();
      m_d.destroy();
      m_f.destroy();
      m_fca.destroy();
      m_urel.destroy();
      m_vrel.destroy();
      m_wrel.destroy();
      m_nai.destroy();
      m_nao.destroy();
      m_cao.destroy();
      m_ki.destroy();
      m_ko.destroy();
      m_cai.destroy();
      m_naiont.destroy();
      m_kiont.destroy();
      m_caiont.destroy();
      m_ileak.destroy();
      m_iup.destroy();
      m_itr.destroy();
      m_irel.destroy();
      m_cmdn.destroy();
      m_trpn.destroy();
      m_nsr.destroy();
      m_jsr.destroy();
      m_csqn.destroy();
    } else {
      m_W_0.destroy();
    }
    delete m_iApp;
    if (FelisceParam::instance().hasAppliedExteriorCurrent)
      delete m_iAppExt;
  }

  void BidomainCurvModel::initializeDerivedLinearProblem() {
    for (std::size_t i=0; i<m_linearProblem.size(); i++) {
      m_linearProblemBidomainCurv.push_back(static_cast<LinearProblemBidomainCurv*>(m_linearProblem[i]));
    }
  }

  //! Define m_schaf and m_bdfEdp (use in linearProblem).
  void BidomainCurvModel::initializeDerivedModel() {
    m_verbose = FelisceParam::verbose();

    if (FelisceParam::instance().typeOfIonicModel == "schaf") {
      //Allocate m_schaf.
      m_schaf = new SchafSolver(m_fstransient);
      m_ionic = m_schaf;
      //Fix order of bdf for schaf solver.
      m_schaf->defineOrderBdf(FelisceParam::instance().orderBdfIonic);
    } else if (FelisceParam::instance().typeOfIonicModel == "fhn") {
      // Initialize Fitzhugh-Nagumo model
      m_fhn = new FhNSolver(m_fstransient);
      m_ionic =m_fhn;
      m_ionic->defineOrderBdf(FelisceParam::instance().orderBdfIonic,1);
    } else if (FelisceParam::instance().typeOfIonicModel == "court") {
      //Allocate m_court.
      m_court = new CourtemancheSolver(m_fstransient);
    }

    //Define order for bdf used by EDP (linearProblem).
    m_bdfEdp.defineOrder(FelisceParam::instance().orderBdfEdp);

    initializeAppCurrent();
    if (FelisceParam::instance().hasAppliedExteriorCurrent)
      initializeAppCurrentExt();

  }

  void BidomainCurvModel::setInitialCondition() {
    for (std::size_t iProblem=0; iProblem<m_linearProblemBidomainCurv.size(); iProblem++) {
      m_U_0.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_U_0.set(0.0);
      if (FelisceParam::instance().hasInitialCondition) {

        int idVar = -1;
        double valueByDofU_0;

        for (std::size_t iListVar = 0; iListVar < m_initialCondition.listVariable().size(); iListVar++) {
          for ( std::size_t iUnknown = 0; iUnknown < m_linearProblemBidomainCurv[iProblem]->listUnknown().size(); iUnknown++) {
            idVar = m_linearProblemBidomainCurv[iProblem]->listUnknown().idVariable(iUnknown);
            if ( m_linearProblemBidomainCurv[iProblem]->listVariable()[idVar].name() == m_initialCondition.listVariable()[iListVar].name() ) {
              felInt numDofLpU = m_linearProblemBidomainCurv[iProblem]->numDofLocalPerUnknown(m_linearProblemBidomainCurv[iProblem]->listUnknown()[iUnknown]);
              felInt idLocalValue[numDofLpU];
              felInt idGlobalValue[numDofLpU];
              for ( felInt ii = 0; ii < numDofLpU; ii++) {
                idLocalValue[ii] = ii;
              }
              ISLocalToGlobalMappingApply(m_linearProblemBidomainCurv[iProblem]->mappingDofLocalToDofGlobal(m_linearProblemBidomainCurv[iProblem]->listUnknown()[iUnknown]),numDofLpU,&idLocalValue[0],&idGlobalValue[0]);
              if (idVar==0) {
                for (felInt ii = 0; ii < numDofLpU; ii++) {
                  valueByDofU_0 = FelisceParam::instance().valueInitCond[iListVar];
                  m_U_0.setValue(idGlobalValue[ii],valueByDofU_0,INSERT_VALUES);;
                }
              } else if (idVar==1) {
                for (felInt ii = 0; ii < numDofLpU; ii++) {
                  if (ii < static_cast<int>(numDofLpU/2)) {
                    valueByDofU_0 = 0.0;
                  } else {
                    valueByDofU_0 = 0.0;
                  }
                  m_U_0.setValue(idGlobalValue[ii],valueByDofU_0,INSERT_VALUES);
                }
              }
            }
          }
        }
      }
      m_U_0.assembly();

      //Initialize solution for solver.
      m_linearProblemBidomainCurv[iProblem]->solution().copyFrom(m_U_0);

    }

  }

  void BidomainCurvModel::initializeAppCurrent() {
    m_iApp = new AppCurrentAtria();
    m_iApp->initialize(m_fstransient);
  }

  void BidomainCurvModel::initializeAppCurrentExt() {
    m_iAppExt = new AppCurrentAtria();
    m_iAppExt->initialize(m_fstransient);
  }

  void BidomainCurvModel::evalIapp() {
    m_iAppValue.clear();
    m_linearProblemBidomainCurv[0]->evalFunctionOnDof(*m_iApp,m_fstransient->time,m_iAppValue);
  }

  void BidomainCurvModel::evalIappExt() {
    m_iAppValueExt.clear();
    m_linearProblemBidomainCurv[0]->evalFunctionOnDof(*m_iAppExt,m_fstransient->time,m_iAppValueExt);
  }

  void BidomainCurvModel::finalizeRHSDP(std::size_t iProblem) {
    int size = m_linearProblemBidomainCurv[iProblem]->numDofLocalPerUnknown(potTransMemb);
    double valueForPetsc[size];
    felInt idLocalValue[size];
    felInt idGlobalValue[size];

    double coefAm = FelisceParam::instance().Am;
    double coefCm = FelisceParam::instance().Cm;


    // RHS = Am*Cm*BDF.RHS (Warning : bdf.RHS containes both problem variables).
    m_linearProblemBidomainCurv[iProblem]->vector().axpy(1.,m_bdfEdp.vector());
    m_linearProblemBidomainCurv[iProblem]->vector().scale(coefAm*coefCm);

    // RHS = RHS + Am*Iapp
    for (felInt i = 0; i < size; i++) {
      idLocalValue[i] = i;
    }
    ISLocalToGlobalMappingApply(m_linearProblemBidomainCurv[iProblem]->mappingDofLocalToDofGlobal(potTransMemb),size,&idLocalValue[0],&idGlobalValue[0]);
    for (felInt i = 0; i < size; i++) {
      felInt indexGlobal=idGlobalValue[i];
      valueForPetsc[i] = coefAm*m_iAppValue[indexGlobal];
    }
    m_linearProblemBidomainCurv[iProblem]->vector().setValues(size,&idGlobalValue[0],&valueForPetsc[0],ADD_VALUES);

    m_linearProblemBidomainCurv[iProblem]->vector().assembly();

    // For the second equation : RHS = 0 ( or applied exterior current )
    int sizePotExtraCell = m_linearProblemBidomainCurv[iProblem]->numDofLocalPerUnknown(potExtraCell);
    double valueForPetscPotExtraCell[sizePotExtraCell];
    felInt idLocalValuePotExtraCell[sizePotExtraCell];
    felInt idGlobalValuePotExtraCell[sizePotExtraCell];

    for (felInt i = 0; i < sizePotExtraCell; i++) {
      idLocalValuePotExtraCell[i] = i;
    }
    ISLocalToGlobalMappingApply(m_linearProblemBidomainCurv[iProblem]->mappingDofLocalToDofGlobal(potExtraCell),sizePotExtraCell,&idLocalValuePotExtraCell[0],&idGlobalValuePotExtraCell[0]);
    for (felInt i = 0; i < sizePotExtraCell; i++) {
      // For an applied exterior current
      if (FelisceParam::instance().hasAppliedExteriorCurrent) {
        valueForPetscPotExtraCell[i] = m_iAppValueExt[idGlobalValuePotExtraCell[i]];
      } else {
        valueForPetscPotExtraCell[i] = 0.0;
      }
    }
    m_linearProblemBidomainCurv[iProblem]->vector().setValues(sizePotExtraCell ,&idGlobalValuePotExtraCell[0],&valueForPetscPotExtraCell[0],INSERT_VALUES);

    m_linearProblemBidomainCurv[iProblem]->vector().assembly();

    if (FelisceParam::instance().typeOfIonicModel == "court") {
      m_linearProblemBidomainCurv[0]->vector().axpy(-coefAm*coefCm,m_court->ion());
    } else {
      m_linearProblemBidomainCurv[0]->vector().axpy(coefAm,m_ionic->ion());
    }

    // Elisa, Dec. 2014 - Luenberger filter
    if (FelisceParam::instance().electrodeControl) {
      m_linearProblemBidomainCurv[iProblem]->addElectrodeCondtrol();
    }

  
  }

  void BidomainCurvModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    //Initialize std::vector useful (with m_W_0) with a std::vector with same structure as _RHS in linearProblem.

    // 1) copy structure in m_W_0 (initialize solution at time 0 of EDO in schaf solver).
    m_W_0.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
    m_W_0.zeroEntries();

    // 2) copy structure in m_extrapolate (contains extrapolate values of linearProblem solution).
    m_extrapolate.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
    m_extrapolate.zeroEntries();

    m_solExtraCell.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
    m_solExtraCell.zeroEntries();

    if (FelisceParam::instance().typeOfIonicModel == "schaf") {
      double valueByDofW_0 = 1./((FelisceParam::instance().vMax - FelisceParam::instance().vMin)*(FelisceParam::instance().vMax - FelisceParam::instance().vMin));
      m_W_0.set(valueByDofW_0);
    }


    if (FelisceParam::instance().typeOfIonicModel == "court") {
      double valueByDofm = 0.00291;
      double valueByDofh = 0.965;
      double valueByDofj = 0.978;
      double valueByDofao = 0.0304;
      double valueByDofio = 0.999;
      double valueByDofua =  0.00496;
      double valueByDofui = 0.999;
      double valueByDofxr = 0.0000329;
      double valueByDofxs = 0.0187;
      double valueByDofd = 0.000137;
      double valueByDoff = 0.999837;
      double valueByDoffca = 0.775;
      double valueByDofvrel = 1.00;
      double valueByDofwrel = 0.999;
      double valueByDofnai = 11.2;
      double valueByDofnao = 140.0;
      double valueByDofcao = 1.8;
      double valueByDofki = 139.0;
      double valueByDofko = 4.5;
      double valueByDofcai = 0.000102;
      double valueByDofcmdn = 0.00205;
      double valueByDoftrpn = 0.0118;
      double valueByDofnsr = 1.49;
      double valueByDofjsr = 1.49;
      double valueByDofcsqn = 6.51;

      m_m.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_m.set(valueByDofm);
      m_h.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_h.set(valueByDofh);
      m_j.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_j.set(valueByDofj);
      m_ao.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_ao.set(valueByDofao);
      m_io.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_io.set(valueByDofio);
      m_ua.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_ua.set(valueByDofua);
      m_ui.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_ui.set(valueByDofui);
      m_xr.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_xr.set(valueByDofxr);
      m_xs.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_xs.set(valueByDofxs);
      m_d.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_d.set(valueByDofd);
      m_f.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_f.set(valueByDoff);
      m_fca.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_fca.set(valueByDoffca);
      m_urel.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_urel.set(0.);
      m_vrel.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_vrel.set(valueByDofvrel);
      m_wrel.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_wrel.set(valueByDofwrel);
      m_nai.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_nai.set(valueByDofnai);
      m_nao.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_nao.set(valueByDofnao);
      m_cao.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_cao.set(valueByDofcao);
      m_ki.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_ki.set(valueByDofki);
      m_ko.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_ko.set(valueByDofko);
      m_cai.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_cai.set(valueByDofcai);
      m_naiont.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_naiont.set(0.);
      m_kiont.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_kiont.set(0.);
      m_caiont.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_caiont.set(0.);
      m_ileak.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_ileak.set(0.);
      m_iup.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_iup.set(0.);
      m_itr.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_itr.set(0.);
      m_irel.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_irel.set(0.);
      m_cmdn.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_cmdn.set(valueByDofcmdn);
      m_trpn.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_trpn.set(valueByDoftrpn);
      m_nsr.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_nsr.set(valueByDofnsr);
      m_jsr.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_jsr.set(valueByDofjsr);
      m_csqn.duplicateFrom(m_linearProblemBidomainCurv[iProblem]->vector());
      m_csqn.set(valueByDofcsqn);
    }

    //Debug print.
    if(m_verbose > 10) {
      PetscPrintf(MpiInfo::petscComm(),"\n\t\t  m_W_0 (==m_solEDO (ionicSolver) at time == 0):");
      m_W_0.view();
    }


    // Read fibers only for testCase == 1.
    if (FelisceParam::instance().testCase == 1) {
      m_linearProblemBidomainCurv[iProblem]->readData(*io());
    }

    //Initialize heterogeneous parameters for schaf and courtemanche solvers.
    if (FelisceParam::instance().typeOfIonicModel == "schaf") {
      initializeSchafParameter();
    }
    if (FelisceParam::instance().typeOfIonicModel == "fhn") {
      initializeFhNParameter();
    } else if (FelisceParam::instance().typeOfIonicModel == "court") {
      initializeCourtParameter();
    }
  }

  void BidomainCurvModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    evalIapp();
    if (FelisceParam::instance().hasAppliedExteriorCurrent)
      evalIappExt();
    finalizeRHSDP(iProblem);
    // Calculate m_RHS = mass * RHS.
    m_linearProblemBidomainCurv[iProblem]->addMatrixRHS();
  }

  void BidomainCurvModel::initializeSchafParameter() {
    // Initialize TauClose.
    m_schaf->fctTauClose().initialize(m_fstransient);
    if (FelisceParam::instance().hasHeteroTauClose) {
      m_linearProblemBidomainCurv[0]->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose());
    }
    // Initialize TauOut : heterogeneous in case of infarct.
    m_schaf->fctTauOut().initialize(m_fstransient);
    if (FelisceParam::instance().hasInfarct) {
      m_linearProblemBidomainCurv[0]->evalFunctionOnDof(m_schaf->fctTauOut(), m_schaf->tauOut());
    }
  }

  void BidomainCurvModel::initializeFhNParameter() {
    if (FelisceParam::instance().hasInfarct) {
      m_fhn->fctSpar().initialize(m_fstransient);
      m_linearProblemBidomainCurv[0]->evalFunctionOnDof(m_fhn->fctSpar(), m_fhn->f0Par());
    }
  }

  void BidomainCurvModel::initializeCourtParameter() {
    // Initialize gToMax
    m_court->fctCourtCondIto().initialize(m_fstransient);
    // Initialize gCaLMax
    m_court->fctCourtCondICaL().initialize(m_fstransient);
    if (FelisceParam::instance().hasHeteroCourtPar) {
      m_linearProblemBidomainCurv[0]->evalFunctionOnDof(m_court->fctCourtCondIto(), m_court->courtCondIto());
      m_linearProblemBidomainCurv[0]->evalFunctionOnDof(m_court->fctCourtCondICaL(), m_court->courtCondICaL());
    }
    if (!FelisceParam::instance().hasHeteroCondAtria) {
      // Initialize multiplicative coefficient
      m_court->fctCourtCondMultCoeff().initialize(m_fstransient);
      // Warning: extraData m_linearProblemBidomainCurv[0]->Reference() size has to be numDof (not number of mesh points).
      m_linearProblemBidomainCurv[0]->evalFunctionOnDof(m_court->fctCourtCondMultCoeff(), m_court->courtCondMultCoeff(), m_linearProblemBidomainCurv[0]->Reference());
    }
  }

  // Pay attention on the call of this :
  // call BidomainModel::writeSolution() function instead of (non-virtual) Model::writeSolution() function
  void BidomainCurvModel::writeSolution() {
    if (MpiInfo::rankProc() == 0) {
      if (m_meshIsWritten == false) writeMesh();
    }

    if( (m_fstransient->iteration % FelisceParam::instance().frequencyWriteSolution == 0) or m_hasFinished) {
      if(FelisceParam::verbose() > 1) PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");
      for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {

        m_linearProblemBidomainCurv[ipb]->writeSolution(MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration);

        if(FelisceParam::instance().printIonicVar) {
          int rankProc;
          MPI_Comm_rank(MpiInfo::petscComm(),&rankProc);
          if ( m_fstransient->iteration == 0 ) {
            double * ionicSolToSave = new double[m_linearProblemBidomainCurv[ipb]->numDofPerUnknown(0)];
            m_linearProblemBidomainCurv[ipb]->fromVecToDoubleStar(ionicSolToSave, m_W_0, rankProc, 1, m_linearProblemBidomainCurv[ipb]->numDofPerUnknown(0));
            if (rankProc == 0) {
              m_linearProblemBidomainCurv[ipb]->writeEnsightScalar(ionicSolToSave, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), "ionicVar");
              m_linearProblemBidomainCurv[ipb]->writeEnsightCase(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,"ionicVar");
            }
            delete [] ionicSolToSave;

          } else {
            double * ionicSolToSave = new double[m_linearProblemBidomainCurv[ipb]->numDofPerUnknown(0)];
            m_linearProblemBidomainCurv[ipb]->fromVecToDoubleStar(ionicSolToSave, m_ionic->sol(), rankProc, 1, m_linearProblemBidomainCurv[ipb]->numDofPerUnknown(0));
            if (rankProc == 0) {
              m_linearProblemBidomainCurv[ipb]->writeEnsightScalar(ionicSolToSave, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), "ionicVar");
              m_linearProblemBidomainCurv[ipb]->writeEnsightCase(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,"ionicVar");
            }
            delete [] ionicSolToSave;
          }
        }
        if (MpiInfo::rankProc() == 0) {
          for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
            if ( m_ios[iio]->typeOutput == 1 ) // 1 : ensight{
              m_ios[iio]->postProcess(m_fstransient->time/*, !m_sameGeometricMeshForVariable*/);
          }
        }
      }
    }
  }

  void BidomainCurvModel::forward() {
    //Write solution with ensight.
    BidomainCurvModel::writeSolution();
    //Advance time step.
    updateTime(FlagMatrixRHS::only_rhs);
    printNewTimeIterationBanner();

    if ( m_fstransient->iteration == 1) {
      m_bdfEdp.initialize(m_linearProblemBidomainCurv[0]->solution());
      m_linearProblemBidomainCurv[0]->initializeTimeScheme(&m_bdfEdp);
      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolate);

      if (FelisceParam::instance().typeOfIonicModel == "court") {
        m_court->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomainCurv[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomainCurv[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomainCurv[0]->ao());
        m_court->initializeExtrap(m_extrapolate);
        m_court->initializeConcentrationsAndGateConditions(m_m, m_h, m_j, m_ao, m_io, m_ua, m_ui, m_xr, m_xs, m_d, m_f, m_fca, m_urel, m_vrel, m_wrel, m_nai, m_nao, m_cao, m_ki, m_ko, m_cai, m_naiont, m_kiont, m_caiont, m_ileak, m_iup, m_itr, m_irel, m_cmdn, m_trpn, m_nsr, m_jsr, m_csqn);
        m_buildCourt = true;
      }
      else {
        m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomainCurv[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomainCurv[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomainCurv[0]->ao());
        m_ionic->initializeExtrap(m_extrapolate);
        m_ionic->initialize(m_W_0);
        m_buildIonic = true;
      }
      if (FelisceParam::instance().dataAssimilation) {
        m_linearProblemBidomainCurv[0]->gatherSolution();
        m_linearProblemBidomainCurv[0]->initSolutionTimeBef();
        m_linearProblemBidomainCurv[0]->readDataForDA(*io(),m_fstransient->iteration);
        m_linearProblemBidomainCurv[0]->initLevelSet();
        m_linearProblemBidomainCurv[0]->levelSet(FelisceParam::instance().minLSPotTM,FelisceParam::instance().maxLSPotTM);
      }
    }
    else {
      m_bdfEdp.update(m_linearProblemBidomainCurv[0]->solution());
      m_bdfEdp.extrapolate(m_extrapolate);
      if (FelisceParam::instance().typeOfIonicModel == "court") {
        m_court->updateExtrap(m_extrapolate);
      }
      else {
        m_ionic->update(m_extrapolate);
      }
    }

    if (FelisceParam::instance().dataAssimilation) {
      m_linearProblemBidomainCurv[0]->gatherSolution();
      m_linearProblemBidomainCurv[0]->solutionTimeBef();
      m_linearProblemBidomainCurv[0]->readDataForDA(*io(),m_fstransient->iteration);
      m_linearProblemBidomainCurv[0]->levelSet(FelisceParam::instance().minLSPotTM,FelisceParam::instance().maxLSPotTM);
    }

    if (FelisceParam::instance().typeOfIonicModel == "court") {
      m_court->computeIon();
    }
    else {
      m_ionic->computeRHS();
      m_ionic->solveEDO();
      m_ionic->computeIon();
      m_ionic->updateBdf();
    }

    m_bdfEdp.computeRHSTime(m_fstransient->timeStep);

    //double timeDA =fmod(m_fstransient->iteration,1);

    // Assemble Matrix of linearProblem.
    if ( m_fstransient->iteration == 1 ) {
      m_linearProblemBidomainCurv[0]->assembleMatrixRHSBD(MpiInfo::rankProc());
    } else if (FelisceParam::instance().dataAssimilation) { //&&(timeDA == 0))
      m_linearProblemBidomainCurv[0]->assembleMatrixRHSBD(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);
    }

    //Specific operations before solve the system : add static matrix _Matrix[1] to the dynamic matrix of the system _Matrix.
    postAssemblingMatrixRHS();
    m_linearProblemBidomainCurv[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

  }
}
