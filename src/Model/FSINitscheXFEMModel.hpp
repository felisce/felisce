//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges and M. A. Fernandez
//

#ifndef _FSINitscheXFEMModel_HPP
#define _FSINitscheXFEMModel_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemFSINitscheXFEMFluid.hpp"
#include "Solver/linearProblemFSINitscheXFEMSolid.hpp"
#include "DegreeOfFreedom/duplicateSupportDof.hpp"

namespace felisce {
  
  /*!
    \brief Model class for FSI with Nitsche XFEM formulation
  */
  template<class SolidPrb> 
  class FSINitscheXFEMModel:
    public Model {
  public:
    // Constructor
    FSINitscheXFEMModel();

    // Destructor
    ~FSINitscheXFEMModel();

    void initializeDerivedLinearProblem() override;

    void initializeDerivedModel() override;

    void preAssemblingMatrixRHS(std::size_t iProblem = 0) override;

    void postAssemblingMatrixRHS(std::size_t iProblem = 0) override;

    void setExternalVec() override;

    void forward() override;

  private:
    void solveProblemFluid();
    void solveProblemSolid();

    void computeTimeSchemeExtrapol();
    void computeTimeSchemeStructureVelExtrapol();
    void computeTimeSchemeStructureExtrapolRHS();

    void writeSolutionAndMeshes();

    void updateInterfacePosition(PetscVector& disp);

    // booleans to know if a std::vector has been allocated
    bool m_isParFluidVelAllocated;
    bool m_isTimeSchemeFluidExtrapolAllocated;
    bool m_isStructureVelAllocated;
    bool m_isStructureVelOldAllocated;
    bool m_isStructureVelOldOldAllocated;
    bool m_isStructureVelOldStoppingCriteriaAllocated;
    bool m_isStructureVelExtrapolAllocated;
    bool m_isSeqStructureTimeRHSAllocated;
    bool m_isSeqStructureVelExtrapolAllocated;
    bool m_isStructureDispAllocated;
    bool m_isStructureDispOldAllocated;
    bool m_isStructureDispExtrapolAllocated;
    bool m_isStopVecAllocated;
    bool m_isStructureNitscheRHSTermsOldAllocated;
    bool m_isStructureTimeSchemeRHSTermsAllocated;
    bool m_isStructureExtrapolatedNitscheTermsRHSAllocated;

    // std::vector containing the fluid velocities of the previous time step in parallel.
    // Each PetscVector can have a different size because the number of duplicated element are different.
    // The goal is to compute the fluid velocity (scaled by a coefficient) in each element, taking
    // into account the duplication at the previous time instants.
    // The size is the same as the order of the time scheme.
    // It is used in the fluid problem to compute:
    //   - RHS coming from the time derivatives
    //   - advection term
    //   - Temam's trick
    //   - SUPG stabilization
    //   - Inflow stabilization (to compute u^* \cdot n)
    //   - Coefficients of the CIP stabilization
    std::vector<PetscVector> m_parFluidVel;


    // std::vector containing the old fluid velocities (parallel) scaled to compute the extrapolation
    // of the fluid velocity for the time schemes.
    // The size of this std::vector is the RN order for scheme 0 and 2, and the order given in the third
    // component of the element field in the data for scheme 3 and 4 (the order of the initial guess of the
    // velocity in the fix point loop for these schemes)
    std::vector<PetscVector> m_timeSchemeFluidExtrapol;


    // velocity of the solid (parallel), used to compute the extrapolation of the velocity
    // of the solid. It is also used to compute the stopping criteria in the stabilized
    // explicit and semi-implicit schemes.
    PetscVector m_structureVel;

    // velocity of the solid at the previous time instant (parallel)
    PetscVector m_structureVelOld;

    // velocity of the solid at the previous previous time instant (parallel)
    PetscVector m_structureVelOldOld;

    // velocity of the solid to compute the stopping criteria (parallel, allocated if needed only)
    PetscVector m_structureVelOldStoppingCriteria;

    // extrapolation of the velocity of the solid (parallel)
    // For scheme 0 and 2, it really is an extrapolation
    // For the other scheme, this is the solid velocity
    PetscVector m_structureVelExtrapol;

    // solid part coming from the time derivative (2 d^{n-1} - d^{n-2})
    // This is given to the solid problem with the first external vec (serial)
    PetscVector m_seqStructureTimeRHS;

    // extrapolation of the velocity of the solid (serial)
    // For scheme 0 and 2, it really is an extrapolation
    // For the other scheme, this is the solid velocity
    PetscVector m_seqStructureVelExtrapol;

    // displacement at the previous time instant (parallel)
    PetscVector m_structureDispOld;

    // old rhs of the solid problem coming from the Nitsche formulation with scheme 4.
    // Not allocated if we use another scheme or if the order of extrapolation is smaller than 2
    PetscVector m_structureNitscheRHSTermsOld;

    // contains the rhs after the assembly of the rhs in the reference configuration of the solid.
    // It is removed from the rhs before the solve of the solid to keep only the rhs coming from
    // the Nitsche formulation. Allocated only when using the scheme 4
    PetscVector m_structureTimeSchemeRHSTerms;

    // Extrapolation of the Nitsche terms in the rhs of the real solid problem for scheme 4
    // Not allocated if we are not using scheme 4.
    PetscVector m_structureExtrapolatedNitscheTermsRHS;

    // std::vector used to compute the stopping criteria (parallel)
    PetscVector m_stopVec;

    // Pointer to the fluid and solid linear problems
    LinearProblemFSINitscheXFEMFluid* m_pLinFluid;
    LinearProblemFSINitscheXFEMSolid<SolidPrb>* m_pLinStruc;

    // bdf for the fluid and solid
    Bdf m_bdfFluid;

    // Information about the duplication of the support elements
    DuplicateSupportDof m_intersectMeshes;

    // Finite elements for the fluid and solid
    CurvilinearFiniteElement* m_feStruc;
    CurrentFiniteElement* m_feVel;
    CurrentFiniteElement* m_fePre;

    std::vector<double> m_interfaceDisplacement;

    ElementFieldDynamicValue *m_elemFieldTimeSchemeType;
    felInt m_timeSchemeType;
    felInt m_subIteration;
    double m_stoppingCriteriaTol;
    felInt m_timeSchemeExtrapolOrder;
    felInt m_RNExtrapolOrder;

    felInt m_indexTime;

    felInt m_countToInitOldObjects;
    bool m_reinitializeFluidBdf;

    felInt m_numStrucComp;
  };
}

#include "FSINitscheXFEMModel.tpp"

#endif
