//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __NSHEATMODEL_HPP__
#define __NSHEATMODEL_HPP__

// System includes

// External includes

// Project includes
#include "Model/NSModel.hpp"
#include "Solver/linearProblemNSHeat.hpp"

namespace felisce {
  class NSHeatModel: public NSModel {
  public:
    NSHeatModel();
    void forward() override;
  };
}
#endif
