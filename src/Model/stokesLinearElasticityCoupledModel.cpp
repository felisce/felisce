//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/stokesLinearElasticityCoupledModel.hpp"
#include "InputOutput/io.hpp"

namespace felisce {
  // ======================================================================
  //                      the public methods
  // ======================================================================

  // constructor: it first call constructor of mother class
  StokesLinearElasticityCoupledModel::StokesLinearElasticityCoupledModel():LinearElasticityModel(),m_timeChecker(nullptr) {
    // change the name of the model
    m_name="Linear Elasticity coupled with Stokes equations";
    // fixing the id of the stokes problem
    // this should be done automatically based on 
    // the names of the linearProblems or on 
    // same check on the type.
    m_idLE=0;
    m_idStokes=1;
    m_interfaceLabels=FelisceParam::instance().fsiInterfaceLabel;
  }

  
  // this function override LinearElasticityModel::initializeDerivedModel()
  // it is then call in initializeModel in the Model class
  void
  StokesLinearElasticityCoupledModel::initializeDerivedModel() { 
    // first a call to the ovverriden method to initialize the mother class
    LinearElasticityModel::initializeDerivedModel();

    // Two static casts to initialize the pointers
    // they are usufull to call functions defined in derived linear problems,
    // but not declared in linearProblem class.
    m_lpbLE     = static_cast<LinearProblemLinearElasticity*> ( m_linearProblem[ m_idLE ]);
    m_lpbStokes = static_cast<LinearProblemNSRS*> ( m_linearProblem[ m_idStokes ] );

    // Setting a flag in LE linProb to true to switch several functionalities on
    m_lpbLE->coupled(true);
    m_lpbLE->initPetscVecsForCoupling();
    // The dofBoundary object of each linear problem is initialized.
    m_lpbLE     ->initializeDofBoundaryAndBD2VolMaps();
    m_lpbStokes ->initializeDofBoundaryAndBD2VolMaps();
    // We build the std::unordered_map the store the correspondence labels of the two meshes
    // it needs to be called after the buildListOfBoundaryPetscDofs
    this->defineMapLabelInterface();
    
    // interface labels are printed on the video to check everything is all right
    if ( FelisceParam::verbose() > 1 ) { 
      std::stringstream labelsStokes, labelsLE;
      labelsStokes    <<"Interface labels of the Stokes mesh            : ";
      labelsLE        <<"Interface labels of the Linear Elasticity mesh : ";
      for(auto it(m_labStokes2LE.begin()); it != m_labStokes2LE.end(); ++it ) {
        labelsStokes  << it->first  << '\t';
        labelsLE      << it->second << '\t';
      }
      labelsStokes    << std::endl;
      labelsLE        << std::endl;
      
      PetscPrintf(MpiInfo::petscComm(),"%s", labelsLE.str().c_str());
      PetscPrintf(MpiInfo::petscComm(),"%s", labelsStokes.str().c_str());
    }
  }

  // Function to initialize the std::unordered_map m_labStokes2LE
  // In this model we make the assumption that this std::unordered_map is the identity, however be build it because of compatibility reason.
  //
  // The side effect of this function, and the real reason why so many lines are needed for this, is to check that the different mapping were correctly built in the previous function
  // the name of the function could be something like initMapLabelInterfaceAndCheckEverything
  void 
  StokesLinearElasticityCoupledModel::defineMapLabelInterface() {
    //saving the std::unordered_map that associates the first point (in the lexicographical order)
    //to each label
    std::map<int, Point > mapStokes = m_lpbStokes->dofBD(/*iBD*/0).getMapLab2FirstPoint();
    std::map<int, Point > mapLE  = m_lpbLE->dofBD(/*iBD*/0).getMapLab2FirstPoint();
    
    // Checking that the number of labels from the surfaces are the same
    std::stringstream errLine;
    errLine<<" Different number of boundary labels describing the interfaces: "<<mapStokes.size()<< "!=" <<mapLE.size()<<std::endl;
    FEL_CHECK(mapStokes.size()==mapLE.size(),errLine.str().c_str());
    
#ifndef NDEBUG
    int counterOfNotPairedPoints(0);
    
    // For readibility, really waiting for c++11
    typedef std::map<int, std::vector< Point > >  map_type;
     
    map_type allPointsStokes = m_lpbStokes->dofBD(/*iBD*/0).lab2AllPoints();
    map_type allPointsLE     = m_lpbLE->dofBD(/*iBD*/0).lab2AllPoints();
    auto listOfPointPerLabelIteratorStokes   = allPointsStokes.begin();
    auto listOfPointPerLabelIteratorLE       = allPointsLE.begin();
    
    for ( ; /* no initialization needed */
          listOfPointPerLabelIteratorStokes != allPointsStokes.end() && listOfPointPerLabelIteratorLE != allPointsLE.end();
          ++listOfPointPerLabelIteratorStokes, ++listOfPointPerLabelIteratorLE ) {

      std::size_t numPointPerLabelStokes = listOfPointPerLabelIteratorStokes->second.size();
      std::size_t numPointPerLabelLE = listOfPointPerLabelIteratorLE->second.size();
      
      FEL_CHECK( numPointPerLabelStokes == numPointPerLabelLE, "Different number of points at the interface for current label\n");

      auto pointStokesIterator = listOfPointPerLabelIteratorStokes->second.begin();
      auto pointLEIterator = listOfPointPerLabelIteratorLE->second.begin();
        
      for( ; /* no initialization needed */
           pointStokesIterator != listOfPointPerLabelIteratorStokes->second.end() && pointLEIterator != listOfPointPerLabelIteratorLE->second.end();
           ++pointStokesIterator, ++pointLEIterator) {
        
        if ( *pointStokesIterator != *pointLEIterator ) {
          ++counterOfNotPairedPoints;
        }
      }
      if ( counterOfNotPairedPoints > 0 ) {
        std::stringstream aus;
        aus<<"checking the interface: "<<counterOfNotPairedPoints<<" points differ, out of <<"<<listOfPointPerLabelIteratorLE->second.size()<<std::endl;
        FEL_ERROR(aus.str().c_str());
      }
      MPI_Barrier(MpiInfo::petscComm()); //We wait for all the procs

      std::stringstream msg;
      msg<<"All right in the mapping: there is a one to one correspondance between all the nodes at the interface"<<std::endl;
      PetscPrintf(MpiInfo::petscComm(), "%s",msg.str().c_str());
    }
#endif

    // for each label of the interface in the Stokes mesh we check the first point
    for(auto firstPointPerLabelStokesIterator = mapStokes.begin(); firstPointPerLabelStokesIterator != mapStokes.end(); ++firstPointPerLabelStokesIterator) {
      // we look for corresponding the label in the other mesh 
      bool Found=false;
      // for each label of the LE mesh 
      for(auto firstPointPerLabelLEIterator = mapLE.begin(); firstPointPerLabelLEIterator != mapLE.end() && !Found; ++firstPointPerLabelLEIterator) {
        // if the first point is the same for this couple of labels
        if ( firstPointPerLabelStokesIterator->second == firstPointPerLabelLEIterator->second ) {
          // then we found the corresponding label
          Found=true;
          // we fill the std::unordered_map
          m_labStokes2LE [ firstPointPerLabelStokesIterator->first ] = firstPointPerLabelLEIterator->first; // <-- key line of the function
        }
      }
      // if we arrived here and we did not find a label it means that ...
      FEL_CHECK(Found," one label of the interface of the Stokes mesh is not present in the LE mesh");
    }
  }


  // Forward function:
  // it overrides NSSimplifiedFSI function, this time we really want to override
  // and we are not going to call the forward method of the mother class inside
  void
  StokesLinearElasticityCoupledModel::forward() {

    //! First several objects and vectors that will be used through
    //! the different iterations are initialized/computed
    
    if ( m_fstransient->iteration == 0 ) {

      //(1)
      //! Chronometers initializations
      m_timeChecker       = felisce::make_shared<ChronoInstance>();
      m_t1                = felisce::make_shared<ChronoInstance>(); 
      m_t2                = felisce::make_shared<ChronoInstance>(); 
      m_lpbStokes->initChronoRS();
      //Total time of forward including all kind of overheads
      m_timeChecker->start();
      
      //(2)
      //! The vectors stores in m_vecs and in m_petscVecs are initialized!
      m_t1->start();
      m_lpbStokes->initPetscVectors();
      m_t1->stop();
      m_t2->start();
      m_lpbLE->initPetscVectors();

      //(3)
      //! The normal field is computed in m_lpbLE and then sent to the stokes.
      m_lpbLE->computeNormalField(m_interfaceLabels,m_lpbLE->iDisplacement());
      m_t2->stop();
      //! In the first time & fixed point iteration the P1 normal-field is exported from LE and read into Stokes
      //! differently from the linearProblemPerfectFluid where we do not have a std::vector variable to save
      //! the normal, here we have all three components of the normalField into another normalField.
      m_t1->start();
      m_lpbStokes->readerInterface(m_lpbLE->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(),
                                   m_lpbLE->get(LinearProblem::sequential,"normalField"),
                                   m_labStokes2LE,
                                   "normalField",
                                   m_lpbStokes->iUnknownVel(),
                                   0/*iUnkComp0, where to read*/
                                   );       
      //(4)
      // Steklov Part!
      if ( FelisceParam::instance().computeSteklov ) {
        if( FelisceParam::instance().useRoSteklov ) {
          const int ivar  = m_lpbStokes->listVariable().getVariableIdList(velocity);
          const int imesh = m_lpbStokes->listVariable()[ivar].idMesh();
          m_lpbStokes->getRings();
          m_lpbStokes->assembleLowRankSteklov(imesh);
          m_lpbStokes->exportAllEig(imesh);
        } else {
          m_lpbStokes->assembleFullRankSteklov();
        }
        // we clean the RHS: we are going to create a new one in solveStokes
        m_lpbStokes->clearMatrixRHS(FlagMatrixRHS::only_rhs);
      }
      m_t1->stop();
      
      m_timeChecker->stop();
    }

    m_timeChecker->start();

    //we do not count the time here since it is only post-processing
    //std::cout<<"PrepareForwardStokes: "<<m_t1->diff()<<std::endl;
    if ( m_fstransient->iteration == 0 ) {
      
      m_t1->start();
      this->prepareForwardStokes(); // It is important to prepare before the stokes and then LE because in stokes we check for iteration == 0
      m_t1->stop();

      m_t2->start(); 
      this->prepareForward(FlagMatrixRHS::matrix_and_rhs);       // We use the prepare forward of the LEModel, we have updateTime here! There is a clean matrixrhs here!
      m_lpbLE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::only_matrix);
      m_lpbLE->addMatrixRHS();
      m_t2->stop();
    } else {
      m_t2->start(); 
      this->prepareForward(FlagMatrixRHS::only_rhs);       // We use the prepare forward of the LEModel, we have updateTime here! There is a clean matrixrhs here!
      m_t2->stop(); 
    }
    
    // initialize the test quantity to one.
    // and the corresponding tollerance from the data file
    double testQuantity(1.0), toll(FelisceParam::instance().domainDecompositionToll);

    // this two integers store the current number of sub-iterations 
    // and of gmres iterations for the current time step
    felInt cIteration(0), cGmresItLE(0), cGmresItStokes(0);

    // subiteration of domain decomposition
    while ( testQuantity > toll && cIteration < FelisceParam::instance().domainDecompositionMaxIt) {
      // update the it counter and display a nice banner
      cIteration++;
      this->displayBannerIterationInit(cIteration);

      /*! LinearElasticity -- Solution Phase 
       *  
       *  The linearElasticity problem is solved
       */
      m_t2->start();
      if ( m_fstransient->iteration == 1 ) {
        m_lpbLE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::only_rhs);
        this->solveLinearElasticity(FlagMatrixRHS::matrix_and_rhs);
      }
      else {
        m_lpbLE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::only_rhs);
        this->solveLinearElasticity(FlagMatrixRHS::only_rhs);
      }
      cGmresItLE += m_lpbLE->kspInterface().getNumOfIteration();

      /*! LinearElasticity -- Post-processing
       * 
       *  Now the linear elasticity problem has been solved.
       *
       *  We have to compute the velocity of the solid at the interface
       *  The current acceleration is also computed in order to compute the velocity.
      */
      m_lpbLE->gatherSolution();
      m_lpbLE->updateCurrentVelocity();
      m_t2->stop();
      /*! Communication phase 
       *
       * Now that the quantity has been computed the Stokes problem has to read it.
       * It is a std::vector quantity
      */
      m_t1->start();
      m_lpbStokes->readerInterface( m_lpbLE->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(),
                                    m_lpbLE->get(LinearProblem::sequential,"velocityCurrent"),
                                    m_labStokes2LE,
                                    "externalVelocity",
                                    m_lpbStokes->iUnknownVel(),
                                    0/*iUnkComp0, where to read it starts from this and then it loops over the space dimension*/
                                    );
      /*! Stokes -- Solution phase
       *
       *  After that the data have been read we proceed and solve the Stokes problem
       *  depending on the data file this will be done either with or without 
       *  using the Steklov operator in its full or reduced version
       */
      this->solveStokes(cIteration);
      cGmresItStokes += FelisceParam::instance().computeSteklov?0:m_lpbStokes->kspInterface().getNumOfIteration();

      /*! Stokes -- Post-processing
       *
       *  After the problem solution we need to compute the normal stress at the interface.
       *
       */
      m_lpbStokes->gatherSolution();
      m_lpbStokes->computeResidualRS();
      m_t1->stop();
      /*! Communication phase 
       *
       * Now that the quantity has been computed the Stokes problem has to read it.
       * It is a std::vector quantity
      */
      m_t2->start();    
      m_lpbLE->accelerationPreStep(m_lpbLE->get(LinearProblem::sequential,"externalStress"));
      m_lpbLE->readStressInterface(m_lpbStokes->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(),
                                   m_lpbStokes->seqResidual(),
                                   /*iUnknComp0*/0);

      /*! Convergence test phase
       *
       *  The test quantities are the normalized L2-norms of the increments
       *  of the two quantities exchanged at the interface:
       *  - the velocity        ( from LE to Stokes )
       *  - the normal stresses ( from Stokes to LE )
       */
      std::pair<double, double> testQuantities = m_lpbLE->computeTestQuantities();
      double testVelocity  = testQuantities.first;
      double testStresses = testQuantities.second;
      testQuantity = testVelocity + testStresses;
      
      m_lpbLE->accelerationPostStep( m_lpbLE->get(LinearProblem::sequential,"externalStress"), cIteration );
      
      m_t2->stop();
      // we display a nice banner
      this->displayBannerIterationEnd(cIteration,testQuantity,testVelocity,testStresses);
      // we clear only the RHS
      // it is important to clear the rhs here, because otherwise you will sum into the previous one
      // the former contribution
      // We do not clear the matrix since we want to re-use it
      this->clearMatrixRHSOfPbs(FlagMatrixRHS::only_rhs);
    }
    if ( cIteration == FelisceParam::instance().domainDecompositionMaxIt ) {
      std::stringstream war;
      war<<"************* Maximum number of iterations reached in "<<__FILE__<<":"<<__LINE__<<"     *********************"<<std::endl;
      FEL_WARNING(war.str().c_str());
    }

    m_timeChecker->stop();

    //! we finalize the forward
    this->finalizeForward(cIteration,cGmresItLE,cGmresItStokes);
  }

  void StokesLinearElasticityCoupledModel::prepareForwardStokes() {
    m_lpbStokes->assembleMatrixRHS(MpiInfo::rankProc());
    // This is necessary since the stokes matrix is in the matrix(1) of linearProblemNS.
    // We have to add it all the time to the matrix(0)
    m_lpbStokes->addMatrixRHS();
    m_lpbStokes->createAndCopyMatrixRHSWithoutBC();
  }
  
  void
  StokesLinearElasticityCoupledModel::writeTimeInfo() {
    
    // we convert the relaxation parameter into a std::string
    std::stringstream aus;
    aus<<MpiInfo::rankProc();

    // we open a file in the folder before the result dir
    // overwriting the previous content
    // the information are described in the header prined here
    std::ofstream iterationFile( std::string(FelisceParam::instance().resultDir + "timeInfo"+aus.str()+".dat").c_str() , std::ios::trunc );
    iterationFile <<"TimeStep"<<'\t'
                  <<"fixedPointIterations"<<'\t'
                  <<"totalGmresIterationsLE"<<'\t'
                  <<"totalGmresIterationsStokes"<<'\t'
                  <<"gmresIterationsPerFixedPointIt"<<'\t'
                  <<"cumulatedTime"<<'\t'
                  <<"onlyForStokes"<<'\t'
                  <<"onlyForLE"
                  <<std::endl;
    for ( std::size_t ts(0); ts<m_numFixedPointItPerTimeStep.size(); ++ts) {
      iterationFile<<ts+1<<'\t'
                   <<m_numFixedPointItPerTimeStep[ts]<<'\t'
                   <<m_totNumOfGMRESItPerTimeStepLE[ts]<<'\t'
                   <<m_totNumOfGMRESItPerTimeStepStokes[ts]<<'\t'
                   <<double(m_totNumOfGMRESItPerTimeStepLE[ts] + m_totNumOfGMRESItPerTimeStepStokes[ts])/double(m_numFixedPointItPerTimeStep[ts])<<'\t'
                   <<m_cumulatedForwardTime[ts]<<'\t'
                   <<m_cumulatedStokesTime[ts]<<'\t'
                   <<m_cumulatedLETime[ts]
                   <<std::endl;
    }
    //we close the file
    iterationFile.close();    
  }
  
  // ======================================================================
  //                   now the privates methods
  // ======================================================================

  // this function overrides 
  //     LinearElasticityModel::finalizeForward();
  void 
  StokesLinearElasticityCoupledModel::finalizeForward(int cIteration, int cGmresItLE, int cGmresItStokes) {
    // we call the function of the mother class
    LinearElasticityModel::finalizeForward();
        
    //first we store the new data
    m_numFixedPointItPerTimeStep.push_back(cIteration);
    m_totNumOfGMRESItPerTimeStepLE.push_back(cGmresItLE);
    m_totNumOfGMRESItPerTimeStepStokes.push_back(cGmresItStokes);
    
    m_cumulatedForwardTime.push_back(m_timeChecker->diff_cumul());
    m_cumulatedStokesTime.push_back(m_t1->diff_cumul());
    m_cumulatedLETime.push_back(m_t2->diff_cumul());

    writeTimeInfo();
  }

  // we reset the interface quantities of both problem
  // typically we are going to use this function with
  //    flag = only-rhs
  // but there is no default
  void 
  StokesLinearElasticityCoupledModel::clearMatrixRHSOfPbs( FlagMatrixRHS flag) {
    m_lpbLE->clearMatrixRHS(flag);
    m_lpbStokes->clearMatrixRHS(flag);
  }
  
  // this function solve the Stokes problem
  // we do not have a similar function for NS since it is in the mother class
  void 
  StokesLinearElasticityCoupledModel::solveStokes(int nfp) {
    if ( FelisceParam::instance().computeSteklov ) {
      if ( FelisceParam::verbose() )
        PetscPrintf(MpiInfo::petscComm(),"Stokes problem using Steklov operator\n");
      m_lpbStokes->solveStokesUsingSteklov(m_fstransient, nfp);
    } else {
      if ( FelisceParam::verbose() )
        PetscPrintf(MpiInfo::petscComm(),"Solving Stokes equations \n");
      // In the first fixed point iteration, we assemble both matrix and rhs, consider that for stokes the matrix does not really change since we have no advection.
      // In the following one we only recompute the the rhs.

      // Some things to think about it..
      // (1) Stokes does not have advection, so the matrix is all in matrix(1)
      // (2) We cannot have time-depending force term because the reduced steklov method does not allow it.
      // (3) the only things that can vary with time are the not-constant boundary conditions which are below.

      if ( nfp == 1 && m_fstransient->iteration == 1) {
        m_lpbStokes->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs); // important for supg
        m_lpbStokes->addMatrixRHS();
        m_lpbStokes->createAndCopyMatrixRHSWithoutBC();
        m_lpbStokes->finalizeEssBCTransient(); // I think it is in preparation of applyBC why then it is not into applyBC?
        m_lpbStokes->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs,FlagMatrixRHS::matrix_and_rhs); // there is a call here to assemblyNaturalBoundaryConditions.
      }
      m_lpbStokes->finalizeEssBCTransient(); // I think it is in preparation of applyBC why then it is not into applyBC?
      m_lpbStokes->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs,FlagMatrixRHS::only_rhs); // there is a call here to assemblyNaturalBoundaryConditions.
      
      m_lpbStokes->chronoRS()->start();
      
      m_lpbStokes->solve(MpiInfo::rankProc(), MpiInfo::numProc());
      m_lpbStokes->chronoRS()->stop();
      m_lpbStokes->exportOutputSteklov(m_fstransient,nfp);
    }
  }

  // two nice banners for the sub-iterations
  void 
  StokesLinearElasticityCoupledModel::displayBannerIterationInit( felInt cIt ) const {
    
    std::stringstream banner;
    for( felInt k(0); k<25; k++)
      banner<<"=";
    banner<<">  iteration    "<<cIt<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s", banner.str().c_str());
  }
  void 
  StokesLinearElasticityCoupledModel::displayBannerIterationEnd( felInt /*cIt*/, double testQuantity, double testVelocity, double testStresses ) const {
    
    std::stringstream banner;
    for( felInt k(0); k<25; k++)
      banner<<"=";
    banner<<">        Test Quantity :"<<testQuantity<<std::endl;
    for( felInt k(0); k<25; k++)
      banner<<" ";
    banner<<"              Velocity :"<<testVelocity<<std::endl;
    for( felInt k(0); k<25; k++)
      banner<<" ";
    banner<<"              Stresses :"<<testStresses<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s", banner.str().c_str());
  }
}
