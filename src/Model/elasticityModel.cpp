//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

/*!
 \file ElasticityModel.cpp
 \authors S. Gilles
 \date 21/02/2013
 \brief Model to manage elasticity problem.
 */

// System includes

// External includes

// Project includes
#include "Model/elasticityModel.hpp"
#include "Solver/linearProblemElasticity.hpp"

namespace felisce 
{

ElasticityModel::ElasticityModel() 
  : Model() 
{
  m_name = "Elasticity";
}

/***********************************************************************************/
/***********************************************************************************/

ElasticityModel::~ElasticityModel()
= default;

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::SolveStaticProblem() 
{
#ifndef NDEBUG
  static bool first_call = true;
  assert(first_call);
  first_call = false;
#endif // NDEBUG

  PetscPrintf(MpiInfo::petscComm(), "\n----------------------------------------------\n");
  PetscPrintf(MpiInfo::petscComm(), "Static problem\n");
  PetscPrintf(MpiInfo::petscComm(), "----------------------------------------------\n");

  LinearProblemElasticityDynamic& linear_problem = dynamic_cast<LinearProblemElasticityDynamic&>(*m_linearProblem[0]);

  //Assembly loop on elements.
  linear_problem.assembleMatrixRHS(MpiInfo::rankProc());

  //Apply boundary conditions.
  linear_problem.finalizeEssBCTransient();
  linear_problem.applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs);

  // Solve the system.
  linear_problem.solve(MpiInfo::rankProc(), MpiInfo::numProc());

  // Update the displacement for the first time iteration.
  linear_problem.UpdateCurrentDisplacement();
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::SolveDynamicProblem() 
{
  SolveStaticProblem();
  AssembleNewmarkMatrices();

  while (!hasFinished())
    forward();
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::updateTime(const FlagMatrixRHS flagMatrixRHS) 
{
  IGNORE_UNUSED_FLAG_MATRIX_RHS;
  m_fstransient->iteration++;
  m_fstransient->time +=m_fstransient->timeStep;
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::AssembleNewmarkMatrices() 
{
  LinearProblemElasticityDynamic& linear_problem = dynamic_cast<LinearProblemElasticityDynamic&>(*m_linearProblem[0]);
  linear_problem.GoToDynamic();
  linear_problem.clearMatrixRHS();
  linear_problem.assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_matrix);
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::forward() 
{
  LinearProblemElasticityDynamic& linear_problem = dynamic_cast<LinearProblemElasticityDynamic&>(*m_linearProblem[0]);

  // Write solution for postprocessing (if required)
  writeSolution();

  // Advance time step.
  updateTime();

  // Print time information
  printNewTimeIterationBanner();

  // In the dynamic system only the RHS is modified each iteration; it is a mere algebric operation.
  linear_problem.ComputeRHS();

  //Apply boundary conditions.
  linear_problem.finalizeEssBCTransient();
  linear_problem.applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs,
                          FlagMatrixRHS::matrix_and_rhs, 0, false, ApplyNaturalBoundaryConditions::no);

  //Solve linear system.
  linear_problem.solve(MpiInfo::rankProc(), MpiInfo::numProc());

  linear_problem.endIteration();
}

/***********************************************************************************/
/***********************************************************************************/

int ElasticityModel::getNstate() const 
{
  return m_linearProblem[0]->numDof();
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::getState(double* & state) 
{
  m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::getState_swig(double* data, felInt numDof) 
{
  double * state;
  m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());

  for (felInt i = 0;  i < numDof ; i++)
    data[i] = state[i];
}

/***********************************************************************************/
/***********************************************************************************/

void ElasticityModel::setState(double* & state) 
{
  m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
}


} // namespace Elasticity
