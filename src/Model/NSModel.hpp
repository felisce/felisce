//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef _NSModel_HPP
#define _NSModel_HPP

// System includes

// External includes
#include <petscvec.h>

// Project includes
#include "Core/felisceParam.hpp"
#include "Model/immersed.hpp"
#include "Model/model.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacCycle.hpp"
#include "Solver/linearProblemNS.hpp"
#include "Solver/linearProblemHarmonicExtension.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Solver/RISModel.hpp"

namespace felisce {

  class NSModel:
    public Model {

    public:
      //!Construtor.
      NSModel();
      //!Destructor.
      ~NSModel() override;

      void initializeDerivedModel() override;
      void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
      void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
      //void extrapolate(bool flag=true);

      void setInitialCondition() override;
      
      //! Manage time iteration.
      void forward() override;
      void RISforward();
      void prepareForward();
      #ifdef FELISCE_WITH_CVGRAPH
      void cvgraphNewTimeStep() override;
      #endif
      void solveNS();
      void solveLinearizedNS(bool linearFlag);

      //! Function to get size of the state std::vector.
      int getNstate() const override;
      //! Function to get state std::vector.
      void getState(double* & state) override;
      felInt numDof_swig();
      void getState_swig(double* data, felInt numDof);
      //! Function to std::set state std::vector.
      void setState(double* & state) override;

      inline const Bdf & bdf() const { return m_bdfNS; }
      inline Bdf & bdf() { return m_bdfNS; }
      void setExternalVec() override;

      //! for FSI coupling
      PetscVector& seqResidual();
      PetscVector& residual();
      PetscVector& lumpedMassRN();
      int& hasLumpedMassRN();

      void solveSystem(int);
      void advanceTimeStep();

      //! Getter for immersed model
      Immersed& immersed();  

      //! Fictitious + lagrange multiplier
      inline void setInterfaceExchangedData(std::vector<double>& itfDisp) { m_itfDisp = &itfDisp; };
      void writeCurrentNormalVector(const int mesh_id, const std::string name);
      void writeCurrentDisplacement(const int mesh_id, const std::string name);

    private:

      void computeLumpedModelBC();
      void firstIteration();

      void initializeStiffnessVector(std::vector<felInt>& mustChangeElementStiffness,int ref);
      void initializeStiffnessVector(std::vector<felInt>& mustChangeElementStiffness,std::vector<int> vectorRef);

      //! ALE
      void computeALEterms();
      void UpdateMeshByVelocity();
      void UpdateMeshByDisplacement();
      void solveHarmExt();
      void updateMesh();
      void computePreStressedDispMeshVector();

      //!EDP time scheme.
      Bdf m_bdfNS;
      Bdf* m_CrankNicolsonNS; //Manage midpoint approximation of u^{n+1} in the convective term

      // RIS valve model
      CardiacCycle* m_cardiacCycle;
      RISModel* m_risModel;
      bool m_chek_use_RISforward;
      PetscMatrix m_storedMatrix;
      PetscVector  m_solExtrapolate;

      std::vector<int>    m_labelFluxListLumpedModelBC;
      std::vector<double> m_fluxLumpedModelBC;
      LumpedModelBC*      m_lumpedModelBC;

      //! ALE
      PetscScalar m_tau;
      PetscVector m_dispMesh;
      PetscVector m_dispMesho;
      PetscVector m_dispMesho_pp;
      PetscVector m_velMesh;
      PetscVector m_solDispl;
      PetscVector m_solDispl_0;
      std::vector<PetscScalar> m_dispMeshVector;
      std::vector<PetscScalar> m_preStressedDispMeshVector;
      int m_numDofExtensionProblem;

      //! Penalization
      PetscVector m_solPrev;

      //! Immersed 
      PetscVector m_delta; // spatially dependent stabilization coef (by element) supg
      Immersed m_immersed;

      //! Fictitious + lagrange multiplier
      std::vector<double>* m_itfDisp = nullptr;
      std::vector<double> m_realDisp;
      std::vector<double> m_fictDisp;

      // Pointer to the linear problems
      LinearProblemNS*                m_pSolverNS = nullptr;
      LinearProblemHarmonicExtension* m_pSolverHE = nullptr;

      // with imposed displacements
      std::vector<PetscScalar> m_tmpDisp;
      PetscScalar m_displTimeStep;
      int m_cycl;

      // for fsi coupling
      bool m_matrixWithoutBCcreated;
      std::vector<PetscInt> m_petscToGlobal;

      PetscVector m_residual;
      PetscVector m_seqResidual;
      PetscVector m_rhsWihoutBC;
      PetscMatrix m_matrixWithoutBC;

      int m_hasLumpedMassRN = 0;
      PetscVector m_lumpedMassRN;
  };
}

#endif
