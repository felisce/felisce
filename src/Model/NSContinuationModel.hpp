//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef _NSCONTINUATIONMODEL_HPP
#define _NSCONTINUATIONMODEL_HPP

// System includes

// External includes

// Project includes
#include <Model/model.hpp>
#include <Core/felisceParam.hpp>
#include <Solver/bdf.hpp>
#include <Solver/linearProblemNSContinuation.hpp>

namespace felisce {
  class NSContinuationModel:public Model{
  public:
    //!Constructor.
    NSContinuationModel();
    //!Destructor.
    ~NSContinuationModel() override= default;

    void initializeDerivedModel() override;    
    void postAssemblingMatrixRHS(size_t iProblem=0) override;
    void preAssemblingMatrixRHS(size_t iProblem=0) override;

    void setExternalVec() override;

    //! Manage time iteration.
    virtual void prepareForward();
    virtual void solveContinuation();
    void forward() override;


    inline PetscVector& displacement() { return m_dispSeq;}


  protected:
    void firstIteration();

    LinearProblemNSContinuation * m_lpb;

    ChronoInstance chronoAssemble, chronoSolve;

    PetscVector m_disp, m_dispSeq;

    std::vector<PetscInt> m_petscToGlobal;
    PetscScalar m_displTimeStep;

    //! ALE
    void computeALEterms();
    void UpdateMeshByDisplacement();
    void solveHarmExt();
    void updateMesh();

    //! ALE
    PetscScalar m_tau;
    PetscVector m_dispMesh;
    PetscVector m_velMesh;
    PetscVector m_solDispl;
    PetscVector m_solDispl_0;
    std::vector<PetscScalar> m_dispMeshVector;
    int m_numDofExtensionProblem;
    std::vector<PetscScalar> m_tmpDisp; // used to extract the values of m_dispMesh


  };
}

#endif
