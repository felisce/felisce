//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/IOPcouplModel.hpp"
#include "InputOutput/io.hpp"

namespace felisce {
  // ======================================================================
  //                      the public methods
  // ======================================================================

  // constructor: it first call constructor of mother class
  IOPcouplModel::IOPcouplModel():NSSimplifiedFSIModel(),m_timeChecker(nullptr) {
    // change the name of the model
    m_name="Navier Stokes coupled with Perfect Fluid";
    // fixing the id of the perfect fluid problem
    // this shuold be done automatically based on 
    // the names of the linearProblems or on 
    // same check on the type.
    m_idPF=1;
    m_interfaceLabels=FelisceParam::instance().labelIOPMesh;
  }

  
  // this function override NSSimplifiedFSIModel::initializeDerivedModel()
  // it is then call in initializeModel in the Model class
  void
  IOPcouplModel::initializeDerivedModel() { 
    // first a call to the ovverriden method to initialize the mother class
    NSSimplifiedFSIModel::initializeDerivedModel();
    // Two static casts to initialize the pointers
    // they are usufull to call functions defined in derived linear problems,
    // but not declared in linearProblem class.
    m_lpbNS = static_cast<LinearProblemNSSimplifiedFSI*> ( m_linearProblem[ m_idNSSimpl ]);
    m_lpbPF = static_cast<LinearProblemPerfectFluidRS*> ( m_linearProblem[ m_idPF ] );
    // setting a flag in NS linProb to true to switch several functionalities on
    m_lpbNS->coupled(true);
    
    // initialization of the boundary mappings
    m_lpbNS->initializeDofBoundaryAndBD2VolMaps();
    m_lpbPF->initializeDofBoundaryAndBD2VolMaps();
    // We build the std::unordered_map the store the correspondence labels of the two meshes
    // it needs to be called after the buildListOfBoundaryPetscDofs
    this->defineMapLabelInterface();
    
    // printing labels on the video to check everything is all right
    if ( FelisceParam::verbose() > 1 ) { 
      std::stringstream labelsIOP, labelsNS;
      labelsIOP<<"label IOP: ";
      labelsNS <<"label NS : ";
      for(auto it=m_labIOP2NS.begin(); it!=m_labIOP2NS.end(); ++it) {
        labelsIOP<<it->first <<'\t';
        labelsNS <<it->second<<'\t';
      }
      labelsIOP<<std::endl;
      labelsNS <<std::endl;
      PetscPrintf(MpiInfo::petscComm(), "%s",labelsNS.str().c_str());
      PetscPrintf(MpiInfo::petscComm(), "%s",labelsIOP.str().c_str());
    }
    
    m_scaleParam = FelisceParam::instance().porousParam;
    if ( m_scaleParam <= 0) {
      double fluidDensity = 1.;
      m_scaleParam = fluidDensity/FelisceParam::instance().timeStep;
    }
  }

  // Function to initialize the std::unordered_map m_labIOP2NS
  void 
  IOPcouplModel::defineMapLabelInterface() {
    //saving the std::unordered_map the associate the first point (in the lexicographical order)
    //to each label
    std::map<int, Point > mapIOP = m_lpbPF->dofBD(/*iBD*/0).getMapLab2FirstPoint();
    std::map<int, Point > mapNS  = m_lpbNS->dofBD(/*iBD*/0).getMapLab2FirstPoint();
    
    // Checking that the number of labels from the surfaces are the same
    std::stringstream errLine;
    errLine<<"different number of boundary labels describing the interface: "<<mapIOP.size()<< "!="<<mapNS.size()<<std::endl;
    FEL_CHECK(mapIOP.size()==mapNS.size(),errLine.str().c_str());
#ifndef NDEBUG
    typedef std::map<int, std::vector< Point > >  map_type;
    map_type allPointsPF = m_lpbPF->dofBD(/*iBD*/0).lab2AllPoints();
    map_type allPointsNS = m_lpbNS->dofBD(/*iBD*/0).lab2AllPoints();
    int cFailed(0);
    for ( auto itPF = allPointsPF.begin(),  itNS = allPointsNS.begin(); itPF != allPointsPF.end() &&  itNS != allPointsNS.end(); ++itPF, ++itNS ) {
      FEL_CHECK(itPF->second.size()==itNS->second.size(), "different number of points at the interface");
      for(auto itPPF = itPF->second.begin(), itPNS = itNS->second.begin(); itPPF != itPF->second.end() && itPNS != itNS->second.end(); ++itPPF, ++itPNS) {
        if ( *itPPF != *itPNS ) {
          //std::cout<<*itPPF<<'\t'<<*itPNS;
          //std::cout<<" fail "<<std::endl;
          ++cFailed;
        }
        // else {
        //   if ( FelisceParam::verbose() > 1 ) {
        //     std::cout<<*itPPF<<'\t'<<*itPNS;
        //     std::cout<<"\t ok "<<std::endl;
        //   }
        // }
      }
      if ( cFailed > 0 ) {
        std::stringstream aus;
        aus<<"checking the interface: "<<cFailed<<" points differ, out of <<"<<itNS->second.size()<<std::endl;
        FEL_ERROR(aus.str().c_str());
      } else {
        std::cout<<"All right in the mapping: there is a one to one correspondance between all the nodes at the interface"<<std::endl;
      }
    }
#endif

    //for each label of the IOP mesh interface
    for(auto itIOP = mapIOP.begin(); itIOP != mapIOP.end(); ++itIOP) {
      // we look for corresponding the label in the other mesh 
      bool Found=false;
      // for each label of the NS mesh 
      for(auto itNS = mapNS.begin(); itNS != mapNS.end() && !Found; ++itNS) {
        // if the first point is the same for this couple of labels
        if ( itIOP->second == itNS->second ) {
          // then we found the corresponding label
          Found=true;
          // we fill the std::unordered_map
          m_labIOP2NS [ itIOP->first ] = itNS->first; // <-- key line of the function
        }
      }
      // if we arrived here and we did not find a label it means that ...
      FEL_CHECK(Found," one label of the interface of the iop mesh is not present in the NS mesh");
    }
  }


  // Forward function:
  // it overrides NSSimplifiedFSI function, this time we really want to override
  // and we are not going to call the forward method of the mother class inside
  void
  IOPcouplModel::forward() {
    if ( m_fstransient->iteration == 0 ) {
      m_timeChecker = felisce::make_shared<ChronoInstance>();
      m_timeOnlyForIOP = felisce::make_shared<ChronoInstance>();
      m_timeOnlyForNS = felisce::make_shared<ChronoInstance>();

      m_lpbPF->initializePetscVectors();
    }
    m_timeChecker->start();

    // We use the prepare forward of the NSModel
    m_timeOnlyForNS->start();
    this->prepareForward();
    m_timeOnlyForNS->stop();

    if ( m_fstransient->iteration == 1 ) {
      //! In the first time & fixed point iteration the P1 normal-field is exported from NS and read into PF
      for ( felInt iComp(0); iComp < m_lpbNS->dimension(); ++iComp ) {
        std::stringstream namevector;
        namevector << "normalVector" << iComp;
        m_lpbPF->readerInterface(m_lpbNS->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(m_lpbNS->iUnknownVel(), iComp),
                                 m_lpbNS->get(LinearProblemNSSimplifiedFSI::sequential,"normalField"),
                                 m_labIOP2NS,
                                 namevector.str()
                                 ); 
      }
      m_lpbPF->exportNormalField();
    }
    // we initialize the flag to both matrix and rhs, however in navier-stokes I think only the advective part it is computed (if present)
    FlagMatrixRHS flag=FlagMatrixRHS::matrix_and_rhs;
    
    // initialize the test quantity to one.
    // and the corresponding tollerance from the data file
    double testQuantity(1.0), toll(FelisceParam::instance().domainDecompositionToll);

    // this two integers store the current number of sub-iterations 
    // and of gmres iterations for the current time step
    felInt cIteration(0), cGmresItNS(0), cGmresItPF(0);

    // subiteration of domain decomposition
    while ( testQuantity > toll && cIteration < FelisceParam::instance().domainDecompositionMaxIt) {
      // update the it counter and display a nice banner
      cIteration++;
      this->displayBannerIterationInit(cIteration);
      
      // solve the Navier Stokes problem without updating the structure
      // the structure it is not updated because in the NSSimplifiedFSI class
      // we refer, in the boudary condition, to the current displacement 
      // as the displacement available at that moment which is that one of the last time step.
      // We also store the number of gmres iteration needed for that
      m_timeOnlyForNS->start();
      this->solveNSSimplifiedFSI(flag);
      m_timeOnlyForNS->stop();
      cGmresItNS += m_lpbNS->kspInterface().getNumOfIteration();
      
      // we update the normal velocity to the perfect fluid problem
      // is going to be its neumann data      
      m_lpbNS->updateNormalVelocityAndCurrentDisplacement();

      //! Comunication phase: PF reads several data coming from NS.
      PetscVector rescaledNormalVelocity;
      rescaledNormalVelocity.duplicateFrom(m_lpbNS->get(LinearProblemNSSimplifiedFSI::sequential,"normalVelocity"));
      rescaledNormalVelocity.copyFrom(m_lpbNS->get(LinearProblemNSSimplifiedFSI::sequential,"normalVelocity"));
      rescaledNormalVelocity.scale(m_scaleParam);

      m_lpbPF->readerInterface( m_lpbNS->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(m_lpbNS->iUnknownPre(), /*icomp*/ 0 ),
                                rescaledNormalVelocity,
                                m_labIOP2NS,
                                "velocityInterface");

      // this could be avoided in the case of simple fixed point iterations,
      // but since we need the last available solution at interface for the relaxation
      // and since we have changed with the acceleration..
      m_lpbPF->readerInterface( m_lpbNS->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(m_lpbNS->iUnknownPre(), /*icomp*/ 0),
                                m_lpbNS->get(LinearProblemNSSimplifiedFSI::sequential,"currentIop"), 
                                m_labIOP2NS,
                                "currentIop"
                                );

      //! Solution phase
      m_timeOnlyForIOP->start();
      if ( FelisceParam::instance().computeSteklov  && m_fstransient->iteration == 1 && cIteration == 1 ) {
        if( FelisceParam::instance().useRoSteklov ) {
          int ivar  = m_lpbPF->listVariable().getVariableIdList(iop);
          int imesh = m_lpbPF->listVariable()[ivar].idMesh();
          m_lpbPF->assembleLowRankSteklov(imesh);
          m_lpbPF->exportAllEig(imesh);
        }
        else 
          m_lpbPF->assembleFullRankSteklov();
        // in the function above the matrix is computed
        flag = FlagMatrixRHS::only_rhs;
        // we clean the RHS: we are going to create a new one in solvePF
        m_lpbPF->clearMatrixRHS(flag);
      }
      //now that we have read the data we solve the perfect fluid problem
      // and we store the number of gmres iteration needed for that
      this->solvePF(flag,cIteration);
      m_timeOnlyForIOP->stop();
      if ( ! FelisceParam::instance().computeSteklov )
        cGmresItPF += m_lpbPF->kspInterface().getNumOfIteration();

      // now we had to read the data from PF
      // there is nothing to compute or to update like in NS
      // it is enough to gather the solution
      // because we simply read the iop at the interface
      m_lpbPF->gatherSolution();

      //! New Comunication phase
      m_lpbNS->accelerationPreStep(m_lpbNS->get(LinearProblem::sequential,"currentIop"));
      m_lpbNS->readIOPInterface(m_lpbPF->dofBD(/*iBD*/0).listOfBoundaryPetscDofs(m_lpbPF->idUnknownIop() , /*icomp*/ 0 ),
                                m_lpbPF->sequentialSolution(), 
                                m_labIOP2NS,
                                MpiInfo::rankProc());

      // we compute the test quantity which are the normalized norm of the increments
      // of two quantities at the interface:
      // - the normal velocity ( from NS to PF )
      // - the iop ( from PF to NS )
      //double testUN  = m_lpbNS->computeL2Difference("normalVelocity","normalVelocityOld") /  (m_lpbNS->computeL2Difference("normalVelocity") + 1e-12 );
      //double testIOP = m_lpbNS->computeL2Difference("currentIop","x1") /  (m_lpbNS->computeL2Difference("currentIop")    + 1e-12 );
      //testQuantity = testUN + testIOP;
      std::pair<double, double> testQuantities=m_lpbNS->computeTestQuantities( FelisceParam::instance().lumpedTest );
      double testUN  = testQuantities.first;
      double testIOP = testQuantities.second;
      testQuantity = testUN + testIOP;
      m_lpbNS->accelerationPostStep(m_lpbNS->get(LinearProblem::sequential,"currentIop"),cIteration);
      
      // now, for sure, we do not need to update the matrices for the other domain decomposition iterations
      flag=FlagMatrixRHS::only_rhs;
      
      // we display a nice banner
      this->displayBannerIterationEnd(cIteration,testQuantity,testUN,testIOP);

      // we clear only the RHS
      // it is important to clear the rhs here, because otherwise you will sum into the previous one
      // the former contribution
      // of course we do not clear the matrix since we want to re-use it
      this->clearMatrixRHSOfPbs(flag);
    }
    if ( cIteration == FelisceParam::instance().domainDecompositionMaxIt ) {
      std::stringstream war;
      war<<"*************Maximum number of iterations reached in "<<__FILE__<<":"<<__LINE__<<std::endl;
      FEL_WARNING(war.str().c_str());
    }
    //! we finalize the forward
    this->finalizeForward(cIteration,cGmresItNS,cGmresItPF);
    m_timeChecker->stop();
    m_cumulatedForwardTime.push_back(m_timeChecker->diff_cumul());
    m_cumulatedIOPTime.push_back(m_timeOnlyForIOP->diff_cumul());
    m_cumulatedNSTime.push_back(m_timeOnlyForNS->diff_cumul());
    writeTimeInfo();
  }
  void
  IOPcouplModel::writeTimeInfo() {
    
    // we convert the relaxation parameter into a std::string
    std::stringstream aus;
    aus<<MpiInfo::rankProc();

    // we open a file in the folder before the result dir
    // overwriting the previous content
    // the information are described in the header prined here
    std::ofstream iterationFile( std::string(FelisceParam::instance().resultDir + "timeInfo"+aus.str()+".dat").c_str() , std::ios::trunc );
    iterationFile <<"TimeStep"<<'\t'
                  <<"fixedPointIterations"<<'\t'
                  <<"totalGmresIterationsNS"<<'\t'
                  <<"totalGmresIterationsPF"<<'\t'
                  <<"gmresIterationsPerFixedPointIt"<<'\t'
                  <<"cumulatedTime"<<'\t'
                  <<"onlyForIOP"<<'\t'
                  <<"onlyForNS"
                  <<std::endl;
    for ( std::size_t ts(0); ts<m_numFixedPointItPerTimeStep.size(); ++ts) {
      iterationFile<<ts+1<<'\t'
                   <<m_numFixedPointItPerTimeStep[ts]<<'\t'
                   <<m_totNumOfGMRESItPerTimeStepNS[ts]<<'\t'
                   <<m_totNumOfGMRESItPerTimeStepPF[ts]<<'\t'
                   <<double(m_totNumOfGMRESItPerTimeStepNS[ts] + m_totNumOfGMRESItPerTimeStepPF[ts])/double(m_numFixedPointItPerTimeStep[ts])<<'\t'
                   <<m_cumulatedForwardTime[ts]<<'\t'
                   <<m_cumulatedIOPTime[ts]<<'\t'
                   <<m_cumulatedNSTime[ts]
                   <<std::endl;
    }
    //we close the file
    iterationFile.close();    
  }
  // ======================================================================
  //                   now the privates methods
  // ======================================================================

  // this function overrides 
  //     NSSimplifiedFSIModel::finalizeForward();
  void 
  IOPcouplModel::finalizeForward(int cIteration, int cGmresItNS, int cGmresItPF) {
    // we call the function of the mother class
    NSSimplifiedFSIModel::finalizeForward();
    
    // the interface quantities of the NS problem are reset to zero
    m_lpbNS->resetInterfaceQuantities();
    m_lpbPF->resetInterfaceQuantities();

    //first we store the new data
     m_numFixedPointItPerTimeStep.push_back(cIteration);
     m_totNumOfGMRESItPerTimeStepNS.push_back(cGmresItNS);
     m_totNumOfGMRESItPerTimeStepPF.push_back(cGmresItPF);

    // in this part we export a file 
    // with the information about the sub-iteration
    // and about the gmres iterations
    // in order to have it even if the program is killed we re-write it all at each time step
    if ( MpiInfo::rankProc() == 0 ) {
      // we convert the relaxation parameter into a std::string
      std::stringstream aus;
      aus<<FelisceParam::instance().relaxationParam;

      // we open a file in the folder before the result dir
      // overwriting the previous content
      // the information are described in the header prined here
      std::ofstream iterationFile( std::string(FelisceParam::instance().resultDir + "iterationsInfo"+aus.str()+".dat").c_str() , std::ios::trunc );
      iterationFile <<"TimeStep"<<'\t'
                    <<"fixedPointIterations"<<'\t'
                    <<"totalGmresIterationsNS"<<'\t'
                    <<"totalGmresIterationsPF"<<'\t'
                    <<"gmresIterationsPerFixedPointIt"
                    <<std::endl;
      for ( std::size_t ts(0); ts<m_numFixedPointItPerTimeStep.size(); ++ts) {
        iterationFile<<ts+1<<'\t'
                     <<m_numFixedPointItPerTimeStep[ts]<<'\t'
                     <<m_totNumOfGMRESItPerTimeStepNS[ts]<<'\t'
                     <<m_totNumOfGMRESItPerTimeStepPF[ts]<<'\t'
                     <<double(m_totNumOfGMRESItPerTimeStepNS[ts] + m_totNumOfGMRESItPerTimeStepPF[ts])/double(m_numFixedPointItPerTimeStep[ts])
                     <<std::endl;
      }
      //we close the file
      iterationFile.close();
    }
  }

  // we reset the interface quantities of both problem
  // typically we are going to use this function with
  //    flag = only-rhs
  // but there is no default
  void 
  IOPcouplModel::clearMatrixRHSOfPbs( FlagMatrixRHS flag) {
    m_linearProblem[m_idNSSimpl]->clearMatrixRHS(flag);
    m_lpbPF->clearMatrixRHS(flag);
  }

  // this function solve the PF problem
  // we do not have a similar function for NS since it is in the mother class
  void 
  IOPcouplModel::solvePF(FlagMatrixRHS flag, int nfp) {
    if ( FelisceParam::instance().computeSteklov ) {
      if ( FelisceParam::verbose() )
        PetscPrintf(MpiInfo::petscComm(),"Perfect Fluid solved using Steklov operator\n");
      m_lpbPF->solvePFUsingSteklov(m_fstransient, nfp);
    } else {
      if ( FelisceParam::verbose() )
        PetscPrintf(MpiInfo::petscComm(),"Perfect Fluid \n");
      m_lpbPF->assembleMatrixRHS(MpiInfo::rankProc(), flag);
      m_lpbPF->finalizeEssBCTransient();
      m_lpbPF->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), flag, flag);
      m_lpbPF->solve(MpiInfo::rankProc(), MpiInfo::numProc());
      m_lpbPF->exportOutputSteklov(m_fstransient,nfp);
      if ( FelisceParam::verbose() > 1) {
        std::vector<double> meanPressure,meanIOP;
        m_lpbNS->computeMeanQuantity(pressure, FelisceParam::instance().labelNSMesh, meanPressure);
        m_lpbPF->computeMeanQuantity(iop, m_interfaceLabels, meanIOP);
        std::stringstream msg;
        for ( std::size_t cLab(0); cLab< m_interfaceLabels.size(); ++cLab) {
          msg<<"meanIOP, label "<<m_interfaceLabels[cLab]    <<", : "<<meanIOP[cLab]<<" -- ";
          msg<<"meanPressure, label "<<FelisceParam::instance().labelNSMesh[cLab]<<", : "<<meanPressure[cLab]<<std::endl;
        }
        PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      }
    }
  }

  // two nice banners for the sub-iterations
  void 
  IOPcouplModel::displayBannerIterationInit( felInt cIt ) const {
    
    std::stringstream banner;
    for( felInt k(0); k<25; k++)
      banner<<"=";
    banner<<">  iteration    "<<cIt<<std::endl;
    PetscPrintf(MpiInfo::petscComm(), "%s",banner.str().c_str());
  }
  void 
  IOPcouplModel::displayBannerIterationEnd( felInt /*cIt*/, double testQuantity, double testUN, double testIOP ) const {
    
    std::stringstream banner;
    for( felInt k(0); k<25; k++)
      banner<<"=";
    banner<<">        Test Quantity :"<<testQuantity<<std::endl;
    for( felInt k(0); k<25; k++)
      banner<<" ";
    banner<<"       Normal Velocity :"<<testUN<<std::endl;
    for( felInt k(0); k<25; k++)
      banner<<" ";
    banner<<" Pressure at Interface :"<<testIOP<<std::endl;
    PetscPrintf(MpiInfo::petscComm(), "%s",banner.str().c_str());
  }
}
