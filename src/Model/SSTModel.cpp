//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/SSTModel.hpp"

namespace felisce {
  SSTModel::SSTModel():Model() {
    m_name = "SST";
  }

  SSTModel::~SSTModel() {
    m_solExtrapolate.destroy();
  }

  void SSTModel::initializeDerivedModel() {
    m_bdfNS.defineOrder(FelisceParam::instance().orderBdfNS);
    m_linearProblem[0]->initializeTimeScheme(&m_bdfNS);
  }

  void SSTModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    if(iProblem == 0) {
      m_U_0.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_U_0.set(0.0);

      m_solExtrapolate.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_solExtrapolate.set( 0.);

      const auto num_dofs = m_linearProblem[iProblem]->numDofPerUnknown(0);
      felInt idGlobalDof[num_dofs];
      double valueByDofU_0[num_dofs];

      for ( felInt i = 0; i < num_dofs; i++) {
        idGlobalDof[i] = i;
        valueByDofU_0[i] = 0.;
      }

      //Use global mapping for parallel computation (in sequential: (before mapping idGlobalDof) == (after mapping idGlobalDof)).
      AOApplicationToPetsc(m_linearProblem[iProblem]->ao(),num_dofs,idGlobalDof);

      // "add" values in Petsc vectors.
      m_U_0.setValues(num_dofs,idGlobalDof,valueByDofU_0,INSERT_VALUES);

      //assemble _U_0.
      m_U_0.assembly();

      //Initialize solution for solver.
      m_linearProblem[iProblem]->solution().copyFrom(m_U_0);

      //First assembly loop in iteration 0 to build static matrix.
      m_linearProblem[iProblem]->assembleMatrixRHS(MpiInfo::rankProc());
    }
  }

  void SSTModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    if(iProblem == 0) {
      // _Matrix = _A + _Matrix (add static matrix to the dynamic matrix to build
      // complete matrix of the system
      m_linearProblem[iProblem]->addMatrixRHS();
    }
  }



  void SSTModel::forward() {
    // Write solution for postprocessing (if required)
    writeSolution();

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();


    /*----------------------------
      Navier Stokes equation
      ----------------------------*/

    // update the bdf scheme
    if ( m_fstransient->iteration == 1) {
      // I use m_sol because from Model::setInitialCondition, I have a different _U_0
      m_bdfNS.initialize( m_linearProblem[0]->solution() );
      m_bdfNS.extrapolate(m_solExtrapolate);
      m_linearProblem[0]->initExtrapol(m_solExtrapolate);
    } else {
      m_bdfNS.update(m_linearProblem[0]->solution());
      m_bdfNS.extrapolate(m_solExtrapolate);
      m_linearProblem[0]->updateExtrapol(m_solExtrapolate);
    }

    m_bdfNS.computeRHSTime(m_fstransient->timeStep);
    m_linearProblem[0]->gatherVectorBeforeAssembleMatrixRHS();

    //Assembly loop on elements.
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc());

    //Specific operations before solve the system.
    postAssemblingMatrixRHS(0);

    //Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    //Solve linear system.
    PetscPrintf(PETSC_COMM_WORLD, "Solving the Navier-Stokes equation\n");
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    m_linearProblem[0]->gatherSolution();

    /*----------------------------
      First ARD equation (omega)
      ----------------------------*/
    //Assembly loop on elements.
    m_linearProblem[1]->assembleMatrixRHS(MpiInfo::rankProc());

    //Apply boundary conditions.
    m_linearProblem[1]->finalizeEssBCTransient();
    m_linearProblem[1]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    /*----------------------------
      Second ARD equation (k)
      ----------------------------*/
    //Assembly loop on elements.
    m_linearProblem[2]->assembleMatrixRHS(MpiInfo::rankProc());

    //Apply boundary conditions.
    m_linearProblem[2]->finalizeEssBCTransient();
    m_linearProblem[2]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    /*----------------------------
      Solve the two ARD equations
      ----------------------------*/
    PetscPrintf(PETSC_COMM_WORLD, "Solving the Omega equation\n");
    m_linearProblem[1]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    m_linearProblem[1]->gatherSolution();

    PetscPrintf(PETSC_COMM_WORLD, "Solving the K equation\n");
    m_linearProblem[2]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    m_linearProblem[2]->gatherSolution();

    // k should be positive, just to be sure.
    //VecAbs(m_linearProblem[2]->seqSolution());
    //VecAbs(m_linearProblem[2]->solution());
  }


  void SSTModel::setExternalVec() {
    // std::set external vectors for linear problems
    // (to be used in assembling routines)

    // Navier-Stokes
    // k
    m_linearProblem[0]->pushBackExternalVec(m_linearProblem[2]->sequentialSolution());
    m_linearProblem[0]->pushBackExternalAO(m_linearProblem[2]->ao());
    m_linearProblem[0]->pushBackExternalDof(m_linearProblem[2]->dof());

    // omega
    m_linearProblem[0]->pushBackExternalVec(m_linearProblem[1]->sequentialSolution());
    m_linearProblem[0]->pushBackExternalAO(m_linearProblem[1]->ao());
    m_linearProblem[0]->pushBackExternalDof(m_linearProblem[1]->dof());

    // y
    //m_linearProblem[0]->pushBackExternalVec(m_distance);
    //m_linearProblem[0]->pushBackExternalAO(m_linearProblem[1]->ao());
    //m_linearProblem[0]->pushBackExternalDof(m_linearProblem[1]->dof());


    // Advection Reaction Diffusion for omega
    // u
    m_linearProblem[1]->pushBackExternalVec(m_linearProblem[0]->sequentialSolution());
    m_linearProblem[1]->pushBackExternalAO(m_linearProblem[0]->ao());
    m_linearProblem[1]->pushBackExternalDof(m_linearProblem[0]->dof());

    // k
    m_linearProblem[1]->pushBackExternalVec(m_linearProblem[2]->sequentialSolution());
    m_linearProblem[1]->pushBackExternalAO(m_linearProblem[2]->ao());
    m_linearProblem[1]->pushBackExternalDof(m_linearProblem[2]->dof());

    // y
    //m_linearProblem[1]->pushBackExternalVec(m_distance);
    //m_linearProblem[1]->pushBackExternalAO(m_linearProblem[1]->ao());
    //m_linearProblem[1]->pushBackExternalDof(m_linearProblem[1]->dof());


    // Advection Reaction Diffusion for k
    // u
    m_linearProblem[2]->pushBackExternalVec(m_linearProblem[0]->sequentialSolution());
    m_linearProblem[2]->pushBackExternalAO(m_linearProblem[0]->ao());
    m_linearProblem[2]->pushBackExternalDof(m_linearProblem[0]->dof());

    // omega
    m_linearProblem[2]->pushBackExternalVec(m_linearProblem[1]->sequentialSolution());
    m_linearProblem[2]->pushBackExternalAO(m_linearProblem[1]->ao());
    m_linearProblem[2]->pushBackExternalDof(m_linearProblem[1]->dof());

    // y
    //m_linearProblem[2]->pushBackExternalVec(m_distance);
    //m_linearProblem[2]->pushBackExternalAO(m_linearProblem[1]->ao());
    //m_linearProblem[2]->pushBackExternalDof(m_linearProblem[1]->dof());
  }


  int SSTModel::getNstate() const {
    return m_linearProblem[0]->numDof();
  }

  void SSTModel::getState(double* & state) {
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }

  void SSTModel::getState_swig(double* data, felInt numDof) {
    double * state;
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
    for (felInt i=0 ;  i<numDof ; i++) {
      data[i] = state[i];
    }
  }

  void SSTModel::setState(double* & state) {
    m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
}
