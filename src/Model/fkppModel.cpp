//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/fkppModel.hpp"

namespace felisce {
  FkppModel::FkppModel():Model() {
    m_name = "FKPP";
  }

  FkppModel::~FkppModel()
  = default;

  void FkppModel::forward() {
    // Write solution for postprocessing (if required)
    writeSolution();
    
    // Advance time step.
    updateTime(FlagMatrixRHS::only_rhs);
    
    // Print time information
    printNewTimeIterationBanner();
    
    //Assembly loop on elements.
    if (m_fstransient->iteration == 1) {
      m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
    }
    else {
      m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);      
    }
    //Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();
    if (m_fstransient->iteration == 1) {
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs);
    } else {
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs);
    }

    //Solve linear system.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
  }

}
