//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone D.Lombardi
//

// System includes

// External includes

// Project includes
#include "Model/PODModel.hpp"

namespace felisce {
  PODModel::PODModel():
    Model() {
    for(std::size_t i=0; i<m_eigenProblem.size(); i++) {
      m_eigenProblem[i] = nullptr;
    }
    m_name = "POD";
  }

  PODModel::~PODModel() {
    for(std::size_t i=0; i<m_eigenProblem.size(); i++) {
      if (m_eigenProblem[i])
        delete m_eigenProblem[i];
    }
  }

  void PODModel::initializeEigenProblem(std::vector<EigenProblemPOD*> eigenPb) {
    std::unordered_map<std::string, int> mapOfType;
    mapOfType["EXPLICIT_EULER"] = 0;
    mapOfType["IMPLICIT_EULER"] = 1;
    mapOfType["SEMI_IMPLICIT_EULER"] = 2;

    m_method = mapOfType[FelisceParam::instance().integrationTimeMethod];

    for (std::size_t i=0; i<eigenPb.size(); i++) {
      m_eigenProblem.push_back(eigenPb[i]);
    }

    for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
      //Define linear problem
      //=======================
      m_eigenProblem[ipb]->initialize(mesh(), m_fstransient, MpiInfo::petscComm());
      if (FelisceParam::instance().solver.size() < eigenPb.size()) {
        m_eigenProblem[ipb]->fixIdOfTheProblemSolver(0);
      } else
        m_eigenProblem[ipb]->fixIdOfTheProblemSolver(ipb);

      //Compute Degrees of freedom (DOF) the problem
      //=========================================
      m_eigenProblem[ipb]->computeDof(MpiInfo::numProc(), MpiInfo::rankProc());
    }
    // Specific initalization of the user model
    initializeDerivedModel();

    //Define initial conditions
    //=========================================
    if (FelisceParam::instance().hasInitialCondition) {
      for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
        for (std::size_t ii = 0; ii < FelisceParam::instance().valueInitCond.size(); ii++) {
          m_initialCondition.addVariable(*m_eigenProblem[ipb]->listVariable().getVariable(FelisceParam::instance().nameVariableInitCond[ii]));
        }
      }
      m_initialCondition.print(FelisceParam::verbose(),std::cout);
    }

    for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
      //Degrees of freedom partitionning with ParMetis
      //===============================================
      m_eigenProblem[ipb]->cutMesh();
      //Allocate memory for the matrix _Matrix and std::vector _RHS in the linearProblem class
      //============================================================

      m_eigenProblem[ipb]->allocateMatrix();

    }
    setExternalVec(); // make the connection between the different linear pb (to be defined in the derived class)
    std::cout << std::endl;
    for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
      //Apply specific operations before assembling
      //===========================================
      preAssemblingMatrixRHS(ipb);

      // Gather to write initial solution.
      m_eigenProblem[ipb]->gatherSolution();

    }
    for(std::size_t iio=0; iio<m_ios.size(); ++iio)
      m_ios[iio]->initializeOutput();
    for(std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      m_eigenProblem[ipb]->clearMatrix();
    }
    setInitialCondition();

    m_fstransient->iteration=0;

    m_eigenProblemIsInitialized = true;
  }


  void PODModel::preAssembleMatrix(const int iProblem) {
    if (FelisceParam::instance().hasInfarct) {
      HeteroSparFHN heterof0;
      std::vector<double> valuef0;
      heterof0.initialize(m_fstransient);
      m_eigenProblem[iProblem]->evalFunctionOnDof(heterof0, valuef0);
      m_eigenProblem[iProblem]->setFhNf0(valuef0);
    }
  }

  // Re-define
  void PODModel::updateTime(const FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_fstransient->iteration++;
    m_fstransient->time +=m_fstransient->timeStep;
  }

  // Pay attention on the call of this :
  // call PODModel::writeSolution() function instead of (non-virtual) Model::writeSolution() function
  void PODModel::writeSolution() {
    if (MpiInfo::rankProc() == 0) {
      if (m_meshIsWritten == false) writeMesh();
    }

    if( (m_fstransient->iteration % FelisceParam::instance().frequencyWriteSolution == 0)
        or m_hasFinished) {
      if(FelisceParam::verbose() > 1) PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");
      for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
        // Calculate FE solution (projection of alpha coeff)
        m_eigenProblem[ipb]->writeEnsightSolution(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution));
      }
    }
  }

  void PODModel::forward() {
    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      if ( m_fstransient->iteration == 0 ) {
        // Initialize Rom object and calculate reduced basis

        // Read initial data
        m_eigenProblem[ipb]->readData(*io());
        preAssembleMatrix(ipb);
        // Create Matrix
        m_eigenProblem[ipb]->assembleMatrix();

        // Read basis from ensight files
        if (FelisceParam::instance().readBasisFromFile) {
          // Build basis reading vectors from ensight files
          m_eigenProblem[ipb]->initializeROM();
        }

        else {
          if (MpiInfo::rankProc() == 0) {
            if (m_meshIsWritten == false) writeMesh();
          }
          // Initialize Slepc solver
          m_eigenProblem[ipb]->buildSolver();
          // Solve with slepc the generilized eigen problem: _Matrix[0] v = m_matrix[1] lambda v
          m_eigenProblem[ipb]->solve();
          // Writes modes (eigenvectors) in ensight format
          m_eigenProblem[ipb]->writeMode();
          m_eigenProblem[ipb]->initializeSolution();
        }

        if (FelisceParam::instance().hasSource)
          postAssembleMatrix(ipb);

        m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->computeTensor();

        m_eigenProblem[ipb]->computeGamma();


        if (FelisceParam::instance().writeECG) {
          m_eigenProblem[ipb]->initializeECG();
          m_eigenProblem[ipb]->writeECG(m_fstransient->iteration);
        }

      }
    }

    //Write solution with ensight.
    PODModel::writeSolution();
    //Advance time step.
    updateTime();
    printNewTimeIterationBanner();



    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      switch (m_method) {
      case 0: // EXPLICIT_EULER

        m_eigenProblem[ipb]->updateBeta();
        m_eigenProblem[ipb]->computeGamma();


        break;
      case 1: // SEMI_IMPLICIT_EULER using fix point method
        //            updateTensor();
        //            semiImplicitFixPt();
        break;
      case 2: // IMPLICIT_EULER
        //            implicitFixPt();
        break;
      default:
        FEL_ERROR("This integration method is not implemented.");
        break;
      }

      if (FelisceParam::instance().writeECG) {
        m_eigenProblem[ipb]->updateEcgOperator();
        m_eigenProblem[ipb]->writeECG(m_fstransient->iteration);
      }

    }

  }

  void PODModel::solveEigenProblem() {
    //Write solution with ensight.
    if (MpiInfo::rankProc() == 0) {
      if (m_meshIsWritten == false) writeMesh();
    }
    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      // Read initial data
      m_eigenProblem[ipb]->assembleMatrix();
      // Read snapshots and compute autocorrelation:
      m_eigenProblem[ipb]->readSnapshot(*io());
      m_eigenProblem[ipb]->computeAutocorr();

      // Initialize Slepc solver
      m_eigenProblem[ipb]->buildSolver();


      // Solve with slepc the generilized eigen problem: _Matrix[0] v = m_matrix[1] lambda v
      m_eigenProblem[ipb]->solve();

      // Writes modes (eigenvectors) in ensight format
      m_eigenProblem[ipb]->writeMode();
    }

    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++)
      m_eigenProblem[ipb]->checkOrthonormality();
    // m_eigenProblem[ipb]->checkBasis();

  }


}
