//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/linearElasticityModel.hpp"
#include "Solver/linearProblemLinearElasticity.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce {
  // Called in Model::initializeLinearProblem, just after the mesh partitioning.
  void LinearElasticityModel::initializeDerivedModel() {
    // Saving the lpb pointer after the static cast
    m_lpbLE = static_cast<LinearProblemLinearElasticity*>(m_linearProblem[0]);
    // Initializing the PetscVectors
    m_lpbLE->initPetscVectors();
  }
  // Pre-assemble, called during initializeLinearProblem
  void LinearElasticityModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    if ( iProblem == (std::size_t) m_idLE) {
      // Zero-ing the solution
      m_lpbLE->solution().zeroEntries();
      // Assembling the static part of the matrix.
      m_lpbLE->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
    }
  }

  // Forward
  void LinearElasticityModel::forward() {
    // Preparation: write solution, update time, print banner..
    prepareForward();
    // Assemble volume stuff
    m_lpbLE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
    // Sum static matrix into matrix(0)
    m_lpbLE->addMatrixRHS();    
    // apply BC and solve
    solveLinearElasticity();
    // update of the velocity from the displacement
    m_lpbLE->updateCurrentVelocity();
    // update old displacement 
    finalizeForward();
  }

  // Write solution, advance in time
  void LinearElasticityModel::prepareForward( FlagMatrixRHS flag ) {
    writeNormal();
    writeSolution();
    updateTime(flag);
    printNewTimeIterationBanner();
  }

  // Boundary conditions and solve
  void LinearElasticityModel::solveLinearElasticity(FlagMatrixRHS flag) {
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");
    
    // finalize essential BC transient
    m_lpbLE->finalizeEssBCTransient();
    // Apply BC
#ifdef FELISCE_WITH_CVGRAPH
    if ( !FelisceParam::instance().withCVG || ! m_linearProblem[0]->slave()->redoTimeStepOnlyCVGData() ) {
      m_lpbLE->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),flag,flag);
    } else {
      m_linearProblem[0]->applyOnlyCVGBoundaryCondition();
    }
#else
    m_lpbLE->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),flag,flag);
#endif

    // For the elasticity and stokes coupled application
    if( m_lpbLE->coupled() && !FelisceParam::instance().withCVG ) {
      m_lpbLE->sumOnBoundary(m_lpbLE->vector(), m_lpbLE->get(LinearProblem::sequential,"externalStress"), m_lpbLE->dofBD(/*iBD*/0));
    }
    
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Solving linear elasticity system \n");

    m_lpbLE->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    if (FelisceParam::instance().withCVG ) {
      m_linearProblem[0]->computeResidual();
    }
  }

  // Update old variables + post-processing
  void LinearElasticityModel::finalizeForward() {
    m_lpbLE->updateOldDisplacement();
    writeNormal();
  }
  
  void LinearElasticityModel::writeNormal() {
    if (FelisceParam::instance().exportP1Normal ) {
      PetscVector vec;
      if( m_fstransient->iteration == 0 ) {
        vec.duplicateFrom(m_lpbLE->sequentialSolution());
        vec.zeroEntries();
      } else {
        vec=m_lpbLE->get(LinearProblem::sequential,"normalField");
      }
      m_lpbLE->writeSolutionFromVec(vec, MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("normal"));
    }
  }
#ifdef FELISCE_WITH_CVGRAPH
  void
  LinearElasticityModel::cvgraphNewTimeStep() {
    m_lpbLE->updateCurrentVelocity();
    this->finalizeForward(); //advance in time
    this->prepareForward();
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
    m_linearProblem[0]->addMatrixRHS();
    m_linearProblem[0]->createAndCopyMatrixRHSWithoutBC();
  }
#endif
}
