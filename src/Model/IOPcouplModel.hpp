//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __IOPCOUPLMODEL_HPP__
#define __IOPCOUPLMODEL_HPP__

// System includes

// External includes

// Project includes
#include "Model/NSSimplifiedFSIModel.hpp"
#include "Solver/linearProblemPerfectFluidRS.hpp"

namespace felisce {
  // ====================================================================
  // IOPcouplModel inherits from NSSimplifiedFSIModel class             =
  // and it deals with the handling of domain decomposition scheme      =
  // used to couple NS and IOP model                                    =
  // ====================================================================
  class IOPcouplModel :
    public NSSimplifiedFSIModel {
  public:
    // ==================================================================
    // methods to initialize the all coupled model                      =
    // - constructor                                                    =
    // - redefinition of the virtual initializeDerivedModel             =
    // - function that fills m_labelIOP2NS std::unordered_map                          =
    // ==================================================================
    IOPcouplModel();
    void initializeDerivedModel() override;
    void defineMapLabelInterface();
    // ==================================================================
    // functions to correctly manage the subiterations                  = 
    // ==================================================================
    void forward() override;
  private:
    ChronoInstance::Pointer m_timeChecker;
    ChronoInstance::Pointer m_timeOnlyForIOP;
    ChronoInstance::Pointer m_timeOnlyForNS;
    std::vector<double> m_cumulatedForwardTime;
    std::vector<double> m_cumulatedIOPTime;
    std::vector<double> m_cumulatedNSTime;
    
    void writeTimeInfo();
    // ==================================================================
    // finalize is called into the forward                              =
    // ==================================================================
    void finalizeForward(int cIteration, int cGmresItNS, int cGmresItPF);
    // ==================================================================
    // Method to solve one sub-iteration of the perfect fluid           =
    // ==================================================================
    void solvePF(FlagMatrixRHS flag,int nfp);
    // ==================================================================
    //  Function displaying a banner for each sub-iteration             =
    // ==================================================================
    void displayBannerIterationInit(felInt cIt) const ;
    void displayBannerIterationEnd(felInt cIt,
                                   double testQuantity, 
                                   double testUN, 
                                   double testIOP) const;
    // ==================================================================
    //  In this function the matrix and/or the rhs are cleared, used to =
    //  correctly manage re-using of matrices and vectors in the        =
    //  sub-iterations                                                  =
    // ==================================================================
    void clearMatrixRHSOfPbs( FlagMatrixRHS flag);

    // ==================================================================
    // id of the perfect fluid linear problem into the std::vector of        =
    // linear problems, the corresponding variable m_idNSSimplFSI       =
    // is already defined into NSSimplifiedFSIModel class               =
    // ==================================================================
    felInt m_idPF;
    // ==================================================================
    // Pointers to the two linear problems after static_cast.           =
    // Usefull when calling functions not present in linearProblem.hpp  =
    // ==================================================================
    LinearProblemNSSimplifiedFSI *m_lpbNS;
    LinearProblemPerfectFluidRS *m_lpbPF;
    // ==================================================================
    // std::unordered_map storing the mapping between labels of iop mesh and ns mesh   =
    // example:                                                         = 
    //   correspondingLabelFromNSMesh = m_labIOP2NS[labelFromIOPMesh];  =
    // ==================================================================
    std::map<int,int> m_labIOP2NS;
    // ==================================================================
    // these vectors are here to observe the number of subiterations    =
    // for each time step                                               =
    // ==================================================================
    std::vector<int> m_numFixedPointItPerTimeStep;
    std::vector<int> m_totNumOfGMRESItPerTimeStepNS;
    std::vector<int> m_totNumOfGMRESItPerTimeStepPF;

    double m_scaleParam;
    
    std::vector<int> m_interfaceLabels;
  };
}
#endif
