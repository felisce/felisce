//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone C. Corrado
//

// System includes

// External includes

// Project includes
#include "Model/bidomainDecoupledModel.hpp"

namespace felisce {
  BidomainDecoupledModel::BidomainDecoupledModel():
    BidomainModel(),
    m_linearProblemBidomainTransMemb(nullptr),
    m_linearProblemBidomainExtraCell(nullptr) {
    m_name = "BidomainDecoupled";
  }

  BidomainDecoupledModel::~BidomainDecoupledModel() {
    m_linearProblemBidomainTransMemb = nullptr;
    m_linearProblemBidomainExtraCell = nullptr;
    m_Ue_0.destroy();
    m_extrapolatePotTransMemb.destroy();
    if(!FelisceParam::instance().monodomain)
      m_extrapolatePotExtraCell.destroy();
  }

  //! Define m_ionic and m_bdfEdp (used in linearProblem).
  void BidomainDecoupledModel::initializeDerivedModel() {
    BidomainModel::initializeDerivedModel();
    //Define order for bdf used to calculate m_extrapolateExtraCell.
    if(!FelisceParam::instance().monodomain)
      m_bdfExtraCell.defineOrder(FelisceParam::instance().orderBdfEdp);
  }

  void BidomainDecoupledModel::initializeDerivedLinearProblem() {
    m_linearProblemBidomainTransMemb = dynamic_cast<LinearProblemBidomainTransMemb*>(m_linearProblem[0]);
    m_linearProblemBidomainExtraCell = dynamic_cast<LinearProblemBidomainExtraCell*>(m_linearProblem[1]);

    initializeAppCurrent();
  }

  void BidomainDecoupledModel::initializeAppCurrent() {
    m_iApp = new AppCurrent();
    m_iApp->initialize(m_fstransient);
  }


  void BidomainDecoupledModel::evalIapp() {
    m_iAppValue.clear();
    if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
      m_linearProblemBidomainTransMemb->evalFunctionOnDof(*m_iApp,m_fstransient->time,m_iAppValue,m_linearProblemBidomainTransMemb->EndocardiumDistance());
    } else {
      m_linearProblemBidomainTransMemb->evalFunctionOnDof(*m_iApp,m_fstransient->time,m_iAppValue);
    }
  }

  void BidomainDecoupledModel::finalizeRHSDP(int iProblem) {
    if (iProblem == 0) {

      int size = m_linearProblem[iProblem]->numDofLocalPerUnknown(potTransMemb);
      double valueForPetsc[size];
      felInt idLocalValue[size];
      felInt idGlobalValue[size];

      double coefAm = FelisceParam::instance().Am;
      double coefCm = FelisceParam::instance().Cm;

      // m_massRHS = Am * Cm * m_bdfEdp.RHS
      m_linearProblemBidomainTransMemb->massRHS().axpy(coefAm*coefCm, m_bdfEdp.vector());

      for (felInt i = 0; i < size; i++) {
        idLocalValue[i] = i;
      }
      ISLocalToGlobalMappingApply(m_linearProblem[iProblem]->mappingDofLocalToDofGlobal(potTransMemb),size,&idLocalValue[0],&idGlobalValue[0]);

      for (felInt i = 0; i < size; i++) {
        valueForPetsc[i] = coefAm*m_iAppValue[idGlobalValue[i]];
      }
      // m_massRHS = m_massRHS + Am * Iapp
      m_linearProblemBidomainTransMemb->massRHS().setValues(size,&idGlobalValue[0],&valueForPetsc[0],ADD_VALUES);
      m_linearProblemBidomainTransMemb->massRHS().assembly();

      // m_massRHS = m_massRHS + Am * Iion
      m_linearProblemBidomainTransMemb->massRHS().axpy(coefAm,m_ionic->ion());

      if(!FelisceParam::instance().monodomain) {
        // _KsigmaiRHS = m_extrapolatePotExtraCell
        m_linearProblemBidomainTransMemb->ksigmaiRHS().axpy(1.,m_extrapolatePotExtraCell);
      }

    } else if (iProblem == 1) {
      m_linearProblemBidomainExtraCell->vector().axpy(1.0,m_linearProblem[0]->solution());
    }

  }

  void BidomainDecoupledModel::preAssemblingMatrixRHS(std::size_t iProblem) {

    // Initialize useful vectors of initial solution and initial extrapolate with a std::vector with same structure of _RHS for each linearProblem.

    if (iProblem == 0) { // First problem solves first equation on potTransMemb
      // Give _U_0 and m_W_0 values:
      // initialize PotTransMemb with value Vmin
      // and W_0 with 1/(vMax-vMin)^2.

      // 1) copy structure in _U_0 and in m_extrapolatePotTransMemb.
      m_U_0.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_U_0.set(FelisceParam::instance().vMin);

      // 2) copy structure in m_W_0 (initialize solution at time 0 of EDO in schaf solver) - only for first equation.
      m_W_0.resize(1);
      m_W_0[0].duplicateFrom(m_linearProblem[iProblem]->vector());
      double valueByDofW_0 = 1./((FelisceParam::instance().vMax - FelisceParam::instance().vMin)*(FelisceParam::instance().vMax - FelisceParam::instance().vMin));
      m_W_0[0].set(valueByDofW_0);

      // 3) copy structure in m_extrapolatePotTransMemb (contains extrapolate values of linearProblem[0] solution).
      m_extrapolatePotTransMemb.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_extrapolatePotTransMemb.zeroEntries();

      // 3) copy structure in m_massRHS and _KsigmaiRHS.
      m_linearProblemBidomainTransMemb->massRHS().duplicateFrom(m_linearProblem[iProblem]->vector());
      m_linearProblemBidomainTransMemb->massRHS().set(0.);
      m_linearProblemBidomainTransMemb->ksigmaiRHS().duplicateFrom(m_linearProblem[iProblem]->vector());
      m_linearProblemBidomainTransMemb->ksigmaiRHS().set(0.);

      //Initialize solution for solver.
      m_linearProblem[iProblem]->solution().copyFrom(m_U_0);

      // Read fibers and distance mapp (for Zygote geometry).
      m_linearProblem[iProblem]->readData(*io());

      //Initialize heterogeneous parameters for schaf solver (useful only for first problem).
      if (FelisceParam::instance().typeOfIonicModel == "schaf") {
        initializeSchafParameter();
      }

    } else if (iProblem == 1) {
      // 1) copy structure in _Ue_0.
      m_Ue_0.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_Ue_0.set(0.0);

      // 2) copy structure in m_extrapolatePotExtraCell (contains extrapolate values of linearProblem[1] solution).
      if(!FelisceParam::instance().monodomain) {
        m_extrapolatePotExtraCell.duplicateFrom(m_linearProblem[iProblem]->vector());
        m_extrapolatePotExtraCell.set(0.);
      }

      //Initialize solution for solver.
      m_linearProblem[iProblem]->solution().copyFrom(m_Ue_0);

      // Read fibers and distance mapp (for Zygote geometry).
      m_linearProblem[iProblem]->readData(*io());
    }

    //Debug print.
    // /todo fix value of verbose
    if (m_verbose > 2 && iProblem == 0) {
      PetscPrintf(MpiInfo::petscComm(),"\n\t\t  _U_0 (==m_sol (linearProblem[0]) at time == 0):\n");
      m_U_0.view();
      PetscPrintf(MpiInfo::petscComm(),"\n\t\t  m_W_0 (==m_solEDO (ionicSolver) at time == 0):\n");
      m_W_0[0].view();
    } else if (m_verbose > 2 && iProblem == 1) {
      PetscPrintf(MpiInfo::petscComm(),"\n\t\t  m_Ue_0 (==m_sol (linearProblem[1]) at time == 0):\n");
      m_Ue_0.view();
    }
  }

  void BidomainDecoupledModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    // Evaluate applied current std::vector.
    if (iProblem == 0) {
      evalIapp();
    }

    // Calculate RHS std::vector (to be multiplied by mass matrix).
    finalizeRHSDP(iProblem);
    // Calculate _RHS :
    // if model == 'Bidomain' => _RHS = mass * RHS.
    // if model == 'BidomainDecoupled' :
    //   iProblem==0 => _RHS=mass*RHS+Ksigmai*u_e
    //   iProblem==1 => _RHS=Ksigmai*V_m
    m_linearProblem[iProblem]->addMatrixRHS();

    //RHS of Luenb. filter
    if (FelisceParam::instance().stateFilter) {
      if (iProblem == 0) {
        double coefAm = FelisceParam::instance().Am;
        m_luenbFlter.zeroEntries();
        m_luenbFlter.copyFrom(m_linearProblem[iProblem]->matrix(1),SAME_NONZERO_PATTERN);
        m_luenbFlter.assembly(MAT_FINAL_ASSEMBLY);
        // copy diagonal stabiliz. matrix in luenberger matrix
        m_luenbFlter.diagonalScale(PetscVector::null(),m_ionic->stabTerm());
        m_luenbFlter.scale(coefAm);
        m_linearProblem[iProblem]->matrix(0).axpy(1,m_luenbFlter,SAME_NONZERO_PATTERN);
      }
    }
  }

  void BidomainDecoupledModel::initializeSchafParameter() {
    // Initialize TauClose.
    if (FelisceParam::instance().hasHeteroTauClose) {
      m_schaf->fctTauClose().initialize(m_fstransient);
      if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
        m_linearProblemBidomainTransMemb->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose(),m_linearProblemBidomainTransMemb->EndocardiumDistance());
      }
      else {
        m_linearProblemBidomainTransMemb->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose());
      }
    }
    // Initialize TauOut : heterogeneous in case of infarct.
    if (FelisceParam::instance().hasInfarct) {
      m_schaf->fctTauOut().initialize(m_fstransient);
      m_linearProblemBidomainTransMemb->evalFunctionOnDof(m_schaf->fctTauOut(), m_schaf->tauOut());
    }
  }

  // Pay attention on the call of this :
  // call BidomainModel::writeSolution() function instead of (non-virtual) Model::writeSolution() function
  void BidomainDecoupledModel::writeSolution() 
  {
    Model::writeSolution();
  }

  void BidomainDecoupledModel::forward() {
    //Write solution with ensight.
    BidomainDecoupledModel::writeSolution();
    //Advance time step.
    updateTime(FlagMatrixRHS::only_rhs);
    printNewTimeIterationBanner();

    if ( m_fstransient->iteration == 1) {
      //Initialization of bdf solvers.
      m_bdfEdp.initialize(m_linearProblem[0]->solution());
      if(!FelisceParam::instance().monodomain)
        m_bdfExtraCell.initialize(m_linearProblem[1]->solution());
      m_linearProblemBidomainTransMemb->initializeTimeScheme(&m_bdfEdp);
      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolatePotTransMemb);
      if(!FelisceParam::instance().monodomain) {
        m_bdfExtraCell.extrapolate(m_extrapolatePotExtraCell);
      }
      //Initialization of ionic solver.
      m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblem[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblem[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());
      m_ionic->initializeExtrap(m_extrapolatePotTransMemb);
      m_ionic->initialize(m_W_0[0]);
      m_buildIonic = true;
    } else {
      m_bdfEdp.update(m_linearProblem[0]->solution());
      if(!FelisceParam::instance().monodomain)
        m_bdfExtraCell.update(m_linearProblem[1]->solution());
      m_bdfEdp.extrapolate(m_extrapolatePotTransMemb);
      if(!FelisceParam::instance().monodomain) {
        m_bdfExtraCell.extrapolate(m_extrapolatePotExtraCell);
      }
      m_ionic->update(m_extrapolatePotTransMemb);
    }

    //Solve Mitchell and Schaeffer model and calculate Iion.
    m_ionic->computeRHS();
    m_ionic->solveEDO();
    m_ionic->computeIon();
    m_ionic->updateBdf();

    m_bdfEdp.computeRHSTime(m_fstransient->timeStep);

    // Assembling of matrices at first iteration (because of needs some coefficients not defined at iteration 0).
    if ( m_fstransient->iteration == 1 ) {
      if ( FelisceParam::instance().hasCoupledAtriaVent )
        m_linearProblemBidomainTransMemb->assembleMatrixRHSCurrentAndCurvilinearElement(MpiInfo::rankProc());
      else
        m_linearProblemBidomainTransMemb->assembleMatrixRHS(MpiInfo::rankProc());
    }

    //Specific operations before solve the system : calculate _RHS std::vector used in solve().
    postAssemblingMatrixRHS(0);

    //Solve the system _Matrix[0]*_V_m=_RHS.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    // Assemble Matrix of linearProblem.
    if ( m_fstransient->iteration == 1 ) {
      if ( FelisceParam::instance().hasCoupledAtriaVent )
        m_linearProblemBidomainExtraCell->assembleMatrixRHSCurrentAndCurvilinearElement(MpiInfo::rankProc());
      else
        m_linearProblemBidomainExtraCell->assembleMatrixRHS(MpiInfo::rankProc());
    }

    //Specific operations before solve the system : calculate _RHS std::vector used in solve().
    postAssemblingMatrixRHS(1);

    //Solve the system _Matrix[0]*m_u_e=_RHS.
    m_linearProblem[1]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    if ( FelisceParam::instance().hasCoupledAtriaVent == false )
      m_linearProblem[1]->removeAverageFromSolution(potExtraCell, MpiInfo::rankProc());

  }

}
