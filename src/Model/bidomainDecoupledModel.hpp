//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone C. Corrado
//

#ifndef _BIDOMAINDECOUPLEDMODEL_HPP
#define _BIDOMAINDECOUPLEDMODEL_HPP

// System includes
#include <vector>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/bidomainModel.hpp"
#include "Solver/linearProblemBidomainTransMemb.hpp"
#include "Solver/linearProblemBidomainExtraCell.hpp"
#include "Solver/schafSolver.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  class BidomainDecoupledModel:
    public BidomainModel {
  public:
    ///Construtor.
    BidomainDecoupledModel();
    ///Destructor.
    ~BidomainDecoupledModel() override;
    void initializeDerivedModel() override;
    void initializeDerivedLinearProblem() override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    /// Manage time iteration.
    void forward() override;
    /// Initialize the SchafSolver and evaluate heterogeneous parameters of SchafSolver.
    void finalizeRHSDP(int iProblem=0) override;
    /// Initialize the SchafSolver and evaluate heterogeneous parameters of SchafSolver.
    void initializeSchafParameter();
    void initializeAppCurrent() override;
    void evalIapp();
    void writeSolution();

  protected:
    LinearProblemBidomainTransMemb* m_linearProblemBidomainTransMemb;
    LinearProblemBidomainExtraCell* m_linearProblemBidomainExtraCell;
  private:
    ///Initialization vectors.
    PetscVector m_Ue_0;
    ///Data for schaf solver from EDP solution.
    Bdf m_bdfExtraCell;
    PetscVector m_extrapolatePotTransMemb;
    PetscVector m_extrapolatePotExtraCell;
  };
}


#endif
