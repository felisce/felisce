//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes

// External includes
#include "petscmat.h"
#include "petscsnes.h"

// Project includes
#include "Core/felisceTools.hpp"
#include "Model/hyperElasticityModel.hpp"
#include "Solver/linearProblemHyperElasticity.hpp"
#include "Core/felisce_error.hpp"

namespace felisce {

HyperElasticityModel::HyperElasticityModel()
    : Model()
{
  m_name = "HyperElasticity";

  FEL_ASSERT(!(FelisceParam::instance().hyperElasticLaw.timeSchemeHyperelasticity != HyperelasticityNS::none && FelisceParam::instance().hyperElasticLaw.pressureData->IsCompressible()));
}

/***********************************************************************************/
/***********************************************************************************/

HyperElasticityModel::~HyperElasticityModel()
= default;

/***********************************************************************************/
/***********************************************************************************/

void HyperElasticityModel::forward()
{
  // Write solution for postprocessing (if required)
  writeSolution();

  // Advance time step.
  updateTime();

  // Print time information
  printNewTimeIterationBanner();

  // Assemble the new stiffness matrix.
  HyperelasticLinearProblem* linear_problem_ptr = dynamic_cast<HyperelasticLinearProblem*>(m_linearProblem[0]);

  FEL_ASSERT(linear_problem_ptr);
  HyperelasticLinearProblem& linear_problem = *linear_problem_ptr;

  // Solve linear system.
  linear_problem.iterativeSolve(MpiInfo::rankProc(), MpiInfo::numProc(), ApplyNaturalBoundaryConditions::no, FlagMatrixRHS::matrix_and_rhs);

  // Update
  linear_problem.endIteration();
}

/***********************************************************************************/
/***********************************************************************************/

void HyperElasticityModel::SolveDynamicProblem()
{

  FEL_ASSERT(FelisceParam::instance().hyperElasticLaw.timeSchemeHyperelasticity != HyperelasticityNS::none);

  SolveStaticProblem();

  HyperelasticLinearProblem* linear_problem_ptr = dynamic_cast<HyperelasticLinearProblem*>(m_linearProblem[0]);
  FEL_ASSERT(linear_problem_ptr);
  HyperelasticLinearProblem& linear_problem = *linear_problem_ptr;

  linear_problem.GoToDynamic(); // Make sure to call it BEFORE clearing Matrix RHS!!!
  linear_problem.clearMatrixRHS();

  while (!hasFinished())
    forward();
}

} // namespace Elasticity
