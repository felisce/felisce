//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _SSTMODEL_HPP
#define _SSTMODEL_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/bdf.hpp"

namespace felisce {
  class SSTModel:
    public Model {
  public:
    /// Construtor.
    SSTModel();
    /// Destructor.
    ~SSTModel() override;

    void initializeDerivedModel() override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    //void extrapolate(bool flag=true);
    void setExternalVec() override;

    ///  Manage time iteration.
    void forward() override;

    ///  Function to get size of the state std::vector.
    int getNstate() const override;
    ///  Function to get state std::vector.
    void getState(double* & state) override;
    felInt numDof_swig();
    void getState_swig(double* data, felInt numDof);
    ///  Function to std::set state std::vector.
    void setState(double* & state) override;

    inline const Bdf & bdf() const {
      return m_bdfNS;
    }
    inline Bdf & bdf() {
      return m_bdfNS;
    }

  private:
    /// EDP time scheme.
    Bdf m_bdfNS;

    PetscVector  m_solExtrapolate;
  };
}

#endif
