//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau, E. Schenone
//

#ifndef _INITIALCONDITION_HPP
#define _INITIALCONDITION_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "DegreeOfFreedom/listVariable.hpp"
#include "InputOutput/ensight.hpp"

namespace felisce {
  class InitialCondition {
    /*!
      \class Initial Condition
      \authors J-F. Gerbeau, E. Schenone
      \date July 2012
      \brief Initial condition (to restart a model from an arbitrary time step)

     */
    ListVariable m_listVariable;
    //! Case of the observation
    EnsightCase* m_initialSolutionCase;
  public:
    InitialCondition() = default;
    void initializeFromFile(std::string inputDirectory, std::string inputCaseFile);
    void addVariable(const Variable& var);
    void readVariable(int iVariable, int indexTime, double* variableValue, felInt sizeVar);
    void print(int verbose, std::ostream& outstr) const;
    inline const ListVariable & listVariable() const {
      return m_listVariable;
    }
    inline ListVariable & listVariable() {
      return m_listVariable;
    }
  };
}

#endif
