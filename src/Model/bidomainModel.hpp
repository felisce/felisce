//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

#ifndef _BIDOMAINMODEL_HPP
#define _BIDOMAINMODEL_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/model.hpp"
#include "Solver/linearProblemBidomain.hpp"
#include "Solver/schafSolver.hpp"
#include "Solver/schafRevisedSolver.hpp"
#include "Solver/FhNSolver.hpp"
#include "Solver/courtemancheSolver.hpp"
#include "Solver/MVSolver.hpp"
#include "Solver/PaciSolver.hpp"
#include "Solver/BCLSolver.hpp"
#include "Solver/bdf.hpp"
#include "Solver/cardiacFunction.hpp"
#include "PETScInterface/romPETSC.hpp"

namespace felisce {
  class BidomainModel:
    public Model {
  public:
    ///Construtor.
    BidomainModel();
    ///Destructor.
    ~BidomainModel() override;
    void initializeDerivedModel() override;
    virtual void userDefinedIonicModel() {}
    void initializeDerivedLinearProblem() override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void updateTime(const FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs) override;
    void writeSolution();
    void userDefinedInitialConditions(std::size_t iProblem=0) override;
    /// Manage time iteration.
    void forward() override;

    void forwardROM();
    /// Initialize the SchafSolver and evaluate heterogeneous parameters of SchafSolver.
    void initializeSchafParameter();
    /// Initialize the MinimalVentricular and evaluate heterogeneous parameters of SchafSolver.
    void initializeMVParameter();
    /// Initialize the BCL and evaluate heterogeneous parameters of SchafSolver.
    void initializeBCLParameter();
    /// Initialize the FithughNagumo and evaluate heterogeneous parameters of SchafSolver.
    void initializeFhNParameter();
    /// Initialize the CourtemancheSolver and evaluate heterogeneous parameters of CourtemancheSolver.
    void initializeCourtParameter();
    // void initializeMVParameter();
    virtual void finalizeRHSDP(int iProblem=0);
    virtual void initializeAppCurrent();
    virtual void initializeAppCurrentExt();
    void evalIapp();
    void evalIappExt();

    MVSolver* getMVSolver();//For assimilation method----> get and std::set values
    std::vector<LinearProblemBidomain*> vecLPB() {
      return m_linearProblemBidomain;
    }

#ifdef FELISCE_WITH_CVGRAPH
    void forwardLight();
    void startIterationCVG() override;
    felInt m_kount;
#endif

  protected:
    RomPETSC* m_rom;
    int m_verbose;
    bool m_buildIonic;
    bool m_buildCourt;
    ///EDO solvers.
    IonicSolver* m_ionic;
    SchafSolver* m_schaf;
    FhNSolver* m_fhn;
    CourtemancheSolver* m_court;
    MVSolver* m_mv;
    PaciSolver* m_paci;
    BCLSolver* m_bcl;

    ///EDP time scheme.
    Bdf m_bdfEdp;
    ///Initialization vectors.
    std::vector<PetscVector> m_W_0;

    //For courtemanche model:
    //Gate conditions
    // INa
    PetscVector m_m;
    PetscVector m_h;
    PetscVector m_j;
    // ITo
    PetscVector m_ao;
    PetscVector m_io;
    // IKur
    PetscVector m_ua;
    PetscVector m_ui;
    // IKr
    PetscVector m_xr;
    // IKs
    PetscVector m_xs;
    // ICaL
    PetscVector m_d;
    PetscVector m_f;
    PetscVector m_fca;
    // Irel
    PetscVector m_urel;
    PetscVector m_vrel;
    PetscVector m_wrel;
    // Ion concentrations
    PetscVector m_nai;
    PetscVector m_nao;
    PetscVector m_cao;
    PetscVector m_ki;
    PetscVector m_ko;
    PetscVector m_cai;
    //Currents
    PetscVector m_naiont;
    PetscVector m_kiont;
    PetscVector m_caiont;
    PetscVector m_ileak;
    PetscVector m_iup;
    PetscVector m_itr;
    PetscVector m_irel;
    // Ca:
    PetscVector m_cmdn;   /* Calmodulin Buffered Ca Concentration (mM) */
    PetscVector m_trpn;   /* Troponin Buffered Ca Concentration (mM) */
    PetscVector m_nsr;    /* NSR Ca Concentration (mM) */
    PetscVector m_jsr;    /* JSR Ca Concentration (mM) */
    PetscVector m_csqn;   /* Calsequestrin Buffered Ca Concentration (mM) */

    AppCurrent* m_iApp;
    std::vector <double> m_iAppValue;
    AppCurrent* m_iAppExt;
    std::vector <double> m_iAppValueExt;
    /// Matrix of the luenberger filter
    PetscMatrix m_luenbFlter;
    std::vector<LinearProblemBidomain*> m_linearProblemBidomain;
    ///Data for schaf solver from EDP solution.
    PetscVector m_extrapolate;

  private:
    /// Case of the observation
    EnsightCase * m_observations;
    /// Vector to store Observations
    PetscVector m_Observ;
    bool m_extrapolateIsCreated;
    // Restart vectors
    PetscVector m_bdf_sol_n;
    PetscVector m_bdf_sol_n_1;
    PetscVector m_bdf_sol_n_2;
    std::vector<PetscVector> m_ionic_sol_n;
    std::vector<PetscVector> m_ionic_sol_n_1;
    std::vector<PetscVector> m_ionic_sol_n_2;
    std::vector<PetscVector> m_courtVarInit;
  };
}


#endif
