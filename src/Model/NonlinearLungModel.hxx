//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. Kazerani
//

#ifndef __FELISCE_NONLINEARLUNGMODEL_CPP__
#define __FELISCE_NONLINEARLUNGMODEL_CPP__

// System includes
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>

// External includes
#include "petscmat.h"
#include "petscsnes.h"
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtCore/QString>
#include <QtCore/QTime>
#include <QtCore/QDateTime>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QStringList>
#include <Qt/qapplication.h>
#include <Qt/qiodevice.h>
#include <Qt/qtextstream.h>

// Project includes
#include "Core/felisceTools.hpp"
#include "MGLungTree.h"
#include "MGGas.h"
#include "MGBreathingPattern.h"
#include "MGET.h"
#include "MGLungFactory.h"
#include "MGFlowData.h"
#include "morphoXMLManager.h"
#include "MGSegment.h"

namespace felisce {


  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::NonlinearLungModel() : Model() {
    m_name = "NonlinearLung";

    static_assert(!(TimeSchemeT != HyperelasticityNS::none && IsIncompressibleT == HyperelasticityNS::Incompressible::yes),
                  "Incompressibility isn't yet implemented for dynamic problems.");

  }



  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::~NonlinearLungModel()
  { }


  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::forward() {
    // Write solution for postprocessing (if required)
    writeSolution();

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();

    // Assemble the new stiffness matrix.
   HyperelasticNonlinearLungProblem* linear_problem_ptr =
      dynamic_cast<HyperelasticNonlinearLungProblem*>(m_linearProblem[0]);

    assert(linear_problem_ptr);
    HyperelasticNonlinearLungProblem& linear_problem = *linear_problem_ptr;
     //Update of IRM data for the non homogenous Dirichlet boundary condition
    int colT;
    colT=(m_fstransient->iteration-1)%period;    //if iteration step is bigger than the number of points within a period, we go back to the iteration%period -1 column of dataIRM
   if(modeLung=="displacement"){ 
     Vec disIRMcurrentBis;

  //  VecDestroy(&linear_problem.disIRMcurrent);
    //VecDestroy(&disIRMcurrentBis);
    VecCreate(PETSC_COMM_WORLD,&linear_problem.disIRMcurrent);
    VecCreate(PETSC_COMM_WORLD,&disIRMcurrentBis);
    VecSetSizes(linear_problem.disIRMcurrent, PETSC_DECIDE, linear_problem.m_numDof);
    VecSetSizes(disIRMcurrentBis, PETSC_DECIDE, linear_problem.m_numDof);
    VecSetFromOptions(linear_problem.disIRMcurrent);
    VecSetFromOptions(disIRMcurrentBis);
    VecZeroEntries(linear_problem.disIRMcurrent);
    VecZeroEntries(disIRMcurrentBis);
    VecAssemblyBegin(linear_problem.disIRMcurrent);
    VecAssemblyBegin(disIRMcurrentBis);

    MatGetColumnVector(dataIRM,linear_problem.disIRMcurrent,colT );
    MatGetColumnVector(dataIRM,disIRMcurrentBis, m_fstransient->iteration%period);
    VecAssemblyEnd(disIRMcurrentBis);
  VecAXPY(linear_problem.disIRMcurrent, -1 , disIRMcurrentBis);
//Added temporarily by  D.
   // VecScale(linear_problem.disIRMcurrent, 0.5);
     VecAssemblyEnd(linear_problem.disIRMcurrent);
 
    double nT;
    VecNorm(linear_problem.disIRMcurrent,NORM_2,&nT);
    std::cout<< "norm disIRMcurrent !! = " << nT << std::endl; 
   }
     //Finalize the Bmatrix assembly
//     linear_problem.divBasis.assembly(MAT_FINAL_ASSEMBLY);
          //Update volumeEvolPrevious and volumeEvol
  	linear_problem.volumeEvolPrevious.copyFrom(linear_problem.volumeEvol);  // will be used to compute the flow : Q_n=(V_n-V_n-1)/dt
	linear_problem.volumeEvol.zeroEntries(); 
        linear_problem.volumeEvol.assembly();
linear_problem.displacementReal.copyFrom(linear_problem.solution());
 //   linear_problem.computeVolumeEvol(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
    //AMatrix update
     linear_problem.AMatrix( inParam, lungTree,motherBranch, listObjectExits,listExitBranches);
    // Put VolumeIntegre eqqual to  0
    //linear_problem.VolumeIndicator = 0; 
    // Solve linear system.
/*
        PetscReal atol, rtol, stol;
        PetscInt maxit, maxf;
        SNESGetTolerances(linear_problem.snesInterface()->getSNES(), &atol, &rtol, &stol, &maxit, &maxf);

        std::cout<<"Snes absoluteTolerance is "<< atol <<std::endl;
        std::cout<<"Snes relativeTolerance is "<< rtol <<std::endl;
        std::cout<<" convergence tolerance in terms of the norm of the change in the solution between steps "<< stol <<std::endl;
        std::cout<<" maximum number of iterations is "<< maxit <<std::endl;
	std::cout<<" maximum number of function evaluations "<<maxf<<std::endl;

        //abort();
*/
/*
PetscInt version;
 PetscReal rtol_0, rtol_max, gamma, alpha, alpha2,threshold; 
SNESKSPGetParametersEW(linear_problem.snesInterface()->getSNES(), &version, &rtol_0, &rtol_max, &gamma, &alpha, &alpha2, &threshold);
        std::cout<<"SnesKsp version is  "<< version <<std::endl;
        std::cout<<"rtol_0 and rtol_max are "<< rtol_0 <<" "<<rtol_max<<std::endl;
        std::cout<<" gamma is "<< gamma <<std::endl;
        std::cout<<" alpha and alpha2 are "<< alpha <<" " <<alpha2<<std::endl;
	std::cout<<" threshold is "<<threshold<<std::endl;
*/
   //Neumann condition update
    int idL[1];
	  idL[0]=m_fstransient->iteration%period;
	  PetscScalar  valP[1];
	  VecGetValues(dataPressure,1,idL,valP);
	  std::cout <<" valPressure = " << valP[0] <<std::endl;
	  linear_problem.currentPressure=valP[0];

   // linear_problem.iterativeSolve(MpiInfo::rankProc(), MpiInfo::numProc(), ApplyNaturalBoundaryConditions::no, FlagMatrixRHS::matrix_and_rhs);
linear_problem.iterativeSolve(MpiInfo::rankProc(), MpiInfo::numProc(), ApplyNaturalBoundaryConditions::yes, FlagMatrixRHS::matrix_and_rhs);
    //Added by D. Kaz for Matrix Free integration
   /*PetscBool flg;
  PetscOptionsHasName(FELISCE_PETSC_NULLPTR,"-user_precond",&flg);//;CHKERRQ(ierr);
    if (flg) {
       std::cout << "Coucou!!" << std::endl;
     KSP ksp;
   PC pc;
  // PetscErrorCode ierr;
   SNESGetKSP(linear_problem.snesInterface()->getSNES(),&ksp);//CHKERRQ(ierr);
   KSPGetPC(ksp,&pc);//CHKERRQ(ierr);
   PCSetType(pc, PCJACOBI);//CHKERRQ(ierr);
   // PCSetType(pc,PCSHELL); //CHKERRQ(ierr);
    //PCShellSetApply(pc,MatrixFreePreconditioner);//CHKERRQ(ierr);

   }

    SNESSetFromOptions(linear_problem.snesInterface()->getSNES());*/


  //finalize volumeEvol and volumeEvol PReviuous assembly and compute the flows
    //linear_problem.volumeEvol.assembly();
        linear_problem.volumeEvolPrevious.assembly();
    // std::cout<<"Volume std::vector is:  "<<std::endl;
//  VecView(linear_problem.volumeEvol.toPetsc(),PETSC_VIEWER_STDOUT_WORLD);
        //flow computation with actualized state
//      m_linearProblem[0]->computeVolumeLungRegions2(m_linearProblemLung.flows,m_linearProblemLung.previousVelocity, m_rankProc, FlagMatrixRHS::only_rhs);
        linear_problem.flows.copyFrom(linear_problem.volumeEvol);
        linear_problem.flows.axpy(-1.,linear_problem.volumeEvolPrevious);
        double dtinv=m_fstransient->timeStep;
        dtinv=1./dtinv;
        linear_problem.flows.scale(dtinv);   //Q_n=(V_n-V_(n-1))/dt

        linear_problem.flows.assembly();

       // linear_problem.displacementReal.copyFrom(linear_problem.solution());

 //Write Volume Evoultion of regions in a text file


/******************************************************************/
//WriteVolReg();
        //PetscVector volumeEvollgath;
        //linear_problem.volumeEvol.scatterToAll(volumeEvollgath,INSERT_VALUES,SCATTER_FORWARD);
	//Writes volume regions evolution
       int nbRegions;
        nbRegions=FelisceParam::instance().nbSubRegions;
	PetscScalar valToGet[nbRegions];
	PetscInt ix[nbRegions];
	for(int i=0;i<nbRegions;i++)
	{
	  ix[i]=i;
        valToGet[i] =0;
	}
	//volumeEvollgath.getValues(nbRegions,ix,valToGet); 
        linear_problem.volumeEvol.getValues(nbRegions,ix,valToGet);
/*
     std::cout<<"ValToGet is:  "<<std::endl;
	for(int i=0;i<nbRegions;i++)
	{
         std::cout<<valToGet[i]<<" ";
	}
std::cout<<std::endl;
*/
	std::ofstream volumeEvolution;
	QString gasName;
	if (density<0.5) {
	  gasName="HeO2";  
	}
	else{
	  gasName="Air";
	}
	
	QString modeBuildTreeFb=inParam->getModelBuildTree();
        QString resistanceMode= inParam->getResistanceMode();

	QString nameFaddb;
	if(modeBuildTreeFb=="stenosis"){
	  nameFaddb= "results/Volume_" + modeBuildTreeFb+branchDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeFb=="generationalStenosis"){
	  nameFaddb= "results/Volume_" + modeBuildTreeFb+nbGeneration +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeFb=="asthma"){
	  nameFaddb= "results/Volume_" + modeBuildTreeFb+lobeDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeFb=="healthy"){
	  if(emphysemaRatio==1){
	    nameFaddb= "results/Volume_" + modeBuildTreeFb + "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	  else{
	    nameFaddb= "results/Volume_MechaDis" +lobeEmph+"_" +emphysemaRat+ "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	}
	std::string nameStrb = nameFaddb.toUtf8().constData();
	const char * nameChb=nameStrb.c_str();
	volumeEvolution.open(nameChb,ios::out | std::ios::app);

	//double oldT;
        double T; 
	//oldT=m_fstransient->time-m_fstransient->timeStep;
        T = (m_fstransient->iteration)*(m_fstransient->timeStep);
	//volumeEvolution<< oldT;
        volumeEvolution<< T;
	for(int i=0;i<nbRegions;i++)
	{
	  volumeEvolution<< " "<< valToGet[i];
	}
	volumeEvolution<<std::endl;
	volumeEvolution.close();

//lobar volume file
	double valLU, valLL, valRU, valRM, valRL;
	valLU=0;
	valLL=0;
	valRU=0;
	valRM=0;
	valRL=0;
	for(int i=0;i<nbRegions;i++){
	  if(listExitBranches[i].startsWith("LU")){
	    valLU=valLU+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("LL")){
	    valLL=valLL+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("RU")){
	    valRU=valRU+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("RM")){
	    valRM=valRM+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("RL")){
	    valRL=valRL+valToGet[i];
	  }
	}
	std::ofstream volumeEvolutionLobar;
	 
	QString modeBuildTreeF=inParam->getModelBuildTree();
	QString nameFadd;
	if(modeBuildTreeF=="stenosis"){
	  nameFadd= "results/VolumeLobar__" + modeBuildTreeF+branchDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeF=="generationalStenosis"){
	  nameFadd= "results/VolumeLobar__" + modeBuildTreeF+nbGeneration +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeF=="asthma"){
	  nameFadd= "results/VolumeLobar__" + modeBuildTreeF+lobeDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeF=="healthy"){
	  if(emphysemaRatio==1){
	    nameFadd= "results/VolumeLobar__" + modeBuildTreeF + "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	  else{
	    nameFadd= "results/VolumeLobar__MechaDis" +lobeEmph+"_" +emphysemaRat+ "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	}
	
	std::string nameStr = nameFadd.toUtf8().constData();
	const char * nameCh=nameStr.c_str();
	volumeEvolutionLobar.open(nameCh,ios::out | std::ios::app);
// 	volumeEvolutionLobar.open("volumeEvolutionLobar.txt",ios::out | std::ios::app);
	//volumeEvolutionLobar<< oldT << " "<< valLU << " " << valLL << " " << valRU << " "<< valRM << " "<< valRL << std::endl;
        volumeEvolutionLobar<< T << " "<< valLU << " " << valLL << " " << valRU << " "<< valRM << " "<< valRL << std::endl;
	volumeEvolutionLobar.close();
/**********************************************************************/
    // Update
    linear_problem.endIteration();
  }

/*************************/
  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>:: WriteVolReg() {

         HyperelasticNonlinearLungProblem* linear_problem_ptr =
         dynamic_cast<HyperelasticNonlinearLungProblem*>(m_linearProblem[0]);

         assert(linear_problem_ptr);
         HyperelasticNonlinearLungProblem& linear_problem = *linear_problem_ptr;
  
     	PetscVector volumeEvollgath;
        linear_problem.volumeEvol.scatterToAll(volumeEvollgath,INSERT_VALUES,SCATTER_FORWARD);
	//Writes volume regions evolution
       int nbRegions;
        nbRegions=FelisceParam::instance().nbSubRegions;
	PetscScalar valToGet[nbRegions];
	PetscInt ix[nbRegions];
	for(int i=0;i<nbRegions;i++)
	{
	  ix[i]=i;
	}
	volumeEvollgath.getValues(nbRegions,ix,valToGet); 
	std::ofstream volumeEvolution;
	QString gasName;
	if (density<0.5) {
	  gasName="HeO2";  
	}
	else{
	  gasName="Air";
	}
	
	QString modeBuildTreeFb=inParam->getModelBuildTree();
        QString resistanceMode= inParam->getResistanceMode();

	QString nameFaddb;
	if(modeBuildTreeFb=="stenosis"){
	  nameFaddb= "results/Volume_" + modeBuildTreeFb+branchDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeFb=="generationalStenosis"){
	  nameFaddb= "results/Volume_" + modeBuildTreeFb+nbGeneration +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeFb=="asthma"){
	  nameFaddb= "results/Volume_" + modeBuildTreeFb+lobeDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeFb=="healthy"){
	  if(emphysemaRatio==1){
	    nameFaddb= "results/Volume_" + modeBuildTreeFb + "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	  else{
	    nameFaddb= "results/Volume_MechaDis" +lobeEmph+"_" +emphysemaRat+ "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	}
	std::string nameStrb = nameFaddb.toUtf8().constData();
	const char * nameChb=nameStrb.c_str();
	volumeEvolution.open(nameChb,ios::out | std::ios::app);

	double oldT;
	oldT=m_fstransient->time-m_fstransient->timeStep;
	volumeEvolution<< oldT;
	for(int i=0;i<nbRegions;i++)
	{
	  volumeEvolution<< " "<< valToGet[i];
	}
	volumeEvolution<<std::endl;
	volumeEvolution.close();

//lobar volume file
	double valLU, valLL, valRU, valRM, valRL;
	valLU=0;
	valLL=0;
	valRU=0;
	valRM=0;
	valRL=0;
	for(int i=0;i<nbRegions;i++){
	  if(listExitBranches[i].startsWith("LU")){
	    valLU=valLU+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("LL")){
	    valLL=valLL+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("RU")){
	    valRU=valRU+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("RM")){
	    valRM=valRM+valToGet[i];
	  }
	  if(listExitBranches[i].startsWith("RL")){
	    valRL=valRL+valToGet[i];
	  }
	}
	std::ofstream volumeEvolutionLobar;
	 
	QString modeBuildTreeF=inParam->getModelBuildTree();
	QString nameFadd;
	if(modeBuildTreeF=="stenosis"){
	  nameFadd= "results/VolumeLobar__" + modeBuildTreeF+branchDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeF=="generationalStenosis"){
	  nameFadd= "results/VolumeLobar__" + modeBuildTreeF+nbGeneration +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeF=="asthma"){
	  nameFadd= "results/VolumeLobar__" + modeBuildTreeF+lobeDis +"_" +diseaseRat + "_" + resistanceMode+"_"+gasName+".txt";
	}
	if(modeBuildTreeF=="healthy"){
	  if(emphysemaRatio==1){
	    nameFadd= "results/VolumeLobar__" + modeBuildTreeF + "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	  else{
	    nameFadd= "results/VolumeLobar__MechaDis" +lobeEmph+"_" +emphysemaRat+ "_" + resistanceMode + "_" + gasName + ".txt";
	  }
	}
	
	std::string nameStr = nameFadd.toUtf8().constData();
	const char * nameCh=nameStr.c_str();
	volumeEvolutionLobar.open(nameCh,ios::out | std::ios::app);
// 	volumeEvolutionLobar.open("volumeEvolutionLobar.txt",ios::out | std::ios::app);
	volumeEvolutionLobar<< oldT << " "<< valLU << " " << valLL << " " << valRU << " "<< valRM << " "<< valRL << std::endl;
	volumeEvolutionLobar.close();
  }
/************************/
  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::SolveStaticProblem() {
#ifndef NDEBUG
    static bool first_call = true;
    assert(first_call);
    first_call = false;
#endif // NDEBUG
       //if iteration step is bigger than the number of points within a period, we go back to the iteration%period -1 column of dataIRM

    PetscPrintf(MpiInfo::petscComm(), "\n----------------------------------------------\n");
    PetscPrintf(MpiInfo::petscComm(), "Static problem\n");
    PetscPrintf(MpiInfo::petscComm(), "----------------------------------------------\n");

    HyperelasticNonlinearLungProblem* linear_problem_ptr = dynamic_cast<HyperelasticNonlinearLungProblem*>(m_linearProblem[0]);
    assert(linear_problem_ptr);
    HyperelasticNonlinearLungProblem& linear_problem = *linear_problem_ptr;
    //MatGetColumnVector(dataIRM,linear_problem.disIRMcurrent,0);
    PetscVector volTmp;    
    int nbRegions=FelisceParam::instance().nbSubRegions;
       //initialize volumeEvol and volumeEvolPrevious
        volTmp.createMPI(PETSC_COMM_WORLD,PETSC_DECIDE,nbRegions);
        volTmp.zeroEntries();
        volTmp.assembly();
        linear_problem.volumeEvol.duplicateFrom(volTmp);
        linear_problem.volumeEvolPrevious.duplicateFrom(volTmp);
        linear_problem.flows.duplicateFrom(volTmp);
        volTmp.destroy();

      //Initialization of displacementReal
        linear_problem.displacementReal.duplicateFrom(linear_problem.m_sol);
        VecSet(linear_problem.displacementReal.toPetsc(),0.0);
        linear_problem.displacementReal.assembly();
    
       // VecCopy(linear_problem.m_sol.toPetsc(), linear_problem.disIRMcurrent);
  //  linear_problem.divBasis.assembly(MAT_FINAL_ASSEMBLY);
    // Solve the system.
          //For Neumann condition
          
          int idL[1];
	  idL[0]=m_fstransient->iteration%period;
	  PetscScalar  valP[1];
	  VecGetValues(dataPressure,1,idL,valP);
	  std::cout <<" valPressure = " << valP[0] <<std::endl;
	  linear_problem.currentPressure=valP[0];
/*
    KSPSetTolerances(linear_problem.ksp(),10.e-6,10.e-12,
                     PETSC_DEFAULT,1000);
*/
       linear_problem.iterativeSolve(MpiInfo::rankProc(), MpiInfo::numProc());
      
   //  linear_problem.computeVolumeEvol(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
//finalize volumeEvol and volumeEvol PReviuous assembly and compute the flows

      // linear_problem.volumeEvol.assembly();
       linear_problem.volumeEvolPrevious.assembly();


        //flow computation with actualized state
//      m_linearProblem[0]->computeVolumeLungRegions2(m_linearProblemLung.flows,m_linearProblemLung.previousVelocity, m_rankProc, FlagMatrixRHS::only_rhs);
        linear_problem.flows.copyFrom(linear_problem.volumeEvol);
        linear_problem.flows.axpy(-1.,linear_problem.volumeEvolPrevious);
        double dtinv=m_fstransient->timeStep;
        dtinv=1./dtinv;
        linear_problem.flows.scale(dtinv);   //Q_n=(V_n-V_(n-1))/dt

        linear_problem.flows.assembly();
 
 }


  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::SolveDynamicProblem() {

    static_assert(TimeSchemeT != HyperelasticityNS::none,
                  "Time scheme can't be none when a dynamic problem is to considered!");

    int nbRegions=FelisceParam::instance().nbSubRegions;    
     PetscVector volTmp;
    

    SolveStaticProblem();

    HyperelasticNonlinearLungProblem* linear_problem_ptr =
      dynamic_cast<HyperelasticNonlinearLungProblem*>(m_linearProblem[0]);
    assert(linear_problem_ptr);
    HyperelasticNonlinearLungProblem& linear_problem = *linear_problem_ptr;
     
     //initialize volumeEvol and volumeEvolPrevious
std::cout <<" Coucou1 : SolvedynamicPb"<<std::endl;
        volTmp.createMPI(PETSC_COMM_WORLD,PETSC_DECIDE,nbRegions);
      volTmp.zeroEntries();
     volTmp.assembly();
         linear_problem.volumeEvol.duplicateFrom(volTmp);
         linear_problem.volumeEvolPrevious.duplicateFrom(volTmp);
         linear_problem.flows.duplicateFrom(volTmp);
     volTmp.destroy();
std::cout <<" Coucou2 : SolvedynamicPb"<<std::endl;

    linear_problem.displacementReal.duplicateFrom(linear_problem.solution());
    VecSet(linear_problem.displacementReal.toPetsc(),0.0);
    linear_problem.displacementReal.assembly();
      
std::cout <<" Coucou3 : SolvedynamicPb"<<std::endl;
    linear_problem.GoToDynamic(); // Make sure to call it BEFORE clearing Matrix RHS!!!
    linear_problem.clearMatrixRHS();

    Vec vec;
    VecCreate(PETSC_COMM_WORLD,&vec);
    VecSetSizes(vec, PETSC_DECIDE, linear_problem.m_numDof);
    VecSetFromOptions(vec);
   VecZeroEntries(vec);
   VecAssemblyBegin(vec);
   // MatGetColumnVector(dataIRM,vec, 0);
std::cout <<" Coucou4 : SolvedynamicPb"<<std::endl;
     if(modeLung=="displacement"){
          MatGetColumnVector(dataIRM, linear_problem.m_sol.toPetsc(), 0);
     }
    VecCopy(vec, linear_problem.m_sol.toPetsc() );
  // VecView(vec,	PETSC_VIEWER_STDOUT_WORLD);
     //MatView(dataIRM,	PETSC_VIEWER_STDOUT_WORLD);
  //  abort();
    //int colT;
 KSPSetTolerances(linear_problem.ksp(),10.e-9,10.e-12,
                     PETSC_DEFAULT,1000000);
    while (!hasFinished()){
std::cout <<" Coucou5 : SolvedynamicPb"<<std::endl;
      forward();
    }
  }

  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::updateTime(const FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_FLAG_MATRIX_RHS;
    m_fstransient->iteration++;
    m_fstransient->time +=m_fstransient->timeStep;
  }
  /**********************/
  /*
 template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::initializeDerivedLinearProblem(){
      HyperelasticNonlinearLungProblem* linear_problem_ptr =
        dynamic_cast<HyperelasticNonlinearLungProblem*>(m_linearProblem[0]);
      assert(linear_problem_ptr);
      HyperelasticNonlinearLungProblem& linear_problem = *linear_problem_ptr;
        int numDofTot;
      VecGetSize(linear_problem.solution().toPetsc(),&numDofTot);
        VecCreate(PETSC_COMM_WORLD,&linear_problem.disIRMcurrent);
        VecSetSizes(linear_problem.disIRMcurrent,PETSC_DECIDE,numDofTot );
             VecSetFromOptions(linear_problem.disIRMcurrent);
        VecZeroEntries(linear_problem.disIRMcurrent);
        VecAssemblyBegin(linear_problem.disIRMcurrent);
         VecAssemblyEnd(linear_problem.disIRMcurrent);
  }*/

  /**************************/

  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  //void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::initializeLungVectors(HyperelasticNonlinearLungProblem InitialNonlinearLungProblem){
  void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::initializeLungVectors( ){
    //intializations of all the vectors needed to solve the FEM system
    int nbRegions;
    nbRegions=FelisceParam::instance().nbSubRegions;         //nb of terminal regions = number of exits of the tree
    modeLung=FelisceParam::instance().modeLung; 		 //possible modes : pressure, displacemement, pressureRealistic, mechanicalVentilation
    int nbTimeSteps;
    nbTimeSteps=FelisceParam::instance().timeIterationMax;   //number of iterations on which the simulation is lauched
    period=FelisceParam::instance().periodNbSteps;   //number of time steps per period. If period=n, at time step m>n, we consider the m%n'th element of dataPressure/dataIRM : no need to store more than one pressure cycle
    flagJacobi=FelisceParam::instance().flagJacobi; 		 //possible modes : pressure, displacemement, pressureRealistic, mechanicalVentilation
  /****
    //solution
   //Commented by D. InitialNonlinearLungProblem.sol.duplicateFrom(m_linearProblem[0]->solution());        //FEM solution. Contain the speed at current time step
     InitialNonlinearLungProblem.sol.duplicateFrom(m_linearProblem[0]->solution());
    //HyperelasticNonlinearLungProblem.solTmp.duplicateFrom(m_linearProblem[0]->solution()); //commented by .D
     InitialNonlinearLungProblem.solTmp.duplicateFrom(m_linearProblem[0]->solution());
    //states : displacement and velocity
     //HyperelasticNonlinearLungProblem Replaced here by HyperelasticNonlinearLungProblem by D.
    InitialNonlinearLungProblem.previousDisplacement.duplicateFrom(m_linearProblem[0]->solution());    //displacement field at previous time step
    InitialNonlinearLungProblem.previousVelocity.duplicateFrom(m_linearProblem[0]->solution());	   //velocity field at previous time step
    InitialNonlinearLungProblem.newDisplacement.duplicateFrom(m_linearProblem[0]->solution());	   //displacement field at current time step
    InitialNonlinearLungProblem.newVelocity.duplicateFrom(m_linearProblem[0]->solution());		   //velocity field at current time step
    InitialNonlinearLungProblem.displacementReal.duplicateFrom(m_linearProblem[0]->solution());	   //used for volume calculation in the displacement mode : displReal=displSurface+displ where displ the the state used for the calculation, null over the surface
    InitialNonlinearLungProblem.displacementTmp.duplicateFrom(m_linearProblem[0]->solution());
    InitialNonlinearLungProblem.velocityTmp.duplicateFrom(m_linearProblem[0]->solution());


    //rhs members
    InitialNonlinearLungProblem.rhsElasticity.duplicateFrom(m_linearProblem[0]->vector());
    InitialNonlinearLungProblem.rhsMinus.duplicateFrom(m_linearProblem[0]->vector());
    InitialNonlinearLungProblem.rhsCoupling.duplicateFrom(m_linearProblem[0]->vector());
    InitialNonlinearLungProblem.rhsTot.duplicateFrom(m_linearProblem[0]->vector());
    InitialNonlinearLungProblem.rhsMinus3.duplicateFrom(m_linearProblem[0]->vector());
    InitialNonlinearLungProblem.rhsMecha.duplicateFrom(m_linearProblem[0]->vector());


    VecDuplicate(InitialNonlinearLungProblem.rhsCoupling.toPetsc(),&InitialNonlinearLungProblem.rhsMinus2);
    VecDuplicate(InitialNonlinearLungProblem.rhsCoupling.toPetsc(),&InitialNonlinearLungProblem.rhsCoupling2);

    // diagonal for preconditionning
    VecDuplicate(InitialNonlinearLungProblem.rhsCoupling.toPetsc(),&InitialNonlinearLungProblem.reciprocalDiago);

    // surface mesh normal std::vector field
  // 	VecDuplicate(InitialNonlinearLungProblem.rhsCoupling.toPetsc(),& InitialNonlinearLungProblem.normalVecmesh->toPetsc());
    InitialNonlinearLungProblem.normalVecmesh->duplicateFrom(m_linearProblem[0]->vector());
  ****/
    if(modeLung=="pressureRealistic"){
      VecDuplicate(m_linearProblem[0]->vector().toPetsc(),&displacementCurrent);
      VecZeroEntries(displacementCurrent);
      VecDuplicate(m_linearProblem[0]->vector().toPetsc(),&displacementPrevious);
      VecZeroEntries(displacementPrevious);
      VecDuplicate(m_linearProblem[0]->vector().toPetsc(),&displacementPrevious2);
      VecZeroEntries(displacementPrevious2);
      VecDuplicate(m_linearProblem[0]->vector().toPetsc(),&velocityCurrent);
      VecZeroEntries(velocityCurrent);
      VecDuplicate(m_linearProblem[0]->vector().toPetsc(),&velocityPrevious);
      VecZeroEntries(velocityPrevious);
    }

  /*****
    //other needed vectors
    VecSet(InitialNonlinearLungProblem.sol.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.solTmp.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.previousDisplacement.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.previousVelocity.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.newDisplacement.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.newVelocity.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.rhsElasticity.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.rhsMinus.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.rhsMinus3.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.rhsMecha.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.rhsCoupling.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.rhsTot.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.displacementReal.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.displacementTmp.toPetsc(),0.0);
    VecSet(InitialNonlinearLungProblem.velocityTmp.toPetsc(),0.0);
    VecZeroEntries(InitialNonlinearLungProblem.rhsMinus2);
    VecZeroEntries(InitialNonlinearLungProblem.rhsCoupling2);
    VecZeroEntries(InitialNonlinearLungProblem.reciprocalDiago);
    VecZeroEntries(InitialNonlinearLungProblem.normalVecmesh->toPetsc());

    //states : displacement and velocity
    InitialNonlinearLungProblem.previousDisplacement.assembly();
    InitialNonlinearLungProblem.previousVelocity.assembly();
    InitialNonlinearLungProblem.newDisplacement.assembly();
    InitialNonlinearLungProblem.newVelocity.assembly();

    //rhs members
    InitialNonlinearLungProblem.rhsElasticity.assembly();
    InitialNonlinearLungProblem.rhsMinus.assembly();
    InitialNonlinearLungProblem.rhsMinus3.assembly();
    InitialNonlinearLungProblem.rhsMecha.assembly();
    InitialNonlinearLungProblem.rhsCoupling.assembly();
    VecAssemblyBegin(InitialNonlinearLungProblem.rhsMinus2);
    VecAssemblyBegin(InitialNonlinearLungProblem.rhsCoupling2);
    VecAssemblyBegin(InitialNonlinearLungProblem.reciprocalDiago);
    VecAssemblyBegin(InitialNonlinearLungProblem.normalVecmesh->toPetsc());
    VecAssemblyEnd(InitialNonlinearLungProblem.rhsMinus2);
    VecAssemblyEnd(InitialNonlinearLungProblem.rhsCoupling2);
    VecAssemblyEnd(InitialNonlinearLungProblem.reciprocalDiago);
    VecAssemblyEnd(InitialNonlinearLungProblem.normalVecmesh->toPetsc());
    InitialNonlinearLungProblem.rhsTot.assembly();
    InitialNonlinearLungProblem.displacementReal.assembly();
    InitialNonlinearLungProblem.displacementTmp.assembly();
    InitialNonlinearLungProblem.velocityTmp.assembly();
  ***/
    PetscInt locSizeProd;
    PetscVector divVelTmp;
    divVelTmp.create(PETSC_COMM_WORLD);
    divVelTmp.setSizes(PETSC_DECIDE,nbRegions);
    divVelTmp.setFromOptions();
    divVelTmp.zeroEntries();
    divVelTmp.assembly();
  /***
  // 	HyperelasticNonlinearLungProblem.divVel.duplicateFrom(m_linearProblem[0]->solution());
    InitialNonlinearLungProblem.divVel.duplicateFrom(divVelTmp);
    ***/
    divVelTmp.destroy();
  /***
    // 	InitialNonlinearLungProblem.divVel.getLocalSize(&locSizeProd);

    InitialNonlinearLungProblem.divVel.zeroEntries();
  // 	InitialNonlinearLungProblem.divVel.assembly();
  ****/

    m_linearProblem[0]->solution().getLocalSize(&locSizeProd);

   int numDofTot; //Commented by D.
    VecGetSize(m_linearProblem[0]->solution().toPetsc(),&numDofTot);

    // InitialNonlinearLungProblem.numDofPb=numDofTot;    //total number of degrees of freedom // Commented by D.
    std::cout<<"number of terminal regions = "<<nbRegions<< std::endl;
    //Resistance matrix read from the input file "ResistanceFile.txt"
          // 	double dataResistance[nbRegions][nbRegions];
          // 	int xR,yR;
          // 	std::ifstream in("ResistanceFile.txt");
          // 	if(!in){
          // 	  std::cout << "cannot open file" << std::endl;
          // 	}
          // 	for(xR=0;xR<nbRegions;xR++){
          // 	  for(yR=0;yR<nbRegions;yR++){
          // 	    in>>dataResistance[xR][yR];
          // 	  }
          // 	}
          // 	in.close();


    if(modeLung=="pressure"){
         //in case a homogenous pressure field is applied, data is read from the input file "dataPressure.txt
   //This part comes fr Nic
      double dataPressuretmp[period][2];
      //double dataPressuretmp[period][1];
      int xD,yD;
      std::ifstream in2("dataPressure.txt");
      if(!in2){
        std::cout << "cannot open file" << std::endl;
      }
      for(xD=0;xD<period;xD++){
   //     for(yD=0;yD<2;yD++)
         for(yD=0;yD<2;yD++)
        {
          in2>>dataPressuretmp[xD][yD];
        }
      }
      in2.close();
      //CAUTION : the time step is constant along the simulation and specified in the data file
      VecCreate(PETSC_COMM_WORLD,&dataPressure);
      VecSetSizes(dataPressure,PETSC_DECIDE,period);
      VecSetFromOptions(dataPressure);
      for(int i=0;i<period;i++)
      {
     //     VecSetValue(dataPressure,i,dataPressuretmp[i][1],INSERT_VALUES);
      
      VecSetValue(dataPressure,i,dataPressuretmp[i][1],INSERT_VALUES);}
      VecAssemblyBegin(dataPressure);
      VecAssemblyEnd(dataPressure);
    }

    if(modeLung=="mechanicalVentilation"){
      //in case the ventilator imposes a pressure at the trachea, data is read from the input file "dataPressureMecha.txt"
      double dataPressuretmp[period][2];
      int xD,yD;
      std::ifstream in2("dataPressureMecha.txt");
      if(!in2){
        std::cout << "cannot open file" << std::endl;
      }
      for(xD=0;xD<period;xD++){
        for(yD=0;yD<2;yD++)
        {
          in2>>dataPressuretmp[xD][yD];
        }
      }
      in2.close();
      //CAUTION : the time step is constant along the simulation and specified in the data file
      VecCreate(PETSC_COMM_WORLD,&dataPressure);
      VecSetSizes(dataPressure,PETSC_DECIDE,period);
      VecSetFromOptions(dataPressure);
      for(int i=0;i<period;i++)
      {
          VecSetValue(dataPressure,i,dataPressuretmp[i][1],INSERT_VALUES);
      }
      VecAssemblyBegin(dataPressure);
      VecAssemblyEnd(dataPressure);

      nbLobes = 0;
      std::ifstream in2b("dataMechanicalVentilation.txt");   //this routines counts the number of lines of the file to deduce the number of lobes considered within the simulation
      std::string unused;
      int numLines=0;
      while ( std::getline(in2b, unused) ){
        ++numLines;
      }
      in2b.close();
      nbLobes=numLines;

      std::cout <<"nbLobes =" << nbLobes << std::endl;

      double dataVolTmp[nbLobes][2];   //col1 is volFRC, col2 is volTLC
      std::ifstream in2bb("dataMechanicalVentilation.txt");
      if(!in2bb){
        std::cout << "cannot open file" << std::endl;
      }
      for(xD=0;xD<nbLobes;xD++){
        for(yD=0;yD<2;yD++)
        {
          in2bb>>dataVolTmp[xD][yD];
        }
      }
      in2bb.close();

      volFRC.create(PETSC_COMM_WORLD);
      volFRC.setSizes(PETSC_DECIDE,nbLobes);
      volFRC.setFromOptions();
      for(int i=0;i<nbLobes;i++)
      {
          volFRC.setValue(i,dataVolTmp[i][0],INSERT_VALUES);
          std::cout << "valT frc "<< dataVolTmp[i][0] << std::endl;
      }
      volFRC.assembly();

      volTLC.create(PETSC_COMM_WORLD);
      volTLC.setSizes(PETSC_DECIDE,nbLobes);
      volTLC.setFromOptions();
      for(int i=0;i<nbLobes;i++)
      {
          volTLC.setValue(i,dataVolTmp[i][1],INSERT_VALUES);
          std::cout << "valT tlc "<< dataVolTmp[i][1] << std::endl;
      }
      volTLC.assembly();

      volCurr.create(PETSC_COMM_WORLD);
      volCurr.setSizes(PETSC_DECIDE,nbLobes);
      volCurr.setFromOptions();
      for(int i=0;i<nbLobes;i++)
      {
          volCurr.setValue(i,0,INSERT_VALUES);
      }
      volCurr.assembly();

      volCurrTmp.create(PETSC_COMM_WORLD);
      volCurrTmp.setSizes(PETSC_DECIDE,nbLobes);
      volCurrTmp.setFromOptions();
      for(int i=0;i<nbLobes;i++)
      {
          volCurrTmp.setValue(i,0,INSERT_VALUES);
      }
      volCurrTmp.assembly();

      volCurrStep.create(PETSC_COMM_WORLD);
      volCurrStep.setSizes(PETSC_DECIDE,nbLobes);
      volCurrStep.setFromOptions();
      for(int i=0;i<nbLobes;i++)
      {
          volCurrStep.setValue(i,0,INSERT_VALUES);
      }
      volCurrStep.assembly();

      volPrev.create(PETSC_COMM_WORLD);
      volPrev.setSizes(PETSC_DECIDE,nbLobes);
      volPrev.setFromOptions();
      for(int i=0;i<nbLobes;i++)
      {
          volPrev.setValue(i,0,INSERT_VALUES);
      }
      volPrev.assembly();
    }
/*
    //divBasis=int_omega_i(div(e_j)). Used for tree-parenchyma coupling
    PetscMatrix divBasisTmp;
    divBasisTmp.createAIJ(PETSC_COMM_WORLD,PETSC_DECIDE,locSizeProd,nbRegions,numDofTot,locSizeProd,FELISCE_PETSC_NULLPTR,numDofTot-locSizeProd,FELISCE_PETSC_NULLPTR);
    divBasisTmp.setFromOptions();
    MatSetOption(divBasisTmp.toPetsc(),MAT_NEW_NONZERO_ALLOCATION_ERR,PETSC_FALSE);
    divBasisTmp.zeroEntries();
    divBasisTmp.assembly(MAT_FINAL_ASSEMBLY);

   // InitialNonlinearLungProblem.divBasis.duplicateFrom(divBasisTmp,MAT_COPY_VALUES);
 //   InitialNonlinearLungProblem.divBasis.zeroEntries();

    divBasisTmp.destroy();*/

    //int nbL, nbC; //Commented by D.
    //MatGetLocalSize(InitialNonlinearLungProblem.divBasis.toPetsc(),&nbL,&nbC); //Commented by D.


    //resistance matrix construction //This part was commented by Nico
  // 	PetscMatrix resistanceMatrixTmp;
  // 	resistanceMatrixTmp.createDense(PETSC_COMM_WORLD,nbL,nbL,nbRegions,nbRegions,FELISCE_PETSC_NULLPTR);
  // 	HyperelasticNonlinearLungProblem.m_nbSubRegions=nbRegions;
  //
  // 	for(int i=0;i<nbRegions;i++)
  // 	{
  // 	  for(int j=0;j<nbRegions;j++)
  // 	  {
  // 	    resistanceMatrixTmp.setValue(i,j,dataResistance[i][j],INSERT_VALUES);
  // 	  }
  // 	}
  // 	resistanceMatrixTmp.assembly(MAT_FINAL_ASSEMBLY);
  // 	HyperelasticNonlinearLungProblem.resistanceMatrix.duplicateFrom(resistanceMatrixTmp,MAT_COPY_VALUES);
  // 	resistanceMatrixTmp.destroy();

    if(modeLung=="displacement"){
      //CREATION of dataIRM matrix
      double valTmp;
      double dataIRMtmp[numDofTot][1];
      int xDb,yDb;
      std::ifstream in2b("dataIRM.txt");
      if(!in2b){
        std::cout << "cannot open file" << std::endl;
      }
      for(xDb=0;xDb<numDofTot;xDb++){
  // 	    for(yDb=0;yDb<period;yDb++)
  // 	    {
          in2b>>dataIRMtmp[xDb][0];
       //   std::cout << "dataIRMtmp["<<xDb<<"]["<<0 <<"] = " << dataIRMtmp[xDb][0] << std::endl;
          //if ( xDb == 34000  ) { std::cout << "numbDofTot is" <<numDofTot<< std::endl; abort();}
  // 	    }
      }
      in2b.close();

      double dt=m_fstransient->timeStep;
      double coeffV=0;
      MatCreateDense(PETSC_COMM_WORLD,PETSC_DECIDE,PETSC_DECIDE,numDofTot,period,FELISCE_PETSC_NULLPTR,&dataIRM);
      for(int i=0;i<numDofTot;i++)
      {
        for(int j=0;j<period;j++)
        {
    //Commented temporarily by D. for the special test  
      coeffV=0.5*(1-std::cos(2*M_PI/4*(j+1)*dt));
        //  coeffV=0.5*(1-std::cos(2*M_PI/4*(j+1)*dt));
  // 	      std::cout << "coeffV = " << coeffV << std::endl;
  // 		  std::cout << "cos = " << coeffV << std::endl;
     //Commented temporarily by D. for the special test      
    coeffV=coeffV*dataIRMtmp[i][0];
     //Added temporarily by D. for the special test           coeffV=dataIRMtmp[i][0];
//if(coeffV==NAN) {std::cout<<"i is "<<i<<std::endl; abort(); }
  // 		  std::cout << "dataIRMtmp[i][0] = " << dataIRMtmp[i][0] << std::endl;
  // 		  std::cout << "coeffV = " << coeffV << std::endl;
             // if (i==0 && j==0) {std::cout<<"Coucou Inside Initialize Lung Vector 3"<<std::endl; abort();}
            MatSetValue(dataIRM,i,j,coeffV,INSERT_VALUES);
             /*******
              PetscInt lo, hi;
              MatGetOwnershipRange(dataIRM, &lo, &hi);
              assert(lo <= i && i < hi);
              PetscInt row_as_array[1];
              row_as_array[0] = i;
              PetscInt col_as_array[1];
              col_as_array[0] = j;

               double ret_array[1];

               MatGetValues(dataIRM, 1, row_as_array, 1, col_as_array, ret_array);
               std::cout << "dataIRM["<< i<<"]["<<j<<"]= " << ret_array[0] << std::endl;
               *****/

  // 	      MatSetValue(dataIRM,i,j,coeffV*dataIRMtmp[i][j],INSERT_VALUES);
        }
      }
      //std::cout<<"Coucou Inside Initialize Lung Vector 3"<<std::endl; abort();
      MatAssemblyBegin(dataIRM,MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(dataIRM,MAT_FINAL_ASSEMBLY);
    }


         // VecView(VecIRM,PETSC_VIEWER_STDOUT_WORLD);



    if(modeLung=="pressureRealistic"){
      //creation of dataDispl matrix
      double valTmp;
      double dataDispltmp[numDofTot][period];
      int xD,yD;
      std::ifstream in3("solutionDisplacement.txt");
      if(!in3){
        std::cout << "cannot open file" << std::endl;
      }
      for(xD=0;xD<numDofTot;xD++){
        for(yD=0;yD<period;yD++)
        {
          in3>>dataDispltmp[xD][yD];
        }
      }
      in3.close();
      MatCreateDense(PETSC_COMM_WORLD,PETSC_DECIDE,PETSC_DECIDE,numDofTot,period,FELISCE_PETSC_NULLPTR,&dataDispl);
      for(int i=0;i<numDofTot;i++)
      {
        for(int j=0;j<period;j++)
        {
          MatSetValue(dataDispl,i,j,dataDispltmp[i][j],INSERT_VALUES);
        }
      }


      double dataNormaltmp[numDofTot][1];
      std::ifstream in33("dataNormal.txt");
      if(!in33){
        std::cout << "cannot open file" << std::endl;
      }
      for(xD=0;xD<numDofTot;xD++){
          in33>>dataNormaltmp[xD][0];
  // 	      std::cout <<dataNormaltmp[xD][0]<< std::endl;
      }
      in33.close();
      //CAUTION : the time step is constant along the simulation and specified in the data file
      VecCreate(PETSC_COMM_WORLD,&vecNormal);
      VecSetSizes(vecNormal,PETSC_DECIDE,numDofTot);
      VecSetFromOptions(vecNormal);
      for(int i=0;i<numDofTot;i++)
      {
          VecSetValue(vecNormal,i,dataNormaltmp[i][0],INSERT_VALUES);
      }
      VecAssemblyBegin(vecNormal);
      VecAssemblyEnd(vecNormal);

    }


   // InitialNonlinearLungProblem.kspT=m_linearProblem[0]->ksp();     //useless - no KSP used for lung simulation //commented by D.

    //list Exits
    QStringList listExTmp;
    QFile inputFile("listExit.txt");
    if (inputFile.open(QIODevice::ReadOnly))
    {
      QTextStream in(&inputFile);
      while (!in.atEnd())
      {
          QString line = in.readLine();
          listExTmp << line;
      }
      inputFile.close();
    }
    //std::cout<<"Coucou Inside Initialize Lung Vector 4"<<std::endl;
    //abort();

    //volumes : will contain volume evolution. Output of the simulation.
    PetscVector volTmp;
    volTmp.createMPI(PETSC_COMM_WORLD,PETSC_DECIDE,nbRegions);
    volTmp.zeroEntries();
    volTmp.assembly();
    /*****
    InitialNonlinearLungProblem.volumeEvol.duplicateFrom(volTmp);
    InitialNonlinearLungProblem.volumeEvolTmp.duplicateFrom(volTmp);
    InitialNonlinearLungProblem.volumeEvolPrevious.duplicateFrom(volTmp);
    InitialNonlinearLungProblem.volumeReg.duplicateFrom(volTmp);
    InitialNonlinearLungProblem.emphysemaCoeff.duplicateFrom(volTmp);
    InitialNonlinearLungProblem.flows.duplicateFrom(volTmp);
    InitialNonlinearLungProblem.pCouplingB.duplicateFrom(volTmp);
    *****/
    volumes(nbTimeSteps, nbRegions);
    volTmp.destroy();

  /****
    VecDuplicate(InitialNonlinearLungProblem.rhsTot.toPetsc(),&disIRMprev);
    VecDuplicate(InitialNonlinearLungProblem.rhsTot.toPetsc(),&velIRMprev);
    VecDuplicate(InitialNonlinearLungProblem.rhsTot.toPetsc(),&disIRMcurrent);
    VecDuplicate(InitialNonlinearLungProblem.rhsTot.toPetsc(),&velIRMcurrent);
    VecDuplicate(InitialNonlinearLungProblem.rhsTot.toPetsc(),&sumDisIRM);
    VecDuplicate(InitialNonlinearLungProblem.rhsTot.toPetsc(),&deltaVelIRM);
    ***/
    /* Commente by D.
    VecZeroEntries(disIRMprev);
    VecZeroEntries(velIRMprev);
    VecZeroEntries(disIRMcurrent);
    VecZeroEntries(velIRMcurrent);
    VecZeroEntries(sumDisIRM);
    VecZeroEntries(deltaVelIRM);
   */


    PetscVector pElastBb;
    pElastBb.create(PETSC_COMM_WORLD);
    pElastBb.setSizes(PETSC_DECIDE,numDofTot);
    pElastBb.setFromOptions();
    for(int i=0;i<numDofTot;i++)
    {
        pElastBb.setValue(i,0,INSERT_VALUES);
    }
    pElastBb.assembly();
    //InitialNonlinearLungProblem.pElastB.duplicateFrom(pElastBb); //Commented by D.
    //InitialNonlinearLungProblem.localExpansion.duplicateFrom(pElastBb); //Commented by D.
    pElastBb.destroy();

    //InitialNonlinearLungProblem.volumeB.duplicateFrom(InitialNonlinearLungProblem.pElastB); //Commented by D.

    std::cout<<"INITIALIZATION VECTORS: DONE"<< std::endl;
  }

template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
void NonlinearLungModel<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT>::initializeTreeComponent(){

 int nbRegions;
 nbRegions=FelisceParam::instance().nbSubRegions;

 QString nameInputFile;
 nameInputFile="inputTree.par";
 inParam = new MGInputParameters(nameInputFile);
 QString modeBuildTree=inParam->getModelBuildTree();
 QString bbbgas="bof";
 MGGas * gas = new MGGas( bbbgas, inParam->getGasTemperature(), inParam->getGasDensity(), inParam->getGasViscosity(), inParam->getGasMFP(), inParam->getLaryngealCoeff() );
 gas->publishProperties( std::cout );
 density = gas->getDensity();
 dynVisc  = gas->getDynViscosity();

 //build listObjectExits for tree description
// 	resMat * resMatObj;
 resMat * resMatObj=new resMat();
 lungTree=resMatObj->buildNeeds(nameInputFile);

 MGSegment * trachea;
 trachea=lungTree->getRoot();
 QMap<QString,MGSegment*> listTrachea;
 listTrachea.insert("T",trachea);

 //build list of exits from txt file
 QFile inputFile("listExit.txt");
 if (inputFile.open(QIODevice::ReadOnly))
 {
   QTextStream in(&inputFile);
   while (!in.atEnd())
   {
       QString line = in.readLine();
       listExitBranches << line;
   }
   inputFile.close();
 }

 // disease properties
 QString branchDisease=inParam->getBranchDisease();
 branchDis=branchDisease;
 QString lobeAsthma=inParam->getLobeAsthma();
 lobeDis=lobeAsthma;
 double diseaseRatio=inParam->getDiseaseRatio();
 QString lobeEmphysema=inParam->getLobeEmphysema();
 lobeEmph=lobeEmphysema;
 emphysemaRatio=inParam->getEmphysemaRatio();


 int nbGen=inParam->getGenerationStenosis();
 std::ostringstream sstreamN;
 sstreamN << nbGen;
 std::string nbGenS = sstreamN.str();
 nbGeneration= QString::fromStdString(nbGenS);

 std::ostringstream sstreamE;
 sstreamE << emphysemaRatio;
 std::string emphysemaRatS = sstreamE.str();
 emphysemaRat= QString::fromStdString(emphysemaRatS);

 std::ostringstream sstreamS;
 sstreamS << diseaseRatio;
 std::string diseaseRatS = sstreamS.str();
 diseaseRat = QString::fromStdString(diseaseRatS);


 QString motherID=inParam->getMotherID();
     QTextStream(stdout) << motherID << std::endl;

// 	MGSegment * motherBranch;
// 	MGSegment * motherBranch=resMatObj->getBranch(motherID,trachea);
 motherBranch=resMatObj->getBranch(motherID,trachea);

 QString nameTest=motherBranch->getSegID();
// 	QTextStream(stdout) << nameTest << std::endl;


 listObjectExits=resMatObj->buildList(modeBuildTree, listExitBranches, motherBranch,branchDisease, lobeAsthma,nbGen,diseaseRatio,lobeEmphysema,lungTree, inParam);
// 	listObjectExits=resMatObj->buildList(modeBuildTree, listExitBranches, trachea,branchDisease, lobeAsthma,diseaseRatio,lobeEmphysema);

 emphysemaCoeff.createMPI(PETSC_COMM_WORLD,PETSC_DECIDE,nbRegions);
 emphysemaCoeff.zeroEntries();


 for(int i=0;i<nbRegions;i++){

   if (listExitBranches[i].startsWith(lobeEmphysema)){
      //HyperelasticNonlinearLungProblem.emphysemaCoeff.setValue(i,emphysemaRatio,INSERT_VALUES);
         emphysemaCoeff.setValue(i,emphysemaRatio,INSERT_VALUES);
   }
   else
   {
     // HyperelasticNonlinearLungProblem.emphysemaCoeff.setValue(i,1,INSERT_VALUES);
       emphysemaCoeff.setValue(i,1,INSERT_VALUES);
   }
 }
// std::cout<<"Inside IntializeTree 1"<<std::endl;
 //abort();
emphysemaCoeff.assembly();

 //list Exits
 QStringList listExTmp;
 QFile inputFileB("listExit.txt");
 if (inputFileB.open(QIODevice::ReadOnly))
 {
   QTextStream in(&inputFileB);
   while (!in.atEnd())
   {
       QString line = in.readLine();
       listExTmp << line;
   }
   inputFileB.close();
 }

 QString gasName;
 if (density<0.5) {
   gasName="HeO2";
 }
 else{
   gasName="Air";
 }

 QString resistanceMode= inParam->getResistanceMode();

 std::ofstream volumeEvolution;
 QString nameFadd;
 if(modeBuildTree=="stenosis"){
   nameFadd= "results/Volume_" + modeBuildTree+branchDis+ "_" +diseaseRat + "_" + resistanceMode + "_" + gasName + ".txt";
 }
 if(modeBuildTree=="generationalStenosis"){
   nameFadd= "results/Volume_" + modeBuildTree+nbGeneration+ "_" +diseaseRat + "_" + resistanceMode + "_" + gasName + ".txt";
 }
 if(modeBuildTree=="asthma"){
   nameFadd= "results/Volume_" + modeBuildTree+lobeDis +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName + ".txt";
 }
 if(modeBuildTree=="healthy"){
   if(emphysemaRatio==1){
     nameFadd= "results/Volume_" + modeBuildTree + "_" + resistanceMode + "_" + gasName + ".txt";
   }
   else{
     nameFadd= "results/Volume_MechaDis" +lobeEmph+"_" + emphysemaRat + "_" + resistanceMode + "_" + gasName + ".txt";
   }
 }
 std::string nameStr = nameFadd.toUtf8().constData();
 const char * nameCh=nameStr.c_str();
 volumeEvolution.open(nameCh,ios::out | std::ios::app);
// 	volumeEvolution.open("volumeEvolution.txt",ios::out);
 volumeEvolution<<"time";
 for(int i=0;i<nbRegions;i++)
 {
   QString ex_i= listExTmp[i];
   std::string ex_iS = ex_i.toStdString();
// 	  volumeEvolution<<" V";
// 	  volumeEvolution<<i;

   volumeEvolution<<" ";
   volumeEvolution<<ex_iS;

 }
 volumeEvolution<<std::endl;
 volumeEvolution.close();


 std::ofstream pleuralEvolution;      // .csv file, will contain pleural pressure to feed the RC model with in order to recover the same evolution thant from the mechanical model
 nameFadd;
 nameFadd= "results/pleural_" + modeBuildTree + "_" + resistanceMode + "_" + gasName + ".csv";
 nameStr = nameFadd.toUtf8().constData();
 nameCh=nameStr.c_str();
 pleuralEvolution.open(nameCh,ios::out | std::ios::app);
 pleuralEvolution<<"time";
 for(int i=0;i<nbRegions;i++)
 {
   QString ex_i= listExTmp[i];
   std::string ex_iS = ex_i.toStdString();
   pleuralEvolution<<";";   //CSV format
   pleuralEvolution<<ex_iS;

 }
 pleuralEvolution<<std::endl;
 pleuralEvolution.close();


 std::ofstream effortEvolution;      // .csv file, will contain pleural pressure to feed the RC model with in order to recover the same evolution thant from the mechanical model
 nameFadd;
 if(modeBuildTree=="stenosis"){
   nameFadd= "results/effort_" + modeBuildTree+branchDis +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName + ".txt";
 }
 if(modeBuildTree=="generationalStenosis"){
   nameFadd= "results/effort_" + modeBuildTree+nbGeneration +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName + ".txt";
 }
 if(modeBuildTree=="asthma"){
   nameFadd= "results/effort_" + modeBuildTree+lobeDis +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName + ".txt";
 }
 if(modeBuildTree=="healthy"){
   if(emphysemaRatio==1){
     nameFadd= "results/effort_" + modeBuildTree + "_" + resistanceMode + "_" + gasName + ".txt";
   }
   else{
     nameFadd= "results/effort_MechaDis" +lobeEmph+"_" +emphysemaRat+ "_" + resistanceMode + "_" + gasName + ".txt";
   }
 }
 nameStr = nameFadd.toUtf8().constData();
 nameCh=nameStr.c_str();
 effortEvolution.open(nameCh,ios::out | std::ios::app);
 effortEvolution<<"time";
 effortEvolution<<" ";
 effortEvolution<<"pElast";
 effortEvolution<<" ";
 effortEvolution<<"pTree";
 effortEvolution<<" ";
 effortEvolution<<"effort";
 effortEvolution<<std::endl;
 effortEvolution.close();


 QString nbRegStr = QString::number(nbRegions);
 std::ofstream volumeRegions;      // .csv file, will contain pleural pressure to feed the RC model with in order to recover the same evolution thant from the mechanical model
 nameFadd= "results/volReg_" + nbRegStr + "Regions" +".txt";
 nameStr = nameFadd.toUtf8().constData();
 nameCh=nameStr.c_str();
 volumeRegions.open(nameCh,ios::out | std::ios::app);
 volumeRegions<<"time";
 for(int i=0;i<nbRegions;i++)
 {
   QString ex_i= listExTmp[i];
   std::string ex_iS = ex_i.toStdString();
   volumeRegions<<" ";
   volumeRegions<<ex_iS;
 }
 volumeRegions<<std::endl;
 volumeRegions.close();


 std::ofstream regCompliance;      // .txt file, will contain pleural pressure to feed the RC model with in order to recover the same evolution thant from the mechanical model
 nameFadd= "results/complianceReg_" + nbRegStr + "Regions" +".txt";
 nameStr = nameFadd.toUtf8().constData();
 nameCh=nameStr.c_str();
 regCompliance.open(nameCh,ios::out | std::ios::app);

 for(int i=0;i<nbRegions;i++)
 {
   QString ex_i= listExTmp[i];
   std::string ex_iS = ex_i.toStdString();
   regCompliance<<ex_iS;
   regCompliance<<" ";
 }

 regCompliance<<std::endl;
 regCompliance.close();



 std::ofstream volumeEvolutionLobar;
 if(modeBuildTree=="stenosis"){
   nameFadd= "results/VolumeLobar__" + modeBuildTree+branchDis +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName +".txt";
 }
 if(modeBuildTree=="generationalStenosis"){
   nameFadd= "results/VolumeLobar__" + modeBuildTree+nbGeneration +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName +".txt";
 }
 if(modeBuildTree=="asthma"){
   nameFadd= "results/VolumeLobar__" + modeBuildTree+lobeDis +"_" +diseaseRat + "_" + resistanceMode + "_" + gasName +".txt";
 }
 if(modeBuildTree=="healthy"){
   if(emphysemaRatio==1){
     nameFadd= "results/VolumeLobar__" + modeBuildTree + "_" + resistanceMode + "_" + gasName + ".txt";
   }
   else{
     nameFadd= "results/VolumeLobar__MechaDis" +lobeEmph+"_" +emphysemaRat+ "_" + resistanceMode + "_" + gasName + ".txt";
   }
 }
 nameStr = nameFadd.toUtf8().constData();
 nameCh=nameStr.c_str();
 volumeEvolutionLobar.open(nameCh,ios::out | std::ios::app);
// 	volumeEvolutionLobar.open("volumeEvolutionLobar.txt",ios::out);
 volumeEvolutionLobar<<"time";
 volumeEvolutionLobar<<" LU LL RU RM RL";
 volumeEvolutionLobar<<std::endl;
 volumeEvolutionLobar.close();

 if(modeLung=="pressureRealistic"){

   QString nbRegStr = QString::number(nbRegions);
   std::ofstream pAppRegions;      // .csv file, will contain pleural pressure to feed the RC model with in order to recover the same evolution thant from the mechanical model
   nameFadd= "results/pApp_REG_" + modeBuildTree + "_" + resistanceMode+"_"+gasName+".csv";
   nameStr = nameFadd.toUtf8().constData();
   nameCh=nameStr.c_str();
   pAppRegions.open(nameCh,ios::out | std::ios::app);
   pAppRegions<<"time";
   for(int i=0;i<nbRegions;i++)
   {
     QString ex_i= listExTmp[i];
     std::string ex_iS = ex_i.toStdString();
     pAppRegions<<";";
     pAppRegions<<ex_iS;
   }
   pAppRegions<<std::endl;
   pAppRegions.close();
 }

 delete gas;
 delete resMatObj;

 std::cout<<"INITIALIZATION TREE : DONE"<< std::endl;
}

}

#endif
