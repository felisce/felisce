//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/elasticCurvedBeamModel.hpp"

namespace felisce
{
  ElasticCurvedBeamModel::ElasticCurvedBeamModel():Model()
  {
    m_name = "Elastic Curved Beam Model";
  }

  ElasticCurvedBeamModel::~ElasticCurvedBeamModel()
  {
    m_dispTimeRHS.destroy();
    m_seqDispTimeRHS.destroy();
  }

  void ElasticCurvedBeamModel::initializeDerivedModel()
  {

    // todo : initialize solution for solver.
    // _linearProblem[0]->set_U_0(_U_0);

    // First assembling loop in iteration 0 to build static matrix.
    //_linearProblem[iProblem]->assembleMatrixRHS(_rankProc);

    // save static matrix in matrix _A.
    //_linearProblem[iProblem]->copyMatrixRHS();
  }


  void ElasticCurvedBeamModel::forward()
  {
    // Write solution for postprocessing (if required)
    writeSolution();

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();

    //Assembly loop on elements.
    m_linearProblem[0]->assembleMatrixRHSBD(MpiInfo::rankProc());

    //Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();

    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    m_dispTimeRHS.copyFrom(m_linearProblem[0]->solution());


    //Solve linear system.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    // computation of the time RHS
    // 2 * X^n - X^(n-1)
    m_dispTimeRHS.axpby(2., -1., m_linearProblem[0]->solution());

    m_linearProblem[0]->gatherSolution();
    m_linearProblem[0]->gatherVector(m_dispTimeRHS, m_seqDispTimeRHS);
  }

  void ElasticCurvedBeamModel::setExternalVec(){
    m_dispTimeRHS.duplicateFrom(m_linearProblem[0]->solution());
    m_dispTimeRHS.set(0.);
    m_seqDispTimeRHS.duplicateFrom(m_linearProblem[0]->sequentialSolution());
    m_seqDispTimeRHS.set(0.);

    m_linearProblem[0]->pushBackExternalVec(m_seqDispTimeRHS);
  }
}
