//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. Kazerani
//

#ifndef __FELISCE_NONLINEARLUNG_MODEL_HPP__
#define __FELISCE_NONLINEARLUNG_MODEL_HPP__

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Model/model.hpp"
#include "Solver/NonlinearProblemLung.hpp"
#include "Solver/NonlinearProblemLung.cpp"
#include "MGLungTree.h"
#include "MGLungFactory.h"
#include "MGFlowData.h"
#include "MGSegment.h"
#include "morphoXMLManager.h"

namespace felisce 
{
  /*!
   * \brief Class in charge of hyperelasticity model. Model to manage non linear elasticity/ bronchial tree for lung.
   * \tparam HyperElasticityLawT The definition of the hyperelasticity laws actually used. This information will be useful to
   * #LinearProblemHyperElasticity.
   * \tparam TimeSchemeT Approximation used to handle non-linear term.
   * \tparam IsIncompressibleT Whether we consider the problem incompressible or not.
   */
  template <class HyperElasticityLawT, HyperelasticityNS::TimeScheme TimeSchemeT, HyperelasticityNS::Incompressible::Value IsIncompressibleT>
  class NonlinearLungModel : public Model {
  public:

    typedef NonlinearProblemLung<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT> HyperelasticNonlinearLungProblem;

    ///  Constructor.
    explicit NonlinearLungModel();

    ///  Destructor.
    ~NonlinearLungModel();

      ///  Function to read the data file associated to the bronchial tree and initialize all attributes linked to the tree
     virtual void initializeTreeComponent();
      //Added by d. Kaz taken from Nico's userLungModel.hpp
      ///  Function to initialize attributes of the linearProblemLung object reading IRM files to create the initial displacement and do other things.
      virtual void initializeLungVectors();

 //writes volume region evolution in a file (added by D. inspired by Nico's forward function
   void WriteVolReg();

    ///  Solve the problem with the initial conditions.
    void SolveStaticProblem();

    /*!
     * \brief Solve the dynamic problem.
     *
     * A call to this method performs all the work, including initial call to solve the static problem.
     */
    void SolveDynamicProblem();


  private:

    ///  Update time. First matrix in LinearProblem::Matrix() is not cleared here, contrary to method in parent class.
    virtual void updateTime(const FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);


  public:
    ///  Manage time iteration (Newton inside).
    virtual void forward();
       //comes from Nico's code (by D. Kaz)
     MGInputParameters * inParam;   //parameters read from inout.par
     //for gas
     double density;
     double dynVisc;
     MGLungTree * lungTree;
     QMap<QString,MGSegment*> listObjectExits;  //keys are the ID of the segments
     QStringList listExitBranches;
     MGSegment * motherBranch;

     QString branchDis; //name of the eventual diseased branch
     QString lobeDis; //name of the eventual diseased region (region fed by this branch)
     QString lobeEmph; //name of the eventual diseased region (region fed by this branch)
     QString diseaseRat; //disease ratio (std::string)
     QString emphysemaRat; //disease ratio (std::string)
     QString nbGeneration; //stenosed generation is the generationalStenosis mode

     double emphysemaRatio;

     PetscVector emphysemaCoeff;    // ratio for Young modulus variation in case we want to simulate emphysema (for now only E is affectedn not the poisson ration)
     //HyperelasticNonlinearLungProblem InitialNonlinearLungProblem;//Added and commented by D.
     UBlasMatrix volumes;   //will contain volume evolution of each terminal region
     std::string modeLung;
     int nbGen;
     int flagJacobi;   //0 if diago is computed at every time step, 1 if we only use the first time step diago to perform the precondtionning at all times

     int period;		// number of time steps in one period
     Vec dataPressure;  //in case a homogneous pressure is chosen as BC, this std::vector will contain the pressure evolution. Valid for pressure and mechanicalVentilation modes


     Vec vecNormal;    //pressureRealistic mode, contains the the std::vector normal to the surface mesh and 0 at internal nodes (outside normalized normal)
     double valPapp;

     //for displacement mode
     Mat dataIRM;	//contains the dataIRM.txt file
     Vec disIRMprev;	//surface displacement field at previous time step
     Vec velIRMprev;	//surface velocity field at previous time step
     //Vec disIRMcurrent;  //surface displacement field at current time step
     Vec velIRMcurrent;  //surface velocity field at current time step
     Vec sumDisIRM;	//intermediate for calculations
     Vec deltaVelIRM;    //intermediate for calculations

     //for pressureRealistic mode
     Mat dataDispl;      //contains the solutionDisplacement.txt file : whole domain displacement over time - from the surface BC healthy simulation
     Vec displacementPrevious;  // displacement field at previous time step
     Vec displacementPrevious2; // intermediate for calculations
     Vec displacementCurrent;   // displacement field at current time step
     Vec velocityPrevious;      // velocity field at previous time step
     Vec velocityCurrent;       // velocity field at current time step

     //for mechanical ventilation mode
     PetscVector volCurr;
     PetscVector volCurrTmp;
     PetscVector volCurrStep;
     PetscVector volPrev;
     PetscVector volFRC;
     PetscVector volTLC;
     int nbLobes;
     //Added by D. but it seems not to be needed since the initial condition is constructed in the initializeLinearProblem of the class model.
    // NonlinearProblemLung<HyperElasticityLawT, TimeSchemeT, IsIncompressibleT> InitialNonlinearLungProblem;
  };
} // namespace felisce
# include "NonlinearLungModel.tpp"
#endif
