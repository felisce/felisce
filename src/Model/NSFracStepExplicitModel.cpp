//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/NSFracStepExplicitModel.hpp"
#include "Solver/linearProblemNSFracStepProj.hpp"

namespace felisce {
  NSFracStepExplicitModel::NSFracStepExplicitModel():Model() {
    m_lumpedModelBC = nullptr;
    m_name = "NSFracStepExplicit";
  }


  NSFracStepExplicitModel::~NSFracStepExplicitModel() {
    if(m_lumpedModelBC) delete m_lumpedModelBC;
    m_solExtrapolate.destroy();
  }


  void NSFracStepExplicitModel::initializeDerivedModel() {
    if( FelisceParam::instance().lumpedModelBCLabel.size() ) {
      m_lumpedModelBC = new LumpedModelBC(m_fstransient);
      static_cast<LinearProblemNSFracStepProj*>(m_linearProblem[2])->initializeLumpedModelBC(m_lumpedModelBC);
    }

    //if (FelisceParam::instance().orderBdfFS > 2)
    //  FEL_ERROR("BDF not yet implemented for order greater than 2 with fractional step model.");
    if (FelisceParam::instance().orderBdfFS > 1)
      FEL_ERROR("BDF not yet implemented for explicit fractional step model.");

    m_bdfFS.defineOrder(FelisceParam::instance().orderBdfFS);
    m_linearProblem[0]->initializeTimeScheme(&m_bdfFS);
    m_linearProblem[1]->initializeTimeScheme(&m_bdfFS);
  }

  // std::set external vectors for linear problems
  // (to be used in assembling routines)
  void NSFracStepExplicitModel::setExternalVec() {
    // problem 0:
    // advection  -> rho/tau*(w - un) + un*grad(w) + stab = 0
    // problem 1:
    // diffusion  -> rho/tau(u,v) + (eps(u),eps(v)) = rho/tau(w,v) - k(grad pn,v)
    // problem 2:
    // projection -> (grad p,grad q) = -rho/tau (div(u),q)
    if (FelisceParam::instance().explicitAdvection) {
      // explicit advection needs: u_diffusion, pressure
      m_linearProblem[0]->pushBackExternalVec(m_linearProblem[1]->sequentialSolution());
      m_linearProblem[0]->pushBackExternalAO(m_linearProblem[1]->ao());
      m_linearProblem[0]->pushBackExternalDof(m_linearProblem[1]->dof());

      // diffusion needs: u_advection, pressure (optional)
      m_linearProblem[1]->pushBackExternalVec(m_linearProblem[0]->sequentialSolution());
      m_linearProblem[1]->pushBackExternalAO(m_linearProblem[0]->ao());
      m_linearProblem[1]->pushBackExternalDof(m_linearProblem[0]->dof());

      m_linearProblem[1]->pushBackExternalVec(m_linearProblem[2]->sequentialSolution());
      m_linearProblem[1]->pushBackExternalAO(m_linearProblem[2]->ao());
      m_linearProblem[1]->pushBackExternalDof(m_linearProblem[2]->dof());

      // projection needs: u_diffusion
      m_linearProblem[2]->pushBackExternalVec(m_linearProblem[1]->sequentialSolution());
      m_linearProblem[2]->pushBackExternalAO(m_linearProblem[1]->ao());
      m_linearProblem[2]->pushBackExternalDof(m_linearProblem[1]->dof());

    } else {
      FEL_ERROR(" This model should be used only with explicitAdvection=true");
    }
  }

  // ---
  // for each problem: assemble matrix and RHS
  // first: assemble static matrix (at iteration 0)
  void NSFracStepExplicitModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    std::cout << " [*] preAssemblingMatrixRHS for problem " << iProblem << " [*] " << std::endl;
    m_solExtrapolate.duplicateFrom(m_linearProblem[0]->vector());
    m_solExtrapolate.zeroEntries();

    // todo : initialize solution for solver.
    // m_linearProblem[0]->set_U_0(_U_0);
    std::cout << " ... assembleMatrixRHS ... " << std::endl;
    // First assembling loop in iteration 0 to build static matrix.
    m_linearProblem[iProblem]->assembleMatrixRHS(MpiInfo::rankProc());
    // save static matrix in matrix _A.
    std::cout << " ... copyMatrixRHS ... " << std::endl;
    m_linearProblem[iProblem]->copyMatrixRHS();
  }

  // first: add dynamic matrix (assembled at each step)
  void NSFracStepExplicitModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    std::cout << " [*] preAssemblingMatrixRHS for problem " << iProblem << " [*] " << std::endl;
    // _Matrix = _A + _Matrix (add static matrix to the dynamic matrix to build
    // complete matrix of the system
    m_linearProblem[iProblem]->addMatrixRHS();
  }

  // advancing
  void NSFracStepExplicitModel::forward() {
    // Write solution for postprocessing (if required)
    writeSolution();
    // Advance time step.
    updateTime();
    // Print time information
    printNewTimeIterationBanner();

    for(std::size_t iLinPb=0; iLinPb<m_linearProblem.size(); iLinPb++) {
      if (FelisceParam::verbose())
        PetscPrintf(MpiInfo::petscComm(), "NSFracStepExplicit: linear problem #%ld \n",static_cast<unsigned long>(iLinPb));

      // initialize BDF for advection
      if ( iLinPb==0 ) {
        if ( m_fstransient->iteration == 1 ) {
          // Initialize of bdf solver.
          m_bdfFS.initialize(m_linearProblem[0]->solution());
          // Compute the extrapolate.
          m_bdfFS.extrapolate(m_solExtrapolate);
          // Initialize it.
          m_linearProblem[0]->initExtrapol(m_solExtrapolate);
        } else {
          // Update bdf.
          m_bdfFS.update(m_linearProblem[0]->solution());
          // Compute the extrapolate.
          m_bdfFS.extrapolate(m_solExtrapolate);
          // Initialize it.
          m_linearProblem[0]->updateExtrapol(m_solExtrapolate);
        }
      }

      // initialize BDF for diffusion
      if (iLinPb==1 ) {
        if ( m_fstransient->iteration == 1 ) {
          // Initialize of bdf solver.
          m_bdfFS.initialize(m_linearProblem[1]->solution());
          // Compute the extrapolate.
          m_bdfFS.extrapolate(m_solExtrapolate);
          // Initialize it.
          m_linearProblem[1]->initExtrapol(m_solExtrapolate);
        } else {
          // Update bdf.
          m_bdfFS.update(m_linearProblem[1]->solution());
          // Compute the extrapolate.
          m_bdfFS.extrapolate(m_solExtrapolate);
          // Initialize it.
          m_linearProblem[1]->updateExtrapol(m_solExtrapolate);
        }
      }

      // last linear problem (projection)
      // lumpedModelBC bc computation
      if (m_lumpedModelBC && iLinPb==2) {
        if ( m_fstransient->iteration == 1 ) {
          // m_labelFluxListLumpedModelBC computation with LumpedModelBC labels
          m_labelFluxListLumpedModelBC.clear();
          for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
            m_labelFluxListLumpedModelBC.push_back(FelisceParam::instance().lumpedModelBCLabel[i]);
          }
        }
        // update the fluxes
        // justine's warning (10-09-12) : with a velocity with divergence not necesary free... ?
        m_linearProblem[0]->computeMeanQuantity(velocity,m_labelFluxListLumpedModelBC,m_fluxLumpedModelBC);
        for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
          m_lumpedModelBC->Q(i) = m_fluxLumpedModelBC[i] ;
        }
        // iterate the lumpedModelBC parameters
        m_lumpedModelBC->iterate();
      }
      // Compute the RHS linked to the time scheme (bdf).
      std::cout << " ... computeRHSTime - problem " << iLinPb << std::endl;
      m_bdfFS.computeRHSTime(m_fstransient->timeStep);
      // Fill m_seqBdfRHS.
      std::cout << " ... gatherVectorBeforeAssembleMatrixRHS - problem " << iLinPb << std::endl;
      m_linearProblem[iLinPb]->gatherVectorBeforeAssembleMatrixRHS();
      //Assembly loop on elements.
      std::cout << " ... assembleMatrixRHS() - problem " << iLinPb << std::endl;
      m_linearProblem[iLinPb]->assembleMatrixRHS(MpiInfo::rankProc());
      std::cout << " end assembling " << std::endl;

      //Specific operations before solve the system.
      postAssemblingMatrixRHS(iLinPb);
      //Apply essential transient boundary conditions.
      m_linearProblem[iLinPb]->finalizeEssBCTransient();
      // Apply all the boundary conditions.
      m_linearProblem[iLinPb]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
      if (iLinPb==0) {
        if (m_fstransient->iteration==2) {
          std::cout << " ipb = " << iLinPb << std::endl;
          m_linearProblem[iLinPb]->writeMatrixForMatlab(21);
          m_linearProblem[iLinPb]->writeRHSForMatlab(21);
          //exit(1);
        }
      }
      //Solve linear system.
      std::cout << " [*] solve linearProblem " << iLinPb << "[*]" << std::endl;
      m_linearProblem[iLinPb]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
      m_linearProblem[iLinPb]->gatherSolution();

    }
  }

  int NSFracStepExplicitModel::getNstate() const {
    return m_linearProblem[0]->numDof();
  }

  void NSFracStepExplicitModel::getState(double* & state) {
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }

  felInt NSFracStepExplicitModel::numDof_swig() {
    return m_linearProblem[0]->numDof();
  }

  void NSFracStepExplicitModel::getState_swig(double* data, felInt numDof) {
    double * state;
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
    for (felInt i=0 ;  i<numDof ; i++) {
      data[i] = state[i];
    }
  }

  void NSFracStepExplicitModel::setState(double* & state) {
    m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }

}
