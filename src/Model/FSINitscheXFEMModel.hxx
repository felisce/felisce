//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges and M. A. Fernandez
//

// System includes

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce {
  template<class SolidPrb>
  FSINitscheXFEMModel<SolidPrb>::FSINitscheXFEMModel():Model()
  {
    m_name = "Nitsche XFEM FSI Model";
    m_isParFluidVelAllocated = false;
    m_isTimeSchemeFluidExtrapolAllocated = false;
    m_isStructureVelAllocated = false;
    m_isStructureVelOldAllocated = false;
    m_isStructureVelOldOldAllocated = false;
    m_isStructureVelOldStoppingCriteriaAllocated = false;
    m_isStructureVelExtrapolAllocated = false;
    m_isSeqStructureTimeRHSAllocated = false;
    m_isSeqStructureVelExtrapolAllocated = false;
    m_isStructureDispAllocated = false;
    m_isStructureDispOldAllocated = false;
    m_isStopVecAllocated = false;
    m_isStructureNitscheRHSTermsOldAllocated = false;
    m_isStructureTimeSchemeRHSTermsAllocated = false;
    m_isStructureExtrapolatedNitscheTermsRHSAllocated = false;

    m_elemFieldTimeSchemeType = NULL;
    m_timeSchemeType = 3;
    m_subIteration = 1;
    m_stoppingCriteriaTol = 1e-3;
    m_timeSchemeExtrapolOrder = 0;
    m_indexTime = 0;
    m_countToInitOldObjects = 0;
    m_reinitializeFluidBdf = false;
    m_feStruc = NULL;
    m_feVel = NULL;
    m_fePre = NULL;

    m_RNExtrapolOrder = 0;
    m_numStrucComp = 0;
  }

  template<class SolidPrb>
  FSINitscheXFEMModel<SolidPrb>::~FSINitscheXFEMModel()
  {
    if(m_isParFluidVelAllocated) {
      for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
        m_parFluidVel[i].destroy();
      m_parFluidVel.clear();
    }

    if(m_isStructureVelAllocated)
      m_structureVel.destroy();

    if(m_isStructureVelOldAllocated)
      m_structureVelOld.destroy();

    if(m_isStructureVelOldOldAllocated)
      m_structureVelOldOld.destroy();

    if(m_isStructureVelOldStoppingCriteriaAllocated)
      m_structureVelOldStoppingCriteria.destroy();

    if(m_isStructureVelExtrapolAllocated)
      m_structureVelExtrapol.destroy();

    if(m_isSeqStructureTimeRHSAllocated)
      m_seqStructureTimeRHS.destroy();

    if(m_isSeqStructureVelExtrapolAllocated)
      m_seqStructureVelExtrapol.destroy();

    if(m_isStructureDispOldAllocated)
      m_structureDispOld.destroy();

    if(m_isStructureNitscheRHSTermsOldAllocated)
      m_structureNitscheRHSTermsOld.destroy();

    if(m_isStructureTimeSchemeRHSTermsAllocated)
      m_structureTimeSchemeRHSTerms.destroy();

    if(m_isStructureExtrapolatedNitscheTermsRHSAllocated)
      m_structureExtrapolatedNitscheTermsRHS.destroy();

    if(m_isStopVecAllocated)
      m_stopVec.destroy();

    if(m_isTimeSchemeFluidExtrapolAllocated) {
      for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
        m_timeSchemeFluidExtrapol[i].destroy();
      m_timeSchemeFluidExtrapol.clear();
    }

    if(m_feStruc != NULL)
      delete m_feStruc;

    if(m_feVel != NULL)
      delete m_feVel;

    if(m_fePre != NULL)
      delete m_fePre;
  }

  /*!
   * Intersect the meshes and std::set the informations in the two solvers
   */
  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::initializeDerivedLinearProblem()
  {
    FEL_CHECK(m_linearProblem.size() == 2, "Need two solver for Nitsche XFEM FSI Model");

    // type conversion of the linear problems
    m_pLinFluid = dynamic_cast<LinearProblemFSINitscheXFEMFluid*>(m_linearProblem[0]);
    m_pLinStruc = dynamic_cast<LinearProblemFSINitscheXFEMSolid<SolidPrb>*>(m_linearProblem[1]);

    if(FelisceParam::instance().duplicateSupportDof) {
      FEL_CHECK(m_mesh.size() > 1, "Need at least 2 meshes: one for the fluid and one for the structure");

      // build the edges of the fluid mesh
      m_mesh[0]->buildEdges();

      // We intersect the two meshes here (because this function is called before computeDof)
      // The meshes can be found in the linear problem
      m_intersectMeshes.setMeshesName("fluidMesh", "strucMesh");
      m_intersectMeshes.setVerbose(FelisceParam::verbose());
      m_intersectMeshes.initAndIntersectMeshes(*m_mesh[0], *m_mesh[1]);

      // give access to the intersection to each linear problems
      m_pLinFluid->setDuplicateSupportObject(&m_intersectMeshes);
      m_pLinStruc->setDuplicateSupportObject(&m_intersectMeshes);

      // give direct access to the other linear problem
      m_pLinFluid->setStrucLinPrb(m_pLinStruc);
      m_pLinStruc->setFluidLinPrb(m_pLinFluid);

      // compute normals
      m_mesh[1]->computeNormalTangent();
      m_mesh[1]->computeElementNormal(m_mesh[1]->listElementNormals());
      m_mesh[1]->computeElementNormal(m_mesh[1]->listElementNormalsInit());
    }
  }

  /*!
   * Initialize time scheme
   */
  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::initializeDerivedModel()
  {
    // get the type of time discretization.
    m_elemFieldTimeSchemeType = FelisceParam::instance().elementFieldDynamicValue("TimeSchemeType");
    if(m_elemFieldTimeSchemeType != NULL) {
      m_timeSchemeType = (felInt) m_elemFieldTimeSchemeType->constant(Comp1);
      if (m_timeSchemeType == 0) {
        // Robin-Neumann scheme (direct implementation)
        // Robin-Neumann extrapolation order
        m_RNExtrapolOrder = (felInt) FelisceParam::instance().fsiRNscheme;
        m_timeSchemeExtrapolOrder = m_RNExtrapolOrder;

        // resize the sequential std::vector in m_pLinFluid and the parallel one here
        m_pLinFluid->initRNFluidExtrapol(m_timeSchemeExtrapolOrder);
        m_timeSchemeFluidExtrapol.resize(m_timeSchemeExtrapolOrder);

     } else if(m_timeSchemeType == 2) {
        // Robin-Neumann scheme a la Stenberg
        // Robin-Neumann extrapolation order
        m_RNExtrapolOrder = (felInt) FelisceParam::instance().fsiRNscheme;
        m_timeSchemeExtrapolOrder = m_RNExtrapolOrder;

        // resize the sequential std::vector in m_pLinFluid and the parallel one here
        m_pLinFluid->initRNFluidExtrapol(m_timeSchemeExtrapolOrder);
        m_timeSchemeFluidExtrapol.resize(m_timeSchemeExtrapolOrder);

        // resize the sequential std::vector in m_pLinStruc
        m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);

      } else if(m_timeSchemeType == 3) {
        // stabilized explicit
        // number of sub iteration (correction + 1)
        if(FelisceParam::instance().couplingSchemeStoppingCriteria == 0)
          m_subIteration = (felInt) m_elemFieldTimeSchemeType->constant(Comp2);
        else if(FelisceParam::instance().couplingSchemeStoppingCriteria == 1) {
          m_stoppingCriteriaTol = (double) m_elemFieldTimeSchemeType->constant(Comp2);
          m_stopVec.duplicateFrom(m_pLinStruc->solution());
          m_isStopVecAllocated = true;
        }

        // extrapolation order of the initial fluid solution in structure solver
        m_timeSchemeExtrapolOrder = (felInt) m_elemFieldTimeSchemeType->constant(Comp3);

        // resize the sequential std::vector in m_pLinStruc and the parallel one here
        m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);
        m_timeSchemeFluidExtrapol.resize(m_timeSchemeExtrapolOrder);

      } else if(m_timeSchemeType == 4) {
        // Robin-Neumann semi-implicit
        // Robin-Neumann extrapolation order
        m_RNExtrapolOrder = (felInt) FelisceParam::instance().fsiRNscheme;

        // number of sub iterations in the (fluid)-(solid/inertia) problem
        if(FelisceParam::instance().couplingSchemeStoppingCriteria == 0)
          m_subIteration = (felInt) m_elemFieldTimeSchemeType->constant(Comp2);
        else if(FelisceParam::instance().couplingSchemeStoppingCriteria == 1) {
          m_stoppingCriteriaTol = (double) m_elemFieldTimeSchemeType->constant(Comp2);
          m_stopVec.duplicateFrom(m_pLinStruc->solution());
          m_isStopVecAllocated = true;
        }

        // extrapolation order of the initial fluid solution in structure solver
        m_timeSchemeExtrapolOrder = (felInt) m_elemFieldTimeSchemeType->constant(Comp3);

        // resize the sequential std::vector in m_pLinStruc and the parallel one here
        m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);
        m_timeSchemeFluidExtrapol.resize(m_timeSchemeExtrapolOrder);
      }
    }

    if (FelisceParam::instance().orderBdfNS > 2)
      FEL_ERROR("BDF not yet implemented for order greater than 2 with FDFSI.");


    // bdf schemes
    // fluid
    m_bdfFluid.defineOrder(FelisceParam::instance().orderBdfNS);
    m_pLinFluid->initializeTimeScheme(&m_bdfFluid);


    // velocity of the structure
    // parallel vectors
    m_structureVel.duplicateFrom(m_pLinStruc->solution());
    m_structureVel.set( 0.);
    m_isStructureVelAllocated = true;

    m_structureVelOld.duplicateFrom(m_pLinStruc->solution());
    m_structureVelOld.set( 0.);
    m_isStructureVelOldAllocated = true;

    m_structureVelOldOld.duplicateFrom(m_pLinStruc->solution());
    m_structureVelOldOld.set( 0.);
    m_isStructureVelOldOldAllocated = true;

    if(FelisceParam::instance().couplingSchemeStoppingCriteria == 1) {
      m_structureVelOldStoppingCriteria.duplicateFrom(m_pLinStruc->solution());
      m_structureVelOldStoppingCriteria.set( 0.);
      m_isStructureVelOldStoppingCriteriaAllocated = true;
    }

    m_structureVelExtrapol.duplicateFrom(m_pLinStruc->solution());
    m_structureVelExtrapol.set( 0.);
    m_isStructureVelExtrapolAllocated = true;

    // sequential vectors
    m_seqStructureTimeRHS.duplicateFrom(m_pLinStruc->sequentialSolution());
    m_seqStructureTimeRHS.set( 0.0);
    m_isSeqStructureTimeRHSAllocated = true;

    m_seqStructureVelExtrapol.duplicateFrom(m_pLinStruc->sequentialSolution());
    m_seqStructureVelExtrapol.set( 0.0);
    m_isSeqStructureVelExtrapolAllocated = true;


    // displacement of the structure
    // parallel vectors
    m_structureDispOld.duplicateFrom(m_pLinStruc->solution());
    m_structureDispOld.set( 0.);
    m_isStructureDispOldAllocated = true;

    if(m_timeSchemeType == 4 && m_RNExtrapolOrder == 2) {
      m_structureNitscheRHSTermsOld.duplicateFrom(m_pLinStruc->vector());
      m_structureNitscheRHSTermsOld.set( 0.);
      m_isStructureNitscheRHSTermsOldAllocated = true;
    }

    if(m_timeSchemeType == 4) {
      m_structureTimeSchemeRHSTerms.duplicateFrom(m_pLinStruc->vector());
      m_structureTimeSchemeRHSTerms.set( 0.);
      m_isStructureTimeSchemeRHSTermsAllocated = true;

      m_structureExtrapolatedNitscheTermsRHS.duplicateFrom(m_pLinStruc->vector());
      m_structureExtrapolatedNitscheTermsRHS.set( 0.);
      m_isStructureExtrapolatedNitscheTermsRHSAllocated = true;
    }


    // Finite element for the linear problems
    // We assume that there is only one type of volume element for the structure
    const std::vector<GeometricMeshRegion::ElementType>& bagElementTypeDomainStruc = m_pLinStruc->meshLocal()->bagElementTypeDomain();
    FEL_ASSERT(bagElementTypeDomainStruc.size() == 1);

    const RefElement *refEleStruc;
    const GeoElement *geoEleStruc;

    geoEleStruc = GeometricMeshRegion::eltEnumToFelNameGeoEle[bagElementTypeDomainStruc[0]].second;

    int idDisplacement = m_pLinStruc->listVariable().getVariableIdList(displacement);
    int typeOfFiniteElement = m_pLinStruc->listVariable()[idDisplacement].finiteElementType();

    refEleStruc = geoEleStruc->defineFiniteEle(bagElementTypeDomainStruc[0], typeOfFiniteElement, *m_pLinStruc->meshLocal());
    m_feStruc = new CurvilinearFiniteElement(*refEleStruc, *geoEleStruc, m_pLinStruc->listVariable()[idDisplacement].degreeOfExactness());

    FEL_ASSERT(m_feStruc);
    m_pLinFluid->setStrucFiniteElement(m_feStruc);

    // We assume that there is only one type of volume element for the fluid
    const std::vector<GeometricMeshRegion::ElementType>& bagElementTypeDomainFluid = m_pLinFluid->meshLocal()->bagElementTypeDomain();
    FEL_ASSERT(bagElementTypeDomainFluid.size() == 1);

    const RefElement *refEleVel;
    const GeoElement *geoEleFluid;
    const RefElement *refElePre;

    geoEleFluid = GeometricMeshRegion::eltEnumToFelNameGeoEle[bagElementTypeDomainFluid[0]].second;

    int idVel = m_pLinFluid->listVariable().getVariableIdList(velocity);
    int idPre = m_pLinFluid->listVariable().getVariableIdList(pressure);
    int typeOfFiniteElementVel = m_pLinFluid->listVariable()[idVel].finiteElementType();
    int typeOfFiniteElementPre = m_pLinFluid->listVariable()[idPre].finiteElementType();

    refEleVel = geoEleFluid->defineFiniteEle(bagElementTypeDomainFluid[0], typeOfFiniteElementVel, *m_pLinStruc->meshLocal());
    m_feVel = new CurrentFiniteElement(*refEleVel, *geoEleFluid, m_pLinFluid->listVariable()[idVel].degreeOfExactness());

    refElePre = geoEleFluid->defineFiniteEle(bagElementTypeDomainFluid[0], typeOfFiniteElementPre, *m_pLinStruc->meshLocal());
    m_fePre = new CurrentFiniteElement(*refElePre, *geoEleFluid, m_pLinFluid->listVariable()[idPre].degreeOfExactness());

    FEL_ASSERT(m_feVel);
    FEL_ASSERT(m_fePre);
    m_pLinStruc->setFluidFiniteElement(m_feVel, m_fePre);


    // initialize the original bdf for the fluid
    m_pLinFluid->initOriginalBdf(m_timeSchemeType, m_timeSchemeExtrapolOrder);

    // Get the number of components in the displacement
    felInt iUnknown = m_pLinStruc->listUnknown().getUnknownIdList(displacement);
    felInt idVar = m_pLinStruc->listUnknown().idVariable(iUnknown);
    m_numStrucComp = m_pLinStruc->listVariable()[idVar].numComponent();
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::preAssemblingMatrixRHS(std::size_t iProblem)
  {
    if(iProblem == 0) {
      // Allocate the extrapolation of the velocity if necessary
      if(!m_isParFluidVelAllocated) {
        // There are orderBdfNS term in the velocity extrapolation
        m_parFluidVel.resize(FelisceParam::instance().orderBdfNS);

        // If we use a projection of the old solution, all the terms have the same size
        if(FelisceParam::instance().useProjectionForNitscheXFEM || m_fstransient->iteration == 0) {
          for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
            m_parFluidVel[i].duplicateFrom(m_pLinFluid->vector());
        } else {
          for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
            m_parFluidVel[i].duplicateFrom(m_pLinFluid->solutionOld(i));
        }

        // std::set the extrapolation term to zero
        for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
           m_parFluidVel[i].set( 0.);

        m_isParFluidVelAllocated = true;
      }

      // initialize fluid extrapolation in fluid linear problem
      if((m_timeSchemeType == 0) || (m_timeSchemeType == 2)) {
        if(!m_isTimeSchemeFluidExtrapolAllocated) {
          if(FelisceParam::instance().useProjectionForNitscheXFEM  || m_fstransient->iteration == 0) {
            for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
              m_timeSchemeFluidExtrapol[i].duplicateFrom(m_pLinFluid->solution());
          } else {
            for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
              m_timeSchemeFluidExtrapol[i].duplicateFrom(m_pLinFluid->solutionOld(i));
          }

          for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
            m_timeSchemeFluidExtrapol[i].set(0.);

          m_pLinFluid->initRNFluidExtrapol(m_timeSchemeExtrapolOrder);
          m_pLinFluid->gatherRNFluidExtrapol(m_timeSchemeFluidExtrapol);

          if(m_timeSchemeType == 0)
            m_isTimeSchemeFluidExtrapolAllocated = true;
        }
      }
    }

    if(iProblem == 1) {
      // initialize fluid extrapolation in structure linear problem
      if ((m_timeSchemeType == 3) || (m_timeSchemeType == 4)) {
        if(!m_isTimeSchemeFluidExtrapolAllocated) {
          if(FelisceParam::instance().useProjectionForNitscheXFEM  || m_fstransient->iteration == 0) {
            for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
              m_timeSchemeFluidExtrapol[i].duplicateFrom(m_pLinFluid->solution());
          } else {
            for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
              m_timeSchemeFluidExtrapol[i].duplicateFrom(m_pLinFluid->solutionOld(i));
          }

          for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
            m_timeSchemeFluidExtrapol[i].set(0.);

          m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);
          m_pLinStruc->gatherStabExplicitFluidExtrapol(m_timeSchemeFluidExtrapol);
          m_isTimeSchemeFluidExtrapolAllocated = true;
        }
      } else if (m_timeSchemeType == 2)  {
        if(!m_isTimeSchemeFluidExtrapolAllocated) {
          m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);
          m_pLinStruc->gatherStabExplicitFluidExtrapol(m_timeSchemeFluidExtrapol);
          m_isTimeSchemeFluidExtrapolAllocated = true;
        }
      }
    }
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::postAssemblingMatrixRHS(std::size_t iProblem)
  {
    if(iProblem == 0) {
      // m_Matrix = m_A + m_Matrix (add static matrix to the dynamic matrix to build
      // complete matrix of the system
      m_linearProblem[iProblem]->addMatrixRHS();
    }
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::setExternalVec()
  {
    // externalVec(0) in m_pLinFluid
    m_pLinFluid->pushBackExternalVec(m_seqStructureVelExtrapol);

    // externalVec(0) in m_pLinSolid
    m_pLinStruc->pushBackExternalVec(m_seqStructureTimeRHS);
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::forward()
  {
    // check solution on crack solid to determine crack spring constant -- if it exists in user
    m_pLinStruc->userComputeCrackStress();
    if(m_intersectMeshes.hasIntersectionChange()) {
      // reallocate everything for the fluid
      // supportDofMeshes, duplication, and dofs
      m_pLinFluid->computeDof(MpiInfo::numProc(), MpiInfo::rankProc());

      // new pattern
      m_pLinFluid->userChangePattern(MpiInfo::numProc(), MpiInfo::rankProc());

      // cut the mesh
      m_pLinFluid->cutMesh();

      // allocate matrices and vectors
      m_pLinFluid->allocateMatrix();

      // determine dof for the boundary conditions
      m_pLinFluid->determineDofAssociateToLabel();
      m_pLinFluid->finalizeEssBCConstantInT();

      // pre assembling to reallocate the extrapolations
      for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
        preAssemblingMatrixRHS(ipb);
      }

      // re build the solver (not possible to change the matrix size in the ksp without destroying it ?)
      m_pLinFluid->buildSolver();

      // Copy all the vectors of the fluid from the original support dof
      m_pLinFluid->copyOriginalSolToCurrentSol();
    }

    if(m_fstransient->iteration == 0) {
      // initialize the old objects
      m_pLinFluid->initOldObjects(m_timeSchemeExtrapolOrder);

      // Write solution for postprocessing (if required)
      writeSolutionAndMeshes();
    }

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();

    ///////////////////////
    // Update Bdf scheme //
    ///////////////////////

    if(m_intersectMeshes.hasIntersectionChange() || m_reinitializeFluidBdf || m_fstransient->iteration == 1) {
      // initialize the bdf for the fluid
      if(FelisceParam::instance().useProjectionForNitscheXFEM) {
        m_pLinFluid->copyOriginalBdfToCurrentBdf(m_timeSchemeType, m_timeSchemeExtrapolOrder);
      } else {
        felInt order = std::max(m_timeSchemeExtrapolOrder, FelisceParam::instance().orderBdfNS);
        PetscVector zero = PetscVector::null();
        if(order == 1) {
          m_bdfFluid.reinitialize(FelisceParam::instance().orderBdfNS, m_pLinFluid->solutionOld(0), zero, zero);
        } else if(order == 2) {
          m_bdfFluid.reinitialize(FelisceParam::instance().orderBdfNS, m_pLinFluid->solutionOld(1), m_pLinFluid->solutionOld(0), zero);
        } else {
          FEL_ERROR("This bdf order is not implemented for this problem");
        }
      }

      // Compute the extrapolation of the velocity and initialize the one in the solver
      m_bdfFluid.extrapolateByPart(m_parFluidVel);
      m_pLinFluid->gatherSeqVelExtrapol(m_parFluidVel);

      // Compute the rhs coming from the time scheme and initialize the one in the solver
      m_bdfFluid.computeRHSTimeByPart(m_fstransient->timeStep, m_parFluidVel);
      m_pLinFluid->gatherSeqBdfRHS(m_parFluidVel);

      // std::set flag to false
      m_reinitializeFluidBdf = false;
    } else {
      m_bdfFluid.update(m_pLinFluid->solution());

      m_bdfFluid.extrapolateByPart(m_parFluidVel);
      m_pLinFluid->gatherSeqVelExtrapol(m_parFluidVel);

      m_bdfFluid.computeRHSTimeByPart(m_fstransient->timeStep, m_parFluidVel);
      m_pLinFluid->gatherSeqBdfRHS(m_parFluidVel);
    }

    // structure
    m_structureDispOld.axpby(2., -1., m_pLinStruc->solution());
    m_pLinStruc->gatherVector(m_structureDispOld, m_seqStructureTimeRHS);
    m_structureDispOld.copyFrom(m_pLinStruc->solution());
    m_pLinStruc->gatherSeqLastTimeSol(m_structureDispOld);


    // move the mesh if the displacement is a std::vector
    // if( m_fstransient->iteration > 1 && m_numStrucComp == m_pLinFluid->dimension()) {
    //   m_pLinStruc->meshLocal()->moved() = false;
    //   m_pLinStruc->mesh()->moved() = false;
    //   m_pLinStruc->meshLocal()->moveMesh(m_interfaceDisplacement, 1);
    //   m_pLinStruc->mesh()->moveMesh(m_interfaceDisplacement, 1);
    // }

    // specific operation of the time schemes
    switch(m_timeSchemeType) {
      case 0: {
        computeTimeSchemeExtrapol();
        m_pLinFluid->gatherRNFluidExtrapol(m_timeSchemeFluidExtrapol);
        m_pLinFluid->setUseCurrentFluidSolution(false);
        m_pLinStruc->setUseCurrentFluidSolution(true);
        solveProblemFluid();
        solveProblemSolid();
        break;
      }

      case 1: {
        solveProblemSolid();
        solveProblemFluid();
        break;
      }

      case 2: {
        // Check if we need to move the mesh to its initial position to assemble solid terms
        bool moveTheMesh = m_fstransient->iteration > 1 && FelisceParam::instance().useDynamicStructure;

        computeTimeSchemeExtrapol();

        m_pLinFluid->gatherRNFluidExtrapol(m_timeSchemeFluidExtrapol);
        m_pLinFluid->setUseCurrentFluidSolution(false);
        solveProblemFluid();

        m_pLinStruc->setUseCurrentFluidSolution(true);

        // Assembly loop
        // 1) build the term on the original mesh
        if(moveTheMesh) {
          m_pLinStruc->meshLocal()->moveMesh(m_interfaceDisplacement, 0);
          m_pLinStruc->mesh()->moveMesh(m_interfaceDisplacement, 0);
        }

        m_pLinStruc->isAssemblingOfSolidProblemTerms(true);
        m_pLinStruc->gatherRNStructExtrapol(m_structureVelExtrapol);
        m_pLinStruc->assembleMatrixRHSBD(MpiInfo::rankProc());

        // build the term with the fluid on the moved mesh
        if(moveTheMesh) {
          m_pLinStruc->meshLocal()->moveMesh(m_interfaceDisplacement, 1.0);
          m_pLinStruc->mesh()->moveMesh(m_interfaceDisplacement, 1.0);
        }

        m_pLinStruc->isAssemblingOfSolidProblemTerms(false);
        m_pLinStruc->initStabExplicitFluidExtrapol(1);
        m_pLinStruc->assembleMatrixRHSBD(MpiInfo::rankProc());

        m_pLinStruc->setUseCurrentFluidSolution(false);

        m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);
        m_pLinStruc->gatherStabExplicitFluidExtrapol(m_timeSchemeFluidExtrapol);
        m_pLinStruc->assembleMatrixRHSBD(MpiInfo::rankProc());

        // Apply boundary conditions.
        m_pLinStruc->setUserBoundaryCondition();

        // Solve linear system.
        PetscPrintf(PETSC_COMM_WORLD, "*** Structure solver ***\n");
        m_pLinStruc->solve(MpiInfo::rankProc(), MpiInfo::numProc());

        ////////////////////////////////////////////////////
        // Compute the vec to link the two linear problem //
        ////////////////////////////////////////////////////

        computeTimeSchemeStructureVelExtrapol();

        // gather solution
        m_pLinStruc->gatherSolution();

        break;
      }

      case 3: {
        // initialize the fluid solution with the extrapolation
        computeTimeSchemeExtrapol();
        m_pLinStruc->gatherStabExplicitFluidExtrapol(m_timeSchemeFluidExtrapol);
        m_pLinFluid->setUseCurrentFluidSolution(false);
        m_pLinStruc->setUseCurrentFluidSolution(false);

        felInt it = 0;
        bool atConvergence = false;
        double abstol = 1e-12;
        while(!atConvergence) {
          // clear the matrices at each sub iteration
          m_pLinFluid->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
          m_pLinStruc->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);

          solveProblemSolid();
          solveProblemFluid();

          // use the fluid solution (not the extrapolation) for the next sub iterations
          if(it == 0) {
            m_pLinStruc->initStabExplicitFluidExtrapol(1);
            m_pLinStruc->gatherStabExplicitFluidExtrapol(m_pLinFluid->solution());

            m_pLinFluid->setUseCurrentFluidSolution(true);
            m_pLinStruc->setUseCurrentFluidSolution(true);
          } else {
            m_pLinStruc->setStabExplicitFluidExtrapol(m_pLinFluid->sequentialSolution());
          }

          // increment the number of iteration
          ++it;

          // compute the stopping criteria
          if(FelisceParam::instance().couplingSchemeStoppingCriteria == 0) {
            // fix number of iteration
            atConvergence = it < m_subIteration ? false : true;
          } else if(FelisceParam::instance().couplingSchemeStoppingCriteria == 1) {
            // compute relative error on structure
            m_stopVec.copyFrom(m_structureVel);
            double rhs = std::sqrt(m_pLinStruc->computeL2Norm(m_stopVec));
            m_stopVec.axpy(-1., m_structureVelOldStoppingCriteria);
            double lhs = std::sqrt(m_pLinStruc->computeL2Norm(m_stopVec));

            atConvergence = ((lhs < m_stoppingCriteriaTol * rhs) || (lhs < abstol)) && (it > 1) ? true : false;
            PetscPrintf(PETSC_COMM_WORLD, "Relative Error after iteration %d : %12.5e --- %12.5e\n\n", it, lhs, m_stoppingCriteriaTol*rhs);
          }
        }
        break;
      }

      case 4: {
        ////////////////////////////////////////////////////
        // fluid-(solid-inertia) problem //
        ////////////////////////////////////////////////////

        // gather the previous solid-velocity and solid-displacement for the (fluid)-(solid-inertia) problem
        m_pLinStruc->gatherRNStructExtrapol(m_structureVelExtrapol);

        // initialize the fluid solution with the extrapolation
        computeTimeSchemeExtrapol();
        m_pLinStruc->gatherStabExplicitFluidExtrapol(m_timeSchemeFluidExtrapol);

        m_pLinStruc->setUseCurrentFluidSolution(false);
        m_pLinFluid->setUseCurrentFluidSolution(false);

        m_pLinStruc->isStrucOperatorExplicitlyComputed(true);

        felInt it = 0;
        bool atConvergence = false;
        double abstol = 1e-12;
        while(!atConvergence) {
          // clear the matrices at each sub iteration
          m_pLinStruc->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
          m_pLinFluid->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);

          solveProblemSolid();
          solveProblemFluid();

          // use the fluid solution (not the extrapolation) for the next sub iterations
          if(it == 0) {
            m_pLinStruc->initStabExplicitFluidExtrapol(1);
            m_pLinStruc->gatherStabExplicitFluidExtrapol(m_pLinFluid->solution());

            m_pLinFluid->setUseCurrentFluidSolution(true);
            m_pLinStruc->setUseCurrentFluidSolution(true);
          } else {
            m_pLinStruc->setStabExplicitFluidExtrapol(m_pLinFluid->sequentialSolution());
          }

          // increment the number of iteration
          ++it;

          // compute the stopping criteria
          if(FelisceParam::instance().couplingSchemeStoppingCriteria == 0) {
            // fix number of iteration
            atConvergence = it < m_subIteration ? false : true;
          } else if(FelisceParam::instance().couplingSchemeStoppingCriteria == 1) {
            // compute relative error on structure
            m_stopVec.copyFrom(m_structureVelExtrapol);
            double rhs = std::sqrt(m_pLinStruc->computeL2Norm(m_stopVec));
            m_stopVec.axpy(-1., m_structureVelOldStoppingCriteria);
            double lhs = std::sqrt(m_pLinStruc->computeL2Norm(m_stopVec));

            atConvergence = ((lhs < m_stoppingCriteriaTol * rhs) || (lhs < abstol)) && (it > 1) ? true : false;
            PetscPrintf(PETSC_COMM_WORLD, "Relative Error after iteration %d : %12.5e --- %12.5e\n\n", it, lhs, m_stoppingCriteriaTol*rhs);
          }
        }


        //////////////////////////////////
        // solve the real solid problem //
        //////////////////////////////////

        m_pLinStruc->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
        m_pLinStruc->isStrucOperatorExplicitlyComputed(false);
        m_pLinStruc->setUseCurrentFluidSolution(true);
        m_pLinStruc->initStabExplicitFluidExtrapol(1);
        m_pLinStruc->gatherStabExplicitFluidExtrapol(m_pLinFluid->solution());

        solveProblemSolid();

        break;
      }

      default: {
        solveProblemFluid();
        solveProblemSolid();
        break;
      }
    }

    // post process //
    // Write solution (to get the real solution define on the duplicated support dof, not on the original one)
    writeSolutionAndMeshes();

    // update the position of the interface and recompute the intersection
    if(FelisceParam::instance().useDynamicStructure) {
      
      updateInterfacePosition( m_pLinStruc->sequentialSolution() );
      m_intersectMeshes.updateInterfacePosition( m_interfaceDisplacement );

      m_mesh[1]->moveMesh(m_interfaceDisplacement, 1.0);
      m_mesh[1]->computeElementNormal(m_mesh[1]->listElementNormals());
      m_mesh[1]->moveMesh(m_interfaceDisplacement, 0.);

      m_intersectMeshes.setIteration(m_fstransient->iteration);
      m_intersectMeshes.intersectMeshes(*m_mesh[0], *m_mesh[1]);
    }

    // save the solution with the original support Dof and delete the matrices, vectors and mapping of the fluid
    if(m_intersectMeshes.hasIntersectionChange()) {
      // copy for the fluid
      m_pLinFluid->copyCurrentSolToOriginalSol();

      // delete dynamic parts
      if(!hasFinished()) {
        m_pLinFluid->updateOldSolution(1);
        m_pLinFluid->deleteDynamicData();
        m_pLinStruc->deleteDynamicData();

        if(m_isTimeSchemeFluidExtrapolAllocated) {
          for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
            m_timeSchemeFluidExtrapol[i].destroy();

          m_isTimeSchemeFluidExtrapolAllocated = false;
        }

        if(m_isParFluidVelAllocated) {
          for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
            m_parFluidVel[i].destroy();

          m_isParFluidVelAllocated = false;
        }
      }

      // std::set to true only to remember that the intersection has changed after this time step
      // If the intersection is not changing at the next time step, we need to reinitialize the old objects
      m_countToInitOldObjects = std::max(m_timeSchemeExtrapolOrder, FelisceParam::instance().orderBdfNS);
    } else {
      m_pLinFluid->updateOldSolution(m_countToInitOldObjects);

      if(m_countToInitOldObjects > 0) {
        if(m_isTimeSchemeFluidExtrapolAllocated) {
          for(std::size_t i=0; i<m_timeSchemeFluidExtrapol.size(); ++i)
            m_timeSchemeFluidExtrapol[i].destroy();

          m_isTimeSchemeFluidExtrapolAllocated = false;
        }

        if(m_isParFluidVelAllocated) {
          for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
            m_parFluidVel[i].destroy();

          m_isParFluidVelAllocated = false;
        }

        // pre assembling to reallocate the extrapolations
        for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
          preAssemblingMatrixRHS(ipb);
        }

        m_countToInitOldObjects -= 1;
        m_reinitializeFluidBdf = true;
      } else {
        if((m_timeSchemeType == 2) || (m_timeSchemeType == 3) || (m_timeSchemeType == 4)) {
          // reset the right size for the extrapolation of the velocity
          m_pLinStruc->initStabExplicitFluidExtrapol(m_timeSchemeExtrapolOrder);
        }
      }
    }
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::solveProblemFluid()
  {
    // Assembly loop for front points
    m_pLinFluid->assembleMatrixFrontPoints();

    // Assembly loop for the ghost penalty stabilization
    if(FelisceParam::instance().useGhostPenalty)
      m_pLinFluid->assembleMatrixGhostPenalty();

    // Assembly loop for the face oriented stabilization
    if(FelisceParam::instance().NSStabType == 1)
      m_pLinFluid->assembleMatrixFaceOriented();

    // Main assembly loop and assembly of the matrix
    // The pattern is reduced to where values have been std::set till here, that is why this loop is called in last
    m_pLinFluid->assembleMatrixRHS(MpiInfo::rankProc());

    // Specific operations before solving the system
    // postAssemblingMatrixRHS(0);

    // Apply boundary conditions.
    // m_pLinFluid->finalizeEssBCConstantInT();
    m_pLinFluid->finalizeEssBCTransient();
    m_pLinFluid->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    // Solve the linear system.
    PetscPrintf(PETSC_COMM_WORLD, "*** Fluid solver ***\n");
    m_pLinFluid->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    // Gather the solution to use it in the solid problem
    m_pLinFluid->gatherSolution();
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::solveProblemSolid()
  {
    // Check if we need to move the mesh to its initial position to assemble solid terms
    bool moveTheMesh = m_fstransient->iteration > 1 && FelisceParam::instance().useDynamicStructure;

    // Assembly loop
    // 1) build the term on the original mesh
    if(moveTheMesh) {
      m_pLinStruc->meshLocal()->moveMesh(m_interfaceDisplacement, 0);
      m_pLinStruc->mesh()->moveMesh(m_interfaceDisplacement, 0);
    }

    m_pLinStruc->isAssemblingOfSolidProblemTerms(true);
    m_pLinStruc->assembleMatrixRHSBD(MpiInfo::rankProc());

    if(m_timeSchemeType == 4 && !m_pLinStruc->isStrucOperatorExplicitlyComputed()) {
      m_structureTimeSchemeRHSTerms.copyFrom(m_pLinStruc->vector());
    }

    // build the term with the fluid on the moved mesh
    if(moveTheMesh) {
      m_pLinStruc->meshLocal()->moveMesh(m_interfaceDisplacement, 1.0);
      m_pLinStruc->mesh()->moveMesh(m_interfaceDisplacement, 1.0);
    }

    m_pLinStruc->isAssemblingOfSolidProblemTerms(false);
    m_pLinStruc->assembleMatrixRHSBD(MpiInfo::rankProc());

    if(m_timeSchemeType == 4 && !m_pLinStruc->isStrucOperatorExplicitlyComputed()) {
      // update the vec
      computeTimeSchemeStructureExtrapolRHS();
    } else if(m_timeSchemeType == 4) {
      // add the extrapolation to the rhs
      m_pLinStruc->vector().axpy(-1., m_structureExtrapolatedNitscheTermsRHS);
    }


    // Apply boundary conditions.
    m_pLinStruc->setUserBoundaryCondition();

    // Solve linear system.
    PetscPrintf(PETSC_COMM_WORLD, "*** Structure solver ***\n");
    if(FelisceParam::instance().useODESolve)
      m_pLinStruc->solve(MpiInfo::rankProc(), MpiInfo::numProc(), !m_pLinStruc->getUseCurrentFluidSolution());
    else
      m_linearProblem[1]->solve(MpiInfo::rankProc(), MpiInfo::numProc());


    ////////////////////////////////////////////////////
    // Compute the vec to link the two linear problem //
    ////////////////////////////////////////////////////

    computeTimeSchemeStructureVelExtrapol();

    // gather solution
    m_pLinStruc->gatherSolution();
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::computeTimeSchemeExtrapol()
  {
    switch (m_timeSchemeExtrapolOrder) {
      case 0:
        break;
      case 1:
        m_timeSchemeFluidExtrapol[0].copyFrom(m_bdfFluid.sol_n());
        break;
      case 2:
        if((m_timeSchemeType == 0) || (m_timeSchemeType == 2)) {
          m_timeSchemeFluidExtrapol[0].copyFrom(m_bdfFluid.sol_n());
          m_timeSchemeFluidExtrapol[1].copyFrom(m_bdfFluid.sol_n_1());
        } else {
          m_timeSchemeFluidExtrapol[0].set( 0.);
          m_timeSchemeFluidExtrapol[1].set( 0.);
          m_timeSchemeFluidExtrapol[0].axpy(  2.0, m_bdfFluid.sol_n());
          m_timeSchemeFluidExtrapol[1].axpy( -1.0, m_bdfFluid.sol_n_1());
        }
        break;
      default:
        FEL_ERROR("unknown time scheme extrapolation order ");
        break;
    }
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::computeTimeSchemeStructureVelExtrapol()
  {
    bool isRNScheme = (m_timeSchemeType == 0) || (m_timeSchemeType == 2) || (m_timeSchemeType == 4 && !m_pLinStruc->isStrucOperatorExplicitlyComputed());

    if(isRNScheme) {
      switch(m_RNExtrapolOrder) {
        case 0:
          break;
        case 1:
          m_structureVelOld.copyFrom(m_structureVel);
          break;
        case 2:
          m_structureVelOldOld.copyFrom(m_structureVelOld);
          m_structureVelOld.copyFrom(m_structureVel);
          break;
        default:
          FEL_ERROR("unknown time scheme extrapolation order");
          break;
      }
    }
    else {
      if(FelisceParam::instance().couplingSchemeStoppingCriteria == 1) {
        if(m_timeSchemeType == 3)
          m_structureVelOldStoppingCriteria.copyFrom(m_structureVel);
        else
          m_structureVelOldStoppingCriteria.copyFrom(m_structureVelExtrapol);
      }
    }

    // compute the velocity of the structure
    // dotX^n = ( X^n - X^(n-1) ) / tau
    double tau = m_fstransient->timeStep;
    if(m_timeSchemeType != 4 || (m_timeSchemeType == 4 && !m_pLinStruc->isStrucOperatorExplicitlyComputed())) {
      m_structureVel.copyFrom(m_pLinStruc->solution());
      m_structureVel.axpby(-1./tau, 1./tau, m_structureDispOld);
    } else {
      m_structureVelExtrapol.copyFrom(m_pLinStruc->solution());
    }


    // compute the extrapolation of the velocity
    if (isRNScheme) {
      switch (m_RNExtrapolOrder) {
        case 0:
          // ddot^{n-1} + dt \pd ddot^{\star}  = ddot^{n-1}
          m_structureVelExtrapol.copyFrom(m_structureVel);
          break;
        case 1:
          // ddot^{n-1} + dt \pd ddot^{{\star}  = 2*ddot^{n-1} - ddot^{n-2}
          m_structureVelExtrapol.copyFrom(m_structureVel);
          m_structureVelExtrapol.axpby(-1., 2. , m_structureVelOld);
          break;
        case 2:
          // ddot^{n-1} + dt \pd ddot^{{\star}  = 3*ddot^{n-1} - 3*ddot^{n-2} + ddot^{n-3}
          m_structureVelExtrapol.copyFrom(m_structureVel);
          m_structureVelExtrapol.axpby(-3., 3., m_structureVelOld);
          m_structureVelExtrapol.axpy( 1. , m_structureVelOldOld);
          break;
        default:
          FEL_ERROR("unknown time scheme extrapolation order");
          break;
      }
    } else if(m_timeSchemeType == 3) {
      // No extrapolation is needed for scheme 3
      m_structureVelExtrapol.copyFrom(m_structureVel);
    }

    // the serial std::vector is passed to the fluid problem
    m_pLinStruc->gatherVector(m_structureVelExtrapol, m_seqStructureVelExtrapol);
  }

  template<class SolidPrb> 
  void FSINitscheXFEMModel<SolidPrb>::computeTimeSchemeStructureExtrapolRHS() 
  {
    switch(m_RNExtrapolOrder) {
    case 0:
      break;

    case 1:
      m_structureExtrapolatedNitscheTermsRHS.copyFrom(m_pLinStruc->vector());
      m_structureExtrapolatedNitscheTermsRHS.axpby(-1., 1., m_structureTimeSchemeRHSTerms);
      break;

    case 2:
      m_structureExtrapolatedNitscheTermsRHS.copyFrom(m_pLinStruc->vector());
      m_structureExtrapolatedNitscheTermsRHS.axpby(-1., 1., m_structureTimeSchemeRHSTerms);
      m_structureExtrapolatedNitscheTermsRHS.axpby(-1., 2. , m_structureNitscheRHSTermsOld);

      m_structureNitscheRHSTermsOld.copyFrom(m_pLinStruc->vector());
      m_structureNitscheRHSTermsOld.axpby(-1., 1., m_structureTimeSchemeRHSTerms);
      break;

    default:
      FEL_ERROR("unknown time scheme extrapolation order");
      break;
    }
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::writeSolutionAndMeshes()
  {
    if( (m_fstransient->iteration % FelisceParam::instance().frequencyWriteSolution == 0)
        or m_hasFinished
        or ( m_fstransient->iteration >= FelisceParam::instance().intervalWriteSolution[0] && m_fstransient->iteration <= FelisceParam::instance().intervalWriteSolution[1] )) {

      // write the mesh
      if(FelisceParam::verbose()>1)
        PetscPrintf(MpiInfo::petscComm(),"Write meshes\n");

      std::map<felInt, std::vector<felInt> > refToListOfIds;
      std::string fileName;
      std::string indexFile;

      for(std::size_t ipb=0; ipb<m_linearProblem.size(); ++ipb) {
        for(std::size_t iUnknown=0; iUnknown<m_linearProblem[ipb]->listUnknown().size(); ++iUnknown) {
          std::stringstream oss;
          oss << m_indexTime;
          if(m_indexTime < 10)
            indexFile = "0000" + oss.str();
          else if(m_indexTime < 100)
            indexFile = "000" + oss.str();
          else if(m_indexTime < 1000)
            indexFile = "00" + oss.str();
          else if(m_indexTime < 10000)
            indexFile = "0" + oss.str();
          else
            indexFile = oss.str();

          int idVar = m_linearProblem[ipb]->listUnknown().idVariable(iUnknown);
          fileName = FelisceParam::instance().resultDir + m_linearProblem[ipb]->listVariable().listVariable()[idVar].name()+".geo."+indexFile+".geo";

          if(ipb == 0) {
            // fluid, write mesh from the support dof to see the duplication
            if(iUnknown == 0) {
              // write the mesh only once (with the velocity, the pressure mesh is the same)
              if(FelisceParam::instance().outputFileFormat == 0)
                m_linearProblem[ipb]->writeGeoForUnknown(iUnknown, fileName);
              else if(FelisceParam::instance().outputFileFormat == 1) {
                m_linearProblem[ipb]->writeGeoForUnknownEnsightGold(iUnknown, refToListOfIds, fileName);
                m_ios[0]->ensight().setRefToListOfIds(refToListOfIds);
              }
              else
                FEL_ERROR("Unknown output file format");
            }
          } else {
            // structure, write mesh from the mesh points to see the displacement
            m_ios[1]->writeMesh(*m_mesh[1], fileName);
          }
        }
      }
      m_meshIsWritten = true;
      ++m_indexTime;


      // write the solutions
      if(FelisceParam::verbose() > 1)
        PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");

      //PetscVector aux;
      //aux.duplicateFrom(m_linearProblem[0]->solution());
      //aux.copyFrom(m_linearProblem[0]->solution());
      //if(m_fstransient->iteration > 0)
      //  m_linearProblem[0]->solution().copyFrom(m_bdfFluid.sol_n());

      for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
        m_linearProblem[ipb]->writeSolution(MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration);
      }

      //m_linearProblem[0]->solution().copyFrom(aux);
      //m_linearProblem[0]->gatherSolution();
      //aux.destroy();

      if (MpiInfo::rankProc() == 0) {
        for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
          if(m_ios[iio]->typeOutput == 1 ) // 1 : ensight
            m_ios[iio]->postProcess(m_fstransient->time, 2);
        }
      }
    }
  }

  template<class SolidPrb>
  void FSINitscheXFEMModel<SolidPrb>::updateInterfacePosition(PetscVector& disp) 
  {
    const felInt dim = m_mesh[1]->numCoor();
    const felInt npt = m_mesh[1]->numPoints();

    m_interfaceDisplacement.resize(dim * npt, 0.);

    double* dispArray;
    disp.getArray(&dispArray);

    if( m_numStrucComp == 1 ) {
      auto& normals = m_mesh[1]->listNormals();
  
      // displacement is scalar
      for (felInt i = 0; i < npt; ++i)
        for(felInt j = 0; j < dim; ++j)
          m_interfaceDisplacement[j + dim*i] = dispArray[i] * normals[i][j];
    } else {
      // displacement is a std::vector
      for(felInt i = 0; i < npt; ++i)
        for(felInt j = 0; j < dim; ++j)
          m_interfaceDisplacement[j + dim*i] = dispArray[j + dim*i];
    }

    disp.restoreArray(&dispArray);
  }
}
