//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/bidomainThoraxModel.hpp"

namespace felisce {
  BidomainThoraxModel::BidomainThoraxModel():
    Model() {
    for(std::size_t i=0; i<m_linearProblemBidomainThorax.size(); i++) {
      m_linearProblemBidomainThorax[i] = nullptr;
    }
    m_name = "BidomainThorax";
  }

  BidomainThoraxModel::~BidomainThoraxModel() {
    for(std::size_t i=0; i<m_linearProblemBidomainThorax.size(); i++) {
      m_linearProblemBidomainThorax[i] = nullptr;
    }
  }

  void BidomainThoraxModel::initializeDerivedModel() {
    if ( FelisceParam::instance().withCVG ){
      m_linearProblemBidomainThorax[0]->initPetscVectors();
    }
  }

  void BidomainThoraxModel::initializeDerivedLinearProblem() {
    for (std::size_t i=0; i<m_linearProblem.size(); i++) {
      m_linearProblemBidomainThorax.push_back(dynamic_cast<LinearProblemBidomainThorax*>(m_linearProblem[i]));
    }
  }

  void BidomainThoraxModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    if (m_fstransient->iteration == 0) {
      m_linearProblemBidomainThorax[iProblem]->readData(*io());
    }
  }

  // Pay attention on the call of this :
  // call BidomainThoraxModel::writeSolution() function instead of (non-virtual) Model::writeSolution() function
  void BidomainThoraxModel::writeSolution() 
  {
    Model::writeSolution();
  }

  void BidomainThoraxModel::forward() {
    //Write solution with ensight.
    BidomainThoraxModel::writeSolution();
    //Advance time step.
    updateTime(FlagMatrixRHS::only_rhs);
    printNewTimeIterationBanner();

    preAssemblingMatrixRHS(0);

    // Assembling of matrices at first iteration (because of needs some coefficients not defined at iteration 0).
    if ( m_fstransient->iteration == 1 ) {
      m_linearProblemBidomainThorax[0]->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
      //Apply boundary conditions.
      m_linearProblem[0]->finalizeEssBCConstantInT();
      m_linearProblem[0]->finalizeEssBCTransient();
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs);
    } else {
      //Apply boundary conditions.
      m_linearProblem[0]->finalizeEssBCConstantInT();
      m_linearProblem[0]->finalizeEssBCTransient();
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs);
    }

    if (FelisceParam::instance().writeMatrixECG) {
      m_linearProblemBidomainThorax[0]->calculateRHS();
    }

    // Apply CVGraph interface conditions
    postAssemblingMatrixRHS(0);

    //Solve the system _Matrix[0]*m_sol=_RHS.
    m_linearProblemBidomainThorax[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    // User-defined function to calculate residual = lines of transfer matrix
    if (FelisceParam::instance().writeMatrixECG ) {
      m_linearProblemBidomainThorax[0]->calculateRes();
    }

  }

#ifdef FELISCE_WITH_CVGRAPH
  void BidomainThoraxModel::startIterationCVG() {
    cvgMainSlave* slave=m_linearProblem[0]->slave();
    if (slave==nullptr) {
      FEL_ERROR("slave not initialized");
    }
    if( slave->newTimeStep() ) {  
      std::stringstream msg;
      msg<<"Starting new time step " << m_fstransient->iteration  <<" , preparing forward"<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
  
      //Write solution with ensight.
      BidomainThoraxModel::writeSolution();
      //Advance time step.

      if ( m_fstransient->iteration == 0 || m_fstransient->iteration == 1 ) {
        //std::cout << "1.1 Slave new time step = " << m_fstransient->iteration  << std::endl; 
        updateTime(FlagMatrixRHS::matrix_and_rhs);
        printNewTimeIterationBanner();
        //m_linearProblem[0]->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);// Done in updateTime()
        m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
        m_linearProblem[0]->createAndCopyMatrixRHSWithoutBC();

        m_linearProblem[0]->finalizeEssBCConstantInT();
        //std::cout << "FinalizeEssBCTransient()...";
        m_linearProblem[0]->finalizeEssBCTransient();
        //std::cout << " Done FEBT." << std::endl;
        //std::cout << "Apply BC...";
        if (slave->thereIsAtLeastOneRobinCondition()){
          //std::cout << "  CVG Robin: we apply the robin condition in the MATRIX ONLY. The RHS parts are handled in cvgAddExternalResidualToRHS() and cvgAddRobinToRHS()." << std::endl;
          m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::only_matrix,0, true, ApplyNaturalBoundaryConditions::yes);
          if (m_fstransient->iteration == 1){
            //std::cout << "Building the mass matrix at the CVG boundary" << std::endl;
            m_linearProblemBidomainThorax[0]->initMassBoundaryForCVG();
          }
        }
        else {
          m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs,0, true, ApplyNaturalBoundaryConditions::no);
        }
      }
      else{
        //std::cout << "1.2 Slave new time step = " << m_fstransient->iteration  << std::endl;
        updateTime(FlagMatrixRHS::only_rhs);
        printNewTimeIterationBanner();

        m_linearProblem[0]->finalizeEssBCConstantInT();
        m_linearProblem[0]->finalizeEssBCTransient();
        m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs, 0, true, ApplyNaturalBoundaryConditions::no);

        //m_linearProblemBidomainThorax[0]->addCurrentToRHS();
      }

    } else if( slave->redoTimeStep() ){
      std::stringstream msg;
      msg << "......................................" << std::endl;
      msg << "Redo time step at time t= " << m_fstransient->time << std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());


      m_linearProblem[0]->clearMatrixRHS(FlagMatrixRHS::only_rhs);

      // No call to assembleMatrixRHS. THIS IS DANGEROUS BUT PROVIDES A HUGE SPEEDUP
      // This only works because the RHS without BC is 0. It is not the case if there are source terms for instance... Eliott
      // I guess many things (e.g. loops on elements) are done in assembleMatrixRHS() even when FlagMatrixRHS is std::set to only_rhs and are therefore useless.
      // m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::only_rhs); 

      m_linearProblem[0]->finalizeEssBCConstantInT();
      m_linearProblem[0]->finalizeEssBCTransient();
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs, 0, true, ApplyNaturalBoundaryConditions::no);
      //m_linearProblemBidomainThorax[0]->addCurrentToRHS();

    } else {
      slave->msgInfo().print(1);
      FEL_ERROR("Error: strange timeStatus");
    }

    if (slave->thereIsAtLeastOneRobinCondition()){
      //std::cout << "  CVG Robin: Add remaining term in RHS: beta_torso*massBD*u_torso" << std::endl;
      m_linearProblemBidomainThorax[0]->cvgAddRobinToRHS();
    }

    //Solve the system _Matrix[0]*m_sol=_RHS.
    //std::cout <<std::endl<<std::endl<< "#######   SOLVE THE LINEAR SYSTEM  ######" <<std::endl<< std::endl;
    m_linearProblemBidomainThorax[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());


  }
#endif


}
