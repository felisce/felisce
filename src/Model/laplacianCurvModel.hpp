//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _LAPLACIANCURVMODEL_HPP
#define _LAPLACIANCURVMODEL_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  class LaplacianCurvModel:
    public Model {
  public:
    /// Construtor.
    LaplacianCurvModel();
    /// Destructor.
    ~LaplacianCurvModel() override;
    ///  Manage time iteration.
    void forward() override;
    ///  Function to get size of the state std::vector.
    int getNstate() const override;
    ///  Function to get state std::vector.
    void getState(double* & state) override;
    ///  Function to std::set state std::vector.
    void setState(double* & state) override;
  };
}

#endif
