//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/NSHeatModel.hpp"
#include "Solver/linearProblemNSHeat.hpp"

namespace felisce {
  NSHeatModel::NSHeatModel():NSModel() {
    m_name = "Navier Stokes and Heat equations coupled";
  }
  void NSHeatModel::forward() {
    if ( m_fstransient->iteration == 0 ) {
      static_cast<LinearProblemNSHeat*>(m_linearProblem[0])->userInitialize();
      if ( FelisceParam::instance().exportP1Normal ) {
        m_linearProblem[0]->initPetscVectors();

        static_cast<LinearProblemNSHeat*>(m_linearProblem[0])->computeNormalField(FelisceParam::instance().labelExportP1Normal,0);
        
        
        PetscVector zero;
        zero.duplicateFrom(m_linearProblem[0]->solution());
        zero.zeroEntries();
        m_linearProblem[0]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("normal"));
      }
    }
    
    NSModel::forward();
    m_linearProblem[0]->gatherSolution();
    static_cast<LinearProblemNSHeat*>(m_linearProblem[0])->userPostProcessing();
    
    if ( FelisceParam::instance().exportP1Normal) {
      m_linearProblem[0]->writeSolutionFromVec(m_linearProblem[0]->get(LinearProblem::sequential,"normalField"),
                                               MpiInfo::rankProc(),
                                               m_ios,
                                               m_fstransient->time,
                                               m_fstransient->iteration,
                                               std::string("normal"));
    }
  }  
}
