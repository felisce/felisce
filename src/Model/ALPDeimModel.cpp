//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/ALPDeimModel.hpp"

namespace felisce {
  ALPDeimModel::ALPDeimModel():
    ALPModel() {
    for(std::size_t i=0; i<m_eigenProblemDeim.size(); i++) {
      m_eigenProblemDeim[i] = nullptr;
    }
  }

  void ALPDeimModel::preAssembleMatrix(const int iProblem) {

    m_eigenProblemDeim.push_back(static_cast<EigenProblemALPDeim*>(m_eigenProblem[iProblem]));

    std::unordered_map<std::string, int> mapOfType;
    mapOfType["EXPLICIT_EULER"] = 0;
    mapOfType["RUNGE_KUTTA_2"] = 1;

    m_method = mapOfType[FelisceParam::instance().integrationTimeMethod];

    m_eigenProblemDeim[iProblem]->setIntegrationMethod(m_method);

    // Read collocation points from file (mesh nodes)
    m_eigenProblemDeim[iProblem]->readCollocationPts();

    if (FelisceParam::instance().hasInfarct) {
      HeteroSparFHN heterof0;
      std::vector<double> valuef0;
      heterof0.initialize(m_fstransient);
      m_eigenProblemDeim[iProblem]->evalFunctionOnDof(heterof0, valuef0);
      m_eigenProblemDeim[iProblem]->setFhNf0(valuef0);
    }
  }

  // Pay attention on the call of this :
  // call ALPModel::writeSolution() function instead of (non-virtual) Model::writeSolution() function
  void ALPDeimModel::writeSolution() {
    if (MpiInfo::rankProc() == 0) {
      if (m_meshIsWritten == false) writeMesh();
    }

    if( (m_fstransient->iteration % FelisceParam::instance().frequencyWriteSolution == 0)
        or m_hasFinished) {
      if(FelisceParam::verbose() > 1) PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");
      for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
        // Calculate FE solution (projection of alpha coeff)
        m_eigenProblemDeim[ipb]->writeEnsightSolution(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution));
      }
    }
  }

  void ALPDeimModel::forward() {
    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      if ( m_fstransient->iteration == 0 ) {
        // Initialize Rom object and calculate reduced basis

        // Read initial data
        m_eigenProblem[ipb]->readData(*io());
        preAssembleMatrix(ipb);
        // Create _Matrix[0] and _Matrix[1] of m_eigenProblem
        m_eigenProblem[ipb]->assembleMatrix();

        // Read basis from ensight files
        if (FelisceParam::instance().readBasisFromFile) {
          // Build basis reading vectors from ensight files
          m_eigenProblemDeim[ipb]->initializeROM();
        }
        // Calculate basis functions solving Schrodinger equation
        else {
          if (MpiInfo::rankProc() == 0) {
            if (m_meshIsWritten == false) writeMesh();
          }
          // Initialize Slepc solver
          m_eigenProblem[ipb]->buildSolver();
          // Solve with slepc the generilized eigen problem: _Matrix[0] v = m_matrix[1] lambda v
          m_eigenProblem[ipb]->solve();
          // Writes modes (eigenvectors) in ensight format
          m_eigenProblem[ipb]->writeMode();
          // m_massDeim = mass matrix on V
          m_eigenProblemDeim[ipb]->computeMassDeim();
          m_eigenProblemDeim[ipb]->initializeSolution();
        }

        if (FelisceParam::instance().hasSource)
          postAssembleMatrix(ipb);

        // m_hatF and M_hatG
        m_eigenProblemDeim[ipb]->computeRHSDeim();

        // m_theta = \Phi^T G F * \Phi
        m_eigenProblemDeim[ipb]->computeTheta();

        // M_ij = \chi / (\lambda_j - \lambda_i) \theta_ij
        m_eigenProblemDeim[ipb]->computeMatrixM();

        // matrices E and Q for improved recontruction and/or bidomain problem
        m_eigenProblemDeim[ipb]->computeTensor();

        // \Pi_V = V W G
        m_eigenProblemDeim[ipb]->computeProjectionPiV();

        if (FelisceParam::instance().writeECG) {
          m_eigenProblemDeim[ipb]->initializeECG();
          m_eigenProblemDeim[ipb]->writeECG(m_fstransient->iteration);
        }

      }
    }

    //Write solution with ensight.
    ALPDeimModel::writeSolution();
    //Advance time step.
    updateTime();
    printNewTimeIterationBanner();

    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      switch (m_method) {
      case 0: // EXPLICIT_EULER
        m_eigenProblemDeim[ipb]->updateBasis();

        m_eigenProblemDeim[ipb]->updateBeta();

        m_eigenProblemDeim[ipb]->updateEigenvalue();

        m_eigenProblemDeim[ipb]->computeHatU();

        m_eigenProblemDeim[ipb]->computeRHSDeim();
        m_eigenProblemDeim[ipb]->computeTheta();
        m_eigenProblemDeim[ipb]->computeMatrixM();

        break;
      case 1: // RUNGE_KUTTA_2
        m_eigenProblemDeim[ipb]->updateBasis();

        m_eigenProblemDeim[ipb]->updateBeta();

        m_eigenProblemDeim[ipb]->updateEigenvalue();

        m_eigenProblemDeim[ipb]->computeHatU();

        m_eigenProblemDeim[ipb]->computeRHSDeim();
        m_eigenProblemDeim[ipb]->computeTheta();
        m_eigenProblemDeim[ipb]->computeMatrixM();
        break;
      default:
        FEL_ERROR("This integration method is not implemented.");
        break;
      }

      if (FelisceParam::instance().writeECG) {
        m_eigenProblemDeim[ipb]->updateEcgOperator();
        m_eigenProblemDeim[ipb]->writeECG(m_fstransient->iteration);
      }

    }

  }


}
