//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone D.Lombardi
//

#ifndef _PODMODEL_HPP
#define _PODMODEL_HPP

// System includes

// External includes
#include <petscvec.h>

// Project includes
#include "Model/model.hpp"
#include "Solver/eigenProblem.hpp"
#include "Solver/eigenProblemPOD.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  /*!
   \file PODModel.hpp
   \authors E. Schenone D.Lombardi
   \date 10/12/2012
   \brief POD model class manages Proper Orthogonal Decomposition.
   */
  class PODModel:
    public Model {
			public:
				///  Construtor.
				PODModel();
				///  Destructor.
				~PODModel() override;
				///  Initializations
				void initializeDerivedModel() override {}
				void initializeEigenProblem(std::vector<EigenProblemPOD*> eigenPb);
				void preAssembleMatrix(const int iProblem);
				virtual void postAssembleMatrix(const int iProblem) {
				  IGNORE_UNUSED_ARGUMENT(iProblem);
				}
				void updateTime(const FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs) override;
				void writeSolution();
				void solveEigenProblem();
				///  Manage time iteration.
				void forward() override;
				///  Acces functions.
				EigenProblemPOD* eigenProblem() {
				  return m_eigenProblem[0];
				}
				EigenProblemPOD* eigenProblem(int iProblem) {
				  return m_eigenProblem[iProblem];
				}
			protected:
				std::vector<EigenProblemPOD*> m_eigenProblem;
			private:
				int m_method;
			};
}

#endif
