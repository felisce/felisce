//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __LINEARELASTICITYMODEL_HPP__
#define __LINEARELASTICITYMODEL_HPP__

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Solver/linearProblemLinearElasticity.hpp"

namespace felisce {
  class LinearElasticityModel:
    public Model {
  public:
    //------- Constructor/Destructor
    //-------------------------------
    LinearElasticityModel() {
      m_name = "LinearElasticity";
      m_idLE=0;
    }
    ~LinearElasticityModel() override = default;

    // Functions called in model:initializeLinearProblem
    void initializeDerivedModel() override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;

    //! Manage time iteration.
    void prepareForward(FlagMatrixRHS flag = FlagMatrixRHS::matrix_and_rhs);
    void forward() override;
    void finalizeForward();
    
    void solveLinearElasticity(FlagMatrixRHS flag = FlagMatrixRHS::matrix_and_rhs);

#ifdef FELISCE_WITH_CVGRAPH
    void cvgraphNewTimeStep() override;
#endif
  protected:
    felInt m_idLE;
    LinearProblemLinearElasticity* m_lpbLE;
  private:
    ChronoInstance m_chronoSolve;
    void writeNormal();
  };
}

#endif
