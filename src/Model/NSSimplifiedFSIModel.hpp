//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _NSSimplifiedFSIModel_HPP
#define _NSSimplifiedFSIModel_HPP

// System includes

// External includes
#include <petscvec.h>

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Solver/bdf.hpp"
#include "Solver/linearProblemNSSimplifiedFSI.hpp"

namespace felisce {
  class NSSimplifiedFSIModel:
    public Model {
    LumpedModelBC* m_lumpedModelBC;
  public:
    /// Construtor.
    NSSimplifiedFSIModel();
    /// Destructor.
    ~NSSimplifiedFSIModel() override;

    void initializeDerivedModel() override;
    void postAssemblingMatrixRHS(std::size_t iProblem) override;
    void preAssemblingMatrixRHS(std::size_t iProblem) override;

    ///  Manage time iteration.
    void forward() override;

    inline const Bdf & bdf() const {
      return m_bdfNS;
    }
    inline Bdf & bdf() {
      return m_bdfNS;
    }

  protected:
    std::size_t m_idNSSimpl;

    void writeExtraVectors();
    void prepareForward();
    void finalizeForward();

    void solveNSSimplifiedFSI(FlagMatrixRHS flagMatrixRHS);

  private:
    /// EDP time scheme.
    Bdf m_bdfNS;

    PetscVector m_solExtrapolate;

    bool m_exportBdQuantities;

    std::vector<int>    m_labelFluxListLumpedModelBC;
    std::vector<double> m_fluxLumpedModelBC;

    ChronoInstance chronoAssemble, chronoSolve ;
    LinearProblemNSSimplifiedFSI* m_SimplifiedFSI;
    void exportBdQuantities();
  };
}

#endif
