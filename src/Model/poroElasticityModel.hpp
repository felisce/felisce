//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//


#ifndef __POROELASTICITYMODEL_HPP__
#define __POROELASTICITYMODEL_HPP__

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Solver/linearProblemPoroElasticity.hpp"

namespace felisce {
  class PoroElasticityModel:
    public Model {
  public:
    ///  Construtor.
    PoroElasticityModel();
    ///  Empty destructor.
    ~PoroElasticityModel() override = default;;

    // Initialization of poroElasticityModel
    void initializeDerivedModel() override;

    // Standard forward
    // It calls prepareForward, it assembles the current matrix matrix.
    // It calls postAssembling MatrixRHS and solvePoroElasticity.
    void forward() override;

    // Boundary conditions and ksp
    void solvePoroElasticity(FlagMatrixRHS flag = FlagMatrixRHS::matrix_and_rhs);
    
#ifdef FELISCE_WITH_CVGRAPH
    void startIterationCVG() override;
#endif

  private:

    // Pointer to linear problem casted to LinearProblemPoroElasticity
    LinearProblemPoroElasticity* m_lpbPE;
    
    // Called in model.cpp, used to assemble the static part of the matrix.
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    
    // Posto processing, advancing in time
    void prepareForward(FlagMatrixRHS flag = FlagMatrixRHS::matrix_and_rhs);

  protected:
    felInt m_idPE;
  };
}

#endif
