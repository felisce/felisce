//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef _STOKESCONTINUATIONMODEL_HPP
#define _STOKESCONTINUATIONMODEL_HPP

// System includes

// External includes

// Project includes
#include <Model/model.hpp>
#include <Core/felisceParam.hpp>
#include <Solver/linearProblemStokesContinuation.hpp>

namespace felisce {
  class StokesContinuationModel:
    public Model{
  public:
    //!Constructor.
    StokesContinuationModel();
    //!Destructor.
    ~StokesContinuationModel() override= default;;
    //! Manage time iteration.
    void forward() override;

  protected:
    LinearProblemStokesContinuation* m_lpb;
  private:
    void prepareForward();
    void initializeDerivedModel() override;
  };
}

#endif
