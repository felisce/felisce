//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/laplacianCurvModel.hpp"
#include "Core/mpiInfo.hpp"

namespace felisce {
  LaplacianCurvModel::LaplacianCurvModel() {
    m_name = "LaplacianCurv";
  }

  LaplacianCurvModel::~LaplacianCurvModel()
  = default;


  void LaplacianCurvModel::forward() {
    PetscPrintf(MpiInfo::petscComm(),"\n\n\tTimestep: %d \n\n", m_fstransient->iteration);

    //Assembly llop on elements.
    m_linearProblem[0]->assembleMatrixRHSBD(MpiInfo::rankProc());

    //Specific operations before solve the system.
    postAssemblingMatrixRHS();

    //Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    //Solve linear system.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    //Advance time step.
    updateTime();

    //Write solution with ensight.
    writeSolution();
  }

  int LaplacianCurvModel::getNstate() const {
    return m_linearProblem[0]->numDof();
  }

  void LaplacianCurvModel::getState(double* & state) {
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }

  void LaplacianCurvModel::setState(double* & state) {
    m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
}


