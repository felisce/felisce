//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J Fouchet & C Bui
//

#ifndef _NSlinCombModel_HPP
#define _NSlinCombModel_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Solver/coefProblem.hpp"
#include "Solver/bdf.hpp"

namespace felisce {
  class NSlinCombModel:
    public Model {
    LumpedModelBC* m_lumpedModelBC;
  public:
    /// Construtor.
    NSlinCombModel();
    /// Destructor.
    ~NSlinCombModel() override;

    void initializeDerivedModel() override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void preProcessing() override;

    ///  Manage time iteration.
    void forward() override;

    // access functions
    //===================

    inline const Bdf & bdf() const {
      return m_bdfNS;
    }
    inline Bdf & bdf() {
      return m_bdfNS;
    }

  protected:

    CoefProblem* coefProblem() {
      return m_coefProblem;
    }

  private:

    Bdf m_bdfNS;
    PetscVector m_solExtrapolate;

    // u_i
    //=====

    std::vector<PetscVector> m_stationnarySolutionsVec;

    std::vector<int> m_labelListlinCombBC;
    std::vector<double> m_fluxLinCombBCtotal;
    std::vector<double> m_fluxLinCombBC;

    // Linear problem to determine the alpha_i
    //=========================================

    CoefProblem* m_coefProblem;

  };
}

#endif
