//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Fernandez & L. Boilevin-Kayl
//

// System includes

// External includes

// Project includes
#include "Model/immersed.hpp"
#include "InputOutput/io.hpp"

namespace felisce 
{
  Immersed::Immersed() {

    m_intfDisp  = nullptr;
    m_intfVelo  = nullptr;
    m_intfForc  = nullptr;
    m_velFluid  = nullptr;
    m_indexTime = 0;

    m_allowProjection = FelisceParam::instance().allowProjectionImmersedStruct;
  }

  Immersed::~Immersed() = default;

  void Immersed::initialize(GeometricMeshRegion::Pointer backMesh){

    m_backMesh = backMesh;

    if ( MpiInfo::rankProc() == 0 ) { // only the first processor possesses the full immersed mesh and will be responsible for the interpolation
      m_readInterfaceMesh(); // read the immersed structure mesh and put it into m_interfMesh

      const int verbose = FelisceParam::verbose();
      const double tolrescaling = FelisceParam::instance().Geo.tolrescaling;

      m_interpolator = felisce::make_shared<Interpolator>(m_backMesh.get(), m_allowProjection, tolrescaling, verbose);
      m_intersector  = felisce::make_shared<Intersector>(m_backMesh.get(), m_interfMesh.get(), tolrescaling, verbose);
    }

    MPI_Bcast(&m_nbPoints, 1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&m_nbCoor, 1, MPI_INT, 0, MpiInfo::petscComm());
  }

  void Immersed::setInterfaceExchangedData(
    std::vector<double>* intfDisp, 
    std::vector<double>* intfVelo, 
    std::vector<double>* intfForc, 
    std::vector<double>* velFluid
    ) {

    m_intfDisp = intfDisp;
    m_intfVelo = intfVelo;
    m_intfForc = intfForc;
    m_velFluid = velFluid;

    m_intfDisp->resize(m_nbCoor*m_nbPoints,0.);
    m_intfVelo->resize(m_nbCoor*m_nbPoints,0.);
  }

  void Immersed::updateInterface(GeometricMeshRegion& mesh) {

    FEL_CHECK(m_nbPoints != 0,"Empty list of immersed points");

    if ( MpiInfo::rankProc() == 0 ) {
      // WARNING: the interpolation only works so far for tetrahedral (in 3D) and triangular (in 2D) meshes
      if ( m_nbVertices == 0 ) {
        m_nbVertices = mesh.domainDim()+1; // number of coordinates = dimension + 1
      }

      // update interface points
      for (int l = 0; l < m_nbPoints; ++l) {
        for (int ic = 0; ic < m_nbCoor; ++ic)
          m_listPoints[l].coor(ic) = m_listPointsRef[l].coor(ic) + (*m_intfDisp)[ic+l*m_nbCoor];
      }

      // update the mesh with m_intfDisp
      m_interfMesh->moveMesh(*m_intfDisp, 1); // move the immersed structure mesh
      m_buildLumpedMassMatrixOfStructure(); // update the lumped mass matrix of the immersed structure mesh

      // interpolation of the immersed structure mesh
      m_interpolator->interpolate(m_indElt, m_intOp_val, m_listPoints);
    }
  }

  void Immersed::updateIntersection(GeometricMeshRegion& mesh, PetscVector& delta, const MPI_Comm& comm) {

    // TODO remove mesh and use m_backMesh

    // only first process will modify the value of the PetscVector delta and will write the files "intersection"
    if ( MpiInfo::rankProc() == 0 ) {

      // locate the elements near the structure interface and the ones in star of the previous elements where the SUPG-PSPG and div-div stabilization will be modified
      std::vector<felInt> elementID(m_interfMesh->numPoints());
      std::set<GeometricMeshRegion::seedElement> elementIDstar;
      double coef = 1./FelisceParam::instance().localDivDivPenalty;
      delta.set(1.0);
      m_intersector->intersectMeshes(m_indElt, elementID);

      for (std::size_t i=0; i < elementID.size(); ++i) {
        delta.setValue(elementID[i], coef, INSERT_VALUES);
        mesh.getElementPatch(mesh.bagElementTypeDomain()[0], elementID[i], elementIDstar);
        for ( const auto& elt : elementIDstar )
          delta.setValue(elt.second, coef, INSERT_VALUES);
      }

      if ( FelisceParam::instance().divDivStabPostProcess ) {
        m_divDivStabPostProcess(elementID);
      }

      ++m_indexTime;
    }

    // once the PetscVector delta has been modified by the main process, broadcast it to all other processes of the communicator
    delta.broadCastSequentialVector(comm, 0);
  }

  void Immersed::applyPenaltyToMatrixAndRHS(AO& ao, Dof& dof, int idVarVel, PetscMatrix& matrix, PetscVector& rhs, bool only_rhs) {

    double lambda_i, lambda_j;
    double eps = FelisceParam::instance().penaltyCoeffImmersedStruct;
    int k, ig, jg;
    double val = 0.0, coef = 1.0;

    if ( MpiInfo::rankProc() == 0 ) { // only the first processor add the "penalized" terms to the NS matrix

      // matrix and rhs contributions for NS matrix
      for (int l=0; l< m_nbPoints; ++l) {
        k = m_indElt[l]; // the element containing the point
        if ( k != -1 ) { // if the interpolation of the immersed structure mesh into the fluid mesh has been successful

          // implicit method
          if ( FelisceParam::instance().fsiRNscheme == -1000 ) {
            if ( FelisceParam::instance().useMassMatrix == 1 ) { // with use of the lumped mass matrix in the fourth block of the fluid problem matrix
              coef = 1.0/(eps*m_lumpedMassMatrix[l]);
            }
            else { // with only penalisation coefficient in the fourth block of the fluid problem matrix
              coef = 1.0/eps;
            }
          }

          // explicit method without penalisation coefficient
          else if ( FelisceParam::instance().fsiRNscheme == 1 ) {
            coef = (m_lumpedMassMatrix[l]*(FelisceParam::instance().densitySolid)*(FelisceParam::instance().thickness))/(FelisceParam::instance().timeStep);
          }


          // if the specified fsiRNscheme has not been recognized, returns an error
          else {
            std::ostringstream tmp;
            tmp << FelisceParam::instance().fsiRNscheme;
            std::string fsiRNscheme_as_string(tmp.str());
            FEL_ERROR(("Immersed::computePenaltyForce: value of fsiRNscheme = " + fsiRNscheme_as_string + " not recognized! STOP HERE.\n"));
          }

          //
          for (int i=0; i < m_nbVertices; ++i) {
            lambda_i = m_intOp_val[m_nbVertices*l+i ];

            if ( not only_rhs ) {
              // matrix
              for (int j=0; j < m_nbVertices; ++j) {
                lambda_j =  m_intOp_val[m_nbVertices*l+j ];
                val = coef*lambda_i*lambda_j; ///(1 + eps*coef);
                for (int ic=0; ic < m_nbCoor; ++ic){
                  dof.loc2glob(k, i, idVarVel, ic,ig);
                  dof.loc2glob(k, j, idVarVel, ic,jg);
                  AOApplicationToPetsc(ao,1,&ig);
                  AOApplicationToPetsc(ao,1,&jg);
                  matrix.setValues(1,&ig,1,&jg,&val,ADD_VALUES);
                }
              }
            }

            // rhs
            for (int ic=0; ic < m_nbCoor; ++ic) {

              // implicit method
              if ( FelisceParam::instance().fsiRNscheme == -1000 ) {
                val = coef*lambda_i*(*m_intfVelo)[ic + l*m_nbCoor ];
              }

              // explicit method without penalisation coefficient
              else if ( (FelisceParam::instance().fsiRNscheme == 1) || (FelisceParam::instance().fsiRNscheme == -1) ) {
                val = coef*lambda_i*(*m_intfVelo)[ic + l*m_nbCoor ] - lambda_i*m_intfForcOld[ic + l*m_nbCoor ];
              }

              // if the specified fsiRNscheme has not been recognized, returns an error
              else {
                std::ostringstream tmp;
                tmp << FelisceParam::instance().fsiRNscheme;
                std::string fsiRNscheme_as_string(tmp.str());
                FEL_ERROR(("Immersed::computePenaltyForce: value of fsiRNscheme = " + fsiRNscheme_as_string + " not recognized! STOP HERE.\n"));
              }

              dof.loc2glob(k,i, idVarVel, ic,ig);
              AOApplicationToPetsc(ao,1,&ig);
              rhs.setValues(1,&ig,&val,ADD_VALUES);
            }
          }
        }
      }
    } // end of MpiInfo::rankProc() == 0

    matrix.assembly(MAT_FINAL_ASSEMBLY);
    rhs.assembly();
  }

  void Immersed::computePenaltyForce(AO& ao, Dof& dof, int idVarVel, PetscVector& seqVel) {

    double lambda_i;
    double eps = FelisceParam::instance().penaltyCoeffImmersedStruct;

    int k, ig;
    double val = 0.0, coef = 1.0;
    std::vector<double> aux(m_nbPoints*m_nbCoor);

    if ( MpiInfo::rankProc() == 0 ) { // only the first processor add the "penalized" terms to the NS matrix

      // compute
      for (int l=0; l< m_nbPoints; ++l) {
        k= m_indElt[l]; // the element containing the point
        if ( k != -1 ) {

          // implicit method
          if ( FelisceParam::instance().fsiRNscheme == -1000 ) {
            if ( FelisceParam::instance().useMassMatrix == 1 ) { // with use of the lumped mass matrix in the fourth block of the fluid problem matrix
              coef = 1.0/(eps*m_lumpedMassMatrix[l]);
            }
            else { // with only penalisation coefficient in the fourth block of the fluid problem matrix
              coef = 1.0/eps;
            }
          }

          // explicit method without penalisation coefficient
          else if ( FelisceParam::instance().fsiRNscheme == 1 ) {
            coef = (m_lumpedMassMatrix[l]*(FelisceParam::instance().densitySolid)*(FelisceParam::instance().thickness))/(FelisceParam::instance().timeStep);
          }

          // if the specified fsiRNscheme has not been recognized, returns an error
          else {
            std::ostringstream tmp;
            tmp << FelisceParam::instance().fsiRNscheme;
            std::string fsiRNscheme_as_string(tmp.str());
            FEL_ERROR(("Immersed::computePenaltyForce: value of fsiRNscheme = " + fsiRNscheme_as_string + " not recognized! STOP HERE.\n"));
          }

          for (int ic=0; ic < m_nbCoor; ++ic) {
            (*m_velFluid)[l*m_nbCoor + ic] = 0.0;

            for (int i=0; i < m_nbVertices; ++i) {
              lambda_i =  m_intOp_val[m_nbVertices*l+i ];
              dof.loc2glob(k,i, idVarVel, ic,ig);
              AOApplicationToPetsc(ao,1,&ig);
              seqVel.getValues(1,&ig,&val);
              (*m_velFluid)[l*m_nbCoor + ic] += val*lambda_i;
            }

            // implicit method
            if ( FelisceParam::instance().fsiRNscheme == -1000 ) {
              (*m_intfForc)[l*m_nbCoor + ic] = coef*((*m_velFluid)[l*m_nbCoor + ic] - (*m_intfVelo)[l*m_nbCoor + ic]);
            }

            // explicit method without penalisation coefficient
            else if ( FelisceParam::instance().fsiRNscheme == 1 ) {
              (*m_intfForc)[l*m_nbCoor + ic] = coef*((*m_velFluid)[l*m_nbCoor + ic] - (*m_intfVelo)[l*m_nbCoor + ic]) + m_intfForcOld[l*m_nbCoor + ic] ;
            }

            // if the specified fsiRNscheme has not been recognized, returns an error
            else {
              std::ostringstream tmp;
              tmp << FelisceParam::instance().fsiRNscheme;
              std::string fsiRNscheme_as_string(tmp.str());
              FEL_ERROR(("Immersed::computePenaltyForce: value of fsiRNscheme = " + fsiRNscheme_as_string + " not recognized! STOP HERE.\n"));
            }
          }
        }

        else {

          std::ostringstream tmp;
          tmp << l;
          std::string l_as_string(tmp.str());
          FEL_ERROR(("Immersed::computePenaltyForce: it seems that the interpolation table met some problems for localising the structure node of label " + l_as_string + ", hence the m_intForc std::vector has been std::set to 0! STOP HERE.\n"));
        }
      }

      // explicit method without penalisation coefficient
      if ( FelisceParam::instance().fsiRNscheme == 1 ) {
        m_intfForcOld = (*m_intfForc);
      }
    }
  }

  void Immersed::m_readInterfaceMesh() {
    std::string inputDirectory1 = "./";
    std::string inputFile1 = "";
    std::string inputMesh1 = FelisceParam::instance().meshFileImmersedStruct;
    std::string outputMesh1 = "toto.geo";
    std::string meshDir1 = FelisceParam::instance().meshDirImmersedStruct;
    std::string resultDir1 = "";
    std::string prefixName1 = "";

    IO io(inputDirectory1, inputFile1, inputMesh1, outputMesh1, meshDir1, resultDir1, prefixName1);

    m_interfMesh = felisce::make_shared<GeometricMeshRegion>();
    io.readMesh(*m_interfMesh, 1.0);

    std::vector<Point>& listPoints = m_interfMesh->listPoints();
    m_nbPoints = listPoints.size();

    m_listPointsRef = listPoints;
    m_listPoints    = listPoints;

    m_nbCoor = m_interfMesh->domainDim()+1; // number of coordinates = dimension + 1 // TODO D.C. doesn't work for .geo mesh

    m_nbVertices = 0; // set in method updateInterface

    // whent the method used is the implicit one with use of the lumped mass matrix in the fourth block of the fluid problem matrix OR the explicit method
    if ( (( FelisceParam::instance().fsiRNscheme == -1000 ) && ( FelisceParam::instance().useMassMatrix == 1 ) ) || ( FelisceParam::instance().fsiRNscheme == 1 ) ) { //
      m_buildLumpedMassMatrixOfStructure(); // build the lumped mass matrix of the immersed structure mesh

      if ( FelisceParam::instance().fsiRNscheme == 1) { // if explicit method
        m_intfForcOld.resize(m_nbCoor*m_nbPoints, 0); // std::vector to store the previous force required for the computation of the new force in the explicit scheme
      }
    }
  }

  void Immersed::m_buildLumpedMassMatrixOfStructure() {

    // The purpose of this method is to build the lumped mass matrix of the structure used into the semi-implicit/explicit scheme

    if ( FelisceParam::verbose() > 1 ) {
      std::cout << "  -- Building of the lumped mass matrix of the structure..." << std::endl;
    }

    // initialization of the lumped mass matrix of the structure
    m_lumpedMassMatrix.resize(m_nbPoints);
    for(int l = 0; l < m_nbPoints; ++l) {
      m_lumpedMassMatrix[l] = 0.;
    }

    typedef felisce::GeometricMeshRegion::ElementType ElementType;
    ElementType eltType; // geometric element type in the mesh.
    int numPointsPerElt = 0; // number of points per geometric element.
    std::vector<Point*> elemPoint; // points of the current element.
    std::vector<felInt> elemIdPoint; // id of points of the element.

    // first loop on geometry type.
    const std::vector<GeometricMeshRegion::ElementType>& bagElementTypeDomain = m_interfMesh->bagElementTypeDomain(); // list of domain element types in the mesh
    for (std::size_t i = 0; i < bagElementTypeDomain.size(); ++i) { // for each domain element type
      eltType = bagElementTypeDomain[i];

      // resize array.
      numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      elemPoint.resize(numPointsPerElt, nullptr);
      elemIdPoint.resize(numPointsPerElt, 0);

      int typeOfFiniteElement = 0;
      const GeoElement *geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement* refEle = geoEle->defineFiniteEle(eltType, typeOfFiniteElement, *m_interfMesh);
      CurvilinearFiniteElement fe(*refEle, *geoEle, DegreeOfExactness_0);

      // second loop on the number of elements for each geometry type.
      for(int j = 0; j<m_interfMesh->numElements(eltType); ++j) { // for each element
        m_interfMesh->getOneElement(eltType, j, elemIdPoint, 0);
        for(int iPoint = 0; iPoint < numPointsPerElt; iPoint++) {
          elemPoint[iPoint] = &m_interfMesh->listPoints()[elemIdPoint[iPoint]];
        }
        fe.updateMeas(0, elemPoint); // update the measure
        for(int iPoint = 0; iPoint < numPointsPerElt; ++iPoint) {
          m_lumpedMassMatrix[elemIdPoint[iPoint]] += fe.measure()/numPointsPerElt;
        }
      }
    }

    if ( FelisceParam::verbose() > 1 ) {
      std::cout << "  -- The lumped mass matrix of the structure for the immersed method has been successfully built!" << std::endl;
    }
  }

  void Immersed::m_divDivStabPostProcess(std::vector<felInt>& elementID){

    double dt = FelisceParam::instance().timeStep;

    std::set<GeometricMeshRegion::seedElement> elementIDstar;
    std::vector<felInt> value(m_backMesh->getNumDomainElement(),0.);

    for (std::size_t i=0; i < elementID.size(); ++i) {
      value[elementID[i]] = 1;
      m_backMesh->getElementPatch(m_backMesh->bagElementTypeDomain()[0], elementID[i], elementIDstar);
      for (const auto& element : elementIDstar)
        value[element.second] = 1;
    }

    {
      // write case 
      FILE * pFile;
      std::string fileName, fileNameDir, fileNameCase;
      
      fileNameDir = FelisceParam::instance().resultDir;
      fileNameCase = fileNameDir+"intersection.case";

      pFile = fopen (fileNameCase.c_str(),"w");
      if (pFile==NULL) {
        std::string command = "mkdir -p " + fileNameDir;
        int ierr = system(command.c_str());
        if( ierr > 0)
          FEL_ERROR("Impossible to write "+fileNameCase+" case file");
        pFile = fopen (fileNameCase.c_str(),"w");
      }
      fprintf( pFile,"FORMAT\n");
      // fprintf( pFile,"type: ensight\n");
      fprintf( pFile,"type: ensight gold\n");
      fprintf( pFile,"GEOMETRY\n");
      // fprintf( pFile,"model: 1 fluid.geo.*****.geo\n");
      fprintf( pFile,"model: 1 fluid.geo\n");
      fprintf( pFile,"VARIABLE\n");
      fprintf( pFile,"scalar per element: 1 intersection intersection.*****.scl\n");
      fprintf( pFile,"TIME\n");
      fprintf( pFile, "time set: %d\n", 1);
      fprintf( pFile,"number of steps: %d \n", m_indexTime+1);
      fprintf( pFile, "filename start number: %d\n", 0);
      fprintf( pFile, "filename increment: %d\n", 1);
      fprintf( pFile, "time values:\n");
    
      int count=0;
      for (int i=0; i < m_indexTime+1; ++i) {
        fprintf( pFile, "%12.5e",i*dt);
        ++count;
        if ( count == 6) {
          fprintf( pFile, "\n" );
          count=0;
        }
      }
      fclose(pFile);
    }

    {
      std::string fileName = FelisceParam::instance().resultDir + "intersection";
      std::stringstream oss;
      oss << m_indexTime;
      std::string aux = oss.str();
      if ( m_indexTime < 10 ) {
        aux = "0000" + aux;
      } else if ( m_indexTime < 100 ) {
        aux = "000" + aux;
      } else if ( m_indexTime < 1000 ) {
        aux = "00" + aux;
      } else if ( m_indexTime < 10000 ) {
        aux = "0" + aux;
      }

      fileName = fileName + "." + aux + ".scl";
      FILE * pFile;
      pFile = fopen (fileName.c_str(),"w");

      if (pFile==nullptr) {
        FEL_ERROR("Impossible to write " + fileName + " scalar ensight solution");
      }

      fprintf(pFile, "Scalar per element\n");
      fprintf(pFile, "part\n");
      fprintf(pFile, "%10d\n",1); // change the value at the end of the line depending on the part in your .geo file which will receive the "intersection" variable for the post-processing part
      if (m_backMesh->domainDim() == 2) { // if the fluid mesh is a 2D mesh
        fprintf(pFile, "tria3\n"); // consider the triangle elements
      }
      else if (m_backMesh->domainDim() == 3) { // if the fluid mesh is a 3D mesh
        fprintf(pFile, "tetra4\n"); // consider the tetraedra elements
      }

      for (std::size_t i=0; i < value.size(); ++i) {
        fprintf(pFile,"%d\n", value[i]);
      }

      fprintf(pFile, "\n");
      fclose(pFile);
    }
  }
}

