//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/ARDModel.hpp"

namespace felisce {
  ARDModel::ARDModel():Model() {
    m_name = "ARD";
  }

  ARDModel::~ARDModel() = default;


  void ARDModel::forward() {
    // Write solution for postprocessing (if required)
    writeSolution();

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();

    //Assembly loop on elements.
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc());

    //Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    //m_linearProblem[0]->eigenvalue();
    //Solve linear system.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
  }

  int ARDModel::getNstate() const {
    return m_linearProblem[0]->numDof();
  }

  void ARDModel::getState(double* & state) {
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }

  void ARDModel::getState_swig(double* data, felInt numDof) {
    double * state;
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
    for (felInt i=0 ;  i<numDof ; i++) {
      data[i] = state[i];
    }
  }

  void ARDModel::setState(double* & state) {
    m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
}
