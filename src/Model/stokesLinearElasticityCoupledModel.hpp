//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __STOKESLINEARELASTICITYCOUPLED_HPP__
#define __STOKESLINEARELASTICITYCOUPLED_HPP__

// System includes

// External includes

// Project includes
#include "Model/linearElasticityModel.hpp"
#include "Solver/linearProblemNSRS.hpp"

namespace felisce {
  class StokesLinearElasticityCoupledModel :
    public LinearElasticityModel {
  public:
    // ==================================================================
    // methods to initialize the whole coupled model                    =
    // - constructor                                                    =
    // - redefinition of the virtual initializeDerivedModel             =
    // - function that initializes m_labelStokes2NS std::unordered_map                 =
    // ==================================================================
    StokesLinearElasticityCoupledModel();
    void initializeDerivedModel() override;
    void defineMapLabelInterface();
    // ==================================================================
    // functions to correctly manage the subiterations                  = 
    // ==================================================================
    void forward() override;
  private:
    ChronoInstance::Pointer m_timeChecker, m_t1, m_t2;
    
    std::vector<double> m_cumulatedForwardTime;
    std::vector<double> m_cumulatedStokesTime;
    std::vector<double> m_cumulatedLETime;
    
    void writeTimeInfo();
    // ==================================================================
    // finalize is called into the forward                              =
    // ==================================================================
    void finalizeForward(int cIteration, int cGmresItNS, int cGmresItPF);
    // ==================================================================
    // Method to solve one sub-iteration of the Stokes                  =
    // ==================================================================
    void solveStokes(int nfp);
    // ==================================================================
    //  Function displaying a banner for each sub-iteration             =
    // ==================================================================
    void displayBannerIterationInit(felInt cIt) const ;
    void displayBannerIterationEnd(felInt cIt,
                                   double testQuantity, 
                                   double testUN, 
                                   double testIOP) const;
    // ==================================================================
    //  In this function the matrix and/or the rhs are cleared, used to =
    //  correctly manage re-using of matrices and vectors in the        =
    //  sub-iterations                                                  =
    // ==================================================================
    void clearMatrixRHSOfPbs( FlagMatrixRHS flag);

    // ==================================================================
    // id of the stokes problem into the std::vector of                      =
    // linear problems, the corresponding variable m_idLE               =
    // is already defined into linearElasticityModel class              =
    // ==================================================================
    felInt m_idStokes;
    // ==================================================================
    // Pointers to the two linear problems after static_cast.           =
    // Usefull when calling functions not present in linearProblem.hpp  =
    // ==================================================================
    LinearProblemLinearElasticity *m_lpbLE;
    LinearProblemNSRS *m_lpbStokes;
    // ==================================================================
    // std::unordered_map storing the mapping between labels of iop mesh and ns mesh   =
    // example:                                                         = 
    //   correspondingLabelFromLEMesh = m_labStokes2LE[labelFromStokesMesh];  =
    // ==================================================================
    std::map<int,int> m_labStokes2LE;
    // ==================================================================
    // these vectors are here to observe the number of subiterations    =
    // for each time step                                               =
    // ==================================================================
    std::vector<int> m_numFixedPointItPerTimeStep;
    std::vector<int> m_totNumOfGMRESItPerTimeStepLE;
    std::vector<int> m_totNumOfGMRESItPerTimeStepStokes;
    std::vector<int> m_interfaceLabels;
    void prepareForwardStokes();
  };
}
#endif
