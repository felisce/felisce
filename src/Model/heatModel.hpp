//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _HEATMODEL_HPP
#define _HEATMODEL_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemHeat.hpp"

namespace felisce {
  class HeatModel:
    public Model {
  public:
    ///Construtor.
    HeatModel();
    ///Destructor.
    ~HeatModel() override= default;;
    /// Manage time iteration.
    void forward() override;
    void solveHeat();
  protected:
    LinearProblemHeat* m_lpb;
  private:
    void prepareForward();
    void preAssemblingMatrixRHS(std::size_t iProblem) override;
    void initializeDerivedModel() override;
#ifdef FELISCE_WITH_CVGRAPH
  private:
    void cvgraphNewTimeStep() override;
#endif
  public:
    // State functions..
    /// Function to get size of the state std::vector.
    int getNstate() const override;
    /// Function to get state std::vector.
    void getState(double* & state) override;
    void getState_swig(double* data, felInt numDof);
    /// Function to std::set state std::vector.
    void setState(double* & state) override;
  };
}

#endif
