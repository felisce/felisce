//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

#ifndef _BIDOMAINCURVMODEL_HPP
#define _BIDOMAINCURVMODEL_HPP

// System includes
#include <vector>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/model.hpp"
#include "Solver/schafSolver.hpp"
#include "Solver/linearProblemBidomainCurv.hpp"
#include "Solver/FhNSolver.hpp"
#include "Solver/courtemancheSolver.hpp"
#include "Solver/bdf.hpp"

namespace felisce {
  class BidomainCurvModel:
    public Model {
  public:
    ///Construtor.
    BidomainCurvModel();
    ///Destructor.
    ~BidomainCurvModel() override;
    void initializeDerivedLinearProblem() override;
    void initializeDerivedModel() override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void writeSolution();
    /// Manage time iteration.
    void forward() override;
    /// Initialize the SchafSolver and evaluate heterogeneous parameters of SchafSolver.
    void initializeSchafParameter();
    /// Initialize the FithughNagumo and evaluate heterogeneous parameters of SchafSolver.
    void initializeFhNParameter();
    /// Initialize the CourtemancheSolver and evaluate heterogeneous parameters of CourtemancheSolver.
    void initializeCourtParameter();
    void finalizeRHSDP(std::size_t iProblem=0);
    virtual void initializeAppCurrent();
    virtual void initializeAppCurrentExt();
    void evalIapp();
    void evalIappExt();
    void setInitialCondition() override;
  protected:
    AppCurrentAtria* m_iApp;
    AppCurrentAtria* m_iAppExt;
    std::vector<LinearProblemBidomainCurv*> m_linearProblemBidomainCurv;
  private:
    ///EDO solvers.
    IonicSolver* m_ionic;
    SchafSolver* m_schaf;
    FhNSolver* m_fhn;
    CourtemancheSolver* m_court;
    ///EDP time scheme.
    Bdf m_bdfEdp;
    ///Initialization vectors.
    PetscVector m_W_0;

    //For courtemanche model:
    //Gate conditions
    // INa
    PetscVector m_m;
    PetscVector m_h;
    PetscVector m_j;
    // ITo
    PetscVector m_ao;
    PetscVector m_io;
    // IKur
    PetscVector m_ua;
    PetscVector m_ui;
    // IKr
    PetscVector m_xr;
    // IKs
    PetscVector m_xs;
    // ICaL
    PetscVector m_d;
    PetscVector m_f;
    PetscVector m_fca;
    // Irel
    PetscVector m_urel;
    PetscVector m_vrel;
    PetscVector m_wrel;

    // Ion concentrations
    PetscVector m_nai;
    PetscVector m_nao;
    PetscVector m_cao;
    PetscVector m_ki;
    PetscVector m_ko;
    PetscVector m_cai;
    //Currents
    PetscVector m_naiont;
    PetscVector m_kiont;
    PetscVector m_caiont;
    PetscVector m_ileak;
    PetscVector m_iup;
    PetscVector m_itr;
    PetscVector m_irel;
    // Ca:
    PetscVector m_cmdn;   /* Calmodulin Buffered Ca Concentration (mM) */
    PetscVector m_trpn;   /* Troponin Buffered Ca Concentration (mM) */
    PetscVector m_nsr;    /* NSR Ca Concentration (mM) */
    PetscVector m_jsr;    /* JSR Ca Concentration (mM) */
    PetscVector m_csqn;   /* Calsequestrin Buffered Ca Concentration (mM) */

    //Solution potExtraCell
    PetscVector m_solExtraCell;

    ///Data for schaf solver from EDP solution.
    PetscVector m_extrapolate;
    bool m_buildIonic;
    bool m_buildCourt;
    int m_verbose;
    std::vector <double> m_iAppValue;
    std::vector <double> m_iAppValueExt;
  };
}


#endif
