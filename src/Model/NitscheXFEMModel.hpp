//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. A. Fernandez, F.M. Gerosa
//

/*!
 \file  linearProblemNitscheXFEM.cpp
 \authors M. A. Fernandez, F.M. Gerosa
 \date  04/2018
 \brief Model class for Navier-Stokes with Nitsche XFEM formulation
 */

#ifndef _NitscheXFEMModel_HPP
#define _NitscheXFEMModel_HPP

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "DegreeOfFreedom/duplicateSupportDof.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Model/model.hpp"
#include "Solver/linearProblemNitscheXFEM.hpp"


namespace felisce {

  class NitscheXFEMModel:
    public Model 
  {
    
    public:
    
      // Constructor
      NitscheXFEMModel();

      // Destructor
      ~NitscheXFEMModel();

      // Intersect the meshes and set the informations in the two solvers
      void initializeDerivedLinearProblem() override;

      void initializeDerivedModel() override;

      void preAssemblingMatrixRHS(size_t iProblem = 0) override;

      void forward() override;
      
      void prepareForward();

      void solve();

      void solveLinearized(bool linearFlag);
      
      void writeSolutionAndMeshes();

      inline void setInterfaceExchangedData(std::vector<double>& intDisp) { m_intfDisp = &intDisp; };

      inline GeometricMeshRegion& interfMesh(){ return  m_interfMesh; };
      
    private:

      // booleans to know if a vector has been allocated
      bool m_isParFluidVelAllocated;

      // vector containing the fluid velocities of the previous time step in parallel.
      // Each PetscVector can have a different size because the number of duplicated element are different.
      // The goal is to compute the fluid velocity (scaled by a coefficient) in each element, taking 
      // into account the duplication at the previous time instants.
      // The size is the same as the order of the time scheme.
      // It is used in the fluid problem to compute:
      //   - RHS coming from the time derivatives
      //   - advection term 
      //   - Temam's trick
      //   - SUPG stabilization
      //   - Inflow stabilization (to compute u^* \cdot n)
      //   - Coefficients of the CIP stabilization
      std::vector<PetscVector> m_parFluidVel;

      // Pointer to the fluid and solid linear problems
      LinearProblemNitscheXFEM* m_pLinFluid;

      // The interface mesh
      GeometricMeshRegion m_interfMesh; // TODO D.C. the model can already have more than one mesh...
      
      // bdf for the fluid and solid
      Bdf m_bdfFluid;

      // Information about the duplication of the support elements
      DuplicateSupportDof m_intersectMeshes;

      // Finite elements for the fluid and solid
      CurvilinearFiniteElement* m_feStruc;

      felInt m_indexTime;

      felInt m_countToInitOldObjects;
      bool m_reinitializeFluidBdf;

      // interface displacement from master
      std::vector<double> *m_intfDisp;
  };
}

#endif
