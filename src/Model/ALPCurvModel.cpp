//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/ALPCurvModel.hpp"

namespace felisce {
  ALPCurvModel::ALPCurvModel():
    ALPModel()
  {}

  ALPCurvModel::~ALPCurvModel()
  = default;

  void ALPCurvModel::initializeEigenProblem(std::vector<EigenProblemALPCurv*> eigenPb) {
    std::unordered_map<std::string, int> mapOfType;
    mapOfType["EXPLICIT_EULER"] = 0;
    mapOfType["EXPLICIT_EULER_MONOLITHIC"] = 1;
    mapOfType["EXPLICIT_EULER_MONOLITHIC2"] = 2;
    mapOfType["IMPLICIT_EULER"] = 3;
    mapOfType["BACKWARD_DF_2"] = 4;

    m_method = mapOfType[FelisceParam::instance().integrationTimeMethod];

    for (std::size_t i=0; i<eigenPb.size(); i++) {
      m_eigenProblem.push_back(eigenPb[i]);
    }

    for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
      //Define linear problem
      //=======================
      m_eigenProblem[ipb]->initialize(mesh(), m_fstransient, MpiInfo::petscComm());
      if (FelisceParam::instance().solver.size() < eigenPb.size()) {
        m_eigenProblem[ipb]->fixIdOfTheProblemSolver(0);
      } else
        m_eigenProblem[ipb]->fixIdOfTheProblemSolver(ipb);

      //Compute Degrees of freedom (DOF) the problem
      //=========================================
      m_eigenProblem[ipb]->computeDof(MpiInfo::numProc(), MpiInfo::rankProc());
    }
    // Specific initalization of the user model
    initializeDerivedModel();

    //Define initial conditions
    //=========================================
    if (FelisceParam::instance().hasInitialCondition) {
      for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
        for (std::size_t ii = 0; ii < FelisceParam::instance().valueInitCond.size(); ii++) {
          m_initialCondition.addVariable(*m_eigenProblem[ipb]->listVariable().getVariable(FelisceParam::instance().nameVariableInitCond[ii]));
        }
      }
      m_initialCondition.print(FelisceParam::verbose(),std::cout);
    }

    for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
      //Degrees of freedom partitionning with ParMetis
      //===============================================
      m_eigenProblem[ipb]->cutMesh();
      //Allocate memory for the matrix m_Matrix and std::vector m_RHS in the linearProblem class
      //============================================================

      m_eigenProblem[ipb]->allocateMatrix();

    }
    setExternalVec(); // make the connection between the different linear pb (to be defined in the derived class)
    std::cout << std::endl;
    for (std::size_t ipb = 0; ipb < eigenPb.size(); ipb++) {
      //Apply specific operations before assembling
      //===========================================
      preAssemblingMatrixRHS(ipb);

      // Gather to write initial solution.
      m_eigenProblem[ipb]->gatherSolution();

    }

    for(std::size_t iio=0; iio<m_ios.size(); ++iio)
      m_ios[iio]->initializeOutput();

    for(std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      m_eigenProblem[ipb]->clearMatrix();
    }
    setInitialCondition();

    for(std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      m_eigenProblem[ipb]->setIntegrationMethod(m_method);
    }

    m_fstransient->iteration=0;

    m_eigenProblemIsInitialized = true;
  }

  void ALPCurvModel::forward() {
    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      if ( m_fstransient->iteration == 0 ) {
        // Initialize Rom object and calculate reduced basis

        // Read initial data
        m_eigenProblem[ipb]->readData(*io());
        preAssembleMatrix(ipb);
        // Create _Matrix[0] and _Matrix[1] of m_eigenProblem
        m_eigenProblem[ipb]->assembleMatrixBD();

        // Read basis from ensight files
        if (FelisceParam::instance().readBasisFromFile) {
          // Build basis reading vectors from ensight files
          m_eigenProblem[ipb]->initializeROM();
        }
        // Calculate basis functions solving Schrodinger equation
        else {
          if (MpiInfo::rankProc() == 0) {
            if (m_meshIsWritten == false) writeMesh();
          }
          // Initialize Slepc solver
          m_eigenProblem[ipb]->buildSolver();
          // Solve with slepc the generilized eigen problem: _Matrix[0] v = m_matrix[1] lambda v
          m_eigenProblem[ipb]->solve();
          // Writes modes (eigenvectors) in ensight format
          m_eigenProblem[ipb]->writeMode();
          m_eigenProblem[ipb]->initializeSolution();
        }

        if (FelisceParam::instance().hasSource)
          postAssembleMatrix(ipb);

        m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->computeTensor();

        m_eigenProblem[ipb]->computeGamma();

        m_eigenProblem[ipb]->computeMatrixM();

        switch (m_method) {
        case 0: // EXPLICIT_EULER
          break;
        case 1: // EXPLICIT_EULER_MONOLITHIC
        case 2: // EXPLICIT_EULER_MONOLITHIC2
        case 3: // IMPLICIT_EULER
        case 4: // BACKWARD_DF_2
          m_eigenProblem[ipb]->initializeSystemSolver();
          break;
        default:
          FEL_ERROR("This integration method is not implemented.");
          break;
        }

        if (FelisceParam::instance().writeECG) {
          m_eigenProblem[ipb]->initializeECG();
          m_eigenProblem[ipb]->writeECG(m_fstransient->iteration);
        }

      }
    }

    //Write solution with ensight.
    ALPModel::writeSolution();
    //Advance time step.
    updateTime();
    printNewTimeIterationBanner();


    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      switch (m_method) {
      case 0: // EXPLICIT_EULER
        m_eigenProblem[ipb]->updateBasis();

        //m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->updateBeta();

        m_eigenProblem[ipb]->updateEigenvalue();

        m_eigenProblem[ipb]->updateTensor();

        m_eigenProblem[ipb]->computeGamma();

        // Compute Matrix M
        m_eigenProblem[ipb]->computeMatrixM();

        break;
      case 1: // EXPLICIT_EULER_MONOLITHIC
        m_eigenProblem[ipb]->updateBasis();

        //m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->updateBetaMonolithic();

        m_eigenProblem[ipb]->updateEigenvalue();

        m_eigenProblem[ipb]->updateTensor();

        m_eigenProblem[ipb]->computeGamma();

        // Compute Matrix M
        m_eigenProblem[ipb]->computeMatrixM();

        break;
      case 2: // EXPLICIT_EULER_MONOLITHIC2
        m_eigenProblem[ipb]->updateBasis();

        //m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->updateBetaMonolithic();

        m_eigenProblem[ipb]->updateTensor();

        m_eigenProblem[ipb]->updateEigenvalue();

        m_eigenProblem[ipb]->computeGamma();

        // Compute Matrix M
        m_eigenProblem[ipb]->computeMatrixM();

        break;
      case 3: // IMPLICIT_EULER
        m_eigenProblem[ipb]->updateBasis();

        //m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->updateBetaEI();

        m_eigenProblem[ipb]->updateTensor();

        m_eigenProblem[ipb]->updateEigenvalue();

        m_eigenProblem[ipb]->computeGamma();

        // Compute Matrix M
        m_eigenProblem[ipb]->computeMatrixM();

        break;
      case 4: // BACKWARD_DF_2
        m_eigenProblem[ipb]->updateBasis();

        //m_eigenProblem[ipb]->computeMatrixZeta();

        m_eigenProblem[ipb]->computeGammaExtrap();

        m_eigenProblem[ipb]->updateBetaBdf2();

        m_eigenProblem[ipb]->updateTensor();

        m_eigenProblem[ipb]->updateEigenvalue();

        m_eigenProblem[ipb]->computeGamma();

        // Compute Matrix M
        m_eigenProblem[ipb]->computeMatrixM();

        break;
      default:
        FEL_ERROR("This integration method is not implemented.");
        break;
      }

      if (FelisceParam::instance().writeECG) {
        m_eigenProblem[ipb]->updateEcgOperator();
        m_eigenProblem[ipb]->writeECG(m_fstransient->iteration);
      }

    }

  }

  void ALPCurvModel::solveEigenProblem() {
    //Write solution with ensight.
    if (MpiInfo::rankProc() == 0) {
      if (m_meshIsWritten == false) writeMesh();
    }
    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++) {
      // Read initial data
      m_eigenProblem[ipb]->readData(*io());
      // Create m_Matrix[0] and m_Matrix[1] of m_eigenProblem
      m_eigenProblem[ipb]->assembleMatrixBD();
      // Initialize Slepc solver
      m_eigenProblem[ipb]->buildSolver();
      // Solve with slepc the generilized eigen problem: m_Matrix[0] v = m_matrix[1] lambda v
      m_eigenProblem[ipb]->solve();
      // Writes modes (eigenvectors) in ensight format
      m_eigenProblem[ipb]->writeMode();
    }

    for (std::size_t ipb = 0; ipb < m_eigenProblem.size(); ipb++)
      m_eigenProblem[ipb]->checkBasis();

  }


}
