//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _ALPMODEL_HPP
#define _ALPMODEL_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/model.hpp"
#include "Solver/eigenProblem.hpp"
#include "Solver/eigenProblemALP.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  /*!
   \file ALPModel.hpp
   \authors E. Schenone
   \date 10/12/2012
   \brief ALP model class manages Approximated Lax Pairs decomposition.
   */
  class ALPModel:
    public Model {
  public:
    //! Construtor.
    ALPModel();
    //! Destructor.
    ~ALPModel() override;
    //! Initializations
    void initializeDerivedModel() override {}
    void initializeEigenProblem(std::vector<EigenProblemALP*> eigenPb);
    virtual void preAssembleMatrix(const int iProblem);
    virtual void postAssembleMatrix(const int iProblem) {
      IGNORE_UNUSED_ARGUMENT(iProblem);
    }
    void updateTime(const FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs) override;
    virtual void writeSolution();
    void solveEigenProblem();
    void readInitialData();
    void optimizePotential();
    //! Manage time iteration.
    void forward() override;
    //! Acces functions.
    EigenProblemALP* eigenProblem() {
      return m_eigenProblem[0];
    }
    EigenProblemALP* eigenProblem(int iProblem) {
      return m_eigenProblem[iProblem];
    }
  protected:
    std::vector<EigenProblemALP*> m_eigenProblem;
    int m_method;
  };
}


#endif
