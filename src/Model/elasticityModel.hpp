//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

/*!
 \file ElasticityModel.hpp
 \authors S. Gilles
 \date 21/02/2013
 \brief Model to manage elasticity problem.
 */

#ifndef __FELISCE_ELASTICITY_MODEL_DYNAMIC_HPP__
# define __FELISCE_ELASTICITY_MODEL_DYNAMIC_HPP__

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class ElasticityModel
  : public Model 
{
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  //!Construtor.
  ElasticityModel();

  //!Destructor.
  ~ElasticityModel() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //! Solve the problem with the initial conditions.
  void SolveStaticProblem() override;

  /*!
    * \brief Solve the dynamic problem.
    *
    * A call to this method performs all the work, including initial call to solve the static problem.
    */
  void SolveDynamicProblem() override;

  //! Function to get size of the state std::vector.
  int getNstate() const override;

  //! Function to get state std::vector.
  void getState(double* & state) override;
  virtual void getState_swig(double* data, felInt numDof);

  //! Function to std::set state std::vector.
  void setState(double* & state) override;

  //! If some data related to the model must be read from data file
  void initializeDerivedModel() override {}

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  //! Assemble once and for all matrices required in Newmark scheme.
  void AssembleNewmarkMatrices();

  //! Update time. First matrix in LinearProblem::Matrix() is not cleared here, contrary to method in parent class.
  void updateTime(const FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs) override;

  //! Manage time iteration.
  void forward() override;
  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

///@}

} // namespace felisce

#endif // __FELISCE_ELASTICITY_MODEL_HPP__
