//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef __FELISCE_HYPER_ELASTICITY_MODEL_HPP__
# define __FELISCE_HYPER_ELASTICITY_MODEL_HPP__

// System includes

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Model/model.hpp"
#include "Solver/linearProblemHyperElasticity.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
/*!
  * \brief Class in charge of hyperelasticity model.
  */
class HyperElasticityModel
  : public Model
{
public:
  ///@name Type Definitions
  ///@{

  typedef LinearProblemHyperElasticity HyperelasticLinearProblem;

  ///@}
  ///@name Life Cycle
  ///@{

  /**
   * Constructor.
   * @param InputParameters The input configuration parameters
   */
  explicit HyperElasticityModel();

  //! Destructor.
  ~HyperElasticityModel() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
    * @brief Solve the dynamic problem.
    * A call to this method performs all the work, including initial call to solve the static problem.
    */
  void SolveDynamicProblem() override;

  //! Manage time iteration (Newton inside).
  void forward() override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

///@}

} // namespace felisce

#endif // __FELISCE_ELASTICITY_MODEL_HPP__
