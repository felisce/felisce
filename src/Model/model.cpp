//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & M. Fragu
//

// System includes
#include <ctime>

// External includes

// Project includes
#include "Core/mpiInfo.hpp"
#include "Model/model.hpp"
#include "Solver/CVGraphInterface.hpp"
#include "Solver/linearProblemHarmonicExtension.hpp"
#include "Tools/fe_utilities.hpp"

namespace felisce 
{

Model::Model()
{
}

/***********************************************************************************/
/***********************************************************************************/

Model::~Model() {

  if ( m_linearProblemIsInitialized ) {
    if(FelisceParam::instance(this->instanceIndex()).duplicateSupportDof == false)
      writeSolution();
  }

  // Prints chrono table
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  if (r_instance.chronoToScreen) {
    r_instance.timer.ToScreen();
  }
  if (r_instance.chronoToFile) {
    r_instance.timer.resultDir() = r_instance.resultDir;
    r_instance.timer.ToFile();
  }

  for(std::size_t i=0; i<m_linearProblem.size(); i++) {
    if(m_linearProblem[i]) delete m_linearProblem[i];
  }

  #ifdef FELISCE_WITH_CVGRAPH
  // necessary to destroy manually all petsc objects before the petsc finalize
  if ( ! m_rhsWithForceTermWithoutBC.isNull() ) {
    m_rhsWithForceTermWithoutBC.destroy();
  }
  #endif
  if  (m_linearProblemIsInitialized) {
    m_U_0.destroy();

    PetscPrintf(MpiInfo::petscComm(), "\n==============================================================\n");
    PetscPrintf(MpiInfo::petscComm(), "FELiScE %s : normal end\n",m_name.c_str());
    // current date and time system
    time_t now = time(nullptr);
    char* date_time = ctime(&now);
    PetscPrintf(MpiInfo::petscComm(), "%s",date_time);
    PetscPrintf(MpiInfo::petscComm(), "==============================================================\n");

    if (m_callPetscInitialize)
      PetscFinalize();
  }

  if  (m_eigenProblemIsInitialized) {

    PetscPrintf(MpiInfo::petscComm(), "\n==============================================================\n");
    PetscPrintf(MpiInfo::petscComm(), "FELiScE %s : normal end\n",m_name.c_str());
    // current date and time system
    time_t now = time(nullptr);
    char* date_time = ctime(&now);
    PetscPrintf(MpiInfo::petscComm(), "%s",date_time);
    PetscPrintf(MpiInfo::petscComm(), "==============================================================\n");

    #ifdef FELISCE_WITH_SLEPC
    if (m_callPetscInitialize)
        SlepcFinalize();
    #endif
  }

  m_ios.clear();
}

/***********************************************************************************/
/***********************************************************************************/

namespace { // anonymous
  // Print which architecture is used
  void printArchitecture(const MPI_Comm& comm) {
    std::ostringstream oconv;
    oconv << "FELiScE ";
    oconv << 8 * sizeof(void*);
    oconv << " bits ";
    PetscPrintf(comm,"%s",oconv.str().c_str());
  }
} // namespace anonymous

/***********************************************************************************/
/***********************************************************************************/

void Model::initializeModel(CommandLineOption& opt, FelisceTransient& fstransient, const MPI_Comm comm, const bool callPetscInitialize) 
{
  // Update instance index
  m_instanceIndex = opt.instanceIndex();
  auto& r_instance = FelisceParam::instance(this->instanceIndex());

  // Get timer
  auto& r_timer = r_instance.timer;
  r_timer.Start("Model::initializeModel");

  m_fstransient = felisce::make_shared<FelisceTransient>(fstransient);
  m_callPetscInitialize = callPetscInitialize;
  const std::string help = "Felisce\n\n";

  //Initialize Petsc (MPI)
  //======================
  if (comm == MPI_COMM_WORLD) { // default communicator
    #ifdef FELISCE_WITH_SLEPC
    SlepcInitialize(&opt.argcPetsc(), &opt.argvPetsc(), FELISCE_PETSC_NULLPTR, help.c_str());
    #else
    PetscInitialize(&opt.argcPetsc(), &opt.argvPetsc(), FELISCE_PETSC_NULLPTR, help.c_str());
    #endif
    MpiInfo::petscComm()=MPI_COMM_WORLD;
  } else { // user defined communicator (for use with CVGraph for instance...) This is a delicate part, think about before touching it!Saverio
    // From petsc documentation:
    // If you wish PETSc code to run ONLY on a subcommunicator of MPI_COMM_WORLD, create that communicator first and assign it to PETSC_COMM_WORLD BEFORE calling PetscInitialize().
    // Thus if you are running a four process job and two processes will run PETSc and have PetscInitialize() and PetscFinalize() and two process will not, then do this.
    // If ALL processes in the job are using PetscInitialize() and PetscFinalize() then you don't need to do this, even if different subcommunicators of the job are doing different things with PETSc.
    PETSC_COMM_WORLD = comm;
    if (callPetscInitialize) {
      #ifdef FELISCE_WITH_SLEPC
      SlepcInitialize(&opt.argcPetsc(),&opt.argvPetsc(), FELISCE_PETSC_NULLPTR, help.c_str());
      #else
      PetscInitialize(&opt.argcPetsc(),&opt.argvPetsc(), FELISCE_PETSC_NULLPTR, help.c_str());
      #endif
    }
    MpiInfo::petscComm() = PETSC_COMM_WORLD;
  }

  int rankProc,numProc;
  MPI_Comm_rank(MpiInfo::petscComm(),&rankProc);
  MPI_Comm_size(MpiInfo::petscComm(),&numProc);
  MpiInfo::rankProc()=rankProc;
  MpiInfo::numProc()=numProc;

  // Printing logo
  if (MpiInfo::rankProc() == 0) {
    std::cout << "   ______ ______ _      _  _____      ______       \n"
              << "  |  ____|  ____| |    (_)/ ____|    |  ____|      \n"
              << "  | |__  | |__  | |     _| (___   ___| |__         \n"
              << "  |  __| |  __| | |    | |\\___ \\ / __|  __|      \n"
              << "  | |    | |____| |____| |____) | (__| |____       \n"
              << "  |_|    |______|______|_|_____/ \\___|______|     \n"
              << "  Finite Elements for Life Sciences and Engineering\n" << std::endl;
  }

  // current date and time system
  time_t now = std::time(nullptr);
  char* date_time = ctime(&now);
  PetscPrintf(MpiInfo::petscComm(), "\n================================================================\n");

  printArchitecture(comm);

  if(MpiInfo::numProc()==1) PetscPrintf(MpiInfo::petscComm(), "on 1 processor \n");
  else PetscPrintf(MpiInfo::petscComm(), "on %d processors \n", MpiInfo::numProc());
  PetscPrintf(MpiInfo::petscComm(), "%s", date_time);
  PetscPrintf(MpiInfo::petscComm(), "=================================================================\n");
  r_instance.print(FelisceParam::verbose());

  //Read the mesh file
  //==================
  readMesh();

  m_modelIsInitialized = true;

  for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
    m_ios[iio]->readInputFile();
  }

  //Verify if this model use CVGraphInterface
  //==================================
  if ( !strcmp(r_instance.CVGraphInterface.c_str(),"default")) {
    m_initializeCVGraphInterface = false;
  } else {
    m_initializeCVGraphInterface = true;
  }

  r_timer.Stop("Model::initializeModel");
}

/***********************************************************************************/
/***********************************************************************************/

void Model::initializeLinearProblem(const std::vector<LinearProblem*>& linearPb, bool doUseSNES) 
{
  std::vector<bool> aDoUseSNES(linearPb.size(), doUseSNES);
  initializeLinearProblem(linearPb,aDoUseSNES);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::initializeLinearProblem(const std::vector<LinearProblem*>& linearPb, const std::vector<bool>& doUseSNES) 
{
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  FEL_CHECK( doUseSNES.size() == linearPb.size(),"Number of linear problems and doUseSNES differ");

  // Check connections between variables and meshes
  checkConnectionVariablesMeshs();

  // Get timer
  auto& r_timer = r_instance.timer;

  // Initilize solution backup
  r_timer.Start("Model::Initialize solution backup");
  m_solutionBackup.resize(linearPb.size());
  for(std::size_t i=0; i<m_solutionBackup.size(); ++i) {
    m_solutionBackup[i].initialize(r_instance.inputDirectory, r_instance.inputFile, r_instance.inputMesh,
                                   r_instance.outputMesh, r_instance.meshDir, r_instance.resultDir, r_instance.prefixName);
  }
  r_timer.Stop("Model::Initialize solution backup");

  // Copy all linear system in m_linearProblem
  for (std::size_t ipb = 0; ipb < linearPb.size(); ipb++) {
    m_linearProblem.push_back(linearPb[ipb]);
    linearPb[ipb]->identifierProblem() = ipb;
  }

  // Definition of specific linear problems in each model
  r_timer.Start("Model::initializeDerivedLinearProblem");
  initializeDerivedLinearProblem();
  r_timer.Stop("Model::initializeDerivedLinearProblem");

  // For each linear system,
  for (std::size_t ipb = 0; ipb < linearPb.size(); ipb++) {
    // Set the model
    m_linearProblem[ipb]->model() = this;

    // Initialize it
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::initialize");
    m_linearProblem[ipb]->initialize(m_mesh, m_fstransient, MpiInfo::petscComm(), doUseSNES[ipb]);
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::initialize");

    if (r_instance.solver.size() < linearPb.size()) {
      m_linearProblem[ipb]->fixIdOfTheProblemSolver(0);
    } else {
      m_linearProblem[ipb]->fixIdOfTheProblemSolver(ipb);
    }

    // And compute the dof.
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::computeDof");
    m_linearProblem[ipb]->computeDof(MpiInfo::numProc(), MpiInfo::rankProc());
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::computeDof");
  }

  // Define initial conditions
  if (r_instance.hasInitialCondition) {
    for (std::size_t ipb = 0; ipb < linearPb.size(); ipb++) {
      for (std::size_t ii = 0; ii < r_instance.valueInitCond.size(); ii++) {
        m_initialCondition.addVariable(*m_linearProblem[ipb]->listVariable().getVariable(r_instance.nameVariableInitCond[ii]));
      }
      m_initialCondition.print(FelisceParam::verbose(),std::cout);
    }
  }

  // For each linear system
  for (std::size_t ipb = 0; ipb < linearPb.size(); ipb++) {
    // userChangePattern: user function to change the pattern if needed in the specific application.
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::userChangePattern");
    m_linearProblem[ipb]->userChangePattern(MpiInfo::numProc(), MpiInfo::rankProc());
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::userChangePattern");

    // cutMesh: partitioning of the dof and elements.
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::cutMesh");
    m_linearProblem[ipb]->cutMesh();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::cutMesh");

    // allocateMatrix: Allocate memory for the matrix _Matrix and std::vector _RHS in the linearProblem class
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::allocateMatrix");
    m_linearProblem[ipb]->allocateMatrix();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::allocateMatrix");
  }

  // Specific initialization of the user model (useful after allocate Matrix of linear problem)
  r_timer.Start("Model::initializeDerivedModel");
  initializeDerivedModel();
  r_timer.Stop("Model::initializeDerivedModel");

  r_timer.Start("Model::setExternalVec");
  // Make the connection between the different linear pb (to be defined in the derived class)
  setExternalVec();
  r_timer.Stop("Model::setExternalVec");

  for (std::size_t ipb = 0; ipb < linearPb.size(); ipb++) {
    //Define boundary condition of the problem
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::defineBC");
    m_linearProblem[ipb]->defineBC();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::defineBC");

    //Compute boundary condition
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::Compute boundary condition");
    m_linearProblem[ipb]->determineDofAssociateToLabel();
    m_linearProblem[ipb]->finalizeEssBCConstantInT();
    m_linearProblem[ipb]->finalizeEssBCTransient();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::Compute boundary condition");

    //Apply specific operations before assembling
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::preAssemblingMatrixRHS");
    preAssemblingMatrixRHS(ipb);
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::preAssemblingMatrixRHS");

    // Gather to write initial solution.
    m_linearProblem[ipb]->gatherSolution();

    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::buildSolver");
    m_linearProblem[ipb]->buildSolver();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::buildSolver");
  }

  // Initialize object which manage output.
  for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
    m_ios[iio]->initializeOutput();
  }

  for(std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::clearMatrixRHS");
    m_linearProblem[ipb]->clearMatrixRHS();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::clearMatrixRHS");
    r_timer.Start("LinearProblem" + std::to_string(ipb) + "::InitializeDerivedAttributes");
    m_linearProblem[ipb]->InitializeDerivedAttributes();
    r_timer.Stop("LinearProblem" + std::to_string(ipb) + "::InitializeDerivedAttributes");
  }
  r_timer.Start("Model::setInitialCondition");
  setInitialCondition();
  r_timer.Stop("Model::setInitialCondition");

  m_fstransient->iteration=0;

  m_linearProblemIsInitialized = true;
}


/***********************************************************************************/
/***********************************************************************************/

void Model::checkConnectionVariablesMeshs()
{
  auto& r_instance = FelisceParam::instance(this->instanceIndex());

  if( m_mesh.size() == 1 ) {
    // If only one mesh provided -> every variables is defined on that mesh 
    r_instance.idMesh.resize(r_instance.physicalVariable.size(), 0);
  }
  else {
    FEL_CHECK(r_instance.idMesh.size() == r_instance.physicalVariable.size(), "Error idmesh->size() != physicalVariable.size()");
    for(std::size_t i=0; i<r_instance.idMesh.size(); ++i){
      FEL_CHECK(static_cast<std::size_t>(r_instance.idMesh[i]) < m_mesh.size(),"Error in idMesh mesh ID greater than m_mesh.size()");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Model::initializeLinearProblem(LinearProblem* linearPb, bool doUseSNES) 
{
  std::vector<LinearProblem*> linPb(1, linearPb);
  initializeLinearProblem(linPb, doUseSNES);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::finalizeLinearProblem(const std::vector<LinearProblem*>& linearPb) 
{
  // For each linear system,
  for (std::size_t ipb = 0; ipb < linearPb.size(); ipb++) {
    m_linearProblem[ipb]->finalize(m_mesh, m_fstransient, MpiInfo::petscComm());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Model::finalizeLinearProblem(LinearProblem* linearPb) 
{
  std::vector<LinearProblem*> linPb(1, linearPb);
  finalizeLinearProblem(linPb);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::updateTime(const FlagMatrixRHS flagMatrixRHS) 
{
  for(std::size_t i=0; i<m_linearProblem.size(); i++) {
    m_linearProblem[i]->clearMatrixRHS(flagMatrixRHS);
  }
  m_fstransient->iteration++;
  m_fstransient->time +=m_fstransient->timeStep;
}

/***********************************************************************************/
/***********************************************************************************/

void Model::SolveStaticProblem()
{
  PetscPrintf(MpiInfo::petscComm(), "----------------------------------------------\n");
  PetscPrintf(MpiInfo::petscComm(), "--------------- Static problem ---------------\n");
  PetscPrintf(MpiInfo::petscComm(), "----------------------------------------------\n");

  // Retrieve linear problem
  auto p_linear_problem = m_linearProblem[0];

  // Solve the system.
  p_linear_problem->iterativeSolve(MpiInfo::rankProc(), MpiInfo::numProc());
}

/***********************************************************************************/
/***********************************************************************************/

void Model::SolveDynamicProblem()
{
  PetscPrintf(MpiInfo::petscComm(), "----------------------------------------------\n");
  PetscPrintf(MpiInfo::petscComm(), "--------------- Dynamic problem --------------\n");
  PetscPrintf(MpiInfo::petscComm(), "----------------------------------------------\n");

  while (!hasFinished()) {
    forward();
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool Model::hasFinished() 
{
  const auto& r_instance = FelisceParam::instance(this->instanceIndex());
  m_hasFinished = ( m_fstransient->time > r_instance.timeMax || m_fstransient->iteration >= r_instance.timeIterationMax );

  return m_hasFinished;
}

/***********************************************************************************/
/***********************************************************************************/

double Model::getTime() const 
{
  return m_fstransient->time;
}

/***********************************************************************************/
/***********************************************************************************/

void Model::printNewTimeIterationBanner() 
{
  PetscPrintf(MpiInfo::petscComm(),"\n----------------------------------------------\n");
  PetscPrintf(MpiInfo::petscComm(),"%s Iteration: %d, Time: %e \n", m_name.c_str(), m_fstransient->iteration, m_fstransient->time);
  PetscPrintf(MpiInfo::petscComm(),"----------------------------------------------\n");
}

/***********************************************************************************/
/***********************************************************************************/

void Model::setTime(const double time) 
{
  m_fstransient->time = time;
}

/***********************************************************************************/
/***********************************************************************************/

void Model::readMesh() 
{
  auto& r_instance = FelisceParam::instance(this->instanceIndex());
  const std::size_t nbMesh = r_instance.inputMesh.size();
  m_mesh.resize(nbMesh);
  m_ios.resize(nbMesh);

  for(std::size_t imesh=0; imesh<nbMesh; ++imesh) {
    auto& p_io = m_ios[imesh];
    auto& r_mesh = m_mesh[imesh];
    p_io = felisce::make_shared<IO>(r_instance.inputDirectory, r_instance.inputFile, r_instance.inputMesh[imesh],
                                    r_instance.outputMesh[imesh], r_instance.meshDir, r_instance.resultDir,
                                    r_instance.prefixName[imesh]);
    r_mesh = felisce::make_shared<GeometricMeshRegion>();

    p_io->readMesh(*r_mesh, r_instance.spaceUnit);

    if (FelisceParam::verbose()) {
      PetscPrintf(MpiInfo::petscComm(),"\n/========Information about the input mesh========/.\n");
      PetscPrintf(MpiInfo::petscComm(),"Name of the input mesh: <%s>.\n", r_instance.inputMesh[imesh].c_str());
    }

    if (MpiInfo::rankProc() == 0) {
      r_mesh->print(FelisceParam::verbose());
    }

    int& num_coor = r_instance.numCoor;
    if (num_coor == 0) {
      num_coor = r_mesh->numCoor();
    } else if (num_coor != r_mesh->numCoor()) {
      FEL_ERROR("Mesh dimensions are inconsistent. Combining 2D and 3D meshes is not supported.");
    }

    // Check if the mesh is inverted
    if (r_instance.checkVolumeOrientation) {
      if (FEUtilities::CheckVolumeIsInverted(*r_mesh)) {
        FEL_ERROR("Mesh is inverted. Check connectivity");
      }
    }
  }
  
  m_meshIsRead = true;
}

/***********************************************************************************/
/***********************************************************************************/

void Model::writeMesh() 
{
  // Getting instance
  const auto& r_instance = FelisceParam::instance(this->instanceIndex());
  
  if(FelisceParam::verbose()>1) {
    PetscPrintf(MpiInfo::petscComm(),"Write mesh\n");
  }

  if(!r_instance.duplicateSupportDof) {
    for(std::size_t imesh=0; imesh<m_mesh.size(); ++imesh)
      m_ios[imesh]->writeMesh(*m_mesh[imesh]);
  } else {
    // because writeGeoForUnknown write the mesh from the support element not from the mesh
    // This way, the duplicated support elements are saved and the mesh has the same dof as the solutions
    for(std::size_t ipb=0; ipb<m_linearProblem.size(); ++ipb) {
      for(std::size_t iUnknown=0; iUnknown<m_linearProblem[ipb]->listUnknown().size(); ++iUnknown) {
        m_linearProblem[ipb]->writeGeoForUnknown(iUnknown);
      }
    }
  }

  m_meshIsWritten = true;
}

/***********************************************************************************/
/***********************************************************************************/

void Model::buildEdges() 
{
  for(std::size_t imesh=0; imesh<m_mesh.size(); ++imesh)
    m_mesh[imesh]->buildEdges();
}

/***********************************************************************************/
/***********************************************************************************/

void Model::buildFaces() 
{
  for(std::size_t imesh=0; imesh<m_mesh.size(); ++imesh)
    m_mesh[imesh]->buildFaces();
}

/***********************************************************************************/
/***********************************************************************************/

void Model::writeSolution() 
{
  const auto& r_instance = FelisceParam::instance(this->instanceIndex());

  if (MpiInfo::rankProc() == 0) {
    if (!m_meshIsWritten) {
      writeMesh();
    }
  }

  if( (m_fstransient->iteration % r_instance.frequencyWriteSolution == 0)
      || m_hasFinished
      || ( m_fstransient->iteration >= r_instance.intervalWriteSolution[0] && m_fstransient->iteration <= r_instance.intervalWriteSolution[1] )) {

    if(FelisceParam::verbose() > 1)
      PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");

    for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
      m_linearProblem[ipb]->writeSolution(MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration);
    }

    if (MpiInfo::rankProc() == 0) {
      for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
        if ( m_ios[iio]->typeOutput == 1 ) // 1 : ensight{
          m_ios[iio]->postProcess(m_fstransient->time/*, !m_sameGeometricMeshForVariable*/);
      }
    }
  }

  // Warning. If using a bdf for time derivative resolution, you have to modify this call with your bdf method order.
  for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
    if(m_solutionBackup[ipb].hasToBackup(m_fstransient->iteration, r_instance.frequencyBackup) ) {
      if(FelisceParam::verbose() > 1)
        PetscPrintf(MpiInfo::petscComm(),"Backup the solutions for linear problem %ld\n", static_cast<unsigned long>(ipb));

      // Backup of linear problem variables
      m_linearProblem[ipb]->writeSolutionBackup(m_initialCondition.listVariable(), MpiInfo::rankProc(), m_solutionBackup[ipb].listIO(), m_fstransient->time, m_fstransient->iteration);

      // This part has to be improved (07/05/13, Elisa) TODO

      // Backup of linear problem meshes
      m_solutionBackup[ipb].backup(m_fstransient->time, m_mesh, MpiInfo::rankProc());
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Model::ComputeStress(int coef, int label, std::vector<double>& stress) 
{
  m_linearProblem[0]->computeBoundaryStress(coef,label,stress);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::addCallbackXYZ(const char *name, const CallbackXYZ& cb) 
{
  m_mapCallbackXYZ[name] = cb;
}

/***********************************************************************************/
/***********************************************************************************/

void Model::addCallbackXYZT(const char* name, const CallbackXYZT& cb) 
{
  m_mapCallbackXYZT[name] = cb;

  //give the callback to each ElementFieldDynamicValue that want it
  Component comp;
  ElementFieldDynamicValue *efDV;
  // iterate on all elementFieldDynamicValue contained in the std::unordered_map
  for (auto it  = FelisceParam::instance(this->instanceIndex()).elementFieldDynamicValueMap.begin() ;
        it != FelisceParam::instance(this->instanceIndex()).elementFieldDynamicValueMap.end() ;
        it++) {
    efDV = it->second;
    // iterate on all components
    for (int icomp=0 ; icomp < efDV->numComp() ; ++icomp) {
      comp = efDV->allComp[icomp];

      // if the component has to be std::set with the function passed to
      // Model::addCallbackXYZT, give it to the ElementFieldDynamicValue
      if ( efDV->typeValueOfElementField(comp) == FROM_FUNCTION && efDV->functionName(comp) == static_cast<std::string>(name)) {
        efDV->setCallbackXYZT(cb,comp);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

CallbackXYZ& Model::callbackXYZ(const char* name) 
{
  auto it = m_mapCallbackXYZ.find(name);
  auto end = m_mapCallbackXYZ.end();

  if (it == end) {
    const int n = m_mapCallbackXYZ.size();
    std::ostringstream msg;
    msg << "No such callback '" << name << "'. ";
    msg << n << (n>1 ? " candidates" : " candidate") << (n>0 ? ": (" : ".");
    std::size_t counter = 1;
    for (it = m_mapCallbackXYZ.begin() ; it != end ; it++) {
      ++counter;
      msg << "'" << it->first << "'" << (counter == m_mapCallbackXYZ.size() ? ")." : ", ");
    }
    FEL_ERROR(msg.str().c_str());
  }

  return it->second;
}

/***********************************************************************************/
/***********************************************************************************/

CallbackXYZT& Model::callbackXYZT(const char* name) 
{
  auto it = m_mapCallbackXYZT.find(name);
  auto end = m_mapCallbackXYZT.end();

  if (it == end) {
    int n = m_mapCallbackXYZT.size();
    std::ostringstream msg;
    msg << "No such callback '" << name << "'. ";
    msg << n << (n>1 ? " candidates" : " candidate") << (n>0 ? ": (" : ".");
    std::size_t counter = 1;
    for (it = m_mapCallbackXYZT.begin() ; it != end; it++) {
      ++counter;
      msg << "'" << it->first << "'" << (counter == m_mapCallbackXYZT.size() ? ")." : ", ");
    }
    FEL_ERROR(msg.str().c_str());
  }

  return it->second;
}

/***********************************************************************************/
/***********************************************************************************/

double Model::evaluateCallback(const char* name, double x, double y, double z) 
{
  CallbackXYZ cb = callbackXYZ(name);
  return cb(x,y,z);
}

/***********************************************************************************/
/***********************************************************************************/

double Model::evaluateCallback(const char* name, double x, double y, double z, double t) 
{
  CallbackXYZT cb = callbackXYZT(name);
  return cb(x,y,z,t);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::setInitialCondition() 
{
  const auto& r_instance = FelisceParam::instance(this->instanceIndex());

  for (std::size_t iProblem=0; iProblem<m_linearProblem.size(); iProblem++) {
    if (!r_instance.restartSolution) {
      m_U_0.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_U_0.set(0.0);
      if (r_instance.hasInitialCondition) {

        int idVar = -1;
        double valueByDofU_0;
        felInt aa = 0;

        for (std::size_t iListVar = 0; iListVar < m_initialCondition.listVariable().size(); iListVar++) {
          for ( std::size_t iUnknown = 0; iUnknown < m_linearProblem[iProblem]->listUnknown().size(); iUnknown++) {
            idVar = m_linearProblem[iProblem]->listUnknown().idVariable(iUnknown);
            if ( m_linearProblem[iProblem]->listVariable()[idVar].name() == m_initialCondition.listVariable()[iListVar].name() ) {
              felInt numDofLpU = m_linearProblem[iProblem]->numDofLocalPerUnknown(m_linearProblem[iProblem]->listUnknown()[iUnknown]);
              felInt idLocalValue[numDofLpU];
              felInt idGlobalValue[numDofLpU];
              for ( felInt ii = 0; ii < numDofLpU; ii++) {
                idLocalValue[ii] = ii;
              }
              ISLocalToGlobalMappingApply(m_linearProblem[iProblem]->mappingDofLocalToDofGlobal(m_linearProblem[iProblem]->listUnknown()[iUnknown]),numDofLpU,&idLocalValue[0],&idGlobalValue[0]);
              for (felInt ii = 0; ii < numDofLpU; ii++) {

                // TODO this part of the function is not defined for a generic Model... maybe it should be moved or rewritten
                if(r_instance.readDisplFromFile && r_instance.useALEformulation) {
                  aa = idGlobalValue[ii];
                  static_cast<LinearProblemHarmonicExtension*>(m_linearProblem[1])->HarmExtSol().getValues(1, &aa, &valueByDofU_0);
                } else {
                  valueByDofU_0 = r_instance.valueInitCond[iListVar];
                }
                m_U_0.setValue(idGlobalValue[ii],valueByDofU_0,INSERT_VALUES);
              }
            }
          }
        }
      }
      m_U_0.assembly();
    } else {
      m_initialCondition.initializeFromFile(r_instance.restartSolutionDir, r_instance.prefixName[m_linearProblem[iProblem]->currentMesh()] + "_bkp.case");
      userDefinedInitialConditions(iProblem);
    }

    //Initialize solution for solver.
    m_linearProblem[iProblem]->solution().copyFrom(m_U_0);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Model::writeMatrixForMatlab(const std::string& filename, unsigned int index) const 
{
  m_linearProblem[index]->writeMatrixForMatlab(std::numeric_limits<int>::max(), filename);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::writeRHSForMatlab(const std::string& filename, unsigned int index) const 
{
  m_linearProblem[index]->writeRHSForMatlab(std::numeric_limits<int>::max(), filename);
}

/***********************************************************************************/
/***********************************************************************************/

#ifdef FELISCE_WITH_CVGRAPH

void Model::startIterationCVG() 
{
  cvgMainSlave* slave=m_linearProblem[0]->slave();
  if (slave==nullptr) {
    FEL_ERROR("slave not initialized");
  }
  if( slave->newTimeStep() ) {
    std::stringstream msg;
    msg<<"Starting a new time step"<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    cvgraphNewTimeStep();
    m_rhsWithoutBC_zeroed=false;
  } else if( slave->redoTimeStep() ){
    std::stringstream msg;
    msg << "......................................" << std::endl;
    msg << "Redo time step at time t= " << m_fstransient->time << std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    rhsWithoutBC_restore();
    m_linearProblem[0]->matrix(0).copyFrom(m_linearProblem[0]->matrixWithoutBC(),SAME_NONZERO_PATTERN);
    m_linearProblem[0]->vector().copyFrom(m_linearProblem[0]->rhsWithoutBC());
  } else if( slave->redoTimeStepOnlyCVGData() ) {
    std::stringstream msg;
    msg << "......................................" << std::endl;
    msg << "Redo time step, with only cvg data, at time t= " << m_fstransient->time << std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    rhsWithoutBC_zeroEntries();
    m_linearProblem[0]->matrix(0).copyFrom(m_linearProblem[0]->matrixWithoutBC(),SAME_NONZERO_PATTERN);
    m_linearProblem[0]->vector().zeroEntries();
    m_linearProblem[0]->applyBC(FelisceParam::instance(this->instanceIndex()).essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_matrix);//We insert dirichlet and robin parts to the matrix.
  } else {
    slave->msgInfo().print(1);
    FEL_ERROR("Error: unexpected timeStatus");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Model::ImposeCVGraphInterfaceSolution(double* ImpValue) 
{
  m_cvgraphInterf->ImposeSolution(ImpValue);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::ImposeCVGraphInterfaceSolutionAndComputedVariable(double* ImpValue, int idVariable) 
{
  m_cvgraphInterf->ImposeSolutionAndComputedVariable(ImpValue, idVariable);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::ExtractCVGraphInterfaceSolution(double* valueDofInterface) 
{
  m_cvgraphInterf->ExtractSolution(m_linearProblem[0]->solution(), valueDofInterface);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::ExtractCVGraphInterfaceSolution(double* valueDofInterface, int idVariable) 
{
  m_cvgraphInterf->ExtractSolution( m_linearProblem[0]->solution(), valueDofInterface, idVariable);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::ExtractCVGraphInterfaceSolutionAndComputedVariable(double* valueDofInterface,int idVariable, std::vector<double>& interfaceComputedVariable) 
{
  m_cvgraphInterf->ExtractSolutionAndComputedVariable( m_linearProblem[0]->solution(), interfaceComputedVariable, valueDofInterface, idVariable);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::StoreCVGraphInterfaceSolution(double* StoringValue) 
{
  m_cvgraphInterf->StoreSolution(StoringValue);
}

/***********************************************************************************/
/***********************************************************************************/

void Model::rhsWithoutBC_zeroEntries() 
{
  // If the m_rhsWithoutBC was already zeroed we do nothing
  if ( !m_rhsWithoutBC_zeroed ) {

    // We save a copy of it before erasing it.
    // So that we can restore it later
    if ( m_rhsWithForceTermWithoutBC.isNull() ) {
      // In case it was not initialized we do it now.
      m_rhsWithForceTermWithoutBC.duplicateFrom(m_linearProblem[0]->rhsWithoutBC());
    }
    m_rhsWithForceTermWithoutBC.copyFrom(m_linearProblem[0]->rhsWithoutBC());

    // We std::set it to zero.
    m_linearProblem[0]->rhsWithoutBC().zeroEntries();
    m_rhsWithoutBC_zeroed=true;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Model::rhsWithoutBC_restore() 
{
  // If it was zeroed, we restore it.
  if (m_rhsWithoutBC_zeroed) {
    m_linearProblem[0]->rhsWithoutBC().copyFrom(m_rhsWithForceTermWithoutBC);
  }
}

#endif
}
