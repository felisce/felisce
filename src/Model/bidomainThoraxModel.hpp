//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _BIDOMAINTHORAXMODEL_HPP
#define _BIDOMAINTHORAXMODEL_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/model.hpp"
#include "Solver/linearProblemBidomainThorax.hpp"

namespace felisce {
  class BidomainThoraxModel:
    public Model {
  public:
    ///Construtor.
    BidomainThoraxModel();
    ///Destructor.
    ~BidomainThoraxModel() override;
    void initializeDerivedModel() override;
    void initializeDerivedLinearProblem() override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void postAssemblingMatrixRHS(std::size_t /*iProblem=0*/) override{};
    void writeSolution();
    /// Manage time iteration.
    void forward() override;
#ifdef FELISCE_WITH_CVGRAPH
    void startIterationCVG() override;
#endif
  protected:
    std::vector<LinearProblemBidomainThorax*> m_linearProblemBidomainThorax;
  };
}


#endif
