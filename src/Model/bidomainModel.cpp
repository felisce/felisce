//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    A. Collin
//

// System includes

// External includes

// Project includes
#include "Model/bidomainModel.hpp"
#include "Solver/CVGraphInterface.hpp"


namespace felisce {
  BidomainModel::BidomainModel():
    Model(),
    m_rom(nullptr),
    m_verbose(0),
    m_buildIonic(false),
    m_buildCourt(false),
    m_ionic(nullptr),
    m_schaf(nullptr),
    m_fhn(nullptr),
    m_court(nullptr),
    m_mv(nullptr),
    m_paci(nullptr),
    m_bcl(nullptr),
    m_observations(nullptr),
    m_extrapolateIsCreated(false) {
    for(std::size_t i=0; i<m_linearProblemBidomain.size(); i++) {
      m_linearProblemBidomain[i] = nullptr;
    }
    m_name = "Bidomain";
  }

  BidomainModel::~BidomainModel() {
    for(std::size_t i=0; i<m_linearProblemBidomain.size(); i++) {
      m_linearProblemBidomain[i] = nullptr;
    }
    if(FelisceParam::instance().useROM) {
      delete m_rom;
      m_rom=nullptr;
    }
    if (m_buildIonic) {
      delete m_ionic;
      m_ionic=nullptr;
    }
    if (m_buildCourt) {
      delete m_court;
      m_court = nullptr;
    }
    if(m_observations) {
      delete m_observations;
      m_observations=nullptr;
    }

    for (std::size_t i=0; i<m_W_0.size(); i++) {
      m_W_0[i].destroy();
    }
    if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      m_m.destroy();
      m_h.destroy();
      m_j.destroy();
      m_ao.destroy();
      m_io.destroy();
      m_ua.destroy();
      m_ui.destroy();
      m_xr.destroy();
      m_xs.destroy();
      m_d.destroy();
      m_f.destroy();
      m_fca.destroy();
      m_urel.destroy();
      m_vrel.destroy();
      m_wrel.destroy();
      m_nai.destroy();
      m_nao.destroy();
      m_cao.destroy();
      m_ki.destroy();
      m_ko.destroy();
      m_cai.destroy();
      m_naiont.destroy();
      m_kiont.destroy();
      m_caiont.destroy();
      m_ileak.destroy();
      m_iup.destroy();
      m_itr.destroy();
      m_irel.destroy();
      m_cmdn.destroy();
      m_trpn.destroy();
      m_nsr.destroy();
      m_jsr.destroy();
      m_csqn.destroy();
    }

    if (m_extrapolateIsCreated)
      m_extrapolate.destroy();
    delete m_iApp;

    if (FelisceParam::instance().hasAppliedExteriorCurrent)
      delete m_iAppExt;

    if(FelisceParam::instance().stateFilter) {
      m_luenbFlter.destroy();
      m_Observ.destroy();
    }

    if (FelisceParam::instance().restartSolution) {
      if (FelisceParam::instance().orderBdfEdp > 0)
        m_bdf_sol_n.destroy();
      if (FelisceParam::instance().orderBdfEdp > 1)
        m_bdf_sol_n_1.destroy();
      if (FelisceParam::instance().orderBdfEdp > 2)
        m_bdf_sol_n_2.destroy();
      if (FelisceParam::instance().orderBdfIonic > 0) {
        for (std::size_t i=0; i<m_ionic_sol_n.size(); i++) {
          m_ionic_sol_n[i].destroy();
        }
      }
      if (FelisceParam::instance().orderBdfIonic > 1) {
        for (std::size_t i=0; i<m_ionic_sol_n_1.size(); i++) {
          m_ionic_sol_n_1[i].destroy();
        }
      }
      if (FelisceParam::instance().orderBdfIonic > 2) {
        for (std::size_t i=0; i<m_ionic_sol_n_2.size(); i++) {
          m_ionic_sol_n_2[i].destroy();
        }
      }
      if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        for (int iCourt=0 ; iCourt<29; iCourt++) {
          m_courtVarInit[iCourt].destroy();
        }
      }
    }
  }

  void BidomainModel::initializeDerivedLinearProblem() {
    for (std::size_t i=0; i<m_linearProblem.size(); i++) {
      m_linearProblemBidomain.push_back(static_cast<LinearProblemBidomain*>(m_linearProblem[i]));
    }
  }



  //! Define m_ionic and m_bdfEdp (used in linearProblem).
  void BidomainModel::initializeDerivedModel() {
    m_verbose = FelisceParam::verbose();

    if ( FelisceParam::instance().withCVG ){
      m_linearProblemBidomain[0]->initPetscVectors();
    }

    //Allocate m_ionic.
    // Choose type of ionic model to be used.
    if ((FelisceParam::instance().typeOfIonicModel == "schaf") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent")) {
      // Initialize Mitchell and Schaeffer model.
      m_schaf = new SchafSolver(m_fstransient);
      m_ionic = m_schaf;

      if (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") {
        // Initialize Courtemanche model
        m_court = new CourtemancheSolver(m_fstransient);
      }
    } else if (FelisceParam::instance().typeOfIonicModel == "schafRev") {
      // Initialize Mitchell and Schaeffer Revised model.
      m_schaf = new SchafRevisedSolver(m_fstransient);
      m_ionic = m_schaf;
    } else if (FelisceParam::instance().typeOfIonicModel == "fhn") {
      // Initialize Fitzhugh-Nagumo model
      m_fhn = new FhNSolver(m_fstransient);
      m_ionic =m_fhn;
    } else if ( (FelisceParam::instance().typeOfIonicModel == "MV") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      // Initialize MV model
      m_mv = new MVSolver(m_fstransient);
      m_ionic = m_mv;

      if (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") {
        // Initialize Courtemanche model
        m_court = new CourtemancheSolver(m_fstransient);
      }
    } else if (FelisceParam::instance().typeOfIonicModel == "Paci") {
      // Initialize Paci model
      m_paci = new PaciSolver(m_fstransient);
      m_ionic = m_paci;
    } else if (FelisceParam::instance().typeOfIonicModel == "BCL") {
      // Initialize BCL model
      m_bcl = new BCLSolver(m_fstransient);
      m_ionic = m_bcl;
    }

    userDefinedIonicModel();

    //Fix order of bdf for ionic solver.
    if ( (FelisceParam::instance().typeOfIonicModel == "MV") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      m_ionic->defineOrderBdf(FelisceParam::instance().orderBdfIonic,3);
      m_W_0.resize(3);
    } else if(FelisceParam::instance().typeOfIonicModel == "Paci") {
      m_ionic->defineOrderBdf(FelisceParam::instance().orderBdfIonic,42);
      m_W_0.resize(42);
    } else if(FelisceParam::instance().typeOfIonicModel == "BCL") {
      m_ionic->defineOrderBdf(FelisceParam::instance().orderBdfIonic,4);
      m_W_0.resize(4);
    } else
      m_ionic->defineOrderBdf(FelisceParam::instance().orderBdfIonic,1);

    //Define order for bdf used by EDP (linearProblem).
    m_bdfEdp.defineOrder(FelisceParam::instance().orderBdfEdp);
    initializeAppCurrent();
    if (FelisceParam::instance().hasAppliedExteriorCurrent)
      initializeAppCurrentExt();
    if(FelisceParam::instance().stateFilter) {
      m_observations = new EnsightCase();
      m_observations->read(FelisceParam::instance().observDir,FelisceParam::instance().observFileName);
    }

  }

  void BidomainModel::initializeAppCurrent() {
    std::unordered_map<std::string, int> mapOfType;
    mapOfType["ellibi"] = 1;
    mapOfType["zygote"] = 2;

    bool warning_Iapp = true;

    switch(mapOfType[FelisceParam::instance().typeOfAppliedCurrent]) {
    case 1 :
      m_iApp = new AppCurrent();
      warning_Iapp = false;
      break;
    case 2 :
      m_iApp = new zygoteIapp();
      warning_Iapp = false;
      break;
    }
    if (warning_Iapp) {
      std::cout << "WARNING !\nApplied current not defined : used non pathological case for ellibi geometry.\n";
      m_iApp = new AppCurrent();
    }
    m_iApp->initialize(m_fstransient);
  }

  void BidomainModel::initializeAppCurrentExt() {
    m_iAppExt = new AppCurrent();
    m_iAppExt->initialize(m_fstransient);
  }


  void BidomainModel::evalIapp() {
    m_iAppValue.clear();
    if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
      // Warning: extra-data m_linearProblemBidomain[0]->EndocardiumDistance() size has to be numDof (not number of mesh points).
      m_linearProblemBidomain[0]->evalFunctionOnDof(*m_iApp,m_fstransient->time,m_iAppValue,m_linearProblemBidomain[0]->EndocardiumDistance());
    }
    else {
      m_linearProblemBidomain[0]->evalFunctionOnDof(*m_iApp,m_fstransient->time,m_iAppValue);
    }
  }

  void BidomainModel::evalIappExt() {
    m_iAppValueExt.clear();
    m_linearProblemBidomain[0]->evalFunctionOnDof(*m_iAppExt,m_fstransient->time,m_iAppValueExt);
  }


  //Calculate RHS std::vector to be multiplied by mass matrix.
  void BidomainModel::finalizeRHSDP(int iProblem) 
  {
    int size = m_linearProblemBidomain[iProblem]->numDofLocalPerUnknown(potTransMemb);
    double* valueForPetsc = new double[size];
    double* obsForPetsc = new double[size];
    felInt* idLocalValue = new felInt[size];
    felInt* idGlobalValue = new felInt[size];

    double coefAm = FelisceParam::instance().Am;
    double coefCm = FelisceParam::instance().Cm;

    double* observ=nullptr;
    if (FelisceParam::instance().stateFilter ) {
      //RHS of Luenb. filter
      m_Observ.set(0.);
      felInt sizeVar = m_linearProblemBidomain[iProblem]->numDofPerUnknown(potTransMemb);
      int indexTime = m_observations->timeIndex(m_fstransient->time);
      int idVariable = m_observations->variableIndex("potTransMemb");
      observ=new double[sizeVar];
      m_observations->readVariable(idVariable, indexTime,observ,sizeVar);
    }

    // RHS = Am*Cm*BDF.RHS (Warning : bdf.RHS containes both problem variables).
    m_linearProblemBidomain[iProblem]->vector().axpy(1.,m_bdfEdp.vector());
    m_linearProblemBidomain[iProblem]->vector().scale(coefAm*coefCm);

    // RHS = RHS + Am*Iapp
    for (felInt i = 0; i < size; i++) {
      idLocalValue[i] = i;
    }
    ISLocalToGlobalMappingApply(m_linearProblemBidomain[iProblem]->mappingDofLocalToDofGlobal(potTransMemb),size,&idLocalValue[0],&idGlobalValue[0]);
    for (felInt i = 0; i < size; i++) {
      felInt indexGlobal=idGlobalValue[i];
      #ifdef NDEBUG
      valueForPetsc[i] = coefAm*m_iAppValue[indexGlobal];
      #else
      valueForPetsc[i] = coefAm*m_iAppValue.at(indexGlobal);
      #endif
      if (FelisceParam::instance().stateFilter ) {
        AOPetscToApplication(m_linearProblemBidomain[iProblem]->ao(),1,&indexGlobal);
        obsForPetsc[i] = observ[indexGlobal];
      }
    }
    m_linearProblemBidomain[iProblem]->vector().setValues(size,&idGlobalValue[0],&valueForPetsc[0],ADD_VALUES);

    m_linearProblemBidomain[iProblem]->vector().assembly();

    // For the second equation : RHS = 0 ( or applied exterior current )
    int sizePotExtraCell = m_linearProblemBidomain[iProblem]->numDofLocalPerUnknown(potExtraCell);
    double* valueForPetscPotExtraCell = new double[sizePotExtraCell];
    felInt* idLocalValuePotExtraCell = new felInt[sizePotExtraCell];
    felInt* idGlobalValuePotExtraCell = new felInt[sizePotExtraCell];

    for (felInt i = 0; i < sizePotExtraCell; i++) {
      idLocalValuePotExtraCell[i] = i;
    }
    ISLocalToGlobalMappingApply(m_linearProblemBidomain[iProblem]->mappingDofLocalToDofGlobal(potExtraCell),sizePotExtraCell,&idLocalValuePotExtraCell[0],&idGlobalValuePotExtraCell[0]);
    for (felInt i = 0; i < sizePotExtraCell; i++) {
      // For an applied exterior current
      if (FelisceParam::instance().hasAppliedExteriorCurrent) {
        valueForPetscPotExtraCell[i] = m_iAppValueExt[idGlobalValuePotExtraCell[i]];
      }
      else {
        valueForPetscPotExtraCell[i] = 0.0;
      }
    }
    m_linearProblemBidomain[iProblem]->vector().setValues(sizePotExtraCell ,&idGlobalValuePotExtraCell[0],&valueForPetscPotExtraCell[0],INSERT_VALUES);

    m_linearProblemBidomain[iProblem]->vector().assembly();

    // RHS = RHS + Am*Iion
    if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      m_linearProblemBidomain[iProblem]->vector().axpy(coefAm,m_ionic->ion());
      m_linearProblemBidomain[iProblem]->vector().axpy(-coefAm*0.001,m_court->ion());
    } else {
      m_linearProblemBidomain[iProblem]->vector().axpy(coefAm,m_ionic->ion());
    }

    // Elisa, Dec. 2014 - Luenberger filter
    if (FelisceParam::instance().electrodeControl) {
      m_linearProblemBidomain[iProblem]->addElectrodeCondtrol();
    }

    // RHS = RHS + Am*A2*A3*H*V_m
    if (FelisceParam::instance().stateFilter ) {
      m_Observ.setValues(size,&idGlobalValue[0],&obsForPetsc[0],INSERT_VALUES);
      m_Observ.assembly();
      pointwiseMult(m_Observ,m_Observ,m_ionic->stabTerm());
      m_linearProblemBidomain[iProblem]->vector().axpy(coefAm,m_Observ);
    }

    // m_linearProblemBidomain[iProblem]->applyUserDefinedStimulation(m_fstransient->time);

    delete [] valueForPetsc;
    delete [] obsForPetsc;
    delete [] idLocalValue;
    delete [] idGlobalValue;
    delete [] valueForPetscPotExtraCell;
    delete [] idLocalValuePotExtraCell;
    delete [] idGlobalValuePotExtraCell;

  }

  void BidomainModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    //Initialize vectors for ionic models and m_bdfEdp (with m_U_0) with a std::vector with same structure as m_RHS in linearProblem.
    //(in this case give PotTransMemb with value Vmin and 0 for PotExtraCell at time t == 0).

    // 1) initialize Vectors for ionic solver
    if ( (FelisceParam::instance().typeOfIonicModel == "MV") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      double valueByDofW0[3];
      valueByDofW0[0] = 1.;
      valueByDofW0[1] = 1.;
      valueByDofW0[2] = 0.;
      for (felInt i=0; i<3; i++) {
        m_W_0[i].duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
        m_W_0[i].set(valueByDofW0[i]);
      }
    }
    else if(FelisceParam::instance().typeOfIonicModel == "Paci") {
      double valueByDofW0[42];

      valueByDofW0[0] = 0.75;
      valueByDofW0[1] = 0.75;
      valueByDofW0[2] = 0.;
      valueByDofW0[3] = 0.;
      valueByDofW0[4] = 1.;
      valueByDofW0[5] = 1.;
      valueByDofW0[6] = 1.;
      valueByDofW0[7] = 0.;
      valueByDofW0[8] = 1.;
      valueByDofW0[9] = 0.;
      valueByDofW0[10] = 1.;
      valueByDofW0[11] = 0.;
      valueByDofW0[12] = 0.1;
      valueByDofW0[13] = 1.;
      valueByDofW0[14] = 0.0002;
      valueByDofW0[15] = 0.3;
      valueByDofW0[16] = 10.;
      valueByDofW0[17] = 0.;
      valueByDofW0[18] = 0.;
      valueByDofW0[19] = 0.;
      valueByDofW0[20] = 0.;
      valueByDofW0[21] = 0.;
      valueByDofW0[22] = 0.;
      valueByDofW0[23] = 0.;
      valueByDofW0[24] = 0.;
      valueByDofW0[25] = 0.;
      valueByDofW0[26] = 0.;
      valueByDofW0[27] = 0.;
      valueByDofW0[28] = 0.;
      valueByDofW0[29] = 0.;
      valueByDofW0[30] = 0.;
      valueByDofW0[31] = 0.;
      valueByDofW0[32] = 0.;
      valueByDofW0[33] = 0.;
      valueByDofW0[34] = 0.;
      valueByDofW0[35] = 0.;
      valueByDofW0[36] = 0.0074621;
      valueByDofW0[37] = 0.692477;
      valueByDofW0[38] = 0.692574;
      valueByDofW0[39] = 0.692591;
      valueByDofW0[40] = 0.496116;
      valueByDofW0[41] = 0.000194015;


      for (felInt i=0; i<42; i++) {
        m_W_0[i].duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
        m_W_0[i].set(valueByDofW0[i]);
      }
    }
    else if (FelisceParam::instance().typeOfIonicModel == "BCL") {
      double valueByDofW0[4];
      valueByDofW0[0] = 1.;
      valueByDofW0[1] = 1.;
      valueByDofW0[2] = 0.;
      valueByDofW0[3] = 1.;

      for (felInt i=0; i<4; i++) {
        m_W_0[i].duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
        m_W_0[i].set(valueByDofW0[i]);
      }
    }
    // "schaf", "schafRev", "courtAtriaSchafVent", "fhn"
    else {
      m_W_0.resize(1);
      m_W_0[0].duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      if (FelisceParam::instance().typeOfIonicModel == "fhn") {
        m_W_0[0].set(0.);
      } else {
        double valueByDofW_0 = 1./((FelisceParam::instance().vMax - FelisceParam::instance().vMin)*(FelisceParam::instance().vMax - FelisceParam::instance().vMin));
        m_W_0[0].set(valueByDofW_0);
      }
    }

    if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      double valueByDofm = 0.00291;
      double valueByDofh = 0.965;
      double valueByDofj = 0.978;
      double valueByDofao = 0.0304;
      double valueByDofio = 0.999;
      double valueByDofua =  0.00496;
      double valueByDofui = 0.999;
      double valueByDofxr = 0.0000329;
      double valueByDofxs = 0.0187;
      double valueByDofd = 0.000137;
      double valueByDoff = 0.999837;
      double valueByDoffca = 0.775;
      double valueByDofvrel = 1.00;
      double valueByDofwrel = 0.999;
      double valueByDofnai = 11.2;
      double valueByDofnao = 140.0;
      double valueByDofcao = 1.8;
      double valueByDofki = 139.0;
      double valueByDofko = 4.5;
      double valueByDofcai = 0.000102;
      double valueByDofcmdn = 0.00205;
      double valueByDoftrpn = 0.0118;
      double valueByDofnsr = 1.49;
      double valueByDofjsr = 1.49;
      double valueByDofcsqn = 6.51;

      m_m.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_m.set(valueByDofm);
      m_h.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_h.set(valueByDofh);
      m_j.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_j.set(valueByDofj);
      m_ao.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_ao.set(valueByDofao);
      m_io.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_io.set(valueByDofio);
      m_ua.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_ua.set(valueByDofua);
      m_ui.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_ui.set(valueByDofui);
      m_xr.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_xr.set(valueByDofxr);
      m_xs.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_xs.set(valueByDofxs);
      m_d.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_d.set(valueByDofd);
      m_f.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_f.set(valueByDoff);
      m_fca.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_fca.set(valueByDoffca);
      m_urel.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_urel.set(0.);
      m_vrel.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_vrel.set(valueByDofvrel);
      m_wrel.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_wrel.set(valueByDofwrel);
      m_nai.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_nai.set(valueByDofnai);
      m_nao.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_nao.set(valueByDofnao);
      m_cao.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_cao.set(valueByDofcao);
      m_ki.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_ki.set(valueByDofki);
      m_ko.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_ko.set(valueByDofko);
      m_cai.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_cai.set(valueByDofcai);
      m_naiont.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_naiont.set(0.);
      m_kiont.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_kiont.set(0.);
      m_caiont.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_caiont.set(0.);
      m_ileak.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_ileak.set(0.);
      m_iup.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_iup.set(0.);
      m_itr.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_itr.set(0.);
      m_irel.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_irel.set(0.);
      m_cmdn.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_cmdn.set(valueByDofcmdn);
      m_trpn.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_trpn.set(valueByDoftrpn);
      m_nsr.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_nsr.set(valueByDofnsr);
      m_jsr.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_jsr.set(valueByDofjsr);
      m_csqn.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_csqn.set(valueByDofcsqn);
    }



    // 2) copy structure in m_extrapolate (contains extrapolate values of linearProblem solution).
    m_extrapolate.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
    m_extrapolate.set(0.);
    m_extrapolateIsCreated = true;

    // Lun. filter
    if(FelisceParam::instance().stateFilter) {
      m_Observ.duplicateFrom(m_linearProblemBidomain[iProblem]->vector());
      m_Observ.set(0.);
    }

    // Read fibers and distance mapp (for Zygote geometry).
    if(FelisceParam::instance().testCase == 1) { // Any testCase with fibers.
      m_linearProblemBidomain[iProblem]->readData(*io());
    }

    //Initialize heterogeneous parameters for schaf solver.
    if ( (FelisceParam::instance().typeOfIonicModel == "schaf") || (FelisceParam::instance().typeOfIonicModel == "schafRev") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") ) {
      initializeSchafParameter();
      if (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent")
        initializeCourtParameter();
    }

    //Initialize heterogeneous parameters for MV solver.
    if ( (FelisceParam::instance().typeOfIonicModel == "MV") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      initializeMVParameter();
      if (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent")
        initializeCourtParameter();
    }

    //Initialize heterogeneous parameters for BCL solver.
    if (FelisceParam::instance().typeOfIonicModel == "BCL") {
      initializeBCLParameter();
    }

    //Initialize heterogeneous parameters for FhN solver.
    if (FelisceParam::instance().typeOfIonicModel == "fhn") {
      initializeFhNParameter();
    }

    if(FelisceParam::instance().useROM) {
      std::cout << "m_rom->initializeRom\n";
      m_rom = new RomPETSC();
      m_rom->initializeRom(m_fstransient, MpiInfo::petscComm(), m_linearProblemBidomain[iProblem]->ao());
      m_linearProblemBidomain[iProblem]->initializeROM(m_rom);
      m_rom->initializeRomBasis();
    }

  }

  void BidomainModel::postAssemblingMatrixRHS(std::size_t iProblem) {

    // Evaluate applied current std::vector.
    if (iProblem == 0) {
      evalIapp();
      if (FelisceParam::instance().hasAppliedExteriorCurrent)
        evalIappExt();
    }


    // Calculate RHS std::vector (to be multiplied by mass matrix).
    // RHS = Am*Ion + Am*Iapp + ...
    finalizeRHSDP(iProblem);

    // Calculate m_RHS :
    // if model == 'Bidomain' => m_RHS = mass * RHS.
    // if model == 'BidomainDecoupled' :
    //   iProblem==0 => m_RHS=mass*RHS+Ksigmai*u_e
    //   iProblem==1 => m_RHS=Ksigmai*V_m
    m_linearProblemBidomain[iProblem]->addMatrixRHS();

    //RHS of Luenb. filter (Cesare)
    if (FelisceParam::instance().stateFilter) {
      if (iProblem == 0) {
        double coefAm = FelisceParam::instance().Am;
        m_luenbFlter.zeroEntries();
        m_luenbFlter.copyFrom(m_linearProblemBidomain[iProblem]->matrix(1),SAME_NONZERO_PATTERN);
        m_luenbFlter.assembly(MAT_FINAL_ASSEMBLY);
        // copy diagonal stabiliz. matrix in luenberger matrix
        m_luenbFlter.diagonalScale(PetscVector::null(),m_ionic->stabTerm());
        m_luenbFlter.scale(coefAm);
        m_linearProblemBidomain[iProblem]->matrix(0).axpy(1,m_luenbFlter,SAME_NONZERO_PATTERN);
      }
    }
  }

  // Re-define class to change clearMatrixRHS call
  void BidomainModel::updateTime(const FlagMatrixRHS flagMatrixRHS) {
    for(std::size_t i=0; i<m_linearProblem.size(); i++) {
      m_linearProblemBidomain[i]->clearMatrixRHS(flagMatrixRHS);
    }
    m_fstransient->iteration++;
    m_fstransient->time +=m_fstransient->timeStep;
  }

  void BidomainModel::initializeSchafParameter() {
    // Initialize TauClose.
    if (FelisceParam::instance().hasHeteroTauClose) {
      m_schaf->fctTauClose().initialize(m_fstransient);
      if ( (FelisceParam::instance().typeOfAppliedCurrent == "zygote") || (FelisceParam::instance().typeOfAppliedCurrent == "heart") || (FelisceParam::instance().typeOfAppliedCurrent == "ellipseheart") ) {
        // Warning: extra-data m_linearProblemBidomain[0]->EndocardiumDistance() size has to be numDof (not number of mesh points).
        m_linearProblemBidomain[0]->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose(),m_linearProblemBidomain[0]->EndocardiumDistance());
      } else {
        m_linearProblemBidomain[0]->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose());
      }
    }
    // Initialize TauOut : heterogeneous in case of infarct.
    if (FelisceParam::instance().hasInfarct) {
      m_schaf->fctTauOut().initialize(m_fstransient);
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_schaf->fctTauOut(), m_schaf->tauOut());
    }
  }

  void BidomainModel::initializeMVParameter() {
    if (FelisceParam::instance().CellsType == "hetero") {
      m_mv->MVCoeff().initialize(m_fstransient);
      // Warning: extra-data m_linearProblemBidomain[0]->EndocardiumDistance() size has to be numDof (not number of mesh points).
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_mv->MVCoeff(), m_mv->cellType(),m_linearProblemBidomain[0]->EndocardiumDistance());
    }
    // Initialize gfi
    if (FelisceParam::instance().hasInfarct) {
      m_mv->fctTauOut().initialize(m_fstransient);
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_mv->fctTauOut(), m_mv->tauOut());
    }
  }

  void BidomainModel::initializeBCLParameter() {
    if (FelisceParam::instance().CellsType == "hetero") {
      m_bcl->MVCoeff().initialize(m_fstransient);
      // Warning: extra-data m_linearProblemBidomain[0]->EndocardiumDistance() size has to be numDof (not number of mesh points).
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_bcl->MVCoeff(), m_bcl->cellType(),m_linearProblemBidomain[0]->EndocardiumDistance());
    }
  }

  void BidomainModel::initializeFhNParameter() {
    if (FelisceParam::instance().hasInfarct) {
      m_fhn->fctSpar().initialize(m_fstransient);
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_fhn->fctSpar(), m_fhn->f0Par());
    }
  }

  void BidomainModel::initializeCourtParameter() {
    // Initialize gToMax
    m_court->fctCourtCondIto().initialize(m_fstransient);
    // Initialize gCaLMax
    m_court->fctCourtCondICaL().initialize(m_fstransient);
    if (FelisceParam::instance().hasHeteroCourtPar) {
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_court->fctCourtCondIto(), m_court->courtCondIto());
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_court->fctCourtCondICaL(), m_court->courtCondICaL());
    }
    // Initialize multiplicative coefficient
    m_court->fctCourtCondMultCoeff().initialize(m_fstransient);
    // Warning: extra-data m_linearProblemBidomain[0]->Reference() size has to be numDof (not number of mesh points).
    if (FelisceParam::instance().testCase == 1)
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_court->fctCourtCondMultCoeff(), m_court->courtCondMultCoeff(), m_linearProblemBidomain[0]->Reference());
    else
      m_linearProblemBidomain[0]->evalFunctionOnDof(m_court->fctCourtCondMultCoeff(), m_court->courtCondMultCoeff());
  }

  // Pay attention on the call of this :
  // call BidomainModel::writeSolution() function instead of (non-virtual) Model::writeSolution() function
  void BidomainModel::writeSolution() {
    if (MpiInfo::rankProc() == 0) {
      if (m_meshIsWritten == false) writeMesh();
    }

    if( (m_fstransient->iteration % FelisceParam::instance().frequencyWriteSolution == 0) or m_hasFinished) {
      if(FelisceParam::verbose() > 1) PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");
      for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {

        m_linearProblemBidomain[ipb]->writeSolution(MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration);

        if(FelisceParam::instance().printIonicVar) {
          int rankProc;
          MPI_Comm_rank(MpiInfo::petscComm(),&rankProc);
          if ( m_fstransient->iteration == 0 ) {
            double * ionicSolToSave = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSave, m_W_0[0], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
            if (rankProc == 0) {
              m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSave, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), "ionicVar");
              m_linearProblemBidomain[ipb]->writeEnsightCase(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,"ionicVar");
            }
            delete [] ionicSolToSave;

            if(FelisceParam::instance().typeOfIonicModel == "MV") {
              //MV
              double * ionicSolToSaveV = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
              double * ionicSolToSaveW = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
              double * ionicSolToSaveS = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];

              std::vector<std::string> varNames;
              varNames.emplace_back("v");
              varNames.emplace_back("w");
              varNames.emplace_back("s");

              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveV, m_W_0[0], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveW, m_W_0[1], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveS, m_W_0[2], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));

              if (MpiInfo::rankProc() == 0) {
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveV, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[0]);
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveW, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[1]);
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveS, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[2]);

                m_linearProblemBidomain[ipb]->writeEnsightCaseCurrents(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,varNames);

              }

              delete [] ionicSolToSaveV;
              delete [] ionicSolToSaveW;
              delete [] ionicSolToSaveS;
            }

            else if(FelisceParam::instance().typeOfIonicModel == "BCL") {
              //BCL
              double * ionicSolToSaveH = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
              double * ionicSolToSaveF = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
              double * ionicSolToSaveR = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
              double * ionicSolToSaveS = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];

              std::vector<std::string> varNames;
              varNames.emplace_back("H");
              varNames.emplace_back("F");
              varNames.emplace_back("R");
              varNames.emplace_back("S");

              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveH, m_W_0[0], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveF, m_W_0[1], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveR, m_W_0[2], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveS, m_W_0[3], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));

              if (MpiInfo::rankProc() == 0) {
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveH, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[0]);
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveF, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[1]);
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveR, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[2]);
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSaveS, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[3]);

                m_linearProblemBidomain[ipb]->writeEnsightCaseCurrents(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,varNames);

              }

              delete [] ionicSolToSaveH;
              delete [] ionicSolToSaveF;
              delete [] ionicSolToSaveR;
              delete [] ionicSolToSaveS;

            }

          } else {
            if(FelisceParam::instance().typeOfIonicModel == "MV") {
              std::vector<std::string> varNames;
              varNames.emplace_back("v");
              varNames.emplace_back("w");
              varNames.emplace_back("s");

              double * ionicSolToSave = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];

              for (int iMvVar=0; iMvVar<3; iMvVar++) {
                m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSave, m_ionic->vec_sol()[iMvVar], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
                if (MpiInfo::rankProc() == 0)
                  m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSave, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[iMvVar]);
              }

              if (MpiInfo::rankProc() == 0)
                m_linearProblemBidomain[ipb]->writeEnsightCaseCurrents(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,varNames);

              delete [] ionicSolToSave;
            } else if(FelisceParam::instance().typeOfIonicModel == "BCL") {
              std::vector<std::string> varNames;
              varNames.emplace_back("H");
              varNames.emplace_back("F");
              varNames.emplace_back("R");
              varNames.emplace_back("S");

              double * ionicSolToSave = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];

              for (int iMvVar=0; iMvVar<4; iMvVar++) {
                m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSave, m_ionic->vec_sol()[iMvVar], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
                if (MpiInfo::rankProc() == 0)
                  m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSave, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), varNames[iMvVar]);
              }

              if (MpiInfo::rankProc() == 0)
                m_linearProblemBidomain[ipb]->writeEnsightCaseCurrents(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,varNames);

              delete [] ionicSolToSave;
            } else if(FelisceParam::instance().typeOfIonicModel == "Paci") {
              std::vector<std::string> varNames;
              varNames.emplace_back("h");
              varNames.emplace_back("j");
              varNames.emplace_back("m");
              varNames.emplace_back("d");
              varNames.emplace_back("fCa");
              varNames.emplace_back("f1");
              varNames.emplace_back("f2");
              varNames.emplace_back("r");
              varNames.emplace_back("q");
              varNames.emplace_back("xr1");
              varNames.emplace_back("xr2");
              varNames.emplace_back("xs");
              varNames.emplace_back("xf");
              varNames.emplace_back("g");
              varNames.emplace_back("Cai");
              varNames.emplace_back("Casr");
              varNames.emplace_back("Nai");
              varNames.emplace_back("df1");
              varNames.emplace_back("INaCa");
              varNames.emplace_back("IpCa");
              varNames.emplace_back("IbCa");
              varNames.emplace_back("ENa");
              varNames.emplace_back("EKs");
              varNames.emplace_back("ECa");
              varNames.emplace_back("INa");
              varNames.emplace_back("ICaL");
              varNames.emplace_back("Ito");
              varNames.emplace_back("IKr");
              varNames.emplace_back("IKs");
              varNames.emplace_back("IK1");
              varNames.emplace_back("If");
              varNames.emplace_back("INaK");
              varNames.emplace_back("Irel");
              varNames.emplace_back("Iup");
              varNames.emplace_back("Ileak");
              varNames.emplace_back("IbNa");

              double * ionicSolToSave = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];

              for (int iPaciVar=0; iPaciVar<42; iPaciVar++) {
                m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSave, m_ionic->vec_sol()[iPaciVar], rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
                if (MpiInfo::rankProc() == 0)
                  m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSave,static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution),varNames[iPaciVar]);
              }
              if (MpiInfo::rankProc() == 0)
                m_linearProblemBidomain[ipb]->writeEnsightCaseCurrents(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,varNames);

              delete [] ionicSolToSave;

            } else {
              double * ionicSolToSave = new double[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)];
              m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSave, m_ionic->sol(), rankProc, 1, m_linearProblemBidomain[ipb]->numDofPerUnknown(0));
              if (rankProc == 0) {
                m_linearProblemBidomain[ipb]->writeEnsightScalar(ionicSolToSave, static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution), "ionicVar");
                m_linearProblemBidomain[ipb]->writeEnsightCase(static_cast<int>(m_fstransient->iteration/FelisceParam::instance().frequencyWriteSolution)+1,"ionicVar");
              }
              delete [] ionicSolToSave;
            }
          }
        }
        if (MpiInfo::rankProc() == 0) {
          for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
            if ( m_ios[iio]->typeOutput == 1 ) // 1 : ensight{
              m_ios[iio]->postProcess(m_fstransient->time/*, !m_sameGeometricMeshForVariable*/);
          }
        }
      }
    }

    for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
      const int iMesh = m_linearProblem[ipb]->currentMesh();

      if( m_solutionBackup[ipb].hasToBackup(m_fstransient->iteration /*+ std::max(FelisceParam::instance().orderBdfEdp,FelisceParam::instance().orderBdfIonic)*/, FelisceParam::instance().frequencyBackup) && ( m_fstransient->iteration > ( std::max(FelisceParam::instance().orderBdfEdp,FelisceParam::instance().orderBdfIonic) - 1 ) ) ) {
        if(FelisceParam::verbose() > 1)
          PetscPrintf(MpiInfo::petscComm(),"Backup the solutions\n");

        double * supportVariable1 = nullptr;
        double * supportVariable2 = nullptr;
        double * supportVariable3 = nullptr;
        double * supportVariable4 = nullptr;
        double * supportVariable5 = nullptr;
        double * supportVariable6 = nullptr;
        std::vector<double*> supportVariableCourt;

        // Backup of linear problem variables
        m_linearProblemBidomain[ipb]->writeSolutionBackup(m_initialCondition.listVariable(),  MpiInfo::rankProc(), m_solutionBackup[ipb].listIO(), m_fstransient->time, m_fstransient->iteration);
        felInt sizeSupportVariable = 0;

        // Backup of linear problem support variables (bdfEdp, bdfIonicModel, IonicModel variables, ... )
        if ( FelisceParam::instance().orderBdfEdp > 0 ) {
          m_bdfEdp.sol_n().getSize(&sizeSupportVariable);
          supportVariable1 = new double[sizeSupportVariable];
          m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariable1, m_bdfEdp.sol_n(), MpiInfo::rankProc(), 1);
          m_linearProblem[ipb]->writeSupportVariableBackup("bdf_sol_n", supportVariable1, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
        }

        if ( FelisceParam::instance().orderBdfEdp > 1 ) {
          m_bdfEdp.sol_n_1().getSize(&sizeSupportVariable);
          supportVariable2 = new double[sizeSupportVariable];
          m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariable2, m_bdfEdp.sol_n_1(), MpiInfo::rankProc(), 1);
          m_linearProblem[ipb]->writeSupportVariableBackup("bdf_sol_n_1", supportVariable2, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
        }

        if ( FelisceParam::instance().orderBdfEdp > 2 ) {
          m_bdfEdp.sol_n_2().getSize(&sizeSupportVariable);
          supportVariable3 = new double[sizeSupportVariable];
          m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariable3, m_bdfEdp.sol_n_2(), MpiInfo::rankProc(), 1);
          m_linearProblem[ipb]->writeSupportVariableBackup("bdf_sol_n_2", supportVariable3, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
        }

        if ( (FelisceParam::instance().typeOfIonicModel == "MV") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
          //MV
          double * ionicSolToSaveV = nullptr;
          double * ionicSolToSaveW = nullptr;
          double * ionicSolToSaveS = nullptr;
          if ( FelisceParam::instance().orderBdfIonic > 0 ) {
            m_ionic->vec_sol(0).getSize(&sizeSupportVariable);
            ionicSolToSaveV = new double[sizeSupportVariable];
            m_ionic->vec_sol(1).getSize(&sizeSupportVariable);
            ionicSolToSaveW = new double[sizeSupportVariable];
            m_ionic->vec_sol(2).getSize(&sizeSupportVariable);
            ionicSolToSaveS = new double[sizeSupportVariable];

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveV, m_ionic->vec_sol(0), MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_v_n", ionicSolToSaveV, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveW, m_ionic->vec_sol(1), MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_w_n", ionicSolToSaveW, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveS, m_ionic->vec_sol(2), MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_s_n", ionicSolToSaveS, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }

          if ( FelisceParam::instance().orderBdfIonic > 1 ) {
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveV, m_ionic->vec_sol_n_1()[0], MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_v_n_1", ionicSolToSaveV, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveW, m_ionic->vec_sol_n_1()[1], MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_w_n_1", ionicSolToSaveW, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveS, m_ionic->vec_sol_n_1()[2], MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_s_n_1", ionicSolToSaveS, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }

          if ( FelisceParam::instance().orderBdfIonic > 2 ) {
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveV, m_ionic->vec_sol_n_2()[0], MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_v_n_2", ionicSolToSaveV, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveW, m_ionic->vec_sol_n_2()[1], MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_w_n_2", ionicSolToSaveW, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);

            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(ionicSolToSaveS, m_ionic->vec_sol_n_2()[2], MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_s_n_2", ionicSolToSaveS, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }
          if (ionicSolToSaveV) delete[] ionicSolToSaveV;
          if (ionicSolToSaveW) delete[] ionicSolToSaveW;
          if (ionicSolToSaveS) delete[] ionicSolToSaveS;
        }
        else {
          if ( FelisceParam::instance().orderBdfIonic > 0 ) {
            m_ionic->sol().getSize(&sizeSupportVariable);
            supportVariable4 = new double[sizeSupportVariable];
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariable4, m_ionic->sol(), MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_n", supportVariable4, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }

          if ( FelisceParam::instance().orderBdfIonic > 1 ) {
            m_ionic->sol_n_1().getSize(&sizeSupportVariable);
            supportVariable5 = new double[sizeSupportVariable];
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariable5, m_ionic->sol_n_1(), MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_n_1", supportVariable5, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }

          if ( FelisceParam::instance().orderBdfIonic > 2 ) {
            m_ionic->sol_n_2().getSize(&sizeSupportVariable);
            supportVariable6 = new double[sizeSupportVariable];
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariable6, m_ionic->sol_n_2(), MpiInfo::rankProc(), 1);
            m_linearProblem[ipb]->writeSupportVariableBackup("ion_sol_n_2", supportVariable6, sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }
        }

        if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
          std::string ionicVarFileName;
          for (int iCourt=0 ; iCourt<29; iCourt++) {
            supportVariableCourt.push_back(new double[sizeSupportVariable]);
            m_linearProblemBidomain[ipb]->fromVecToDoubleStar(supportVariableCourt[iCourt], m_court->ionicVariable(iCourt), MpiInfo::rankProc(), 1);
            ionicVarFileName = "court_ionic_var" + std::to_string(iCourt);
            m_linearProblem[ipb]->writeSupportVariableBackup(ionicVarFileName, supportVariableCourt[iCourt], sizeSupportVariable, 1, MpiInfo::rankProc(), m_solutionBackup[ipb].io(iMesh), m_fstransient->time, m_fstransient->iteration);
          }
        }

        m_solutionBackup[ipb].backup(m_fstransient->time, m_mesh, MpiInfo::rankProc());

        if (supportVariable1 != nullptr)
          delete [] supportVariable1;
        if (supportVariable2 != nullptr)
          delete [] supportVariable2;
        if (supportVariable3 != nullptr)
          delete [] supportVariable3;
        if (supportVariable4 != nullptr)
          delete [] supportVariable4;
        if (supportVariable5 != nullptr)
          delete [] supportVariable5;
        if (supportVariable6 != nullptr)
          delete [] supportVariable6;
        if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
          for (int iCourt=0 ; iCourt<29; iCourt++) {
            if (supportVariableCourt[iCourt] != nullptr)
              delete [] supportVariableCourt[iCourt];
          }
        }
      }
    }
  }

  void BidomainModel::userDefinedInitialConditions(std::size_t ipb) {
    if(FelisceParam::verbose() > 0) PetscPrintf(MpiInfo::petscComm(),"Restore solutions with previous simulation.\n");
    double * supportVariable = nullptr;
      // for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
      int iterationToRead = FelisceParam::instance().restartSolutionIter + 1 - std::max(FelisceParam::instance().orderBdfEdp, FelisceParam::instance().orderBdfIonic);

      int count = 0;
      supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
      m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
      count++;
      for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
        supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
      }
      m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_U_0, m_linearProblemBidomain[ipb]->numDof());
      if(FelisceParam::verbose() >= 40)
        m_U_0.view();
      delete [] supportVariable;
      supportVariable = nullptr;

      if ( FelisceParam::instance().orderBdfEdp > 0 ) {
        supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
        m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
        for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
          supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
        }
        count++;
        m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_bdf_sol_n, m_linearProblemBidomain[ipb]->numDof());
        if(FelisceParam::verbose() >= 40)
          m_bdf_sol_n.view();
        delete [] supportVariable;
        supportVariable = nullptr;
      }

      if ( FelisceParam::instance().orderBdfEdp > 1 ) {
        supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
        m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
        count++;
        for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
          supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
        }
        m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_bdf_sol_n_1, m_linearProblemBidomain[ipb]->numDof());
        if(FelisceParam::verbose() >= 40)
          m_bdf_sol_n_1.view();
        delete [] supportVariable;
        supportVariable = nullptr;
      }

      if ( FelisceParam::instance().orderBdfEdp > 2 ) {
        supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
        m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
        count++;
        for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
          supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
        }
        m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_bdf_sol_n_2, m_linearProblemBidomain[ipb]->numDof());
        if(FelisceParam::verbose() >= 40)
          m_bdf_sol_n_2.view();
        delete [] supportVariable;
        supportVariable = nullptr;
      }

      if ( (FelisceParam::instance().typeOfIonicModel == "MV") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        if ( FelisceParam::instance().orderBdfIonic > 0 ) {
          m_ionic_sol_n.resize(3);
          for (int iVar=0; iVar<3; iVar++) {
            supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
            m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
            count++;
            for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
              supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
            }
            m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_ionic_sol_n[iVar], m_linearProblemBidomain[ipb]->numDof());
            if(FelisceParam::verbose() >= 40)
              m_ionic_sol_n[iVar].view();
            delete [] supportVariable;
            supportVariable = nullptr;
          }
        }
        if ( FelisceParam::instance().orderBdfIonic > 1 ) {
          m_ionic_sol_n_1.resize(3);
          for (int iVar=0; iVar<3; iVar++) {
            supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
            m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
            count++;
            for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
              supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
            }
            m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_ionic_sol_n_1[iVar], m_linearProblemBidomain[ipb]->numDof());
            if(FelisceParam::verbose() >= 40)
              m_ionic_sol_n_1[iVar].view();
            delete [] supportVariable;
            supportVariable = nullptr;
          }
        }
        if ( FelisceParam::instance().orderBdfIonic > 2 ) {
          m_ionic_sol_n_2.resize(3);
          for (int iVar=0; iVar<3; iVar++) {
            supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
            m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
            count++;
            for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
              supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
            }
            m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_ionic_sol_n_2[iVar], m_linearProblemBidomain[ipb]->numDof());
            if(FelisceParam::verbose() >= 40)
              m_ionic_sol_n_2[iVar].view();
            delete [] supportVariable;
            supportVariable = nullptr;
          }
        }
      }
      else {
        if ( FelisceParam::instance().orderBdfIonic > 0 ) {
          m_ionic_sol_n.resize(1);
          supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
          m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
          count++;
          for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
            supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
          }
          m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_ionic_sol_n[0], m_linearProblemBidomain[ipb]->numDof());
          if(FelisceParam::verbose() >= 40)
            m_ionic_sol_n[0].view();
          delete [] supportVariable;
          supportVariable = nullptr;
        }

        if ( FelisceParam::instance().orderBdfIonic > 1 ) {
          m_ionic_sol_n_1.resize(1);
          supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
          m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
          count++;
          for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
            supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
          }
          m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_ionic_sol_n_1[0], m_linearProblemBidomain[ipb]->numDof());
          if(FelisceParam::verbose() >= 40)
            m_ionic_sol_n_1[0].view();
          delete [] supportVariable;
          supportVariable = nullptr;
        }

        if ( FelisceParam::instance().orderBdfIonic > 2 ) {
          m_ionic_sol_n_2.resize(1);
          supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
          m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDofPerUnknown(0) );
          count++;
          for (felInt idof=0 ; idof<m_linearProblemBidomain[ipb]->numDofPerUnknown(0); idof++) {
            supportVariable[m_linearProblemBidomain[ipb]->numDofPerUnknown(0)+idof] = 0.0;
          }
          m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_ionic_sol_n_2[0], m_linearProblemBidomain[ipb]->numDof());
          if(FelisceParam::verbose() >= 40)
            m_ionic_sol_n_2[0].view();
          delete [] supportVariable;
          supportVariable = nullptr;
        }
      }

      if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        std::string ionicVarFileName;
        m_courtVarInit.resize(29);
        for (int iCourt=0 ; iCourt<29; iCourt++) {
          supportVariable = new double[m_linearProblemBidomain[ipb]->numDof()];
          m_initialCondition.readVariable(count, iterationToRead, supportVariable, m_linearProblemBidomain[ipb]->numDof());
          count++;
          m_linearProblemBidomain[ipb]->fromDoubleStarToVec(supportVariable,&m_courtVarInit[iCourt], m_linearProblemBidomain[ipb]->numDof());
          if(FelisceParam::verbose() >= 40)
            m_courtVarInit[iCourt].view();
          delete [] supportVariable;
          supportVariable = nullptr;
        }
      }

//    }

  }

  void BidomainModel::forward()
  {


    //Write solution with ensight.
    BidomainModel::writeSolution();

    //Advance time step.
    if (FelisceParam::instance().stateFilter) {
      // clear RHS, and m_Matrix[0].
      updateTime(FlagMatrixRHS::matrix_and_rhs);
    }
    else {
      // clear only RHS, NOT m_Matrix[0].
      updateTime(FlagMatrixRHS::only_rhs);
    }
    printNewTimeIterationBanner();


    if ( m_fstransient->iteration == 1 && !FelisceParam::instance().restartSolution) {

      //Initialization of bdf solver.
      m_bdfEdp.initialize(m_linearProblemBidomain[0]->solution());
      m_linearProblemBidomain[0]->initializeTimeScheme(&m_bdfEdp);

      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolate);

      //Initialization of Schaf solver.
      m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());

      m_ionic->initializeExtrap(m_extrapolate);
      m_ionic->initialize(m_W_0);
      m_buildIonic = true;
      if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        m_court->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());
        m_court->initializeExtrap(m_extrapolate);
        m_court->initializeConcentrationsAndGateConditions(m_m, m_h, m_j, m_ao, m_io, m_ua, m_ui, m_xr, m_xs, m_d, m_f, m_fca, m_urel, m_vrel, m_wrel, m_nai, m_nao, m_cao, m_ki, m_ko, m_cai, m_naiont, m_kiont, m_caiont, m_ileak, m_iup, m_itr, m_irel, m_cmdn, m_trpn, m_nsr, m_jsr, m_csqn);
        m_buildCourt = true;
      }

    }
    else if ( m_fstransient->iteration == 1 && FelisceParam::instance().restartSolution) {
      // Read vectors:
      //Initialization of bdf solver.
      if (FelisceParam::instance().orderBdfEdp == 1) {
        m_bdfEdp.initialize(m_bdf_sol_n);
      }
      else if (FelisceParam::instance().orderBdfEdp == 2) {
        m_bdfEdp.initialize(m_bdf_sol_n_1,m_bdf_sol_n);
      }
      else if (FelisceParam::instance().orderBdfEdp == 2) {
        m_bdfEdp.initialize(m_bdf_sol_n_2,m_bdf_sol_n_1,m_bdf_sol_n);
      }
      m_linearProblemBidomain[0]->initializeTimeScheme(&m_bdfEdp);
      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolate);


      //Initialization of ionic solver.
      m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());

      if (FelisceParam::instance().orderBdfIonic == 1) {
        m_ionic->initialize(m_ionic_sol_n);
      }
      else if (FelisceParam::instance().orderBdfIonic == 2) {
        m_ionic->initialize(m_ionic_sol_n, m_ionic_sol_n_1);
      }
      else if (FelisceParam::instance().orderBdfIonic == 3) {
        m_ionic->initialize(m_ionic_sol_n, m_ionic_sol_n_1, m_ionic_sol_n_2);
      }
      m_ionic->initializeExtrap(m_extrapolate);
      m_buildIonic = true;

      if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        m_court->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());
        m_court->initializeExtrap(m_extrapolate);
        m_court->initializeConcentrationsAndGateConditions(m_courtVarInit[0], m_courtVarInit[1], m_courtVarInit[2], m_courtVarInit[3], m_courtVarInit[4], m_courtVarInit[5], m_courtVarInit[6], m_courtVarInit[7], m_courtVarInit[8], m_courtVarInit[9], m_courtVarInit[10], m_courtVarInit[11], m_courtVarInit[12], m_courtVarInit[13], m_courtVarInit[14], m_courtVarInit[15], m_courtVarInit[16], m_courtVarInit[17], m_courtVarInit[18], m_courtVarInit[19], m_courtVarInit[20], m_courtVarInit[21], m_courtVarInit[22], m_courtVarInit[23], m_courtVarInit[23], m_courtVarInit[23], m_courtVarInit[23], m_courtVarInit[23], m_courtVarInit[24], m_courtVarInit[25], m_courtVarInit[26], m_courtVarInit[27], m_courtVarInit[28]);
        m_buildCourt = true;
      }

    }
    else {
      m_bdfEdp.update(m_linearProblemBidomain[0]->solution());
      m_bdfEdp.extrapolate(m_extrapolate);
      m_ionic->update(m_extrapolate);

      if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
        m_court->updateExtrap(m_extrapolate);
      }
    }

    // Long QT segment during period [torsadeTimeBegin,torsadeTimeEnd]
    if (FelisceParam::instance().torsade) {
      if ( std::abs( m_fstransient->time - FelisceParam::instance().torsadeTimeBegin ) < 2*FelisceParam::instance().timeStep ) {
        // Warning: extra-data m_linearProblemBidomain[0]->EndocardiumDistance() size has to be numDof (not number of mesh points).
        m_linearProblemBidomain[0]->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose(),m_linearProblemBidomain[0]->EndocardiumDistance(), m_fstransient->time);
      }
      if ( std::abs( m_fstransient->time - FelisceParam::instance().torsadeTimeEnd ) < 2*FelisceParam::instance().timeStep ) {
        // Warning: extra-data m_linearProblemBidomain[0]->EndocardiumDistance() size has to be numDof (not number of mesh points).
        m_linearProblemBidomain[0]->evalFunctionOnDof(m_schaf->fctTauClose(), m_schaf->tauClose(),m_linearProblemBidomain[0]->EndocardiumDistance(), m_fstransient->time);
      }
    }

    //Solve ionic model(s) and calculate Iion.

    m_ionic->computeRHS();
    m_ionic->solveEDO();
    m_ionic->computeIon();
    if ( (FelisceParam::instance().typeOfIonicModel != "BCL") && (FelisceParam::instance().typeOfIonicModel != "Paci")) {
      m_ionic->updateBdf();
    }



    if ( (FelisceParam::instance().typeOfIonicModel == "courtAtriaSchafVent") || (FelisceParam::instance().typeOfIonicModel == "courtAtriaMvVent") ) {
      m_court->computeIon();
    }

    m_bdfEdp.computeRHSTime(m_fstransient->timeStep);

    // Assembling of matrices at first iteration (because of needs some coefficients not defined at iteration 0).

    if ( m_fstransient->iteration == 1 ) {
      if (FelisceParam::instance().hasMoveMesh) {
        m_linearProblemBidomain[0]->readDataDisplacement(m_ios,m_fstransient->iteration);
        m_linearProblemBidomain[0]->meshLocal()->moveMesh(m_linearProblemBidomain[0]->Displacement(),1.0);
        m_linearProblemBidomain[0]->assembleMatrixRHS(MpiInfo::rankProc());
      }
      else {
        if ( FelisceParam::instance().hasCoupledAtriaVent ) {
          m_linearProblemBidomain[0]->assembleMatrixRHSCurrentAndCurvilinearElement(MpiInfo::rankProc());
          //m_linearProblemBidomain[0]->initAndStoreMassMatrix();
        }
        else {
          m_linearProblemBidomain[0]->assembleMatrixRHS(MpiInfo::rankProc());
        }

        if(FelisceParam::instance().stateFilter) {
          m_luenbFlter.duplicateFrom(m_linearProblemBidomain[0]->matrix(1),MAT_COPY_VALUES);
          m_luenbFlter.assembly(MAT_FINAL_ASSEMBLY);
          m_luenbFlter.zeroEntries();
        }
      }
    }
    else {
      if (FelisceParam::instance().hasMoveMesh) {
        m_linearProblemBidomain[0]->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
        m_linearProblemBidomain[0]->clearMatrix(1);
        m_linearProblemBidomain[0]->readDataDisplacement(m_ios,m_fstransient->iteration);
        if (FelisceParam::instance().hasMoveFibers)
          m_linearProblemBidomain[0]->updateFibers(*io(),m_fstransient->iteration*m_fstransient->timeStep);
        m_linearProblemBidomain[0]->meshLocal()->moveMesh(m_linearProblemBidomain[0]->Displacement(),1.0);
        m_linearProblemBidomain[0]->assembleMatrixRHS(MpiInfo::rankProc());

      }
    }



    //Specific operations before solve the system : calculate m_RHS std::vector used in solve().
    postAssemblingMatrixRHS();


    //Simulation of MEA experimentation
    if(FelisceParam::instance().bc_potExtraCell == "MEA_grounds") {
      m_linearProblem[0]->finalizeEssBCTransient();
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
    }


    //Solve the system m_Matrix[0]*m_sol=m_RHS.
    //std::cout << "MPI INFO : " << MpiInfo::rankProc() << " " << MpiInfo::numProc() << std::endl;
    m_linearProblemBidomain[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    // Choose zero mean value solution.
    if(FelisceParam::instance().bc_potExtraCell != "MEA_grounds") {
       m_linearProblemBidomain[0]->removeAverageFromSolution(potExtraCell, MpiInfo::rankProc());
    }
    //m_linearProblemBidomain[0]->removeAverageForPotExtraCell();


  }

  void BidomainModel::forwardROM() {
    //Write solution with ensight.
    BidomainModel::writeSolution();
    //Advance time step.
    updateTime(FlagMatrixRHS::only_rhs);
    printNewTimeIterationBanner();

    if ( m_fstransient->iteration == 1) {
      //Initialization of bdf solver.
      m_bdfEdp.initialize(m_linearProblemBidomain[0]->solution());
      m_linearProblemBidomain[0]->initializeTimeScheme(&m_bdfEdp);
      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolate);
      //Initialization of Schaf solver.
      m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());
      m_ionic->initializeExtrap(m_extrapolate);
      m_ionic->initialize(m_W_0[0]);
      m_buildIonic = true;
    } else {
      m_bdfEdp.update(m_linearProblemBidomain[0]->solution());
      m_bdfEdp.extrapolate(m_extrapolate);
      m_ionic->update(m_extrapolate);
    }

    //Solve Mithcell and Schaeffer model and calculate Iion.
    m_ionic->computeRHS();
    m_ionic->solveEDO();
    m_ionic->computeIon();
    m_ionic->updateBdf();

    m_bdfEdp.computeRHSTime(m_fstransient->timeStep);

    // Assembling of matrices at first iteration (because of needs some coefficients not defined at iteration 0).
    if ( m_fstransient->iteration == 1 ) {
      m_linearProblemBidomain[0]->assembleMatrixRHS(MpiInfo::rankProc());
      if(FelisceParam::instance().stateFilter) {
        m_luenbFlter.duplicateFrom(m_linearProblemBidomain[0]->matrix(1),MAT_COPY_VALUES);
        m_luenbFlter.assembly(MAT_FINAL_ASSEMBLY);
        m_luenbFlter.zeroEntries();
      }
    }
    //Specific operations before solve the system : calculate m_RHS std::vector used in solve().
    postAssemblingMatrixRHS();
    //Solve the system m_Matrix[0]*m_sol=m_RHS.
    m_rom->solveROM(m_linearProblemBidomain[0]->solution());

    m_linearProblemBidomain[0]->removeAverageFromSolution(potExtraCell, MpiInfo::rankProc());

  }

  MVSolver* BidomainModel::getMVSolver() {
    return m_mv;
  }
#ifdef FELISCE_WITH_CVGRAPH
  void BidomainModel::forwardLight() {

    //Advance time step.
    std::stringstream msg;
    msg<<"Starting a new time step, preparing forward"<<std::endl;
    PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
    BidomainModel::writeSolution();
    updateTime(FlagMatrixRHS::only_rhs);
    printNewTimeIterationBanner();

    // Initialize a bunch of stuff. Should be moved elsewhere.
    if (m_fstransient->iteration == 1) {
      //Initialization of bdf solver.
      m_bdfEdp.initialize(m_linearProblemBidomain[0]->solution());
      m_linearProblemBidomain[0]->initializeTimeScheme(&m_bdfEdp);
      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolate);
      //Initialization of ionic solver.
      m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());
      m_ionic->initializeExtrap(m_extrapolate);
      m_ionic->initialize(m_W_0);
      m_buildIonic = true;
    } else {
      m_bdfEdp.update(m_linearProblemBidomain[0]->solution());
      m_bdfEdp.extrapolate(m_extrapolate);
      m_ionic->update(m_extrapolate);
    }

    m_ionic->computeRHS();
    m_ionic->solveEDO();
    m_ionic->computeIon();
    m_ionic->updateBdf();
    m_bdfEdp.computeRHSTime(m_fstransient->timeStep);

    if (m_fstransient->iteration == 1){
      m_linearProblemBidomain[0]->assembleMatrixRHSCurrentAndCurvilinearElement(MpiInfo::rankProc());
      //m_linearProblem[0]->createAndCopyMatrixRHSWithoutBC(FlagMatrixRHS::only_matrix);
      m_linearProblemBidomain[0]->initAndStoreMassMatrix();
    }

    //Specific operations before solve the system : calculate m_RHS std::vector used in solve().
    postAssemblingMatrixRHS();

    // Apply the BC
    m_linearProblem[0]->finalizeEssBCConstantInT();
    m_linearProblem[0]->finalizeEssBCTransient();
    if (m_fstransient->iteration == 1){
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs, 0, true, ApplyNaturalBoundaryConditions::no);
    } else {
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs, 0, true, ApplyNaturalBoundaryConditions::no);
    }


    //Solve the system m_Matrix[0]*m_sol=m_RHS.
    m_linearProblemBidomain[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    // Remove average
    m_linearProblemBidomain[0]->removeAverageForPotExtraCell();
    //m_linearProblemBidomain[0]->removeAverageFromSolution(potExtraCell, MpiInfo::rankProc());

  }


  void BidomainModel::startIterationCVG() {
    cvgMainSlave* slave=m_linearProblem[0]->slave();
    if (slave==nullptr) {
      FEL_ERROR("slave not initialized");
    }
    //Advance time step.
    if( slave->newTimeStep() ) {
      m_kount = 0;
      std::stringstream msg;
      msg<<"Starting a new time step, preparing forward"<<std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      BidomainModel::writeSolution();
      updateTime(FlagMatrixRHS::only_rhs);
      printNewTimeIterationBanner();
    } else if( slave->redoTimeStep() ){
      m_kount++;
      std::stringstream msg;
      msg << "......................................" << std::endl;
      msg << "Redo time step at time t= " << m_fstransient->time << std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      m_linearProblemBidomain[0]->clearMatrixRHS(FlagMatrixRHS::only_rhs);
    } else {
      slave->msgInfo().print(1);
      FEL_ERROR("Error: strange timeStatus");
    }


    // Initialize a bunch of stuff. Should be moved elsewhere.
    if ( ( m_fstransient->iteration == 1) && slave->newTimeStep()) {
      //Initialization of bdf solver.
      m_bdfEdp.initialize(m_linearProblemBidomain[0]->solution());
      m_linearProblemBidomain[0]->initializeTimeScheme(&m_bdfEdp);
      //m_extrapolate = initial solution.
      m_bdfEdp.extrapolate(m_extrapolate);
      //Initialization of ionic solver.
      m_ionic->defineSizeAndMappingOfIonicProblem(m_linearProblemBidomain[0]->numDofLocalPerUnknown(potTransMemb), m_linearProblemBidomain[0]->mappingDofLocalToDofGlobal(potTransMemb), m_linearProblemBidomain[0]->ao());
      m_ionic->initializeExtrap(m_extrapolate);
      m_ionic->initialize(m_W_0);
      m_buildIonic = true;
    } else if (slave->newTimeStep()) {
      m_bdfEdp.update(m_linearProblemBidomain[0]->solution());
      m_bdfEdp.extrapolate(m_extrapolate);
      m_ionic->update(m_extrapolate);
    }

    if (slave->newTimeStep()){
      m_ionic->computeRHS();
      m_ionic->solveEDO();
      m_ionic->computeIon();
      m_ionic->updateBdf();
      m_bdfEdp.computeRHSTime(m_fstransient->timeStep);
    }


    if ( (m_fstransient->iteration == 1) && slave->newTimeStep() ){
      m_linearProblemBidomain[0]->assembleMatrixRHSCurrentAndCurvilinearElement(MpiInfo::rankProc());
      m_linearProblem[0]->createAndCopyMatrixRHSWithoutBC(FlagMatrixRHS::only_matrix);
      m_linearProblemBidomain[0]->initAndStoreMassMatrix();
    }

    //Specific operations before solve the system : calculate m_RHS std::vector used in solve().
    postAssemblingMatrixRHS();
    m_linearProblemBidomain[0]->applyUserDefinedStimulation(m_fstransient->time);
    m_linearProblem[0]->createAndCopyMatrixRHSWithoutBC(FlagMatrixRHS::only_rhs);

    // Apply the BC
    m_linearProblem[0]->finalizeEssBCConstantInT();
    m_linearProblem[0]->finalizeEssBCTransient();
    if (( m_fstransient->iteration == 1)  && slave->newTimeStep() ){
      //std::cout << "BidomainModel::startIterationCVG(): FIRST TIME STEP" <<std::endl;
      if (slave->thereIsAtLeastOneRobinCondition()){
        //std::cout << "  CVG Robin: we apply the robin condition in the MATRIX ONLY. The RHS parts are handled in cvgAddExternalResidualToRHS() and cvgAddRobinToRHS()." << std::endl;
        m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod,MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::only_matrix, 0, true, ApplyNaturalBoundaryConditions::yes);
      } else {
        //std::cout << "  CVG Neumann: Neumann from other compartment handled in cvgAddExternalResidualToRHS()" << std::endl;
        m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs, 0, true, ApplyNaturalBoundaryConditions::no);
      }
    } else {
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs, 0, true, ApplyNaturalBoundaryConditions::no);
    }

    if (slave->thereIsAtLeastOneRobinCondition()){
      //std::cout << "  CVG Robin: Add remaining term in RHS: beta_torso*massBD*u_torso" << std::endl;
      m_linearProblemBidomain[0]->cvgAddRobinToRHS();
    }
    // Add current from other compartment
    //m_linearProblemBidomain[0]->addCurrentToRHS();
    //Solve the system m_Matrix[0]*m_sol=m_RHS.
    double tic, toc, tsolve;
    tic = clock();
    m_linearProblemBidomain[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    toc = clock();
    tsolve = (toc-tic)/CLOCKS_PER_SEC;
    // Remove average
    //double trmvav, trmvav2;
    //m_linearProblemBidomain[0]->removeAverageFromSolution(potExtraCell, MpiInfo::rankProc());
    //toc = clock();
    //trmvav = (toc-tic)/CLOCKS_PER_SEC;
    //tic = clock();

    m_linearProblemBidomain[0]->removeAverageForPotExtraCell();

    //toc = clock();
    //trmvav = (toc-tic)/CLOCKS_PER_SEC;
    std::cout << "Tsolve = " << tsolve <<  std::endl;// ",Trmvav2=" << trmvav2<< std::endl;
  }
#endif

}
