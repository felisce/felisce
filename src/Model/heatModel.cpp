//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/heatModel.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce {
  HeatModel::HeatModel():Model() {
    m_name = "Heat";
  }
  // Called in Model::initializeLinearProblem, just after the mesh partitioning.
  void HeatModel::initializeDerivedModel() {
    // Saving the lpb pointer after the static cast
    m_lpb = static_cast<LinearProblemHeat*>(m_linearProblem[0]);
    // Initializing the PetscVectors
    m_lpb->initPetscVectors();
  }

  void HeatModel::preAssemblingMatrixRHS(std::size_t /*iProblem*/) {
    // Zero-ing the solution
    m_lpb->solution().zeroEntries();
    // Assembling the static part of the matrix.
    m_lpb->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
  }

  void HeatModel::prepareForward() {
    // Write solution for postprocessing (if required)
    writeSolution();

    // Advance time step.
    updateTime();
    m_lpb->updateOldQuantities();
    // Print time information
    printNewTimeIterationBanner();
  }
  void HeatModel::solveHeat() {
    //Apply boundary conditions.
    m_lpb->finalizeEssBCTransient();
    #ifdef FELISCE_WITH_CVGRAPH
    if ( !FelisceParam::instance().withCVG || ! m_linearProblem[0]->slave()->redoTimeStepOnlyCVGData() ) {
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
    } else {
      m_linearProblem[0]->applyOnlyCVGBoundaryCondition();
    }
    #else
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
    #endif

    //Solve linear system.
    m_lpb->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    
    if (FelisceParam::instance().withCVG ) {
      m_linearProblem[0]->computeResidual();
    }
  }
  void HeatModel::forward() {
    prepareForward();
    //Assembly loop on elements.
    if (FelisceParam::instance().hasMoveMesh) {
      m_lpb->readDataDisplacement(m_ios, m_fstransient->iteration);
      m_lpb->meshLocal()->moveMesh(m_lpb->Displacement(),1.0);
    }

    m_lpb->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
    m_lpb->matrix(0).axpy(1,m_lpb->matrix(1),SAME_NONZERO_PATTERN);
    
    solveHeat();
  }


  #ifdef FELISCE_WITH_CVGRAPH
  void HeatModel::cvgraphNewTimeStep() {
    prepareForward();

    m_lpb->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);
    m_lpb->matrix(0).axpy(1,m_lpb->matrix(1),SAME_NONZERO_PATTERN);
    m_lpb->createAndCopyMatrixRHSWithoutBC();
  }
  #endif

  // State functions!
  int HeatModel::getNstate() const {
    return m_lpb->numDof();
  }
  void HeatModel::getState(double* & state) {
    m_lpb->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
  void HeatModel::getState_swig(double* data, felInt numDof) {
    double * state;
    m_lpb->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
    for (felInt i=0 ;  i<numDof ; i++) {
      data[i] = state[i];
    }
  }
  void HeatModel::setState(double* & state) {
    m_lpb->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
}
