//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Aletti
//

#ifndef _CVGMAINSLAVE_HPP
#define _CVGMAINSLAVE_HPP

// System includes

// Project-specific includes
#include "Core/configure.hpp"

#ifdef FELISCE_WITH_CVGRAPH

// External includes

// Project includes
#include "Graph/cvgraph.hpp"
#include "MessageInterface/messageAdm.hpp"
#include "Solver/linearProblem.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "Core/chrono.hpp"

using namespace cvgraph;

/*!
  \file cvgMainSlave.hpp
  \authors M. Aletti
  \date Oct 2016
  \brief cvgMainSlave class.

  This class is meant to be a wrapper of the cvgraph utilities.
  This class can be used  (and compiled) only when the flag FELISCE_WITHCVGRAPH
  is activated.
  The class is meant to work correctly in parallel, however the communications
  and the interpolation are done only by the master.
  This parallel/sequential difference should be handled inside the class.
  From outside this class should be considered as parallel.

  This object is associated to a compartment (node) of the graph.

  TODO:
  1. change the name from cvgMainSlave to something better.
  2. the class contains a pointer to a linear problem. What if we want to use a felisce model that contains two linear problems?
*/
namespace felisce 
{
class cvgMainSlave 
{
private:
  // The empty constructor is removed.
  cvgMainSlave();
public:
  // Constructor
  cvgMainSlave(int argc, char ** argv);
  //Sending time before exchanging initial condition
  void exchangeInitialCondition();
  // To build m_interpolator matrix, and exchange interface information.
  void buildInterpolator(LinearProblem* pt);
  // First contact with the master
  void firstContact();
  // Interpolate the data and send them
  void sendData(std::vector<PetscVector>& sequentialVolumeVectors, std::size_t iConn);
  // just receive them
  void receiveData(std::vector<PetscVector>& sequentialVolumeVectors, std::size_t iConn);
  // update time iteration in the case of a new time step
  void printIteration(int& timeIteration);

  // Useful functions to check the time status.
  inline bool newTimeStep() const {
    return this->checkTimeStatus(cvgraph::CVG_TIME_STATUS_NEW_TIME_STEP);
  }
  inline bool redoTimeStep() const {
    return this->checkTimeStatus(cvgraph::CVG_TIME_STATUS_REDO_TIME_STEP);
  }
  inline bool redoTimeStepOnlyCVGData() const {
    return this->checkTimeStatus(cvgraph::CVG_TIME_STATUS_REDO_TIME_STEP_ONLY_CVGDATA);
  }
  inline bool stop() const {
    return this->checkTimeStatus(cvgraph::CVG_TIME_STATUS_STOP);
  }
  inline const std::vector<int>& interfaceLabels(std::size_t iConn) const {
    return m_interfaceLabels[iConn];
  }
  // Getters
  inline const MessageInfo& msgInfo() const {
    return m_msgInfo;
  }
  inline const CVGraphParam& cvgParam() const {
    return CVGraphParam::instance();
  }
  inline std::size_t numVarToRead(std::size_t iConn) const {
    return m_numVarToRead[iConn];
  }
  inline std::size_t numVarToSend(std::size_t iConn) const {
    return m_numVarToSend[iConn];
  }
  inline const std::string& sendVariable(std::size_t iConn, std::size_t cVar)  const {
    FEL_ASSERT(iConn<m_sendVariables.size());
    FEL_ASSERT(cVar<m_sendVariables[iConn].size());
    return m_sendVariables[iConn][cVar];
  }
  inline const std::string& readVariable(std::size_t iConn, std::size_t cVar) const {
    return m_readVariables[iConn][cVar];
  }
  inline std::size_t numConnections() const {
    return m_numOfNeighbours;
  }
  inline std::size_t residualInterpolationType(std::size_t iConn) const {
    return m_residualInterpolationType[iConn];
  }

  bool initialConditionNeeded( std::size_t iConn ) const;

  // Ids
  std::size_t iConn(std::size_t IdConn) const {
    return m_iConnFromIds.at(IdConn);
  }
  std::size_t IdConn(std::size_t iConn) const {
    return m_IdsOfConnections[iConn];
  }

  void print() const;
private:
  // Data of the compartment
  int m_cmptId;  // id of the current compartment in the graph
  std::string m_cmptName;    // its name
  LinearProblem* m_lpb; // its linear problem

  // General cvg info
  CVGraph m_cvg;           // the graph
  int m_protocol;          // protocol
  MessageInfo m_msgInfo;   // message info
  MessageAdm* m_msg;       // ptr to the message administrator
  int m_verbose;           // cvgraph verbosity level

  // COMMENT ON THE IDs.
  // the current compartment has m_numOfNeighbours neighbours.
  // Each of these compartments is connected to the current one via one connections.
  //
  // Inside this class iConn and iCmpt take values in 0...m_numOfNeighbours - 1 and are interchangeable.
  // However, in the global graph each compartment is described by a different ids in 0...NUM OF NODES
  // Similarly there are ids for the connections in 0...NUM OF EDGES
  // Consider that NUM OF NODES != NUM OF EDGES.
  //
  // Finally consider that in cvgraph the so-called descriptor are used to identify a connection-edge or a compartment-node.
  // These are object of the boost library and they can be obtained from the Ids through some utility functions in cvgraph.
  std::size_t m_numOfNeighbours;    // the number of nodes that are connected to it
  std::vector<std::size_t> m_IdsOfOtherCompartments;  // the ids of those nodes in the graph
  std::vector<std::size_t> m_IdsOfConnections;        // the ids of the corresponding connections
  std::map<std::size_t,std::size_t> m_iConnFromIds; // inverse of the std::vector above


  // For each connection we have an interpolator
  std::vector<PetscMatrix*> m_interpolator;             // interpolator matrix from this compartments to another one
  std::vector<PetscMatrix*> m_interpolatorBackward;     // interpolator matrix from another compartment to this one. (its transpose has the same dimension as the interpolator

  std::vector<std::vector<int> > m_interfaceLabels; // for each connection, the list of the surface label of the interface

  std::vector<CVGraphData*> m_data_recv;
  std::vector<CVGraphData*> m_data_send;

  std::vector<int> m_dimOfDataToReceive; //ncols;
  std::vector<int> m_dimOfDataToSend;    //nrows;

  std::vector<PetscVector> m_dataSendPreInterpolation;
  std::vector<PetscVector> m_dataSend;

  bool checkTimeStatus( const cvgraph::CVGStatus flag) const;
  void checkCustomFileCompleted( std::string keyword, std::size_t iBD ) const;
  void customFileCompleted( std::string keyword, std::size_t iBD ) const;
  std::string fileName(std::string keyword, std::size_t iConn, std::string readOrWrite, std::string folder="") const;
  ChronoInstance::Pointer m_receiveChronoPtr;

  // For each connection, this compartment knows which variable (denoted by a std::string) has to be sent or read.
  // depending on this the user can make the different choices.
  // These to functions are private because the user is not supposed to use them, he should use the getters sendVariable/readVariable
  std::string variableToSend( int IdConn, std::size_t cVar ) {
    return m_cvg.graph()[m_cvg.connectDescriptor( IdConn ) ].toBeSentVariable(m_cmptName, cVar);
  }
  std::string variableToRead( int IdConn, std::size_t cVar ) {
    return m_cvg.graph()[m_cvg.connectDescriptor( IdConn ) ].toBeReadVariable(m_cmptName, cVar);
  }
  std::vector<std::size_t> m_numVarToRead, m_numVarToSend;
  std::vector<std::vector<std::string> > m_sendVariables, m_readVariables;

  // id of the variable which has to be passed. For instance 0 for velocity in NS. If we wanted for some reason to couple the pressure it should be one
  std::vector<std::size_t> m_IdsOfVarToExchange; // It is std::vector because in principle we could couple more than one variable at the same time (think about poro elasticity, where could couple displacement, but also the flow). For now, it has been always used as a scalar.

  void handleCouplingVariablesAndOverwriteBC();
  std::vector<int> m_residualInterpolationType;
  void exchangeBackwardInterpolators();

public:
  // Utility functions to classify the nature of the BCs on a given connection

  // Wether we need to sum the residual directly in the RHS.
  // This happend for neumann and for robin conditions, but only when the residualInterpType == 1
  inline bool sumDirectlyIntoRHS(std::size_t iConn) const {
    return ( neumann(iConn) || robin(iConn) ) && m_residualInterpolationType[iConn] == 1;
  }
  // neumann bondary condition, one variable to read and of type neumann
  inline bool neumann(std::size_t iConn) const {
    return m_numVarToRead[iConn] == 1 && CVGraph::neumannTypeVariable(m_readVariables[iConn][0]);
  }
  // dirichlet bondary condition, one variable to read and of type dirichlet
  inline bool dirichlet(std::size_t iConn) const {
    return m_numVarToRead[iConn] == 1 && CVGraph::dirichletTypeVariable(m_readVariables[iConn][0]);
  }
  // robin boundary condition, two variables to read.
  inline bool robin(std::size_t iConn) const {
    return m_numVarToRead[iConn] == 2;
  }
  // returns the neumann of the boundary condition if it is of neumann or robin type
  // if it is of dirichlet type it returns "Error"
  std::string neumannVariable(std::size_t iConn) const;

  // return true if there is at least one dirichlet condition
  inline bool thereIsAtLeastOneDirichletCondition() const {
    return thereIsAtLeastOneConditionOfType(&cvgMainSlave::dirichlet);
  }
  // return true if there is at least one robin condition
  inline bool thereIsAtLeastOneRobinCondition() const {
    return thereIsAtLeastOneConditionOfType(&cvgMainSlave::robin);
  }
  // return true if there is at least one neumann condition
  inline bool thereIsAtLeastOneNeumannCondition() const {
    return thereIsAtLeastOneConditionOfType(&cvgMainSlave::neumann);
  }
  inline bool thereIsAtLeastOneNaturalCondition() const {
    return thereIsAtLeastOneNeumannCondition() || thereIsAtLeastOneRobinCondition();
  }
private:
  // return true if there is at least one connection satisfying the function test.
  bool thereIsAtLeastOneConditionOfType(bool(cvgMainSlave::*test)(std::size_t) const) const;
};
}
#endif
#endif
