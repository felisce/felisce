//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//


// System includes
#include <iostream>

// External includes
#ifdef FELISCE_WITH_QT
#include <QtCore/QFile>
#include <QtCore/QString>
#include <QtCore/QList>
#endif

// Project includes
#include "Model/MGInputParameters.hpp"

#ifdef FELISCE_WITH_QT

namespace felisce {

MGInputParameters::MGInputParameters():
        _success( true )
{
}

MGInputParameters::MGInputParameters( QString fileName )
{
        _success = readData( fileName );
}

MGInputParameters::~MGInputParameters()
{
}


bool MGInputParameters::readData( QString fileName )
{
        bool success = true;

        QFile dataFile( fileName );
        if (!dataFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
                QString Pb = QString("MGInputParameters::MGInputParameters : pb lecture fichier : %1").arg( fileName );
                std::cout << Pb.toLocal8Bit().constData() << std::endl;
        return false;
        }

        // ### MORPHOLOGY
        QString Line = dataFile.readLine();
        // # LungTree
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        _morphoFile = Line.replace( "\n", "" );
        int pos = _morphoFile.indexOf(".");
        // # Gas
        Line = dataFile.readLine();
        Line = dataFile.readLine();
         QStringList Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        bool ok;
        _tGas = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _dGas = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _vGas = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _mfpGas = Line.toDouble(&ok);
        // ### Ventilation
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        if ( Line.compare("SIMPLE") )
        {
                _ventType = MGInputParameters::SIMPLE;
        }
        if ( Line.compare("REAL") )
        {
                _ventType = MGInputParameters::REAL;
        }
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _tVol = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _tInh = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _tPau = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _tExh = Line.toDouble(&ok);
        // ### non linear compliance model parameters
        Line = dataFile.readLine();
        Line = dataFile.readLine();
         Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _lungCompliance = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _Vrv = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _Vfrc = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _Vtlc = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _Vfrc0g = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _gravity = Line.toInt(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _position = Line.replace( "\n", "" );
        // ### treeProperties
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _nbGeneration = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        // if ( Line.compare("healthy") )
        // {
        //        _modelBuildTree = "healthy";
        // }
        // if ( Line.compare("asthma") )
        // {
        //        _modelBuildTree = "asthma";
        // }
        // if ( Line.compare("stenosis") )
        // {
        //        _modelBuildTree = "stenosis";
        // }
        _modelBuildTree = Line.replace( "\n", "" );

        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _motherID = Line.replace( "\n", "" );

        // ### diseaseProperties
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        //_branchDisease = Line.toStdString();
        _branchDisease = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _lobeAsthma = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _diseaseRatio = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _lobeEmphysema = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _emphysemaRatio = Line.toDouble(&ok);
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _generationStenosis= Line.toDouble(&ok);
        // ### resistanceModel
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _resistanceMode = Line.replace( "\n", "" );
        // ### filesLocation
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileInhaleKatz = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileExhaleKatz = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _filePleural = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileNameFlux = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileNameVolume = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileNameFluxLOBE = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileNameVolumeLOBE = Line.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        Line = Numbers.at(1);
        _fileNamePressure = Line.replace( "\n", "" );
        // ### inputFiles
        Line = dataFile.readLine();
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        _dicoFile = Numbers.at(1);
        _dicoFile = _dicoFile.replace( "\n", "" );
        Line = dataFile.readLine();
        Numbers = Line.split("=", Qt::SkipEmptyParts);
        _vidaMorphoFile = Numbers.at(1);
         _vidaMorphoFile = _vidaMorphoFile.replace( "\n", "" );
        // ### Solver

        // ### END
        dataFile.close();

        return success;
}

QList<double> MGInputParameters::computeLogNormDistribution( double cmd, double gsd, double nbPart )
{
        QList<double> particles;

        particles.clear();

        return particles;
}

QString MGInputParameters::getMorphoFileName()
{
        return _morphoFile;
}

double MGInputParameters::getLaryngealCoeff()
{
        return _laryngealCoeff;
}

double MGInputParameters::getGasTemperature()
{
        return _tGas;
}

double MGInputParameters::getGasDensity()
{
        return _dGas;
}

double MGInputParameters::getGasViscosity()
{
        return _vGas;
}

double MGInputParameters::getGasMFP()
{
        return _mfpGas;
}

QList<double> MGInputParameters::getParticleDiameters()
{
        return _pDiam;
}

double MGInputParameters::getParticleDiameter( int particleIndex )
{
        return _pDiam.at( particleIndex );
}

double MGInputParameters::getParticleDensity()
{
        return _pDens;
}

int MGInputParameters::getNbParticles()
{
        return _nbDiam;
}

double MGInputParameters::getCMD()
{
        return _CMD;
}

double MGInputParameters::getGSD()
{
        return _GSD;
}

double MGInputParameters::getTidalVolume()
{
        return _tVol;
}

double MGInputParameters::getInhalationDuration()
{
        return _tInh;
}

double MGInputParameters::getPauseDuration()
{
        return _tPau;
}

double MGInputParameters::getExhalationDuration()
{
        return _tExh;
}

bool MGInputParameters::getSuccess()
{
    // test du succes de la lecture du fichier
        return _success;
}
int MGInputParameters::getINbGenT()
{
        return _iNbGenT;
}
int MGInputParameters::getENbGenT()
{
        return _eNbGenT;
}
int MGInputParameters::getINbGenPlug()
{
        return _iNbGenPlug;
}
int MGInputParameters::getENbGenPlug()
{
        return _eNbGenPlug;
}

double MGInputParameters::getFRC()
{
        return _FRC;
}

double MGInputParameters::getHeight()
{
        return _height;
}

int MGInputParameters::getAlveolarModel()
{
    return _alveolarModel;
}

int MGInputParameters::getMouthType()
{
    return _mouthType;
}

double MGInputParameters::getLungCompliance()
{
    return _lungCompliance;
}

double MGInputParameters::getVrv()
{
    return _Vrv;
}

double MGInputParameters::getVfrc()
{
    return _Vfrc;
}

double MGInputParameters::getVtlc()
{
    return _Vtlc;
}

double MGInputParameters::getVfrc0g()
{
    return _Vfrc0g;
}

int MGInputParameters::getGravity()
{
    return _gravity;
}

int MGInputParameters::getTerminalGeneration()
{
    return _nbGeneration;
}

QString MGInputParameters::getPosition()
{
    return _position;
}

QString MGInputParameters::getModelBuildTree()
{
    return _modelBuildTree;
}

QString MGInputParameters::getMotherID()
{
    return _motherID;
}

QString MGInputParameters::getBranchDisease()
{
    return _branchDisease;
}


double MGInputParameters::getDiseaseRatio()
{
    return _diseaseRatio;
}

QString MGInputParameters::getLobeEmphysema()
{
    return _lobeEmphysema;
}

QString MGInputParameters::getLobeAsthma()
{
    return _lobeAsthma;
}

double MGInputParameters::getEmphysemaRatio()
{
    return _emphysemaRatio;
}

double MGInputParameters::getGenerationStenosis()
{
    return _generationStenosis;
}

QString MGInputParameters::getResistanceMode()
{
    return _resistanceMode;
}

QString MGInputParameters::getFileInhaleKatz()
{
    return _fileInhaleKatz;
}

QString MGInputParameters::getFileExhaleKatz()
{
    return _fileExhaleKatz;
}

QString MGInputParameters::getFilePleural()
{
    return _filePleural;
}

QString MGInputParameters::getFileNameFlux()
{
    return _fileNameFlux;
}

QString MGInputParameters::getFileNameVolume()
{
    return _fileNameVolume;
}

QString MGInputParameters::getFileNameFluxLOBE()
{
    return _fileNameFluxLOBE;
}

QString MGInputParameters::getFileNameVolumeLOBE()
{
    return _fileNameVolumeLOBE;
}

QString MGInputParameters::getFileNamePressure()
{
    return _fileNamePressure;
}

QString MGInputParameters::getVidaMorphoFile()
{
    return _vidaMorphoFile;
}
QString MGInputParameters::getDicoFile()
{
    return _dicoFile;
}

}

#endif
