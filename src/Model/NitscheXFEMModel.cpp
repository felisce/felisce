//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. A. Fernandez & F.M. Gerosa
//

/*!
 \file  linearProblemNitscheXFEM.cpp
 \authors M. Fernandez & F.M. Gerosa
 \date  04/2018
 \brief Model class for Navier-Stokes with Nitsche XFEM formulation
 */

// System includes

// External includes

// Project includes
#include "FiniteElement/elementVector.hpp"
#include "Model/NitscheXFEMModel.hpp"

namespace felisce 
{

NitscheXFEMModel::NitscheXFEMModel():Model() 
{
  m_name = "Nitsche XFEM Navier-Stokes Model";
  m_isParFluidVelAllocated = false;
  m_pLinFluid = nullptr;
  m_feStruc = nullptr;
  m_indexTime = 0;
  m_countToInitOldObjects = 0;
  m_reinitializeFluidBdf = false;
  m_intfDisp = nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

NitscheXFEMModel::~NitscheXFEMModel() 
{
  
  if( m_isParFluidVelAllocated ) {
    for(std::size_t i = 0; i < m_parFluidVel.size(); ++i)
      m_parFluidVel[i].destroy();
    m_parFluidVel.clear();
  }

  if( m_feStruc != NULL )
    delete m_feStruc;
} 

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::initializeDerivedLinearProblem() 
{
  // type conversion of the linear problems
  m_pLinFluid = dynamic_cast<LinearProblemNitscheXFEM*>(m_linearProblem[0]);
  
  std::cout << " Reading interface mesh... " ;
  std::string inputDirectory1 = "./";
  std::string inputFile1 = "";
  std::string inputMesh1 = FelisceParam::instance().meshFileImmersedStruct;
  std::string outputMesh1 = "toto.geo"; 
  std::string meshDir1 = FelisceParam::instance().meshDirImmersedStruct;
  std::string resultDir1 = "";
  std::string prefixName1 = "";
    
  IO io(inputDirectory1, inputFile1, inputMesh1, outputMesh1, meshDir1, resultDir1, prefixName1);
    
  io.readMesh(m_interfMesh, 1.0);
  std::cout << " done. " << std::endl; // TODO D.C. the model can already have more than one mesh...

  m_pLinFluid->setInterfaceMesh(&m_interfMesh);
  
  if( !FelisceParam::instance().duplicateSupportDof )
    FEL_ERROR("NXFEM model requires duplicateSupportDof = true");
    
  // build the edges or faces of the fluid mesh
  if( m_mesh[0]->domainDim() == 2 )
    m_mesh[0]->buildEdges();
  else 
    m_mesh[0]->buildFaces();
  
  // We intersect the two meshes here (because this function is called before computeDof)
  // The meshes can be found in the linear problem
  m_intersectMeshes.setMeshesName("fluidMesh", "strucMesh");
  m_intersectMeshes.setVerbose(FelisceParam::verbose());
  m_intersectMeshes.initAndIntersectMeshes(*m_mesh[0], m_interfMesh);
  
  // give access to the intersection to each linear problems
  m_pLinFluid->setDuplicateSupportObject(&m_intersectMeshes);

  // compute normals
  m_interfMesh.computeElementNormal(m_interfMesh.listElementNormals());
}

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::initializeDerivedModel() 
{
  
  if (FelisceParam::instance().orderBdfNS > 2)
    FEL_ERROR("BDF not yet implemented for order greater than 2 with Nitsche-XFEM.");

  // Definition and initialization of bdf schemes
  m_bdfFluid.defineOrder(FelisceParam::instance().orderBdfNS);
  m_pLinFluid->initializeTimeScheme(&m_bdfFluid);

  // Finite element for the linear problems
  // We assume that there is only one type of volume element for the structure
  const std::vector<GeometricMeshRegion::ElementType>& bagElementTypeDomainStruc = m_interfMesh.bagElementTypeDomain();
  FEL_ASSERT(bagElementTypeDomainStruc.size() == 1);

  const int typeOfFiniteElement = 0;
  const GeoElement *geoEleStruc = GeometricMeshRegion::eltEnumToFelNameGeoEle[bagElementTypeDomainStruc[0]].second;
  const RefElement *refEleStruc = geoEleStruc->defineFiniteEle(bagElementTypeDomainStruc[0], typeOfFiniteElement, m_interfMesh);
  m_feStruc = new CurvilinearFiniteElement(*refEleStruc, *geoEleStruc, DegreeOfExactness_2); // WARNING: Provide the value with the fluid or by data file 

  FEL_ASSERT(m_feStruc);
  m_pLinFluid->setStrucFiniteElement(m_feStruc); // TODO D.C. remove this part once merged multimesh in master
  
  // We assume that there is only one type of volume element for the fluid
  FEL_ASSERT(m_pLinFluid->meshLocal()->bagElementTypeDomain().size() == 1);
}

/***********************************************************************************/
/***********************************************************************************/
  
void NitscheXFEMModel::preAssemblingMatrixRHS(std::size_t /*iProblem*/) 
{

  // Allocate the extrapolation of the velocity if necessary
  if( !m_isParFluidVelAllocated ) {
    // There are orderBdfNS term in the velocity extrapolation
    m_parFluidVel.resize(FelisceParam::instance().orderBdfNS);
    
    // If we use a projection of the old solution, all the terms have the same size
    if( FelisceParam::instance().useProjectionForNitscheXFEM || m_fstransient->iteration == 0 ) {
      for(std::size_t i = 0; i < m_parFluidVel.size(); ++i)
        m_parFluidVel[i].duplicateFrom(m_pLinFluid->vector());
    } else {
      for(std::size_t i = 0; i < m_parFluidVel.size(); ++i)
        m_parFluidVel[i].duplicateFrom(m_pLinFluid->solutionOld(i));
    }
    
    // set the extrapolation term to zero
    for(std::size_t i = 0; i < m_parFluidVel.size(); ++i)
      m_parFluidVel[i].set( 0.);
    
    m_isParFluidVelAllocated = true;
  }  
}

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::forward() 
{
  prepareForward();
  solve();
}

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::prepareForward() 
{

  // Write solution for postprocessing (if required)
  if ( m_fstransient->iteration > 0 ){
    writeSolutionAndMeshes();
    if( FelisceParam::verbose() > 4 )
      m_pLinFluid->printStructStress(m_fstransient->iteration, m_fstransient->timeStep);
  }

  // update the position of the interface and recompute the intersection
  if ( ( FelisceParam::instance().useDynamicStructure ) and (m_fstransient->iteration > 0)  ) {
    FEL_CHECK(m_intfDisp != 0, "Error: the pointer of the interface force has not been initialized yet.");

    m_intersectMeshes.updateInterfacePosition( *m_intfDisp );
    
    m_interfMesh.moveMesh( *m_intfDisp, 1.0);
    m_interfMesh.computeElementNormal(m_interfMesh.listElementNormals());

    m_intersectMeshes.setIteration(m_fstransient->iteration);
    m_intersectMeshes.intersectMeshes(*m_mesh[0],  m_interfMesh);
  }

  // save the solution with the original support Dof and delete the matrices, vectors and mapping of the fluid
  if ( m_fstransient->iteration > 0 ) {

    if( m_intersectMeshes.hasIntersectionChange() ) {
      
      // delete dynamic parts
      m_pLinFluid->updateOldSolution(1);
      m_pLinFluid->deleteDynamicData();
    
      if(m_isParFluidVelAllocated) {
        for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
          m_parFluidVel[i].destroy();

        m_isParFluidVelAllocated = false;
      }

      // set to true only to remember that the intersection has changed after this time step
      // If the intersection is not changing at the next time step, we need to reinitialize the old objects
      m_countToInitOldObjects = FelisceParam::instance().orderBdfNS;
    } else {

      m_pLinFluid->updateOldSolution(m_countToInitOldObjects);

      if(m_countToInitOldObjects > 0) {
        
        if(m_isParFluidVelAllocated) {
          for(std::size_t i=0; i<m_parFluidVel.size(); ++i)
            m_parFluidVel[i].destroy();
          
          m_isParFluidVelAllocated = false;
        }

        // pre assembling to reallocate the extrapolations
        for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++) {
          preAssemblingMatrixRHS(ipb);
        }
        
        m_countToInitOldObjects -= 1;
        m_reinitializeFluidBdf = true;
      }
    }
  }
  
  // Note that hasIntersectionChange() is true only if the duplicated elements or the signs of the original fluid vertices (for instance, when the structure intersects a new edge or face) are different
  if( m_intersectMeshes.hasIntersectionChange() ) {
    // reallocate everything for the fluid
    // supportDofMeshes, duplication, and dofs
    m_pLinFluid->computeDof(MpiInfo::numProc(), MpiInfo::rankProc());

    // new pattern
    m_pLinFluid->userChangePattern(MpiInfo::numProc(), MpiInfo::rankProc());

    // cut the mesh
    m_pLinFluid->cutMesh();

    // allocate matrices and vectors
    m_pLinFluid->allocateMatrix();
      
    // determine dof for the boundary conditions
    m_pLinFluid->determineDofAssociateToLabel();

    m_pLinFluid->finalizeEssBCConstantInT();

    // pre assembling to reallocate the extrapolations
    for (std::size_t ipb = 0; ipb < m_linearProblem.size(); ipb++)
      preAssemblingMatrixRHS(ipb);
    
    // re build the solver (not possible to change the matrix size in the ksp without destroying it ?)
    m_pLinFluid->buildSolver();
  }

  if( m_fstransient->iteration == 0 ) {
    // initialize the old objects
    m_pLinFluid->initOldObjects();

    writeSolutionAndMeshes();
    if( FelisceParam::verbose() > 4)
      m_pLinFluid->printStructStress(m_fstransient->iteration, m_fstransient->timeStep);
  }

  // if ( MpiInfo::rankProc() == 0 ){ 
  //   // m_ios[ipb]->writeMesh(m_mesh[0]);
  //   m_ios[0]->ensight().writerGeoGold(m_mesh[0], FelisceParam::instance().resultDir+"fluid.geo");
  //   m_pLinFluid->printMeshPartition(m_fstransient->iteration, m_fstransient->timeStep);
  // }
  
  // Advance time step.
  updateTime();

  // Print time information
  printNewTimeIterationBanner();

  ///////////////////////
  // Update Bdf scheme //
  ///////////////////////
  if( m_intersectMeshes.hasIntersectionChange() || m_reinitializeFluidBdf || m_fstransient->iteration == 1 ) {
    // initialize the bdf for the fluid
    PetscVector zero = PetscVector::null();
    felInt order =  FelisceParam::instance().orderBdfNS;
    if( order == 1 )
      m_bdfFluid.reinitialize(FelisceParam::instance().orderBdfNS, m_pLinFluid->solutionOld(0), zero, zero);
    else if( order == 2 )
      m_bdfFluid.reinitialize(FelisceParam::instance().orderBdfNS, m_pLinFluid->solutionOld(1), m_pLinFluid->solutionOld(0), zero);
    else
      FEL_ERROR("This bdf order is not implemented for this problem");

    // Compute the extrapolation of the velocity and initialize the one in the solver
    m_bdfFluid.extrapolateByPart(m_parFluidVel);
    m_pLinFluid->gatherSeqVelExtrapol(m_parFluidVel);
      
    // Compute the rhs coming from the time scheme and initialize the one in the solver
    m_bdfFluid.computeRHSTimeByPart(m_fstransient->timeStep, m_parFluidVel);
    m_pLinFluid->gatherSeqBdfRHS(m_parFluidVel);

    // set flag to false
    m_reinitializeFluidBdf = false;
  } else {
    m_bdfFluid.update(m_pLinFluid->solution());

    m_bdfFluid.extrapolateByPart(m_parFluidVel);
    m_pLinFluid->gatherSeqVelExtrapol(m_parFluidVel);

    m_bdfFluid.computeRHSTimeByPart(m_fstransient->timeStep, m_parFluidVel);
    m_pLinFluid->gatherSeqBdfRHS(m_parFluidVel);
  }

  // clear the matrices at each sub iteration and computation of the matrix and rhs
  m_pLinFluid->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs);
  m_pLinFluid->vector(1).zeroEntries();

  if( FelisceParam::instance().confIntersection ){

    m_pLinFluid->assembleMatrixFrontPoints();
  } else {
    m_pLinFluid->assembleMatrixTip();
    m_pLinFluid->assembleMatrixTipFaces(); 
  }

  if(FelisceParam::instance().useGhostPenalty)
    m_pLinFluid->assembleMatrixGhostPenalty();

  if(FelisceParam::instance().NSStabType == 1)
    m_pLinFluid->assembleMatrixFaceOriented();

  if( FelisceParam::instance().contactDarcy > 0 ) 
    m_pLinFluid->assembleMatrixBCDarcy();
    
  // Main assembly loop and assembly of the matrix
  // The pattern is reduced to where values have been set till here, that is why this loop is called in last
  m_pLinFluid->assembleMatrixRHS(MpiInfo::rankProc());

  m_pLinFluid->addMatrixRHS();
}

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::solve() 
{
  
  // Apply boundary conditions.
  m_pLinFluid->finalizeEssBCTransient();
  m_pLinFluid->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

  // Solve the linear system.
  PetscPrintf(PETSC_COMM_WORLD, "*** Fluid solver ***\n");
  m_pLinFluid->solve(MpiInfo::rankProc(), MpiInfo::numProc());

  // Gather the solution to use it in the solid problem
  m_pLinFluid->gatherSolution();

  // evaluation stresses
  if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Evaluating stress \n");
  
  m_pLinFluid->computeInterfaceStress();
}

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::solveLinearized(bool linearFlag) 
{
  
  m_pLinFluid->printSkipVolume(false);
  
  m_linearProblem[0]->linearizedFlag() = linearFlag;
  
  m_linearProblem[0]->vector().zeroEntries();
  m_pLinFluid->vector(1).zeroEntries();
  
  m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::only_rhs);

  if ( !linearFlag )
    m_linearProblem[0]->addMatrixRHS();   

  if ( linearFlag && FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"NS linearized solver \n");
  else if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"NS solver \n");

  if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");
  
  //Apply essential transient boundary conditions.
  m_linearProblem[0]->finalizeEssBCTransient();

  if ( linearFlag ) // only Dirichlet BC is applied (RHS)
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, 
                                MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs,
                                0, true, ApplyNaturalBoundaryConditions::no);
  else  // all BC applied (RHS): 
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, 
                                MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs,
                                0, true, ApplyNaturalBoundaryConditions::yes);
  
  if ( linearFlag && FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Solving linearized NS system (same matrix) \n");
  else if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Solving NS system (same matrix) \n");
  
  m_linearProblem[0]->solveWithSameMatrix();

  m_linearProblem[0]->gatherSolution();

  if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Evaluating stress \n");

  m_pLinFluid->computeInterfaceStress();

  m_pLinFluid->printSkipVolume(true);
}

/***********************************************************************************/
/***********************************************************************************/

void NitscheXFEMModel::writeSolutionAndMeshes() 
{

  if( (m_fstransient->iteration % FelisceParam::instance().frequencyWriteSolution == 0) ) {

    // write the mesh
    if(FelisceParam::verbose()>1)
      PetscPrintf(MpiInfo::petscComm(),"Write meshes\n");

    std::map<felInt, std::vector<felInt> > refToListOfIds;
    std::string fileName;
    std::string indexFile;

    std::size_t ipb=0;  
    for(std::size_t iUnknown=0; iUnknown<m_linearProblem[ipb]->listUnknown().size(); ++iUnknown) {
      std::stringstream oss;
      oss << m_indexTime;
      if(m_indexTime < 10)
        indexFile = "0000" + oss.str();
      else if(m_indexTime < 100)
        indexFile = "000" + oss.str();
      else if(m_indexTime < 1000)
        indexFile = "00" + oss.str();
      else if(m_indexTime < 10000)
        indexFile = "0" + oss.str();
      else
        indexFile = oss.str();

      fileName = FelisceParam::instance().resultDir+FelisceParam::instance().outputMesh[0]+"."+indexFile+".geo";

      // fluid, write mesh from the support dof to see the duplication
      if(iUnknown == 0) {
        // write the mesh only once (with the velocity, the pressure mesh is the same)
        if(FelisceParam::instance().outputFileFormat == 0){
          m_linearProblem[ipb]->writeGeoForUnknown(iUnknown, fileName);
        }
        else if(FelisceParam::instance().outputFileFormat == 1) {
          m_linearProblem[ipb]->writeGeoForUnknownEnsightGold(iUnknown, refToListOfIds, fileName);
          m_ios[0]->ensight().setRefToListOfIds(refToListOfIds);
        }
        else
          FEL_ERROR("Unknown output file format");
      }
    }
    m_meshIsWritten = true;
    ++m_indexTime;
    
    // write the solutions
    if(FelisceParam::verbose() > 1)
      PetscPrintf(MpiInfo::petscComm(),"Write solutions\n");

    m_linearProblem[ipb]->writeSolution(MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration);

    if (MpiInfo::rankProc() == 0) 
      m_pLinFluid->writeDuplication(MpiInfo::rankProc(), *m_ios[0], m_fstransient->time, m_fstransient->iteration);

    if (MpiInfo::rankProc() == 0) {
      for(std::size_t iio=0; iio<m_ios.size(); ++iio) {
        if(m_ios[iio]->typeOutput == 1 ) // 1 : ensight
          m_ios[iio]->postProcess(m_fstransient->time, 2);
      }
    }
  }

  if(FelisceParam::verbose()>1)
    PetscPrintf(MpiInfo::petscComm(),"End Write meshes\n");
}

}
