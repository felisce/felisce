//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/NSFracStepModel.hpp"
#include "Solver/linearProblemHarmonicExtension.hpp"
#include "Solver/linearProblemNSFracStepAdvDiff.hpp"
#include "Solver/linearProblemNSFracStepProj.hpp"

namespace felisce {
  NSFracStepModel::NSFracStepModel():Model() {
    m_lumpedModelBC = nullptr;
    m_name = "NSFracStep";
    m_matrixWithoutBCcreated = false;
  }
  
  
  NSFracStepModel::~NSFracStepModel() {
    if(m_lumpedModelBC)
      delete m_lumpedModelBC;
    m_solExtrapolate.destroy();
  }
  
  //***************************** Initialization ************************************
  
  void NSFracStepModel::initializeDerivedModel() {

    if( FelisceParam::instance().lumpedModelBCLabel.size() ) {
      m_lumpedModelBC = new LumpedModelBC(m_fstransient);
      static_cast<LinearProblemNSFracStepProj*>(m_linearProblem[1])->initializeLumpedModelBC(m_lumpedModelBC);
    }
    
    if(FelisceParam::instance().useALEformulation ) {
      // Resizing the auxiliary vectors needed in updateMesh()
      m_numDofExtensionProblem = m_linearProblem[2]->numDof();
      m_dispMeshVector.resize( m_linearProblem[2]->supportDofUnknown(0).listNode().size() * m_linearProblem[2]->meshLocal()->numCoor() );
      m_petscToGlobal.resize(m_numDofExtensionProblem);
      if ( FelisceParam::instance().readPreStressedDisplFromFile )
        m_preStressedDispMeshVector.resize(m_numDofExtensionProblem);
      // From PETSc order to global
      for (int ipg=0; ipg< m_numDofExtensionProblem; ipg++)
        m_petscToGlobal[ipg] = ipg;
      
      AOApplicationToPetsc(m_linearProblem[2]->ao(),m_numDofExtensionProblem,m_petscToGlobal.data());
      static_cast<LinearProblemNSFracStepAdvDiff*>(m_linearProblem[0])->pressureFWI() = 0;
      
      if ( FelisceParam::instance().readDisplFromFile ||  FelisceParam::instance().readPreStressedDisplFromFile ) {
        // defined in linearProblemHarmonicExtension
        LinearProblemHarmonicExtension* lpHarmExt = static_cast<LinearProblemHarmonicExtension*>(m_linearProblem[2]);
        lpHarmExt->readMatchFile_ALE(m_linearProblem[2]->listVariable(), FelisceParam::instance().MoveMeshMatchFile);
        lpHarmExt->identifyIdDofToMatch_ALE(m_linearProblem[2]->dof());
        
        if (FelisceParam::instance().hasInitialCondition ||  FelisceParam::instance().readPreStressedDisplFromFile ) {
          PetscPrintf(MpiInfo::petscComm(),"\nCycle 1, Read data displacement at time t = 0.00000\n");
          lpHarmExt->readDataDisplacement(m_ios, 0.0);
        }
      }
    }
    
    if (FelisceParam::instance().orderBdfFS > 2)
      FEL_ERROR("BDF not yet implemented for order greater than 2 with fractional step model.");
    
    m_bdfFS.defineOrder(FelisceParam::instance().orderBdfFS);
    m_linearProblem[0]->initializeTimeScheme(&m_bdfFS);
    
    if ( FelisceParam::instance().fsiCoupling) {
      m_rhsWihoutBC.duplicateFrom(m_linearProblem[0]->vector());
      m_rhsPressWihoutBC.duplicateFrom(m_linearProblem[1]->vector());
      m_viscousResidual.duplicateFrom(m_linearProblem[0]->vector());
      m_residual.duplicateFrom(m_linearProblem[0]->vector());
      m_lumpedMassRN.duplicateFrom(m_linearProblem[0]->vector());
      m_seqResidual.duplicateFrom(m_linearProblem[0]->sequentialSolution());
      m_seqViscousResidual.duplicateFrom(m_linearProblem[0]->sequentialSolution());
      m_seqPressLoad.duplicateFrom(m_linearProblem[0]->sequentialSolution());
    }
    
    if (FelisceParam::instance().orderPressureExtrapolation > 0) {
      m_prevPress.duplicateFrom(m_linearProblem[1]->solution());
      m_prevPress.zeroEntries();
    }
    
  }
  
  
  void NSFracStepModel::setExternalVec() {
    // std::set external vectors for linear problems
    // (to be used in assembling routines)
    if (FelisceParam::instance().explicitAdvection) {
      // this has been moved into NSFracStepExplicitModel
      FEL_ERROR("NSFracStepModel should be used without explicitAdvection");
      
    } else {
      // standard fracstep
      m_linearProblem[0]->pushBackExternalVec(m_linearProblem[1]->sequentialSolution()); // externalVec(0)  in m_linearProblem[0]
      m_linearProblem[1]->pushBackExternalVec(m_linearProblem[0]->sequentialSolution()); // externalVec(0)  in m_linearProblem[1]
      
      m_linearProblem[0]->pushBackExternalAO(m_linearProblem[1]->ao());
      m_linearProblem[1]->pushBackExternalAO(m_linearProblem[0]->ao());
      
      m_linearProblem[0]->pushBackExternalDof(m_linearProblem[1]->dof());
      m_linearProblem[1]->pushBackExternalDof(m_linearProblem[0]->dof());
      
      // Initialization of the std::vector containing the convective velocity
      m_beta.duplicateFrom(m_linearProblem[0]->sequentialSolution());
      m_beta.zeroEntries();
      
      m_linearProblem[1]->pushBackExternalVec(m_beta);  // externalVec(1)  in m_linearProblem[1]
      
      if(FelisceParam::instance().useALEformulation > 0) {
        
        // fsi challenge
        if ( FelisceParam::instance().useElasticExtension )  {
          m_dispMesho_pp.duplicateFrom(m_linearProblem[2]->solution());
          m_dispMesho_pp.zeroEntries();
        }
        
        //1) Initialization of the std::vector containing the displacement of the mesh d^n
        m_dispMesh.duplicateFrom(m_linearProblem[2]->sequentialSolution());
        m_dispMesh.zeroEntries();
        
        //2) Initialization of the std::vector containing the velocity of the mesh w^n
        m_velMesh.duplicateFrom(m_linearProblem[2]->sequentialSolution());
        m_velMesh.zeroEntries();
        
        // Passing the ao and dof
        m_linearProblem[0]->pushBackExternalAO(m_linearProblem[2]->ao());    //m_externalAO[1]  in m_linearProblem[0]
        m_linearProblem[0]->pushBackExternalDof(m_linearProblem[2]->dof());  //m_externalDof[1] in m_linearProblem[0]
        // Passing the velocity of the mesh to the AdvDiff problem
        m_linearProblem[0]->pushBackExternalVec(m_velMesh);                  //_externalVec[1]  in m_linearProblem[0]
        // Resizing the auxiliary vectors needed in updateMesh()
        m_tmpDisp.resize(m_numDofExtensionProblem);
        
        
        
        m_displTimeStep = 0.;
      }
      
    }
    
  }
  
  
  //************************** Pre & Post assembleMatrixRHS *******************************
  
  void NSFracStepModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    
    if (iProblem == 0) {
      m_solExtrapolate.duplicateFrom(m_linearProblem[0]->vector());
      m_solExtrapolate.zeroEntries();
      
      m_linearProblem[0]->solution().zeroEntries();
    }
    
    if(FelisceParam::instance().useALEformulation == 0) {
      //First assembling loop in iteration 0 to build static matrix.
      m_linearProblem[iProblem]->assembleMatrixRHS(MpiInfo::rankProc());
      // save static matrix in matrix _A.
      m_linearProblem[iProblem]->copyMatrixRHS();
    } else {
      if ( ( iProblem == 2 ) && ( !FelisceParam::instance().useElasticExtension ) ) {
        // First assembling loop in iteration 0 to build static matrix of the extension problem
        m_linearProblem[2]->assembleMatrixRHS(MpiInfo::rankProc());
        // Save the static part of the extension problem in the matrix _A
        m_linearProblem[2]->copyMatrixRHS();
      }
    }
  }
  
  void NSFracStepModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    // _Matrix = _A + _Matrix (add static matrix to the dynamic matrix to build
    // complete matrix of the system
    if(FelisceParam::instance().useALEformulation == 0) {
      m_linearProblem[iProblem]->addMatrixRHS();
    } else {
      if (iProblem == 2) {
        m_linearProblem[2]->addMatrixRHS();
      }
    }
  }
  
  
  void NSFracStepModel::solveProjectionStep(bool linearFlag) {
    
    m_linearProblem[1]->linearizedFlag() = linearFlag;
    
    if (  linearFlag )
      m_linearProblem[1]->vector().zeroEntries();
    else
      m_linearProblem[1]->vector().copyFrom(m_rhsPressWihoutBC);
    
    if ( linearFlag && FelisceParam::verbose() )
      PetscPrintf(MpiInfo::petscComm()," -- Projection-step linearized solver \n");
    else if ( FelisceParam::verbose() )
      PetscPrintf(MpiInfo::petscComm()," -- Projection-step solver \n");
    
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");
    
    //Apply essential transient boundary conditions.
    m_linearProblem[1]->finalizeEssBCTransient();
    
    m_linearProblem[1]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs,
                                0, true, ApplyNaturalBoundaryConditions::yes);
    
    PetscPrintf(MpiInfo::petscComm(),"Solving Projection-step (same matrix) \n");
    
    //Solve linear system.
    m_linearProblem[1]->solveWithSameMatrix();
    
    if( (!linearFlag )  && (FelisceParam::instance().orderPressureExtrapolation > 0)  ) // && (m_fstransient->iteration > 1)
      m_linearProblem[1]->solution().axpy(1, m_prevPress);
    
    m_linearProblem[1]->gatherSolution();
    
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Evaluating fluid load \n");
    
    
    // Pressure load calculation in FWI fashion
    static_cast<LinearProblemNSFracStepAdvDiff*>(m_linearProblem[0])->pressureFWI() = 1;
    m_linearProblem[0]->vector().set(0.);
    m_linearProblem[0]->assembleMatrixRHSNaturalBoundaryCondition(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, 0);
    static_cast<LinearProblemNSFracStepAdvDiff*>(m_linearProblem[0])->pressureFWI() = 0;
    
    if ( linearFlag )
      m_residual.copyFrom(m_linearProblem[0]->vector());
    else
      waxpy(m_residual,1., m_linearProblem[0]->vector(),m_viscousResidual);
    
    m_linearProblem[0]->gatherVector(m_residual, m_seqResidual);
    m_linearProblem[0]->linearizedFlag() = false;
    
  }
  
  void NSFracStepModel::forward() {
    
    if (  FelisceParam::instance().useALEformulation && FelisceParam::instance().initALEDispByFile
        && (m_fstransient->iteration == 0  ) )  {
      
      // init diplacement from file
      std::vector<double> vector;
      std::vector<double> vv;
      std::vector<int> ix;
      for (int ilinPb =1; ilinPb<=2; ++ilinPb)
      {
        int numDof;
        if( FelisceParam::instance().FusionDof && (ilinPb == 2) )
          numDof = m_linearProblem[ilinPb]->supportDofUnknown(0).listNode().size() * m_linearProblem[ilinPb]->meshLocal()->numCoor();
        else if ( FelisceParam::instance().FusionDof && (ilinPb == 1) )
          numDof = m_linearProblem[ilinPb]->supportDofUnknown(0).listNode().size();
        else
          numDof = m_linearProblem[ilinPb]->numDof();
        
        vector.resize( numDof, 0.);
        
        m_ios[0]->readVariable(ilinPb, 0.0, vector.data(), numDof);
        
        if( FelisceParam::instance().FusionDof && (ilinPb == 1) ) {
          int numNodes = m_linearProblem[ilinPb]->supportDofUnknown(0).listNode().size();
          int idof;
          std::vector<double> aux(vector);
          for (int ip = 0; ip < numNodes; ip++) {
            idof = m_linearProblem[1]->supportDofUnknown(0).listPerm()[ip];
            vector[idof] = aux[ip];
          }
        }
        
        if( FelisceParam::instance().FusionDof && (ilinPb == 2) ) {
          int numCompDisp  = m_linearProblem[2]->dimension();
          int numCoorMesh  = m_linearProblem[2]->meshLocal()->numCoor();
          int numNodes = m_linearProblem[ilinPb]->supportDofUnknown(0).listNode().size();
          int idof;
          std::vector<double> aux(vector);
          for (int ip = 0; ip < numNodes ; ip++) {
            idof = m_linearProblem[2]->supportDofUnknown(0).listPerm()[ip];
            for (felInt ic=0; ic < numCompDisp; ++ic)
              vector[idof*numCompDisp+ic] = aux[ip*numCoorMesh+ic];
          }
        }
        
        int low, high;
        VecGetOwnershipRange(m_linearProblem[ilinPb]->solution().toPetsc(),&low,&high);
        
        int localSize = high-low;
        
        ix.resize(localSize);
        int j=0;
        for (int i=low; i<high;++i)
        {
          ix[j] = i;
          ++j;
        }
        vv.resize(localSize);
        
        AOPetscToApplication(m_linearProblem[ilinPb]->ao(),localSize,ix.data());
        for (int i=0; i<localSize;++i)
          vv[i] = vector[ix[i]];
        AOApplicationToPetsc(m_linearProblem[ilinPb]->ao(),localSize,ix.data());
        
        m_linearProblem[ilinPb]->solution().setValues(localSize,ix.data(),vv.data(),INSERT_VALUES);
        m_linearProblem[ilinPb]->solution().assembly();
        m_linearProblem[ilinPb]->gatherSolution();
        
      }
    }
    
    // TO DO: pressure initialization challenge FSI to me removed
    // if (  FelisceParam::instance().fsiCoupling && ( (FelisceParam::instance().testCase == 20) ||| (FelisceParam::instance().testCase == 23) )
    // 	  && FelisceParam::instance().useALEformulation && (m_fstransient->iteration == 0  ) ) {
    //   PetscInt localSize = m_linearProblem[1]->numDofLocalPerUnknown(m_linearProblem[1]->listUnknown()[0]);
    //   std::vector<felInt> idLocal(localSize);
    //   std::vector<felInt> idGlobalP(localSize);
    //   std::vector<felInt> idGlobalA(localSize);
    //   std::vector<double> value(localSize);
    //   for (felInt  i = 0; i < localSize; ++i) {
    // 	idLocal[i] = i;
    // 	value[i]=0.;
    //   }
    //   ISLocalToGlobalMappingApply(m_linearProblem[1]->mappingDofLocalToDofGlobal(m_linearProblem[1]->listUnknown()[0]),localSize,idLocal.data(),idGlobalP.data());
    // 	for (felInt  i = 0; i < localSize; ++i)
    // 	  idGlobalA[i] = idGlobalP[i];
    
    // 	AOPetscToApplication(m_linearProblem[1]->ao(),localSize,idGlobalA.data());
    
    // 	Point pt;
    // 	for (felInt i = 0; i < localSize; ++i) {
    // 	  m_linearProblem[1]->findCoordinateWithIdDof(idGlobalA[i],pt);
    // 	  value[i] =    17827 - FelisceParam::instance().density * 980.665 * (pt.y()+ 2.638);
    // 	}
    // 	m_linearProblem[1]->solution().setValues(localSize,idGlobalP.data(),value.data(),INSERT_VALUES);
    // 	m_linearProblem[1]->solution().assembly();
    // 	m_linearProblem[1]->gatherSolution();
    // }
    
    // Write solution for postprocessing (if required)
    writeSolution();
    
    // Advance time step.
    updateTime();
    // Print time information
    printNewTimeIterationBanner();
    
    if( FelisceParam::instance().orderPressureExtrapolation > 0 )
      m_prevPress.copyFrom(m_linearProblem[1]->solution());
    
    for(std::size_t iLinPb=0; iLinPb<2; iLinPb++) {
      if (FelisceParam::verbose())
        PetscPrintf(MpiInfo::petscComm(), "NSFracStep: linear problem #%ld\n",static_cast<unsigned long>(iLinPb));
      
      // first linear problem
      if ( iLinPb==0 ) {
        if ( m_fstransient->iteration == 1 ) {
          // Initialize of bdf solver.
          m_bdfFS.initialize(m_linearProblem[0]->solution());
          // Compute the extrapolate.
          m_bdfFS.extrapolate(m_solExtrapolate);
          // Initialize it.
          m_linearProblem[0]->initExtrapol(m_solExtrapolate);
          
          if(FelisceParam::instance().useALEformulation)
            m_tau = 1./(m_fstransient->timeStep);
          
          if ( FelisceParam::instance().readPreStressedDisplFromFile )
            computePreStressedDispMeshVector();
          
          
        } else {
          // Update bdf.
          m_bdfFS.update(m_linearProblem[0]->solution());
          // Compute the extrapolate.
          m_bdfFS.extrapolate(m_solExtrapolate);
          // Initialize it.
          m_linearProblem[0]->updateExtrapol(m_solExtrapolate);
        }
        // convective velocity for supg stabilization in projection step
        m_linearProblem[0]->gatherVector(m_solExtrapolate,m_beta);
        
      }
      
      // second problem (projection)
      // lumpedModelBC bc computation
      if (m_lumpedModelBC && iLinPb==1) {
        if ( m_fstransient->iteration == 1 ) {
          // m_labelFluxListLumpedModelBC computation with LumpedModelBC labels
          m_labelFluxListLumpedModelBC.clear();
          for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
            m_labelFluxListLumpedModelBC.push_back(FelisceParam::instance().lumpedModelBCLabel[i]);
          }
        }
        // update the fluxes
        // justine's warning (10-09-12) : with a velocity with divergence not necesary free... ?
        m_linearProblem[0]->computeMeanQuantity(velocity,m_labelFluxListLumpedModelBC,m_fluxLumpedModelBC);
        for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
          m_lumpedModelBC->Q(i) = m_fluxLumpedModelBC[i] ;
        }
        // iterate the lumpedModelBC parameters
        m_lumpedModelBC->iterate();
      }
      
      if (FelisceParam::verbose() and ( iLinPb == 0 ))
        PetscPrintf(MpiInfo::petscComm(),"Linear problems NS: Advection-Diffusion-step \n");
      else if (FelisceParam::verbose())
        PetscPrintf(MpiInfo::petscComm(),"Linear problems NS: Projection-step \n");
      
      // Compute the RHS linked to the time scheme (bdf).
      m_bdfFS.computeRHSTime(m_fstransient->timeStep);
      // Fill _seqBdfRHS.
      m_linearProblem[iLinPb]->gatherVectorBeforeAssembleMatrixRHS();
      
      if( iLinPb==0 && FelisceParam::instance().useALEformulation > 0) {
        computeALEterms();
      } else {
        //Embedded interface in the projection step
        if( iLinPb==1 && FelisceParam::instance().EmbeddedInterface){
          if ( FelisceParam::instance().fsiCoupling )
            m_linearProblem[iLinPb]->meshLocal()->moveMesh(m_dispMeshVector,0.0);
          
          // Enbedded interface crossed terms are calcualted in the reference configuration
          static_cast<LinearProblemNSFracStepProj*>(m_linearProblem[1])->userAssembleMatrixRHSEmbeddedInterface();
          
          if ( FelisceParam::instance().fsiCoupling )
            m_linearProblem[iLinPb]->meshLocal()->moveMesh(m_dispMeshVector,1.0);
        }
        
        //Assembly loop on elements.
        m_linearProblem[iLinPb]->assembleMatrixRHS(MpiInfo::rankProc());
        
        if (  FelisceParam::instance().fsiCoupling ) {
          if ( iLinPb != 1 ) {
            std::cout << "BUG !!!! "<<std::endl;
            exit(1);
          }
          m_rhsPressWihoutBC.copyFrom(m_linearProblem[iLinPb]->vector());
        }
      }
      
      //Specific operations before solving the system.
      postAssemblingMatrixRHS(iLinPb);
      
      if ( FelisceParam::instance().fsiCoupling )
        m_linearProblem[iLinPb]->meshLocal()->moveMesh(m_dispMeshVector,0.0);
      if ( FelisceParam::instance().readPreStressedDisplFromFile )
        m_linearProblem[iLinPb]->meshLocal()->moveMesh(m_preStressedDispMeshVector,1.0);
      
      if (FelisceParam::verbose())
        PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");
      
      //Apply essential transient boundary conditions.
      m_linearProblem[iLinPb]->finalizeEssBCTransient();
      
      // Apply all the boundary conditions.
      m_linearProblem[iLinPb]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
      
      if ( FelisceParam::instance().readPreStressedDisplFromFile )
        m_linearProblem[iLinPb]->meshLocal()->moveMesh(m_preStressedDispMeshVector,-1.0);
      if ( FelisceParam::instance().fsiCoupling )
        m_linearProblem[iLinPb]->meshLocal()->moveMesh(m_dispMeshVector,1.0);
      
      //Solve linear system.
      m_linearProblem[iLinPb]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
      
      //Compute pressure if incremental scheme is used (note that we solve for the increment \delta p )
      if ( ( iLinPb == 1 ) && (FelisceParam::instance().orderPressureExtrapolation > 0)  )  { // && (m_fstransient->iteration > 1)
        // Correction of the pressure: p^n = p^{n-1} + \delta p^n
        m_linearProblem[1]->solution().axpy(1, m_prevPress);
      }
      
      //Gather solution
      m_linearProblem[iLinPb]->gatherSolution();
      
    }
    
    if ( FelisceParam::instance().fsiCoupling )
      computeFluidLoad();
  }
  
  int NSFracStepModel::getNstate() const {
    return m_linearProblem[0]->numDof();
  }
  
  void NSFracStepModel::getState(double* & state) {
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
  
  felInt NSFracStepModel::numDof_swig() {
    return m_linearProblem[0]->numDof();
  }
  
  void NSFracStepModel::getState_swig(double* data, felInt numDof) {
    double * state;
    m_linearProblem[0]->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
    for (felInt i=0 ;  i<numDof ; i++) {
      data[i] = state[i];
    }
  }
  
  void NSFracStepModel::setState(double* & state) {
    m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  }
  
  
  // *********************** ALE  ***********************
  void NSFracStepModel::computeALEterms() {
    // Assembly the RHS (flagMatrixRHS = FlagMatrixRHS::only_rhs) in the previous mesh configuration for the AdvDiff problem:
    
    if ( m_fstransient->iteration>1 ) {
      m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);
      if (  FelisceParam::instance().fsiCoupling )
        m_rhsWihoutBC.copyFrom(m_linearProblem[0]->vector());
    }
    
    // Solve the extension problem and move the mesh of _linearProblem[0] and _linearProblem[1]
    // according to the mesh displacement obtained
    if( FelisceParam::instance().updateMeshByVelocity ) {
      // the HarmonicExtention unknown is velocity
      // dispalcement is computed analitycally
      UpdateMeshByVelocity();
    } else {
      // the HarmonicExtention unknown is displacement
      // velocity is computed analitycally
      UpdateMeshByDisplacement();
    }
    
    if ( m_fstransient->iteration == 1 ) {
      m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);
      if (  FelisceParam::instance().fsiCoupling )
        m_rhsWihoutBC.copyFrom(m_linearProblem[0]->vector());
    }
    
    // m_beta = u^{n-1}-w^n
    m_beta.axpy(1.,m_velMesh);
    
    // Assembly the Matrix (flagMatrixRHS = FlagMatrixRHS::only_matrix) in the current mesh configuration
    // for the AdvDiff problem
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_matrix);
    
    if ( FelisceParam::instance().fsiCoupling )  {
      
      // Saturate the matrix to impose cero Drichlet conditions on \pd \Sigma
      if ( m_hasLumpedMassRN  && ( m_fstransient->iteration == 1 ) && (FelisceParam::instance().fsiRNscheme != -1) ) {
        PetscScalar dti = 1./m_fstransient->timeStep;
        m_lumpedMassRN.scale(dti);
      }
      if ( FelisceParam::instance().fsiRNscheme != -1)
        m_linearProblem[0]->matrix().diagonalSet(m_lumpedMassRN,ADD_VALUES);
      
      if (m_matrixWithoutBCcreated == 0) {
        m_matrixWithoutBC.duplicateFrom(m_linearProblem[0]->matrix(),MAT_COPY_VALUES);
        m_matrixWithoutBCcreated = true;
      }
      m_matrixWithoutBC.copyFrom(m_linearProblem[0]->matrix(), SAME_NONZERO_PATTERN);
      
    }
  }
  
  
  void NSFracStepModel::UpdateMeshByVelocity() {
    
    if(m_fstransient->iteration>1) {
      m_solDispl.copyFrom(m_linearProblem[2]->solution());
    }
    
    felInt displIterMax = FelisceParam::instance().timeMaxCaseFile; //  to do: it should be taken automatically form displacement  file.case
    
    if(m_fstransient->iteration == m_cycl * displIterMax) { // to read the file case in cycle
      m_displTimeStep = 0.;
      m_cycl++;
    }
    PetscPrintf(MpiInfo::petscComm(),"Cycle %d, Read data at time t = %f\n", m_cycl, m_displTimeStep);
    static_cast<LinearProblemHarmonicExtension*>(m_linearProblem[2])->readDataDisplacement(m_ios, m_displTimeStep); // defined in linearProblemHarmonicExtention
    
    //Solve the extension problem
    solveHarmExt();
    
    // Store velocity and apply it to the NS problem. seqSolution is the velocity
    m_linearProblem[2]->gatherSolution();
    m_velMesh.copyFrom(m_linearProblem[2]->sequentialSolution());
    m_velMesh.scale(-1.);
    //Compute displacement: d^n = w^n*dt + d^{n-1}
    //VecAXPBY(Vec y,PetscScalar a,PetscScalar b,Vec x);  -> y=a∗x+b∗y
    // solution() == w^n
    //_solDispl == d^{n-1}
    m_linearProblem[2]->solution().axpby(1, FelisceParam::instance().timeStep, m_solDispl);
    m_linearProblem[2]->gatherSolution();
    // seqSolution is now the displacement
    
    updateMesh();
  }
  
  void NSFracStepModel::UpdateMeshByDisplacement() {
    
    // fsi challenge
    if ( FelisceParam::instance().useElasticExtension )
      m_dispMesho_pp.copyFrom( m_linearProblem[2]->solution() );
    
    // m_dispMesh = 0 at m_fstransient->iteration = 1
    m_velMesh.copyFrom(m_dispMesh);
    //Solve the extension problem
    solveHarmExt();
    //Gather solution
    m_linearProblem[2]->gatherSolution();
    //Update mesh
    updateMesh();
    
    // Compute velocity and apply it to the NS problem. seqSolution is the displacement
    // VecAXPBY(Vec y,PetscScalar a,PetscScalar b,Vec x);  -> y=a∗x+b∗y
    // now m_velMesh == d^{n-1}
    // and m_dispMesh == d^{n}
    if ( m_fstransient->iteration > 1 )
      m_velMesh.axpby(-m_tau,m_tau,m_dispMesh);
    // Result: m_velMesh ==  -w^{n}  =  \dfrac{1}{\dt} (-d^{n} + d^{n-1})
    //double norm;
    //VecNorm(m_velMesh.toPetsc(), NORM_INFINITY ,&norm);
    //std::cout << std::endl << " Norm vel mesh = " << norm << std::endl <<std::endl;
    
  }
  
  
  void NSFracStepModel::solveHarmExt() {
    //Solve the extension problem
    //Assemble matrix and RHS
    m_linearProblem[2]->assembleMatrixRHS(MpiInfo::rankProc());
    //Specific operations before solve the system
    postAssemblingMatrixRHS(2);
    //Apply essential transient boundary conditions
    m_linearProblem[2]->finalizeEssBCTransient();
    m_linearProblem[2]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
    //Solve linear system
    m_linearProblem[2]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    
    // fsi challenge (update displacement with increment)
    if ( FelisceParam::instance().useElasticExtension )
      m_linearProblem[2]->solution().axpy(1.,m_dispMesho_pp);
    
  }
  
  void NSFracStepModel::updateMesh() {
    
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Update mesh\n");
    
    m_dispMesh.copyFrom(m_linearProblem[2]->sequentialSolution());
    // _dispMesh is the solution obtained with solveHarmExt()
    // Extract the values of _dispMesh and put them in the std::vector _dispMeshVector
    // according to the application ordering of the nodes
    m_dispMesh.getValues(m_numDofExtensionProblem, m_petscToGlobal.data(), m_tmpDisp.data());
    
    felInt numNodes = m_linearProblem[2]->supportDofUnknown(0).listNode().size();
    felInt idof;
    felInt numCompDisp = m_linearProblem[2]->dimension();
    (void)numCompDisp;
    felInt numCoorMesh = m_linearProblem[2]->meshLocal()->numCoor();
    
    if(FelisceParam::instance().FusionDof) {
      for (felInt ip = 0; ip < numNodes; ip++) {
        idof = m_linearProblem[1]->supportDofUnknown(0).listPerm()[ip];
        if ( numCompDisp < numCoorMesh ) {
          m_dispMeshVector[ip*3]   = m_tmpDisp[idof*2];
          m_dispMeshVector[ip*3+1] = m_tmpDisp[idof*2+1];
          m_dispMeshVector[ip*3+2] = 0.;
        }
        else if ( numCompDisp == numCoorMesh )
          for (felInt ic=0; ic < numCoorMesh; ++ic)
            m_dispMeshVector[ip*numCoorMesh+ic] = m_tmpDisp[idof*numCoorMesh+ic];
      }
    }
    else
      m_dispMeshVector = m_tmpDisp;
    
    //Move the mesh in the AdvDiff and Proj problems
    m_linearProblem[0]->meshLocal()->moveMesh(m_dispMeshVector,1.0);
    m_linearProblem[1]->meshLocal()->moveMesh(m_dispMeshVector,1.0);
    
    if ( FelisceParam::instance().useElasticExtension )
      // extension computed on the deformed mesh (takes into account the size of the deformed elements)
      m_linearProblem[2]->meshLocal()->moveMesh(m_dispMeshVector,1.0);
    
  }
  
  
  // *********************** COMPUTE PRE-STRESSED DISP ***********************
  void NSFracStepModel::computePreStressedDispMeshVector() {
    
    static_cast<LinearProblemHarmonicExtension*>(m_linearProblem[2])->HarmExtSol().getValues(m_numDofExtensionProblem, m_petscToGlobal.data(), m_tmpDisp.data());
    
    felInt numNodes = m_linearProblem[2]->supportDofUnknown(0).listNode().size();
    if(FelisceParam::instance().FusionDof) {
      // if there are e.g. RIS model, the number of dofs are re-numerated.
      m_preStressedDispMeshVector.resize(numNodes*3, 0.);
      for (felInt ip = 0; ip < numNodes; ip++) {
        m_preStressedDispMeshVector[ip*3]   = m_tmpDisp[m_linearProblem[2]->supportDofUnknown(0).listPerm()[ip]*3];
        m_preStressedDispMeshVector[ip*3+1] = m_tmpDisp[m_linearProblem[2]->supportDofUnknown(0).listPerm()[ip]*3+1];
        m_preStressedDispMeshVector[ip*3+2] = m_tmpDisp[m_linearProblem[2]->supportDofUnknown(0).listPerm()[ip]*3+2];
      }
    } else
      m_preStressedDispMeshVector = m_tmpDisp;
  }
  
  // *********************** FSI  ***********************
  void NSFracStepModel::computeFluidLoad() {
    
    // Viscous residual calculation. We calculate -ViscousResidual
    m_matrixWithoutBC.scale(-1.);
    multAdd(m_matrixWithoutBC,m_linearProblem[0]->solution(),m_rhsWihoutBC,m_viscousResidual);
    m_linearProblem[0]->gatherVector(m_viscousResidual, m_seqViscousResidual);
    
    // Pressure load calculation in FWI fashion
    static_cast<LinearProblemNSFracStepAdvDiff*>(m_linearProblem[0])->pressureFWI() = 1;
    m_linearProblem[0]->vector().set(0.);
    m_linearProblem[0]->assembleMatrixRHSNaturalBoundaryCondition(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, 0);
    //_linearProblem[0]->gatherVector(_linearProblem[0]->RHS(), _seqPressLoad);
    static_cast<LinearProblemNSFracStepAdvDiff*>(m_linearProblem[0])->pressureFWI() = 0;
    
    // PressureFWI - ViscousResidual
    waxpy(m_residual,1., m_linearProblem[0]->vector(),m_viscousResidual);
    m_linearProblem[0]->gatherVector(m_residual, m_seqResidual);
    
  }
  
  PetscVector& NSFracStepModel::seqResidual() {
    return m_seqResidual;
  }
  PetscVector& NSFracStepModel::residual() {
    return m_residual;
  }
  PetscVector& NSFracStepModel::seqViscousResidual() {
    return m_seqViscousResidual;
  }
  
  int& NSFracStepModel::hasLumpedMassRN() {
    return m_hasLumpedMassRN;
  }
  PetscVector& NSFracStepModel::lumpedMassRN() {
    return m_lumpedMassRN;
  }
  
  // *********************** Incremental scheme  ***********************
  void NSFracStepModel::computePressure(){
    // Correction of the pressure: p^n = p^{n-1} + \delta p^n
    m_linearProblem[1]->solution().axpy(1, m_prevPress);
    m_prevPress.copyFrom(m_linearProblem[1]->solution());
  }
  
}
