//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef ElasticStringModel_hpp
#define ElasticStringModel_hpp

// System includes

// External includes

// Project includes
#include "Model/model.hpp"

namespace felisce {
  class ElasticStringModel:public Model {
  public:
    ElasticStringModel();
    ~ElasticStringModel() override;

    void initializeDerivedModel() override;
    void setExternalVec() override;
    void forward() override;

  private:
    PetscVector m_dispTimeRHS;
    PetscVector m_seqDispTimeRHS;
  };    
}

#endif
