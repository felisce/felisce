//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include <Model/stokesContinuationModel.hpp>

namespace felisce {
  StokesContinuationModel::StokesContinuationModel():Model() {
    m_name = "StokesContinuation";
  }

  // Called in Model::initializeLinearProblem, just after the mesh partitioning.
  void StokesContinuationModel::initializeDerivedModel() {
    // Saving the lpb pointer after the static cast
    m_lpb = static_cast<LinearProblemStokesContinuation*>(m_linearProblem[0]);

    // Initializing the PetscVectors
    m_lpb->initPetscVectors();
  }

  void StokesContinuationModel::prepareForward() {

    writeSolution();
    
    updateTime(); //Here the matrix(0), the non constant one, is cleared and the rhs also.

    printNewTimeIterationBanner();

    m_lpb->gatherVectorBeforeAssembleMatrixRHS();

    m_lpb->readData(*io(), m_fstransient->time);
  }

  // this function is written for the time-independent case
  void StokesContinuationModel::forward() {
    prepareForward();

    // Zero-ing the solution
    m_lpb->solution().zeroEntries();
    
    // Assemble the stabilization terms
    m_lpb->assembleCIPStabilization();

    // Assemble the linear system
    m_lpb->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs);

    //Apply boundary conditions for the dual variable
    m_lpb->finalizeEssBCTransient();
    m_lpb->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());
    
    // Solve the linear system
    m_lpb->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    m_lpb->gatherSolution();
  }
}
