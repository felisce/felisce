//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: 
//

// System includes

// External includes

// Project includes
#include "Model/elasticStringModel.hpp"

namespace felisce{
  ElasticStringModel::ElasticStringModel():Model() {
    m_name = "Elastic String Model";
  }


  ElasticStringModel::~ElasticStringModel() = default;


  void ElasticStringModel::initializeDerivedModel() {
    // Here, std::set the initial condition for example
  }

  
  void ElasticStringModel::forward() {
    // Write solution for postprocessing (if required)
    writeSolution();
    
    // Advance time step.
    updateTime();
    
    // Print time information
    printNewTimeIterationBanner();

    // rhs for the first iteration
    // if (m_fstransient->iteration == 1)
    // m_linearProblem[0]->vector().set(-10.); 

    // Assembly loop on elements.
    m_linearProblem[0]->assembleMatrixRHSBD(MpiInfo::rankProc());
    
    // Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    // Set the displacement velocity to d^{n-1}
    m_dispTimeRHS.copyFrom(m_linearProblem[0]->solution());


    // Solve the linear system
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    // Compute the displacement time rhs -> (2 * d^n - d^{n-1})
    m_dispTimeRHS.axpby(2., -1, m_linearProblem[0]->solution());

    // Gather the solutions
    m_linearProblem[0]->gatherSolution();
    m_linearProblem[0]->gatherVector(m_dispTimeRHS, m_seqDispTimeRHS);
  }


  void ElasticStringModel::setExternalVec() {
    // allocate the parallel displacement velocity
    m_dispTimeRHS.duplicateFrom(m_linearProblem[0]->solution());
    m_dispTimeRHS.set(0.);
    
    // allocate the serial displacement velocity
    m_seqDispTimeRHS.duplicateFrom(m_linearProblem[0]->sequentialSolution());
    m_seqDispTimeRHS.set(0.);

    // communicate the serial displacement velocity to the linear problem
    m_linearProblem[0]->pushBackExternalVec(m_seqDispTimeRHS);
  }
}
