//   ______ ______ _    _  _____    ______
//  |  ____|  ____| |  (_)/ ____|  |  ____|
//  | |__  | |__  | |   _| (___   ___| |__
//  |  __| |  __| | |  | |\___ \ / __|  __|
//  | |  | |____| |____| |____) | (__| |____
//  |_|  |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:   LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J Fouchet & C Bui
//

// System includes

// External  includes

// Project includes
#include "Model/NSlinCombModel.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {
  NSlinCombModel::NSlinCombModel():Model() {
    m_lumpedModelBC = nullptr;
    m_name = "NSlinComb";
    m_coefProblem = nullptr;
  }

  NSlinCombModel::~NSlinCombModel() {
    if(m_lumpedModelBC) delete m_lumpedModelBC;
    if(m_coefProblem) delete m_coefProblem;
    m_stationnarySolutionsVec.clear();
    m_labelListlinCombBC.clear();
    m_fluxLinCombBCtotal.clear();
    m_fluxLinCombBC.clear();
    m_solExtrapolate.destroy();
  }

  void NSlinCombModel::initializeDerivedModel() {
    if( FelisceParam::instance().lumpedModelBCLabel.size() ) {
      m_lumpedModelBC = new LumpedModelBC(m_fstransient);
      static_cast<LinearProblemNS*>(m_linearProblem[0])->initializeLumpedModelBC(m_lumpedModelBC);
    }

    if (FelisceParam::instance().orderBdfNS > 2)
      FEL_ERROR("BDF not yet implemented for order greater than 2 with Navier-Stokes.");

    if (FelisceParam::instance().NSequationFlag > 0 && FelisceParam::instance().characteristicMethod == 0)
      FEL_ERROR("You have to use the method of characteristics with this model.");

    m_bdfNS.defineOrder(FelisceParam::instance().orderBdfNS);
    m_linearProblem[0]->initializeTimeScheme(&m_bdfNS);

    m_coefProblem = new CoefProblem();
    m_coefProblem->initializeCoefProblem(MpiInfo::petscComm());
  }

  void NSlinCombModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    m_U_0.duplicateFrom(m_linearProblem[iProblem]->vector());
    m_U_0.set(0.0);

    m_solExtrapolate.duplicateFrom(m_linearProblem[iProblem]->vector());
    m_solExtrapolate.zeroEntries();

    const auto num_dofs = m_linearProblem[iProblem]->numDofPerUnknown(0);
    felInt idGlobalDof[num_dofs];
    felReal valueByDofU_0[num_dofs];

    for ( felInt i = 0; i < num_dofs; i++) {
      idGlobalDof[i] = i;
      valueByDofU_0[i] = 0.;
    }

    //Use global mapping for parallel computation (in sequential: (before mapping idGlobalDof) == (after mapping idGlobalDof)).
    AOApplicationToPetsc(m_linearProblem[iProblem]->ao(),num_dofs,idGlobalDof);
    // "add" values in Petsc vectors.
    m_U_0.setValues(num_dofs,idGlobalDof,valueByDofU_0,INSERT_VALUES);
    //assemble m_U_0.
    m_U_0.assembly();

    //Initialize solution for solver.
    m_linearProblem[iProblem]->solution().copyFrom(m_U_0);
    // First assembly loop in iteration 0 to build static matrix.
    m_linearProblem[iProblem]->assembleMatrixRHS(MpiInfo::rankProc());
  }

  void NSlinCombModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    // _Matrix = _A + _Matrix (add static matrix to the dynamic matrix to build
    // complete matrix of the system
    m_linearProblem[iProblem]->addMatrixRHS();
  }

  void NSlinCombModel::preProcessing() {
    PetscPrintf(MpiInfo::petscComm(),"\n----------------------------------------------\n");
    PetscPrintf(MpiInfo::petscComm(),"   Pre-processing ... ... ...");
    PetscPrintf(MpiInfo::petscComm(),"\n----------------------------------------------\n");

    m_coefProblem->buildSolver();

    //==========================================================
    // Compute the inflowOutflowBCnumber time-indepedent functions u_i
    //==========================================================

    int numInflowOutflowBC = m_linearProblem[0]->getBoundaryConditionList().numNeumannNormalBoundaryCondition();

    // fill m_labelListlinCombBC
    for (int i = 0; i < numInflowOutflowBC; i++) {
      // add the corresponding labels
      std::copy( m_linearProblem[0]->getBoundaryConditionList().NeumannNormal(i)->listLabel().begin(), m_linearProblem[0]->getBoundaryConditionList().NeumannNormal(i)->listLabel().end(), std::back_inserter( m_labelListlinCombBC ) );
    }

    // change of the bc
    //==================

    // If we use the implicit algorithm, all the lumpedModel boundary conditions are defined as NeumannNormal ones ( cf defineBC() ) and
    // all the Dirichlet BC are defined temporarily as homogeneous Dirichlet BC.
    if (m_linearProblem[0]->getBoundaryConditionList().numDirichletBoundaryCondition()) {
      BoundaryCondition* BC = m_linearProblem[0]->getBoundaryConditionList().Dirichlet(0);
      std::vector<double> valueOfBC(BC->getComp().size(), 0.);
      for (std::size_t iDirichletBC = 0; iDirichletBC < m_linearProblem[0]->getBoundaryConditionList().numDirichletBoundaryCondition(); iDirichletBC++) {
        BC = m_linearProblem[0]->getBoundaryConditionList().Dirichlet(iDirichletBC);
        BC->setValue(valueOfBC);
      }
      valueOfBC.clear();
    }

    // "forward" of the preprocessing
    //================================

    PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Allocate Matrix RHS for Alpha_i problem --- --- --- \n ");

    m_coefProblem->allocateMatrixRHS(numInflowOutflowBC, MpiInfo::rankProc());
    m_stationnarySolutionsVec.resize(numInflowOutflowBC);

    postAssemblingMatrixRHS();

    for (int idInflowOutflowBC = 0; idInflowOutflowBC < numInflowOutflowBC; idInflowOutflowBC++) {

      m_linearProblem[0]->vector().zeroEntries();

      PetscPrintf(MpiInfo::petscComm(),"\n-----------------------------\n");
      PetscPrintf(MpiInfo::petscComm(),"   u_i with i=%d ... ... ...",idInflowOutflowBC);
      PetscPrintf(MpiInfo::petscComm(),"\n-----------------------------\n");

      PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Solve Stokes problem --- --- --- \n ");

      PetscPrintf(MpiInfo::petscComm(),"\n ApplyBC ... \n ");

      // The NeumannNormal boundary conditions are updated in allocateElemFieldBoundaryConditionDerivedLinPb()
      m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::matrix_and_rhs, FlagMatrixRHS::matrix_and_rhs, idInflowOutflowBC);

      PetscPrintf(MpiInfo::petscComm(),"\n Solve ... \n ");

      // Solve linear system.
      m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

      PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Compute flux --- --- --- \n ");

      // Computing flux : \int_{\gamma_i}  u_j \cdot normale
      m_linearProblem[0]->computeMeanQuantity(velocity,m_labelListlinCombBC,m_fluxLinCombBC );

      PetscPrintf(MpiInfo::petscComm(),"\n labels :");
      for (unsigned int i = 0; i < m_labelListlinCombBC.size(); i++)
        PetscPrintf(MpiInfo::petscComm()," %d",m_labelListlinCombBC[i]);

      PetscPrintf(MpiInfo::petscComm(),"\n compute Flux :");
      for (unsigned int i = 0; i < m_fluxLinCombBC.size(); i++)
        PetscPrintf(MpiInfo::petscComm()," %f",m_fluxLinCombBC[i]);
      PetscPrintf(MpiInfo::petscComm(),"\n");

      // Stock the solution u_i
      //=============================

      m_stationnarySolutionsVec[idInflowOutflowBC].duplicateFrom(m_linearProblem[0]->solution());
      m_stationnarySolutionsVec[idInflowOutflowBC].copyFrom(m_linearProblem[0]->solution());

      //=============================================
      // Assembling of A (to determine the \alpha_i)
      //=============================================

      PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Assemble Matrix for Alpha_i problem --- --- --- \n ");

      m_coefProblem->assembleMatrix(idInflowOutflowBC, m_fluxLinCombBC, MpiInfo::rankProc());
      //m_coefProblem->writeMatrixForMatlab(30);
    }

    m_linearProblem[0]->solution().zeroEntries();
    m_linearProblem[0]->sequentialSolution().zeroEntries();

    // initialize m_fluxLinCombBCtotal to 0
    m_fluxLinCombBCtotal.resize(m_fluxLinCombBC.size(), 0.);

    // std::set the right values for all the Dirichlet boundary conditions except for the transient ones
    m_linearProblem[0]->finalizeEssBCConstantInT();
  }

  void NSlinCombModel::forward() {
    // Write solution for postprocessing (if required)
    // Update m_seqSol (!). Write it.
    writeSolution();

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();

    if ( m_fstransient->iteration == 1) {
      m_bdfNS.initialize(m_U_0);
      m_bdfNS.extrapolate(m_solExtrapolate);
      m_linearProblem[0]->initExtrapol(m_solExtrapolate);
    } else {
      m_bdfNS.update(m_linearProblem[0]->solution());
      m_bdfNS.extrapolate(m_solExtrapolate);
      m_linearProblem[0]->updateExtrapol(m_solExtrapolate);
    }

    m_bdfNS.computeRHSTime(m_fstransient->timeStep);

    //=====================
    // Compute \tilde{u}^n
    //=====================

    // m_seqSol = u^n-1 thanks to writeSolution()

    // Fill the m_seqBdfRHS
    m_linearProblem[0]->gatherVectorBeforeAssembleMatrixRHS();

    // Assembly loop on elements.
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc());

    // Specific operations before solve the system.
    postAssemblingMatrixRHS();

    // Apply essential transient boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();

    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    // Solve linear system.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    //========================================
    // Assembling of RHS for \alpha_i problem
    //========================================

    PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Compute flux --- --- --- \n ");

    // Computing flux : \int_{\gamma_i}  \tilde{u}^n \cdot normale
    m_linearProblem[0]->computeMeanQuantity(velocity, m_labelListlinCombBC, m_fluxLinCombBC);

    PetscPrintf(MpiInfo::petscComm(),"\n labels :");
    for (unsigned int i = 0; i < m_labelListlinCombBC.size(); i++)
      PetscPrintf(MpiInfo::petscComm()," %d",m_labelListlinCombBC[i]);

    PetscPrintf(MpiInfo::petscComm(),"\n compute Flux :");
    for (unsigned int i = 0; i < m_fluxLinCombBC.size(); i++)
      PetscPrintf(MpiInfo::petscComm()," %f",m_fluxLinCombBC[i]);
    PetscPrintf(MpiInfo::petscComm(),"\n");

    PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Assemble RHS for Alpha_i problem --- --- --- \n ");

    unsigned int numNeumannNormalBCwithoutWindkessel = m_fluxLinCombBC.size() - FelisceParam::instance().lumpedModelBCLabel.size();
    std::vector<double> pressure;
    if (numNeumannNormalBCwithoutWindkessel) {
      BoundaryCondition* BC;
      int idBC;
      for(unsigned int iNeumannNormalBC = 0; iNeumannNormalBC < numNeumannNormalBCwithoutWindkessel; iNeumannNormalBC++) {
        BC = m_linearProblem[0]->getBoundaryConditionList().NeumannNormal(iNeumannNormalBC);
        idBC = m_linearProblem[0]->getBoundaryConditionList().idBCOfNeumannNormal(iNeumannNormalBC);
        switch ( BC->typeValueBC() ) {
        case Constant:
          pressure.push_back(FelisceParam::instance().value[m_linearProblem[0]->getBoundaryConditionList().startIndiceOfValue(idBC)]);
          break;
        case FunctionT:
          // warning: one has to define in his userNS file the following virtual function
          // double userComputeValueNeumannNormalTransient(const int iNeumannNormalBC)
          pressure.push_back(m_linearProblem[0]->userComputeValueNeumannNormalTransient(iNeumannNormalBC));
          break;
        case FunctionS:
        case FunctionTS:
        case Vector:
        case EnsightFile:
          FEL_ERROR("Impossible to compute the value with this type of Boundary Condition.");
          break;
        }
      }
    }

    // here m_fluxLinCombBCtotal = \sum_{j=1}^{n-1} ( \int_{\gamma_i} u^j \cdot normale )
    m_coefProblem->assembleRHS(pressure, m_fluxLinCombBC, m_fluxLinCombBCtotal, MpiInfo::rankProc());
    pressure.clear();
    //m_coefProblem->writeRHSForMatlab(30);

    //==================================================
    // Solve (I-A)alpha = b (to determine the \alpha_i)
    //==================================================

    PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Solve Alpha_i --- --- --- \n ");

    m_coefProblem->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    //==========================
    // Compute the solution u^n
    //==========================

    PetscPrintf(MpiInfo::petscComm(),"\n --- --- --- Revise the solution --- --- --- \n ");

    // m_sol = u^n = \tilde{u}^n + \sum_i alpha_i^n u_i
    static_cast<LinearProblemNS*>(m_linearProblem[0])->reviseSolution(m_coefProblem->solution(), m_stationnarySolutionsVec);

    // Computing flux : \int_{\gamma_i}  u^n \cdot normale
    m_linearProblem[0]->computeMeanQuantity(velocity,m_labelListlinCombBC,m_fluxLinCombBC);
    // update m_fluxLinCombBCtotal = \sum_{j=1}^n ( \int_{\gamma_i} u^j \cdot normale )
    for (unsigned int i = 0; i < m_fluxLinCombBC.size(); i++)
      m_fluxLinCombBCtotal[i] += m_fluxLinCombBC[i];
  }

}
