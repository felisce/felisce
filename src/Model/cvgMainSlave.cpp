//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Aletti
//


// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#ifdef FELISCE_WITH_CVGRAPH
#include "Core/cvgraphUtil.hpp"
#endif

// Project includes
#include "Core/mpiInfo.hpp"
#include "Model/cvgMainSlave.hpp"
#include "Geometry/surfaceInterpolator.hpp"

#ifdef FELISCE_WITH_CVGRAPH

namespace felisce {
  // Initialization:
  // - read the cvgraph data file
  // - overwrite the time loop information in FelisceParam
  // - overwrite the boundary condition type in FelisceParam
  // - build the graph (m_cvg)
  // - initialize the msgAdministrator (m_msg)
  // - std::set other variables such as protocol, compartments ids, names, connections ids
  cvgMainSlave::cvgMainSlave(int argc, char ** argv) {
    // IMPORTANT WARNING:
    // Remember to initialize the slave AFTER the model.
    // The reason is that the model initializes MPI and it should be done as the first thing.

    // ========== Handling parameters =====================//
    // Reading CVGraph parameters
    CVGraphParam::instance().getInputFile(argc, argv, FelisceParam::instance().dataFileCVG.c_str());

    // Overwriting felisce time-parameters parameters
    // These information are used to initialize felisceTransient
    // IMPORTANT WARNING: We have to be careful and construct the felisceTransient after cvgMainSlave.
    FelisceParam::instance().timeStep = CVGraphParam::instance().dt;
    FelisceParam::instance().timeIterationMax  = CVGraphParam::instance().maxIter;
    // Graph initialization and creation
    m_cvg.SetParam();

    // ========== IDs and MSG administrator ===============//
    // id of the current compartment
    m_cmptName = FelisceParam::instance().compartmentName;
    if ( m_cmptName == "none" )
      FEL_ERROR("You must specify the compartmentName in the data file, in the cvgraph section");
    m_cmptId = m_cvg.compartId(m_cmptName);
    m_cvg.adjacentCompartmentsAndConnections(m_cmptName,m_IdsOfOtherCompartments,m_IdsOfConnections);
    m_numOfNeighbours=m_IdsOfOtherCompartments.size();
    for ( std::size_t iConn(0); iConn<m_numOfNeighbours; ++iConn ) {
      m_iConnFromIds[m_IdsOfConnections[iConn]]=iConn;
    }
    // Getting the cvgraph verbosity
    // The policy is that the verbosity of the cvgMainSlave class is controlled by the cvgVerbosity, while the
    // rest of felisce is controlled by the verbosity of FelisceParam.
    m_verbose = CVGraphParam::instance().verb[m_cmptId];
    if ( m_verbose > 1)
      m_cvg.print(m_verbose);
    // TODO this print and the one below
    // are done by all the processors, we should be able to get the rank
    // and let only the master do it.
    if ( m_verbose > 1 )
      CVGraphParam::instance().print(m_verbose);

    // Message administrator (slave) initialization
    m_protocol = 1; // TODO: what is this protocol about? Check in cvgraph.
    m_msg = new MessageAdm(m_verbose,m_protocol,m_cvg.graph(),m_cmptId );

    this->handleCouplingVariablesAndOverwriteBC();

    // TODO: In the case where there are several connections, the variable to exchange may be different. For now FelisceParam::instance().idVarToExchange is a single integer but it should be a std::vector of integers in the future.
    for ( std::size_t iConn(0); iConn<m_numOfNeighbours; ++iConn ) {
      m_IdsOfVarToExchange.push_back(FelisceParam::instance().idVarToExchange);
    }
    m_residualInterpolationType.resize(m_numOfNeighbours);
    for ( std::size_t iConn(0); iConn<m_numOfNeighbours; ++iConn ) {
      int idconn = m_IdsOfConnections[iConn]; //id connection
      // 0: Internodes, 1: transposed interpolator
      m_residualInterpolationType[iConn] = CVGraphParam::instance().residualInterpType[idconn];
      // TODO: in principle we could use a different residual interpolation method for the forward and backward direction
      // for now we assume that the entire connection uses the same approach
    }

    if ( FelisceParam::verbose() > 1)
      std::cout<<"Slave "<<m_cmptName<<" initialization completed."<<std::endl;
  }

  void cvgMainSlave::handleCouplingVariablesAndOverwriteBC() {

    /*
      For each connection we have one (or more) variables to send
      and one (or more) variable to read.

      - The number of variables to read for the connection iConn is m_numVarToRead[iConn]
      - The number of variables to send for the connection iConn is m_numVarToSend[iConn]

      - The list of boundary labels for the connection iConn is m_interfaceLabels[iConn]

      For now, std::vector variables such as the velocity, are NOT stored component-wise.
      We could pass separately u_x,u_y,u_z, but we just pass \vec u.

      Depending on the variable to read the code overwrite the boundary condition type
      with the appropriate type. For instance, if we read VELOCITY, we are going to replace
      the type of the boundary condition (CVGRAPH) with Dirichlet.

      Moreover if the number of variables to read is equal to two, we assume that the type
      of boundary conditions is Robin.

      Be careful when using Robin boundary condition: in the felisce datafile all physical robin boundary
      conditions should be listed BEFORE the cvgraph boundary condition.
      The alphaRobin parameter is specified in the cvgraph datafile.
    */

    m_interfaceLabels.resize(numConnections());
    m_numVarToRead.resize(numConnections());
    m_numVarToSend.resize(numConnections());

    for ( std::size_t iConn(0); iConn<m_numOfNeighbours; ++iConn ) {
      int idconn = m_IdsOfConnections[iConn]; //id connection

      std::vector<std::string> readVar;
      m_numVarToRead[iConn] = m_cvg.graph()[m_cvg.connectDescriptor( idconn ) ].numToBeReadVariables(m_cmptName);
      for (std::size_t v(0); v<m_numVarToRead[iConn]; v++){
        readVar.push_back(this->variableToRead(idconn,v));
      }
      m_readVariables.push_back(readVar);

      std::vector<std::string> sendVar;
      m_numVarToSend[iConn] = m_cvg.graph()[m_cvg.connectDescriptor( idconn ) ].numToBeSentVariables(m_cmptName);
      for (std::size_t v(0); v<m_numVarToSend[iConn]; v++){
        sendVar.push_back(this->variableToSend(idconn,v));
      }
      m_sendVariables.push_back(sendVar);
    }

    std::size_t start(0);
    for ( std::size_t iBC(0); iBC<FelisceParam::instance().type.size(); ++iBC ) {
      std::size_t numLabel=FelisceParam::instance().numLabel[iBC];
      bool cvgraphRobin(false);
      bool found(false);
      for ( std::size_t iConn(0); iConn<m_numOfNeighbours && !found; ++iConn ) {
        int idconn = m_IdsOfConnections[iConn]; //id connection
        std::string otherCmpt = m_cvg.compartNameFromId(m_IdsOfOtherCompartments[iConn]);
        std::stringstream tmpType;
        tmpType<<"cvgraph"<<otherCmpt;
        if ( FelisceParam::instance().type[iBC] == tmpType.str() ) {
          found=true;
          //Extract the labels
          for ( std::size_t iLab(0); iLab<numLabel; ++iLab ) {
            m_interfaceLabels[iConn].push_back(FelisceParam::instance().label[start+iLab]);
          }
          // TODO: this it should be more flexible.
          // It is very likely that it causes problems.
          if ( neumann(iConn) ) {
            FelisceParam::instance().type[iBC]="Neumann";
          } else if ( dirichlet(iConn) ) {
            FelisceParam::instance().type[iBC]="Dirichlet";
          } else if ( robin(iConn) ) {
            cvgraphRobin=true;
            FelisceParam::instance().type[iBC] = "Robin";
            FelisceParam::instance().betaRobin.push_back(1.);
            if (m_cvg.isSource(m_cmptId, idconn)) {
              FelisceParam::instance().alphaRobin.push_back(CVGraphParam::instance().robSource[idconn]);
            } else {
              FelisceParam::instance().alphaRobin.push_back(CVGraphParam::instance().robTarget[idconn]);
            }
          }
        } else if (cvgraphRobin && FelisceParam::instance().type[iBC]=="Robin") {
          FEL_ERROR("When using Robin BC with both cvgraph and physical BCs, cvgraph BCs should be placed AFTER the physical robin BC in the felisce data file");
        }
      }
      start += numLabel;
    }
#ifndef NDEBUG
    // In debug mode we count all the interface labels and we verify that we replaced all the BCs.
    std::vector<int> allInterfaceLabels;
    for ( std::size_t iConn(0); iConn<m_numOfNeighbours; ++iConn ) {
      for ( std::size_t iDebug(0); iDebug<m_interfaceLabels[iConn].size(); ++iDebug )
        allInterfaceLabels.push_back(m_interfaceLabels[iConn][iDebug]);
    }
    std::vector<int> tmp=FelisceParam::instance().interfaceLabels;
    Tools::printVector(tmp,"tmp");
    Tools::printVector(allInterfaceLabels,"allInterfaceLabels");
    FEL_ASSERT(tmp.size()==allInterfaceLabels.size());
    sort(allInterfaceLabels.begin(),allInterfaceLabels.end());
    sort(tmp.begin(),tmp.end());
    FEL_ASSERT(tmp.size()==allInterfaceLabels.size());
    for ( std::size_t iDebug(0); iDebug<tmp.size(); ++iDebug ) {
      FEL_ASSERT(tmp[iDebug]==allInterfaceLabels[iDebug]);
    }
#endif
  }


  // The goals of this function are:
  // - send to the other compartments the information necessary to build the interpolators (points of the current mesh, ids).
  // - receive the same information from the other compartments.
  // - initialize properly the volume-boundary mappings in dofBoundary
  // - use the functions of the surfaceInterpolator class to build the interpolators.
  // Note that the surfaceInterpolator object is used to build this matrix and then destroyed
  void cvgMainSlave::buildInterpolator(LinearProblem* pt) {
    // Surface Interpolator is felisce class that is
    // able to construct an interpolator matrix between two surfaces.
    // This class needs informations in linear problem and in the dofBoundary class.
    SurfaceInterpolator surfInterpolator;
    m_interpolator.resize(m_numOfNeighbours);
    m_dimOfDataToReceive.resize(m_numOfNeighbours);
    m_dimOfDataToSend.resize(m_numOfNeighbours);
    // Resizing std::vector of dofBoundary in linearProblem
    pt->dofBDVec().resize(m_numOfNeighbours);
    pt->massBDVec().resize(m_numOfNeighbours);
    pt->kspMassBDVec().resize(m_numOfNeighbours);
    std::vector<DofBoundary*> dofBDVec(m_numOfNeighbours);
    for ( std::size_t iCmpt(0); iCmpt<m_numOfNeighbours; ++iCmpt ) {
      dofBDVec[iCmpt]=&pt->dofBD(iCmpt);
    }
    surfInterpolator.initSurfaceInterpolator(pt,dofBDVec);

    // Initialization of mesh and dof boundary.
    // It is important to use the global mesh because of the algorithm
    // that projects a point on a surface.
    // Both dofBoundary class in linearProblem
    // and the geometricMeshRegion class holding the global mesh needs to be correctly initialized.
    m_lpb=pt;
    surfInterpolator.initializeSurfaceInterpolator();

    for ( std::size_t iCmpt(0); iCmpt<m_numOfNeighbours; ++iCmpt ) {
      m_lpb->dofBD(iCmpt).initialize(m_lpb);
      // Here the dofBoundary class is used to be the mapping that connects
      // indices on the surfaces to the indices of the volume
      // this is done with the help of the linearProblem class.
      // TODO for now we are considering the first unknown and its components to be coupled.
      // one should be able to define in the felisce data file which unknown and which components.
      // This has to be done also below where the interface file is written.
      std::vector<int> listComp;
      int iVar = m_IdsOfVarToExchange[iCmpt]; // iConn = iCmpt ??
      std::size_t numComp=m_lpb->listVariable()[iVar].numComponent();
      for ( std::size_t idim(0); idim < numComp; ++idim) {
        m_lpb->dofBD(iCmpt).buildListOfBoundaryPetscDofs(m_lpb, m_interfaceLabels[iCmpt], iVar, idim);
        listComp.push_back(idim);
      }
      m_lpb->dofBD(iCmpt).buildBoundaryVolumeMapping(iVar,listComp);

      // Only the master does the job here.
      if ( MpiInfo::rankProc() == 0 ) {
        // SENDING INFO TO THE OTHER COMPARTMENT
        surfInterpolator.writeInterfaceFile(iVar,numComp,fileName("Interface",iCmpt,"write"),CVGraphParam::instance().dirTmpFile, iCmpt);
        std::cout<<CVGraphParam::instance().dirTmpFile <<fileName("Interface",iCmpt,"write")<<" written."<<std::endl;
        // Sending a message to the other compartment saying that the interface file has been written and that they can start reading it.
        this->customFileCompleted("interface",iCmpt);
      }
    }
    for ( std::size_t iCmpt(0); iCmpt<m_numOfNeighbours; ++iCmpt ) {
      if ( MpiInfo::rankProc() == 0 ) {
        // RECEIVING INFO FROM THE OTHER COMPARTMENT
        // the list of points to be projected along with the surface label where to find them
        std::vector< std::pair< Point*, felInt > > pointsAndCorrespondingLabel;
        // the corresponding row ids. If more than one component is used the ids are stores as id(P1,icomp0),id(P1,icomp1),id(P1,icomp2), id(P2,icomp0),id(P2,icomp1),id(P2,icomp2)
        std::vector<int> rowNumbering;
        int iVar = m_IdsOfVarToExchange[iCmpt];
        // Before starting to read we check if the other compartment has finished to write the file.
        this->checkCustomFileCompleted("interface",iCmpt);

        // Reading the file through the functions of surfInterpolator
        surfInterpolator.readInterfaceFile(fileName("Interface",iCmpt,"read"),pointsAndCorrespondingLabel,rowNumbering, CVGraphParam::instance().dirTmpFile);
        std::cout<<CVGraphParam::instance().dirTmpFile <<fileName("Interface",iCmpt,"read")<<" read."<<std::endl;

        // Build the interpolator matrix (m_interpolator)
        surfInterpolator.buildInterpolator(*m_interpolator[iCmpt],pointsAndCorrespondingLabel,rowNumbering,m_lpb->listVariable()[0],iVar,iCmpt,this->interfaceLabels(iCmpt),fileName("LogInterpolator",iCmpt,"write",CVGraphParam::instance().dirTmpFile));

        // Extract the information about the dimension of the data to be sent and to be received.
        m_dimOfDataToReceive[iCmpt] = surfInterpolator.ncols(iCmpt); //num of dof in this linearProblem (on the surface)
        m_dimOfDataToSend[iCmpt] = surfInterpolator.nrows(iCmpt); //num of dof in the other linearProblem (on the surface)
        if ( FelisceParam::verbose() > 1 ) {
          std::cout<<"Interpolator for "<<m_cvg.compartNameFromId(m_IdsOfOtherCompartments[iCmpt])<<"built, nrows: "<<m_dimOfDataToSend[iCmpt]<<", ncols: "<<m_dimOfDataToReceive[iCmpt]<<std::endl;
        }
      }
    }

    this->exchangeBackwardInterpolators();

    std::stringstream chronoName;
    chronoName<<"receive_"<<m_cmptName;
    m_receiveChronoPtr = felisce::make_shared<ChronoInstance>();
  }

  void cvgMainSlave::exchangeBackwardInterpolators() {
    if ( MpiInfo::rankProc() == 0 ) {
      for ( std::size_t iCmpt(0); iCmpt<m_numOfNeighbours; ++iCmpt ) {
        if ( m_residualInterpolationType[iCmpt] == 1 ) {
          //writing interpolator matrix
          m_interpolator[iCmpt]->saveInBinaryFormat(MPI_COMM_SELF,fileName("interpolatorMatrix",iCmpt,"write").c_str(),CVGraphParam::instance().dirTmpFile);
          this->customFileCompleted("interpolatorMatrix",iCmpt);
        }
      }
      m_interpolatorBackward.resize(m_numOfNeighbours);
      for ( std::size_t iCmpt(0); iCmpt<m_numOfNeighbours; ++iCmpt ) {
        if ( m_residualInterpolationType[iCmpt] == 1 ) {
          // Memory allocation
          m_interpolatorBackward[iCmpt]->createSeqAIJ(MPI_COMM_SELF,m_dimOfDataToReceive[iCmpt],m_dimOfDataToSend[iCmpt],0,nullptr);
          m_interpolatorBackward[iCmpt]->setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
          m_interpolatorBackward[iCmpt]->setFromOptions();
          // Read from file
          this->checkCustomFileCompleted("interpolatorMatrix",iCmpt);
          m_interpolatorBackward[iCmpt]->loadFromBinaryFormat(MPI_COMM_SELF,fileName("interpolatorMatrix",iCmpt,"read").c_str(),CVGraphParam::instance().dirTmpFile);
        }
      }
    }
  }

  std::string cvgMainSlave::fileName(std::string keyword, std::size_t iConn, std::string readOrWrite, std::string folder) const {
    std::stringstream filename;
    filename<<folder<<"/"<<keyword;
    if ( readOrWrite == "read" ) {
      filename<<"_"<<m_cvg.compartNameFromId(m_IdsOfOtherCompartments[iConn])<<"_"<<m_cmptName;
    } else if ( readOrWrite == "write" ) {
      filename<<"_"<<m_cmptName<<"_"<<m_cvg.compartNameFromId(m_IdsOfOtherCompartments[iConn]);
    }
    return filename.str();
  }
  // It writes an empty file just to tell the other compartment that the interface file is completed.
  void cvgMainSlave::customFileCompleted(std::string keyword, std::size_t iBD) const {
    std::stringstream file;
    file<<CVGraphParam::instance().dirTmpFile<<"/"<<keyword<<m_cmptName<<"_"<<m_cvg.compartNameFromId(m_IdsOfOtherCompartments[iBD])<<"completed.ok";
    cvgraph::writeEmptyFile(file.str().c_str(),CVGraphParam::instance().verbose);
  }

  // It check if the (empty) file is present, if yes it means that we can start reading the interface file
  // and we can remove the empty file since it is no longer needed.
  void cvgMainSlave::checkCustomFileCompleted(std::string keyword,std::size_t iBD) const {
    // TODO:
    // These communications should be handled also via MPI and pvm
    // In general cvgMainSlave has been designed for file communications, but it should be easy to generalize it.
    std::stringstream fname;
    fname<<CVGraphParam::instance().dirTmpFile<<"/"<<keyword<<m_cvg.compartNameFromId(m_IdsOfOtherCompartments[iBD])<<"_"<<m_cmptName<<"completed.ok";
    cvgraph::wait_and_remove_file(fname.str().c_str(),CVGraphParam::instance().verbose);
  }

  // First contact between master and slave.
  // Main task of this function:
  // - initialize the m_data_recv(send) vectors with the correct sizes.
  // TODO i think an unnecessary copy of the data is done.
  // probably we should use a petscVector defined on the boundary to store the data
  // and give his underlying pointer to the data to the cvg data structures.
  // to avoid at maximum the duplications.
  void cvgMainSlave::firstContact() {
    // initialization
    m_data_recv.resize(m_numOfNeighbours);
    m_data_send.resize(m_numOfNeighbours);
    m_dataSendPreInterpolation.resize(m_numOfNeighbours);
    m_dataSend.resize(m_numOfNeighbours);
    if (MpiInfo::rankProc() == 0) {
      for (std::size_t iCmpt(0); iCmpt<m_numOfNeighbours; ++iCmpt) {
        int idconn = m_IdsOfConnections[iCmpt]; //id connection

        std::vector<std::size_t> dim_recv(m_numVarToRead[iCmpt]), dim_sent(m_numVarToSend[iCmpt]);
        for(std::size_t iSend=0;iSend<m_numVarToSend[iCmpt];iSend++)
          dim_sent[iSend] = m_dimOfDataToSend[iCmpt];
        for(std::size_t iRead=0;iRead<m_numVarToRead[iCmpt];iRead++)
          dim_recv[iRead] = m_dimOfDataToReceive[iCmpt];

        // Now we can allocate the vectors of the CVGgraph
        m_data_recv[iCmpt] = &m_cvg.initDataReceived(dim_recv, m_cmptName, m_cvg.connectNameFromId(idconn));
        m_data_send[iCmpt] = &m_cvg.initDataSent(dim_sent, m_cmptName, m_cvg.connectNameFromId(idconn));

        m_interpolator[iCmpt]->getVecs(m_dataSendPreInterpolation[iCmpt],m_dataSend[iCmpt]);
      }
      //Now we exchange some quantities with the master.
      int rcvProtocol;
      m_msg->receiveInitFromMaster(rcvProtocol);
      // We also check that the master and the slave are on the same page.
      if(rcvProtocol != m_protocol){
        CVG_ERROR("Protocol mismatch !");
      }
      if(!Tools::equal(FelisceParam::instance().timeStep,CVGraphParam::instance().dt)){
        CVG_ERROR("Different time step Master-Slave!");
      }
      if(FelisceParam::instance().timeIterationMax != CVGraphParam::instance().maxIter){
        std::cout<<FelisceParam::instance().timeIterationMax<<" <-fp cvgp-> "<<CVGraphParam::instance().maxIter<<std::endl;
        CVG_ERROR("Different number of max Iterations Master-Slave!");
      }

      int physUnit = CVGraphParam::instance().physUnit[m_cmptId];
      int spaceDim = CVGraphParam::instance().spaceDim[m_cmptId];
      double physTime = CVGraphParam::instance().physTime[m_cmptId];

      m_msg->sendInitToMaster(physUnit,spaceDim,physTime);
      std::cout<<"First contact phase completed."<<std::endl;

    }
  }
  void cvgMainSlave::exchangeInitialCondition() {
    if ( MpiInfo::rankProc() == 0 ) {
      m_msg->PrintCommonIterationBanner(m_msgInfo,/*iter*/0);
      cvgraph::ConnectionIterator ed, lastE;
      for (std::tie(ed,lastE) = edges(m_cvg.graph()); ed != lastE; ed++) {
        if ( m_cvg.isSource(m_cmptId, m_cvg.connectId(*ed)) or m_cvg.isTarget(m_cmptId,m_cvg.connectId(*ed)) ) {
          m_msg->receiveTimeFromMaster(m_msgInfo,*ed);
          m_msg->PrintIteration(m_msgInfo, /*iter*/0, m_cvg.connectName(*ed));
          m_lpb->sendInitialCondition(this->iConn(m_cvg.connectId(*ed)));
        }
      }
    }
  }
  // Given a sequential volume std::vector (such as the sequential solution of the problem for instance)
  // the data of the interface are extracted, interpolated on the other compartment mesh and sent to the master.
  // The unknown and the components have been used
  // to initialize the dofBoundary class in buildInterpolator, so the functions already knows which dofs have to be extracted).
  // It has to be a sequential std::vector for now since we do getValues with all the surface dofs
  // TODO: I think we could pass also a parallel std::vector, extract only the local values gather them in one boundary std::vector
  // and send them. All the utilities to do that are already in dofBoundary and linearProblem.
  void cvgMainSlave::sendData(std::vector<PetscVector>& sequentialVolumeVectors, std::size_t iConn){
    if ( MpiInfo::rankProc() == 0 ) {
      std::cout<<"sending data to master..."<<std::endl;
      for ( std::size_t cVar(0); cVar<m_numVarToSend[iConn]; ++cVar ) {
        double * dataSendPreInterpolationArray;
        double * dataSendArray;
        // Get the pointer to the array underlying the PetscVector m_dataSendPreInterpolation
        m_dataSendPreInterpolation[iConn].getArray(&dataSendPreInterpolationArray);
        // Getting the values of sequential volume std::vector located in glob2PetscVol locations  saving them to the array pointer by dataSendPreInterpolationArray.
        // Which means that they are saved in the PetscVector m_dataSendPreInterpolation in the location from 0 to numGlobalDofInterface -1;
        sequentialVolumeVectors[cVar].getValues  ( m_dimOfDataToReceive[iConn], m_lpb->dofBD(iConn).glob2PetscVolPtr(),dataSendPreInterpolationArray);
        // Restore array. We no longer need this pointer. We restore it. (see petsc documentation)
        m_dataSendPreInterpolation[iConn].restoreArray(&dataSendPreInterpolationArray);
        // Apply the interpolator matrix to m_dataSendPreInterpolation save the result in dataSend
        if ( m_residualInterpolationType[iConn] == 1 && CVGraph::neumannTypeVariable(sendVariable(iConn,cVar)) ) {
          multTranspose(*m_interpolatorBackward[iConn],m_dataSendPreInterpolation[iConn],m_dataSend[iConn]);
        } else {
          mult(*m_interpolator[iConn],m_dataSendPreInterpolation[iConn],m_dataSend[iConn]);
        }
        // Get the pointer to this array
        m_dataSend[iConn].getArray(&dataSendArray);
        // Copy the content of m_dataSend into m_data_send data structure.
        double* cvgDataSend = m_data_send[iConn]->begin();
        for ( int k(0); k < m_dimOfDataToSend[iConn] ; ++k) {
          cvgDataSend[k+cVar*m_dimOfDataToSend[iConn]] = dataSendArray[k];
        }
        //Restoring
        m_dataSend[iConn].restoreArray(&dataSendArray);
      }
      m_msg->sendDataToMaster(m_msgInfo, *m_data_send[iConn], m_cvg.connectDescriptor(m_IdsOfConnections[iConn] ));
      std::cout<<"done."<<std::endl;
    }
  }
  // Data are simply received and stored in a sequentialVolumeVector, such as the sequential solution of the problem for instance)
  // only the master reads these data, but then they are broadcasted to everyone.
  // TODO this function could be generalized to work with a parallel std::vector.
  void cvgMainSlave::receiveData(std::vector<PetscVector>& sequentialVolumeVectors, std::size_t iConn){
    if ( MpiInfo::rankProc() == 0 ) {
      std::cout<<"receiving data from master..."<<std::endl;
      m_receiveChronoPtr->start();
      m_msg->receiveDataFromMaster(m_msgInfo, *m_data_recv[iConn],m_cvg.connectDescriptor(m_IdsOfConnections[iConn] ));
      m_receiveChronoPtr->stop();
      for ( std::size_t cVar(0); cVar<m_numVarToRead[iConn]; ++cVar ) {
        sequentialVolumeVectors[cVar].setValues  ( m_dimOfDataToReceive[iConn], m_lpb->dofBD(iConn).glob2PetscVolPtr(), m_data_recv[iConn]->begin(cVar*m_dimOfDataToReceive[iConn]), INSERT_VALUES);
        sequentialVolumeVectors[cVar].assembly();
      }
      std::cout<<"done"<<std::endl;
    }
    m_receiveChronoPtr->toFileMax(); // It has to be outside of the if because it contains MPI communications
    for ( std::size_t cVar(0); cVar<m_numVarToRead[iConn]; ++cVar ) {
      sequentialVolumeVectors[cVar].broadCastSequentialVector(MpiInfo::petscComm(),/*master id with respect to the communicator*/0);
    }
  }
  // If a new time iteration starts, print a banner and update the counter
  void cvgMainSlave::printIteration(int& timeIteration) {
    if ( MpiInfo::rankProc() == 0 ){
      if ( m_msgInfo.timeStatus() == CVG_TIME_STATUS_NEW_TIME_STEP ) {
        timeIteration++;
        m_msg->PrintCommonIterationBanner(m_msgInfo, timeIteration);
      }
    }
    MPI_Bcast(&timeIteration,1,MPI_INT,/*master*/0,MpiInfo::petscComm());
  }

  // General function: given a time status flag the master checks if this is current status
  // and then broadcast the answer.
  bool cvgMainSlave::checkTimeStatus(const cvgraph::CVGStatus flag) const {
    // False by default
    int result(0);
    // Master does the check, it is the only one that handles communications
    if ( MpiInfo::rankProc() == 0 ) {
      result = m_msgInfo.timeStatus() == flag;
    }
    // The result is broadcasted to the other processors of this slave.
    MPI_Bcast(&result, 1, MPI_INT, /*master*/0, MpiInfo::petscComm());
    // Then the result is returned casted to bool.
    return bool(result);
  }

  bool cvgMainSlave::initialConditionNeeded( std::size_t iConn ) const {
    return m_cvg.cmptHaveToSendInitialCondition( m_cmptId, this->IdConn(iConn) );
  }
  void cvgMainSlave::print() const {
    std::cout << "--------------------------------------------------" << std::endl;
    std::cout << "                 cvgMainSlave                     " << std::endl;
    std::cout << " Compartment-node name: "<<m_cmptName               << std::endl;
    std::cout << " Number of neighbours:  "<<m_numOfNeighbours << std::endl;
    // TODO: print more info.
    std::cout << "--------------------------------------------------" << std::endl;
  }
  bool cvgMainSlave::thereIsAtLeastOneConditionOfType(bool(cvgMainSlave::*test)(std::size_t) const) const {
    for ( std::size_t iConn(0); iConn<m_numOfNeighbours; ++iConn ) {
      if ( (this->*test)(iConn) )
        return true;
    }
    return false;
  }
  std::string cvgMainSlave::neumannVariable(std::size_t iConn) const {
    if ( neumann(iConn) ) {
      return m_readVariables[iConn][0];
    }
    if ( robin(iConn) ) {
      if ( CVGraph::neumannTypeVariable(m_readVariables[iConn][0]) ) {
        return m_readVariables[iConn][0];
      } else if( CVGraph::neumannTypeVariable(m_readVariables[iConn][1] ) ) {
        return m_readVariables[iConn][1];
      }
    }
    return "Error";
  }
}
#endif
