//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _ALPCURVMODEL_HPP
#define _ALPCURVMODEL_HPP

// System includes

// External includes
#include <petscvec.h>

// Project includes
#include "Model/ALPModel.hpp"
#include "Solver/eigenProblem.hpp"
#include "Solver/eigenProblemALPCurv.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  /*!
   \file ALPCurvModel.hpp
   \authors E. Schenone
   \date 10/12/2012
   \brief ALP model class manages Approximated Lax Pairs decomposition.
   */
  class ALPCurvModel:
    public ALPModel {
  public:
    /// Construtor.
    ALPCurvModel();
    /// Destructor.
    ~ALPCurvModel() override;
    void initializeEigenProblem(std::vector<EigenProblemALPCurv*> eigenPb);
    void solveEigenProblem();
    /// Manage time iteration.
    void forward() override;
  protected:
  private:
  };
}


#endif
