//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & M. Fragu
//

#ifndef _MODEL_HPP
#define _MODEL_HPP

// System includes
#include <unordered_map>
#include <string>
#include <vector>

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#ifdef FELISCE_WITH_SLEPC
#include "Core/NoThirdPartyWarning/Slepc/slepc.hpp"
#endif

// Project includes
#include "Solver/linearProblem.hpp"
#include "Core/commandLineOption.hpp"
#include "Core/felisceParam.hpp"
#include "Core/felisceTransient.hpp"
#include "Core/mpiInfo.hpp"
#include "Core/shared_pointers.hpp"
#include "Model/initialCondition.hpp"
#include "Model/solutionBackup.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/*!
  \class Model
  \authors J. Foulon & M. Fragu
  \date 04/02/2011
  \brief Manage a general Felisce Model.
  */
class Model {
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Model
  FELISCE_CLASS_POINTER_DEFINITION(Model);

  ///@}
  ///@name Life Cycle
  ///@{

  ///Constructor.
  //=============
  Model();

  ///Destructor.
  //============
  virtual ~Model();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /// Function to initialize model
  //==============================
  virtual void initializeModel(
    CommandLineOption& opt, 
    FelisceTransient& fstransient,
    const MPI_Comm comm = MPI_COMM_WORLD,
    const bool callPetscInitialize = true
    );

  virtual void initializeDerivedModel() {}

  virtual void initializeDerivedLinearProblem() {}

  virtual void setExternalVec() {}

  /*!
    * \brief Function to initialize linear problem m_linearProblem[k].
    *
    * \param[in] doUseSNES True if you are handling an non-linear problem and using Petsc's SNES Newton-Raphson method
    * to manage it.
    */
  void initializeLinearProblem(LinearProblem* linearPb, bool doUseSNES = false);

  /*!
   \brief Initialization of all linear systems.
   \param[in] linearPb the vector of pointer to the linear problems.
  */
  void initializeLinearProblem(const std::vector<LinearProblem*>& linearPb, bool doUseSNES = false);

  void initializeLinearProblem(const std::vector<LinearProblem*>& linearPb, const std::vector<bool>& doUseSNES);

  /*!
    * \brief Function to finalizes linear problem m_linearProblem[k].
    *
    * \param[in] doUseSNES True if you are handling an non-linear problem and using Petsc's SNES Newton-Raphson method
    * to manage it.
    */
  void finalizeLinearProblem(LinearProblem* linearPb);

  void finalizeLinearProblem(const std::vector<LinearProblem*>& linearPb);

  //================

  /// Virtual function to specify special action before assembling(duplicate a matrix, work specific on RHS,...)
  virtual void preAssemblingMatrixRHS(std::size_t iProblem=0) {
    (void) iProblem;
  }

  /// Virtual function to specify special action after assembling(duplicate a matrix, work specific on RHS,...)
  virtual void postAssemblingMatrixRHS(std::size_t iProblem=0) {
    (void) iProblem;
  }

  /// Virtual function to specify special action before time loop (solve time-independent problem, ...)
  virtual void preProcessing() {}

  /// Manage time iteration.
  virtual void updateTime(FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs);

  /// Advance time step.
  virtual void forward()=0;

  /// Solve the problem with the initial conditions.
  virtual void SolveStaticProblem();

  /**
    * @brief Solve the dynamic problem.
    * A call to this method performs all the work, including initial call to solve the static problem.
    */
  virtual void SolveDynamicProblem();

  /// Get boolean to stop loop on time.
  bool hasFinished();

  /// Make m_hasFinished true when stop loop on time according to
  /// another criterion (steady-state for example) before reaching timeMax or timeIterationMax
  void makeHasFinishedTrue() {
    m_hasFinished = true;
  }

  ///Input and output tools
  //=======================

  // reah mesh file to define problem
  virtual void readMesh();

  // reah mesh file to define problem
  virtual void writeMesh();

  // build edges of mesh
  void buildEdges();

  // build faces of mesh
  void buildFaces();

  // write solution with ensight
  void writeSolution();

  /*!
    * \brief Write the matrix in Matlab format
    *
    * \param[in] filename Name of the file to be written
    * \param[in] index Index in the std::vector of the linear problem which matrix is printed
    */
  void writeMatrixForMatlab(const std::string& filename, unsigned int index = 0) const;

  /*!
  * \brief Write the RHS in Matlab format
  *
  * \param[in] filename Name of the file to be written
  * \param[in] index Index in the std::vector of the linear problem which rhs is printed
  */
  void writeRHSForMatlab(const std::string& filename, unsigned int index = 0) const;

  // Function tu compute boundary stress in a specific label
  void ComputeStress(int coef, int label, std::vector<double>& stress);

  virtual void setInitialCondition();
  
  virtual void userDefinedInitialConditions(std::size_t iProblem=0) {(void) iProblem;}

  //Callback functions.
  //==================

  /// Register a callable (function, functor) in the callback std::unordered_map.
  void addCallbackXYZ (const char *callbackName, const CallbackXYZ &callbackXYZ);
  void addCallbackXYZT(const char *callbackName, const CallbackXYZT &callbackXYZT);

  /// Get result of evaluation of callable named 'name' with given arguments.
  double evaluateCallback(const char *name, double x, double y, double z);
  double evaluateCallback(const char *name, double x, double y, double z, double t);

  // CVGraph
  //========
  #ifdef FELISCE_WITH_CVGRAPH
  virtual void startIterationCVG();
  virtual void cvgraphNewTimeStep(){};

  /// Impose the entire solution at the interface
  void ImposeCVGraphInterfaceSolution(double* ImpValue);

  /// Impose the solution corresponding to the variable with idVariable and a computed variable needed by the users
  void ImposeCVGraphInterfaceSolutionAndComputedVariable(double* ImpValue, int idVariable);
  
  /// Extract the entire solution at the interface
  virtual void ExtractCVGraphInterfaceSolution(double* valueDofInterface);
  
  /// Extract only the solution corresponding to the variable with idVariable
  void ExtractCVGraphInterfaceSolution(double* valueDofInterface, int idVariable);
  
  /// Extract the solution with idVariable and a specific variable computed by the user
  void ExtractCVGraphInterfaceSolutionAndComputedVariable(double* valueDofInterface,int idVariable,std::vector<double>& interfaceComputedVariable);

  /// Store the entire solution at the interface in order to use it as previous time step solution
  void StoreCVGraphInterfaceSolution(double* StoringValue);
  #endif

  ///@}
  ///@name Access
  ///@{

  IO* io(std::size_t i=0) {
    return m_ios[i].get();
  }

  FelisceTransient::Pointer fstransient() {
    return m_fstransient;
  }

  GeometricMeshRegion::Pointer mesh(const std::size_t i=0) {
    return m_mesh[i];
  }

  LinearProblem* linearProblem(const std::size_t i=0) {
    return m_linearProblem[i];
  }

  /// Get model name
  const std::string& name() const {
    return m_name;
  }

  /// Get model instance index
  std::size_t& instanceIndex() {
    return m_instanceIndex;
  }

  std::size_t instanceIndex() const {
    return m_instanceIndex;
  }

  /// print a banner with text, the time and the iteration number
  void printNewTimeIterationBanner(); 

  ///Get Time.
  double getTime() const;
  ///Set Time.
  void setTime(const double time);

  /// Function to get size of the state std::vector.
  virtual int getNstate() const {
    return 0;
  }
  virtual int getNstate(PhysicalVariable variable) const {
    (void) variable;
    return 0;
  }

  /// Function to get state vector.
  virtual void getState(double* & state) {
    (void) state;
  }

  /// Function to std::set state std::vector.
  virtual void setState(double* & state) {
    (void) state;
  }

  inline const MapCallbackXYZ&  mapCallbackXYZ () const {
    return m_mapCallbackXYZ;
  }
  inline       MapCallbackXYZ&  mapCallbackXYZ ()       {
    return m_mapCallbackXYZ;
  }
  inline const MapCallbackXYZT& mapCallbackXYZT() const {
    return m_mapCallbackXYZT;
  }
  inline       MapCallbackXYZT& mapCallbackXYZT()       {
    return m_mapCallbackXYZT;
  }

  CallbackXYZ&  callbackXYZ (const char* name);
  CallbackXYZT& callbackXYZT(const char* name);

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  /// model name
  std::string m_name = "GIVE_A_NAME_TO_YOUR_MODEL";

  /// Instance index
  std::size_t m_instanceIndex = 0;

  /// Global mesh
  std::vector<GeometricMeshRegion::Pointer> m_mesh;

  /// InputOutput manager
  std::vector<IO::Pointer> m_ios;

  FelisceTransient::Pointer m_fstransient = nullptr;

  /// Linear Problem
  std::vector<LinearProblem*> m_linearProblem;

  // Has finished flag
  bool m_hasFinished = false;

  //CVGraphInterface
  CVGraphInterface::Pointer m_cvgraphInterf = nullptr;

  // initialization flags
  bool m_modelIsInitialized = false;
  bool m_linearProblemIsInitialized = false;
  bool m_eigenProblemIsInitialized = false;
  bool m_meshIsRead = false;
  bool m_meshIsWritten = false;
  bool m_initializeCVGraphInterface = false;
  bool m_callPetscInitialize = false;

  // Callback maps
  MapCallbackXYZ  m_mapCallbackXYZ;
  MapCallbackXYZT m_mapCallbackXYZT;

  ///Initialization vectors.
  PetscVector m_U_0;
  std::vector<SolutionBackup> m_solutionBackup;
  InitialCondition m_initialCondition;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  #ifdef FELISCE_WITH_CVGRAPH
  void rhsWithoutBC_zeroEntries();
  void rhsWithoutBC_restore();
  #endif

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  #ifdef FELISCE_WITH_CVGRAPH
  PetscVector m_rhsWithForceTermWithoutBC;
  bool m_rhsWithoutBC_zeroed;
  #endif

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  void checkConnectionVariablesMeshs();

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

///@}

} // namespace felisce

#endif
