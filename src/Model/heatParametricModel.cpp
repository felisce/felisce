//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/heatParametricModel.hpp"

namespace felisce {
  HeatParametricModel::HeatParametricModel():Model(),
    Kparameter(1.),
    RHSparameter(1.) {
    m_name = "HeatParametric";
  }

  HeatParametricModel::~HeatParametricModel()
  = default;

  void HeatParametricModel::initializeDerivedModel() {
    Kparameter = FelisceParam::instance().Kparameter;
    RHSparameter = FelisceParam::instance().RHSparameter;
  }

  void HeatParametricModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    IGNORE_UNUSED_ARGUMENT(iProblem)

    // First assembly loop in iteration 0 to build static matrix.
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc());

    // save static matrix in matrix _A and the F std::vector.
    m_linearProblem[0]->copyMatrixRHS();
  }


  void HeatParametricModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    IGNORE_UNUSED_ARGUMENT(iProblem)

    // complete matrix of the system
    m_linearProblem[0]->addScaleMatrix(Kparameter);
    m_linearProblem[0]->addScaleRHS(RHSparameter);
  }

  void HeatParametricModel::forward() {
    PetscPrintf(MpiInfo::petscComm(),"\n\n\tTimestep: %d \n\n", m_fstransient->iteration);

    //Assembly llop on elements.
    m_linearProblem[0]->assembleMatrixRHS(MpiInfo::rankProc());

    //Specific operations before solve the system.
    postAssemblingMatrixRHS();

    //Apply boundary conditions.
    m_linearProblem[0]->finalizeEssBCTransient();
    m_linearProblem[0]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc());

    //Solve linear system.
    m_linearProblem[0]->solve(MpiInfo::rankProc(), MpiInfo::numProc());

    //Advance time step.
    updateTime();

    //Write solution with ensight.
    writeSolution();
  }

  int HeatParametricModel::getNstate() const {
    return m_linearProblem[0]->numDof()+1;
  }

  void HeatParametricModel::getState(double* & state) {
    double * solution;
    m_linearProblem[0]->getSolution(solution, MpiInfo::numProc(), MpiInfo::rankProc());
    state = new double[getNstate()];

    for (int i = 0; i < getNstate() - 1; i++ )
      state[i] = solution[i];
    state[getNstate() - 1] = RHSparameter;

    /*for (int i = 0; i < getNstate(); i++)
     std::cout << state[i] << std::endl;
     std::cout << std::endl << std::endl;*/
  }

  void HeatParametricModel::setState(double* & state) {
    m_linearProblem[0]->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
    RHSparameter = state[getNstate() - 1];
  }
}
