//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _ALPDEIMMODEL_HPP
#define _ALPDEIMMODEL_HPP

// System includes

// External includes
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "Model/ALPModel.hpp"
#include "Solver/eigenProblem.hpp"
#include "Solver/eigenProblemALPDeim.hpp"
#include "Solver/cardiacFunction.hpp"

namespace felisce {
  /*!
   \file ALPDeimModel.hpp
   \authors E. Schenone
   \date 10/12/2012
   \brief ALPDeim model class manages Approximated Lax Pairs decomposition with DEIM method.
   */
  class ALPDeimModel:
    public ALPModel {
  public:
    //! Construtor.
    ALPDeimModel();
    //! Destructor.
    ~ALPDeimModel() override = default;
    //! Initializations
    void preAssembleMatrix(const int iProblem) override;
    //! Manage time iteration.
    void forward() override;
    void writeSolution() override;
  protected:
    std::vector<EigenProblemALPDeim*> m_eigenProblemDeim;
  };
}


#endif
