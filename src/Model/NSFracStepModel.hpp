//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _NSFracStepModel_HPP
#define _NSFracStepModel_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/lumpedModelBC.hpp"
#include "Solver/bdf.hpp"

namespace felisce {
  class NSFracStepModel:
    public Model {
  public:
    /// Construtor.
    NSFracStepModel();
    /// Destructor.
    ~NSFracStepModel() override;
    void initializeDerivedModel() override;
    void setExternalVec() override;
    void postAssemblingMatrixRHS(std::size_t iProblem=0) override;
    void preAssemblingMatrixRHS(std::size_t iProblem=0) override;

    ///  Manage time iteration.
    void forward() override;
    
    void solveProjectionStep(bool linearFlag);

    ///  Function to get size of the state std::vector.
    int getNstate() const override;
    ///  Function to get state std::vector.
    void getState(double* & state) override;
    felInt numDof_swig();
    void getState_swig(double* data, felInt numDof);
    ///  Function to std::set state std::vector.
    void setState(double* & state) override;

    ///  Access functions:
    inline const Bdf & bdf() const {
      return m_bdfFS;
    }
    inline Bdf & bdf() {
      return m_bdfFS;
    }

    ///  for FSI coupling
    PetscVector& seqResidual();
    PetscVector& seqViscousResidual();
    PetscVector& residual();
    PetscVector& lumpedMassRN();
    int& hasLumpedMassRN();

  private:

    ///  ALE
    void computeALEterms();
    void UpdateMeshByVelocity();
    void UpdateMeshByDisplacement();
    void solveHarmExt();
    void updateMesh();

    ///  FSI
    void computeFluidLoad();
    void computePreStressedDispMeshVector();

    ///  Incremental scheme
    void computePressure();

    ///  Lumped model:
    LumpedModelBC* m_lumpedModelBC;
    std::vector<int> m_labelFluxListLumpedModelBC;
    std::vector<double> m_fluxLumpedModelBC;

    ///  Time scheme.
    Bdf m_bdfFS;
    PetscVector  m_solExtrapolate;

    ///  ALE
    PetscScalar m_tau;
    PetscVector m_dispMesh;
    PetscVector m_dispMesho_pp;
    PetscVector m_velMesh;
    PetscVector m_beta;
    PetscVector m_solDispl;

    std::vector<PetscScalar> m_dispMeshVector;
    std::vector<PetscScalar> m_preStressedDispMeshVector;
    int m_numDofExtensionProblem;

    // with imposed displacements
    std::vector<PetscScalar> m_tmpDisp;
    PetscScalar m_displTimeStep;
    int m_cycl;

    // for fsi coupling
    bool m_matrixWithoutBCcreated;
    std::vector<PetscInt> m_petscToGlobal;

    PetscVector m_residual;
    PetscVector m_seqResidual;
    PetscVector m_viscousResidual;
    PetscVector m_seqViscousResidual;
    PetscVector m_seqPressLoad;
    PetscVector m_rhsWihoutBC;
    PetscVector m_rhsPressWihoutBC;
    PetscMatrix m_matrixWithoutBC;

    int m_hasLumpedMassRN;
    PetscVector m_lumpedMassRN;

    // for incremental scheme
    PetscVector m_prevPress;
    
  };
}

#endif
