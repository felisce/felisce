//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Model/NSModel.hpp"
#include "Solver/CVGraphInterface.hpp"
#include "Solver/linearProblemNS.hpp"
#include "Solver/linearProblemHarmonicExtension.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif

namespace felisce {

NSModel::NSModel():Model() 
{
  m_lumpedModelBC = nullptr;
  m_risModel = nullptr;
  m_name = "NS";
  m_matrixWithoutBCcreated = false;
  m_CrankNicolsonNS =nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

NSModel::~NSModel() 
{
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Write displacement and normal
  if ( r_instance.useFDlagrangeMult ) {
    
    // Modify pressure
    m_pSolverNS->computeRealPressure();

    const int ivar  = m_pSolverNS->listVariable().getVariableIdList(lagMultiplier);
    const int imesh = m_pSolverNS->listVariable()[ivar].idMesh();
    writeCurrentDisplacement(imesh, "displacement");
    writeCurrentNormalVector(imesh, "normal");

    if ( r_instance.useFicItf ) {
      // writeCurrentDisplacement(2, "dummydisp"); // TODO D.C.
      writeCurrentNormalVector(2, "dummynormal");
    }
  }
    
  if ( m_lumpedModelBC )
    delete m_lumpedModelBC;
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::setInitialCondition() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( r_instance.hasInitialCondition && r_instance.useALEformulation ) {

    // TODO D.C. only in case of ALE we can have an initial condition??? i don't think so...
    int ivar  = m_pSolverHE->listVariable().getVariableIdList(displacement);
    int iunk  = m_pSolverHE->listUnknown().getUnknownIdList(displacement);
    int imesh = m_pSolverHE->listVariable()[ivar].idMesh();

    // displacement
    int numDof;
    if( r_instance.FusionDof ) 
      numDof = m_pSolverHE->supportDofUnknown(iunk).listNode().size() * m_pSolverHE->meshLocal(imesh)->numCoor();
    else
      numDof = m_pSolverHE->numDof();

    // displacement
    std::vector<double> vector( numDof, 0.);
    m_ios[imesh]->readVariable(2, 0.0, vector.data(), numDof);

    if( r_instance.FusionDof ) {
      int numCompDisp  = m_pSolverHE->dimension();
      int numCoorMesh  = m_pSolverHE->meshLocal(imesh)->numCoor();
      int numNodes = m_pSolverHE->supportDofUnknown(iunk).listNode().size();
      int idof;
      std::vector<double> aux(vector);
      for (int ip = 0; ip < numNodes; ip++) {
        idof = m_pSolverHE->supportDofUnknown(iunk).listPerm()[ip];
        for (felInt ic=0; ic < numCompDisp; ++ic)
          vector[idof*numCompDisp+ic] = aux[ip*numCoorMesh+ic];
      }
    }

    int low, high;
    VecGetOwnershipRange(m_pSolverHE->solution().toPetsc(),&low,&high);

    int localSize = high-low;
    std::vector<int> ix(localSize);
    std::vector<double> vv(localSize);
    std::iota(ix.begin(),ix.end(),low); // fills ix with values from low to high

    AOPetscToApplication(m_pSolverHE->ao(),localSize,ix.data());
    for (int i=0; i<localSize;++i)
      vv[i] = vector[ix[i]];
    AOApplicationToPetsc(m_pSolverHE->ao(),localSize,ix.data());

    m_pSolverHE->solution().setValues(localSize,ix.data(),vv.data(),INSERT_VALUES);
    m_pSolverHE->solution().assembly();
    m_pSolverHE->gatherSolution();
    updateMesh();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::initializeDerivedModel() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  //Error message if method of characteristics is used in parallel, to be removed when this method is implemented in parallel
  if (r_instance.NSequationFlag == 1 && r_instance.characteristicMethod != 0) {
    if (MpiInfo::numProc() > 1)
      FEL_ERROR("Method of characteristics has not been implemented yet in parallel!");
  }

  m_pSolverNS = static_cast<LinearProblemNS*>(m_linearProblem[0]);
  if ( r_instance.useALEformulation  )
    m_pSolverHE = static_cast<LinearProblemHarmonicExtension*>(m_linearProblem[1]);

  if (m_initializeCVGraphInterface) {
    m_cvgraphInterf = felisce::make_shared<CVGraphInterface>();
    m_cvgraphInterf->initialize(&m_pSolverNS->listVariable(), r_instance.CVGraphInterface);
    m_cvgraphInterf->identifyIdDof(m_pSolverNS->dof());
    m_pSolverNS->cvgraphInterf() = m_cvgraphInterf;
  }

  if ( r_instance.lumpedModelBCLabel.size() ) {
    m_lumpedModelBC = new LumpedModelBC(m_fstransient);
    m_pSolverNS->initializeLumpedModelBC(m_lumpedModelBC);
  }

  if ( r_instance.RISModels ) {
    m_risModel = new RISModel (m_fstransient, m_linearProblem) ;
    m_pSolverNS->initializeRISModel(m_risModel);
    m_chek_use_RISforward = false;
  }
  
  if ( r_instance.CardiacCycle ) {
    m_cardiacCycle = new CardiacCycle (m_fstransient, m_risModel, m_linearProblem) ;
    m_pSolverNS->initializeCardiacCycle(m_cardiacCycle);
  }

  if ( r_instance.hasImmersedData ) {
    m_immersed.initialize(m_pSolverNS->mesh());
  }

  // monolithic fsi
  // m_disp.duplicateFrom(m_pSolverNS->vector());
  // m_disp.zeroEntries();

  if ( r_instance.useALEformulation ) {
    const int ivar  = m_pSolverHE->listVariable().getVariableIdList(displacement);
    const int iunk  = m_pSolverHE->listUnknown().getUnknownIdList(displacement);
    const int iMesh = m_pSolverHE->listVariable()[ivar].idMesh();

    m_numDofExtensionProblem = m_pSolverHE->numDof();
    m_dispMeshVector.resize( m_pSolverHE->supportDofUnknown(iunk).listNode().size() * m_pSolverHE->meshLocal(iMesh)->numCoor() );
    m_petscToGlobal.resize(m_numDofExtensionProblem);
    if ( r_instance.readPreStressedDisplFromFile )
      m_preStressedDispMeshVector.resize(m_numDofExtensionProblem);
    for (int ipg=0; ipg< m_numDofExtensionProblem; ipg++)
      m_petscToGlobal[ipg] = ipg;

    AOApplicationToPetsc(m_pSolverHE->ao(),m_numDofExtensionProblem,m_petscToGlobal.data());

    if ( r_instance.readDisplFromFile ||  r_instance.readPreStressedDisplFromFile ) {
      // defined in linearProblemHarmonicExtention
      m_pSolverHE->readMatchFile_ALE(m_pSolverHE->listVariable(), r_instance.MoveMeshMatchFile);
      m_pSolverHE->identifyIdDofToMatch_ALE(m_pSolverHE->dof());

      if (r_instance.hasInitialCondition ||  r_instance.readPreStressedDisplFromFile ) {
        PetscPrintf(MpiInfo::petscComm(),"\nCycle 1, Read data displacement at time t = 0.00000\n");
        m_pSolverHE->readDataDisplacement(m_ios, 0.0);
      }
    }
  }

  if ( r_instance.orderBdfNS > 2 )
    FEL_ERROR("BDF not yet implemented for order greater than 2 with Navier-Stokes.");

  m_bdfNS.defineOrder(r_instance.orderBdfNS);
  m_pSolverNS->initializeTimeScheme(&m_bdfNS);

  // Crank-Nicolson time marching
  // Manage midpoint approximation for the advection term
  // TODO D.C. does it work???
  if ( Tools::equal(r_instance.theta,0.5) ) {
    m_CrankNicolsonNS = new Bdf;
    m_CrankNicolsonNS->defineOrder(2);
    // Second order approximation of the midpoint
    m_CrankNicolsonNS->beta(0) = 3./2;
    m_CrankNicolsonNS->beta(1) = -1./2;
  }

  // Initialization of residual stuff
  if ( r_instance.withCVG && !r_instance.fsiCoupling ) {
    m_rhsWihoutBC.duplicateFrom(m_pSolverNS->vector());
    m_residual.duplicateFrom(m_pSolverNS->vector());
    m_seqResidual.duplicateFrom(m_pSolverNS->sequentialSolution());
  }

  if ( r_instance.fsiCoupling ) {
    // TODO: we can use the functions createAndCopyMatrixRHSWithoutBC and computeResidual in LinearProblem
    m_rhsWihoutBC.duplicateFrom(m_pSolverNS->vector());
    m_residual.duplicateFrom(m_pSolverNS->vector());
    m_lumpedMassRN.duplicateFrom(m_pSolverNS->vector());
    m_seqResidual.duplicateFrom(m_pSolverNS->sequentialSolution());
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::setExternalVec() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  //=========
  //   ALE
  //=========
  if ( r_instance.useALEformulation ) {
    //1) Initialization of the vector containing the displacement of the mesh d^{n}, sequential std::vector
    m_dispMesh.duplicateFrom(m_pSolverHE->sequentialSolution());
    m_dispMesh.zeroEntries();

    // fsi challenge
    if ( r_instance.useElasticExtension )  {
      m_dispMesho_pp.duplicateFrom(m_pSolverHE->solution());
      m_dispMesho_pp.zeroEntries();
    }

    if ( Tools::equal(r_instance.theta,0.5) ) {
      m_dispMesho.duplicateFrom(m_pSolverHE->sequentialSolution());
      m_dispMesho.zeroEntries();
    }

    //2) Initialization of the std::vector containing the displacement of the mesh d^{n-1}, parallel std::vector
    // m_solDispl == vectors of dispalcement at every time step (n>1)
    // m_solDispl_0 == vectors of dispalcement at first time step (n==1)
    // Two vectors of displacements are necessary because when a
    // cardiac cycle re-start we need to read the std::vector of
    // displacement at time 0, which is stored in m_solDispl_0
    m_solDispl.duplicateFrom(m_pSolverHE->solution());
    m_solDispl_0.duplicateFrom(m_pSolverHE->solution());
    m_solDispl_0.zeroEntries();
    m_solDispl.zeroEntries();

    //3) Initialization of the vector containing the velocity of the mesh w, sequential vector
    m_velMesh.duplicateFrom(m_pSolverHE->sequentialSolution());
    m_velMesh.zeroEntries();

    // Passing the ao and dof
    m_pSolverNS->pushBackExternalAO(m_pSolverHE->ao());
    m_pSolverNS->pushBackExternalDof(m_pSolverHE->dof());
    // Passing the velocity of the mesh to the NS problem
    m_pSolverNS->pushBackExternalVec(m_velMesh);  //externalVec(0) in m_linearProblem[0]
    // Resizing the auxiliar vectors needed in updateMesh()
    m_tmpDisp.resize(m_numDofExtensionProblem);

    m_displTimeStep = 0.;

    if (r_instance.nonLinearFluid) {
      // create a sequential PetscVector whose size is equal to the total number of fluid elements
      m_solPrev.duplicateFrom(m_pSolverNS->solution());
      m_solPrev.zeroEntries();
      // push it back to the sets of linear problems
      m_pSolverNS->pushBackExternalVec(m_solPrev);
    }
  }

  if ( r_instance.hasImmersedData && r_instance.hasDivDivStab ) {

    // create a sequential PetscVector whose size is equal to the total number of fluid elements
    int ivar  = m_pSolverNS->listVariable().getVariableIdList(velocity);
    int imesh = m_pSolverNS->listVariable()[ivar].idMesh();

    m_delta.createSeq(PETSC_COMM_SELF, m_pSolverNS->mesh(imesh)->getNumDomainElement() );
    m_delta.set(0.);

    // push it back to the sets of linear problems
    m_pSolverNS->pushBackExternalVec(m_delta); // TODO D.C. not really smart to do a pushBack in vector since the order may change depending on the problem we solve... mayba a map is better...
  }

  // for monolithic fsi
  // m_dispSeq is a sequential PetscVector whose size is equal to the total number of fluid elements
  // m_dispSeq.createSeq(PETSC_COMM_SELF, m_pSolverNS->numDof() );
  // m_dispSeq.set(0.);
  // m_pSolverNS->pushBackExternalVec(m_dispSeq); //externalVec(0) in m_pSolverNS or 1 if ALE
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::preAssemblingMatrixRHS(std::size_t iProblem) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( iProblem == 0 ) {
    m_solExtrapolate.duplicateFrom(m_pSolverNS->vector());
    m_solExtrapolate.zeroEntries();

    m_pSolverNS->initExtrapol(m_solExtrapolate);

    m_pSolverNS->solution().zeroEntries();

    //! First assembly loop in iteration 0 to build static matrix.
    if ( r_instance.useALEformulation == 0 ) {

      if ( r_instance.NSStabType == 1 )
        m_pSolverNS->assembleFaceOrientedStabilization();

      m_pSolverNS->assembleMatrixRHS(MpiInfo::rankProc());
    }
  }

  //=========
  //   ALE
  //=========
  if ( r_instance.useALEformulation ) {
    if ( (iProblem == 1) && (!r_instance.useElasticExtension) ) {
      // First assembly loop in iteration 0 to build static matrix of the extension problem
      m_pSolverHE->assembleMatrixRHS(MpiInfo::rankProc());
      // Save the static part of the extension problem in the matrix _A
      m_pSolverHE->copyMatrixRHS();
    }
    if ( (r_instance.useALEformulation == 2) && (iProblem == 0) ) {
      // assembly loop to put values at the locations of the complementary pattern
      if ( r_instance.NSStabType == 1 )
        m_pSolverNS->assembleFaceOrientedStabilization();

      // First assembly loop in iteration 0 to build the initial mass matrix (initial configuration)
      m_pSolverNS->assembleMatrixRHS(MpiInfo::rankProc());
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::postAssemblingMatrixRHS(std::size_t iProblem) 
{ 
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // matrix(0) = matrix(0) + matrix(1) (add static matrix to the dynamic matrix to build
  // complete matrix of the system )
  m_linearProblem[iProblem]->addMatrixRHS();
  
  if ( iProblem == 0 && r_instance.withCVG )
    m_pSolverNS->createAndCopyMatrixRHSWithoutBC();
  
  //Apply CVGraphInterface condition directly in RHS
  if ( iProblem == 0 && r_instance.CVGraphInterfaceVariationalBC == 1 ) {
    m_pSolverNS->vector().setValues(m_cvgraphInterf->SizeListOfDofForVariable(),
        m_cvgraphInterf->listOfDofInInterface(), m_cvgraphInterf->ComputedVariableToImpose(), ADD_VALUES);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::RISforward()
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  m_chek_use_RISforward = true;

  if ( m_risModel->admissibleStatus() == 1 ) {
    writeSolution();
    if ( m_lumpedModelBC && MpiInfo::rankProc() == 0 )
      m_lumpedModelBC->write(r_instance.resultDir+"/output_windkessel");

    if ( MpiInfo::rankProc() == 0 ) 
      m_risModel->write(r_instance.resultDir+"/outputValves");

    // Advance time step.
    updateTime();

    if ( r_instance.useALEformulation  == 2 ) {

      if ( m_fstransient->iteration == 1 ) {
        //create m_storedMatrix with the same structure of matrix(1)
        m_storedMatrix.duplicateFrom(m_pSolverNS->matrix(1), MAT_COPY_VALUES);
      }

      if ( m_fstransient->iteration > 1 ) {
        // add Matrix(1) to matrix(0)
        m_pSolverNS->matrix(0).axpy(1,m_pSolverNS->matrix(1),SAME_NONZERO_PATTERN);
        // copy matrix(1) in m_storedMatrix to use it in the NS recomputation
        m_storedMatrix.copyFrom(m_pSolverNS->matrix(1), SAME_NONZERO_PATTERN);
        //clear matrix(1) to be updated in the forward()
        m_pSolverNS->clearMatrix(1);
      }
    }

    // This is usefeul to read data_displacements
    m_displTimeStep += m_fstransient->timeStep;
    // Print time information
    printNewTimeIterationBanner();

    m_risModel->UpdateResistances();

    if(r_instance.CardiacCycle)
      m_cardiacCycle->ApplyCardiacCycleInput();

    forward();
    m_risModel->CheckValveStatus();
  }

  while( m_risModel->admissibleStatus() == 0 ) { // This "while" is important, an "if" does not work in all cases
    PetscPrintf(MpiInfo::petscComm(),"\n Recomputation with the new RIS status\n");

    for(std::size_t ilr=0; ilr<m_linearProblem.size(); ilr++) {
      m_linearProblem[ilr]->clearMatrixRHS(FlagMatrixRHS::matrix_and_rhs); // 0 is flagMatrixRHS clear matrix(0) and RHS
    }

    if( (r_instance.useALEformulation  == 2) && ( m_fstransient->iteration > 1 ) ) {
      // we recomputed the matrix(0) at t^{n} and we add m_storedMatrix == matrix(1) a t^{n-1}
      m_pSolverNS->matrix(0).axpy(1,m_storedMatrix,SAME_NONZERO_PATTERN);
      m_storedMatrix.setUnfactored();
      m_storedMatrix.zeroEntries();
    }

    m_risModel->UpdateResistances();

    if( r_instance.CardiacCycle )
      m_cardiacCycle->ApplyCardiacCycleInput();

    forward();
    m_risModel->CheckValveStatus();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::forward() 
{
  prepareForward();
  solveNS();
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::prepareForward() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  //  Initial displacement for ALE
  if ( r_instance.initALEDispByFile && (m_fstransient->iteration == 0  ) )  {

    m_pSolverHE->readMatchFile_ALE(m_pSolverHE->listVariable(), r_instance.MoveMeshMatchFile);
    m_pSolverHE->identifyIdDofToMatch_ALE(m_pSolverHE->dof());
    m_pSolverHE->readDataDisplacement(m_ios, 0.0);
    int low, high;
    VecGetOwnershipRange(m_pSolverHE->solution().toPetsc(),&low,&high);
    int size = high-low;
    std::vector<double> vv(size);
    std::vector<int> ix(size);
    std::iota(ix.begin(),ix.end(),low); // fills ix with values from low to high

    VecGetValues(m_pSolverHE->HarmExtSol().toPetsc(),size,ix.data(),vv.data());
    
    VecSetValues(m_pSolverHE->solution().toPetsc(),size,ix.data(),vv.data(), INSERT_VALUES);
    m_pSolverHE->solution().assembly();
    m_pSolverHE->gatherSolution();
    updateMesh();
  }

  if ( !r_instance.RISModels ) {

    if ( r_instance.useFDlagrangeMult ) {

      // Compute the displacements of the interface meshes
      m_pSolverNS->computeItfDisp(*m_itfDisp);

      // Move the interface meshes
      m_pSolverNS->moveItfMeshes();

      // Write displacement and normal
      const int ivar  = m_pSolverNS->listVariable().getVariableIdList(lagMultiplier);
      const int imesh = m_pSolverNS->listVariable()[ivar].idMesh();
      writeCurrentDisplacement(imesh, "displacement");
      writeCurrentNormalVector(imesh, "normal");

      // if ( r_instance.useFicItf ) {
        // writeCurrentDisplacement(2, "dummydisp");
        // writeCurrentNormalVector(2, "dummynormal");
      // }
    
      // Modify pressure
      if ( r_instance.massConstraint )
        m_pSolverNS->computeRealPressure();
    }

    // Write solution for postprocessing (if required)
    writeSolution();
    if ( m_lumpedModelBC ) {
      if ( MpiInfo::rankProc() == 0 ) 
        m_lumpedModelBC->write(r_instance.resultDir+"/output_windkessel");
    }

    updateTime(); //Here the matrix(0), the non constant one, is cleared and the rhs also.

    if( (r_instance.useALEformulation == 2) && ( m_fstransient->iteration > 1 ) ) {
      m_pSolverNS->matrix(0).axpy(1,m_pSolverNS->matrix(1),SAME_NONZERO_PATTERN);
      m_pSolverNS->clearMatrix(1);
    }

    // Print time information
    printNewTimeIterationBanner();

    // This is usefeul to read data_displacements
    m_displTimeStep += m_fstransient->timeStep;
  }
  else if ( /*r_instance.RISModels &&*/ m_chek_use_RISforward == false ) {

    FEL_ERROR("Your are not using the corret forward for valve implementation !")
  }

  m_pSolverNS->onPreviousMesh();

  // Function only at iteration == 1
  firstIteration();

  if ( m_fstransient->iteration > 1 ) {
    m_bdfNS.update(m_pSolverNS->solution());
    m_bdfNS.extrapolate(m_solExtrapolate);
    m_pSolverNS->updateExtrapol(m_solExtrapolate);

    if ( Tools::equal(r_instance.theta,0.5) ) {
      m_CrankNicolsonNS->update(m_pSolverNS->solution());
      m_CrankNicolsonNS->extrapolate(m_solExtrapolate);
      m_pSolverNS->updateExtrapol(m_solExtrapolate);
    }
  }

  // If lumpedModel bc computation
  computeLumpedModelBC();

  m_bdfNS.computeRHSTime(m_fstransient->timeStep);
  m_pSolverNS->gatherVectorBeforeAssembleMatrixRHS();

  // If fictitious domain + penalization update interface mesh
  if ( r_instance.hasImmersedData ) {
    GeometricMeshRegion& mesh = *m_pSolverNS->mesh();
    m_immersed.updateInterface(mesh); // TODO D.C. make more general

    if ( r_instance.hasDivDivStab ) {
      // call updateIntersection of the model "Immersed" in order to set the values of the PetscVector m_delta for the main process and to broadcast it to the other processes
      m_immersed.updateIntersection(mesh, m_delta, MpiInfo::petscComm());
    }
  }

  // If fictitious domain with lagrange multiplier
  if ( r_instance.useFDlagrangeMult ) {

    // Localize interface quadrature point in fluid mesh and evaluate new pattern
    m_pSolverNS->computeSolidFluidMaps();
    m_pSolverNS->evaluateFluidStructurePattern();

    m_pSolverNS->computeLagrangeMultiplierOnInterface(FlagMatrixRHS::only_matrix);

    if ( r_instance.BHstab )
      m_pSolverNS->assembleBarbosaHughesStab();

    if ( r_instance.BPstab )
      m_pSolverNS->assembleBrezziPitkarantaStab();

    if ( r_instance.massConstraint ) {
      m_pSolverNS->assembleJapanConstraintBoundary();
      m_pSolverNS->assembleJapanConstraintInterface();
    }
  }

  if ( r_instance.useALEformulation ) {
    
    computeALEterms();
  } else {

    if ( r_instance.NSStabType == 1 )
      m_pSolverNS->assembleFaceOrientedStabilization();
    
    m_pSolverNS->assembleMatrixRHS(MpiInfo::rankProc());
  }

  // TODO: we can use the functions createAndCopyMatrixRHSWithoutBC and computeResidual in LinearProblem
  if ( r_instance.fsiCoupling )
    m_rhsWihoutBC.copyFrom(m_pSolverNS->vector());

  // Specific operations before solve the system.
  postAssemblingMatrixRHS(0);

  // TODO: we can use the functions createAndCopyMatrixRHSWithoutBC and computeResidual in LinearProblem
  if (  r_instance.fsiCoupling && !r_instance.useFDlagrangeMult )  {
    if ( m_matrixWithoutBCcreated == 0 ) {
      m_matrixWithoutBC.duplicateFrom(m_pSolverNS->matrix(),MAT_COPY_VALUES); 
      m_matrixWithoutBCcreated = true;
    }
    m_matrixWithoutBC.copyFrom(m_pSolverNS->matrix(), SAME_NONZERO_PATTERN);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::solveNS()
{
  const auto& r_instance = FelisceParam::instance(instanceIndex());

  const int ivar  = m_pSolverNS->listVariable().getVariableIdList(velocity);
  const int imesh  = m_pSolverNS->listVariable()[ivar].idMesh();
  
  if ( r_instance.fsiCoupling && !r_instance.hasImmersedData && !r_instance.useFDlagrangeMult ) {
    if ( m_fstransient->iteration == 1  ) {
      if ( m_hasLumpedMassRN ) {
        PetscScalar dti = 1./m_fstransient->timeStep;
        m_lumpedMassRN.scale(dti);
      }
    }

    if ( r_instance.bcInRefConfiguration && !m_hasLumpedMassRN ) {
      m_pSolverNS->meshLocal(imesh)->moveMesh(m_dispMeshVector,0.0);
    }
    if ( r_instance.bcInRefConfiguration && r_instance.readPreStressedDisplFromFile ) {
      m_pSolverNS->meshLocal(imesh)->moveMesh(m_preStressedDispMeshVector,1.0);
    }
  } else if ( r_instance.useALEformulation && r_instance.bcInRefConfiguration && r_instance.NSequationFlag != 0 ) {
    m_pSolverNS->meshLocal(imesh)->moveMesh(m_dispMeshVector,0.0);
  }

  if ( FelisceParam::verbose() ) {
    if ( r_instance.nonLinearFluid )
      PetscPrintf(MpiInfo::petscComm(),"NS non-linear problem \n");
    else
      PetscPrintf(MpiInfo::petscComm(),"NS linear problem \n");
    }
  
  if ( r_instance.hasImmersedData ) {
    AO& ao = m_pSolverNS->ao();
    PetscMatrix& matrix = m_pSolverNS->matrix(0);
    PetscVector& rhs = m_pSolverNS->vector();
    Dof& dof = m_pSolverNS->dof();
    int idVarVel = m_pSolverNS->idVarVel();
    m_immersed.applyPenaltyToMatrixAndRHS(ao,dof,idVarVel,matrix,rhs);
  }

  if ( r_instance.useFDlagrangeMult)
    m_pSolverNS->computeLagrangeMultiplierOnInterface(FlagMatrixRHS::only_rhs);

  if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");

  //Apply essential transient boundary conditions.
  m_pSolverNS->finalizeEssBCTransient();

  #ifdef FELISCE_WITH_CVGRAPH
    if ( !r_instance.withCVG || ! m_pSolverNS->slave()->redoTimeStepOnlyCVGData() ) {
      m_pSolverNS->applyBC(r_instance.essentialBoundaryConditionsMethod, MpiInfo::rankProc());
  } else {
      m_pSolverNS->applyOnlyCVGBoundaryCondition();
  }
  #else
    m_pSolverNS->applyBC(r_instance.essentialBoundaryConditionsMethod, MpiInfo::rankProc());
  #endif
  
  if ( r_instance.fsiCoupling ) {

    if ( r_instance.useLumpedMassRN ) {
      m_pSolverNS->matrix().diagonalSet(m_lumpedMassRN,ADD_VALUES);
     }
    if ( r_instance.bcInRefConfiguration && r_instance.readPreStressedDisplFromFile ) {
      m_pSolverNS->meshLocal(imesh)->moveMesh(m_preStressedDispMeshVector,-1.0);
    }
    if ( r_instance.bcInRefConfiguration && !m_hasLumpedMassRN ) {
      m_pSolverNS->meshLocal(imesh)->moveMesh(m_dispMeshVector,1.0);
     }

    m_pSolverNS->kspInterface().setDiagonalScale();
  } 
  else if ( r_instance.useALEformulation && r_instance.bcInRefConfiguration && r_instance.NSequationFlag != 0 ) {
    m_pSolverNS->meshLocal(imesh)->moveMesh(m_dispMeshVector,1.0);
  }

  if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Solving NS system \n");

  if ( r_instance.nonLinearFluid ) {
    // solve non Linear system
    m_pSolverNS->iterativeSolve(MpiInfo::rankProc(), MpiInfo::numProc(), ApplyNaturalBoundaryConditions::no, FlagMatrixRHS::matrix_and_rhs);
  } else {
    // solve Linear system
    m_pSolverNS->solve(MpiInfo::rankProc(), MpiInfo::numProc());
  }

  if ( r_instance.withCVG ) {
    m_pSolverNS->computeResidual();
  }

  m_pSolverNS->gatherSolution();

  if ( r_instance.fsiCoupling ) {

    if ( FelisceParam::verbose() )
      PetscPrintf(MpiInfo::petscComm(),"Evaluating residual \n");

    // TODO: we can use the functions createAndCopyMatrixRHSWithoutBC and computeResidual in LinearProblemNS
    // Compute residual
    if ( r_instance.useFDlagrangeMult ) {
      m_pSolverNS->computeInterfaceStress();
    } else if ( r_instance.hasImmersedData ) {
      AO& ao = m_pSolverNS->ao();
      PetscVector& seqVel = m_pSolverNS->sequentialSolution();
      Dof& dof = m_pSolverNS->dof();
      int idVarVel = m_pSolverNS->idVarVel();
      m_immersed.computePenaltyForce(ao,dof,idVarVel,seqVel);
    } else {
      m_matrixWithoutBC.scale(-1.);
      if ( r_instance.nonLinearFluid )
        m_rhsWihoutBC.scale(-1.);
      
      multAdd(m_matrixWithoutBC,m_pSolverNS->solution(),m_rhsWihoutBC,m_residual);

      m_pSolverNS->gatherVector(m_residual, m_seqResidual);
      m_matrixWithoutBC.scale(-1.);
      if ( r_instance.nonLinearFluid )
        m_rhsWihoutBC.scale(-1.);
    }
  }

  // (interface) displacement update, monolthic FSI
  // d^n = d^{n-1} + \tau * u^n
  // m_disp.axpy(m_fstransient->timeStep,m_pSolverNS->solution());
  // m_pSolverNS->gatherVector(m_disp, m_dispSeq);
  // m_pSolverNS->gatherSolution();  
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::solveLinearizedNS(bool linearFlag) 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());
  
  m_pSolverNS->linearizedFlag() = linearFlag;

  // back to original matrix
  m_pSolverNS->kspInterface().setDiagonalScaleFix();

  if ( linearFlag )
    m_pSolverNS->vector().zeroEntries();
  else
    m_pSolverNS->vector().copyFrom(m_rhsWihoutBC);

  if ( linearFlag && FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"NS linearized solver \n");
  else if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"NS solver \n");

  if ( r_instance.hasImmersedData ) {
    AO& ao = m_pSolverNS->ao();
    PetscMatrix& matrix = m_pSolverNS->matrix(0);
    PetscVector& rhs = m_pSolverNS->vector();
    Dof& dof = m_pSolverNS->dof();
    int idVarVel = m_pSolverNS->idVarVel();
    m_immersed.applyPenaltyToMatrixAndRHS(ao,dof,idVarVel,matrix,rhs,true);
  }

  if ( r_instance.useFDlagrangeMult) {
    m_pSolverNS->computeLagrangeMultiplierOnInterface(FlagMatrixRHS::only_rhs);
  }

  if (FelisceParam::verbose())
    PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");

  // Apply essential transient boundary conditions.
  m_pSolverNS->finalizeEssBCTransient();

  if ( linearFlag ) // only Dirichlet BC on the interface is applied (RHS)
    m_pSolverNS->applyBC(r_instance.essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs, 0, true, ApplyNaturalBoundaryConditions::no);
  else  // all BC applied (RHS)
    m_pSolverNS->applyBC(r_instance.essentialBoundaryConditionsMethod, MpiInfo::rankProc(), FlagMatrixRHS::only_rhs, FlagMatrixRHS::only_rhs, 0, true, ApplyNaturalBoundaryConditions::yes);

  m_pSolverNS->kspInterface().setDiagonalScale();

  if ( FelisceParam::verbose() ) {
    if ( linearFlag )
      PetscPrintf(MpiInfo::petscComm(),"Solving linearized NS system (same matrix) \n");
    else
      PetscPrintf(MpiInfo::petscComm(),"Solving NS system (same matrix) \n");
  }

  // Solve linear system with same matrix as in solveNS                                                                   
  m_pSolverNS->solveWithSameMatrix();

  m_pSolverNS->gatherSolution();
  
  if ( FelisceParam::verbose() )
    PetscPrintf(MpiInfo::petscComm(),"Evaluating residual \n");

  if ( r_instance.useFDlagrangeMult ) {
    m_pSolverNS->computeInterfaceStress();
  } else if ( r_instance.hasImmersedData ) {
    AO& ao = m_pSolverNS->ao();
    PetscVector& seqVel = m_pSolverNS->sequentialSolution();
    Dof& dof = m_pSolverNS->dof();
    int idVarVel = m_pSolverNS->idVarVel();
    m_immersed.computePenaltyForce(ao,dof,idVarVel,seqVel);
  } else {
    //Compute residual
    m_matrixWithoutBC.scale(-1.);
    if ( linearFlag ) {
      m_pSolverNS->vector().zeroEntries();
      multAdd(m_matrixWithoutBC,m_pSolverNS->solution(),m_pSolverNS->vector(),m_residual);
    }
    else
      multAdd(m_matrixWithoutBC,m_pSolverNS->solution(),m_rhsWihoutBC,m_residual);
    m_pSolverNS->gatherVector(m_residual, m_seqResidual);
    m_matrixWithoutBC.scale(-1.);
  }

  m_pSolverNS->linearizedFlag() = false;  
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::computeLumpedModelBC() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if (m_lumpedModelBC) {
    // update the fluxes
    m_pSolverNS->computeMeanQuantity(velocity, m_labelFluxListLumpedModelBC, m_fluxLumpedModelBC);

    for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
      m_lumpedModelBC->Q(i) = m_fluxLumpedModelBC[i] ;
    }

    // update compliance if non linear compliance
    if ( r_instance.lumpedModelBC_C_type == 2 ) {
      m_pSolverNS->updateVolume();
    }

    // iterate the lumpedModelBC parameters
    m_lumpedModelBC->iterate();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::firstIteration() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( m_fstransient->iteration == 1 ) {
    if ( r_instance.useALEformulation ) {
      m_tau = 1./(m_fstransient->timeStep);
      m_cycl = 1; // For movemesh case
    }

    if (m_lumpedModelBC) {
      // m_labelFluxListLumpedModelBC computation with LumpedModelBC labels
      m_labelFluxListLumpedModelBC.clear();
      for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
        m_labelFluxListLumpedModelBC.push_back(r_instance.lumpedModelBCLabel[i]);
      }
    }

    m_bdfNS.initialize(m_pSolverNS->solution());
    m_bdfNS.extrapolate(m_solExtrapolate);
    m_pSolverNS->initExtrapol(m_solExtrapolate);

    // Crank-Nicolson time marching
    if ( Tools::equal(r_instance.theta,0.5) ) {
      m_CrankNicolsonNS->initialize(m_pSolverNS->solution());
      m_CrankNicolsonNS->extrapolate(m_solExtrapolate);
      m_pSolverNS->updateExtrapol(m_solExtrapolate);
    }

    if ( r_instance.readPreStressedDisplFromFile  ) {
      computePreStressedDispMeshVector();
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

int NSModel::getNstate() const 
{
  return m_pSolverNS->numDof();
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::getState(double* & state) 
{
  m_pSolverNS->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
}

/***********************************************************************************/
/***********************************************************************************/

felInt NSModel::numDof_swig() 
{
  return m_pSolverNS->numDof();
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::getState_swig(double* data, felInt numDof) 
{
  double * state;
  m_pSolverNS->getSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
  for (felInt i=0 ;  i<numDof ; i++) {
    data[i] = state[i];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::setState(double* & state) 
{
  m_pSolverNS->setSolution(state, MpiInfo::numProc(), MpiInfo::rankProc());
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::computeALEterms() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  // Assembly the RHS (flagMatrixRHS = FlagMatrixRHS::only_rhs) in the previous mesh configuration:
  // \dfrac{\rho}{\dt} \int_{\Omega^{n-1}} u^{n-1} v +   \int_{\Omega^{n-1}} f v
  m_pSolverNS->onPreviousMesh();
  m_pSolverNS->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);

  //Save velocity previous mesh
  if ( r_instance.nonLinearFluid ) {
    m_solPrev.zeroEntries();
    m_solPrev.copyFrom(m_pSolverNS->vector());
  }

  //First distinguish the elastic extension and harmonic extension.
  if ( r_instance.useElasticExtension ) {
    UpdateMeshByDisplacement();
  }
  else{
    // Solve the extension problem and move the mesh of m_pSolverNS
    // according to the mesh displacement obtained
    if( r_instance.updateMeshByVelocity ) {
      // the HarmonicExtention unknown is velocity
      // dispalcement is computed analitycally
      UpdateMeshByVelocity();
    } else {
      // the HarmonicExtention unknown is displacement
      // velocity is computed analitycally
      UpdateMeshByDisplacement();
    }
  }

  m_pSolverNS->onCurrentMesh();

  // Assembly the Matrix (flagMatrixRHS = FlagMatrixRHS::only_matrix) in the current mesh configuration
  if ( r_instance.NSStabType == 1 )
    m_pSolverNS->assembleFaceOrientedStabilization();

  m_pSolverNS->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_matrix);
  m_pSolverNS->assembleMatrixRHS(MpiInfo::rankProc(), FlagMatrixRHS::only_rhs);
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::UpdateMeshByVelocity() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if (  r_instance.hasInitialCondition ) { // m_solDispl_0 = d^{0} != 0
    m_solDispl_0.copyFrom(m_pSolverHE->solution());
    //else m_solDispl_0 = d^{0} == 0 defined in ExternalVec()
  }

  if(m_fstransient->iteration>1 ) {
    m_solDispl.copyFrom(m_pSolverHE->solution());
  }

  felInt displIterMax = r_instance.timeMaxCaseFile; //  todo: it should be taken automatically form displacement  file.case

  if(m_fstransient->iteration == m_cycl * displIterMax +1) { // to read the file case in cycle
    m_displTimeStep = m_fstransient->timeStep ;
    m_cycl++;
  }
  PetscPrintf(MpiInfo::petscComm(),"Cycle %d, Read data at time t = %f\n", m_cycl, m_displTimeStep);
  m_pSolverHE->readDataDisplacement(m_ios, m_displTimeStep); // defined in linearProblemHarmonicExtention

  //Solve the extension problem
  solveHarmExt();

  // Store velocity and apply it to the NS problem. seqSolution is the velocity
  m_pSolverHE->gatherSolution();
  m_velMesh.copyFrom(m_pSolverHE->sequentialSolution());
  m_velMesh.scale(-1.);

  /*In which follow compute displacement: d^n = w^n*dt + d^{n-1}
    VecAXPBY(Vec y,PetscScalar a,PetscScalar b,Vec x);  -> y=a∗x+b∗y
    solution() == w^n
    m_solDispl == d^{n-1}
    when a cardiac cycle re-start we need to initilize the displacement(t==0)
    because the displacement(0) != dispalcement(timeMaxCaseFile),
    where timeMaxCaseFile is last time step*/
  if(m_fstransient->iteration == m_cycl * displIterMax + 1)
    m_pSolverHE->solution().axpby(1, r_instance.timeStep, m_solDispl_0);
  else
    m_pSolverHE->solution().axpby(1, r_instance.timeStep, m_solDispl);

  m_pSolverHE->gatherSolution();
  // seqSolution() is now the displacement because in VecAXPBY we change solution()

  updateMesh();
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::UpdateMeshByDisplacement() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  if ( r_instance.readDisplFromFile )
    m_pSolverHE->readDataDisplacement(m_ios, m_displTimeStep); // defined in linearProblemHarmonicExtention

  // fsi challenge
  if ( r_instance.useElasticExtension ) 
    m_dispMesho_pp.copyFrom( m_pSolverHE->solution() );


  if ( Tools::equal(r_instance.theta,0.5) ) {
    m_velMesh.copyFrom(m_dispMesho);
    m_dispMesho.copyFrom(m_dispMesh);
  } else {
    // m_dispMesh = 0 at m_fstransient->iteration = 1
    m_velMesh.copyFrom(m_dispMesh);
  }

  //Solve the extension problem
  solveHarmExt();
  m_pSolverHE->gatherSolution();

  updateMesh();


  // Compute velocity and apply it to the NS problem. seqSolution is the displacement
  //VecAXPBY(Vec y,PetscScalar a,PetscScalar b,Vec x);  -> y=a∗x+b∗y
  // now m_velMesh == d^{n-1}
  // and m_dispMesh == d^{n}
  m_velMesh.axpby(-m_tau,m_tau,m_dispMesh);
  // Result: m_velMesh =  -w^{n}  =  \dfrac{1}{\dt} (-d^{n} + d^{n-1})
  if ( Tools::equal(r_instance.theta,0.5) )
    m_velMesh.scale(0.5);
  // Result: m_velMesh =  -w^{n+1/2}  =  0.5 \dfrac{1}{\dt} (-d^{n} + d^{n-2})
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::solveHarmExt() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

    //Assemble matrix and RHS
  m_pSolverHE->assembleMatrixRHS(MpiInfo::rankProc());
    //Specific operations before solve the system
    postAssemblingMatrixRHS(1);
    //Apply essential transient boundary conditions
  m_pSolverHE->finalizeEssBCTransient();
  m_pSolverHE->applyBC(r_instance.essentialBoundaryConditionsMethod, MpiInfo::rankProc());
    //Solve linear system
  m_pSolverHE->solve(MpiInfo::rankProc(), MpiInfo::numProc());

  // fsi challenge (update displacement with increment)
  if ( r_instance.useElasticExtension ) 
    m_pSolverHE->solution().axpy(1.,m_dispMesho_pp);
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::updateMesh() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  const int ivar  = m_pSolverHE->listVariable().getVariableIdList(displacement);
  const int iunk  = m_pSolverHE->listUnknown().getUnknownIdList(displacement);
  const int iMesh = m_pSolverHE->listVariable()[ivar].idMesh();

  //=========
  //   ALE
  //=========
  PetscPrintf(MpiInfo::petscComm(),"Update mesh\n");

  m_dispMesh.copyFrom(m_pSolverHE->sequentialSolution());
  // m_dispMesh is the solution obtained with solveHarmExt()
  // Extract the values of m_dispMesh and put them in the std::vector m_dispMeshVector
  // according to the application ordering of the nodes
  m_dispMesh.getValues(m_numDofExtensionProblem, m_petscToGlobal.data(), m_tmpDisp.data());

  felInt numNodes = m_pSolverHE->supportDofUnknown(iunk).listNode().size();
  felInt idof;
  felInt numCompDisp = m_pSolverHE->dimension();
  felInt numCoorMesh = m_pSolverHE->meshLocal(iMesh)->numCoor();

  if ( r_instance.FusionDof ) {
    for (felInt ip = 0; ip < numNodes; ip++) {
      idof = m_pSolverHE->supportDofUnknown(iunk).listPerm()[ip];      
      if ( numCompDisp < numCoorMesh ) {
        m_dispMeshVector[ip*3]   = m_tmpDisp[idof*2];
        m_dispMeshVector[ip*3+1] = m_tmpDisp[idof*2+1];
        m_dispMeshVector[ip*3+2] = 0.;
      }
      else if ( numCompDisp == numCoorMesh ) {
        for (felInt ic = 0; ic < numCoorMesh; ++ic)
          m_dispMeshVector[ip*numCoorMesh+ic] = m_tmpDisp[idof*numCoorMesh+ic];
        }
      }
    }
  else // to be modified: does not work if numCompDisp < numCoorMesh // TODO
    m_dispMeshVector = m_tmpDisp;

  //Move the mesh in the fluid problem
  if ( r_instance.NSequationFlag != 0 ) {
    m_pSolverNS->meshLocal(iMesh)->moveMesh(m_dispMeshVector,1.0);
    if ( (MpiInfo::rankProc() == 0) && r_instance.hasImmersedData ) // Attention: we move also the points of the global mesh, this can impact the fluid.geo if written at each time-step 
      m_pSolverNS->mesh(iMesh)->moveMesh(m_dispMeshVector,1.0); 
    if ( r_instance.useElasticExtension ) 
      // extension computed on the deformed mesh (takes into account the size of the deformed elements)
      m_pSolverHE->meshLocal(iMesh)->moveMesh(m_dispMeshVector,1.0);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::computePreStressedDispMeshVector() 
{
  // Retrieve instance
  auto& r_instance = FelisceParam::instance(instanceIndex());

  m_pSolverHE->HarmExtSol().getValues(m_numDofExtensionProblem, m_petscToGlobal.data(), m_tmpDisp.data());

  const int iunk  = m_pSolverHE->listUnknown().getUnknownIdList(displacement);
  
  felInt numNodes = m_pSolverHE->supportDofUnknown(iunk).listNode().size();
  if(r_instance.FusionDof) {

    // if there are e.g. RIS model, the number of dofs are re-numerated.
    m_preStressedDispMeshVector.resize(numNodes*3, 0.);
    for (felInt ip = 0; ip < numNodes; ip++) {
      m_preStressedDispMeshVector[ip*3]   = m_tmpDisp[m_pSolverHE->supportDofUnknown(iunk).listPerm()[ip]*3];  // TODO D.C. supportDofUnknown(0) is velocity? maybe is better get the id of the unknow from listVariable
      m_preStressedDispMeshVector[ip*3+1] = m_tmpDisp[m_pSolverHE->supportDofUnknown(iunk).listPerm()[ip]*3+1];
      m_preStressedDispMeshVector[ip*3+2] = m_tmpDisp[m_pSolverHE->supportDofUnknown(iunk).listPerm()[ip]*3+2];
    }
  } else
    m_preStressedDispMeshVector = m_tmpDisp;
}

/***********************************************************************************/
/***********************************************************************************/

PetscVector& NSModel::seqResidual() 
{
  return m_seqResidual;
}

/***********************************************************************************/
/***********************************************************************************/

PetscVector&  NSModel::residual() 
{
  return m_residual;
}

/***********************************************************************************/
/***********************************************************************************/

int& NSModel::hasLumpedMassRN() 
{
  return m_hasLumpedMassRN;
}

/***********************************************************************************/
/***********************************************************************************/

PetscVector&  NSModel::lumpedMassRN() 
{
  return m_lumpedMassRN;
}

/***********************************************************************************/
/***********************************************************************************/

Immersed & NSModel::immersed() 
{
  return m_immersed;
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::writeCurrentDisplacement(const int mesh_id, const std::string name)
{

  if ( MpiInfo::rankProc() != 0 )
    return;

  const int points = m_mesh[mesh_id]->numPoints();
  const int dim = 3*points;
  double* disp = new double[dim];

  for (int i = 0; i < points; ++i){
    for (int j = 0; j < 3; ++j)
      disp[3*i+j] = m_mesh[mesh_id]->listPoints()[i].coor(j) - m_mesh[mesh_id]->listReferencePoints()[i].coor(j);
  }
  m_ios[mesh_id]->writeSolution(MpiInfo::rankProc(), m_fstransient->time, m_fstransient->iteration, 1, name , disp, points, 3, true);
}

/***********************************************************************************/
/***********************************************************************************/

void NSModel::writeCurrentNormalVector(const int mesh_id, const std::string name)
{
  if ( MpiInfo::rankProc() != 0 )
    return;

  const int points = m_pSolverNS->mesh(mesh_id)->listNormals().size();
  const int dim = 3*points;
  double* normal = new double[dim];
  for (int i = 0; i < points; ++i){
    for (int j = 0; j < 3; ++j)
      normal[3*i+j] = m_pSolverNS->mesh(mesh_id)->listNormal(i).coor(j);
  }

  m_ios[mesh_id]->writeSolution(MpiInfo::rankProc(), m_fstransient->time, m_fstransient->iteration, 1, name , normal, points, 3, true);
}

/***********************************************************************************/
/************************* CVGRAPH *************************************************/

#ifdef FELISCE_WITH_CVGRAPH
void NSModel::cvgraphNewTimeStep() 
{
  this->prepareForward();
}
#endif
}
