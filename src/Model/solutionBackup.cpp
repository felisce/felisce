//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau, E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/solutionBackup.hpp"

namespace felisce 
{
SolutionBackup::SolutionBackup():
  m_meshIsWritten(false)
{}

SolutionBackup::~SolutionBackup() = default;

void SolutionBackup::initialize(const std::string& inputDirectory,const std::string& inputFile, const std::vector<std::string>& inputMesh, const std::vector<std::string>& outputMesh, 
                                const std::string& meshDir, const std::string& resultDir, const std::vector<std::string>& prefixName) 
{
  m_io.resize(inputMesh.size());
  m_meshIsWritten.resize(inputMesh.size(),false);

  for (std::size_t i = 0; i < m_io.size(); i++){
    std::string outputMesh_bkp = outputMesh[i];
    outputMesh_bkp.replace(outputMesh_bkp.end()-4,outputMesh_bkp.end(),"_bkp.geo");

    m_io[i] = felisce::make_shared<IO>(inputDirectory, inputFile, inputMesh[i], outputMesh_bkp, meshDir, resultDir+"/bkp/", prefixName[i]+"_bkp");
    m_io[i]->initializeOutput();
  }
}

bool SolutionBackup::hasToBackup(int iter, int freq) const 
{
  if (freq != 0 && iter % freq == 0) {
    return true;
  } else {
    return false;
  }
}

void SolutionBackup::backup(double time, std::vector<GeometricMeshRegion::Pointer>& mesh, int rank) 
{
  for (std::size_t i = 0; i < m_io.size(); i++){
    if (!m_meshIsWritten[i]) {
      m_io[i]->writeMesh(*mesh[i]);
      m_meshIsWritten[i] = true;
    }

    if ( rank == 0 ) {
      if ( m_io[i]->typeOutput == 1 ) { // 1 : ensight{
        m_io[i]->postProcess(time);
      }
    }
  }
}

void SolutionBackup::print(int verbose, std::ostream& outstr) const 
{
  IGNORE_UNUSED_VERBOSE;
  IGNORE_UNUSED_OUTSTR;
}

}

