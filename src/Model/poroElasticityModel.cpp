//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/poroElasticityModel.hpp"
#ifdef FELISCE_WITH_CVGRAPH
#include "Model/cvgMainSlave.hpp"
#endif
#include "Core/mpiInfo.hpp"

namespace felisce {
  PoroElasticityModel::PoroElasticityModel():Model() {
    m_name = "Linear Poro Elasticity";
    m_idPE=0;
  }
  
  void PoroElasticityModel::initializeDerivedModel() {
    // The pointer to the linear problem
    // is casted to a LinearProblemPoroElasticity pointer.
    // In this way we can access functions defined only
    // in that linear problem and not in linearProblem.hpp.
    m_lpbPE = static_cast<LinearProblemPoroElasticity*>(m_linearProblem[m_idPE]);
    // The petscVectors stored in m_vecs and m_seqVecs are initialized:
    // their structures are copied from the solution and from the sequential solution.
    m_lpbPE->initPetscVectors();
    // Custom initialization defined in the user files.
    m_lpbPE->userInitialize();
  }
  
  
  void PoroElasticityModel::preAssemblingMatrixRHS(std::size_t /*iProblem*/) {
    // Zeroing the solution.
    m_lpbPE->solution().zeroEntries();
    // Assembling the static part of the matrix.
    m_lpbPE->assembleMatrixRHS(MpiInfo::rankProc());// no flag, by default it means matrix_and_rhs
  }

  void PoroElasticityModel::forward() {
    // Post-processing of last iteration, t=t+dt
    prepareForward();
    // Assembling the non-static part of the matrix
    m_lpbPE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
    // Summing static and non static part of the matrix
    m_lpbPE->addMatrixRHS();
    if ( FelisceParam::instance().withCVG ) {
      m_lpbPE->createAndCopyMatrixRHSWithoutBC();
    }
    // Solving the problem ( BC + ksp )
    solvePoroElasticity();
  }


  void PoroElasticityModel::prepareForward( FlagMatrixRHS flag ) {
    // Post processing of last time step + export
    if( m_fstransient->iteration == 0 ) {
      m_lpbPE->exportInitialFiltrationVelocity(m_ios);
    } else {
      m_lpbPE->advanceInTime();
      m_lpbPE->exportFiltrationVelocity(m_ios);
    }
    writeSolution();
    // t=t+dt, plus clear matrix(1) and rhs
    updateTime(flag);
    printNewTimeIterationBanner();
  }

  void PoroElasticityModel::solvePoroElasticity(FlagMatrixRHS flag) {
    // Applying boundary conditions
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Applying boundary conditions \n");
    // Apply essential transient boundary conditions.
    m_lpbPE->finalizeEssBCTransient();
    m_lpbPE->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(),flag,flag);
    if (FelisceParam::verbose())
      PetscPrintf(MpiInfo::petscComm(),"Solving linear poro-elasticity system \n");
    // Solve linear system.
    m_lpbPE->solve(MpiInfo::rankProc(), MpiInfo::numProc());
    // Computing residual stresses
    if (FelisceParam::instance().withCVG ) {
      m_lpbPE->computeResidual();
    }
  }

#ifdef FELISCE_WITH_CVGRAPH
  void PoroElasticityModel::startIterationCVG() {
    cvgMainSlave* slave=m_lpbPE->slave();
    if (slave==nullptr) {
      FEL_ERROR("slave not initialized");
    }
    if( slave->newTimeStep() ) {
      prepareForward();
      m_lpbPE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
      m_lpbPE->addMatrixRHS();
      m_lpbPE->createAndCopyMatrixRHSWithoutBC();
    } else if( slave->redoTimeStep() ) {
      std::stringstream msg;
      msg << "......................................" << std::endl;
      msg << "Redo time step at time t= " << m_fstransient->time << std::endl;
      PetscPrintf(MpiInfo::petscComm(),"%s",msg.str().c_str());
      m_linearProblem[0]->matrix(0).copyFrom(m_linearProblem[0]->matrixWithoutBC(),SAME_NONZERO_PATTERN);
      m_linearProblem[0]->vector().copyFrom(m_linearProblem[0]->rhsWithoutBC());
      // m_lpbPE->clearMatrixRHS();
      // m_lpbPE->assembleMatrixRHS(MpiInfo::rankProc(),FlagMatrixRHS::matrix_and_rhs);
      // m_lpbPE->addMatrixRHS();
      // m_lpbPE->createAndCopyMatrixRHSWithoutBC();
    } else {
      slave->msgInfo().print(1);
      FEL_ERROR("Error: strange timeStatus");
    }
  }
#endif
}
