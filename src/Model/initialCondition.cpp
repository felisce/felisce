//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau, E. Schenone
//

// System includes

// External includes

// Project includes
#include "Model/initialCondition.hpp"

namespace felisce {

  void InitialCondition::initializeFromFile(std::string inputDirectory, std::string inputCaseFile) {
    m_initialSolutionCase = new EnsightCase();
    m_initialSolutionCase->read(inputDirectory,inputCaseFile);
  }

  void InitialCondition::addVariable(const Variable& var) {
    m_listVariable.addVariable(var);
  }

  void InitialCondition::readVariable(int iVariable, int indexTime, double* variableValue, felInt sizeVar) {
    m_initialSolutionCase->readVariable(iVariable, indexTime, variableValue, sizeVar);
  }

  //! Print function
  void InitialCondition::print(int verbose, std::ostream& outstr) const {
    if (verbose > 0 ) {
      outstr << "IC Variable : " << std::endl;
      for ( unsigned int iVar = 0; iVar < m_listVariable.size(); iVar++)
        m_listVariable[iVar].print(verbose);
    }
  }

}

