//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau, E. Schenone
//

#ifndef _SolutionBackup_HPP
#define _SolutionBackup_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "DegreeOfFreedom/listVariable.hpp"
#include "InputOutput/io.hpp"

namespace felisce {
class SolutionBackup {
  /*!
    \class SolutionBackup
    \authors J-F. Gerbeau, E. Schenone
    \date July 2012
    \brief To backup the variables

  */
  std::vector<IO::Pointer> m_io;
  std::vector<bool> m_meshIsWritten;

public:
  SolutionBackup();
  ~SolutionBackup();

  //! Copy constructor, to avoid warning due to user-declared destructor.
  SolutionBackup(const SolutionBackup&) = default;

  void initialize(const std::string& inputDirectory,const std::string& inputFile, const std::vector<std::string>& inputMesh, const std::vector<std::string>& outputMesh, 
                  const std::string& meshDir, const std::string& resultDir, const std::vector<std::string>& prefixName);

  void print(int verbose, std::ostream& outstr) const;
  
  bool hasToBackup(int iter, int freq) const;
  
  void backup(double time, std::vector<GeometricMeshRegion::Pointer>& mesh, const int rank=0);
  
  const std::vector<IO::Pointer>& listIO() const {
    return m_io;
  }
  
  std::vector<IO::Pointer>& listIO() {
    return m_io;
  }

  const IO::Pointer io(int id=0) const {
    return m_io[id];
  }
  
  IO::Pointer io(int id=0) {
    return m_io[id];
  }

};
}

#endif
