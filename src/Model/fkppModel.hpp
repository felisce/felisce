//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _FKPPMODEL_HPP
#define _FKPPMODEL_HPP

// System includes

// External includes

// Project includes
#include "Model/model.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  class FkppModel:
    public Model {
  public:
    ///Construtor.
    FkppModel();
    ///Destructor.
    ~FkppModel() override;
    /// Manage time iteration.
    void forward() override;

  };
}

#endif
