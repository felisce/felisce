//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/NSSimplifiedFSIModel.hpp"
#include "Solver/linearProblemNSSimplifiedFSI.hpp"

namespace felisce {

  NSSimplifiedFSIModel::NSSimplifiedFSIModel():Model() {
    m_lumpedModelBC = nullptr;
    m_name = "NS";
    m_exportBdQuantities=true;
    m_idNSSimpl = 0; // in this class is obviously zero, but what if you have derived model?
    m_SimplifiedFSI=nullptr;
  }


  NSSimplifiedFSIModel::~NSSimplifiedFSIModel() {
    if( m_SimplifiedFSI )
      if ( m_SimplifiedFSI->autoregulated() ) {
        m_SimplifiedFSI->exportControl(MpiInfo::rankProc());
      }
    if(m_lumpedModelBC)
      delete m_lumpedModelBC;
  }


  void NSSimplifiedFSIModel::initializeDerivedModel() {
    if( FelisceParam::instance().lumpedModelBCLabel.size() ) {
      m_lumpedModelBC = new LumpedModelBC( m_fstransient );
      static_cast<LinearProblemNSSimplifiedFSI*>(m_linearProblem[m_idNSSimpl])->initializeLumpedModelBC(m_lumpedModelBC);
    }

    if (FelisceParam::instance().orderBdfNS > 2)
      FEL_ERROR("BDF not yet implemented for order greater than 2 with Navier-Stokes.");

    m_bdfNS.defineOrder(FelisceParam::instance().orderBdfNS);
    m_linearProblem[m_idNSSimpl]->initializeTimeScheme(&m_bdfNS);
  }


  void NSSimplifiedFSIModel::preAssemblingMatrixRHS(std::size_t iProblem) {
    if ( iProblem == m_idNSSimpl) {
      m_solExtrapolate.duplicateFrom(m_linearProblem[iProblem]->vector());
      m_solExtrapolate.zeroEntries();
    }  
    m_linearProblem[iProblem]->solution().zeroEntries();
    m_linearProblem[iProblem]->assembleMatrixRHS(MpiInfo::rankProc());
  }

  void NSSimplifiedFSIModel::postAssemblingMatrixRHS(std::size_t iProblem) {
    // _Matrix = _A + _Matrix (add static matrix to the dynamic matrix to build
    // complete matrix of the system
    m_linearProblem[iProblem]->addMatrixRHS();
  }

  void NSSimplifiedFSIModel::writeExtraVectors() {
    m_SimplifiedFSI = static_cast<LinearProblemNSSimplifiedFSI*>(m_linearProblem[m_idNSSimpl]);
    if ( m_exportBdQuantities ) {
      PetscVector zero;
      zero.duplicateFrom(m_linearProblem[m_idNSSimpl]->solution());
      zero.zeroEntries();
      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("displacement"));
        
      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("normal"));
      if ( FelisceParam::instance().exportDofPartition )
        m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("partition"));
        
      if (FelisceParam::instance().exportPrincipalDirectionsAndCurvature ) {
        m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("maxEigenVector"));
        m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("minEigenVector"));
        m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("meanCurvature"));
        m_linearProblem[m_idNSSimpl]->writeSolutionFromVec( zero   , MpiInfo::rankProc(), m_ios, m_fstransient->time, m_fstransient->iteration, std::string("koiterCoeff"));
      }
    }
  }

  
  //Everything in this function should depend only on the previous time step and NOT on current data!!
  void NSSimplifiedFSIModel::prepareForward(){
    if( m_fstransient->iteration == 0 ) {
      writeExtraVectors();
    }

    // Write solution for postprocessing (if required)
    writeSolution();

    // Advance time step.
    updateTime();

    // Print time information
    printNewTimeIterationBanner();

    if ( m_fstransient->iteration == 1) {

      m_SimplifiedFSI->initStructure();
      if ( FelisceParam::verbose() > 0 )
        PetscPrintf(MpiInfo::petscComm()," Structure initialized \n");
      if (m_lumpedModelBC) {
        // m_labelFluxListLumpedModelBC computation with LumpedModelBC labels
        m_labelFluxListLumpedModelBC.clear();
        for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
          m_labelFluxListLumpedModelBC.push_back(FelisceParam::instance().lumpedModelBCLabel[i]);
        }
      }

      m_bdfNS.initialize(m_linearProblem[m_idNSSimpl]->solution());
      m_bdfNS.extrapolate(m_solExtrapolate);
      m_linearProblem[m_idNSSimpl]->initExtrapol(m_solExtrapolate);
    } else {
      m_bdfNS.update(m_linearProblem[m_idNSSimpl]->solution());
      m_bdfNS.extrapolate(m_solExtrapolate);
      m_linearProblem[m_idNSSimpl]->updateExtrapol(m_solExtrapolate);
    }

    // lumpedModelBC bc computation
    if (m_lumpedModelBC) {
      m_SimplifiedFSI->handleWindkessel(m_fstransient->time);
      // update the fluxes
      m_linearProblem[m_idNSSimpl]->computeMeanQuantity(velocity,m_labelFluxListLumpedModelBC,m_fluxLumpedModelBC );

      for(std::size_t i=0; i<m_lumpedModelBC->size(); i++) {
        m_lumpedModelBC->Q(i) = m_fluxLumpedModelBC[i] ;
      }
      // update compliance if non linear compliance
      if ( FelisceParam::instance().lumpedModelBC_C_type == 2 ) {
        static_cast<LinearProblemNSSimplifiedFSI*>(m_linearProblem[m_idNSSimpl])->updateVolume();
      }
      // iterate the lumpedModelBC parameters
      m_lumpedModelBC->iterate();
    }

    m_bdfNS.computeRHSTime(m_fstransient->timeStep);
    m_linearProblem[m_idNSSimpl]->gatherVectorBeforeAssembleMatrixRHS();
  }

  void NSSimplifiedFSIModel::solveNSSimplifiedFSI(FlagMatrixRHS flagMatrixRHS){
    if (FelisceParam::verbose() )
      PetscPrintf(MpiInfo::petscComm(),"NS SimplifiedFSI \n");

    m_linearProblem[m_idNSSimpl]->assembleMatrixRHS(MpiInfo::rankProc(),flagMatrixRHS); // assembly loop over the domain elements
    if (flagMatrixRHS == FlagMatrixRHS::matrix_and_rhs || flagMatrixRHS == FlagMatrixRHS::only_matrix)
      postAssemblingMatrixRHS(m_idNSSimpl); // sum "constant" matrix with time dependent

    if ( FelisceParam::verbose()>2)
      std::cout<<"Time for assembling matrix and rhs: "<<chronoAssemble.diff()<<std::endl;

    if (FelisceParam::verbose()>2)
      PetscPrintf(MpiInfo::petscComm(),"Applying BC \n");

    m_linearProblem[m_idNSSimpl]->finalizeEssBCTransient(); 
    m_linearProblem[m_idNSSimpl]->applyBC(FelisceParam::instance().essentialBoundaryConditionsMethod, MpiInfo::rankProc(), flagMatrixRHS,flagMatrixRHS);  

    if (FelisceParam::verbose()>2)
      PetscPrintf(MpiInfo::petscComm(),"Solving NS system \n");

    m_linearProblem[m_idNSSimpl]->solve(MpiInfo::rankProc(), MpiInfo::numProc());
  }

  void NSSimplifiedFSIModel::finalizeForward() {
    // update the structure! <with this kind of model, the structure is only a variable defined on the surface>
    if( FelisceParam::verbose() > 2 ) 
      std::cout<<"["<<MpiInfo::rankProc()<<"] start - update structure"<<std::endl;
    m_SimplifiedFSI->updateStructure();

    if( m_exportBdQuantities ) {
      exportBdQuantities();
    }
    if( m_SimplifiedFSI->autoregulated())
      m_SimplifiedFSI->updateControl(m_fstransient->time, MpiInfo::rankProc());
  }

  void NSSimplifiedFSIModel::forward() {
  
    prepareForward();

    solveNSSimplifiedFSI(FlagMatrixRHS::matrix_and_rhs);

    finalizeForward();
  }

  void NSSimplifiedFSIModel::exportBdQuantities() {

    m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::sequential,"displacement"),
        MpiInfo::rankProc(),
        m_ios,
        m_fstransient->time,
        m_fstransient->iteration,
        std::string("displacement"));

    m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::sequential,"normalField"),
        MpiInfo::rankProc(),
        m_ios,
        m_fstransient->time,
        m_fstransient->iteration,
        std::string("normal"));
    if ( FelisceParam::instance().exportDofPartition )
      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::sequential,"partition"),
        MpiInfo::rankProc(),
        m_ios,
        m_fstransient->time,
        m_fstransient->iteration,
        std::string("partition"));
    if (FelisceParam::instance().exportPrincipalDirectionsAndCurvature) {
      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::parallel,"meanCurvature"),
                                               MpiInfo::rankProc(),
                                               m_ios,
                                               m_fstransient->time,
                                               m_fstransient->iteration,
                                               std::string("meanCurvature"));

      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::sequential,"koiterCoefficients"),
                                               MpiInfo::rankProc(),
                                               m_ios,
                                               m_fstransient->time,
                                               m_fstransient->iteration,
                                               std::string("koiterCoeff"));

      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::sequential,"maxEigVec"),
                                               MpiInfo::rankProc(),
                                               m_ios,
                                               m_fstransient->time,
                                               m_fstransient->iteration,
                                               std::string("maxEigenVector"));
      
      m_linearProblem[m_idNSSimpl]->writeSolutionFromVec(m_SimplifiedFSI->get(LinearProblemNSSimplifiedFSI::sequential,"minEigVec"),
                                               MpiInfo::rankProc(),
                                               m_ios,
                                               m_fstransient->time,
                                               m_fstransient->iteration,
                                               std::string("minEigenVector"));
    }

  }

}
