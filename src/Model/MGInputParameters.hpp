//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//


#ifndef MGINPUTPARAMETERS_HPP
#define MGINPUTPARAMETERS_HPP

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#ifdef FELISCE_WITH_QT
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>
#endif

// Project includes


#ifdef FELISCE_WITH_QT

namespace felisce {

class MGInputParameters
{
    public:
        enum partType
        {
                MONODISPERSE,
                LOGNORMAL,
        };

        enum ventilationType    
        {
                SIMPLE,
                REAL
        };

        MGInputParameters();
        MGInputParameters( QString fileName );
        virtual ~MGInputParameters();
        bool readData( QString fileName );
        QList<double> computeLogNormDistribution( double cmd, double gsd, double nbPart );
        QString getMorphoFileName();
        double getLaryngealCoeff();
        double getGasTemperature();
        double getGasDensity();
        double getGasViscosity();
        double getGasMFP();
        QList<double> getParticleDiameters();
        double getParticleDiameter( int particleIndex );
        double getParticleDensity();
        int getNbParticles();
        double getCMD();
        double getGSD();
        double getTidalVolume();
        double getInhalationDuration();
        double getPauseDuration();
        double getExhalationDuration();
        bool getSuccess();
        int getINbGenT();
        int getENbGenT();
        int getINbGenPlug();
        int getENbGenPlug();
        double getFRC();
        double getHeight();
        int getMouthType();
        int getAlveolarModel();
        double getLungCompliance();
        double getVrv();
        double getVfrc();
        double getVtlc();
        double getVfrc0g();
        int getGravity();
        QString getPosition();
        int getTerminalGeneration();
        QString getModelBuildTree();
        QString getMotherID();
        QString getBranchDisease();
        QString getLobeAsthma();
        double getDiseaseRatio();
        QString getLobeEmphysema();
        double getEmphysemaRatio();
	double getGenerationStenosis();
        QString getResistanceMode();
        QString getFileInhaleKatz();
        QString getFileExhaleKatz();
        QString getFilePleural();
        QString getFileNameFlux();
        QString getFileNameVolume();
        QString getFileNameFluxLOBE();
        QString getFileNameVolumeLOBE();
        QString getFileNamePressure();
        QString getVidaMorphoFile();
        QString getDicoFile();


    private:
        bool _success;

        // MORPHO
        QString _morphoFile;
        double _laryngealCoeff;

        // GAS
        double _tGas, _dGas, _vGas, _mfpGas;

        // PARTICLES
        partType _parType;
        QList<double> _pDiam;
        double _CMD, _GSD;
        double _pDens;
        int _nbDiam;

        // LUNG
        double _lungCompliance;
        double _Vrv;
        double _Vfrc;
        double _Vtlc;
        double _Vfrc0g;
        int _gravity;
        QString _position;

        // TREE
        int _nbGeneration;
        QString _modelBuildTree;
        QString _motherID;

        //DISEASE
        QString _branchDisease;
        QString _lobeAsthma;
        double _diseaseRatio;
        QString _lobeEmphysema;
        double _emphysemaRatio;
        double _generationStenosis;

        //RESISTANCE MODEL
        QString _resistanceMode;

        //FILES
        QString _fileInhaleKatz;
        QString _fileExhaleKatz;
        QString _filePleural;
        QString _fileNameFlux;
        QString _fileNameVolume;
        QString _fileNameFluxLOBE;
        QString _fileNameVolumeLOBE;
        QString _fileNamePressure;


        // VENTILATION
        ventilationType _ventType;
        QList<double> _time, _volume;
        double _tVol, _tInh, _tPau, _tExh;

        int _iNbGenT, _eNbGenT, _iNbGenPlug, _eNbGenPlug;

        double _FRC;
        double _height;

        int _mouthType;
        int _alveolarModel;
        QString _dicoFile;
        QString _vidaMorphoFile;

};

}

#endif /*MGINPUTPARAMETERS_HPP*/
#endif

