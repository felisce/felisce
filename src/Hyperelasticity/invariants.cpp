//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Hyperelasticity/invariants.hpp"

namespace felisce::Invariants {

namespace { // anonymous

  UBlasMatrix InitMatrixSecondDerivativeInvariant2_3D() 
  {
    UBlasMatrix ret(6, 6);
    ret.clear();

    // [0, 1, 1, 0   , 0   , 0   ],
    // [1, 0, 1, 0   , 0   , 0   ],
    // [1, 1, 0, 0   , 0   , 0   ],
    // [0, 0, 0, -0.5, 0   , 0   ],
    // [0, 0, 0, 0   , -0.5, 0   ],
    // [0, 0, 0, 0   , 0   , -0.5]

    ret(3, 3) = ret(4, 4) = ret(5, 5) = -.5;

    for (std::size_t i = 0; i < 3; ++i)
      for (std::size_t j = i + 1; j < 3; ++j)
        ret(i, j) = ret(j, i) = 1.;

    return ret;
  }

  // Constant matrix obtained for second derivative of invariant 2 in 3D case
  static const UBlasMatrix MatrixSecondDerivativeInvariant2_3D = std::move(InitMatrixSecondDerivativeInvariant2_3D());

  UBlasMatrix InitMatrixSecondDerivativeInvariant23_2D() 
  {
    UBlasMatrix ret(3, 3);
    ret.clear();

    // [0, 1, 0]
    // [1, 0, 0]
    // [0, 0, -0.5]

    ret(2, 2) = -0.5;
    ret(0, 1) = ret(1, 0) = 1.;

    return ret;
  }

  // Constant matrix obtained for second derivative of invariant 2 and 3 in 2D case
  static const UBlasMatrix MatrixSecondDerivativeInvariant23_2D = std::move(InitMatrixSecondDerivativeInvariant23_2D());

  UBlasVector InitVectorFirstDerivativeInvariant1_2D() 
  {
    UBlasVector ret(3);
    ret(0) = 1.;
    ret(1) = 1.;
    ret(2) = 0.;
    return ret;
  }

  static const UBlasVector VectorFirstDerivativeInvariant1_2D = std::move(InitVectorFirstDerivativeInvariant1_2D());

  UBlasVector InitVectorFirstDerivativeInvariant1_3D() 
  {
    UBlasVector ret(6);
    ret.clear();
    ret(0) = 1.;
    ret(1) = 1.;
    ret(2) = 1.;
    return ret;
  }

  static const UBlasVector VectorFirstDerivativeInvariant1_3D = std::move(InitVectorFirstDerivativeInvariant1_3D());

  template<int SizeT>
  UBlasMatrix InitEmptyMatrix() {
    UBlasMatrix ret(SizeT, SizeT);
    ret.clear();
    return ret;
  }

  static const UBlasMatrix EmptyMatrix_2D = std::move(InitEmptyMatrix<3>());
  static const UBlasMatrix EmptyMatrix_3D = std::move(InitEmptyMatrix<6>());

} // namespace anonymous

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor2D::xx() const 
{
  return m_components[0];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor3D::xx() const 
{
  return m_components[0];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor2D::yy() const 
{
  return m_components[1];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor3D::yy() const 
{
  return m_components[1];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor3D::zz() const 
{
  return m_components[2];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor2D::xy() const 
{
  return m_components[2];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor3D::xy() const 
{
  return m_components[3];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor3D::yz() const {
  return m_components[4];
}

/***********************************************************************************/
/***********************************************************************************/

double CauchyGreenTensor3D::xz() const {
  return m_components[5];
}

/***********************************************************************************/
/***********************************************************************************/

double Invariant1(const CauchyGreenTensor2D& tensor) 
{
  return tensor.xx() + tensor.yy() + 1.;
}

/***********************************************************************************/
/***********************************************************************************/

double Invariant1(const CauchyGreenTensor3D& tensor)
{
  return tensor.xx() + tensor.yy() + tensor.zz();
}

/***********************************************************************************/
/***********************************************************************************/

double Invariant2(const CauchyGreenTensor2D& tensor) 
{
  const auto& r_components = tensor.components();

  return r_components[0] * r_components[1] + r_components[0] + r_components[1] - std::pow(r_components[2], 2);
}

/***********************************************************************************/
/***********************************************************************************/

double Invariant2(const CauchyGreenTensor3D& tensor) 
{
  const auto& r_components = tensor.components();

  return r_components[0] * r_components[1] + r_components[1] * r_components[2] + r_components[0] * r_components[2] - std::pow(r_components[3], 2) - std::pow(r_components[5], 2) - std::pow(r_components[4], 2);
}

/***********************************************************************************/
/***********************************************************************************/

double Invariant3(const CauchyGreenTensor2D& tensor) 
{
  return tensor.xx() * tensor.yy() - std::pow(tensor.xy(), 2);
}

/***********************************************************************************/
/***********************************************************************************/

double Invariant3(const CauchyGreenTensor3D& tensor) 
{
  const auto& r_components = tensor.components();

  return r_components[0] * r_components[1] * r_components[2] - r_components[0] * std::pow(r_components[4], 2) - r_components[1] * std::pow(r_components[5], 2) - r_components[2] * std::pow(r_components[3], 2) + 2. * r_components[3] * r_components[4] * r_components[5];
}

/***********************************************************************************/
/***********************************************************************************/

CauchyGreenTensor2D::CauchyGreenTensor2D(
  const UBlasVector& gradient_component_dispx,
  const UBlasVector& gradient_component_dispy
  ) 
{
  std::array<double, 3> components;

  // Component Cxx
  components[0] = 1. + 2. * gradient_component_dispx[0]
                  + std::pow(gradient_component_dispx[0], 2)
                  + std::pow(gradient_component_dispy[0], 2);

  // Component Cyy
  components[1] = 1. + 2. * gradient_component_dispy[1]
                  + std::pow(gradient_component_dispx[1], 2)
                  + std::pow(gradient_component_dispy[1], 2);

  // Component Cxy
  components[2] = gradient_component_dispx[1] + gradient_component_dispy[0]
                  + gradient_component_dispx[0] * gradient_component_dispx[1]
                  + gradient_component_dispy[0] * gradient_component_dispy[1];

  SetComponents(std::move(components));
}

/***********************************************************************************/
/***********************************************************************************/

CauchyGreenTensor3D::CauchyGreenTensor3D(const UBlasVector& gradient_component_dispx,
  const UBlasVector& gradient_component_dispy,
  const UBlasVector& gradient_component_dispz
  ) 
{

  std::array<double, 6> components;

  // Component Cxx
  components[0] = 1. + 2. * gradient_component_dispx[0]
                  + std::pow(gradient_component_dispx[0], 2)
                  + std::pow(gradient_component_dispy[0], 2)
                  + std::pow(gradient_component_dispz[0], 2);

  // Component Cyy
  components[1] = 1. + 2. * gradient_component_dispy[1]
                  + std::pow(gradient_component_dispx[1], 2)
                  + std::pow(gradient_component_dispy[1], 2)
                  + std::pow(gradient_component_dispz[1], 2);

  // Component Czz
  components[2] = 1. + 2. * gradient_component_dispz[2]
                  + std::pow(gradient_component_dispx[2], 2)
                  + std::pow(gradient_component_dispy[2], 2)
                  + std::pow(gradient_component_dispz[2], 2);

  // Component Cxy
  components[3] = gradient_component_dispx[1] + gradient_component_dispy[0]
                  + gradient_component_dispx[0] * gradient_component_dispx[1]
                  + gradient_component_dispy[0] * gradient_component_dispy[1]
                  + gradient_component_dispz[0] * gradient_component_dispz[1];

  // Component Cyz
  components[4] = gradient_component_dispy[2] + gradient_component_dispz[1]
                  + gradient_component_dispx[2] * gradient_component_dispx[1]
                  + gradient_component_dispy[2] * gradient_component_dispy[1]
                  + gradient_component_dispz[2] * gradient_component_dispz[1];

  // Component Cxz
  components[5] = gradient_component_dispz[0] + gradient_component_dispx[2]
                  + gradient_component_dispx[2] * gradient_component_dispx[0]
                  + gradient_component_dispy[2] * gradient_component_dispy[0]
                  + gradient_component_dispz[2] * gradient_component_dispz[0];

  SetComponents(std::move(components));
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector FirstDerivativeInvariant1CauchyGreen(const CauchyGreenTensor2D& ) 
{
  return VectorFirstDerivativeInvariant1_2D;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector FirstDerivativeInvariant1CauchyGreen(const CauchyGreenTensor3D& ) 
{
  return VectorFirstDerivativeInvariant1_3D;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector FirstDerivativeInvariant2CauchyGreen(const CauchyGreenTensor2D& tensor) 
{
  UBlasVector ret(3);
  ret(0) = tensor.yy() + 1.;
  ret(1) = tensor.xx() + 1.;
  ret(2) = -tensor.xy();
  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector FirstDerivativeInvariant2CauchyGreen(const CauchyGreenTensor3D& tensor) 
{
  UBlasVector ret(6);
  ret.clear();

  const auto& r_components = tensor.components();

  ret(0) = r_components[1] + r_components[2];
  ret(1) = r_components[0] + r_components[2];
  ret(2) = r_components[0] + r_components[1];
  ret(3) = -r_components[3];
  ret(4) = -r_components[4];
  ret(5) = -r_components[5];

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector FirstDerivativeInvariant3CauchyGreen(const CauchyGreenTensor2D& tensor) {
  UBlasVector ret(3);

  const auto& r_components = tensor.components();

  ret(0) = r_components[1];
  ret(1) = r_components[0];
  ret(2) = -r_components[2];

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector FirstDerivativeInvariant3CauchyGreen(const CauchyGreenTensor3D& tensor) 
{
  UBlasVector ret(6);
  ret.clear();

  const auto& r_components = tensor.components();

  ret(0) = r_components[1] * r_components[2] - std::pow(r_components[4], 2);
  ret(1) = r_components[0] * r_components[2] - std::pow(r_components[5], 2);
  ret(2) = r_components[0] * r_components[1] - std::pow(r_components[3], 2);
  ret(3) = r_components[4] * r_components[5] - r_components[2] * r_components[3];
  ret(4) = r_components[3] * r_components[5] - r_components[0] * r_components[4];
  ret(5) = r_components[3] * r_components[4] - r_components[1] * r_components[5];

  return ret;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix SecondDerivativeInvariant1CauchyGreen(const CauchyGreenTensor2D& ) 
{
  return EmptyMatrix_2D;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix SecondDerivativeInvariant1CauchyGreen(const CauchyGreenTensor3D& ) 
{
  return EmptyMatrix_3D;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix SecondDerivativeInvariant2CauchyGreen(const CauchyGreenTensor2D& )
{
  return MatrixSecondDerivativeInvariant23_2D;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix SecondDerivativeInvariant2CauchyGreen(const CauchyGreenTensor3D& ) 
{
  return MatrixSecondDerivativeInvariant2_3D;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix SecondDerivativeInvariant3CauchyGreen(const CauchyGreenTensor2D&) 
{
  return MatrixSecondDerivativeInvariant23_2D;
}


/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix SecondDerivativeInvariant3CauchyGreen(const CauchyGreenTensor3D& tensor) 
{
  UBlasMatrix ret(6, 6);
  ret.clear();

  // [0    , Czz  , Cyy  , 0        , -Cyz     , 0        ],
  // [Czz  , 0    , Cxx  , 0        , 0        , -Cxz     ],
  // [Cyy  , Cxx  , 0    , -Cxy     , 0        , 0        ],
  // [0    , 0    , -Cxy , -0.5*Czz , 0.5*Cxz  , 0.5*Cyz  ],
  // [-Cyz , 0    , 0    , 0.5*Cxz  , -0.5*Cxx , 0.5*Cxy  ],
  // [0    , -Cxz , 0    , 0.5*Cyz  , 0.5*Cxy  , -0.5*Cyy ]

  const auto& r_components = tensor.components();

  ret(3, 3) = -0.5 * r_components[2];
  ret(4, 4) = -0.5 * r_components[0];
  ret(5, 5) = -0.5 * r_components[1];

  ret(1, 0) = ret(0, 1) = r_components[2];
  ret(2, 0) = ret(0, 2) = r_components[1];
  ret(1, 2) = ret(2, 1) = r_components[0];

  ret(0, 4) = ret(4, 0) = -r_components[4];
  ret(1, 5) = ret(5, 1) = -r_components[5];
  ret(2, 3) = ret(3, 2) = -r_components[3];

  ret(3, 4) = ret(4, 3) = 0.5 * r_components[5];
  ret(3, 5) = ret(5, 3) = 0.5 * r_components[4];
  ret(5, 4) = ret(4, 5) = 0.5 * r_components[3];

  return ret;
}

} // namespace felisce
