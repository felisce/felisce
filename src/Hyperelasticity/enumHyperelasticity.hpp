//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef _ENUM_HYPER_ELASTICITY_HPP
#define _ENUM_HYPER_ELASTICITY_HPP

// System includes

// External includes

// Project includes

namespace felisce {

  namespace HyperelasticityNS {
    enum TimeScheme {
      none,
      half_sum,
      midpoint
    };

    namespace MatrixNS {
      // Note: as it will be used mostly as an array index, enum in namespace more pratical than enum class.
      enum Index {
        system = 0,
        current_stiffness,
        new_stiffness,
        mass_per_square_time, // index for matrix 2 * Mass / delta_tˆ2
        Nmatrices
      };
    } // namespace MatrixNS


    namespace VectorNS {
      enum Index {
        system = 0,
        current_stiffness = 1, // contribution of stiffness from previous iteration to RHS
        new_stiffness = 2,
        Nvector
      };
      
    } // namespace VectorNS

  } // namespace HyperelasticityNS


} // namespace felisce


#endif // _ENUM_HYPER_ELASTICITY_HPP
