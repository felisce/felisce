//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/listVariable.hpp"
#include "FiniteElement/listCurrentFiniteElement.hpp"
#include "Hyperelasticity/pressureDataIncompressible.hpp"

namespace felisce::Private::Hyperelasticity {

PressureDataIncompressible::PressureDataIncompressible()
  : m_fePressure(nullptr)
{

}

/***********************************************************************************/
/***********************************************************************************/

void PressureDataIncompressible::Init(const ListVariable& list_variable, ListCurrentFiniteElement& list_finite_element)
{
  m_iPressure = list_variable.getVariableIdList(pressure);
  m_fePressure = list_finite_element[m_iPressure];
}

/***********************************************************************************/
/***********************************************************************************/

int PressureDataIncompressible::IndexPressure() const
{
  return m_iPressure;
}

/***********************************************************************************/
/***********************************************************************************/

bool PressureDataIncompressible::IsCompressible() const
{
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

CurrentFiniteElement* PressureDataIncompressible::FiniteElementPressure() const
{
  FEL_ASSERT(!(!m_fePressure));
  return m_fePressure;
}

} // namespace felisce
