//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef _FELISCE_PRIVATE_LINEAR_PROBLEM_HYPER_ELASTICITY_HPP
# define _FELISCE_PRIVATE_LINEAR_PROBLEM_HYPER_ELASTICITY_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/currentFiniteElement.hpp"
#include "Hyperelasticity/invariants.hpp"
#include "Hyperelasticity/enumHyperelasticity.hpp"
#include "HyperelasticityLaws/HyperElasticLaw.hpp"

namespace felisce {

  namespace Private {

    namespace  HyperElasticity {

      enum ForceType {
        volumic = 1,
        surfacic = 2
      };

      // Assign the type of force applied from the integer read in the data file
      ForceType AssignForceType(int data_parameter);

      /*!
       * \brief Helper method for #calculateLinearAndNonLinearMatrixAndRhs.
       *
       * The point is mostly to put in common most of 2D (only plane strain considered at the moment) and 3D cases.
       */
      template<class CauchyGreenTensorT>
      void calculateLinearAndNonLinearMatrixAndRhsHelper(HyperElasticLaw::Pointer pLaw,
          const CauchyGreenTensorT& rCauchyGreenTensor,
          double pressure,
          const UBlasMatrix& De,
          UBlasMatrix& linearPart,
          UBlasVector& dW,
          UBlasVector& rhsPart,
          const bool IsIncompressible
          )
      {
        {
          std::array<double, 3> invariants {
            Invariants::Invariant1(rCauchyGreenTensor),
            Invariants::Invariant2(rCauchyGreenTensor),
            Invariants::Invariant3(rCauchyGreenTensor)
          };

          pLaw->SetInvariants(invariants);
        }

        const UBlasVector dI1dC = std::move(FirstDerivativeInvariant1CauchyGreen(rCauchyGreenTensor));
        const UBlasVector dI2dC = std::move(FirstDerivativeInvariant2CauchyGreen(rCauchyGreenTensor));
        const UBlasVector dI3dC = std::move(FirstDerivativeInvariant3CauchyGreen(rCauchyGreenTensor));

        const double dWdI1 = pLaw->FirstDerivativeWFirstInvariant();
        const double dWdI2 = pLaw->FirstDerivativeWSecondInvariant();
        const double dWdI3 = pLaw->FirstDerivativeWThirdInvariant();

        UBlasMatrix d2W;
        {
          UBlasMatrix d2W_first_term, d2W_second_term;

          const UBlasMatrix d2I1dC = std::move(Invariants::SecondDerivativeInvariant1CauchyGreen(rCauchyGreenTensor));
          const UBlasMatrix d2I2dC = std::move(Invariants::SecondDerivativeInvariant2CauchyGreen(rCauchyGreenTensor));
          const UBlasMatrix d2I3dC = std::move(Invariants::SecondDerivativeInvariant3CauchyGreen(rCauchyGreenTensor));

          d2W_first_term = (dWdI1 * d2I1dC + dWdI2 * d2I2dC + dWdI3 * d2I3dC);

          if (IsIncompressible) {
            double dWcdI3 = pLaw->FirstDerivativeWcThirdInvariant(pressure);
            d2W_first_term += dWcdI3 * d2I3dC;
          }


          {
            const UBlasVector transp_dI1dC = trans(dI1dC);
            const UBlasVector transp_dI2dC = trans(dI2dC);
            const UBlasVector transp_dI3dC = trans(dI3dC);

            const double d2WdI1dI1 = pLaw->SecondDerivativeWFirstInvariant();
            const double d2WdI2dI2 = pLaw->SecondDerivativeWSecondInvariant();
            const double d2WdI3dI3 = pLaw->SecondDerivativeWThirdInvariant();

            const double d2WdI1dI2 = pLaw->SecondDerivativeWFirstAndSecondInvariant();
            const double d2WdI1dI3 = pLaw->SecondDerivativeWFirstAndThirdInvariant();
            const double d2WdI2dI3 = pLaw->SecondDerivativeWSecondAndThirdInvariant();

            d2W_second_term = d2WdI1dI1 * outer_prod(dI1dC, transp_dI1dC)
                              + d2WdI1dI2 * outer_prod(dI1dC, transp_dI2dC)
                              + d2WdI1dI3 * outer_prod(dI1dC, transp_dI3dC)
                              + d2WdI1dI2 * outer_prod(dI2dC, transp_dI1dC)
                              + d2WdI2dI2 * outer_prod(dI2dC, transp_dI2dC)
                              + d2WdI2dI3 * outer_prod(dI2dC, transp_dI3dC)
                              + d2WdI1dI3 * outer_prod(dI3dC, transp_dI1dC)
                              + d2WdI2dI3 * outer_prod(dI3dC, transp_dI2dC)
                              + d2WdI3dI3 * outer_prod(dI3dC, transp_dI3dC);

            if (IsIncompressible) {
              const double d2WcdI3dI3 = pLaw->SecondDerivativeWcThirdInvariant(pressure);
              d2W_second_term += d2WcdI3dI3 * d2I3dC;
            }

          }

          d2W = 4.0 * (d2W_first_term + d2W_second_term);
        }

        {
          const UBlasMatrix buf = prod(trans(De), d2W);
          linearPart = prod(buf, De);
        }

        dW = 2.0 * (dWdI1 * dI1dC + dWdI2 * dI2dC + dWdI3 * dI3dC);

        if (IsIncompressible) {
          const double dWcdI3 = pLaw->FirstDerivativeWcThirdInvariant(pressure);
          dW += 2.0 * dWcdI3 * dI3dC;
        }

        rhsPart = prod(trans(dW), De);
      }

      /*!
       * \brief Calculates linear and non-linear part of the matrix for a given quadrature point.
       */
      void calculateLinearAndNonLinearMatrixAndRhs(HyperElasticLaw::Pointer pLaw,
          const CurrentFiniteElement& feDisplacement,
          const std::vector<UBlasVector>& displacementPreviousIterationPerComponent,
          const UBlasVector& pressure_vector,
          unsigned int quadraturePointIndex,
          UBlasMatrix& linearPart,
          UBlasMatrix& nonLinearPart,
          UBlasVector& rhsPart,
          const bool IsIncompressible);

    } // namespace HyperElasticity

  } // namespace Private

} // namespace felisce

# endif // _FELISCE_PRIVATE_LINEAR_PROBLEM_HYPER_ELASTICITY_HPP
