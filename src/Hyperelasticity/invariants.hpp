//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef ___FELISCE__Invariants__
# define ___FELISCE__Invariants__

// System includes
#include <array>
#include <memory>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Hyperelasticity/invariantHelper.hpp"

namespace felisce {

  namespace Invariants {

    class CauchyGreenTensor2D : public Private::Component<3> {
    public:

      // Typedef upon shared smart pointer
      typedef std::shared_ptr<CauchyGreenTensor2D> Ptr;

      /*!
       * \brief Constructor from the gradients of the displacement.
       *
       * \param[in] gradient_component_dispx Vector that contains dx(dispx), dy(dispx), dz(displx) where dispx
       * is x component of the displacement.
       * \param[in] gradient_component_dispy Same for dispy.
       */
      explicit CauchyGreenTensor2D(const UBlasVector& gradient_component_dispx,
                                   const UBlasVector& gradient_component_dispy);

      // Destructor
      ~CauchyGreenTensor2D() = default;

      // Access the value of xx component
      inline double xx() const;

      // Access the value of yy component
      inline double yy() const;

      // Access the value of xy component
      inline double xy() const;
    };

    class CauchyGreenTensor3D : public Private::Component<6> {
    public:

      // Typedef upon shared smart pointer
      typedef std::shared_ptr<CauchyGreenTensor3D> Ptr;

      /*!
       * \brief Constructor from the gradients of the displacement.
       *
       * \param[in] gradient_component_dispx Vector that contains dx(dispx), dy(dispx), dz(displx) where dispx
       * is x component of the displacement.
       * \param[in] gradient_component_dispy Same for dispy.
       * \param[in] gradient_component_dispz Same for dispz.
       */
      explicit CauchyGreenTensor3D(const UBlasVector& gradient_component_dispx,
                                   const UBlasVector& gradient_component_dispy,
                                   const UBlasVector& gradient_component_dispz);


      // Destructor
      ~CauchyGreenTensor3D() = default;

      // Access the value of xx component.
      inline double xx() const;

      // Access the value of yy component.
      inline double yy() const;

      // Access the value of zz component.
      inline double zz() const;

      // Access the value of xy component.
      inline double xy() const;

      // Access the value of yz component.
      inline double yz() const;

      // Access the value of xz component
      inline double xz() const;

    };

    // Returns first reduced invariant.
    double Invariant1(const CauchyGreenTensor2D& tensor);
    double Invariant1(const CauchyGreenTensor3D& tensor);

    // Returns second reduced invariant.
    double Invariant2(const CauchyGreenTensor2D& tensor);
    double Invariant2(const CauchyGreenTensor3D& tensor);

    // Returns third reduced invariant.
    double Invariant3(const CauchyGreenTensor2D& tensor);
    double Invariant3(const CauchyGreenTensor3D& tensor);

    // Returns first derivative of first reduced invariant with respect to Cauchy-Green tensor.
    UBlasVector FirstDerivativeInvariant1CauchyGreen(const CauchyGreenTensor2D& tensor);
    UBlasVector FirstDerivativeInvariant1CauchyGreen(const CauchyGreenTensor3D& tensor);

    // Returns first derivative of second reduced invariant with respect to Cauchy-Green tensor.
    UBlasVector FirstDerivativeInvariant2CauchyGreen(const CauchyGreenTensor2D& tensor);
    UBlasVector FirstDerivativeInvariant2CauchyGreen(const CauchyGreenTensor3D& tensor);

    // Returns first derivative of third reduced invariant with respect to Cauchy-Green tensor.
    UBlasVector FirstDerivativeInvariant3CauchyGreen(const CauchyGreenTensor2D& tensor);
    UBlasVector FirstDerivativeInvariant3CauchyGreen(const CauchyGreenTensor3D& tensor);

    // Returns second derivative of first reduced invariant with respect to Cauchy-Green tensor.
    UBlasMatrix SecondDerivativeInvariant1CauchyGreen(const CauchyGreenTensor2D& tensor);
    UBlasMatrix SecondDerivativeInvariant1CauchyGreen(const CauchyGreenTensor3D& tensor);

    // Returns second derivative of second reduced invariant with respect to Cauchy-Green tensor.
    UBlasMatrix SecondDerivativeInvariant2CauchyGreen(const CauchyGreenTensor2D& tensor);
    UBlasMatrix SecondDerivativeInvariant2CauchyGreen(const CauchyGreenTensor3D& tensor);

    // Returns second derivative of third reduced invariant with respect to Cauchy-Green tensor.
    UBlasMatrix SecondDerivativeInvariant3CauchyGreen(const CauchyGreenTensor2D& tensor);
    UBlasMatrix SecondDerivativeInvariant3CauchyGreen(const CauchyGreenTensor3D& tensor);

  } // namespace Invariants


} // namespace felisce

#endif /* defined(___FELISCE__Invariants__) */
