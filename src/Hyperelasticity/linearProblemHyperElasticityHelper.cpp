//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes
#include <cmath>

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Core/felisceTools.hpp"
#include "Hyperelasticity/linearProblemHyperElasticityHelper.hpp"

namespace felisce::Private::HyperElasticity
{

// Assign the type of force applied from the integer read in the data file
ForceType AssignForceType(int data_parameter) {
  // Check whether a surfacic or volumic force has been chosen
  switch (data_parameter) {
    case static_cast<int>(surfacic):
      return surfacic;
    case static_cast<int>(volumic):
      return volumic;
    default:
      FEL_ERROR("Force applied should be either volumic or surfacic; make sure [solid] volumicOrSurfacicForce "
                "is correctly filled in your data file and encompass an acceptable value");
  }

  FEL_ERROR("Should never be reached; if so bug to correct in the function!!!");
  return volumic; // to avoid warning about missing warning

}

/***********************************************************************************/
/***********************************************************************************/

void calculateLinearAndNonLinearMatrixAndRhs(
    HyperElasticLaw::Pointer pLaw,
    const CurrentFiniteElement& feDisplacement,
    const std::vector<UBlasVector>& displacementPreviousIterationPerComponent,
    const UBlasVector& pressure_vector,
    unsigned int quadraturePointIndex,
    UBlasMatrix& linearPart,
    UBlasMatrix& nonLinearPart,
    UBlasVector& rhsPart,
    const bool IsIncompressible
    )
{
  FEL_ASSERT_LT(quadraturePointIndex, feDisplacement.numQuadraturePoint());
  const UBlasMatrix& dPhi = feDisplacement.dPhi[quadraturePointIndex]; // alias

  const std::size_t dimension = displacementPreviousIterationPerComponent.size();

  double pressure = -99.;

  if (IsIncompressible)
    pressure = inner_prod(feDisplacement.phi[quadraturePointIndex], pressure_vector);

  UBlasMatrix De;

  switch(dimension) {
    case 2: {
      FEL_ASSERT_EQUAL(nonLinearPart.size1(), 4);
      FEL_ASSERT_EQUAL(nonLinearPart.size2(), 4);
      nonLinearPart.clear();

      De.resize(3, 4);

      Invariants::CauchyGreenTensor2D::Ptr cauchy_green_tensor_ptr;

      {
        De.clear();

        const UBlasVector gradient_component_dispx = prod(dPhi, displacementPreviousIterationPerComponent[0]);
        const UBlasVector gradient_component_dispy = prod(dPhi, displacementPreviousIterationPerComponent[1]);

        De(0, 0) = 1. + gradient_component_dispx(0);
        De(0, 2) = gradient_component_dispy(0);

        De(1, 1) = gradient_component_dispx(1);
        De(1, 3) = 1. + gradient_component_dispy(1);

        De(2, 0) = gradient_component_dispx(1);
        De(2, 1) = 1. + gradient_component_dispx(0);
        De(2, 2) = 1. + gradient_component_dispy(1);
        De(2, 3) = gradient_component_dispy(0);

        cauchy_green_tensor_ptr =
          Invariants::CauchyGreenTensor2D::Ptr(new Invariants::CauchyGreenTensor2D(gradient_component_dispx,
                                                gradient_component_dispy));
      }

      FEL_ASSERT(cauchy_green_tensor_ptr);
      const Invariants::CauchyGreenTensor2D& cauchy_green_tensor = *cauchy_green_tensor_ptr;

      UBlasVector dW;
      calculateLinearAndNonLinearMatrixAndRhsHelper(pLaw, cauchy_green_tensor,
          pressure,
          De, linearPart, dW, rhsPart, IsIncompressible);

      for (std::size_t i = 0; i < 2; ++i) {
        std::size_t twice = 2 * i;

        for (std::size_t iComp = 0; iComp < 2; ++iComp)
          nonLinearPart(twice + iComp, twice + iComp) = dW(iComp); // Term SIGMA_xx, SIGMA_yy

        nonLinearPart(twice, twice + 1) = nonLinearPart(twice + 1, twice) = dW(2); // Term SIGMA_xy
      }

      break;
    }
    case 3: {
      FEL_ASSERT_EQUAL(nonLinearPart.size1(), 9);
      FEL_ASSERT_EQUAL(nonLinearPart.size2(), 9);
      nonLinearPart.clear();

      De.resize(6, 9);
      Invariants::CauchyGreenTensor3D::Ptr cauchy_green_tensor_ptr;

      {
        De.clear();

        const UBlasVector gradient_component_dispx = prod(dPhi, displacementPreviousIterationPerComponent[0]);
        const UBlasVector gradient_component_dispy = prod(dPhi, displacementPreviousIterationPerComponent[1]);
        const UBlasVector gradient_component_dispz = prod(dPhi, displacementPreviousIterationPerComponent[2]);

        De(0, 0) = 1. + gradient_component_dispx(0);
        De(0, 3) = gradient_component_dispy(0);
        De(0, 6) = gradient_component_dispz(0);

        De(1, 1) = gradient_component_dispx(1);
        De(1, 4) = 1. + gradient_component_dispy(1);
        De(1, 7) = gradient_component_dispz(1);

        De(2, 2) = gradient_component_dispx(2);
        De(2, 5) = gradient_component_dispy(2);
        De(2, 8) = 1. + gradient_component_dispz(2);

        De(3, 0) = gradient_component_dispx(1);
        De(3, 1) = 1. +  gradient_component_dispx(0);
        De(3, 3) = 1. +  gradient_component_dispy(1);
        De(3, 4) = gradient_component_dispy(0);
        De(3, 6) = gradient_component_dispz(1);
        De(3, 7) = gradient_component_dispz(0);

        De(4, 1) = gradient_component_dispx(2);
        De(4, 2) = gradient_component_dispx(1);
        De(4, 4) = gradient_component_dispy(2);
        De(4, 5) = 1. + gradient_component_dispy(1);
        De(4, 7) = 1. + gradient_component_dispz(2);
        De(4, 8) = gradient_component_dispz(1);

        De(5, 0) = gradient_component_dispx(2);
        De(5, 2) = 1. + gradient_component_dispx(0);
        De(5, 3) = gradient_component_dispy(2);
        De(5, 5) = gradient_component_dispy(0);
        De(5, 6) = 1. + gradient_component_dispz(2);
        De(5, 8) = gradient_component_dispz(0);

        cauchy_green_tensor_ptr =
          Invariants::CauchyGreenTensor3D::Ptr(new Invariants::CauchyGreenTensor3D(gradient_component_dispx,
                                                gradient_component_dispy,
                                                gradient_component_dispz));
      }

      FEL_ASSERT(cauchy_green_tensor_ptr);
      const Invariants::CauchyGreenTensor3D& r_cauchy_green_tensor = *cauchy_green_tensor_ptr;

      UBlasVector dW;
      calculateLinearAndNonLinearMatrixAndRhsHelper(pLaw, r_cauchy_green_tensor,
          pressure, De,
          linearPart, dW, rhsPart, IsIncompressible);

      for (std::size_t i = 0; i < 3; ++i) {
        const std::size_t thrice = 3 * i;

        for (std::size_t iComp = 0; iComp < 3; ++iComp)
          nonLinearPart(thrice + iComp, thrice + iComp) = dW(iComp);                           // Term SIGMA_xx, SIGMA_yy, SIGMA_zz

        nonLinearPart(thrice, thrice + 1) = nonLinearPart(thrice + 1, thrice) = dW(3);         // Term SIGMA_xy
        nonLinearPart(thrice + 1, thrice + 2) = nonLinearPart(thrice + 2, thrice + 1) = dW(4); // Term SIGMA_yz
        nonLinearPart(thrice, thrice + 2) = nonLinearPart(thrice + 2, thrice) = dW(5);         // Term SIGMA_xz
      }

      break;
    }
    default:
      FEL_ERROR("Invalid dimension; should never happen!");
  }
}

} // namespace felisce
