//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef ___FELISCE__PressureData__
# define ___FELISCE__PressureData__

// System includes

// External includes

// Project includes
#include "Hyperelasticity/enumHyperelasticity.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce {

  // Forward declarations.
  class ListVariable;
  class ListCurrentFiniteElement;
  class CurrentFiniteElement;

  namespace Private {

    namespace Hyperelasticity {

      /*!
       * \brief In charge of handling pressure-related data and methods.
       *
       * Does anything only for the incompressible problem."
       *
       * This class is intended to be a parent of template class linearProblemHyperelasticity.
       */
      class PressureData {

      public:
        /// Pointer definition of PressureData
        FELISCE_CLASS_POINTER_DEFINITION(PressureData);

        // Constructor.
        PressureData();

        // Destructor.
        virtual ~PressureData() = default;

        // Left undefined on purpose in the general case.
        virtual void Init(const ListVariable&, ListCurrentFiniteElement& );

        // Left undefined on purpose in the general case.
        virtual int IndexPressure() const;

        // If the formulation is compressible or not
        virtual bool IsCompressible() const;

        // Left undefined on purpose in the general case.
        virtual CurrentFiniteElement* FiniteElementPressure() const;
      };

    } // namespace Hyperelasticity

  } // namespace Private

} // namespace felisce

#endif /* defined(___FELISCE__PressureData__) */
