//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef ___FELISCE__PressureDataIncompressible__
# define ___FELISCE__PressureDataIncompressible__

// System includes

// External includes

// Project includes
#include "Hyperelasticity/pressureData.hpp"

namespace felisce {

  // Forward declarations.
  class ListVariable;
  class ListCurrentFiniteElement;
  class CurrentFiniteElement;

  namespace Private {

    namespace Hyperelasticity {

      class PressureDataIncompressible
        : public PressureData
      {
      public:
        /// Pointer definition of PressureData
        FELISCE_CLASS_POINTER_DEFINITION(PressureDataIncompressible);

        // Constructor.
        PressureDataIncompressible();

        // Destructor.
        ~PressureDataIncompressible() override = default;

        /*!
         * \brief Init the data related to pressure.
         *
         * \param[in] list_variable LinearProblem::m_listVariable expected here.
         * \param[in] list_finite_element LinearProblem::m_listCurrentFiniteElement expected here.
         */
        void Init(const ListVariable& list_variable, ListCurrentFiniteElement& list_finite_element) override;

        // Return the index related to pressure.
        int IndexPressure() const override;

        // If the formulation is compressible or not
        bool IsCompressible() const override;

        // Return the finite element related to pressure.
        CurrentFiniteElement* FiniteElementPressure() const override;

      protected:

        // Index related to pressure.
        int m_iPressure;

        // Finite element related to pressure.
        CurrentFiniteElement* m_fePressure;

      };

    } // namespace Hyperelasticity

  } // namespace Private

} // namespace felisce

#endif /* defined(___FELISCE__PressureDataIncompressible__) */
