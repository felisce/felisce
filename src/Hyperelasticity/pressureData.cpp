//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes

// External includes

// Project includes
#include "DegreeOfFreedom/listVariable.hpp"
#include "FiniteElement/listCurrentFiniteElement.hpp"
#include "Hyperelasticity/pressureData.hpp"

namespace felisce::Private::Hyperelasticity
{

PressureData::PressureData()
{

}

/***********************************************************************************/
/***********************************************************************************/

void PressureData::Init(const ListVariable& list_variable, ListCurrentFiniteElement& list_finite_element)
{
  IGNORE_UNUSED_ARGUMENT(list_variable);
  IGNORE_UNUSED_ARGUMENT(list_finite_element);
}

/***********************************************************************************/
/***********************************************************************************/

int PressureData::IndexPressure() const
{
  return 0;
}

/***********************************************************************************/
/***********************************************************************************/

bool PressureData::IsCompressible() const
{
  return false;
}

/***********************************************************************************/
/***********************************************************************************/

CurrentFiniteElement* PressureData::FiniteElementPressure() const
{
  return nullptr;
}

} // namespace felisce
