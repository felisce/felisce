//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef _FELISCE_InvariantHelper_hpp
#define _FELISCE_InvariantHelper_hpp

// System includes

// External includes

// Project includes

namespace felisce {

  namespace Invariants {

    namespace Private {

      template<unsigned int NcomponentT>
      class Component {
      public:

        // Set components
        void SetComponents(std::array<double, NcomponentT> components)
        {
          m_components = std::move(components);
        }

        /*!
         * \brief Return the array containing all components.
         *
         * Beware because it assumes you know which is the ordering used, contrary to individual components above.
         */
        std::array<double, NcomponentT> components() const
        {
          return std::move(m_components);
        }

      protected:

        // Constructor.
        Component() = default;

        // Destructor.
        ~Component() = default;


      protected:

        /*!
         * \brief Components if the Cauchy Green tensor.
         *
         * They are stored in the following order: xx, yy, xy for 2D and xx, yy, zz, xy, yz, xz for 3D.
         */
        std::array<double, NcomponentT> m_components;

      };
    } // namespace Private

  } // namespace Invariants

} // namespace felisce

#endif
