//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef __FELISCE_HYPER_ELASTICITY_LAWS_CIARLET_GEYMONAT_HPP
# define __FELISCE_HYPER_ELASTICITY_LAWS_CIARLET_GEYMONAT_HPP

// System includes

// External includes

// Project includes
# include "HyperelasticityLaws/HyperElasticLaw.hpp"

namespace felisce 
{
/*!
  * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw
  */
class CiarletGeymonat 
  : public HyperElasticLaw
{
public:
  ///@name Type Definitions
  ///@{

  /// Base class definition
  typedef HyperElasticLaw BaseClassType;

  /// Pointer definition of CiarletGeymonat
  FELISCE_CLASS_POINTER_DEFINITION(CiarletGeymonat);

  ///@}
  ///@name Life Cycle
  ///@{

  // Constructor
  explicit CiarletGeymonat();

  // Destructor
  ~CiarletGeymonat() override = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /*!
    * \brief Initialise the arguments kappa1, kappa2 and bulk from the input parameter file.
    *
    * If left undefined there will be an exception when the methods are called.
    */
  void InitParameters() override;

  // Function W
  double W() const override;

  // Derivative of W with respect of first invariant (dWdI1)
  double FirstDerivativeWFirstInvariant() const override;

  // Derivative of W with respect of second invariant (dWdI2)
  double FirstDerivativeWSecondInvariant() const override;

  // Derivative of W with respect of third invariant (dWdI3)
  double FirstDerivativeWThirdInvariant() const override;

  // Second derivative of W with respect of first invariant (d2WdI1dI1)
  double SecondDerivativeWFirstInvariant() const override;

  // Second derivative of W with respect of second invariant (d2WdI2dI2)
  double SecondDerivativeWSecondInvariant() const override;

  // Second derivative of W with respect of third invariant (d2WdI3dI3)
  double SecondDerivativeWThirdInvariant() const override;

  // Second derivative of W with respect of first and second invariant (d2WdI1dI2)
  double SecondDerivativeWFirstAndSecondInvariant() const override;

  // Second derivative of W with respect of first and third invariant (d2WdI1dI3)
  double SecondDerivativeWFirstAndThirdInvariant() const override;

  // Second derivative of W with respect of second and third invariant (d2WdI2dI3)
  double SecondDerivativeWSecondAndThirdInvariant() const override;

  // Function Wc (for incompressible case).
  double Wc(const double pressure) const override;

  // Derivative of Wc with respect of third invariant (dWcdI3) (for incompressible case).
  double FirstDerivativeWcThirdInvariant(const double pressure) const override;

  // Second derivative of Wc with respect of third invariant (d2WcdI3dI3) (for incompressible case).
  double SecondDerivativeWcThirdInvariant(const double pressure) const override;
  
  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /**
   * @brief This returns the name of the hyperlastic law
   * @return The name of the hyperlastic law
   */
  std::string name() override
  {
    return "CiarletGeymonat";
  } 

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  // Forbid recopy, move, etc...
  CiarletGeymonat(const CiarletGeymonat&);

  CiarletGeymonat(CiarletGeymonat&&) noexcept ;

  CiarletGeymonat operator=(const CiarletGeymonat);

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  // Kappa1
  double m_kappa1;

  // Kappa2
  double m_kappa2;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

} // namespace felisce

#endif // __FELISCE_HYPER_ELASTICITY_LAWS_CIARLET_GEYMONAT_HPP_HPP
