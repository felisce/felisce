//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef __FELISCE_HYPER_ELASTICITY_LAW_HPP
# define __FELISCE_HYPER_ELASTICITY_LAW_HPP

// System includes
#include <vector>
#include <array>
#include <bitset>
#include <cmath>
#include <exception>
#include <unordered_map>

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Core/felisce_error.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

struct ErrorUninitializedHyperElasticLaw : public std::exception {
  ~ErrorUninitializedHyperElasticLaw() noexcept override;

  // Default constructor.
  ErrorUninitializedHyperElasticLaw() = default;

  // Copy constructor, to avoid warning due to user-declared destructor.
  ErrorUninitializedHyperElasticLaw(const ErrorUninitializedHyperElasticLaw&) = default;

  const char* what() const noexcept override;
};

class ErrorInvalidParameter : public std::exception {

public:

  /*!
    * \brief Constructor.
    *
    * \param[in] parametersStatus  Specifies which parameters are not properly initialized
    * First index is true if poisson is not properly std::set
    * Second stands for young
    * Third for bulk.
    */
  ErrorInvalidParameter(std::bitset<3> parametersStatus);

  ~ErrorInvalidParameter() noexcept override = default;

  // Copy constructor, to avoid warning due to user-declared destructor.
  ErrorInvalidParameter(const ErrorInvalidParameter&) = default;

  // what() overload
  const char* what() const noexcept override;


private:

  // Message to be displayed by what()
  std::string m_message;
};

/**
 * \brief HyperElasticityLaw law
 */
class HyperElasticLaw
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of HyperElasticLaw
  FELISCE_CLASS_POINTER_DEFINITION(HyperElasticLaw);

  ///@}
  ///@name Life Cycle
  ///@{

  // Constructor
  explicit HyperElasticLaw();

  // Destructor
  virtual ~HyperElasticLaw() = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /*!
    * \brief Initialise the arguments kappa1, kappa2 and bulk from the input parameter file.
    *
    * If left undefined there will be an exception when the methods are called.
    */
  virtual void InitParameters();

  // Function W
  virtual double W() const;

  // Derivative of W with respect of first invariant (dWdI1)
  virtual double FirstDerivativeWFirstInvariant() const;

  // Derivative of W with respect of second invariant (dWdI2)
  virtual double FirstDerivativeWSecondInvariant() const;

  // Derivative of W with respect of third invariant (dWdI3)
  virtual double FirstDerivativeWThirdInvariant() const;

  // Second derivative of W with respect of first invariant (d2WdI1dI1)
  virtual double SecondDerivativeWFirstInvariant() const;

  // Second derivative of W with respect of second invariant (d2WdI2dI2)
  virtual double SecondDerivativeWSecondInvariant() const;

  // Second derivative of W with respect of third invariant (d2WdI3dI3)
  virtual double SecondDerivativeWThirdInvariant() const;

  // Second derivative of W with respect of first and second invariant (d2WdI1dI2)
  virtual double SecondDerivativeWFirstAndSecondInvariant() const;

  // Second derivative of W with respect of first and third invariant (d2WdI1dI3)
  virtual double SecondDerivativeWFirstAndThirdInvariant() const;

  // Second derivative of W with respect of second and third invariant (d2WdI2dI3)
  virtual double SecondDerivativeWSecondAndThirdInvariant() const;

  // Function Wc (for incompressible case).
  virtual double Wc(const double pressure) const;

  // Derivative of Wc with respect of third invariant (dWcdI3) (for incompressible case).
  virtual double FirstDerivativeWcThirdInvariant(const double pressure) const;

  // Second derivative of Wc with respect of third invariant (d2WcdI3dI3) (for incompressible case).
  virtual double SecondDerivativeWcThirdInvariant(const double pressure) const;

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief Sets the invariants
   * @details An exception will be thrown if a calculation method is called before this method has been called.
   * Contrary to #InitKappasAndBulk(), this method should be called multiple times in a given problem
   * (for each dof)
   * @param I1 The first invariant
   * @param I2 The first invariant
   * @param I3 The first invariant
   */
  virtual void SetInvariants(const double I1, const double I2, const double I3)
  {
    m_invariants[0] = I1;
    m_invariants[1] = I2;
    m_invariants[2] = I3;
    m_isInitialised.set(static_cast<std::size_t>(index_invariants_));
  }

  /**
   * @brief Sets the invariants
   * @details An exception will be thrown if a calculation method is called before this method has been called.
   * Contrary to #InitKappasAndBulk(), this method should be called multiple times in a given problem
   * (for each dof)
   * @param rInvariants The invariants
   */
  virtual void SetInvariants(const std::array<double, 3>& rInvariants)
  {
    m_invariants = rInvariants;
    m_isInitialised.set(static_cast<std::size_t>(index_invariants_));
  }

  /**
   * @brief Gets the invariants
   * @return The invariants
   */
  std::array<double, 3>& GetInvariants()
  {
    return m_invariants;
  }

  /**
   * @brief Sets the bulk value
   * @param Bult The bulk value
   */
  void SetBulk(const double Bulk)
  {
    m_bulk = Bulk;
  }

  /**
   * @brief Gets the bulk value
   * @return The bulk value
   */
  double& GetBulk()
  {
    return m_bulk;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /**
   * @brief This returns the name of the hyperlastic law
   * @return The name of the hyperlastic law
   */
  virtual std::string name()
  {
    return "HyperElasticLaw";
  } 

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  enum InitIndexes {
    index_invariants_ = 0,
    index_kappaAndBulk_ = 1,
    NinitConditions_ = 2
  };

  // Vector storing the invariants.
  std::array<double, 3> m_invariants;

  // Bulk
  double m_bulk;

  /*!
    * \brief Each bitset describes whether a quantity has been initialized or not.
    *
    * Meaning of each bitset is stored in InitIndexes.
    */
  std::bitset<static_cast<std::size_t>(NinitConditions_)> m_isInitialised;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  // Throws an exception if a method is called without initialisation properly completed.
  // \todo: At the moment it is always performed; later it should be checked only in debug mode
  void checkInitDone() const;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  // Forbid recopy, move, etc...
  HyperElasticLaw(const HyperElasticLaw&);

  HyperElasticLaw(HyperElasticLaw&&) noexcept ;

  ///@}
  ///@name Private Operators
  ///@{

  HyperElasticLaw operator=(const HyperElasticLaw);

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

/**
 * \brief HyperElasticityLaw database
 */
class HyperElasticityLawDatabase
{
public:
  ///@name Type Definitions
  ///@{

  /// The database type
  typedef std::unordered_map<std::string, const HyperElasticLaw::Pointer> HyperElasticityLawDatabaseType;

  ///@}
  ///@name Life Cycle
  ///@{

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  ///@}
  ///@name Static Member Variables
  ///@{

  static HyperElasticityLawDatabaseType DatabaseOfHyperelasticLaws;

  ///@}
};

/**
 * \brief HyperElasticityLaw manager
 */
class HyperElasticityLawManager
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of HyperElasticityLawManager
  FELISCE_CLASS_POINTER_DEFINITION(HyperElasticityLawManager);

  /// The database type
  typedef std::unordered_map<int, HyperElasticLaw::Pointer> HyperElasticityLawManagerType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor
  HyperElasticityLawManager();

  /// Default destructor
  ~HyperElasticityLawManager() = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Retrieves the HyperElasticLaw corresponding to the given Label
   * @param LabelElement The label corresponding to the element
   */
  HyperElasticLaw::Pointer GetHyperElasticLaw(const int LabelElement);

  /**
   * @brief Inits all the laws
   */
  void InitParameters();

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief Gets the invariants
   * @return The invariants
   */
  HyperElasticityLawManagerType& GetHyperElasticityLawManager()
  {
    return mDatabaseOfHyperelasticLaws;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{
  
  HyperElasticityLawManagerType mDatabaseOfHyperelasticLaws; /// The database of the hyperelasticlaws

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

///@}

} // namespace felisce

#endif // __FELISCE_HYPER_ELASTICITY_LAW_HPP
