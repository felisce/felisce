//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes
#include <string>

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Core/felisceParam.hpp"
#include "HyperelasticityLaws/HyperElasticLaw.hpp"
#include "HyperelasticityLaws/stVenantKirchhoff.hpp"
#include "HyperelasticityLaws/ciarletGeymonat.hpp"
#include "HyperelasticityLaws/Ogden.hpp"

namespace felisce
{

HyperElasticityLawDatabase::HyperElasticityLawDatabaseType HyperElasticityLawDatabase::DatabaseOfHyperelasticLaws = {
  std::make_pair("StVenantKirchhoff" , felisce::make_shared<StVenantKirchhoff>()),
  std::make_pair("CiarletGeymonat"   , felisce::make_shared<CiarletGeymonat>()),
  std::make_pair("Ogden"             , felisce::make_shared<Ogden>())
  };

/***********************************************************************************/
/***********************************************************************************/

ErrorInvalidParameter::ErrorInvalidParameter(std::bitset<3> parametersStatus)
{
  std::ostringstream oconv;
  oconv << "The following parameters were not properly set in the input parameters file (in [solid]): ";

  // Aesthetic note: I don't like to put a condition on a single line, but XCode seems lost with indent otherwise.
  if (parametersStatus[0]) {
    oconv << "poisson  ";
  }
  if (parametersStatus[1]) {
    oconv << "young  ";
  }
  if (parametersStatus[2]) {
    oconv << "bulk";
  }

  m_message = oconv.str();
}

/***********************************************************************************/
/***********************************************************************************/

const char* ErrorInvalidParameter::what() const noexcept
{
  return m_message.c_str();
}

/***********************************************************************************/
/***********************************************************************************/

const char* ErrorUninitializedHyperElasticLaw::what() const noexcept
{
  return "HyperelasticLaw was not correctly initialised; make sure Young modulus, Poisson coefficient, "
          " dimension, hyperelasticity bulk and so forth were properly initialized (through InitParameters() and "
          " SetInvariants()).";
}

/***********************************************************************************/
/***********************************************************************************/

ErrorUninitializedHyperElasticLaw::~ErrorUninitializedHyperElasticLaw() noexcept
= default;

/***********************************************************************************/
/***********************************************************************************/

HyperElasticLaw::HyperElasticLaw()
  : m_bulk(1.e20)
{
  FEL_ASSERT(m_isInitialised.none()); // standard should initialise bitset with all bits unset
}

/***********************************************************************************/
/***********************************************************************************/

void HyperElasticLaw::InitParameters()
{
  m_bulk = FelisceParam::instance().hyperelastic_bulk;
  m_isInitialised.set(1); // index_kappaAndBulk_
}

/***********************************************************************************/
/***********************************************************************************/

void HyperElasticLaw::checkInitDone() const
{
  FEL_ASSERT_EQUAL(m_invariants.size(), 3);

  if (!m_isInitialised.all()) {
    throw ErrorUninitializedHyperElasticLaw();
  }
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::W() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::FirstDerivativeWFirstInvariant() const {
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::FirstDerivativeWSecondInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::FirstDerivativeWThirdInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWFirstInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWSecondInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWThirdInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWFirstAndThirdInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWSecondAndThirdInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWFirstAndSecondInvariant() const
{
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::Wc(const double pressure) const
{
  IGNORE_UNUSED_ARGUMENT(pressure);
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::FirstDerivativeWcThirdInvariant(const double pressure) const
{
  IGNORE_UNUSED_ARGUMENT(pressure);
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double HyperElasticLaw::SecondDerivativeWcThirdInvariant(const double pressure) const
{
  IGNORE_UNUSED_ARGUMENT(pressure);
  checkInitDone();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

HyperElasticityLawManager::HyperElasticityLawManager()
{
  const auto& r_instance = FelisceParam::instance();
  std::string hyperelastic_law;
  int counter_label = 0;
  mDatabaseOfHyperelasticLaws.insert({0, nullptr});
  for (std::size_t i = 0; i < r_instance.hyperElasticLaw.type.size(); ++i) {
    hyperelastic_law = r_instance.hyperElasticLaw.type[i];
    HyperElasticLaw::Pointer hyperElasticLaw = HyperElasticityLawDatabase::DatabaseOfHyperelasticLaws.find(hyperelastic_law) != HyperElasticityLawDatabase::DatabaseOfHyperelasticLaws.end() ? HyperElasticityLawDatabase::DatabaseOfHyperelasticLaws[hyperelastic_law] : nullptr;
    if (r_instance.hyperElasticLaw.label.size() == 0) {
      FEL_ASSERT(i == 0);
      mDatabaseOfHyperelasticLaws[0] = hyperElasticLaw;
    } else {
      const int num_label = r_instance.hyperElasticLaw.numLabel[i];
      for (int j = 0; j < num_label; ++j) {
        const int label = r_instance.hyperElasticLaw.label[counter_label];
        if (mDatabaseOfHyperelasticLaws.find(label) != mDatabaseOfHyperelasticLaws.end()) {
          mDatabaseOfHyperelasticLaws[label] = hyperElasticLaw;
        } else {
          mDatabaseOfHyperelasticLaws.insert({label, hyperElasticLaw});
        }
        ++counter_label;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void HyperElasticityLawManager::InitParameters()
{
  // Init all laws
  for (auto& r_law : mDatabaseOfHyperelasticLaws) {
    r_law.second->InitParameters();
  }
}

/***********************************************************************************/
/***********************************************************************************/

HyperElasticLaw::Pointer HyperElasticityLawManager::GetHyperElasticLaw(const int LabelElement)
{
  auto it_law = mDatabaseOfHyperelasticLaws.find(LabelElement);
  if (it_law != mDatabaseOfHyperelasticLaws.end()) {
    return it_law->second;
  } else { // Default is label 0
    return mDatabaseOfHyperelasticLaws[0];
  }
}

} // namespace felisce
