//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <string>
#include <cmath>
#include <limits>

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Core/felisceParam.hpp"
#include "HyperelasticityLaws/Ogden.hpp"

namespace felisce
{

Ogden::Ogden()
  : BaseClassType(),
    m_C1(1.e20), // silly value on purpose
    m_C2(1.e20),
    m_a(1.e20)
{
}

/***********************************************************************************/
/***********************************************************************************/

void Ogden::InitParameters()
{
  // Call base class
  BaseClassType::InitParameters();

  const auto& r_ogden = FelisceParam::instance().Ogden;
  m_C1 = r_ogden.C1;
  m_C2 = r_ogden.C2;
  m_a  = r_ogden.a;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::W() const
{
  BaseClassType::W();

  const double I1 = HyperElasticLaw::m_invariants[0];
  const double I2 = HyperElasticLaw::m_invariants[1];
  const double I3 = HyperElasticLaw::m_invariants[2];

  // Law:
  //    W = C1*(I1-3)+C2*(I2-3)+a*(I3-1)-(C1+2*C2+a)*Log[I3]

  return m_C1 * (I1 - 3.0) + m_C2 * (I2 - 3.0) + m_a * (I3 - 1.0) - (m_C1 + 2.0 * m_C2 + m_a) * std::log(I3);
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::FirstDerivativeWFirstInvariant() const
{
  BaseClassType::FirstDerivativeWFirstInvariant();
  return m_C1;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::FirstDerivativeWSecondInvariant() const
{
  BaseClassType::FirstDerivativeWSecondInvariant();
  return m_C2;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::FirstDerivativeWThirdInvariant() const
{
  BaseClassType::FirstDerivativeWThirdInvariant();

  const double I3 = HyperElasticLaw::m_invariants[2];
  FEL_ASSERT("I3 should be strictly positive! (and more precisely somewhere in the vicinity of 1.)"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  return  m_a - (m_C1 + 2.0 * m_C2 + m_a)/I3;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::SecondDerivativeWFirstInvariant() const
{
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::SecondDerivativeWSecondInvariant() const
{
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::SecondDerivativeWThirdInvariant() const
{
  BaseClassType::SecondDerivativeWThirdInvariant();

  const double I3 = HyperElasticLaw::m_invariants[2];

  FEL_ASSERT("I3 should be strictly positive!"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  return (m_C1 + 2.0 * m_C2 + m_a)/std::pow(I3, 2);
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::SecondDerivativeWFirstAndSecondInvariant() const
{
  BaseClassType::SecondDerivativeWFirstAndSecondInvariant();

  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::SecondDerivativeWFirstAndThirdInvariant() const
{
  BaseClassType::SecondDerivativeWFirstAndThirdInvariant();

  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double Ogden::SecondDerivativeWSecondAndThirdInvariant() const
{
  BaseClassType::SecondDerivativeWSecondAndThirdInvariant();

  return 0.0;
}

} // namespace felisce
