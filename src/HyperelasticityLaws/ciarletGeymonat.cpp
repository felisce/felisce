//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes
#include <string>
#include <limits>

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Core/felisceParam.hpp"
#include "HyperelasticityLaws/ciarletGeymonat.hpp"

namespace felisce
{
CiarletGeymonat::CiarletGeymonat()
  : BaseClassType(),
    m_kappa1(1.e20), // silly value on purpose
    m_kappa2(1.e20)
{
}

/***********************************************************************************/
/***********************************************************************************/

void CiarletGeymonat::InitParameters()
{
  // Call base class
  BaseClassType::InitParameters();

  const auto& r_kappas = FelisceParam::instance().CiarletGeymonat;
  m_kappa1 = r_kappas.kappa1;
  m_kappa2 = r_kappas.kappa2;

  // Check the values were correctly initialized in the input file
  {
    std::bitset<3> are_parameters_unused;
    if (m_kappa1 < 0.)
      are_parameters_unused.set(0);
    if (m_kappa2 < 0.)
      are_parameters_unused.set(1);
    if (m_bulk < 0.)
      are_parameters_unused.set(2);

    if (are_parameters_unused.any())
      throw ErrorInvalidParameter(are_parameters_unused);
  }
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::W() const
{
  BaseClassType::W();

  const double I1 = HyperElasticLaw::m_invariants[0];
  const double I2 = HyperElasticLaw::m_invariants[1];
  const double I3 = HyperElasticLaw::m_invariants[2];
  const double I3_pow_minus_one_third = std::pow(I3, -1.0/3.0);

  return m_kappa1 * (I1 * I3_pow_minus_one_third - 3.0)
          + m_kappa2 * (I2 * std::pow(I3_pow_minus_one_third, 2) - 3.0);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::FirstDerivativeWFirstInvariant() const
{
  BaseClassType::FirstDerivativeWFirstInvariant();
  return m_kappa1 * std::pow(HyperElasticLaw::m_invariants[2], -1.0/3.0);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::FirstDerivativeWSecondInvariant() const
{
  BaseClassType::FirstDerivativeWSecondInvariant();
  return m_kappa2 * std::pow(HyperElasticLaw::m_invariants[2], -2.0/3.0);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWFirstInvariant() const
{
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWSecondInvariant() const
{
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWFirstAndSecondInvariant() const
{
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::FirstDerivativeWThirdInvariant() const
{
  BaseClassType::FirstDerivativeWThirdInvariant();

  const double I1 = HyperElasticLaw::m_invariants[0];
  const double I2 = HyperElasticLaw::m_invariants[1];
  const double I3 = HyperElasticLaw::m_invariants[2];

  FEL_ASSERT("I3 should be strictly positive! (and more precisely somewhere in the vicinity of 1.)"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  const double minus_one_third = -1.0/3.0;

  return m_kappa1 * I1 * minus_one_third * std::pow(I3, 4. * minus_one_third)
          + m_kappa2 * I2 * 2. * minus_one_third * std::pow(I3, 5. * minus_one_third);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWThirdInvariant() const
{
  BaseClassType::SecondDerivativeWThirdInvariant();

  const double I1 = HyperElasticLaw::m_invariants[0];
  const double I2 = HyperElasticLaw::m_invariants[1];
  const double I3 = HyperElasticLaw::m_invariants[2];
  const double minus_one_third = -1.0/3.0;
  const double one_ninth = 1.0/9.0;

  FEL_ASSERT("I3 should be strictly positive!"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  return m_kappa1 * I1 * 4.0 * one_ninth * std::pow(I3, 7.0 * minus_one_third)
          + m_kappa2 * I2 * 10.0 * one_ninth * std::pow(I3, 8.0 * minus_one_third);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWFirstAndThirdInvariant() const
{
  BaseClassType::SecondDerivativeWFirstAndThirdInvariant();

  const double I3 = HyperElasticLaw::m_invariants[2];
  const double minus_one_third = -1.0/3.0;

  return m_kappa1 * minus_one_third * std::pow(I3, 4.0 * minus_one_third);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWSecondAndThirdInvariant() const
{
  BaseClassType::SecondDerivativeWSecondAndThirdInvariant();
  const double I3 = HyperElasticLaw::m_invariants[2];
  const double minus_one_third = -1.0/3.0;

  return m_kappa2 * 2. * minus_one_third * std::pow(I3, 5.0 * minus_one_third);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::Wc(const double pressure) const
{
  BaseClassType::Wc(pressure);
  const double I3 = HyperElasticLaw::m_invariants[2];
  FEL_ASSERT("I3 should be strictly positive! (and more precisely somewhere in the vicinity of 1.)"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  return pressure * (std::sqrt(I3) - 1.0 - 0.5 * std::log(I3));
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::FirstDerivativeWcThirdInvariant(const double pressure) const
{
  BaseClassType::FirstDerivativeWcThirdInvariant(pressure);
  const double I3 = HyperElasticLaw::m_invariants[2];
  FEL_ASSERT("I3 should be strictly positive! (and more precisely somewhere in the vicinity of 1.)"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  return pressure * (0.5/std::sqrt(I3) - 0.5/I3);
}

/***********************************************************************************/
/***********************************************************************************/

double CiarletGeymonat::SecondDerivativeWcThirdInvariant(const double pressure) const
{
  BaseClassType::SecondDerivativeWcThirdInvariant(pressure);
  const double I3 = HyperElasticLaw::m_invariants[2];
  FEL_ASSERT("I3 should be strictly positive! (and more precisely somewhere in the vicinity of 1.)"  && std::abs(I3) > std::numeric_limits<double>::epsilon());

  return pressure * (-0.25 * std::pow(I3, -1.5) + 0.5/std::pow(I3, 2));
}

} // namespace felisce

