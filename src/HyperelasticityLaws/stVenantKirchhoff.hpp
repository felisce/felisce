//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

#ifndef __FELISCE_HYPER_ELASTICITY_LAWS_ST_VENANT_KIRCHHOFF_HPP
# define __FELISCE_HYPER_ELASTICITY_LAWS_ST_VENANT_KIRCHHOFF_HPP

// System includes

// External includes

// Project includes
# include "HyperelasticityLaws/HyperElasticLaw.hpp"

namespace felisce 
{
/*!
  * \brief StVenant-Kirchhoff laws, to use a a policy of class HyperElasticityLaw.
  *
  * Note: this class defines the St venant Kirchhoff laws using invariants, thus complying with the generic
  * interface of ElasticityModel class. However, these laws could be defined without those invariants, and
  * this definition would be probably better in term of calculation time. So if Saint Venant Kirchhoff laws are
  * to be used extensively, the possibility to implement them without invariants should remain open.
  */
class StVenantKirchhoff 
  : public HyperElasticLaw
{
public:
  ///@name Type Definitions
  ///@{

  /// Base class definition
  typedef HyperElasticLaw BaseClassType;

  /// Pointer definition of StVenantKirchhoff
  FELISCE_CLASS_POINTER_DEFINITION(StVenantKirchhoff);

  ///@}
  ///@name Life Cycle
  ///@{

  // Constructor.
  explicit StVenantKirchhoff();

  // Destructor
  ~StVenantKirchhoff() override = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /*!
    * \brief Initialise the arguments kappa1, kappa2 and bulk from the input parameter file.
    *
    * If left undefined there will be an exception when the methods are called.
    */
  void InitParameters() override;

  // Function W
  double W() const override;

  // Derivative of W with respect of first invariant (dWdI1)
  double FirstDerivativeWFirstInvariant() const override;

  // Derivative of W with respect of second invariant (dWdI2)
  double FirstDerivativeWSecondInvariant() const override;

  // Derivative of W with respect of third invariant (dWdI3)
  double FirstDerivativeWThirdInvariant() const override;

  // Second derivative of W with respect of first invariant (d2WdI1dI1)
  double SecondDerivativeWFirstInvariant() const override;

  // Second derivative of W with respect of second invariant (d2WdI2dI2)
  double SecondDerivativeWSecondInvariant() const override;

  // Second derivative of W with respect of third invariant (d2WdI3dI3)
  double SecondDerivativeWThirdInvariant() const override;

  // Second derivative of W with respect of first and second invariant (d2WdI1dI2)
  double SecondDerivativeWFirstAndSecondInvariant() const override;

  // Second derivative of W with respect of first and third invariant (d2WdI1dI3)
  double SecondDerivativeWFirstAndThirdInvariant() const override;

  // Second derivative of W with respect of second and third invariant (d2WdI2dI3)
  double SecondDerivativeWSecondAndThirdInvariant() const override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /**
   * @brief This returns the name of the hyperlastic law
   * @return The name of the hyperlastic law
   */
  std::string name() override
  {
    return "StVenantKirchhoff";
  } 
  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected Life Cycle
  ///@{

  ///@}
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private Life Cycle
  ///@{

  // Forbid recopy, move, etc...
  StVenantKirchhoff(const StVenantKirchhoff&);

  StVenantKirchhoff(StVenantKirchhoff&&) noexcept ;

  StVenantKirchhoff operator=(const StVenantKirchhoff);

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  // Lame coefficient lambda.
  double m_lame_lambda;

  // Lame coefficient mu.
  double m_lame_mu;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

} // namespace felisce

#endif // __FELISCE_HYPER_ELASTICITY_LAWS_ST_VENANT_KIRCHHOFF_HPP_HPP
