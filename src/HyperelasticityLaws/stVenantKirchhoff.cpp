//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes
#include <string>
#include <limits>

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "Core/felisceParam.hpp"
#include "HyperelasticityLaws/stVenantKirchhoff.hpp"

namespace felisce 
{
StVenantKirchhoff::StVenantKirchhoff()
  : BaseClassType(),
    m_lame_lambda(1.e20), // silly value on purpose
    m_lame_mu(1.e20)
{
}

/***********************************************************************************/
/***********************************************************************************/

void StVenantKirchhoff::InitParameters() 
{
  // Call base class
  BaseClassType::InitParameters();

  const double poisson_ratio = FelisceParam::instance().poisson;
  const double young_modulus = FelisceParam::instance().young;

  // Check the values were correctly initialized in the input file
  {
    std::bitset<3> are_parameters_unused;
    if (poisson_ratio < 0.)
      are_parameters_unused.set(0);
    if (young_modulus < 0.)
      are_parameters_unused.set(1);
    if (m_bulk < 0.)
      are_parameters_unused.set(2);

    if (are_parameters_unused.any())
      throw ErrorInvalidParameter(are_parameters_unused);
  }

  m_lame_lambda = poisson_ratio * young_modulus / ((1. + poisson_ratio) * (1. - 2. * poisson_ratio));
  m_lame_mu = young_modulus * 0.5 / (1. + poisson_ratio);
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::W() const 
{
  BaseClassType::W();

  const double I1 = HyperElasticLaw::m_invariants[0];
  const double I2 = HyperElasticLaw::m_invariants[1];

  // Law: St-Venant - Kirchhoff
  //       W = lambda(Tr(E))^2 + mu Tr(E^2)
  //       W=e1/8(gi1-3)^2+e2/4(gi1^2-2gi2-2gi1+3)
  return (0.125 * m_lame_lambda+ 0.25 * m_lame_mu) * std::pow(I1, 2)
          - (0.75 * m_lame_lambda+ 0.5 * m_lame_mu) * I1
          - 0.5 * m_lame_mu* I2
          + 1.125 * m_lame_lambda+ 0.75 * m_lame_mu;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::FirstDerivativeWFirstInvariant() const 
{
  BaseClassType::FirstDerivativeWFirstInvariant();
  const double I1 = HyperElasticLaw::m_invariants[0];

  return (0.25 * m_lame_lambda+ 0.5 * m_lame_mu) * I1
          - (0.75 * m_lame_lambda+ 0.5 * m_lame_mu);
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::FirstDerivativeWSecondInvariant() const 
{
  BaseClassType::FirstDerivativeWSecondInvariant();
  return - 0.5 * m_lame_mu;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::FirstDerivativeWThirdInvariant() const 
{
  BaseClassType::FirstDerivativeWThirdInvariant();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::SecondDerivativeWFirstInvariant() const 
{
  BaseClassType::SecondDerivativeWFirstInvariant();
  return (0.25 * m_lame_lambda+ 0.5 * m_lame_mu);
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::SecondDerivativeWSecondInvariant() const 
{
  BaseClassType::SecondDerivativeWSecondInvariant();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::SecondDerivativeWThirdInvariant() const 
{
  BaseClassType::SecondDerivativeWThirdInvariant();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::SecondDerivativeWFirstAndThirdInvariant() const 
{
  BaseClassType::SecondDerivativeWFirstAndThirdInvariant();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::SecondDerivativeWSecondAndThirdInvariant() const 
{
  BaseClassType::SecondDerivativeWSecondAndThirdInvariant();
  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

double StVenantKirchhoff::SecondDerivativeWFirstAndSecondInvariant() const 
{
  BaseClassType::SecondDerivativeWFirstAndSecondInvariant();
  return 0.0;
}

} // namespace felisce

