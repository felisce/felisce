//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <mpi.h>

// External includes

// Project includes
#include "InputOutput/ensight.hpp"
#include "InputOutput/io.hpp"

int main(int argc, char** argv)
{

  int dim = 3;
  std::vector<double> values(dim*2*45);
  int id_variable = 0;
  int index_time = 1000;

  std::string input_directory = "/Users/dcorti/Desktop/3D_valve/Results/ThinShell/PostS/";
  std::string input_case_file = "struct.case";
  std::string mesh_file       = "solid";

  felisce::EnsightCase reader;

  // Read case file
  reader.read(input_directory,input_case_file);

  // Get values
  reader.readVariable(id_variable, index_time, values.data(), values.size());

  // Parameters instance
  auto& r_instance = felisce::FelisceParam::instance();
  r_instance.readNodesReferences = false;

  // Open mesh
  auto p_io          = felisce::make_shared<felisce::IO>( input_directory, mesh_file + ".geo",  mesh_file + ".geo",  mesh_file + ".out.geo",  input_directory,  input_directory, "ensight");
  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();
  p_io->readMesh(*p_mesh_region, 1.0);

  // Set mesh dimension 
  p_mesh_region->numCoor() = dim;

  // Move mesh
  p_mesh_region->moveMesh(values, 1.0);

  // Convert into meshb format
  auto p_io_mesh_post = felisce::make_shared<felisce::IO>( input_directory, mesh_file + ".mesh", mesh_file + ".mesh", mesh_file + ".out.mesh", input_directory,  input_directory, "mesh");
  p_io_mesh_post->writeMesh(*p_mesh_region, mesh_file + ".out.mesh");  
  
  return 0;
}