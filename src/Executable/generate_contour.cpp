//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/contour_utilities.hpp"
#include "Tools/simple_cmd.hpp"

int main(int argc, const char* argv[])
{
  struct Options
  {
    std::string InputFile{""};
    bool ImposeParentReferences{false};
  };

  auto parser = CmdOptions<Options>::Create({
    {"--file", &Options::InputFile },
    {"--parent_references", &Options::ImposeParentReferences }
  });

  auto options = parser->parse(argc, argv);

  if (options.InputFile == "") {
    std::cout << "The following input values must be set: " << std::endl;
    std::cout << "\tInput file:\t--file"<< std::endl;
    std::cout << "\tParent references:\t--parent_references"<< std::endl;
    return 1;
  }

  std::cout << "The following input values has been considered" << std::endl;
  std::cout << "\tInput file =\t" << options.InputFile << std::endl;
  std::cout << "\tParent references imposed =\t" << options.ImposeParentReferences << std::endl;

  std::string current_path = std::filesystem::current_path();
  current_path += "/";
  auto p_io = felisce::make_shared<felisce::IO>( current_path,  options.InputFile + ".mesh" , options.InputFile + ".mesh", options.InputFile + ".out.mesh",  current_path,  current_path, "extrude_");
  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();
  p_io->readMesh(*p_mesh_region, 1.0);

  // Generate the contour
  felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region, options.ImposeParentReferences);

  // Export mesh
  p_io->writeOutput(*p_mesh_region, 0);


  return 0;
}
