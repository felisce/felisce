//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/simple_cmd.hpp"

int main(int argc, const char* argv[])
{


  //--- Read current path
  std::string current_path = std::filesystem::current_path();
  current_path += "/";

  //--- Read variable from file
  felisce::EnsightCase fl_ensight_case, st_ensight_case;

  // Create case object
  fl_ensight_case.read(current_path+"PostF", "fluid.case");
  st_ensight_case.read(current_path+"PostS", "struct.case");

  const int nbSteps = std::min(fl_ensight_case.timeSet(0).nbSteps(), st_ensight_case.timeSet(0).nbSteps()); 

  // Loop over time step
  for (auto time = 0; time < nbSteps; ++time){

    // Create mesh
    auto fl_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    // Create reader for mesh
    felisce::Ensight fl_ensight("", "", fl_ensight_case.geometryFile(time), "", current_path+"/PostF/", "");
    
    // Read mesh
    fl_ensight.readerGeo(*fl_mesh_region, 1.0);
    
    // Detect connected components
    // For every ElementType we have a vector specifying the if of the connected component for every element
    using ElementType = felisce::GeometricMeshRegion::ElementType;
    std::map<ElementType, std::vector<std::size_t>> RefToElements; 
    fl_mesh_region->computeConnectedComponents(RefToElements);

    // Get the vertices corresponding to the connected component selected
    std::set<felisce::felInt> list_vertices;
    for (auto it = RefToElements.begin(); it != RefToElements.end(); ++it){
      std::vector<felisce::felInt> list_id_points(felisce::GeometricMeshRegion::m_numVerticesPerElt[it->first]);

      auto& list_elements = it->second;
      for (std::size_t i = 0; i < list_elements.size(); ++i){
        if (list_elements[i] == 2){
          fl_mesh_region->getOneElement(it->first, i, list_id_points);
          
          list_vertices.insert(list_id_points.begin(),list_id_points.end());
        }
      }
    }

    // Allocate vectors to read variables
    std::vector<double> fl_values(fl_mesh_region->numPoints());
    std::vector<double> st_values(1); // todo

    // Read variable
    fl_ensight_case.readVariable(1, time, fl_values.data(), fl_values.size());
    st_ensight_case.readVariable(2, time, st_values.data(), 1);
    
    // Change values
    for (auto it = list_vertices.begin(); it != list_vertices.end(); ++it){
      fl_values[*it] -= st_values[0];
    }
    
    // Set variable size
    fl_ensight_case.variable(1).setSize(fl_values.size());
    fl_ensight_case.variable(1).setSolution(fl_values.data());

    // Write
    fl_ensight_case.writeVariable(current_path+"PostF", "pressure", time);
  }

  return 0;
}
