//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Tools/ksp_solver.hpp"
#include "Tools/simple_cmd.hpp"

int main(int argc, const char* argv[])
{
  struct Options
  {
    std::string InputFileMatrix{""};
    std::string InputFileVector{""};
    std::string SolverType{"MUMPS"};
    std::string PreconditionerType{""};
  };

  auto parser = CmdOptions<Options>::Create({
    {"--file_matrix", &Options::InputFileMatrix },
    {"--file_vector", &Options::InputFileVector },
    {"--solver_type", &Options::SolverType },
    {"--preconditioner_type", &Options::PreconditionerType }
  });

  auto options = parser->parse(argc, argv);

  if (options.InputFileMatrix == "") {
    std::cout << "The following input values must be set: " << std::endl;
    std::cout << "\tInput file matrix:\t--file_matrix"<< std::endl;
    return 1;
  }
  if (options.InputFileVector == "") {
    options.InputFileVector = options.InputFileMatrix;
    std::cout << "\tAssuming same file for matrix and vector"<< std::endl;
  }

  std::cout << "The following input values has been considered" << std::endl;
  std::cout << "\tInput file matrix =\t" << options.InputFileMatrix << std::endl;
  std::cout << "\tInput file vector =\t" << options.InputFileVector << std::endl;
  std::cout << "\tSolver type =\t" << options.SolverType << std::endl;
  std::cout << "\tPreconditioner type =\t" << options.PreconditionerType << std::endl;

  std::string current_path = std::filesystem::current_path();
  current_path += "/";
  const int check = SolveMatrix(current_path + options.InputFileMatrix, current_path + options.InputFileVector, options.SolverType, options.PreconditionerType);
  return check;
}
