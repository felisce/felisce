//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/simple_cmd.hpp"

bool replace(std::string& str, const std::string& from, const std::string& to) {
  size_t start_pos = str.rfind(from);
  if(start_pos == std::string::npos)
      return false;
  str.replace(start_pos, from.length(), to);
  return true;
}

int main(int argc, const char* argv[])
{
  struct Options
  {
    std::string InputFile{""};
    bool IncludeNodes{false};
  };

  auto parser = CmdOptions<Options>::Create({
    {"--file", &Options::InputFile },
    {"--include_nodes", &Options::IncludeNodes }
  });

  auto options = parser->parse(argc, argv);

  if (options.InputFile == "") {
    std::cout << "The following input values must be set: " << std::endl;
    std::cout << "\tInput file:\t--file"<< std::endl;
    return 1;
  }

  std::cout << "The following input values has been considered" << std::endl;
  std::cout << "\tInput file =\t" << options.InputFile << std::endl;
  std::cout << "\tInclude nodes =\t" << options.IncludeNodes << std::endl;

  // Parameters instance
  auto& r_instance = felisce::FelisceParam::instance();
  r_instance.readNodesReferences = options.IncludeNodes;

  std::string current_path = std::filesystem::current_path();
  current_path += "/";
  replace(options.InputFile, ".mesh", "");
  auto p_io = felisce::make_shared<felisce::IO>( current_path,  options.InputFile + ".mesh" , options.InputFile + ".mesh", options.InputFile + ".out.mesh",  current_path,  current_path, "mesh");
  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();
  p_io->readMesh(*p_mesh_region, 1.0);

  // Convert into ensight format
  auto p_io_ensigth_post = felisce::make_shared<felisce::IO>( current_path,  options.InputFile + ".geo" , options.InputFile + ".geo", options.InputFile + ".out.geo",  current_path,  current_path, "ensight");
  r_instance.outputFileFormat = felisce::ENSIGHT6;
  p_io_ensigth_post->initializeOutput();
  p_io_ensigth_post->writeMesh(*p_mesh_region, options.InputFile + ".out.geo");
  double time = 0.0;
  p_io_ensigth_post->postProcess(time, 0);

  return 0;
}
