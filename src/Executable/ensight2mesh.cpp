//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    D. C. Corti
//

// System includes

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/simple_cmd.hpp"

bool replace(std::string& str, const std::string& from, const std::string& to) {
  size_t start_pos = str.rfind(from);
  if(start_pos == std::string::npos)
      return false;
  str.replace(start_pos, from.length(), to);
  return true;
}

int main(const int argc, const char** argv)
{
  struct Options
  {
    std::string InputFile{""};
    bool IncludeNodes{false};
    int  Dimension{3};
  };

  auto parser = CmdOptions<Options>::Create({
    {"--file", &Options::InputFile },
    {"--include_nodes", &Options::IncludeNodes },
    {"--dim", &Options::Dimension }
  });

  auto options = parser->parse(argc, argv);

  if (options.InputFile == "") {
    std::cout << "The following input values must be set: " << std::endl;
    std::cout << "\tInput file:\t--file"<< std::endl;
    return 1;
  }

  std::cout << "The following input values has been considered" << std::endl;
  std::cout << "\tInput file =\t" << options.InputFile << std::endl;
  std::cout << "\tInclude nodes =\t" << options.IncludeNodes << std::endl;
  std::cout << "\tDimension =\t" << options.Dimension << std::endl;

  // Parameters instance
  auto& r_instance = felisce::FelisceParam::instance();
  r_instance.readNodesReferences = options.IncludeNodes;

  std::string current_path = std::filesystem::current_path();
  current_path += "/";
  replace(options.InputFile, ".geo", "");
  auto p_io = felisce::make_shared<felisce::IO>( current_path,  options.InputFile + ".geo" , options.InputFile + ".geo", options.InputFile + ".out.geo",  current_path,  current_path, "ensight");
  auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();
  p_io->readMesh(*p_mesh_region, 1.0);

  // Set mesh dimension 
  p_mesh_region->numCoor() = options.Dimension;

  // Convert into meshb format
  auto p_io_mesh_post = felisce::make_shared<felisce::IO>( current_path,  options.InputFile + ".mesh" , options.InputFile + ".mesh", options.InputFile + ".out.mesh",  current_path,  current_path, "mesh");
  p_io_mesh_post->writeMesh(*p_mesh_region, options.InputFile + ".out.mesh");
}