//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

/*!
 \file felisce.hpp
 \authors J-F. Gerbeau
 */

#ifndef _FELISCE_HPP
#define _FELISCE_HPP

// System includes
#include <functional>
#include <unordered_map>
#include <map>
#include <string>

// External includes
#include "Core/NoThirdPartyWarning/Boost/Ublas/ublas.hpp"
#include "Core/NoThirdPartyWarning/Petsc/sys.hpp"

// Project includes
#include "Core/configure.hpp"
#include "Core/felisce_error.hpp"
#include "Geometry/point.hpp"

// To ignore warnings about some unused arguments, use the first definition provided below.
// On the contrary, to activate those warnings,comment (void) and the remaining of the line
#define IGNORE_UNUSED_ARGUMENT(arg) static_cast<void>(arg);

// Specific case of unused arguments that appear very often in the code
// To activate warnings, comment (void) and the remaining of the line
// For instance, #define IGNORE_UNUSED_RANK
#define IGNORE_UNUSED_RANK static_cast<void>(rank)
#define IGNORE_UNUSED_OUTSTR static_cast<void>(outstr)
#define IGNORE_UNUSED_SIZE_PROC static_cast<void>(sizeProc)
#define IGNORE_UNUSED_NUM_PROC static_cast<void>(numProc)
#define IGNORE_UNUSED_RANK_PROC static_cast<void>(rankProc)
#define IGNORE_UNUSED_FLAG_MATRIX_RHS static_cast<void>(flagMatrixRHS)
#define IGNORE_UNUSED_ELEM_POINT static_cast<void>(elemPoint)
#define IGNORE_UNUSED_ELEM_NORMAL static_cast<void>(elemNormal)
#define IGNORE_UNUSED_ELEM_TANGENT static_cast<void>(elemTangent)
#define IGNORE_UNUSED_ELEM_ID_POINT static_cast<void>(elemIdPoint)
#define IGNORE_UNUSED_ELEM_POINT_VOL static_cast<void>(elemPointVol)
#define IGNORE_UNUSED_ELEM_ID_POINT_VOL static_cast<void>(elemIdPointVol)
#define IGNORE_UNUSED_ELEM_ID_POINT_LOCAL static_cast<void>(elemIdPointLocal)
#define IGNORE_UNUSED_IEVOL static_cast<void>(ielVol);
#define IGNORE_UNUSED_VERBOSE static_cast<void>(verbose)
#define IGNORE_UNUSED_IEL static_cast<void>(iel)
#define IGNORE_UNUSED_ELT_TYPE static_cast<void>(eltType)
#define IGNORE_UNUSED_SIZE static_cast<void>(size)

#define IS_LOCAL_TO_GLOBAL_MAPPING_MACRO(cm, bs, n, indices, mode, mapping) ISLocalToGlobalMappingCreate(cm, bs, n, indices, mode, mapping);

// Some interesting macros
#define PETSC_EXIT_SUCCESS 0

/**
 * @namespace felisce
 * @brief This namespace is supposed to contain all the felisce library
 */
namespace felisce {

///@name felisce Globals
///@{

  // The following block defines the macro FELISCE_START_IGNORING_DEPRECATED_FUNCTION_WARNING
  // If written in a file, for the following lines of code the compiler will not print warnings of type 'deprecated function'.
  // The scope ends where FELISCE_STOP_IGNORING_DEPRECATED_FUNCTION_WARNING is called.
  // NOTE!! this macro is not intented for extensive use, it's just for temporary use in methods exported to Python which
  // are still calling a C++ deprecated function.
  #if defined(__clang__)
  #define FELISCE_PRAGMA_INSIDE_MACRO_DEFINITION(x) _Pragma(#x)
  #define FELISCE_START_IGNORING_DEPRECATED_FUNCTION_WARNING \
  FELISCE_PRAGMA_INSIDE_MACRO_DEFINITION(clang diagnostic push) \
  FELISCE_PRAGMA_INSIDE_MACRO_DEFINITION(clang diagnostic ignored "-Wdeprecated-declarations")
  #elif defined(__GNUC__) || defined(__GNUG__)
  #define FELISCE_PRAGMA_INSIDE_MACRO_DEFINITION(x) _Pragma(#x)
  #define FELISCE_START_IGNORING_DEPRECATED_FUNCTION_WARNING \
  FELISCE_PRAGMA_INSIDE_MACRO_DEFINITION(GCC diagnostic push) \
  FELISCE_PRAGMA_INSIDE_MACRO_DEFINITION(GCC diagnostic ignored "-Wdeprecated-declarations")
  #elif defined(_MSC_VER)
  #define FELISCE_START_IGNORING_DEPRECATED_FUNCTION_WARNING \
  __pragma(warning(push))\
  __pragma(warning(disable: 4996))
  #endif

  // Defining the architecture (see https://sourceforge.net/p/predef/wiki/Architectures/)
  // Check Windows
  #if defined(_WIN32) || defined(_WIN64)
    #if defined(_WIN64)
      #define FELISCE_ENV64BIT
    #else
      #define FELISCE_ENV32BIT
    #endif
  #else // It is POSIX (Linux, MacOSX, BSD...)
    #if defined(__x86_64__) || defined(__ppc64__) || defined(__aarch64__)
      #define FELISCE_ENV64BIT
    #else // This includes __arm__ and __x86__
      #define FELISCE_ENV32BIT
    #endif
  #endif

  // The following block defines the macro FELISCE_STOP_IGNORING_DEPRECATED_FUNCTION_WARNING which ends the scope for
  // ignoring the warnings of type 'deprecated function'.
  #if defined(__clang__)
  #define FELISCE_STOP_IGNORING_DEPRECATED_FUNCTION_WARNING \
  _Pragma("clang diagnostic pop")
  #elif defined(__GNUC__) || defined(__GNUG__)
  #define FELISCE_STOP_IGNORING_DEPRECATED_FUNCTION_WARNING \
  _Pragma("GCC diagnostic pop")
  #elif defined(_MSC_VER)
  #define FELISCE_STOP_IGNORING_DEPRECATED_FUNCTION_WARNING \
  __pragma(warning(pop))
  #endif

  // Deprecated macros
  #if __cplusplus >= 201402L
  #define FELISCE_DEPRECATED [[deprecated]]
  #define FELISCE_DEPRECATED_MESSAGE(deprecated_message) [[deprecated(deprecated_message)]]
  #elif __GNUC__
  #define FELISCE_DEPRECATED __attribute__((deprecated))
  #define FELISCE_DEPRECATED_MESSAGE(deprecated_message) FELISCE_DEPRECATED
  #elif defined(_MSC_VER)
  #define FELISCE_DEPRECATED __declspec(deprecated)
  #define FELISCE_DEPRECATED_MESSAGE(deprecated_message) FELISCE_DEPRECATED
  #else
  #pragma message("WARNING: You need to implement DEPRECATED for this compiler")
  #define FELISCE_DEPRECATED
  #define FELISCE_DEPRECATED_MESSAGE(deprecated_message)
  #endif

  //Print Trace if defined
  #define FELISCE_WATCH(variable) std::cerr << #variable << " : " << variable << std::endl;

///@}
///@name Type Definitions
///@{

  typedef double ( * FunctionXYZ ) (const Point&);
  typedef double ( * FunctionXYZT ) (const Point&,double);

  typedef boost::numeric::ublas::range UBlasRange;
  typedef boost::numeric::ublas::vector<double> UBlasVector;
  typedef boost::numeric::ublas::vector_range< UBlasVector > UBlasVectorRange;
  typedef boost::numeric::ublas::triangular_matrix<double, boost::numeric::ublas::lower> UBlasLowerTriMatrix;
  typedef boost::numeric::ublas::matrix<double> UBlasMatrix;
  typedef boost::numeric::ublas::matrix_range< UBlasMatrix > UBlasMatrixRange;
  typedef boost::numeric::ublas::matrix_column< UBlasMatrix > UBlasMatrixColumn;
  typedef boost::numeric::ublas::matrix_row< UBlasMatrix > UBlasMatrixRow;
  typedef boost::numeric::ublas::permutation_matrix<std::size_t> UBlasPermutationMatrix;
  typedef boost::numeric::ublas::unit_vector<double> UBlasUnitVector;
  typedef boost::numeric::ublas::zero_vector<double> UBlasZeroVector;
  typedef boost::numeric::ublas::identity_matrix<double> UBlasIdentityMatrix;
  typedef boost::numeric::ublas::zero_matrix<double> UBlasZeroMatrix;
  template<typename TDataType, std::size_t TSize1, std::size_t TSize2> using UBlasBoundedMatrix = boost::numeric::ublas::bounded_matrix<TDataType, TSize1, TSize2>;
  template<typename TDataType, std::size_t TSize> using UBlasBoundedVector = boost::numeric::ublas::bounded_vector<TDataType, TSize>;

  typedef PetscInt    felInt;
  typedef PetscScalar felReal;

  typedef std::function<double (const double, const double, const double)> CallbackXYZ;
  typedef std::unordered_map<std::string,CallbackXYZ> MapCallbackXYZ;

  typedef std::function<double (const double,const double,const double,const double)> CallbackXYZT;
  typedef std::unordered_map<std::string,CallbackXYZT> MapCallbackXYZT;
  typedef std::set<std::pair<felInt,Point>,bool(*)(std::pair<felInt,Point >, std::pair<felInt,Point >) > setOfPairOfIntAndPoint;

///@}
///@name  Enum's
///@{

  enum PhysicalVariable {temperature, velocity, pressure, velocityDiv, potExtraCell, potTransMemb, section, displacement, rotation,
    velocityAdvection, potThorax, turbulenceK, turbulenceOM, iop,vorticity,velocity_square,stressX,stressY,stressZ, stressDotNormal, WG, wallShearStress, pressureDarcy, lagMultiplier};

  enum TypeOfBoundaryCondition {Dirichlet, Neumann, NeumannNormal, Robin, RobinNormal, EmbedFSI, EssentialLumpedModelBC, NaturalLumpedModelBC, BackflowStab};

  enum TypeValueOfBoundaryCondition {Constant, Vector, FunctionS, FunctionT, FunctionTS, EnsightFile};

  enum TypeValueOfComplianceLumpedModelBC {Cste, FunctionV};

  enum Component {CompNA, Comp1, Comp2, Comp12, Comp3, Comp13, Comp23, Comp123 };
  extern const char* strComponent[8];

  // enum to known on which side of the interface nodes or subelements are.
  enum sideOfInterface { UNDEFINED = 0, LEFT = 1, RIGHT = -1 };

  //newelemfield
  enum ElementFieldType {CONSTANT_FIELD, DOF_FIELD, QUAD_POINT_FIELD};
  extern const std::size_t numElementFieldType;
  extern const char* strElementFieldType[3];

  enum TypeValueOfElementField {FROM_CONSTANT, FROM_FUNCTION, FROM_FILE};
  extern const std::size_t numTypeValueOfElementField;
  extern const char* strTypeValueOfElementField[3];

  ///@}
  ///@name  Functions
  ///@{

  void initFELiScE();

  ///@}

} // namespace felisce
#endif
