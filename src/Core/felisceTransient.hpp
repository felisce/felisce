//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _FELISCE_TRANSIENT_HPP
#define _FELISCE_TRANSIENT_HPP

// System includes

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Core/felisceParam.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class FelisceTransient 
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Point
  FELISCE_CLASS_POINTER_DEFINITION(FelisceTransient);

  ///@}
  ///@name Life Cycle
  ///@{

  /// default constructor
  FelisceTransient();

  // copy constructor
  FelisceTransient(const FelisceTransient& rFelisceTransient);

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{
  
  /**
   * @brief Increase the iterations counter
   */
  void increaseCounter() {++non_linear_iteration;++total_number_iterations;}

  /**
   * @brief This method returns the iteration number as a string
   * @details Always returns 5 spaces
   * @note This can be optimized 
   */
  std::string itStr() const;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /**
   * @brief This prints the information of the transient class
   */ 
  void print() const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  double timeStep = 0.0;           /// The increment of the time step
  double time = 0.0;               /// The current time of the simulation
  int iteration = 0;               /// The non-linear iteration number (NOTE: Should be unsigned)
  int non_linear_iteration = -1;   /// The non-linear iteration number (NOTE: Should be unsigned, but interacts with PETSc)
  int total_number_iterations = 0; /// The total number of iterations

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif
