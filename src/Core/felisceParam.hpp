//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _FELISCE_PARAM_HPP
#define _FELISCE_PARAM_HPP

// System includes
#include <cstdlib>
#include <map>
#include <unordered_map>

// External includes

// Project includes
#include "Core/getPotCustomized.hpp"
#include "Core/shared_pointers.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Core/felisce.hpp"
#include "Core/felisce_error.hpp"
#include "Core/singleton.hpp"
#include "Core/chrono.hpp"
#include "Core/util_string.hpp"
#include "Core/commandLineOption.hpp"
#include "Hyperelasticity/enumHyperelasticity.hpp"
#include "Hyperelasticity/pressureData.hpp"
#include "HyperelasticityLaws/HyperElasticLaw.hpp"

// TODO: Refactor all this in order to use variables instead of member variables, otherwise this object is very heavy

namespace felisce
{
typedef std::unordered_map<std::string,ElementFieldDynamicValue*> ElementFieldDynamicValueMapType;

/**
 * @class UniqueFelisceParam
 * @brief Auxiliary class for reading the felisce data in the data file and on the command line option.
 */
class UniqueFelisceParam
{
private:
  std::unordered_map<std::string, std::vector<std::string> > m_help;

  //Disable default copy-constructor and copy-assignement.
  //If copy-constructor is needed, proper copy of ElementFieldDynamicValueMap has
  //to be written.
  UniqueFelisceParam(const UniqueFelisceParam &);
  UniqueFelisceParam(UniqueFelisceParam &);
  UniqueFelisceParam& operator=(const UniqueFelisceParam&);
  UniqueFelisceParam& operator=(UniqueFelisceParam&);

  std::size_t mInstanceIndex = 0;

public:

  /// Pointer definition of UniqueFelisceParam
  FELISCE_CLASS_POINTER_DEFINITION(UniqueFelisceParam);

  UniqueFelisceParam(const std::size_t instanceIndex = 0);

  virtual ~UniqueFelisceParam();

  const std::size_t& getInstanceIndex() { return mInstanceIndex; };

  CommandLineOption initialize(const int argc, const char** argv);

  void initialize(CommandLineOption & opt);

  /*!
    \brief Read the felisce data in the data file and on the command line option.
    */
  void parseInputFile(int argc,char **argv,std::string filename);

  /*!
    \brief Read the felisce data in the data file and on the command line option.
          With data only dependent on the application.
    */
  virtual void userParseInputFile(GetPotCustomized& /*gpc*/){};

  /*!
    * In verbose mode, display the read values .
    */
  void print(int verbose = 0, std::ostream& outstr = std::cout) const;

  /*!
    * Display help about available Felisce data, like a manpage.
    */
  void printHelp();

  /*
    * Get an *elementFieldDynamicValue from ElementFieldDynamicValueMap,
    * returns NULL the std::unordered_map  does not contain it.
    */
  ElementFieldDynamicValue *elementFieldDynamicValue(const char* name);

  /*
    * \brief Expand environment variables $HOME, $FELISCE_DATA_DIR, $FELISCE_INRIA_DATA_DIR and $FELISCE_RESULT_DIR in a std::string.
    *
    * \param[in] inputPath      The input std::string, that may contains $HOME or $FELISCE_DATA_DIR or $FELISCE_INRIA_DATA_DIR or FELISCE_RESULT_DIR
    * \param[in] datafile       data file where inputPath come from, for debug message purpose
    * \param[in] section        data file section where inputPath come from, for debug message purpose
    * \param[in] inputPathName  data file variable where inputPath come from, for debug message purpose
    * \param[out] pathExpanded  inputPathName where environment variables have been expanded
    *
    * FEL_ERROR is called in case of errors, with various corresponding error
    * messages.
    */
  std::string expandEnvironmentVariables(const std::string inputPath,
                                          const std::string datafile, const std::string section, const std::string inputPathName);

public:

  int tmpLinSolverVerbose;

  //Debug
  int m_verbose = 0;
  int m_refVerbose = 0;

  // Chrono
  Chrono timer;
  bool chronoToScreen;
  bool chronoToFile;

  // Data
  std::string inputDirectory;
  std::string inputDirRaw;
  std::string inputFile;
  std::string podBasisFile;
  felInt timeMaxCaseFile;
  bool readDisplFromFile;
  bool readPreStressedDisplFromFile;

  // Transient
  double timeStep;
  double timeMax;
  double time;
  double timeStepCharact;
  int iteration;
  int timeIterationMax;
  int frequencyWriteSolution;
  int periodNbSteps;
  std::vector<int> intervalWriteSolution;
  std::string integrationTimeMethod;
  bool writeSolutionMeshes;

  // Variable
  std::vector<std::string> nameVariable;
  std::vector<int> typeOfFiniteElement;
  std::vector<int> degreeOfExactness;
  std::vector<bool> decomposePlaneTransverse;
  std::vector<int> planeDegreeOfExactness;
  std::vector<int> transverseDegreeOfExactness;
  std::vector<PhysicalVariable> physicalVariable;

  // Mesh
  std::string meshDir;
  std::string meshDirRaw;
  std::vector<std::string> inputMesh;
  std::vector<std::string> outputMesh;
  felInt outputFileFormat = 0;
  std::vector<int> idMesh;
  std::string resultDirRaw;
  std::string resultDir;
  std::string resultDirStressesRaw;
  std::string resultDirStresses;
  std::vector<std::string> prefixName;
  std::string CVGraphInterface;
  std::unordered_map<std::string , std::map<int, std::string> > felName2MapintRef2DescLine;
  bool duplicateSupportDof = false;
  bool effective2dGEOmesh = false;
  bool flipNormal;
  bool readNodesReferences = false;
  // List of surfaces (or segments) that are part of the domain and not boundary of tetrahedra (or surfaces)
  std::vector<felInt> notBoundarySurfaceLabels;
  bool reinterpolation;
  bool extendToLowerDimensions;
  std::string choiceDirectionTangent;
  bool whenExtrudePreserveOriginalCoordinates;
  bool checkVolumeOrientation;
  bool checkNormalOrientation;
  int numCoor = 0; // Number of coordinates (2D/3D)

  // Geometry
  struct S_Geo {
    double tolrescaling = 1.;
  } Geo;

  // Physics
  int unit;
  double spaceUnit; // This number multiplies all the coordinates of the mesh. Example: if the mesh is in millimeters, spaceUnit = 0.1 convert it in centimeters
  std::string model;
  int testCase;
  double u_max_inlet;
  double max_flow_scaling;
  double T_sis;
  double T;
  double symmetry;
  std::vector<int> dimension;

  // ROM
  std::string ROMmethod;
  bool solveEigenProblem;
  bool optimizePotential;
  double chiSchrodinger;
  bool useROM;
  felInt dimRomBasis;
  felInt dimCollocationPts;
  felInt numApproxMode;
  felInt nCutOff;
  std::string collocationPtsFile;
  bool useImprovedRec;
  felInt dimOrthComp;
  int orderALP;
  bool readBasisFromFile;
  bool writeBasisEvolution;
  bool hasSource;
  int numberOfSource;
  felInt numberOfSnapshot;
  felInt samplingFrequency;

  // POD
  bool verbose_ensight;
  int nbTestCase;
  int snap_centered;
  int L2Mass;
  std::string output_dir;
  std::string output_file;
  std::string case_dir;
  std::string case_file_name;
  std::string snap_interval;
  std::string variable_name;
  std::string podFolder;
  double timeStepSnap;

  // Optimal reconstruction
  double targetHR;
  double timeWindowSize;
  std::string image_modality;
  double sistolicTime;
  std::string patients_data_dir;
  double HRwindowSize;
  int nbMeasures;
  int nbModes;
  std::string targetTime;
  bool useField2Measures;
  bool doSnapshotsSVD;
  bool calculateRieszRepresenters;
  std::string measuresFile;
  std::string targetSim;
  std::string rrFolder;
  std::string fieldFolder;
  std::string hashTableMesh;
  std::string modelReductionTechnique;

  // Pressure estimation
  int inletBoundaryLabel;
  int wallBoundaryLabel;
  std::vector<int> outletBoundaryLabels;
  std::vector<std::string> testFunctionsPath;
  std::vector<std::string> uStarPath;

  // PostProcCFD
  std::string caseDirCFD;
  std::string caseFileNameCFD;

  // Fluid
  int orderBdfNS;
  int orderBdfFS;
  bool quasistatic;
  double density;
  double viscosity;
  double NS_alpha; // like a capacitance in Rp,C,Rd lumpedModelBC model
  double NS_beta;  // like inverse of distal resistance in Rp,C,Rd lumpedModelBC model
  double NS_gamma; // like proximal resistance in Rp,C,Rd lumpedModelBC model
  int NSmodifFlag; // flag: 0 = no modified NS model
  int NS_modifLabel; // currentLabel where we apply modified NS terms
  int inletLabel; // label of an inlet of interest
  int outletLabel; // label of an outlet of interest
  double referenceValue1; // any reference value (general purpose)
  double referenceValue2; // any reference value (general purpose)
  double referenceValue3; // any reference value (general purpose)
  double referenceValue4; // any reference value (general purpose)
  int characteristicMethod; // to switch from semi-implicit resolution to method of characteristics
  int addedBoundaryFlag; // flag: 0 = no stabilization
  std::vector<int> stabilizationLabel; // label where we use stabilization
  double stabilizationCoef; // coef for tune stabilization
  int explicitAdvection;
  int CVGraphInterfaceVariationalBC;
  double NitscheStabilizParam; //Nitsche's stabilization  parameter
  double NitschePenaltyParam; //Nitsche's penalty parameter
  double ContactTolerance; //Tolerance value for the contact with a simple straight wall handled by the structure
  bool useSymmetricStress;
  int NSequationFlag; // Flag to choose implementation method of advective term
  double stabSUPG; // SUPG stabilization parameter
  double stabdiv; // stab parameter div div
  int typeSUPG; // type of SUPG stabilization
  int NSStabType; // type of stabilization for advection and finite element for NS equation
  double stabFOVel; // velocity jump coefficient for face-oriented stabilization
  double stabFOPre; // pressure jump coefficient for face-oriented stabilization
  int orderPressureExtrapolation; // order of the pressure extrapolation for NSFracStep solvers
  int incrementalFS;
  double coefRotScheme; // coef for the rotational incremental NSFracStep solvers
  int useALEformulation; // 0: no ALE 1: standard ALE, 2: stabilized ALE
  bool bcInRefConfiguration; // = true means BC are applied on the reference ALE configuration
  bool useElasticExtension;
  bool initALEDispByFile;
  bool updateMeshByVelocity;
  double theta; // theta parameter (0 \geq theta \geq 1) in the theta-method (time marching)
  bool nonLinearFluid; //bool non linear solver
  int timeScheme;
  double gammaNM;
  double betaNM;

  // Poroelasticity
  double biotCoefficient;
  double biotModulus;
  double isoPermeab;
  double anisoPermeab;
  int validationTest;
  bool scalePressureEq;

  // FSI
  bool fsiCoupling;
  std::string typeOfInterface;
  int fsiRNscheme;
  double gammaRN; // Robin parameter for iterative RN coupling with solid
  bool useLumpedMassRN;
  bool useMassLumping;
  std::vector<int> fsiInterfaceLabel;
  double domainDecompositionToll;
  int domainDecompositionMaxIt;
  bool hasOldMaster;

  // ZeroMQ
  std::string socketTransport; // communication transport protocol (tcp, ipc, inproc,...)
  std::string socketAddress; // transport-specific address (interface) to connect to (ex: 127.0.0.1 for localhost)
  std::string socketPort; // for tcp transport, specifies the port on the interface

  //immersed data for the structure penalisation
  bool hasImmersedData;
  std::string meshDirImmersedStruct;
  std::string meshFileImmersedStruct;
  double penaltyCoeffImmersedStruct;
  int allowProjectionImmersedStruct;
  double valueSourceImmersedStruct;
  int useMassMatrix; // specify the content of the fourth block of the fluid matrix for the implicit immersed problems. 1: Penalisation coefficient * Mass matrix of the structure. 0: Penalisation coefficient.
  bool hasDivDivStab; // modification of the SUPG and div div stabilization to improve mass convervation across the valve (cf. Kamensky et al., CMAME, 284, 1005-1053)
  double localDivDivPenalty;
  bool divDivStabPostProcess; // boolean to print the results relative to the intersection of the structure mesh with the fluid mesh in files "intersection.****.scl"
  bool intersectedSubElementsPostProcess; // boolean to print the results relative to the edges of the fluid mesh intersected by the structure mesh in files "intersectedEdges.****.scl"
  bool useFDlagrangeMult;
  bool massConstraint;
  std::vector<felInt> japanLabels;
  bool BHstab;
  bool BHstabSymm;
  bool BPstab;
  double stabCoeffLag;
  bool useFicItf;
  bool usePhyAndFict;
  bool flipItfNormal;
  bool useAverageNormal;
  bool compPressure;
  felInt compPresGerm;

  // M1G library interface
  struct S_M1G {
    bool   enable;
    int    verbose;
    double Hscaling;
    bool   CheckOrientation;
    std::vector<int> labelPhys;
    std::vector<int> labelFict;
    std::vector<felInt> triLst;
    std::vector<felInt> verLst;
    std::string outdir;
    bool writeM1Gmeshes;
  } M1G;
  
  //fkpp
  std::vector<double> sourcePos;

  // Nitsche-XFEM formulation
  bool    useDynamicStructure;
  bool    useGhostPenalty;
  double  coefGhostPenalty;
  bool    useProjectionForNitscheXFEM;
  bool    useInterfaceFlowStab;
  felInt  couplingSchemeStoppingCriteria;
  bool    useODESolve;
  bool    usePenaltyFreeNitscheMethod;
  felInt  contactDarcy;
  double  epsDarcy;
  double  kDarcy;
  bool    writeXFEMmeshes;
  bool    confIntersection;
  std::vector<int> structures;  // Structure label (mesh file)
  std::vector<int> numficxstr;  // Number of fictitious structure per structure
  std::vector<int> fictitious;  // Fictitious structure label (mesh file)(one or more for evry structure)
  std::vector<int> meanLabel;

  // Solid
  double densitySolid;
  double young;
  double poisson;
  double coeffReaction;  //coeff for diaphragm and chest reaction - lung model, mechanically induced respiration
  double lambda_lame;
  double mu_lame;
  double thickness;
  double alpha_rayleigh;
  double beta_rayleigh;
  bool hasRayleighDamping = false;
  int planeStressStrain;
  int volumicOrSurfacicForce;
  struct CiarletGeymonatType {
    double kappa1;
    double kappa2;
  } CiarletGeymonat;
  struct OgdenType {
    double C1;
    double C2;
    double a;
  } Ogden;
  int nbSubRegions;
  int flagJacobi;

  double hyperelastic_bulk; // for penalization term in hyperelasticity constitutive law.
  double volumic_mass;
  bool hasSolidMidpoint;
  double solidRadius;
  std::string typeOfElasticityModel;
  std::string typeOfShellModel;
  std::vector<int> contactLabelPair;
  std::vector<int> rayOrientation;
  bool symmetricContact;
  double contactGap;
  double contactPenaltyParam;
  bool evalEnergy;
  bool velocityConstrained;

  // Beam
  struct Beam {
    int sub_integration;
    bool timoshenko;
    double diameter;
    double inner_diameter;
    double depth;
    double cross_area;
    double effective_area_y;
    double effective_area_z;
    double moment_of_intertia_y;
    double moment_of_intertia_z;
    double torsional_inertia;
  } beam;

  //elec
  bool hasCoupledAtriaVent;
  int orderBdfEdp;
  int orderBdfIonic;
  //elec - model
  std::string typeOfIonicModel;

  std::string CellsType;

  bool spontaneous;
  bool hasHeteroTauClose;
  bool hasHeteroCourtPar;
  bool hasHeteroCondAtria;
  bool hasAppliedExteriorCurrent;
  //elec - Schaf solver
  bool printIonicVar;
  double tauOpen;
  double tauClose;
  double tauCloseEndo;
  double tauCloseCell;
  double tauCloseEpi;
  double tauCloseRV;
  double tauIn;
  double tauOut;
  double kTanhMSR;
  double vMin;
  double vMax;
  double vGate;
  //elec - FhN solver
  double epsilon;
  double beta;
  double gammaEl;
  double f0;
  double alpha;
  //elec - MV solver parameters for inverse problem
  double tau_so_1_epi;
  double tau_so_1_endo;
  double tau_so_1_mcel;
  double tau_so_1_rv;
  //elec - MV conductance parameters
  double gfi_rv;
  double gfi_endo;
  double gfi_mid;
  double gfi_epi;
  double gso_rv;
  double gso_endo;
  double gso_mid;
  double gso_epi;
  double gsi_rv;
  double gsi_endo;
  double gsi_mid;
  double gsi_epi;
  //elec - Courtemanche solver parameters for inverse problem
  double g_Na_PM;
  double g_Na_CT;
  double g_Na_BB;
  //elec - Luenberger filter
  bool stateFilter;
  bool schroFilter;
  bool electrodeControl;
  double aFilter;
  double gain;
  std::string observDir;
  std::string observFileName;
  //elec - parameters
  double Am;
  double Cm;
  bool monodomain;
  double extraTransvTensor;
  double intraTransvTensor;
  double extraFiberTensor;
  double intraFiberTensor;
  double extraTransvTensorAtria;
  double intraTransvTensorAtria;
  double extraFiberTensorAtria;
  double intraFiberTensorAtria;
  double extraTransvTensorVent;
  double intraTransvTensorVent;
  double extraFiberTensorVent;
  double intraFiberTensorVent;
  double sigmaThorax;
  double sigmaLung;
  double sigmaBone;
  //elec - applied current
  std::string typeOfAppliedCurrent;
  std::string typeOfAppliedCurrentExt;

  //MEA
  int NbElectrodes;
  std::vector<int> ElecStim;
  std::vector<double> pulseValues;
  std::vector<double> pulseValuesDuration;
  std::vector<std::string> pulseType;
  std::vector<double> timePeriodMEA;
  std::vector<double> timePeriodMEAtrain;
  std::vector<double> trainDuration;
  std::vector<double> tMaxStim;
  std::vector<double> Ri;
  std::vector<double> Rel;
  std::vector<double> Cel;
  std::vector<double> Diam;

  double timePeriod;
  double timePeriodVentricle;
  double timePeriodAtria;
  int beatNumber;
  double beatDecreasing;
  std::vector<double> delayStim;
  std::vector<double> delayStimVentricles;
  double stimTime;
  double stimTimeLV;
  double stimTimeRV;
  double Iapp_x_coord;
  double Iapp_y_coord;
  double Iapp_z_coord;
  //elec - pathologies
  //elec - infarct
  bool hasInfarct;
  double x_infarct;
  double y_infarct;
  double z_infarct;
  double radius_infarct;
  //elec - ventricles BBB
  bool hasVentriclesBundleBrunchBlock;
  std::string sideOfBBB;
  bool BBBwithDelay;
  bool BBBwithAngleBlock;
  double BBBDelayStim;
  double angleIappBBB;
  //elec - Bachmann Bundle Block
  bool hasPartialBachmannBundleBlock;
  double valueBBBlock;
  //elec - Wenckebach atrioventricular Block
  bool hasWenckebachBlock;
  //elec - Kent Bundle
  bool hasKentBundle;
  //elec - Torsades de pointe
  bool torsade;
  double torsadeTimeBegin;
  double torsadeTimeEnd;
  double tauCloseTorsade;
  double tauCloseEndoTorsade;
  double tauCloseCellTorsade;
  double tauCloseEpiTorsade;
  double tauCloseRVTorsade;
  //elec - ECGwriter
  bool writeECG;
  bool writeElectrodesMeas;
  bool writeElectrodesMean;
  bool writeMatrixECG;
  std::string ECGelectrodeFile;
  std::string ECGmatchFile;
  std::string ECGThoraxmatchFile;
  std::string ECGmatrixFile;
  std::string ECGfileName;
  bool ECGtabHeader;
  bool ECGcreateFigure;
  //elec - dataAssimilation
  bool dataAssimilation;
  double minLSPotTM;
  double maxLSPotTM;
  double insidePar;
  double outsidePar;

  //fsi 1D, possibly other parameters for 2D/3D:
  std::vector<double> betaNormalized;
  std::vector<double> sectionAtRest;

  //initial condition
  bool hasInitialCondition;               // has or not initial conditions.
  bool restartSolution;                   // use previous solution as initial solution.
  std::string restartSolutionDirRaw;         // directory where the initial solution is backed up.
  std::string restartSolutionDir;         // directory where the initial solution is backed up.
  int restartSolutionIter;                // iteration of the backup solution used by the restart.
  std::vector <std::string> nameVariableInitCond;   // variable associate to the IC (to be saved if doing backup).
  std::vector<std::string> componentInitCond;       // components associate to the IC.
  std::vector<double> valueInitCond;           // value associate to the IC.
  int frequencyBackup;                    // frequancy of backup (if == 0 , not doing backup copies).

  // Boundary condition
  int essentialBoundaryConditionsMethod;    // 2:penalization or 0:pseudo-elimination.
  std::vector<std::string> type;            // Dirichlet, Neumann, NeumannNormal, Robin, RobinNormal, EmbedFSI.
  std::vector<std::string> typeValue;       // Constant, Vector, FunctionT, FunctionS, FunctionTS or EnsightFile
  std::vector<int> numLabel;                // number of labels concerned by this boundary condition.
  std::vector<int> label;                   // label (number) in the mesh file.
  std::vector<std::string> labelName;       // name in the mesh file.
  std::vector<std::string> variable;        // variable to apply BC.
  std::vector<std::string> component;       // components associate to the BC.
  std::vector<double> value;           // value associate to the BC.
  std::vector<double> userParameter;   // container for all others parameters.
  std::vector<double> alphaRobin;   // coeff before variable of Robin BC: betaRobin * grad u dot n + alphaRobin * u = g.
  std::vector<double> betaRobin;   // coeff before normal derivative of Robin BC: betaRobin * grad u dot n + alphaRobin * u = g.
  std::vector<double> alphaRobinNormal;   // coeff before variable of RobinNormal BC
  double penValueForEmbedFSI; //penalization value for imposing EmbedFSI bc.
  std::string bcCondDir;                // directory of input boundary values files (ensight files).
  bool useEssDerivedBoundaryCondition;//true if you want to use essential BC DERIVED, even without having any dirichlet BC, added for embedFSI, to impose 0 displacement at the surface boarders
  std::string modeLung;
  //TEST_RECUP_DONNEE
  std::string bc_potExtraCell;


  //BCdata x SimplifiedFSI model and boundary condition.
  //common properties
  bool switchSimplFSIOff;
  double tssParam;
  bool tssConsistent;

  bool nonLinearKoiterModel;
  bool zeroDisplacementAtBoarders;
  double densityStructure;
  double viscosityStructure;
  //properties of the shell
  double youngShell;
  double poissonShell;
  double widthShell;
  //properties of the fibers
  double widthFiber;
  double preStressFibers;
  double youngFibers;
  std::vector<double> fiberDensity; // Density of fibers by label (coefficient multiplying (preStressFibers+extraTension) and youngFibers). The list is ordered as the labels defining the EmbedFSI type of BC.
  double fiberDispersionParameter;
  double outsidePressure;
  //properties for 2D testing
  double radius;

  //other
  int idCase;
  std::string nameTest;
  double incomingPressure;
  double propCardiacCycle;

  //NSHeat
  double coeffOfThermalExpansion;
  double thermalDiffusion;
  double initialTemperature;
  double referenceTemperature;
  std::vector<double> gravity;
  std::vector<felInt> stevinoLabels;
  double flowRate;

  // data for retinal autoregulation parameters
  bool autoregulated;
  double maxT;
  double q;
  double up;
  double down;
  double nominalPressure;
  bool useRegulationInWindkessel;
  std::vector<double> lumpedModelBC_regParam;
  double windkesselProp;
  int idControlFreeCase;
  bool exportPrincipalDirectionsAndCurvature;
  bool exportDofPartition;

  //IOPcoupling
  std::vector<int> labelIOPMesh;
  std::vector<int> labelNSMesh;
  double porousParam;
  bool computeSteklov;
  double relaxationParam;
  bool exportSteklovData;
  int optionForSteklov;
  std::string steklovDataDir;
  std::string steklovDataDirRaw;
  std::string steklovMatrixName;
  int accelerationMethod;
  double omegaAcceleration;
  bool lumpedTest;
  bool notConnectedCylinders;

  bool useRoSteklov;
  int nbOfLaplacianEigenFunction;
  double percentageOfExtraEig;
  int lowRank;
  bool tryAcceleration;
  double projOnlineRatio;
  bool onlyConstResp;

  //  export configuration
  bool exportP1Normal;
  std::vector<int> labelExportP1Normal;
  bool exportAll;
  bool exportLaplacianEigenValues;
  bool exportLaplacianEigenVectors;
  bool exportSteklovEigenValues;
  bool exportSteklovEigenVectors;
  bool exportFTOfSteklovEigenVectors;
  bool exportInputOfSteklov;
  bool exportOutputOfSteklov;
  bool exportOnlyLastInput;
  bool exportOnlyLastOutput;
  bool exportMassMatrix;
  bool exportLaplacianMatrix;
  bool exportSteklovMatrix;
  bool exportReducedEigenMatrix;
  bool exportOffLineVolumeSolution;
  bool exportFiltrationVelocity;


  std::string bcCondDirRaw;                // directory of input boundary values files (ensight files).


  //move mesh
  bool hasMoveMesh;
  bool hasMoveFibers;
  std::string MoveMeshMatchFile;
  double scaleCoeff;

  // bc - lumpedModelBC BC (fluid)
  std::vector<int> lumpedModelBCType;
  std::vector<int> lumpedModelBCAlgo;
  std::vector<int> lumpedModelBCLabel;
  std::vector<double> lumpedModelBC_Rprox;
  std::vector<double> lumpedModelBC_Rdist;
  std::vector<double> lumpedModelBC_C;
  std::vector<double> lumpedModelBC_Pvenous;
  std::vector<double> lumpedModelBC_Pdist_init;
  std::vector<std::string> lumpedModelBCName;

  // linear/non linear compliance (C=C(V))
  int lumpedModelBC_C_type;
  int powerNonLinearCompliance;
  double lumpedModelBC_volumeMax;
  double lumpedModelBC_volumeMin;
  // volume at rest
  double lumpedModelBC_volume0;
  double lumpedModelBC_referenceVolume0;

  // bc - fluid bc (user parameters)
  double inflowPressureMaxQuiteInspi;
  double inflowPressureMaxSpiroInspi;
  double inflowPressureMaxSpiroExpi;
  int inflowLabel;

  // bc - to impose the tangential components of the velocity std::vector equal to 0
  std::vector<int> compTangBClabel;

  //CardiacCycle input condition
  int CardiacCycleLabel;
  bool CardiacCycle;
  double duration_ejection;
  double duration_isovol_relax;
  double duration_filling;
  double duration_isovol_contract;

  double cardiacOutput; // volume of blood ejected per minute (typicaly in liter)
  double heartRate; // number of heart beat per minute

  //FusionDof
  bool FusionDof;
  std::vector<std::string> FusionVariable;  //variable whose dofs have to be fusioned
  std::vector<int> FusionNumLabel;          // total number of labels to be fusioned
  std::vector<int> FusionLabel;             // label (number) in the mesh file.
  double FusionTolerance;
  std::vector<bool> AllToOne;               // All dofs fusioned in a single one
  bool AllInLabelToOne; // All dofs of a label fusioned in a single one

  //EmbeddedInterface
  bool EmbeddedInterface;
  std::vector<std::string> EmbeddedVariable; //variable whose dofs have to be matched
  std::vector<int> EmbeddedNumInterface;         // total number of labels to be matched
  std::vector<int> EmbeddedLabelPairs;            // label (number) in the mesh file.
  double EmbeddedMatchTolerance;

  //Crack
  bool hasCrack;
  std::vector<std::string> CrackVariable;  // variable used in crack term
  std::vector<int> CrackNum;         // total number of labels indicating crack
  std::vector<int> CrackLabelPairs;            // label (number) in the mesh file.
  std::vector<int> CrackElemID;            // label (number) in the mesh file.
  double CrackConstant; // working crack "spring" constant
  double CrackConstantBefore; // crack "spring" constant before cracking
  double CrackConstantAfter; // crack "spring" constant after cracking
  double CrackTolerance; // tolerance between points for crack
  double CrackStressTolerance; // tolerance for stress before switching to "CrackConstant"

  //Penalization valves
  bool PenalizationMethod;
  double Gamma_penalization;
  std::vector<int> closed_penalization;

  //RISModels
  bool RISModels;
  int initiallyOpenedValveIndex; // index of which valve is initially open
  int crashIfStatusNotAdmissible; // variable to prevent the simulation to stop if a status is not admissible
  std::vector<int> closed_surf;
  std::vector<int> open_surf;
  std::vector<int> fake_surf;
  int nbTimeSteps_refractory_time; // number of time steps used for defining the refractory time
  int nbTimeSteps_linear_transition; // number of time steps used for defining the linear evolution in the value of the resistances
  std::vector<int> nbTimeSteps_linear_transition_twoValves; // number of time steps used for defining the linear evolution in the value of the resistances of the proximal and distal valves
  double R_active_surf; // resistance of closed valve
  double R_inactive_surf; // resistance of open valve
  double R_activeClosed_TwoValves_Intermediate; // resistance of closed valve just after the closing - only used for two valves
  double R_activeClosed_TwoValves_Final; // final resistance of closed valve after several iterations after the closing - only used for two valves
  double R_FirstValve_ClosingSpeed; // rate of speed for the evolution of the resistance of the closed surface of the first valve from R_Intermediate to R_Final - only used for two valves
  double R_SecondValve_ClosingSpeed; // rate of speed for the evolution of the resistance of the closed surface of the second valve from R_Intermediate to R_Final - only used for two valves
  double flow_ref_oneValve; // flow reference for one valve
  std::vector<double> flow_ref_twoValves; // flow reference for two valves
  int preventValvesFromOpeningTooFast; // specify if a closed valve should be prevented from opening if another valve has recently closed
  int nbIterationsPreventValvesFromOpeningTooFast; // number of iterations during a closed valve cannot open whereas another valve has recently closed
  int verboseRIS; // verbose for the RIS model

  //Pressure correction for full closed cavity where only displacements are imposed on its boundaries
  bool useElectroMechanicalPressure; // introducing electromechanical pressure
  bool usePressureCorrection; // introducing pressure correction
  bool correctionExternalPressure; // introducing external pressure correction
  bool correctionALEPressure; // introducing ALE pressure correction
  int typeOfPressureCorrection; // type of pressure correction. 0 for enabling the pressure correction anytime, 1 only when all the valves are closed.
  std::vector<int> labelsToCorrectPressure; // vector of labels where the pressure correction is applied
  std::vector<int> signPressureCorrection; // vector of coefficients (-1 or 1) for the correction of the pressure
  std::vector<int> labelsToComputeMeanPressure; // vector of labels where the mean pressure is computed and used for the pressure correction
  std::vector<int> labelsToComputeFluxThrough; // vector of labels where the flux through is computed and used for the ALE pressure correction
  std::vector<int> exteriorNormalOfLabelsToComputeFluxThrough; // vector of orientation of the exterior normals of the labels where the flux through is computed and used for the ALE pressure correction
  std::vector<int> forbiddenLabelsForCorrection; // vector of labels where the resistive term called in computeElementArrayBoundaryCondition will not be applied in order to avoid duplications
  int verbosePressureCorrection; // verbose for the pressure correction

  //Stiffening
  bool stiffening;
  std::vector<int> stiffeningLabels;
  int skipStiffeningFirstIteration;
  double initStiff;
  double finalStiff;

  //Regurgitation
  bool regurgitation;
  int regurgitationType;
  std::vector<double> regurgitationPosition;
  std::vector<int> regurgitationLabels;
  double regurgitationRadius;

  //ShellModels

  // PETSc
  int linearSolverVerbose;
  std::vector<std::string> solver;
  std::vector<int> gmresRestart;
  std::vector<std::string> preconditioner;
  std::vector<std::string> setPreconditionerOption;
  std::vector<double> relativeTolerance;
  std::vector<double> absoluteTolerance;
  std::vector<double> solutionTolerance;
  std::vector<double> divergenceTolerance;
  std::vector<int> maxIteration;
  std::vector<int> maxIterationSNES;
  std::vector<int> maxFunctionEvaluatedSNES;
  std::vector<bool> initSolverWithPreviousSolution;

  //verdandi
  //The background error variance value.
  double stateErrorVarianceValue;
  double Kparameter;
  double RHSparameter;

  //cvgraph
  std::string compartmentName;
  std::vector<int> interfaceLabels;
  std::vector<int> correspondingInterfaceLabels;
  bool withCVG;
  std::string dataFileCVG;
  //interpolator parameters (Still in cvgraph section)
  double interpolatorThreshold;
  int maxDepth;
  int idVarToExchange;

  //ElementField
  ElementFieldDynamicValueMapType elementFieldDynamicValueMap;

  // Hyperelastic variables
  struct hyperElasticLawVariables  {
    std::vector<std::string> type;                                /// Dirichlet, Neumann, NeumannNormal, Robin, RobinNormal, EmbedFSI.
    std::vector<int> numLabel;                                    /// Number of labels concerned by this boundary condition.
    std::vector<int> label;                                       /// Label (number) in the mesh file.
    Private::Hyperelasticity::PressureData::Pointer pressureData; /// The pressure data considered
    HyperElasticityLawManager::Pointer hyperElasticLaws;          /// Pointer to the hyperelastic law manager considered
    HyperelasticityNS::TimeScheme timeSchemeHyperelasticity;      /// The time scheme considered
  } hyperElasticLaw;
};

/**
 * @class FelisceParam
 * @brief Read the felisce data in the data file and on the command line option.
 */
class FelisceParam
  : public Singleton<UniqueFelisceParam>
{
  friend class Singleton<UniqueFelisceParam>;

protected:
  FelisceParam();
  virtual ~FelisceParam();

public:

  /*
  Folling data are read in configuration file and on the command line.
  They are grouped by configuration file sections.
  */

  static const int& verbose(const std::size_t instanceIndex = 0) {
    return FelisceParam::instance(instanceIndex).m_verbose;
  }

  static const bool& chronoToFile(const std::size_t instanceIndex = 0) {
    return FelisceParam::instance(instanceIndex).chronoToFile;
  }

  static void linearSolverVerboseOFF(const std::size_t instanceIndex = 0) {
    FelisceParam::instance(instanceIndex).tmpLinSolverVerbose = FelisceParam::instance(instanceIndex).linearSolverVerbose;
    FelisceParam::instance(instanceIndex).linearSolverVerbose = 0;
  }

  static void linearSolverVerboseON(const std::size_t instanceIndex = 0) {
    /// Use it ONLY after the OFF version!
    FelisceParam::instance(instanceIndex).linearSolverVerbose = FelisceParam::instance(instanceIndex).tmpLinSolverVerbose;
  }

  static void alterVerbosity(int newValue, const std::size_t instanceIndex = 0) {
    FelisceParam::instance(instanceIndex).m_refVerbose=FelisceParam::instance(instanceIndex).m_verbose;
    FelisceParam::instance(instanceIndex).m_verbose=newValue;
  }
  static void restoreVerbosity(const std::size_t instanceIndex = 0) {
    /// Use it ONLY after alterVerbosity
    FelisceParam::instance(instanceIndex).m_verbose=FelisceParam::instance(instanceIndex).m_refVerbose;
  }
};

}

#endif
