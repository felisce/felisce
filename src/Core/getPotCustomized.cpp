//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    David Froger
//

// System includes
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <sstream>

// External includes

// Project includes
#include "Core/getPotCustomized.hpp"

namespace felisce 
{

  /* ================================================================ */
  /* ElementFieldDynamicValue                                         */
  /* ================================================================ */

  std::size_t ElementFieldDynamicValue::numAllComp = 3;
  Component ElementFieldDynamicValue::allComp[3] = {Comp1,Comp2,Comp3};

  // init,delete functions

  ElementFieldDynamicValue::ElementFieldDynamicValue():
    numCompMax_(3),
    name_("Undefined"),
    numComp_(-1) {
    initialize("Undefined");
  }

  ElementFieldDynamicValue::ElementFieldDynamicValue(const char *aName):
    numCompMax_(3),
    name_("Undefined"),
    numComp_(-1) {
    initialize(aName);
  }

  ElementFieldDynamicValue::~ElementFieldDynamicValue() = default;

  void ElementFieldDynamicValue::initialize(const char *aName) {
    name_ = aName;

    for (int iComp=0 ; iComp<numCompMax_ ; ++iComp) {
      typeValueOfElementField_[iComp] = (TypeValueOfElementField) -1;
      constant_[iComp] = -9999.;
      fileName_[iComp] = "Undefined";
      functionName_[iComp] = "Undefined";
      isReady_[iComp] = false;
    }
  }

  // std::set functions
  void ElementFieldDynamicValue::setElementFieldType(ElementFieldType type) {
    elementFieldType_ = type;
  }

  void ElementFieldDynamicValue::setTypeValueOfElementField(TypeValueOfElementField type, Component Comp) {
    int iComp = CompToIndex_(Comp);
    typeValueOfElementField_[iComp] = type;
  }

  void ElementFieldDynamicValue::setNumComp(int numCmp) {
    numComp_ = numCmp;
  }

  void ElementFieldDynamicValue::setConstant(double constVal, Component Comp) {
    int iComp = CompToIndex_(Comp);
    constant_[iComp] = constVal;
    isReady_[iComp] = true;
  }

  void ElementFieldDynamicValue::setFileName(std::string filename, Component Comp) {
    int iComp = CompToIndex_(Comp);
    fileName_[iComp] = filename;
  }

  void ElementFieldDynamicValue::setFunctionName(std::string funcName, Component Comp) {
    int iComp = CompToIndex_(Comp);
    functionName_[iComp] = funcName;
  }

  void ElementFieldDynamicValue::setCallbackXYZT(const CallbackXYZT &callabackXYZT, Component Comp) {
    int iComp = CompToIndex_(Comp);

    if ( typeValueOfElementField(Comp) != FROM_FUNCTION) {
      std::ostringstream msg;
      msg << "Attemp to std::set Component '"
          << strComponent[(int)Comp] << "' "
          << "of ElementFieldDynamicValue with a callback, "
          << "but value type for this component is "
          << strTypeValueOfElementField[(int)Comp] << std::endl;
      FEL_ERROR(msg.str());
    }

    callbackXYZT_[iComp] = callabackXYZT;
    isReady_[iComp] = true;
  }


  // access functions

  std::string ElementFieldDynamicValue::name() {
    return name_;
  }

  ElementFieldType ElementFieldDynamicValue::elementFieldType() {
    return elementFieldType_;
  }

  TypeValueOfElementField ElementFieldDynamicValue::typeValueOfElementField(Component Comp) {
    int iComp = CompToIndex_(Comp);
    return typeValueOfElementField_[iComp];
  }

  int ElementFieldDynamicValue::numComp() const {
    return numComp_;
  }

  double ElementFieldDynamicValue::constant(Component Comp) {
    int iComp = CompToIndex_(Comp);
    return constant_[iComp];
  }

  std::string ElementFieldDynamicValue::fileName(Component Comp) {
    int iComp = CompToIndex_(Comp);
    return fileName_[iComp];
  }

  std::string ElementFieldDynamicValue::functionName(Component Comp) {
    int iComp = CompToIndex_(Comp);
    return functionName_[iComp];
  }

  CallbackXYZT ElementFieldDynamicValue::callbackXYZT(Component Comp) {
    int iComp = CompToIndex_(Comp);
    FEL_ASSERT( isReady_[iComp] );
    return callbackXYZT_[iComp];
  }

  // other public functions

  void ElementFieldDynamicValue::display() {

    // title
    std::cout << "ElementFieldDynamicValue" << std::endl;


    // name
    std::cout << "  name             : "
         << name_
         << std::endl;

    // elementFieldType
    int itype = (int) elementFieldType_;
    FEL_ASSERT_LT( -2, itype);
    FEL_ASSERT_LT( itype, (int) numElementFieldType );
    std::cout << "  elementFieldType : "
         << (itype==-1 ? "Undefined" : strElementFieldType[itype])
         << std::endl;

    // numComp
    FEL_ASSERT_LT(numComp_,numCompMax_+1);
    std::cout << "  numComp          : "
         << (numComp_==-1 ? "Undefined" : std::to_string(numComp_).c_str())
         << std::endl;

    int vtype;

    for (int iComp=0 ; iComp<numComp_ ; ++iComp) {

      std::cout << "" << "  iComp = " << iComp+1 << std::endl;

      // typeValueOfElementField
      vtype = (int) typeValueOfElementField_[iComp];
      FEL_ASSERT_LT( -2, vtype);
      FEL_ASSERT_LT( vtype, (int) numTypeValueOfElementField);
      std::cout << "    typeValueOfElementField : "
           << (vtype==-1 ? "Undefined" : strTypeValueOfElementField[vtype])
           << std::endl;

      switch (vtype) {
      case FROM_CONSTANT:
        std::cout << "    Constant                : " << constant_[iComp] << std::endl;
        break;
      case FROM_FILE:
        std::cout << "    fileName                : " << fileName_[iComp] << std::endl;
        break;
      case FROM_FUNCTION:
        std::cout << "    functionName            : " << functionName_[iComp] << std::endl;
        break;
      default:
        // We have check this could not append
        FEL_ERROR("There is a bug with TypeValueOfElementField.");
        break;
      }

      std::cout << "    isReady                 : " << (isReady_[iComp] ? "Yes" : "No") << std::endl;

    }
  }

  // private function
  int ElementFieldDynamicValue::CompToIndex_(Component Comp) {
    int index = 0;

    switch (Comp) {
    case Comp1:
      index = 0;
      break;
    case Comp2:
      index = 1;
      break;
    case Comp3:
      index = 2;
      break;
    case CompNA:
    case Comp12:
    case Comp13:
    case Comp23:
    case Comp123:
      std::ostringstream msg;
      msg << "ElementFieldDynamicValue Component must be Comp1, Comp2, or Comp3, "
          << "got " << (int) Comp << ".";
      FEL_ERROR(msg.str());
    }

    return index;
  }

  /* ================================================================ */
  /* constructor, desctructor, initalize                              */
  /* ================================================================ */

  GetPotCustomized::GetPotCustomized():
    m_getpotFile(nullptr),
    m_getpotCl(nullptr),
    m_filename("data"),
    prefixOptionFelisce("--felisce-") {
  }

  void GetPotCustomized::initialize(int argc,char **argv,std::string filename) {
    m_filename = filename;

    //check file exists
    std::ifstream tmpFile(m_filename.c_str());
    if (! tmpFile.good() ) {
      std::ostringstream msg;
      msg << "Bad data file '" << m_filename  << "'. "
          << "File does not exists.'";
      FEL_ERROR(msg.str());
    }
    tmpFile.close();

    m_getpotFile = new GetPot(filename.c_str());
    m_getpotCl = new GetPot(argc, argv);

  }

  GetPotCustomized::~GetPotCustomized() {
    if(m_getpotFile) {
      delete m_getpotFile;
      m_getpotFile=nullptr;
    }
    if(m_getpotCl) {
      delete m_getpotCl;
      m_getpotCl=nullptr;
    }
  }

  /* ================================================================ */
  /* get scalar                                                       */
  /* ================================================================ */

  // std::string
  void GetPotCustomized::get(std::string& result, const char *name, const char *defaultValue, const char *help) {
    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    result = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    result = (*m_getpotCl)  ( paramFromCl.c_str(), result.c_str());
    if (help != nullptr) m_addToHelp(name,"STRING",defaultValue,help);
  }

  // integer
  void GetPotCustomized::get(int& result, const char *name, int defaultValue, const char *help) {
    validateIntParam(name);

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    result = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    result = (*m_getpotCl)  ( paramFromCl.c_str(), result);
    if (help != nullptr) m_addToHelp(name,"INTEGER",defaultValue,help);
  }

  // double
  void GetPotCustomized::get(double& result, const char *name, double defaultValue, const char *help) {
    validateDoubleParam(name);

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    result = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    result = (*m_getpotCl)  ( paramFromCl.c_str(), result);
    if (help != nullptr) m_addToHelp(name,"DOUBLE",defaultValue,help);
  }

  // boolean
  void GetPotCustomized::get(bool& result, const char *name, bool defaultValue, const char *help) {
    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    std::string resultStr;
    resultStr = (*m_getpotFile)( paramFromFile.c_str(), bool2string(defaultValue).c_str() );
    resultStr = (*m_getpotCl)  ( paramFromCl.c_str(), resultStr.c_str() );

    // convert std::string value to bool value
    try {
      result = string2bool(resultStr);
    } catch(...) {
      FEL_ERROR( "In file '" + m_filename
                 + "', section '" + m_section
                 + "', parameter '" + name
                 + "' expecting double, got '" + resultStr
                 + "'."
               );
    }

    if (help != nullptr) m_addToHelp(name,"[true|false]",defaultValue,help);
  }

  /* ================================================================ */
  /* get std::vector                                                       */
  /* ================================================================ */

  // std::string std::vector
  void GetPotCustomized::get(std::vector<std::string>& result, const char *name, const char *defaultValue, const char *help) {

    std::string onestring;

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    onestring = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    onestring = (*m_getpotCl)  ( paramFromCl.c_str(), onestring.c_str());

    // split "foo bar baz" into {"foo", "bar", "baz"}
    split(onestring,result," \n\t");
    if (help != nullptr) m_addToHelp(name,"STRING VECTOR",defaultValue,help);
  }

  // integer std::vector
  void GetPotCustomized::get(std::vector<int>& result, const char *name, const char *defaultValue, const char *help) {
    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    std::string onestring;
    onestring = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    onestring = (*m_getpotCl)  ( paramFromCl.c_str(), onestring.c_str());

    // split "1 2 3" into {"1", "2", "3"}
    std::vector<std::string> vecstring;

    split(onestring,vecstring," \n\t");


    // convert std::string elements to int elements
    result.clear();
    for ( std::size_t i = 0; i < vecstring.size(); i++) {
      try {
        result.push_back( std::stoi( vecstring[i] ) );
      } catch( const std::invalid_argument& ) {
        FEL_ERROR( "In file '" + m_filename
                   + "', section '" + m_section
                   + "', parameter '" + name
                   + "' expecting integer, got '" + vecstring[i]
                   + "'."
                 ) ;
      }
    }
    if (help != nullptr) m_addToHelp(name,"INTEGER VECTOR",defaultValue,help);
  }

  // double std::vector
  void GetPotCustomized::get(std::vector<double>& result, const char *name, const char *defaultValue, const char *help) {
    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    std::string onestring;
    onestring = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    onestring = (*m_getpotCl)  ( paramFromCl.c_str(), onestring.c_str());

    // split "1.0 2.0 3.0" into {"1.0", "2.0", "3.0"}
    std::vector<std::string> vecstring;
    split(onestring,vecstring," \n\t");

    // convert std::string elements to double elements
    result.clear();
    for ( std::size_t i = 0; i < vecstring.size(); i++) {
      try {
        result.push_back( std::stod( vecstring[i] ) );
      } catch( const std::invalid_argument& ) {
        FEL_ERROR( "In file '" + m_filename
                   + "', section '" + m_section
                   + "', parameter '" + name
                   + "' expecting double, got '" + vecstring[i]
                   + "'."
                 ) ;
      }
    }
    if (help != nullptr) m_addToHelp(name,"DOUBLE VECTOR",defaultValue,help);
  }

  // boolean std::vector
  void GetPotCustomized::get(std::vector<bool>& result, const char *name, const char *defaultValue, const char *help) {

    std::string onestring;

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);


    onestring = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
    onestring = (*m_getpotCl)  ( paramFromCl.c_str(), onestring.c_str());

    // split "true false true" into {"true", "false", "true"}
    std::vector<std::string> vecstring;
    split(onestring,vecstring," \n\t");

    // convert std::string elements to boolean elements
    result.clear();
    for ( std::size_t i = 0; i < vecstring.size(); i++) {
      try {
        result.push_back( string2bool( vecstring[i] ) );
      }

      catch(...) {
        FEL_ERROR( "In file '" + m_filename
                   + "', section '" + m_section
                   + "', variable '" + name
                   + "' expecting 'true' or 'false', got '" + vecstring[i]
                   + "'."
                 ) ;
      }
    }
    if (help != nullptr) m_addToHelp(name,"BOOLEAN VECTOR",defaultValue,help);
  }

  /* ================================================================ */
  /* require scalar                                                   */
  /* ================================================================ */

  // std::string
  void GetPotCustomized::require(std::string& result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,"",help);
  }

  // integer
  void GetPotCustomized::require(int& result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,0,help);
  }

  // double
  void GetPotCustomized::require(double& result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,0.,help);
  }

  // boolean
  void GetPotCustomized::require(bool& result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,false,help);
  }

  /* ================================================================ */
  /* require std::vector                                                   */
  /* ================================================================ */

  // std::string std::vector
  void GetPotCustomized::require(std::vector<std::string>& result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,"",help);
  }

  // integer std::vector
  void GetPotCustomized::require(std::vector<int>   & result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,"",help);
  }

  // double std::vector
  void GetPotCustomized::require(std::vector<double>& result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,"",help);
  }

  // boolean std::vector
  void GetPotCustomized::require(std::vector<bool>  & result, const char *name, const char *help) {
    raiseErrorIfNotExists(name);
    get(result,name,"",help);
  }

  /* ================================================================ */
  /* ElementFieldDynamicValue                                         */
  /* ================================================================ */

  std::set<std::string> GetPotCustomized::getElementFieldSection() {

    std::vector<std::string> unusedVariables =
      (*m_getpotFile).unidentified_variables( m_identifiedVariablesFile);

    std::string prefix = "ElementField:";
    std::string delimiters = "/";

    std::vector<std::string> sectionAndName;
    std::set<std::string> elementFieldSection;

    for (std::size_t i=0 ; i != unusedVariables.size() ; ++i) {

      std::string variable = unusedVariables[i]; // "section/name"

      if (startswith(variable,prefix)) {

        // split "section/name" into ["section","name"]
        split(variable, sectionAndName, delimiters);

        elementFieldSection.insert(sectionAndName[0]); // "section"
      }
    }

    return elementFieldSection;
  }

  ElementFieldDynamicValue* GetPotCustomized::getElementFieldDynamicValue(std::string section) {
    std::vector<std::string> words;
    const std::string space = " ";

    // read name
    std::string name = elementFieldNameFromSection(section);

    // create new ElementFieldDynamicValue
    ElementFieldDynamicValue *res = new ElementFieldDynamicValue(name.c_str());
    moveToSection(section);

    // read ElementFieldType
    ElementFieldType elementFieldType;
    require(elementFieldType,"type",(int) numElementFieldType,strElementFieldType);
    res->setElementFieldType(elementFieldType);

    // read numComp
    raiseErrorIfNotExists("Comp1");
    int numComp = 1;
    if ( exists("Comp2") ) {
      numComp = 2;
      if (exists("Comp3")) {
        numComp = 3;
      }
    } else {
      raiseErrorIfExists("Comp3");
    }
    res->setNumComp(numComp);

    // read TypeValueOfElementField and corresponding value
    Component Comp;
    for (int iComp=0 ; iComp < numComp ; ++iComp) {
      Comp = res->allComp[iComp];

      // data file contains for example a line:
      //   Comp1 = "FROM_FUNCTION f0"
      // (Comp1,Comp2,Comp3 authorized).

      // read data file line
      std::string strName = strComponent[Comp]; // strName = "Comp1"
      std::string strValue;
      require(strValue,strName.c_str()); // strValue = "FROM_FUNCTION  f0"
      split(strValue, words, space); // words = {"FROM_FUNCTION", "f0"}

      // check right value ("FROM_FUNCTION f0") is two blank-separated words
      if (words.size() != 2) {
        std::ostringstream msg;
        msg << "Bad data file '" << m_filename  << "'. "
            << "'In section '" << m_section
            << "', parameter '" << strName
            << "' expected: 'FROM_[CONSTANT|FILE|FUNCTION] value', "
            << "got: '" << strValue << "'.";
        FEL_ERROR(msg.str());
      }

      // convert typeValueOfElementField std::string to enum ("FROM_FUNCTION" -> FROM_FUNCTION)
      int buf = ppcharIndex(
                  numTypeValueOfElementField,
                  strTypeValueOfElementField,
                  words[0].c_str());

      // check the conversion is successfull
      if (buf == -1) {
        std::ostringstream msg;
        msg << "Bad data file '" << m_filename  << "'. "
            << "'In section '" << m_section
            << "', parameter '" << strName
            << "' expected: 'FROM_[CONSTANT|FILE|FUNCTION]', "
            << "got: '" << words[0] << "'.";
        FEL_ERROR(msg.str());
      }

      TypeValueOfElementField typeValueOfElementField = static_cast<TypeValueOfElementField>(buf);


      // record typeValueOfElementField in the object that the function will return
      res->setTypeValueOfElementField(typeValueOfElementField,Comp);

      //check elementFieldType and typeValueOfElementField are compatible
      if (elementFieldType==CONSTANT_FIELD) {
        if (typeValueOfElementField==FROM_FUNCTION ||
            typeValueOfElementField==FROM_FILE) {
          std::ostringstream msg;
          msg << "Bad data file '" << m_filename  << "'. "
              << "'In section '" << m_section
              << "', parameter '" << strName
              << "', only FROM_CONSTANT are allowed for CONSTANT_FIELD, "
              << "got: '" << strTypeValueOfElementField[typeValueOfElementField] << "'.";
          FEL_ERROR(msg.str());
        }
      }

      // read and record correponding value ("f0")
      switch (typeValueOfElementField) {
        case FROM_CONSTANT:
          try {
            const double constant = std::stod(words[1]);
            res->setConstant(constant,Comp);
          } catch( const std::invalid_argument&) {
            FEL_ERROR( "In file '" + m_filename
                        + "', section '" + m_section
                        + "', parameter '" + name
                        + "' expecting double, got '" + words[1]
                        + "'."
                    ) ;
          }
          break;
        case FROM_FILE:
          res->setFileName(words[1],Comp);
          break;
        case FROM_FUNCTION:
          res->setFunctionName(words[1],Comp);
          break;
          // Default case should appear with a warning at compile time instead of an error in runtime
          // (that's truly the point of using enums as switch cases)
          //  default:
          // We have check this could not append
          //     FEL_ERROR("There is a bug with TypeValueOfElementField.");
          //    break;
      }
    }

    return res;
  }

  std::string GetPotCustomized::elementFieldNameFromSection(std::string section) {

    std::string name;

    // check that section std::string is like "ElementField_thename"
    std::string prefix = "ElementField:";
    if (! startswith(section,prefix) || section.length() <= prefix.length() ) {
      std::ostringstream msg;
      msg << "Bad data file '" << m_filename  << "'. "
          << "Failed to extract elementField name from section '"
          << section << "'.";

      FEL_ERROR(msg.str());
    }

    // get "thename" from "ElementField:thename"
    name = section.substr(prefix.length(), section.length()-prefix.length() );

    return name;
  }

  /* ================================================================ */
  /* various functions                                                */
  /* ================================================================ */

  // move to a section
  void GetPotCustomized:: moveToSection(std::string section) {
    m_section = section;
  }

  // detect ufos
  void GetPotCustomized::checkUndefinedVariable() {

    m_identifiedVariablesCl.emplace_back("--felisce-cfg");

    // configuration file ufos
    std::vector<std::string> variable_ufos =
      (*m_getpotFile).unidentified_variables( m_identifiedVariablesFile);

    if (variable_ufos.size()) {
      std::cout << "Incorrect input file, the following variables are undefined:" << std::endl;
      for (auto it = variable_ufos.begin(); it != variable_ufos.end(); ++it) {
        std::cout << "    " << *it << std::endl ;
      }
      FEL_ERROR("'Incorrect input file.'");
    }

    // command line ufos
    std::vector<std::string> current;
    std::vector<std::string> all;
    std::vector<std::string> empty;

    // the follwing suffer strange getpot bugs, but it's almost good
    // it may become wrong if getpot bugs are fixed...

    // variables
    all = (*m_getpotCl).unidentified_variables(m_identifiedVariablesCl);

    // options
    current = (*m_getpotCl).unidentified_options();
    for (auto it = current.begin(); it != current.end(); ++it) {
      if ( it->find('=') == std::string::npos )
        all.push_back( *it );
    }

    // nominuses
    current = (*m_getpotCl).unidentified_nominuses(m_identifiedVariablesCl);
    all.insert( all.end(), current.begin(), current.end() );

    if ( all.size()) {
      std::cout << "The following options passed to command line are incorrect:" << std::endl;

      for (auto it = all.begin(); it != all.end(); ++it)
        std::cout << "    " << *it << std::endl ;

      std::cout << std::endl << "Good syntax is for example (use --help for a list of section and variable):" << std::endl;
      std::cout << "--felisce-section-variable='astring'" << std::endl;
      std::cout << "--felisce-section-variable='true'" << std::endl;
      std::cout << "--felisce-section-variable='1. 2. 3.'" << std::endl << std::endl;
      FEL_ERROR("'Incorrect command line option(s)");
    }
  }

  // check that blank-separated std::string lists have the same lengths
  void GetPotCustomized::assertSameLength(int count, ...) {

    va_list args;
    char *name;
    char *name_ref = nullptr;
    std::size_t length, length_ref = 0.;

    va_start(args,count);

    // getting the length of the first blank-separated std::string list.
    // this will be the reference
    if (count>0) {
      name_ref = va_arg(args,char*);
      length_ref = getLength(name_ref);
    }

    // comparing all the others lengths with the reference.
    // if one length is not equal to the reference, this is an error.
    for (int i = 1; i < count; i++) {
      name = va_arg(args,char*);
      length = getLength(name);
      if (length != length_ref) {
        std::ostringstream msg;
        msg << "bad input file: ";
        msg << "In section '" << m_section << "', ";
        msg << "'" << name << "' (length=" << length << ")";
        msg << " and '" << name_ref << "' (length=" << length_ref << ")";
        msg << " must have the same length,";
        FEL_ERROR(msg.str());
      }
    }

    va_end(args);
  }

  std::unordered_map<std::string, std::vector<std::string> > GetPotCustomized::getHelp() {
    return m_help;
  }

  bool GetPotCustomized::exists(const char *name) {
    std::string valueStr;

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);


    // check that the value read as a std::string is not ""
    valueStr = (*m_getpotFile)( paramFromFile.c_str(), "");
    valueStr = (*m_getpotCl)  ( paramFromCl.c_str(), valueStr.c_str());

    if (valueStr=="")
      return false;
    else
      return true;
  }


  void GetPotCustomized::raiseErrorIfNotExists(const char *name) {
    if (! exists(name) ) {
      std::ostringstream msg;
      msg << "Bad data file '" << m_filename << "':"
          << "In section '" << m_section << "', "
          << "'" << name << "' is recquired.";
      FEL_ERROR(msg.str().c_str());
    }
  }

  void GetPotCustomized::raiseErrorIfExists(const char *name) {
    if ( exists(name) ) {
      std::ostringstream msg;
      msg << "Bad data file '" << m_filename << "':"
          << "In section '" << m_section << "', "
          << "'" << name << "' is incompatible with other data.";
      FEL_ERROR(msg.str().c_str());
    }
  }
  // get the length of a blank-separated std::string list
  std::size_t GetPotCustomized::getLength(const char *name) {
    std::string path = m_section + '/' + name;
    std::string str = (*m_getpotFile)(path.c_str(),"");
    std::vector <std::string> vstring;
    split(str,vstring," \n\t");
    return vstring.size();
  }

  void GetPotCustomized::validateIntParam(const char* name) {
    std::string resultStr;

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);


    resultStr = (*m_getpotFile)( paramFromFile.c_str(), "1");
    resultStr = (*m_getpotCl)  ( paramFromCl.c_str(), resultStr.c_str());
    try {
      std::stoi(resultStr);
    } catch( const std::invalid_argument&) {
      FEL_ERROR( "In file '" + m_filename
                 + "', section '" + m_section
                 + "', parameter '" + name
                 + "' expecting integer, got '" + resultStr
                 + "'."
               ) ;
    }
  }

  void GetPotCustomized::validateDoubleParam(const char* name) {
    std::string resultStr;

    std::string paramFromFile = m_paramFromFile(name);
    std::string paramFromCl = m_paramFromCl(name);

    resultStr = (*m_getpotFile)( paramFromFile.c_str(), "1.");
    resultStr = (*m_getpotCl)  ( paramFromCl.c_str(), resultStr.c_str());
    try {
      std::stod(resultStr);
    } catch( const std::invalid_argument&) {
      FEL_ERROR( "In file '" + m_filename
                 + "', section '" + m_section
                 + "', parameter '" + name
                 + "' expecting double, got '" + resultStr
                 + "'."
               ) ;
    }
  }


  /* ================================================================ */
  /* privates functions                                               */
  /* ================================================================ */

  std::string GetPotCustomized::m_paramFromFile(const char *name) {
    std::string pathInFile = m_section + (std::string) "/" + (std::string) name;

    // register this section+variable as valid, for latter ufo detection
    m_identifiedVariablesFile.push_back(pathInFile);

    return pathInFile;
  }

  std::string GetPotCustomized::m_paramFromCl(const char *name) {

    // the option name is: --felisce-section-variable
    std::string option_name = prefixOptionFelisce
                         + m_section
                         + "-"
                         + std::string(name)
                         ;

    // register this option as valid, for latter ufo detection
    m_identifiedVariablesCl.push_back(option_name);

    return option_name;
  }


  void GetPotCustomized::addToIdentifiedVariables(const char *name) {
    m_identifiedVariablesCl.emplace_back(name);
    m_identifiedVariablesFile.emplace_back(name);
  }

}
