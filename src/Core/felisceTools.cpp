//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Sebastien Gilles
//

// System includes
#include <fstream>
#include <set>
#include <vector>

// External includes

// Project includes
#include "felisceTools.hpp"
#include "felisce_error.hpp"

namespace felisce::Tools 
{
  void allGatherSet( std::set<int> &locCont,  std::set<int> &globCont ) 
  {
    int locContainerAsVec[locCont.size()];
    int locCount(0), globCount(0);

    for (auto it = locCont.begin(); it != locCont.end(); it++ ) {
      locContainerAsVec[locCount] = *it;
      locCount++;
    }
      
    MPI_Allreduce(&locCount, &globCount,1,MPI_INT,MPI_SUM,MpiInfo::petscComm());

    int globContainerAsVec[globCount];

    int displacements[MpiInfo::numProc()];
    int localCountBroadcasted[MpiInfo::numProc()];
    int ones[MpiInfo::numProc()];

    for ( int proc(0); proc < MpiInfo::numProc(); proc++) {
      localCountBroadcasted[proc]=0;
      displacements[proc] = proc;
      ones[proc] = 1;
    }
    
    MPI_Allgatherv(&locCount,1,MPI_INT,localCountBroadcasted,ones,displacements,MPI_INT,MpiInfo::petscComm());
    
    displacements[0]=0;
    for ( int proc(0); proc < MpiInfo::numProc()-1; proc++) {
      displacements[proc+1] = displacements[proc] + localCountBroadcasted[proc];
    }

    MPI_Allgatherv(locContainerAsVec,locCount,MPI_INT,globContainerAsVec,localCountBroadcasted,displacements, MPI_INT,MpiInfo::petscComm());

    for ( int i(0); i< globCount; i++ ) {
      globCont.insert( globCont.end(), globContainerAsVec[i] );
    }
  }
  } // namespace felisce


