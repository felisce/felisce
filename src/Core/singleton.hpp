//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Cedric Doucet
//

/// \file singleton.hpp
/// \authors Cedric Doucet
/// \date 06/06/2014
/// \brief Implementation of singleton design pattern.

#ifndef SINGLETON_HPP
#define SINGLETON_HPP

// System includes
#include <unordered_map>

// External includes

// Project includes

/// \brief Singleton class may be used to create classes which can be instantiated once at most.
/// If Foo is such a class, it must fulfill the following requirements: \n
///
/// 1. Foo must publicly inherit from Singleton<Foo>; \n
/// 2. Foo must declare Singleton<Foo> as a friend class; \n
/// 3. Foo must have no other constructor than the default one; \n
/// 4. Foo's default constructor must be private; \n
/// 5. Foo's destructor must be private; \n
/// 6. Foo must be a non-copyable class; \n
///
/// In summary, Foo class should be based on the following example:
/// \code
/// class Foo : public Singleton<Foo> {
///
///   friend class Singleton<Foo>;
///
/// private:\n
///   Foo();  // defined in foo.cpp \n
///   ~Foo(); // defined in foo.cpp \n
///
/// private: \n
/// Foo(Foo const &);              // NEVER defined (non-copyable)! \n
/// Foo & operator=(Foo const &);  // NEVER defined (non-assignable)! \n
///
/// };
/// \endcode

namespace felisce 
{
  template<class T>
  class Singleton {

    //=================================
    // Public interface
    //=================================
  public:
    /// \brief Return the unique instance of the singleton
    static T & instance(const std::size_t instanceIndex = 0);

  protected:
    Singleton() = default;
    ~Singleton();
    //==================================
    // Singleton class MUST NOT be copyable!
    // Do NOT modify encapsulation here
    //==================================
  private:
    /// \brief Copy constructor is NOT available (NEVER defined)
    Singleton(Singleton const &);
    /// \brief Assignement operator is NOT available (NEVER defined)
    Singleton& operator=(Singleton const &);

    //=================================
    // Data members
    //=================================
  protected:
    /// \brief Global pointer on the unique instance of the singleton
    static std::unordered_map<std::size_t, T*> m_instance;

  }; // class Singleton

} // namespace felisce

// System includes

// External includes

// Project includes
#include "singleton.tpp"

#endif // SINGLETON_HPP
