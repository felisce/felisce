//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    https://www.boost.org/doc/libs/1_74_0/libs/smart_ptr/doc/html/smart_ptr.html#intrusive_ptr
//

#if !defined(FELISCE_MEMORY_H_INCLUDED )
#define  FELISCE_MEMORY_H_INCLUDED

/* System includes */
#include <iostream>
#include <utility>
#include <memory>

/* External includes */
#include "intrusive_ptr.hpp"

namespace felisce {

template<class T>
using shared_ptr = std::shared_ptr<T>;

template<class T>
using weak_ptr = std::weak_ptr<T>;

template<class T>
using unique_ptr = std::unique_ptr<T>;

template<typename C, typename...Args>
intrusive_ptr<C> make_intrusive(Args &&...args) {
    return intrusive_ptr<C>(new C(std::forward<Args>(args)...));
}
template<typename C, typename...Args>
shared_ptr<C> make_shared(Args &&...args) {
    return std::make_shared<C>(std::forward<Args>(args)...);
}

template<typename C, typename...Args>
unique_ptr<C> make_unique(Args &&...args) {
    // Note: std::make_unique is C++14, this can be updated once we upgrade from C++11
    return unique_ptr<C>(new C(std::forward<Args>(args)...));
}

template<class T>
std::ostream& operator <<(std::ostream& rOStream, const felisce::weak_ptr<T>& rData) {

  if(!rData.expired())
    rOStream << *rData.lock().get();
  else
    rOStream <<" expired weak_ptr ";

  return rOStream;
}

template<class T>
std::ostream& operator <<(std::ostream& rOStream, const felisce::intrusive_ptr<T>& rData) {
  rOStream << *rData.get();
  return rOStream;
}

} // namespace felisce

#define FELISCE_CLASS_POINTER_DEFINITION(a) typedef felisce::shared_ptr<a > Pointer; \
typedef felisce::shared_ptr<a > SharedPointer; \
typedef felisce::weak_ptr<a > WeakPointer; \
typedef felisce::unique_ptr<a > UniquePointer

namespace felisce {
template< class T > class GlobalPointer;
}

#define FELISCE_CLASS_INTRUSIVE_POINTER_DEFINITION(a) typedef typename felisce::intrusive_ptr<a > Pointer; \
typedef felisce::GlobalPointer<a > WeakPointer; \
typedef felisce::unique_ptr<a > UniquePointer; \
typename a::Pointer shared_from_this(){ return a::Pointer(this); }

#endif /* FELISCE_MEMORY_H_INCLUDED  defined */
