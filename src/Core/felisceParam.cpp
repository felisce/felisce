//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes
#include <iomanip>
#include <ctime>
#include <regex>
#include <numeric>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Hyperelasticity/pressureDataIncompressible.hpp"

namespace felisce
{
FelisceParam::FelisceParam()
{
}

/***********************************************************************************/
/***********************************************************************************/

FelisceParam::~FelisceParam()
{
}

/***********************************************************************************/
/***********************************************************************************/

UniqueFelisceParam::UniqueFelisceParam(const std::size_t instanceIndex):
  mInstanceIndex(instanceIndex),
  timer(false)
{
}

/***********************************************************************************/
/***********************************************************************************/

UniqueFelisceParam::~UniqueFelisceParam()
{
  for (auto it = elementFieldDynamicValueMap.begin(); it != elementFieldDynamicValueMap.end(); it++) {
    delete it->second;
  }
}

/***********************************************************************************/
/***********************************************************************************/

CommandLineOption UniqueFelisceParam::initialize(const int argc, const char** argv)
{
  auto opt = CommandLineOption(argc, argv, mInstanceIndex);
  this->initialize(opt);
  return opt;
}

/***********************************************************************************/
/***********************************************************************************/

void UniqueFelisceParam::initialize(CommandLineOption& opt)
{
  parseInputFile(opt.argcFelisce(), opt.argvFelisce(), opt.dataFileName());
  if (opt.printHelp()) {
    printHelp();
    exit(0);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void UniqueFelisceParam::parseInputFile(int argc, char** argv,std::string filename)
{
  // Create out customized parser
  GetPotCustomized gpc;
  gpc.initialize(argc, argv, filename);

  // Debug
  gpc.moveToSection("debug");
  gpc.get(m_verbose,"verbose", 0,"");

  // Chrono
  gpc.moveToSection("chrono");
  gpc.get(chronoToScreen,"toScreen",false,"Output chrono to screen");
  gpc.get(chronoToFile,"toFile",false,"Write chrono measures to file <resultDir>/chrono.md");
  if ( chronoToScreen || chronoToFile )
    timer.State() = true;

  // Data
  gpc.moveToSection("data");
  gpc.get(inputDirRaw,"inputDirectory","./","Directory where to read data. "
          "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
  inputDirectory = inputDirRaw;
  if (inputDirectory.at(inputDirectory.length()-1) != '/') inputDirectory += '/';
  inputDirectory = expandEnvironmentVariables(filename,"data","inputDirectory",inputDirectory);
  FEL_WARNING_IF_NO_DIRECTORY(inputDirectory);
  gpc.get(inputFile,"inputFile", "none","");
  gpc.get(podBasisFile,"podBasisFile","non","File containing the POD basis to be used.");
  gpc.get(timeMaxCaseFile,"timeMaxCaseFile",0,"Maximum time of the case file- to do:  delete and read directly in the file");
  gpc.get(readDisplFromFile,"readDisplFromFile",false,"Bool variable to impose external movement to the mesh.");
  gpc.get(readPreStressedDisplFromFile,"readPreStressedDisplFromFile",false,"Bool variable to impose pre-stressed displacement in an FSI simulation.");

  // Transient
  gpc.moveToSection("transient");
  gpc.get(timeStep,"timeStep",0.1,"Time step beetween two iterations.");
  gpc.get(timeMax,"timeMax",0.,"Maximum time");
  gpc.get(time,"time",0.,"Physical time");
  gpc.get(timeStepCharact,"timeStepCharact",0.01,"Time step tolerated while backtracking the characteristic lines");
  iteration = 0;
  gpc.get(timeIterationMax,"timeIterationMax",1,"Maximum number of time iterations");
  //    iterationMax = (int)((timeMax - time) / timeStep);
  gpc.get(frequencyWriteSolution, "frequencyWriteSolution", 1,"");
  gpc.get(periodNbSteps, "periodNbSteps", 1,"");
  gpc.get(intervalWriteSolution, "intervalWriteSolution", "0 0","");
  FEL_ASSERT_EQUAL(intervalWriteSolution.size(),2);
  //FEL_CHECK(intervalWriteSolution.size()==2, "intervalWriteSolution must be an interval, with a size equal to 2")
  gpc.get(integrationTimeMethod,"integrationTimeMethod", "EXPLICIT_EULER", "Time integration method - used for instance in ALPmodel and thin-walled solid models.");
  gpc.get(writeSolutionMeshes,"writeSolutionMeshes",false,"call writeSolutionAndMeshes() write .geo in time");

  // Variable
  gpc.moveToSection("variable");
  gpc.assertSameLength(2,"variable","typeOfFiniteElement");
  gpc.get(nameVariable,"variable","None","");
  gpc.get(typeOfFiniteElement,"typeOfFiniteElement","None","0:linear, 1:quadratic, 2:bubble");
  for (std::size_t i=0; i<typeOfFiniteElement.size(); ++i) {
    if ( typeOfFiniteElement[i] < 0 || typeOfFiniteElement[i] > 2 ) {
      std::ostringstream msg;
      msg << "In section variable, typeOfFiniteElement must be an array of 0 (linear) or 1 (quadratic) or 2 (bubble), but " << i << "st " << "element value is : " << typeOfFiniteElement[i] << std::endl;
      FEL_ERROR(msg.str().c_str());
    }
  }
  gpc.get(degreeOfExactness,"degreeOfExactness","2","The degree of exactness in the FE Gauss integration");
  gpc.assertSameLength(2,"variable","degreeOfExactness");
  gpc.get(decomposePlaneTransverse,"decomposePlaneTransverse","false","If the integration is decomposed in plane-transverse integration");
  std::string auxiliar_string_exactness = std::to_string(degreeOfExactness[0]);
  for (std::size_t i = 1; i < nameVariable.size(); i++) {
    auxiliar_string_exactness += " ";
    auxiliar_string_exactness += std::to_string(degreeOfExactness[i]);
  }
  gpc.get(planeDegreeOfExactness,"planeDegreeOfExactness",auxiliar_string_exactness.c_str(),"Same default as global degreeOfExactness");
  gpc.get(transverseDegreeOfExactness,"transverseDegreeOfExactness",auxiliar_string_exactness.c_str(),"Same default as global degreeOfExactness");
  physicalVariable.clear();
  // TODO: Replace with switch
  for ( std::size_t i = 0; i < nameVariable.size(); i++) {
  gpc.get(idMesh, "idMesh", "0", "id of the mesh associated to the variable");

    if (nameVariable[i] == "temperature")
      physicalVariable.push_back(temperature);

    else if (nameVariable[i] == "velocityAdvection")
      physicalVariable.push_back(velocityAdvection);

    else if (nameVariable[i] == "velocity")
      physicalVariable.push_back(velocity);

    else if (nameVariable[i] == "pressure")
      physicalVariable.push_back(pressure);

    else if (nameVariable[i] == "velocityDiv")
      physicalVariable.push_back(velocityDiv);

    else if (nameVariable[i] == "potExtraCell")
      physicalVariable.push_back(potExtraCell);

    else if (nameVariable[i] == "potTransMemb")
      physicalVariable.push_back(potTransMemb);

    else if (nameVariable[i] ==  "section")
      physicalVariable.push_back(section);

    else if (nameVariable[i] ==  "displacement")
      physicalVariable.push_back(displacement);

    else if (nameVariable[i] ==  "rotation")
      physicalVariable.push_back(rotation);

    else if (nameVariable[i] == "potThorax")
      physicalVariable.push_back(potThorax);

    else if (nameVariable[i] == "turbulenceK")
      physicalVariable.push_back(turbulenceK);

    else if (nameVariable[i] == "turbulenceOM")
      physicalVariable.push_back(turbulenceOM);

    else if (nameVariable[i] == "iop")
      physicalVariable.push_back(iop);

    else if (nameVariable[i] == "vorticity")
      physicalVariable.push_back(vorticity);

    else if (nameVariable[i] == "velocity_square")
      physicalVariable.push_back(velocity_square);

    else if (nameVariable[i] == "stressX")
      physicalVariable.push_back(stressX);

    else if (nameVariable[i] == "stressY")
      physicalVariable.push_back(stressY);

    else if (nameVariable[i] == "stressZ")
      physicalVariable.push_back(stressZ);

    else if (nameVariable[i] == "stressDotNormal")
      physicalVariable.push_back(stressDotNormal);

    else if (nameVariable[i] == "WG")
      physicalVariable.push_back(WG);

    else if (nameVariable[i] == "wallShearStress")
      physicalVariable.push_back(wallShearStress);

    else if (nameVariable[i] == "pressureDarcy")
      physicalVariable.push_back(pressureDarcy);

    else if (nameVariable[i] == "lagMultiplier")
      physicalVariable.push_back(lagMultiplier);

    else if (nameVariable[i] == "None")
      ;

    else {
      std::cout << "Physical variable = " << nameVariable[i] << std::endl;
      FEL_ERROR("This physical is not in the Felisce database");
    }
  }

  // Mesh
  gpc.moveToSection("mesh");
  gpc.get(meshDirRaw,"meshDir","./","Directory where to read data. " "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
    {     
      meshDir = meshDirRaw;
      if (meshDir.at(meshDir.length()-1) != '/') meshDir += '/';
      meshDir = expandEnvironmentVariables(filename,"mesh","meshDir",meshDir);
      FEL_WARNING_IF_NO_DIRECTORY(meshDir);
    }
  gpc.get(inputMesh,        "inputMesh","default.mesh","Input Mesh file name");
  gpc.get(resultDirRaw,"resultDir","./","Directory where to write data. " "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
    {
      resultDir = resultDirRaw;
      if (resultDir.at(resultDir.length()-1) != '/') resultDir += '/';
      resultDir = expandEnvironmentVariables(filename,"mesh","resultDir",resultDir);
    }  
  gpc.get(outputMesh,       "outputMesh","out.geo","Ouput Mesh file name");
    {
      FEL_CHECK(inputMesh.size() == outputMesh.size(), "inputMesh and outputMesh must be the same sizes");
    }
  gpc.get(prefixName,"prefixName","default","?");
    {
      FEL_CHECK(prefixName.size() == inputMesh.size(), "prefixName, inputMesh and outputMesh must have the same size");
    }
  gpc.get(resultDirStressesRaw,"resultDirStresses","./","Directory where to write data. " "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
    {
      resultDirStresses= resultDirStressesRaw;
      if (resultDirStresses.at(resultDirStresses.length()-1) != '/') resultDirStresses += '/';
      resultDirStresses = expandEnvironmentVariables(filename,"mesh","resultDirStresses",resultDirStresses);
    }
  gpc.get(outputFileFormat, "outputFileFormat", 0, "0 = ensight6, 1 = ensight gold, 2 = binary ensight6"); // TODO D.C. same for every mesh? or depends on the variables
  gpc.get(reinterpolation,"reinterpolation",false,"true if you treat the Numerical Locking"); // TODO D.C. same for every mesh? does it depend on the mesh or on the FE used (in this case maybe move in appropriate section)
  gpc.get(extendToLowerDimensions,"extendToLowerDimensions",false,"true if we consider lower order identities as boundaries");  // TODO D.C. same for every mesh?
  gpc.get(choiceDirectionTangent,"choiceDirectionTangent","e1"," choice direction to build Tangents vectors to shell model ; 1-> e=x, 2-> e=y, 3-> e=z "); // TODO D.C. same for every mesh?
  gpc.get(whenExtrudePreserveOriginalCoordinates,"whenExtrudePreserveOriginalCoordinates",true,"If we preserve the original coordinates when extruding"); // TODO D.C. same for every mesh?
  gpc.get(checkVolumeOrientation,"checkVolumeOrientation",false,"Check if the elements are inverted or not");  // TODO D.C. same for every mesh?
  gpc.get(checkNormalOrientation,"checkNormalOrientation",false,"Checks if the normals are properly oriented");  // TODO D.C. same for every mesh?
  gpc.get(CVGraphInterface,"CVGraphInterface","default","Matching file for coupling"); // TODO D.C. same for every mesh?
  gpc.get(duplicateSupportDof, "duplicateSupportDof", false, "Set to true if some support dof are to be duplicated");  // TODO D.C. same for every mesh? this ooes not really depend only on the meshes, it involves meshes variales solutions etc...
  gpc.get(effective2dGEOmesh, "effective2dGEOmesh", false, "Set to true if GEO file, which is 3D, is effectively 2D for XFEM, etc."); // TODO D.C. same for every mesh?
  gpc.get(flipNormal, "flipNormal", false, "if set to true it will flip all the normals"); // TODO D.C. same for every mesh?
  gpc.get(readNodesReferences, "readNodesReferences", false, "if we read the ids of the nodes"); // TODO D.C. same for every mesh?
  gpc.get(notBoundarySurfaceLabels, "notBoundarySurfaceLabels", "", "Labels of surface element that are part of the domain but are not boundary"); // TODO D.C. same for every mesh? the real question is: can different meshes have the same labels???

  std::string section;
  std::vector<int> mesh_label;
  std::vector<std::string> mesh_stringLabel;
  
  //iterating over elements type (eg: Nodes, Segments2, Triangles3, Triangles6, Tetrahedra4,...)
  FEL_CHECK((std::size_t)GeometricMeshRegion::m_numTypesOfElement==GeometricMeshRegion::eltEnumToFelNameGeoEle.size(),"missing some elements");
  for (int i=0 ; i< GeometricMeshRegion::m_numTypesOfElement ; i++) {
    section = GeometricMeshRegion::eltEnumToFelNameGeoEle[i].first;
    gpc.moveToSection(section.c_str());
    gpc.get(mesh_label,"label","","label indice");
    gpc.get(mesh_stringLabel,"stringLabel","","label name associated to label indice");

    //iteration over label and stringLabel
    for (unsigned int j=0; j<std::min(mesh_label.size(),mesh_stringLabel.size()) ; j++)
      felName2MapintRef2DescLine[ section ][ mesh_label[j] ] = mesh_stringLabel[j];

    for (unsigned int j=0; j< mesh_stringLabel.size(); j++)
      GeometricMeshRegion::descriptionLineEnsightToListLabel[mesh_stringLabel[j]].insert(mesh_label[j]); // TODO D.C. only one? the real question is: can different meshes have the same labels???

    mesh_label.clear();
    mesh_stringLabel.clear();
  }
  // TODO D.C. for multiple meshes
  //iterating over elements type (eg: Nodes, Segments2, Triangles3, Triangles6, Tetrahedra4,...)
  // felName2MapintRef2DescLine.resize(inputMesh.size());
  // for (std::size_t iMesh = 0; iMesh < inputMesh.size(); ++iMesh) {
  //   std::stringstream iMeshString;
  //   iMeshString << iMesh;

  //   for (int i=0 ; i< GeometricMeshRegion::m_numTypesOfElement ; i++) {
  //     section = GeometricMeshRegion::eltEnumToFelNameGeoEle[i].first;
  //     gpc.moveToSection(section.c_str()+iMeshString.str().c_str());
  //     gpc.get(mesh_label,"label","","label indice");
  //     gpc.get(mesh_stringLabel,"stringLabel","","label name associated to label indice");

  //     //iteration over label and stringLabel
  //     for (unsigned int j=0; j<std::min(mesh_label.size(),mesh_stringLabel.size()) ; j++)
  //       felName2MapintRef2DescLine[iMesh][ section ][ mesh_label[j] ] = mesh_stringLabel[j];

  //     for (unsigned int j=0; j< mesh_stringLabel.size(); j++)
  //       GeometricMeshRegion::descriptionLineEnsightToListLabel[mesh_stringLabel[j]].insert(mesh_label[j]);

  //     mesh_label.clear();
  //     mesh_stringLabel.clear();
  //   }
  // }

  // Geometry
  gpc.moveToSection("geometry");
  gpc.get(Geo.tolrescaling, "tolrescaling", 1., "Rescaling factor for the default geometric tolerance.");

  // Physics
  gpc.moveToSection("physics");
  gpc.get(unit,"unit",1,"?");
  gpc.get(spaceUnit,"spaceUnit",1.,"This number multiplies the coordinates of the mesh");
  gpc.get(testCase,"testCase",0,"Test case number");
  gpc.get(model,"model","model","Model name");
  gpc.get(u_max_inlet,"u_max_inlet", 1., "Maximum velocity at the inlet when parabolic case is chosen");
  gpc.get(max_flow_scaling,"max_flow_scaling", 1., "Parameter to scale unitary flow inlet");
  gpc.get(T_sis, "T_sis", 0.25, "systolic time");
  gpc.get(T, "T", 0.8, "Duration of one cardiac cycle");
  gpc.get(symmetry, "symmetry", 0., "Factor to control symmetry of bc at inlet");
  gpc.get(dimension, "dimension", "", "Physical dimesion for every problem");

  //POD
  gpc.moveToSection("POD");
  gpc.get(nbTestCase,"nbTestCase", 16, "number of testCase to consider (the first nbTestCase of the list)");
  gpc.get(snap_centered,"snap_centered",0,"Consider the snapshots centered with respect to the temporal mean");
  gpc.get(L2Mass,"L2Mass",0,"Comparison between POD and full model is performed by mass matrix");
  gpc.get(output_dir, "output_dir", "./PODBasis", "directory where the pod basis will be write");
  gpc.get(output_file, "output_file", "PODbasis", "pod basis filename (filename.dim, filename.mean filename.pod)");
  gpc.get(case_dir,"case_dir","../../rb_offline_phase/offline_data_base/SIM#0", "list of the directory where find the case_file");
  gpc.get(case_file_name, "case_file_name", "NavierStokes.case", "name of the case file corresponding to the case_dir directory");
  gpc.get(snap_interval, "snap_interval", "[0,1,200]", "for each testCase associate the following: [start,step,end];");
  gpc.get(variable_name, "variable_name", "pressure", "the name of the variables in ensight");
  gpc.get(verbose_ensight, "verbose_ensight", true, "verbosity of ensight reader");
  gpc.get(podFolder, "podFolder", "./POD", "POD folder");
  gpc.get(timeStepSnap, "timeStepSnap", 1, "time difference between snapshots");

  //Optimal Reconstruction
  gpc.moveToSection("optrec");
  gpc.get(targetHR,"targetHR", 0.0, "target heart rate of patient");
  gpc.get(HRwindowSize, "HRwindowSize", 0.0, " ");
  gpc.get(image_modality, "image_modality", "CFI", " ");
  gpc.get(patients_data_dir, "patients_data_dir", "/", " ");
  gpc.get(sistolicTime, "sistolicTime", 0.0, " ");
  gpc.get(timeWindowSize,"timeWindowSize", 0.0, "size of time window to do model reduction");
  gpc.get(nbMeasures, "nbMeasures", -1, "number of sample volumes");
  gpc.get(modelReductionTechnique,"modelReductionTechnique", "--", "Either POD or MBOMP");
  gpc.get(nbModes,"nbModes",10,"number of modes used in the reconstruction");
  gpc.get(targetTime, "targetTime","[0,0.01,0.5]", " ");
  gpc.get(useField2Measures, "useField2Measures", true, "bool to std::set measures origin");
  gpc.get(doSnapshotsSVD, "doSnapshotsSVD", true, "bool to std::set is the POD have to be done");
  gpc.get(calculateRieszRepresenters, "calculateRieszRepresenters", true, "bool to std::set if the Riesz representers have to be calculated calculated");
  gpc.get(measuresFile, "measuresFile", "./velocityMeasures", "file of measures");
  gpc.get(targetSim, "targetSim", "./targetSim", ".vct file with target simulation");
  gpc.get(rrFolder, "rrFolder", "./RieszRep", "path to save Riesz representers");
  gpc.get(fieldFolder, "fieldFolder", "./uReconst", "path to save reconstructed field");
  gpc.get(hashTableMesh, "hashTableMesh", "hashTable.mesh", "unestructured mesh representing hash table to locate points in the domain");

  //PressureEstimation
  gpc.moveToSection("PressureEstimation");
  gpc.get(inletBoundaryLabel,"inletBoundaryLabel", 0, "Inlet boudary label. Pressyre drops will be calculated respect to this label.");
  gpc.get(wallBoundaryLabel,"wallBoundaryLabel", 0, "Wall boundary label.");
  gpc.get(outletBoundaryLabels,"outletBoundaryLabels", "", "Outlet boundary labels.");
  gpc.get(testFunctionsPath, "testFunctionsPath", "", "Path of functions to test momentum conservation equation.");
  gpc.get(uStarPath, "uStarPath", "", "Path to u*");

  //PostProcCFD
  gpc.moveToSection("PostProcCFD");
  gpc.get(caseDirCFD, "caseDirCFD", "./Solution", "Case file directory");
  gpc.get(caseFileNameCFD, "caseFileNameCFD", "./ns", "Case file name");

  //Rom
  gpc.moveToSection("rom");
  gpc.get(ROMmethod,"ROMmethod","ROMmethod","ROM method: ALP, ALP-DEIM, POD.");
  gpc.get(solveEigenProblem,"solveEigenProblem",false,"Set to true if solve an eigenvalues problem.");
  gpc.get(optimizePotential,"optimizePotential",false,"Set to true if the initial potential is optimized to minimize the initial error in the solution.");
  gpc.get(chiSchrodinger,"chiSchrodinger",0.0,"Parameter of Schrodinger operator when solving ALP eigen problem.");
  gpc.get(useROM,"useROM",false,"Set to true if use a reduced order model.");
  gpc.get(dimRomBasis,"dimRomBasis",0,"Dimension of reduced model basis.");
  gpc.get(dimCollocationPts,"dimCollocationPts",0,"Number of collocation points for ALP-DEIM-ROM method.");
  gpc.get(numApproxMode,"numApproxMode",0,"Number of modes used to fix initial potential.");
  gpc.get(nCutOff,"nCutOff",0,"Size of the space = dimCollocationPts in ALP-DEIM case.");
  gpc.get(collocationPtsFile,"collocationPtsFile","collocationPtsFile","File containing collocation points id for ALP-DEIM-ROM method.");
  gpc.get(useImprovedRec,"useImprovedRec",false,"Set to true if you want to use the improved reconstruction");
  gpc.get(dimOrthComp,"dimOrthComp",0,"Dimension of orthogonal complement to improve the reconstruction");
  gpc.get(orderALP,"orderALP",0,"Order of ALP tensors.");
  gpc.get(readBasisFromFile,"readBasisFromFile",false,"Read basis from ensight files or calculate a new RO basis.");
  gpc.get(writeBasisEvolution,"writeBasisEvolution",false,"Write basis in ensight files at each time step.");
  gpc.get(hasSource,"hasSource",false,"Has a term source.");
  gpc.get(numberOfSource,"numberOfSource",0,"Number of source terms.");
  gpc.get(numberOfSnapshot, "numberOfSnapshot",0,"Number of snapshots.");
  gpc.get(samplingFrequency,"samplingFrequency",0,"Sampling Frequency.");

  // Fluid
  gpc.moveToSection("fluid");
  gpc.get(orderBdfNS, "orderBdfNS",1,"Bdf order for NS model.");
  gpc.get(orderBdfFS, "orderBdfFS",1,"Bdf order for fractional step model.");
  gpc.get(quasistatic, "quasistatic", false, "When true the term rho/dt in the NS system is std::set to zero. Everywhere else (Windkessel, stabilization, ...) nothing is changed");
  gpc.get(density,"density",1.,"Fluid density.");
  gpc.get(viscosity,"viscosity",1.,"Fluid Viscosity.");
  gpc.get(NS_alpha,"NS_alpha",0.,"like a capacitance in Rp,C,Rd lumpedModelBC model");
  gpc.get(NS_beta,"NS_beta",0.,"like inverse of distal resistance in Rp,C,Rd lumpedModelBC model");
  gpc.get(NS_gamma,"NS_gamma",0.,"like proximal resistance in Rp,C,Rd lumpedModelBC model");
  gpc.get(NSmodifFlag,"NSmodifFlag",0,"Flag: 0 = no modified NS model");
  gpc.get(NS_modifLabel,"NS_modifLabel",1492817,"label where we apply modified NS");
  gpc.get(characteristicMethod,"characteristicMethod",0,"to switch from semi-implicit resolution (= 0) to method of characteristics (= 1 with RK4 scheme, else with Euler scheme)");
  gpc.get(addedBoundaryFlag,"addedBoundaryFlag",0,"Flag to add scpecific boundary terms: 0= add nothing, 1 = RIS model, 2 = outflow stab, 3 = cardiac cycle");
  gpc.get(stabilizationLabel,"stabilizationLabel","","Label where we use stabilization");
  gpc.get(stabilizationCoef,"stabilizationCoef",0.,"Coef used in stabilization function");
  gpc.get(inletLabel,"inletLabel",0,"Label of an inlet of interest");
  gpc.get(outletLabel,"outletLabel",0,"Label of an outlet of interest");
  gpc.get(referenceValue1,"referenceValue1",1.,"General purpose reference value");
  gpc.get(referenceValue2,"referenceValue2",1.,"General purpose reference value");
  gpc.get(referenceValue3,"referenceValue3",1.,"General purpose reference value");
  gpc.get(referenceValue4,"referenceValue4",1.,"General purpose reference value");
  gpc.get(explicitAdvection,"explicitAdvection",0,"Advection in Fractional Step.");
  gpc.get(CVGraphInterfaceVariationalBC,"CVGraphInterfaceVariationalBC",0,"Apply variational BC in RHS for coupling");

  gpc.get(NitscheStabilizParam,"NitscheStabilizParam",0.,"Stabilization parameter for Nitsche's formulation");
  gpc.get(NitschePenaltyParam,"NitschePenaltyParam",0.,"Penalty parameter for Nitsche's formulation");
  gpc.get(ContactTolerance, "ContactTolerance", 0.01, "Tolerance value for the contact with a simple straight wall handled by the structure");
  gpc.get(useALEformulation,"useALEformulation",0,"Use the NS ALE formulation: 0: no ALE 1: standard ALE, 2: stabilized ALE");
  gpc.get(bcInRefConfiguration,"bcInRefConfiguration",false,"BC applied in the ALE reference configuration");

  gpc.get(useElasticExtension,"useElasticExtension",false,"Use a linear elastic extension to construct the ALE map (By default, an harmonic ext. is used)");

  gpc.get(initALEDispByFile,"initALEDispByFile",false,"Initial ALE mesh displacement read from file");

  gpc.get(updateMeshByVelocity,"updateMeshByVelocity",false,"Harmonic extension evaluated on the velocity");
  gpc.get(useSymmetricStress,"useSymmetricStress",false,"Use the symmetric stress in the variational formulation");
  gpc.get(NSequationFlag,"NSequationFlag",1,"Implementation method of convective term");
  gpc.get(stabSUPG,"stabSUPG",0.1,"SUPG stabilization parameter");
  gpc.get(stabdiv,"stabdiv",1.,"div stabilization parameter");
  gpc.get(typeSUPG,"typeSUPG",1,"SUPG stabilization type");
  gpc.get(NSStabType, "NSStabType", 0, "0 : SUPG, 1: Face-oriented");
  gpc.get(stabFOVel, "stabFOVel", 0.01, "velocity jump coefficient for face-oriented stabilization");
  gpc.get(stabFOPre, "stabFOPre", 0.01, "pressure jump coefficient for face-oriented stabilization");

  gpc.get(orderPressureExtrapolation, "orderPressureExtrapolation", 0, "NSFracStep pressure extrapolation");
  gpc.get(incrementalFS, "incrementalFS", 0, "Flag for FracStep incremental scheme");
  gpc.get(coefRotScheme, "coefRotScheme", 0., "NSFracStep rotational incremental coef");

  gpc.get(theta,"theta",1.,"Theta parameter in the theta method: 1: (default) Backward Euler,  0.5: Crank Nicolson");

  gpc.get(nonLinearFluid, "nonLinearFluid", false, "activation / deactivation non linear solver");



  // Fluid-Structure Interaction (FSI)
  gpc.moveToSection("fsi");
  gpc.get(fsiCoupling,"fsiCoupling",false,"Enable tools for fsi coupling (residual computation, etc.)");
  gpc.get(typeOfInterface,"typeOfInterface","conform","conform (ALE) or immersed");
  gpc.get(fsiRNscheme,"fsiRNscheme",-1000,"-1: RN inner-iterations, 0: RN no-extrap, 1: RN 1st-order extrap, 2: RN 2nd-order extrap");
  gpc.get(gammaRN,"gammaRN",1.,"Robin parameter for RN iterations");
  gpc.get(useLumpedMassRN,"useLumpedMassRN",false,"Penalization of the structure edges to impose homogeneous boundary conditions");
  gpc.get(useMassLumping, "useMassLumping", false, "Penalization of the matrix mass with fe.measure()/fe.numDof() to fit the fsi coupling into the FictitiousDomain-RN explicit scheme");
  gpc.get(fsiInterfaceLabel,"fsiInterfaceLabel","1","Label(s) of the solid interface surface in a FSI problem");
  // These parameters control the domain decomposition loop implemented in IOPcouplModel and in stokesLinearElasticityCoupledModel
  // However, the different accelerations of the fixed point scheme are implemented in linearProblem
  // and the parameters can be used in other different models.
  gpc.get(domainDecompositionToll,"domainDecompositionToll",1.e-4,"Tollerance on the relative error. Used, for instance, in stokesLinearElasticityCoupledModel");
  gpc.get(domainDecompositionMaxIt,"domainDecompositionMaxIt",100,"Maximum number of . Used, for instance, in stokesLinearElasticityCoupledModel");
  gpc.get(accelerationMethod,"accelerationMethod",0,"acceleration method for coupling. 0: default fixed point ( no acceleration indeed ), 1: relaxation, you have to give a parameter omegaAcceleration, 2: aitken 3: aitken modified");
  gpc.get(omegaAcceleration,"omegaAcceleration",1,"see accelerationMethod");
  gpc.get(hasOldMaster,"hasOldMaster",false,"old version of MasterFSI");
  gpc.get(betaNormalized,"betaNormalized","1.","?");
  gpc.get(sectionAtRest,"sectionAtRest","1.","?");

  // ZeroMQ
  gpc.moveToSection("ZeroMQ");
  gpc.get(socketTransport, "socketTransport", "tcp", "communication transport protocol");
  gpc.get(socketAddress, "socketAddress", "127.0.0.1", "transport-specific address (interface) to connect to");
  gpc.get(socketPort, "socketPort", "5555", "for tcp transport, specifies the port on the interface");

  // Immersed data for the structure penalisation
  gpc.moveToSection("immersedData");
  gpc.get(hasImmersedData,"hasImmersedData",false,"");
  gpc.get(meshDirImmersedStruct, "meshDirImmersedStruct", "./", "directory where to read data");
  gpc.get(meshFileImmersedStruct, "meshFileImmersedStruct", "", "name of the mesh file");
  gpc.get(penaltyCoeffImmersedStruct, "penaltyCoeffImmersedStruct", 1.e-6, "value of the penalisation coefficient");
  gpc.get(allowProjectionImmersedStruct, "allowProjectionImmersedStruct", 0, "boolean to specify the projection");
  gpc.get(valueSourceImmersedStruct, "valueSourceImmersedStruct", 0., "value used for the additional term in the source term");
  gpc.get(useMassMatrix, "useMassMatrix", 1, "Specify the content of the fourth block of the fluid matrix for the implicit immersed problems. 1: Penalisation coefficient * Mass matrix of the structure. 0: Penalisation coefficient.");
  gpc.get(hasDivDivStab,"hasDivDivStab",false,"");
  gpc.get(localDivDivPenalty,"localDivDivPenalty",1.,"local penalty coefficient for the div div stabilization.");
  gpc.get(divDivStabPostProcess,"divDivStabPostProcess",false,"boolean to print the results relative to the intersection of the structure mesh with the fluid mesh in files \"intersection.****.scl\"");
  gpc.get(intersectedSubElementsPostProcess,"intersectedSubElementsPostProcess",false,"boolean to print the results relative to the edges of the fluid mesh intersected by the structure mesh in files \"intersectedEdges.****.scl\"");

  gpc.get(useFDlagrangeMult,"useFDlagrangeMult",false,"");
  gpc.get(massConstraint,"massConstraint",false,"");
  gpc.get(japanLabels, "japanLabels", "", "Labels of the boundary elements use to compute Japan constraint");
  gpc.get(BHstab,"BHstab",false,"");
  gpc.get(BHstabSymm,"BHstabSymm",true,"");
  gpc.get(BPstab,"BPstab",false,"");
  gpc.get(stabCoeffLag,"stabCoeffLag",1,"");
  gpc.get(useFicItf,"useFicItf",false,"");
  gpc.get(usePhyAndFict,"usePhyAndFict",false,"");  
  gpc.get(flipItfNormal,"flipItfNormal",false,"");
  gpc.get(useAverageNormal,"useAverageNormal",false,"");
  gpc.get(compPressure,"compPressure",false,"");
  gpc.get(compPresGerm,"compPresGerm",0,"");
  FEL_CHECK( (BHstab != true) || (BPstab != true), "Choose only one stabilization between Barbosa-Hughes and Brezzi-Pitkaranta" );
  FEL_CHECK( !((massConstraint == false) && (useFicItf == true)), "If massConstraint is false then useFicItf msut be false" );

  // Interface to M1G library
  gpc.moveToSection("M1G");
  gpc.get(M1G.enable,"enable",false,"Set to true to use M1G module (provided with libXfm)");
  gpc.get(M1G.verbose,"verbose",0,"Set verbosity level");
  gpc.get(M1G.Hscaling,"Hscaling",1.,"Set H scaling factor");
  gpc.get(M1G.CheckOrientation,"CheckOrientation",true,"Set flag for checking orientation");
  gpc.get(M1G.labelPhys,"labelPhys","","Edges labels of the physical interface to be linked with the fictitious interface");
  gpc.get(M1G.labelFict,"labelFict","","Edges labels of the fictitious interface to be linked with the physical interface");
  gpc.get(M1G.triLst,"triLst","","List of vertices composing the germ triangles for M1G module");
  gpc.get(M1G.verLst,"verLst","","List of germ vertices for M1G module");
  gpc.get(M1G.outdir,"outdir",resultDir.data(),"Directory where to write data." "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
  {
    if ( M1G.outdir.at(M1G.outdir.length()-1) != '/') M1G.outdir += '/';
    M1G.outdir = expandEnvironmentVariables(filename,"M1G","outdir",M1G.outdir);
  }
  gpc.get(M1G.writeM1Gmeshes,"writeM1Gmeshes",false,"");
  FEL_CHECK(M1G.triLst.size() % 3 == 0, "M1G.triLst, size must be multiple of 3 (since every triangle has 3 vertices...)");

  // Solid
  gpc.moveToSection("solid");
  gpc.get(densitySolid,"densitySolid",1.,"Solid density");
  gpc.get(young,"young",-1.,"Young coefficient.");
  gpc.get(poisson,"poisson",-1.,"Poisson coefficient.");
  gpc.get(coeffReaction,"coeffReaction",-1.,"coeffReaction.");
  gpc.get(lambda_lame,"lambda_lame",-1.,"Lame Lambda coefficient.");
  gpc.get(mu_lame,"mu_lame",-1.,"Lame mu coefficient.");
  gpc.get(thickness,"thickness",0.,"Thickness.");
  gpc.get(alpha_rayleigh,"alpha_rayleigh",0.,"Mass Rayleigh damping coefficient.");
  gpc.get(beta_rayleigh,"beta_rayleigh",0.,"Beta Rayleigh damping coefficient.");
  gpc.get(hasRayleighDamping,"hasRayleighDamping",false,"Rayleigh damping matrix.");
  gpc.get(planeStressStrain, "planeStressStrain", -1, "Kinematic parameter; 1 -> plane stress; 2 -> plane strain");
  gpc.get(volumicOrSurfacicForce, "volumicOrSurfacicForce", -1, "1 for a volumic force is applied, 2 for a surfacic one");
  gpc.get(CiarletGeymonat.kappa1, "kappa1", -1., "Ciarlet-Geymonat kappa1 parameter");
  gpc.get(CiarletGeymonat.kappa2, "kappa2", -1., "Ciarlet-Geymonat kappa2 parameter");
  gpc.get(hyperelastic_bulk, "bulk", 1.0e24, "Ciarlet-Geymonat or St Venant Kirchhoff parameter");
  gpc.get(Ogden.C1, "C1", -1., "Ogden C1 parameter");
  gpc.get(Ogden.C2, "C2", -1., "Ogden C2 parameter");
  gpc.get(Ogden.a, "a"  , -1., "Ogden  a parameter");
  gpc.get(volumic_mass, "volumic_mass", -1., "Volumic mass");
  gpc.get(hasSolidMidpoint, "hasSolidMidpoint", false, "Midpoint time integration in the structure solver");
  gpc.get(solidRadius, "solidRadius", 0., "Radius of a cylinder.");

  gpc.get(nbSubRegions, "nbSubRegions", -1., "number of terminal regions.");
  gpc.get(flagJacobi, "flagJacobi", -1., "flag 0 is system diagonal is computed at each time step for Jacobi peconditionning, else if only the first time step diagonal is used all over the simulation");

  gpc.get(typeOfElasticityModel, "typeOfElasticityModel", "nonLinear", "type of elasticity used for the solid model");
  // for thin shell models
  gpc.get(typeOfShellModel, "typeOfShellModel", "Mitc3", "type of model used for the thin shell structure solver");
  gpc.get(contactLabelPair,"contactLabelPair","","contact labels");
  gpc.get(rayOrientation,"rayOrientation",""," 1 or -1 w.r.t to the element normal");
  gpc.get(symmetricContact, "symmetricContact", true, "symmetric contact between master and slave");
  gpc.get(contactGap, "contactGap", 0., "minimal distance between 2 solids.");
  gpc.get(contactPenaltyParam, "contactPenaltyParam", 0., "penalty parameter for contact.");
  gpc.get(evalEnergy, "evalEnergy",false, "evaluate mechanical energy for contact problems");
  gpc.get(velocityConstrained, "velocityConstrained",false, "add a velocity constraint for FSI with enclosed domains");

  // For linear elasticity
  gpc.get(timeScheme,"timeScheme",0,"0: standard finite difference, 1: newmark scheme");
  gpc.get(gammaNM,"gamma",0.5,"Gamma parameter in the newmark method method.");
  gpc.get(betaNM,"beta",0.25,"Beta parameter in the newmark method");

  // Beam
  gpc.moveToSection("beam");
  gpc.get(beam.sub_integration,"sub_integration",1,"If we consider subintegrated formulation");
  gpc.get(beam.timoshenko,"timoshenko",true,"If we consider Timoshenko formulation");
  gpc.get(beam.diameter,"diameter",-1.0,"Diameter of the beam.");
  gpc.get(beam.inner_diameter,"inner_diameter",-1.0,"Inner diameter of the beam.");
  gpc.get(beam.depth,"depth",1.0,"Depth of the beam.");
  gpc.get(beam.cross_area,"cross_area",0.0,"The cross area of the beam. It's relative to the axial deformation");
  gpc.get(beam.effective_area_y,"effective_area_y",0.0,"The effective cross area of the beam to the shear in the y direction");
  gpc.get(beam.effective_area_z,"effective_area_z",0.0,"The efffective cross area of the beam to the shear in the z direction");
  gpc.get(beam.moment_of_intertia_y,"moment_of_intertia_y",0.0,"This is the moment of inertia of the beam in the y axis");
  gpc.get(beam.moment_of_intertia_z,"moment_of_intertia_z",0.0,"This is the moment of inertia of the beam in the z axis");
  gpc.get(beam.torsional_inertia,"torsional_inertia",0.0,"This is the torsional inertia of the beam");

  if (beam.diameter > 0.0) { // Considering circular section
    beam.cross_area = M_PI/4.0 * std::pow(beam.diameter, 2);
    beam.effective_area_y = 0.9 * M_PI/4.0 * std::pow(beam.diameter, 2);
    beam.effective_area_z = beam.effective_area_y;
    beam.moment_of_intertia_y = M_PI/64.0 * std::pow(beam.diameter, 4);
    beam.moment_of_intertia_z = beam.moment_of_intertia_y;
    beam.torsional_inertia = 2.0 * beam.moment_of_intertia_y;
    if (beam.inner_diameter > 0.0) { // Considering hole in the circular section
      beam.cross_area -= M_PI/4.0 * std::pow(beam.inner_diameter, 2);
      beam.effective_area_y -= 0.9 * M_PI/4.0 * std::pow(beam.inner_diameter, 2);
      beam.effective_area_z = beam.effective_area_y;
      beam.moment_of_intertia_y -= M_PI/64.0 * std::pow(beam.inner_diameter, 4);
      beam.moment_of_intertia_z = beam.moment_of_intertia_y;
      beam.torsional_inertia = 2.0 * beam.moment_of_intertia_y;
    }
  } else if (beam.depth > 0.0 && thickness > 0.0) { // Considering rectangular section
    beam.cross_area = beam.depth * thickness;
    beam.effective_area_y = 5.0/6.0 * beam.depth * thickness;
    beam.effective_area_z = beam.effective_area_y;
    beam.moment_of_intertia_y = 1.0/12.0 * std::pow(beam.depth, 3) * thickness;
    beam.moment_of_intertia_z = 1.0/12.0 * beam.depth * std::pow(thickness, 3);
    //beam.torsional_inertia = (thickness * beam.depth * (std::pow(thickness, 2) + std::pow(beam.depth, 2)))/12.0;
    const double h = thickness > beam.depth ? thickness : beam.depth;
    const double w = thickness < beam.depth ? thickness : beam.depth;
    beam.torsional_inertia = 1.0/16.0 * h * std::pow(w, 3) * (16.0/3.0 - 3.36 * w/h * (1.0 - std::pow(w, 4)/(12.0 * std::pow(h, 4))));
  }

  // Poroelasticity
  gpc.moveToSection("poroElasticity");
  gpc.get(biotCoefficient,"biotCoefficient",1.,"Coefficient b in the linear poroelastic formulation");
  gpc.get(biotModulus,"biotModulus",1.,"Modulus M in the linear poroelastic formulation, in Pascal");
  gpc.get(isoPermeab,"isoPermeab",1.,"k0 in the poroelastic model");
  gpc.get(anisoPermeab,"anisoPermeab",1.,"k1 in the poroelastic model");
  gpc.get(validationTest,"validationTest",0,"id of the validation test to run");
  gpc.get(scalePressureEq,"scalePressureEq",false,"to scale (or not) the pressure equations");

  // fkpp
  gpc.moveToSection("fkpp");
  gpc.get(sourcePos, "sourcePos", "", "position of the source");

  // Nitsche-XFEM formulation
  gpc.moveToSection("nitscheXFEM");
  gpc.get(useDynamicStructure, "useDynamicStructure", true, "Dynamic structure or not");
  gpc.get(useGhostPenalty, "useGhostPenalty", true, "Use the ghost penalty stabilization");
  gpc.get(coefGhostPenalty, "coefGhostPenalty", 1., "Set the coefficient for the ghost penalty stabilization");
  gpc.get(useProjectionForNitscheXFEM, "useProjectionForNitscheXFEM", true, "use a projection of the solution from one supportDofMesh to another between time step");
  gpc.get(useInterfaceFlowStab, "useInterfaceFlowStab", false, "stabilized the convection at the interface");
  gpc.get(couplingSchemeStoppingCriteria, "couplingSchemeStoppingCriteria", 0, "0 = fix number of iteration, 1= relative error of velocity of structure");
  gpc.get(useODESolve, "useODESolve", false, "true = different solve function for contact problems, false = classic solve function");
  gpc.get(usePenaltyFreeNitscheMethod, "usePenaltyFreeNitscheMethod", false, "false = classic Nitsche's formulation, true = penalty free Nitsche's formulation");
  gpc.get(contactDarcy, "contactDarcy", 0, "0 = porous media non considered, 1 = porous media with zero tangential traction, 2 = BJS condition for tangential traction");
  gpc.get(epsDarcy, "epsDarcy", 0. , "Set the coefficient for the darcy problem");
  gpc.get(kDarcy, "kDarcy", 0. , "Set the coefficient for the darcy problem");
  gpc.get(writeXFEMmeshes, "writeXFEMmeshes", false, "Print intersected meshes or not");
  gpc.get(confIntersection,"confIntersection",false,"Conformal intersection algorithm");
  gpc.get(structures,"structures","","Structure label (mesh file).");
  gpc.get(numficxstr,"numficxstr","","Number of fictitious structure per structure");
  gpc.get(fictitious,"fictitious","","Fictitious structure label (mesh file)(one or more for evry structure)");
  FEL_CHECK(structures.size() == numficxstr.size(), "Error in input file: the dimensions of 'structures' and 'numficxstr' differ");
  FEL_CHECK(std::accumulate(numficxstr.begin(), numficxstr.end(), 0u) == fictitious.size(), "Error in input file: 'fictitious' size does not match the size indicated in numficxstr");
  gpc.get(meanLabel,"meanLabel","","");




  //elec
  gpc.moveToSection("elec");
  gpc.get(hasCoupledAtriaVent,"hasCoupledAtriaVent",false,"Bool variable to couple atria and ventricles.");
  gpc.get(orderBdfEdp, "orderBdfEdp",1,"Bdf order for system solver.");
  gpc.get(orderBdfIonic, "orderBdfIonic",1,"Bdf order for ionic solver.");
  //elec - parameters
  gpc.get(typeOfIonicModel,"typeOfIonicModel","schaf","Type of ionic model : 'schaf'=Mitchell-Schaeffer, 'fhn'=Fitzhugh-Nagumo, 'court'=Courtemanche-Ramirez-Nattel, 'courtAtriaSchafVent'=Courtemanche-Ramirez-Nattel for Atria and Mitchell-Schaeffer for ventricles, 'MV'=minimal ventricular, 'Paci'=Paci.");
  gpc.get(spontaneous,"spontaneous",true);
  gpc.get(printIonicVar,"printIonicVar",false,"Bool variable to print the ionic variable in an ensight file with its own case file.");
  //elec - Schaf solver
  gpc.get(tauOpen,"tauOpen",300.,"tau_open parameter for second Mitchell and Schaeffer equation.");
  gpc.get(hasHeteroTauClose,"hasHeteroTauClose",false,"Bool variable to std::set heterogeneous tau_close.");
  gpc.get(hasHeteroCourtPar,"hasHeteroCourtPar",false,"Bool variable to std::set heterogeneous parameters in Courtemanche model.");
  gpc.get(hasHeteroCondAtria,"hasHeteroCondAtria",false,"Bool variable to std::set heterogeneous conductivities in Atria (BB, CT, F0 ...).");
  gpc.get(tauClose,"tauClose",100.,"Homogeneous tau_close parameter for second Mitchell and Schaeffer equation.");
  gpc.get(tauCloseEndo,"tauCloseEndo",130.,"In case of hasHeteroTauClose=true, left ventricle endocardial value.");
  gpc.get(tauCloseCell,"tauCloseCell",140.,"In case of hasHeteroTauClose=true, left ventricle median cells value.");
  gpc.get(tauCloseEpi,"tauCloseEpi",90.,"In case of hasHeteroTauClose=true, left ventricle epicardial value.");
  gpc.get(tauCloseRV,"tauCloseRV",120.,"In case of hasHeteroTauClose=true, right ventricle value.");
  gpc.get(tauIn,"tauIn",8.,"tau_in parameter for first Mitchell and Schaeffer equation.");
  gpc.get(tauOut,"tauOut",180.,"tau_out parameter for first Mitchell and Schaeffer equation.");
  gpc.get(kTanhMSR,"kTanhMSR",1.,"k parameter for tanh of the second Mitchell and Schaeffer Revised equation.");
  gpc.get(vMin,"vMin",-80.,"Minimal value of transmembrane potential.");
  gpc.get(vMax,"vMax",20.,"Minimal value of transmembrane potential.");
  gpc.get(vGate, "vGate",-67.,"Gate value of transmembrane potential for Mitchell and Schaeffer model.");
  //elec - FhN solver
  gpc.get(epsilon, "epsilon",0.0032,"epsilon parameter for fHn Model (warning: usually epsilon<<1).");
  gpc.get(beta, "beta",1.0,"beta parameter for fHn Model, multipliyng w in ionic eq. (if different to 1: Aliev-Panfilov model).");
  gpc.get(gammaEl, "gammaEl",0.32,"gamma parameter for fHn Model (multipliyng Vm).");
  gpc.get(f0, "f0",1.0,"Amplification factor of ionic current ");
  gpc.get(alpha, "alpha",0.25,"alpha parameter of ion current (f0*v(v-a)(1-v)) ");
  //elec - MV solver parameters for inverse problem
  gpc.get(tau_so_1_epi, "tau_so_1_epi",-1.,"tau_so_1 parameter for MV model in epicardium. ");
  gpc.get(tau_so_1_endo, "tau_so_1_endo",-1.,"tau_so_1 parameter for MV model in endocardium. ");
  gpc.get(tau_so_1_mcel, "tau_so_1_mcel",-1.,"tau_so_1 parameter for MV model in midmiocardium. ");
  gpc.get(tau_so_1_rv, "tau_so_1_rv",-1.,"tau_so_1 parameter for MV model in right ventricle. ");
  //elec - MV conductance parameters
  gpc.get(gfi_rv,"gfi_rv",1.,"Jfi multiplication factor for MV model in right ventricle. ");
  gpc.get(gso_rv,"gso_rv",1.,"Jso multiplication factor for MV model in right ventricle. ");
  gpc.get(gsi_rv,"gsi_rv",1.,"Jsi multiplication factor for MV model in right ventricle. ");
  gpc.get(gfi_endo,"gfi_endo",1.,"Jfi multiplication factor for MV model in LV endocardium. ");
  gpc.get(gso_endo,"gso_endo",1.,"Jso multiplication factor for MV model in LV endocardium. ");
  gpc.get(gsi_endo,"gsi_endo",1.,"Jsi multiplication factor for MV model in LV endocardium. ");
  gpc.get(gfi_mid,"gfi_mid",1.,"Jfi multiplication factor for MV model in LV mid-myocardium. ");
  gpc.get(gso_mid,"gso_mid",1.,"Jso multiplication factor for MV model in LV mid-myocardium. ");
  gpc.get(gsi_mid,"gsi_mid",1.,"Jsi multiplication factor for MV model in LV mid-myocardium. ");
  gpc.get(gfi_epi,"gfi_epi",1.,"Jfi multiplication factor for MV model in LV epicardium. ");
  gpc.get(gso_epi,"gso_epi",1.,"Jso multiplication factor for MV model in LV epicardium. ");
  gpc.get(gsi_epi,"gsi_epi",1.,"Jsi multiplication factor for MV model in LV epicardium. ");
  //elec - Courtemanche solver parameters for inverse problem (see HeteroCourtModelMultCoeff::CourtCondMultCoeff in cardiacFunction.cpp for default values)
  gpc.get(g_Na_PM, "g_Na_PM",-1.,"g_Na parameter in Courtemanche ionic model, multiplicative factor in Pectinate Muscles : g_Na_RegTissue*g_Na_PM.");
  gpc.get(g_Na_CT, "g_Na_CT",-1.,"g_Na parameter in Courtemanche ionic model, multiplicative factor in Crista Terminalis : g_Na_RegTissue*g_Na_CT.");
  gpc.get(g_Na_BB, "g_Na_BB",-1.,"g_Na parameter in Courtemanche ionic model, multiplicative factor in Bachmann Bundle : g_Na_RegTissue*g_Na_BB.");
  // elec-params
  gpc.get(Am, "Am",200.,"Rate of membrane area per volume unit.");
  gpc.get(Cm, "Cm",0.001,"Membrane capacitance.");
  gpc.get(extraTransvTensor, "extraTransvTensor",0.0012,"Extracellular conductivity in the transverse direction of fibers.");
  gpc.get(intraTransvTensor, "intraTransvTensor",0.0003,"Intracellular conductivity in the transverse direction of fibers.");
  gpc.get(extraFiberTensor, "extraFiberTensor",0.003,"Extracellular conductivity along fibers direction.");
  gpc.get(intraFiberTensor, "intraFiberTensor",0.003,"Intracellular conductivity along fibers direction.");
  gpc.get(extraTransvTensorAtria, "extraTransvTensorAtria",0.0012,"Extracellular conductivity in the transverse direction of fibers  for the atria.");
  gpc.get(intraTransvTensorAtria, "intraTransvTensorAtria",0.0003,"Intracellular conductivity in the transverse direction of fibers for the atria.");
  gpc.get(extraFiberTensorAtria, "extraFiberTensorAtria",0.003,"Extracellular conductivity along fibers direction for the atria.");
  gpc.get(intraFiberTensorAtria, "intraFiberTensorAtria",0.003,"Intracellular conductivity along fibers direction for the atria.");
  gpc.get(extraTransvTensorVent, "extraTransvTensorVent",0.0012,"Extracellular conductivity in the transverse direction of fibers for the ventricles.");
  gpc.get(intraTransvTensorVent, "intraTransvTensorVent",0.0003,"Intracellular conductivity in the transverse direction of fibers for the ventricles.");
  gpc.get(extraFiberTensorVent, "extraFiberTensorVent",0.003,"Extracellular conductivity along fibers direction for the ventricles.");
  gpc.get(intraFiberTensorVent, "intraFiberTensorVent",0.003,"Intracellular conductivity along fibers direction for the ventricles.");
  gpc.get(monodomain, "monodomain",false,"Tells if monodomain or bidomain will be solved (default: bidomain).");
  gpc.get(sigmaThorax,"sigmaThorax",0.0006,"Thorax tissue conductivity.");
  gpc.get(sigmaLung,"sigmaLung",0.00024,"Lungs tissue conductivity.");
  gpc.get(sigmaBone,"sigmaBone",0.00004,"Bones conductivity.");
  gpc.get(CellsType,"CellsType","epi","Type of cell");
  //elec - applied current
  gpc.get(typeOfAppliedCurrent,"typeOfAppliedCurrent","","Type of applied current: 'ellibi'=normal, 'Qwave'=can simulate Q-wave - need delay, 'constant'=homogeneous on the domain, 'multiple'=for accelerated beats, 'BBB'=boundle brunch block, 'sphere'=a sphere centered in x0,y0,z0, 'zygote'=non-pathological case for zygote geometry...");

  //For MEA
  gpc.get(NbElectrodes,"NbElectrodes",1,"Number of electrodes");
  gpc.get(ElecStim,"ElecStim","1","Number of electrode stimulation");
  gpc.get(pulseValues,"pulseValues","10.","Values of pulses");
  gpc.get(pulseValuesDuration,"pulseValuesDuration","1.","Duration of pulse");
  gpc.get(pulseType,"pulseType","monophasic","Type of pulse");
  gpc.get(timePeriodMEA,"timePeriodMEA","500.","Time period for a new pulse");
  gpc.get(timePeriodMEAtrain,"timePeriodMEAtrain","0.","Time period for a new train");
  gpc.get(trainDuration,"trainDuration","0.","Duration of one train");
  gpc.get(tMaxStim,"tMaxStim","0.","time max for stimulations");
  gpc.get(Ri,"Ri","0.","Internal Resistances");
  gpc.get(Rel,"Rel","0.","Electrodes Resistances");
  gpc.get(Cel,"Cel","0.","Electrodes Capacitances");
  gpc.get(Diam,"Diam","0.","Electrodes diameters");


  gpc.get(hasAppliedExteriorCurrent,"hasAppliedExteriorCurrent",false,"Bool variable to have an exterior applied current.");
  gpc.get(typeOfAppliedCurrentExt,"typeOfAppliedCurrentExt","","Type of applied current.");
  gpc.get(timePeriod, "timePeriod",700.,"Stimulation frequency.");
  gpc.get(timePeriodVentricle, "timePeriodVentricle",700.,"Ventricles stimulation frequency.");
  gpc.get(timePeriodAtria, "timePeriodAtria",700.,"Atria stimulation frequency.");
  gpc.get(beatNumber, "beatNumber", 1,"In case of typeOfAppliedCurrent=true, number of beats.");
  gpc.get(beatDecreasing, "beatDecreasing", 0.,"In case of acceleratedBeat=true, time decrease between beats.");
  gpc.get(delayStim, "delayStim","0.","Stimulation delay.");
  gpc.get(delayStimVentricles, "delayStimVentricles","0.","Ventricles stimulation delay, useful for ventricles-atria coupled heart.");
  gpc.get(stimTime, "stimTime",10.,"Time of stimulation.");
  gpc.get(stimTimeLV, "stimTimeLV",10.,"Time of stimulation in left ventricle.");
  gpc.get(stimTimeRV, "stimTimeRV",6.,"Time of stimulation in right ventricle.");
  gpc.get(Iapp_x_coord, "Iapp_x_coord",0.,"x coordinate center of applied current.");
  gpc.get(Iapp_y_coord, "Iapp_y_coord",0.,"y coordinate center of applied current.");
  gpc.get(Iapp_z_coord, "Iapp_z_coord",0.,"z coordinate center of applied current.");
  //elec - infarct
  gpc.get(hasInfarct,"hasInfarct",false,"Bool variable to std::set infarction simulation.");
  gpc.get(x_infarct,"x_infarct",0.,"In case of hasInfarct=true, x variable value of infarct center.");
  gpc.get(y_infarct,"y_infarct",0.,"In case of hasInfarct=true, y variable value of infarct center.");
  gpc.get(z_infarct,"z_infarct",0.,"In case of hasInfarct=true, z variable value of infarct center.");
  gpc.get(radius_infarct,"radius_infarct",0.,"In case of hasInfarct=true, infarct radius.");
  //elec - BBB
  gpc.get(hasVentriclesBundleBrunchBlock,"hasVentriclesBundleBrunchBlock",false,"Ventricles Bundle Brunch Block Left or Right.");
  gpc.get(sideOfBBB,"sideOfBBB","left","In case of typeOfAppliedCurrent=BBB, side of BBB : 'left' or 'right'");
  gpc.get(BBBwithDelay,"BBBwithDelay",false,"In case of typeOfAppliedCurrent=BBB, bool variable to simulate BB with a time delay in stimulation.");
  gpc.get(BBBwithAngleBlock,"BBBwithAngleBlock",false,"In case of typeOfAppliedCurrent=BBB, bool variable to simulate BB with a block of the angle of stimulation.");
  gpc.get(BBBDelayStim,"BBBDelayStim",0.,"In case of typeOfAppliedCurrent=BBB and BBwithDelay=true, time delay in applied current for left or right BBB");
  gpc.get(angleIappBBB, "angleIappBBB",360.,"In case of typeOfAppliedCurrent=BBB and BBwithAngleBlock=true, maximum angle of applied stimulus in left or right ventricle.");
  //elec - Bachmann Bundle Block
  gpc.get(hasPartialBachmannBundleBlock,"hasPartialBachmannBundleBlock",false,"Bool variable to have a bachmann bundle block.");
  gpc.get(valueBBBlock, "valueBBBlock",1.0,"The value of Bachmann Bundle Block (without BBBlock the value equals to 6.0).");
  //elec - Wenckebach Atrioventricular Block
  gpc.get(hasWenckebachBlock,"hasWenckebachBlock",false,"Bool variable to have a Wenckebach Atrioventricular Block.");
  //elec - Kent Bundle
  gpc.get(hasKentBundle,"hasKentBundle",false,"Bool variable to have a Kent bundle.");
  //elec - Torsades de pointe
  gpc.get(torsade,"torsade",false,"Bool variable to simulate a Torades de pointe.");
  gpc.get(torsadeTimeBegin,"torsadeTimeBegin",0.,"Initial time of long-QT segment phase - due for instance to some medicine.");
  gpc.get(torsadeTimeEnd,"torsadeTimeEnd",0.,"End time of long-QT segment phase - due for instance to some medicine.");
  gpc.get(tauCloseTorsade,"tauCloseTorsade",100.,"Tau_close during long-QT segment phase - due for instance to some medicine.");
  gpc.get(tauCloseEndoTorsade,"tauCloseEndoTorsade",130.,"Tau_close endocardium during long-QT segment phase - due for instance to some medicine.");
  gpc.get(tauCloseCellTorsade,"tauCloseCellTorsade",140.,"Tau_close m-cells during long-QT segment phase - due for instance to some medicine.");
  gpc.get(tauCloseEpiTorsade,"tauCloseEpiTorsade",90.,"Tau_close epicardium during long-QT segment phase - due for instance to some medicine.");
  gpc.get(tauCloseRVTorsade,"tauCloseRVTorsade",120.,"Tau_close right venticle during long-QT segment phase - due for instance to some medicine.");
  //elec - ECG
  gpc.get(writeECG,"writeECG",false,"Bool variable to call createECG function for Bidomain problem (heart).");
  gpc.get(writeElectrodesMeas,"writeElectrodesMeas",false,"Bool variable to evaluate electrodes meausures using a heart-torso transfer matrix (bidomain-heart problem).");
  gpc.get(writeElectrodesMean,"writeElectrodesMean",false,"Bool variable to evaluate electrodes mean meausures.");
  gpc.get(writeMatrixECG,"writeMatrixECG",false,"Bool variable to create heart-torso tranfer matrix (bifomain-thorax problem).");
  gpc.get(ECGmatchFile,"ECGmatchFile","heart.match","Match file name for ECG writer.");
  gpc.get(ECGThoraxmatchFile,"ECGThoraxmatchFile","thorax.match","Match file name for ECG transfer matrix.");
  gpc.get(ECGmatrixFile,"ECGmatrixFile","matrix_transpose","Transfer matrix file name for ECG writer.");
  gpc.get(ECGelectrodeFile,"ECGelectrodeFile","electrode","Electrodes id file name.");
  gpc.get(ECGfileName,"ECGfileName","ecg","Output ECG writer file name.");
  gpc.get(ECGtabHeader,"ECGtabHeader",true,"Bool variable to std::set or not ecg.tab file header.");
  gpc.get(ECGcreateFigure,"ECGcreateFigure",false,"Bool variable to std::set default creation of .eps file.");

  //elec - dataAssimilation
  gpc.get(dataAssimilation,"dataAssimilation",false,"Bool variable to have the data assimilation.");
  gpc.get(minLSPotTM,"minLSPotTM",-1.0,"The minimal value of the level std::set of the transmembrane potential.");
  gpc.get(maxLSPotTM,"maxLSPotTM",1.0,"The maximal value of the level std::set of the transmembrane potential.");
  gpc.get(insidePar,"insidePar",100.0,"The data parameter before the inside data.");
  gpc.get(outsidePar,"outsidePar",100.0,"The data parameter before the outside data.");

  //elec - state filter
  gpc.moveToSection("elecEstim");
  gpc.get(stateFilter,"stateFilter",false,"Bool variable to std::set or Luenberger state filter");
  gpc.get(schroFilter,"schroFilter",false,"Bool variable to std::set or Luenberger state filter based on Schrodinger modes");
  gpc.get(electrodeControl,"electrodeControl",false,"Bool variable to std::set or Luenberger state filter -- control with electrodes measures.");
  gpc.get(aFilter,"aFilter",0.0,"Coeff a of -aX^t for ALP stabilization.");
  gpc.get(gain,"gain",0.0,"Add a diagonal matrix of value gain to the stabilization term");
  gpc.get(observDir,"observDir","./SolutionDir","Tell where observations reside");
  gpc.get(observFileName,"observFile","bidomain","Prefix of the observation file. Warning: up to now only ensight files");


  // Initial condition
  gpc.moveToSection("initialCondition");
  gpc.get(hasInitialCondition,"hasInitialCondition",false,"Bool variable to std::set or not initial conditions.");
  gpc.get(restartSolution,"restartSolution",false,"Bool variable to std::set or not restart solution.");
  gpc.get(restartSolutionDirRaw,"restartSolutionDir","./","Directory of initial conditions."
          "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
  restartSolutionDir = restartSolutionDirRaw;
  if (restartSolutionDir.at(restartSolutionDir.length()-1) != '/') restartSolutionDir += '/';
  restartSolutionDir = expandEnvironmentVariables(filename,"data","restartSolutionDir",restartSolutionDir);
  if (restartSolution) {
    FEL_WARNING_IF_NO_DIRECTORY(restartSolutionDir);
  }
  gpc.get(restartSolutionIter,"restartSolutionIter",0,"Iteration of backup solution used by the restart.");
  gpc.get(nameVariableInitCond,"nameVariableInitCond","default","Variable name the initial condition apply to");
  gpc.get(componentInitCond,"componentInitCond","default","Comp1, Comp2 or Comp3");
  gpc.get(valueInitCond,"valueInitCond","0.","Initial value for this variable.");
  gpc.get(frequencyBackup, "frequencyBackup", 0,"Frequency of backup copies, if == 0 not doing the backup.");

  // Move mesh
  gpc.moveToSection("moveMesh");
  gpc.get(hasMoveMesh,"hasMoveMesh",false,"Bool variable to have a dynamic mesh.");
  gpc.get(hasMoveFibers,"hasMoveFibers",false,"Bool variable to have a dynamic fibers in eletrophysiology.");
  gpc.get(MoveMeshMatchFile,"MoveMeshMatchFile","displacement.match","Match file name for move mesh.");
  gpc.get(scaleCoeff,"scaleCoeff",1.,"scaling coefficient");

  // Boundary condition
  gpc.moveToSection("boundaryCondition");
  gpc.get(essentialBoundaryConditionsMethod,"essentialBoundaryConditionsMethod", 0, "0: non-symmetric pseudo-elimination, 1: symmetric pseudo-elimination, 2:penalization");
  gpc.get(type,"type","","Dirichlet or Neumann");
  gpc.get(typeValue,"typeValue","","Constant, Vector, FunctionT (depend on time), FunctionS (depend on space), FunctionTS (depend on time and space) or EnsightFile (read Dof values from an Ensight file).");
  gpc.get(numLabel,"numLabel","","Removed in new version?");
  gpc.get(label,"label","","Apply boundary condition on elements with this label");
  gpc.get(labelName,"labelName","","used only in the std::cout of this file, could improve readability.");
  gpc.get(variable,"variable","","Variable name the boundary condition apply to");
  gpc.get(component,"component","","Comp1, Comp2 or Comp3");
  gpc.get(value,"value","","Variable value at this boundary.");
  gpc.get(userParameter, "userParameter", "-1.","?");
  gpc.get(alphaRobin,"alphaRobin","","Coeff before variable of Robin BC: betaRobin * grad u dot n + alphaRobin * u = g");
  gpc.get(betaRobin,"betaRobin","","Coeff before normal derivative of the variable of Robin BC: betaRobin * grad u dot n + alphaRobin * u = g");
  gpc.get(alphaRobinNormal,"alphaRobinNormal","0.","Coeff before variable of Robin Normal BC");
  gpc.get(penValueForEmbedFSI,"penValueForEmbedFSI",1e20,"Penalization coefficient in EmbedFSI boundary condition.");
  gpc.get(useEssDerivedBoundaryCondition,"useEssDerivedBoundaryCondition",false,"true if you want to use essential BC DERIVED, even without having any dirichlet BC"); // added for embedFSI, to impose 0 displacement at the surface boundaries
  gpc.get(modeLung,"modeLung","","type of BC");
  unsigned int countRobinBC(0);
  for ( std::size_t itBC(0); itBC<type.size(); ++itBC) {
    if ( type[itBC] == "Robin" )
      ++countRobinBC;
  }
  FEL_ASSERT ( countRobinBC == alphaRobin.size() );
  if ( betaRobin.size() != alphaRobin.size() && ( betaRobin.size() >1 ) ) {
    FEL_ERROR("Something wrong with BC");
  } else if ( betaRobin.size() == 0 ) {
    betaRobin.resize(alphaRobin.size(),1.);
  }  else  if (betaRobin.size() == 1){
    double aus(betaRobin[0]);
    betaRobin.resize(alphaRobin.size(),aus);
  }

  gpc.get(bc_potExtraCell,"bc_potExtraCell","Rien","Boundary Condition for the Extracellular Potential");

  gpc.get(bcCondDirRaw,"bcCondDir","./","directory of input boundary values files (ensight files)."
          "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
  bcCondDir = bcCondDirRaw;
  if (bcCondDir.at(bcCondDir.length()-1) != '/') bcCondDir += '/';
  bcCondDir = expandEnvironmentVariables(filename,"boundaryCondition","bcCondDir",bcCondDir);
  FEL_WARNING_IF_NO_DIRECTORY(bcCondDir);

  //bc - check size
  FEL_ASSERT_EQUAL(numLabel.size(),type.size());
  FEL_ASSERT_EQUAL(numLabel.size(),typeValue.size());
  FEL_ASSERT_EQUAL(numLabel.size(),variable.size());
  FEL_ASSERT_EQUAL(numLabel.size(),component.size());

  //bc - lumpedModel
  gpc.get(lumpedModelBCType,"lumpedModelBCType","","1: RCR,R ; 2: RC.");
  gpc.get(lumpedModelBCAlgo,"lumpedModelBCAlgo","","1: explicit, 2: implicit");
  gpc.get(lumpedModelBCLabel,"lumpedModelBCLabel","","?");
  gpc.get(lumpedModelBCName,"lumpedModelBCName","","name of the region where the lumped model applies");

  while( lumpedModelBCAlgo.size() < lumpedModelBCLabel.size() )
    lumpedModelBCAlgo.push_back( lumpedModelBCAlgo[0] );
  while( lumpedModelBCType.size() < lumpedModelBCLabel.size() )
    lumpedModelBCType.push_back( lumpedModelBCType[0] );
  while( lumpedModelBCName.size() < lumpedModelBCLabel.size() )
    lumpedModelBCName.push_back( "Lumped" + std::to_string(lumpedModelBCName.size()) );

  gpc.get(lumpedModelBC_Rprox,"lumpedModelBC_Rprox","","?");
  gpc.get(lumpedModelBC_Rdist,"lumpedModelBC_Rdist","","?");
  gpc.get(lumpedModelBC_C,"lumpedModelBC_C","","?");
  gpc.get(lumpedModelBC_Pvenous,"lumpedModelBC_Pvenous","","?");
  gpc.get(lumpedModelBC_Pdist_init,"lumpedModelBC_Pdist_init","","?");

  // bc - linear/non linear compliance (C=C(V))
  gpc.get(lumpedModelBC_C_type,"lumpedModelBC_C_type",1,"1: linear, 2: non-linear C=C(V)");
  gpc.get(powerNonLinearCompliance,"powerNonLinearCompliance",1,"powerNonLinearCompliance");
  gpc.get(lumpedModelBC_volumeMax,"lumpedModelBC_volumeMax",0.,"Vmax");
  gpc.get(lumpedModelBC_volumeMin,"lumpedModelBC_volumeMin",0.,"Vmin");
  gpc.get(lumpedModelBC_volume0,"lumpedModelBC_volume0",0.,"V0");
  gpc.get(lumpedModelBC_referenceVolume0,"lumpedModelBC_referenceVolume0",2500.,"V0ref");

  //bc - fluid bc (user parameters)
  gpc.get(inflowPressureMaxQuiteInspi,"inflowPressureMaxQuiteInspi",-100.,"inflowPressureMaxQuiteInspi");
  gpc.get(inflowPressureMaxSpiroInspi,"inflowPressureMaxSpiroInspi",-20.,"inflowPressureMaxSpiroInspi");
  gpc.get(inflowPressureMaxSpiroExpi,"inflowPressureMaxSpiroExpi",30.,"inflowPressureMaxSpiroExpi");
  gpc.get(inflowLabel,"inflowLabel",-1,"Label of the inflow");

  // bc - to impose the tangential component of the velocity std::vector equal to 0
  gpc.get(compTangBClabel,"compTangBClabel","","?");

  //bc - label for cardiac cycle
  gpc.get(CardiacCycleLabel,"CardiacCycleLabel",-1,"label in wich impose the cardiac cycle input function");
  if(CardiacCycleLabel !=- 1) {
    addedBoundaryFlag = 3;
    CardiacCycle = true;
  } else {
    CardiacCycle = false;
  }

  gpc.get(duration_ejection,"duration_ejection",0.,"duration of the ejection phase");
  gpc.get(duration_isovol_relax,"duration_isovol_relax",0.,"duration of the isovolumetric relaxation phase");
  gpc.get(duration_filling,"duration_filling",0.,"duration of the filling phase");
  gpc.get(duration_isovol_contract,"duration_isovol_contract",0.,"duration of the isovolumetric contraction phase");

  gpc.get(cardiacOutput,"cardiacOutput",5.,"in liter per minute");
  gpc.get(heartRate,"heartRate",60,"number of heart beats per minute");

  //BCdata x SimplifiedFSI model and  boundary condition.
  gpc.moveToSection("SimplifiedFSI");

  gpc.get(switchSimplFSIOff,"switchSimplFSIOff",false,"switch everything off");
  gpc.get(tssParam,"tssParam",0.,"stabilization parameter for tss (transpiration surface stabilization");
  gpc.get(tssConsistent,"tssConsistent",true, "wether to use or not the consistent version of the tss");

  //common properties
  gpc.get(nonLinearKoiterModel,"nonLinearKoiterModel",false,"false: linear Koiter model, true: non linear Koiter model");
  gpc.get(zeroDisplacementAtBoarders, "zeroDisplacementAtBoarders",false,"true if you want to impose zero dirichlet on the displacement"); //TODO boarder -> boundary.
  gpc.get(densityStructure,"densityStructure",1.,"density of the whole structure, both shell and fibers");
  gpc.get(viscosityStructure,"viscosityStructure",0.,"viscosity parameter for the structure in front of the term  dot eta");

  //properties of the shell
  gpc.get(youngShell,"youngShell",0.0,"Young modulus of the shell layer");
  gpc.get(poissonShell,"poissonShell",0.5,"Poisson coefficient of the shell layer");
  gpc.get(widthShell,"widthShell",0.0,"Width of the shell layer");

  //property of the fiber layer
  gpc.get(widthFiber,"widthFiber",0.0,"Width of the fiber layer");
  gpc.get(preStressFibers,"preStressFibers",0.0,"Pre-stress the fibers");
  gpc.get(youngFibers,"youngFibers",0.,"Young modulus of the fibers");
  gpc.get(fiberDensity,"fiberDensity","","Density of fibers by label (coefficient multiplying (preStressFibers+extraTension) and youngFibers)");
  gpc.get(fiberDispersionParameter,"fiberDispersionParameter",0.2,"double: 0-> fibers all in the principal direction of curvature, 1-> fibers equally distributed over all the directions");
  gpc.get(outsidePressure,"outsidePressure",0.,"pressure outside the vessel wall, it is associated with no displacement");

  //property in case of a 2D domain
  gpc.get(radius,"radius",0.0,"Radius of a cylinder, used a scale parameter in the structural model for the 2D test");

  //otherdata TODO move them to another section
  gpc.get(idCase,"idCase", 3, "id of the case for user");
  gpc.get(nameTest,"nameTest","noName", "name of the test");
  gpc.get(incomingPressure, "incomingPressure",28,"mean pressure at the inlet");
  gpc.get(propCardiacCycle, "propCardiacCycle",0.1,"for inflow data");

  //Section for linearProblemNSHeat
  gpc.moveToSection("NSHeat");
  gpc.get(coeffOfThermalExpansion,"coeffOfThermalExpansion",3.e-4,"coefficient of thermal expansion");
  gpc.get(thermalDiffusion,"thermalDiffusion",1.,"coefficient of the thermal diffusion");
  gpc.get(initialTemperature,"initialTemperature",303.15,"Constant initial temperature of the system, in Kelvin");
  gpc.get(referenceTemperature,"referenceTemperature",303.15,"reference temperature, in Kelvin");
  gpc.get(gravity,"gravity","0 -10","Gravity");
  gpc.get(stevinoLabels, "stevinoLabels", "", "Labels of the bounadry where we impose also the hydrostatic pressure (rho g z)");
  gpc.get(flowRate,"flowRate",0,"flowRate used to compute a velocity bc in userNSHeat");

  //Section with retinal autoregulation parameters
  gpc.moveToSection("autoregulation");
  gpc.get(autoregulated,"autoregulated",false,"used in autoregulation.cpp");
  gpc.get(maxT,"maxT", 1.e3,"maximum fibers active pressure in the control function");
  gpc.get(q,"q",0.95,"shape parameter of the control function ");
  gpc.get(up,"up",1000,"upper limit of the range for the control");
  gpc.get(down,"down",0,"lower limit of the range for the control");
  gpc.get(nominalPressure, "nominalPressure",35.," reference value of pressure associated with no control");
  gpc.get(useRegulationInWindkessel, "useRegulationInWindkessel", false, "if true winkdessel changes during the simulation to regulate blood flow");
  gpc.get(lumpedModelBC_regParam,"lumpedModelBC_RMax","","Maximum increment of the pressure due to regulation");
  if ( lumpedModelBC_regParam.size() != lumpedModelBCType.size() && useRegulationInWindkessel ) {
    std::size_t oldSize( lumpedModelBC_regParam.size() );
    double regValue( lumpedModelBC_regParam.back() );

    std::cout<<" autoregulation parameter std::set for only : "<< oldSize <<" outlets"<<std::endl;
    std::cout<<" extending the last value: "<< regValue <<" to the others"<<std::endl;
    for ( std::size_t k( oldSize ); k < lumpedModelBCType.size(); ++k ) {
      lumpedModelBC_regParam.push_back(regValue);
    }
    FEL_ASSERT( lumpedModelBC_regParam.size() == lumpedModelBCType.size());
  }
  gpc.get(windkesselProp, "windkesselProp",0.46,"amount of resistance with no autoregulation");
  gpc.get(idControlFreeCase,"idControlFreeCase",0,"id of the control free case for user");
  gpc.get(exportPrincipalDirectionsAndCurvature,"exportPrincipalDirectionsAndCurvature",false,"true if you want to take a look at the principal direction of curvature"); //TODO move it elsewhere
  gpc.get(exportDofPartition,"exportDofPartition",false,"true if you want to take a look at the partition");

  gpc.moveToSection("IOPcoupling");
  gpc.get(labelIOPMesh,"labelIOPMesh","","labels on the IOP mesh that are part of the interface");
  gpc.get(labelNSMesh,"labelNSMesh","","labels on the IOP mesh that are part of the interface");
  gpc.get(porousParam,"porousParam",-1.,"if negative we are considering the fluid as potential if not as a porous media and this coefficient is related to the permeability");
  gpc.get(computeSteklov,"computeSteklov",false,"true if you want to compute the steklov-poincare operator");
  gpc.get(relaxationParam,"relaxationParam",0,"relaxation parameter when solving a fully neumann problem");
  gpc.get(exportSteklovData,"exportSteklovData",false,"exporting inputs and outputs of the steklov operator");//TODO: remove it!
  gpc.get(optionForSteklov,"optionForSteklov",1,"0=load it, 1=compute it, 2=compute and save it");
  gpc.get(steklovDataDirRaw,"steklovDataDir","./","Directory where to read/save steklov matrix. "
          "Accept envrionment variables: HOME, FELISCE_DATA_DIR, FELISCE_INRIA_DATA_DIR, FELISCE_RESULT_DIR");
  steklovDataDir = steklovDataDirRaw;
  if (steklovDataDir.at(steklovDataDir.length()-1) != '/') steklovDataDir += '/';
  steklovDataDir = expandEnvironmentVariables(filename,"IOPcoupling","steklovDataDir",steklovDataDir);
  if ( optionForSteklov == 0 )
    FEL_WARNING_IF_NO_DIRECTORY(steklovDataDir);
  gpc.get(steklovMatrixName,"steklovMatrixName","","name of the file containing steklovMatrix");
  gpc.get(lumpedTest,"lumpedTest",false,"if you std::set it true then the test quantities will be computed using only the diagonal of the mass matrix (for the L2 scalar product) ");//remove this option..
  gpc.get(notConnectedCylinders,"notConnectedCylinders",false,"true if you are computing the eigenfunctions on a non connected domain");

  gpc.moveToSection("ROSteklov");
  gpc.get(useRoSteklov,"useRoSteklov",false,"whether to assemble and use the reduced order steklov operator instead of the real one.");
  gpc.get(nbOfLaplacianEigenFunction,"nbOfLaplacianEigenFunction",10,"used in linearProblemPerfectFluid, it is the number of the surface laplacian eigenpairs that will be approximated");
  gpc.get(percentageOfExtraEig,"percentageOfExtraEig",10.0,"If it is std::set, for instance, to 25 the required number of laplacianEigenFunction requested to SLEPc will be ceil(1.25*nbOfLaplacian.. ");
  gpc.get(lowRank,"lowRank",5,"used in linearProblemPerfectFluid, rank of the reduced order steklov operator.");
  FEL_ASSERT( lowRank <= nbOfLaplacianEigenFunction);
  gpc.get(tryAcceleration,"tryAcceleration",false,"used in perfectfluid");
  gpc.get(projOnlineRatio,"projOnlineRatio",0.995,"used in perfectfluid");
  gpc.get(onlyConstResp,"onlyConstResp",false," true if you do want to use only the constant response and not the off-line basis part");
  // export configuration for post-processing.
  gpc.moveToSection("export");
  gpc.get(exportP1Normal, "exportP1Normal", false, "true if you want to export the P1 field");
  gpc.get(labelExportP1Normal, "labelExportP1Normal","","Labels of the boundary for you want to export the normal.");
  gpc.get(exportAll, "exportAll", false, "true if you want to overwrite the behavior of all the other flag of this section"); //TODO change the name in ExportAll Steklov
  gpc.get(exportLaplacianEigenValues,    "exportLaplacianEigenValues", false, "true if you want to export the approximated eigenvalues  of the laplacian operator");
  gpc.get(exportLaplacianEigenVectors,   "exportLaplacianEigenVectors", false, "true if you want to export the approximated eigenvectors of the laplacian operator");
  gpc.get(exportSteklovEigenValues,      "exportSteklovEigenValues", false, "true if you want to export the approximated eigenvalues  of the steklov operator");
  gpc.get(exportSteklovEigenVectors,     "exportSteklovEigenVectors", false, "true if you want to export the approximated eigenvectors of the steklov operator");
  gpc.get(exportFTOfSteklovEigenVectors, "exportFTOfSteklovEigenVectors", false, "true if you want to export the Fourier Transform of the Steklov EigenVectors w.r.t the Laplacian eigenVector");
  gpc.get(exportInputOfSteklov,      "exportInputOfSteklov", false, "true if you want to export the input vectors to the steklov operator");
  gpc.get(exportOutputOfSteklov,     "exportOutputOfSteklov", false, "true if you want to export the output vectors to the steklov operator" );
  gpc.get(exportOnlyLastInput,           "exportOnlyLastInput", true,  "false if you want to export all the input through the domain decomposition iterations");
  gpc.get(exportOnlyLastOutput,          "exportOnlyLastOutput", true,  "false if you want to export all the input through the domain decomposition iterations");
  gpc.get(exportMassMatrix,    "exportMassMatrix", false, "true if you want to export the mass matrix");
  gpc.get(exportLaplacianMatrix, "exportLaplacianMatrix", false, "true if you want to export the Laplacian matrix");
  gpc.get(exportSteklovMatrix,    "exportSteklovMatrix", false, "true if you want to export the Steklov matrix");
  gpc.get(exportReducedEigenMatrix, "exportReducedEigenMatrix", false,"true if you want to export the matrix of the steklov eigen problem written w.r.t the the laplacian eigenvectors");
  gpc.get(exportOffLineVolumeSolution, "exportOffLineVolumeSolution", false, "true to export the volume solution computed during the offline phase of the reduced steklov");
  gpc.get(exportFiltrationVelocity, "exportFiltrationVelocity", false, "true if you want to compute and export the filtration velocity (in poro-elasticity)");


  if ( exportAll || exportSteklovData ) { //exportSteklovData should be removed
    exportLaplacianEigenValues = true;
    exportLaplacianEigenVectors = true;
    exportSteklovEigenValues = true;
    exportSteklovEigenVectors = true;
    exportFTOfSteklovEigenVectors = true;
    exportInputOfSteklov = true;
    exportOutputOfSteklov = true;
    exportOnlyLastInput = false;
    exportOnlyLastOutput = false;
    exportMassMatrix = true;
    exportLaplacianMatrix = true;
    exportSteklovMatrix = true;
    exportReducedEigenMatrix = true;
    exportFiltrationVelocity = true;
  }

  //dofs fusion (ex. periodic bundary conditions)
  gpc.moveToSection("FusionDof");
  gpc.get(FusionDof,"FusionDof",false,"fuion or not fusion dofs");
  gpc.get(FusionVariable,"variable","default","Variable name to be fusioned");
  gpc.get(FusionNumLabel,"numLabel","0","number of pairs of label to fusion");
  gpc.get(FusionLabel,"label","-1","Fusion dofs of first label in the second, dofLabel1 == dofLabel2");
  gpc.get(FusionTolerance,"tolerance",1.e-12,"Mininum distance between supportDOF nodes to enable fusion");
  gpc.get(AllToOne,"allToOne","false","All dofs fusioned into a single one");
  gpc.get(AllInLabelToOne,"allInLabelToOne",false,"All dofs of a label fusioned into a single one");

  //Embedded interface matching
  gpc.moveToSection("EmbeddedInterface");
  gpc.get(EmbeddedInterface,"EmbeddedInterface",false,"activation / desactivation EmbeddedInterface");
  gpc.get(EmbeddedVariable,"embeddedVariable","default","Variable name to be matched");
  gpc.get(EmbeddedNumInterface,"numEmbeddedInterface","-1","number of pairs of labels to match");
  gpc.get(EmbeddedLabelPairs,"embeddedLabelPairs","-1","Label pairs defining the embedded interface");
  gpc.get(EmbeddedMatchTolerance,"embeddedMatchTolerance",1.e-12,"Mininum distance between supportDOF nodes to match");

  // Crack point
  gpc.moveToSection("CrackInterface");
  gpc.get(hasCrack,"hasCrack",false,"crack or no");
  gpc.get(CrackVariable,"crackVariable","default","Variable name to be crack model");
  gpc.get(CrackNum,"numCracks","-1","number of pairs of labels to crack");
  gpc.get(CrackLabelPairs,"crackLabelPairs","-1","Label pairs defining the crack point");
  gpc.get(CrackElemID,"crackElemID","-1","True ID for geo file [not bdry label] corresponding to crackLabel");
  gpc.get(CrackConstantBefore,"crackConstantBefore",1e4,"Crack spring constant before");
  gpc.get(CrackConstantAfter,"crackConstantAfter",1e-4,"Crack spring constant after");
  gpc.get(CrackTolerance,"crackTolerance",1.e-12,"Mininum distance between supportDOF nodes");
  gpc.get(CrackStressTolerance,"crackStressTolerance",1.e12,"Stress tolerance on crack point before cracking");

  //std::set initial crack constant
  gpc.get(CrackConstant,"crackConstantBefore",1e16,"Crack spring constant");

  //Penalization Method
  gpc.moveToSection("PenalizationValves");
  gpc.get(PenalizationMethod, "PenalizationMethod", false, "activation / deactivation penalization method (non linear solver)");
  gpc.get(closed_penalization, "closed_penalization", "-1 -1", "close surface penalization");
  gpc.get(Gamma_penalization, "Gamma_penalization", 1.e-5, "value of the penalization");

  //RISmodel (ex.valves)
  gpc.moveToSection("RISModels");
  gpc.get(RISModels, "RISModels",false,"activation / deactivation RIS models");
  gpc.get(initiallyOpenedValveIndex, "initiallyOpenedValveIndex", 0, "index of which valve is initially open. By default, the first valve is open");
  gpc.get(crashIfStatusNotAdmissible, "crashIfStatusNotAdmissible", 1, "variable to specify if the simulation must stop when a status is not admissible");
  gpc.get(closed_surf, "closed_surf", "-1", "closed surfaces");
  gpc.get(open_surf, "open_surf", "-1", "open surfaces");
  gpc.get(fake_surf, "fake_surf", "-1", "fake surfaces");
  gpc.get(nbTimeSteps_refractory_time, "nbTimeSteps_refractory_time", 5, "number of time steps used for defining the refractory time");
  gpc.get(nbTimeSteps_linear_transition, "nbTimeSteps_linear_transition", 0, "number of time steps used for defining the linear evolution in the value of the resistances");
  gpc.get(nbTimeSteps_linear_transition_twoValves, "nbTimeSteps_linear_transition_twoValves", "0. 0.", "number of time steps used for defining the linear evolution in the value of the resistances of the proximal and distal valves");
  gpc.get(R_active_surf, "R_active_surf", 1.e5, "resistance of closed valve");
  gpc.get(R_inactive_surf, "R_inactive_surf", 0., "resistance of open valve");
  gpc.get(R_activeClosed_TwoValves_Intermediate, "R_activeClosed_TwoValves_Intermediate", 1.e4, "resistance of closed valve just after the closing - only used for two valves");
  gpc.get(R_activeClosed_TwoValves_Final, "R_activeClosed_TwoValves_Final", 1.e6, "final resistance of closed valve after several iterations after the closing - only used for two valves");
  gpc.get(R_FirstValve_ClosingSpeed, "R_FirstValve_ClosingSpeed", 40., "rate of speed for the evolution of the resistance of the closed surface of the first valve from R_Intermediate to R_Final - only used for two valves");
  gpc.get(R_SecondValve_ClosingSpeed, "R_SecondValve_ClosingSpeed", 30., "rate of speed for the evolution of the resistance of the closed surface of the second valve from R_Intermediate to R_Final - only used for two valves");
  gpc.get(flow_ref_oneValve, "flow_ref_oneValve", 1.e-2, "flow reference for one valve");
  gpc.get(flow_ref_twoValves, "flow_ref_twoValves", "-2. 0.", "flow reference for two valves");
  gpc.get(preventValvesFromOpeningTooFast, "preventValvesFromOpeningTooFast", 0, "specify if a closed valve should be prevented from opening if another valve has recently closed");
  if ( (preventValvesFromOpeningTooFast != 0) && (preventValvesFromOpeningTooFast != 1) ) {
    FEL_ERROR("ERROR OF DATA: error of value for preventValvesFromOpeningTooFast: unknown value!");
  }
  gpc.get(nbIterationsPreventValvesFromOpeningTooFast, "nbIterationsPreventValvesFromOpeningTooFast", 10, "number of iterations during a closed valve cannot open whereas another valve has recently closed");
  gpc.get(verboseRIS, "verboseRIS", 1, "verbose for the RIS model");

  //Pressure correction for full closed cavity where only displacements are imposed on its boundaries
  gpc.moveToSection("PressureCorrection");
  gpc.get(useElectroMechanicalPressure, "useElectroMechanicalPressure", false, "introducing electromechanical pressure");
  gpc.get(usePressureCorrection, "usePressureCorrection", false, "introducing pressure correction");
  gpc.get(correctionExternalPressure, "correctionExternalPressure", false, "introducing external pressure correction");
  gpc.get(correctionALEPressure, "correctionALEPressure", false, "introducing ALE pressure correction");
  gpc.get(typeOfPressureCorrection, "typeOfPressureCorrection", 0, "type of pressure correction. 0 for enabling the pressure correction anytime, 1 only when all the valves are closed");
  gpc.get(labelsToCorrectPressure, "labelsToCorrectPressure", "", "labels where the pressure correction is applied. CONVENTION: Always mitral valve first then aortic valve!!");
  gpc.get(signPressureCorrection, "signPressureCorrection", "", "sign of the pressure correction. either equal to -1 or 1");
  gpc.get(labelsToComputeMeanPressure, "labelsToComputeMeanPressure", "", "labels where the mean pressure is computed and used for the pressure correction");
  if ( ((signPressureCorrection.size()) != (labelsToCorrectPressure.size())) || ((signPressureCorrection.size()) != (labelsToComputeMeanPressure.size())) || ((labelsToCorrectPressure.size()) != (labelsToComputeMeanPressure.size())) ) {
    FEL_ERROR("ERROR OF DATA: error of size for labelsToCorrectPressure, signPressureCorrection and labelsToComputeMeanPressure: it should be the same!");
  }
  gpc.get(labelsToComputeFluxThrough, "labelsToComputeFluxThrough", "", "std::vector of labels where the flux through is computed and used for the ALE pressure correction. CONVENTION: Always mitral valve first then aortic valve and at the end ventricle wall!");
  gpc.get(exteriorNormalOfLabelsToComputeFluxThrough, "exteriorNormalOfLabelsToComputeFluxThrough", "", "// std::vector of orientation of the exterior normals of the labels where the flux through is computed and used for the ALE pressure correction. either equal to -1 or 1");
  if ( (labelsToComputeFluxThrough.size()) != (exteriorNormalOfLabelsToComputeFluxThrough.size())) {
    FEL_ERROR("ERROR OF DATA: error of size for labelsToComputeFluxThrough and exteriorNormalOfLabelsToComputeFluxThrough: it should be the same!");
  }
  gpc.get(forbiddenLabelsForCorrection, "forbiddenLabelsForCorrection", "", "std::vector of labels where the resistive term called in computeElementArrayBoundaryCondition will not be applied in order to avoid duplications");
  gpc.get(verbosePressureCorrection, "verbosePressureCorrection", 1, "verbose for the pressure correction");

  // Stiffening
  gpc.moveToSection("stiffening");
  gpc.get(stiffening, "stiffening", false, "Introducing stiffening");
  gpc.get(stiffeningLabels, "stiffeningLabels", "", "Surface labels requiring stiffening");
  gpc.get(skipStiffeningFirstIteration, "skipStiffeningFirstIteration", 0, "Boolean for the specific stiffening during the first iteration");
  gpc.get(initStiff, "initStiff", 200., "Initial stiffening coefficient");
  gpc.get(finalStiff, "finalStiff", 200., "Final stiffening coefficient");

  // Regurgitation
  gpc.moveToSection("Regurgitation");
  gpc.get(regurgitation,"regurgitation",false, "Introducing regurgitation");
  gpc.get(regurgitationType, "regurgitationType", 0, "Regurgitation type (0: Ball, 1: regurgitation curve, 2: Ball at first iteration only");
  gpc.get(regurgitationPosition,"regurgitationPosition", "0. 0. 0.", "regurgitation center");
  gpc.get(regurgitationLabels,"regurgitationLabels", "", "regurgitation labels");
  gpc.get(regurgitationRadius,"regurgitationRadius", 0.3,"regurgitation radius");

  // PETSc
  gpc.moveToSection("petsc");
  gpc.get(linearSolverVerbose,"linearSolverVerbose",0,"Verbosity of the linear systems petsc solvers");
  gpc.get(solver,"solver","gmres","gmres or ...");
  gpc.get(gmresRestart,"gmresRestart","200","");
  while(gmresRestart.size() < solver.size()){
    gmresRestart.push_back(200);
  }
  gpc.get(preconditioner,"preconditioner","ilu","ilu or ..");
  gpc.get(setPreconditionerOption,"setPreconditionerOption","SAME_NONZERO_PATTERN","SAME_PRECONDITIONER,SAME_NONZERO_PATTERN or DIFFERENT_NONZERO_PATTERN");
  while ( setPreconditionerOption.size() < solver.size()) {
    setPreconditionerOption.emplace_back("SAME_NONZERO_PATTERN");
  }
  gpc.get(relativeTolerance,"relativeTolerance","1e-9","Relative tolerance.");
  while(relativeTolerance.size() < solver.size()){
    relativeTolerance.push_back(1e-9);
  }
  gpc.get(absoluteTolerance,"absoluteTolerance","1e-50","Absolute tolerance.");
  while(absoluteTolerance.size() < solver.size()){
    absoluteTolerance.push_back(1e-50);
  }
  gpc.get(solutionTolerance,"solutionTolerance","-2","Convergence tolerance in terms of the norm of the change in the solution between steps, || delta x || < stol*|| x || ");
  while(solutionTolerance.size() < solver.size()){
    solutionTolerance.push_back(-2);
  }
  gpc.get(divergenceTolerance,"divergenceTolerance","-1.0","Divergence tolerance. If negative will not be changed");
  while(divergenceTolerance.size() < solver.size()){
    divergenceTolerance.push_back(-1.0);
  }
  gpc.get(maxIteration,"maxIteration", "1000","Maximum iteration (to solve linear system)");
  while(maxIteration.size() < solver.size()){
    maxIteration.push_back(1000);
  }
  gpc.get(maxIterationSNES,"maxIterationSNES", "50","Maximum iteration to SNES (to solve non linear system)");
  while(maxIterationSNES.size() < solver.size()){
    maxIterationSNES.push_back(50);
  }
  gpc.get(maxFunctionEvaluatedSNES,"maxFunctionEvaluatedSNES", "-1","Maximum number of function evaluations (-1 indicates no limit) ");
  while(maxFunctionEvaluatedSNES.size() < solver.size()){
    maxFunctionEvaluatedSNES.push_back(-1);
  }
  gpc.get(initSolverWithPreviousSolution,"initSolverWithPreviousSolution","","Init solver with previous solution");
  while ( initSolverWithPreviousSolution.size() < solver.size()) {
    initSolverWithPreviousSolution.push_back(false);
  }
  //Petsc check size
  FEL_ASSERT_EQUAL(gmresRestart.size(), solver.size());
  FEL_ASSERT_EQUAL(preconditioner.size(), solver.size());
  FEL_ASSERT_EQUAL(setPreconditionerOption.size(), solver.size());
  FEL_ASSERT_EQUAL(relativeTolerance.size(), solver.size());
  FEL_ASSERT_EQUAL(absoluteTolerance.size(), solver.size());
  FEL_ASSERT_EQUAL(solutionTolerance.size(), solver.size());
  FEL_ASSERT_EQUAL(divergenceTolerance.size(), solver.size());
  FEL_ASSERT_EQUAL(maxIteration.size(), solver.size());
  FEL_ASSERT_EQUAL(maxIterationSNES.size(), solver.size());
  FEL_ASSERT_EQUAL(maxFunctionEvaluatedSNES.size(), solver.size());
  FEL_ASSERT_EQUAL(initSolverWithPreviousSolution.size(), solver.size());

  //Verdandi
  gpc.moveToSection("verdandi");
  gpc.get(stateErrorVarianceValue,"stateErrorVarianceValue",1.,"?");
  gpc.get(Kparameter,"Kparameter",1.,"?");
  gpc.get(RHSparameter,"RHSparameter",1.,"?");

  //CVGraph
  gpc.moveToSection("cvgraph");
  gpc.get(compartmentName,"compartmentName","none","The name of the current compartment");
  gpc.get(dataFileCVG,"dataFileCVG","./dataCVG","The cvgraph data file.");
  gpc.get(interfaceLabels,"interfaceLabels", "","The labels of the interface");
  gpc.get(correspondingInterfaceLabels,"correspondingInterfaceLabels", "","Same labels as interfaceLabels but on the other mesh");
  // If correspondingInterfaceLabels is not specified, we assume a 1 to 1 correspondance.
  if ( correspondingInterfaceLabels.size() == 0 ) {
    correspondingInterfaceLabels = interfaceLabels;
  }
  gpc.get(withCVG,"withCVG",false," with cvgraph ");
  gpc.get(interpolatorThreshold,"interpolatorThreshold",1.e-8,"Threshold used for determining whether a point lies inside or outside a triangle");
  gpc.get(maxDepth,"maxDepth",1,"Max depth for used for recursive search in getClosestSurfaceElement (used in interpolator)");
  gpc.get(idVarToExchange,"idVarToExchange",0,"The ID of the variable to send/receive (used in cvgMainSlave::buildInterpolator).");

  // Hyperelastic
  gpc.moveToSection("Hyperelastic");
  gpc.get(hyperElasticLaw.type,"type","","StVenantKirchhoff or CiarletGeymonat or Ogden");
  gpc.get(hyperElasticLaw.numLabel,"numLabel","","Number of labels");
  gpc.get(hyperElasticLaw.label,"label","","Apply boundary condition on elements with this label");
  std::string time_scheme;
  gpc.get(time_scheme,"TimeScheme","half_sum","The time scheme considered for the hyperelastic law, half_sum by default");
  hyperElasticLaw.timeSchemeHyperelasticity = time_scheme == "half_sum" ? HyperelasticityNS::half_sum : HyperelasticityNS::midpoint;
  bool is_incompressible;
  gpc.get(is_incompressible,"IsIncompressible",false,"If the hyperlastic law considered is compressible or not");
  hyperElasticLaw.pressureData = is_incompressible ? felisce::make_shared<Private::Hyperelasticity::PressureDataIncompressible>() : felisce::make_shared<Private::Hyperelasticity::PressureData>();
  if (hyperElasticLaw.type.size() > 0) {
    hyperElasticLaw.hyperElasticLaws = felisce::make_shared<HyperElasticityLawManager>();
  } else {
    hyperElasticLaw.hyperElasticLaws = nullptr;
  }

  // Read elementFieldDynamicValue
  auto elementFieldSectionList = gpc.getElementFieldSection();
  std::string name;

  for (auto isection = elementFieldSectionList.begin() ;isection != elementFieldSectionList.end() ; ++isection ) {
    name = gpc.elementFieldNameFromSection(*isection);
    elementFieldDynamicValueMap[name] = gpc.getElementFieldDynamicValue(*isection);
  }

  this->userParseInputFile(gpc);

  // Check for undefined variable
  gpc.checkUndefinedVariable();

  // Help
  m_help = gpc.getHelp();
}

/***********************************************************************************/
/***********************************************************************************/

void UniqueFelisceParam::print(int aVerbose, std::ostream& outstr) const {
  if(aVerbose>2) {
    std::cout << "# -*- getpot -*-" << std::endl;
    std::cout << std::endl;

    std::cout << "[debug]" << std::endl;
    std::cout <<  "verbose = " << aVerbose << std::endl;
    std::cout << std::endl;

    std::cout << "[chrono]" << std::endl;
    std::cout <<  "toFile = " << chronoToFile << std::endl;
    std::cout << std::endl;

    std::cout << "[data]" << std::endl;
    std::cout <<  "inputDirectory = " << inputDirectory << std::endl;
    std::cout <<  "inputFile = " << inputFile << std::endl;
    if (useROM) std::cout << "podBasisFile = " << podBasisFile << std::endl;
    std::cout << std::endl;

    std::cout << "[transient]" << std::endl;
    std::cout <<  "timeStep = " << timeStep << std::endl ;
    std::cout <<  "time = " << time << std::endl ;
    std::cout <<  "timeMax = " << timeMax << std::endl ;
    std::cout <<  "timeStepCharact = " << timeStepCharact << std::endl ;
    std::cout <<  "iteration = " << iteration << std::endl ;
    std::cout <<  "timeIterationMax = " << timeIterationMax << std::endl ;
    std::cout <<  "frequencyWriteSolution = " << frequencyWriteSolution << std::endl;

    std::cout <<  "periodNbSteps = " << periodNbSteps << std::endl;
    std::cout << "intervalWriteSolution : [ " << std::endl;

    std::cout <<  "intervalWriteSolution : [ " << std::endl;

    for (std::size_t i = 0; i < intervalWriteSolution.size(); i++) {
      std::cout << intervalWriteSolution[i] << " " ;
    }
    std::cout << " ] " << std::endl;

    std::cout <<  "integrationTimeMethod = " << integrationTimeMethod << std::endl;
    std::cout << std::endl;

    std::cout << "[variable]" << std::endl;
    for (std::size_t i = 0; i < nameVariable.size(); i++) {
      std::cout << nameVariable[i] << " physcial variable: " ;
      std::cout << physicalVariable[i] ;
      std::cout << " with the finite element type " << typeOfFiniteElement[i] ;
      std::cout << " with the degree of exactness " << degreeOfExactness[i] ;
    }
    std::cout << std::endl;

    std::cout << "[mesh]" << std::endl;
    std::cout << "meshDirRaw = " << meshDirRaw << std::endl;
    std::cout << "meshDir = " << meshDir << std::endl;
    std::cout << "inputMeshes = " << std::endl;
    for(std::size_t ii=0; ii<inputMesh.size(); ++ii)
      std::cout << ii << " -> " << inputMesh[ii] << std::endl;
    std::cout << std::endl;
    std::cout << "outputMesh = " << std::endl;
    for(std::size_t ii=0; ii<outputMesh.size(); ++ii)
      std::cout << ii << " -> " << outputMesh[ii] << std::endl;
    std::cout << std::endl;
    std::cout << "idMesh = ";
    for(std::size_t ii=0; ii<idMesh.size(); ++ii)
      std::cout << idMesh[ii] << " ";
    std::cout << std::endl;
    std::cout << "resultDir = " << resultDir << std::endl;
    std::cout << "prefixNames = ";
    for(std::size_t i=0; i<prefixName.size(); ++i)
      std::cout << prefixName[i] << " ";
    std::cout << std::endl;
    std::cout << "CVGraphInterface = " << CVGraphInterface << std::endl;
    std::cout << std::endl;

    std::cout << "flipNormal = "<< flipNormal << std::endl;

    // Iterating over elements type (eg: Nodes, Segments2, Triangles3, ...)
    for ( auto it_map = felName2MapintRef2DescLine.begin(); it_map != felName2MapintRef2DescLine.end(); it_map++) {

      std::cout << it_map->first << std::endl ;

      //iterating over label and stringLabel
      std::cout << std::setiosflags(std::ios::left) << std::setw(10) << "label" << "stringLabel" << std::endl;
      for ( auto it_map2 = it_map->second.begin(); it_map2 != it_map->second.end(); it_map2++) {
        std::cout << std::setiosflags(std::ios::left) << std::setw(10) << it_map2->first << it_map2->second << std::endl;
      }

      std::cout << std::endl;

    }
    std::cout << std::endl;

    std::cout << "[physics]" << std::endl;
    std::cout << "unit = " << unit << " # physical units 1: SI, 2: CGS "<< std::endl;
    std::cout << "model = " << model << std::endl;

    if (useROM) {
      std::cout << "[rom]" << std::endl;
      std::cout << "ROMmethod = " << ROMmethod << std::endl;
      std::cout << "solveEigenProblem = " << solveEigenProblem << std::endl;
      std::cout << "optimizePotential = " << optimizePotential << std::endl;
      if (solveEigenProblem) std::cout << "chiSchrodinger = " << chiSchrodinger << std::endl;
      std::cout << "useROM = " << useROM << std::endl;
      std::cout << "dimRomBasis" << dimRomBasis << std::endl;
      std::cout << "dimCollocationPts" << dimCollocationPts << std::endl;
      std::cout << "numApproxMode" << numApproxMode << std::endl;
      std::cout << "nCutOff" << nCutOff << std::endl;
      std::cout << "orderALP" << orderALP << std::endl;
      std::cout << "readBasisFromFile" << readBasisFromFile << std::endl;
      std::cout << "writeBasisEvolution" << writeBasisEvolution << std::endl;
      std::cout << "hasSource" << hasSource << std::endl;
      std::cout << "numberOfSource" << numberOfSource << std::endl;

    }

    std::cout << "[fluid]" << std::endl;
    std::cout << "orderBdfNS = " << orderBdfNS << "# fluid bdf order for NS model (default : 1)" << std::endl;
    std::cout << "orderBdfFS = " << orderBdfFS << "# fluid bdf order for fractional step (default : 1)" << std::endl;
    std::cout << "density = " << density << " # fluid density"<< std::endl;
    std::cout << "viscosity = " << viscosity << " # fluid viscosity"<< std::endl;
    std::cout << "explicitAdvection = " << explicitAdvection << " # explicit Advection scheme (FracStep)"<< std::endl;

    std::cout << "NSmodifFlag = " << NSmodifFlag << " # Flag for modified NS model 0 => no modified NS model" << std::endl;
    std::cout << "NS_modifLabel = " << NS_modifLabel << " # Label where we apply modified NS"<< std::endl;
    std::cout << "characteristicMethod = " << characteristicMethod << " # 0 => semi-implicit, if != 0 method of characteristics (= 1 RK4 scheme, else Euler scheme)" << std::endl;
    std::cout << "NS_alpha like a capacitance in Rp,C,Rd LumpedModelBC model = " << NS_alpha << " # C/Volume"<< std::endl;
    std::cout << "NS_beta like inverse of distal resistance in Rp,C,Rd LumpedModelBC model =  " << NS_beta << " # 1/(Rd*Volume)"<< std::endl;
    std::cout << "NS_gamma like proximal resistance in Rp,C,Rd LumpedModelBC model = " << NS_gamma << " # Rp*Surface/Long"<< std::endl;

    std::cout << "addedBoundaryFlag = " << addedBoundaryFlag << " # Flag to add scpecific boundary terms: 0= add nothing, 1 = RIS model, 2 = outflow stab, 3 = cardiac cycle" << std::endl;
    std::cout << "stabilizationLabel = ";
    std::cout << "inletLabel = "  << inletLabel << std::endl;
    std::cout << "outletLabel = "  << outletLabel << std::endl;
    std::cout << "referenceValue1 = "  << referenceValue1 << std::endl;
    std::cout << "referenceValue2 = "  << referenceValue2 << std::endl;
    std::cout << "referenceValue3 = "  << referenceValue3 << std::endl;
    std::cout << "referenceValue4 = "  << referenceValue4 << std::endl;
    for(std::size_t ilab=0; ilab<stabilizationLabel.size(); ilab++) {
      std::cout << stabilizationLabel[ilab] << "  ";
    }
    std::cout << std::endl;
    std::cout << "stabilizationCoef = " << stabilizationCoef << " # Coef for stabilization default = 0.5 " << std::endl;
    std::cout << "NSequationFlag = " << NSequationFlag << " 0 => Stokes, 1 => (default value) normal component of stress as natural boundary condition, 2 => total pressure with rotational formulation as natural boundary condition, 3 => total pressure without rotational formulation as natural boundary condition " << std::endl;
    std::cout << "stabSUPG = " << stabSUPG << " # SUPG stabilization parameter " << std::endl;
    std::cout << "stabdiv = " << stabdiv << " # div stabilization parameter " << std::endl;
    std::cout << "typeSUPG = " << typeSUPG << " # type SUPG stabilization   " << std::endl;
    std::cout << "gammaRN = " << gammaRN << " # Robin parameter for RN iterations   " << std::endl;
    std::cout << "orderPressureExtrapolation = " << orderPressureExtrapolation << " # NSFracStep pressure extrapolation order" << std::endl;
    std::cout << "incrementalFS = " << incrementalFS << " # Flag for FracStep incremental scheme" << std::endl;
    std::cout << "coefRotScheme = " << coefRotScheme << " # NSFracStep rotational incremental coef" << std::endl;


    std::cout << "[fsi]" << std::endl;
    for (unsigned int i = 0; i < betaNormalized.size(); i++) {
      std::cout << "beta/rho on branch"<< i << " = " << betaNormalized[i] << ", ";
      std::cout << "section at rest on branch "<< i << " = " << sectionAtRest[i] << ", ";
      std::cout << std::endl;
    }

    std::cout << "[solid]" << std::endl;
    std::cout << "densitySolid = " << densitySolid << " # Solid density"<< std::endl;
    std::cout << "young = " << young << " # solid Young's modulus"<< std::endl;
    std::cout << "poisson = " << poisson << " # solid Poisson's coefficient"<< std::endl;
    std::cout << "coeffReaction = " << coeffReaction << " # coeffReaction"<< std::endl;
    std::cout << "thickness = " << thickness << " # solid thickness (std::string model)"<< std::endl;
    std::cout << "alpha_rayleigh = " <<alpha_rayleigh << " # solid mass Rayleigh damping coefficient"<< std::endl;
    std::cout << "beta_rayleigh = "  << beta_rayleigh << " # solid stiffness Rayleigh damping coefficient"<< std::endl;
    std::cout << "planeStressStrain = " <<  planeStressStrain << " # plane Stress (1) or Strain assumption (2) "<< std::endl;

    if ((model=="Bidomain") || (model=="BidomainDecoupled") || (model=="BidomainThorax")) {
      std::cout << "[elec]" << std::endl;
      //elec
      std::cout << "orderBdfEdp = " << orderBdfEdp << "# elec orderBdfEdp" << std::endl;
      std::cout << "orderBdfIonic = " << orderBdfIonic << "# elec oderBdfIonic" << std::endl;
      //elec - parameters
      std::cout << "typeOfIonicModel = " << typeOfIonicModel << "# elec typeOfIonicModel" << std::endl;
      std::cout << "hasHeteroTauClose = " << hasHeteroTauClose << "# elec hasHeteroTauClose" << std::endl;
      std::cout << "hasAppliedExteriorCurrent = " << hasAppliedExteriorCurrent << "# elec hasAppliedExteriorCurrent" << std::endl;
      if(typeOfIonicModel=="schaf") {
        std::cout << "printIonicVar" << printIonicVar << "# elec printIonicVar" << std::endl;
        std::cout << "tauOpen = " << tauOpen << "# elec tauOpen" << std::endl;
        std::cout << "tauClose = " << tauClose << "# elec tauClose" << std::endl;
        std::cout << "tauCloseEndo = " << tauCloseEndo << "# elec tauCloseEndo" << std::endl;
        std::cout << "tauCloseCell = " << tauCloseCell << "# elec tauCloseCell" << std::endl;
        std::cout << "tauCloseEpi = " << tauCloseEpi << "# elec tauCloseEpi" << std::endl;
        std::cout << "tauCloseRV = " << tauCloseRV << "# elec tauCloseRV" << std::endl;
        std::cout << "tauIn = " << tauIn << "# elec tauIn" << std::endl;
        std::cout << "tauOut = " << tauOut << "# elec tauOut" << std::endl;
        std::cout << "kTanhMSR = " << kTanhMSR << "# elec kTanhMSR" << std::endl;
        std::cout << "vGate = " << vGate << "# elec vGate" << std::endl;
      }

      else if(typeOfIonicModel=="fhn") {
        std::cout << "printIonicVar" << printIonicVar << "# elec printIonicVar" << std::endl;
        std::cout << "epsilon = " << epsilon << "# elec epsilon" << std::endl;
        std::cout << "beta = " << beta << "# elec beta" << std::endl;
        std::cout << "gammaEl = " << gammaEl << "# elec gammaEl" << std::endl;
        std::cout << "f0 = " << f0 << "# elec f0" << std::endl;
        std::cout << "alpha = " << alpha << "# alpha" << std::endl;
      }
      std::cout << "vMin = " << vMin << "# elec vMin" << std::endl;
      std::cout << "vMax = " << vMax << "# elec vMax" << std::endl;
      std::cout << "Am = " << Am << "# elec Am" << std::endl;
      std::cout << "Cm = " << Cm << "# elec Cm" << std::endl;
      std::cout << "extraFiberTensor = " << extraFiberTensor << "# elec extraFiberTensor" << std::endl;
      std::cout << "intraFiberTensor = " << intraFiberTensor << "# elec intraFiberTensor" << std::endl;
      std::cout << "extraTransvTensor = " << extraTransvTensor << "# elec extraTransvTensor" << std::endl;
      std::cout << "intraTransvTensor = " << intraTransvTensor << "# elec intraTransvTensor" << std::endl;
      std::cout << "extraFiberTensorAtria = " << extraFiberTensorAtria << "# elec extraFiberTensor" << std::endl;
      std::cout << "intraFiberTensorAtria = " << intraFiberTensorAtria << "# elec intraFiberTensor" << std::endl;
      std::cout << "extraTransvTensorAtria = " << extraTransvTensorAtria << "# elec extraTransvTensor" << std::endl;
      std::cout << "intraTransvTensorAtria = " << intraTransvTensorAtria << "# elec intraTransvTensor" << std::endl;
      std::cout << "extraFiberTensorVent = " << extraFiberTensorVent << "# elec extraFiberTensorVent" << std::endl;
      std::cout << "intraFiberTensorVent = " << intraFiberTensorVent << "# elec intraFiberTensorVent" << std::endl;
      std::cout << "extraTransvTensorVent = " << extraTransvTensorVent << "# elec extraTransvTensorVent" << std::endl;
      std::cout << "intraTransvTensorVent = " << intraTransvTensorVent << "# elec intraTransvTensorVent" << std::endl;
      if(monodomain==1) {
        std::cout << "monodomain model" << std::endl;
      } else {
        std::cout << "bidomain model" << std::endl;
      }
      std::cout << "sigmaThorax = " << sigmaThorax << "# elec sigmaThorax" << std::endl;
      std::cout << "sigmaLung = " << sigmaLung << "# elec sigmaLung" << std::endl;
      std::cout << "sigmaBone = " << sigmaBone << "# elec sigmaBone" << std::endl;
      //elec - applied current
      std::cout << "typeOfAppliedCurrent = " << typeOfAppliedCurrent << "# elec typeOfAppliedCurrent" << std::endl;
      std::cout << "typeOfAppliedCurrentExt = " << typeOfAppliedCurrentExt << "# elec typeOfAppliedCurrentExt" << std::endl;
      std::cout << "timePeriod = " << timePeriod << "# elec timePeriod" << std::endl;
      std::cout << "timePeriodVentricle = " << timePeriodVentricle << "# elec timePeriodVentricle" << std::endl;
      std::cout << "timePeriodAtria = " << timePeriodAtria << "# elec timePeriodAtria" << std::endl;
      if (typeOfAppliedCurrent=="multiple") {
        std::cout << "beatNumber = " << beatNumber << "# elec beatNumber" << std::endl;
        std::cout << "beatDecreasing = " << beatDecreasing << "# elec beatDecreasing" << std::endl;
      }
      std::cout << "delayStim: ";
      for (unsigned int i = 0; i < delayStim.size(); i++)
        std::cout << delayStim[i] << ", ";
      std::cout << std::endl;
      std::cout << "delayStimVentricles: ";
      for (unsigned int i = 0; i < delayStimVentricles.size(); i++)
        std::cout << delayStimVentricles[i] << ", ";
      std::cout << std::endl;
      std::cout << "stimTime = " << stimTime << "# elec stimTime" << std::endl;
      std::cout << "stimTimeLV = " << stimTimeLV << "# elec stimTimeLV" << std::endl;
      std::cout << "stimTimeRV = " << stimTimeRV << "# elec stimTimeRV" << std::endl;
      //elec - infarct
      std::cout << "hasInfarct = " << hasInfarct << "# elec hasInfarct" << std::endl;
      if (hasInfarct) {
        std::cout << "x_infarct = " << x_infarct << "# elec x_infarct" << std::endl;
        std::cout << "y_infarct = " << y_infarct << "# elec y_infarct" << std::endl;
        std::cout << "z_infarct = " << z_infarct << "# elec z_infarct" << std::endl;
        std::cout << "radius_infarct = " << radius_infarct << "# elec radius_infarct" << std::endl;
      }
      //elec - BBB
      if (typeOfAppliedCurrent=="BBB") {
        std::cout << "hasVentriclesBundleBrunchBlock" << hasVentriclesBundleBrunchBlock << "# elec hasVentriclesBundleBrunchBlock" << std::endl;
        std::cout << "sideOfBBB = " << sideOfBBB << "# elec sideOfBBB" << std::endl;
        std::cout << "BBBwithDelay = " << BBBwithDelay << "# elec BBBwithDelay" << std::endl;
        std::cout << "BBBwithAngleBlock = " << BBBwithAngleBlock << "# elec BBBwithAngleBlock" << std::endl;
        std::cout << "BBBDelayStim = " << BBBDelayStim << "# elec BBBDelayStim" << std::endl;
        std::cout << "angleIappBBB = " << angleIappBBB << "# elec angleIappBBB" << std::endl;
      }
      //elec - Bachmann Bundle Block
      if (hasPartialBachmannBundleBlock) {
        std::cout << "hasPartialBachmannBundleBlock = " << hasPartialBachmannBundleBlock << "# elec hasPartialBachmannBundleBlock" << std::endl;
        std::cout <<  "valueBBBlock = " << valueBBBlock << "# elec valueBBBlock" << std::endl;
      }
      //elec - Kent Bundle
      if (hasKentBundle) {
        std::cout << "hasKentBundle = " << hasKentBundle << "#elec hasKentBundle" << std::endl;
      }
      //elec - Torsades de pointe
      if (torsade) {
        std::cout << "torsade = " << torsade << "#elec torsade" << std::endl;
        std::cout << "torsadeTimeBegin = " << torsadeTimeBegin << "#elec torsadeTimeBegin" << std::endl;
        std::cout << "torsadeTimeEnd = " << torsadeTimeEnd << "#elec torsadeTimeEnd" << std::endl;
        std::cout << "tauCloseTorsade = " << tauCloseTorsade << "#elec tauCloseTorsade" << std::endl;
        std::cout << "tauCloseEndoTorsade = " << tauCloseEndoTorsade << "#elec tauCloseEndoTorsade" << std::endl;
        std::cout << "tauCloseCellTorsade = " << tauCloseCellTorsade << "#elec tauCloseCellTorsade" << std::endl;
        std::cout << "tauCloseEpiTorsade = " << tauCloseEpiTorsade << "#elec tauCloseEpiTorsade" << std::endl;
        std::cout << "tauCloseRVTorsade = " << tauCloseRVTorsade << "#elec tauCloseRVTorsade" << std::endl;
      }
      //elec - ECG
      std::cout << "writeECG = " << writeECG << "# elec writeECG" << std::endl;
      std::cout << "writeElectrodesMeas = " << writeElectrodesMeas << "# elec writeElectrodesMeas" << std::endl;
      std::cout << "writeMatrixECG = " << writeMatrixECG << "# elec writeMatrixECG" << std::endl;
      if(writeECG | writeElectrodesMeas | writeMatrixECG) {
        std::cout << "ECGmatchFile = " << ECGmatchFile << "# elec ECGmatchFile" << std::endl;
        std::cout << "ECGThoraxmatchFile = " << ECGThoraxmatchFile << "# elec ECGThoraxmatchFile" << std::endl;
        std::cout << "ECGmatrixFile = " << ECGmatrixFile << "# elec ECGmatrixFile" << std::endl;
        std::cout << "ECGfileName = " << ECGfileName << "# elec ECGfileName" << std::endl;
        std::cout << "ECGtabHeader = " << ECGtabHeader << "# elec ECGtabHeader" << std::endl;
        std::cout << "ECGcreateFigure = " << ECGcreateFigure << "# elec ECGcreateFigure" << std::endl;
      }
      std::cout << std::endl;
    }


    std::cout << "[initialCondition]" << std::endl;
    std::cout << "Initial condition imposed on " << valueInitCond.size() << " variables:" << std::endl;

    if (hasInitialCondition) {
      for (std::size_t i = 0; i < valueInitCond.size(); i++) {
        std::cout << nameVariableInitCond[i] << " on components :" << componentInitCond[i] << ", with values : " << valueInitCond[i]<< std::endl;
      }
      std::cout << "restartSolution = " << restartSolution << std::endl;
      if (restartSolution) {
        std::cout << "restartSolutionDir = " << restartSolutionDir << std::endl;
        std::cout << "restartSolutionIter = " << restartSolutionIter << std::endl;
      }
      std::cout << "frequencyBackup = " << frequencyBackup << std::endl;
    } else {
      std::cout << "no variable has imposed initial condition." << std::endl;
    }

    std::cout << "[boundary condition]" << std::endl;
    std::cout << "Boundary condition applied by: ";
    switch (essentialBoundaryConditionsMethod) {
      case 0:
        std::cout << "non- symmetric pseudo-elimination. ";
        break;
      case 1:
        std::cout << "symmetric pseudo-elimination.";
        break;
      case 2:
        std::cout << "penalization. ";
        break;
      default:
        FEL_ERROR("This method for boundary condition doesn't exist.");
        break;
    }
    std::cout << "They are: \n";
    int cptValue = 0;
    int start = 0;
    for (std::size_t i = 0; i < type.size(); i++) {
      std::cout << type[i] ;
      std::cout << " for " << variable[i] ;
      std::cout << " and on components: " << component[i];
      std::cout << " with value type: " << typeValue[i] ;
      if ( label.size() > labelName.size() ) {
        std::cout << " on elements with labels: ";
        for (int j = 0; j<numLabel[i]; j++)
          std::cout << label[j+start] << " " ;
        start +=numLabel[i];
      } else {
        std::cout << " on elements with label name: ";
        for (int j = 0; j<numLabel[i]; j++)
          std::cout << labelName[j+start] << " ("<<label[j+start] <<") ";
        start += numLabel[i];
      }
      if (typeValue[i]=="Constant") {
        std::cout << " with value: \n";
        if ( component[i] == "Comp1") {
          outstr << "on x:" << value[cptValue] << std::endl;
          cptValue += 1;
        } else if ( component[i] == "Comp2" ) {
          outstr << "on y:" << value[cptValue] << std::endl;
          cptValue += 1;
        } else if ( component[i] == "Comp3" ) {
          outstr << "on z:" << value[cptValue] << std::endl;
          cptValue += 1;
        } else if ( component[i] == "Comp12" ) {
          outstr << "on x:" << value[cptValue] << ", on y:" << value[cptValue+1] << std::endl;
          cptValue += 2;
        } else if ( component[i] == "Comp13" ) {
          outstr << "on x: "<< value[cptValue] << ", on z:" << value[cptValue+1] << std::endl;
          cptValue += 2;
        } else if ( component[i] == "Comp23" ) {
          outstr << "on y: "<< value[cptValue] << ", on z:" << value[cptValue+1] << std::endl;
          cptValue += 2;
        } else if ( component[i] == "Comp123" ) {
          outstr << "on x: "<< value[cptValue] << ", on y:" << value[cptValue+1] << " ,on z: " << value[cptValue+2];
          cptValue += 3;
        } else if ( component[i] == "CompNA" ) {
          outstr << value[cptValue];
          cptValue += 1;
        } else
          FEL_ERROR("problem with components.");
      }
      std::cout << std::endl;
    }
    if (lumpedModelBCLabel.size()>0) {
      std::cout << "and lumpedModelBC : \n";
      for (std::size_t i = 0; i < lumpedModelBCLabel.size(); i++) {
        std::cout << "lumpedModelBC " << i+1 << " -> " ;
        std::cout << "Type lumpedModelBC = " ;
        switch (lumpedModelBCType[i]) {
          case 1: // RCR
            std::cout<< "RCR or R" ;
            break;
          case 2: // RC
            std::cout<< "RC" ;
            break;
        }
        std::cout<< " // Algo lumpedModelBC = " ;
        switch (lumpedModelBCAlgo[i]) {
          case 1:
            std::cout<< "explicit" << std::endl;
            break;
          case 2:
            std::cout<< "implicit" << std::endl;
            break;
        }
        std::cout << " on elements with label name: " << lumpedModelBCLabel[i] << " // ";
        std::cout << "Name = " << lumpedModelBCName[i] << " //" ;
        std::cout<< "Rd = " << lumpedModelBC_Rdist[i] <<  " // " ;
        std::cout<< "Rp = " << lumpedModelBC_Rprox[i] <<  " // " ;
        std::cout<< "C = " << lumpedModelBC_C[i] <<  " // " ;
        std::cout<< "Pv = " << lumpedModelBC_Pvenous[i] <<  " // " ;
        std::cout<< "Pd0 = " << lumpedModelBC_Pdist_init[i]   ;
        std::cout << std::endl;
      }
    }

    std::cout << "Additionnal parameters: ";
    for (unsigned int i = 0; i < userParameter.size(); i++)
      std::cout << userParameter[i] << ", ";
    std::cout << std::endl;

    std::cout << "alphaRobin -- Coeff in front of variable for Robin BC: ";
    for (unsigned int i = 0; i < alphaRobin.size(); i++)
      std::cout <<   alphaRobin[i] << ", ";
    std::cout << std::endl;

    std::cout << "betaRobin -- Coeff of normal derivative for Robin BC: ";
    for (unsigned int i = 0; i < betaRobin.size(); i++)
      std::cout <<   betaRobin[i] << ", ";
    std::cout << std::endl;


    std::cout << "Coeff for Robin Normal BC: ";
    for (unsigned int i = 0; i < alphaRobinNormal.size(); i++)
      std::cout <<   alphaRobinNormal[i] << ", ";
    std::cout << std::endl;


    if ( nonLinearKoiterModel ) {
      std::cout << " Strucutre model via a non Linear Koiter Membrane"<< std::endl;
    } else {
      std::cout << " Strucutre model via a Linear Koiter Membrane"<< std::endl;
    }
    std::cout <<"Penalization coefficient for EmbedFSI: "<<penValueForEmbedFSI<<std::endl;
    std::cout <<"Data for EmbedFSI boundary condition: "<<std::endl;
    std::cout <<"           shell young modulus: "<<youngShell<<std::endl;
    std::cout <<"               fiber preStress: "<<preStressFibers<<std::endl;
    std::cout <<"           fiber Young modulus: "<<youngFibers<<std::endl;
    std::cout <<"           Density of fiber per EmbedFSI label: ";
    for ( std::size_t i = 0; i< fiberDensity.size(); i++) {
      std::cout<<fiberDensity[i]<<"  ";
    }
    std::cout<<std::endl;
    std::cout <<"     shell poisson coefficient: "<<poissonShell<<std::endl;
    std::cout <<"      density of the structure: "<<densityStructure<<std::endl;
    std::cout <<"            width of the shell: "<<widthShell<<std::endl;
    std::cout <<"            reference pressure: "<<outsidePressure<<std::endl;
    std::cout <<"             current case Test: "<<idCase<<std::endl;
    std::cout <<"       name of this simulation: "<<nameTest<<std::endl;
    std::cout <<"    incoming averaged pressure: "<<incomingPressure<<std::endl;
    std::cout <<"shape param for pressure input: "<<propCardiacCycle<<std::endl;
    std::cout <<std::endl;
    std::cout <<"Parameters for autoregulation: "<<std::endl;
    std::cout <<"Maximum active pressure: "<<maxT<<std::endl;
    std::cout <<"Shape parameter defining the range: "<<q<<std::endl;
    std::cout <<"Range of the regulation: [ "<<down<<" : "<<up<<" ] "<<std::endl;
    std::cout <<"Nominal pressure value: "<<nominalPressure<<std::endl;
    std::cout <<"Wheter you are regulating flow with outlet condition: "<<useRegulationInWindkessel<<std::endl;
    std::cout <<"Proportion of total distal resistance due to capillaries and veins: "<<windkesselProp<<std::endl;
    std::cout <<"Parameter for oulet regulation: ";
    for ( std::size_t i = 0; i< lumpedModelBC_regParam.size(); i++) {
      std::cout<<lumpedModelBC_regParam[i]<<"  ";
    }
    std::cout<<std::endl;
    std::cout<<"id of the case where control is not used: "<<idControlFreeCase<<std::endl;
    std::cout<<std::endl;

    std::cout << "cardiacOutput = " << cardiacOutput << std::endl;
    std::cout << "heartRate = " << heartRate << std::endl;
    std::cout << "[petsc section]"<< std::endl;
    std::cout << "linearSolverVerbose" << linearSolverVerbose << std::endl;
    for (std::size_t i = 0; i < solver.size(); i++) {
      std::cout << "[petsc configuration for " << i << "th linearProblem]" << std::endl;

      std::cout << "Name of the solver: " << solver[i] << " # gmres bicgstab " << std::endl;
      std::cout << "Name of the preconditioner: " << preconditioner[i] << std::endl;
      std::cout << "Option for re-use of preconditioner : " << setPreconditionerOption[i] << std::endl;
      std::cout << "Relative tolerance: " << relativeTolerance[i] << std::endl;
      std::cout << "Absolute tolerance: " << absoluteTolerance[i] << std::endl;
      std::cout << "Number maximum of iteration to solve the system: " << maxIteration[i] << std::endl;
      std::cout << "Restart for gmres: " << gmresRestart[i] << std::endl;
      std::cout << "Initialize solver with solution in previous timestep: " << initSolverWithPreviousSolution[i] << std::endl;
      std::cout << std::endl;
    }

    std::cout << "[verdandi]" << std::endl;
    std::cout << "Diagonal value of the backgroung error variance: " << stateErrorVarianceValue << std::endl;
    std::cout << "Coefficient use on heat equation to do parameter assimilation ";
    std::cout << "before laplacian matrix: " << Kparameter << std::endl;
    std::cout << "Coefficient use on heat equation to do parameter assimilation ";
    std::cout << "before source term: " << RHSparameter << std::endl;
    std::cout << std::endl;

    // ElementFieldDynamicValue
    std::cout << "[elementFieldValueMap]" << std::endl;
    for (auto it = elementFieldDynamicValueMap.begin(); it != elementFieldDynamicValueMap.end(); it++) {
          it->second->display();
          std::cout << std::endl;
      }
    }
}

/***********************************************************************************/
/***********************************************************************************/

void UniqueFelisceParam::printHelp() {
  for (auto section = m_help.begin(); section != m_help.end (); ++section) {
    std::cout << "[" << section->first << "]\n\n";

    for (auto param = section->second.begin(); param != section->second.end(); ++param)
      std::cout << *param << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

ElementFieldDynamicValue* UniqueFelisceParam::elementFieldDynamicValue(const char* name) {
  auto it = elementFieldDynamicValueMap.find(name);

  if (it == elementFieldDynamicValueMap.end()) {
    return nullptr;

  } else {
    return it->second;
  }
}

/***********************************************************************************/
/***********************************************************************************/

std::string UniqueFelisceParam::expandEnvironmentVariables(
                                                const std::string datafile, const std::string section,
                                                const std::string variableName, const std::string variableValue) {
  std::vector<std::string> allowed_envvar;
  allowed_envvar.emplace_back("HOME");
  allowed_envvar.emplace_back("FELISCE_DATA_DIR");
  allowed_envvar.emplace_back("FELISCE_INRIA_DATA_DIR");
  allowed_envvar.emplace_back("FELISCE_RESULT_DIR");

  // For now it's simpler to create the regex here (evaluated every time
  // the function is called). It's not time-consuming because will call
  // this function only a few time at the start of FELiScE. If the function
  // become called many times, the regex should become an attribute of
  // UniqueFelisceParam, evaluated in the constructors.

  // A environment variable name is introduced by the dollar sign $,
  // has letter as first character, and then possibly multiple letters,
  // digits and underscores.
  // We extract the environment variable name without the dollar sign,
  // as well as the prefix and suffix (part of the path before and after
  // the environment variable.
  // Environment variable name may be enclosed in parentheses: $(...).
  // for example: path = "/path/to/$ENV_VAR/some/dir"
  //                      <-------> <-----><------->
  //                      prefix    envvar  suffix
  std::regex environmentVariableRegex(
                                      "(.*)"                     // group 1: prefix: "/path/to/"
                                      "\\$"                      // dollar sign: "$"
                                      "\\([a-z]\\):\\([0-9]\\)"  // group 2: One or zero opening parenthese
                                      "([a-zA-Z_][a-zA-Z0-9_]*)" // group 3: environment variable name: "ENV_VAR"
                                      "(.*)"                     // group 4: suffix: "/some/dir"
                                      );

  // Some debug info included in error messages.
  std::ostringstream errinfo;
  errinfo << "In data file <" << datafile << ">, "
  << "section <" << section << ">, "
  << "variable <" << variableName << "> "
  << "has value <" << variableValue << "> ";

  std::match_results<std::string::const_iterator> results;
  std::ostringstream msg;
  std::string pathExpanded = variableValue;
  std::string prefix, envvar, suffix;
  char* envvar_value_pchar;
  unsigned int iter;
  bool normal_loop_exit = false;

  for (iter=0 ; iter<=50 ; iter++) {
    // Continue until all environment variables have been expanded.
    if (! std::regex_search(pathExpanded, results, environmentVariableRegex)) {
      normal_loop_exit = true;
      break;
    }

    // Extract parts of the regular expression match.
    prefix = results[1];
    envvar = results[3];
    suffix = results[4];

    // Error if env var not allowed.
    if (! vector_string_contains(allowed_envvar,envvar) ) {
      msg << "Not allowed environment variable "
      << "<" << envvar << ">. "
      << errinfo.str()
      << "which contains environment variable <" << envvar << ">, "
      << "but only those environment variables are allowed: ";
      for (unsigned int i=0 ; i<allowed_envvar.size()-1 ; i++) {
        msg << "<" << allowed_envvar[i] << ">, ";
      }
      msg << "<" << allowed_envvar[allowed_envvar.size()-1] << ">.";
      FEL_ERROR(msg.str().c_str());
    }

    // Error if env var not defined.
    envvar_value_pchar = getenv(envvar.c_str());
    if (envvar_value_pchar==nullptr) {
      msg << "Undefined environment variable "
      << "<" << envvar << ">. "
      << errinfo.str()
      << "which contains environment variable <" << envvar << ">, "
      << "which is not defined.";
      FEL_ERROR(msg.str().c_str());
    }

    // Error if env var contains only white characters.
    std::string envvar_value = (std::string) envvar_value_pchar;
    if (envvar_value.find_first_not_of("\t\n ") == std::string::npos) {
      msg << "Empty environment variable "
      << "<" << envvar << ">. "
      << errinfo.str()
      << "which contains environment variable <" << envvar << ">, "
      << "which contains only white charaters.";

      FEL_ERROR(msg.str().c_str());
    }
    pathExpanded = prefix + envvar_value + suffix;
  }

  // Error if infinite loop (should be because by a recursive
  // situation: FOO='$FOO')
  if (! normal_loop_exit) {
    msg << "Infinite loop. "
    << errinfo.str()
    << "which caused an infinte loop: "
    << "Does an environment variable reference itself?";
    FEL_ERROR(msg.str().c_str());
  }

  // Error if of a miss-formatted environment variable.
  if (string_contains_substring(pathExpanded,"$")) {
    msg << "Miss-formatted environment variable. "
    << errinfo.str()
    << "Which contains '$' sign, but the syntax does not "
    << "match a regular expression.";
    FEL_ERROR(msg.str().c_str());
  }

  return pathExpanded;
}
}
