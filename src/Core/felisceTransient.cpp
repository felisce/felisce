//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "Core/felisceTransient.hpp"

namespace felisce 
{

FelisceTransient::FelisceTransient():
  timeStep(FelisceParam::instance().timeStep),
  time(FelisceParam::instance().time),
  iteration(FelisceParam::instance().iteration) 
{

}

/***********************************************************************************/
/***********************************************************************************/

FelisceTransient::FelisceTransient(const FelisceTransient& rFelisceTransient):
  timeStep(rFelisceTransient.timeStep),
  time(rFelisceTransient.time),
  iteration(rFelisceTransient.iteration) 
{

}

/***********************************************************************************/
/***********************************************************************************/

std::string FelisceTransient::itStr() const 
{
  const int N = 5;
  std::stringstream aus;
  int k;
  if ( iteration < 10 ) {
    k=1;
  } else if ( iteration < 100 ) {
    k=2;
  } else if ( iteration < 1000 ) {
    k=3;
  } else {
    k=4;
  }
  for(int nzero = 0; nzero < N - k; ++nzero)
    aus << "0";
  aus << iteration;
  return aus.str().c_str();
}

/***********************************************************************************/
/***********************************************************************************/

void FelisceTransient::print() const 
{
  std::cout << "=============================================================="
            << "\nFelisceTransient\n"
            << "=============================================================="
            <<  "\ntimeStep = " << timeStep
            <<  "\ntime = " << time 
            <<  "\niteration = " << iteration << "\n" << std::endl;
}

}
