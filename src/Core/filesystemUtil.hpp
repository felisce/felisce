//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//
//

#ifndef FELISCE_FILESYSTEM_UTIL_HPP
#define FELISCE_FILESYSTEM_UTIL_HPP

// System includes
// We haven't checked which filesystem to include yet
#ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
// Check for feature test macro for <filesystem>
#   if defined(__cpp_lib_filesystem)
#     define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
// Check for feature test macro for <experimental/filesystem>
#   elif defined(__cpp_lib_experimental_filesystem)
#     define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
// We can't check if headers exist...
// Let's assume experimental to be safe
#   elif !defined(__has_include)
#     define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
// Check if the header "<filesystem>" exists
#   elif __has_include(<filesystem>)
// If we're compiling on Visual Studio and are not compiling with C++17, we need to use experimental
#     ifdef _MSC_VER
// Check and include header that defines "_HAS_CXX17"
#       if __has_include(<yvals_core.h>)
#         include <yvals_core.h>
// Check for enabled C++17 support
#         if defined(_HAS_CXX17) && _HAS_CXX17
// We're using C++17, so let's use the normal version
#           define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#         endif
#       endif
// If the marco isn't defined yet, that means any of the other VS specific checks failed, so we need to use experimental
#       ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
#         define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
#       endif
// Not on Visual Studio. Let's use the normal version
#     else // #ifdef _MSC_VER
#       define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 0
#     endif
// Check if the header "<filesystem>" exists
#   elif __has_include(<experimental/filesystem>)
#     define INCLUDE_STD_FILESYSTEM_EXPERIMENTAL 1
// Fail if neither header is available with a nice error message
#   else
#     error Could not find system header "<filesystem>" or "<experimental/filesystem>"
#   endif
// We priously determined that we need the exprimental version
#   if INCLUDE_STD_FILESYSTEM_EXPERIMENTAL
#     include <experimental/filesystem>
// We need the alias from std::experimental::filesystem to std::filesystem
namespace std {
  namespace filesystem = experimental::filesystem;
}
// We have a decent compiler and can use the normal version
#   else
#     include <filesystem>
#   endif
#endif // #ifndef INCLUDE_STD_FILESYSTEM_EXPERIMENTAL

#include <cstdio>  /* defines FILENAME_MAX */
#if defined(_WIN32) // Windows compilation
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif
#include <string>

// External includes

// Project includes

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

namespace felisce
{
 /**
  * @namespace filesystemUtil
  * @ingroup Core
  * @brief Functions to manipulate files and directories
  * @details This is keep due to not full support of C++17 in the compilers from Ubuntu 18.04. This should be replaced with std::filesystem
  * @author Vicente Mataix Ferrandiz + some people I cannot track in SVN
  */
 namespace filesystemUtil
 {

    /**
     * @brief Test that a directory exists.
     * @param[in] path Path of the directory (preferably absolute path).
     * @return Return value is true if directory exists, false else.
     */
    bool directoryExists(const char *path);

    /**
     * @brief Test that a file exists.
     * @param[in] path Path of the file (preferably absolute path).
     * @return Return value is true if file exists, false else.
     */
    bool fileExists (const std::string& name);

    /**
     * @brief Copies the file in a different location
     * @param inFileName Input filename
     * @param outFileName Output filename
     */
    void copyFileSomewhereElse(
      const std::string& inFileName,
      const std::string& outFileName
      );
  }
}

#endif
