//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Julien Castelneau, Dominique Chapelle, Miguel Fernandez,
//                   Jeremie Foulon, David Froger, Jean-Frederic Gerbeau,
//                   Vincent Martin, Philippe Moireau, Marina Vidrascu
//

// System includes

// External includes

// Project includes
#include "Core/util_string.hpp"

namespace felisce 
{
bool string2bool(const std::string& str) 
{
  // value is true
  if (str=="true" || str=="True") {
    return true;
  } else if (str=="false" || str== "False") { // value is false
    return false;
  } else { // value is not a boolean, error
    throw 0;
  }
}

/***********************************************************************************/
/***********************************************************************************/

std::string bool2string(const bool value) 
{
  if (value)
    return "true";
  else
    return "false";
}

/***********************************************************************************/
/***********************************************************************************/

void split(const std::string& str, std::vector<std::string>& vect, const std::string& delimiters) 
{
  vect.clear();

  std::string tmp;
  std::string::size_type index_beg, index_end;

  index_beg = str.find_first_not_of(delimiters);

  while (index_beg != std::string::npos) {
    index_end = str.find_first_of(delimiters, index_beg);
    tmp = str.substr(index_beg, index_end == std::string::npos ? std::string::npos : (index_end - index_beg));
    vect.push_back(tmp);
    index_beg = str.find_first_not_of(delimiters, index_end);
  }
}

/***********************************************************************************/
/***********************************************************************************/

char* c_string(const std::string& str) 
{
  std::string buffer(str);
  std::size_t sizeChar = buffer.size() + 1;
  char *strChar = new char[ sizeChar ];
  strncpy( strChar, buffer.c_str(), sizeChar );
  return strChar;
}

/***********************************************************************************/
/***********************************************************************************/

int CountOccurenceChar(const std::string& str,char c) 
{
  int count=0;
  char* ch_ptr = c_string(str);
  char * start_string=ch_ptr;
  while(*ch_ptr != '\0') {
    if (*ch_ptr == c) ++count;
    ++ch_ptr;
  }
  delete [] start_string;
  start_string = nullptr;
  ch_ptr = nullptr;
  return count;
}

/***********************************************************************************/
/***********************************************************************************/

std::string ReplaceWilcardByInt(const std::string& strWildcard, const int i,char c) 
{
  std::string strInt=strWildcard;
  const std::size_t dim = CountOccurenceChar(strWildcard, c);
  std::ostringstream index;
  index << i;
  if(index.str().size() > dim) {
    FEL_ERROR("Integer too large for the dim of wildcard")
  }
  std::string zero="";
  for(std::size_t j=0; j<dim-index.str().size(); j++) zero += "0";
  const std::string num = zero + index.str();
  const std::size_t start = strInt.find(c);
  strInt.replace(start,dim,num);
  return strInt;
}

/***********************************************************************************/
/***********************************************************************************/

bool is_integer(const std::string& str) 
{
  bool ans;

  ans = (str.size() > 0 && std::isdigit(str[0]))
        || (str.size() > 1 && (str[0] == '+' || str[0] == '-'));

  unsigned int i(1);
  while (i < str.size() && ans) {
    ans = ans && isdigit(str[i]);
    i++;
  }

  return ans;
}

/***********************************************************************************/
/***********************************************************************************/

bool startswith(const std::string& str, const std::string& prefix) 
{
  return (str.compare(0,prefix.size(),prefix) == 0);
}

/***********************************************************************************/
/***********************************************************************************/

void ppcharFromVstring(std::vector<std::string>& vstring, int& nstr, char**& ppchar) 
{
  nstr = vstring.size();

  std::vector<std::size_t> strLength(nstr);

  for (int istr=0 ; istr<nstr ; istr++)
    strLength[istr] = vstring[istr].size();

  ppcharAllocate(strLength, ppchar);

  for (int istr=0 ; istr<nstr ; istr++)
    strcpy( ppchar[istr], vstring[istr].c_str() );
}

/***********************************************************************************/
/***********************************************************************************/

void ppcharToVstring(const int nstr, const char** ppchar, std::vector<std::string>& vstring) 
{
  vstring.resize(nstr);

  std::string elt;
  for (int istr=0 ; istr<nstr ; istr++) {
    elt = ppchar[istr];   // convert from char* to std::string (required)
    vstring[istr] = elt;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ppcharFree(int& nstr, char**& ppchar) 
{
  if (ppchar!=nullptr) {
    for (int istr=0; istr<nstr; istr++) {
      if (ppchar[istr]!=nullptr) {
        delete[] ppchar[istr];
      }
    }
    delete[] ppchar;
    ppchar = nullptr;
    nstr = 0;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ppcharAllocate(std::vector<std::size_t>& strLength, char**& ppchar) 
{
  const std::size_t nstr = strLength.size();
  ppchar = new char*[nstr]();
  for (std::size_t istr=0 ; istr < nstr ; istr++)
    ppchar[istr] = new char[strLength[istr]+1]();
}

/***********************************************************************************/
/***********************************************************************************/

void ppcharCopy(int  nstrSrc, char**  ppcharSrc,
                int& nstrDst, char**& ppcharDst) 
{
  nstrDst = nstrSrc;

  std::vector<std::size_t> strLength(nstrSrc);

  for (int istr=0 ; istr<nstrSrc ; istr++)
    strLength[istr] = strlen(ppcharSrc[istr]);

  ppcharAllocate(strLength, ppcharDst);

  for (int istr=0 ; istr<nstrSrc ; istr++)
    strcpy( ppcharDst[istr], ppcharSrc[istr] );
}

/***********************************************************************************/
/***********************************************************************************/

int ppcharIndex(int nstr, const char** ppchar, const char* str) 
{
  for (int istr=0 ; istr<nstr; istr++) {
    if ( strcmp(str,ppchar[istr]) == 0 )
      return istr;
  }

  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

bool string_contains_substring(const std::string& str, const std::string& substr) 
{
  return str.find(substr) != std::string::npos;
}

/***********************************************************************************/
/***********************************************************************************/

bool vector_string_contains(const std::vector<std::string>& vector_str, const std::string& str) 
{
  return std::find(vector_str.begin(), vector_str.end(), str) != vector_str.end();
}

}
