//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Julien Castelneau, Dominique Chapelle, Miguel Fernandez,
//                   Jeremie Foulon, David Froger, Jean-Frederic Gerbeau,
//                   Vincent Martin, Philippe Moireau, Marina Vidrascu
//

#ifndef UTIL_STRING_HPP
#define UTIL_STRING_HPP

// System includes
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce 
{
  /*!
   Splits the std::string str by any char appears in delim, not including this char.
   For instance, the call SplitString( "abcdefghabcd", "ade", v);
   will put in v the strings "bc" "fgh" "bc".
   */
  void split(const std::string& str, std::vector<std::string>& vect, const std::string& delimiters) ;

  char* c_string(const std::string& str);

  /*! Count the occurence of char 'c' in std::string str */
  int CountOccurenceChar(const std::string& str,char c);

  /*! Replace a wilcard 'c' by the value of an integer
   Example: ReplaceWilcardByInt("name.****.vct",12,'*')
   returns name.0012.vct
   */
  std::string ReplaceWilcardByInt(const std::string& strWildcard, const int i,char c) ;

  void convert(const std::string& s, std::string& out);

  /// Checks whether a string is an integer.
  /*!
   \param[in] str string to be checked.
   \return True if \a str is an integer, false otherwise.
   */
  bool is_integer(const std::string& str);

  // return true if 'str' starts with 'prefix'
  bool startswith(const std::string& str, const std::string& prefix);

  bool string2bool(const std::string& str);

  std::string bool2string(const bool value);

  //---------------------------------------------------------------------------
  // Functions to work on a C-style list of strings
  //---------------------------------------------------------------------------

  /*!
   * \brief Copy a C-style list of std::string to a C++-style list of std::string.
   *
   *  Clear vstring content and fill it with strings contained in ppchar.
   *
   * \param[in]  ppchar   C-style list of strings.
   * \param[in]  nstr     number of strings contained in ppchar.
   * \param[out] vstring  C++-style list of strings.
   */
  void ppcharToVstring(const int nstr, const char **ppchar, std::vector<std::string>& vstring);

  /*!
   * \brief Create a C-style list of std::string from a C++-style list of std::string.
   *
   * Allocate memory pointed by ppchar and fill it with strings contained
   * in vstring.
   *
   * \param[in]      vstring  C++-sytle list of strings.
   * \param[in,out]  ppchar   In-value must be NULL.  Out-value is a C-style list of strings.
   * \param[out]     nstr     number of strings contained in ppchar.
   *
   * \warning Memory management:
   * - before to call: To avoid memory leak, make sure to free the memory
   *   (if any) that ppchar points to (you could use ppcharFree).
   * - after to call: Once you have finished with ppchar, make sure to deallocate
   *   its memory using the ppcharFree function.
   */
  void ppcharFromVstring(std::vector<std::string>& vstring, int &nstr, char **&ppchar);

  /*!
   * \brief Allocate memory for a C-style list of std::string.
   *
   * \param[in]      strLength Length of the strings that we want to allocate memory for.
   * \param[in,out]  ppchar    In-value must be NULL. Out-value points to the allocated memory.
   */
  void ppcharAllocate(std::vector<std::size_t>& strLength, char **&ppchar);

  /*!
   * \brief Free the memory of a C-style list of strings.
   *
   * If ppchar is NULL, nothing is done. Otherwise, deletes the memory
   * that ppchar points to.
   *
   * \param[in,out]  nstr    In-value is the number of strings contained in ppchar. Out-value is zero.
   * \param[in,out]  ppchar  In-value is a C-style list of strings. Out-value is NULL
   *
   * \warning Memory management:
   * Don't call this function on a bad pointer, for example
   * an uninitialized ppchar: "char **ppchar;" this would yields to
   * a segmentation fault or something like this.
   */
  void ppcharFree(int &nstr, char **&ppchar);


  /*!
   * \brief Copy a C-style list of std::string.
   *
   * \param[in]     nstrSrc    Number of std::string to be copied.
   * \param[in]     ppcharSrc  Strings to be copied.
   * \param[out]    nstrDst    A copy of nstrSrc.
   * \param[in,out] ppcharDst  In-value must be NULL. Out-value is an allocated C-sytle list of std::string
   *                           containing the same values as ppcharSrc.
   *
   * \warning Memory management:
   * - before to call: To avoid memory leak, make sure to free the memory
   *   (if any) that ppcharDst points to (you could use ppcharFree).
   * - after to call: Once you have finished with ppcharDst, make sure to deallocate
   *   its memory using the ppcharFree function.
   */
  void ppcharCopy(int  nstrSrc, char ** ppcharSrc,
                  int &nstrDst, char **&ppcharDst);

  /*!
   * \brief Find index of a std::string in a C-style list of strings.
   *
   * \param[in] nstr   Number of strings.
   * \param[in] ppchar List of strings.
   * \param[in] str    Searched std::string.
   *
   * Returned value is the index of the std::string, or -1 if the std::string
   * is not in the list.
   */
  int ppcharIndex(int nstr, const char **ppchar, const char * str);

  /*!
   * \brief Test if a std::string contains a substring
   *
   * \param[in] str     The std::string.
   * \param[in] substr  The searched substring.
   *
   * Returned value is true if str contains substr, false else.
   */
  bool string_contains_substring(const std::string& str, const std::string& substr);

  /*!
   * \brief Test if a std::vector of strings contains a std::string.
   *
   * \param[in] vector_str   The std::vector of strings.
   * \param[in] str          The searched std::string.
   *
   * Returned value is true if vector_str contains str, false else.
   */
  bool vector_string_contains(const std::vector<std::string>& vector_str, const std::string& str);
}
#endif
