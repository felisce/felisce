//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"

namespace felisce {
namespace filesystemUtil {

bool directoryExists(const char *path)
{
    const std::string string_path(path);
    return std::filesystem::exists(string_path);
}

/***********************************************************************************/
/***********************************************************************************/

bool fileExists (const std::string& name)
{
  return std::filesystem::exists(name);
}

/***********************************************************************************/
/***********************************************************************************/

void copyFileSomewhereElse(
  const std::string& inFileName,
  const std::string& outFileName
  )
{
  std::filesystem::copy_file(inFileName, outFileName, std::filesystem::copy_options::update_existing);
}

}
}
