//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <string>

// External includes

// Project includes

#pragma once

namespace felisce {

// felisce Minor and Major
#ifndef FELISCE_MAJOR_VERSION
#define FELISCE_MAJOR_VERSION 2
#endif

#ifndef FELISCE_MINOR_VERSION
#define FELISCE_MINOR_VERSION 0
#endif

#define FELISCE_VERSION_EQ(MAJOR,MINOR) \
((FELISCE_MAJOR_VERSION == (MAJOR)) && (FELISCE_MINOR_VERSION == (MINOR)))

#define FELISCE_VERSION_ FELISCE_VERSION_EQ

#define FELISCE_VERSION_LT(MAJOR,MINOR)                                  \
  (FELISCE_MAJOR_VERSION < (MAJOR) || (FELISCE_MAJOR_VERSION == (MAJOR) &&   \
                                   (FELISCE_MINOR_VERSION < (MINOR) )))

#define FELISCE_VERSION_LE(MAJOR,MINOR) \
  (FELISCE_VERSION_LT(MAJOR,MINOR) || FELISCE_VERSION_EQ(MAJOR,MINOR))

#define FELISCE_VERSION_GT(MAJOR,MINOR) (0 == FELISCE_VERSION_LE(MAJOR,MINOR))

#define FELISCE_VERSION_GE(MAJOR,MINOR) (0 == FELISCE_VERSION_LT(MAJOR,MINOR))

constexpr int GetMajorVersion() {
    return FELISCE_MAJOR_VERSION;
}

constexpr int GetMinorVersion() {
    return FELISCE_MINOR_VERSION;
}

} // namespace felisce
