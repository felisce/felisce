//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _FELISCE_ERROR_HPP
#define _FELISCE_ERROR_HPP

// System includes
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <string>
#include <iostream>
#include <sstream>
#include <exception>

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"

namespace felisce 
{

#ifdef FELISCE_WITH_EXCEPTION // TODO D.C.
  class FelisceError: public std::exception {
  public:
    FelisceError();
    FelisceError(const char * str,const char * file,int line);
    virtual ~FelisceError() throw() {}
    virtual const char* what() const throw();
  private:
    char *str_;
  };

  class FelisceAssertionError: public std::exception {
  public:
    FelisceAssertionError();
    FelisceAssertionError(const char * str,const char * file,int line);
    virtual ~FelisceAssertionError() throw() {}
    virtual const char* what() const throw();
  private:
    char *str_;
  };

  class FelisceEqualAssertionError: public std::exception {
  public:
    FelisceEqualAssertionError();
    FelisceEqualAssertionError(const char * str,const char * file,int line);
    virtual ~FelisceEqualAssertionError() throw() {}
    virtual const char* what() const throw();
  private:
    char *str_;
  };
#endif

  void felisce_error(const char* str,const char* file,int line);
  void felisce_error(std::string str,const char* file,int line);
  void felisce_warning(const char* str,const char* file,int line);
  void felisce_warning(std::string str,const char* file,int line);
  void felisce_warning_if_no_directory(const char* dirName, const std::string dirPath,
                                       const char* file, int line);
  void felisce_info(const char* str,const char* file,int line);
  void felisce_assert(const char* str,const char* file,int line);
  void felisce_assert(std::string str,const char* file,int line);
  void felisce_assert_equal(const char* expr_a, const char* expr_b, int a, int b, const char* file, int line);
  void felisce_assert_lt(const char* expr_a, const char* expr_b, int a, int b, const char* file, int line);

#ifdef NDEBUG
  #define FEL_ASSERT(test)
  #define FEL_ASSERT_EQUAL(a,b)
  #define FEL_ASSERT_LT(a,b)
#else
  #define FEL_ASSERT(test)  if (!(test)) felisce_assert(#test,__FILE__,__LINE__);
  // Beware: a and b are expected to be integers in the two macros below!
  #define FEL_ASSERT_EQUAL(a,b)  felisce_assert_equal(#a,#b,a,b,__FILE__,__LINE__);
  #define FEL_ASSERT_LT(a,b)  felisce_assert_lt(#a,#b,a,b,__FILE__,__LINE__);
#endif

#undef VERBOSE_DEBUG

#ifdef VERBOSE_DEBUG
  #define FEL_DEBUG(text) std::cout << "DEBUG: " << text;
#else
  #define FEL_DEBUG(text)
#endif

#define FEL_WARNING(msg)  felisce::felisce_warning(msg,__FILE__,__LINE__);
#define FEL_WARNING_IF_NO_DIRECTORY(path) felisce::felisce_warning_if_no_directory(#path,path,__FILE__,__LINE__);
#define FEL_INFO(msg)  felisce::felisce_info(msg,__FILE__,__LINE__);

#define FEL_ERROR(msg)  felisce::felisce_error(msg,__FILE__,__LINE__);
#define FEL_CHECK(test,msg) if (!(test)){std::cout << msg << std::endl; felisce::felisce_assert(#test,__FILE__,__LINE__);};

}
#endif
