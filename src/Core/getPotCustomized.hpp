//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

/*!
 * \file getPotCustomized.hpp
 * \authors David Froger
 * \date 06/02/2011
 *
 * \brief Extend the file/command line parser Getpot capabilities to fit our needs.
 *
 * The main function is the overloaded get functions, that have some advantages
 * to the base GetPot equivalent:
 *
 * - Both the command line and the data file are read with only one call to
 * the get functions. For each data, a default value is first supply via a
 * get function argument. If the data is present in the data file, this value
 * override the default one. Finally, the command line will orrivide the current value
 * if it specify this datum.
 *
 * - If the user specify a wrong option in the data file or on the command
 * line, the error has to be detected. The list of the correct datum name is
 * automatically constructed during the call to the get functions.
 *
 * - A "help" std::string is automatically constructed during the call to the get
 * functions, intended to be displayed using --help on the command line.
 *
 * - A boolean type is added.
 *
 * - Reading enum is added.
 *
 * - Vector support is added for std::string, int, double and bool.
 *
 * - Recquire function that raise errors if name not found.
 *
 * - Combined with the moveToSection function, it avoid to have to repeat the
 *    section name for each datum.
 *
 * \note: Overloading is choosen for the get function. A template+specialization
 * approach is not ideal, because bool and std::string are special cases.
 *
 * \note result values are named: result_SWIG_TYPED_OUT in the header only. It is
 * used to provide additionnal informations to SWIG so it can generate
 * particular Python wrapping on this argument. This should not be change to
 * not break the Python wrapping.
 *
 * \warning: Data value std::string (for std::vector only) are casted to int or double
 * using boost when they are read from the command line, but using another way
 * (in GetPot) when read in the data file. Boost seams to be better that GetPort to
 * detect cast errors.
 *
 * \todo: remove try/catch (or be sure that it does not alter global code
 * performance).
 *
 * \todo: have consistent conversion std::string->int and std::string->double. For scalar,
 * getpot is used, for std::vector, boost is used.
 *
 */

/* TODO: what does this documentation refer to?
 * Read an enum in a data file
 *
 *   Enumerations must start from zero, and be always incremented by one.
 *
 *   \param[in]   name         Name of to parameter to read in data file.
 *   \param[in]   numEnum      Number of variable in the enum.
 *   \param[in]   strEnum      Mapping between the enum and its std::string representation
 *   \param[out]  result       Enum value, converted from the read enum
 *                             std::string representation
 *
 * Example:
 * -------
 *
 * datafile:
 *   [section]
 *   name = "foo"
 *
 * variables:
 *   enum FooBarBaz {foo,baz,bar}
 *   const char* strEnum[3] = {"foo","bar","baz"}
 *   FoobarBaz fbb;
 *
 * moveToSection("section");
 * require(fbb,"name",3,strEnum) // fbb is foo
 */

#ifndef _GET_POT_CUSTOMIZED_HPP
#define _GET_POT_CUSTOMIZED_HPP

// System includes
#include <vector>
#include <unordered_map>
#include <set>
#include <string>
#include <iostream>

// External includes
#include "Core/NoThirdPartyWarning/GetPot/getpot.hpp" 

// Project includes
#include "Core/felisce.hpp"
#include "Core/util_string.hpp"

namespace felisce 
{
// TODO: Move this class to another file
class ElementFieldDynamicValue {

public:
  // components managed
  static std::size_t numAllComp;
  static Component allComp[3];

  // init,delete functions
  ElementFieldDynamicValue();
  ElementFieldDynamicValue(const char* name);
  ~ElementFieldDynamicValue();
  void initialize(const char* name);

  // std::set functions
  void setElementFieldType(ElementFieldType elementFieldType);
  void setTypeValueOfElementField(TypeValueOfElementField typeValueOfElementField, Component Comp);
  void setNumComp(int numComp);
  void setConstant(double constant, Component Comp);
  void setFileName(std::string fileName, Component Comp);
  void setFunctionName(std::string FunctionName, Component Comp);
  void setCallbackXYZT(const CallbackXYZT &callbackXYZT, Component Comp);

  // access functions
  std::string name();
  ElementFieldType elementFieldType();
  TypeValueOfElementField typeValueOfElementField(Component Comp);
  int numComp() const;
  double constant(Component Comp);
  std::string fileName(Component Comp);
  std::string functionName(Component Comp);
  CallbackXYZT callbackXYZT(Component Comp);

  // others functions
  void display();

private:
  int numCompMax_;
  std::string name_;
  ElementFieldType elementFieldType_;
  int numComp_;
  TypeValueOfElementField typeValueOfElementField_[3];
  double constant_[3];
  std::string fileName_[3];
  std::string functionName_[3];
  CallbackXYZT callbackXYZT_[3];
  bool isReady_[3];

  int CompToIndex_(Component Comp);
};

class GetPotCustomized: public GetPot {

public:
  GetPotCustomized();
  ~GetPotCustomized();

  void initialize(int argc,char **argv,std::string filename);

  /*! \brief Get scalar value, provide a default one if not present in data file
    */
  void  get(std::string& result_SWIG_TYPED_OUT, const char *name, const char * defaultValue, const char *help=nullptr);
  void  get(int   & result_SWIG_TYPED_OUT, const char *name, int    defaultValue, const char *help=nullptr);
  void  get(double& result_SWIG_TYPED_OUT, const char *name, double defaultValue, const char *help=nullptr);
  void  get(bool  & result_SWIG_TYPED_OUT, const char *name, bool   defaultValue, const char *help=nullptr);

  template <class Enum>
  void get(Enum &result_SWIG_TYPED_OUT, const char *name, Enum defaultValue,
            int numEnum,  const char **strEnum, const char *help=nullptr);

  /*! \brief Get std::vector values, provide a default one if not present in data file
    */
  void  get(std::vector<std::string>& result_SWIG_TYPED_OUT, const char *name, const char *defaultValue, const char *help=nullptr);
  void  get(std::vector<int>   & result_SWIG_TYPED_OUT, const char *name, const char *defaultValue, const char *help=nullptr);
  void  get(std::vector<double>& result_SWIG_TYPED_OUT, const char *name, const char *defaultValue, const char *help=nullptr);
  void  get(std::vector<bool>  & result_SWIG_TYPED_OUT, const char *name, const char *defaultValue, const char *help=nullptr);

  template <class Enum>
  void get(std::vector<Enum> &result_SWIG_TYPED_OUT, const char *name, const char *defaultValue,
            int numEnum,  const char **strEnum, const char *help=nullptr);

  /*! \brief Get scalar value, but this is an error if not present in data file.
    *
    * \note GetPot provide only function to read data and use default value if
    * data si no present. So we fist read the data as a std::string and check the
    * std::string is not "", then we really read the data, providing a default value
    * that will be overriden.
    */
  void require(std::string& result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);
  void require(int   & result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);
  void require(double& result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);
  void require(bool  & result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);

  template <class Enum>
  void require(Enum &result_SWIG_TYPED_OUT, const char *name,
                int numEnum,  const char **strEnum, const char *help=nullptr);

  void require(std::vector<std::string>& result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);
  void require(std::vector<int>   & result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);
  void require(std::vector<double>& result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);
  void require(std::vector<bool>  & result_SWIG_TYPED_OUT, const char *name, const char *help=nullptr);

  template <class Enum>
  void require(std::vector<Enum> &result_SWIG_TYPED_OUT, const char *name,
                int numEnum,  const char **strEnum, const char *help=nullptr);

  //ElementField
  std::set<std::string> getElementFieldSection();
  ElementFieldDynamicValue* getElementFieldDynamicValue(std::string section);
  std::string elementFieldNameFromSection(std::string section);

  /*! \brief Test whether an data is defined in the file or not
    *
    */
  bool exists(const char *name);

  /*! \brief call FEL_ERROR in name is not defined in current fileanme/section.
    *
    */
  void raiseErrorIfNotExists(const char *name);

  /*! \brief call FEL_ERROR if name is defined in current fileanme/section.
    *
    */
  void raiseErrorIfExists(const char *name);

  /*!
    * \brief Move to a data file section to read data in.
    */
  void moveToSection(std::string section);

  /*!
    * \brief Check that the datum name supplied by the user in the data file and
    * on the command line are correct.
    *
    * The is done using the lists of correct datum names, m_identifiedVariablesCl
    * and m_identifiedVariablesFile
    */
  void checkUndefinedVariable();

  /*!
    * \brief Check that, in the current section, several std::vector datum  have the same
    * length.
    */
  void assertSameLength(int count, ...);

  /*!
    * \brief Return informations about Felisce datum, intented to be printed with
    * --help, like a manpage.
    */
  std::unordered_map<std::string, std::vector<std::string> > getHelp();

  void addToIdentifiedVariables(const char *name);

  /*!
    * \brief validate a parameter in data file can be converted to int.
    *
    * GetPot provide poor conversion checking, e.g. "foo" can be converted to 0.
    * We use std::stod and std::stoi to provide more reliable data file reading.
    */
  void validateIntParam(const char* name);
  void validateDoubleParam(const char* name);

private:
  std::size_t getLength(const char *name);
  std::string m_paramFromFile(const char *name);
  std::string m_paramFromCl  (const char *name);
  template<class T>
  void m_addToHelp(const char *name, const char *type, T defaultValue, const char *help);

  GetPot* m_getpotFile;
  GetPot* m_getpotCl;

  std::string m_section;
  std::string m_filename;
  std::string prefixOptionFelisce;

  std::vector<std::string> m_identifiedVariablesCl;
  std::vector<std::string> m_identifiedVariablesFile;

  std::unordered_map<std::string, std::vector<std::string> > m_help;
};

/* =========================================================================
  * template implementation
  *========================================================================*/

// get Enum

template <class Enum>
void GetPotCustomized::get(Enum &result_SWIG_TYPED_OUT, const char *name, Enum defaultValue,
                            int numEnum,  const char **strEnum, const char *help) {

  // check that defaultValue is correct
  if ((int) defaultValue >= numEnum) {
    std::ostringstream msg;
    msg << "Bad Data file  '" << m_filename << "': "
        << "In section '" << m_section << "', "
        << "(int) defaultValue = " << int (defaultValue)
        << " is greater or equal than numEnum = " << numEnum;
    FEL_ERROR(msg.str().c_str());
  }

  const char *defaultValueStr = strEnum[(int) defaultValue];

  // read name as a std::string
  std::string resultStr;

  std::string paramFromFile = m_paramFromFile(name);
  std::string paramFromCl = m_paramFromCl(name);

  resultStr = (*m_getpotFile)(paramFromFile.c_str() , defaultValueStr);
  resultStr = (*m_getpotCl)  (paramFromCl.c_str(), resultStr.c_str());

  // convert std::string to its value
  int buf = ppcharIndex(numEnum,strEnum,resultStr.c_str());

  if (buf == -1) {
    std::cout << "INFO: In section '" << m_section
              << "' valid values for '" << name
              << "' are:" << std::endl;

    for (int istr=0 ; istr<numEnum ; istr++)
      std::cout << "    " << strEnum[istr] << std::endl;

    std::ostringstream msg;
    msg << "Bad data file '" << m_filename << "': "
        << "In section '" << m_section << "', "
        << "enum '" << name << "' "
        << "has wrong value '" << resultStr << "'.";
    FEL_ERROR(msg.str().c_str());
  }

  result_SWIG_TYPED_OUT = static_cast<Enum>(buf);

  if (help != nullptr) m_addToHelp(name,"ENUM",defaultValueStr,help);
}

// get std::vector<Enum>

template <class Enum>
void GetPotCustomized::get(std::vector<Enum> &result_SWIG_TYPED_OUT, const char *name, const char *defaultValue,
                            int numEnum,  const char **strEnum, const char *help) {
  std::string onestring;

  std::string paramFromFile = m_paramFromFile(name);
  std::string paramFromCl = m_paramFromCl(name);

  onestring = (*m_getpotFile)( paramFromFile.c_str(), defaultValue);
  onestring = (*m_getpotCl)  ( paramFromCl.c_str(), onestring.c_str());

  // split "Comp1 Comp2 Comp3" into {"Comp1", "Comp2", "Comp3"}
  std::vector<std::string> vecstring;
  split(onestring,vecstring," \n\t");

  // convert std::string element to Enum Element
  Enum item;
  for ( unsigned int i = 0; i < vecstring.size(); i++) {
    item = (Enum) ppcharIndex(numEnum,strEnum,vecstring[i].c_str());

    if ((int) item == -1) {
      std::cout << "INFO: In section '" << m_section
                << "' valid values for '" << name
                << "' are:" << std::endl;

      for (int istr=0 ; istr<numEnum ; istr++)
        std::cout << "    " << strEnum[istr] << std::endl;

      std::ostringstream msg;
      msg << "Bad data file '" << m_filename << "': "
          << "In section '" << m_section << "', "
          << "enum '" << name << "' "
          << "has wrong value '" << vecstring[i].c_str() << "'.";
      FEL_ERROR(msg.str().c_str());
    }

    result_SWIG_TYPED_OUT.push_back(item);
  }

  if (help != nullptr) m_addToHelp(name,"ENUM VECTOR",defaultValue,help);
}

// require enum

template <class Enum>
void GetPotCustomized::require(Enum &result_SWIG_TYPED_OUT, const char *name,
                                int numEnum,  const char **strEnum, const char *help) {
  raiseErrorIfNotExists(name);
  get(result_SWIG_TYPED_OUT,name,(Enum) 0,numEnum,strEnum,help);
}

// require std::vector<enum>

template <class Enum>
void GetPotCustomized::require(std::vector<Enum> &result_SWIG_TYPED_OUT, const char *name,
                                int numEnum, const char **strEnum, const char *help) {
  raiseErrorIfNotExists(name);
  get(result_SWIG_TYPED_OUT,name,"",numEnum,strEnum,help);
}


template <class T>
void GetPotCustomized::m_addToHelp(const char *name, const char *type, T defaultValue, const char *help) {

  std::ostringstream stream;

  stream << "    --felisce-" << m_section << "-" <<  name << "=" << type << std::endl
          << "         DEFAULT: " << defaultValue << std::endl
          << "         " << help << std::endl
          ;

  m_help[m_section].push_back( stream.str() );
}

}

#endif
