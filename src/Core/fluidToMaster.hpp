#ifndef FLUID_TO_MASTER_HPP
#define FLUID_TO_MASTER_HPP

#include <vector>
#include <iostream>

// Project-specific includes
#include "Core/configure.hpp"

#if defined(FELISCE_WITH_PVM) || defined(FELISCE_WITH_ZMQ)

#ifdef FELISCE_WITH_PVM
#include <pvm3.h>
#endif

#ifdef FELISCE_WITH_ZMQ
// #include <zmq.h>
#include <zmq.hpp> // zmq.h is includes in this header (ubuntu last LTS)
#endif

namespace felisce
{

  class FluidToMaster {
    
  public:
    typedef std::vector<double> Vector;

#ifdef FELISCE_WITH_PVM
    FluidToMaster(int fsiStatus,
		  int nbCoor,
		  std::vector<int>&  match,  
		  std::vector<double>& vec1_master,   
		  std::vector<double>& vec2_master,
		  std::vector<double>& lumpMmaster,
		  std::vector<double>& force_fluid,
		  std::vector<double>& velo_fluid);
#endif
#ifdef FELISCE_WITH_ZMQ
    FluidToMaster(int fsiStatus,
                  int nbCoor,
                  std::vector<int>&  match,
                  std::vector<double>& vec1_master,
                  std::vector<double>& vec2_master,
                  std::vector<double>& lumpMmaster,
                  std::vector<double>& force_fluid,
                  std::vector<double>& velo_fluid,
                  std::string socketTransport,
                  std::string socketAddress,
                  std::string socketPort);
#endif
    
    void setMaster();
      
    void sdToMasterFSI();  
    void sdInterfCoorToMasterFSI();
    void rvFromMasterFSI(int msg);
    
    int status();
    int hasLumpedMass();
    int nbNodes(); 
    double timeStep();

    /**
    * @brief encapsuled function for recv function of zmq to avoid warning
    *        included memcpy of the data.
    *
    */
#ifdef FELISCE_WITH_ZMQ
    void recvZEROMQ_default(zmq::message_t &msg, void * data, std::size_t sizeData);
#endif

  private:
    int m_nbCoor;
    int m_nbNodeFSI;
    int m_ntdlfConf;
#ifdef FELISCE_WITH_PVM
    int m_masterID;
#endif
#ifdef FELISCE_WITH_ZMQ
    std::string m_socketTransport; // e.g. tcp
    std::string m_socketAddress; // e.g. 127.0.0.1 (localhost)
    std::string m_socketPort; // e.g. 5555
    zmq::context_t m_zmq_context;
    zmq::socket_t m_socket;
#endif
    int m_fsiStatus;
    std::vector<int>&  m_match;
    std::vector<double> m_vec1_master;
    std::vector<double> m_vec2_master;
    std::vector<double>& m_disp_master;
    std::vector<double>& m_velo_master;
    std::vector<double>& m_lumpMmaster;
    std::vector<double>& m_force_fluid;
    std::vector<double>& m_velo_fluid;
   
    double m_dt;  
    int m_hasLumpedMass;
    int m_dimLumpMass;
    
  };
  
}

#endif
#endif
