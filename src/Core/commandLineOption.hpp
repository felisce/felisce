//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    David Froger
//

#ifndef COMMANDLINEOPTION_HPP
#define COMMANDLINEOPTION_HPP

// System includes
#include <iostream>
#include <vector>

// External includes
#include "GetPot"

// Project includes
#include "Core/util_string.hpp"
#include "Core/felisce.hpp"

/*! \namespace felisce */
namespace felisce 
{
/**
 * \brief Preprocess command line options to send it to the appropriate library.
 *
 * It is necessary to split the command line options std::string list into separate lists,
 * that are each passed to the appropriate function (Petsc initialisation, Felisce
 * data parsing function, ...).
 *
 * Indeed, a Petsc command line option passed the Felisce data parser would
 * be detected as a wrong option and cause a error.
 *
 * Similary, passing Felisce data option to the Petsc intialization function
 * would make Petsc print warning.
 *
 * \authors David Froger
 * \date 07/02/2012
 */
class CommandLineOption 
{
public:

  CommandLineOption();

  CommandLineOption(const int argc, const char** argv, const std::size_t instanceIndex = 0);

  FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor is deprecated, please use constructor with const argc and argv")
  CommandLineOption(int argc, char** argv);

  CommandLineOption(const CommandLineOption& opt);

  ~CommandLineOption();

  //-------------------------------------------------------------------------
  // Main functions
  //-------------------------------------------------------------------------
  /*!
    * \brief Preprocess command line options to send it to the appropriate library.
    *
    * \param[in]  argc        Number of command line options.
    * \param[in]  argv        Command line options strings.
    *
    * Initialize class attribute:
    *   m_argcSpecial, m_argvSpecial,
    *   m_argcFelisce, m_argvFelisce,
    *   m_argcPetsc,   m_argvPetsc,
    *   m_dataFileName,
    *   m_printHelp.
  */
  void initialize(const int argc, const char **argv);

  //-------------------------------------------------------------------------
  // Access functions
  //-------------------------------------------------------------------------
  // Instance Index
  std::size_t& instanceIndex() {
    return mInstanceIndex;
  }

  // data file
  std::string& dataFileName() {
    return m_dataFileName;
  }

  // help printing
  bool printHelp() const {
    return m_printHelp;
  }

  // felisce opt
  int& argcFelisce() {
    return m_argcFelisce;
  }

  const int& argcFelisce() const {
    return m_argcFelisce;
  }

  char**& argvFelisce() {
    return m_argvFelisce;
  }

  char** argvFelisce() const {
    return m_argvFelisce;
  }

  void optFelisce(int &argcFelisce, char**& argvFelisce);

  // petsc opt
  int& argcPetsc() {
    return m_argcPetsc;
  }
  char**& argvPetsc()       {
    return m_argvPetsc;
  }
  void optPetsc(int &argcPetsc, char **&argvPetsc);

  /*!
    * \brief Transfert options from argv to argvSpecial.
    *
    * For the moment, only --help, --file and -f are special options.
    *
    * \return True if a option has been transfered.
    */
  bool transferSpecialOption(
    std::vector<std::string>& argvec,
    std::vector<std::string>& argvecSpecial
    );

  /*!
    * \brief Transfert options from argv to argvFelisce.
    *
    * Felisce data options have the syntax: --felisce-section-variable=value
    *
    * \return True if a option has been transfered.
    */
  bool transferFelisceOption(
    std::vector<std::string>& argvec,
    std::vector<std::string>& argvecFelisce);

  /*!
    * \brief Transfert options from argv to argvFelisce.
    *
    * For the moment, we consider that every options that is not special nor
    * a Felisce data is for Petsc.
    *
    * In the future, if more libraries need to have options passed, we may want to
    * to have a more constraining method (for example, like the g++ -Wl, option):
    *     -P,--petsc-an-options=value,--petsc-another-options=value,petsc-flag
    * instead of:
    *     --petsc-an-option=value --petsc-another-option=value -petsc-flag
    *
    * Both of them can be anyway preprocessed to create:
    *      argvPetsc = ['--petsc-an-option=value',
    *                   '--petsc-an-otheroption=value',
    *                   'petsc-flag']
    * and to pass argvPetsc to the Petsc intialization function.
    *
    * \return True if a option has been transfered.
    */
  bool transferPetscOption(
    std::vector<std::string>& argvec,
    std::vector<std::string>& argvecPetsc);

  /*!
    * \brief Parses special options (--help, --file, -f).
    */
  void parseSpecialOptions(
    int argcSpecial, 
    char** argvSpecial,
    bool& printHelp, 
    std::string& dataFileName);


  void print(int verbose = 0, std::ostream& outstr = std::cout) const;

private:

  std::size_t mInstanceIndex = 0; // The current instance index

  // data file
  std::string m_dataFileName = "";

  // help printing
  bool m_printHelp = false;

  // special opt
  int m_argcSpecial = 0;
  char** m_argvSpecial = nullptr;

  // felisce opt
  int m_argcFelisce = 0;
  char** m_argvFelisce = nullptr;

  // petsc opt
  int m_argcPetsc = 0;
  char** m_argvPetsc = nullptr;
};
}

#endif
