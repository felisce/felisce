//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:     Vicente Mataix Ferrandiz
//

#ifndef _CHRONO_HPP_INCLUDE
#define _CHRONO_HPP_INCLUDE

// System includes
#include <chrono>
#include <iomanip>
#include <thread>
#include <fstream>
#include <map>
#include <stdint.h>
// Windows
#ifdef _WIN32
#include <intrin.h>
#endif

// External includes
#include <mpi.h>
#include <tabulate.hpp>

// Project includes
#include "Core/felisce_error.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class ChronoInstance
 * @details It is used to measure the time between two events.
 * @brief Minimal class to manage chrono.
 */
class ChronoInstance
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of ChronoInstance
  FELISCE_CLASS_POINTER_DEFINITION(ChronoInstance);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Constructor
  ChronoInstance() = default;

  /// Destructor
  ~ChronoInstance() = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  // Windows
#if defined(_WIN32)
  static uint64_t rdtsc(){
      return __rdtsc();
  }
#elif defined(__ARM_ARCH)
  // Support for Apple Silicon (arm64), following https://github.com/google/benchmark/blob/main/src/cycleclock.h
  static uint64_t rdtsc(){
    // System timer of ARMv8 runs at a different frequency than the CPU's.
    // The frequency is fixed, typically in the range 1-50MHz.  It can be
    // read at CNTFRQ special register.  We assume the OS has set up
    // the virtual timer properly.
    int64_t virtual_timer_value;
    asm volatile("mrs %0, cntvct_el0" : "=r"(virtual_timer_value));
    return virtual_timer_value;
  }
#else // Linux/GCC
  static uint64_t rdtsc(){
      unsigned int lo,hi;
      __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
      return ((uint64_t)hi << 32) | lo;
  }
#endif

  /**
   * @brief Start the chrono
   */
  std::chrono::system_clock::time_point& start() { 
    m_start_cycles = rdtsc();
    m_start = std::chrono::system_clock::now(); 
    m_started = true; 
    return m_start; 
  }
  
  /**
   * @brief Stop the chrono
   */
  std::chrono::system_clock::time_point& stop() { 
    m_end_cycles = rdtsc();
    m_stop = std::chrono::system_clock::now();    
    m_stopped = true; 
    m_dt += this->diff();
    m_cn++;
    return m_stop; 
  }
  
  /**
   * @brief Returns if the chrono is started
   * @return True if the chrono is started, false otherwise
   */
  bool isStarted() { 
    return m_started; 
  }

  /**
   * @brief Returns if the chrono is started
   * @return True if the chrono is started, false otherwise
   */
  bool isStarted() const{ 
    return m_started; 
  }
  
  /**
   * @brief Returns if the chrono is stopped
   * @return True if the chrono is stopped, false otherwise
   */
  bool isStopped() { 
    return m_stopped; 
  }

  /**
   * @brief Returns if the chrono is stopped
   * @return True if the chrono is stopped, false otherwise
   */
  bool isStopped() const { return m_stopped; }
  
  /**
   * @brief Resets the chrono instance
   */
  void reset() 
  {
    this->start();
    m_dt = 0;
    m_cn = 0;
  }

  /**
   * @brief Returns the chrono duration accumulated
   * @return The chrono duration accumulated
   */
  double diff_cumul() const 
  {
    return std::max(m_dt, 0.);
  }

  /**
   * @brief Returns the number of calls to chrono
   * @return The number of calls to chrono
   */
  uint64_t num_call() const 
  {
    return m_cn;
  }

  /**
   * @brief Returns the chrono duration
   * @return The chrono duration
   */
  double diff() {
    if (isStarted() && isStopped()) { 
      return std::chrono::duration_cast<std::chrono::duration<double>>(m_stop - m_start).count(); 
    } else { 
      std::cerr << "ChronoInstance:: start() and stop() must be called first." << std::endl; 
      return 0.0;
    } 
  }
  
  /**
   * @brief Returns the chrono duration
   * @return The chrono duration
   */
  double diff() const {
    if (isStarted() && isStopped()) { 
      return std::chrono::duration_cast<std::chrono::duration<double>>(m_stop - m_start).count(); 
    } else { 
      std::cerr << "ChronoInstance:: start() and stop() must be called first." << std::endl; 
      return 0.0;
    } 
  }

  /**
   * @brief Returns the chrono duration in CPU cycles
   * @return The chrono duration
   */
  uint64_t diffCycles() {
    if (isStarted() && isStopped()) { 
      return m_end_cycles - m_start_cycles;
    } else { 
      std::cerr << "ChronoInstance:: start() and stop() must be called first." << std::endl; 
      return 0.0;
    } 
  }
  
  /**
   * @brief Returns the chrono duration in CPU cycles
   * @return The chrono duration
   */
  uint64_t diffCycles() const {
    if (isStarted() && isStopped()) { 
      return m_end_cycles - m_start_cycles; 
    } else { 
      std::cerr << "ChronoInstance:: start() and stop() must be called first." << std::endl; 
      return 0.0;
    } 
  }

  /**
   * @brief Returns the start time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   * @return The start time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   */
  std::string startTime() { 
    if (isStarted()) {
      std::stringstream ss;
      const std::time_t tt = std::chrono::system_clock::to_time_t(m_start);
      ss << std::put_time(std::localtime(&tt), "%T") << std::flush;
      return ss.str();
    }
    return "Not started";
  }  

  /**
   * @brief Returns the start time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   * @return The start time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   */
  std::string startTime() const { 
    if (isStarted()) {
      std::stringstream ss;
      const std::time_t tt = std::chrono::system_clock::to_time_t(m_start);
      ss << std::put_time(std::localtime(&tt), "%T") << std::flush;
      return ss.str();
    }
    return "Not started";
  }

  /**
   * @brief Returns the stop time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   * @return The stop time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   */
  std::string stopTime() const { 
    if (isStopped()) {
      std::stringstream ss;
      const std::time_t tt = std::chrono::system_clock::to_time_t(m_stop);
      ss << std::put_time(std::localtime(&tt), "%T") << std::flush;
      return ss.str();
    }
    return "Not stopped";
  }

  /**
   * @brief Returns the stop time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   * @return The stop time in equivalent to "%H:%M:%S" (the ISO 8601 time format) in a string
   */
  std::string stopTime() { 
    if (isStopped()) {
      std::stringstream ss;
      const std::time_t tt = std::chrono::system_clock::to_time_t(m_stop);
      ss << std::put_time(std::localtime(&tt), "%T") << std::flush;
      return ss.str();
    }
    return "Not stopped";
  }

  /**
   * @brief Returns chrono as a file
   */
  void toFile() 
  {
    std::string resultDir = ".";    // Directory where to write the results
    MPI_Comm comm = MPI_COMM_WORLD; // MPI communicator
    int rankProc = -1;              // Rank of the process
    int numProc = -1;               // Number of processes

    MPI_Comm_rank(comm,&rankProc);
    MPI_Comm_size(comm,&numProc);

    // Process 0 gathers diff from all process 
    double my_diff = this->diff();
    double * allProcessDiff = nullptr;

    if (rankProc == 0)
      allProcessDiff = new double[numProc];

    MPI_Barrier(comm);
    MPI_Gather(&my_diff,1,MPI_DOUBLE, allProcessDiff,1,MPI_DOUBLE, 0,comm);
    MPI_Barrier(comm);

    // remaining of the code is done by only process 0 
    if (rankProc != 0)
      return;

    // write all process diff in a file 
    std::ostringstream filename;
    filename << resultDir << "/" << "chrono.txt";
    std::ofstream f;
    f.open(filename.str().c_str());

    if ( f.good() ) {
      f << "# seconds, one line per processor" << std::endl;
      for (int rankProc = 0; rankProc < numProc; ++rankProc)
        f << allProcessDiff[rankProc] << std::endl;
    } else {
      std::ostringstream msg;
      msg << "dumpAllProcess: can not write in file " << filename.str() << std::endl;
      FEL_WARNING(msg.str().c_str());
    }

    delete[] allProcessDiff;
  }

  /**
   * @brief Returns chrono as a file max
   */
  void toFileMax() 
  {
    std::string resultDir = ".";    // Directory where to write the results
    MPI_Comm comm = MPI_COMM_WORLD; // MPI communicator
    int rankProc = -1;              // Rank of the process
    int numProc = -1;               // Number of processes

    MPI_Comm_rank(comm,&rankProc);
    MPI_Comm_size(comm,&numProc);

    // Process 0 gathers diff from all process 
    double my_diff = this->diff();
    double maxDiff(0);

    MPI_Reduce(&my_diff, &maxDiff, 1, MPI_DOUBLE, MPI_MAX, 0, comm);

    // rest of the code is done by only process 0 
    if (rankProc != 0)
      return;

    // write all process diff in a file 
    std::ostringstream filename;
    filename << resultDir << "/" << "chrono.txt";
    std::ofstream f;
    f.open(filename.str().c_str(), std::ios::app);

    if ( f.good() ) {
      f << maxDiff << std::endl;
    } else {
      std::ostringstream msg;
      msg << "dumpAllProcess: can not write in file " << filename.str() << std::endl;
      FEL_WARNING(msg.str().c_str());
    }
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  uint64_t m_start_cycles = 0;                   /// The starting point in cycles
  std::chrono::system_clock::time_point m_start; /// The starting point of the chrono
  bool m_started = false;                        /// True if the chrono is started

  uint64_t m_end_cycles = 0;                     /// The stopping point in cycles
  std::chrono::system_clock::time_point m_stop;  /// The stopping point of the chrono
  bool m_stopped = false;                        /// True if the chrono is stopped

  double m_dt = 0.0;                             /// Time increment

  uint64_t m_cn = 0;                             /// Number of call

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

/**
 * @class Chrono
 * @brief A chrono general instance class
 * @details This class is a general chrono instance class. It stores all times in several chronos instances. Generates a table with all the information.
 */
class Chrono
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Chrono
  FELISCE_CLASS_POINTER_DEFINITION(Chrono);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Constructor
  Chrono(const bool active);

  /// Constructor with additional information
  Chrono(const bool active, const std::string resultDir, MPI_Comm comm = MPI_COMM_WORLD);

  /// Destructor
  ~Chrono() = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Sleep the current thread for a given time
   * @param Time Time to be elapsed
   */
  static void Sleep(const int TimeToSleep);

  /**
   * @brief Start the chronometer
   * @param rLabel The label of the chrono
   * @param SubLabel The sublabel of the chrono (integer)
   */
  void Start(
    const std::string& rLabel, 
    const int SubLabel = 0
    );

  /**
   * @brief Stop the chronometer
   * @param rLabel The label of the chrono
   * @param SubLabel The sublabel of the chrono (integer)
   */
  void Stop(
    const std::string& rLabel, 
    const int SubLabel = 0
    );

  /**
   * @brief The difference of the chrono instance
   * @param rLabel The label of the chrono
   * @param SubLabel The sublabel of the chrono (integer)
   * @return time measured in last [start,stop] interval
   */
  double Diff(
    const std::string& rLabel, 
    const int SubLabel = 0
    );

  /**
   * @brief The difference of the chrono instance since its creation
   * @details Once is called, the chrono is stopped if it is not already stopped.
   * @param StopChrono If true, the chrono is stopped manually
   * @return Time measured since creation of instance
   */
  double getTotalTime(const bool StopChrono = false) { 
    if (!m_globalChrono.isStopped()) {
      m_globalChrono.stop(); 
    } else if (StopChrono) {
      m_globalChrono.stop(); 
    }
    return m_globalChrono.diff(); 
  }

  /**
   * @brief The difference of the chrono instance since its creation
   * @return Time measured since creation of instance
   */
  double getTotalTime() const { 
    return m_globalChrono.diff(); 
  }

  /** 
   * @brief Write chrono measures to screen
   */
  void ToScreen();

  /** 
   * @brief Write chrono measures to file <resultDir>/chrono_<chronoName>.md 
   */
  void ToFile();

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief Returns the result directory
   * @return The result directory
   */
  std::string& resultDir()
  {
    return m_resultDir;
  }

  /**
   * @brief The table where the chrono information is stored
   * @return The table where the chrono information is stored
   */
  tabulate::Table& table()
  {
    return m_table;
  }

  ///@}
  ///@name Inquiry
  ///@{

  inline const bool& State() const { return m_active; }
  inline       bool& State()       { return m_active; }

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  bool m_active;  /// Bool chrono is active or not
  bool m_isTableBuilt = false; /// Bool m_table is built or not

  std::string m_resultDir; /// Directory where to write the results
  MPI_Comm m_comm;         /// MPI communicator

  std::map<std::string, std::map<int, ChronoInstance>> m_chrono; /// Chrono map

  ChronoInstance m_globalChrono; /// Global chrono

  tabulate::Table m_table; /// Table containing all the chrono measures
  std::size_t m_rows = 0;  /// Number of rows in the table

  ///@}
  ///@name Private Operations
  ///@{

  /**
   * @brief This method fills the table
   */
  void fillTable();

  /**
   * @brief 
   * @return tabulate::Table 
   */
  tabulate::Table generateGlobalTable();

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif
