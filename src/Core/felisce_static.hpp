//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

/*!
 \file felisce_static.hpp
 \authors J-F. Gerbeau

 \brief File containing static variables of the FELiScE library

 This file contains the static variables. It \b must be included in the file containing the \c main()
 function, and \b only in this one.
 */

#ifndef _FELISCE_STATIC_HPP
#define _FELISCE_STATIC_HPP

// System includes

// External includes

// Project includes
#include "FiniteElement/refElement.hpp"

namespace felisce {
  //   std::unordered_map<DegreeOfFreedomSupport,std::string> RefElement::m_nameSupportDOF;
  //   std::unordered_map<DegreeOfFreedomType,std::string> RefElement::m_nameTypeDOF;
}
#endif

