//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    David Froger
//

// System includes

// External includes

// Project includes
#include "Core/commandLineOption.hpp"

namespace felisce 
{
CommandLineOption::CommandLineOption()
{
}

/***********************************************************************************/
/***********************************************************************************/

CommandLineOption::CommandLineOption(const int argc, const char** argv, const std::size_t instanceIndex)
  : mInstanceIndex(instanceIndex)
{
  // Proper initialization of the command line options
  initialize(argc,argv);
}

/***********************************************************************************/
/***********************************************************************************/

CommandLineOption::CommandLineOption(int argc, char** argv)
  : CommandLineOption(argc, const_cast<const char**>(argv))
{
}

/***********************************************************************************/
/***********************************************************************************/

CommandLineOption::CommandLineOption(const CommandLineOption& opt) 
{
  m_dataFileName = opt.m_dataFileName;
  m_printHelp = opt.m_printHelp;

  ppcharCopy(opt.m_argcSpecial, opt.m_argvSpecial, m_argcSpecial, m_argvSpecial);
  ppcharCopy(opt.m_argcFelisce, opt.m_argvFelisce, m_argcFelisce, m_argvFelisce);
  ppcharCopy(opt.m_argcPetsc,   opt.m_argvPetsc,   m_argcPetsc,   m_argvPetsc);
}

/***********************************************************************************/
/***********************************************************************************/

CommandLineOption::~CommandLineOption() 
{
  ppcharFree(m_argcSpecial, m_argvSpecial);
  ppcharFree(m_argcFelisce, m_argvFelisce);
  ppcharFree(m_argcPetsc  , m_argvPetsc);
}

//---------------------------------------------------------------------------
// Main functions
//---------------------------------------------------------------------------
void CommandLineOption::initialize(const int argc, const char **argv) 
{
  // For commodity, create C++-style list of strings instead
  // of C-style list of strings,
  std::vector<std::string> argvec,
          argvecFelisce,
          argvecPetsc,
          argvecSpecial;
  ppcharToVstring(argc,argv,argvec);

  // Everyone wants the executable name.
  argvecFelisce.push_back( argvec.front() );
  argvecPetsc  .push_back( argvec.front() );
  argvecSpecial.push_back( argvec.front() );
  argvec.erase( argvec.begin() );

  // Take and consumes options from the front of the argv list
  // until the list is empty, and put its to appropriate list.
  int checkInfiniteLoop = 0;
  while (argvec.size() > 0) {
    // Infinte loop
    if ( checkInfiniteLoop++ > 10000) {
      FEL_ERROR("Entered into an infinite loop")
    }

    // Distribute options
    if ( CommandLineOption::transferSpecialOption(argvec, argvecSpecial) ) continue;
    if ( CommandLineOption::transferFelisceOption(argvec, argvecFelisce) ) continue;
    if ( CommandLineOption::transferPetscOption  (argvec, argvecPetsc  ) ) continue;

    // Invalid option
    std::string msg = "Invalid command line option: ";
    msg += argvec[0];
    FEL_ERROR(msg);
  }

  // Convert back C++-style list of strings to the obligatory C-style list
  // of strings.
  ppcharFromVstring(argvecSpecial, m_argcSpecial, m_argvSpecial);
  ppcharFromVstring(argvecFelisce, m_argcFelisce, m_argvFelisce);
  ppcharFromVstring(argvecPetsc,   m_argcPetsc,   m_argvPetsc);

  parseSpecialOptions(m_argcSpecial,m_argvSpecial,m_printHelp,m_dataFileName);
}

//---------------------------------------------------------------------------
// Access functions
//---------------------------------------------------------------------------
void CommandLineOption::optFelisce(int& argcFel, char**& argvFel) 
{
  ppcharCopy(m_argcFelisce, m_argvFelisce, argcFel, argvFel);
}

void CommandLineOption::optPetsc(int& argcPet, char**& argvPet) 
{
  ppcharCopy(m_argcPetsc, m_argvPetsc, argcPet, argvPet);
}

//---------------------------------------------------------------------------
// Static functions
//---------------------------------------------------------------------------
bool CommandLineOption::transferSpecialOption(
  std::vector<std::string>& argvec,
  std::vector<std::string>& argvecSpecial
  ) 
{
  std::string arg = argvec.front();

  if (arg=="-f" || arg=="--file") {
    argvecSpecial.push_back(arg);
    argvec.erase( argvec.begin() );

    if (argvec.size() >= 1) {
      std::size_t counter_to_erase = 0;
      for (auto& r_input : argvec) {
        if (r_input.find("--help") != std::string::npos) {
          break;
        }
        ++counter_to_erase;
      }

      arg = *(argvec.begin() + this->instanceIndex());
      argvecSpecial.push_back(arg);
      for (std::size_t i = 0; i < counter_to_erase; ++i) {
        argvec.erase( argvec.begin() );
      }
      return true;
    } else {
      FEL_ERROR("Missing filename. Syntax: --file FILENAME or -f FILENAME");
      return false; // to avoid warnings
    }
  } else if (arg=="--help") {
    argvecSpecial.push_back(arg);
    argvec.erase( argvec.begin() );
    return true;
  } else {
    return false;
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool CommandLineOption::transferFelisceOption(
  std::vector<std::string>& argvec,
  std::vector<std::string>& argvecFelisce
  ) 
{
  std::string arg = argvec.front();

  std::string prefix = "--felisce-";

  // Option is for Felisce if it starts with --felisce-.
  if ( startswith(arg,prefix) ) {
    argvecFelisce.push_back(arg);
    argvec.erase( argvec.begin() );
    return true;
  } else {
    return false;
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool CommandLineOption::transferPetscOption(
  std::vector<std::string>& argvec,
  std::vector<std::string>& argvecPetsc
  ) 
{
  std::string arg = argvec.front();

  argvecPetsc.push_back(arg);
  argvec.erase( argvec.begin() );
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

void CommandLineOption::parseSpecialOptions(
  int argcSpecial, 
  char** argvSpecial,
  bool& printHelp, 
  std::string& dataFileName
  ) 
{
  GetPot parser(argcSpecial, argvSpecial);
  dataFileName = parser.follow("data", 2, "-f", "--file");
  printHelp = parser.search("--help");
}

/***********************************************************************************/
/***********************************************************************************/

void CommandLineOption::print(int verbose, std::ostream& outstr) const 
{
  if (verbose) {
    outstr << "Special option:" << std::endl;
    for (int i = 0; i < m_argcSpecial; ++i)
      outstr << m_argvSpecial[i] << std::endl;

    outstr << "\nFelisce option:" << std::endl;
    for (int i = 0; i < m_argcFelisce; ++i)
      outstr << m_argvFelisce[i] << std::endl;

    outstr << "\nPetsc option:" << std::endl;
    for (int i = 0; i < m_argcPetsc; ++i)
      outstr << m_argvPetsc[i] << std::endl;
  }
}
}
