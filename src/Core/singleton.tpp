//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Cedric Doucet
//

#ifndef SINGLETON_TPP
#define SINGLETON_TPP

#ifndef SINGLETON_HPP
#error Include singleton.hpp instead
#endif

// System includes
#include <cstddef>

// External includes

// Project includes

namespace felisce 
{

template<class T>
Singleton<T>::~Singleton()
{
  for (auto& r_map : m_instance) {
    delete r_map.second;
  }
  m_instance.clear();
}

/***********************************************************************************/
/***********************************************************************************/

template<class T>
T& Singleton<T>::instance(const std::size_t instanceIndex) {
  if (m_instance.find(instanceIndex) == m_instance.end()) {
    m_instance.insert({instanceIndex, new T(instanceIndex)});
  }
  return *m_instance[instanceIndex];
}

/***********************************************************************************/
/***********************************************************************************/

template <class T>
std::unordered_map<std::size_t, T*> Singleton<T>::m_instance;

} // namespace felisce


#endif // SINGLETON_TPP
