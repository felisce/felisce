#ifndef SOLID_TO_MASTER_HPP
#define SOLID_TO_MASTER_HPP

#include <vector>
#include <iostream>

// Project-specific includes
#include "Core/configure.hpp"

#if defined(FELISCE_WITH_PVM) || defined(FELISCE_WITH_ZMQ)

#ifdef FELISCE_WITH_PVM
#include <pvm3.h>
#endif

#ifdef FELISCE_WITH_ZMQ
#include <zmq.hpp>
#endif

namespace felisce
{

  class SolidToMaster {

  public:
    typedef std::vector<double> Vector;

#ifdef FELISCE_WITH_PVM
    SolidToMaster(int fsiStatus, int nbinter, int nbcoor, Vector& lumpM, Vector& dispS, Vector& veloS, Vector& forceF, Vector& forceC);
#endif
#ifdef FELISCE_WITH_ZMQ
    SolidToMaster(int fsiStatus, int nbinter, int nbcoor, Vector& lumpM, Vector& dispS, Vector& veloS, Vector& forceF, Vector& forceC,
                  std::string socketTransport,
                  std::string socketAddress,
                  std::string socketPort);
#endif

    void setMaster();

    void sdToMasterFSI(int msg);

    void rvFromMasterFSI(int msg=130);

    int status() const;

    int hasLumpedMass() const;

    double timeStep() const;

    void setEnergy(double& energy);

    /**
    * @brief encapsuled function for recv function of zmq to avoid warning
    *        include memcpy of the data.
    *
    */
#ifdef FELISCE_WITH_ZMQ
    void recvZEROMQ_default(zmq::message_t &msg, void * data, std::size_t sizeData);
#endif

  private:
    int m_nbInter;
    int m_nbCoor;
#ifdef FELISCE_WITH_PVM
    int m_masterID;
#endif
#ifdef FELISCE_WITH_ZMQ
    std::string m_socketTransport; // e.g. tcp
    std::string m_socketAddress; // e.g. 127.0.0.1 (localhost)
    std::string m_socketPort; // e.g. 5555
    zmq::context_t m_zmq_context;
    zmq::socket_t m_socket;
#endif
    int m_fsiStatus;

    Vector m_vec1_master;
    Vector m_vec2_master;

    Vector& m_lumpM;
    Vector& m_dispS;
    Vector& m_veloS;
    Vector& m_forceF;
    Vector& m_forceC;

    double m_dt;
    int m_hasLumpedMass;
    // int m_dimLumpMass;
    bool m_hasEnergy;
    double* m_energy;
  };

}

#endif
#endif
