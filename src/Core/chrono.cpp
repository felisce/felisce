//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:     Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Core/chrono.hpp"

namespace felisce
{

Chrono::Chrono(const bool active):
  m_active(active),
  m_resultDir("."),
  m_comm(MPI_COMM_WORLD)
{
  m_globalChrono.start();
}

/***********************************************************************************/
/***********************************************************************************/

Chrono::Chrono(const bool active, const std::string resultDir, MPI_Comm comm):
  m_active(active),
  m_resultDir(resultDir),
  m_comm(comm)
{
  m_globalChrono.start();
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::Sleep(const int TimeToSleep)
{
  std::this_thread::sleep_for(std::chrono::seconds(TimeToSleep));
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::Start(
  const std::string& rLabel, 
  const int SubLabel
  )
{
  if ( !m_active )
    return;

  auto find_label_it = m_chrono.find(rLabel);
  if (find_label_it == m_chrono.end()) {
    m_chrono[rLabel] = std::map<int, ChronoInstance>();
    auto& r_chrono = m_chrono[rLabel];
    r_chrono[SubLabel] = ChronoInstance();
    r_chrono[SubLabel].start();
  } else {
    auto& r_sub_map = find_label_it->second;
    auto find_sub_label_it = r_sub_map.find(SubLabel);
    if (find_sub_label_it == r_sub_map.end())
      r_sub_map[SubLabel] = ChronoInstance();
    r_sub_map[SubLabel].start();
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::Stop(
  const std::string& rLabel, 
  const int SubLabel
  )
{
  if ( !m_active )
    return;

  auto find_label_it = m_chrono.find(rLabel);
  if (find_label_it == m_chrono.end()) {
    std::cerr << "Chrono::Stop: Chrono with name " << rLabel << " not defined. Call Start first" << std::endl;
  } else {
    auto& r_sub_map = find_label_it->second;
    auto find_sub_label_it = r_sub_map.find(SubLabel);
    if (find_sub_label_it == r_sub_map.end()) {
      std::cerr << "Chrono::Stop: Chrono with name " << rLabel << " and sublabel " << SubLabel << " not defined. Call Start first" << std::endl;
    } else {
      auto& r_chrono = find_sub_label_it->second;
      r_chrono.stop();
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

double Chrono::Diff(
  const std::string& rLabel, 
  const int SubLabel
  )
{
  if ( !m_active )
    return 0.0;

  auto find_label_it = m_chrono.find(rLabel);
  if (find_label_it == m_chrono.end()) {
    std::cerr << "Chrono::Diff: Chrono with name " << rLabel << " not defined. Call Start and Stop first" << std::endl;
  } else {
    auto& r_sub_map = find_label_it->second;
    auto find_sub_label_it = r_sub_map.find(SubLabel);
    if (find_sub_label_it == r_sub_map.end()) {
      std::cerr << "Chrono::Diff: Chrono with name " << rLabel << " and sublabel " << SubLabel << " not defined. Call Start and Stop first" << std::endl;
    } else {
      auto& r_chrono = find_sub_label_it->second;
      return r_chrono.diff();
    }
  }

  return 0.0;
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::fillTable()
{
  if ( m_isTableBuilt )
    return;

  m_isTableBuilt = true;  

  // Set percentages
  const double total_time = getTotalTime();

  // Get the rank of the process
  int rankProc, numProc;  
  MPI_Comm_rank(m_comm,&rankProc);
  MPI_Comm_size(m_comm,&numProc);

  m_table.add_row({"Label", "Sublabel", "Time elapsed [CPU"+std::to_string(rankProc)+"] (s)", "Perc. tot. time (%)", "# Calls", "Average time elapsed [CPU"+std::to_string(rankProc)+"] (s)", "Average perc. tot. time (%)"});
  m_rows += 1;

  // Center-align and color header cells
  for (std::size_t i = 0; i < 7; ++i) {
    m_table[0][i].format()
      .font_color(tabulate::Color::yellow)
      .font_align(tabulate::FontAlign::center)
      .font_style({tabulate::FontStyle::bold});
  }

  for (auto& r_label_chronos: m_chrono) {
    const auto& r_label_name  = r_label_chronos.first;
    const auto& r_sub_chronos = r_label_chronos.second;

    for (auto& r_sub_label_chrono: r_sub_chronos) {
      const auto& r_sub_name   = r_sub_label_chrono.first;
      const auto& r_sub_chrono = r_sub_label_chrono.second;

      // Fill the table
      const double diff = r_sub_chrono.diff_cumul();
      const double percentage = diff / total_time * 100;
      std::stringstream buffer;
      buffer << std::setprecision(16) << std::scientific << diff;

      const uint64_t num = r_sub_chrono.num_call();
      const double ave_diff = diff / num;
      const double ave_perc = ave_diff / total_time * 100;
      std::stringstream buffer_ave;
      buffer_ave << std::setprecision(16) << std::scientific << ave_diff;

      m_table.add_row({r_label_name, std::to_string(r_sub_name), buffer.str(), std::to_string(percentage), std::to_string(num), buffer_ave.str(), std::to_string(ave_perc) });
      m_table[m_rows][0].format()
      .font_color(tabulate::Color::magenta)
      .font_align(tabulate::FontAlign::left)
      .font_style({tabulate::FontStyle::bold});
      m_rows += 1;
    }
  }

  // Center align
  m_table.column(0).format().font_align(tabulate::FontAlign::left);
  for (std::size_t i = 1; i < 7; ++i) {
    m_table.column(i).format().font_align(tabulate::FontAlign::center);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::ToScreen()
{
  // Get the rank of the process
  int rankProc, numProc;  
  MPI_Comm_rank(m_comm,&rankProc);
  MPI_Comm_size(m_comm,&numProc);

  // Build table
  fillTable();

  // Print to screen
  for (int i = 0; i < numProc; ++i) {

    MPI_Barrier(MPI_COMM_WORLD);

    if ( rankProc == i )
      std::cout << "\n\n" << m_table << "\n\n" << generateGlobalTable() << std::endl;
    
    MPI_Barrier(MPI_COMM_WORLD);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::ToFile()
{
  // Get the rank of the process
  int rankProc, numProc;  
  MPI_Comm_rank(m_comm,&rankProc);
  MPI_Comm_size(m_comm,&numProc);

  // Build table
  fillTable();

  /* Writes table into file */
  for (int i = 0; i < numProc; ++i) {

    if ( rankProc == i ) {
      std::ostringstream filename;
      filename << m_resultDir << "/" << "chrono" << std::to_string(rankProc) << ".md";
      std::ofstream f;
      f.open(filename.str().c_str());

      if ( f.good() ) {
        // Exported Markdown
        tabulate::MarkdownExporter exporter_md;
        auto markdown = exporter_md.dump(m_table);
        f << markdown << std::endl;
        f.close();
      } else {
        std::ostringstream msg;
        msg << "Can not write in file " << filename.str() << std::endl;
        FEL_WARNING(msg.str().c_str());
      }
    }
  }

  /* Writes table into file */
  for (int i = 0; i < numProc; ++i) {

    if ( rankProc == i ) {
      std::ostringstream filename;
      filename << m_resultDir << "/" << "chrono_total" << std::to_string(rankProc) << ".md";
      std::ofstream f;
      f.open(filename.str().c_str());

      if ( f.good() ) {
        // Exported Markdown
        tabulate::MarkdownExporter exporter_md;
        auto total_table = generateGlobalTable();
        auto markdown = exporter_md.dump(total_table);
        f << markdown << std::endl;
        f.close();
      } else {
        std::ostringstream msg;
        msg << "Can not write in file " << filename.str() << std::endl;
        FEL_WARNING(msg.str().c_str());
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Chrono::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << m_table << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

tabulate::Table Chrono::generateGlobalTable()
{
  tabulate::Table global_table;

  // Get the total time
  const double total_time = getTotalTime();
  double remaining_time = total_time;
  const std::string total_time_string = std::to_string(total_time);

  // Get the rank of the process
  int rankProc, numProc;  
  MPI_Comm_rank(m_comm,&rankProc);
  MPI_Comm_size(m_comm,&numProc);

  global_table.add_row({"Label", "Time elapsed [CPU"+std::to_string(rankProc)+"] (s)", "T. time elapsed [CPU"+std::to_string(rankProc)+"] (s)", "Perc. tot. time (%)"});

  // Center-align and color header cells
  for (std::size_t i = 0; i < 4; ++i) {
    global_table[0][i].format()
      .font_color(tabulate::Color::yellow)
      .font_align(tabulate::FontAlign::center)
      .font_style({tabulate::FontStyle::bold});
  }

  // Add rows
  std::size_t i = 1;
  double total_time_label = 0.0;
  for (auto& r_label_chronos: m_chrono) {
    total_time_label = 0.0;
    const auto& r_label_name = r_label_chronos.first;
    const auto& r_sub_chronos = r_label_chronos.second;
    for (auto& r_sub_label_chrono: r_sub_chronos) {
      total_time_label += (r_sub_label_chrono.second).diff_cumul();
    }
    remaining_time -= total_time_label;
    global_table.add_row({r_label_name, std::to_string(total_time_label), total_time_string, std::to_string((total_time_label/total_time)*100)});
    global_table[i][0].format()
      .font_color(tabulate::Color::magenta)
      .font_align(tabulate::FontAlign::left)
      .font_style({tabulate::FontStyle::bold});
    ++i;
  }
  // In case remaining time is not null
  if (remaining_time > 0.0) {
    global_table.add_row({"Remaining time", std::to_string(remaining_time), total_time_string, std::to_string((remaining_time/total_time)*100)});
    global_table[i][0].format()
        .font_color(tabulate::Color::magenta)
        .font_align(tabulate::FontAlign::left)
        .font_style({tabulate::FontStyle::bold});
  }

  // Center align
  global_table.column(0).format().font_align(tabulate::FontAlign::left);
  for (std::size_t i = 1; i < 4; ++i) {
    global_table.column(i).format().font_align(tabulate::FontAlign::center);
  }

  return global_table;
}
}
