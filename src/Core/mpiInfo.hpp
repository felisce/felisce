//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

/*!
 \file mpiInfo.hpp
 \brief Contains besic info about mpi things, such as rankProc, petsc communicator, numproc.
 */

#ifndef _MPIINFO_HPP
#define _MPIINFO_HPP

// System includes

// External includes
#include <mpi.h>

// Project includes
#include "Core/singleton.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce {
  
  class MpiInfo 
    : public Singleton<MpiInfo> {
    
    friend class Singleton<MpiInfo>;

  private:
    //Disable default copy-constructor and copy-assignement.
    //If copy-constructor is needed, proper copy of ElementFieldDynamicValueMap has
    //to be written.
    MpiInfo(const MpiInfo &);
    MpiInfo(MpiInfo &);
    MpiInfo& operator=(const MpiInfo&);
    MpiInfo& operator=(MpiInfo&);

  protected:
    MpiInfo(const std::size_t /*instanceIndex*/ = 0) {}
    
    virtual ~MpiInfo()= default;
  public:
    /// Pointer definition of MpiInfo
    FELISCE_CLASS_POINTER_DEFINITION(MpiInfo);

    static MPI_Comm& petscComm() {
      return instance().m_petscComm;
    }
    static int& rankProc() {
      return instance().m_rankProc;
    }
    static int& numProc() {
      return instance().m_numProc;
    }
  private:
    //! Parallel informations
    MPI_Comm m_petscComm;
    int m_rankProc;
    int m_numProc;
  };
}
#endif
