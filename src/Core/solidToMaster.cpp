#include <Core/solidToMaster.hpp>
#include <cstdlib>
#include <Core/felisce_error.hpp>

#if defined(FELISCE_WITH_PVM) || defined(FELISCE_WITH_ZMQ)
namespace felisce {

#ifdef FELISCE_WITH_PVM
  #ifdef FELISCE_WITH_ZMQ
    #error PVM and ZMQ cannot be combined.Please select one
  #endif
#endif

#ifdef FELISCE_WITH_PVM
  SolidToMaster::SolidToMaster(int fsiStatus, int nbinter, int nbcoor, Vector& lumpM, Vector& dispS, Vector& veloS, Vector& forceF, Vector& forceC)
#endif
#ifdef FELISCE_WITH_ZMQ  
  SolidToMaster::SolidToMaster(int fsiStatus, int nbinter, int nbcoor, Vector& lumpM, Vector& dispS, Vector& veloS, Vector& forceF, Vector& forceC, std::string socketTransport,std::string socketAddress,std::string socketPort)
#endif
  :m_nbInter(nbinter),
    m_nbCoor(nbcoor),
#ifdef FELISCE_WITH_PVM
    m_masterID(0),
#endif
#ifdef FELISCE_WITH_ZMQ
    m_socketTransport(socketTransport),
    m_socketAddress(socketAddress),
    m_socketPort(socketPort),
    m_zmq_context(1),
    m_socket(m_zmq_context, ZMQ_PAIR),
#endif
    m_fsiStatus(fsiStatus),
    m_lumpM(lumpM),
    m_dispS(dispS),
    m_veloS(veloS),
    m_forceF(forceF),
    m_forceC(forceC),
    m_dt(0.0)
  {
    m_vec1_master.resize(3*nbinter);
    m_vec2_master.resize(3*nbinter);
    for(int i=0; i< 3*nbinter; ++i) {
      m_vec1_master[i] = 0.;
      m_vec2_master[i] = 0.;
    }
    m_hasEnergy = false;

  }




  void SolidToMaster::setMaster() {
#ifdef FELISCE_WITH_PVM
    m_masterID = pvm_parent();
    if( m_masterID == PvmNoParent )
    {
      std::cout << " I am a poor lonesome job\n ";
      exit(1);
    }
    else if( m_masterID == 0 )
    {
      std::cout << "PVM is not ok\n";
      exit(1);
    }

    std::cout <<"%%%%%%%%%%% \n";
    std::cout << " I am the slave " << pvm_mytid()<< std::endl;
    std::cout << " My master is " << m_masterID << std::endl;
#endif
#ifdef FELISCE_WITH_ZMQ
    std::string socket_endpoint;
    if(m_socketPort != "")
      socket_endpoint = m_socketTransport + "://" + m_socketAddress + ":" + m_socketPort;
    else
      socket_endpoint = m_socketTransport + "://" + m_socketAddress;

    std::cout << "[ZeroMQ] Connect to " << socket_endpoint << std::endl;
    if ( zmq_connect(m_socket,socket_endpoint.c_str()) != 0 ) {
      std::cout << "Problem while connecting to : " << socket_endpoint << std::endl;
      exit(1);
    }
#endif
  }


  void SolidToMaster::setEnergy(double& energy) {
    m_energy = &energy;
    m_hasEnergy = true;
  }

  void SolidToMaster::sdToMasterFSI(int msg) {
    int nbDOF = 3*m_nbInter;

    std::cout <<  "---> Solid sends data to Master message " << msg << "... ";

#ifdef FELISCE_WITH_PVM
    if (msg == 99 ) {
      int lumpMdim, thickStruct;
      lumpMdim = nbDOF;
      thickStruct = 0;

      pvm_initsend(PvmDataDefault);
      pvm_pkint(&thickStruct,1,1);
      pvm_pkint(&lumpMdim,1,1);

      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j)
          m_vec1_master[3*i+j] = m_lumpM[m_nbCoor*i+j];
      pvm_pkdouble(m_vec1_master.data(),lumpMdim,1);

      pvm_pkint(&nbDOF,1,1);

      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_dispS[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_veloS[m_nbCoor*i+j];
        }
      pvm_pkdouble(m_vec1_master.data(),nbDOF,1);
      pvm_pkdouble(m_vec2_master.data(),nbDOF,1);
      pvm_send(m_masterID,100);
    }
    if (msg == 100 ) {
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&nbDOF,1,1);
      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_dispS[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_veloS[m_nbCoor*i+j];
        }
      pvm_pkdouble(m_vec1_master.data(),nbDOF,1);
      pvm_pkdouble(m_vec2_master.data(),nbDOF,1);
      pvm_send(m_masterID,100);
    }
    if (msg == 1100 ) {
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&nbDOF,1,1);
      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_dispS[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_veloS[m_nbCoor*i+j];
        }
      pvm_pkdouble(m_vec1_master.data(),nbDOF,1);
      pvm_pkdouble(m_vec2_master.data(),nbDOF,1);
      pvm_pkdouble(m_energy,1,1);
      pvm_send(m_masterID,1100);
    }
#endif
#ifdef FELISCE_WITH_ZMQ
    if (msg == 99 ) {
      int lumpMdim, thickStruct;
      lumpMdim = nbDOF;
      thickStruct = 0;
      //
      zmq::message_t msg1(1*sizeof(int));
      memcpy(msg1.data(),&thickStruct,1*sizeof(int));
      m_socket.send(msg1,zmq::send_flags::none);
      //
      zmq::message_t msg2(1*sizeof(int));
      memcpy(msg2.data(),&lumpMdim,1*sizeof(int));
      m_socket.send(msg2,zmq::send_flags::none);
      //
      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j)
          m_vec1_master[3*i+j] = m_lumpM[m_nbCoor*i+j];
      //
      zmq::message_t msg3(lumpMdim*sizeof(double));
      memcpy(msg3.data(),m_vec1_master.data(),lumpMdim*sizeof(double));
      m_socket.send(msg3,zmq::send_flags::none);
      //
      zmq::message_t msg4(1*sizeof(int));
      memcpy(msg4.data(),&nbDOF,1*sizeof(int));
      m_socket.send(msg4,zmq::send_flags::none);
      //
      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_dispS[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_veloS[m_nbCoor*i+j];
        }
      zmq::message_t msg5;
      recvZEROMQ_default(msg5, m_vec1_master.data(), nbDOF*sizeof(double));
      // zmq::recv_result_t recvResult5 = m_socket.recv(msg5,zmq::recv_flags::none);
      // FEL_ASSERT( recvResult5 == nbDOF*sizeof(double) );
      // memcpy(m_vec1_master.data(),msg5.data(),nbDOF*sizeof(double));
      //
      zmq::message_t msg6;
      recvZEROMQ_default(msg6, m_vec2_master.data(), nbDOF*sizeof(double));
      // zmq::recv_result_t recvResult6 = m_socket.recv(msg6,zmq::recv_flags::none);
      // FEL_ASSERT( recvResult6 == nbDOF*sizeof(double) );
      // memcpy(m_vec2_master.data(),msg6.data(),nbDOF*sizeof(double));
      //
    }
    if (msg == 100 ) {
      zmq::message_t msg1(1*sizeof(int));
      memcpy(msg1.data(),&nbDOF,1*sizeof(int));
      m_socket.send(msg1,zmq::send_flags::none);
      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_dispS[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_veloS[m_nbCoor*i+j];
        }
      zmq::message_t msg2(nbDOF*sizeof(double));
      memcpy(msg2.data(),m_vec1_master.data(),nbDOF*sizeof(double));
      m_socket.send(msg2,zmq::send_flags::none);

      zmq::message_t msg3(nbDOF*sizeof(double));
      memcpy(msg3.data(),m_vec2_master.data(),nbDOF*sizeof(double));
      m_socket.send(msg3,zmq::send_flags::none);
    }
#endif
    std::cout << " done.\n";
  }

  void SolidToMaster::rvFromMasterFSI(int msg) {

    std::cout << "Solid Receives Message " << msg << std::endl;
    int dim;

#ifdef FELISCE_WITH_PVM
    pvm_recv(m_masterID,130);
    pvm_upkint(&m_fsiStatus,1,1);
    std::cout << "-----> fsiStatus = " << m_fsiStatus << std::endl;

    if(m_fsiStatus != -1)
    {
      pvm_upkdouble(&m_dt,1,1);
      pvm_upkint(&dim,1,1);

      if(dim != 3* m_nbInter)
        std::cout << "rv: dim = " << dim << ", 3 * m_nbIner = " << 3 * m_nbInter << std::endl;

      pvm_upkdouble(m_vec1_master.data(),3*m_nbInter,1);
      if ( msg == 1100 )
        pvm_upkdouble(m_vec2_master.data(),3*m_nbInter,1);

      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j)
          m_forceF[m_nbCoor*i+j] = m_vec1_master[3*i+j];
      if ( msg == 1100 )
        for(int i=0; i<m_nbInter; ++i)
          for(int j=0; j<m_nbCoor; ++j)
            m_forceC[m_nbCoor*i+j] = m_vec2_master[3*i+j];

    }
#endif
#ifdef FELISCE_WITH_ZMQ
    zmq::message_t msg1;
    recvZEROMQ_default(msg1, &m_fsiStatus,  1*sizeof(int));
    // zmq::recv_result_t recvResult1 = m_socket.recv(msg1,zmq::recv_flags::none);
    // FEL_ASSERT( recvResult1 == sizeof(int) );
    // memcpy(&m_fsiStatus,msg1.data(),1*sizeof(int));
    std::cout << "-----> fsiStatus = " << m_fsiStatus << std::endl;

    if(m_fsiStatus != -1){
      zmq::message_t msg2;
      recvZEROMQ_default(msg2, &m_dt,  1*sizeof(double));
      // zmq::recv_result_t recvResult2 = m_socket.recv(msg2,zmq::recv_flags::none);
      // FEL_ASSERT( recvResult2 == sizeof(double) );
      // memcpy(&m_dt,msg2.data(),1*sizeof(double));
      //
      zmq::message_t msg3;
      recvZEROMQ_default(msg3, &dim,  1*sizeof(int));
      // zmq::recv_result_t recvResult3 = m_socket.recv(msg3,zmq::recv_flags::none);
      // FEL_ASSERT( recvResult3 == sizeof(int) );
      // memcpy(&dim,msg3.data(),1*sizeof(int));
      //
      if(dim != 3*m_nbInter)
        std::cout << "rv: dim = " << dim << ", 3 * m_nbIner = " << 3 * m_nbInter << std::endl;
      //
      zmq::message_t msg4;
      recvZEROMQ_default(msg4, m_vec1_master.data(), 3*m_nbInter*sizeof(double));
      // zmq::recv_result_t recvResult4 = m_socket.recv(msg4,zmq::recv_flags::none);
      // FEL_ASSERT( recvResult4 == 3*m_nbInter*sizeof(double) );
      // memcpy(m_vec1_master.data(),msg4.data(),3*m_nbInter*sizeof(double));
      //
      for(int i=0; i<m_nbInter; ++i)
        for(int j=0; j<m_nbCoor; ++j)
          m_forceF[m_nbCoor*i+j] = m_vec1_master[3*i+j];
    }
#endif
  }

  double SolidToMaster::timeStep() const {return m_dt;}

  int SolidToMaster::hasLumpedMass() const {return m_hasLumpedMass;}

  int SolidToMaster::status() const {return m_fsiStatus;}

  #ifdef FELISCE_WITH_ZMQ
void SolidToMaster::recvZEROMQ_default(zmq::message_t &msg, void * data, std::size_t sizeData)
  {
    // default correspond to used the flags zmq::recv_flags::none
    zmq::recv_result_t recvResult = m_socket.recv( msg, zmq::recv_flags::none);
    memcpy( data, msg.data(),sizeData);
    if( recvResult != sizeData){
      FEL_ASSERT( recvResult == sizeData );
    }
  }
#endif

}
#endif
