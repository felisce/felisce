#include <Core/fluidToMaster.hpp>
#include <Core/felisceParam.hpp>
#include <cstdlib>

#if defined(FELISCE_WITH_PVM) || defined(FELISCE_WITH_ZMQ)
namespace felisce {

#ifdef FELISCE_WITH_PVM
  FluidToMaster::FluidToMaster(int fsiStatus,
                               int nbCoor,
                               std::vector<int>&  match,
                               std::vector<double>& disp_master,
                               std::vector<double>& velo_master,
                               std::vector<double>& lumpMmaster,
                               std::vector<double>& force_fluid,
                               std::vector<double>& velo_fluid):
#endif
#ifdef FELISCE_WITH_ZMQ
  FluidToMaster::FluidToMaster(int fsiStatus,
                               int nbCoor,
                               std::vector<int>&  match,
                               std::vector<double>& disp_master,
                               std::vector<double>& velo_master,
                               std::vector<double>& lumpMmaster,
                               std::vector<double>& force_fluid,
                               std::vector<double>& velo_fluid,
                               std::string socketTransport,std::string socketAddress,std::string socketPort):
#endif
  m_nbCoor(nbCoor),
  m_nbNodeFSI(0),
  m_ntdlfConf(0),
#ifdef FELISCE_WITH_PVM
  m_masterID(0),
#endif
#ifdef FELISCE_WITH_ZMQ
  m_socketTransport(socketTransport),
  m_socketAddress(socketAddress),
  m_socketPort(socketPort),
  m_zmq_context(1),
  m_socket(m_zmq_context, ZMQ_PAIR),
#endif
  m_fsiStatus(fsiStatus),
  m_match(match),
  m_disp_master(disp_master),
  m_velo_master(velo_master),
  m_lumpMmaster(lumpMmaster),
  m_force_fluid(force_fluid),
  m_velo_fluid(velo_fluid),
  m_dt(0.0),
  m_hasLumpedMass(0),
  m_dimLumpMass(0)
  {}

#ifdef FELISCE_WITH_ZMQ
void FluidToMaster::recvZEROMQ_default(zmq::message_t &msg, void * data, std::size_t sizeData)
  {
    // default correspond to used the flags zmq::recv_flags::none
    zmq::recv_result_t recvResult = m_socket.recv( msg, zmq::recv_flags::none);
    memcpy( data, msg.data(),sizeData);
    if( recvResult != sizeData){
      FEL_ASSERT( recvResult == sizeData );
    }
  }
#endif

  //  dispm_master, velo_master, lumpMmaster, force_fluid, velo_fluid have the correct size (i.e., nbCoor * nbNodeFSI)

  void FluidToMaster::setMaster() {
#ifdef FELISCE_WITH_PVM
    m_masterID = pvm_parent();
    if( m_masterID == PvmNoParent )
    {
      std::cout << " I am a poor lonesome job\n ";
      exit(1);
    }
    else if( m_masterID == 0 )
    {
      std::cout << "PVM is not ok\n";
      exit(1);
    }

    std::cout <<"%%%%%%%%%%% \n";
    std::cout << " I am the slave " << pvm_mytid()<< std::endl;
    std::cout << " My master is " << m_masterID << std::endl;
#endif
#ifdef FELISCE_WITH_ZMQ
    std::string socket_endpoint;
    if(m_socketPort != "")
      socket_endpoint = m_socketTransport + "://" + m_socketAddress + ":" + m_socketPort;
    else
      socket_endpoint = m_socketTransport + "://" + m_socketAddress;
    
    std::cout << "[ZeroMQ] Connect to " << socket_endpoint << std::endl;
    if ( zmq_connect(m_socket,socket_endpoint.c_str()) != 0 ) {
      std::cout << "Problem while connecting to : " << socket_endpoint << std::endl;
      exit(1);
    }
#endif
  }

  void FluidToMaster::sdToMasterFSI() {

    static int ipass=0;
    int nt = 3*m_nbNodeFSI;
    double vref=1.;

    std::cout <<  "---> Fluid sends data to Master\n" << std::endl;

#ifdef FELISCE_WITH_PVM
    pvm_initsend(PvmDataDefault);
    pvm_pkint(&nt,1,1);

    if (  FelisceParam::instance().hasOldMaster ) {
      for(int i=0; i<m_nbNodeFSI; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_velo_fluid[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_force_fluid[m_nbCoor*i+j];
        }

      pvm_pkdouble(m_vec1_master.data(),nt,1);
      pvm_pkdouble(m_vec2_master.data(),nt,1);
    }
    else {
      for(int i=0; i<m_nbNodeFSI; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          //m_vec1_master[3*i+j] = m_velo_fluid[m_nbCoor*i+j];  // uncoment for R-N iterations
          m_vec2_master[3*i+j] = m_force_fluid[m_nbCoor*i+j];
        }

      //pvm_pkdouble(m_vec1_master.data(),nt,1); // uncoment for R-N iterations
      pvm_pkdouble(m_vec2_master.data(),nt,1);
    }
    if(!ipass){
      pvm_pkdouble(&vref,1,1);
      ipass = 1;
    }
    pvm_send(m_masterID,120);
#endif
#ifdef FELISCE_WITH_ZMQ
    zmq::message_t msg1(1*sizeof(int));
    memcpy(msg1.data(),&nt,1*sizeof(int));
    m_socket.send(msg1,zmq::send_flags::none);
    //
    if (  FelisceParam::instance().hasOldMaster ) {
      for(int i=0; i<m_nbNodeFSI; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          m_vec1_master[3*i+j] = m_velo_fluid[m_nbCoor*i+j];
          m_vec2_master[3*i+j] = m_force_fluid[m_nbCoor*i+j];
        }

      zmq::message_t msg2(nt*sizeof(double));
      memcpy(msg2.data(),m_vec1_master.data(),nt*sizeof(double));
      m_socket.send(msg2,zmq::send_flags::none);

      zmq::message_t msg3(nt*sizeof(double));
      memcpy(msg3.data(),m_vec2_master.data(),nt*sizeof(double));
      m_socket.send(msg3,zmq::send_flags::none);
    }
    else {
      for(int i=0; i<m_nbNodeFSI; ++i)
        for(int j=0; j<m_nbCoor; ++j) {
          //m_vec1_master[3*i+j] = m_velo_fluid[m_nbCoor*i+j];  // uncoment for R-N iterations
          m_vec2_master[3*i+j] = m_force_fluid[m_nbCoor*i+j];
        }
      // zmq::message_t msg2(nt*sizeof(double)); // uncoment for R-N iterations
      // memcpy(msg2.data(),m_vec1_master.data(),nt*sizeof(double)); // uncoment for R-N iterations
      // m_socket.send(msg2,zmq::send_flags::none); // uncoment for R-N iterations

      zmq::message_t msg3(nt*sizeof(double));
      memcpy(msg3.data(),m_vec2_master.data(),nt*sizeof(double));
      m_socket.send(msg3,zmq::send_flags::none);
    }
    if(!ipass){
      zmq::message_t msg4(1*sizeof(double));
      memcpy(msg4.data(),&vref,1*sizeof(double));
      m_socket.send(msg4,zmq::send_flags::none);
      ipass = 1;
    }
#endif
  }

  void FluidToMaster::rvFromMasterFSI(int msg) {
    std::cout << "<-- Fluid receives from Master\n" << std::endl;
    switch(msg){
      case 99:
      {
        std::cout << " -- Fluid Receives Message 99\n" << std::endl;
#ifdef FELISCE_WITH_PVM
        pvm_recv(m_masterID,99);
        pvm_upkint(&m_hasLumpedMass,1,1);
        std::cout << " m_hasLumpedMass = " << m_hasLumpedMass << std::endl;
        pvm_upkint(&m_dimLumpMass,1,1);
        if ( (m_dimLumpMass != 3*m_nbNodeFSI) && m_hasLumpedMass ){
          std::cout << "In rvFromMasterFSI(msg=99) m_dimLumpMass != 3*m_nbNodeFSI  " << m_dimLumpMass << "  "  << 3*m_nbNodeFSI  << std::endl;
          exit(1);
        }
        pvm_upkdouble(m_vec1_master.data(),3*m_nbNodeFSI,1);
#endif
#ifdef FELISCE_WITH_ZMQ
        zmq::message_t msg1;
        recvZEROMQ_default(msg1, &m_hasLumpedMass, 1*sizeof(int));
        // zmq::recv_result_t recvResult1 = m_socket.recv(msg1,recv_flags_none);
        // FEL_ASSERT( recvResult1 == sizeof(int) );
        // memcpy(&m_hasLumpedMass,msg1.data(),1*sizeof(int));
        std::cout << " m_hasLumpedMass = " << m_hasLumpedMass << std::endl;

        zmq::message_t msg2;
        recvZEROMQ_default(msg2, &m_dimLumpMass, 1*sizeof(int));
        // zmq::recv_result_t recvResult2 = m_socket.recv(msg2,recv_flags_none);
        // FEL_ASSERT(recvResult2 == sizeof(int) );
        // memcpy(&m_dimLumpMass,msg2.data(),1*sizeof(int));

        if ( (m_dimLumpMass != 3*m_nbNodeFSI) && m_hasLumpedMass ){
          std::cout << "In rvFromMasterFSI(msg=99) m_dimLumpMass != 3*m_nbNodeFSI  " << m_dimLumpMass << "  "  << 3*m_nbNodeFSI  << std::endl;
          exit(1);
        }
        zmq::message_t msg3;
        recvZEROMQ_default(msg3, m_vec1_master.data(), 3*m_nbNodeFSI*sizeof(double));
        // zmq::recv_result_t recvResult3 = m_socket.recv(msg3,recv_flags_none);
        // FEL_ASSERT( recvResult3 == 3*m_nbNodeFSI*sizeof(double) );
        // memcpy(m_vec1_master.data(),msg3.data(),3*m_nbNodeFSI*sizeof(double));
#endif
        // reduce to true size
        for(int i=0; i<m_nbNodeFSI; ++i)
          for(int j=0; j<m_nbCoor; ++j)
            m_lumpMmaster[m_nbCoor*i+j] = m_vec1_master[3*i+j];
      }
        break;
      case 140:
      {
        std::cout << " -- Fluid Receives Message 140\n" << std::endl;
        int nnifsConf, nnifsIm, nnifsCrack, ntdlfIm,  ntdlfCrack ;
#ifdef FELISCE_WITH_PVM
        pvm_recv(m_masterID,140);
        pvm_upkint(&nnifsConf,1,1);
        pvm_upkint(&nnifsIm,1,1);
        pvm_upkint(&nnifsCrack,1,1);

        m_nbNodeFSI= nnifsConf +  nnifsIm;

        m_match.resize(m_nbNodeFSI);

        pvm_upkint(&m_ntdlfConf,1,1);
        pvm_upkint(&ntdlfIm,1,1);
        pvm_upkint(&ntdlfCrack,1,1);
        pvm_upkint(m_match.data(),nnifsConf,1);
#endif
#ifdef FELISCE_WITH_ZMQ
        zmq::message_t msg1;
        recvZEROMQ_default(msg1, &nnifsConf, 1*sizeof(int));
        // zmq::recv_result_t recvResult1 = m_socket.recv(msg1,recv_flags_none);
        // FEL_ASSERT(recvResult1 == sizeof(int) );
        // memcpy(&nnifsConf,msg1.data(),1*sizeof(int));
        //
        zmq::message_t msg2;
        recvZEROMQ_default(msg2, &nnifsIm, 1*sizeof(int));
        // zmq::recv_result_t recvResult2 = m_socket.recv(msg2,recv_flags_none);
        // FEL_ASSERT(recvResult2 == sizeof(int) );
        // memcpy(&nnifsIm,msg2.data(),1*sizeof(int));
        //
        zmq::message_t msg3;
        recvZEROMQ_default(msg3,&nnifsCrack,1*sizeof(int));
        // zmq::recv_result_t recvResult3 = m_socket.recv(msg3,recv_flags_none);
        // FEL_ASSERT(recvResult3 == sizeof(int) );
        // memcpy(&nnifsCrack,msg3.data(),1*sizeof(int));
        //
        m_nbNodeFSI= nnifsConf +  nnifsIm;

        m_match.resize(m_nbNodeFSI);
        //
        zmq::message_t msg4;
        recvZEROMQ_default(msg4,&m_ntdlfConf,1*sizeof(int));
        // zmq::recv_result_t recvResult4 = m_socket.recv(msg4,recv_flags_none);
        // FEL_ASSERT(recvResult4 == sizeof(int) );
        // memcpy(&m_ntdlfConf,msg4.data(),1*sizeof(int));
        //
        zmq::message_t msg5;
        recvZEROMQ_default(msg5,&ntdlfIm,1*sizeof(int));
        // zmq::recv_result_t recvResult5 = m_socket.recv(msg5,recv_flags_none);
        // FEL_ASSERT(recvResult5 == sizeof(int) );
        // memcpy(&ntdlfIm,msg5.data(),1*sizeof(int));
        //
        zmq::message_t msg6;
        recvZEROMQ_default(msg6,&ntdlfCrack,1*sizeof(int));
        // zmq::recv_result_t recvResult6 = m_socket.recv(msg6,recv_flags_none);
        // FEL_ASSERT(recvResult6 == sizeof(int) );
        // memcpy(&ntdlfCrack,msg6.data(),1*sizeof(int));
        //
        zmq::message_t msg7;
        recvZEROMQ_default(msg7,m_match.data(),nnifsConf*sizeof(int));
        // zmq::recv_result_t recvResult7 = m_socket.recv(msg7,recv_flags_none);
        // FEL_ASSERT(recvResult7 == nnifsConf*sizeof(int) );
        // memcpy(m_match.data(),msg7.data(),nnifsConf*sizeof(int));
#endif
        std::cout << "--> Nodes FSI:" << m_nbNodeFSI <<std::endl;


        m_vec1_master.resize(3*m_nbNodeFSI);
        m_vec2_master.resize(3*m_nbNodeFSI);

        m_disp_master.resize(m_nbCoor*m_nbNodeFSI);
        m_velo_master.resize(m_nbCoor*m_nbNodeFSI);
        m_force_fluid.resize(m_nbCoor*m_nbNodeFSI);
        m_velo_fluid.resize(m_nbCoor*m_nbNodeFSI);
        m_lumpMmaster.resize(m_nbCoor*m_nbNodeFSI);

        for(int i=0; i< m_nbCoor * m_nbNodeFSI; ++i)  {
          m_disp_master[i] = 0.;
          m_velo_master[i] = 0.;
          m_force_fluid[i] = 0.;
          m_velo_fluid[i] = 0.;
          m_lumpMmaster[i] = 0.;
        }

        for(int i=0; i< 3 * m_nbNodeFSI; ++i)  {
          m_vec1_master[i] = 0.;
          m_vec2_master[i] = 0.;
        }
        break;
      }
      case 110:
      {
        std::cout << "Fluid Receives Message 110\n" << std::endl;
        int ntdlf;
#ifdef FELISCE_WITH_PVM
        pvm_recv(m_masterID,110);
        pvm_upkint(&m_fsiStatus,1,1);
        std::cout << "-----> fsiStatus = " << m_fsiStatus << std::endl << std::endl;

        if(m_fsiStatus>=0){

          pvm_upkdouble(&m_dt,1,1);
          pvm_upkint(&ntdlf,1,1);

          if(ntdlf != 3*m_nbNodeFSI)
            std::cout << "rv: ntdlf = " << ntdlf << " 3*nbNodeFSI ="
            << 3*m_nbNodeFSI << std::endl;

          pvm_upkdouble(m_vec1_master.data(),3*m_nbNodeFSI,1);
          pvm_upkdouble(m_vec2_master.data(),3*m_nbNodeFSI,1);
          for(int i=0; i<m_nbNodeFSI; ++i)
            for(int j=0; j<m_nbCoor; ++j) {
              m_disp_master[m_nbCoor*i+j] = m_vec1_master[3*i+j];
              m_velo_master[m_nbCoor*i+j] = m_vec2_master[3*i+j];
            }
        }
#endif
#ifdef FELISCE_WITH_ZMQ
        zmq::message_t msg1;
        recvZEROMQ_default(msg1,&m_fsiStatus,1*sizeof(int));
        //m_socket.recv(msg1,recv_flags_none);
        // zmq::recv_result_t recvResult1 = m_socket.recv(msg1,recv_flags_none);
        // FEL_ASSERT( recvResult1 == sizeof(int) );
        // memcpy(&m_fsiStatus,msg1.data(),1*sizeof(int));
        std::cout << "-----> fsiStatus = " << m_fsiStatus << std::endl << std::endl;

        if(m_fsiStatus>=0){
          zmq::message_t msg2;
          recvZEROMQ_default(msg2,&m_dt,1*sizeof(double));
          //m_socket.recv(msg2,recv_flags_none);
          // zmq::recv_result_t recvResult2 = m_socket.recv(msg2,recv_flags_none);
          // FEL_ASSERT(recvResult2 == sizeof(double) );
          // memcpy(&m_dt,msg2.data(),1*sizeof(double));

          //
          zmq::message_t msg3;
          recvZEROMQ_default(msg3,&ntdlf,1*sizeof(int));
          // zmq::recv_result_t recvResult3 = m_socket.recv(msg3,recv_flags_none);
          // FEL_ASSERT(recvResult3 == sizeof(int) );
          // memcpy(&ntdlf,msg3.data(),1*sizeof(int));
          //
          if(ntdlf != 3*m_nbNodeFSI)
            std::cout << "rv: ntdlf = " << ntdlf << " 3*nbNodeFSI ="
            << 3*m_nbNodeFSI << std::endl;
          //

          zmq::message_t msg4;
          recvZEROMQ_default(msg4,m_vec1_master.data(),3*m_nbNodeFSI*sizeof(double));
          // zmq::recv_result_t recvResult4 = m_socket.recv(msg4,recv_flags_none);
          // FEL_ASSERT( recvResult4 == 3*m_nbNodeFSI*sizeof(double) );
          // memcpy(m_vec1_master.data(),msg4.data(),3*m_nbNodeFSI*sizeof(double));
          //
          zmq::message_t msg5;
          recvZEROMQ_default(msg5,m_vec2_master.data(),3*m_nbNodeFSI*sizeof(double));
          // zmq::recv_result_t recvResult5 = m_socket.recv(msg5,recv_flags_none);
          // FEL_ASSERT( recvResult5 == 3*m_nbNodeFSI*sizeof(double) );
          // memcpy(m_vec2_master.data(),msg5.data(),3*m_nbNodeFSI*sizeof(double));
          //
          for(int i=0; i<m_nbNodeFSI; ++i)
            for(int j=0; j<m_nbCoor; ++j) {
              m_disp_master[m_nbCoor*i+j] = m_vec1_master[3*i+j];
              m_velo_master[m_nbCoor*i+j] = m_vec2_master[3*i+j];
            }
        }
#endif
      }
    }
  }

  void FluidToMaster::sdInterfCoorToMasterFSI() {

    std::cout <<  "---> Fluid sends Interface Coor to Master\n" << std::endl;
#ifdef FELISCE_WITH_PVM
    pvm_initsend(PvmDataDefault);
    pvm_pkint(&m_ntdlfConf,1,1);

    pvm_pkdouble(m_vec1_master.data(),m_ntdlfConf,1);

    pvm_send(m_masterID,125);
#endif
#ifdef FELISCE_WITH_ZMQ
    zmq::message_t msg1(1*sizeof(int));
    memcpy(msg1.data(),&m_ntdlfConf,1*sizeof(int));
    m_socket.send(msg1,zmq::send_flags::none);
    //
    zmq::message_t msg2(m_ntdlfConf*sizeof(double));
    memcpy(msg2.data(),m_vec1_master.data(),m_ntdlfConf*sizeof(double));
    m_socket.send(msg2,zmq::send_flags::none);
    //
#endif

  }

  double FluidToMaster::timeStep() {return m_dt;}

  int FluidToMaster::nbNodes() {return m_nbNodeFSI;}

  int FluidToMaster::hasLumpedMass() {return m_hasLumpedMass;}

  int FluidToMaster::status() {return m_fsiStatus;}

}

#endif


