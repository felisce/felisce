//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//
//

#if !defined(FELISCE_INDEXED_OBJECT_H_INCLUDED )
#define  FELISCE_INDEXED_OBJECT_H_INCLUDED

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce
{

///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class IndexedObject
 * @ingroup Core
 * @brief This object defines an indexed object
 * @details It is used as base of any class which must be indexed
 * @author Vicente Mataix Ferrandiz
 */
class IndexedObject
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IndexedObject
  FELISCE_CLASS_POINTER_DEFINITION(IndexedObject);

  /// This defines the index type
  typedef std::size_t IndexType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  explicit IndexedObject(IndexType NewId = 0) : mId(NewId) {}

  /// Destructor.
  virtual ~IndexedObject() {}

  /// Copy constructor.
  IndexedObject(IndexedObject const& rOther) : mId(rOther.mId) {}

  ///@}
  ///@name Operators
  ///@{

  /// Assignment operator.
  IndexedObject& operator=(IndexedObject const& rOther)
  {
    mId = rOther.mId;

    return *this;
  }

  template<class TObjectType>
  IndexType operator()(TObjectType const& rThisObject) const
  {
    return rThisObject.Id();
  }

  ///@}
  ///@name Operations
  ///@{

  ///@}
  ///@name Access
  ///@{

  IndexType Id() const
  {
    return mId;
  }

  IndexType GetId() const
  {
    return mId;
  }

  virtual void SetId(IndexType NewId)
  {
    mId = NewId;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    std::stringstream buffer;
    buffer << "indexed object # "
         << mId;
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream& rOStream) const
  {
    rOStream << Info();
  }

  /// Print object's data.
  virtual void PrintData(std::ostream& rOStream) const
  {
    IGNORE_UNUSED_ARGUMENT(rOStream);
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  IndexType mId;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class IndexedObject

///@}

///@name Type Definitions
///@{


///@}
///@name Input and output
///@{


/// input stream function
inline std::istream& operator >> (std::istream& rIStream,
                  IndexedObject& rThis);

/// output stream function
inline std::ostream& operator << (std::ostream& rOStream,
                  const IndexedObject& rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}


}  // namespace felisce.

#endif // FELISCE_INDEXED_OBJECT_H_INCLUDED  defined


