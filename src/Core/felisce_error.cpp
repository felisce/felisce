//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/felisce_error.hpp"

namespace felisce {

#ifdef FELISCE_WITH_EXCEPTION

  // FelisceError
  FelisceError::FelisceError():
    str_(NULL) {
  }

  FelisceError::FelisceError(const char * str,const char * file,int line):
    str_(NULL) {
    std::ostringstream msg;
    msg << file << ":" << line << ": " << str ;
    msg.flush();

    str_ = new char[ strlen( msg.str().c_str() )+1 ];
    strcpy(str_, msg.str().c_str() );
  }

  const char* FelisceError::what() const throw() {
    return str_;
  }

  // FelisceAssertionError
  FelisceAssertionError::FelisceAssertionError() {
  }

  FelisceAssertionError::FelisceAssertionError(const char * str,const char * file,int line):
    str_(NULL) {
    std::ostringstream msg;
    msg << file << ":" << line << ": " << str ;
    msg.flush();

    str_ = new char[ strlen( msg.str().c_str() )+1 ];
    strcpy(str_, msg.str().c_str() );
  }

  const char* FelisceAssertionError::what() const throw() {
    return str_;
  }

  // FelisceEqualAssertionError
  FelisceEqualAssertionError::FelisceEqualAssertionError() {
  }

  FelisceEqualAssertionError::FelisceEqualAssertionError(const char * str,const char * file,int line):
    str_(NULL) {
    std::ostringstream msg;
    msg << file << ":" << line << ": " << str ;
    msg.flush();

    str_ = new char[ strlen( msg.str().c_str() )+1 ];
    strcpy(str_, msg.str().c_str() );
  }

  const char* FelisceEqualAssertionError::what() const throw() {
    return str_;
  }

  // error
  void felisce_error(const char * str,const char * file,int line) {
    FelisceError err(str,file,line);
    throw err;
  }

  void felisce_error(std::string str,const char * file,int line) {
    FelisceError err(str.c_str(),file,line);
    throw err;
  }

  //assert
  void felisce_assert(const char * str,const char * file,int line) {
    FelisceAssertionError err(str,file,line);
    throw err;
  }

  void felisce_assert(std::string str,const char * file,int line) {
    FelisceAssertionError err(str.c_str(),file,line);
    throw err;
  }

  //assert equal
  void felisce_assert_equal(const char* expr_a, const char* expr_b, int a, int b, const char * file,int line) {
    if (a!=b) {
      std::ostringstream stream;
      stream << "expected '" << expr_a << " == " << expr_b << "'"
             << ", got: '" << a << " != " << b << "'";
      FelisceEqualAssertionError err(stream.str().c_str(),file,line);
      throw err;
    }
  }

  //assert lt
  void felisce_assert_lt(const char* expr_a, const char* expr_b, int a, int b, const char * file,int line) {
    if (a>=b) {
      std::ostringstream stream;
      stream << "expected '" << expr_a << " < " << expr_b << "'"
             << ", got: '" << a << " >= " << b << "'";
      FelisceEqualAssertionError err(stream.str().c_str(),file,line);
      throw err;
    }
  }

#else

  //I changed all this exit(1) in order to be able to get the stack in gdb. Now debugging is easier.
  __attribute__ ((noreturn)) void felisce_error(const char * str,const char * file,int line) {
    std::cout << file << ":" << line << ": error: " << str << std::endl;
    std::abort();
    //exit(1);
  }

  __attribute__ ((noreturn)) void felisce_error(std::string str,const char * file,int line) {
    std::cout << file << ":" << line << ": error: " << str << std::endl;
    std::abort();
    //exit(1);
  }

  __attribute__ ((noreturn)) void felisce_assert(const char * str,const char * file,int line) {
    (void) str;
    std::cout << file << ":" << line << ": error: Assertion failed." << std::endl;
    std::abort();
    //exit(1);
  }

  __attribute__ ((noreturn)) void felisce_assert(std::string str,const char * file,int line) {
    (void) str;
    std::cout << file << ":" << line << ": error: Assertion failed." << std::endl;
    std::abort();
    //exit(1);
  }

  void felisce_assert_equal(const char* expr_a, const char* expr_b, int a, int b, const char * file,int line) {
    if (a!=b) {
      printf("%s:%i: error: Expected '%s == %s', got: '%i != %i'.\n", file,line,expr_a,expr_b,a,b);
      std::abort();
      //exit(1);
    }
  }

  void felisce_assert_lt(const char* expr_a, const char* expr_b, int a, int b, const char * file,int line) {
    if (a>=b) {
      printf("%s:%i: error: Expected '%s < %s', got: '%i >= %i'.\n", file,line,expr_a,expr_b,a,b);
      std::abort();
      //exit(1);
    }
  }
#endif

  void felisce_info(const char * str,const char * file,int line) {
    std::cout << file << ":" << line << ": info: " << str << std::endl;
  }

  void felisce_warning(const char * str,const char * file,int line) {
    std::cout << file << ":" << line << ": warning: " << str << std::endl;
  }

  void felisce_warning(std::string str,const char * file,int line) {
    std::cout << file << ":" << line << ": warning: " << str << std::endl;
  }

  void felisce_warning_if_no_directory(const char* dirName, const std::string dirPath,
                                       const char* file, int line) {
    std::ostringstream msg;
    if (! filesystemUtil::directoryExists(dirPath.c_str()) ) {
      msg << "Directory " << dirName
          << " <" << dirPath << "> "
          << "does not exists.";
      felisce_warning(msg.str(),file,line);
    }
  }

  void initFELiScE() {
    FEL_DEBUG("Init FELiScE");
  }
}
