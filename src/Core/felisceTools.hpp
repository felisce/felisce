//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Sebastien Gilles
//

#ifndef _FELISCE_TOOLS_HPP_
#define _FELISCE_TOOLS_HPP_

// System includes
#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>
#include <set>
#include <unordered_map>
#include <sstream>
#include <fstream>

// External includes
#include "Core/NoThirdPartyWarning/Mpi/mpi.hpp"

// Project includes
#include "Core/felisce_error.hpp"
#include "Core/mpiInfo.hpp"

namespace felisce {
  namespace Tools {

    static constexpr double ZeroTolerance = std::numeric_limits<double>::epsilon();

    template<class T,class J>
    inline bool insideVec(const J& vec, const T& elem) {
      return find( vec.begin(), vec.end(), elem) != vec.end();
    }

    //! supposed to work for ublas
    template< class M>
    double trace(const M& mat) {
      double tr(0);
      for(std::size_t i(0); i<mat.size1(); ++i) {
        tr+=mat(i,i);
      }
      return tr;
    }
    
    void allGatherSet( std::set<int> &locCont,  std::set<int> &globCont );

    //! Safe comparison between floating-point numbers
    template<class T>
    bool equal(T lvalue, T rvalue) {
      return ( std::fabs( rvalue - lvalue ) <= ZeroTolerance );
    }

    //! Safe comparison between floating-point numbers
    template<class T>
    bool equalTol(T lvalue, T rvalue, double tol) {
      return ( std::fabs( rvalue - lvalue ) <= tol );
    }

    //! Safe comparison between floating-point numbers
    template<class T>
    bool almostEqual(T lvalue, T rvalue, double rTol) {
      return ( std::fabs( rvalue - lvalue ) <= rTol * std::max(std::fabs(rvalue),std::fabs(lvalue)) );
    }

    //! Safe comparison between floating-point numbers
    template<class T>
    bool notEqual(T lvalue, T rvalue) {
      return ( std::fabs( rvalue - lvalue ) > ZeroTolerance );
    }

    template< class T>
    void saveVectorInFile( std::vector<T> v, std::string filename, std::string folder ) {
      std::stringstream filenameWithFolder;
      if ( folder[folder.size()-1] != '/') {
        folder.push_back('/');
      }
      filenameWithFolder<<folder<<filename;
      std::ofstream outputFile( filenameWithFolder.str().c_str(), std::ios::trunc );
      for ( unsigned int i(0); i<v.size(); i++ ) {
        outputFile<<v[i]<<std::endl;
      }
      outputFile.close();
    }    

	template< class T>
    bool loadVectorFromFile( std::vector<T> &v, std::string filename, std::string folder ) {
      std::stringstream filenameWithFolder;
      if ( folder[folder.size()-1] != '/') {
        folder.push_back('/');
      }
      filenameWithFolder<<folder<<filename;
      T tmp;
      std::ifstream inputFile( filenameWithFolder.str().c_str());
      while ( inputFile >> tmp ) {
        v.push_back(tmp);
      }
      if ( !inputFile.eof() ) {
        std::cout << "[error reading] "<<filename<<std::endl;
        return false;
      }
      inputFile.close();
      return true;
    }   
    
    template< class T>
    void printVector( std::vector<T> v) {
      std::cout<< " Printing the std::vector "<<std::endl;
      for ( unsigned int i(0); i<v.size(); i++ ) {
        std::cout<<v[i]<<"\t";
      }
      std::cout<<std::endl;
      std::cout<<" End of the std::vector" <<std::endl;
    }
    template< class T>
    void printVector( std::vector<T> v, std::string nameOfTheVector, std::ostream& out=std::cout,bool noEnd=false) {
      out<< " "<<nameOfTheVector<<" : "<<std::endl;
      for ( unsigned int i(0); i<v.size(); i++ ) {
        out<<v[i]<<"\t";
      }
      out<<std::endl;
      if(!noEnd)
        out<<" End of the "<<nameOfTheVector<<std::endl;
    }

    template < class T>
    bool pairComparison(std::pair<int, T > a, std::pair<int, T > b) {
      return a.first < b.first;
    }
    
    template < class T>
    bool lgPairComparison(std::pair<int, T > a, std::pair<int, T > b) {
      if ( a.second.x() < b.second.x() )
        return true;
      else if ( a.second.x() > b.second.x() )
        return false;
      if ( a.second.y() < b.second.y() )
        return true;
      else if ( a.second.y() > b.second.y() )
        return false;
      if ( a.second.z() < b.second.z() )
        return true;
      else if ( a.second.z() > b.second.z() )
        return false;
      if ( a.first < b.first )
        return true;
      else if ( a.first > b.first )
        return false;
      else 
        return false;
    }

    template < class T, class Poin> // This function is meant to be used only with Poin==Point, I used a template only to avoid including geometry/point.hpp which includes felisceTools.hpp as well.
    // It was not possible to use a forward declaration because you can not call, for instance, the x() method on an incomplete type, but you can do that on a template parameter!
    void allGatherSetPair( 
      std::set<std::pair<int,Poin>,T> &locCont,  
      std::set<std::pair<int,Poin>,T> &globCont 
      ) 
    {
      double *locContainerAsVec = new double[ 4 * locCont.size()];
      int locCount(0), globCount(0);

      for ( auto it = locCont.begin(); it != locCont.end(); it++ ) {
        locContainerAsVec[locCount] = static_cast<double>( it->first );
        locCount++;
        locContainerAsVec[locCount] = it->second.x();
        locCount++;
        locContainerAsVec[locCount] = it->second.y();
        locCount++;
        locContainerAsVec[locCount] = it->second.z();
        locCount++;        
      }
        
      MPI_Allreduce(&locCount, &globCount,1,MPI_INT,MPI_SUM,MpiInfo::petscComm());

      double globContainerAsVec[globCount];
  
      int displacements[MpiInfo::numProc()];
      int localCountBroadcasted[MpiInfo::numProc()];
      int ones[MpiInfo::numProc()];

      for ( int proc(0); proc < MpiInfo::numProc(); proc++) {
        localCountBroadcasted[proc]=0;
        displacements[proc] = proc;
        ones[proc] = 1;
      }
      
      MPI_Allgatherv(&locCount,1,MPI_INT,localCountBroadcasted,ones,displacements,MPI_INT,MpiInfo::petscComm());
      
      displacements[0]=0;
      for ( int proc(0); proc < MpiInfo::numProc()-1; proc++) {
        displacements[proc+1] = displacements[proc] + localCountBroadcasted[proc];
      }
      
      MPI_Allgatherv(locContainerAsVec,locCount,MPI_DOUBLE,globContainerAsVec,localCountBroadcasted,displacements, MPI_DOUBLE,MpiInfo::petscComm());

      for ( int k(0); k< globCount; ) {
        int theInt = static_cast<int>(round(globContainerAsVec[k]));
        Poin P(globContainerAsVec[k+1],globContainerAsVec[k+2],globContainerAsVec[k+3]);
        k=k+4;
        globCont.insert( std::make_pair(theInt,P));
      }
    }
    
    // this class inherits from the regular std::unordered_map
    // it is just a decoration of the regular std::unordered_map.
    // Get who check before accessing the std::unordered_map ( operator[] would have 
    // initialized a new element of the std::unordered_map)
    // I think it is usefull for small maps and it helps a lot the debugging phase
    // I added also an Init for readibility
    // and at operator could be usufull later
    template <class K, class V>
    class mapHolder 
      : public std::unordered_map < K , V >
    {
    public:
      // just for readibility
      inline void Init(K key) {this->operator[](key);}
      
      inline V& Get(K key) { 
        if ( this->count(key)>0 )
          return this->operator[](key);
        std::stringstream err;
        err<<"accessing a non-existing key: "<<key;
        FEL_ERROR(err.str().c_str());
        return this->operator[](key);
      }

      // only similar to the at of c++11..
      // while waiting for it..
      inline const V& At(K key) { return *(this->find(key)); }

    };
  } // namespace Tools
} // namespace felisce


#endif // _FELISCE_TOOLS_HPP_
