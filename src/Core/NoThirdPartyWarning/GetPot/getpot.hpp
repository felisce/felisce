//
//  getpot.hpp
//  HappyHeartNative
//
//  Created by Sébastien Gilles on 11/10/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef FELISCE_GETPOT_HPP
#define FELISCE_GETPOT_HPP

#include "Core/NoThirdPartyWarning/pragmaMacro.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")

#include "GetPot"

PRAGMA_DIAGNOSTIC(pop)

#endif // FELISCE_GETPOT_HPP
