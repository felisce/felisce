//
//  Mpi.hpp
//  HappyHeartNative
//
//  Created by Sébastien Gilles on 11/10/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef _FELISCE_MPI_HPP_
# define _FELISCE_MPI_HPP_

# include "Core/NoThirdPartyWarning/pragmaMacro.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")

#ifdef __clang__
PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
PRAGMA_DIAGNOSTIC(ignored "-Wdisabled-macro-expansion")
PRAGMA_DIAGNOSTIC(ignored "-Wsometimes-uninitialized")
#endif

#include <mpi.h>

PRAGMA_DIAGNOSTIC(pop)

#endif // _FELISCE_MPI_HPP_
