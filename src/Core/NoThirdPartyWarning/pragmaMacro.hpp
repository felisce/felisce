//
//  PragmaMacro.hpp
//
//  Created by Sebastien Gilles on 27/02/14.
//  Copyright (c) 2014 Inria. All rights reserved.
//
#ifndef _FELISCE_PRAGMA_MACRO_HPP_
#define _FELISCE_PRAGMA_MACRO_HPP_
#define STR_EXPAND(tok) #tok
/*!
 * \brief STR transform into a std::string litteral the tok argument.
 *
 * Why we need STR_EXPAND is honestly a bit blurry for me, but we really need it.
 */
#define STR(tok) STR_EXPAND(tok)
/*!
 * \brief Helper to define PRAGMA_DIAGNOSTIC.
 *
 * It will choose the syntax to employ depending on the compiler used (clang and gcc supported at the moment).
 */
#ifdef __clang__
#define PREPARE_PRAGMA_STRING(tok) clang diagnostic tok
# ifdef __APPLE__
#define LLVM_VERSION (__clang_major__ * 100 + __clang_minor__ ) // called this way because on Mac it returns LLVM
// versions rather than clang.
# endif // __APPLE__
#elif __GNUC__
#define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#if GCC_VERSION > 40600
#define PREPARE_PRAGMA_STRING(tok) GCC diagnostic tok
#else
#define PRAGMA_DIAGNOSTIC(tok)
#endif // GCC_VERSION
#endif // __clang__
/*!
 * \brief The interesting one: apply a pragma diagnostic directive.
 *
 * \param[in] tok Token given to the macro. It is not a std::string litteral yet; do no put double quotes around this argument.
 *
 * \code
 * #PRAGMA_DIAGNOSTIC(push)
 * #PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
 * #PRAGMA_DIAGNOSTIC(ignored "-Wcast-align")
 * #include "parmetis.h"
 * #PRAGMA_DIAGNOSTIC(pop)
 *
 * \endcode
 *
 * The advantage over a direct call to _Pragma is that the compiler is automatically handled.
 * (without that the line would be #pragma clang diagnostic ignored "-Wconversion" for clang
 * and #pragma GCC diagnostic ignored "-Wconversion" for GCC, with a directive condition to rule out between both).
 */
#ifdef __clang__
#define PRAGMA_DIAGNOSTIC(tok) _Pragma(STR(PREPARE_PRAGMA_STRING(tok)))
#elif __GNUC__
#if GCC_VERSION > 40600
#define PRAGMA_DIAGNOSTIC(tok) _Pragma(STR(PREPARE_PRAGMA_STRING(tok)))
#else
#define PRAGMA_DIAGNOSTIC(tok)
#endif // GCC_VERSION
#endif
#endif // _FELISCE_PRAGMA_MACRO_HPP_
