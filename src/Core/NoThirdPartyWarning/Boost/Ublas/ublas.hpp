//
//  ublas.hpp
//  FelisceM3DISIM
//
//  Created by Sebastien Gilles on 28/02/14.
//  Copyright (c) 2014 Inria. All rights reserved.
//

#ifndef _FELISCE_UBLAS_HPP_
# define _FELISCE_UBLAS_HPP_

# include "Core/NoThirdPartyWarning/pragmaMacro.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wshadow")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")
PRAGMA_DIAGNOSTIC(ignored "-Wswitch-enum")
PRAGMA_DIAGNOSTIC(ignored "-Wsign-compare")
PRAGMA_DIAGNOSTIC(ignored "-Wunused-function")
PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")

#ifdef __clang__
#  if LLVM_VERSION > 500
PRAGMA_DIAGNOSTIC(ignored "-Wextra-semi")
#  endif // LLVM_VERSION
PRAGMA_DIAGNOSTIC(ignored "-Wweak-vtables")
PRAGMA_DIAGNOSTIC(ignored "-Wc++98-compat")
PRAGMA_DIAGNOSTIC(ignored "-Wc++98-compat-pedantic")
PRAGMA_DIAGNOSTIC(ignored "-Wdeprecated")
#endif

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/vector_expression.hpp>
#include <boost/numeric/ublas/storage.hpp>
#include <boost/numeric/ublas/detail/vector_assign.hpp>


PRAGMA_DIAGNOSTIC(pop)

#endif // _FELISCE_UBLAS_HPP_
