#ifndef _FELISCE_SCIPLOT_HPP_
#define _FELISCE_SCIPLOT_HPP_

#include "Core/configure.hpp"

#ifdef FELISCE_WITH_SCIPLOT

#include "Core/NoThirdPartyWarning/pragmaMacro.hpp"

// TODO: Fix the warnings code and do a PR
PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wunused-variable")
PRAGMA_DIAGNOSTIC(ignored "-Wreorder")
PRAGMA_DIAGNOSTIC(ignored "-Wsign-compare")
PRAGMA_DIAGNOSTIC(ignored "-Wsuggest-override")

#include <sciplot/sciplot.hpp>

PRAGMA_DIAGNOSTIC(pop)

#endif
#endif // _FELISCE_SCIPLOT_HPP_
