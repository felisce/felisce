//
//  PetscViewer.hpp
//  HappyHeartNative
//
//  Created by Sebastien Gilles on 06/02/14.
//  Copyright (c) 2014 Inria. All rights reserved.
//

#ifndef _FELISCE_PETSC_VIEWER_HPP_
# define _FELISCE_PETSC_VIEWER_HPP_

# include "Core/NoThirdPartyWarning/pragmaMacro.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")

#include <petscviewer.h>

PRAGMA_DIAGNOSTIC(pop)


#endif // _FELISCE_PETSC_VIEWER_HPP_
