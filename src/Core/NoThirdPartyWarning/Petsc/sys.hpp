//
//  PetscSys.hpp
//  HappyHeartNative
//
//  Created by Sebastien Gilles on 10/12/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef _FELISCE_PETSC_SYS_HPP_
# define _FELISCE_PETSC_SYS_HPP_

# include "Core/NoThirdPartyWarning/pragmaMacro.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")

#include <petscsys.h>

PRAGMA_DIAGNOSTIC(pop)

// depreciated name macro
#if PETSC_VERSION_LT(3, 19, 0)
#define FELISCE_PETSC_NULLPTR PETSC_NULL
#else
#define FELISCE_PETSC_NULLPTR PETSC_NULLPTR
#endif

#endif // _FELISCE_PETSC_SYS_HPP_
