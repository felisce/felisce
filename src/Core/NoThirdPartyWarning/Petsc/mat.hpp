//
//  Petsc.hpp
//  HappyHeartNative
//
//  Created by Sébastien Gilles on 11/10/13.
//  Copyright (c) 2013 Inria. All rights reserved.
//

#ifndef _FELISCE_PETSC_MAT_HPP_
# define _FELISCE_PETSC_MAT_HPP_

# include "Core/NoThirdPartyWarning/pragmaMacro.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")

#include <petscmat.h>

PRAGMA_DIAGNOSTIC(pop)


#endif // _FELISCE_PETSC_MAT_HPP_
