#ifndef _FELISCE_SLEPC_HPP_
#define _FELISCE_SLEPC_HPP_

#include "Core/NoThirdPartyWarning/pragmaMacro.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")

#include <slepc.h> 

PRAGMA_DIAGNOSTIC(pop)


#endif // _FELISCE_SLEPC_HPP_
