#ifndef _FELISCE_SLEPC_SVD_HPP_
#define _FELISCE_SLEPC_SVD_HPP_

#include "Core/NoThirdPartyWarning/pragmaMacro.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wunused-parameter")

#include <slepcsvd.h>

PRAGMA_DIAGNOSTIC(pop)


#endif // _FELISCE_SLEPC_SVD_HPP_
