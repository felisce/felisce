#ifndef _FELISCE_SLEPC_EPS_HPP_
#define _FELISCE_SLEPC_EPS_HPP_

#include "Core/NoThirdPartyWarning/pragmaMacro.hpp"


PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wfloat-equal")

#include <slepceps.h> 

PRAGMA_DIAGNOSTIC(pop)


#endif // _FELISCE_SLEPC_EPS_HPP_
