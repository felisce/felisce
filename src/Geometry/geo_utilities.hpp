//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    V.Martin, L.Boilevin-Kayl
//

#ifndef GEO_UTILITIES_HPP
#define GEO_UTILITIES_HPP

// System includes

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/point.hpp"

namespace felisce
{

class GeoUtilities
{
  public:
    ///@name Type Definitions
    ///@{

    enum class Position : unsigned short int 
    {
      Outside = 0,
      OnVertex = 1,
      OnEdge = 2,
      OnFace = 3,
      Inside = 4
    };

    using Location = std::pair<Position,unsigned short int>;

    ///@}
    ///@name Public static Member Variables
    ///@{

    static constexpr double GEO_TOL = 1.e-5;

    ///@}
    ///@name Public static Member Methods
    ///@{

    // Check if a point is insede an element
    static bool isPointInElement(const GeometricMeshRegion::ElementType eltType, const std::vector<Point*>& pointsOfElt, const Point& point, const double tol);

    // Check if a point is insede a tetrahedron
    static bool isPointInTetra(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 4>& bary, const double tol);

    // Check if a point is insede a triangle
    static bool isPointInTri(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 3>& bary, const double tol);

    // Check if a point is insede a segment
    static bool isPointInSeg(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 2>& bary, const double tol);

    // Check if a point is insede a tetrahedron, returns where the point is wrt the tetrahedron
    static bool isPointInTetra(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 4>& bary, Location& location, const double tol);

    // Check if a point is insede a triangle, returns where the point is wrt the triangle
    static bool isPointInTri(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 3>& bary, Location& location, const double tol);

    // Compute projection of a point on the boundaries of an element
    static void getClosestProjectionOnElement(const GeometricMeshRegion::ElementType eltType, const std::vector<Point*>& pointsOfElt, const Point& point, Point& projection, const double tol);

    // Get closest projection on segment
    static void getClosestProjectionSeg(const std::vector<Point*>& pointsOfElt, const Point& point, Point& projection, const double tol);

    // Get closest projection on triangle
    static void getClosestProjectionTri(const std::vector<Point*>& pointsOfElt, const Point& point, Point& projection, const double tol);

    // Compute element normal
    static void computeElementNormal(const GeometricMeshRegion::ElementType eltType, const std::vector<Point*>& pointsOfElt, Point& normal);

    // Compute triangle normal
    static void triNormal(const Point& p0, const Point& p1, const Point& p2, Point& norm);

    // Compute tetrahedron volume
    static double tetraVolume(const Point& p0, const Point& p1, const Point& p2, const Point& p3);
    
    // Compute triangle area
    static double triVolume(const Point& p0, const Point& p1, const Point& p2, const Point& norm = Point(0,0,1));
    
    // Compute tetrahedron pseudo-bary
    static void tetraBary(const std::vector<Point*>& eltPoints, const Point& point, std::array<double, 4>& bary);
    
    // Compute triangle pseudo-bary
    static void triBary(const std::vector<Point*>& eltPoints, const Point& point, const Point& norm, std::array<double, 3>& bary);

    // Compute point projection on geometric entity
    static void projectOnGeomEntity(const felInt dim, const std::vector<Point*>& eltPoints, const Point& point, Point& projection);

    // Compute point projection on an edge
    static void projectOnLine(const std::vector<Point*>& eltPoints, const Point& p, Point& projection);

    // Compute point projection on a plane
    static void projectOnPlane(const std::vector<Point*>& eltPoints, const Point& point, Point& projection);

    // Compute intersection segment - segment
    static int computeSegmentsIntersection(const Point& P0, const Point& P1, const Point& Q0, const Point& Q1, Point& intPnt0, Point& intPnt1, const double tol);

    // Compute intersection ray vs triangle
    static int computeRayTriangleIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol);
    
    // Compute intersection segment vs triangle
    static int computeSegmentTriangleIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol);

    // Compute intersection ray vs plane
    static int computeRayPlaneIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol);
    
    // Compute intersection segment vs plane
    static int computeSegmentPlaneIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol);

    // Compute intersection plane - plane
    static int computePlanesIntersection(const Point& P0, const Point& P1, const Point& P2, const Point& Q0, const Point& Q1, const Point& Q2, Point& p0Line, Point& p1Line, const double tol);

    // Create the map of matching points (in the boundary elements label1 and label2) from mesh1 to mesh2 
    static void findMatchingPoints(GeometricMeshRegion* mesh1, const std::vector<int>& label1, GeometricMeshRegion* mesh2, const std::vector<int>& label2, std::map<felInt,felInt>& link, const double tol);

    ///@}
};

}
#endif
