//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef SURFACEINTERPOLATOR_HPP
#define SURFACEINTERPOLATOR_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Solver/linearProblem.hpp"
#include "DegreeOfFreedom/dofBoundary.hpp"
#include "PETScInterface/petscMatrix.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce 
{
class SurfaceInterpolator {
public:
  typedef GeometricMeshRegion::ElementType ElementType;
  
  SurfaceInterpolator()= default;;
  void initSurfaceInterpolator(LinearProblem* pb, DofBoundary* dofbd) {
    std::vector<DofBoundary*> tmp(1, dofbd);
    this->initSurfaceInterpolator(pb,tmp);
  }
  void initSurfaceInterpolator(LinearProblem* pb, std::vector<DofBoundary*> dofbd) {
    m_pb = pb;
    m_dofBD = dofbd;
    m_nrows.resize(dofbd.size());
    m_ncols.resize(dofbd.size());
    m_mesh = pb->mesh().get();
  }
  void writeInterfaceFile(felInt iunknown, felInt numComp, std::string filename, std::string folder, std::size_t iBD=0 ) const;
  void readInterfaceFile(std::string filename, std::vector< std::pair< Point*, felInt > >& pointsAndCorrespondingSurfLabel, std::vector<int>& rowNumbering, std::string folder = FelisceParam::instance().resultDir);
  void buildInterpolator( PetscMatrix& interpolator,
                          const std::vector< std::pair< Point*, felInt > >& pointsAndCorrespondingSurfLabel,
                          const std::vector<int> rowNumbering,
                          const Variable& var, felInt ivar, std::size_t iBD=0);
  void buildInterpolator( PetscMatrix& interpolator,
                          const std::vector< std::pair< Point*, felInt > >& pointsAndCorrespondingSurfLabel,
                          const std::vector<int> rowNumbering,
                          const Variable& var, felInt ivar, std::size_t iBD, const std::vector<int> alternateLabels, std::string logFile);

  int nrows(std::size_t iBD=0) {return m_nrows[iBD];}
  int ncols(std::size_t iBD=0) {return m_ncols[iBD];}

  void initializeSurfaceInterpolator();

  /**
   * @param thePoint         [in]  the point to be projected
   * @param surfaceLabel     [in]  the label of the surface where we are looking for the point
   * @param eltTypeReturn    [out] if the point is found into one of the boundary element this is his type
   * @param elemPointReturn  [out] if the point is found into one of the boundary element this are his points
   * @param projection       [out] if the point is found into one of the boundary element this is its projection onto the element plane
   * @param ielSupportDof    [out] ielSupportDof of the element
   * @param iel2ScoreByEdge  [out] if the point is NOT found into one of the boundary element it contains a std::unordered_map from an ielSupportDofByType to the best edge distance, if the point is found on the element this values are incomplete and should not be used
   * @param distance         [in/out] smaller distant from a single point, this value is updated only if it is better than the previous results
   * @param idElOfClosestPoint [in/out] the id of the el by typt closest point, this value is updated only if it is better than the previous results
   */
  int getClosestSurfaceElement(
    Point* thePoint,
    felInt surfaceLabel, ElementType& eltTypeReturn,
    std::vector<Point*>& elemPointReturn,
    Point& projection, felInt& ielSupportDof,
    std::map<int,double>& iel2ScoreByEdge,
    double& distance,
    int& idElOfClosestPoint
    );

  int lookIntoStar(
    int idClosestPoint,
    int surfaceLabel,
    Point* thePoint,
    ElementType& eltTypeReturn,
    std::vector<Point*>& elemPointReturn,
    Point& projection,
    felInt& ielSupportDof,
    felInt& depth,
    double& score,
    std::map<int,double>& iel2ScoreByEdge
  );

  void displayDataForSurfaceInterpolator();

  ElementType getETandAssert2D(std::set<ElementType> setOfET);

  inline const bool& surfaceInterpolatorInitialized() const {
    return m_surfaceInterpolatorInitialized;
  }
  inline bool& surfaceInterpolatorInitialized() {
    return m_surfaceInterpolatorInitialized;
  }

private:
  void checkInterpolatorStatistics(int foundInTria, int foundInTriaAltLab, int foundInEdge, int foundIntPoint, int pointNotFound, int nPoints ) const;


  // Is D inside ABC?
  bool isPointInTriangle(const double *A, const double *B,const double *C, const double *D, double& alpha, double& beta);
  bool isPointInEdgeAndIfYesHowFar( const double *p0,const double *p1,const double* thePoint,double& distance, double& t); 

  void checkEdge(const double *p0,const double *p1,Point* thePoint,double& bestDistance);
  bool checkTriangle( const Point& projection, const std::vector<Point*>& elemPoint, double& alpha, double& beta, Point* thePoint, double& bestEdgeDistance );
  bool checkQuadrangle( const Point& projection, const std::vector<Point*>& elemPoint, double& alpha, double& beta, Point* thePoint, double& bestEdgeDistance );

  void projectOnElementPlane( const std::vector<Point*>& elemPoint, Point* thePoint, Point& projection );
  void projectOnBestEdge( const std::vector<Point*>& elemPoint, Point* thePoint, Point& projection);
  void projectOnBestPoint( const std::vector<Point*>& elemPoint, Point* thePoint, Point& projection);
  void projectOnEdge(const double *p0,const double *p1,const double* thePoint, double& t);

  double computeScore(double alpha, double beta);
  void updateBestScore( double alpha, double beta, double& bestScore, double& alphabest, double& betabest);
  int closestPoint(const std::set< std::pair< Point*, felInt > >& setOfPointsAndIds, const Point* thePoint, Point*& closest, double& distance);


  LinearProblem* m_pb;
  std::vector<DofBoundary*> m_dofBD;

  std::vector<int> m_nrows;
  std::vector<int> m_ncols;

  GeometricMeshRegion* m_mesh;

  std::map< /*surfLabel*/ felInt, std::set< std::pair<Point*, /*idPoint*/ felInt> > > m_surfLab2SetOfPointsAndIds;
  std::map< /*surfLabel*/ felInt, std::map< /*IdPoint*/ felInt, /*star*/std::vector< /*idByType*/ felInt> > > m_surfLabelAndIdPoint2Star;

  bool m_surfaceInterpolatorInitialized = false;
};
}
#endif
