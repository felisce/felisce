//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Matteo
//

// System includes
#include <vector>

// External includes

// Project includes
#include "Geometry/curvatures.hpp"

namespace felisce 
{
Curvatures::Curvatures() {
  m_minProportion=FelisceParam::instance().fiberDispersionParameter;
}

void Curvatures::update(const ElementField& normal, const UBlasMatrix& firstFundForm, const UBlasMatrix& covBasis) {

  m_firstFundForm = firstFundForm;

  computeSecondFundamentalForm(covBasis,normal);
  computeShapeOperator();
  computeCurvatures();
  computePrincipalDirections();
  computeArcLengths();
  computeProportions();
  computeProjectors();
  computeEigInCartCoordinate(covBasis,normal);

}

void Curvatures::computeEigInCartCoordinate(const UBlasMatrix& covBasis, const ElementField& normal ) {
  m_eigMax.resize(3);
  m_eigMin.resize(3);
  for (int c(0); c<3; c++) {
    m_eigMax(c) = m_maxEigVec(0)*covBasis(0,c) + m_maxEigVec(1)*covBasis(1,c);
    m_eigMin(c) = m_minEigVec(0)*covBasis(0,c) + m_minEigVec(1)*covBasis(1,c);
  }
  m_eigMax = m_eigMax/norm_2(m_eigMax);
  m_eigMin = m_eigMin/norm_2(m_eigMin);
  
  UBlasVector ones(3);
  
  ones(0) = 1;
  ones(1) = 1;
  ones(2) = 0;
  if ( inner_prod ( m_eigMin, ones ) < 0 )
    m_eigMin = -1*m_eigMin;

  UBlasVector n(3);
  n(0)=normal.val(0,0);
  n(1)=normal.val(1,0);
  n(2)=normal.val(2,0);
  
  UBlasVector crossProd(3);
  crossProd(0) = m_eigMax(1)*m_eigMin(2) - m_eigMax(2)*m_eigMin(1);
  crossProd(1) = m_eigMax(2)*m_eigMin(0) - m_eigMax(0)*m_eigMin(2);
  crossProd(2) = m_eigMax(0)*m_eigMin(1) - m_eigMax(1)*m_eigMin(0);

  if ( inner_prod(crossProd, n) < 0 )
    m_eigMax = -1*m_eigMax;
}

void Curvatures::computeSecondFundamentalForm( const UBlasMatrix& covBasis, const ElementField& normal ) {

  felInt dimension = 3;
  std::vector<double> d10,d20;
  d10.resize(dimension,0.0);
  d20.resize(dimension,0.0);

  // gradient of the normal
  for ( int c(0.0); c<dimension; ++c) {
    d10[c] = normal.val(c,1) - normal.val(c,0);
    d20[c] = normal.val(c,2) - normal.val(c,0);
  }

  double e(0),f1(0),f2(0),g(0);
  // second fundamental form b_{\alpha,\beta} = - \partial_{\alpha}a_3 \dot a_{\beta}
  for ( int c(0); c < dimension; ++c) {
    e  += -d10[c]*covBasis(0,c);
    f1 += -d10[c]*covBasis(1,c);
    f2 += -d20[c]*covBasis(0,c);
    g  += -d20[c]*covBasis(1,c);
  }

  m_secondFundForm.resize(2,2);
  // numerically it is not symmetric -> we take its symmetric part
  m_secondFundForm(0,0) = e;
  m_secondFundForm(1,1) = g;
  m_secondFundForm(0,1) = (f1 + f2)*0.5;
  m_secondFundForm(1,0) = (f1 + f2)*0.5;
}

void Curvatures::computeShapeOperator() {

  m_invFFT.resize(2,2);

  m_invFFT(0,0) = m_firstFundForm(1,1);
  m_invFFT(1,1) = m_firstFundForm(0,0);

  m_invFFT(0,1) = - m_firstFundForm(0,1);
  m_invFFT(1,0) = - m_firstFundForm(1,0);

  m_invFFT =  m_invFFT/( m_firstFundForm(0,0)*m_firstFundForm(1,1)
                          -m_firstFundForm(0,1)*m_firstFundForm(1,0) );

  m_shapeOperator = prod( m_invFFT, m_secondFundForm);

}

void Curvatures::computeCurvatures() {

  double tr(0.0),det(1.0);
  tr = ( m_shapeOperator(0,0) + m_shapeOperator(1,1) );
  det =  m_shapeOperator(0,0)*m_shapeOperator(1,1) - m_shapeOperator(0,1)*m_shapeOperator(1,0);

  // max and min here refer to the case of normal going inside.
  // since the normal is going outside the domain they are swapped
  m_maxCurv = (tr - std::sqrt( tr*tr - 4*det ) )/2;
  m_minCurv = (tr + std::sqrt( tr*tr - 4*det ) )/2;

  m_meanCurv = tr/2;
  m_gaussCurv = det;

}

void Curvatures::print() const {
  std::cout<<"================CURVATURES================="<<std::endl;
  std::cout<<" Kmax:                     "<<m_maxCurv<<std::endl;
  std::cout<<" Kmin:                     "<<m_minCurv<<std::endl;
  std::cout<<" GaussK:                   "<<m_gaussCurv<<std::endl;
  std::cout<<" MeanK:                    "<<m_meanCurv<<std::endl;
  std::cout<<" Principal direction ( max "<<m_maxEigVec<<std::endl;
  std::cout<<" Principal direction ( min "<<m_minEigVec<<std::endl;
  std::cout<<" Length Arc (lambda max)   "<<m_rayleighMaxEigVec<<std::endl;
  std::cout<<" Length Arc (lambda min)   "<<m_rayleighMinEigVec<<std::endl;
  std::cout<<" Proportions: max:         "<<m_maxProp<<std::endl;
  std::cout<<" Proportions: min:         "<<m_minProp<<std::endl;
  std::cout<<" Projectors: max:          "<<m_projOnMaxEigVec<<std::endl;
  std::cout<<" Projectors: min:          "<<m_projOnMinEigVec<<std::endl;
  std::cout<<"==========================================="<<std::endl;
}

void Curvatures::computePrincipalDirections() {

  UBlasMatrix Smin(2,2), Smax(2,2);
  double toll( std::max( 1e-4 * norm_frobenius(m_shapeOperator), 1e-10 )); //it used to be std::set to 0.01

  m_maxEigVec.resize(2);
  m_minEigVec.resize(2);

  Smin = m_shapeOperator - m_minCurv*( UBlasIdentityMatrix ( 2 ) );
  Smax = m_shapeOperator - m_maxCurv*( UBlasIdentityMatrix ( 2 ) );

  if ( std::fabs( Smin(0,0) ) > toll ) {
    m_minEigVec(0) = -Smin(0,1)/Smin(0,0);
    m_minEigVec(1) = 1;
  } else if ( std::fabs( Smin(0,1) ) > toll ) {
    m_minEigVec(0) = 1;
    m_minEigVec(1) = -Smin(0,0)/Smin(0,1);
  } else if ( std::fabs( Smin(1,1) ) > toll ) {
    m_minEigVec(0) = 1;
    m_minEigVec(1) = -Smin(1,0)/Smin(1,1);
  } else if ( std::fabs( Smin(1,0) ) > toll ) {
    m_minEigVec(0) = -Smin(1,1)/Smin(1,0);
    m_minEigVec(1) = 1;
  } else if (   std::fabs( Smin(0,0) )
                + std::fabs( Smin(1,0) )
                + std::fabs( Smin(0,1) )
                + std::fabs( Smin(1,1) ) < toll ) {
    m_minEigVec(0) = 1;
    m_minEigVec(1) = 0;
  } else {
    std::cout<<"toll : "<<toll<<std::endl;
    std::cout<<"Shape operator: ";
    std::cout<<m_shapeOperator<<std::endl;
    std::cout<<"Shape operator - lambda I:";
    std::cout<<Smin<<std::endl;
    FEL_ERROR(" TODO in curvatures.cpp ");
  }

  m_minEigVec = m_minEigVec/norm_2(m_minEigVec);


  if ( std::fabs( Smax(0,0) ) > toll ) {
    m_maxEigVec(0) = -Smax(0,1)/Smax(0,0);
    m_maxEigVec(1) = 1;
  } else if ( std::fabs( Smax(0,1) ) > toll ) {
    m_maxEigVec(0) = 1;
    m_maxEigVec(1) = -Smax(0,0)/Smax(0,1);
  } else if ( std::fabs( Smax(1,1) ) > toll ) {
    m_maxEigVec(0) = 1;
    m_maxEigVec(1) = -Smax(1,0)/Smax(1,1);
  } else if ( std::fabs( Smax(1,0) ) > toll ) {
    m_maxEigVec(0) = -Smax(1,1)/Smax(1,0);
    m_maxEigVec(1) = 1;
  } else if (   std::fabs( Smax(0,0) )
                + std::fabs( Smax(1,0) )
                + std::fabs( Smax(0,1) )
                + std::fabs( Smax(1,1) ) < toll ) {
    m_maxEigVec(0) = 0;
    m_maxEigVec(1) = 1;
  } else {
    std::cout<<"toll : "<<toll<<std::endl;
    std::cout<<"Shape operator: ";
    std::cout<<m_shapeOperator<<std::endl;
    std::cout<<"Shape operator - lambda I:";
    std::cout<<Smin<<std::endl;
    FEL_ERROR(" TODO in curvatures.cpp");
  }

  m_maxEigVec = m_maxEigVec/norm_2(m_maxEigVec);
  
}

void Curvatures::computeArcLengths() {

  m_rayleighMaxEigVec = std::sqrt( inner_prod( m_maxEigVec,
                                          prod( m_firstFundForm, m_maxEigVec )
                                        ));

  m_rayleighMinEigVec = std::sqrt( inner_prod( m_minEigVec,
                                          prod( m_firstFundForm, m_minEigVec )
                                        ));
}

void Curvatures::computeProportions() {

  double c(0.5);

  double k1 = m_minCurv;
  double k2 = m_maxCurv;

  double toll = 1;
  if( k1 * k2 < 0 ) { //k2 positive and k1 negative ( saddle point )
    c = 0;
  } else if( k1 + k2 > - toll) {//both negative or both equal to zero
    c = 0.5;
  } else if( k1 <= 0 ) {//both positive
    c = 0.5*k1/k2;
  } else {
    FEL_ERROR(" TODO in curvatures.cpp ");
  }

  m_maxProp =  (1-c)*(1-m_minProportion) + m_minProportion/2;
  m_minProp =    (c)*(1-m_minProportion) + m_minProportion/2;
}

void Curvatures::computeProjectors() {

  m_projOnMaxEigVec = m_maxProp/(m_rayleighMaxEigVec*m_rayleighMaxEigVec)*outer_prod( m_maxEigVec, m_maxEigVec);
  m_projOnMinEigVec = m_minProp/(m_rayleighMinEigVec*m_rayleighMinEigVec)*outer_prod( m_minEigVec, m_minEigVec);

}

UBlasVector Curvatures::testFuncGradient( int idLocDof ) {
  UBlasVector testFuncGrad(2);
  switch(idLocDof) {
  case 0:
    testFuncGrad(0) = -1;
    testFuncGrad(1) = -1;
    break;
  case 1:
    testFuncGrad(0) = 1;
    testFuncGrad(1) = 0;
    break;
  case 2:
    testFuncGrad(0) = 0;
    testFuncGrad(1) = 1;
    break;
  default:
    FEL_ERROR("you can not use testFuncGradient on elements with more than 3 vertices");
    break;
  }
  return testFuncGrad;
}

UBlasVector Curvatures::elemFieldGradient( const ElementField& f ) {
  UBlasVector fGrad(2);
  fGrad(0) = f.val(0,1) - f.val(0,0);
  fGrad(1) = f.val(0,2) - f.val(0,0);
  return fGrad;
}

double Curvatures::mNormOfGradient( const ElementField& f , const UBlasMatrix& M) {
  return inner_prod( elemFieldGradient(f), prod(M,elemFieldGradient(f) ) );
}
}
