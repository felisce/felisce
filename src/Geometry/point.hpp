//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

#ifndef _FEL_POINT_HPP
#define _FEL_POINT_HPP

// System includes
#include <cstdlib>
#include <iostream>
#include <vector>
#include <atomic>
#include <array>

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Core/felisceTools.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

class Point
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Point
  FELISCE_CLASS_INTRUSIVE_POINTER_DEFINITION(Point);

  /// Definition of the size type
  typedef std::size_t SizeType;

  /// Definition of the index type
  typedef std::size_t IndexType;

  /// Toleranc definition
  static constexpr double absToll = 1.e-10;

  ///@}
  ///@name Life Cycle
  ///@{

  // default constructor
  Point() = default;

  // construction from a scalar
  Point(double val) {
    m_coor[0] = val;
    m_coor[1] = val;
    m_coor[2] = val;
  }
  
  // construction from an array
  Point(const std::array<double, 3>& coord) : m_coor(coord) {}

  // construction from a C array
  Point(const double* coord)
  {
    m_coor[0] = coord[0];
    m_coor[1] = coord[1];
    m_coor[2] = coord[2];
  }

  // construction from a std::vector
  Point(const std::vector<double>& coord)
  {
    FEL_ASSERT_EQUAL(coord.size(), 3);
    m_coor[0] = coord[0];
    m_coor[1] = coord[1];
    m_coor[2] = coord[2];
  }

  // construction from three scalars
  Point(const double xval, const double yval, const double zval) 
  {
    m_coor[0] = xval;
    m_coor[1] = yval;
    m_coor[2] = zval;
  }

  // copy constructor
  Point(const Point& P) : m_coor(P.m_coor) {}

  ///@}
  ///@name Operators
  ///@{

  // Assignment.
  Point& operator=(const Point& P) {
    m_coor = P.m_coor;

    return *this;
  }

  inline double operator [](const IndexType  i) const {
    return m_coor[i];
  }

  inline double& operator [] (const IndexType  i) {
    return m_coor[i];
  }

  inline bool operator!=(const Point& rOther) const {
    return ! this->operator==(rOther);
  }

  inline void operator/=(const double Value) {
    m_coor[0] /= Value;
    m_coor[1] /= Value;
    m_coor[2] /= Value;
  }

  inline void operator*=(const double Value) {
    m_coor[0] *= Value;
    m_coor[1] *= Value;
    m_coor[2] *= Value;
  }

  inline void operator+=(const Point& rOther) {
    m_coor[0] += rOther.x();
    m_coor[1] += rOther.y();
    m_coor[2] += rOther.z();
  }

  inline void operator-=(const Point& rOther) {
    m_coor[0] -= rOther.x();
    m_coor[1] -= rOther.y();
    m_coor[2] -= rOther.z();
  }

  inline bool operator==(const Point& rOther) const {
    return
      Tools::equalTol(m_coor[0],rOther.x(),absToll) &&
      Tools::equalTol(m_coor[1],rOther.y(),absToll) &&
      Tools::equalTol(m_coor[2],rOther.z(),absToll);
  }

  friend std::ostream & operator<<(std::ostream& os, const Point& mc) {
      return os <<"P: x = " << mc.x() << " y = " << mc.y() << " z = " << mc.z();
  }

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief This method creates a clone of the current point
   * @return A new pointer with exactly the same coordinates
   */
  Pointer Clone()
  {
      auto p_new_point = felisce::make_intrusive<Point>(this->coor());
      return p_new_point;
  }

  // Method to compute the norm of the coordinates
  inline double norm() const {
    double norm = 0.;
    for(std::size_t icoor=0; icoor<3; ++icoor) {
        norm += std::pow(m_coor[icoor], 2);
    }
    return std::sqrt(norm);
  }

  // coordinate getters
  inline double x() const {
    return m_coor[0];
  }
  inline double y() const {
    return m_coor[1];
  }
  inline double z() const {
    return m_coor[2];
  }
  inline const double* coor() const {
    return m_coor.data();
  }

  // coordinate setters
  inline double& x() {
    return m_coor[0];
  }
  inline double& y() {
    return m_coor[1];
  }
  inline double& z() {
    return m_coor[2];
  }
  inline double coor(const IndexType  i) const {
    return m_coor[i];
  }
  inline double& coor(const IndexType  i) {
    return m_coor[i];
  }

  const std::array<double, 3>& getCoor() const;

  std::array<double, 3>& getCoor();

  void getCoor(std::vector<double>& coord);

  void setCoor(const std::vector<double>& coord);

  void setCoor(const std::array<double,3>& coord);

  void setCoor(const double* coord);

  //distance between this point and another point
  double dist(const Point& b) const {
    double d(0);
    for (IndexType i = 0; i<3; ++i) {
      d+=(m_coor[i]-b.coor(i))*(m_coor[i]-b.coor(i));
    }
    return std::sqrt(d);
  }

  void clear()
  {
    this->x() = 0.0;
    this->y() = 0.0;
    this->z() = 0.0;
  }

  ///@}
  ///@name Access
  ///@{

  inline double* coor() {
    return m_coor.data();
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  // info
  void print(const IndexType  verbose, std::ostream& c = std::cout) const;
  void printWithTab(std::ostream& outstr) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  std::array<double, 3> m_coor;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  //this block is needed for refcounting
  mutable std::atomic<int> mReferenceCounter;

  ///@}
  ///@name Private Operators
  ///@{

  friend void intrusive_ptr_add_ref(const Point* x)
  {
      x->mReferenceCounter.fetch_add(1, std::memory_order_relaxed);
  }

  friend void intrusive_ptr_release(const Point* x)
  {
      if (x->mReferenceCounter.fetch_sub(1, std::memory_order_release) == 1) {
      std::atomic_thread_fence(std::memory_order_acquire);
      delete x;
      }
  }

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
///@name Operators
///@{
inline Point operator*(const double value, const Point& point) 
{
  Point pt(point);
  pt *= value;
  return pt;
}

inline Point operator*(const Point& point, const double value) 
{
  Point pt(point);
  pt *= value;
  return pt;
}

inline Point operator/(const Point& point, const double value) 
{
  Point pt(point);
  pt /= value;
  return pt;
}

inline Point operator+(const Point& lPoint, const Point& rPoint) 
{
  Point pt(lPoint);
  pt += rPoint;
  return pt;
}

inline Point operator-(const Point& lPoint, const Point& rPoint) 
{
  Point pt(lPoint);
  pt -= rPoint;
  return pt;
}

inline double operator*(const Point& lPoint, const Point& rPoint) 
{
  return lPoint[0]*rPoint[0]+lPoint[1]*rPoint[1]+lPoint[2]*rPoint[2];
}
///@}
} /* namespace felisce.*/

#endif /* _FEL_POINT_HPP  defined */

