//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Geometry/geometricEdges.hpp"
#include "Geometry/neighFacesOfEdges.hpp"
#include "Geometry/neighVolumesOfEdges.hpp"

namespace felisce 
{  
  ////////////////
  // EDGE CLASS //
  ////////////////

  //----------------
  Edge::Edge(const felInt & anIdBeg, const felInt & anIdEnd, const felInt & idEdge) :
    m_ptrNext(nullptr), m_idBeg(anIdBeg), m_idEnd(anIdEnd),m_id(idEdge),m_idOnlySupporting(-1)
  {}

  int Edge::numOfDofSupported(const RefElement& refEle) const {
    int ndof(0);
    // We check in volume neighbour how many dofs this edge has.
    // It is useless to check all the neighbour since they have to compatible.
    if ( m_listNeighVolumesOfEdges.size()>0 ) 
      ndof=refEle.numDOFPerEdge(m_listNeighVolumesOfEdges[0]->idLocalEdge());
    return ndof;
  }
  //----------------
  void Edge::print( std::ostream& outstr, int verbose, bool printNextEdge ) const {
    outstr << m_idBeg << " " << m_idEnd << "   " <<  m_id << "\t";
    if (verbose <= 1) {
      outstr << " numNeighFacesOfEdges = " << m_listNeighFacesOfEdges.size();
      outstr << " numNeighVolumesOfEdges = " << m_listNeighVolumesOfEdges.size();
    } else {
      for (unsigned int ineigh = 0 ; ineigh < m_listNeighFacesOfEdges.size(); ineigh++) {
        m_listNeighFacesOfEdges[ineigh]->print(outstr, verbose);
      }
      outstr << "++ || ";
      for (unsigned int ineigh = 0 ; ineigh < m_listNeighVolumesOfEdges.size(); ineigh++) {
        m_listNeighVolumesOfEdges[ineigh]->print(outstr, verbose);
      }
    }
    outstr << std::endl;
    if ( printNextEdge ) {
      if (m_ptrNext == nullptr) {
        if ( verbose > 30 ) outstr << "-> NULL " << std::endl;
      } else {
        m_ptrNext->print(outstr, verbose);
      }
    }
  }
}
