//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

#ifndef GEOMETRICEDGES_HPP
#define GEOMETRICEDGES_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/refElement.hpp"

namespace felisce {

  ////////////////
  // EDGE CLASS //
  ////////////////
  /*!
   \class Edge
   \authors J.Castelneau & J.Foulon

   \brief Class implementing a geometric edge

   An edge is defined by
   - the global number of the 1st vertex
   - the global number of the 2nd vertex
   - a list of faces neighbours
   - a list of volumes neighbours
   - a pointer on next edge with same 1st vertex

   */
  class NeighVolumesOfEdges;
  class NeighFacesOfEdges;

  class Edge {
    typedef std::shared_ptr<NeighFacesOfEdges> shar_ptrFace;
    typedef std::shared_ptr<NeighVolumesOfEdges> shar_ptrVol;
  private:
    Edge                            *m_ptrNext;
    felInt                           m_idBeg; // really useful?? vm
    felInt                           m_idEnd;
    felInt                           m_id;
    // This id (m_idOnlySupporting) is rarely used.
    // It is important when using a refElem that has a different number of dof
    // on different edges.
    // This id is std::set by a function in listEdges.
    felInt                        m_idOnlySupporting;
    std::vector < shar_ptrFace >  m_listNeighFacesOfEdges;
    std::vector < shar_ptrVol  >  m_listNeighVolumesOfEdges;

  public:

    // Constructor / Destructor
    // ========================
    Edge(): m_ptrNext(nullptr), m_idBeg(0), m_idEnd(0),m_id(0){}
    Edge(const felInt & idBeg, const felInt & idEnd, const felInt & idEdge);

    // getter
    // ======
    inline const felInt & idBeg() const   {
      return m_idBeg;
    }
    inline const felInt & idEnd() const   {
      return m_idEnd;
    }
    inline const std::vector < shar_ptrFace > & listNeighFacesOfEdges() const {
      return m_listNeighFacesOfEdges;
    }
    inline const std::vector < shar_ptrVol > & listNeighVolumesOfEdges() const {
      return m_listNeighVolumesOfEdges;
    }
    inline const felInt & idOnlySupporting() const {
      return m_idOnlySupporting;
    }

    // setter
    // ======
    inline Edge*  & ptrNext() {
      return m_ptrNext;
    }
    inline felInt & idBeg()   {
      return m_idBeg;
    }
    inline felInt & idEnd()   {
      return m_idEnd;
    }
    inline felInt & id() {
      return m_id;
    }
    inline std::vector < shar_ptrFace > & listNeighFacesOfEdges() {
      return m_listNeighFacesOfEdges;
    }
    inline std::vector < shar_ptrVol > & listNeighVolumesOfEdges() {
      return m_listNeighVolumesOfEdges;
    }
    inline felInt & idOnlySupporting() {
      return m_idOnlySupporting;
    }
    int numOfDofSupported(const RefElement& refEle) const;

    // print an edge
    // =============
    void print(  std::ostream& outstr = std::cout, int verbose = 0, bool printNextEdge = false) const;
  };

}

#endif
