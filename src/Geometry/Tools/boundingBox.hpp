//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef FEL_BBOX_HPP
#define FEL_BBOX_HPP

// System includes

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce
{
  ///@name felisce Globals
  ///@{

  ///@}
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name  Enum's
  ///@{

  ///@}
  ///@name  Functions
  ///@{

  ///@}
  ///@name felisce Classes
  ///@{

  class BoundingBox
  {
    public:
      ///@name Type Definitions
      ///@{

      /// Pointer definition of BoundingBox
      FELISCE_CLASS_POINTER_DEFINITION(BoundingBox);

      ///@}
      ///@name Life Cycle
      ///@{

      BoundingBox() = delete;

      BoundingBox(const GeometricMeshRegion& mesh);

      template<class T>
      BoundingBox(const int dim, const T& list);

      BoundingBox(const BoundingBox& ) = delete;

      BoundingBox(BoundingBox&& ) = delete;

      BoundingBox& operator=(const BoundingBox& ) = delete;

      BoundingBox& operator=(BoundingBox&& ) = delete;

      ~BoundingBox() = default;

      ///@}
      ///@name Variables
      ///@{

      ///@}      
      ///@name Operators
      ///@{

      ///@}
      ///@name Operations
      ///@{

      void computeDelta();

      template<class P>
      bool isInBBox(P& point, const double tol=0.);

      ///@}
      ///@name Access
      ///@{

      ///@}
      ///@name Inquiry
      ///@{

      inline Point delta() const { return m_delta; };
      inline Point max() const { return m_max; };
      inline Point min() const { return m_min; };

      ///@}
      ///@name Input and output
      ///@{

      ///@}
      ///@name Friends
      ///@{

      ///@}
    protected:
      ///@name Protected static Member Variables
      ///@{

      ///@}
      ///@name Protected member Variables
      ///@{

      ///@}
      ///@name Protected Operators
      ///@{

      ///@}
      ///@name Protected Operations
      ///@{

      ///@}
      ///@name Protected  Access
      ///@{

      ///@}
      ///@name Protected Inquiry
      ///@{

      ///@}
      ///@name Protected LifeCycle
      ///@{

      ///@}
    private:
      ///@name Private static Member Variables
      ///@{

      ///@}
      ///@name Private member Variables
      ///@{

      int   m_dim;
      Point m_delta;
      Point m_max;
      Point m_min;

      ///@}
      ///@name Private Operators
      ///@{

      ///@}
      ///@name Private Operations
      ///@{

      template<class T>
      inline void setMax(T& pnt) { for (int i = 0; i < m_dim; ++i) if ( pnt[i] > m_max.coor(i) ) m_max.coor(i) = pnt[i]; }; 

      template<class T>
      inline void setMin(T& pnt) { for (int i = 0; i < m_dim; ++i) if ( pnt[i] < m_min.coor(i) ) m_min.coor(i) = pnt[i]; }; 

      ///@}
      ///@name Private  Access
      ///@{

      ///@}
      ///@name Private Inquiry
      ///@{

      ///@}
      ///@name Private LifeCycle
      ///@{

      ///@}
  };
  ///@}
  ///@name Type Definitions
  ///@{

  ///@}
} /* namespace felisce.*/

#include "boundingBox.tpp"

#endif /* FEL_BBOX_HPP  defined */
