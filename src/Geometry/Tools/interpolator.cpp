//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes
#include "Geometry/Tools/interpolator.hpp"

namespace felisce 
{

Interpolator::Interpolator(GeometricMeshRegion* mesh, const bool allowProjection, const double tolRescaling, const int verbose) : 
  Locator(mesh, allowProjection, tolRescaling, verbose) 
{
  if ( m_mesh->domainDim() == GeometricMeshRegion::GeoMesh2D ) {
    if ( m_mesh->bagElementTypeDomain().size() != 1 )
      FEL_ERROR("Interpolator 2D works only for domain made of Tria3.");
    if ( m_mesh->bagElementTypeDomain()[0] != GeometricMeshRegion::Tria3 )
      FEL_ERROR("Interpolator 2D works only for domain made of Tria3.");
    if ( allowProjection ) {
      if ( m_mesh->bagElementTypeDomainBoundary().size() != 1 )
        FEL_ERROR("Interpolator 2D works only for boundaries made of Seg2.");
      if ( m_mesh->bagElementTypeDomainBoundary()[0] != GeometricMeshRegion::Seg2 )
        FEL_ERROR("Interpolator 2D works only for boundaries made of Seg2.");
    }
  }
  else if ( m_mesh->domainDim() == GeometricMeshRegion::GeoMesh3D ) {
    if ( m_mesh->bagElementTypeDomain().size() != 1 )
      FEL_ERROR("Interpolator 3D works only for domain made of Tetra4.");
    if ( m_mesh->bagElementTypeDomain()[0] != GeometricMeshRegion::Tetra4 )
      FEL_ERROR("Interpolator 3D works only for domain made of Tetra4.");
    if ( allowProjection ) {
      if ( m_mesh->bagElementTypeDomainBoundary().size() != 1 )
        FEL_ERROR("Interpolator 3D works only for boundaries made of Tria3");
      if ( m_mesh->bagElementTypeDomainBoundary()[0] != GeometricMeshRegion::Tria3 )
        FEL_ERROR("Interpolator 3D works only for boundaries made of Tria3");
    }
  }
  else {
    FEL_ERROR("Interpolator works only in 3D or 2D");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Interpolator::interpolate(std::vector<felInt>& indElt, std::vector<double>& intVal, std::vector<Point>& lstPoints)
{
  if ( m_mesh->domainDim() == GeometricMeshRegion::GeoMesh3D ){

    interpolate3D(indElt, intVal, lstPoints);
  } 
  else if ( m_mesh->domainDim() == GeometricMeshRegion::GeoMesh2D ) {

    interpolate2D(indElt, intVal, lstPoints);

  } else {

    FEL_ERROR("Interpolator: Not implemented yet for dimension " + std::to_string(m_mesh->domainDim()) + " !" );
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Interpolator::interpolate2D(std::vector<felInt>& idxElt, std::vector<double>& intVal, const std::vector<Point>& lstPoints)
{
  Point point;
  seedElement element;
  std::array<double, 3> bary;

  // Initialize return arguments
  idxElt.assign(lstPoints.size(), -1);
  intVal.assign(3*lstPoints.size(), -1.);

  if ( m_verbose > 3 ) 
    std::cout << "Interpolator: process in progress." << std::endl;

  // For each point of the structure mesh
  for (std::size_t k = 0; k < lstPoints.size(); ++k) {
    point = lstPoints[k];

    if ( m_verbose > 3 ) 
      std::cout << "Interpolator: Processing point " << k << std::endl;

    // Locate the point and even project it if necessary - when it is inside the box but not inside the mesh
    if ( !( findPointInMesh(element, lstPoints[k]) ) )
      // Failure
      FEL_ERROR( "Interpolator: point " + std::to_string(k) + " has not been localized even with the projection on the mesh (if it has been allowed)!" );

    // Compute barycentric coordinates
    m_mesh->isPointInTri(element.second, point, bary, m_tol);

    // Save results
    idxElt[k] = element.second;

    intVal[3*k]   = bary[1];
    intVal[3*k+1] = bary[2];
    intVal[3*k+2] = bary[0];
  }

  if ( m_verbose > 3 ) 
    std::cout << "Interpolator: process completed." << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

void Interpolator::interpolate3D(std::vector<felInt>& idxElt, std::vector<double>& intVal, const std::vector<Point>& lstPoints)
{
  Point point;
  seedElement element;
  std::array<double,4> bary;

  // Initialize return arguments
  idxElt.assign(lstPoints.size(), -1);
  intVal.assign(4*lstPoints.size(), -1.);

  if ( m_verbose > 3 ) 
    std::cout << "Interpolator: process in progress." << std::endl;

  // For each point of the structure mesh
  for (std::size_t k = 0; k < lstPoints.size(); ++k) {
    point = lstPoints[k];

    if ( m_verbose > 3 ) 
      std::cout << "Interpolator: Processing point " << k << std::endl;

    // Locate the point and even project it if necessary - when it is inside the box but not inside the mesh
    if ( !( findPointInMesh(element, lstPoints[k]) ) )
      // Failure
      FEL_ERROR( "Interpolator: point " + std::to_string(k) + " has not been localized even with the projection on the mesh (if it has been allowed)!" );

    // Compute barycentric coordinates
    m_mesh->isPointInTetra(element.second, point, bary, m_tol);

    // Save results
    idxElt[k] = element.second;

    intVal[4*k]   = bary[2];
    intVal[4*k+1] = bary[3];
    intVal[4*k+2] = bary[1];
    intVal[4*k+3] = bary[0];
  }

  if ( m_verbose > 3 ) 
    std::cout << "Interpolator: process completed." << std::endl;
}

}
