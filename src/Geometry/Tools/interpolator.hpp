//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef FEL_INTERPOLATOR_HPP
#define FEL_INTERPOLATOR_HPP

// System includes

// External includes

// Project includes
// #include "Core/shared_pointers.hpp"
#include "Geometry/Tools/locator.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

class Interpolator:
  public Locator
{
  public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of Interpolator
    FELISCE_CLASS_POINTER_DEFINITION(Interpolator);

    ///@}
    ///@name Life Cycle
    ///@{

    Interpolator() = delete;

    Interpolator(GeometricMeshRegion* mesh, const bool allowProjection = false, const double tolRescaling = 1., const int verbose = 0);

    Interpolator(const Interpolator& ) = delete;

    Interpolator(Interpolator&& ) = delete;

    Interpolator& operator=(const Interpolator& ) = delete;

    Interpolator& operator=(Interpolator&& ) = delete;

    ~Interpolator() = default;

    ///@}  
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    void interpolate(std::vector<felInt>& indElt, std::vector<double>& intVal, std::vector<Point>& lstPoints);

    void interpolate2D(std::vector<felInt>& idxElt, std::vector<double>& intVal, const std::vector<Point>& lstPoints);

    void interpolate3D(std::vector<felInt>& idxElt, std::vector<double>& intVal, const std::vector<Point>& lstPoints);

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    ///@}
    ///@name Friends
    ///@{

    ///@}
  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}
  private:
    ///@name Private static Member Variables
    ///@{

    ///@}
    ///@name Private member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    int projectPointOnClosestTriangle(Point const& ppt, Point& pptProjected) const;

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Private LifeCycle
    ///@{

    ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif /* FEL_INTERPOLATOR_HPP  defined */

