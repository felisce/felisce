//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes

namespace felisce
{

template<class T>
BoundingBox::BoundingBox(const int dim, const T& list):
  m_dim(dim), m_delta(0.), m_max(-1.e30), m_min(1.e30)
{
  FEL_ASSERT(m_dim >= 0 && m_dim <= 3);

  for (std::size_t k = 0; k < list.size(); ++k) {

    if constexpr ( !std::is_pointer<typename T::value_type>::value ) {

      // Update maximum
      setMax(list[k]);

      // Update minimum
      setMin(list[k]);
    }
    else {

      // Update maximum
      setMax(*list[k]);

      // Update minimum
      setMin(*list[k]);
    }
  }

  for (int i = m_dim; i < 3; ++i) {
    m_max.coor(i) = 0.;
    m_min.coor(i) = 0.;
  }
}

/***********************************************************************************/
/***********************************************************************************/

template<class T>
bool BoundingBox::isInBBox(T& point, const double tol)
{
  for (int icoor = 0; icoor < m_dim; ++icoor){
    if ( point[icoor] < min()[icoor] - tol || point[icoor] > max()[icoor] + tol )
      return false;
  } 

  return true; 
}

}
