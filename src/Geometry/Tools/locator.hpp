//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef FEL_LOCATOR_HPP
#define FEL_LOCATOR_HPP

// System includes

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/Tools/hashtable.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

class Locator
{
  public:
    ///@name Type Definitions
    ///@{

    static constexpr std::size_t HASHSIZ = 256;

    typedef GeometricMeshRegion::seedElement seedElement;

    typedef GeometricMeshRegion::ElementType ElementType;

    /// Pointer definition of Locator
    FELISCE_CLASS_POINTER_DEFINITION(Locator);

    ///@}
    ///@name Life Cycle
    ///@{

    Locator() = delete;

    Locator(GeometricMeshRegion* mesh, const bool allowProjection = false, const double tolRescaling = 1., const int verbose = 0);

    Locator(const Locator& ) = delete;

    Locator(Locator&& ) = delete;

    Locator& operator=(const Locator& ) = delete;

    Locator& operator=(Locator&& ) = delete;

    ~Locator();

    ///@}
    ///@name Variables
    ///@{

    ///@}      
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    void findListPointsInMesh(std::vector<felInt>& indElt, std::vector<ElementType>& typElt, const std::vector<Point>& listPoint);

    void findListPointsInMesh(std::vector<seedElement>& elt, const std::vector<Point>& listPoint);

    bool findPointInMesh(seedElement& elt, const Point& point);

    seedElement localizePoint(seedElement seed, const Point& point, Point& pointInRefElm, std::size_t mark);

    void resetMarkElement();

    ///@}
    ///@name Access
    ///@{

    inline const std::size_t & getMark() const { return m_mark; }
    inline       std::size_t & getMark()       { return m_mark; }

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    ///@}
    ///@name Friends
    ///@{

    ///@}
  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    /// Flag to allow projection
    bool m_allowProjection = false;

    /// Tolerance
    double m_tol;

    /// Verbose level
    const int m_verbose; 

    /// Marked element counter
    std::size_t m_mark;

    /// Marked element vector
    std::array<std::size_t*, GeometricMeshRegion::m_numTypesOfElement> m_markElement;

    /// Geometric mesh
    GeometricMeshRegion *m_mesh = nullptr; 

    /// Hash table 
    HashTable::Pointer   m_hash = nullptr;

    /// Bounding box
    BoundingBox::Pointer m_bbox = nullptr;

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}
  private:
    ///@name Private static Member Variables
    ///@{

    ///@}
    ///@name Private member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    bool projectPointOnMesh(seedElement& elt, const Point& pntIn);

    ///@}
    ///@name Private  Access
    ///@{

    inline std::size_t & markElement(ElementType ityp, felInt ind)       { return m_markElement[ityp][ind]; }
    inline std::size_t   markElement(ElementType ityp, felInt ind) const { return m_markElement[ityp][ind]; }

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Private LifeCycle
    ///@{

    ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif /* FEL_LOCATOR_HPP  defined */

