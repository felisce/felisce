//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes
#include <algorithm>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Geometry/Tools/hashtable.hpp"

namespace felisce 
{

HashTable::HashTable(GeometricMeshRegion* mesh, std::size_t size, BoundingBox* bbox) :
  m_mesh(mesh), m_bbox(bbox), m_size(size)
{

  if ( FelisceParam::verbose() > 3 )
    std::cout << "HashTable: initialization in progress." << std::endl;

  // Set maximum "key" value : "key" ranges form 0 to m_size^dim-1 included
  m_maxKey = static_cast<std::size_t>(std::pow(m_size,mesh->spatialDim()));

  // Memory allocation
  m_head = new felInt[ m_maxKey ];
  for(std::size_t i = 0; i < m_maxKey; ++i)
    m_head[i] = -1;
  
  m_link = new felInt[m_mesh->numPoints()];
  for(felInt i = 0; i < m_mesh->numPoints(); ++i)
    m_link[i] = -1;

  // Insert vertices in hashtable 
  Point pnt;
  felInt key;
  std::array<felInt,3> index{0, 0, 0};
  const Point delta = m_bbox->delta();
  const Point min   = m_bbox->min();

  for (felInt k = 0; k < m_mesh->numPoints(); ++k) {

    // Scale point and compute hash key
    pnt = m_size * (m_mesh->listPoint(k) - min);
    for (int i = 0; i < m_mesh->numCoor(); ++i){
      pnt[i] /= delta[i];

      index[i] = std::max( 0, static_cast<felInt>( pnt[i] - EPS * delta[i] ) );
    }
    key = ( index[2] * m_size + index[1] ) * m_size + index[0];

    if ( m_head[key] < 0 ) {
      m_head[key] = k;
    } else {
      m_link[k]   = m_head[key];
      m_head[key] = k;
    }
  }

  // Fill m_seed and m_type
  int numPntElm = 0;
  ElementType eltType;
  std::vector<felInt> elem;

  m_seed = new felInt[m_mesh->numPoints()];
  m_type = new ElementType[m_mesh->numPoints()];
  
  // Loop over element type
  for (std::size_t i = 0; i < m_mesh->bagElementTypeDomain().size(); ++i) {
    eltType   = m_mesh->bagElementTypeDomain()[i];
    numPntElm = m_mesh->numPtsPerElement(eltType);
    elem.resize(numPntElm, 0);

    // Initialize arrays
    for (felInt k = 0; k < m_mesh->numElements(eltType); ++k) {

      // get element vertices
      m_mesh->getOneElement(eltType, k, elem, 0);

      // set seed for every vertex
      for (int iPnt = 0; iPnt < numPntElm; ++iPnt) {
        m_type[ elem[iPnt] ] = eltType;
        m_seed[ elem[iPnt] ] = k;
      }
    }
  }

  if ( FelisceParam::verbose() > 3 )
    std::cout << "HashTable: initialization completed." << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

HashTable::~HashTable() 
{
  if( m_head ) {    
    delete [] m_head;
    m_head = nullptr;
  }

  if( m_link ) {
    delete [] m_link;
    m_link = nullptr;
  }

  if ( m_seed ) {
    delete [] m_seed;
    m_seed = nullptr;
  }

  if ( m_type ) {
    delete [] m_type;
    m_type = nullptr;
  }
}

/***********************************************************************************/
/***********************************************************************************/

HashTable::seedElement HashTable::findSeed(Point pntIn) const 
{
  const felInt iVer = closestVertex(pntIn);
  
  return HashTable::seedElement{m_type[iVer], m_seed[iVer]};
}

/***********************************************************************************/
/***********************************************************************************/

felInt HashTable::closestVertex(Point pntIn) const 
{
  Point     pnt;
  felInt    iPrv, iNxt;
  double    dPrv, dNxt;
  std::array<felInt,3> index{0, 0, 0};

  Point delta = m_bbox->delta();

  // Scale point
  pntIn = m_size * (pntIn - m_bbox->min());
  for (int i = 0; i < m_mesh->numCoor(); ++i) {
    pntIn[i] /= delta[i];

    index[i] = std::max( 0, static_cast<felInt>( pntIn[i] - EPS * delta[i] ) );
  }
  felInt key = ( index[2] * m_size + index[1] ) * m_size + index[0];

  // The provided point must be inside the bounding box
  FEL_ASSERT( key < static_cast<felInt>(m_maxKey) );

  // Check current cell
  if ( m_head[key] >= 0 ) {
    iPrv = m_head[key];

    // Get point
    pnt = m_mesh->listPoint(iPrv);

    // Compute distance^2
    dPrv = std::pow( pnt.dist(pntIn), 2);

    // All the points with the same hash key
    iNxt = iPrv;
    while ( m_link[iNxt] >= 0 ) {  
      iNxt = m_link[iNxt];

      //  Get point
      pnt = m_mesh->listPoint(iNxt);

      // Compute distance^2
      dNxt = std::pow( pnt.dist(pntIn), 2);

      // If Nxt closer than Prv -> update
      if ( dNxt < dPrv ) {
        iPrv = iNxt;
        dPrv = dNxt;
      }
    }

    if ( FelisceParam::verbose() > 3 )
      std::cout << "HashTable: germ vertex is " << iPrv << "." << std::endl;

    return iPrv;
  }

  // if the previous tests have failed, check into the neighbors
  std::array<std::array<felInt, 2>,3> indexMinMax = {{ {{0, 0}}, {{0, 0}}, {{0, 0}} }};
  felInt d = 1;
  do {
    for (int i = 0; i < m_mesh->numCoor(); ++i)
      indexMinMax[i] = { std::max( 0, index[i] - d ),  std::min( index[i] + d, m_size-1 ) };

    // Windows of neighbour nodes
    for (felInt k = indexMinMax[2][0]; k <= indexMinMax[2][1]; ++k)
      for (felInt j = indexMinMax[1][0]; j <= indexMinMax[1][1]; ++j)
        for (felInt i = indexMinMax[0][0]; i <= indexMinMax[0][1]; ++i) {

          // Get neighbor key
          key = ( k * m_size + j ) * m_size + i;
          FEL_ASSERT( key < static_cast<felInt>(m_maxKey) );

          // Check neighbor cell
          iPrv = m_head[key];
          if ( iPrv < 0 ) continue;

          if ( FelisceParam::verbose() > 3 )
            std::cout << "HashTable: germ vertex is " << iPrv << "." << std::endl;

          return iPrv;
        }
  } while ( ++d < m_size );

  FEL_ERROR("HashTable: cannot find closest vertex!");

  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

void HashTable::print() 
{
  felInt iPnt;

  for (std::size_t key = 0; key < m_maxKey; ++key) {

    // Get first hashed point
    iPnt = m_head[key];
    if ( iPnt < 0 )
      continue;

    // Print all the points with the same hash key
    std::cout <<"Key = " << key << std::endl;
    std::cout <<"List of points: " << iPnt;

    iPnt = m_link[iPnt];
    while ( iPnt >= 0 ) {  
      std::cout << " " << iPnt;

      // Get next point
      iPnt = m_link[iPnt];
    }
    std::cout << std::endl << std::endl;
  }

  for (felInt k = 0; k < m_mesh->numPoints(); ++k) 
    std::cout <<"Seed of vertex " << k << " is element: " << GeometricMeshRegion::eltEnumToFelNameGeoEle[m_type[k]].first << " " << m_seed[k] << std::endl;
}

}

