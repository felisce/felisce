//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef HASHT_HPP
#define HASHT_HPP

// System includes

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/Tools/boundingBox.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class HashTable 
{
  public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of HashTable
    FELISCE_CLASS_POINTER_DEFINITION(HashTable);

    typedef GeometricMeshRegion::ElementType ElementType;
    typedef GeometricMeshRegion::seedElement seedElement;

    ///@}
    ///@name Public static Member Variables
    ///@{

    static constexpr std::size_t HASHSIZE = 256;

    ///@}
    ///@name Life Cycle
    ///@{

    HashTable() = delete;

    HashTable(GeometricMeshRegion* mesh, std::size_t size, BoundingBox* bbox);

    HashTable(const HashTable& ) = delete;

    HashTable(HashTable&& ) = delete;

    HashTable& operator=(const HashTable& ) = delete;

    HashTable& operator=(HashTable&& ) = delete;

    ~HashTable();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{ 

    // Getters
    inline       felInt &size()       { return m_size; }
    inline const felInt &size() const { return m_size; }

    // Provide a seed for point pntIn, pntIn MUST be in bbox
    seedElement findSeed(Point pntIn) const;

    // Find the closest vertice of the point pntIn, pntIn MUST be in bbox
    felInt closestVertex(Point pntIn) const;   

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    // Prints
    void print();

    ///@}
    ///@name Friends
    ///@{

    ///@}
  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}
  private:

    ///@name Private static Member Variables
    ///@{

    static constexpr double EPS = 1.e-7;

    ///@}
    ///@name Private member Variables

    GeometricMeshRegion *m_mesh = nullptr; 

    BoundingBox *m_bbox = nullptr;

    felInt      *m_head = nullptr;
    felInt      *m_link = nullptr;
    felInt      *m_seed = nullptr;
    ElementType *m_type = nullptr;

    felInt m_size;
    
    std::size_t m_maxKey;

    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Private LifeCycle
    ///@{

    ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif
