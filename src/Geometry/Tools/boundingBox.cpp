//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes
#include "Geometry/Tools/boundingBox.hpp"

namespace felisce 
{

BoundingBox::BoundingBox(const GeometricMeshRegion& mesh):
  BoundingBox::BoundingBox(mesh.numCoor(), mesh.listPoints()) {}

/***********************************************************************************/
/***********************************************************************************/

void BoundingBox::computeDelta()
{
  double tmp;
  double delta = -1;
  for (int i = 0; i < m_dim; ++i) {
    tmp = std::fabs( m_max.coor(i) - m_min.coor(i) );
    // if ( tmp > m_delta.coor(i) ) m_delta.coor(i) = tmp;
    if ( tmp > delta ) delta = tmp;
  }

  for (int i = 0; i < m_dim; ++i)
    m_delta.coor(i) = delta;
}

}