//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes
#include "Geometry/geometricEdges.hpp"
#include "Geometry/geometricFaces.hpp"
#include "Geometry/neighFacesOfEdges.hpp"
#include "Geometry/neighVolumesOfFaces.hpp"
#include "Geometry/Tools/intersector.hpp"

namespace felisce 
{

Intersector::Intersector(GeometricMeshRegion* volMesh, GeometricMeshRegion* strMesh, const double tolRescaling, const int verbose):
  m_volMesh(volMesh), m_strMesh(strMesh), m_verbose(verbose)
{
  // Set tolerance
  m_tol = GeoUtilities::GEO_TOL * tolRescaling;
  
  FEL_CHECK( m_volMesh->domainDim() == 2 || m_volMesh->domainDim() == 3,"Intersector: cannot be called for a mesh with domainDim = " + std::to_string(m_volMesh->domainDim()) + " different from 2 or 3!\n" );
  FEL_CHECK( m_volMesh->bagElementTypeDomain().size() == 1 && 
            (m_volMesh->bagElementTypeDomain()[0] == GeometricMeshRegion::Tria3 ||
             m_volMesh->bagElementTypeDomain()[0] == GeometricMeshRegion::Tetra4 ), "Intersector: volume mesh must contain only Tria3 or Tetra4!\n");
  FEL_CHECK( m_strMesh->bagElementTypeDomain().size() == 1 && 
            (m_strMesh->bagElementTypeDomain()[0] == GeometricMeshRegion::Seg2 ||
             m_strMesh->bagElementTypeDomain()[0] == GeometricMeshRegion::Tria3 ), "Intersector: interface mesh must contain only Seg2 or Tria3!\n");
}

/***********************************************************************************/
/***********************************************************************************/

void Intersector::intersectMeshes(const std::vector<felInt>& listVerToElt, std::vector<felInt>& listOfIntElt)  const
{
  FEL_ASSERT ( m_strMesh->numPoints() == static_cast<int>(listVerToElt.size()) );

  if ( m_volMesh->domainDim() == 2 )
    findIntersectedElements2D(listVerToElt, listOfIntElt);
  else /*if ( m_volMesh->domainDim() == 3 )*/
    findIntersectedElements3D(listVerToElt, listOfIntElt);
}

/***********************************************************************************/
/***********************************************************************************/

void Intersector::findIntersectedElements2D(const std::vector<felInt>& listVerToElt, std::vector<felInt>& listOfIntElt)  const
{
  std::vector<Point*> pntOfTria(3, nullptr);
  std::vector<felInt> verOfTria(3, 0);
  std::vector<Point>  pntOfEdge(2);
  std::vector<felInt> verOfEdge(2, -1);
  std::vector<Point>  intPoint(2);
  std::array<Location,2> intLocation;
  std::array<double, 3> bary;
  bool isInside0, isInside1;
  felInt idxOfPreElt, idxOfNxtElt, idxOfElt = -1;

  // Set of intersected elements
  std::set<felInt> setOfIntElt;

  // Build the edges of the volume mesh
  if ( !m_volMesh->statusEdges() )
    m_volMesh->buildEdges(); 

  // For each edge of the structure mesh
  for (felInt i = 0; i < m_strMesh->getNumElement1D(); ++i) { 

    // Get nodes of the current structure edge
    m_strMesh->getOneElement(GeometricMeshRegion::Seg2, i, verOfEdge, 0);
    pntOfEdge[0] = m_strMesh->listPoints()[verOfEdge[0]];
    pntOfEdge[1] = m_strMesh->listPoints()[verOfEdge[1]];

    if ( m_verbose > 3 )
      std::cout << "Intersector: Processing edge " << i << " = " << verOfEdge[0] << " " << verOfEdge[1] << std::endl;

    // Find volume element to start the intersection
    if ( listVerToElt[ verOfEdge[0] ] != -1 ) {
      idxOfElt = listVerToElt[ verOfEdge[0] ];
    }
    else if ( listVerToElt[ verOfEdge[1] ] != -1 ) {
      idxOfElt = listVerToElt[ verOfEdge[1] ];

      std::swap(verOfEdge[0], verOfEdge[1]);
      std::swap(pntOfEdge[0], pntOfEdge[1]);
    }
    else {
      idxOfElt = getGermElement2D(verOfEdge);
    }

    if ( m_verbose > 3 )
      std::cout << "Interface edge " << i << " = " << verOfEdge[0] << " " << verOfEdge[1] << ", vertex " << verOfEdge[0] << " is in volume element " << idxOfElt << std::endl;

    // Reset previous intersected element to -1
    idxOfPreElt = -1;

    // Look for intersection until pntOfEdge[1] is inside idxOfElt
    do {

      // Get nodes and coordinates of the current triangle
      m_volMesh->getOneElement(GeometricMeshRegion::Tria3, idxOfElt, verOfTria, pntOfTria, 0);

      if ( m_verbose > 9 )
        std::cout << "Volume element " << idxOfElt << " = " << verOfTria[0] << " " << verOfTria[1] << " " << verOfTria[2] << "." << std::endl;

      // Check the position of pntOfEdge[0] wrt to the current element
      isInside0 = GeoUtilities::isPointInTri(pntOfTria, pntOfEdge[0], bary, intLocation[0], m_tol);

      if ( !isInside0 ){
        FEL_ERROR("Intersector::findIntersectedElements2D : pntOfEdge[0] must be inside idxOfElt !")
      }

      if ( m_verbose > 9 )
        std::cout << "Point0 " << pntOfEdge[0] << " is in element " << idxOfElt << " with case = " << static_cast<std::underlying_type_t<Position>>(intLocation[0].first) << " and position = " <<  static_cast<std::underlying_type_t<Position>>(intLocation[0].second) << "." << std::endl;
      
      // Add the intersected elements in the list
      addIntersetedEltToList2D(idxOfElt, intLocation[0], setOfIntElt);

      // Check the position of pntOfEdge[0] wrt to the current element
      isInside1 = GeoUtilities::isPointInTri( pntOfTria, pntOfEdge[1], bary, intLocation[1], m_tol);

      // If pntOfEdge[1] is inside the current element, add the intersected elements to the list
      if ( isInside1 ) {

        if ( m_verbose > 9 )
          std::cout << "Point1 " << pntOfEdge[1] << " is in element " << idxOfElt << " with case = " << static_cast<std::underlying_type_t<Position>>(intLocation[1].first) << " and position = " <<  static_cast<std::underlying_type_t<Position>>(intLocation[1].second) << "." << std::endl;

        // Add the intersected elements in the list
        addIntersetedEltToList2D(idxOfElt, intLocation[1], setOfIntElt);
      }
      // If pntOfEdge[1] is outside the current element, we must detect the intersections 
      else {

        if ( m_verbose > 9 )
          std::cout << "Point1 " << pntOfEdge[1] << " is not in the volume element " << idxOfElt << " so there is an intersection to detect." << std::endl;

        bool intFnd = false;
        // If verOfEdge[0] on a vertex
        if ( intLocation[0].first == Position::OnVertex )
          intFnd = computeIntersectionVertex2D(idxOfElt, verOfTria[intLocation[0].second], idxOfPreElt, idxOfNxtElt, pntOfEdge, intPoint, isInside1);
        // If verOfEdge[0] on an edge
        else if ( intLocation[0].first == Position::OnEdge ) {
          intFnd = computeIntersectionEdge2D(idxOfElt, intLocation[0].second, idxOfPreElt, idxOfNxtElt, pntOfEdge, intPoint, isInside1);
        }
        else { // if ( intLocation[0].first == Position::Inside ) {
          intFnd = computeIntersectionInside2D(idxOfElt, idxOfNxtElt, pntOfEdge, intPoint);
        }

        if ( !intFnd )
          FEL_ERROR("Intersector::findIntersectedElements2D : there must be at least one intersection point !");

        if ( m_verbose > 9 )
          std::cout << "Intersection detected " << intPoint[0] << "." << std::endl;

        // Update pntOfEdge
        pntOfEdge[0] = intPoint[0];

        // Update previous intersected element
        idxOfPreElt = idxOfElt;

        // Update triangle for new intersections
        idxOfElt = idxOfNxtElt;
      }
    } while ( !isInside1 );
  }

  // Fill the vector of intersected elements
  listOfIntElt.assign(setOfIntElt.begin(), setOfIntElt.end());

  if ( m_verbose > 9 )
    std::cout << "Intersector: all the edges of the structure have been considered!" << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

void Intersector::addIntersetedEltToList2D(const felInt idxOfElt, const Location& location, std::set<felInt>& setOfIntElt) const
{
  felInt eltIdx;
  std::vector<felInt> verOfTria(3, 0);

  // Get nodes and coordinates of the current triangle
  m_volMesh->getOneElement(GeometricMeshRegion::Tria3, idxOfElt, verOfTria, 0);

  switch ( location.first ) {

    // Vertex is on a vertex of the triangle
    case Position::OnVertex : {

      // Variables
      GeometricMeshRegion::ElementType eltTyp;
      GeometricMeshRegion::BallMap ball;
      felInt verPosInBall;

      // Get ball
      m_volMesh->getVertexBall(verOfTria[location.second], GeometricMeshRegion::Tria3, idxOfElt, ball);

      // Loop over ball
      for (std::size_t iel = 0; iel < ball.size(); ++iel) {
        std::tie(eltTyp, eltIdx, verPosInBall) = ball[iel];

        // Add element to the set of intersected elements
        setOfIntElt.insert(eltIdx);
      }
      break;
    }
    // Vertex is on an edge of the triangle
    case Position::OnEdge : {

      // Variables
      std::vector<felInt> verEdgOfTria(2,-1);
      Edge edgOfElt;

      auto& mapEltEdgToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tria3];

      // Get the ied-th edge of the triangle
      verEdgOfTria[0] = verOfTria[ mapEltEdgToVer[location.second][0] ];
      verEdgOfTria[1] = verOfTria[ mapEltEdgToVer[location.second][1] ];
      m_volMesh->getEdge(verEdgOfTria, edgOfElt);

      // Loop over elements sharing the edge 
      auto& listNeigh = edgOfElt.listNeighFacesOfEdges();
      for (std::size_t iel = 0; iel < listNeigh.size(); ++iel) {
        eltIdx = listNeigh[iel]->idFace();

        // Add element to the set of intersected elements
        setOfIntElt.insert(eltIdx);
      }
      break;
    }
    // Vertex is inside the triangle
    case Position::Inside : {

      // Add element to the set of intersected elements
      setOfIntElt.insert(idxOfElt);
      break;
    }

    default : {
      FEL_ERROR("Intersector::addIntersetedEltToList2D : the vertex must be on a vertex, edge or inside the triangle !");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionVertex2D(const felInt idxOfElt, const felInt idxOfVer, const felInt idxOfPreElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const
{
  GeometricMeshRegion::ElementType eltTyp;
  felInt eltIdx;
  felInt verPos;
  unsigned short int ied;
  int nbrIntPnt;
  std::vector<Point*> pntOfElt(3, nullptr);
  std::vector<felInt> verOfElt(3, 0);
  std::array<double,3> bary;
  std::array<Point*,2> pntEdgOfElt;

  GeometricMeshRegion::BallMap ball;

  auto& mapEltEdgToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tria3];

  // Get ball
  m_volMesh->getVertexBall(idxOfVer, GeometricMeshRegion::Tria3, idxOfElt, ball);

  // Loop over ball
  for (std::size_t iel = 0; iel < ball.size(); ++iel) {
    std::tie(eltTyp, eltIdx, verPos) = ball[iel];

    if ( eltIdx == idxOfPreElt )
      continue;

    // Get nodes and coordinates of the current triangle
    m_volMesh->getOneElement(GeometricMeshRegion::Tria3, eltIdx, verOfElt, pntOfElt, 0);

    // Check if pntOfEdge[1] is inside the current element
    // If it is not inside, look for an intersection with the opposite edge
    if ( !GeoUtilities::isPointInTri(pntOfElt, pntOfEdge[1], bary, m_tol) ) {

      // Get the points of the edge opposite to the vertex
      ied = mapEltEdgToVer[verPos][1];
      pntEdgOfElt[0] = pntOfElt[ mapEltEdgToVer[ied][0] ];
      pntEdgOfElt[1] = pntOfElt[ mapEltEdgToVer[ied][1] ];

      // Compute intersection with opposite edge
      nbrIntPnt = GeoUtilities::computeSegmentsIntersection(pntOfEdge[0], pntOfEdge[1], *pntEdgOfElt[0], *pntEdgOfElt[1], intPoint[0], intPoint[1], m_tol);

      // If intersection
      if ( nbrIntPnt > 0 ) {

        if ( m_verbose > 9 )
          std::cout << "computeIntersectionVertex2D: Intersection detected in " << intPoint[0] << "." << std::endl;

        // Return next element to intersect
        idxNxtElt = eltIdx;

        return true;
      }
    }
    // If it is inside, nothing to do
    else {

      if ( m_verbose > 9 )
        std::cout << "computeIntersectionVertex2D: Point " << pntOfEdge[1] << " has been found in element " << eltIdx << "." << std::endl;

      // Set intersection
      intPoint[0] = pntOfEdge[1];

      // Return next element to intersect
      idxNxtElt = eltIdx;

      // Point inside element
      isInside = true;

      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionEdge2D(const felInt idxOfElt, const int idxEdgInt, const felInt idxOfPreElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const
{
  felInt eltIdx;
  int nbrIntPnt;
  std::vector<Point*> pntOfNxtElt(3, nullptr);
  std::vector<felInt> verOfNxtElt(3, 0);
  std::vector<felInt> verOfElt(3, 0);
  std::vector<felInt> verEdgOfElt(2,-1);
  std::array<double,3> bary;
  std::array<felInt,2> verEdgOfNxtElt;
  std::array<Point*,2> pntEdgOfNxtElt;
  Edge edgOfElt;

  auto& mapEltEdgToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tria3];

  // Get nodes and coordinates of idxOfElt
  m_volMesh->getOneElement(GeometricMeshRegion::Tria3, idxOfElt, verOfElt, 0);

  // Get the ied-th edge of the triangle
  verEdgOfElt[0] = verOfElt[ mapEltEdgToVer[idxEdgInt][0] ];
  verEdgOfElt[1] = verOfElt[ mapEltEdgToVer[idxEdgInt][1] ];
  m_volMesh->getEdge(verEdgOfElt, edgOfElt);

  // Loop over elements sharing the edge 
  auto& listNeigh = edgOfElt.listNeighFacesOfEdges();
  for (std::size_t iel = 0; iel < listNeigh.size(); ++iel) {
    eltIdx = listNeigh[iel]->idFace();

    if ( eltIdx == idxOfPreElt )
      continue;

    // Get nodes and coordinates of the current triangle
    m_volMesh->getOneElement(GeometricMeshRegion::Tria3, eltIdx, verOfNxtElt, pntOfNxtElt, 0);

    // Check if pntOfEdge[1] is inside the current element
    // If it is not inside, look for an intersection with the opposite edges
    if ( !GeoUtilities::isPointInTri(pntOfNxtElt, pntOfEdge[1], bary, m_tol) ) {

      // Loop over the two opposite edges
      for (std::size_t ied = 0; ied < 3; ++ied) {
        
        // Get vertices of the ied-th edge
        verEdgOfNxtElt[0] = verOfNxtElt[ mapEltEdgToVer[ied][0] ];
        verEdgOfNxtElt[1] = verOfNxtElt[ mapEltEdgToVer[ied][1] ]; 

        // Skip edge 
        if ( std::is_permutation(verEdgOfElt.begin(), verEdgOfElt.end(), verEdgOfNxtElt.begin()) )
          continue;

        // Get the edge
        pntEdgOfNxtElt[0] = pntOfNxtElt[ mapEltEdgToVer[ied][0] ];
        pntEdgOfNxtElt[1] = pntOfNxtElt[ mapEltEdgToVer[ied][1] ];

        // Compute intersection with the edge
        nbrIntPnt = GeoUtilities::computeSegmentsIntersection(pntOfEdge[0], pntOfEdge[1], *pntEdgOfNxtElt[0], *pntEdgOfNxtElt[1], intPoint[0], intPoint[1], m_tol);
        
        // If intersection
        if ( nbrIntPnt > 0 ) {

          if ( m_verbose > 9 )
            std::cout << "computeIntersectionEdge2D: Intersection detected in " << intPoint[0] << "." << std::endl;

          // Return next element to intersect
          idxNxtElt = eltIdx;

          return true;
        }
      }
    }
    // If it is inside, nothing to do
    else {

      if ( m_verbose > 9 )
        std::cout << "computeIntersectionEdge2D: Point " << pntOfEdge[1] << " has been found in element " << eltIdx << "." << std::endl;

      // Set intersection
      intPoint[0] = pntOfEdge[1];

      // Return next element to intersect
      idxNxtElt = eltIdx;

      // Point inside element
      isInside = true;

      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionInside2D(const felInt idxOfElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint) const
{
  int nbrIntPnt;
  std::vector<Point*> pntOfElt(3, nullptr);
  std::vector<felInt> verOfElt(3, 0);
  std::array<Point*,2> pntEdgOfElt;

  auto& mapEltEdgToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tria3];

  // Get nodes and coordinates of the current triangle
  m_volMesh->getOneElement(GeometricMeshRegion::Tria3, idxOfElt, verOfElt, pntOfElt, 0);

  // Loop over the edges
  for (std::size_t ied = 0; ied < 3; ++ied) {

    // Get the edge
    pntEdgOfElt[0] = pntOfElt[ mapEltEdgToVer[ied][0] ];
    pntEdgOfElt[1] = pntOfElt[ mapEltEdgToVer[ied][1] ];

    // Compute intersection with the edge
    nbrIntPnt = GeoUtilities::computeSegmentsIntersection(pntOfEdge[0], pntOfEdge[1], *pntEdgOfElt[0], *pntEdgOfElt[1], intPoint[0], intPoint[1], m_tol);

    // If intersection
    if ( nbrIntPnt > 0 ) {

      // Return next element to intersect
      idxNxtElt = idxOfElt;

      return true;
    }    
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

felInt Intersector::getGermElement2D(const std::vector<felInt>& verOfEdge) const
{
  std::vector<Point*> pntOfTria(3, nullptr);
  std::vector<felInt> verOfTria(3, 0);
  std::array<double, 3> bary;

  // Go through all elements of the mesh
  for (felInt k = 0; k < m_volMesh->numElements(GeometricMeshRegion::Tria3); ++k) {

    // Get nodes of the current triangle
    m_volMesh->getOneElement(GeometricMeshRegion::Tria3, k, verOfTria, pntOfTria, 0); 

    if( GeoUtilities::isPointInTri( pntOfTria, m_strMesh->listPoints()[verOfEdge[0]], bary, m_tol) )
      return k;
  }

  FEL_ERROR("Intersector::getGermElement2D cannot localize the point into the volume mesh.");
  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

void Intersector::findIntersectedElements3D(const std::vector<felInt>& listVerToElt, std::vector<felInt>& listOfIntElt) const
{
  std::vector<Point*> pntOfTetra(4, nullptr);
  std::vector<felInt> verOfTetra(4, 0);
  std::vector<Point>  pntOfEdge(2);
  std::vector<felInt> verOfEdge(2, -1);
  std::vector<Point>  intPoint(2);
  std::array<Location,2> intLocation;
  std::array<double, 4> bary;
  bool isInside0, isInside1;
  felInt idxOfPreElt, idxOfNxtElt, idxOfElt = -1;

  // Set of intersected elements
  std::set<felInt> setOfIntElt;
  
  // Build the edges of the interface mesh
  if ( !m_strMesh->statusEdges() )
    m_strMesh->buildEdges(); 

  // Build the faces of the volume mesh
  if ( !m_volMesh->statusFaces() )
    m_volMesh->buildFaces(); 

  // For each edge of the structure mesh
  for (felInt i = 0; i < m_strMesh->numEdges(); ++i) {

    // Get nodes of the current structure edge
    verOfEdge[0] = m_strMesh->listEdges()[i]->idBeg();
    verOfEdge[1] = m_strMesh->listEdges()[i]->idEnd();
    pntOfEdge[0] = m_strMesh->listPoints()[verOfEdge[0]];
    pntOfEdge[1] = m_strMesh->listPoints()[verOfEdge[1]];

    if ( m_verbose > 3 )
      std::cout << "Intersector: Processing edge " << i << " = " << verOfEdge[0] << " " << verOfEdge[1] << std::endl;

    // Find volume element to start the intersection
    if ( listVerToElt[ verOfEdge[0] ] != -1 ) {
      idxOfElt = listVerToElt[ verOfEdge[0] ];
    }
    else if ( listVerToElt[ verOfEdge[1] ] != -1 ) {
      idxOfElt = listVerToElt[ verOfEdge[1] ];

      std::swap(verOfEdge[0], verOfEdge[1]);
      std::swap(pntOfEdge[0], pntOfEdge[1]);
    }
    else {
      idxOfElt = getGermElement3D(verOfEdge);
    }

    if ( m_verbose > 3 )
     std::cout << "Interface edge " << i << " = " << verOfEdge[0] << " " << verOfEdge[1] << ", vertex " << verOfEdge[0] << " is in volume element " << idxOfElt << std::endl;

    // Reset previous intersected element to -1
    idxOfPreElt = -1;

    // Look for intersection until pntOfEdge[1] is inside idxOfElt
    do {

      // Get nodes and coordinates of the current tetrahedron
      m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, idxOfElt, verOfTetra, pntOfTetra, 0);

      if ( m_verbose > 9 )
        std::cout << "Volume element " << idxOfElt << " = " << verOfTetra[0] << " " << verOfTetra[1] << " " << verOfTetra[2] << " " << verOfTetra[3] << "." << std::endl;
    
      // Check the position of pntOfEdge[0] wrt to the current element
      isInside0 = GeoUtilities::isPointInTetra(pntOfTetra, pntOfEdge[0], bary, intLocation[0], m_tol);

      if ( !isInside0 ){
        FEL_ERROR("Intersector::findIntersectedElements3D : pntOfEdge[0] must be inside idxOfElt !")
      }
      
      if ( m_verbose > 9 )
        std::cout << "Point0 " << pntOfEdge[0] << " is in element " << idxOfElt << " with case = " << static_cast<std::underlying_type_t<Position>>(intLocation[0].first) << " and position = " <<  static_cast<std::underlying_type_t<Position>>(intLocation[0].second) << "." << std::endl;

      // Add the intersected elements in the list
      addIntersetedEltToList3D(idxOfElt, intLocation[0], setOfIntElt);      

      // Check the position of pntOfEdge[0] wrt to the current element
      isInside1 = GeoUtilities::isPointInTetra(pntOfTetra, pntOfEdge[1], bary, intLocation[1], m_tol);

      // If pntOfEdge[1] is inside the current element, add the intersected elements to the list
      if ( isInside1 ) {

        if ( m_verbose > 9 )
          std::cout << "Point1 " << pntOfEdge[1] << " is in element " << idxOfElt << " with case = " << static_cast<std::underlying_type_t<Position>>(intLocation[1].first) << " and position = " <<  static_cast<std::underlying_type_t<Position>>(intLocation[1].second) << "." << std::endl;

        // Add the intersected elements in the list
        addIntersetedEltToList3D(idxOfElt, intLocation[1], setOfIntElt);
      }
      // If pntOfEdge[1] is outside the current element, we must detect the intersections 
      else {

        if ( m_verbose > 9 )
          std::cout << "Point1 " << pntOfEdge[1] << " is not in the volume element " << idxOfElt << " so there is an intersection to detect." << std::endl;
        
        bool intFnd = false;
        // If verOfEdge[0] on a vertex
        if ( intLocation[0].first == Position::OnVertex ){
          intFnd = computeIntersectionVertex3D(idxOfElt, verOfTetra[intLocation[0].second], idxOfPreElt, idxOfNxtElt, pntOfEdge, intPoint, isInside1);
        }
        // If verOfEdge[0] on an edge
        else if ( intLocation[0].first == Position::OnEdge ) {
          auto& edge = GeometricMeshRegion::m_eltTypMapEdgToVer[GeometricMeshRegion::Tetra4][intLocation[0].second];
          intFnd = computeIntersectionEdge3D(idxOfElt, {verOfTetra[edge[0]], verOfTetra[edge[1]]}, idxOfPreElt, idxOfNxtElt, pntOfEdge, intPoint, isInside1);
        }
        else if ( intLocation[0].first == Position::OnFace ) {
          intFnd = computeIntersectionFace3D(idxOfElt, intLocation[0].second, idxOfPreElt, idxOfNxtElt, pntOfEdge, intPoint, isInside1);
        }
        else { // if ( intLocation[0].first == Position::Inside ) {
          intFnd = computeIntersectionInside3D(idxOfElt, idxOfNxtElt, pntOfEdge, intPoint);
        }

        if ( !intFnd )
          FEL_ERROR("Intersector::findIntersectedElements3D : there must be at least one intersection point !");

        if ( m_verbose > 9 )
          std::cout << "Intersection detected " << intPoint[0] << "." << std::endl;

        // Update pntOfEdge
        pntOfEdge[0] = intPoint[0];

        // Update previous intersected element
        idxOfPreElt = idxOfElt;

        // Update element for new intersections
        idxOfElt = idxOfNxtElt;   
      }
    } while ( !isInside1 );
  }

  // Fill the vector of intersected elements
  listOfIntElt.assign(setOfIntElt.begin(), setOfIntElt.end());

  if ( m_verbose > 9 )
    std::cout << "Intersector: all the edges of the structure have been considered!" << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

void Intersector::addIntersetedEltToList3D(const felInt idxOfElt, const Location& location, std::set<felInt>& setOfIntElt) const
{
  felInt eltIdx;
  std::vector<felInt> verOfTetra(4, 0);

  // Get nodes and coordinates of the current element
  m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, idxOfElt, verOfTetra, 0);

  switch ( location.first ) {

    // Vertex is on a vertex of the element
    case Position::OnVertex : {

      // Variables
      GeometricMeshRegion::ElementType eltTyp;
      GeometricMeshRegion::BallMap ball;
      felInt verPosInBall;

      // Get ball
      m_volMesh->getVertexBall(verOfTetra[location.second], GeometricMeshRegion::Tetra4, idxOfElt, ball);

      // Loop over ball
      for (std::size_t iel = 0; iel < ball.size(); ++iel) {
        std::tie(eltTyp, eltIdx, verPosInBall) = ball[iel];

        // Add element to the set of intersected elements
        setOfIntElt.insert(eltIdx);
      }
      break;
    }
    // Vertex is on an edge of the element
    case Position::OnEdge : {

      // Variables
      GeometricMeshRegion::ElementType eltTyp;
      GeometricMeshRegion::BallMap shell;
      felInt edgPosInShell;

      // Get map edge to ver
      auto& edge = GeometricMeshRegion::m_eltTypMapEdgToVer[GeometricMeshRegion::Tetra4][location.second];

      // Get shell
      m_volMesh->getEdgeShell({verOfTetra[edge[0]], verOfTetra[edge[1]]}, GeometricMeshRegion::Tetra4, idxOfElt, shell);

      // Loop over the shell
      for (std::size_t iel = 0; iel < shell.size(); ++iel) {
        std::tie(eltTyp, eltIdx, edgPosInShell) = shell[iel];

        // Add element to the set of intersected elements
        setOfIntElt.insert(eltIdx);        
      }
      break;
    }
    // Vertex is on a face of the element
    case Position::OnFace : {

      // Variables
      std::vector<felInt> verFacOfElt(3,-1);
      Face facOfElt;

      auto& mapEltFacToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tetra4];

      // Get the ied-th edge of the element
      verFacOfElt[0] = verOfTetra[ mapEltFacToVer[location.second][0] ];
      verFacOfElt[1] = verOfTetra[ mapEltFacToVer[location.second][1] ];
      verFacOfElt[2] = verOfTetra[ mapEltFacToVer[location.second][2] ];
      m_volMesh->getFace(verFacOfElt, facOfElt);

      // Loop over elements sharing the face 
      auto& listNeigh = facOfElt.listNeighVolumes();
      for (std::size_t iel = 0; iel < listNeigh.size(); ++iel) {
        eltIdx = listNeigh[iel]->idVolume();

        // Add element to the set of intersected elements
        setOfIntElt.insert(eltIdx);
      }
      break;
    }
    // Vertex is inside the element
    case Position::Inside : {

      // Add element to the set of intersected elements
      setOfIntElt.insert(idxOfElt);
      break;
    }
    default : {
      FEL_ERROR("Intersector::addIntersetedEltToList3D : the vertex must be on a vertex, edge or inside the triangle !");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionVertex3D(const felInt idxOfElt, const felInt idxOfVer, const felInt idxOfPreElt, felInt& idxOfNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const
{
  GeometricMeshRegion::ElementType eltTyp;
  felInt eltIdx;
  felInt verPos;
  unsigned short int ifa;
  int nbrIntPnt;
  std::vector<Point*> pntOfNxtElt(4, nullptr);
  std::vector<felInt> verOfNxtElt(4, 0);
  std::array<double,4> bary;
  std::array<Point*,3> pntFacOfElt;

  GeometricMeshRegion::BallMap ball;

  const std::array<const unsigned short int,4> mapVerToOppFac{2, 3, 1, 0};
  
  auto& mapEltFacToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tetra4];

  // Get ball
  m_volMesh->getVertexBall(idxOfVer, GeometricMeshRegion::Tetra4, idxOfElt, ball);

  // Loop over ball
  for (std::size_t iel = 0; iel < ball.size(); ++iel) {
    std::tie(eltTyp, eltIdx, verPos) = ball[iel];

    if ( eltIdx == idxOfPreElt )
      continue;

    // Get nodes and coordinates of the current element
    m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, eltIdx, verOfNxtElt, pntOfNxtElt, 0);

    // Check if pntOfEdge[1] is inside the current element
    // If it is not inside, look for an intersection with the opposite face
    if ( !GeoUtilities::isPointInTetra(pntOfNxtElt, pntOfEdge[1], bary, m_tol) ) {

      // Get the points of the face opposite to the vertex
      ifa = mapVerToOppFac[verPos];
      for (std::size_t ive = 0; ive < pntFacOfElt.size(); ++ive)
        pntFacOfElt[ive] = pntOfNxtElt[ mapEltFacToVer[ifa][ive] ];

      // Compute intersection with opposite face
      nbrIntPnt = GeoUtilities::computeSegmentTriangleIntersection(pntOfEdge[0], pntOfEdge[1], *pntFacOfElt[0], *pntFacOfElt[1], *pntFacOfElt[2], intPoint[0], m_tol);

      // If intersection
      if ( nbrIntPnt > 0 ) {

        if ( m_verbose > 9 )
          std::cout << "computeIntersectionVertex3D: Intersection detected in " << intPoint[0] << "." << std::endl;

        // Return next element to intersect
        idxOfNxtElt = eltIdx;

        return true;
      }
    }
    // If it is inside, nothing to do
    else {

      if ( m_verbose > 9 )
        std::cout << "computeIntersectionVertex3D: Point " << pntOfEdge[1] << " has been found in element " << eltIdx << "." << std::endl;

      // Set intersection
      intPoint[0] = pntOfEdge[1];

      // Return next element to intersect
      idxOfNxtElt = eltIdx;

      // Point inside element
      isInside = true;

      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionEdge3D(const felInt idxOfElt, const std::vector<felInt>& idxOfVer, const felInt idxOfPreElt, felInt& idxOfNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const
{
  GeometricMeshRegion::ElementType eltTyp;
  felInt eltIdx;
  felInt edgPos;
  int nbrIntPnt;
  std::vector<Point*> pntOfNxtElt(4, nullptr);
  std::vector<felInt> verOfNxtElt(4, 0);
  std::array<double,4> bary;
  std::array<Point*,3> pntFacOfNxtElt;

  GeometricMeshRegion::BallMap shell;

  auto& mapEltFacToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tetra4];
  auto& mapEltEdgToFac = GeometricMeshRegion::m_eltTypMapEdgToFac[GeometricMeshRegion::Tetra4];

  // Get shell
  m_volMesh->getEdgeShell(idxOfVer, GeometricMeshRegion::Tetra4, idxOfElt, shell);

  // Loop over the shell
  for (std::size_t iel = 0; iel < shell.size(); ++iel) {
    std::tie(eltTyp, eltIdx, edgPos) = shell[iel];

    if ( eltIdx == idxOfPreElt )
      continue;
    
    // Get nodes and coordinates of the current element
    m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, eltIdx, verOfNxtElt, pntOfNxtElt, 0);

    // Check if pntOfEdge[1] is inside the current element
    // If it is not inside, look for an intersection with the opposite edges
    if ( !GeoUtilities::isPointInTetra(pntOfNxtElt, pntOfEdge[1], bary, m_tol) ) {

      // Loop over the two opposite faces
      for (std::size_t ifa = 0; ifa < 4; ++ifa) {

        // Skip face
        if ( ifa == mapEltEdgToFac[edgPos][0] || ifa == mapEltEdgToFac[edgPos][1] )
          continue;

        // Get the edge
        pntFacOfNxtElt[0] = pntOfNxtElt[ mapEltFacToVer[ifa][0] ];
        pntFacOfNxtElt[1] = pntOfNxtElt[ mapEltFacToVer[ifa][1] ];
        pntFacOfNxtElt[2] = pntOfNxtElt[ mapEltFacToVer[ifa][2] ];

        // Compute intersection with opposite edge
        nbrIntPnt = GeoUtilities::computeSegmentTriangleIntersection(pntOfEdge[0], pntOfEdge[1], *pntFacOfNxtElt[0], *pntFacOfNxtElt[1], *pntFacOfNxtElt[2], intPoint[0], m_tol);

        // If intersection
        if ( nbrIntPnt > 0 ) {

          if ( m_verbose > 9 )
            std::cout << "computeIntersectionEdge3D: Intersection detected in " << intPoint[0] << "." << std::endl;

          // Return next element to intersect
          idxOfNxtElt = eltIdx;

          return true;
        }
      }
    }
    // If it is inside, nothing to do
    else {

      if ( m_verbose > 9 )
        std::cout << "computeIntersectionEdge3D: Point " << pntOfEdge[1] << " has been found in element " << eltIdx << "." << std::endl;

      // Set intersection
      intPoint[0] = pntOfEdge[1];

      // Return next element to intersect
      idxOfNxtElt = eltIdx;

      // Point inside element
      isInside = true;

      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionFace3D(const felInt idxOfElt, const int idxFacInt, const felInt idxOfPreElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const
{
  felInt eltIdx;
  int nbrIntPnt;
  std::vector<Point*> pntOfNxtElt(4, nullptr);
  std::vector<felInt> verOfNxtElt(4, 0);
  std::vector<felInt> verOfElt(4, 0);
  std::vector<felInt> verFacOfElt(3,-1);
  std::array<double,4> bary;
  std::array<felInt,3> verFacOfNxtElt;
  std::array<Point*,3> pntFacOfNxtElt;
  Face facOfElt;

  auto& mapEltFacToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tetra4];

  // Get nodes and coordinates of idxOfElt
  m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, idxOfElt, verOfElt, 0);

  // Get the ied-th edge of the triangle
  verFacOfElt[0] = verOfElt[ mapEltFacToVer[idxFacInt][0] ];
  verFacOfElt[1] = verOfElt[ mapEltFacToVer[idxFacInt][1] ];
  verFacOfElt[2] = verOfElt[ mapEltFacToVer[idxFacInt][2] ];
  m_volMesh->getFace(verFacOfElt, facOfElt);

  // Loop over elements sharing the face 
  auto& listNeigh = facOfElt.listNeighVolumes();
  for (std::size_t iel = 0; iel < listNeigh.size(); ++iel) {
    eltIdx = listNeigh[iel]->idVolume();

    if ( eltIdx == idxOfPreElt )
      continue;

    // Get nodes and coordinates of the current element
    m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, eltIdx, verOfNxtElt, pntOfNxtElt, 0);

    // Check if pntOfEdge[1] is inside the current element
    // If it is not inside, look for an intersection with the opposite edges
    if ( !GeoUtilities::isPointInTetra(pntOfNxtElt, pntOfEdge[1], bary, m_tol) ) {

      // Loop over the three opposite faces
      for (std::size_t ifa = 0; ifa < 4; ++ifa) {
        
        // Get vertices of the ied-th face
        verFacOfNxtElt[0] = verOfNxtElt[ mapEltFacToVer[ifa][0] ];
        verFacOfNxtElt[1] = verOfNxtElt[ mapEltFacToVer[ifa][1] ]; 
        verFacOfNxtElt[2] = verOfNxtElt[ mapEltFacToVer[ifa][2] ]; 

        // Skip edge 
        if ( std::is_permutation(verFacOfElt.begin(), verFacOfElt.end(), verFacOfNxtElt.begin()) )
          continue;

        // Get the face
        pntFacOfNxtElt[0] = pntOfNxtElt[ mapEltFacToVer[ifa][0] ];
        pntFacOfNxtElt[1] = pntOfNxtElt[ mapEltFacToVer[ifa][1] ];
        pntFacOfNxtElt[2] = pntOfNxtElt[ mapEltFacToVer[ifa][2] ];

        // Compute intersection with the edge
        nbrIntPnt = GeoUtilities::computeSegmentTriangleIntersection(pntOfEdge[0], pntOfEdge[1], *pntFacOfNxtElt[0], *pntFacOfNxtElt[1], *pntFacOfNxtElt[2], intPoint[0], m_tol);
        
        // If intersection
        if ( nbrIntPnt > 0 ) {

          if ( m_verbose > 9 )
            std::cout << "computeIntersectionFace3D: Intersection detected in " << intPoint[0] << "." << std::endl;

          // Return next element to intersect
          idxNxtElt = eltIdx;

          return true;
        }
      }
    }
    // If it is inside, nothing to do
    else {

      if ( m_verbose > 9 )
        std::cout << "computeIntersectionFace3D: Point " << pntOfEdge[1] << " has been found in element " << eltIdx << "." << std::endl;

      // Set intersection
      intPoint[0] = pntOfEdge[1];

      // Return next element to intersect
      idxNxtElt = eltIdx;

      // Point inside element
      isInside = true;

      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool Intersector::computeIntersectionInside3D(const felInt idxOfElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint) const
{
  int nbrIntPnt;
  std::vector<Point*> pntOfNxtElt(4, nullptr);
  std::vector<felInt> verOfNxtElt(4, 0);
  std::array<Point*,3> pntFacOfElt;

  auto& mapEltFacToVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tetra4];

  // Get nodes and coordinates of the current triangle
  m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, idxOfElt, verOfNxtElt, pntOfNxtElt, 0);

  // Loop over the faces
  for (std::size_t ifa = 0; ifa < 4; ++ifa) {

    // Get the face
    pntFacOfElt[0] = pntOfNxtElt[ mapEltFacToVer[ifa][0] ];
    pntFacOfElt[1] = pntOfNxtElt[ mapEltFacToVer[ifa][1] ];
    pntFacOfElt[2] = pntOfNxtElt[ mapEltFacToVer[ifa][2] ];

    // Compute intersection with the face
    nbrIntPnt = GeoUtilities::computeSegmentTriangleIntersection(pntOfEdge[0], pntOfEdge[1], *pntFacOfElt[0], *pntFacOfElt[1], *pntFacOfElt[2], intPoint[0], m_tol);

    // If intersection
    if ( nbrIntPnt > 0 ) {

      // Return next element to intersect
      idxNxtElt = idxOfElt;

      return true;
    }    
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

felInt Intersector::getGermElement3D(const std::vector<felInt>& verOfEdge) const
{
  std::vector<Point*> pntOfTetra(4, nullptr);
  std::vector<felInt> verOfTetra(4, 0);
  std::array<double, 4> bary;

  // Go through all elements of the mesh
  for (felInt k = 0; k < m_volMesh->numElements(GeometricMeshRegion::Tetra4); ++k) {

    // Get nodes of the current triangle
    m_volMesh->getOneElement(GeometricMeshRegion::Tetra4, k, verOfTetra, pntOfTetra, 0); 

    if( GeoUtilities::isPointInTetra( pntOfTetra, m_strMesh->listPoints()[verOfEdge[0]], bary, m_tol) )
      return k;
  }

  FEL_ERROR("Intersector::getGermElement3D cannot localize the point into the volume mesh.");
  return -1;  
}

}
