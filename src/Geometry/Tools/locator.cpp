//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes
#include <numeric>

// External includes

// Project includes
#include "Geometry/Tools/locator.hpp"
#include "Geometry/geo_utilities.hpp"

namespace felisce 
{

Locator::Locator(GeometricMeshRegion* mesh, const bool allowProjection, const double tolRescaling, const int verbose):
  m_allowProjection(allowProjection), m_verbose(verbose)
{
  // Set tolerance
  m_tol = GeoUtilities::GEO_TOL * tolRescaling;

  // Set mesh pointer
  m_mesh = mesh;

  // Allocate and Initialize array m_markElement
  for (std::size_t i_eltyp = 0; i_eltyp < m_markElement.size(); ++i_eltyp)
    m_markElement[i_eltyp] = nullptr;

  ElementType eltType;
  for (std::size_t i = 0; i < m_mesh->bagElementTypeDomain().size(); ++i) {
    eltType = m_mesh->bagElementTypeDomain()[i];

    m_markElement[eltType] = new std::size_t[m_mesh->numElements(eltType)];
    for (felInt k = 0; k < m_mesh->numElements(eltType); ++k)
      m_markElement[eltType][k] = 0;
  }
  m_mark = 0;

  // Build edges or faces
  if ( m_mesh->domainDim() == 2 ) {
    if ( !m_mesh->statusEdges() )
      m_mesh->buildEdges();
  } else if ( m_mesh->domainDim() == 3 ) {
    if ( !m_mesh->statusFaces() )
      m_mesh->buildFaces();
  }

  // Build bounding box
  m_bbox = felisce::make_shared<BoundingBox>(*m_mesh);
  m_bbox->computeDelta();

  // If default hashtab size < numPoints reduce it
  if ( static_cast<std::size_t>(m_mesh->numPoints()) < HashTable::HASHSIZE )
    m_hash = felisce::make_shared<HashTable>(m_mesh, static_cast<std::size_t>(m_mesh->numPoints()), m_bbox.get());
  else
    m_hash = felisce::make_shared<HashTable>(m_mesh, HashTable::HASHSIZE, m_bbox.get());
}

/***********************************************************************************/
/***********************************************************************************/

Locator::~Locator()
{
  for (std::size_t i_eltyp = 0; i_eltyp < m_markElement.size(); ++i_eltyp) {
    if( m_markElement[i_eltyp] != nullptr ) {

      delete [] m_markElement[i_eltyp];
      m_markElement[i_eltyp] = nullptr;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Locator::findListPointsInMesh(std::vector<felInt>& indElt, std::vector<ElementType>& typElt, const std::vector<Point>& lstPoints)
{
  std::vector<seedElement> elt;

  findListPointsInMesh(elt, lstPoints);

  for (std::size_t i = 0; i < lstPoints.size(); ++i) {
    typElt[i] = elt[i].first;
    indElt[i] = elt[i].second; 
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Locator::findListPointsInMesh(std::vector<seedElement>& elt, const std::vector<Point>& lstPoints)
{
  elt.assign(lstPoints.size(), seedElement(GeometricMeshRegion::ELEMENT_TYPE_COUNTER, -1) );

  // This method requires the hashtable
  if ( !m_hash ) FEL_ERROR("Localization: hashtable not allocated");

  // For each point of the structure mesh
  for (std::size_t k = 0; k < lstPoints.size(); ++k) { 
        
    if ( m_verbose > 3 ) 
      std::cout << "Localization: Processing point " << k << " = " << lstPoints[k] <<std::endl;

    // Point has not been localised yet
    if( elt[k].second == -1 ) { 

      // Locate the point and even project it if necessary - when it is inside the box but not inside the mesh
      if ( !(findPointInMesh(elt[k], lstPoints[k])) ) {
        // Failure
        if ( m_allowProjection )
          FEL_ERROR( "Localization: point " + std::to_string(k) + " has not been localized even with the projection on the mesh!" );
        
        std::cout << "Localization: point " + std::to_string(k) + " has not been localized!" << std::endl;
      }
    }
  }

  if ( m_verbose > 3 )
    std::cout << "Localization: all points in the list have been analysed!" << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

bool Locator::findPointInMesh(seedElement& elt, const Point& point)
{
  ElementType eltType;
  seedElement tmpSeed;
  Point pointInRefElm;
  std::size_t mark = ++getMark();
 
  // If the point is in the bounding box of the mesh                         
  if( m_bbox->isInBBox(point, m_tol) ) { 

    // Try to locate the point in an optimized way with the hashtable
    tmpSeed = m_hash->findSeed(point); 
    tmpSeed = localizePoint(tmpSeed, point, pointInRefElm, mark);

    // If succeeded, return True
    if ( tmpSeed.first != GeometricMeshRegion::ELEMENT_TYPE_COUNTER && tmpSeed.second > -1 ) { 

      if ( m_verbose > 3 )
        std::cout << "Localization: success, point located using the hash table!" << std::endl;

      // Copy information from the hashtable
      elt = tmpSeed;

      return true;
    } 

    // If failure,
    if ( m_verbose > 3 )
      std::cout << "Localization: failure, point cannot be located using the hash table!" << std::endl;

    // Go through all type of elements of mesh
    mark = ++getMark();
    for (std::size_t l = 0; l < m_mesh->bagElementTypeDomain().size(); ++l) {
      eltType = m_mesh->bagElementTypeDomain()[l];

      const int nbrPnt = GeometricMeshRegion::m_numPointsPerElt[eltType];
      std::vector<felInt> veridxOfElt( nbrPnt, 0);
      std::vector<Point*> pointsOfElt( nbrPnt, nullptr);

      // Go through all elements of this type
      for (felInt jel = 0; jel < m_mesh->numElements(eltType); ++jel) {
        tmpSeed.first  = eltType;
        tmpSeed.second = jel;

        // Skip already visited elements
        if ( markElement(eltType, jel) == mark )
          continue;

        // Check bounding box
        m_mesh->getOneElement(eltType, jel, veridxOfElt, pointsOfElt, 0);
        BoundingBox box(m_mesh->numCoor(), pointsOfElt);

        if ( !box.isInBBox(point, m_tol) )
          continue;

        // Check if point inside current element
        if ( m_mesh->findLocalCoord(tmpSeed.first, tmpSeed.second, point, pointInRefElm) ) {

          if ( m_verbose > 3 )
            std::cout << "Localization: success, the point has been located in one of the element of the mesh!" << std::endl;

          // Copy information from the hashtable
          elt = tmpSeed;

          return true;
        }
      }

      // If failure
      if ( m_verbose > 3 )
        std::cout << "Localization: failure, point cannot be found into the mesh, projection needed!" << std::endl;   
    }
  }

  // If the point is outside the bounding box or it needs to be projected
  if ( m_allowProjection ) {

    // If the projection is successful
    if( projectPointOnMesh(elt, point) ) {

      if ( m_verbose > 3 )
        std::cout << "Localization: success, the projection has been located in one of the element of the mesh!" << std::endl;

      return true;
    }
  }

  // If failure
  if ( m_verbose > 3 )
    std::cout << "Localization: failure, the point (as well as its projection if enable) has not been localised into the mesh!" << std::endl;

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

Locator::seedElement Locator::localizePoint(seedElement seed, const Point& point, Point& pointInRefElm, std::size_t mark)
{

  // Localize point in mesh
  while (1) {

    // Boundary reached
    if ( seed.second < 0 ) {
      seed.first  = GeometricMeshRegion::ELEMENT_TYPE_COUNTER;
      seed.second = -1;
      
      return seed;
    }

    // Element already marked -> looping
    if ( markElement(seed.first, seed.second) == mark ) {
      seed.first  = GeometricMeshRegion::ELEMENT_TYPE_COUNTER;
      seed.second = -1;

      return seed;
    }

    // Mark current element
    markElement(seed.first, seed.second) = mark;

    // If point inside element, seed fuond. Otherwise go to next element
    if ( m_mesh->findLocalCoord(seed.first, seed.second, point, pointInRefElm) )
      return seed;
  }

  FEL_ERROR("Localization: this point should never be reached seed found, or stuck on boundary or cycle");

  return seed;
}

/***********************************************************************************/
/***********************************************************************************/

bool Locator::projectPointOnMesh(seedElement& seed, const Point& pntIn)
{
  bool   isProjectionInMesh, isProjectionFound;
  int    numPointPerElt;
  double distProjPlane;
  double distToElt, minDistToElt;
  Point  projection_tmp, projection;

  std::vector<felInt> veridxOfElt;
  std::vector<Point*> pointsOfElt;

  ElementType eltTypeBD;

  minDistToElt = std::numeric_limits<double>::max();

  // Go through all the boundary elements of the mesh 
  isProjectionFound = false;
  const std::vector<ElementType>& bagElementTypeBoundary = m_mesh->bagElementTypeDomainBoundary();
  for (std::size_t i = 0; i < bagElementTypeBoundary.size(); ++i) {
    eltTypeBD = bagElementTypeBoundary[i];

    // Resize vectors
    numPointPerElt = GeometricMeshRegion::m_numPointsPerElt[eltTypeBD];
    veridxOfElt.assign(numPointPerElt, 0);
    pointsOfElt.assign(numPointPerElt, nullptr);

    for (felInt ii = 0; ii < m_mesh->numElements(eltTypeBD); ++ii) {

      // Get vertices of the current element
      m_mesh->getOneElement(eltTypeBD, ii, veridxOfElt, pointsOfElt, 0);

      // We compute the element normal
      GeoUtilities::projectOnGeomEntity( static_cast<felInt>(m_mesh->domainDim()), pointsOfElt, pntIn, projection_tmp);

      // Compute the distance from the point to its projection on the plane
      distProjPlane = projection_tmp.dist(pntIn);

      // If the new projected point is closer than the previous ones
      if ( distProjPlane < minDistToElt ) {

        // Get closest projection
        GeoUtilities::getClosestProjectionOnElement(eltTypeBD, pointsOfElt, projection_tmp, projection_tmp, m_tol);

        // Compute total distance from the point and its projection in the current considered element
        distToElt = projection_tmp.dist(pntIn);

        // Looking for the minimal total distance
        if ( distToElt < minDistToElt ) {

          // Update minimal distance
          minDistToElt = distToElt;

          // Save coordinates of the point which minimizes the total distance
          projection = projection_tmp;

          // Projection found 
          isProjectionFound = true;
        }
      }
    }
  }

  if ( !isProjectionFound ) {

    if ( m_verbose > 3 )
      std::cout << "Localization: failure, no valid projection could be found!" << std::endl;

    seed.first  = GeometricMeshRegion::ELEMENT_TYPE_COUNTER;
    seed.second = -1;

    return false;
  }


  if ( m_verbose > 3 )
    std::cout << "Localization: Projected point is " << projection << std::endl;

  // Turn off projection to avoid infinity loop
  m_allowProjection = false;

  // Try to localize the projected point
  isProjectionInMesh = findPointInMesh(seed, projection);

  // Turn on projection
  m_allowProjection = true;

  return isProjectionInMesh;
}

/***********************************************************************************/
/***********************************************************************************/

void Locator::resetMarkElement()
{
  // Reset m_markElement
  ElementType eltType;
  for (std::size_t i = 0; i < m_mesh->bagElementTypeDomain().size(); ++i) {
    eltType = m_mesh->bagElementTypeDomain()[i];

    if ( m_markElement[eltType] ) {

      // Reset arrays
      for (felInt k = 0; k < m_mesh->numElements(eltType); ++k)
        m_markElement[eltType][k] = 0;
    }
  }

  m_mark = 0;
}

}
