//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef FEL_INTERSECTOR_HPP
#define FEL_INTERSECTOR_HPP

// System includes

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Geometry/geo_utilities.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

class Intersector
{
  public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of Intersector
    FELISCE_CLASS_POINTER_DEFINITION(Intersector);

    using Location = GeoUtilities::Location;
    using Position = GeoUtilities::Position;

    ///@}
    ///@name Life Cycle
    ///@{

    Intersector() = delete;

    Intersector(GeometricMeshRegion* volMesh, GeometricMeshRegion* strMesh, const double tolRescaling = 1., const int verbose = 0);

    Intersector(const Intersector& ) = delete;

    Intersector(Intersector&& ) = delete;

    Intersector& operator=(const Intersector& ) = delete;

    Intersector& operator=(Intersector&& ) = delete;

    ~Intersector() = default;

    ///@}  
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /// Detect the volume elements intersected by the interface
    void intersectMeshes(const std::vector<felInt>& listVerToElt, std::vector<felInt>& listOfIntElt)  const;

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    ///@}
    ///@name Friends
    ///@{

    ///@}
  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}
  private:
    ///@name Private static Member Variables
    ///@{

    ///@}
    ///@name Private member Variables
    ///@{

    /// Volume mesh
    GeometricMeshRegion* m_volMesh;

    /// Structure mesh 
    GeometricMeshRegion* m_strMesh;

    /// Tolerance
    double m_tol;

    /// Verbose level
    const int m_verbose; 

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    /**
     * @brief Main method of intersection 2D.
     *        Warning: all points of mesh m_strMesh must be inside mesh m_volMesh.
     */
    void findIntersectedElements2D(const std::vector<felInt>& listVerToElt, std::vector<felInt>& listOfIntElt) const;

    /**
     * @brief Main method of intersection 3D.
     *        Warning: all points of mesh m_strMesh must be inside mesh m_volMesh.
     *        Warning: In 3D this class simply computes the intersections between the edges of the interface mesh
     *                 and the elements of the volume mesh. However there may exist elements of the volume mesh
     *                 that are intersected by the interface but not by its edges.
     *                 This means that some intersected elements may be not detected...
     */    
    void findIntersectedElements3D(const std::vector<felInt>& listVerToElt, std::vector<felInt>& listOfIntElt) const;

    /// Find vertex germ 2D
    felInt getGermElement2D(const std::vector<felInt>& verOfEdge) const;

    /// Find vertex germ  3D
    felInt getGermElement3D(const std::vector<felInt>& verOfEdge) const;

    /// Compute next intersection when the previous intersection is on a vertex of the element
    bool computeIntersectionVertex2D(const felInt idxOfTria, const felInt idxOfVer, const felInt idxOfPreTria, felInt& idxNxtTria, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const;
    
    /// Compute next intersection when the previous intersection is on an edge of the element
    bool computeIntersectionEdge2D(const felInt idxOfTria, const int idxEdgInt, const felInt idxOfPreTria, felInt& idxNxtTria, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const;
    
    /// Compute next intersection when the previous intersection is inside the element
    bool computeIntersectionInside2D(const felInt idxOfTria, felInt& idxNxtTria, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint) const;

    /// Compute next intersection when the previous intersection in on a vertex of the element
    bool computeIntersectionVertex3D(const felInt idxOfElt, const felInt idxOfVer, const felInt idxOfPreElt, felInt& idxOfNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const;
    
    /// Compute next intersection when the previous intersection is on an edge of the element
    bool computeIntersectionEdge3D(const felInt idxOfElt, const std::vector<felInt>& idxOfVer, const felInt idxOfPreElt, felInt& idxOfNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const;
    
    /// Compute next intersection when the previous intersection is on a face of the element
    bool computeIntersectionFace3D(const felInt idxOfElt, const int idxFacInt, const felInt idxOfPreElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint, bool& isInside) const;
    
    /// Compute next intersection when the previous intersection is inside the element
    bool computeIntersectionInside3D(const felInt idxOfElt, felInt& idxNxtElt, const std::vector<Point>& pntOfEdge, std::vector<Point>& intPoint) const;

    /// Add intersected elements to the intersected elements list: 2D case
    void addIntersetedEltToList2D(const felInt idxOfTria, const Location& location, std::set<felInt>& setOfIntElt) const;
    
    /// Add intersected elements to the intersected elements list: 3D case
    void addIntersetedEltToList3D(const felInt idxOfElt, const Location& location, std::set<felInt>& setOfIntElt) const;

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Private LifeCycle
    ///@{

    ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif /* _FEL_INTERSECTOR_HPP  defined */

