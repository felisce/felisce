//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & V. Martin
//

#ifndef NEIGHVOLUMESOFFACES_HPP
#define NEIGHVOLUMESOFFACES_HPP

// System includes
#include <ostream>

// External includes

// Project includes
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce 
{
///////////////////////////////////////
// NEIGHBOURS VOLUMES OF FACES CLASS //
///////////////////////////////////////
/*!
\class NeighVolumesOfFaces
\authors J.Castelneau & J.Foulon

\brief Class implementing the volumes neighbours

A neighbour is defined by
- his global number
- his felisce name
- the local number of the face

*/

class NeighVolumesOfFaces {
public:

  typedef GeometricMeshRegion::ElementType ElementType;

  // Constructor
  // ===========
  // NeighVolumesOfFaces() {}
  // ~NeighVolumesOfFaces() {}

  // getter
  // ======
  inline const felInt & idVolume() const        {
    return m_idVolume;
  }
  inline const ElementType & typeVolume() const {
    return m_typeVolume;
  }
  inline const int & idLocalFace() const        {
    return m_idLocalFace;
  }


  // setter
  // ======
  inline felInt & idVolume()        {
    return m_idVolume;
  }
  inline ElementType & typeVolume() {
    return m_typeVolume;
  }
  inline int & idLocalFace()        {
    return m_idLocalFace;
  }

  // print a volume neighbour
  // ========================
  void print(  std::ostream& outstr = std::cout, int verbose = 0 ) const;

private:
  felInt      m_idVolume;    //!volume's ID per eltType
  ElementType m_typeVolume;  //!eltType of the volume
  int         m_idLocalFace; //!local number of the face in the volume

};

}

#endif
