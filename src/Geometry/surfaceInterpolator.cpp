//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "Geometry/surfaceInterpolator.hpp"
#include "Geometry/point.hpp"
#include "Geometry/geo_utilities.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce {
//default parameter for alternate labels: an empty std::vector.
void SurfaceInterpolator::buildInterpolator( PetscMatrix& interpolator,
                                              const std::vector< std::pair< Point*, felInt > >& pointsAndCorrespondingSurfLabel,
                                              const std::vector<int> rowNumbering,
                                              const Variable& var, felInt ivar, std::size_t iBD ) {
  std::vector<int> alternativeLabels;
  std::string logFile="logFile";
  this->buildInterpolator( interpolator,
                            pointsAndCorrespondingSurfLabel,
                            rowNumbering,
                            var,ivar,iBD,
                            alternativeLabels,
                            logFile);
}

namespace {
  void printLog(std::ostream& log, int id, Point* thePoint, Point& pointProjection, double distance) {
    log<<id<<'\t';
    thePoint->printWithTab(log);
    log<<'\t';
    pointProjection.printWithTab(log);
    log<<'\t'<<distance<<std::endl;
  }
}

/*
  * Let f_1 be a finite-element function defined on the surface on the current domain (1).
  *   f_1(x) = sum_j phi_{1,j}(x) f_{1,j}
  *   where phi_{1,j} are the basis function of the surface.
  *
  * We want to pass this information to another domain (2) that share a surface, but has not conforming mesh.
  *
  *   f_2(x) = sum_i phi_{2,i}(x) f_{2,i}
  * where f_{2,i} = f_1(x_i), x_i are the points coming from the second domain.
  * f_{2,i}=f_1(x_i) = sum_j phi_{1,j}(x_i) f_{1,j}.
  *
  * From this we get the matrix representation of the relationship between the two finite-element representation.
  * f_{2,i}= A_{i,j} f_{1,j}
  * where A_{i,j} = phi_{1,j}(x_i) is the interpolator that we want to build in this function!
  *
  * A will be parallel with respect to the column!
  */
void SurfaceInterpolator::buildInterpolator( PetscMatrix& interpolator,
                        const std::vector< std::pair< Point*, felInt > >& pointsAndCorrespondingSurfLabel,
                        const std::vector<int> rowNumbering,
                        const Variable& var, felInt ivar, std::size_t iBD,
                        const std::vector<int> alternativeLabels, std::string logFile) 
{
  if ( MpiInfo::rankProc() == 0 ) {
    std::ofstream log;
    log.open(logFile.c_str());
  // First we extract the information.
  // the points in pointsAndCorrespondingSurfLabel are the x_i.

  // Warning: if the x_i belongs to more than one label, it does not matter.
  //          in fact, we assume that the edges of the surface are geometrically conforming. Which means that they can have, as 1D manifolds, different finite-element
  //          representation, but they overlap.

  // Number of points coming from the other mesh.
  felInt nPoints = pointsAndCorrespondingSurfLabel.size(); // physical points that can support more than one dofs. Think about std::vector variables.
  if ( nPoints == 0 )
    FEL_ERROR("Zero points in surface interpolator.");
  // Number of dofs of the other mesh, which is also the number of rows
  m_nrows[iBD] = rowNumbering.size();
  // We assume that all the points support the same number of dofs
  FEL_ASSERT( m_nrows[iBD] % nPoints == 0 );
  // We compute this number
  felInt nComp = m_nrows[iBD]/nPoints; // 1 if we are passing scalars, 2 or 3 if we are passing vectors

  // We first save the interpolator in this data structure
  // We assume that rowNumbering contains all and only the numbers from 0 to m_nrows-1;
  std::vector< std::vector < std::pair < /*column numbering*/ felInt, /*value*/ double > > > temporaryInterpolator;
  temporaryInterpolator.resize(m_nrows[iBD]);

  // eltType will be used in the loop to distinguish between triangles and quadrangles
  // previousEltType will be used to store the previous value of eltType and check if it has changed
  // It is initialized to Tetra4 because Tetra4 can not be an element of the boundary, the idea is to std::set this element type to "NULL"
  ElementType eltType, previousEltType=GeometricMeshRegion::Tetra4;
  // Vector to store vertex of the current element
  std::vector< Point*> elemPoint;
  // Curvilinear finite element on the boundary, to access basis functions
  CurvilinearFiniteElement* currentFe=nullptr;
  // Number of degrees of freedom of the current element
  felInt nDofEle = 0;
  // Global index of the element
  felInt ielSupportDof;
  // Id of the column
  felInt idColumn;
  // Auxiliary vectors to temporary store the values of the matrix
  std::vector<double> values;
  // projected point
  Point pointProjection(0.0);

  int idForLog = 0; //0 inside elem, 1 inside elem alternative lab, 2 edge, 3 point 4 not found

  const int iMesh = var.idMesh();

  // For each x_i
  int pointNotFound(0);
  int foundInsideAnElement(0), foundInsideAnElementAltLab(0), foundInEdge(0),foundInPoint(0);
  for ( std::size_t cPoint(0); cPoint< (std::size_t) nPoints; ++cPoint) {

    // 1) IDENTIFY THE ELEMENT, ON THIS MESH, CLOSEST TO THE POINT.
    std::map<int,double> iel2ScoreByEdge;
    double pointDistance(1.e20);
    int idOfElOfClosestPoint,ielSupportDofByType(0);
    int notFound;
    Point* thePoint = pointsAndCorrespondingSurfLabel[cPoint].first;
    // 1a) check if the projection can be done on a triangle on the given label.
    //     at the same time, best point and best edges are computed for the current label
      notFound = getClosestSurfaceElement(thePoint, /* [in] the point*/
                                          pointsAndCorrespondingSurfLabel[cPoint].second,/* [in] the surface label where to look for*/
                                          eltType,         /* [out] type of the element */
                                          elemPoint,       /* [out] verteces of the element */
                                          pointProjection, /* [out] the projection of the given point on the element*/
                                          ielSupportDof,   /* [out] the id of the element*/
                                          iel2ScoreByEdge, /* [out] updated std::unordered_map from ielByType to the best edge score*/
                                          pointDistance,   /* [out] updated best distance from a point of the mesh*/
                                          idOfElOfClosestPoint); /* [out] updated id of the closest point of the mesh*/
      foundInsideAnElement += (1-notFound);
      if ( ! notFound ) {
        idForLog=0;
      }
      if ( notFound ) {
        // 1b) check if the projection can be done on a triangle on the other possible labels.
        //     at the same time, best point and best edges are possibily updated for the current label
        for ( std::size_t iL(0); iL<alternativeLabels.size() && notFound; ++iL ) {
          if ( alternativeLabels[iL] != pointsAndCorrespondingSurfLabel[cPoint].second ) {
            notFound = getClosestSurfaceElement(thePoint, /* [in] the point*/
                                                alternativeLabels[iL],/* [in] the surface label where to look for*/
                                                eltType,        /* [out] type of the element */
                                                elemPoint,      /* [out] verteces of the element */
                                                pointProjection,/* [out] the projection of the given point on the element*/
                                                ielSupportDof,iel2ScoreByEdge,pointDistance,idOfElOfClosestPoint); /* [out] the id of the element*/
            foundInsideAnElementAltLab+= (1-notFound);
            if ( ! notFound ) {
              idForLog=1;
            }
          }
        }
        // 1c) It was not possible to project it on a triangle, we have to project it either on an edge or on a point
        if ( notFound ) {
          // 1d) we look for the best edge
          double best=100;
          for ( auto it = iel2ScoreByEdge.begin(); it != iel2ScoreByEdge.end(); ++it ) {
            if ( it->second < best ) {
              ielSupportDofByType = it->first;
              best = it->second;
            }
          }
          // 1e) we check if it is better to project on a point instead
          if ( best > pointDistance ) {
            foundInPoint++;
            ielSupportDofByType = idOfElOfClosestPoint;
            idForLog=3;
          } else {
            foundInEdge++;
            idForLog=2;
          }
          // We hope the eltType of the element is the same on all the alternative labels..todo!!
          ElementType eltType2 = getETandAssert2D(m_pb->mesh(iMesh)->intRefToEnum().at(pointsAndCorrespondingSurfLabel[cPoint].second));
          ielSupportDof=0;//very important
          m_pb->mesh(iMesh)->getIdElemFromTypeElemAndIdByType(eltType,ielSupportDofByType,ielSupportDof);
          std::vector<int> elemIdPoint( GeometricMeshRegion::m_numPointsPerElt[eltType2] );
          elemPoint.resize( GeometricMeshRegion::m_numPointsPerElt[eltType2] );

          m_pb->mesh(iMesh)->getOneElement(eltType2, ielSupportDofByType, elemIdPoint, 0);

          for (int iPoint = 0; iPoint < GeometricMeshRegion::m_numPointsPerElt[eltType2]; iPoint++) {
            elemPoint[iPoint] = &m_pb->mesh(iMesh)->listPoints()[elemIdPoint[iPoint]];
          }

          if ( best > pointDistance ) {//case of the point
            projectOnBestPoint ( elemPoint,thePoint,pointProjection);
          } else {//case of the edge
            projectOnBestEdge  ( elemPoint,thePoint,pointProjection);
        }
      }
    }
    // Final assert!
    double projectionDistance = thePoint->dist(pointProjection);
    double epsAssert(0.1);//TODO
    if ( projectionDistance > epsAssert ) {
      std::cout<<"=========================The point is quite far from the projection."<<std::endl;
      idForLog=4;
      pointNotFound++;
    }
    printLog(log, idForLog, thePoint, pointProjection, projectionDistance);
    // 2) IF NEEDED, INITIALIZATION OF THE CURVILINEAR FINITE ELEMENT
    if ( eltType != previousEltType ) {
      if ( currentFe )
        delete currentFe;

      // Get standard information for the element
      int typeOfFiniteElement = var.finiteElementType();
      const GeoElement* geoEle = GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second;
      const RefElement* refEle = geoEle->defineFiniteEle(eltType,typeOfFiniteElement, *m_pb->mesh(iMesh));

      // Initialization
      currentFe = new CurvilinearFiniteElement(*refEle,*geoEle,var.degreeOfExactness());

      nDofEle= currentFe->numDof();
      values.resize(nDofEle);

      // Update of previous type of element
      previousEltType=eltType;
    }

    // 3) UPDATE OF THE BASIS FUNCTIONS IN CURVILINEAR FINITE ELEMENT
    currentFe->updateMeas(0,elemPoint);

    // 4) EVALUATION OF THE BASIS FUNCTION IN THE POINT
    currentFe->evaluateBasisFunctionsInPoint(&pointProjection,values);

    // 5) FILLING THE DATA STRUCTURE
    for ( std::size_t iComp(0); iComp < static_cast<std::size_t>(nComp); ++iComp ) {
      for ( std::size_t iDof(0); iDof < static_cast<std::size_t>(nDofEle); ++iDof ) {
        // get the global application numbering volume
        m_pb->dof().loc2glob(ielSupportDof,iDof,ivar,iComp,  idColumn);
        // Tranforming it into globalPetscOrdering volume
        AOApplicationToPetsc(m_pb->ao(), 1, & idColumn);
        // Moving to global application ordering bd
        idColumn = m_dofBD[iBD]->petscVol2ApplicationBD( idColumn );
        // Saving the values in the temporary interpolator matrix
        temporaryInterpolator[ rowNumbering[ cPoint*nComp + iComp ] ].push_back( std::make_pair( idColumn, values[iDof] ) );
      }
    }
  }
  this->checkInterpolatorStatistics(foundInsideAnElement, foundInsideAnElementAltLab, foundInEdge, foundInPoint, pointNotFound, nPoints );

  // 6) Build the pattern and the data in csr format
  std::vector<felInt> icsr(m_nrows[iBD]+1);
  std::vector<felInt> jcsr;
  std::vector<double> data;
  icsr[0]=0;
  for ( felInt iRow(0); iRow< m_nrows[iBD]; ++iRow ) {
    icsr[iRow+1]=icsr[iRow] + temporaryInterpolator[iRow].size();
    if ( temporaryInterpolator[iRow].size () > 5 )
      std::cout<<"iRow: "<<iRow<<" size "<<temporaryInterpolator[iRow].size()<<std::endl;
    for ( std::size_t j(0); j<temporaryInterpolator[iRow].size(); ++j) {
      jcsr.push_back(temporaryInterpolator[iRow][j].first);
      data.push_back(temporaryInterpolator[iRow][j].second);
    }
  }
  // 7) Allocating the matrix
  std::vector<felInt> nnz( m_nrows[iBD] );
  for ( std::size_t nRow = 0; nRow < (std::size_t) m_nrows[iBD]; nRow++ ) {
    nnz[nRow] = icsr[nRow+1]-icsr[nRow];
  }
  m_ncols[iBD] = m_dofBD[iBD]->numGlobalDofInterface();
  interpolator.createSeqAIJ(MPI_COMM_SELF, m_nrows[iBD], m_ncols[iBD], 0, nnz.data());
  interpolator.setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
  interpolator.setFromOptions();
  for ( felInt iRow(0); iRow< m_nrows[iBD]; ++iRow ) {
    interpolator.setValues(1,&iRow,nnz[iRow],&jcsr[icsr[iRow]],&data[icsr[iRow]],INSERT_VALUES);
  }
  interpolator.assembly(MAT_FINAL_ASSEMBLY);
  }//endif
}

void SurfaceInterpolator::writeInterfaceFile(felInt iunknown, felInt numComp, std::string filename, std::string folder, std::size_t iBD) const 
{
  if ( MpiInfo::rankProc() == 0 ) {
    //All the infos are in dofBD
    m_dofBD[iBD]->writeSurfaceInterpolatorFile(iunknown,numComp,filename,folder);
  }
}

void SurfaceInterpolator::readInterfaceFile( std::string filename, std::vector< std::pair< Point*, felInt > >& pointsAndCorrespondingSurfLabel, std::vector<int>& rowNumbering, std::string folder) 
{
  if ( MpiInfo::rankProc() == 0 ) {

  // 0) Make sure we got empty output variable
  pointsAndCorrespondingSurfLabel.clear();
  rowNumbering.clear();

  // 1) Opening the file
  std::stringstream fname;
  fname<<folder<<"/"<<filename;
  std::ifstream iFile;
  iFile.open(fname.str().c_str());
  if ( ! iFile.good() ) {
    iFile.close();
    std::cout<<fname.str()<<std::endl;
    FEL_ERROR("Problem in opening the file");
  }
  // 2) Get the number of points in the file
  felInt nPoints;
  iFile>>nPoints;
  if ( FelisceParam::verbose() > 2 ) {
    std::cout<<"Found "<<nPoints<<" points in "<<fname.str().c_str()<<std::endl;
  }

  // 3) Temporary variable declaration
  // Number of components
  felInt nComp;
  // The pointer to the Point
  Point* newPoint;
  // The surface label
  felInt label;
  // The if of the dof
  felInt id;

  // 4) Reading
  for ( int curPoint(0); curPoint<nPoints; ++curPoint) {
    // get the number of components
    iFile>>nComp;

    // get the point
    newPoint = new Point;
    iFile>>newPoint->x();
    iFile>>newPoint->y();
    iFile>>newPoint->z();

    // get the label
    iFile>>label;

    // verify label correctness, TODO improve this check
    // especially in case 1 label is mapped into more labels or viceversa.
    if ( FelisceParam::instance().withCVG ) {
      int otherLabel=label;
      FEL_ASSERT( FelisceParam::instance().interfaceLabels.size() == FelisceParam::instance().correspondingInterfaceLabels.size() );
      for ( std::size_t k(0); k<FelisceParam::instance().interfaceLabels.size(); ++k) {
        if ( FelisceParam::instance().correspondingInterfaceLabels[k] == otherLabel ) {
          label = FelisceParam::instance().interfaceLabels[k];
        }
      }
    }
    // save point and label in data structure
    pointsAndCorrespondingSurfLabel.emplace_back(newPoint,label);

    // get the id and save them
    for ( int iComp(0); iComp<nComp; ++iComp) {
      iFile>>id;
      rowNumbering.push_back(id);
    }
  }
  }
}

void SurfaceInterpolator::checkInterpolatorStatistics(int foundInsideAnElement, int foundInsideAnElementAltLab, int foundInEdge, int foundInPoint, int pointNotFound, int nPoints ) const 
{
  if ( FelisceParam::verbose() > 1 ) {
    std::stringstream msg;
    double percentageInsideElement = (double) foundInsideAnElement  / (double) nPoints;
    double percentageInsideElementAltLab = (double) foundInsideAnElementAltLab  / (double) nPoints;
    double percentageEdge = (double) foundInEdge  / (double) nPoints;
    double percentagePoint= (double) foundInPoint / (double) nPoints;

    int prec = 2;
    int oldPrec=std::cout.precision();
    if  ( percentageInsideElement*100 < 0.01 )
      prec = 4;
    else if (percentageInsideElement*100 > 1 )
      prec=oldPrec;
    std::cout<<"Points projected on an element:                                                     "<<foundInsideAnElement<<"/"
        <<nPoints<<", percentage="
        <<std::setprecision(prec)    <<percentageInsideElement*100
        <<std::setprecision(oldPrec) <<"%"<<std::endl;
    if  ( percentageInsideElement*100 < 0.01 )
      prec = 4;
    else if (percentageInsideElement*100 > 1 )
      prec=oldPrec;
    std::cout<<"Points projected on an element on a different label than the one specified:         "<<foundInsideAnElementAltLab<<"/"
        <<nPoints<<", percentage="
        <<std::setprecision(prec)    <<percentageInsideElementAltLab*100
        <<std::setprecision(oldPrec) <<"%"<<std::endl;

    if ( percentageEdge*100 < 0.01 )
      prec = 4;
    std::cout<<"Points projected on an edge:                                                        "<<foundInEdge<<"/"
        <<nPoints<<", percentage="
        <<std::setprecision(prec)    <<percentageEdge*100
        <<std::setprecision(oldPrec) <<"%"<<std::endl;
    if ( percentagePoint*100 < 0.01 )
      prec = 4;
    std::cout<<"Points projected on the closest point:                                              "<<foundInPoint<<"/"
        <<nPoints<<", percentage="
        <<std::setprecision(prec)    <<percentagePoint*100
        <<std::setprecision(oldPrec) <<"%"<<std::endl;
  }
  if ( pointNotFound > 0 ) {
    std::stringstream msg;
    double percentage = (double) pointNotFound/ (double) nPoints;
    int prec = 2;
    int oldPrec=std::cout.precision();
    if ( percentage*100 < 0.01 )
      prec = 4;
    msg<<"For "<<pointNotFound<<" point(s) the algorithm (total nb of points="
        <<nPoints<<", percentage="
        <<std::setprecision(prec)    <<percentage*100
        <<std::setprecision(oldPrec) <<"%), with threshold "
        <<FelisceParam::instance().interpolatorThreshold<<", was not able to find an element containing the point projection."<<std::endl;
    FEL_ERROR(msg.str().c_str());
  }
}

void SurfaceInterpolator::initializeSurfaceInterpolator()
{
  std::vector<Point*> elemPoint;
  std::vector<felInt> elemIdPoint;
  // Counter, necessary to compute the id of element by type;
  felInt numElementPerEltType;

  // For each type of element present on the boundary: i.e. triangles, quadrangles
  for (std::size_t cElemType = 0; cElemType < m_mesh->bagElementTypeDomainBoundary().size(); ++cElemType) {
    ElementType eltType = m_mesh->bagElementTypeDomainBoundary()[cElemType];
    elemPoint.resize(GeometricMeshRegion::m_numPointsPerElt[eltType]);
    elemIdPoint.resize(GeometricMeshRegion::m_numPointsPerElt[eltType]);
    numElementPerEltType=0;
    // We loop over the associated labels
    for (auto itLabelMesh = m_mesh->intRefToBegEndMaps[eltType].begin(); itLabelMesh != m_mesh->intRefToBegEndMaps[eltType].end(); itLabelMesh++) {
      // We get the label
      felInt cLabel = itLabelMesh->first;
      // We get the number of elements present on this label
      felInt numElemsPerLabel = itLabelMesh->second.second;
      // For each of this element...
      for ( felInt iel = 0; iel < numElemsPerLabel; iel++) {
        // Even in the case of P2, I just need the verteces of the element.
        // The reason is that I need them to find the element in the future!
        // Than of course, the interpolation will depend on the type of finite-element
        // used.
        m_mesh->getOneElement(eltType, numElementPerEltType+iel, elemIdPoint, 0);
        for (int iPoint = 0; iPoint < GeometricMeshRegion::m_numPointsPerElt[eltType]; iPoint++) {
          elemPoint[iPoint]= &m_mesh->listPoints()[elemIdPoint[iPoint]];
          // For each point we save the index of the elements of the part of the star
          // on this label.
          m_surfLabelAndIdPoint2Star[cLabel][elemIdPoint[iPoint]].push_back(numElementPerEltType+iel); // this is an index local to this proc, pay attention when going parallel
          m_surfLab2SetOfPointsAndIds[cLabel].insert(std::make_pair(elemPoint[iPoint],elemIdPoint[iPoint]));
        }
      }
      numElementPerEltType+=numElemsPerLabel;
    }
  }
  m_surfaceInterpolatorInitialized = true;
}

int SurfaceInterpolator::getClosestSurfaceElement(
  Point* thePoint,
  felInt surfaceLabel,
  ElementType& eltTypeReturn,
  std::vector<Point*>& elemPointReturn,
  Point& projection,
  felInt& ielSupportDof,
  std::map<int,double>& iel2ScoreByEdge,
  double& distance,
  int& idElOfClosestPoint
  )
{
  if ( ! m_surfaceInterpolatorInitialized ) {
    FEL_ERROR("You have to call initializeSurfaceInterpolator");
  }
  if ( FelisceParam::verbose() > 2 ) {
    std::cout<<"Point to be projected: "; thePoint->print(10);
  }

  // We extract the std::set of points and the corresponding ids for this label
  auto setOfPointsAndIds = m_surfLab2SetOfPointsAndIds.at(surfaceLabel);

  // We start looking for the closest point
  Point* closest;
  double curDist;
  felInt curIdClosestPoint = closestPoint( setOfPointsAndIds, thePoint, closest, curDist );
  if ( curDist < distance ) {
    distance = curDist;
    std::vector<felInt> star = m_surfLabelAndIdPoint2Star.at(surfaceLabel).at(curIdClosestPoint);
    idElOfClosestPoint = star[0];
  }
  if ( FelisceParam::verbose() > 2 ) {
    std::cout<<"The closest point on label "<<surfaceLabel<<" is: "; closest->print(10);
  }
  int depth(0);
  double score;
  return lookIntoStar(curIdClosestPoint, surfaceLabel, thePoint, eltTypeReturn, elemPointReturn, projection, ielSupportDof, depth, score,iel2ScoreByEdge);
}

int SurfaceInterpolator::lookIntoStar(
  int idClosestPoint,
  int surfaceLabel,
  Point* thePoint,
  ElementType & eltTypeReturn,
  std::vector<Point*>& elemPointReturn,
  Point& projection, felInt& ielSupportDof,
  felInt& depth,
  double& minScore,
  std::map<int,double>& iel2distFromEdge
  )
{
  // We start looking for the closest element
  felInt idClosestElement=-1;
  // Points of the element
  std::vector<Point*> elemPoint;
  // ..and corresponding ids.
  std::vector<felInt> elemIdPoint;

  // We assume that the closest element is in the star of this point
  // We extract the star
  std::vector<felInt> star = m_surfLabelAndIdPoint2Star.at(surfaceLabel).at(idClosestPoint);
  // We loop over its element, until we find the closest one
  const std::size_t star_size = star.size();
  if ( FelisceParam::verbose() > 2 ) {
    std::cout<<"Size of the star: "<<star_size<<std::endl;
  }
  std::vector<double> alpha(star_size);
  std::vector<double> beta(star_size);
  for( std::size_t k(0); k<star_size && idClosestElement<0; ++k) {
    // ==================EXTRACTING ELEMENT INFORMATION==================== //
    // We get the type of element
    ElementType eltType=getETandAssert2D(m_mesh->intRefToEnum().at(surfaceLabel));
    // ..and we resize this two vectors accoringly
    elemPoint.resize(GeometricMeshRegion::m_numPointsPerElt[eltType]);
    elemIdPoint.resize(GeometricMeshRegion::m_numPointsPerElt[eltType]);
    // We extract the element and its points
    m_mesh->getOneElement(eltType, star[k], elemIdPoint, 0);
    // We assume that we have only triangles or quadrangles
    FEL_ASSERT(elemIdPoint.size()>=3 && elemIdPoint.size()<=4);

    // We get the pointers of the points trough their ids
    for (int iPoint = 0; iPoint < GeometricMeshRegion::m_numPointsPerElt[eltType]; iPoint++) {
      elemPoint[iPoint] = &m_mesh->listPoints()[elemIdPoint[iPoint]];
    }

    // ==================PLANE PROJETION=================================== //
    projectOnElementPlane(elemPoint,thePoint,projection);
    if ( FelisceParam::verbose() > 3 ) {
      std::cout<<" its idByType: "<<star[k]<<std::endl;
      std::cout<<" its position in the star: "<<k<<std::endl;
      std::cout<<" its points: "<<std::endl;
      for ( int iPoint = 0; iPoint < GeometricMeshRegion::m_numPointsPerElt[eltType]; iPoint++) {
        elemPoint[iPoint]->print(10);
      }
      std::cout<<"TheProjection ";
      projection.print(10);
    }

    // ==================CHECK IF THE THE PROJECTION BELONGS TO THE ELEMENT AND COMPUTE BEST EDGE DISTANCE==================== //
    bool pointInElement=false;
    if ( elemPoint.size() == 3 ) {
      double edgeDist=1e20;
      pointInElement = checkTriangle(projection,elemPoint,alpha[k],beta[k],thePoint,edgeDist);
      // saving bestEdge information
      iel2distFromEdge[star[k]]=edgeDist;
    } else if ( elemPoint.size() == 4 ) {
      double edgeDist=1.e20;
      pointInElement = checkQuadrangle(projection,elemPoint,alpha[k],beta[k],thePoint,edgeDist);
      // saving bestEdge information
      iel2distFromEdge[star[k]]=edgeDist;
    }
    // We check if the projection is inside or outside the element.
    if ( pointInElement ) {
      // The correct element has been found
      idClosestElement = star[k];
      ielSupportDof=0;
      m_mesh->getIdElemFromTypeElemAndIdByType(eltType,idClosestElement,ielSupportDof);
      eltTypeReturn = eltType;
      elemPointReturn = elemPoint;
      if ( FelisceParam::verbose() > 2 ) {
        std::cout<<" its idByType: "<<idClosestElement<<std::endl;
        std::cout<<" its position in the star: "<<k<<std::endl;
        std::cout<<" its points: "<<std::endl;
        for ( int iPoint = 0; iPoint < GeometricMeshRegion::m_numPointsPerElt[eltType]; iPoint++) {
          elemPoint[iPoint]->print(10);
        }
        std::cout<<"TheProjection "; projection.print(10);
      }
    }
  }
  // If found it so simply we just skip all this part and return 0 (success);
  // If we have not found it
  if (idClosestElement<0) {
    if ( FelisceParam::verbose() > 3 ) {
      std::cout<<"Point: ";
      thePoint->print(10);
    }
    // We compute the score of each element in the star!
    minScore=1e20;
    for ( std::size_t k(0); k<star.size(); ++k ) {
      double score=computeScore(alpha[k],beta[k]);
      if ( score < minScore) {
        minScore = score;
        idClosestElement = star[k];
      }
    }
    if ( FelisceParam::verbose() > 3 )
      std::cout<<"SPECIAL CASE OF GET CLOSEST SURFACE ELEMENT: Score of current star: "<<minScore<<std::endl;

    // For now this element is our best!
    // We extract the return parameters
    ElementType eltType=getETandAssert2D(m_mesh->intRefToEnum().at(surfaceLabel));
    eltTypeReturn = eltType;
    ielSupportDof=0;
    m_mesh->getIdElemFromTypeElemAndIdByType(eltType,idClosestElement,ielSupportDof);
    m_mesh->getOneElement(eltType, idClosestElement, elemIdPoint, 0);

    elemPointReturn.resize( GeometricMeshRegion::m_numPointsPerElt[eltType] );
    for (int iPoint = 0; iPoint < GeometricMeshRegion::m_numPointsPerElt[eltType]; iPoint++) {
      elemPointReturn[iPoint] = &m_mesh->listPoints()[elemIdPoint[iPoint]];
    }

    // We also look into the adjacent stars.
    if ( depth >= FelisceParam::instance().maxDepth ) {
      if ( FelisceParam::verbose() > 3 )
        std::cout<<"Max depth reached, iterations concluded"<<std::endl;
    } else {
      depth++;
      int result;
      double score=100;
      ElementType eltTypeReturnTmp;
      std::vector<Point*> elemPointReturnTmp;
      Point projectionTmp(0.0);
      felInt ielSupportDofTmp;
      // I look in the other stars close to this element.
      for ( std::size_t k(0); k<elemIdPoint.size(); ++k) {
        // I skip the current star.
        if ( elemIdPoint[k] != idClosestPoint ) {
          if ( FelisceParam::verbose() > 3 )
            std::cout<<"Descending into the star of point number "<<k<<std::endl;
          result = this->lookIntoStar(/*where to look*/elemIdPoint[k], surfaceLabel, thePoint, eltTypeReturnTmp, elemPointReturnTmp, projectionTmp, ielSupportDofTmp, depth , score, iel2distFromEdge );
          // We found it in this star!
          if ( result == 0 ) {
            if ( FelisceParam::verbose() > 3 )
              std::cout<<"found it in the current star!"<<std::endl;
            // We do not go any further! we update the parameters and return success.
            eltTypeReturn = eltTypeReturnTmp;
            elemPointReturn = elemPointReturnTmp;
            projection = projectionTmp;
            ielSupportDof = ielSupportDofTmp;
            return 0;//success
          } else {
            // We have not found it, but maybe we found something slightly better

            // is this better than what we have?
            // if not we ignore it
            if ( score < minScore ) {
              if ( FelisceParam::verbose() > 3 )
                std::cout<<"Not found, but score improved!"<<std::endl;
              minScore = score;
              //update current best
              eltTypeReturn = eltTypeReturnTmp;
              elemPointReturn = elemPointReturnTmp;
              projection = projectionTmp;
              ielSupportDof = ielSupportDofTmp;
            }
          }
        }
      }
    }
    return 1; // not found
  } else {
    //standard result
    return 0; // found
  }
}

void SurfaceInterpolator::displayDataForSurfaceInterpolator() 
{
  int rank;
  int numproc;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &numproc);

  for ( int cProc(0); cProc<numproc; ++cProc ) {
    if( rank == cProc ) {
      std::cout << "---------- Proc numero: " << rank << std::endl;
      for (auto i = m_surfLab2SetOfPointsAndIds.begin(); i != m_surfLab2SetOfPointsAndIds.end(); ++i) {
        std::cout << "----- " << "Label: " << i->first << std::endl;
        for (auto j = i->second.begin(); j != i->second.end(); ++j) {
          std::cout << "P: "; j->first->print(10);
          std::cout << "--> Number of Elements in the star: " << m_surfLabelAndIdPoint2Star[i->first][j->second].size() << std::endl;
          std::cout << "----> Elements ids: ";
          for(std::size_t k = 0; k < m_surfLabelAndIdPoint2Star[i->first][j->second].size(); ++k)
            std::cout << m_surfLabelAndIdPoint2Star[i->first][j->second][k] << " ";
          std::cout << std::endl;
        }
      }
    }
    MPI_Barrier(MpiInfo::petscComm());
  }
}

GeometricMeshRegion::ElementType SurfaceInterpolator::getETandAssert2D(std::set<ElementType> setOfET){
  int nTypeElPerLab(0);
  ElementType eltType = static_cast<ElementType>(0);
  for ( std::set<ElementType>::iterator et=setOfET.begin();
        et!=setOfET.end();
        ++et ) {
    int first3DElement = 12; //TODO!
    int first2DElement = 4; //TODO!
    ElementType eltType2 = *et;
    if( (int) eltType2 < first3DElement && (int)eltType2 >= first2DElement ) {
      nTypeElPerLab++;
      eltType=*et;
    }
  }
  // We assume that each label can have only one type of element.
  FEL_ASSERT(nTypeElPerLab==1);
  FEL_ASSERT(eltType!=0);
  return eltType;
}






bool SurfaceInterpolator::isPointInTriangle(const double* A,const double* B,const double* C,const double* D, double& alpha, double& beta) 
{

  double V1[3],V2[3],H[3];

  V1[0]=B[0]-A[0];
  V1[1]=B[1]-A[1];
  V1[2]=B[2]-A[2];

  V2[0]=C[0]-A[0];
  V2[1]=C[1]-A[1];
  V2[2]=C[2]-A[2];

  H[0]=D[0]-A[0];
  H[1]=D[1]-A[1];
  H[2]=D[2]-A[2];

  double det,det1,det2;
  det = V1[0]*V2[1] -  V2[0]*V1[1];
  det1 = V1[0]*V2[2] -  V2[0]*V1[2];
  det2 = V1[1]*V2[2] -  V2[1]*V1[2];
  double fdet,fdet1,fdet2;
  fdet=std::fabs(det);
  fdet1=std::fabs(det1);
  fdet2=std::fabs(det2);

  alpha = -1;
  beta = -1;
  if ( fdet> fdet1 && fdet > fdet2 )  {
    alpha=(V2[1]*H[0] - V2[0]*H[1])/det;
    beta =(V1[0]*H[1] - V1[1]*H[0])/det;
  } else if  ( fdet1> fdet && fdet1 > fdet2 ){
    alpha=(V2[2]*H[0] - V2[0]*H[2])/det1;
    beta =(V1[0]*H[2] - V1[2]*H[0])/det1;
  } else if  ( fdet2> fdet && fdet2 > fdet1 ){
    alpha=(V2[2]*H[1] - V2[1]*H[2])/det2;
    beta =(V1[1]*H[2] - V1[2]*H[1])/det2;
  }
  double eps = FelisceParam::instance().interpolatorThreshold;
  return alpha>=-eps && beta >= -eps && alpha+beta<=1+eps;
}

void SurfaceInterpolator::checkEdge(const double *p0,const double *p1,Point* thePoint,double& bestDistance) 
{
  double distance,t;
  bool inside = isPointInEdgeAndIfYesHowFar( p0, p1, thePoint->coor(), distance, t);
  if ( inside && distance < bestDistance ) {
    bestDistance = distance;
  }
}

bool SurfaceInterpolator::checkTriangle( const Point& projection, const std::vector<Point*>& elemPoint, double& alpha, double& beta, Point* thePoint, double& bestEdgeDistance ) 
{
  // Check if it is in the triangle
  bool pointInElement(false);
  pointInElement = isPointInTriangle(elemPoint[0]->coor(),elemPoint[1]->coor(),elemPoint[2]->coor(),projection.coor(),alpha,beta);
  if ( pointInElement ){
    return pointInElement;   //no need to check the edges;
  }
  checkEdge(elemPoint[0]->coor(),elemPoint[1]->coor(),thePoint,bestEdgeDistance);
  checkEdge(elemPoint[1]->coor(),elemPoint[2]->coor(),thePoint,bestEdgeDistance);
  checkEdge(elemPoint[2]->coor(),elemPoint[0]->coor(),thePoint,bestEdgeDistance);
  return pointInElement;
}

bool SurfaceInterpolator::checkQuadrangle( const Point& projection, const std::vector<Point*>& elemPoint, double& alpha, double& beta, Point* thePoint, double& bestEdgeDistance ) 
{
  bool pointInElement(false);
  double alphaTmp,betaTmp, bestScore;
  bestScore=1e20;
  pointInElement = isPointInTriangle(elemPoint[0]->coor(),elemPoint[1]->coor(),elemPoint[2]->coor(),projection.coor(),alphaTmp,betaTmp);
  updateBestScore(alphaTmp,betaTmp,bestScore,alpha,beta);
  pointInElement = isPointInTriangle(elemPoint[1]->coor(),elemPoint[2]->coor(),elemPoint[3]->coor(),projection.coor(),alphaTmp,betaTmp) || pointInElement;
  updateBestScore(alphaTmp,betaTmp,bestScore,alpha,beta);
  pointInElement = isPointInTriangle(elemPoint[2]->coor(),elemPoint[3]->coor(),elemPoint[0]->coor(),projection.coor(),alphaTmp,betaTmp) || pointInElement;
  updateBestScore(alphaTmp,betaTmp,bestScore,alpha,beta);
  pointInElement = isPointInTriangle(elemPoint[3]->coor(),elemPoint[0]->coor(),elemPoint[1]->coor(),projection.coor(),alphaTmp,betaTmp) || pointInElement;
  updateBestScore(alphaTmp,betaTmp,bestScore,alpha,beta);
  if ( pointInElement ){
    return pointInElement;   //no need to check the edges;
  }
  checkEdge(elemPoint[0]->coor(),elemPoint[1]->coor(),thePoint,bestEdgeDistance);
  checkEdge(elemPoint[1]->coor(),elemPoint[2]->coor(),thePoint,bestEdgeDistance);
  checkEdge(elemPoint[2]->coor(),elemPoint[3]->coor(),thePoint,bestEdgeDistance);
  checkEdge(elemPoint[3]->coor(),elemPoint[0]->coor(),thePoint,bestEdgeDistance);
  return pointInElement;
}

bool SurfaceInterpolator::isPointInEdgeAndIfYesHowFar( const double *p0,const double *p1,const double* thePoint,double& distance, double&t) 
{
  projectOnEdge(p0,p1,thePoint,t);
  double eps = FelisceParam::instance().interpolatorThreshold;
  if ( t < 1 + eps && t > -eps ) {
    double dist(0);
    for ( int i(0); i<3; ++i ) {
      dist += ((1-t)*p0[i] +t*p1[i]-thePoint[i])*((1-t)*p0[i] +t*p1[i]-thePoint[i]);
    }
    distance = std::sqrt(dist);
    return true;
  } else {
    distance = 1.e10;
    return false;
  }
}

// elemPoint  [in]  points defining the plane, only the first three are used
// thePoint   [in]  the point to be projected
// projection [out] the projection of this point on the plane
void SurfaceInterpolator::projectOnElementPlane( const std::vector<Point*>& elemPoint, Point* thePoint, Point& projection ) 
{
  Point normal; // Normal to the element plane
  MathUtilities::CrossProduct(normal.getCoor(), (*elemPoint[0] - *elemPoint[1]).getCoor(), (*elemPoint[0] - *elemPoint[2]).getCoor());

  double nnorm = normal.norm();
  for( int coor(0); coor<3; coor++) {
    normal.coor(coor)/=nnorm;
  }
  // We compute the intercept of the plane containing the element
  double intercept = (*elemPoint[0]) * normal;

  // P_proj = P + s*normal
  // We compute s, by imposing that P_proj is on the plane.
  double s = intercept - (*thePoint) * normal;
  projection = s * normal + (*thePoint);
}

void SurfaceInterpolator::projectOnEdge(const double *p0,const double *p1,const double* thePoint, double& t) 
{
  double denom(0);
  double num(0);
  for ( int i(0); i<3; ++i ) {
    num   +=  - p0[i]*p1[i]-thePoint[i]*p0[i]+thePoint[i]*p1[i] + p0[i]*p0[i];
    denom += -2*p0[i]*p1[i]           +p1[i]*p1[i]              + p0[i]*p0[i];
  }
  t = num/denom;
}

void SurfaceInterpolator::projectOnBestEdge(const std::vector<Point*>& elemPoint, Point* thePoint, Point& projection) 
{
  double bestDist(1e20),bestT(0.0),bestEdgeA(0.0),bestEdgeB(0.0);
  double d,t;
  for (std::size_t iPoint(0); iPoint < elemPoint.size(); iPoint++) {
    // We assume that the point describing the element are in an order such that
    // the edges are described by consecutive points
    isPointInEdgeAndIfYesHowFar(elemPoint[iPoint]->coor(),elemPoint[( iPoint + 1 ) % elemPoint.size() ]->coor(),thePoint->coor(), d,t);
    if ( d < bestDist ) {
      bestDist = d;
      bestT = t;
      bestEdgeA = iPoint;
      bestEdgeB = (iPoint+1)%elemPoint.size();
    }
  }
  for( int coor(0); coor<3; coor++) {
    projection.coor()[coor] = (1-bestT)*elemPoint[bestEdgeA]->coor()[coor]+bestT*elemPoint[bestEdgeB]->coor()[coor];
  }
}

void SurfaceInterpolator::projectOnBestPoint(const std::vector<Point*>& elemPoint, Point* thePoint, Point& projection) 
{
  double best=1e20;
  for (std::size_t iPoint = 0; iPoint < elemPoint.size(); iPoint++) {
    double dist = elemPoint[iPoint]->dist( *thePoint);
    if ( dist<best ) {
      best=dist;
      projection=*elemPoint[iPoint];
    }
  }
}

double SurfaceInterpolator::computeScore(double alpha, double beta)
{
  double score(0);
  if ( alpha < 0 )       score +=  -alpha;
  if ( alpha > 1 )       score +=  alpha-1;
  if ( beta  < 0 )       score +=  -beta;
  if ( beta  > 1 )       score +=  beta-1;
  if ( alpha+beta  < 0 ) score +=  -(alpha+beta);
  if ( alpha+beta  > 1 ) score +=  alpha+beta-1;
  return score;
}

void SurfaceInterpolator::updateBestScore( double alpha, double beta, double& bestScore, double& alphabest, double& betabest)
{
  double score = computeScore(alpha,beta);
  if( score < bestScore ) {
    bestScore = score;
    alphabest = alpha;
    betabest  = beta;
  }
}

int SurfaceInterpolator::closestPoint(const std::set< std::pair< Point*, felInt > >& setOfPointsAndIds, const Point* thePoint, Point*& closest, double& distance) 
{
  // We start looking for the closest point
  felInt idClosestPoint=-1;
  double dist(0), distMin=1.e30;
  // We compute the distance between each point and our point
  for(auto  cPoint=setOfPointsAndIds.begin();
       cPoint!=setOfPointsAndIds.end(); ++cPoint ) {
    // Distance
    dist = 0;
    for( int coor(0); coor<3; coor++) {
      dist += std::pow((thePoint->coor(coor)-cPoint->first->coor(coor)),2);
    }
    // Check if it is closer than the previous best
    if ( dist < distMin ) {
      distMin=dist;
      idClosestPoint=cPoint->second;
      closest = cPoint->first;
    }
  }
  distance = std::sqrt(distMin);
  return idClosestPoint;
}

}

