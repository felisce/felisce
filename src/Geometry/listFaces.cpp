//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

// System includes

// External includes

// Project includes
#include "Geometry/listFaces.hpp"
#include "Geometry/geometricFaces.hpp"
#include "Geometry/neighVolumesOfFaces.hpp"

namespace felisce 
{
  
const felInt ListFaces::m_null_int = -1;

//////////////////////
// LIST FACES CLASS //
//////////////////////

//----------------
ListFaces::~ListFaces() {
  for (std::size_t iface = 0; iface < m_list.size(); ++iface) {
    delete m_list[iface];
  }

  m_list.clear();
  m_listBegin.clear();
}


void ListFaces::clearAndInit(felInt numPts) {
  for (std::size_t iface = 0; iface < m_list.size(); ++iface)
    delete m_list[iface];
        
  m_list.clear();
  m_listBegin.clear();

  m_numFaces = 0;
  m_numGivenFaces = 0;

  m_listBegin.resize(numPts, nullptr);
}

//----------------
void ListFaces::searchAddFace(std::vector<felInt> facePts, NeighVolumesOfFaces* the_vol_neighbour,
                              bool check_surface_mesh_flag) {
  IGNORE_UNUSED_ARGUMENT(check_surface_mesh_flag);
  //-------
  //!STEP 1: prepare the face:

  //! reorder the face: convention: smallest point first
  std::vector<felInt> orderedFacePts(facePts);
  permutFacePointsSmallIDFirst(facePts, orderedFacePts);

  //! create opposite face, and reorder it following the convention
  std::vector<felInt> orderedOppFacePts(facePts.size(), 0);
  giveOppositeFace(facePts, orderedOppFacePts);
  permutFacePointsSmallIDFirst(orderedOppFacePts);

  //-------
  //!STEP 2: search the face in the existing list of faces
  Face* begin_face = m_listBegin[ orderedFacePts[0] ];

  if ( begin_face == nullptr ) {
    //! this vertex has never been the beginning of a face: add the face
    Face* the_face = new Face(orderedFacePts, m_numFaces);
    if (the_vol_neighbour != nullptr)
      the_face->listNeighVolumes().push_back(the_vol_neighbour);

    m_list.push_back( the_face );
    m_listBegin[ orderedFacePts[0] ] = the_face;
    m_numFaces++;
    return;
  }

  //! otherwise: there already exists (at least) one face starting with this vertex:
  Face* current_face = begin_face;
  Face* previous_face;
  do { //! search FacePts or OppFacePts among all faces starting with this vertex
    previous_face = current_face;
    if ( compareFaces( current_face->vecFace(), orderedFacePts ) or
          compareFaces( current_face->vecFace(), orderedOppFacePts ) ) {
      //! this face exists already
      if (the_vol_neighbour != nullptr)
        current_face->listNeighVolumes().push_back(the_vol_neighbour);
      return; //! we do not insert this existing face.
    }
    current_face = previous_face->ptrNext();
  } while ( current_face != nullptr );

  //! If we did not exit so far, this is a new face: we add it
  Face* the_face = new Face(orderedFacePts, m_numFaces);
  if (the_vol_neighbour != nullptr)
    the_face->listNeighVolumes().push_back(the_vol_neighbour);

  m_list.push_back( the_face );
  //! link between previous face and current:
  previous_face->ptrNext() = m_list[m_numFaces];
  m_numFaces ++;
}

//----------------
felInt ListFaces::findFace(const std::vector <felInt> & facePts) const {
  const felInt resultFaceNotFound = -1;

  //! reorder the face: convention: smallest point first
  std::vector<felInt> orderedFacePts(facePts);
  permutFacePointsSmallIDFirst(facePts, orderedFacePts);

  //! create opposite face, and reorder it following the convention
  std::vector<felInt> orderedOppFacePts(facePts.size(), 0);
  giveOppositeFace(facePts, orderedOppFacePts);
  permutFacePointsSmallIDFirst(orderedOppFacePts);

  Face* current_face = m_listBegin[ orderedFacePts[0] ];
  if ( current_face == nullptr ) {
    FEL_WARNING("You are searching a face with an unknow first point!!!!\n");
    return resultFaceNotFound;
  }

  do {
    if ( compareFaces( current_face->vecFace(), orderedFacePts ) or
          compareFaces( current_face->vecFace(), orderedOppFacePts ) ) {
      return current_face->id();
    }
    current_face = current_face->ptrNext();
  } while ( current_face != nullptr );

  FEL_WARNING("The sought face was not found.");
  return resultFaceNotFound;
}

//----------------
//! if not used: remove it! 2012/08 vm
felInt ListFaces::findFace(const Face& the_face) const {
  return findFace( the_face.vecFace() );
}

//----------------
//! Like findFace, but instead of returning the face ID, return a copy of the face. (Really useful:??? VM 2012/09)
void ListFaces::searchFace(const std::vector <felInt> & facePts, Face& resultFace) const {
  felInt faceID = findFace(facePts);
  resultFace.copyFace( *( list(faceID) ) );
}


//----------------
//! return true if the faces are equal
//! we assume that the starting point are equal
bool ListFaces::compareFaces(const std::vector<felInt>& face0, const std::vector<felInt>& face1) const {
  FEL_ASSERT( face0[0] == face1[0] );
  //! we assume that a quadrangle and a triangle cannot be equal:
  if ( face0.size() != face1.size() ) return false;

  switch (face0.size()) {
    case 3:
      if (face0[1] != face1[1]) return false;
      if (face0[2] != face1[2]) return false;
      return true;
    case 4:
      if (face0[1] != face1[1]) return false;
      if (face0[2] != face1[2]) return false;
      if (face0[3] != face1[3]) return false;
      return true;
    case 6:
      if (face0[1] != face1[1]) return false;
      if (face0[2] != face1[2]) return false;
      if (face0[3] != face1[3]) return false;
      if (face0[4] != face1[4]) return false;
      if (face0[5] != face1[5]) return false;
      return true;
    default:
      FEL_ERROR("Number of points in the face incorrect!!!");
      return false; //to avoid a warning
  }
}


//----------------
//! Permute id point of face description:
//! convention smallest point id first.
void ListFaces::permutFacePointsSmallIDFirst(const std::vector <felInt>& theFacePts,
    std::vector <felInt>& theOrderedFactePts) const {
  felInt minPt = theOrderedFactePts[0];
  int posMin = 0;
  int dim = static_cast<int>( theOrderedFactePts.size() );
  for (unsigned int ipt = 1; ipt < theOrderedFactePts.size(); ipt++) {
    if ( theOrderedFactePts[ipt] < minPt ) {
      minPt = theOrderedFactePts[ipt];
      posMin = ipt;
    }
  }

  for (unsigned int ipt = 0; ipt < theOrderedFactePts.size(); ipt++) {
    int newPos = ( (ipt - posMin)+dim*100 ) % dim; //newPos >=0 because a%b is >=0 if a and b are >=0
    theOrderedFactePts[newPos] = theFacePts[ipt];
  }
}
void ListFaces::permutFacePointsSmallIDFirst(std::vector <felInt>& facePts) const {
  std::vector<felInt> tmpFacePts(facePts);
  permutFacePointsSmallIDFirst(tmpFacePts, facePts);
}

//----------------
void ListFaces::giveOppositeFace(const std::vector <felInt>& theFacePts,
                                  std::vector <felInt>& theOppositeFacePts) const {
  FEL_ASSERT( theFacePts.size() == theOppositeFacePts.size() );
  switch ( theFacePts.size() ) {
    case 3:
      theOppositeFacePts[0] = theFacePts[1];
      theOppositeFacePts[1] = theFacePts[0];
      theOppositeFacePts[2] = theFacePts[2];
      break;

    case 4:
      theOppositeFacePts[0] = theFacePts[1];
      theOppositeFacePts[1] = theFacePts[0];
      theOppositeFacePts[2] = theFacePts[3];
      theOppositeFacePts[3] = theFacePts[2];
      break;

    case 6:
      theOppositeFacePts[0] = theFacePts[1];
      theOppositeFacePts[1] = theFacePts[0];
      theOppositeFacePts[2] = theFacePts[3];
      theOppositeFacePts[3] = theFacePts[2];
      theOppositeFacePts[4] = theFacePts[5];
      theOppositeFacePts[5] = theFacePts[4];
      break;
    default:
      FEL_ERROR("Number of points in the face incorrect!!!");
  }
}

//----------------
void ListFaces::print( std::ostream& outstr, int verbose, int orderFormat) const {
  switch ( orderFormat ) {
  case 0: { // print by increasing edge ID
    outstr << "Print the list of faces: " << m_numFaces
            << " faces (ordered by face ID) (" << m_numGivenFaces
            << " first faces were provided.)\n";
    bool print_next_face = false;
    for(auto it_edg =  m_list.begin();
          it_edg != m_list.end(); it_edg++) {
      (*it_edg)->print( outstr, verbose, print_next_face );
    }
  }
  break;
  case 1: { // print by increasing BeginPoint
    felInt begPt = 0;
    outstr << "Print the list of faces: " << m_numFaces
            << " faces (ordered by increasing BeginPoint (" << m_numGivenFaces
            << " faces were provided.))\n";
    bool print_next_face = true;
    for(auto it_beg =  m_listBegin.begin();
          it_beg != m_listBegin.end(); it_beg++) {
      if ( (*it_beg) == NULL  ) {
        if ( verbose > 30 ) outstr << begPt << " ------> NULL "<< std::endl;
      } else {
        (*it_beg)->print( outstr, verbose, print_next_face  );
      }
      begPt++;
    }
  }
  break;
  default:
    FEL_ERROR("Problem: choose the order for face printing");
  }
}
}
