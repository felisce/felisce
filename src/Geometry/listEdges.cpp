//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

// System includes

// External includes

// Project includes
#include "Geometry/geometricEdges.hpp"
#include "Geometry/listEdges.hpp"
#include "Geometry/neighFacesOfEdges.hpp"
#include "Geometry/neighVolumesOfEdges.hpp"

namespace felisce
{
const felInt ListEdges::m_null_int = -1;

//////////////////////
// LIST EDGES CLASS //
//////////////////////

//----------------
ListEdges::~ListEdges() {
  for (unsigned int iedge = 0; iedge < m_listEdges.size(); iedge++) {
    delete m_listEdges[iedge];
  }
  m_listEdges.clear();
  m_listBeginEdge.clear();
}

//----------------
void ListEdges::clearAndInit(felInt numPts) {
  m_listBeginEdge.clear();
  m_listEdges.clear();
  m_numEdges = 0;
  m_numGivenEdges = 0;

  m_listBeginEdge.resize( numPts, nullptr );
}

//----------------
void ListEdges::searchAddEdge(felInt pt_edge_beg, felInt pt_edge_end,
                              shar_ptrFace   the_face_neighbour,
                              shar_ptrVol the_vol_neighbour) {
  Edge* begin_edge;
  begin_edge = m_listBeginEdge[ pt_edge_beg ];
  if ( begin_edge == nullptr ) {
    //! this vertex has never been the beginning of an edge: add the edge
    Edge* the_edge = new Edge(pt_edge_beg, pt_edge_end, m_numEdges);
    m_insertNeighbors(the_edge, the_face_neighbour, the_vol_neighbour);

    m_listEdges.push_back( the_edge );
    m_listBeginEdge[ pt_edge_beg ] = the_edge;
    m_numEdges ++;
    return;
  }

  //! otherwise: there already exists (at least) one edge starting with this vertex:
  Edge*   current_edge;
  Edge*   previous_edge;
  current_edge = begin_edge;
  //! search all edges starting from pt_edge_beg
  do {
    previous_edge = current_edge;
    if ( previous_edge->idEnd() != pt_edge_end ) { //! edge not found
      current_edge = previous_edge->ptrNext(); //! go to next edge
    } else { //! this edge exists already
      m_insertNeighbors(current_edge, the_face_neighbour, the_vol_neighbour);
      return; //! we do not insert this existing edge.
    }

  } while ( current_edge != nullptr );

  //! If we did not exit so far, this is a new edge: we add it
  Edge* the_edge = new Edge(pt_edge_beg, pt_edge_end, m_numEdges);
  m_insertNeighbors(the_edge, the_face_neighbour, the_vol_neighbour);

  m_listEdges.push_back( the_edge );
  //! link between previous edge and current:
  previous_edge->ptrNext() = m_listEdges[m_numEdges];
  m_numEdges ++;
}

//----------------
void ListEdges::m_insertNeighbors(Edge* the_edge,
                                  shar_ptrFace   the_face_neighbour,
                                  shar_ptrVol the_vol_neighbour) const {
  if (the_face_neighbour)// != NULL)
    the_edge->listNeighFacesOfEdges().push_back(the_face_neighbour);
  if (the_vol_neighbour)// != NULL)
    the_edge->listNeighVolumesOfEdges().push_back(the_vol_neighbour);
}

void ListEdges::searchEdge(const std::vector<felInt>& edgePts, Edge& resultEdge) const {
  felInt iD;
  FEL_ASSERT(edgePts.size()==2)
    if ( edgePts[0] < edgePts[1] )
      iD=findEdge(edgePts[0],edgePts[1]);
    else
      iD=findEdge(edgePts[1],edgePts[0]);
  resultEdge = *m_listEdges[iD]; // now it is safe with shared_ptr
}

//----------------
felInt ListEdges::findEdge(felInt pt_edge_beg, felInt pt_edge_end) const {

  FEL_ASSERT(static_cast<int>(m_listBeginEdge.size()) > pt_edge_beg)

  const felInt resultEdgeNotFound = -1;

  if ( pt_edge_beg >= pt_edge_end ) {
    FEL_ERROR("The starting point should be < the ending point in findEdge!\n");
  }

  Edge* current_edge = m_listBeginEdge[ pt_edge_beg ];
  if ( current_edge == nullptr ) {
    FEL_WARNING("You are searching a edge with an unknow first point!!!!\n");
    return resultEdgeNotFound;
  }

  do {
    if ( current_edge->idEnd() == pt_edge_end ) {
      return current_edge->id();
    } else {
      current_edge = current_edge->ptrNext();
    }
  } while ( current_edge != nullptr );

  FEL_WARNING("The sought edge was not found.");
  return resultEdgeNotFound;
}

//----------------
void ListEdges::print( std::ostream& outstr, int verbose, int orderFormat ) const {
  switch ( orderFormat ) {
  case 0: { // print by increasing edge ID
    outstr << "Print the list of edges: " << m_numEdges
            << " edges (ordered by edge ID) (" << m_numGivenEdges
            << " first edges were provided.)\n";
    bool print_next_edge = false;
    for(auto it_edg =  m_listEdges.begin();
          it_edg != m_listEdges.end(); it_edg++) {
      (*it_edg)->print( outstr, verbose, print_next_edge );
    }
  }
  break;
  case 1: { // print by increasing BeginPoint
    felInt begPt = 0;
    outstr << "Print the list of edges: " << m_numEdges
            << " edges (ordered by increasing BeginPoint) (" << m_numGivenEdges
            << " edges were provided.)\n";
    bool print_next_edge = true;
    for(auto it_beg =  m_listBeginEdge.begin();
          it_beg != m_listBeginEdge.end(); it_beg++) {
      if ( (*it_beg) == NULL ) {
        if ( verbose > 30 ) outstr << begPt << " ------> NULL "<< std::endl;
      } else {
        (*it_beg)->print( outstr, verbose, print_next_edge );
      }
      begPt++;
    }
  }
  break;
  default:
    FEL_ERROR("Problem: choose the order for edge printing");
  }
}

double ListEdges::countAndComputeIdEdgesSupportingDofs(const RefElement& refEle) {
  if ( m_numOfEdgesSupportingADof > 0) {
    // It has been computed before, it is returned.
    return m_numOfEdgesSupportingADof;
  } else {
    // In this function we want to count the edges that are actually supporting a dof!
    // While we count them we assign them an id. This id starts at 0 (idSupporting);
    double idSupporting(0);
    // Number of dof supported by the current edge.
    felInt numExtraDof(0);
    // Loop over all the edges of the list
    for (std::size_t i(0); i < m_listEdges.size(); ++i) {
      // We get the number of dof supported by the current edge.
      // Of course for doing this we need to know which kind of reference element we are using.
      numExtraDof = m_listEdges[i]->numOfDofSupported(refEle);
      // If the edge is indeed supporting sum dofs
      // we count it and we assign an id to him
      if ( numExtraDof > 0 ) {
        m_numOfEdgesSupportingADof += numExtraDof;
        m_listEdges[i]->idOnlySupporting() = idSupporting;
        idSupporting++;
      }
    }
    return m_numOfEdgesSupportingADof;
  }
}
}
