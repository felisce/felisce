//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau J.Foulon V.Martin
//

#ifndef GEOMETRICFACES_HPP
#define GEOMETRICFACES_HPP

// System includes
#include <vector>
#include <list>
#include <iostream>

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce {
  /*!
  \class Face
  \authors J.Castelneau & J.Foulon

  \brief Class implementing a geometric face

  A face is defined by
  - its vertices
  - a list of volumes neighbours
  - a pointer on next face with same 1st vertex

  */
  class NeighVolumesOfFaces;

  class Face {
  private:
    Face                                 *m_ptrNext;
    std::vector <felInt>                 m_vecFace;
    felInt                               m_id;
    std::vector < NeighVolumesOfFaces* > m_listNeighVolumes;

  public:

    // Constructor
    Face(): m_ptrNext(nullptr), m_id(0) {} // todo: remove it vm 2012/08
    Face(const std::vector <felInt>& ptOfFace, const felInt & idFace);
    ~Face();
    void copyFace(Face& face);

    // getters
    //inline const Face* & ptrNext() const { return m_ptrNext; }
    inline const std::vector <felInt> & vecFace() const {
      return m_vecFace;
    }
    inline const felInt & id() const {
      return m_id;
    }
    inline const std::vector < NeighVolumesOfFaces* > & listNeighVolumes() const {
      return m_listNeighVolumes;
    }

    // setters
    inline Face*& ptrNext() {
      return m_ptrNext;  // todo: remove it vm 2012/08
    }
    inline std::vector <felInt> & vecFace() {
      return m_vecFace; // todo: remove it?? vm 2012/08
    }
    inline felInt & id() {
      return m_id; // todo: remove it vm 2012/08
    }
    inline std::vector < NeighVolumesOfFaces* > & listNeighVolumes() {
      return m_listNeighVolumes;
    }

    // print a face
    void print(  std::ostream& outstr = std::cout, int verbose = 0, bool printNextFace = false ) const;

  };
}

#endif
