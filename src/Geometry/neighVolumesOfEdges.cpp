//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & V. Martin
//

// System includes

// External includes

// Project includes
#include "Geometry/neighVolumesOfEdges.hpp"

namespace felisce 
{
void NeighVolumesOfEdges::print( std::ostream& outstr, int verbose ) const {
  IGNORE_UNUSED_VERBOSE;
  outstr << "V->" << GeometricMeshRegion::eltEnumToFelNameGeoEle[m_typeVolume].first
          << "->" << m_idVolume << " id loc " << m_idLocalEdge << " || ";
}
}
