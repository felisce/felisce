//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & V. Martin
//

// System includes

// External includes

// Project includes
#include "Geometry/neighFacesOfEdges.hpp"

namespace felisce 
{
void NeighFacesOfEdges::print( std::ostream& outstr, int verbose ) const {
  IGNORE_UNUSED_VERBOSE;
  outstr << "F->" << GeometricMeshRegion::eltEnumToFelNameGeoEle[m_typeFace].first
          << "->" << m_idFace << " id loc " << m_idLocalEdge << " || ";
}
}
