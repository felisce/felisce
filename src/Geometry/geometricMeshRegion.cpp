//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//:
//  Main authors:    J.Castelneau & J.Foulon & V.Martin
//

// System includes
#include <numeric>
#include <queue>

// External includes

// Project includes
#include "Core/array_1d.hpp"
#include "Core/felisceParam.hpp"
#include "FiniteElement/geoElement.hpp"
#include "Geometry/geometricEdges.hpp"
#include "Geometry/geometricFaces.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/neighFacesOfEdges.hpp"
#include "Geometry/neighVolumesOfEdges.hpp"
#include "Geometry/neighVolumesOfFaces.hpp"
#include "Geometry/geo_utilities.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce 
{

const int GeometricMeshRegion::m_numPointsPerElt[] = {
  1, //0D
  2,3,3, //1D
  3,4,6, 4,5,6,8,9,  //2D
  4,5,10, //3D
  5,13, //3D
  6,9,15, //3D
  8,9,12,20,26,27 //3D
};

const int GeometricMeshRegion::m_numVerticesPerElt[] = {
  1, //0D
  2,2,2, //1D
  3,3,3, 4,4,4,4,  //2D
  4,4,4, //3D
  5,5, //3D
  6,6,6, //3D
  8,8,8,8,8,8 //3D
};

const int GeometricMeshRegion::m_numEdgesPerElt[] = {
  0, //0D
  1,1,1, //1D
  3,3,3, 4,4,4,4,4, //2D
  6,6,6, //3D
  0,0, //3D
  9,9,9, //3D
  12,12,12,12,12,12 //3D
};

const int GeometricMeshRegion::m_numFacesPerElt[] = {
  0, //0D
  0,0,0, //1D
  1,1,1, 1,1,1,1,1,  //2D
  4,4,4, //3D
  0,0, //3D
  5,5,5, //3D
  6,6,6,6,6,6 //3D
};

GeometricMeshRegion::EnumToFelNameGeoEle_type GeometricMeshRegion::eltEnumToFelNameGeoEle = GeometricMeshRegion::EnumToFelNameGeoEle_type({
      std::make_pair("Nodes" ,       &geoElementNode)           // 0D
    , std::make_pair("Segments2",    &geoElementSegmentP1)      // 1D
    , std::make_pair("Segments3b",   &geoElementSegmentP1b)
    , std::make_pair("Segments3",    &geoElementSegmentP2)
    , std::make_pair("Triangles3",   &geoElementTriangleP1)     // 2D
    , std::make_pair("Triangles4",   &geoElementTriangleP1b)
    , std::make_pair("Triangles6",   &geoElementTriangleP2)
    , std::make_pair("Quadrangles4", &geoElementQuadrangleQ1)
    , std::make_pair("Quadrangles5", &geoElementQuadrangleQ1b)
    , std::make_pair("Quadrangles6", &geoElementQuadrangleP1xP2)
    , std::make_pair("Quadrangles8", &geoElementQuadrangleQ2)
    , std::make_pair("Quadrangles9", &geoElementQuadrangleQ2c)
    , std::make_pair("Tetrahedra4",  &geoElementTetrahedronP1)  // 3D
    , std::make_pair("Tetrahedra5",  &geoElementTetrahedronP1b)
    , std::make_pair("Tetrahedra10", &geoElementTetrahedronP2)
    , std::make_pair("Pyramids5",    &geoElementNULL)
    , std::make_pair("Pyramids13",   &geoElementNULL)
    , std::make_pair("Prisms6",      &geoElementPrismR1)
    , std::make_pair("Prisms9",      &geoElementPrismP1xP2)
    , std::make_pair("Prisms15",     &geoElementPrismR2)
    , std::make_pair("Hexahedra8",   &geoElementHexahedronQ1)
    , std::make_pair("Hexahedra9",   &geoElementHexahedronQ1b)
    , std::make_pair("Hexahedra12",  &geoElementNULL)
    , std::make_pair("Hexahedra20",  &geoElementHexahedronQ2)
    , std::make_pair("Hexahedra26",  &geoElementNULL)
    , std::make_pair("Hexahedra27",  &geoElementHexahedronQ2c)
    });

GeometricMeshRegion::StringToElementType_type GeometricMeshRegion::eltFelNameToEnum = {
      std::make_pair("Nodes" ,       GeometricMeshRegion::Nod)
    , std::make_pair("Segments2",    GeometricMeshRegion::Seg2)
    , std::make_pair("Segments3b",   GeometricMeshRegion::Seg3b)
    , std::make_pair("Segments3",    GeometricMeshRegion::Seg3)
    , std::make_pair("Triangles3",   GeometricMeshRegion::Tria3)
    , std::make_pair("Triangles4",   GeometricMeshRegion::Tria4)
    , std::make_pair("Triangles6",   GeometricMeshRegion::Tria6)
    , std::make_pair("Quadrangles4", GeometricMeshRegion::Quad4)
    , std::make_pair("Quadrangles5", GeometricMeshRegion::Quad5)
    , std::make_pair("Quadrangles6", GeometricMeshRegion::Quad6)
    , std::make_pair("Quadrangles8", GeometricMeshRegion::Quad8)
    , std::make_pair("Quadrangles9", GeometricMeshRegion::Quad9)
    , std::make_pair("Tetrahedra4",  GeometricMeshRegion::Tetra4)
    , std::make_pair("Tetrahedra5",  GeometricMeshRegion::Tetra5)
    , std::make_pair("Tetrahedra10", GeometricMeshRegion::Tetra10)
    , std::make_pair("Pyramids5",    GeometricMeshRegion::Pyram5)
    , std::make_pair("Pyramids13",   GeometricMeshRegion::Pyram13)
    , std::make_pair("Prisms6",      GeometricMeshRegion::Prism6)
    , std::make_pair("Prisms9",      GeometricMeshRegion::Prism9)
    , std::make_pair("Prisms15",     GeometricMeshRegion::Prism15)
    , std::make_pair("Hexahedra8",   GeometricMeshRegion::Hexa8)
    , std::make_pair("Hexahedra9",   GeometricMeshRegion::Hexa9)
    , std::make_pair("Hexahedra12",  GeometricMeshRegion::Hexa12)
    , std::make_pair("Hexahedra20",  GeometricMeshRegion::Hexa20)
    , std::make_pair("Hexahedra26",  GeometricMeshRegion::Hexa26)
    , std::make_pair("Hexahedra27",  GeometricMeshRegion::Hexa27)
    };

GeometricMeshRegion::ElementTypeLinearToDifferentElementType_type GeometricMeshRegion::eltLinearToEltQuad = {
      std::make_pair(GeometricMeshRegion::Seg2,   GeometricMeshRegion::Seg3)
    , std::make_pair(GeometricMeshRegion::Tria3,  GeometricMeshRegion::Tria6)
    , std::make_pair(GeometricMeshRegion::Quad4,  GeometricMeshRegion::Quad8)
    , std::make_pair(GeometricMeshRegion::Quad6,  GeometricMeshRegion::Quad8)
    , std::make_pair(GeometricMeshRegion::Tetra4, GeometricMeshRegion::Tetra10)
    , std::make_pair(GeometricMeshRegion::Pyram5, GeometricMeshRegion::Pyram13)
    , std::make_pair(GeometricMeshRegion::Prism6, GeometricMeshRegion::Prism15)
    , std::make_pair(GeometricMeshRegion::Prism9, GeometricMeshRegion::Prism15)
    , std::make_pair(GeometricMeshRegion::Hexa8,  GeometricMeshRegion::Hexa20)
    };

GeometricMeshRegion::ElementTypeLinearToDifferentElementType_type GeometricMeshRegion::eltLinearToEltBubble = {
      std::make_pair(GeometricMeshRegion::Seg2,   GeometricMeshRegion::Seg3b)
    , std::make_pair(GeometricMeshRegion::Tria3,  GeometricMeshRegion::Tria4)
    , std::make_pair(GeometricMeshRegion::Quad4,  GeometricMeshRegion::Quad5)
    , std::make_pair(GeometricMeshRegion::Tetra4, GeometricMeshRegion::Tetra5)
    , std::make_pair(GeometricMeshRegion::Pyram5, GeometricMeshRegion::Pyram5)
    , std::make_pair(GeometricMeshRegion::Prism6, GeometricMeshRegion::Prism9)
    , std::make_pair(GeometricMeshRegion::Hexa8,  GeometricMeshRegion::Hexa9)
    };

std::vector<GeometricMeshRegion::ElementType> GeometricMeshRegion::bagElementType0D = { GeometricMeshRegion::Nod };

std::vector<GeometricMeshRegion::ElementType> GeometricMeshRegion::bagElementType1D = {
      GeometricMeshRegion::Seg2
    , GeometricMeshRegion::Seg3b
    , GeometricMeshRegion::Seg3
    };

std::vector<GeometricMeshRegion::ElementType> GeometricMeshRegion::bagElementType2D = {
      GeometricMeshRegion::Tria3
    , GeometricMeshRegion::Tria4
    , GeometricMeshRegion::Tria6
    , GeometricMeshRegion::Quad4
    , GeometricMeshRegion::Quad5
    , GeometricMeshRegion::Quad6
    , GeometricMeshRegion::Quad8
    , GeometricMeshRegion::Quad9
    };

std::vector<GeometricMeshRegion::ElementType> GeometricMeshRegion::bagElementType3D = {
      GeometricMeshRegion::Tetra4
    , GeometricMeshRegion::Tetra5
    , GeometricMeshRegion::Tetra10
    , GeometricMeshRegion::Prism6
    , GeometricMeshRegion::Prism9
    , GeometricMeshRegion::Prism15
    , GeometricMeshRegion::Hexa8
    , GeometricMeshRegion::Hexa9
    , GeometricMeshRegion::Hexa12
    , GeometricMeshRegion::Hexa20
    , GeometricMeshRegion::Hexa26
    , GeometricMeshRegion::Hexa27
    };

std::vector<GeometricMeshRegion::ElementType> GeometricMeshRegion::bagElementTypeLinear = {
      GeometricMeshRegion::Seg2
    , GeometricMeshRegion::Tria3
    , GeometricMeshRegion::Quad4
    , GeometricMeshRegion::Quad6
    , GeometricMeshRegion::Tetra4
    , GeometricMeshRegion::Pyram5
    , GeometricMeshRegion::Prism6
    , GeometricMeshRegion::Prism9
    , GeometricMeshRegion::Hexa8
    };

GeometricMeshRegion::NameLabelToListLabel_type GeometricMeshRegion::descriptionLineEnsightToListLabel;

const std::vector< std::vector< std::vector<unsigned short int> > > GeometricMeshRegion::m_eltTypMapVerToFac = 
{
  {}, // "Nodes"    
  { {0} }, // "Segments2"   
  { {0} }, // "Segments3b"  
  { {0} }, // "Segments3"   
  { {2,0}, {0,1}, {1,2} }, // "Triangles3"  
  { {2,0}, {0,1}, {1,2} }, // "Triangles4"  
  { {2,0}, {0,1}, {1,2} }, // "Triangles6"  
  { {3,0}, {0,1}, {1,2}, {2,3} }, // "Quadrangles4"
  { {3,0}, {0,1}, {1,2}, {2,3} }, // "Quadrangles5"
  { {3,0}, {0,1}, {1,2}, {2,3} }, // "Quadrangles6"
  { {3,0}, {0,1}, {1,2}, {2,3} }, // "Quadrangles8"
  { {3,0}, {0,1}, {1,2}, {2,3} }, // "Quadrangles9"
  { {0,1,3}, {0,1,2}, {0,2,3}, {1,2,3} }, // "Tetrahedra4" 
  { {0,1,3}, {0,1,2}, {0,2,3}, {1,2,3} }, // "Tetrahedra5" 
  { {0,1,3}, {0,1,2}, {0,2,3}, {1,2,3} }, // "Tetrahedra10"
  {}, // "Pyramids5"   
  {}, // "Pyramids13"  
  {}, // "Prisms6"     
  {}, // "Prisms9"     
  {}, // "Prisms15"   
  {}, // "Hexahedra8"  
  {}, // "Hexahedra9"
  {}, // "Hexahedra12"  
  {}, // "Hexahedra20" 
  {}, // "Hexahedra26" 
  {}  // "Hexahedra27"
}; // TODO replace this ugly structure defining a class for every element type + polymorphism

const std::vector< std::vector< std::vector<unsigned short int> > > GeometricMeshRegion::m_eltTypMapFacToVer = 
{
  { {0} }, // "Nodes"    
  { {0,1} }, // "Segments2"   
  { {0,1} }, // "Segments3b"  
  { {0,1} }, // "Segments3"   
  { {0,1}, {1,2}, {2,0} }, // "Triangles3"  
  { {0,1}, {1,2}, {2,0} }, // "Triangles4"  
  { {0,1}, {1,2}, {2,0} }, // "Triangles6"  
  { {0,1}, {1,2}, {2,3}, {3,0} }, // "Quadrangles4"
  { {0,1}, {1,2}, {2,3}, {3,0} }, // "Quadrangles5"
  { {0,1}, {1,2}, {2,3}, {3,0} }, // "Quadrangles6"
  { {0,1}, {1,2}, {2,3}, {3,0} }, // "Quadrangles8"
  { {0,1}, {1,2}, {2,3}, {3,0} }, // "Quadrangles9"
  { {0,1,2}, {0,3,1}, {1,3,2}, {0,2,3} }, // "Tetrahedra4" 
  { {0,1,2}, {0,3,1}, {1,3,2}, {0,2,3} }, // "Tetrahedra5" 
  { {0,1,2}, {0,3,1}, {1,3,2}, {0,2,3} }, // "Tetrahedra10"
  {}, // "Pyramids5"   
  {}, // "Pyramids13"  
  {}, // "Prisms6"     
  {}, // "Prisms9"     
  {}, // "Prisms15"   
  {}, // "Hexahedra8"  
  {}, // "Hexahedra9"
  {}, // "Hexahedra12"  
  {}, // "Hexahedra20" 
  {}, // "Hexahedra26" 
  {}  // "Hexahedra27"
}; // TODO replace this ugly structure defining a class for every element type + polymorphism

const std::vector< std::vector< std::vector<unsigned short int> > > GeometricMeshRegion::m_eltTypMapEdgToVer = 
{
  { }, // "Nodes"    
  { }, // "Segments2"   
  { }, // "Segments3b"  
  { }, // "Segments3"   
  { }, // "Triangles3"  
  { }, // "Triangles4"  
  { }, // "Triangles6"  
  { }, // "Quadrangles4"
  { }, // "Quadrangles5"
  { }, // "Quadrangles6"
  { }, // "Quadrangles8"
  { }, // "Quadrangles9"
  { {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3} }, // "Tetrahedra4" 
  { {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3} }, // "Tetrahedra5" 
  { {0,1}, {0,2}, {0,3}, {1,2}, {1,3}, {2,3} }, // "Tetrahedra10"
  {}, // "Pyramids5"   
  {}, // "Pyramids13"  
  {}, // "Prisms6"     
  {}, // "Prisms9"     
  {}, // "Prisms15"   
  {}, // "Hexahedra8"  
  {}, // "Hexahedra9"
  {}, // "Hexahedra12" 
  {}, // "Hexahedra20" 
  {}, // "Hexahedra26" 
  {}  // "Hexahedra27"
}; // TODO replace this ugly structure defining a class for every element type + polymorphism

const std::vector< std::vector< std::vector<unsigned short int> > > GeometricMeshRegion::m_eltTypMapEdgToFac = 
{
  { }, // "Nodes"    
  { }, // "Segments2"   
  { }, // "Segments3b"  
  { }, // "Segments3"   
  { {0}, {0}, {0} }, // "Triangles3"  
  { {0}, {0}, {0} }, // "Triangles4"  
  { {0}, {0}, {0} }, // "Triangles6"  
  { {0}, {0}, {0}, {0} }, // "Quadrangles4"
  { {0}, {0}, {0}, {0} }, // "Quadrangles5"
  { {0}, {0}, {0}, {0} }, // "Quadrangles6"
  { {0}, {0}, {0}, {0} }, // "Quadrangles8"
  { {0}, {0}, {0}, {0} }, // "Quadrangles9"
  { {0,1}, {0,3}, {1,3}, {0,2}, {1,2}, {2,3} }, // "Tetrahedra4" 
  { {0,1}, {0,3}, {1,3}, {0,2}, {1,2}, {2,3} }, // "Tetrahedra5" 
  { {0,1}, {0,3}, {1,3}, {0,2}, {1,2}, {2,3} }, // "Tetrahedra10"
  {}, // "Pyramids5"   
  {}, // "Pyramids13"  
  {}, // "Prisms6"     
  {}, // "Prisms9"     
  {}, // "Prisms15"   
  {}, // "Hexahedra8"  
  {}, // "Hexahedra9"
  {}, // "Hexahedra12"  
  {}, // "Hexahedra20" 
  {}, // "Hexahedra26" 
  {}  // "Hexahedra27"
}; // TODO replace this ugly structure defining a class for every element type + polymorphism










/***********************************************************************************/
/***********************************************************************************/

GeometricMeshRegion::GeometricMeshRegion()
{
  // Initialization of tab m_numElements
  for (int ityp = 0; ityp < m_numTypesOfElement; ++ityp) {
    m_numElements[ityp]  = 0;
    m_listElements[ityp] = nullptr;
  }
}

/***********************************************************************************/
/***********************************************************************************/

GeometricMeshRegion::~GeometricMeshRegion() 
{
  for (int ityp = 0; ityp < m_numTypesOfElement; ++ityp)
    if ( m_numElements[ityp] != 0 )
      delete [] m_listElements[ityp];
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setDomainDim()
{
  if ( m_domainDim != GeoMeshUndefined )
    FEL_ERROR("Problem: you can std::set the mesh dimension only once!");

  for (auto it_eltype = bagElementType3D.begin(); it_eltype != bagElementType3D.end() && m_domainDim == GeoMeshUndefined ; ++it_eltype) {
    if ( m_numElements[*it_eltype] > 0 )
      m_domainDim = GeoMesh3D;
  }

  for (auto it_eltype = bagElementType2D.begin(); it_eltype != bagElementType2D.end() && m_domainDim == GeoMeshUndefined ; ++it_eltype) {
    if ( m_numElements[*it_eltype] > 0 )
      m_domainDim = GeoMesh2D;
  }

  for (auto it_eltype = bagElementType1D.begin(); it_eltype != bagElementType1D.end() && m_domainDim == GeoMeshUndefined ; ++it_eltype) {
    if ( m_numElements[*it_eltype] > 0 )
      m_domainDim = GeoMesh1D;
  }

  for (auto it_eltype = bagElementType0D.begin(); it_eltype != bagElementType0D.end() && m_domainDim == GeoMeshUndefined ; ++it_eltype) {
    if ( m_numElements[*it_eltype] > 0 )
      m_domainDim = GeoMesh0D;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::allocateElements(const ElementType& eltType)
{

  m_listElements[eltType] = new felInt[ m_numElements[eltType] * m_numPointsPerElt[eltType] ];
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::deleteElements()
{
  for (int ityp = 0; ityp < m_numTypesOfElement; ++ityp)
    if ( m_numElements[ityp] != 0 )
      delete [] m_listElements[ityp];
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::deleteElementsPerType(const ElementType& eltType)
{
  if ( m_numElements[eltType] != 0 )
    delete [] m_listElements[eltType];
  m_numElements[eltType] = 0;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::reorderListElePerRef(const std::map<int, std::vector<felInt>>& RefToElements, const ElementType eltType)
{
  felInt tmpSizeElements, begElement, nbElements;
  int refNumber;

  if ( RefToElements.size() > 0 ) {

    tmpSizeElements = m_numElements[eltType] * m_numPointsPerElt[eltType];

    felInt *tmpElements = new felInt[ tmpSizeElements ];
    begElement = 0;
    nbElements = 0;
    for(auto it_ref = RefToElements.begin(); it_ref !=RefToElements.end(); ++it_ref) {
      refNumber = it_ref->first;

      // Retrieve all elements having the given ref (their IDs are given by it_ref->second)
      getElements(eltType, it_ref->second.data(), it_ref->second.size(), tmpElements, tmpSizeElements, begElement);
      
      nbElements = it_ref->second.size();
      intRefToBegEndMaps[eltType][refNumber] = std::make_pair(begElement, nbElements);
      
      // Shift the starting position
      begElement += it_ref->second.size() * m_numPointsPerElt[eltType];
    }    
    delete[] m_listElements[eltType];
    
    // swap the lists.
    m_listElements[eltType] = tmpElements;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setBagElementTypeDomain()
{
  m_bagElementTypeDomain.clear();

  switch ( m_domainDim ) {

    case GeoMesh0D: // 0D
      for (auto it_eltype = bagElementType0D.begin(); it_eltype != bagElementType0D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomain.push_back(*it_eltype);

      break;

    case GeoMesh1D: // 1D
      for (auto it_eltype = bagElementType1D.begin(); it_eltype != bagElementType1D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomain.push_back(*it_eltype);

      break;

    case GeoMesh2D: // 2D
      for (auto it_eltype = bagElementType2D.begin(); it_eltype != bagElementType2D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomain.push_back(*it_eltype);

      break;

    case GeoMesh3D: // 3D
      for (auto it_eltype = bagElementType3D.begin(); it_eltype != bagElementType3D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomain.push_back(*it_eltype);

      break;

    case GeoMeshUndefined:
      FEL_ERROR("GeometricMeshRegion: you must define the mesh dimension!");

      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setBagElementTypeDomainBoundary()
{
  const auto& r_instance = FelisceParam::instance();
  const bool extendToLowerDimensions = r_instance.extendToLowerDimensions;
  m_bagElementTypeDomainBoundary.clear();

  switch ( m_domainDim ) {

    case GeoMesh0D: // 0D

      break;

    case GeoMesh1D: // 1D
      for (auto it_eltype = bagElementType0D.begin(); it_eltype != bagElementType0D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomainBoundary.push_back(*it_eltype);

      break;

    case GeoMesh2D: // 2D

      // TODO
      // if ( extendToLowerDimensions ) {
      //   for (auto it_eltype = bagElementType0D.begin(); it_eltype != bagElementType0D.end(); ++it_eltype)
      //     if ( m_numElements[*it_eltype] > 0 )
      //       m_bagElementTypeDomainBoundary.push_back(*it_eltype);
      // }

      for (auto it_eltype = bagElementType1D.begin(); it_eltype != bagElementType1D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomainBoundary.push_back(*it_eltype);

      break;

    case GeoMesh3D: // 3D

      if ( extendToLowerDimensions ) {
        for (auto it_eltype = bagElementType1D.begin(); it_eltype != bagElementType1D.end(); ++it_eltype)
          if ( m_numElements[*it_eltype] > 0 )
            m_bagElementTypeDomainBoundary.push_back(*it_eltype);
      }

      for (auto it_eltype = bagElementType2D.begin(); it_eltype != bagElementType2D.end(); ++it_eltype)
        if ( m_numElements[*it_eltype] > 0 )
          m_bagElementTypeDomainBoundary.push_back(*it_eltype);

      break;

    case GeoMeshUndefined:
      FEL_ERROR("GeometricMeshRegion: you must define the mesh dimension!");

      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setLocalMesh(GeometricMeshRegion& meshGlobal, const std::vector<int>& eltPartition,
                                       const int rank, std::vector<felInt>& loc2globElem)
{
  m_builtEdges = false;
  m_builtFaces = false;
  m_flagFormatMesh = meshGlobal.m_flagFormatMesh;
  m_listPoints.clear();
  m_listNormals.clear();
  m_listTangents.clear();
  m_numCoor = meshGlobal.numCoor();
  m_domainDim = meshGlobal.domainDim();
  m_createNormalTangent = meshGlobal.createNormalTangent();
  m_listTangents.resize(m_domainDim);
  for (int ityp = 0; ityp < m_numTypesOfElement; ++ityp) {
    m_numElements[ityp] = 0;
  }

  // Copy of all mesh points
  m_listPoints = meshGlobal.listPoints();
  if (m_createNormalTangent) {
    m_listNormals = meshGlobal.listNormals();
    for (int i = 0 ; i<m_domainDim; i++) {
      m_listTangents[i] = meshGlobal.m_listTangents[i];
    }
  }

  // Allocation of memory for DOMAIN m_listElements and BOUNDARY m_listElements
  felInt countEltTot = 0;
  auto& r_element_type_domain = meshGlobal.bagElementTypeDomain();
  m_allocateListElementsByRef(meshGlobal.bagElementTypeDomain(), meshGlobal,
                              eltPartition, rank, countEltTot);

  auto& r_element_type_domain_boundary = meshGlobal.bagElementTypeDomainBoundary();
  m_allocateListElementsByRef(r_element_type_domain_boundary, meshGlobal,
                              eltPartition, rank, countEltTot);

  // Reset to 0 the element counter
  countEltTot = 0;

  // Reset intRefToBegEndMaps for domain an boundary
  for (auto  it_eltype =  r_element_type_domain.begin(); it_eltype != r_element_type_domain.end(); ++it_eltype) {
      ElementType eltType = static_cast<ElementType>(*it_eltype);
      intRefToBegEndMaps[eltType].clear();
  }
  for (auto it_eltype = r_element_type_domain_boundary.begin(); it_eltype != r_element_type_domain_boundary.end(); ++it_eltype) {
      ElementType eltType = static_cast<ElementType>(*it_eltype);
      intRefToBegEndMaps[eltType].clear();
  }

  // Fill of m_listElements and map intRefToBegEndMaps
  m_fillListElementsByRef(r_element_type_domain, meshGlobal, eltPartition, rank, loc2globElem, countEltTot);

  // Fill of m_listElements and map intRefToBegEndMaps
  m_fillListElementsByRef(r_element_type_domain_boundary, meshGlobal, eltPartition, rank, loc2globElem, countEltTot);

  // Set dim and bags
  setBagElementTypeDomain();
  setBagElementTypeDomainBoundary();
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::buildEdges()
{
  if ( !m_builtEdges ) {
    m_listEdges.clearAndInit( this->numPoints() );

    //! order of the list of edges: first 1d, then 2d then 3d.
    //! (given edges first, then edges of faces, then edges of volumes.)
    m_buildEdgesPerBag(bagElementType1D, 1);
    m_listEdges.setNumGivenEdges(); // right after having inserted the given edges
    m_buildEdgesPerBag(bagElementType2D, 2);
    m_buildEdgesPerBag(bagElementType3D, 3);

    felInt numElem1D = getNumElement1D();
    if ( m_listEdges.numGivenEdges() <  numElem1D ) {
      std::cout << "We found  " << m_listEdges.numGivenEdges() << " edges, while " << numElem1D << " edges were provided in the mesh!\n";
      FEL_WARNING("Beware: some duplicated edges in the mesh were discarded. (Or there is a problem in the buildEdge!)");
    }

    if ( m_listEdges.numGivenEdges() > numElem1D ) {
      std::cout << "We found  " << m_listEdges.numGivenEdges() << " edges, while " << numElem1D << " edges were provided in the mesh!\n";
      FEL_ERROR("There is a problem in the buildEdge: the number of given edges is not correct!");
    }

    m_builtEdges = true;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::buildFaces()
{
  if ( !m_builtFaces ) {
    m_listFaces.clearAndInit(this->numPoints());

    //! order of the list of faces: first 2d, then 3d.
    //! (given faces first, then faces of volumes.)
    m_buildFacesPerBag(bagElementType2D, 2);
    m_listFaces.setNumGivenFaces();// right after having inserted the given faces
    m_buildFacesPerBag(bagElementType3D, 3);

    felInt numElem2D = getNumElement2D();
    if ( m_listFaces.numGivenFaces() < numElem2D ) {
      std::cout << "We found  " << m_listFaces.numGivenFaces() << " faces, while " << numElem2D << " faces were provided in the mesh!\n";
      FEL_WARNING("Beware: some duplicated faces in the mesh were discarded. (Or there is a problem in the buildFace!)");
    }
    if ( m_listFaces.numGivenFaces() > numElem2D ) {
      std::cout << "We found  " << m_listFaces.numGivenFaces() << " faces, while " << numElem2D << " faces were provided in the mesh!\n";
      FEL_ERROR("There is a problem in the buildFace: the number of given faces is not correct!");
    }

    m_builtFaces = true;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::moveMesh(const std::vector<double>& disp, const felReal& coef) 
{
  if ( static_cast<int>(disp.size()) != m_numCoor * this->numPoints() ) {
    // PetscPrintf(MpiInfo::petscComm(),"Displacement size = %ld vs numCoor * numPoints = %d \n",static_cast<unsigned long>(disp.size()), m_numCoor * numPoints()); We shouldn't use Mpi routines for cout... because i may want to use this function not within a model but in an external executable without MPI
    std::cerr << "Displacement size = " << disp.size() << " vs numCoor * numPoints = " << m_numCoor * numPoints() << std::endl;
    FEL_ERROR("GeometricMeshRegion: We can not move the mesh with this displacement\n" );
  }

  if ( !m_moved ) {
    // We store the reference position of the mesh
    m_moved = true;
    m_listReferencePoints.resize( numPoints() );
    for (int i = 0; i < numPoints(); ++i)
      m_listReferencePoints[i] = m_listPoints[i];
  }

  for (int i = 0; i < numPoints() ; ++i)
    for (int j = 0; j < m_numCoor; ++j)
      m_listPoints[i].coor(j) = m_listReferencePoints[i].coor(j) + coef*disp[j + m_numCoor*i];
}

/***********************************************************************************/
/***********************************************************************************/
// TODO this function should be rewritten introducing a method "computeNormal" for every shape
void GeometricMeshRegion::computeNormalTangent(const InitialNormalTangentProvided initialNormalTangentProvided)
{
  // Initialization
  felInt numPointPerElement = 0;
  double norm = 0.;
  const std::size_t number_of_points = m_listPoints.size();
  std::vector<double> meas(number_of_points, 0.0);
  std::vector<felInt> listPt;
  ElementType eltType;
  ElementType previous_eltType = ELEMENT_TYPE_COUNTER;

  // Allocate values
  allocateListNormalsAndTangents(initialNormalTangentProvided);

  // We want compute the average normal on the points of the mesh : n_A / || n_A ||
  // with n_A = ( \sum_i n_i A_i / || n_i || ) / ( \sum_i A_i)
  // where n_i and A_i are the normal and the area of the i-th element, respectively.
  // Therefore we have :
  // n_A / || n_A || = ( \sum_i n_i A_i / || n_i || ) / || ( \sum_i n_i A_i / || n_i || ) ||
  // Depending on how the normal is computed, for some shapes, this formula can be further simplified.
  // Same holds for the tangent vector

  const auto& r_bag_elements = m_domainDim == 3 ? m_bagElementTypeDomainBoundary : m_bagElementTypeDomain;
  if ( m_domainDim == 1 ) {

    // Loop over the elements
    for (std::size_t itype = 0; itype < r_bag_elements.size(); ++itype) {
      eltType = r_bag_elements[itype];
      numPointPerElement = m_numPointsPerElt[eltType];
      listPt.resize(numPointPerElement);

      if ( eltType == Seg2 ) {
        // In this case the formula reduces to : t_A / || t_A || = \sum_i t_i / || \sum_i t_i ||
        
        // Loop over the elements
        for (felInt iel = 0; iel < m_numElements[eltType]; ++iel) {

          // Get element point ids
          for (felInt ipt = 0; ipt < numPointPerElement; ++ipt)
            listPt[ipt] = m_listElements[eltType][numPointPerElement * iel + ipt];

          // Get element points
          const auto& r_point_0 = m_listPoints[listPt[0]];
          const auto& r_point_1 = m_listPoints[listPt[1]];

          // Compute temporary tangent
          Point tangent_tmp(r_point_1-r_point_0);
          
          // Average temporary tangent
          for (auto it = listPt.begin(); it != listPt.end(); ++it)
            m_listTangents[0][*it] += tangent_tmp;
        }

        // Compute the tangent at node
        for (std::size_t i = 0; i < m_listTangents[0].size(); ++i) {
          const double norm = m_listTangents[0][i].norm();
          if ( norm > std::numeric_limits<double>::epsilon() )
            m_listTangents[0][i] /= norm;
        }

        // If we are in 2D
        if ( m_numCoor == 2 ){
          // Compute the normal at node from the tangent
          for (std::size_t i = 0; i < number_of_points; ++i) {
            m_listNormals[i].y() =  m_listTangents[0][i].x();
            m_listNormals[i].x() = -m_listTangents[0][i].y();
          }
        }
        // If we are in 3D
        else if ( m_numCoor == 3 ) {
          // Compute the orthonormal base 
          array_1d<double, 3> normal, tangent_1, tangent_2;
          if (initialNormalTangentProvided == InitialNormalTangentProvided::NONE) { // Compute whole base
            for(std::size_t i=0; i<number_of_points; ++i) {
              tangent_1.data() = m_listTangents[0][i].getCoor();
              MathUtilities::OrthonormalBasis(tangent_1, normal, tangent_2);
              m_listNormals[i].getCoor() = normal.data();
              m_listTangents[1][i].getCoor() = tangent_2.data();
            }
          } else if (initialNormalTangentProvided == InitialNormalTangentProvided::NORMAL) {
            for(std::size_t i=0; i<number_of_points; ++i) {
              normal.data() = m_listNormals[i].getCoor();
              tangent_1.data() = m_listTangents[0][i].getCoor();
              MathUtilities::UnitCrossProduct(tangent_2, tangent_1, normal);
              m_listTangents[1][i].getCoor() = tangent_2.data();
            }
          } else if (initialNormalTangentProvided == InitialNormalTangentProvided::TANGENT_2) {
            for(std::size_t i=0; i<number_of_points; ++i) {
              tangent_1.data() = m_listTangents[0][i].getCoor();
              tangent_2.data() = m_listTangents[1][i].getCoor();
              MathUtilities::UnitCrossProduct(normal, tangent_2, tangent_1);
              m_listNormals[i].getCoor() = normal.data();
            }
          } else {
            FEL_ERROR("Tangent 1 cannot be provided, must be normal or tangent 2")
          }
        }
      }
      else {
        FEL_ERROR("Not implemented for this type of shape.");
      }
    }
  } else {

    // Compute the normals on each elements
    // Loop over the elements
    for(std::size_t itype=0; itype<r_bag_elements.size(); ++itype) {
      eltType = r_bag_elements[itype];
      numPointPerElement = m_numPointsPerElt[eltType];
      listPt.resize(numPointPerElement);

      // Compute normal and tangents in 3D
      int iq[2][numPointPerElement];
      std::vector<felInt> listPtQ1Elem;
      std::array<Point, 2> vecTangentialPlan;   // < a1,a2 > define the plan gives by the triangle

      for(felInt iel=0; iel<m_numElements[eltType]; ++iel) {
        for(felInt ipt=0; ipt<numPointPerElement; ++ipt)
          listPt[ipt] = m_listElements[eltType][numPointPerElement * iel + ipt];

        // ElementTriangleP1 iq = { {1,2,0},{2,0,1} };
        // ElementQuadrangleQ1 iq = { {1,2,3,0},{3,0,1,2} };
        if  (eltType == Tria3 || eltType == Quad4) {
          for (int l=0 ; l<numPointPerElement-1 ; l++) {
            iq[0][l]=l+1;
            iq[1][l+1]=l;
          }
          iq[0][numPointPerElement-1]=0;
          iq[1][0]=numPointPerElement-1;
        } else if (eltType == Quad9) {
          listPtQ1Elem.resize(16);
          for (int i=0 ; i<4 ; i++) {
            listPtQ1Elem[4*i]   = listPt[i];
            listPtQ1Elem[4*i+1] = listPt[4+i];
            listPtQ1Elem[4*i+2] = listPt[8];
            listPtQ1Elem[4*i+3] = listPt[3+i];
          }
          listPtQ1Elem[3] = listPt[7];

          for (int l=0 ; l<3 ; l++) {
            iq[0][l]=l+1;
            iq[1][l+1]=l;
          }
          iq[0][4-1]=0;
          iq[1][0]=4-1;
        } else {
          if (eltType != previous_eltType) {
            std::cout << "WARNING: Not compatible geometry type: " << eltType << std::endl;
          }
          previous_eltType = eltType;
          continue;
        }

        if  (eltType == Quad9) {
          for (int k=0 ; k<4 ; k++) {
            for (int i=0 ; i<4 ; i++) {

              for (int j=0 ; j<2 ; j++)
                vecTangentialPlan[j] = m_listPoints[ listPtQ1Elem[4*k+iq[j][i]] ] - m_listPoints[ listPtQ1Elem[4*k+i] ];

              m_listNormals[listPtQ1Elem[4*k+i]].x() += vecTangentialPlan[0].y()*vecTangentialPlan[1].z()-vecTangentialPlan[0].z()*vecTangentialPlan[1].y();
              m_listNormals[listPtQ1Elem[4*k+i]].y() += vecTangentialPlan[0].z()*vecTangentialPlan[1].x()-vecTangentialPlan[0].x()*vecTangentialPlan[1].z();
              m_listNormals[listPtQ1Elem[4*k+i]].z() += vecTangentialPlan[0].x()*vecTangentialPlan[1].y()-vecTangentialPlan[0].y()*vecTangentialPlan[1].x();
            }
          }
        } else {
          // Tria3 || Quad4
          for (int i = 0; i < numPointPerElement; ++i) {

            for (int j = 0 ; j < 2; ++j)
              vecTangentialPlan[j] = m_listPoints[ listPt[ iq[j][i] ] ] - m_listPoints[ listPt[i] ];

            m_listNormals[listPt[i]].x() += vecTangentialPlan[0].y()*vecTangentialPlan[1].z()-vecTangentialPlan[0].z()*vecTangentialPlan[1].y();
            m_listNormals[listPt[i]].y() += vecTangentialPlan[0].z()*vecTangentialPlan[1].x()-vecTangentialPlan[0].x()*vecTangentialPlan[1].z();
            m_listNormals[listPt[i]].z() += vecTangentialPlan[0].x()*vecTangentialPlan[1].y()-vecTangentialPlan[0].y()*vecTangentialPlan[1].x();
          }
        }
      }
    }

    // Normalize normals
    for(std::size_t i=0; i<m_listNormals.size(); i++) {
      const double norm_normal = m_listNormals[i].norm();
      if (norm_normal > std::numeric_limits<double>::epsilon()) {
        m_listNormals[i] /= norm_normal;
      }
    }
    std::unordered_map<std::string, int> mapOfTypeDirectionTangent;
    mapOfTypeDirectionTangent["e1"] = 1;
    mapOfTypeDirectionTangent["e2"] = 2;
    mapOfTypeDirectionTangent["e3"] = 3;
    m_choiceDirectionTangent  = mapOfTypeDirectionTangent[FelisceParam::instance().choiceDirectionTangent];
    std::array<double,3> e;

    // Compute V1
    for(std::size_t i=0; i<m_listPoints.size(); ++i) {
      if (m_listNormals[i].norm() > std::numeric_limits<double>::epsilon()) {
        bool firstTangentValidated = false;
        //test the good choice of std::vector e
        while (!firstTangentValidated) {
          switch (m_choiceDirectionTangent) {
          case 1 :
            e[0] = 1.; e[1] = 0.; e[2] = 0.;
            break;
          case 2 :
            e[0] = 0.; e[1] = 1.; e[2] = 0.;
            break;
          case 3 :
            e[0] = 0.; e[1] = 0.; e[2] = 1.;
            break;
          default:
            e[0] = 1.; e[1] = 0.; e[2] = 0.;
            break;
          }

          m_listTangents[0][i].x() = m_listNormals[i].y()*e[2]-m_listNormals[i].z()*e[1];
          m_listTangents[0][i].y() = m_listNormals[i].z()*e[0]-m_listNormals[i].x()*e[2];
          m_listTangents[0][i].z() = m_listNormals[i].x()*e[1]-m_listNormals[i].y()*e[0];
          norm = m_listTangents[0][i].norm();
          if (norm<0.001) {
            m_choiceDirectionTangent++;
          } else {
            firstTangentValidated = true;
          }
        }
        m_listTangents[0][i] /= norm;
        //compute V2
        m_listTangents[1][i].x() = m_listNormals[i].y()*m_listTangents[0][i].z()-m_listNormals[i].z()*m_listTangents[0][i].y();
        m_listTangents[1][i].y() = m_listNormals[i].z()*m_listTangents[0][i].x()-m_listNormals[i].x()*m_listTangents[0][i].z();
        m_listTangents[1][i].z() = m_listNormals[i].x()*m_listTangents[0][i].y()-m_listNormals[i].y()*m_listTangents[0][i].x();
        m_listTangents[1][i] /= m_listTangents[1][i].norm();
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::allocateListNormalsAndTangents(const InitialNormalTangentProvided initialNormalTangentProvided)
{
  // TODO: Normals and Tangents should be arrays instead of Points for efficiency
  const std::size_t number_of_points = this->numPoints();
  if (m_listNormals.size() != number_of_points) {
    m_listNormals.resize(number_of_points, Point(0.0));
    const int aux_size = this->domainDim() > this->numCoor() - 1 ? this->domainDim() : this->numCoor() - 1;
    m_listTangents.resize(aux_size, std::vector<Point>(number_of_points,0.0));
  } else {
    if (initialNormalTangentProvided == InitialNormalTangentProvided::NONE) {
      std::fill(m_listNormals.begin(), m_listNormals.end(), 0.);
      std::for_each(m_listTangents.begin(), m_listTangents.end(), [](auto& listTan){ std::fill(listTan.begin(), listTan.end(), 0.); } );
    } else if (initialNormalTangentProvided == InitialNormalTangentProvided::NORMAL) {
      std::for_each(m_listTangents.begin(), m_listTangents.end(), [](auto& listTan){ std::fill(listTan.begin(), listTan.end(), 0.); } );
    } else if (initialNormalTangentProvided == InitialNormalTangentProvided::TANGENT_2) {
      std::fill(m_listNormals.begin(), m_listNormals.end(), 0.);
      std::fill(m_listTangents[0].begin(), m_listTangents[0].end(), 0.);
    } else{
      FEL_ERROR("Tangent 1 cannot be provided, must be normal or tangent 2");  
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::computeElementNormal(std::vector<std::vector<Point> >& listElementNormals) const
{
  // Initialization
  felInt numPointPerElement;
  ElementType eltType;
  std::vector<felInt> veridxOfElt;
  std::vector<Point*> pointsOfElt;

  // Allocate values
  if ( listElementNormals.size() != static_cast<std::size_t>(m_numTypesOfElement) )
    listElementNormals.resize(m_numTypesOfElement);

  // Loop over all the domain elements
  for(std::size_t itype = 0; itype < m_bagElementTypeDomain.size(); ++itype) {

    // get element type
    eltType = m_bagElementTypeDomain[itype];

    // get number point per elementtype
    numPointPerElement = m_numPointsPerElt[eltType];

    // resize vector
    listElementNormals[eltType].resize(m_numElements[eltType]);
    veridxOfElt.resize(numPointPerElement);
    pointsOfElt.resize(numPointPerElement);

    // Compute normal for every "eltType" element 
    for(felInt iel = 0; iel < m_numElements[eltType]; ++iel) {

      // Get element vertices
      getOneElement(eltType, iel, veridxOfElt, pointsOfElt, 0);
      
      // Compute normal
      GeoUtilities::computeElementNormal(eltType, pointsOfElt, listElementNormals[eltType][iel]);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getElements(const ElementType& eltType, const felInt* indexList, felInt numIndexList,
                                      felInt* array, int sizeArray, felInt startPosArray) const 
{
  if ( m_numElements[eltType] == 0 ) {
    std::cout << "Warning: trying to access a type of element with no elements: " << eltEnumToFelNameGeoEle[eltType].first << "!"  << std::endl;
    return;
  }

  const int nVpEl = m_numPointsPerElt[eltType];

  // Check if all the elements can fit in array
  FEL_ASSERT( sizeArray >= numIndexList*nVpEl + startPosArray );

  for (felInt ii = 0; ii < numIndexList; ++ii) {
    felInt kind = indexList[ii];

    FEL_ASSERT(kind >= 0 && kind < m_numElements[eltType]);

    for (int jj = 0; jj < nVpEl; ++jj)
      array[ startPosArray + ii*nVpEl + jj ] = m_listElements[eltType][ kind*nVpEl + jj];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setElements(const ElementType& eltType, const felInt* indexList, 
                                      felInt numIndexList, const felInt* array, int sizeArray) 
{
  const int nVpEl = m_numPointsPerElt[eltType];

  FEL_ASSERT(sizeArray == numIndexList*nVpEl);

  for (felInt ii = 0; ii < numIndexList; ++ii) {
    felInt kind = indexList[ii];

    FEL_ASSERT(kind >= 0 && kind < m_numElements[eltType]);

    for (felInt jj = 0; jj< m_numPointsPerElt[eltType]; ++jj)
      m_listElements[eltType][ kind*nVpEl + jj ] = array[ ii*nVpEl + jj ];
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getOneElement(const ElementType& eltType, const felInt index, std::vector<felInt>& array, 
                                        const felInt startindex, const bool flagIncrement) const 
{
  const int nVpEl = m_numPointsPerElt[eltType];

  FEL_ASSERT( array.size() == static_cast<std::size_t>(nVpEl) );
  FEL_ASSERT( index >= 0 && index < m_numElements[eltType]);

  for (int jj = 0; jj < nVpEl; ++jj)
    array[jj] = m_listElements[eltType][ startindex + index*nVpEl + jj ] + flagIncrement;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getOneElement(const ElementType& eltType, const felInt index, std::vector<felInt>& array,
                                        std::vector<Point*>& rPoints, const felInt startindex, const bool flagIncrement) const 
{
  getOneElement(eltType, index, array, startindex, flagIncrement);

  const int numPointsPerElt = GeometricMeshRegion::m_numPointsPerElt[eltType];
  for (int i_point = 0; i_point < numPointsPerElt; ++i_point)
    rPoints[i_point] = const_cast<Point*>(&(m_listPoints[array[i_point]]));
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getOneElement(const felInt idElem, std::vector<felInt>& array, const bool flagIncrement) const 
{
  ElementType eltType;
  felInt idByType = -1;

  getTypeElemFromIdElem(idElem, eltType, idByType);
  getOneElement(eltType, idByType, array, 0, flagIncrement);
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setOneElement(const ElementType& eltType, const felInt index,
                                        const std::vector<felInt>& array, const bool flagDecrement)
{
  const int nVpEl = m_numPointsPerElt[eltType];

  FEL_ASSERT( array.size() == static_cast<std::size_t>(nVpEl) );
  FEL_ASSERT( index >= 0 && index < m_numElements[eltType] );

  for (int jj = 0; jj < nVpEl; jj++)
    m_listElements[eltType][ index*nVpEl + jj ] = array[jj] - flagDecrement;
}

/***********************************************************************************/
/***********************************************************************************/

int GeometricMeshRegion::getElementsPerRef(const ElementType& eltType, const int ref, std::vector<felInt>& array) const 
{
  if ( m_numElements[eltType] == 0 ) {
    std::cout << "Warning: trying to access a type of element with no elements: " << eltEnumToFelNameGeoEle[eltType].first << "!" << std::endl;
    return 0;
  }

  auto it_ref = intRefToBegEndMaps[eltType].find(ref);
  if ( it_ref == intRefToBegEndMaps[eltType].end() ) {
    std::cout << "Warning: trying to access an unexisting reference: " << ref << " for element type "<< eltType << " !" << std::endl;
    return 0;
  }

  const int nVpEl = m_numPointsPerElt[eltType];
  const felInt startindex = it_ref->second.first;
  const felInt numRefEle  = it_ref->second.second;

  array.resize(nVpEl*numRefEle);

  for (felInt iel = 0 ; iel < numRefEle; ++iel)
    for (int jvert = 0; jvert < nVpEl; ++jvert)
      array[ iel*nVpEl + jvert] = m_listElements[eltType][ startindex + iel*nVpEl + jvert];

  return numRefEle;
}

/***********************************************************************************/
/***********************************************************************************/

// TODO felInt& out_startindex, felInt& out_numRefEle 
void GeometricMeshRegion::getElementsIDPerRef(const ElementType& eltType, const int ref, felInt* out_startindex, felInt* out_numRefEle) const 
{
  if ( m_numElements[eltType] == 0 )
    std::cout << "Warning: trying to access a type of element with no elements: " << eltEnumToFelNameGeoEle[eltType].first << "!" << std::endl;

  auto it_ref = intRefToBegEndMaps[eltType].find(ref);
  if ( it_ref == intRefToBegEndMaps[eltType].end() )
    std::cout << "Warning: trying to access an unexisting reference: " << ref << " for element type "<< eltType << " !" << std::endl;

  const int nVpEl = m_numPointsPerElt[eltType];
  const felInt startindex = it_ref->second.first / nVpEl;
  const felInt numRefEle  = it_ref->second.second;

  *out_startindex = startindex;
  *out_numRefEle  = numRefEle;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getTypeElemFromIdElem(const felInt idElem, ElementType& eltType, felInt& idByType) const 
{
  felInt numEltPerLabel;
  felInt countElt = 0;

  std::array<const std::vector<ElementType>*,2> bags{ &m_bagElementTypeDomain, &m_bagElementTypeDomainBoundary };

  if( idElem >= getNumElementByBag(*bags[0]) + getNumElementByBag(*bags[1]) )
    FEL_ERROR("GeometricMeshRegion: element not found!");

  for(std::size_t ibag = 0; ibag < bags.size(); ++ibag) {
    if( idElem < countElt + getNumElementByBag(*bags[ibag]) ) {

      for (std::size_t itype = 0; itype < bags[ibag]->size(); ++itype) {
        eltType  = (*bags[ibag])[itype];
        idByType = 0;

        if( idElem < countElt + m_numElements[eltType] ) {
          for (auto itRef = intRefToBegEndMaps[eltType].begin(); itRef != intRefToBegEndMaps[eltType].end(); ++itRef) {
            numEltPerLabel = itRef->second.second;

            if( idElem < countElt + numEltPerLabel ) {
              idByType += idElem - countElt;
              countElt  = idElem;

              return;
            } else {
              idByType += numEltPerLabel;
              countElt += numEltPerLabel;
            }
          }
        } else {
          idByType += m_numElements[eltType];
          countElt += m_numElements[eltType];
        }
      }
    } else {
      countElt += getNumElementByBag(*bags[ibag]);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getIdElemFromTypeElemAndIdByType(const ElementType eltType, const felInt iElByType, felInt& idElt) const
{
  ElementType eltTypeTest;

  std::array<const std::vector<ElementType>*,2> bags{ &m_bagElementTypeDomain, &m_bagElementTypeDomainBoundary };

  // first, look in the bagDomain for the element type, then look in the bagDomainBoundary for the element type
  idElt = 0;
  for(std::size_t ibag = 0; ibag < bags.size(); ++ibag) {
    for (std::size_t ieltype = 0; ieltype < bags[ibag]->size(); ++ieltype) {
      eltTypeTest = (*bags[ibag])[ieltype];

      if ( eltTypeTest == eltType ) {
        idElt += iElByType;
        return;
      } else {
        idElt += numElements(eltTypeTest);
      }
    }
  }

  // if we are here, it means that this element type is not known
  FEL_ERROR("GeometricMeshRegion: We did not find the type " + eltEnumToFelNameGeoEle[eltType].first + " in the mesh!");
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getElementFromBdElem(const std::vector<felInt>& facePts, std::vector<felInt>& elemIdPointVol,
                                               std::vector< Point*>& elemPointVol, int& idLocFace, felInt& idElemVol)
{
  ElementType eltType;
  int nVpEl;

  switch ( facePts.size() ) {
    
    case 3:
    case 4: {

      Face foundFace;
      getFace(facePts, foundFace);

      FEL_ASSERT( ( foundFace.listNeighVolumes().size() ) == 1 ); // face on boundary!

      idElemVol = ( foundFace.listNeighVolumes()[0] )->idVolume();
      idLocFace = ( foundFace.listNeighVolumes()[0] )->idLocalFace();

      eltType = static_cast<ElementType>( ( foundFace.listNeighVolumes()[0] )->typeVolume() );

      nVpEl = m_numPointsPerElt[eltType];
      elemIdPointVol.resize(nVpEl, 0);
      elemPointVol.resize(nVpEl, nullptr);
      getOneElement( eltType, idElemVol, elemIdPointVol, elemPointVol, 0);

      break;
    }

    case 2: {

      Edge foundEdge;
      getEdge(facePts, foundEdge);
      
      FEL_ASSERT( ( foundEdge.listNeighFacesOfEdges().size() ) == 1 ); // Edge on boundary!
      
      idElemVol = ( foundEdge.listNeighFacesOfEdges()[0] )->idFace();
      idLocFace = ( foundEdge.listNeighFacesOfEdges()[0] )->idLocalEdge();
      
      eltType = static_cast<ElementType>( ( foundEdge.listNeighFacesOfEdges()[0] )->typeFace() );

      nVpEl = m_numPointsPerElt[eltType];
      elemIdPointVol.resize(nVpEl, 0);
      elemPointVol.resize(nVpEl, nullptr);
      getOneElement( eltType, idElemVol, elemIdPointVol, elemPointVol, 0);
      
      break;
    }

    default:
      FEL_ERROR("GeometricMeshRegion: wrong number of poins!");
  }
}

/***********************************************************************************/
/***********************************************************************************/

bool GeometricMeshRegion::getAdjElement(ElementType& eltType, felInt& idElem, int localIndexOfEdgeOrFace) const
{
  ElementType eltTypeTmp;
  felInt      idElemTmp;

  const GeoElement* geoEle = eltEnumToFelNameGeoEle[eltType].second;
  const auto domain_dim = domainDim();

  // TODO is really stupid to use two differnt names (i.e. edge and face) for the boundaries of an element...
  // for every dimension we must write exactly the same thing twice...

  if ( domain_dim == 2 ) {

    std::vector<felInt> edges(geoEle->shape().numEdge(),0);

    FEL_ASSERT( localIndexOfEdgeOrFace <= geoEle->shape().numEdge() );

    getAllEdgeOfElement(eltType, idElem, edges);

    auto neighFaces = m_listEdges.list()[edges[localIndexOfEdgeOrFace]]->listNeighFacesOfEdges();
    std::size_t numNeigh = neighFaces.size();

    // case when going out of the domain
    if ( numNeigh < 2 )
      return false;

    for(std::size_t iNeigh = 0; iNeigh < numNeigh; ++iNeigh) {
      eltTypeTmp = static_cast<ElementType>( neighFaces[iNeigh]->typeFace() );
      idElemTmp  = neighFaces[iNeigh]->idFace();

      // found new element (not the element itself)
      if ( eltTypeTmp != eltType || idElemTmp != idElem ) { 
        eltType = eltTypeTmp;
        idElem  = idElemTmp;

        return true;
      }
    }

    return false;
  } else if ( domain_dim == 3 ) {
    
    std::vector<felInt> faces(geoEle->shape().numFace(),0);
    
    FEL_ASSERT( localIndexOfEdgeOrFace <= geoEle->shape().numFace() );
    
    getAllFaceOfElement(eltType, idElem, faces);

    // small std::vector: copy it for legibility...
    auto neighVols = m_listFaces.list()[faces[localIndexOfEdgeOrFace]]->listNeighVolumes();
    std::size_t numNeigh = neighVols.size();

    // case when going out of the domain
    if ( numNeigh < 2 ) 
      return false;

    for(std::size_t iNeigh = 0; iNeigh < numNeigh; ++iNeigh) {
      eltTypeTmp = static_cast<ElementType>( neighVols[iNeigh]->typeVolume() );
      idElemTmp  = neighVols[iNeigh]->idVolume();

      // found new element (not the element itself)
      if ( eltTypeTmp != eltType || idElemTmp != idElem ) {
        eltType = eltTypeTmp;
        idElem  = idElemTmp;

        return true;
      }
    }

    return false;
  }

  FEL_ERROR("GeometricMeshRegion: the method should have been already exited at this point");

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

std::pair<bool, GeometricMeshRegion::ElementType> GeometricMeshRegion::getAdjElement(felInt& idElem, int localIndexOfEdgeOrFace) const
{
  ElementType eltType;
  felInt idByType = -1;

  getTypeElemFromIdElem(idElem, eltType, idByType);

  bool isfound = getAdjElement(eltType, idByType, localIndexOfEdgeOrFace);
  idElem = idByType;
  return std::make_pair(isfound, eltType);
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setListNormal(int iNode, Point vecNormal)
{
  m_listNormals[iNode] = vecNormal;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::setListTangent(int idim, int iNode, Point vecTangent)
{
  m_listTangents[idim][iNode] = vecTangent;
}

/***********************************************************************************/
/***********************************************************************************/

felInt GeometricMeshRegion::getNumElementByBag(const std::vector<ElementType>& bagElem) const 
{
  felInt numElem = 0;
  for (auto it_eltype = bagElem.begin(); it_eltype !=  bagElem.end(); ++it_eltype)
    numElem += numElements( *it_eltype );

  return numElem;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getAllEdgeOfElement(const ElementType eltType, const felInt iel, std::vector<felInt>& array) const
{
  const auto& r_edges  = eltEnumToFelNameGeoEle[eltType].second;
  const felInt numEdge = r_edges->numEdge();

  FEL_ASSERT( array.size() == static_cast<std::size_t>(numEdge) );

  if ( !m_builtEdges )
    FEL_ERROR("GeometricMeshRegion: Edges not build!");

  felInt id_pt0 = 0, id_pt1 = 0;
  felInt pt_edge_beg = 0, pt_edge_end = 0;
  std::vector<felInt> the_elem( m_numPointsPerElt[eltType], 0 );

  // Get the ids of the points in the iel-th elem
  getOneElement(eltType, iel, the_elem, 0);

  for (felInt iedge = 0; iedge < numEdge; ++iedge) {

    // the first point in an edge has the smallest id
    id_pt0 = the_elem[ r_edges->pointOfEdge(iedge,0) ];
    id_pt1 = the_elem[ r_edges->pointOfEdge(iedge,1) ];
    if ( id_pt0 < id_pt1 ) {
      pt_edge_beg = id_pt0;
      pt_edge_end = id_pt1;
    } else {
      pt_edge_beg = id_pt1;
      pt_edge_end = id_pt0;
    }

    // find the edge
    array[iedge] = m_listEdges.findEdge(pt_edge_beg, pt_edge_end);

    if( array[iedge] == -1 )
      FEL_ERROR("GeometricMeshRegion: This edge doesn't exist");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getAllEdgeOfElement(const felInt iel, std::vector<felInt>& array) const
{
  ElementType eltType;
  felInt idByType = -1;

  getTypeElemFromIdElem(iel, eltType, idByType);
  getAllEdgeOfElement(eltType, idByType, array);
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getAllFaceOfElement(const ElementType eltType, const felInt iel, std::vector<felInt>& array) const 
{
  const auto& r_faces  = eltEnumToFelNameGeoEle[eltType].second;
  const felInt numFace = r_faces->numFace();

  FEL_ASSERT( array.size() == static_cast<std::size_t>(numFace) );

  if ( !m_builtFaces )
    FEL_ERROR("GeometricMeshRegion: Faces not build!");

  std::vector<felInt> the_elem( m_numPointsPerElt[eltType], 0);
  std::vector<felInt> ptOfFace;

  // Get the ids of the points in the iel-th elem
  getOneElement(eltType, iel, the_elem, 0);
  for (felInt iface = 0; iface < numFace; ++iface) {
    ptOfFace.resize( eltEnumToFelNameGeoEle[eltType].second->numPointPerFace( iface ), 0);

    for (std::size_t iptface = 0; iptface < ptOfFace.size(); iptface++ )
      ptOfFace[iptface] = the_elem[ r_faces->pointOfFace(iface, iptface) ];

    // look for current face
    array[iface] = m_listFaces.findFace(ptOfFace);
    
    if( array[iface] == -1 )
      FEL_ERROR("GeometricMeshRegion: This face doesn't exist");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getAllFaceOfElement(const felInt iel, std::vector<felInt>& array) const
{
  ElementType eltType;
  felInt idByType = -1;

  getTypeElemFromIdElem(iel, eltType, idByType);
  getAllFaceOfElement(eltType, idByType, array);
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getEdge(const std::vector<felInt>& edgePts, Edge& resultEdge) const
{
  if ( m_builtEdges ) {
    m_listEdges.searchEdge(edgePts, resultEdge);
  } else {
    FEL_ERROR("GeometricMeshRegion: Edges not build!");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getFace(const std::vector<felInt>& facePts, Face& resultFace) const
{
  if ( m_builtFaces ) {
    m_listFaces.searchFace(facePts, resultFace);
  } else {
    FEL_ERROR("GeometricMeshRegion: Faces not build!");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::print(int verbose, std::ostream& outstr) const 
{
  int rank = 0;
  // MPI_Comm_rank(PETSC_COMM_WORLD, &rank);
  
  if ( verbose ) {
    outstr << "[" << rank << "] Dimension: " << m_domainDim << std::endl;
    outstr << "[" << rank << "] Number of points: " << m_listPoints.size() << std::endl;

    // print only the num of elements:
    outstr << "\n[" << rank << "] Domain elements: " << getNumDomainElement() << std::endl;
    printElements(m_bagElementTypeDomain, /*verbose=*/ 1, outstr);
    outstr << "[" << rank << "] Boundary elements: " << getNumBoundaryElement() << std::endl;
    printElements(m_bagElementTypeDomainBoundary, 1, outstr);

    outstr << "[" << rank << "] Domain Map: ref -> <startPos, numElements>\n";
    printRefToBegEnd(m_bagElementTypeDomain, verbose, outstr);
    outstr << "[" << rank << "] Boundary Map: ref -> <startPos, numElements>\n";
    printRefToBegEnd(m_bagElementTypeDomainBoundary, verbose, outstr);
  }

  // print all vertex/elem info.
  if ( verbose > 28 ) { 
    printVertices(outstr);

    outstr << "[" << rank << "] List of domain elements:\n";
    printElements(m_bagElementTypeDomain, verbose, outstr, false);
    outstr << "[" << rank << "] List of boundary elements:\n";
    printElements(m_bagElementTypeDomainBoundary, verbose, outstr, false);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::printVertices(std::ostream& outstr) const 
{
  int rank = 0;
  // MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  outstr << "[" << rank << "] List of Points: " << m_listPoints.size() << "\n";
  outstr << "[" << rank << "] ";

  for (auto& r_point : m_listPoints)
    r_point.print(1, outstr);

  outstr << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::printElements(const std::vector<ElementType>& bagElem, const int verbose, 
                                        std::ostream& outstr, const bool incrementPointID) const 
{
  int rank = 0;
  MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  std::vector<felInt> elem;
  for (auto it_eltype = bagElem.begin(); it_eltype != bagElem.end(); ++it_eltype) {
    ElementType eltType = static_cast<ElementType>(*it_eltype);

    outstr << "[" << rank << "] " << eltEnumToFelNameGeoEle[eltType].first << ": " << m_numElements[eltType] << std::endl;
    
    // print all elem info.
    if (verbose > 28) {
      int nVpEl = m_numPointsPerElt[eltType];
      elem.resize(nVpEl, 0);

      for ( felInt iel = 0; iel < m_numElements[eltType]; ++iel) {
        getOneElement(eltType, iel, elem, 0, incrementPointID);

        outstr << "[" << rank << "] ";
        for (int iv = 0; iv < nVpEl; ++iv)
          outstr << elem[iv] << " ";
        outstr << "\n";
      }
    }

    outstr << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::printRefToBegEnd(const std::vector<ElementType>& bagElem, const int verbose, std::ostream& outstr) const 
{
  int rank = 0;
  // MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

  std::vector<felInt> elem;
  for (auto it_eltype = bagElem.begin(); it_eltype != bagElem.end(); ++it_eltype) {
    ElementType eltType = static_cast<ElementType>(*it_eltype);
    
    outstr <<  "[" << rank << "] " << eltEnumToFelNameGeoEle[eltType].first << ": " << intRefToBegEndMaps[eltType].size() << std::endl;
    
    // print all info.
    if (verbose > 10) { 
      int nVpEl = m_numPointsPerElt[eltType];
      elem.resize(nVpEl, 0);

      for (auto it_ref = intRefToBegEndMaps[eltType].begin(); it_ref != intRefToBegEndMaps[eltType].end(); ++it_ref)
        outstr << "[" << rank << "] ref = " << it_ref->first << " \t-->\t < startPos = "
               << it_ref->second.first << " ; numElements = " << it_ref->second.second  << " >\n";
    }

    outstr << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::m_allocateListElementsByRef(const std::vector<ElementType>& theElementBag, GeometricMeshRegion& meshGlobal,
                                                      const std::vector<int>& eltPartition, const int rank, felInt& countEltTot)
{
  for (auto it_eltype = theElementBag.begin(); it_eltype != theElementBag.end(); ++it_eltype) {
    ElementType eltType = static_cast<ElementType>(*it_eltype);

    // Order the elements by reference
    for (auto itRef = meshGlobal.intRefToBegEndMaps[eltType].begin(); itRef != meshGlobal.intRefToBegEndMaps[eltType].end(); ++itRef) {

      felInt numElemPerRefGlob = itRef->second.second;

      for (felInt iel = 0; iel < numElemPerRefGlob; iel++) {
        if (eltPartition[ countEltTot ] == rank )
          m_numElements[eltType]++;

        countEltTot++;
      }
    }

    allocateElements(eltType);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::m_fillListElementsByRef(const std::vector<ElementType>& theElementBag, GeometricMeshRegion& meshGlobal, 
                                                  const std::vector<int>& eltPartition, const int rank, std::vector<felInt>& loc2globElem, felInt& countEltTot)
{
  std::vector<felInt> elem;

  // for each element type in the bag
  for (auto it_eltype = theElementBag.begin(); it_eltype != theElementBag.end(); ++it_eltype) {
    ElementType eltType = static_cast<ElementType>(*it_eltype);
    felInt neTotLoc = 0;
    int nVpEl = m_numPointsPerElt[eltType];

    elem.resize(nVpEl, 0);

    // select the elements in the partition (ordered by reference)
    // for each label in the global mesh for the current element type
    for (auto itRef = meshGlobal.intRefToBegEndMaps[eltType].begin(); itRef != meshGlobal.intRefToBegEndMaps[eltType].end(); ++itRef) {
      const int theRef = itRef->first;
      const felInt numElemPerRefGlobal = itRef->second.second;

      felInt numElemPerRefLocal = 0;
      const felInt posRefElemLocal = neTotLoc*nVpEl;
      const felInt startindex = itRef->second.first;

      // for each element with the current label
      for (felInt iel = 0; iel < numElemPerRefGlobal; iel++) {
        if  ( eltPartition[ countEltTot ] == rank ) {
          // fill the local to global mapping
          loc2globElem.push_back(countEltTot);

          // get the coordinate of the cooresponding global element
          meshGlobal.getOneElement(eltType, iel, elem, startindex);

          // std::set the local element wi
          setOneElement(eltType, neTotLoc, elem, false);

          // increment the number of elements
          neTotLoc++;
          numElemPerRefLocal++;
        }
        countEltTot++;
      }

      // Update intRefToBegEndMapsb
      if ( numElemPerRefLocal != 0 ) {
        intRefToBegEndMaps[eltType][theRef] = std::make_pair(posRefElemLocal, numElemPerRefLocal);
        m_intRefToEnum[ theRef ].insert( eltType );
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::m_buildEdgesPerBag(const std::vector<ElementType>& theBagElt, int theDim)
{
  felInt pt_edge_beg = 0, pt_edge_end = 0;
  felInt id_pt0 = 0, id_pt1 = 0;

  for (auto it_elttype = theBagElt.begin(); it_elttype != theBagElt.end(); ++it_elttype) {
    ElementType eltType = (ElementType)*it_elttype;
    std::vector<felInt> the_elem( m_numPointsPerElt[eltType] , m_listEdges.m_null_int );

    for ( felInt iel = 0; iel < m_numElements[eltType]; iel++ ) {
      getOneElement(eltType, iel, the_elem, 0);

      for (felInt iedge = 0; iedge < eltEnumToFelNameGeoEle[eltType].second->numEdge(); iedge ++ ) {
        NeighFacesOfEdges::Pointer the_FaceNeighbour = nullptr;
        NeighVolumesOfEdges::Pointer the_VolumeNeighbour = nullptr;
        
        switch (theDim) {

          case 1:
            break;

          case 2:
            the_FaceNeighbour = felisce::make_shared<NeighFacesOfEdges>();
            the_FaceNeighbour->idFace()      = iel;
            the_FaceNeighbour->typeFace()    = eltType;
            the_FaceNeighbour->idLocalEdge() = iedge;
            break;

          case 3:
            the_VolumeNeighbour = felisce::make_shared<NeighVolumesOfEdges>();
            the_VolumeNeighbour->idVolume()    = iel;
            the_VolumeNeighbour->typeVolume()  = eltType;
            the_VolumeNeighbour->idLocalEdge() = iedge;
            break;

          default:
            FEL_ERROR( "ERROR: dimension for edge building should be 1, 2 or 3. Exiting." );
        }

        id_pt0 = the_elem[ eltEnumToFelNameGeoEle[eltType].second->pointOfEdge(iedge, 0) ];
        id_pt1 = the_elem[ eltEnumToFelNameGeoEle[eltType].second->pointOfEdge(iedge, 1) ];
        
        if ( id_pt0 < id_pt1 ) {
          pt_edge_beg = id_pt0;
          pt_edge_end = id_pt1;
        } else {
          pt_edge_beg = id_pt1;
          pt_edge_end = id_pt0;
        }

        m_listEdges.searchAddEdge(pt_edge_beg,  pt_edge_end,
                                  the_FaceNeighbour, the_VolumeNeighbour);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::m_buildFacesPerBag(const std::vector<ElementType>& theBagElt, int theDim)
{
  bool check_surface_mesh_flag = false;

  for (auto it_elttype = theBagElt.begin(); it_elttype != theBagElt.end(); ++it_elttype) {
    ElementType eltType = static_cast<ElementType>(*it_elttype);
    std::vector <felInt> the_elem( m_numPointsPerElt[eltType] , m_listFaces.m_null_int );
    std::vector <felInt> ptOfFace;

    for ( felInt iel = 0; iel < m_numElements[eltType]; iel++ ) {
      getOneElement(eltType, iel, the_elem, 0);
      
      for (felInt iface = 0; iface < eltEnumToFelNameGeoEle[eltType].second->numFace(); iface ++) {
        NeighVolumesOfFaces* the_VolumeNeighbour = nullptr;
        
        switch (theDim) {

          case 2:
            break;

          case 3:
            the_VolumeNeighbour = new NeighVolumesOfFaces;
            the_VolumeNeighbour->idVolume()    = iel;
            the_VolumeNeighbour->typeVolume()  = eltType;
            the_VolumeNeighbour->idLocalFace() = iface;
            break;

          default:
            FEL_ERROR( "ERROR: dimension for face building should be 2 or 3. Exiting." );
        }

        //! a priori: for each element, a face can switch to quadrangle or triangle (think of prisms).
        int numPointPerFace = eltEnumToFelNameGeoEle[eltType].second->numPointPerFace( iface ); //std::cout <<" test face " << std::endl;
        ptOfFace.resize( numPointPerFace, 0);
        
        // global point numbering of the local face points:
        for (int iptface = 0; iptface < numPointPerFace; iptface ++) {
          ptOfFace[iptface] = the_elem[ eltEnumToFelNameGeoEle[eltType].second->pointOfFace(iface, iptface) ];
        }
        m_listFaces.searchAddFace(ptOfFace, the_VolumeNeighbour, check_surface_mesh_flag);
      }
    }
  }
}
































/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::extractVerticesFromBoundary(const std::vector<int>& label, std::set<felInt>& list)
{
  int currentLabel;
  std::vector<felInt> elmList;

  for (std::size_t iEltType = 0; iEltType < bagElementTypeDomainBoundary().size(); ++iEltType) {
    ElementType eltType = bagElementTypeDomainBoundary()[iEltType];

    auto& listRefPerType = intRefToBegEndMaps[eltType];
    for (auto itRef = listRefPerType.begin(); itRef != listRefPerType.end(); ++itRef) {
      currentLabel = itRef->first;

      // Skip boundaries not included in label
      if ( std::find(label.begin(), label.end(), currentLabel) == label.end() )
        continue;
      
      // Get elements and fill the set with points
      getElementsPerRef(eltType, currentLabel, elmList);

      // Store ids
      list.insert(elmList.begin(), elmList.end());
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/
// TODO this function should be split into two different functions 
// 1) compute local coordinates
// 2) isPointInElement
bool GeometricMeshRegion::findLocalCoord(ElementType& ityp, felInt& iel, const Point& point, Point& pointLoc)
{
  const GeoElement* geoEl = eltEnumToFelNameGeoEle[ityp].second;
  // const int        nbrVer = m_numPointsPerElt[ityp];
  const int        nbrVer = geoEl->numPoint();

  std::vector<felInt> veridxOfElt( nbrVer, 0);
  std::vector<Point*> pointsOfElt( nbrVer, nullptr);

  getOneElement(ityp, iel, veridxOfElt, pointsOfElt, 0);

  // Point localPt(0.0);
  pointLoc.clear();

  UBlasVector RHS(m_numCoor);
  UBlasVector X(m_numCoor);
  UBlasMatrix Jac(m_numCoor, m_numCoor);
  UBlasMatrix invJac(m_numCoor, m_numCoor);
  double det;

  // Determines the local coordinate
  std::size_t iter  = 0;
  double      resid = NWTOLL + 1.;
  while ( resid > NWTOLL && iter < NMAX ) {

    // Assemble matrix and RHS
    Jac.clear();
    for (int iDim = 0; iDim < m_numCoor; ++iDim) {
      RHS(iDim) = - point.coor(iDim);

      for(int iPoint = 0; iPoint < nbrVer; ++iPoint) {
        RHS(iDim) += geoEl->basisFunction().phi(  iPoint, pointLoc) * pointsOfElt[iPoint]->coor(iDim);
        
        for (int jDim = 0; jDim < m_numCoor; ++jDim)
          Jac(iDim,jDim) -= geoEl->basisFunction().dPhi( iPoint, jDim, pointLoc) * pointsOfElt[iPoint]->coor(iDim);
      }
    }

    MathUtilities::InvertMatrix(Jac, invJac, det);

    noalias(X) = prod(invJac, RHS);

    resid = 0.0;
    for(int iCoor = 0; iCoor < m_numCoor; ++iCoor) {
      resid += std::pow( X[iCoor], 2);
      pointLoc.coor(iCoor) += X[iCoor];
    }

    iter++;
  }

  if ( iter >= NMAX ) {
    fprintf(stderr,"WARNING: maximum number of iterations %d reached for element %d of type...",static_cast<int>(NMAX),iel);
  }

  /*
    To access at the edge defined by the 2 vertices (i,j):
    - go to the i-th index in the list of pointer
    - browse the chained list of edges where 1st vertex is i
    */

  // Check if point is interior or not
  felInt tmpNeigh;
  ElementType tmptyp;

  TypeShape elShape = static_cast<TypeShape>( geoEl->shape().typeShape() );
  std::vector<felInt> faces(0,0);
  std::vector<felInt> edges(0,0);

  if ( m_numCoor == 2 ) {
    edges.resize( geoEl->shape().numEdge(), 0);
    getAllEdgeOfElement(ityp, iel, edges);
  }
  else if ( m_numCoor == 3 ) {
    faces.resize( geoEl->shape().numFace(), 0);
    getAllFaceOfElement(ityp, iel, faces);
  }

  switch (elShape) {

    case Segment:

      if ( pointLoc[0] < - 1 - GeoUtilities::GEO_TOL )
        return false;

      if ( pointLoc[0] > + 1 + GeoUtilities::GEO_TOL )
        return false;

      return true;

      break;

    case Triangle:

      // edge 0 ->adjacency
      if ( pointLoc[1] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[0]]->listNeighFacesOfEdges().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[0]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[0]]->listNeighFacesOfEdges()[i]->idFace();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // edge 2 ->adjacency
      if ( pointLoc[0] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[2]]->listNeighFacesOfEdges().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[2]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[2]]->listNeighFacesOfEdges()[i]->idFace();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // edge 1 ->adjacency
      if ( 1 - pointLoc[0] - pointLoc[1] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[1]]->listNeighFacesOfEdges().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[1]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[1]]->listNeighFacesOfEdges()[i]->idFace();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      return true;

      break;

    case Quadrilateral:

      // edge 3 ->adjacency
      if ( pointLoc[0] < - 1 - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[3]]->listNeighFacesOfEdges().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[3]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[3]]->listNeighFacesOfEdges()[i]->idFace();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // edge 1 ->adjacency
      if ( pointLoc[0] > + 1 + GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[1]]->listNeighFacesOfEdges().size();
        for (std::size_t i=0; i<numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[1]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[1]]->listNeighFacesOfEdges()[i]->idFace();

          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // edge 0 ->adjacency
      if ( pointLoc[1] < - 1 - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[0]]->listNeighFacesOfEdges().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[0]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[0]]->listNeighFacesOfEdges()[i]->idFace();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // edge 2 ->adjacency
      if ( pointLoc[1] > + 1 + GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listEdges.list()[edges[2]]->listNeighFacesOfEdges().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listEdges.list()[edges[2]]->listNeighFacesOfEdges()[i]->typeFace());
          tmpNeigh = m_listEdges.list()[edges[2]]->listNeighFacesOfEdges()[i]->idFace(); 

          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      return true;

      break;

    case Tetrahedron:

      // face 0 ->adjacency
      if ( pointLoc[2] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[0]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[0]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[0]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 1 ->adjacency
      if ( pointLoc[1] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[1]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[1]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[1]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 3 ->adjacency
      if ( pointLoc[0] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[3]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[3]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[3]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 2 ->adjacency
      if ( 1 - pointLoc[0] - pointLoc[1] - pointLoc[2] < - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[2]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[2]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[2]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      return true;

      break;

    case Hexahedron:

      // face 0 ->adjacency
      if ( pointLoc[2] < - 1 - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[0]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[0]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[0]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 3->adjacency
      if ( pointLoc[2] > + 1 + GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[3]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[3]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[3]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 2 ->adjacency
      if ( pointLoc[1] > + 1 + GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[2]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[2]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[2]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 5 ->adjacency
      if ( pointLoc[1] < - 1 - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[5]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[5]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[5]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 1 ->adjacency
      if ( pointLoc[0] < - 1 - GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[1]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[1]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[1]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      // face 4 ->adjacency
      if ( pointLoc[0] > + 1 + GeoUtilities::GEO_TOL ) {

        std::size_t numNeigh = m_listFaces.list()[faces[4]]->listNeighVolumes().size();
        for (std::size_t i = 0; i < numNeigh; ++i) {
          tmptyp   = static_cast<ElementType>(m_listFaces.list()[faces[4]]->listNeighVolumes()[i]->typeVolume());
          tmpNeigh = m_listFaces.list()[faces[4]]->listNeighVolumes()[i]->idVolume();
          
          if ( tmptyp != ityp || tmpNeigh != iel ) {
            ityp = tmptyp;
            iel  = tmpNeigh;
            break;
          }
        }

        return false;
      }

      return true;

      break;

    case NullShape:
    case Node:
    case Prism:
    case Pyramid: {
      FEL_ERROR("unknown (or not implemented) element Shape")
      break;
    }
  }

  ityp = ELEMENT_TYPE_COUNTER;
  iel  = -1;

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool GeometricMeshRegion::isPointInTetra(const felInt iel, const Point& p, std::array<double, 4>& bary, const double tol)
{
  std::vector<felInt> verOfTetra(4, -1);
  std::vector<Point*> pntOfTetra(4, nullptr);

  // Get points of the tetrahedra
  getOneElement(felisce::GeometricMeshRegion::Tetra4, iel, verOfTetra, pntOfTetra, 0);

  // Check if the point is inside
  return GeoUtilities::isPointInTetra(pntOfTetra, p, bary, tol);
}

/***********************************************************************************/
/***********************************************************************************/

bool GeometricMeshRegion::isPointInTri(const felInt iel, const Point& p, std::array<double, 3>& bary, const double tol)
{
  std::vector<felInt> verOfTri(3, -1);
  std::vector<Point*> pntOfTri(3, nullptr);

  // Get nodes of the triangle
  getOneElement(felisce::GeometricMeshRegion::Tria3, iel, verOfTri, pntOfTri, 0);

  // Check if the point is inside
  return GeoUtilities::isPointInTri(pntOfTri, p, bary, tol);
}

/***********************************************************************************/
/***********************************************************************************/

bool GeometricMeshRegion::isPointInSeg(const felInt iel, const Point& p, std::array<double, 2>& bary, const double tol)
{
  std::vector<felInt> verOfSeg(2, -1);
  std::vector<Point*> pntOfSeg(2, nullptr);

  // Get nodes of the segment
  getOneElement(felisce::GeometricMeshRegion::Seg2, iel, verOfSeg, pntOfSeg, 0);

  // Check if the point is inside
  return GeoUtilities::isPointInSeg(pntOfSeg, p, bary, tol);
}

/***********************************************************************************/
/***********************************************************************************/

bool GeometricMeshRegion::isVertexOfElt(const felInt idxVer, const ElementType eltTyp, const felInt eltIdx, felInt &verPos) const
{
  std::vector<felInt> eltIdxVer(m_numPointsPerElt[eltTyp]);

  getOneElement(eltTyp, eltIdx, eltIdxVer, 0, 0);

  for(felInt i = 0; i < static_cast<felInt>(eltIdxVer.size()); ++i) {
    if ( eltIdxVer[i] == idxVer ) {
      verPos = i;
      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool GeometricMeshRegion::isEdgeOfElt(const std::vector<felInt>& idxVer, const ElementType eltTyp, const felInt eltIdx, felInt &verPos) const
{
  std::array<felInt,2> edgVer;
  std::vector<felInt> eltIdxVer(m_numPointsPerElt[eltTyp]);
  auto& mapEdgToVer = m_eltTypMapEdgToVer[eltTyp];

  getOneElement(eltTyp, eltIdx, eltIdxVer, 0, 0);

  for(felInt i = 0; i < static_cast<felInt>(mapEdgToVer.size()); ++i) {
    edgVer[0] = eltIdxVer[mapEdgToVer[i][0]];
    edgVer[1] = eltIdxVer[mapEdgToVer[i][1]];

    if ( std::is_permutation(edgVer.begin(), edgVer.end(), idxVer.begin()) ) {
      verPos = i;
      return true;
    }
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getVertexBall(const felInt idxVer, ElementType eltTyp, const felInt eltGrm, BallMap& ball) const 
{
  ElementType ngbTyp;
  felInt eltIdx, ngbIdx;
  felInt verPos, ngbPos;

  std::set<std::pair<ElementType, felInt>> mark;
  
  ball.clear();

  // Get position of vertex in element and insert in ball
  if ( isVertexOfElt(idxVer, eltTyp, eltGrm, verPos) ) { // TODO warning here this methos should work with shapes and not with geoelements
    ball.push_back( {eltTyp, eltGrm, verPos} );
    mark.insert( {eltTyp, eltGrm} );
  }
  else {
    FEL_ERROR("Intersection: Provided vertex is not in element!");
  }

  // Loop over the neighbors to set the ball
  for (std::size_t iel = 0; iel < ball.size(); ++iel) {
    std::tie(eltTyp, eltIdx, verPos) = ball[iel];

    // Loop over edges/faces/volumes sharing that vertex
    auto& verToFac = m_eltTypMapVerToFac[eltTyp][verPos]; 
    for (std::size_t ifa = 0; ifa < verToFac.size(); ++ifa) {

      // For every edge/face/volume surrounding the vertex, add neighbor element in ball 
      ngbTyp = eltTyp;
      ngbIdx = eltIdx;
      if ( !getAdjElement(ngbTyp, ngbIdx, verToFac[ifa]) )
        continue;

      if ( !(mark.insert({ngbTyp, ngbIdx})).second )
        continue;

      // Get local position of the edge/face/volume
      isVertexOfElt(idxVer, ngbTyp, ngbIdx, ngbPos);

      // Insert new element in ball
      ball.push_back( {ngbTyp, ngbIdx, ngbPos} );
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getEdgeShell(const std::vector<felInt>& idxVer, ElementType eltTyp, const felInt eltGrm, BallMap& shell) const 
{
  ElementType ngbTyp;
  felInt eltIdx, ngbIdx;
  felInt edgPos, ngbPos;

  std::set<std::pair<ElementType, felInt>> mark;
  
  shell.clear();

  // Get position of edge in element and insert in shell
  if ( isEdgeOfElt(idxVer, eltTyp, eltGrm, edgPos) ) { // TODO warning here this methos should work with shapes and not with geoelements
    shell.push_back( {eltTyp, eltGrm, edgPos} );
    mark.insert( {eltTyp, eltGrm} );
  }
  else {
    FEL_ERROR("Intersection: Provided vertex is not in element!");
  }

  // Loop over the neighbors to set the shell
  for (std::size_t iel = 0; iel < shell.size(); ++iel) {
    std::tie(eltTyp, eltIdx, edgPos) = shell[iel];

    // Loop over edges/faces/volumes sharing that vertex
    auto& edgToFac = m_eltTypMapEdgToFac[eltTyp][edgPos]; 
    for (std::size_t ifa = 0; ifa < edgToFac.size(); ++ifa) {

      // For every edge/face/volume surrounding the vertex, add neighbor element in shell 
      ngbTyp = eltTyp;
      ngbIdx = eltIdx;
      if ( !getAdjElement(ngbTyp, ngbIdx, edgToFac[ifa]) )
        continue;

      if ( !(mark.insert({ngbTyp, ngbIdx})).second )
        continue;

      // Get local position of the edge/face/volume
      isEdgeOfElt(idxVer, ngbTyp, ngbIdx, ngbPos);

      // Insert new element in shell
      shell.push_back( {ngbTyp, ngbIdx, ngbPos} );
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getElementPatch(const ElementType typGrm, const felInt eltGrm, std::set<seedElement>& elementsInPatch) const
{
  BallMap ball;
  ElementType eltTyp;
  felInt eltIdx, verPos;
  std::vector<felInt> veridxOfElt(GeometricMeshRegion::m_numPointsPerElt[typGrm], 0); 

  // Clean vector
  elementsInPatch.clear();

  // Get element vertices
  getOneElement(typGrm, eltGrm, veridxOfElt, 0);

  // Loop over the shape vertices 
  const int numVer = eltEnumToFelNameGeoEle[typGrm].second->shape().numVertex();
  for (int ive = 0; ive < numVer; ++ive) {

    // Get ball
    getVertexBall(veridxOfElt[ive], typGrm, eltGrm, ball); 

    // Save elements
    for (std::size_t iel = 0; iel < ball.size(); ++iel) {
      std::tie(eltTyp, eltIdx, verPos) = ball[iel];

      // Insert elements in vector
      elementsInPatch.insert( {eltTyp, eltIdx} );
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getElementDeepPatch(std::set<seedElement>& elementsInPatch, std::size_t patch_level) const
{
  std::queue<seedElement>  list_elt;
  std::vector<seedElement> list_next_elt;
  std::set<seedElement> localPatch;

  // Initializa list_next_elt
  for (const seedElement& elt : elementsInPatch)
    list_next_elt.emplace_back(elt);

  // Clean-up
  elementsInPatch.clear();

  // Loop
  while ( patch_level > 0 ) {

    // Add element to elementsInPatch (removing duplicates)
    for (const auto& elt : list_next_elt) {
      auto it_patch = elementsInPatch.insert(elt);
      
      // Initialize current list of elements
      if ( it_patch.second )
        list_elt.emplace(elt);
    }

    // Clean next list of element to process
    list_next_elt.clear();

    // While there are elements to check
    while ( !list_elt.empty() ) {

      // Clean local patch 
      localPatch.clear();

      // Get element and deepness id
      seedElement& curr_elt = list_elt.front();

      // Get the patch of the current element
      getElementPatch(curr_elt.first, curr_elt.second, localPatch);

      // Remove current element from localPatch
      localPatch.erase(curr_elt);

      // Copy element in local patch to next list of element to process 
      for (const auto& elt : localPatch)
        list_next_elt.emplace_back(elt);

      // Remove first element from list
      list_elt.pop();
    }

    // Reorder and remove duplicated elements
    std::sort( list_next_elt.begin(), list_next_elt.end() ); 
    list_next_elt.erase( std::unique( list_next_elt.begin(), list_next_elt.end() ), list_next_elt.end() );

    // Decrease deep level
    patch_level--;
  }

  // Add element to elementsInPatch (removing duplicates)
  for (const auto& elt : list_next_elt)
    elementsInPatch.insert(elt);
}

/***********************************************************************************/
/***********************************************************************************/

std::size_t GeometricMeshRegion::computeConnectedComponents(std::map<ElementType,std::vector<std::size_t>>& mapCco)
{
  // Get the array to use to know the number of neighbor TODO D.C. this should be done using polymorphism
  const int* numNeigh = nullptr;
  if ( m_domainDim == GeoMesh2D ) {
    numNeigh = m_numEdgesPerElt;
    buildEdges();
  }
  else if ( m_domainDim == GeoMesh3D ) {
    numNeigh = m_numFacesPerElt;
    buildFaces();
  }
  else {
    FEL_ERROR("Not implemented for this case");
  }

  std::queue<std::pair<ElementType, felInt>> queue;
  ElementType eltTyp, neiTyp;
  felInt prvGlbIdx = 0;
  felInt eltIdx, neiIdx;
  felInt nbrElt, glbNbrElt = 0;

  // Global number of elements and initialize mapCco
  for (std::size_t i = 0; i < m_bagElementTypeDomain.size(); ++i) {
    eltTyp = m_bagElementTypeDomain[i];
    nbrElt = m_numElements[eltTyp];

    if ( nbrElt > 0 )
      mapCco[eltTyp].assign(nbrElt,0);

    glbNbrElt += nbrElt;
  }

  // Set counter to zero
  felInt cnt = 0;
  // Set connected component progressive idx to 0
  std::size_t ccoIdx = 0;

  // Find all connected components
  NextCco:
  ccoIdx++;

  // Find connected component germ
  for(felInt eltGlbIdx = prvGlbIdx; eltGlbIdx < glbNbrElt; ++eltGlbIdx) {

    // Get element type and id by element
    getTypeElemFromIdElem(eltGlbIdx, eltTyp, eltIdx);

    // If element not marked yet
    if ( mapCco[eltTyp][eltIdx] == 0 ) {

      // Update
      prvGlbIdx = eltGlbIdx+1;

      // Insert in the queue
      queue.emplace(std::make_pair(eltTyp, eltIdx));

      // Set connected component
      mapCco[eltTyp][eltIdx] = ccoIdx;

      // Increase counter
      cnt++;
   
      break; 
    }
  }

  // Color the current connected component
  while ( cnt < glbNbrElt ) { 

    // Get front element 
    eltTyp = queue.front().first;
    eltIdx = queue.front().second;

    //- For every neighbor of iElt:
    for(int ine = 0; ine < numNeigh[eltTyp]; ++ine) {

      // Get the neighbor element if it exists
      neiTyp = eltTyp;
      neiIdx = eltIdx;
      if ( !getAdjElement(neiTyp, neiIdx, ine) )
        continue;

      //- If the neighbor exists
      if ( mapCco[neiTyp][neiIdx] == 0 ) {
        // Insert in the queue
        queue.emplace(std::make_pair(neiTyp, neiIdx));
        // Set connected component
        mapCco[neiTyp][neiIdx] = ccoIdx;
        // Increase counter
        cnt++;
      }
    }

    // Remove first element from list
    queue.pop();

    // Check if we found all the connected component elements
    if ( queue.empty() )
      goto NextCco;
  }

  //- Check if all elements have been marked
  FEL_CHECK( cnt == glbNbrElt, "Problem detecting connected components! ");

  // Return number of connected components
  return ccoIdx;
}

/***********************************************************************************/
/***********************************************************************************/

void GeometricMeshRegion::getElementsInRegion(seedElement germ_elt, std::set<seedElement>& set_elt) const
{
  seedElement cur_elt, nei_elt;
  std::queue<seedElement> queue;

  // Get the array to use to know the number of neighbor TODO D.C. this should be done using polymorphism
  const int* numNeigh = nullptr;
  if ( m_domainDim == GeoMesh2D )
    numNeigh = m_numEdgesPerElt;
  else if ( m_domainDim == GeoMesh3D )
    numNeigh = m_numFacesPerElt;
  else
    FEL_ERROR("Not implemented for this case");

  // Add germ element in set
  set_elt.insert(germ_elt);

  // Initialize list of element to analyze
  queue.emplace(germ_elt);

  // Get elements in region
  while ( !queue.empty() ) { 

    // Get front element 
    cur_elt = queue.front();

    // For every neighbor of iElt:
    for(int ine = 0; ine < numNeigh[cur_elt.first]; ++ine) {

      // Get the neighbor element if it exists
      nei_elt = cur_elt;

      // If the neighbor doesn't exist
      if ( !getAdjElement(nei_elt.first, nei_elt.second, ine) )
        continue;

      // Insert in set
      auto res = set_elt.insert(nei_elt);

      // If the neibor element was not in set_elt insert in the queue
      if ( res.second )
        queue.emplace(nei_elt);
    }

    // Remove first element from list
    queue.pop();
  }
}

}
