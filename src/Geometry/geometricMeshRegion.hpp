//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon & V.Martin
//

#ifndef GEOMETRICMESHREGION_HPP
#define GEOMETRICMESHREGION_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Geometry/listEdges.hpp"
#include "Geometry/listFaces.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class GeoElement;
class GeometricMeshRegion 
{
  public:
    ///@name Enum Definitions
    ///@{

    /*****************************************
     /
     /   This part contains the number of types of element,
     /   number of vertices per element, and maps from
     /   name felisce to keyword (medit, ensight, geometric element, ...)
     /   used in felisce
     /
     /   Mapping between an int and the type of element
     /   DO NOT USE THE INT as new types of element could be added!
     /   <0,Nod>
     /   <1,Seg2> <2,Seg3b> <3,Seg3>
     /   <4,Tria3> <5,Tria4>  <6,Tria6>
     /   <7,Quad4> <8,Quad5> <9,Quad8> <10,Quad8> <11,Quad9>
     /   <12,Tetra4> <13,Tetra5> <14,Tetra10>
     /   <15,Pyram5> <16,Pyram13>
     /   <17,Prism6> <18,Prism15>
     /   <19,Hexa8> <20,Hexa9> <21,Hexa20> <22,Hexa26> <23,Hexa27>
     /
     /  Note: a Pyram is a pentahedron whose faces are 4 triangles and 1 quadrangle.
     /        a Prism is a pentahedron whose faces are 2 triangles and 3 quadrangles.
     /
     /   Note: To add a new element:
     /   1/ add the element type in ElementType enum.
     /   2/ set its number of points in m_numPointsPerElt, at the right place!
     /   3/ fill in the dictionnary eltFelNameToEnum,...
     /   4/ fill the corresponding std::unordered_map in the ensight classe (m_eltFelNameToEns6Pair)
     /
     *****************************************/
    enum ElementType: std::size_t {
      Nod,
      Seg2, Seg3b, Seg3,
      Tria3, Tria4, Tria6,
      Quad4, Quad5, Quad6, Quad8, Quad9,
      Tetra4, Tetra5, Tetra10,
      Pyram5, Pyram13,
      Prism6, Prism9, Prism15,
      Hexa8, Hexa9, Hexa12, Hexa20, Hexa26, Hexa27,
      ELEMENT_TYPE_COUNTER
    };

    enum class InitialNormalTangentProvided {
      NONE,
      NORMAL,
      TANGENT_1,
      TANGENT_2
    };

    enum DomainDimensionType {
      GeoMeshUndefined = -1,
      GeoMesh0D        =  0,
      GeoMesh1D        =  1,
      GeoMesh2D        =  2,
      GeoMesh3D        =  3
    };

    //Manage Input/Output Format Type
    enum FormatType {
      FormatUndefined = -1,
      FormatMedit     =  0,
      FormatEnsight6  =  1
    };

    ///@}
    ///@name Type Definitions
    ///@{

    /// Pointer definition of GeometricMeshRegion
    FELISCE_CLASS_POINTER_DEFINITION(GeometricMeshRegion);

    typedef std::pair<std::string , const GeoElement* >     FelNameGeoElePair_type;
    typedef std::vector<FelNameGeoElePair_type >            EnumToFelNameGeoEle_type;
    typedef std::unordered_map<std::string, ElementType >   StringToElementType_type;
    typedef std::unordered_map<ElementType, ElementType >   ElementTypeLinearToDifferentElementType_type;
    typedef std::map<int, std::set<ElementType> >           IntRefToElementType_type;
    typedef std::unordered_map<std::string, std::set<int> > NameLabelToListLabel_type;

    typedef std::map<int, std::pair<felInt,felInt> > IntRefToBegEndIndex_type;

    typedef std::pair<ElementType, felInt> seedElement;

    ///@}
    ///@name Public static Member Variables
    ///@{

    static constexpr double NWTOLL = 1e-7;
    static constexpr std::size_t NMAX = 200;

    // Number of type
    static constexpr int m_numTypesOfElement = static_cast<int>(ELEMENT_TYPE_COUNTER);

    // Number of points per element
    static const int m_numPointsPerElt[];
    // Number of vertices per element
    static const int m_numVerticesPerElt[];
    // Number of edges per element
    static const int m_numEdgesPerElt[];
    // Number of faces per element
    static const int m_numFacesPerElt[];

    // static members (constant in pratice)
    static EnumToFelNameGeoEle_type eltEnumToFelNameGeoEle;
    static StringToElementType_type eltFelNameToEnum;
    static ElementTypeLinearToDifferentElementType_type eltLinearToEltQuad;
    static ElementTypeLinearToDifferentElementType_type eltLinearToEltBubble;
    static std::vector<ElementType> bagElementType0D;
    static std::vector<ElementType> bagElementType1D;
    static std::vector<ElementType> bagElementType2D;
    static std::vector<ElementType> bagElementType3D;
    static std::vector<ElementType> bagElementTypeLinear;

    // static (truly not constant)
    static NameLabelToListLabel_type descriptionLineEnsightToListLabel;

    ///@}    
    ///@name Public member Variables
    ///@{

    /// map used to reorder elements per references
    /// To a (int) element reference, it associates a pair:
    ///    1/ the starting index to the elements having this reference
    ///    2/ the number of elements having this reference.
    IntRefToBegEndIndex_type intRefToBegEndMaps[ m_numTypesOfElement ];

    ///@}
    ///@name Life Cycle
    ///@{

    /// Constructors
    GeometricMeshRegion();

    /// Copy constructors
    GeometricMeshRegion(const GeometricMeshRegion& ) = delete;

    /// Move constructor
    GeometricMeshRegion(GeometricMeshRegion&& ) = delete;

    /// Destructor
    ~GeometricMeshRegion();

    ///@}  
    ///@name Operators
    ///@{

    /// Copy assignment
    GeometricMeshRegion& operator=(const GeometricMeshRegion& ) = delete;

    /// Move assignment
    GeometricMeshRegion& operator=(GeometricMeshRegion&& ) = delete;

    ///@}
    ///@name Operations
    ///@{

    /**
     * @brief set the dimension
     */
    void setDomainDim();

    /// Allocate the list of elements of a given type
    void allocateElements(const ElementType& eltType);

    /// Delete the list of elements
    void deleteElements();

  /// Delete the list of elements per given type
    void deleteElementsPerType(const ElementType& eltType);

    /// Reorder the list of elements "eltType" 
    void reorderListElePerRef(const std::map<int,std::vector<felInt>>& RefToElements, const ElementType eltType);

    /**
     * @brief set the bag m_bagElementTypeDomain
     */
    void setBagElementTypeDomain();
    void setBagElementTypeDomainBoundary();

    /**
     * @brief Create the local mesh from the global mesh
     * @param[in] meshGlobal The global mesh
     * @param[in] eltPartition The partitioning of element.
     * @param[in] rank The rank of the process.
     * @param[out] loc2globElem Local to global mapping of the elements.
     */
    void setLocalMesh(GeometricMeshRegion& meshGlobal, const std::vector<int>& eltPart, const int rank, std::vector<felInt>& loc2globElem);

    /// Build the list of edges
    void buildEdges();

    /// Build the list of faces
    void buildFaces();

    /// Mesh movement
    void moveMesh(const std::vector<double>& disp, const felReal& coef);

    /// Creation of Normal and Tangent at each node
    void computeNormalTangent(const InitialNormalTangentProvided initialNormalTangentProvided = InitialNormalTangentProvided::NONE);

    /// Allocate normals and tangents
    void allocateListNormalsAndTangents(const InitialNormalTangentProvided initialNormalTangentProvided = InitialNormalTangentProvided::NONE);

    /// Compute the Normal for each element
    void computeElementNormal(std::vector<std::vector<Point> >& listElementNormals) const;

    ///@}
    ///@name Access
    ///@{

    /// Number coordinates
    inline const int& numCoor() const { return m_numCoor; }
    inline       int& numCoor()       { return m_numCoor; }

    /// Spatial dimension
    inline const int& spatialDim() const { return m_numCoor; }
    inline       int& spatialDim()       { return m_numCoor; }

    /// Domain dimension
    inline const DomainDimensionType& domainDim() const { return m_domainDim; }
    inline       DomainDimensionType& domainDim()       { return m_domainDim; }

    /// Mesh format
    inline const FormatType& flagFormatMesh() const { return m_flagFormatMesh; }
    inline       FormatType& flagFormatMesh()       { return m_flagFormatMesh; }

    /// Return true/false wheter the mesh has been moved
    inline const bool& moved() const { return m_moved; }
    inline       bool& moved()       { return m_moved; }

    /// Number of mesh points
    inline felInt numPoints() const { return m_listPoints.size(); } // TODO std::size_t vs felInt

    /// Number of elements per element type
    inline const felInt& numElements( const ElementType & eltType ) const { return m_numElements[eltType]; }
    inline       felInt& numElements( const ElementType & eltType )       { return m_numElements[eltType]; }

    /// List of mesh points
    inline const std::vector<Point>& listPoints() const { return m_listPoints; }
    inline       std::vector<Point>& listPoints()       { return m_listPoints; }

    /// List of mesh points in reference position
    inline const std::vector<Point>& listReferencePoints() const { return m_listReferencePoints; }
    inline       std::vector<Point>& listReferencePoints()       { return m_listReferencePoints; }

    /// Get i-th point in mesh
    inline const Point& listPoint(const felInt & ivert) const { return m_listPoints[ivert]; }
    inline       Point& listPoint(const felInt & ivert)       { return m_listPoints[ivert]; }

    /// Get list of elements per element type
    inline felInt* listElements( const ElementType& eltType) const { return m_listElements[ eltType ]; }
    inline felInt* listElements( const ElementType& eltType)       { return m_listElements[ eltType ]; }

    /// Get m_intRefToEnum
    inline const IntRefToElementType_type& intRefToEnum() const { return m_intRefToEnum; }
    inline       IntRefToElementType_type& intRefToEnum()       { return m_intRefToEnum; }

    /// Get bag element
    inline const std::vector<ElementType>& bagElementTypeDomain()         const { return m_bagElementTypeDomain; }
    inline const std::vector<ElementType>& bagElementTypeDomainBoundary() const { return m_bagElementTypeDomainBoundary; }

    /// 
    inline const bool& createNormalTangent() const { return m_createNormalTangent; }
    inline       bool& createNormalTangent()       { return m_createNormalTangent; }

    /// List of normals on mesh points
    inline const std::vector<Point>& listNormals() const { return m_listNormals; }
    inline       std::vector<Point>& listNormals()       { return m_listNormals; }

    /// Normal on i-th point
    inline const Point& listNormal(const felInt& ivert) const { return m_listNormals[ivert]; }
    inline       Point& listNormal(const felInt& ivert)       { return m_listNormals[ivert]; }

    /// List of tangent on mesh points
    inline const std::vector<std::vector<Point>>& listTangents() const { return m_listTangents; }
    inline       std::vector<std::vector<Point>>& listTangents()       { return m_listTangents; }

    /// Tangents on i-th point
    inline const std::vector<Point>& listTangents(const felInt dim) const { return m_listTangents[dim]; }
    inline       std::vector<Point>& listTangents(const felInt dim)       { return m_listTangents[dim]; }

    /// Dim-th tangent on i-th point
    inline const Point& listTangent(const felInt dim, const felInt ivert) const { return m_listTangents[dim][ivert]; }
    inline       Point& listTangent(const felInt dim, const felInt ivert)       { return m_listTangents[dim][ivert]; }

    /// List of element normals
    inline const std::vector<std::vector<Point>>& listElementNormals() const { return m_listEltNormals; }
    inline       std::vector<std::vector<Point>>& listElementNormals()       { return m_listEltNormals; }

    /// Normal of i-th element
    inline const Point& listElementNormal(const ElementType eltType, const felInt& iel) const { return m_listEltNormals[eltType][iel]; }
    inline       Point& listElementNormal(const ElementType eltType, const felInt& iel)       { return m_listEltNormals[eltType][iel]; }

    /// List of element normals in reference configuration
    inline const std::vector<std::vector<Point>>& listElementNormalsInit() const { return m_listEltNormalsInit; }
    inline       std::vector<std::vector<Point>>& listElementNormalsInit() { return m_listEltNormalsInit; }

    /// Normal of i-th element in reference configuration
    inline const Point& listElementNormalInit(const ElementType eltType, const felInt& iel) const { return m_listEltNormalsInit[eltType][iel]; }
    inline       Point& listElementNormalInit(const ElementType eltType, const felInt& iel)       { return m_listEltNormalsInit[eltType][iel]; }

    /// Status edges
    inline const bool& statusEdges() const { return m_builtEdges; }
    inline       bool& statusEdges()       { return m_builtEdges; }

    /// List of edges
    inline const ListEdges& listEdges() const { return m_listEdges; }
    inline       ListEdges& listEdges()       { return m_listEdges; }

    /// Status faces
    inline const bool& statusFaces() const { return m_builtFaces; }
    inline       bool& statusFaces()       { return m_builtFaces; }

    /// List of faces
    inline const ListFaces& listFaces() const { return m_listFaces; }
    inline       ListFaces& listFaces()       { return m_listFaces; }

    /// Number of edges or faces
    inline felInt numEdges() const { return m_listEdges.numEdges(); }
    inline felInt numFaces() const { return m_listFaces.numFaces(); }

    /// Get number points per element type
    inline int numPtsPerElement(const ElementType& ityp) const { return m_numPointsPerElt[ityp]; }
    
    /**
     * @brief Get multiple elements
     * @details Retrieve all elements in m_listElements that are indexed by "indexList" and insert
       them into "array", starting from the position "startPosArray" in the array.
       Ex.: if trias={4 3 0  4 0 1  5 4 1  5 1 2  7 6 3  7 3 4 ...} and indexList={1,3,4}
            then array = {..., 4 0 1  5 1 2  7 6 3} ("4": at startPosArray in array).
       Beware: the arrays must be allocated before calling getElements.
     * @param[in] eltType The type of the elements to retrieve
     * @param[in] indexList The list of id of the elements to retrieve
     * @param[in] numIndexList The number of element to retrieve
     * @param[out] array Array with the retrieved elements
     * @param[in] sizeArray The size of array
     * @param[in] startPosArray The position from where to insert the elements
     */
    // TODO
    void getElements(const ElementType& eltType, const felInt* indexList, felInt numIndexList,
                     felInt* array, int sizeArray, felInt startPosInArray = 0) const;

    /**
     * @brief Fill m_listElements with multiple elements
     * @param[in] eltType The type of the elements to insert
     * @param[in] indexList The list of id of the elements to insert
     * @param[in] numIndexList The number of element to insert
     * @param[in] array The array containing the elements to insert
     * @param[in] sizeArray the size of array
     */
    // TODO
    void setElements(const ElementType& eltType, const felInt* indexList, felInt numIndexList,
                     const felInt* array, int sizeArray);

    /**
     * @brief Get one element in m_listElements.
     * @details One can increment the point ID: flagIncrement usage: felisce format (start from 0) -> {medit or ensight...} (start from 1)
     * @param[in] eltType The type of the element to get
     * @param[in] index The id of the element
     * @param[out] array The array to fill with the element
     * @param[in] startindex The index where to start in m_listElements
     * @param[in] flagIncrement A flag to tell if we start from 0 or 1
     */
    void getOneElement(const ElementType& eltType, const felInt index, std::vector<felInt>& array,
                       const felInt startindex = 0, const bool flagIncrement = false) const;

    void getOneElement(const ElementType& eltType, const felInt index, std::vector<felInt>& array,
                       std::vector<Point*>& rPoints, const felInt startindex = 0, const bool flagIncrement = false) const;

    void getOneElement(const felInt idElem, std::vector<felInt>& array, const bool flagIncrement = false) const;

    /**
     * @brief Set one element in m_listElements
     * @details flagDecrement usage: {medit or ensight...} (start from 1) -> felisce format (start from 0)
     * @param[in] eltType The type of element to std::set
     * @param[in] index The id of the element to std::set in m_listElements
     * @param[in] array The array containing the element to std::set
     * @param[in] flagDecrement A flag to tell if we start from 0 or 1
     */
    void setOneElement(const ElementType& eltType, const felInt index,
                       const std::vector<felInt>& array, const bool flagDecrement = false);

    /// Return the number of found elements, put in array all vertices of the elements having the given ref
    int getElementsPerRef(const ElementType& eltType, const int ref, std::vector<felInt>& array) const;

    /// Get the indices of the elements of type eltType for the given reference
    void getElementsIDPerRef(const ElementType& eltType, const int ref, felInt* out_startindex, felInt* out_numRefEle) const;

    /// Get the element type and its id with respect to the element type of an element
    void getTypeElemFromIdElem(const felInt idElem, ElementType& eltType, felInt& idByType) const;

    /// Get the element global id by type and its id with respect to the element type
    void getIdElemFromTypeElemAndIdByType(const ElementType eltType, const felInt iElByType, felInt& position) const;

    /// Search a volumic element from the points ID of the face (in 3D).
    /// Return the id of the neighboring volume, and the coordinates of its points. 
    void getElementFromBdElem(const std::vector<felInt>& facePts, std::vector<felInt>& elemIdPointVol,
                              std::vector< Point* >& elemPointVol, int& idLocFace, felInt& idElemVol);

    /// Search the adjacent element (by eltType and index) sharing one edge/face
    bool getAdjElement(ElementType& eltType, felInt& index, int localIndexOfEdgeOrFace) const;

    /// Search the adjacent element (by global element id) sharing one edge/face
    std::pair<bool, ElementType> getAdjElement(felInt& idElem, int localIndexOfEdgeOrFace) const;

    /// Set normal in a node
    void setListNormal(int iNode, Point vecNormal);

    /// Set tangent in a node
    void setListTangent(int idim, int iNode, Point vecTangent);

    ///@}
    ///@name Inquiry
    ///@{

    /// Get number of elements per Bag
    felInt getNumElementByBag(const std::vector<ElementType>& them_bagElem) const;

    /// Get number of element per spatial dimesion
    inline felInt getNumElement1D() const { return getNumElementByBag(bagElementType1D); };
    inline felInt getNumElement2D() const { return getNumElementByBag(bagElementType2D); };
    inline felInt getNumElement3D() const { return getNumElementByBag(bagElementType3D); };

    /// Get number of elements
    inline felInt getNumDomainElement()   const { return getNumElementByBag( m_bagElementTypeDomain ); };
    inline felInt getNumBoundaryElement() const { return getNumElementByBag( m_bagElementTypeDomainBoundary ); };
    inline felInt getNumElement()         const { return getNumElementByBag( m_bagElementTypeDomain ) + getNumElementByBag( m_bagElementTypeDomainBoundary ); };

    void getAllEdgeOfElement(const ElementType eltType, const felInt iel, std::vector<felInt>& array) const;
    void getAllEdgeOfElement(const felInt iel, std::vector<felInt>& array) const;

    void getAllFaceOfElement(const ElementType eltType, const felInt iel, std::vector<felInt>& array) const;
    void getAllFaceOfElement(const felInt iel, std::vector<felInt>& array) const;

    void getEdge(const std::vector<felInt>& edgePts, Edge& resultEdge) const;
    void getFace(const std::vector<felInt>& facePts, Face& resultFace) const;

    ///@}
    ///@name Input and output
    ///@{

    /**
     * @brief Print information on the mesh
     * @details Print info for elements in domain and boundary bag. Print info for elements' ref.
     * @param[in] verbose level of verbose
     * @param[in] outstr stream to where the informations are printed
     */
    void print(int verbose = 0, std::ostream& outstr = std::cout) const;

    /// print info for vertices
    void printVertices(std::ostream& outstr = std::cout) const;

    /**
     * @brief Print info for elements in a bag
     * @param[in] bagElem The element bag of elements
     * @param[in] verbose level of verbose
     * @param[in] outstr stream where to print the infos
     * @param[in] incrementPointID To tell if indexes start from 0 or 1
     */
    void printElements(const std::vector<ElementType>& bagElem, const int verbose = 0, 
                       std::ostream& outstr = std::cout, const bool incrementPointID = false) const;

    /**
     * @brief Print info for elements' ref.
     * @param bagElem The elements bag
     * @param verbose level of verbose
     * @param outstr stream where to print
     */
    void printRefToBegEnd(const std::vector<ElementType>& bagElem, const int verbose = 0, std::ostream& outstr = std::cout) const;

    ///@}
    ///@name Friends
    ///@{

    ///@}

    void extractVerticesFromBoundary(const std::vector<int>& label, std::set<felInt>& list);

    // TODO
    bool findLocalCoord(ElementType& ityp, felInt& iel, const Point& point, Point& pointLoc);
    bool isPointInTetra(const felInt iel, const Point& p, std::array<double, 4>& bary, const double tol);
    bool isPointInTri(const felInt iel, const Point& p, std::array<double, 3>& bary, const double tol);
    bool isPointInSeg(const felInt iel, const Point& p, std::array<double, 2>& bary, const double tol);

    typedef std::vector<std::tuple<ElementType, felInt, felInt>> BallMap;

    void getVertexBall(const felInt idxVer, const ElementType eltTyp, const felInt eltGrm, BallMap& ball) const;
    void getEdgeShell(const std::vector<felInt>& idxVer, ElementType eltTyp, const felInt eltGrm, BallMap& shell) const;
    bool isVertexOfElt(const felInt idxVer, ElementType eltTyp, const felInt eltIdx, felInt &verPos) const;
    bool isEdgeOfElt(const std::vector<felInt>& idxVer, const ElementType eltTyp, const felInt eltIdx, felInt &verPos) const;
    void getElementPatch(const ElementType typGrm, const felInt eltGrm, std::set<seedElement>& elementsInPatch) const;
    void getElementDeepPatch(std::set<seedElement>& elementsInPatch, std::size_t patch_level = 1) const;
    void getElementsInRegion(seedElement germ_elt, std::set<seedElement>& set_elt) const;
    std::size_t computeConnectedComponents(std::map<ElementType,std::vector<std::size_t>>& mapCco);

    static const std::vector< std::vector< std::vector<unsigned short int> > > m_eltTypMapVerToFac;
    static const std::vector< std::vector< std::vector<unsigned short int> > > m_eltTypMapFacToVer;
    static const std::vector< std::vector< std::vector<unsigned short int> > > m_eltTypMapEdgToVer;
    static const std::vector< std::vector< std::vector<unsigned short int> > > m_eltTypMapEdgToFac;

  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}
  private:
    ///@name Private static Member Variables
    ///@{

    ///@}
    ///@name Private member Variables
    ///@{

    /// Spatial dimension (1, 2, 3)
    int m_numCoor = 0;

    /// Domain Dimension (e.g. =2 in 3D for a 2-manifold or surface)
    DomainDimensionType m_domainDim = GeoMeshUndefined;

    /// O: if Medit; 1 if Ensight6
    FormatType m_flagFormatMesh = FormatUndefined;

    /// Array of Points (coordinates)
    bool m_moved = false;
    std::vector<Point> m_listPoints;
    std::vector<Point> m_listReferencePoints;

    /// For each type of element the number of elements in the mesh
    felInt m_numElements[ m_numTypesOfElement ];

    /// For each type of element we point to the (1d) array of points of the elements (if present in the mesh)
    felInt* m_listElements[ m_numTypesOfElement ];

    /// Reference to element type std::unordered_map
    IntRefToElementType_type m_intRefToEnum;

    /// List of domain element types in the mesh
    std::vector<ElementType> m_bagElementTypeDomain;

    /// List of boundary element types in mesh
    std::vector<ElementType> m_bagElementTypeDomainBoundary;

    /// List of points normals and tangents
    bool m_createNormalTangent = false;
    int  m_choiceDirectionTangent;
    std::vector<Point> m_listNormals;
    std::vector<std::vector<Point>>  m_listTangents;

    /// List of element normals
    std::vector<std::vector<Point> > m_listEltNormals;
    std::vector<std::vector<Point> > m_listEltNormalsInit;

    /// List of mesh edges
    ListEdges m_listEdges;

    /// true if edges already built
    bool m_builtEdges = false;

    /// List of mesh faces
    ListFaces m_listFaces;

    /// true if faces already built
    bool m_builtFaces = false;

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    /**
     * @brief Allocate m_listElements.
     * @details From a bag of elements, one allocates the list of elements of the current mesh, keeping only the elements that have the given rank.
     * @param[in] theElementBag The bag containing the element type
     * @param[in] meshGlobal The global mesh
     * @param[in] eltPartition The partition of the elements
     * @param[in] rank The rank of the process
     * @param[in,out] countEltTot To count the elements.
     */
    void m_allocateListElementsByRef(const std::vector<ElementType>& theElementBag, GeometricMeshRegion& meshGlobal,
                                     const std::vector<int>& eltPart, const int rank, felInt& numEltTot);

    /**
     * @brief Fill m_listElements and update intRefToBegEndMaps
     * @details From a bag of elements, one fills in the list of elements of the current mesh, keeping only the elements that have the given rank. (Do this only after having allocated m_listElements.)
     * @param[in] theElementBag The bag containing the element types to fill
     * @param[in] meshGlobal The global mesh
     * @param[in] eltPartition The partition of the elements
     * @param[in] rank The rank of the process
     * @param[out] loc2globElem The local to global mapping for the elements.
     * @param[in,out] countEltTot To count the elements
     */
    void m_fillListElementsByRef(const std::vector<ElementType>& theElementBag, GeometricMeshRegion& meshGlobal,
                                 const std::vector<int>& eltPart, const int rank, std::vector<felInt>& loc2globElem, felInt& numEltTot);

    /// helpers for edges and faces
    void m_buildEdgesPerBag(const std::vector<ElementType>& theBagElt, int theDim);
    void m_buildFacesPerBag(const std::vector<ElementType>& theBagElt, int theDim);

    ///@}
    ///@name Private Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Private LifeCycle
    ///@{

    ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif /* GEOMETRICMESHREGION_HPP  defined */
