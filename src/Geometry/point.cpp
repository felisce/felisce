//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "Geometry/point.hpp"
#include "Core/felisce_error.hpp"

namespace felisce 
{
const std::array<double, 3>& Point::getCoor() const
{
  return m_coor;
}

/***********************************************************************************/
/***********************************************************************************/

std::array<double, 3>& Point::getCoor()
{
  return m_coor;
}

/***********************************************************************************/
/***********************************************************************************/

void Point::getCoor(std::vector<double>& coord)
{
  for(std::size_t i = 0; i < coord.size(); ++i)
    coord[i] = m_coor[i];
}

/***********************************************************************************/
/***********************************************************************************/

void Point::setCoor(const std::vector<double>& coord)
{
  if ( coord.size() != 3 ) {
    FEL_ERROR("expecting 3 coordinates")
  }
  m_coor[0] = coord[0];
  m_coor[1] = coord[1];
  m_coor[2] = coord[2];
}

/***********************************************************************************/
/***********************************************************************************/

void Point::setCoor(const std::array<double, 3>& coord)
{
  m_coor = coord;
}

/***********************************************************************************/
/***********************************************************************************/

void Point::setCoor(const double* coord)
{
  m_coor[0] = coord[0];
  m_coor[1] = coord[1];
  m_coor[2] = coord[2];
}

/***********************************************************************************/
/***********************************************************************************/

void Point::print(
  const std::size_t verbose,
  std::ostream& outstr
  ) const
{
  if(verbose) {
    outstr << "[" << m_coor[0] << ", " << m_coor[1] << ", " << m_coor[2] << "]  "
            << "\n";
  }
}

/***********************************************************************************/
/***********************************************************************************/

void Point::printWithTab(std::ostream& outstr) const
{
  outstr <<m_coor[0] << '\t' << m_coor[1] << '\t' << m_coor[2];
}

}
