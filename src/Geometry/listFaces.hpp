//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

#ifndef LISTFACES_HPP
#define LISTFACES_HPP

// System includes
#include <vector>
#include <list>
#include <iostream>

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce 
{
//////////////////////
// LIST FACES CLASS //
//////////////////////
/*!
  \class ListFaces
  \authors J.Castelneau & J.Foulon

  \brief Class implementing the list of geometric faces

  A list of faces is defined by
  - a list of pointer on face
  - the total number of faces
  - a chained list of faces

  To access at the face defined by the std::set of vertices (i,j,k) or (i,j,k,l):
  - go to the i-th index in the list of pointer
  - browse the chained list of faces where 1st vertex is i
  */

class Face;
class NeighVolumesOfFaces;

class ListFaces {

public: // le deplacer ailleurs? ->Core?
  static const felInt   m_null_int;

  // Constructor
  // ===========
  ListFaces() : m_numFaces(0), m_numGivenFaces(0) {}
  ~ListFaces();

  //! Copy constructor, to avoid warning due to user-declared destructor.
  ListFaces(const ListFaces&) = default;

  void searchAddFace(std::vector<felInt> ptOfFace, NeighVolumesOfFaces* the_neighbour,
                      bool check_surface_mesh_flag);

  void searchFace(const std::vector <felInt> & facePts, Face& face) const;

  felInt findFace(const std::vector <felInt> & facePts) const;
  felInt findFace(const Face& the_face) const; //! if not used: remove it! 2012/08 vm

  bool compareFaces(const std::vector<felInt>& face1, const std::vector<felInt>& face2) const;

  void permutFacePointsSmallIDFirst(const std::vector <felInt>& theFacePts,
                                    std::vector <felInt>& theOrderedFactePts) const;
  void permutFacePointsSmallIDFirst(std::vector <felInt>& facePts) const;

  void giveOppositeFace(const std::vector <felInt>& theFacePts,
                        std::vector <felInt>& theOppositeFacePts) const;

  void print( std::ostream& outstr = std::cout, int verbose = 0, int orderFormat = 0 ) const;

  void clearAndInit(felInt numPts);

  // getters
  // ======
  inline const std::vector <Face*> & listBegin() const   {
    return m_listBegin;
  }
  inline const std::vector <Face*> & list() const        {
    return m_list;  // CHANGER CE NOM! VM 2012/09
  }
  inline const felInt & numFaces() const            {
    return m_numFaces;
  }
  inline const felInt & numGivenFaces() const       {
    return m_numGivenFaces;
  }

  inline Face* list(felInt faceID) const    {
    return m_list[faceID];
  }


  // setters
  // ======
  inline std::vector <Face*> & listBegin() {
    return m_listBegin;  // A VIRER! VM 2012/09
  }
  inline std::vector <Face*> & list()      {
    return m_list;  // A VIRER! VM 2012/09
  }
  inline felInt & numFaces()          {
    return m_numFaces;
  }

  void setNumGivenFaces() {
    m_numGivenFaces = m_numFaces;
  }

private:
  felInt         m_numFaces;
  felInt         m_numGivenFaces; // num of faces coming from the mesh
  std::vector<Face*>  m_listBegin;
  std::vector<Face*>  m_list;

};
}
#endif
