//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Castelneau & J.Foulon
//

#ifndef LISTEDGES_HPP
#define LISTEDGES_HPP

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Geometry/geometricEdges.hpp"

namespace felisce
{
//////////////////////
// LIST EDGES CLASS //
//////////////////////
/*!
  \class ListEdges
  \authors J.Castelneau & J.Foulon

  \brief Class implementing the list of geometric edges

  A list of edges is defined by
  - a list of pointer on edge
  - the total number of edges
  - a chained list of edges

  To access at the edge defined by the 2 vertices (i,j):
  - go to the i-th index in the list of pointer
  - browse the chained list of edges where 1st vertex is i
  */
// class Edge;
class NeighFacesOfEdges;
class NeighVolumesOfEdges;

class ListEdges {

  typedef felisce::shared_ptr<NeighFacesOfEdges> shar_ptrFace;
  typedef felisce::shared_ptr<NeighVolumesOfEdges> shar_ptrVol;

public:

  static const felInt   m_null_int;

  // Constructor / Destructor
  // =======================
  ListEdges(): m_numEdges(0), m_numGivenEdges(0),m_numOfEdgesSupportingADof(0) {}
  ~ListEdges();

  //! Copy constructor, to avoid warning due to user-declared destructor.
  ListEdges(const ListEdges&) = default;

public:

  void searchAddEdge(felInt pt_edge_beg,  felInt pt_edge_end,
                      shar_ptrFace   the_face_neighbour,
                      shar_ptrVol     the_vol_neighbour);

  // return the edge id, if edge=[beg,end] found, else return -1
  felInt findEdge( felInt pt_edge_beg,  felInt pt_edge_end ) const;
  void searchEdge(const std::vector<felInt>& edgePts, Edge& resultEdge) const;
  void print( std::ostream& outstr = std::cout, int verbose = 0, int orderFormat = 0 ) const;

  // getters
  // ======
  inline const std::vector<Edge*>& list() const {
    return m_listEdges;
  }
  inline const std::vector<Edge*>& listBegin() const {
    return m_listBeginEdge;
  }
  inline const felInt       & numEdges()  const {
    return m_numEdges;
  }
  inline const felInt       & numGivenEdges() const {
    return m_numGivenEdges;
  }
  inline const felInt       & numEdgesSupportingADof() const {
    return m_numOfEdgesSupportingADof;
  }

  // setters
  // ======
  inline std::vector <Edge*> & list()      {
    return m_listEdges;
  }
  inline std::vector <Edge*> & listBegin() {
    return m_listBeginEdge;
  }
  //inline felInt         & numEdges()  { return m_numEdges; }
  Edge* operator[](felInt i) {
    return m_listEdges[i];
  }
  inline const Edge* operator[](felInt i) const {
    return m_listEdges[i];
  }

  void setNumGivenEdges() {
    m_numGivenEdges = m_numEdges;
  }

  void clearAndInit(felInt numPts);
  double countAndComputeIdEdgesSupportingDofs(const RefElement& refEle);

private:
  felInt            m_numEdges;
  felInt            m_numGivenEdges;
  felInt m_numOfEdgesSupportingADof;
  std::vector<Edge*>     m_listBeginEdge;
  std::vector<Edge*>     m_listEdges;

  void m_insertNeighbors(Edge* the_edge,
                          shar_ptrFace   the_face_neighbour,
                          shar_ptrVol the_vol_neighbour) const;

};
}

#endif
