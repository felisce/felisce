//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Matteo
//

#ifndef _Curvatures_HPP
#define _Curvatures_HPP

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/elementField.hpp"

namespace felisce {
class Curvatures 
{
public:
  Curvatures();
  ~Curvatures() = default;
  void update(const ElementField& normal, const UBlasMatrix& firstFundForm, const UBlasMatrix& covBasis);

  double gaussCurv() const {
    return m_gaussCurv;
  }
  double meanCurv() const {
    return m_meanCurv;
  }
  double maxCurv() const {
    return m_maxCurv;
  }
  double minCurv() const {
    return m_minCurv;
  }

  double maxEigVCartComp( std::size_t i) {
    return m_eigMax(i);
  }
  double minEigVCartComp( std::size_t i) {
    return m_eigMin(i);
  }
  double coeffKoiterLinear(const double& poisson) const {
    return 4*std::pow(m_meanCurv,2)-2*(1-poisson)*m_gaussCurv;
  }
  double coeffKoiterQuadratic(const double& poisson) const {
    return 8*std::pow(m_meanCurv,3)-2*(3-poisson)*m_meanCurv*m_gaussCurv;
  }
  double coeffKoiterCubic(const double& poisson) const {
    return 4*(std::pow(m_meanCurv,4)-std::pow(m_meanCurv,2)*m_gaussCurv)+
      0.5*(1+poisson)*std::pow(m_gaussCurv,2);
  }
  double coeffKoiterMeanOfSquaredCurvatures() const {
    return 2*std::pow(m_meanCurv,2)-m_gaussCurv;
  }
  
  double coeffFibersLinearMax() const {
    return m_maxProp*std::pow(m_maxCurv,2);
  }

  double coeffFibersQuadraticMax() const {
    return m_maxProp*std::pow(m_maxCurv,3);
  }

  double coeffFibersCubicMax() const {
    return m_maxProp*std::pow(m_maxCurv,4);
  }
  
  double coeffFibersLinearMin() const {
    return m_minProp*std::pow(m_minCurv,2);
  }

  double coeffFibersQuadraticMin() const {
    return m_minProp*std::pow(m_minCurv,3);
  }

  double coeffFibersCubicMin() const {
    return m_minProp*std::pow(m_minCurv,4);
  }

  double coeffFiberProportionMin() const {
    return m_minProp;
  }

  double coeffFiberProportionMax() const {
    return m_maxProp;
  }

  UBlasMatrix invMetricTensor() const { 
    return m_invFFT;
  }

  UBlasMatrix minFiberTensor() const {
    return m_projOnMinEigVec;
  }

  UBlasMatrix maxFiberTensor() const {
    return m_projOnMaxEigVec;
  }

  UBlasMatrix minProjTensor() const {
    return m_projOnMinEigVec/m_minProp;
  }

  UBlasMatrix maxProjTensor() const {
    return m_projOnMaxEigVec/m_maxProp;
  }

  UBlasMatrix shapeOperator() const {
    return m_shapeOperator;
  }

  UBlasMatrix cMatrix() const {
    return prod(m_shapeOperator,m_invFFT);
  }

  UBlasMatrix cSquaredMatrix() const {
    return prod(m_shapeOperator, cMatrix() );
  }
  
  static UBlasVector testFuncGradient( int idLocDof );
  
  static UBlasVector elemFieldGradient(const ElementField& f);
  
  static double mNormOfGradient( const ElementField& f , const UBlasMatrix& M);

  void print() const;

private:

  void computeSecondFundamentalForm( const UBlasMatrix& covBasis, const ElementField& normal);
  void computeShapeOperator();
  void computeCurvatures();
  void computePrincipalDirections();
  void computeArcLengths();
  void computeProportions();
  void computeProjectors();
  void computeEigInCartCoordinate( const UBlasMatrix& covBasis, const ElementField& normal);
  
  double m_gaussCurv;
  double m_meanCurv;

  double m_maxCurv;
  double m_minCurv;

  double m_rayleighMaxEigVec;
  double m_rayleighMinEigVec;

  UBlasMatrix m_firstFundForm;
  UBlasMatrix m_invFFT;
  UBlasMatrix m_secondFundForm;
  UBlasMatrix m_shapeOperator;

  UBlasMatrix m_projOnMaxEigVec;
  UBlasMatrix m_projOnMinEigVec;

  UBlasVector m_maxEigVec;
  UBlasVector m_minEigVec;

  double m_maxProp;
  double m_minProp;
  double m_minProportion;

  UBlasVector m_eigMax;
  UBlasVector m_eigMin;
};
}
#endif
