//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    V.Martin, L.Boilevin-Kayl
//

// System includes
#include <numeric>

// External includes

// Project includes
#include "FiniteElement/geoElement.hpp"
#include "Geometry/geo_utilities.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce {

bool GeoUtilities::isPointInElement(const GeometricMeshRegion::ElementType eltType, const std::vector<Point*>& pointsOfElt, const Point& point, const double tol)
{
  // TODO D.C. i don't like this function at all
  // we should rewrite it using polymorphism
  TypeShape elShape = static_cast<TypeShape>( GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->shape().typeShape() );

  switch ( elShape ) {
    
    case Segment: {
      std::array<double, 2> bary;
      return isPointInSeg(pointsOfElt, point, bary, tol);

      break;
    }

    case Triangle: {
      std::array<double, 3> bary;
      return isPointInTri(pointsOfElt, point, bary, tol);

      break;
    }

    case Tetrahedron: {
      std::array<double, 4> bary;
      return isPointInTetra(pointsOfElt, point, bary, tol);

      break;
    }

    case NullShape:
    case Node:
    case Quadrilateral: 
    case Hexahedron:
    case Prism:
    case Pyramid:

      FEL_ERROR("Not implemented for this element Shape")
      break;
  }

  return false;
}

/***********************************************************************************/
/***********************************************************************************/

bool GeoUtilities::isPointInTetra(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 4>& bary, const double tol)
{
  // Compute inverse of tetrahedron volume
  const double ivo = 1. / GeoUtilities::tetraVolume(*eltPoints[0],*eltPoints[1],*eltPoints[2],*eltPoints[3]);

  // Compute barycentric coordinates
  GeoUtilities::tetraBary(eltPoints, p, bary);
  
  // Normalize barycentric coordinates
  std::for_each(bary.begin(), bary.end(), [&](auto& val){ val *= ivo; });

  // Check if point is inside
  for (auto it = bary.begin(); it != bary.end(); ++it)
    if ( *it < - tol )
      // Point is outside
      return false;

  // Point is inside
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

bool GeoUtilities::isPointInTetra(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 4>& bary, Location& location, const double tol)
{
  // Compute barycentric coordinates
  GeoUtilities::tetraBary(eltPoints, p, bary);

  // Compute faces surface
  Point nrm;
  std::array<double,4> srf;
  GeoUtilities::triNormal(*eltPoints[0],*eltPoints[1],*eltPoints[2], nrm);
  srf[0] = 0.5 * nrm.norm();
  GeoUtilities::triNormal(*eltPoints[0],*eltPoints[3],*eltPoints[1], nrm);
  srf[1] = 0.5 * nrm.norm();
  GeoUtilities::triNormal(*eltPoints[1],*eltPoints[3],*eltPoints[2], nrm);
  srf[2] = 0.5 * nrm.norm();
  GeoUtilities::triNormal(*eltPoints[0],*eltPoints[2],*eltPoints[3], nrm);
  srf[3] = 0.5 * nrm.norm();

  // Check if point is inside
  unsigned short int nbrBarPos = 0, nbrBarEps = 0;
  std::array<unsigned short int,4> idxBarPos, idxBarEps;

  for (std::size_t i = 0; i < bary.size(); ++i) {
    if ( 6.*bary[i] > tol*srf[i] ) {
      idxBarPos[nbrBarPos] = i;
      nbrBarPos++;
    }
    else if ( 6.*std::abs(bary[i]) <= tol*srf[i] ) {
      idxBarEps[nbrBarEps] = i;
      nbrBarEps++;
    }
    else {
      // Point is outside
      return false;      
    }
  }

  switch (nbrBarPos) {
    case 0 :
      FEL_ERROR("GeoUtilities::isPointInTetra : Bary cannot be all <= tol !");
      break;

    case 1 :
      location.first  = Position::OnVertex;
      location.second = std::array<unsigned short int,4>{3, 2, 0, 1}[idxBarPos[0]];
      break;

    case 2 :
      location.first  = Position::OnEdge;
      location.second = std::array<unsigned short int,16>{ 6, 0, 3, 1, /**/ 0, 6, 4, 2, /**/ 3, 4, 6, 5,  /**/ 1, 2, 5, 6}[4*idxBarEps[0]+idxBarEps[1]];
      break;

    case 3 :
      location.first  = Position::OnFace;
      location.second = idxBarEps[0];
      break;

    case 4 :
      location.first  = Position::Inside;
      location.second = 0;
      break;

    default:
      FEL_ERROR("GeoUtilities::isPointInTetra : Error !");
  }

  // Point is inside
  return true;
}
/***********************************************************************************/
/***********************************************************************************/

bool GeoUtilities::isPointInTri(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 3>& bary, const double tol)
{  
  Point normal;

  // Compute triangle volume
  GeoUtilities::triNormal(*eltPoints[0],*eltPoints[1],*eltPoints[2], normal);

  // Compute inverse norm and volume
  const double ino = 1. / normal.norm();
  const double ivo = ino / 0.5;

  // Normalize normal
  normal *= ino;

  // Compute barycentric coordinates
  GeoUtilities::triBary(eltPoints, p, normal, bary);

  // Normalize barycentric coordinates
  std::for_each(bary.begin(), bary.end(), [&](auto& val){ val *= ivo; });

  // Check if point is inside
  for (auto it = bary.begin(); it != bary.end(); ++it)
    if ( *it < - tol )
      // Point is outside
      return false;

  // Point is inside
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

bool GeoUtilities::isPointInTri(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 3>& bary, Location& location, const double tol)
{  
  Point normal;

  // Compute triangle volume
  GeoUtilities::triNormal(*eltPoints[0],*eltPoints[1],*eltPoints[2], normal);

  // Compute inverse norm and volume
  const double ino = 1. / normal.norm();

  // Normalize normal
  normal *= ino;

  // Compute barycentric coordinates
  GeoUtilities::triBary(eltPoints, p, normal, bary);

  // Compute edges lenght 
  std::array<double,3> len;
  len[0] = ( *eltPoints[0] - *eltPoints[1] ).norm();
  len[1] = ( *eltPoints[1] - *eltPoints[2] ).norm();
  len[2] = ( *eltPoints[2] - *eltPoints[0] ).norm();

  // Check if point is inside
  unsigned short int nbrBarPos = 0, nbrBarEps = 0;
  unsigned short int idxBarPos, idxBarEps;

  for (std::size_t i = 0; i < bary.size(); ++i) {
    if ( 2.*bary[i] > tol*len[i] ) {
      nbrBarPos++;
      idxBarPos = i;
    }
    else if ( 2.*std::abs(bary[i]) <= tol*len[i] ) {
      nbrBarEps++;
      idxBarEps = i;
    }
    else {
      // Point is outside
      return false;      
    }
  }

  switch (nbrBarPos) {
    case 0 :
      FEL_ERROR("GeoUtilities::isPointInTri : Bary cannot be all <= tol !");
      break;

    case 1 :
      location.first  = Position::OnVertex;
      location.second = std::array<unsigned short int,3>{2, 0, 1}[idxBarPos];
      break;

    case 2 :
      location.first  = Position::OnEdge;
      location.second = idxBarEps;
      break;

    case 3 :
      location.first  = Position::Inside;
      location.second = 0;
      break;

    default:
      FEL_ERROR("GeoUtilities::isPointInTri : Error !");
  }

  // Point is inside
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

bool GeoUtilities::isPointInSeg(const std::vector<Point*>& eltPoints, const Point& p, std::array<double, 2>& bary, const double tol)
{
  const Point edge = *eltPoints[1] - *eltPoints[0];
  const Point vec0 = p - *eltPoints[0];
  const Point vec1 = p - *eltPoints[1];

  // Compute inverse of segment length
  const double ivo = edge.norm();

  // Compute barycentric coordinates
  bary[0] =   ( edge * vec0 );
  bary[1] = - ( edge * vec1 );


  // Normalize barycentric coordinates
  std::for_each(bary.begin(), bary.end(), [&](auto& val){ val *= ivo; });

  // Check if point is inside
  for (auto it = bary.begin(); it != bary.end(); ++it)
    if ( *it < - tol )
      // Point is outside
      return false;

  // Point is inside
  return true;
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::getClosestProjectionOnElement(const GeometricMeshRegion::ElementType eltType, const std::vector<Point*>& pointsOfElt, const Point& point, Point& projection, const double tol)
{
  // TODO D.C. i don't like this function at all
  // we should rewrite it using polymorphism
  
  TypeShape elShape = static_cast<TypeShape>( GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->shape().typeShape() );

  switch ( elShape ) {

    case Segment: {

      getClosestProjectionSeg(pointsOfElt, point, projection, tol);

      break;
    }

    case Triangle: {

      getClosestProjectionTri(pointsOfElt, point, projection, tol);

      break;
    }

    case NullShape:
    case Node:
    case Quadrilateral: 
    case Tetrahedron:
    case Hexahedron:
    case Prism:
    case Pyramid:

      FEL_ERROR("Not implemented for this element Shape");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::getClosestProjectionSeg(const std::vector<Point*>& pointsOfElt, const Point& point, Point& projection, const double tol)
{
  std::array<double, 2> bary;
  if ( isPointInSeg(pointsOfElt, point, bary, tol) ) {
    projection = point;
    return;
  }

  if ( bary[0] < - tol )
    projection = *pointsOfElt[0];
  else if ( bary[1] < - tol )
    projection = *pointsOfElt[1]; 
  else
    FEL_ERROR("It is not possible to have 2 negative bary.");
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::getClosestProjectionTri(const std::vector<Point*>& pointsOfElt, const Point& point, Point& projection, const double tol)
{ 
  std::array<double, 3> bary;
  if ( isPointInTri(pointsOfElt, point, bary, tol) ) {
    projection = point;
    return;
  }

  std::size_t nbrPos = 0;
  std::size_t idxPos = 0, idxNeg = 0;
  for (std::size_t i = 0; i < bary.size(); ++i){
    if ( bary[i] > - tol ){
      nbrPos++;
      idxPos = i;
    }
    else {
      idxNeg = i;
    }
  }

  switch (nbrPos) {

    // Two bary negative -> closest point is one of the triangle vertices
    case 1: {

      unsigned short int idxVer = GeometricMeshRegion::m_eltTypMapVerToFac[GeometricMeshRegion::Tria3][idxPos][0];
      projection = *pointsOfElt[idxVer];     

      break;
    }

    // One bary negative -> closest point it's on the edge or it is one of the edge extreme points 
    case 2: {

      auto idxVer = GeometricMeshRegion::m_eltTypMapFacToVer[GeometricMeshRegion::Tria3][idxNeg];

      // Get edge
      std::vector<Point*> edge{ pointsOfElt[idxVer[0]], pointsOfElt[idxVer[1]] };

      // Project point on edge
      GeoUtilities::projectOnLine(edge, point, projection);

      // Get closest projection on edge
      getClosestProjectionSeg(edge, projection, projection, tol);

      break; 
    }

    default:
      FEL_ERROR("It is not possible to have 3 negative bary.");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::computeElementNormal(const GeometricMeshRegion::ElementType eltType, const std::vector<Point*>& pointsOfElt, Point& normal)
{
  // TODO D.C. i don't like this function at all
  // we should remove it and use polymorphism instead
  TypeShape elShape = static_cast<TypeShape>( GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->shape().typeShape() );

  // TODO here we work on the shape... so we need only the points of the shape (Tria9 -> only three vertices, are the first three nodes???)

  switch ( elShape ) {
    
    case Segment:

      normal.x() = pointsOfElt[0]->y() - pointsOfElt[1]->y();
      normal.y() = pointsOfElt[1]->x() - pointsOfElt[0]->x();
      normal.z() = 0.;
      normal *= 1. / normal.norm(); 

      break;

    case Triangle:

      MathUtilities::CrossProduct(normal.getCoor(), (*pointsOfElt[0] - *pointsOfElt[1]).getCoor(), (*pointsOfElt[0] - *pointsOfElt[2]).getCoor());

      normal *= 1. / normal.norm(); 

      break;

    case NullShape:
    case Node:
    case Quadrilateral: 
    case Tetrahedron:
    case Hexahedron:
    case Prism:
    case Pyramid:

      FEL_ERROR("Not implemented for this element Shape")
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::triNormal(const Point& p0, const Point& p1, const Point& p2, Point& norm)
{
  MathUtilities::CrossProduct(norm.getCoor(), (p1 - p0).getCoor(), (p2 - p0).getCoor());
  return;
}

/***********************************************************************************/
/***********************************************************************************/

double GeoUtilities::tetraVolume(const Point& p0, const Point& p1, const Point& p2, const Point& p3)
{
  Point cross(0.); 

  Point p0p1( p1 - p0 );
  Point p0p2( p2 - p0 );
  Point p0p3( p3 - p0 );

  MathUtilities::CrossProduct(cross.getCoor(), p0p2.getCoor(), p0p3.getCoor());

  return std::inner_product(p0p1.getCoor().begin(), p0p1.getCoor().end(), cross.getCoor().begin(), 0.) / 6.;
}

/***********************************************************************************/
/***********************************************************************************/

double GeoUtilities::triVolume(const Point& p0, const Point& p1, const Point& p2, const Point& norm)
{
  Point cross;
  MathUtilities::CrossProduct(cross.getCoor(), (p1 - p0).getCoor(), (p2 - p0).getCoor());

  return 0.5 * (cross * norm);
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::tetraBary(const std::vector<Point*>& eltPoints, const Point& point, std::array<double, 4>& bary)
{
  // bary.resize(4);

  bary[0] = tetraVolume(*eltPoints[0], *eltPoints[1], *eltPoints[2], point);
  bary[1] = tetraVolume(*eltPoints[0], *eltPoints[3], *eltPoints[1], point);
  bary[2] = tetraVolume(*eltPoints[1], *eltPoints[3], *eltPoints[2], point);
  bary[3] = tetraVolume(*eltPoints[0], *eltPoints[2], *eltPoints[3], point);
  
  return;  
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::triBary(const std::vector<Point*>& eltPoints, const Point& point, const Point& norm, std::array<double, 3>& bary)
{
  // bary.resize(4);

  bary[0] = triVolume(*eltPoints[0], *eltPoints[1], point, norm);
  bary[1] = triVolume(*eltPoints[1], *eltPoints[2], point, norm);
  bary[2] = triVolume(*eltPoints[2], *eltPoints[0], point, norm);
  
  return;  
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::projectOnGeomEntity(const felInt dim, const std::vector<Point*>& eltPoints, const Point& point, Point& projection)
{
  switch ( dim ) {

    case 2 :
      FEL_ASSERT( eltPoints.size() >= 2 );
      GeoUtilities::projectOnLine(eltPoints, point, projection);
      break;

    case 3 :
      FEL_ASSERT( eltPoints.size() >= 3 );
      GeoUtilities::projectOnPlane(eltPoints, point, projection);
      break;

    default:
      FEL_ERROR("Not implemented yet!");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::projectOnLine(const std::vector<Point*>& eltPoints, const Point& point, Point& projection)
{
  Point edge = *eltPoints[1] - *eltPoints[0];
  Point vec0 = point - *eltPoints[0];

  // Normaliza edge
  edge /= edge.norm();

  // Compute projection
  projection = *eltPoints[0] + (edge * vec0) * edge;
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::projectOnPlane(const std::vector<Point*>& eltPoints, const Point& point, Point& projection)
{
  // Compute plane normal
  Point normal;
  MathUtilities::CrossProduct(normal.getCoor(), (*eltPoints[0] - *eltPoints[1]).getCoor(), (*eltPoints[0] - *eltPoints[2]).getCoor());
  normal *= 1. / normal.norm(); 

  // Projection of the point on the plane
  double coeff = normal * ( point - (*eltPoints[0]) );

  // Compute the coordinates of the point projected on the plane
  projection = point - normal * coeff;
}

/***********************************************************************************/
/***********************************************************************************/

int GeoUtilities::computeSegmentsIntersection(const Point& P0, const Point& P1, const Point& Q0, const Point& Q1, Point& intPnt0, Point& intPnt1, const double tol) 
{
  // computeSegmentsIntersection(): find the intersection of 2 finite segments
  //    Input:  two finite segments S1 and S2, made respectively of p0Seg and P1 for the first segment (given in pointors) and of Q0 and Q1 for the second segment (given in pointors)
  //    Output: intPnt0 = intersection point (when it exists) (given in pointor)
  //            intPnt1 = endpoint of intersection segment [made of intPnt0 and intPnt1] (when it exists) (given in pointor)
  //    Return: 0 = disjoint (no intersection)
  //            1 = intersect in unique point intPnt0
  //            2 = overlap in segment from intPnt0 to intPnt1

  std::array<double, 2> bary;

  Point Crss, Ctmp, Ctmp1, Ctmp2;
  const Point Seg0 = P1 - P0;
  const Point Seg1 = Q1 - Q0;
  const Point Q0P0 = P0 - Q0;

  // Test if they are parallel (includes either being a point)
  MathUtilities::CrossProduct(Crss.getCoor(), Seg0.getCoor(), Seg1.getCoor());

  const double cnorm = Crss.norm();
  const double lSeg0 = Seg0.norm();
  const double lSeg1 = Seg1.norm();

  // Check if they are degenerate points:
  // both segments are points
  if ( lSeg0 < tol && lSeg1 < tol ) {
    
    // They are distinct points
    if ( Q0P0.norm() > tol )
      return 0;

    // They are the same point
    intPnt0 = P0;                
    return 1;
  }
  
  // S1 is a single point
  if ( lSeg0 < tol ) {
    // but is not in S2
    Point proj;
    GeoUtilities::projectOnLine( {const_cast<Point*>(&Q0), const_cast<Point*>(&Q1)}, P0, proj);

    if ( P0.dist(proj) < tol && GeoUtilities::isPointInSeg( {const_cast<Point*>(&Q0), const_cast<Point*>(&Q1)}, proj, bary, tol) ) {// TODO don't like const_cast
      intPnt0 = P0;
      return 1;
    }
    return 0;
  }

  // S2 a single point
  if ( lSeg1 < tol ) {
    // but is not in S1
    Point proj;
    GeoUtilities::projectOnLine( {const_cast<Point*>(&P0), const_cast<Point*>(&P1)}, Q0, proj);
    
    if ( Q0.dist(proj) < tol && GeoUtilities::isPointInSeg({const_cast<Point*>(&P0), const_cast<Point*>(&P1)}, proj, bary, tol) ) {// TODO don't like const_cast
      intPnt0 = Q0;
      return 1;
    }

    return 0;
  }

  // The two segments are parallel or collinear
  if ( cnorm < tol * lSeg0 * lSeg1 ) {

    // They are NOT collinear
    MathUtilities::CrossProduct(Ctmp1.getCoor(), Seg0.getCoor(), Q0P0.getCoor());
    MathUtilities::CrossProduct(Ctmp2.getCoor(), Seg1.getCoor(), Q0P0.getCoor());
    if ( Ctmp1.norm() > tol*lSeg0*Q0P0.norm() || Ctmp2.norm() > tol*lSeg1*Q0P0.norm() )
      return 0;

    // They are collinear segments
    double t0, t1;

    // Compute position of P0 and P1 on Seg1
    t0 = ( P0 - Q0 ) * Seg1 / ( lSeg1 * lSeg1 );
    t1 = ( P1 - Q0 ) * Seg1 / ( lSeg1 * lSeg1 );

    // Reorient ( as if Seg0 and Seg1 were pointing in the same direction )
    if ( t0 > t1 ) 
      std::swap(t0,t1);

    // No overlap
    if ( ( t0 > 1. + tol ) || ( t1 < - tol ) )
      return 0;

    // Clip intersected segment
    t0 = t0 <    - tol ? 0 : t0;
    t1 = t1 > 1. + tol ? 1 : t1;

    // The two segments overlap in a valid subsegment
    intPnt0 = Q0 + t0 * Seg1;
    intPnt1 = Q0 + t1 * Seg1;
    return 2;
  }

  // The two segments are not in the same plane
  if ( std::fabs(Crss*Q0P0) > tol*cnorm )
    return 0;

  // The segments are skew and may intersect in a point
  // Compute the intersect parameter for the first segment
  MathUtilities::CrossProduct(Ctmp.getCoor(), Seg1.getCoor(), Q0P0.getCoor());
  const double sI = ( Ctmp * Crss ) / ( cnorm * cnorm );

  // No intersect with the first segment
  if ( sI < - tol || sI > 1. + tol )
    return 0;

  // Compute the intersect parameter for the second segment
  MathUtilities::CrossProduct(Ctmp.getCoor(), Seg0.getCoor(), Q0P0.getCoor());
  const double tI = ( Ctmp * Crss ) / ( cnorm * cnorm );

  // No intersect with the second segment
  if ( tI < - tol || tI > 1. + tol ) 
    return 0;

  // Compute the intersect point with the first segment
  intPnt0 = P0 + sI * Seg0;

  return 1;
}

/***********************************************************************************/
/***********************************************************************************/

namespace {

  int computeRayOrSegWithTriangleOrPlaneIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, bool isSeg, bool isTri, const double tol) 
  {
    // computeRayOrSegWithTriangleOrPlaneIntersection(): find the 3D intersection of a ray/segment with a triangle/plane
    //    Input:  a ray R made of two points P0 and P1, and a triangle T made of three points T0, T1 and T2
    //    Output: intPnt = intersection point (when it exists)
    //    Return: -1 = triangle is degenerate (a segment or point but not a triangle)
    //             0 = disjoint (no intersection)
    //             1 = intersection in unique point given in intPnt
    //             2 = are in the same plane

    std::array<double, 3> bary;

    Point Crss;
    const Point Seg  = P1 - P0;
    const Point Edg0 = T1 - T0;
    const Point Edg1 = T2 - T0;
    const double lSeg  = Seg.norm();
    const double lEdg0 = Edg0.norm();
    const double lEdg1 = Edg1.norm();

    //  Segment is degenerate
    if ( lSeg < tol )
      return -1;

    // We compute the normal of the triangle
    MathUtilities::CrossProduct(Crss.getCoor(), Edg0.getCoor(), Edg1.getCoor());

    // Triangle is degenerate
    if ( Crss.norm() < tol * lEdg0 * lEdg1 )
      return -1;

    // Normalize the normal vector
    Crss /= Crss.norm();

    const double N = Crss * ( T0 - P0 ); 
    const double D = Crss * Seg;

    // Ray is parallel to the plane
    if ( std::fabs(D) < tol * lSeg ) {
      // Ray lies in the plane
      if ( std::fabs(N) < tol  * lEdg0 * lEdg1 )
        return 2;
      // Ray disjoint from the plane
      else
        return 0;
    }

    // Get intersect point of ray with plane
    const double R = N / D;

    // If the intersection is done with a finite segment
    if ( isSeg ) { 
      if ( ( R < - tol ) || ( R > 1 + tol ) ) {
        // No intersection
        return 0;
      }
    }

    // Compute intersection point
    intPnt = P0 + R * Seg;

    if ( !isTri )
      return 1;

    // is intPnt inside the triangle?
    if ( GeoUtilities::isPointInTri({const_cast<Point*>(&T0),const_cast<Point*>(&T1),const_cast<Point*>(&T2)}, intPnt, bary, tol) ) // TODO don't like const_cast
      return 1;

    return 0;
  }

}

/***********************************************************************************/
/***********************************************************************************/

int GeoUtilities::computeRayTriangleIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol)
{
  return computeRayOrSegWithTriangleOrPlaneIntersection(P0, P1, T0, T1, T2, intPnt, false, true, tol);
}

/***********************************************************************************/
/***********************************************************************************/

int GeoUtilities::computeSegmentTriangleIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol)
{
  return computeRayOrSegWithTriangleOrPlaneIntersection(P0, P1, T0, T1, T2, intPnt, true, true, tol);
}

/***********************************************************************************/
/***********************************************************************************/

int GeoUtilities::computeRayPlaneIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol)
{
  return computeRayOrSegWithTriangleOrPlaneIntersection(P0, P1, T0, T1, T2, intPnt, false, false, tol);
}

/***********************************************************************************/
/***********************************************************************************/

int GeoUtilities::computeSegmentPlaneIntersection(const Point& P0, const Point& P1, const Point& T0, const Point& T1, const Point& T2, Point& intPnt, const double tol)
{
  return computeRayOrSegWithTriangleOrPlaneIntersection(P0, P1, T0, T1, T2, intPnt, true, false, tol);
}

/***********************************************************************************/
/***********************************************************************************/

int GeoUtilities::computePlanesIntersection(const Point& P0, const Point& P1, const Point& P2, const Point& Q0, const Point& Q1, const Point& Q2, Point& p0Line, Point& p1Line, const double tol) 
{
  // computePlanesIntersection(): find the 3D intersection of two planes
  //    Input:  two planes P (= plane0) and Q (= plane1) defined respectively by P0, P1 and P2 for the first plane (given in pointors) and Q0, Q1 and Q2 for the second plane
  //    Output: p0Line and p1Line = the intersection line (when it exists) defined by the two points p0Line and p1Line
  //    Return: 0 = disjoint (no intersection point)
  //            1 = the two planes coincide
  //            2 = intersection in the unique line L defined by p0Line and p1Line

  Point normalP, normalQ, intPnt, crss;

  // we compute the normal of the first plane
  MathUtilities::CrossProduct(normalP.getCoor(), (P1-P0).getCoor(), (P2-P0).getCoor());

  // we compute the normal of the second plane
  MathUtilities::CrossProduct(normalQ.getCoor(), (Q1-Q0).getCoor(), (Q2-Q0).getCoor());

  // we compute the cross product of the two normals
  MathUtilities::CrossProduct(crss.getCoor(), normalP.getCoor(), normalQ.getCoor());

  // test if the two planes are parallel
  if ( crss.norm() < tol ) {

    // test if disjoint or coincide
    if ( std::fabs( normalP * (Q0 - P0) ) < tol )
      // plane0 and plane1 coincide
      return 1;
    else
      // plane0 and plane1 are disjoint
      return 0;
  }

  // plane0 and plane1 intersect in a line
  auto& cpCoor = crss.getCoor();
  const int maxc = std::distance(cpCoor.begin(), std::max_element(cpCoor.begin(), cpCoor.end(), [](double a, double b) { return std::fabs(a) < std::fabs(b); } ) );

  // next, to get a point on the intersect line
  // zero the max coord, and solve for the other two
  const double d1 = - ( normalP * P0 );
  const double d2 = - ( normalQ * Q0 );

  // select max coordinate
  switch ( maxc ) {

    // intersect with x = 0
    case 0:
      intPnt.coor(0) = 0;
      intPnt.coor(1) = ( + d2*normalP.coor(2) - d1*normalQ.coor(2) ) / crss.coor(0);
      intPnt.coor(2) = ( - d2*normalP.coor(1) + d1*normalQ.coor(1) ) / crss.coor(0);
      break;

    // intersect with y = 0
    case 1:
      intPnt.coor(0) = ( - d2*normalP.coor(2) + d1*normalQ.coor(2) ) / crss.coor(1);
      intPnt.coor(1) = 0;
      intPnt.coor(2) = ( + d2*normalP.coor(0) - d1*normalQ.coor(0) ) / crss.coor(1);
      break;

    // intersect with z = 0
    case 2:
      intPnt.coor(0) = ( + d2*normalP.coor(1) - d1*normalQ.coor(1) ) / crss.coor(2);
      intPnt.coor(1) = ( - d2*normalP.coor(0) + d1*normalQ.coor(0) ) / crss.coor(2);
      intPnt.coor(2) = 0;
      break;
  }
  
  //L.P0 = iP;
  //L.P1 = iP + u;
  p0Line = intPnt;
  p1Line = intPnt + crss;

  return 2;
}

/***********************************************************************************/
/***********************************************************************************/

void GeoUtilities::findMatchingPoints(GeometricMeshRegion* mesh1, const std::vector<int>& label1, GeometricMeshRegion* mesh2, const std::vector<int>& label2, std::map<felInt,felInt>& link, const double tol)
{
  std::set<felInt> list1, list2;

  // Extract list of point from mesh1
  mesh1->extractVerticesFromBoundary(label1, list1);
  
  // Extract list of point from mesh2
  mesh2->extractVerticesFromBoundary(label2, list2);

  // Create map
  auto& listPoints1 = mesh1->listPoints(); 
  auto& listPoints2 = mesh2->listPoints(); 
  for (auto it1 = list1.begin(); it1 !=  list1.end(); ++it1) {
    Point& point1 = listPoints1[*it1];

    for (auto it2 = list2.begin(); it2 !=  list2.end(); ++it2) {
      Point& point2 = listPoints2[*it2];

      if ( point1.dist(point2) < tol )
        link.insert(std::make_pair(*it1, *it2));
    }
  }
}

}
