//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & V. Martin
//


#include <Geometry/neighVolumesOfFaces.hpp>

namespace felisce 
{
void NeighVolumesOfFaces::print( std::ostream& outstr, int verbose ) const {
  IGNORE_UNUSED_VERBOSE;

  outstr << GeometricMeshRegion::eltEnumToFelNameGeoEle[m_typeVolume].first
          << "->" << m_idVolume << " id loc " << m_idLocalFace << " || ";
}
}
