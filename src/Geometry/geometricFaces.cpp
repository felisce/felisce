//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Geometry/geometricFaces.hpp"
#include "Geometry/neighVolumesOfFaces.hpp"

namespace felisce {
  

  Face::Face(const std::vector <felInt>& ptOfFace, const felInt & idFace):
    m_ptrNext(nullptr),
    m_vecFace(ptOfFace),
    m_id(idFace)
  {}

  //----------------
  Face::~Face() {
    for ( std::size_t i(0); i<m_listNeighVolumes.size(); ++i)
      delete m_listNeighVolumes[i];
    m_listNeighVolumes.clear();
  }

  //----------------
  void Face::copyFace(Face& face) {
    m_vecFace = face.vecFace();
    m_id = face.id();
    std::vector < NeighVolumesOfFaces* > aus = face.listNeighVolumes();
    m_listNeighVolumes.resize(aus.size());
    for ( std::size_t i = 0; i<aus.size();++i) {
      NeighVolumesOfFaces *current = new NeighVolumesOfFaces;
      *current = *aus[i];
      m_listNeighVolumes[i] = current;
    }
    m_ptrNext = nullptr; //Why is it std::set to 0?
  }

  //----------------
  void Face::print( std::ostream& outstr, int verbose, bool printNextFace ) const {
    for (auto it_vec = m_vecFace.begin(); it_vec != m_vecFace.end(); it_vec++) {
      outstr << *it_vec << " " << std::flush;
    }
    outstr << "   " << m_id << "\t=> " << std::flush;
    for (unsigned int ineigh = 0 ; ineigh < m_listNeighVolumes.size(); ineigh++) {
      m_listNeighVolumes[ineigh]->print(outstr, verbose);
    }
    outstr << std::endl;
    if ( printNextFace ) {
      if (m_ptrNext == nullptr) {
        if ( verbose > 30 ) outstr << "-> NULL " << std::endl;
      } else {
        m_ptrNext->print(outstr, verbose);
      }
    }
  }
}
