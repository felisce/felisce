//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & V. Martin
//

#ifndef GEOMETRICNEIGHFACESOFEDGES_HPP
#define GEOMETRICNEIGHFACESOFEDGES_HPP

// System includes
#include <ostream>

// External includes

// Project includes
#include "Core/shared_pointers.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce 
{
///////////////////////////////////////
// NEIGHBOURS FACES OF EDGES CLASS //
///////////////////////////////////////

//class GeometricMeshRegion;
class NeighFacesOfEdges {
public:

  /// Pointer definition of NeighFacesOfEdges
  FELISCE_CLASS_POINTER_DEFINITION(NeighFacesOfEdges);

  typedef GeometricMeshRegion::ElementType ElementType;

  // Constructor / Destructor
  // ========================
  NeighFacesOfEdges() = default;
  ~NeighFacesOfEdges() = default;
  
  // getter
  // ======
  inline const felInt& idFace() const        {
    return m_idFace;
  }
  inline const ElementType& typeFace() const {
    return m_typeFace;
  }
  inline const int& idLocalEdge() const      {
    return m_idLocalEdge;
  }

  // setter
  // ======
  inline felInt& idFace()        {
    return m_idFace;
  }
  inline ElementType& typeFace() {
    return m_typeFace;
  }
  inline int& idLocalEdge()      {
    return m_idLocalEdge;
  }

  // print a face neighbour
  // ======================
  void print(  std::ostream& outstr = std::cout, int verbose = 0 ) const;

private:
  felInt      m_idFace;        //!face's number per eltType
  ElementType m_typeFace;      //!eltType of the face
  int         m_idLocalEdge;   //!local number of the edge in the face
};
}

#endif
