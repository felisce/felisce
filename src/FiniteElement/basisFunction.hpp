//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef BASISFUNCTION_H
#define BASISFUNCTION_H

// System includes
#include <string>
#include <cassert>

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce
{
/*!
  \class BasisFunction
  \authors J-F. Gerbeau
  \brief Class implementing the basis functions
  */

class BasisFunction {
protected:
  std::string m_name;
  const int m_size; //!< number of basis functions
  const int m_numCoor; //!< number of coordinates used by the functions
  const int m_dim; //!< dimension of the result (in general 1)
  const FunctionXYZ* m_phi; //!< pointer on the basis functions
  const FunctionXYZ* m_dPhi; //!< pointer on the derivatives of the basis functions
  const FunctionXYZ* m_d2Phi; //!< pointer on the second derivatives of the basis functions
public:
  BasisFunction(const std::string& name,int size,int numCoor,const FunctionXYZ* phi, const FunctionXYZ* dPhi,const FunctionXYZ* d2Phi);
  BasisFunction(const std::string& name,int size,int numCoor,int dim,const FunctionXYZ* phi,const FunctionXYZ* dPhi,const FunctionXYZ* d2Phi);
  inline const std::string& name() const {
    return m_name;
  }
  inline int size() const {
    return m_size;
  }
  inline int numCoor() const {
    return m_numCoor;
  }
  void print(int verbose,std::ostream& c=std::cout) const;
  //! return the value of the i-th basis function on point (x,y,z)
  inline double phi( int i, const Point& pt) const {
    return m_phi[ i ] (pt);
  }
  // \todo : change x,y,z by Point in the following functions :
  inline double phi( const int i, const double x, const double y, const double z ) const {
    FEL_ASSERT(m_dim == 1 && i < m_size);
    return m_phi[ i ] ( Point(x, y, z) );
  }
  inline double phi( const int i, const double x, const double y, const double z, const int idim) const {
    FEL_ASSERT(idim < m_dim && i < m_size);
    return m_phi[ m_dim*idim + i ] ( Point(x, y, z) );
  }
  //! return the value of the icoor-th derivative of the i-th basis function on point (x,y,z)
  inline double dPhi( const int i, const int icoor, const double x, const double y, const double z ) const {
    FEL_ASSERT(m_dim == 1);
    FEL_ASSERT( i < m_size && icoor < m_numCoor );
    return m_dPhi[ i * m_numCoor + icoor ] ( Point(x, y, z) );
  }
  inline double dPhi( const int i, const int icoor, const Point& pt) const {
    FEL_ASSERT(m_dim == 1);
    FEL_ASSERT( i < m_size && icoor < m_numCoor );
    return m_dPhi[ i * m_numCoor + icoor ] (pt);
  }
  //!  return the value of the (icoor,jcoor)-th second derivative of the i-th basis function on point (x,y,z)
  inline double d2Phi( const int i, const int icoor, const int jcoor, const double x, const double y, const double z ) const {
    FEL_ASSERT(m_dim == 1);
    FEL_ASSERT( i < m_size && icoor < m_numCoor && jcoor < m_numCoor );
    return m_d2Phi[ ( i * m_numCoor + icoor ) * m_numCoor + jcoor ] ( Point(x, y, z) );
  }
  inline double divPhi(const int i, const Point& pt) {
    (void) pt;
    (void) i;
    FEL_ASSERT(m_dim == m_numCoor && i<m_size);
    return 0.; // TODO !
  }

};

//=============================================================================================

extern const BasisFunction basisFunctionNULL;
extern const BasisFunction basisFunction0d;
extern const BasisFunction basisFunction1dP1;
extern const BasisFunction basisFunction1dP1b;
extern const BasisFunction basisFunction1dP2;
extern const BasisFunction basisFunction1dP3H;
extern const BasisFunction basisFunction2dP1;
extern const BasisFunction basisFunction2dP1b;
extern const BasisFunction basisFunction2dP2;
extern const BasisFunction basisFunction2dQ1;
extern const BasisFunction basisFunction2dP1xP2;
extern const BasisFunction basisFunction2dQ1b;
extern const BasisFunction basisFunction2dQ2;
extern const BasisFunction basisFunction2dQ2c;
extern const BasisFunction basisFunction3dP1;
extern const BasisFunction basisFunction3dP1b;
extern const BasisFunction basisFunction3dP2;
extern const BasisFunction basisFunction3dQ1;
extern const BasisFunction basisFunction3dQ1b;
extern const BasisFunction basisFunction3dQ2;
extern const BasisFunction basisFunction3dQ2c;
extern const BasisFunction basisFunction3dR1; // prism
extern const BasisFunction basisFunction3dP1xP2; // prism
extern const BasisFunction basisFunction3dR2;
extern const BasisFunction basisFunction3dRT0Tetra;

}

#endif
