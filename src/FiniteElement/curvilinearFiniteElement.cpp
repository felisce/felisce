//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes
#include <iomanip>
#include <array>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/felisceTools.hpp"
#include "FiniteElement/curvilinearFiniteElement.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce
{
CurvilinearFiniteElement::CurvilinearFiniteElement(
  const RefElement& refEle,
  const GeoElement& geoEle,
  const DegreeOfExactness& degOfExactness
  ): CurBaseFiniteElement(refEle, geoEle, degOfExactness)
{
  // Set the number of coordinates
  const int numCoor = FelisceParam::instance().numCoor;
  if (numCoor == 0) {
    m_numCoor = m_geoEle.numCoor()+1;
  } else { // We set manually. TODO: Implement line in 3D
    m_numCoor = numCoor;
  }

  FEL_ASSERT( m_numCoor <= 3 ); // curvilinear element not devised for 3D yet. VM

  m_covBasis = std::vector<UBlasMatrix>(m_numQuadraturePoint);
  m_F0.resize(m_numQuadraturePoint);
  m_F1.resize(m_numQuadraturePoint);
  m_covCompleteBasis = std::vector<UBlasMatrix>(m_numQuadraturePoint);
  contravariantCompleteBasis = std::vector<UBlasMatrix>(m_numQuadraturePoint);
  tangent = std::vector<UBlasMatrix>(m_numQuadraturePoint);
  normal = std::vector<UBlasVector>(m_numQuadraturePoint);
  covMetric = std::vector<UBlasMatrix>(m_numQuadraturePoint);
  contravariantMetric = std::vector<UBlasMatrix>(m_numQuadraturePoint);

  m_point.resize(m_numPoint,m_numCoor);
  m_tangentVertice.resize(m_numCoor - 1);
  for(int i=0 ; i<m_numCoor -1 ; i++)
    m_tangentVertice[i].resize(m_numPoint,m_numCoor);
  m_normalVertice.resize(m_numPoint,m_numCoor);

  for(int ig=0; ig<m_numQuadraturePoint; ig++) {
    m_covBasis[ig].resize(m_numRefCoor,m_numCoor);
    m_F0[ig].resize(m_numCoor,m_numCoor);
    m_F1[ig].resize(m_numCoor,m_numCoor);
    m_covCompleteBasis[ig].resize(m_numCoor,m_numCoor);
    contravariantCompleteBasis[ig].resize(m_numCoor,m_numCoor);
    tangent[ig].resize(m_numCoor - 1,m_numCoor);
    normal[ig].resize(m_numCoor);
    covMetric[ig].resize(m_numRefCoor,m_numRefCoor);
    contravariantMetric[ig].resize(m_numRefCoor,m_numRefCoor);
  }
}

/***********************************************************************************/
/***********************************************************************************/

CurvilinearFiniteElement::~CurvilinearFiniteElement()
{
  for(int i = 0; i < m_numQuadraturePoint; i++) {
    m_covBasis[i].clear();
    m_covCompleteBasis[i].clear();
    contravariantCompleteBasis[i].clear();
    covMetric[i].clear();
    contravariantMetric[i].clear();
    tangent[i].clear();
    normal[i].clear();
    m_F0[i].clear();
    m_F1[i].clear();
  }
  m_covBasis.clear();
  m_covCompleteBasis.clear();
  contravariantCompleteBasis.clear();
  covMetric.clear();
  contravariantMetric.clear();
  tangent.clear();
  normal.clear();
  m_tangentVertice.clear();
  m_normalVertice.clear();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeCovariantBasis()
{
  // Derivatives of geo map:
  for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
    noalias(m_covBasis[ig]) = prod(m_dPhiGeo[ig],m_point);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeMeas()
{
  for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
    noalias(covMetric[ig]) = prod(m_covBasis[ig],trans(m_covBasis[ig]));
  }
  switch (m_numRefCoor) {  
    case 0:
      for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
        m_meas[ig] = 1.0;
        weightMeas(ig) = 1.0;
      }
      break;
    case 1:
      for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
        m_meas[ig] = std::sqrt(covMetric[ig](0,0));
        weightMeas(ig) = m_meas(ig) * m_quadratureRule->weight(ig);
      }
      break;
    case 2:
      for(int ig = 0; ig <m_numQuadraturePoint; ig++) {
        m_meas[ig] = std::sqrt(covMetric[ig](0,0)*covMetric[ig](1,1) - covMetric[ig](0,1)*covMetric[ig](1,0));
        weightMeas(ig) = m_meas(ig) * m_quadratureRule->weight(ig);
      }
      break;
    default:
      FEL_ERROR("CurvilinearFiniteElement::computeCovariantBasis: numRefCoor must be 0, 1 or 2");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeContravariantMetric()
{
  UBlasMatrix inv_cov_metric(m_numRefCoor,m_numRefCoor);
  double det;
  for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
    MathUtilities::InvertMatrix(covMetric[ig], inv_cov_metric, det);
    contravariantMetric[ig].assign(UBlasIdentityMatrix (m_numRefCoor));
    contravariantMetric[ig] = prod(inv_cov_metric, contravariantMetric[ig]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeContravariantBasis()
{
  UBlasMatrix inv_cov_compete_basis(m_numCoor,m_numCoor);
  double det;
  for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
    for(int iRefcoor=0; iRefcoor<m_numRefCoor; iRefcoor++) {
      for(int jcoor=0; jcoor<m_numCoor; jcoor++) {
        m_covCompleteBasis[ig](iRefcoor,jcoor) = m_covBasis[ig](iRefcoor,jcoor);
      }
    }
    for(int jcoor=0; jcoor<m_numCoor; jcoor++) {
      m_covCompleteBasis[ig](m_numRefCoor,jcoor) = normal[ig](jcoor);
    }

    MathUtilities::InvertMatrix(m_covCompleteBasis[ig], inv_cov_compete_basis, det);
    contravariantCompleteBasis[ig].assign(UBlasIdentityMatrix(m_numCoor));
    contravariantCompleteBasis[ig] = prod(inv_cov_compete_basis, contravariantCompleteBasis[ig]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeTangentNormal()
{
  computeTangent();
  computeNormal();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeNormal()
{
  double norm,n1,n2,n3;
  switch (m_numRefCoor) {
  case 0:
    break;
  case 1:
    for(int ig = 0; ig <m_numQuadraturePoint; ig++) {
      n1 =   m_covBasis[ig](0,1);
      n2 = - m_covBasis[ig](0,0);
      norm = std::sqrt(n1*n1+n2*n2);
      normal[ig](0) = m_sign*n1/norm;
      normal[ig](1) = m_sign*n2/norm;
    }
    break;
  case 2:
    for(int ig = 0; ig <m_numQuadraturePoint; ig++) {
      n1 = m_covBasis[ig](0,1) * m_covBasis[ig](1,2) - m_covBasis[ig](0,2)*m_covBasis[ig](1,1);
      n2 = m_covBasis[ig](0,2) * m_covBasis[ig](1,0) - m_covBasis[ig](0,0)*m_covBasis[ig](1,2);
      n3 = m_covBasis[ig](0,0) * m_covBasis[ig](1,1) - m_covBasis[ig](0,1)*m_covBasis[ig](1,0);
      norm = std::sqrt( n1 * n1 + n2 * n2 + n3 * n3 );
      normal[ig](0) = m_sign*n1 / norm;
      normal[ig](1) = m_sign*n2 / norm;
      normal[ig](2) = m_sign*n3 / norm;
    }
    break;
  default:
    FEL_ERROR("CurvilinearFiniteElement::computeNormal: numRefCoor must be 0, 1 or 2");
    break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeTangent()
{
  double norm;
  for(int ig = 0; ig <m_numQuadraturePoint; ig++ ) {
    for(int irefcoor=0; irefcoor<m_numRefCoor; irefcoor++) {
      norm = 0.0;
      for(int jcoor=0; jcoor<m_numCoor; jcoor++) {
        norm += m_covBasis[ig](irefcoor,jcoor)*m_covBasis[ig](irefcoor,jcoor);
      }
      norm = std::sqrt(norm);
      for(int jcoor=0; jcoor<m_numCoor; jcoor++) {
        tangent[ig](irefcoor,jcoor) = m_covBasis[ig](irefcoor,jcoor)/norm;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::evaluateBasisFunctionsInPoint(const Point* p, std::vector<double>& values) const
{
  // TODO: update these multiplications for being more efficient. Specially use bounded matrix
  FEL_ASSERT(m_numCoor==3);
  // 0. Computing x0
  // For triangles is just m_point( 0, : );
  // For quadrangles this is not true since, for csi = 0 we do not obtain a point, but we obtain
  // the center of the quadrangle
  UBlasBoundedVector<double, 3> x0;
  for(std::size_t k = 0; k<3; k++) {
    x0(k)=0;
    for(int i(0); i<m_numDof; i++) {
      x0(k)+=m_geoEle.basisFunction().phi(i, 0, 0, 0 )*m_point(i,k);
    }
  }

  // 1. Inverse the mapping.
  UBlasBoundedVector<double, 3> q;
  for(std::size_t k = 0; k<3; k++) {
      q(k) = p->coor(k) - x0(k);
  }

  const UBlasMatrix jacT= prod(m_dPhiGeo[0],m_point);

  const UBlasMatrix jac = trans(jacT);
  const UBlasVector qtilde = prod(jacT,q);

  const UBlasBoundedMatrix<double, 2, 2> A = prod(jacT,jac);
  double d;
  UBlasBoundedMatrix<double, 2, 2> invA;
  MathUtilities::InvertMatrix(A, invA, d);

  // the point has been mapped from R^3 to the reference element.
  const UBlasVector csi=prod(invA,qtilde);

  // 2. evalute basis functions here
  for(int i=0; i<m_numDof; i++ ) {
    values[i]=m_refEle.basisFunction().phi(i, csi[0], csi[1], /*z*/ 0 );
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::update(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = false;
  m_hasNormal = false;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::update(const int id, const UBlasMatrix& point, const std::vector<int>& ipt)
{
  updatePoint(point, ipt);
  m_hasMeas = false;
  m_hasNormal = false;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeas(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = false;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeas(const int id, const UBlasMatrix& point, const std::vector<int>& ipt)
{
  updatePoint(point, ipt);
  m_hasMeas = true;
  m_hasNormal = false;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasQuadPt(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = false;
  m_hasTangent = false;
  m_hasQuadPtCoor = true;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeCurrentQuadraturePoint();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormal(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeNormal();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormalQuadPt(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeNormal();
  computeCurrentQuadraturePoint();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormal(const int id, const UBlasMatrix& point, const std::vector<int>& ipt)
{
  updatePoint(point, ipt);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeNormal();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormalContra(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_hasOriginalQuadPoint = true;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeNormal();
  computeContravariantMetric();
  computeContravariantBasis();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormalContra(const int id, const UBlasMatrix& point, const std::vector<int>& ipt)
{
  updatePoint(point, ipt);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeNormal();
  computeContravariantMetric();
  computeContravariantBasis();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormalTangent(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = true;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeTangentNormal();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateMeasNormalTangent(const int id, const UBlasMatrix& point, const std::vector<int>& ipt)
{
  updatePoint(point, ipt);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = true;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeCovariantBasis();
  computeMeas();
  computeTangentNormal();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateBasisAndNormalContra(const int id, const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_hasOriginalQuadPoint = true;
  m_currentId = id;

  std::vector<Point> pts(m_numQuadraturePoint);
  for(felInt ipt=0; ipt<m_numQuadraturePoint; ++ipt)
    pts[ipt] = m_quadratureRule->quadraturePoint(ipt);

  // recompute the basis function at the original quadrature point
  updateBasisWithQuadPoint(pts);

  // normal contra
  computeCovariantBasis();
  computeMeas();
  computeNormal();
  computeContravariantMetric();
  computeContravariantBasis();
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateSubElementMeasNormal(
  const int id,
  const std::vector<Point*>& ptElem,
  const std::vector<Point*> ptSubElem
  )
{
  updatePoint(ptElem);
  m_hasMeas = true;
  m_hasNormal = true;
  m_hasTangent = false;
  m_hasQuadPtCoor = false;
  m_hasOriginalQuadPoint = false;
  m_currentId = id;

  // find the coordinate of the sub element in the reference element of the element
  std::vector<Point*> ptSubRef(ptSubElem.size());
  switch(m_geoEle.shape().typeShape()) {
  case Segment: {
    for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
      ptSubRef[ipt] = new Point();
      double dx, dy;
      dx =  std::abs(ptElem[1]->x() - ptElem[0]->x());
      dy =  std::abs(ptElem[1]->y() - ptElem[0]->y());
      if(dx >= dy)
        ptSubRef[ipt]->x() = 2.*(ptSubElem[ipt]->x() - ptElem[0]->x())/(ptElem[1]->x() - ptElem[0]->x()) - 1;
      else
        ptSubRef[ipt]->x() = 2.*(ptSubElem[ipt]->y() - ptElem[0]->y())/(ptElem[1]->y() - ptElem[0]->y()) - 1;

      ptSubRef[ipt]->y() = 0;
    }
    break;
  }
  case NullShape:
  case Node:
  case Triangle: {
    const double det1 = (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->x() - ptElem[0]->x())
                - (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->y() - ptElem[0]->y());

    const double det2 = (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->z() - ptElem[0]->z())
                - (ptElem[2]->z() - ptElem[0]->z())*(ptElem[1]->x() - ptElem[0]->x());

    const double det3 = (ptElem[2]->z() - ptElem[0]->z())*(ptElem[1]->y() - ptElem[0]->y())
                - (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->z() - ptElem[0]->z());

    if(std::abs(det1) >= std::abs(det2) && std::abs(det1) >= std::abs(det3)){
      for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
        ptSubRef[ipt] = new Point();

        ptSubRef[ipt]->x() = (ptElem[2]->y() - ptElem[0]->y())*(ptSubElem[ipt]->x() - ptElem[0]->x())/det1
                            + (ptElem[0]->x() - ptElem[2]->x()) * (ptSubElem[ipt]->y() - ptElem[0]->y())/det1;

        ptSubRef[ipt]->y() = (ptElem[0]->y() - ptElem[1]->y())*(ptSubElem[ipt]->x() - ptElem[0]->x())/det1
                            + (ptElem[1]->x() - ptElem[0]->x())*(ptSubElem[ipt]->y() - ptElem[0]->y())/det1;

        ptSubRef[ipt]->z() = 0.0;
      }
    } else if(std::abs(det2) >= std::abs(det1) && std::abs(det2) >= std::abs(det3)){
      for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
        ptSubRef[ipt] = new Point();

        ptSubRef[ipt]->x() = (ptElem[2]->x() - ptElem[0]->x())*(ptSubElem[ipt]->z() - ptElem[0]->z())/det2
                            + (ptElem[0]->z() - ptElem[2]->z()) * (ptSubElem[ipt]->x() - ptElem[0]->x())/det2;

        ptSubRef[ipt]->y() = (ptElem[0]->x() - ptElem[1]->x())*(ptSubElem[ipt]->z() - ptElem[0]->z())/det2
                            + (ptElem[1]->z() - ptElem[0]->z())*(ptSubElem[ipt]->x() - ptElem[0]->x())/det2;

        ptSubRef[ipt]->z() = 0.0;
      }
    } else {
      for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
        ptSubRef[ipt] = new Point();

        ptSubRef[ipt]->x() = (ptElem[2]->z() - ptElem[0]->z())*(ptSubElem[ipt]->y() - ptElem[0]->y())/det3
                            + (ptElem[0]->y() - ptElem[2]->y()) * (ptSubElem[ipt]->z() - ptElem[0]->z())/det3;

        ptSubRef[ipt]->y() = (ptElem[0]->z() - ptElem[1]->z())*(ptSubElem[ipt]->y() - ptElem[0]->y())/det3
                            + (ptElem[1]->y() - ptElem[0]->y())*(ptSubElem[ipt]->z() - ptElem[0]->z())/det3;

        ptSubRef[ipt]->z() = 0.0;
      }
    }

    break;
  }
  case Quadrilateral:
  case Tetrahedron:
  case Hexahedron:
  case Prism:
  case Pyramid: {
    FEL_ERROR("The transformation is not implemented for this type of mesh cells");
    break;
  }
  }

  // create the curvilinear finite element for the sub element and update it
  CurvilinearFiniteElement feSubElt(m_refEle, m_geoEle, (DegreeOfExactness) m_quadratureRule->degreeOfExactness());

  feSubElt.updateMeasQuadPt(id, ptSubRef);

  // delete sub element coordinates
  for(std::size_t ipt=0; ipt<ptSubRef.size(); ++ipt)
    delete ptSubRef[ipt];
  ptSubRef.clear();

  // recompute the basis function at the new quadrature points for the element
  updateBasisWithQuadPoint(feSubElt.currentQuadPoint);

  // update the derivatives for the element
  computeCovariantBasis();
  computeMeas();
  computeNormal();
  // change the weight of the quadrature point
  for(int ig=0; ig<feSubElt.numQuadraturePoint(); ++ig) {
    weightMeas(ig) = m_meas(ig) * feSubElt.weightMeas(ig);
  }
  for(int ig=feSubElt.numQuadraturePoint(); ig<m_numQuadraturePoint; ++ig) {
    weightMeas(ig) = 0.0;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "CurvilinearFiniteElement: " << "\n";
    m_printBase(verbose, c);

    if( m_hasPoint && verbose > 1 ) c << " point = " << m_point << "\n";
    if( m_hasMeas && verbose > 1 )  c << " meas   = " << measure() << "\n";
    if( verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " phi[ig=" <<ig<< "] = " << phi[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " phiGeo[ig=" <<ig<< "] = " << m_phiGeo[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " dphiGeo[ig=" <<ig<< "] = " << m_dPhiGeo[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " dphiRef[ig=" <<ig<< "] = " << m_dPhiRef[ig] << "\n";
      }
    }
    if( m_hasNormal && verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " normal[ig=" <<ig<< "] = " << normal[ig] << "\n";
      }
    }
    if( m_hasTangent && verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " tangent[ig=" <<ig<< "] = " << tangent[ig] << "\n";
      }
    }
    if( m_hasNormal && verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " covBasis[ig=" <<ig<< "] = " << m_covBasis[ig] << "\n";
      }
    }
    if( m_hasNormal && verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " covMetric[ig=" <<ig<< "] = " << covMetric[ig] << "\n";
      }
    }
    if( m_hasQuadPtCoor && verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " contravariantMetric[ig=" <<ig<< "] = " << contravariantMetric[ig] << "\n";
      }
    }
    if( m_hasQuadPtCoor && verbose > 2 ) {
      for(int ig = 0; ig <numQuadraturePoint(); ig++ ) {
        c << " currentQuadPoint[ig=" <<ig<< "] : ";
        currentQuadPoint[ig].print(verbose,c);
      }
    }
    std::cout << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateNormalTangentMesh (const std::vector<Point*>& normalFE, const std::vector< std::vector<Point*> >& tangentFE)
{
  for(int i=0; i<m_numPoint; i++) {
    for(int icoor=0; icoor<m_numCoor; icoor++) {
      m_normalVertice(i,icoor) = normalFE[i]->coor(icoor);
      for(int j=0; j<m_numCoor - 1; j++)
        m_tangentVertice[j](i,icoor) = tangentFE[j][i]->coor(icoor);
    }
  }
  m_hasNormal = true;
  m_hasTangent = true;
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::compute_weightMeas()
{
  for(int ir = 0; ir<m_numQuadraturePoint; ir++)
    weightMeas[ir] =  m_quadratureRule->weight(ir);
  m_hasMeas = true;
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::compute_F0_F1_beam(const double thickness)
{
  int iRefcoor=0; // = r
  double sum1, sum2, sum3;
  for(int ir = 0; ir<m_numQuadraturePoint; ir++) {
    m_F1[ir].clear();

    for(int jcoor=0; jcoor<m_numCoor; jcoor++){
      sum1= 0.0;
      sum2= 0.0;
      sum3= 0.0;
      for(int iNode=0; iNode<m_numPoint; iNode++){
        sum1 += m_dPhiGeo[ir](iRefcoor,iNode)*m_point(iNode,jcoor);
        sum2 += 0.5*thickness*m_phiGeo[ir](iNode)*m_normalVertice(iNode,jcoor);
        sum3 += 0.5*thickness*m_dPhiGeo[ir](iRefcoor,iNode)*m_normalVertice(iNode,jcoor);
      }
      m_F0[ir](0,jcoor) = sum1;
      m_F0[ir](1,jcoor) = sum2;
      m_F1[ir](0,jcoor) = sum3;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::compute_F0_F1_thinShell(const double thickness)
{
  std::array<double, 5> sum;

  for(int ir = 0; ir<m_numQuadraturePoint; ir++) {
    m_F1[ir].clear();
    for(int jcoor=0; jcoor<m_numCoor; jcoor++){
      for(int i=0 ; i<5; i++)
        sum[i]= 0.0;
      for(int iNode=0; iNode<m_numPoint; iNode++){
        // iRefcoor = 0 -> g_r // iRefcoor = 1 -> g_s
        for(int iRefcoor=0; iRefcoor<m_numRefCoor; iRefcoor++) {
          sum[2*iRefcoor] += m_dPhiGeo[ir](iRefcoor,iNode)*m_point(iNode,jcoor);
          sum[2*iRefcoor+1] += 0.5*thickness*m_dPhiGeo[ir](iRefcoor,iNode)*m_normalVertice(iNode,jcoor);
        }
        // g_z
        sum[4] += 0.5*thickness*m_phiGeo[ir](iNode)*m_normalVertice(iNode,jcoor);
      }
      for(int iRefcoor=0; iRefcoor<m_numRefCoor; iRefcoor++){
        m_F0[ir](iRefcoor,jcoor) = sum[2*iRefcoor];
        m_F1[ir](iRefcoor,jcoor) = sum[2*iRefcoor+1];
      }
      m_F0[ir](2,jcoor) = sum[4];
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::compute_Grr0_Grr1_Grr2(const UBlasMatrix& rF0, const UBlasMatrix& rF1)
{
  m_Grr0 = 0.0; m_Grr1 = 0.0; m_Grr2 = 0.0;

  for(int jcoor = 0; jcoor<m_numCoor; jcoor++) {
    m_Grr0 += rF0(0,jcoor)*rF0(0,jcoor);
    m_Grr1 += 2.*rF0(0,jcoor)*rF1(0,jcoor);
    m_Grr2 += rF1(0,jcoor)*rF1(0,jcoor);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::compute_Gss0_Gss1_Gss2(const UBlasMatrix& rF0, const UBlasMatrix& rF1)
{
    m_Gss0 = 0.0; m_Gss1 = 0.0; m_Gss2 = 0.0;

    for(int jcoor = 0; jcoor<m_numCoor; jcoor++) {
      m_Gss0 += rF0(1,jcoor)*rF0(1,jcoor);
      m_Gss1 += 2.*rF0(1,jcoor)*rF1(1,jcoor);
      m_Gss2 += rF1(1,jcoor)*rF1(1,jcoor);
    }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::compute_Grs0_Grs1_Grs2(const UBlasMatrix& rF0, const UBlasMatrix& rF1)
{
    m_Grs0 = 0.0; m_Grs1 = 0.0; m_Grs2 = 0.0;

    for(int jcoor = 0; jcoor<m_numCoor; jcoor++) {
      m_Grs0 += rF0(0,jcoor)*rF0(1,jcoor);
      m_Grs1 += rF0(0,jcoor)*rF1(1,jcoor)+rF0(1,jcoor)*rF1(0,jcoor);
      m_Grs2 += rF1(0,jcoor)*rF1(1,jcoor);
    }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::computeNormalThinShell()
{
  double sum;

  for(int ir = 0; ir<m_numQuadraturePoint; ir++) {
    for(int jcoor=0; jcoor<m_numCoor; jcoor++){
      sum= 0.0;
      for(int iNode=0; iNode<m_numPoint; iNode++){
        sum += m_phiGeo[ir](iNode)*m_normalVertice(iNode,jcoor);
      }
      normal[ir](jcoor) = sum;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateBeam (const int id, const std::vector<Point*>& point, const std::vector<Point*>& normalFE,const std::vector< std::vector<Point*> >& tangentFE, const double thickness)
{
  updatePoint(point);
  updateNormalTangentMesh (normalFE, tangentFE);
  m_hasNormal = true;
  m_hasTangent = true;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  compute_weightMeas();
  // compute convariant basis
  compute_F0_F1_beam(thickness);
}

/***********************************************************************************/
/***********************************************************************************/

void CurvilinearFiniteElement::updateThinShell(const int id, const std::vector<Point*>& point, const std::vector<Point*>& normalFE, const std::vector <std::vector<Point*> >& tangentFE, const double thickness)
{
  updatePoint(point);
  updateNormalTangentMesh (normalFE, tangentFE);
  m_hasNormal = true;
  m_hasTangent = true;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  compute_weightMeas();
  // compute covariant basis linear part
  compute_F0_F1_thinShell(thickness);
}

/***********************************************************************************/
/***********************************************************************************/

bool CurvilinearFiniteElement::isInBallRange(double* c, double r)
{
  //For each points of the element (3 for a triangle for example), compute the distance to the ball center.
  //If the distance is shorter than R, the point is inside the ball
  //We consider that the element is inside the ball if at least one element is part of the ball

    for(std::size_t numPoint = 0; numPoint < m_point.size1(); numPoint++){
      double distanceSquared = 0;
      for(std::size_t numCoor = 0 ; numCoor < m_point.size2(); numCoor++){
        const double coor = m_point(numPoint,numCoor);
        const double ballCoor = c[numCoor];
        //std::cout << coor << " - " << ballCoor << std::endl;
        distanceSquared += (coor-ballCoor)*(coor-ballCoor);
      }
      if(distanceSquared < (r*r)){
        //std::cout << "Found an element in ball range" << std::endl;
        return true;
      }
      //std::cout << distanceSquared << std::endl;
      //std::cout << std::endl;
    }
    //std::cout << "Did not found any element in ball range" << std::endl;
    return false;
}

} // end class curvilinearFiniteElement
