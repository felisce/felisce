//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: J-F. Gerbeau
//

// System includes
#include <string>
#include <cmath>
#include <iostream>

// External includes

// Project includes
#include "FiniteElement/quadratureRule.hpp"
#include "FiniteElement/basisFunction.hpp"
#include "FiniteElement/geoElement.hpp"
#include "FiniteElement/refElement.hpp"

/*!
  \file definitionGlobalVariables.cpp
  \authors J-F. Gerbeau

  \brief Basis functions, quadrature rules, geometric and reference elements

  This file contains the definition of all the basis functions (basisFunction.hpp),
  quadrature rules (quadratureRule.hpp), geometric elements (geoElement.hpp)
  and reference elements (refElement.hpp) used in FELiScE.

  The instances of these classes must be declared as \c extern in their respective \c .hpp file and
  defined in the present file.

  \todo Add new finite elements !

  \see basisFunction.hpp, quadratureRule.hpp, geoElement.hpp, refElement.hpp

*/

namespace felisce
{
  const char *strComponent[8] = {"CompNA", "Comp1", "Comp2", "Comp12",
                                 "Comp3", "Comp13", "Comp23", "Comp123"
                                };

  const std::size_t numElementFieldType = 3;
  const char *strElementFieldType[3] = {"CONSTANT_FIELD", "DOF_FIELD", "QUAD_POINT_FIELD"};

  const std::size_t numTypeValueOfElementField = 3;
  const char *strTypeValueOfElementField[3] = {"FROM_CONSTANT", "FROM_FUNCTION", "FROM_FILE"};

  /*========================================================================
    !
    !                              BASIS FUNCTIONS
    !
    =======================================================================*/

  /************************************************************************
   *   basisFunctionNULL
   *************************************************************************/
  double basisFuncNULL(const Point&);
  double basisFuncNULL(const Point& pt) {
    (void) pt;
    return 0.;
  }

  static const FunctionXYZ _FuncNULL[] = {basisFuncNULL};
  static const FunctionXYZ _FuncDiffNULL[] = {basisFuncNULL};
  static const FunctionXYZ _FuncDiffHessNULL[] = {basisFuncNULL};

  const BasisFunction basisFunctionNULL("basisFunctionNULL",1,0,_FuncNULL,_FuncDiffNULL,_FuncDiffHessNULL);

  /************************************************************************
   *   basisFunction0d
   *************************************************************************/
  double basisFunc0d(const Point&) {
    return 1.0;
  }
  double basisFuncDiff0d(const Point&) {
    return 0.0;
  }
  static const FunctionXYZ _Func0d[] = {basisFunc0d};
  static const FunctionXYZ _FuncDiff0d[] = {basisFuncDiff0d};
  static const FunctionXYZ _FuncDiffHess0d[] = {basisFuncNULL};

  const BasisFunction basisFunction0d("basisFunction0d",1,0,_Func0d,_FuncDiff0d,_FuncDiffHess0d);

  /************************************************************************
   *   basisFunction1dP1
   *************************************************************************/
  double basis1Func1dP1(const Point&);
  double basis2Func1dP1(const Point&);
  double basis1FuncDiffr1dP1(const Point&);
  double basis2FuncDiffr1dP1(const Point&);
  double basis1FuncDiffrr1dP1(const Point&);
  double basis1Func1dP1(const Point& pt) {
    return 0.5*(1-pt.x());
  }
  double basis2Func1dP1(const Point& pt) {
    return 0.5*(1+pt.x());
  }
  double basis1FuncDiffr1dP1(const Point&) {
    return -.5;
  }
  double basis2FuncDiffr1dP1(const Point&) {
    return  .5;
  }
  double basis1FuncDiffrr1dP1(const Point&) {
    return 0.;
  }

  static const FunctionXYZ _Func1dP1[] = {basis1Func1dP1, basis2Func1dP1};
  static const FunctionXYZ _FuncDiff1dP1[] = {basis1FuncDiffr1dP1, basis2FuncDiffr1dP1};
  static const FunctionXYZ _FuncDiffHess1dP1[] = {basis1FuncDiffrr1dP1, basis1FuncDiffrr1dP1};

  const BasisFunction basisFunction1dP1("basisFunction1dP1",2,1,_Func1dP1,_FuncDiff1dP1,_FuncDiffHess1dP1);

  /************************************************************************
   *   basisFunction1dP1b = P1 + bubble
   *************************************************************************/
  double basis3Func1dP1b(const Point&);
  double basis3Func1dP1b(const Point& pt) {
    return 1.-pt.x()*pt.x();
  }

  // first derivative
  double basis3FuncDiffr1dP1b(const Point&);
  double basis3FuncDiffr1dP1b(const Point& pt) {
    return  -2.*pt.x();
  }

  // second derivative
  double basis3FuncDiffrr1dP1b(const Point&);
  double basis3FuncDiffrr1dP1b(const Point&) {
    return -2.;
  }

  static const FunctionXYZ _Func1dP1b[] = {basis1Func1dP1, basis2Func1dP1, basis3Func1dP1b};
  static const FunctionXYZ _FuncDiff1dP1b[] = {basis1FuncDiffr1dP1, basis2FuncDiffr1dP1, basis3FuncDiffr1dP1b};
  static const FunctionXYZ _FuncDiffHess1dP1b[] = {basis1FuncDiffrr1dP1, basis1FuncDiffrr1dP1, basis3FuncDiffrr1dP1b};

  const BasisFunction basisFunction1dP1b("basisFunction1dP1b",3,1,_Func1dP1b,_FuncDiff1dP1b,_FuncDiffHess1dP1b);

  /************************************************************************
   *   basisFunction1dP2
   *************************************************************************/

  //\todo WARNING : P2 FUNCTIONS ARE WRONG (still in [0,1] -> move them to [-1,1] !!!!!!)
  // I try to correct this. VM+JF 07/2011
  /*
    double basis1Func1dP2(const Point& pt){ return 2. * ( pt.x() - 1. ) * ( pt.x() - 0.5 ) ;}
    double basis2Func1dP2(const Point& pt){ return 2. * pt.x() * ( pt.x() - 0.5 ) ;}
    double basis3Func1dP2(const Point& pt){ return 4. * pt.x() * ( 1. - pt.x() ) ;}

    double basis1FuncDiffr1dP2(const Point& pt){return 4. * pt.x() - 3. ;}
    double basis2FuncDiffr1dP2(const Point& pt){return 4. * pt.x() - 1.;}
    double basis3FuncDiffr1dP2(const Point& pt){return -8. * pt.x() + 4.;}

    double basis1FuncDiffrr1dP2(const Point& pt){return 4.;}
    double basis2FuncDiffrr1dP2(const Point& pt){return 4.;}
    double basis3FuncDiffrr1dP2(const Point& pt){return -8.;}

    static const FunctionXYZ _Func1dP2[] = {basis1Func1dP2, basis2Func1dP2, basis3Func1dP2}

    static const FunctionXYZ _FuncDiff1dP2[] = {basis1FuncDiffr1dP2, basis2FuncDiffr1dP2,basis3FuncDiffr1dP2}

    static const FunctionXYZ _FuncDiffHess1dP2[] = {basis1FuncDiffrr1dP2, basis2FuncDiffrr1dP2, basis3FuncDiffrr1dP2}

    const BasisFunction basisFunction1dP2("basisFunction1dP2",3,1,_Func1dP2,_FuncDiff1dP2,_FuncDiffHess1dP2);
  */
  double basis1Func1dP2(const Point& pt);
  double basis2Func1dP2(const Point& pt);
  double basis3Func1dP2(const Point& pt);
  double basis1Func1dP2(const Point& pt) {
    return -0.5 * ( 1. - pt.x()  ) * pt.x() ;
  }
  double basis2Func1dP2(const Point& pt) {
    return 0.5 * ( 1. + pt.x() ) * pt.x()  ;
  }
  double basis3Func1dP2(const Point& pt) {
    return ( 1. - pt.x() ) * ( 1. + pt.x() ) ;
  }

  double basis1FuncDiffr1dP2(const Point& pt);
  double basis2FuncDiffr1dP2(const Point& pt);
  double basis3FuncDiffr1dP2(const Point& pt);
  double basis1FuncDiffr1dP2(const Point& pt) {
    return pt.x() - 0.5 ;
  }
  double basis2FuncDiffr1dP2(const Point& pt) {
    return pt.x() + 0.5;
  }
  double basis3FuncDiffr1dP2(const Point& pt) {
    return -2. * pt.x();
  }

  double basis1FuncDiffrr1dP2(const Point&);
  double basis2FuncDiffrr1dP2(const Point&);
  double basis3FuncDiffrr1dP2(const Point&);
  double basis1FuncDiffrr1dP2(const Point& pt) {
    (void) pt;
    return 1.;
  }
  double basis2FuncDiffrr1dP2(const Point& pt) {
    (void) pt;
    return 1.;
  }
  double basis3FuncDiffrr1dP2(const Point& pt) {
    (void) pt;
    return -2.;
  }

  static const FunctionXYZ _Func1dP2[] = {basis1Func1dP2, basis2Func1dP2, basis3Func1dP2};

  static const FunctionXYZ _FuncDiff1dP2[] = {basis1FuncDiffr1dP2, basis2FuncDiffr1dP2,basis3FuncDiffr1dP2};

  static const FunctionXYZ _FuncDiffHess1dP2[] = {basis1FuncDiffrr1dP2, basis2FuncDiffrr1dP2, basis3FuncDiffrr1dP2};

  const BasisFunction basisFunction1dP2("basisFunction1dP2",3,1,_Func1dP2,_FuncDiff1dP2,_FuncDiffHess1dP2);

  /************************************************************************
   *   basisFunction1dP3H = P3-Hermite
   *************************************************************************/
  double basis1Func1dP3H(const Point&);
  double basis2Func1dP3H(const Point&);
  double basis3Func1dP3H(const Point&);
  double basis4Func1dP3H(const Point&);
  double basis1Func1dP3H(const Point& pt) {
    return 0.25*( 2. +pt.x()*(pt.x()*pt.x() -3.));
  }
  double basis2Func1dP3H(const Point& pt) {
    return 0.25*( 1. +pt.x()*(pt.x()*pt.x() -pt.x() -1.));
  }
  double basis3Func1dP3H(const Point& pt) {
    return 0.25*( 2. +pt.x()*(3. -pt.x()*pt.x()));
  }
  double basis4Func1dP3H(const Point& pt) {
    return 0.25*(-1. +pt.x()*(pt.x()*pt.x() +pt.x() -1.));
  }

  // first derivatives
  double basis1FuncDiffr1dP3H(const Point& pt);
  double basis2FuncDiffr1dP3H(const Point& pt);
  double basis3FuncDiffr1dP3H(const Point& pt);
  double basis4FuncDiffr1dP3H(const Point& pt);
  double basis1FuncDiffr1dP3H(const Point& pt) {
    return 0.75*( pt.x()*pt.x() -1. );
  }
  double basis2FuncDiffr1dP3H(const Point& pt) {
    return 0.25*( 3.*pt.x()*pt.x() -2.*pt.x() -1. );
  }
  double basis3FuncDiffr1dP3H(const Point& pt) {
    return 0.75*( 1. -pt.x()*pt.x() );
  }
  double basis4FuncDiffr1dP3H(const Point& pt) {
    return 0.25*( 3.*pt.x()*pt.x() +2.*pt.x() -1. );
  }

  // second derivatives
  double basis1FuncDiffrr1dP3H(const Point& pt);
  double basis2FuncDiffrr1dP3H(const Point& pt);
  double basis3FuncDiffrr1dP3H(const Point& pt);
  double basis4FuncDiffrr1dP3H(const Point& pt);
  double basis1FuncDiffrr1dP3H(const Point& pt) {
    return 1.5*pt.x();
  }
  double basis2FuncDiffrr1dP3H(const Point& pt) {
    return 0.25*(6.*pt.x() -2.);
  }
  double basis3FuncDiffrr1dP3H(const Point& pt) {
    return -1.5*pt.x();
  }
  double basis4FuncDiffrr1dP3H(const Point& pt) {
    return 0.25*(6.*pt.x() +2.);
  }

  static const FunctionXYZ _Func1dP3H[] =  {basis1Func1dP3H,  basis2Func1dP3H,  basis3Func1dP3H,  basis4Func1dP3H};
  static const FunctionXYZ _FuncDiff1dP3H[] = {basis1FuncDiffr1dP3H, basis2FuncDiffr1dP3H, basis3FuncDiffr1dP3H, basis4FuncDiffr1dP3H};
  static const FunctionXYZ _FuncDiffHess1dP3H[] = {basis1FuncDiffrr1dP3H, basis2FuncDiffrr1dP3H,  basis3FuncDiffrr1dP3H,  basis4FuncDiffrr1dP3H};

  const BasisFunction basisFunction1dP3H("basisFunction1dP3H",4,1,_Func1dP3H,_FuncDiff1dP3H,_FuncDiffHess1dP3H);

  /************************************************************************
   *  basisFunction2dP1
   *************************************************************************/

  template <int i>
  double basisFunc2dP1(const Point& pt) {
    if constexpr( i == 0 )
      return 1. - pt.x() - pt.y();
    else if constexpr( i == 1 )
      return pt.x();
    else
      return pt.y();
  }

  // first derivatives
  template <int i>
  double basisFuncDiffr2dP1(const Point& pt) {
    (void) pt;
    if constexpr( i == 0 )
      return -1.;
    else if constexpr( i == 1 )
      return 1.;
    else
      return 0.;
  }

  template <int i>
  double basisFuncDiffs2dP1(const Point& pt) {
    (void) pt;
    if constexpr( i == 0 )
      return -1.;
    else if constexpr( i == 1 )
      return 0.;
    else
      return 1.;
  }

  // Second derivatives
  template <int i>
  double basisFuncDiffrr2dP1(const Point& pt) {
    (void) pt;
    return 0.;
  }

  template <int i>
  double basisFuncDiffrs2dP1(const Point& pt) {
    (void) pt;
    return 0.;
  }

  template <int i>
  double basisFuncDiffsr2dP1(const Point& pt) {
    (void) pt;
    return 0.;
  }

  template <int i>
  double basisFuncDiffss2dP1(const Point& pt) {
    (void) pt;
    return 0.;
  }

  static const FunctionXYZ _Func2dP1[] = {basisFunc2dP1<0>, basisFunc2dP1<1>, basisFunc2dP1<2>};

  static const FunctionXYZ _FuncDiff2dP1[] = {
    basisFuncDiffr2dP1<0>, basisFuncDiffs2dP1<0>,
    basisFuncDiffr2dP1<1>, basisFuncDiffs2dP1<1>,
    basisFuncDiffr2dP1<2>, basisFuncDiffs2dP1<2>
  };
  static const FunctionXYZ _FuncDiffHess2dP1[] = {
    basisFuncDiffrr2dP1<0>, basisFuncDiffrs2dP1<0>, basisFuncDiffsr2dP1<0>, basisFuncDiffss2dP1<0>,
    basisFuncDiffrr2dP1<0>, basisFuncDiffrs2dP1<0>, basisFuncDiffsr2dP1<0>, basisFuncDiffss2dP1<0>,
    basisFuncDiffrr2dP1<0>, basisFuncDiffrs2dP1<0>, basisFuncDiffsr2dP1<0>, basisFuncDiffss2dP1<0>
  };

  const BasisFunction basisFunction2dP1("basisFunction2dP1",3,2,_Func2dP1,_FuncDiff2dP1,_FuncDiffHess2dP1);

  /************************************************************************
  *  basisFunction2dP1b = P1 + bubble
  *************************************************************************/
  // function
  template <int i>
  double basisFunc2dP1b(const Point& pt) {
    if constexpr( i == 0 )
      return 1. - pt.x() - pt.y();
    else if constexpr( i == 1 )
      return pt.x();
    else if constexpr( i == 2 )
      return pt.y();
    else
      return 27. * (1. - pt.x() - pt.y()) * pt.x() * pt.y();
  }

  // first derivatives
  template <int i>
  double basisFuncDiffr2dP1b(const Point& pt) {
    if constexpr( i == 0 )
      return -1.;
    else if constexpr( i == 1 )
      return 1.;
    else if constexpr( i == 2 )
      return 0.;
    else
      return 27. * (1. - 2. * pt.x() - pt.y()) * pt.y();
  }

  template <int i>
  double basisFuncDiffs2dP1b(const Point& pt) {
    if constexpr( i == 0 )
      return -1.;
    else if constexpr( i == 1 )
      return 0.;
    else if constexpr( i == 2 )
      return 1.;
    else
      return 27. * (1. - pt.x() - 2. * pt.y()) * pt.x();
  }

  // Second derivatives
  template <int i>
  double basisFuncDiffrr2dP1b(const Point& pt) {
    if constexpr( i < 3 )
      return 0.;
    else
      return -54. * pt.y();
  }

  template <int i>
  double basisFuncDiffrs2dP1b(const Point& pt) {
    if constexpr( i < 3 )
      return 0.;
    else
      return 27. * (1. - 2. * pt.x() - 2. * pt.y());
  }

  template <int i>
  double basisFuncDiffsr2dP1b(const Point& pt) {
    if constexpr( i < 3 )
      return 0.;
    else
      return 27. * (1. - 2. * pt.x() - 2. * pt.y());
  }

  template <int i>
  double basisFuncDiffss2dP1b(const Point& pt) {
    if constexpr( i < 3 )
      return 0.;
    else
      return -54. * pt.x();
  }

  static const FunctionXYZ _Func2dP1b[] = {basisFunc2dP1b<0>, basisFunc2dP1b<1>, basisFunc2dP1b<2>, basisFunc2dP1b<3>};

  static const FunctionXYZ _FuncDiff2dP1b[] = {
    basisFuncDiffr2dP1b<0>, basisFuncDiffs2dP1b<0>,
    basisFuncDiffr2dP1b<1>, basisFuncDiffs2dP1b<1>,
    basisFuncDiffr2dP1b<2>, basisFuncDiffs2dP1b<2>,
    basisFuncDiffr2dP1b<3>, basisFuncDiffs2dP1b<3>
  };

  static const FunctionXYZ _FuncDiffHess2dP1b[] = {
    basisFuncDiffrr2dP1b<0>, basisFuncDiffrs2dP1b<0>, basisFuncDiffsr2dP1b<0>, basisFuncDiffss2dP1b<0>,
    basisFuncDiffrr2dP1b<1>, basisFuncDiffrs2dP1b<1>, basisFuncDiffsr2dP1b<1>, basisFuncDiffss2dP1b<1>,
    basisFuncDiffrr2dP1b<2>, basisFuncDiffrs2dP1b<2>, basisFuncDiffsr2dP1b<2>, basisFuncDiffss2dP1b<2>,
    basisFuncDiffrr2dP1b<3>, basisFuncDiffrs2dP1b<3>, basisFuncDiffsr2dP1b<3>, basisFuncDiffss2dP1b<3>
  };

  const BasisFunction basisFunction2dP1b("basisFunction2dP1b",4,2,_Func2dP1b,_FuncDiff2dP1b,_FuncDiffHess2dP1b);


  /************************************************************************
   *  basisFunction2dP2
   *************************************************************************/
  template <int i>
  double basisFunc2dP2(const Point& pt) {
    if constexpr( i == 0 )
      return ( 1. -pt.x() - pt.y() ) * ( 1. - pt.x() - pt.x() - pt.y() - pt.y() );
    else if constexpr( i == 1 )
      return -pt.x() * ( 1. - pt.x() - pt.x() );
    else if constexpr( i == 2 )
      return ( -pt.y() * ( 1. - pt.y() - pt.y() ) );
    else if constexpr( i == 3 )
      return ( 4. * pt.x() * ( 1. - pt.x() - pt.y() ) );
    else if constexpr( i == 4 )
      return ( 4. * pt.x() * pt.y() );
    else
      return ( 4. * pt.y() * ( 1. - pt.x() - pt.y() ) );
  }

  // first derivatives

  template <int i>
  double basisFuncDiffr2dP2(const Point& pt) {
    if constexpr( i == 0 )
      return 4. * ( pt.x() + pt.y() ) - 3.;
    else if constexpr( i == 1 )
      return 4. * pt.x() - 1.;
    else if constexpr( i == 2 )
      return 0.;
    else if constexpr( i == 3 )
      return 4. * ( 1. - pt.x() - pt.x() - pt.y() );
    else if constexpr( i == 4 )
      return 4. * pt.y();
    else
      return -4. * pt.y();
  }


  template <int i>
  double basisFuncDiffs2dP2(const Point& pt) {
    if constexpr( i == 0 )
      return 4. * ( pt.x() + pt.y() ) - 3.;
    else if constexpr( i == 1 )
      return 0.;
    else if constexpr( i == 2 )
      return 4. * pt.y() - 1.;
    else if constexpr( i == 3 )
      return -4. * pt.x();
    else if constexpr( i == 4 )
      return 4. * pt.x();
    else
      return 4. * ( 1. - pt.x() - pt.y() - pt.y() );
  }
  /*

    double basis1FuncDiffr2dP2(const Point& pt){return ( 4. * ( pt.x() + pt.y() ) - 3. );}
    double basis1FuncDiffs2dP2(const Point& pt){return ( 4. * ( pt.x() + pt.y() ) - 3. );}

    double basis2FuncDiffr2dP2(const Point& pt){return ( 4. * pt.x() - 1. );}
    double basis2FuncDiffs2dP2(const Point& pt){return ( 0. );}

    double basis3FuncDiffr2dP2(const Point& pt){return ( 0. );}
    double basis3FuncDiffs2dP2(const Point& pt){return ( 4. * pt.y() - 1. );}

    double basis4FuncDiffr2dP2(const Point& pt){return ( 4. * ( 1. - pt.x() - pt.x() - pt.y() ) );}
    double basis4FuncDiffs2dP2(const Point& pt){return ( -4. * pt.x() );}

    double basis5FuncDiffr2dP2(const Point& pt){return ( 4. * pt.y() );}
    double basis5FuncDiffs2dP2(const Point& pt){return ( 4. * pt.x() );}

    double basis6FuncDiffr2dP2(const Point& pt){return ( -4. * pt.y() );}
    double basis6FuncDiffs2dP2(const Point& pt){return ( 4. * ( 1. - pt.x() - pt.y() - pt.y() ) );}
  */
  // Second derivatives
  template <int i>
  double basisFuncDiffrr2dP2(const Point& pt) {
    (void) pt;
    if constexpr( i == 0 )
      return 4.;
    else if constexpr( i == 1 )
      return 4.;
    else if constexpr( i == 2 )
      return 0.;
    else if constexpr( i == 3 )
      return -8.;
    else if constexpr( i == 4 )
      return 0.;
    else
      return 0.;
  }

  template <int i>
  double basisFuncDiffrs2dP2(const Point& pt) {
    (void) pt;
    if constexpr( i == 0 )
      return 4.;
    else if constexpr( i == 1 )
      return 0.;
    else if constexpr( i == 2 )
      return 0.;
    else if constexpr( i == 3 )
      return -4.;
    else if constexpr( i == 4 )
      return 4.;
    else
      return -4.;
  }

  template <int i>
  double basisFuncDiffsr2dP2(const Point& pt) {
    (void) pt;
    if ( i == 0 )
      return 4.;
    else if constexpr( i == 1 )
      return 0.;
    else if constexpr( i == 2 )
      return 0.;
    else if constexpr( i == 3 )
      return -4.;
    else if constexpr( i == 4 )
      return 4.;
    else
      return -4.;
  }


  template <int i>
  double basisFuncDiffss2dP2(const Point& pt) {
    (void) pt;
    if constexpr( i == 0 )
      return 4.;
    else if constexpr( i == 1 )
      return 0.;
    else if constexpr( i == 2 )
      return 4.;
    else if constexpr( i == 3 )
      return 0.;
    else if constexpr( i == 4 )
      return 0.;
    else
      return -8.;
  }

  static const FunctionXYZ _Func2dP2[] = {basisFunc2dP2<0>, basisFunc2dP2<1>, basisFunc2dP2<2>, basisFunc2dP2<3>, basisFunc2dP2<4>, basisFunc2dP2<5>};

  static const FunctionXYZ _FuncDiff2dP2[] = {
    basisFuncDiffr2dP2<0>, basisFuncDiffs2dP2<0>,
    basisFuncDiffr2dP2<1>, basisFuncDiffs2dP2<1>,
    basisFuncDiffr2dP2<2>, basisFuncDiffs2dP2<2>,
    basisFuncDiffr2dP2<3>, basisFuncDiffs2dP2<3>,
    basisFuncDiffr2dP2<4>, basisFuncDiffs2dP2<4>,
    basisFuncDiffr2dP2<5>, basisFuncDiffs2dP2<5>,
  };

  static const FunctionXYZ _FuncDiffHess2dP2[] = {
    basisFuncDiffrr2dP2<0>, basisFuncDiffrs2dP2<0>, basisFuncDiffsr2dP2<0>, basisFuncDiffss2dP2<0>,
    basisFuncDiffrr2dP2<1>, basisFuncDiffrs2dP2<1>, basisFuncDiffsr2dP2<1>, basisFuncDiffss2dP2<1>,
    basisFuncDiffrr2dP2<2>, basisFuncDiffrs2dP2<2>, basisFuncDiffsr2dP2<2>, basisFuncDiffss2dP2<2>,
    basisFuncDiffrr2dP2<3>, basisFuncDiffrs2dP2<3>, basisFuncDiffsr2dP2<3>, basisFuncDiffss2dP2<3>,
    basisFuncDiffrr2dP2<4>, basisFuncDiffrs2dP2<4>, basisFuncDiffsr2dP2<4>, basisFuncDiffss2dP2<4>,
    basisFuncDiffrr2dP2<5>, basisFuncDiffrs2dP2<5>, basisFuncDiffsr2dP2<5>, basisFuncDiffss2dP2<5>
  };


  const BasisFunction basisFunction2dP2("basisFunction2dP2",6,2,_Func2dP2,_FuncDiff2dP2,_FuncDiffHess2dP2);

  /************************************************************************
   *  basisFunction2dQ1
   *************************************************************************/
  double basis1Func2dQ1(const Point&);
  double basis2Func2dQ1(const Point&);
  double basis3Func2dQ1(const Point&);
  double basis4Func2dQ1(const Point&);
  double basis1Func2dQ1(const Point& pt) {
    return ( 0.25*( 1. - pt.x() ) * ( 1. - pt.y()) );
  }
  double basis2Func2dQ1(const Point& pt) {
    return ( 0.25*( 1. + pt.x() ) * ( 1. - pt.y()) );
  }
  double basis3Func2dQ1(const Point& pt) {
    return ( 0.25*( 1. + pt.x() ) * ( 1. + pt.y()) );
  }
  double basis4Func2dQ1(const Point& pt) {
    return ( 0.25*( 1. - pt.x() ) * ( 1. + pt.y()) );
  }
  // first derivatives
  double basis1FuncDiffr2dQ1(const Point&);
  double basis1FuncDiffs2dQ1(const Point&);
  double basis1FuncDiffr2dQ1(const Point& pt) {
    return ( -0.25*( 1. - pt.y() ) );
  }
  double basis1FuncDiffs2dQ1(const Point& pt) {
    return ( -0.25*( 1. - pt.x() ) );
  }

  double basis2FuncDiffr2dQ1(const Point&);
  double basis2FuncDiffs2dQ1(const Point&);
  double basis2FuncDiffr2dQ1(const Point& pt) {
    return ( 0.25*( 1. - pt.y() ) );
  }
  double basis2FuncDiffs2dQ1(const Point& pt) {
    return ( -0.25*( 1. + pt.x() ) );
  }

  double basis3FuncDiffr2dQ1(const Point&);
  double basis3FuncDiffs2dQ1(const Point&);
  double basis3FuncDiffr2dQ1(const Point& pt) {
    return ( 0.25*( 1. + pt.y() ) );
  }
  double basis3FuncDiffs2dQ1(const Point& pt) {
    return ( 0.25*( 1. + pt.x() ) );
  }

  double basis4FuncDiffr2dQ1(const Point&);
  double basis4FuncDiffs2dQ1(const Point&);
  double basis4FuncDiffr2dQ1(const Point& pt) {
    return ( -0.25*(1. + pt.y() ) );
  }
  double basis4FuncDiffs2dQ1(const Point& pt) {
    return ( 0.25*( 1. - pt.x() ) );
  }
  // Second derivatives
  double basis1FuncDiffrr2dQ1(const Point&);
  double basis1FuncDiffrs2dQ1(const Point&);
  double basis1FuncDiffsr2dQ1(const Point&);
  double basis1FuncDiffss2dQ1(const Point&);
  double basis1FuncDiffrr2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis1FuncDiffrs2dQ1(const Point& pt) {
    (void) pt;
    return ( 0.25 );
  }
  double basis1FuncDiffsr2dQ1(const Point& pt) {
    (void) pt;
    return ( 0.25 );
  }
  double basis1FuncDiffss2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }

  double basis2FuncDiffrr2dQ1(const Point&);
  double basis2FuncDiffrs2dQ1(const Point&);
  double basis2FuncDiffsr2dQ1(const Point&);
  double basis2FuncDiffss2dQ1(const Point&);
  double basis2FuncDiffrr2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis2FuncDiffrs2dQ1(const Point& pt) {
    (void) pt;
    return ( -0.25 );
  }
  double basis2FuncDiffsr2dQ1(const Point& pt) {
    (void) pt;
    return ( -0.25 );
  }
  double basis2FuncDiffss2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }

  double basis3FuncDiffrr2dQ1(const Point&);
  double basis3FuncDiffrs2dQ1(const Point&);
  double basis3FuncDiffsr2dQ1(const Point&);
  double basis3FuncDiffss2dQ1(const Point&);
  double basis3FuncDiffrr2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis3FuncDiffrs2dQ1(const Point& pt) {
    (void) pt;
    return ( 0.25 );
  }
  double basis3FuncDiffsr2dQ1(const Point& pt) {
    (void) pt;
    return ( 0.25 );
  }
  double basis3FuncDiffss2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }

  double basis4FuncDiffrr2dQ1(const Point&);
  double basis4FuncDiffrs2dQ1(const Point&);
  double basis4FuncDiffsr2dQ1(const Point&);
  double basis4FuncDiffss2dQ1(const Point&);
  double basis4FuncDiffrr2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis4FuncDiffrs2dQ1(const Point& pt) {
    (void) pt;
    return ( -0.25 );
  }
  double basis4FuncDiffsr2dQ1(const Point& pt) {
    (void) pt;
    return ( -0.25 );
  }
  double basis4FuncDiffss2dQ1(const Point& pt) {
    (void) pt;
    return ( 0. );
  }

  static const FunctionXYZ _Func2dQ1[] = {basis1Func2dQ1, basis2Func2dQ1, basis3Func2dQ1, basis4Func2dQ1};

  static const FunctionXYZ _FuncDiff2dQ1[] = {
    basis1FuncDiffr2dQ1, basis1FuncDiffs2dQ1,
    basis2FuncDiffr2dQ1, basis2FuncDiffs2dQ1,
    basis3FuncDiffr2dQ1, basis3FuncDiffs2dQ1,
    basis4FuncDiffr2dQ1, basis4FuncDiffs2dQ1
  };
  static const FunctionXYZ _FuncDiffHess2dQ1[] = {
    basis1FuncDiffrr2dQ1, basis1FuncDiffrs2dQ1, basis1FuncDiffsr2dQ1, basis1FuncDiffss2dQ1,
    basis2FuncDiffrr2dQ1, basis2FuncDiffrs2dQ1, basis2FuncDiffsr2dQ1, basis2FuncDiffss2dQ1,
    basis3FuncDiffrr2dQ1, basis3FuncDiffrs2dQ1, basis3FuncDiffsr2dQ1, basis3FuncDiffss2dQ1,
    basis4FuncDiffrr2dQ1, basis4FuncDiffrs2dQ1, basis4FuncDiffsr2dQ1, basis4FuncDiffss2dQ1,
  };

  const BasisFunction basisFunction2dQ1("basisFunction2dQ1",4,2,_Func2dQ1,_FuncDiff2dQ1,_FuncDiffHess2dQ1);

  /************************************************************************
   *  basisFunction2dP1xP2
   *************************************************************************/
  double basis1Func2dP1xP2(const Point&);
  double basis2Func2dP1xP2(const Point&);
  double basis3Func2dP1xP2(const Point&);
  double basis4Func2dP1xP2(const Point&);
  double basis5Func2dP1xP2(const Point&);
  double basis6Func2dP1xP2(const Point&);

  double basis1Func2dP1xP2(const Point& pt) {
    return ( -0.25*( 1. - pt.x() ) * ( 1. - pt.y()) * pt.y() );
  }
  double basis2Func2dP1xP2(const Point& pt) {
    return ( -0.25*( 1. + pt.x() ) * ( 1. - pt.y()) * pt.y() );
  }
  double basis3Func2dP1xP2(const Point& pt) {
    return (  0.25*( 1. + pt.x() ) * ( 1. + pt.y()) * pt.y() );
  }
  double basis4Func2dP1xP2(const Point& pt) {
    return (  0.25*( 1. - pt.x() ) * ( 1. + pt.y()) * pt.y() );
  }
  double basis5Func2dP1xP2(const Point& pt) {
    return (  0.5*( 1. + pt.x() ) * ( 1. + pt.y()) * ( 1. - pt.y() ) );
  }
  double basis6Func2dP1xP2(const Point& pt) {
    return (  0.5*( 1. - pt.x() ) * ( 1. + pt.y()) * ( 1. - pt.y() ) );
  }

  // first derivatives
  double basis1FuncDiffr2dP1xP2(const Point&);
  double basis1FuncDiffs2dP1xP2(const Point&);
  double basis1FuncDiffr2dP1xP2(const Point& pt) {
    return (  0.25 * ( 1. - pt.y() ) * pt.y() );
  }
  double basis1FuncDiffs2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. - pt.x() ) * ( 1. - 2*pt.y() ) );
  }

  double basis2FuncDiffr2dP1xP2(const Point&);
  double basis2FuncDiffs2dP1xP2(const Point&);
  double basis2FuncDiffr2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. - pt.y() ) * pt.y() );
  }
  double basis2FuncDiffs2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. + pt.x() ) * ( 1. - 2*pt.y() ) );
  }

  double basis3FuncDiffr2dP1xP2(const Point&);
  double basis3FuncDiffs2dP1xP2(const Point&);
  double basis3FuncDiffr2dP1xP2(const Point& pt) {
    return (  0.25 * ( 1. + pt.y() ) * pt.y() );
  }
  double basis3FuncDiffs2dP1xP2(const Point& pt) {
    return (  0.25 * ( 1. + pt.x() ) * ( 1. + 2*pt.y() ) );
  }

  double basis4FuncDiffr2dP1xP2(const Point&);
  double basis4FuncDiffs2dP1xP2(const Point&);
  double basis4FuncDiffr2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. + pt.y() ) * pt.y() );
  }
  double basis4FuncDiffs2dP1xP2(const Point& pt) {
    return (  0.25 * ( 1. - pt.x() ) * ( 1. + 2*pt.y() ) );
  }

  double basis5FuncDiffr2dP1xP2(const Point&);
  double basis5FuncDiffs2dP1xP2(const Point&);
  double basis5FuncDiffr2dP1xP2(const Point& pt) {
    return ( +0.5*( 1. + pt.y() ) * ( 1. - pt.y() ) );
  }
  double basis5FuncDiffs2dP1xP2(const Point& pt) {
    return ( - ( 1. + pt.x() ) * pt.y() );
  }

  double basis6FuncDiffr2dP1xP2(const Point&);
  double basis6FuncDiffs2dP1xP2(const Point&);
  double basis6FuncDiffr2dP1xP2(const Point& pt) {
    return ( -0.5*( 1. + pt.y() ) * ( 1. - pt.y() ) );
  }
  double basis6FuncDiffs2dP1xP2(const Point& pt) {
    return ( - ( 1. - pt.x() ) * pt.y() );
  }

  // Second derivatives
  double basis1FuncDiffrr2dP1xP2(const Point&);
  double basis1FuncDiffrs2dP1xP2(const Point&);
  double basis1FuncDiffsr2dP1xP2(const Point&);
  double basis1FuncDiffss2dP1xP2(const Point&);
  double basis1FuncDiffrr2dP1xP2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis1FuncDiffrs2dP1xP2(const Point& pt) {
    return ( 0.25 * ( 1. - 2.*pt.y() ) );
  }
  double basis1FuncDiffsr2dP1xP2(const Point& pt) {
    return ( 0.25 * ( 1. - 2.*pt.y() ) );
  }
  double basis1FuncDiffss2dP1xP2(const Point& pt) {
    return ( 0.5 * ( 1. - pt.x() )  );
  }

  double basis2FuncDiffrr2dP1xP2(const Point&);
  double basis2FuncDiffrs2dP1xP2(const Point&);
  double basis2FuncDiffsr2dP1xP2(const Point&);
  double basis2FuncDiffss2dP1xP2(const Point&);
  double basis2FuncDiffrr2dP1xP2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis2FuncDiffrs2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. - 2.*pt.y() ) );
  }
  double basis2FuncDiffsr2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. - 2.*pt.y() ) );
  }
  double basis2FuncDiffss2dP1xP2(const Point& pt) {
    return ( 0.5 * ( 1. + pt.x() )  );
  }

  double basis3FuncDiffrr2dP1xP2(const Point&);
  double basis3FuncDiffrs2dP1xP2(const Point&);
  double basis3FuncDiffsr2dP1xP2(const Point&);
  double basis3FuncDiffss2dP1xP2(const Point&);
  double basis3FuncDiffrr2dP1xP2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis3FuncDiffrs2dP1xP2(const Point& pt) {
    return ( 0.25 * ( 1. + 2.*pt.y() ) );
  }
  double basis3FuncDiffsr2dP1xP2(const Point& pt) {
    return ( 0.25 * ( 1. + 2.*pt.y() ) );
  }
  double basis3FuncDiffss2dP1xP2(const Point& pt) {
    return ( 0.5 * ( 1. + pt.x() )  );
  }


  double basis4FuncDiffrr2dP1xP2(const Point&);
  double basis4FuncDiffrs2dP1xP2(const Point&);
  double basis4FuncDiffsr2dP1xP2(const Point&);
  double basis4FuncDiffss2dP1xP2(const Point&);
  double basis4FuncDiffrr2dP1xP2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis4FuncDiffrs2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. + 2.*pt.y() ) );
  }
  double basis4FuncDiffsr2dP1xP2(const Point& pt) {
    return ( -0.25 * ( 1. + 2.*pt.y() ) );
  }
  double basis4FuncDiffss2dP1xP2(const Point& pt) {
    return ( 0.5 * ( 1. - pt.x() )  );
  }


  double basis5FuncDiffrr2dP1xP2(const Point&);
  double basis5FuncDiffrs2dP1xP2(const Point&);
  double basis5FuncDiffsr2dP1xP2(const Point&);
  double basis5FuncDiffss2dP1xP2(const Point&);
  double basis5FuncDiffrr2dP1xP2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis5FuncDiffrs2dP1xP2(const Point& pt) {
    return ( pt.y() );
  }
  double basis5FuncDiffsr2dP1xP2(const Point& pt) {
    return ( pt.y() );
  }
  double basis5FuncDiffss2dP1xP2(const Point& pt) {
    return ( - ( 1. - pt.x() ) );
  }


  double basis6FuncDiffrr2dP1xP2(const Point&);
  double basis6FuncDiffrs2dP1xP2(const Point&);
  double basis6FuncDiffsr2dP1xP2(const Point&);
  double basis6FuncDiffss2dP1xP2(const Point&);
  double basis6FuncDiffrr2dP1xP2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis6FuncDiffrs2dP1xP2(const Point& pt) {
    return ( -pt.y() );
  }
  double basis6FuncDiffsr2dP1xP2(const Point& pt) {
    return ( -pt.y() );
  }
  double basis6FuncDiffss2dP1xP2(const Point& pt) {
    return ( - ( 1. + pt.x() ) );
  }


  static const FunctionXYZ _Func2dP1xP2[] = {basis1Func2dP1xP2, basis2Func2dP1xP2, basis3Func2dP1xP2, basis4Func2dP1xP2, basis5Func2dP1xP2, basis6Func2dP1xP2};

  static const FunctionXYZ _FuncDiff2dP1xP2[] = {
    basis1FuncDiffr2dP1xP2, basis1FuncDiffs2dP1xP2,
    basis2FuncDiffr2dP1xP2, basis2FuncDiffs2dP1xP2,
    basis3FuncDiffr2dP1xP2, basis3FuncDiffs2dP1xP2,
    basis4FuncDiffr2dP1xP2, basis4FuncDiffs2dP1xP2,
    basis5FuncDiffr2dP1xP2, basis5FuncDiffs2dP1xP2,
    basis6FuncDiffr2dP1xP2, basis6FuncDiffs2dP1xP2
  };
  static const FunctionXYZ _FuncDiffHess2dP1xP2[] = {
    basis1FuncDiffrr2dP1xP2, basis1FuncDiffrs2dP1xP2, basis1FuncDiffsr2dP1xP2, basis1FuncDiffss2dP1xP2,
    basis2FuncDiffrr2dP1xP2, basis2FuncDiffrs2dP1xP2, basis2FuncDiffsr2dP1xP2, basis2FuncDiffss2dP1xP2,
    basis3FuncDiffrr2dP1xP2, basis3FuncDiffrs2dP1xP2, basis3FuncDiffsr2dP1xP2, basis3FuncDiffss2dP1xP2,
    basis4FuncDiffrr2dP1xP2, basis4FuncDiffrs2dP1xP2, basis4FuncDiffsr2dP1xP2, basis4FuncDiffss2dP1xP2,
    basis5FuncDiffrr2dP1xP2, basis5FuncDiffrs2dP1xP2, basis5FuncDiffsr2dP1xP2, basis5FuncDiffss2dP1xP2,
    basis6FuncDiffrr2dP1xP2, basis6FuncDiffrs2dP1xP2, basis6FuncDiffsr2dP1xP2, basis6FuncDiffss2dP1xP2,
  };

  const BasisFunction basisFunction2dP1xP2("basisFunction2dP1xP2",6,2,_Func2dP1xP2,_FuncDiff2dP1xP2,_FuncDiffHess2dP1xP2);



  /************************************************************************
  *  basisFunction2dQ1b = Q1 + bubble
  *************************************************************************/
  double basis5Func2dQ1b(const Point&);
  double basis5Func2dQ1b(const Point& pt) {
    return ( (pt.x() * pt.x() - 1.) * (pt.y() * pt.y() - 1.) );
  }

  // first derivatives
  double basis5FuncDiffr2dQ1b(const Point&);
  double basis5FuncDiffs2dQ1b(const Point&);
  double basis5FuncDiffr2dQ1b(const Point& pt) {
    return ( 2. * pt.x() * (pt.y() * pt.y() - 1.) );
  }
  double basis5FuncDiffs2dQ1b(const Point& pt) {
    return ( 2. * pt.y() * (pt.x() * pt.x() - 1.) );
  }

  // Second derivatives
  double basis5FuncDiffrr2dQ1b(const Point&);
  double basis5FuncDiffrs2dQ1b(const Point&);
  double basis5FuncDiffsr2dQ1b(const Point&);
  double basis5FuncDiffss2dQ1b(const Point&);
  double basis5FuncDiffrr2dQ1b(const Point& pt) {
    return ( 2. * (pt.y() * pt.y() - 1.) );
  }
  double basis5FuncDiffrs2dQ1b(const Point& pt) {
    return ( 4. * pt.x() * pt.y() );
  }
  double basis5FuncDiffsr2dQ1b(const Point& pt) {
    return ( 4. * pt.x() * pt.y() );
  }
  double basis5FuncDiffss2dQ1b(const Point& pt) {
    return ( 2. * (pt.x() * pt.x() - 1.) );
  }

  static const FunctionXYZ _Func2dQ1b[] = {basis1Func2dQ1, basis2Func2dQ1, basis3Func2dQ1, basis4Func2dQ1, basis5Func2dQ1b};

  static const FunctionXYZ _FuncDiff2dQ1b[] = {
    basis1FuncDiffr2dQ1, basis1FuncDiffs2dQ1,
    basis2FuncDiffr2dQ1, basis2FuncDiffs2dQ1,
    basis3FuncDiffr2dQ1, basis3FuncDiffs2dQ1,
    basis4FuncDiffr2dQ1, basis4FuncDiffs2dQ1,
    basis5FuncDiffr2dQ1b, basis5FuncDiffs2dQ1b
  };
  static const FunctionXYZ _FuncDiffHess2dQ1b[] = {
    basis1FuncDiffrr2dQ1, basis1FuncDiffrs2dQ1, basis1FuncDiffsr2dQ1, basis1FuncDiffss2dQ1,
    basis2FuncDiffrr2dQ1, basis2FuncDiffrs2dQ1, basis2FuncDiffsr2dQ1, basis2FuncDiffss2dQ1,
    basis3FuncDiffrr2dQ1, basis3FuncDiffrs2dQ1, basis3FuncDiffsr2dQ1, basis3FuncDiffss2dQ1,
    basis4FuncDiffrr2dQ1, basis4FuncDiffrs2dQ1, basis4FuncDiffsr2dQ1, basis4FuncDiffss2dQ1,
    basis5FuncDiffrr2dQ1b, basis5FuncDiffrs2dQ1b, basis5FuncDiffsr2dQ1b, basis5FuncDiffss2dQ1b
  };

  const BasisFunction basisFunction2dQ1b("basisFunction2dQ1b",5,2,_Func2dQ1b,_FuncDiff2dQ1b,_FuncDiffHess2dQ1b);



  /************************************************************************
   *  basisFunction2dQ2
   *************************************************************************/
  double basis1Func2dQ2(const Point&);
  double basis2Func2dQ2(const Point&);
  double basis3Func2dQ2(const Point&);
  double basis4Func2dQ2(const Point&);
  double basis5Func2dQ2(const Point&);
  double basis6Func2dQ2(const Point&);
  double basis7Func2dQ2(const Point&);
  double basis8Func2dQ2(const Point&);
  double basis1Func2dQ2(const Point& pt) {
    return ( 0.25*(-1.-1.*pt.x()-1.*pt.y())*(1.-pt.x())*(1.-1.*pt.y()) );
  }
  double basis2Func2dQ2(const Point& pt) {
    return ( 0.25*(-1.+1.*pt.x()-1.*pt.y())*(1.+pt.x())*(1.-1.*pt.y()) );
  }
  double basis3Func2dQ2(const Point& pt) {
    return ( 0.25*(-1.+1.*pt.x()+1.*pt.y())*(1.+pt.x())*(1.+1.*pt.y()) );
  }
  double basis4Func2dQ2(const Point& pt) {
    return ( 0.25*(-1.-1.*pt.x()+1.*pt.y())*(1.-pt.x())*(1.+1.*pt.y()) );
  }
  double basis5Func2dQ2(const Point& pt) {
    return ( 0.5*(1.-(pt.x()*pt.x()))*(1.-1.*pt.y()) );
  }
  double basis6Func2dQ2(const Point& pt) {
    return ( 0.5*(1.+pt.x()*1.)*(1.-1.*(pt.y()*pt.y())) );
  }
  double basis7Func2dQ2(const Point& pt) {
    return ( 0.5*(1.-(pt.x()*pt.x()))*(1.+1.*pt.y()) );
  }
  double basis8Func2dQ2(const Point& pt) {
    return ( 0.5*(1.+pt.x()*(-1.))*(1.-1.*(pt.y()*pt.y())) );
  }
  // first derivatives
  double basis1FuncDiffr2dQ2(const Point&);
  double basis1FuncDiffs2dQ2(const Point&);
  double basis1FuncDiffr2dQ2(const Point& pt) {
    return ( -0.25*( 1. - pt.y() )*( -2.*pt.x() + pt.y() ) );
  }
  double basis1FuncDiffs2dQ2(const Point& pt) {
    return ( -0.25*( 1. - pt.x() )*( -1.*pt.x() - 2.*pt.y() ) );
  }

  double basis2FuncDiffr2dQ2(const Point&);
  double basis2FuncDiffs2dQ2(const Point&);
  double basis2FuncDiffr2dQ2(const Point& pt) {
    return ( 0.25*( 1. - pt.y() )*( 2.*pt.x() + pt.y() ) );
  }
  double basis2FuncDiffs2dQ2(const Point& pt) {
    return ( -0.25*( 1. + pt.x() )*( 1.*pt.x() - 2.*pt.y() ) );
  }

  double basis3FuncDiffr2dQ2(const Point&);
  double basis3FuncDiffs2dQ2(const Point&);
  double basis3FuncDiffr2dQ2(const Point& pt) {
    return ( 0.25*( 1. + pt.y() )*( 2.*pt.x() - pt.y() ) );
  }
  double basis3FuncDiffs2dQ2(const Point& pt) {
    return ( 0.25*( 1. + pt.x() )*( 1.*pt.x() + 2.*pt.y() ) );
  }

  double basis4FuncDiffr2dQ2(const Point&);
  double basis4FuncDiffs2dQ2(const Point&);
  double basis4FuncDiffr2dQ2(const Point& pt) {
    return ( -0.25*( 1. + pt.y() )*( -2.*pt.x() - pt.y() ) );
  }
  double basis4FuncDiffs2dQ2(const Point& pt) {
    return ( 0.25*( 1. - pt.x() )*( -1.*pt.x() + 2.*pt.y() ) );
  }

  double basis5FuncDiffr2dQ2(const Point&);
  double basis5FuncDiffs2dQ2(const Point&);
  double basis5FuncDiffr2dQ2(const Point& pt) {
    return ( -pt.x()*(1. - pt.y() ) );
  }
  double basis5FuncDiffs2dQ2(const Point& pt) {
    return ( -0.5*( 1. - pt.x()*pt.x() ) );
  }

  double basis6FuncDiffr2dQ2(const Point&);
  double basis6FuncDiffs2dQ2(const Point&);
  double basis6FuncDiffr2dQ2(const Point& pt) {
    return ( 0.5*( 1. - pt.y()*pt.y() ) );
  }
  double basis6FuncDiffs2dQ2(const Point& pt) {
    return ( -pt.y()*( 1. + pt.x() ) );
  }

  double basis7FuncDiffr2dQ2(const Point&);
  double basis7FuncDiffs2dQ2(const Point&);
  double basis7FuncDiffr2dQ2(const Point& pt) {
    return ( -pt.x()*(1. + pt.y() ) );
  }
  double basis7FuncDiffs2dQ2(const Point& pt) {
    return ( 0.5*( 1. - pt.x()*pt.x() ) );
  }

  double basis8FuncDiffr2dQ2(const Point&);
  double basis8FuncDiffs2dQ2(const Point&);
  double basis8FuncDiffr2dQ2(const Point& pt) {
    return ( -0.5*( 1. - pt.y()*pt.y() ) );
  }
  double basis8FuncDiffs2dQ2(const Point& pt) {
    return ( -pt.y()*( 1. - pt.x() ) );
  }

  // Second derivatives
  double basis1FuncDiffrr2dQ2(const Point&);
  double basis1FuncDiffrs2dQ2(const Point&);
  double basis1FuncDiffsr2dQ2(const Point&);
  double basis1FuncDiffss2dQ2(const Point&);
  double basis1FuncDiffrr2dQ2(const Point& pt) {
    return ( 0.5*( 1. - pt.y() ) );
  }
  double basis1FuncDiffrs2dQ2(const Point& pt) {
    return ( 0.25*( -2.*pt.x() + 2.*pt.y() - 1. )  );
  }
  double basis1FuncDiffsr2dQ2(const Point& pt) {
    return ( 0.25*( -2.*pt.x() - 2.*pt.y() + 1. ) );
  }
  double basis1FuncDiffss2dQ2(const Point& pt) {
    return ( 0.5*( 1. - pt.x() ) );
  }

  double basis2FuncDiffrr2dQ2(const Point&);
  double basis2FuncDiffrs2dQ2(const Point&);
  double basis2FuncDiffsr2dQ2(const Point&);
  double basis2FuncDiffss2dQ2(const Point&);
  double basis2FuncDiffrr2dQ2(const Point& pt) {
    return ( 0.5*( 1. - pt.y() ) );
  }
  double basis2FuncDiffrs2dQ2(const Point& pt) {
    return ( -0.25*( 2.*pt.x() + 2.*pt.y() - 1. ) );
  }
  double basis2FuncDiffsr2dQ2(const Point& pt) {
    return ( -0.25*( 2.*pt.x() - 2.*pt.y() + 1. ) );
  }
  double basis2FuncDiffss2dQ2(const Point& pt) {
    return ( 0.5*( 1. + pt.x() ) );
  }

  double basis3FuncDiffrr2dQ2(const Point&);
  double basis3FuncDiffrs2dQ2(const Point&);
  double basis3FuncDiffsr2dQ2(const Point&);
  double basis3FuncDiffss2dQ2(const Point&);
  double basis3FuncDiffrr2dQ2(const Point& pt) {
    return ( 0.5*( 1. + pt.y() ) );
  }
  double basis3FuncDiffrs2dQ2(const Point& pt) {
    return ( 0.25*( 2.*pt.x() - 2.*pt.y() - 1. ) );
  }
  double basis3FuncDiffsr2dQ2(const Point& pt) {
    return ( 0.25*( 2.*pt.x() + 2.*pt.y() + 1. ) );
  }
  double basis3FuncDiffss2dQ2(const Point& pt) {
    return ( 0.5*( 1. + pt.x() ) );
  }

  double basis4FuncDiffrr2dQ2(const Point&);
  double basis4FuncDiffrs2dQ2(const Point&);
  double basis4FuncDiffsr2dQ2(const Point&);
  double basis4FuncDiffss2dQ2(const Point&);
  double basis4FuncDiffrr2dQ2(const Point& pt) {
    return ( 0.5*( 1. + pt.y() ) );
  }
  double basis4FuncDiffrs2dQ2(const Point& pt) {
    return ( -0.25*( -2.*pt.x() - 2.*pt.y() - 1. ) );
  }
  double basis4FuncDiffsr2dQ2(const Point& pt) {
    return ( -0.25*( -2.*pt.x() + 2.*pt.y() +1. ) );
  }
  double basis4FuncDiffss2dQ2(const Point& pt) {
    return ( 0.5*( 1. - pt.x() ) );
  }

  double basis5FuncDiffrr2dQ2(const Point&);
  double basis5FuncDiffrs2dQ2(const Point&);
  double basis5FuncDiffsr2dQ2(const Point&);
  double basis5FuncDiffss2dQ2(const Point&);
  double basis5FuncDiffrr2dQ2(const Point& pt) {
    return ( -1. + pt.y() );
  }
  double basis5FuncDiffrs2dQ2(const Point& pt) {
    return ( pt.x() );
  }
  double basis5FuncDiffsr2dQ2(const Point& pt) {
    return ( pt.x() );
  }
  double basis5FuncDiffss2dQ2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }

  double basis6FuncDiffrr2dQ2(const Point&);
  double basis6FuncDiffrs2dQ2(const Point&);
  double basis6FuncDiffsr2dQ2(const Point&);
  double basis6FuncDiffss2dQ2(const Point&);
  double basis6FuncDiffrr2dQ2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis6FuncDiffrs2dQ2(const Point& pt) {
    return ( -pt.y() );
  }
  double basis6FuncDiffsr2dQ2(const Point& pt) {
    return ( -pt.y() );
  }
  double basis6FuncDiffss2dQ2(const Point& pt) {
    return ( -1. - pt.x() );
  }

  double basis7FuncDiffrr2dQ2(const Point&);
  double basis7FuncDiffrs2dQ2(const Point&);
  double basis7FuncDiffsr2dQ2(const Point&);
  double basis7FuncDiffss2dQ2(const Point&);
  double basis7FuncDiffrr2dQ2(const Point& pt) {
    return ( -1. - pt.y() );
  }
  double basis7FuncDiffrs2dQ2(const Point& pt) {
    return ( -pt.x() );
  }
  double basis7FuncDiffsr2dQ2(const Point& pt) {
    return ( -pt.x() );
  }
  double basis7FuncDiffss2dQ2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }

  double basis8FuncDiffrr2dQ2(const Point&);
  double basis8FuncDiffrs2dQ2(const Point&);
  double basis8FuncDiffsr2dQ2(const Point&);
  double basis8FuncDiffss2dQ2(const Point&);
  double basis8FuncDiffrr2dQ2(const Point& pt) {
    (void) pt;
    return ( 0. );
  }
  double basis8FuncDiffrs2dQ2(const Point& pt) {
    return ( pt.y() );
  }
  double basis8FuncDiffsr2dQ2(const Point& pt) {
    return ( pt.y() );
  }
  double basis8FuncDiffss2dQ2(const Point& pt) {
    return ( -1. + pt.x() );
  }

  static const FunctionXYZ _Func2dQ2[] = {
    basis1Func2dQ2, basis2Func2dQ2, basis3Func2dQ2, basis4Func2dQ2,
    basis5Func2dQ2, basis6Func2dQ2, basis7Func2dQ2, basis8Func2dQ2,
  };


  static const FunctionXYZ _FuncDiff2dQ2[] = {
    basis1FuncDiffr2dQ2, basis1FuncDiffs2dQ2,
    basis2FuncDiffr2dQ2, basis2FuncDiffs2dQ2,
    basis3FuncDiffr2dQ2, basis3FuncDiffs2dQ2,
    basis4FuncDiffr2dQ2, basis4FuncDiffs2dQ2,
    basis5FuncDiffr2dQ2, basis5FuncDiffs2dQ2,
    basis6FuncDiffr2dQ2, basis6FuncDiffs2dQ2,
    basis7FuncDiffr2dQ2, basis7FuncDiffs2dQ2,
    basis8FuncDiffr2dQ2, basis8FuncDiffs2dQ2,
  };

  static const FunctionXYZ _FuncDiffHess2dQ2[] = {
    basis1FuncDiffrr2dQ2, basis1FuncDiffrs2dQ2, basis1FuncDiffsr2dQ2, basis1FuncDiffss2dQ2,
    basis2FuncDiffrr2dQ2, basis2FuncDiffrs2dQ2, basis2FuncDiffsr2dQ2, basis2FuncDiffss2dQ2,
    basis3FuncDiffrr2dQ2, basis3FuncDiffrs2dQ2, basis3FuncDiffsr2dQ2, basis3FuncDiffss2dQ2,
    basis4FuncDiffrr2dQ2, basis4FuncDiffrs2dQ2, basis4FuncDiffsr2dQ2, basis4FuncDiffss2dQ2,
    basis5FuncDiffrr2dQ2, basis5FuncDiffrs2dQ2, basis5FuncDiffsr2dQ2, basis5FuncDiffss2dQ2,
    basis6FuncDiffrr2dQ2, basis6FuncDiffrs2dQ2, basis6FuncDiffsr2dQ2, basis6FuncDiffss2dQ2,
    basis7FuncDiffrr2dQ2, basis7FuncDiffrs2dQ2, basis7FuncDiffsr2dQ2, basis7FuncDiffss2dQ2,
    basis8FuncDiffrr2dQ2, basis8FuncDiffrs2dQ2, basis8FuncDiffsr2dQ2, basis8FuncDiffss2dQ2
  };

  const BasisFunction basisFunction2dQ2("basisFunction2dQ2",8,2,_Func2dQ2,_FuncDiff2dQ2,_FuncDiffHess2dQ2);


  /************************************************************************
   *  basisFunction2dQ2c
   *************************************************************************/
  double basis1Func2dQ2c(const Point&);
  double basis2Func2dQ2c(const Point&);
  double basis3Func2dQ2c(const Point&);
  double basis4Func2dQ2c(const Point&);
  double basis5Func2dQ2c(const Point&);
  double basis6Func2dQ2c(const Point&);
  double basis7Func2dQ2c(const Point&);
  double basis8Func2dQ2c(const Point&);
  double basis9Func2dQ2c(const Point&);
  double basis1Func2dQ2c(const Point& pt) {
    return ( 0.25*( 1. - pt.x() )*( 1. - pt.y() )*pt.x()*pt.y() );
  }
  double basis2Func2dQ2c(const Point& pt) {
    return ( -0.25*( 1. + pt.x() )*( 1. - pt.y() )*pt.x()*pt.y() );
  }
  double basis3Func2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.x() )*( 1. + pt.y() )*pt.x()*pt.y() );
  }
  double basis4Func2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.x() )*( 1. + pt.y() )*pt.x()*pt.y() );
  }
  double basis5Func2dQ2c(const Point& pt) {
    return ( -0.5*( 1. - pt.x()*pt.x() )*(1. - pt.y() )*pt.y() );
  }
  double basis6Func2dQ2c(const Point& pt) {
    return ( 0.5*( 1. + pt.x() )*( 1. - pt.y()*pt.y() )*pt.x() );
  }
  double basis7Func2dQ2c(const Point& pt) {
    return ( 0.5*( 1. - pt.x()*pt.x() )*(1. + pt.y() )*pt.y() );
  }
  double basis8Func2dQ2c(const Point& pt) {
    return ( -0.5*( 1. - pt.x() )*( 1. - pt.y()*pt.y() )*pt.x() );
  }
  double basis9Func2dQ2c(const Point& pt) {
    return ( ( 1. - pt.x() )*( 1. + pt.x() )*( 1. - pt.y() )*( 1. + pt.y() ) );
  }
  // first derivatives
  double basis1FuncDiffr2dQ2c(const Point&);
  double basis1FuncDiffs2dQ2c(const Point&);
  double basis1FuncDiffr2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.y() )*pt.x()*pt.y() + 0.25*( 1. - pt.x() )*( 1. - pt.y() )*pt.y() );
  }
  double basis1FuncDiffs2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.x() )*pt.x()*pt.y() + 0.25*( 1. - pt.x() )*( 1. - pt.y() )*pt.x() );
  }

  double basis2FuncDiffr2dQ2c(const Point&);
  double basis2FuncDiffs2dQ2c(const Point&);
  double basis2FuncDiffr2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.y() )*pt.x()*pt.y() - 0.25*( 1. + pt.x() )*( 1. - pt.y() )*pt.y() );
  }
  double basis2FuncDiffs2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.x() )*pt.x()*pt.y() - 0.25*( 1. + pt.x() )*( 1. - pt.y() )*pt.x() );
  }

  double basis3FuncDiffr2dQ2c(const Point&);
  double basis3FuncDiffs2dQ2c(const Point&);
  double basis3FuncDiffr2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.y() )*pt.x()*pt.y() + 0.25*( 1. + pt.x() )*( 1. + pt.y() )*pt.y() );
  }
  double basis3FuncDiffs2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.x() )*pt.x()*pt.y() + 0.25*( 1. + pt.x() )*( 1. + pt.y() )*pt.x() );
  }

  double basis4FuncDiffr2dQ2c(const Point&);
  double basis4FuncDiffs2dQ2c(const Point&);
  double basis4FuncDiffr2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.y() )*pt.x()*pt.y() - 0.25*( 1. - pt.x() )*( 1. + pt.y() )*pt.y() );
  }
  double basis4FuncDiffs2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.x() )*pt.x()*pt.y() - 0.25*( 1. - pt.x() )*( 1. + pt.y() )*pt.x() );
  }

  double basis5FuncDiffr2dQ2c(const Point&);
  double basis5FuncDiffs2dQ2c(const Point&);
  double basis5FuncDiffr2dQ2c(const Point& pt) {
    return ( pt.x()*pt.y()*( 1. - pt.y() ) );
  }
  double basis5FuncDiffs2dQ2c(const Point& pt) {
    return ( 0.5*( 1. - pt.x()*pt.x() )*( pt.y() - ( 1. - pt.y() ) ) );
  }

  double basis6FuncDiffr2dQ2c(const Point&);
  double basis6FuncDiffs2dQ2c(const Point&);
  double basis6FuncDiffr2dQ2c(const Point& pt) {
    return  (0.5*pt.x()*(1-pt.y()*pt.y())+0.5*(1+pt.x())*(1-pt.y()*pt.y()));
  }
  double basis6FuncDiffs2dQ2c(const Point& pt) {
    return ( -pt.x()*pt.y()*( 1. + pt.x() ) );
  }

  double basis7FuncDiffr2dQ2c(const Point&);
  double basis7FuncDiffs2dQ2c(const Point&);
  double basis7FuncDiffr2dQ2c(const Point& pt) {
    return ( -pt.x()*pt.y()*( 1. + pt.y() ) );
  }
  double basis7FuncDiffs2dQ2c(const Point& pt) {
    return ( 0.5*( 1. - pt.x()*pt.x() )*( pt.y() + ( 1. + pt.y() ) ) );
  }

  double basis8FuncDiffr2dQ2c(const Point&);
  double basis8FuncDiffs2dQ2c(const Point&);
  double basis8FuncDiffr2dQ2c(const Point& pt) {
    return ( 0.5*pt.x()*(1-pt.y()*pt.y())-0.5*(1-pt.x())*(1-pt.y()*pt.y()));
  }
  double basis8FuncDiffs2dQ2c(const Point& pt) {
    return ( pt.x()*pt.y()*( 1. - pt.x() ) );
  }

  double basis9FuncDiffr2dQ2c(const Point&);
  double basis9FuncDiffs2dQ2c(const Point&);
  double basis9FuncDiffr2dQ2c(const Point& pt) {
    return ( -2.*pt.x()*( 1. - pt.y() )*( 1. + pt.y() ) );
  }
  double basis9FuncDiffs2dQ2c(const Point& pt) {
    return ( -2.*pt.y()*( 1. - pt.x() )*( 1. + pt.x() ) );
  }

  // Second derivatives
  double basis1FuncDiffrr2dQ2c(const Point&);
  double basis1FuncDiffrs2dQ2c(const Point&);
  double basis1FuncDiffsr2dQ2c(const Point&);
  double basis1FuncDiffss2dQ2c(const Point&);
  double basis1FuncDiffrr2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.y() )*pt.y() - 0.25*(1. - pt.y() )*pt.y() );
  }
  double basis1FuncDiffrs2dQ2c(const Point& pt) {
    return ( -0.25*pt.x()*( -2.*pt.y() + 1. )  );
  }
  double basis1FuncDiffsr2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - 2.*pt.x() )*( pt.y() - ( 1. - pt.y() ) ) );
  }
  double basis1FuncDiffss2dQ2c(const Point& pt) {
    return ( -0.5*( 1. - pt.x() )*pt.x() );
  }

  double basis2FuncDiffrr2dQ2c(const Point&);
  double basis2FuncDiffrs2dQ2c(const Point&);
  double basis2FuncDiffsr2dQ2c(const Point&);
  double basis2FuncDiffss2dQ2c(const Point&);
  double basis2FuncDiffrr2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - pt.y() )*pt.y() - 0.25*(1. - pt.y() )*pt.y() );
  }
  double basis2FuncDiffrs2dQ2c(const Point& pt) {
    return ( -0.25*pt.x()*( -2.*pt.y() + 1. ) );
  }
  double basis2FuncDiffsr2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + 2.*pt.x() )*( pt.y() - ( 1. - pt.y() ) ) );
  }
  double basis2FuncDiffss2dQ2c(const Point& pt) {
    return ( 0.5*( 1. + pt.x() )*pt.x() );
  }

  double basis3FuncDiffrr2dQ2c(const Point&);
  double basis3FuncDiffrs2dQ2c(const Point&);
  double basis3FuncDiffsr2dQ2c(const Point&);
  double basis3FuncDiffss2dQ2c(const Point&);
  double basis3FuncDiffrr2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.y() )*pt.y() + 0.25*(1. + pt.y() )*pt.y() );
  }
  double basis3FuncDiffrs2dQ2c(const Point& pt) {
    return ( 0.25*pt.x()*( 2.*pt.y() + 1. ) );
  }
  double basis3FuncDiffsr2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + 2.*pt.x() )*( pt.y() + ( 1. + pt.y() ) ) );
  }
  double basis3FuncDiffss2dQ2c(const Point& pt) {
    return ( 0.5*( 1. + pt.x() )*pt.x() );
  }

  double basis4FuncDiffrr2dQ2c(const Point&);
  double basis4FuncDiffrs2dQ2c(const Point&);
  double basis4FuncDiffsr2dQ2c(const Point&);
  double basis4FuncDiffss2dQ2c(const Point&);
  double basis4FuncDiffrr2dQ2c(const Point& pt) {
    return ( 0.25*( 1. + pt.y() )*pt.y() + 0.25*(1. + pt.y() )*pt.y() );
  }
  double basis4FuncDiffrs2dQ2c(const Point& pt) {
    return ( 0.25*pt.x()*( 2.*pt.y() + 1. ) );
  }
  double basis4FuncDiffsr2dQ2c(const Point& pt) {
    return ( -0.25*( 1. - 2.*pt.x() )*( pt.y() + ( 1. + pt.y() ) ) );
  }
  double basis4FuncDiffss2dQ2c(const Point& pt) {
    return ( -0.5*( 1. - pt.x() )*pt.x() );
  }

  double basis5FuncDiffrr2dQ2c(const Point&);
  double basis5FuncDiffrs2dQ2c(const Point&);
  double basis5FuncDiffsr2dQ2c(const Point&);
  double basis5FuncDiffss2dQ2c(const Point&);
  double basis5FuncDiffrr2dQ2c(const Point& pt) {
    return ( pt.y()*( 1. - pt.y() ) );
  }
  double basis5FuncDiffrs2dQ2c(const Point& pt) {
    return ( pt.x()*( 1. - 2.*pt.y() ) );
  }
  double basis5FuncDiffsr2dQ2c(const Point& pt) {
    return ( - pt.x()*( pt.y() - ( 1. - pt.y() ) ) );
  }
  double basis5FuncDiffss2dQ2c(const Point& pt) {
    return ( 1. - pt.x()*pt.x() );
  }

  double basis6FuncDiffrr2dQ2c(const Point&);
  double basis6FuncDiffrs2dQ2c(const Point&);
  double basis6FuncDiffsr2dQ2c(const Point&);
  double basis6FuncDiffss2dQ2c(const Point&);
  double basis6FuncDiffrr2dQ2c(const Point& pt) {
    return ( 0.5*( 1. - pt.y()*pt.y() ) );
  }
  double basis6FuncDiffrs2dQ2c(const Point& pt) {
    return ( -pt.y()*( 1. + pt.x() ) );
  }
  double basis6FuncDiffsr2dQ2c(const Point& pt) {
    return ( -pt.y()*( 1. + 2.*pt.x() ) );
  }
  double basis6FuncDiffss2dQ2c(const Point& pt) {
    return ( -pt.x()*( 1. + pt.x() ) );
  }

  double basis7FuncDiffrr2dQ2c(const Point&);
  double basis7FuncDiffrs2dQ2c(const Point&);
  double basis7FuncDiffsr2dQ2c(const Point&);
  double basis7FuncDiffss2dQ2c(const Point&);
  double basis7FuncDiffrr2dQ2c(const Point& pt) {
    return ( -pt.y()*( 1. + pt.y() ) );
  }
  double basis7FuncDiffrs2dQ2c(const Point& pt) {
    return ( -pt.x()*( 1. + 2.*pt.y() ) );
  }
  double basis7FuncDiffsr2dQ2c(const Point& pt) {
    return ( - pt.x()*( pt.y() + ( 1. + pt.y() ) ) );
  }
  double basis7FuncDiffss2dQ2c(const Point& pt) {
    return ( 1. - pt.x()*pt.x() );
  }

  double basis8FuncDiffrr2dQ2c(const Point&);
  double basis8FuncDiffrs2dQ2c(const Point&);
  double basis8FuncDiffsr2dQ2c(const Point&);
  double basis8FuncDiffss2dQ2c(const Point&);
  double basis8FuncDiffrr2dQ2c(const Point& pt) {
    return ( 0.5*( 1. - pt.y()*pt.y() ) );
  }
  double basis8FuncDiffrs2dQ2c(const Point& pt) {
    return ( pt.y()*( 1. - pt.x() ) );
  }
  double basis8FuncDiffsr2dQ2c(const Point& pt) {
    return ( pt.y()*( 1. - 2.*pt.x() ) );
  }
  double basis8FuncDiffss2dQ2c(const Point& pt) {
    return ( pt.x()*( 1. - pt.x() ) );
  }

  double basis9FuncDiffrr2dQ2c(const Point&);
  double basis9FuncDiffrs2dQ2c(const Point&);
  double basis9FuncDiffsr2dQ2c(const Point&);
  double basis9FuncDiffss2dQ2c(const Point&);
  double basis9FuncDiffrr2dQ2c(const Point& pt) {
    return ( -2.*( 1. - pt.y() )*( 1. + pt.y() ) );
  }
  double basis9FuncDiffrs2dQ2c(const Point& pt) {
    return ( 4.*pt.x()*pt.y() );
  }
  double basis9FuncDiffsr2dQ2c(const Point& pt) {
    return ( 4.*pt.x()*pt.y() );
  }
  double basis9FuncDiffss2dQ2c(const Point& pt) {
    return ( -2.*(1. - pt.x() )*( 1. + pt.x() ) );
  }

  static const FunctionXYZ _Func2dQ2c[] = {
    basis1Func2dQ2c, basis2Func2dQ2c, basis3Func2dQ2c, basis4Func2dQ2c,
    basis5Func2dQ2c, basis6Func2dQ2c, basis7Func2dQ2c, basis8Func2dQ2c,
    basis9Func2dQ2c
  };


  static const FunctionXYZ _FuncDiff2dQ2c[] = {
    basis1FuncDiffr2dQ2c, basis1FuncDiffs2dQ2c,
    basis2FuncDiffr2dQ2c, basis2FuncDiffs2dQ2c,
    basis3FuncDiffr2dQ2c, basis3FuncDiffs2dQ2c,
    basis4FuncDiffr2dQ2c, basis4FuncDiffs2dQ2c,
    basis5FuncDiffr2dQ2c, basis5FuncDiffs2dQ2c,
    basis6FuncDiffr2dQ2c, basis6FuncDiffs2dQ2c,
    basis7FuncDiffr2dQ2c, basis7FuncDiffs2dQ2c,
    basis8FuncDiffr2dQ2c, basis8FuncDiffs2dQ2c,
    basis9FuncDiffr2dQ2c, basis9FuncDiffs2dQ2c
  };

  static const FunctionXYZ _FuncDiffHess2dQ2c[] = {
    basis1FuncDiffrr2dQ2c, basis1FuncDiffrs2dQ2c, basis1FuncDiffsr2dQ2c, basis1FuncDiffss2dQ2c,
    basis2FuncDiffrr2dQ2c, basis2FuncDiffrs2dQ2c, basis2FuncDiffsr2dQ2c, basis2FuncDiffss2dQ2c,
    basis3FuncDiffrr2dQ2c, basis3FuncDiffrs2dQ2c, basis3FuncDiffsr2dQ2c, basis3FuncDiffss2dQ2c,
    basis4FuncDiffrr2dQ2c, basis4FuncDiffrs2dQ2c, basis4FuncDiffsr2dQ2c, basis4FuncDiffss2dQ2c,
    basis5FuncDiffrr2dQ2c, basis5FuncDiffrs2dQ2c, basis5FuncDiffsr2dQ2c, basis5FuncDiffss2dQ2c,
    basis6FuncDiffrr2dQ2c, basis6FuncDiffrs2dQ2c, basis6FuncDiffsr2dQ2c, basis6FuncDiffss2dQ2c,
    basis7FuncDiffrr2dQ2c, basis7FuncDiffrs2dQ2c, basis7FuncDiffsr2dQ2c, basis7FuncDiffss2dQ2c,
    basis8FuncDiffrr2dQ2c, basis8FuncDiffrs2dQ2c, basis8FuncDiffsr2dQ2c, basis8FuncDiffss2dQ2c,
    basis9FuncDiffrr2dQ2c, basis9FuncDiffrs2dQ2c, basis9FuncDiffsr2dQ2c, basis9FuncDiffss2dQ2c
  };

  const BasisFunction basisFunction2dQ2c("basisFunction2dQ2c",9,2,_Func2dQ2c,_FuncDiff2dQ2c,_FuncDiffHess2dQ2c);

  /************************************************************************
   *   basisFunction3dP1
   *************************************************************************/
  double basis1Func3dP1(const Point&);
  double basis2Func3dP1(const Point&);
  double basis3Func3dP1(const Point&);
  double basis4Func3dP1(const Point&);
  double basis1Func3dP1(const Point& pt) {
    return (1.- pt.x() - pt.y() - pt.z());
  }
  double basis2Func3dP1(const Point& pt) {
    return pt.x();
  }
  double basis3Func3dP1(const Point& pt) {
    return pt.y();
  }
  double basis4Func3dP1(const Point& pt) {
    return pt.z();
  }
  // First derivatives
  double basis1FuncDiffr3dP1(const Point&);
  double basis1FuncDiffs3dP1(const Point&);
  double basis1FuncDifft3dP1(const Point&);
  double basis2FuncDiffr3dP1(const Point&);
  double basis2FuncDiffs3dP1(const Point&);
  double basis2FuncDifft3dP1(const Point&);
  double basis3FuncDiffr3dP1(const Point&);
  double basis3FuncDiffs3dP1(const Point&);
  double basis3FuncDifft3dP1(const Point&);
  double basis4FuncDiffr3dP1(const Point&);
  double basis4FuncDiffs3dP1(const Point&);
  double basis4FuncDifft3dP1(const Point&);
  double basis1FuncDiffr3dP1(const Point&) {
    return -1.;
  }
  double basis1FuncDiffs3dP1(const Point&) {
    return -1.;
  }
  double basis1FuncDifft3dP1(const Point&) {
    return -1.;
  }
  double basis2FuncDiffr3dP1(const Point&) {
    return 1.;
  }
  double basis2FuncDiffs3dP1(const Point&) {
    return 0.;
  }
  double basis2FuncDifft3dP1(const Point&) {
    return 0.;
  }
  double basis3FuncDiffr3dP1(const Point&) {
    return 0.;
  }
  double basis3FuncDiffs3dP1(const Point&) {
    return 1.;
  }
  double basis3FuncDifft3dP1(const Point&) {
    return 0.;
  }
  double basis4FuncDiffr3dP1(const Point&) {
    return 0.;
  }
  double basis4FuncDiffs3dP1(const Point&) {
    return 0.;
  }
  double basis4FuncDifft3dP1(const Point&) {
    return 1.;
  }
  // Second derivatives
  double basisFuncDiffrr3dP1(const Point&);
  double basisFuncDiffrr3dP1(const Point&) {
    return 0.;
  }

  static const FunctionXYZ _Func3dP1[] = {basis1Func3dP1, basis2Func3dP1, basis3Func3dP1, basis4Func3dP1};

  static const FunctionXYZ _FuncDiff3dP1[] = {
    basis1FuncDiffr3dP1, basis1FuncDiffs3dP1, basis1FuncDifft3dP1,
    basis2FuncDiffr3dP1, basis2FuncDiffs3dP1, basis2FuncDifft3dP1,
    basis3FuncDiffr3dP1, basis3FuncDiffs3dP1, basis3FuncDifft3dP1,
    basis4FuncDiffr3dP1, basis4FuncDiffs3dP1, basis4FuncDifft3dP1
  };

  static const FunctionXYZ _FuncDiffHess3dP1[] = {
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1
  };

  const BasisFunction basisFunction3dP1("basisFunction3dP1",4,3,_Func3dP1,_FuncDiff3dP1,_FuncDiffHess3dP1);



  /************************************************************************
   *   basisFunction3dP1b = P1 + bubble
   *************************************************************************/
  double basis5Func3dP1b(const Point&);
  double basis5Func3dP1b(const Point& pt) {
    return 256.*(1.-pt.x()-pt.y()-pt.z())*pt.x()*pt.y()*pt.z();
  }

  // First derivatives
  double basis5FuncDiffr3dP1b(const Point&);
  double basis5FuncDiffs3dP1b(const Point&);
  double basis5FuncDifft3dP1b(const Point&);
  double basis5FuncDiffr3dP1b(const Point& pt) {
    return 256.*(1.-2.*pt.x()-pt.y()-pt.z())*pt.y()*pt.z();
  }
  double basis5FuncDiffs3dP1b(const Point& pt) {
    return 256.*(1.-pt.x()-2.*pt.y()-pt.z())*pt.x()*pt.z();
  }
  double basis5FuncDifft3dP1b(const Point& pt) {
    return 256.*(1.-pt.x()-pt.y()-2.*pt.z())*pt.x()*pt.y();
  }

  // Second derivatives
  double basis5FuncDiffrr3dP1b(const Point&);
  double basis5FuncDiffrs3dP1b(const Point&);
  double basis5FuncDiffrt3dP1b(const Point&);
  double basis5FuncDiffsr3dP1b(const Point&);
  double basis5FuncDiffss3dP1b(const Point&);
  double basis5FuncDiffst3dP1b(const Point&);
  double basis5FuncDifftr3dP1b(const Point&);
  double basis5FuncDiffts3dP1b(const Point&);
  double basis5FuncDifftt3dP1b(const Point&);
  double basis5FuncDiffrr3dP1b(const Point& pt) {
    return -512.*pt.y()*pt.z();
  }
  double basis5FuncDiffrs3dP1b(const Point& pt) {
    return 256.*(1.-2.*pt.x()-2.*pt.y()-pt.z())*pt.z();
  }
  double basis5FuncDiffrt3dP1b(const Point& pt) {
    return 256.*(1.-2.*pt.x()-pt.y()-2.*pt.z())*pt.y();
  }
  double basis5FuncDiffsr3dP1b(const Point& pt) {
    return 256.*(1.-2.*pt.x()-2.*pt.y()-pt.z())*pt.z();
  }
  double basis5FuncDiffss3dP1b(const Point& pt) {
    return -512.*pt.x()*pt.z();
  }
  double basis5FuncDiffst3dP1b(const Point& pt) {
    return 256.*(1.-pt.x()-2.*pt.y()-2.*pt.z())*pt.x();
  }
  double basis5FuncDifftr3dP1b(const Point& pt) {
    return 256.*(1.-2.*pt.x()-pt.y()-2.*pt.z())*pt.y();
  }
  double basis5FuncDiffts3dP1b(const Point& pt) {
    return 256.*(1.-pt.x()-2.*pt.y()-2.*pt.z())*pt.x();
  }
  double basis5FuncDifftt3dP1b(const Point& pt) {
    return -512.*pt.x()*pt.y();
  }


  static const FunctionXYZ _Func3dP1b[] = {basis1Func3dP1, basis2Func3dP1, basis3Func3dP1, basis4Func3dP1, basis5Func3dP1b};

  static const FunctionXYZ _FuncDiff3dP1b[] = {
    basis1FuncDiffr3dP1, basis1FuncDiffs3dP1, basis1FuncDifft3dP1,
    basis2FuncDiffr3dP1, basis2FuncDiffs3dP1, basis2FuncDifft3dP1,
    basis3FuncDiffr3dP1, basis3FuncDiffs3dP1, basis3FuncDifft3dP1,
    basis4FuncDiffr3dP1, basis4FuncDiffs3dP1, basis4FuncDifft3dP1,
    basis5FuncDiffr3dP1b, basis5FuncDiffs3dP1b, basis5FuncDifft3dP1b
  };

  static const FunctionXYZ _FuncDiffHess3dP1b[] = {
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1, basisFuncDiffrr3dP1,
    basis5FuncDiffrr3dP1b, basis5FuncDiffrs3dP1b, basis5FuncDiffrt3dP1b, basis5FuncDiffsr3dP1b, basis5FuncDiffss3dP1b, basis5FuncDiffst3dP1b, basis5FuncDifftr3dP1b, basis5FuncDiffts3dP1b, basis5FuncDifftt3dP1b
  };

  const BasisFunction basisFunction3dP1b("basisFunction3dP1b",5,3,_Func3dP1b,_FuncDiff3dP1b,_FuncDiffHess3dP1b);

  /************************************************************************
   *   basisFunction3dP2
   *************************************************************************/
  double basis1Func3dP2(const Point&);
  double basis2Func3dP2(const Point&);
  double basis3Func3dP2(const Point&);
  double basis4Func3dP2(const Point&);
  double basis5Func3dP2(const Point&);
  double basis6Func3dP2(const Point&);
  double basis7Func3dP2(const Point&);
  double basis8Func3dP2(const Point&);
  double basis9Func3dP2(const Point&);
  double basis10Func3dP2(const Point&);
  double basis1Func3dP2(const Point& pt) {
    return -( 1. - pt.x() - pt.y() - pt.z() ) * ( 1. - 2. * ( 1. - pt.x() - pt.y() - pt.z() ) ) ;
  }
  double basis2Func3dP2(const Point& pt) {
    return -pt.x() * ( 1. - 2. * pt.x() ) ;
  }
  double basis3Func3dP2(const Point& pt) {
    return -pt.y() * ( 1. - 2. * pt.y() ) ;
  }
  double basis4Func3dP2(const Point& pt) {
    return -pt.z() * ( 1. - 2. * pt.z() ) ;
  }
  double basis5Func3dP2(const Point& pt) {
    return 4. * pt.x() * ( 1. - pt.x() - pt.y() - pt.z() ) ;
  }
  double basis6Func3dP2(const Point& pt) {
    return 4. * pt.x() * pt.y() ;
  }
  double basis7Func3dP2(const Point& pt) {
    return 4. * pt.y() * ( 1. - pt.x() - pt.y() - pt.z() ) ;
  }
  double basis8Func3dP2(const Point& pt) {
    return 4. * pt.z() * ( 1. - pt.x() - pt.y() - pt.z() ) ;
  }
  double basis9Func3dP2(const Point& pt) {
    return 4. * pt.x() * pt.z() ;
  }
  double basis10Func3dP2(const Point& pt) {
    return 4. * pt.y() * pt.z() ;
  }

  // First derivatives
  double basis1FuncDiffr3dP2(const Point&);
  double basis1FuncDiffs3dP2(const Point&);
  double basis1FuncDifft3dP2(const Point&);
  double basis1FuncDiffr3dP2(const Point& pt) {
    return -3. + 4. * pt.x() + 4. * pt.y() + 4. * pt.z() ;
  }
  double basis1FuncDiffs3dP2(const Point& pt) {
    return -3. + 4. * pt.x() + 4. * pt.y() + 4. * pt.z() ;
  }
  double basis1FuncDifft3dP2(const Point& pt) {
    return -3. + 4. * pt.x() + 4. * pt.y() + 4. * pt.z() ;
  }

  double basis2FuncDiffr3dP2(const Point&);
  double basis2FuncDiffs3dP2(const Point&);
  double basis2FuncDifft3dP2(const Point&);
  double basis2FuncDiffr3dP2(const Point& pt) {
    return -1. + 4. * pt.x() ;
  }
  double basis2FuncDiffs3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }
  double basis2FuncDifft3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }

  double basis3FuncDiffr3dP2(const Point&);
  double basis3FuncDiffs3dP2(const Point&);
  double basis3FuncDifft3dP2(const Point&);
  double basis3FuncDiffr3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }
  double basis3FuncDiffs3dP2(const Point& pt) {
    return -1. + 4. * pt.y() ;
  }
  double basis3FuncDifft3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }

  double basis4FuncDiffr3dP2(const Point&);
  double basis4FuncDiffs3dP2(const Point&);
  double basis4FuncDifft3dP2(const Point&);
  double basis4FuncDiffr3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }
  double basis4FuncDiffs3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }
  double basis4FuncDifft3dP2(const Point& pt) {
    return -1. + 4. * pt.z() ;
  }

  double basis5FuncDiffr3dP2(const Point&);
  double basis5FuncDiffs3dP2(const Point&);
  double basis5FuncDifft3dP2(const Point&);
  double basis5FuncDiffr3dP2(const Point& pt) {
    return 4. - 8. * pt.x() - 4. * pt.y() - 4. * pt.z() ;
  }
  double basis5FuncDiffs3dP2(const Point& pt) {
    return -4. * pt.x() ;
  }
  double basis5FuncDifft3dP2(const Point& pt) {
    return -4. * pt.x() ;
  }

  double basis6FuncDiffr3dP2(const Point&);
  double basis6FuncDiffs3dP2(const Point&);
  double basis6FuncDifft3dP2(const Point&);
  double basis6FuncDiffr3dP2(const Point& pt) {
    return 4. * pt.y() ;
  }
  double basis6FuncDiffs3dP2(const Point& pt) {
    return 4. * pt.x() ;
  }
  double basis6FuncDifft3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }

  double basis7FuncDiffr3dP2(const Point&);
  double basis7FuncDiffs3dP2(const Point&);
  double basis7FuncDifft3dP2(const Point&);
  double basis7FuncDiffr3dP2(const Point& pt) {
    return -4. * pt.y() ;
  }
  double basis7FuncDiffs3dP2(const Point& pt) {
    return 4. - 4. * pt.x() - 8. * pt.y() - 4. * pt.z() ;
  }
  double basis7FuncDifft3dP2(const Point& pt) {
    return -4. * pt.y() ;
  }

  double basis8FuncDiffr3dP2(const Point&);
  double basis8FuncDiffs3dP2(const Point&);
  double basis8FuncDifft3dP2(const Point&);
  double basis8FuncDiffr3dP2(const Point& pt) {
    return -4. * pt.z() ;
  }
  double basis8FuncDiffs3dP2(const Point& pt) {
    return -4. * pt.z() ;
  }
  double basis8FuncDifft3dP2(const Point& pt) {
    return 4. - 4. * pt.x() - 4. * pt.y() - 8. * pt.z() ;
  }

  double basis9FuncDiffr3dP2(const Point&);
  double basis9FuncDiffs3dP2(const Point&);
  double basis9FuncDifft3dP2(const Point&);
  double basis9FuncDiffr3dP2(const Point& pt) {
    return 4. * pt.z() ;
  }
  double basis9FuncDiffs3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }
  double basis9FuncDifft3dP2(const Point& pt) {
    return 4. * pt.x() ;
  }

  double basis10FuncDiffr3dP2(const Point&);
  double basis10FuncDiffs3dP2(const Point&);
  double basis10FuncDifft3dP2(const Point&);
  double basis10FuncDiffr3dP2(const Point& pt) {
    (void) pt;
    return 0. ;
  }
  double basis10FuncDiffs3dP2(const Point& pt) {
    return 4. * pt.z() ;
  }
  double basis10FuncDifft3dP2(const Point& pt) {
    return 4. * pt.y() ;
  }

  // Second derivatives
  double basis1FuncDiffrr3dP2(const Point&);
  double basis1FuncDiffrs3dP2(const Point&);
  double basis1FuncDiffrt3dP2(const Point&);
  double basis1FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis1FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis1FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }

  double basis1FuncDiffsr3dP2(const Point&);
  double basis1FuncDiffss3dP2(const Point&);
  double basis1FuncDiffst3dP2(const Point&);
  double basis1FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis1FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis1FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }

  double basis1FuncDifftr3dP2(const Point&);
  double basis1FuncDiffts3dP2(const Point&);
  double basis1FuncDifftt3dP2(const Point&);
  double basis1FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis1FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis1FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }

  double basis2FuncDiffrr3dP2(const Point&);
  double basis2FuncDiffrs3dP2(const Point&);
  double basis2FuncDiffrt3dP2(const Point&);
  double basis2FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis2FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis2FuncDiffsr3dP2(const Point&);
  double basis2FuncDiffss3dP2(const Point&);
  double basis2FuncDiffst3dP2(const Point&);
  double basis2FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis2FuncDifftr3dP2(const Point&);
  double basis2FuncDiffts3dP2(const Point&);
  double basis2FuncDifftt3dP2(const Point&);
  double basis2FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis3FuncDiffrr3dP2(const Point&);
  double basis3FuncDiffrs3dP2(const Point&);
  double basis3FuncDiffrt3dP2(const Point&);
  double basis3FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis3FuncDiffsr3dP2(const Point&);
  double basis3FuncDiffss3dP2(const Point&);
  double basis3FuncDiffst3dP2(const Point&);
  double basis3FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis3FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis3FuncDifftr3dP2(const Point&);
  double basis3FuncDiffts3dP2(const Point&);
  double basis3FuncDifftt3dP2(const Point&);
  double basis3FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis4FuncDiffrr3dP2(const Point&);
  double basis4FuncDiffrs3dP2(const Point&);
  double basis4FuncDiffrt3dP2(const Point&);
  double basis4FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis4FuncDiffsr3dP2(const Point&);
  double basis4FuncDiffss3dP2(const Point&);
  double basis4FuncDiffst3dP2(const Point&);
  double basis4FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis4FuncDifftr3dP2(const Point&);
  double basis4FuncDiffts3dP2(const Point&);
  double basis4FuncDifftt3dP2(const Point&);
  double basis4FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }

  double basis5FuncDiffrr3dP2(const Point&);
  double basis5FuncDiffrs3dP2(const Point&);
  double basis5FuncDiffrt3dP2(const Point&);
  double basis5FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  -8. ;
  }
  double basis5FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis5FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }

  double basis5FuncDiffsr3dP2(const Point&);
  double basis5FuncDiffss3dP2(const Point&);
  double basis5FuncDiffst3dP2(const Point&);
  double basis5FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis5FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis5FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis5FuncDifftr3dP2(const Point&);
  double basis5FuncDiffts3dP2(const Point&);
  double basis5FuncDifftt3dP2(const Point&);
  double basis5FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis5FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis5FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis6FuncDiffrr3dP2(const Point&);
  double basis6FuncDiffrs3dP2(const Point&);
  double basis6FuncDiffrt3dP2(const Point&);
  double basis6FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis6FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis6FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis6FuncDiffsr3dP2(const Point&);
  double basis6FuncDiffss3dP2(const Point&);
  double basis6FuncDiffst3dP2(const Point&);
  double basis6FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis6FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis6FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis6FuncDifftr3dP2(const Point&);
  double basis6FuncDiffts3dP2(const Point&);
  double basis6FuncDifftt3dP2(const Point&);
  double basis6FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis6FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis6FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis7FuncDiffrr3dP2(const Point&);
  double basis7FuncDiffrs3dP2(const Point&);
  double basis7FuncDiffrt3dP2(const Point&);
  double basis7FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis7FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis7FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  0.  ;
  }

  double basis7FuncDiffsr3dP2(const Point&);
  double basis7FuncDiffss3dP2(const Point&);
  double basis7FuncDiffst3dP2(const Point&);
  double basis7FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis7FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  -8. ;
  }
  double basis7FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }

  double basis7FuncDifftr3dP2(const Point&);
  double basis7FuncDiffts3dP2(const Point&);
  double basis7FuncDifftt3dP2(const Point&);
  double basis7FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis7FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis7FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis8FuncDiffrr3dP2(const Point&);
  double basis8FuncDiffrs3dP2(const Point&);
  double basis8FuncDiffrt3dP2(const Point&);
  double basis8FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis8FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis8FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }

  double basis8FuncDiffsr3dP2(const Point&);
  double basis8FuncDiffss3dP2(const Point&);
  double basis8FuncDiffst3dP2(const Point&);
  double basis8FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis8FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis8FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }

  double basis8FuncDifftr3dP2(const Point&);
  double basis8FuncDiffts3dP2(const Point&);
  double basis8FuncDifftt3dP2(const Point&);
  double basis8FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis8FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  -4. ;
  }
  double basis8FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  -8. ;
  }

  double basis9FuncDiffrr3dP2(const Point&);
  double basis9FuncDiffrs3dP2(const Point&);
  double basis9FuncDiffrt3dP2(const Point&);
  double basis9FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis9FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis9FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }

  double basis9FuncDiffsr3dP2(const Point&);
  double basis9FuncDiffss3dP2(const Point&);
  double basis9FuncDiffst3dP2(const Point&);
  double basis9FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis9FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis9FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis9FuncDifftr3dP2(const Point&);
  double basis9FuncDiffts3dP2(const Point&);
  double basis9FuncDifftt3dP2(const Point&);
  double basis9FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis9FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis9FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis10FuncDiffrr3dP2(const Point&);
  double basis10FuncDiffrs3dP2(const Point&);
  double basis10FuncDiffrt3dP2(const Point&);
  double basis10FuncDiffrr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis10FuncDiffrs3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis10FuncDiffrt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis10FuncDiffsr3dP2(const Point&);
  double basis10FuncDiffss3dP2(const Point&);
  double basis10FuncDiffst3dP2(const Point&);
  double basis10FuncDiffsr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis10FuncDiffss3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis10FuncDiffst3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }

  double basis10FuncDifftr3dP2(const Point&);
  double basis10FuncDiffts3dP2(const Point&);
  double basis10FuncDifftt3dP2(const Point&);
  double basis10FuncDifftr3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis10FuncDiffts3dP2(const Point& pt) {
    (void) pt;
    return  4. ;
  }
  double basis10FuncDifftt3dP2(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  static const FunctionXYZ _Func3dP2[10] = {basis1Func3dP2, basis2Func3dP2, basis3Func3dP2, basis4Func3dP2,basis5Func3dP2, basis6Func3dP2, basis7Func3dP2, basis8Func3dP2, basis9Func3dP2, basis10Func3dP2
                                           };

  static const FunctionXYZ _FuncDiff3dP2[30] = {
    basis1FuncDiffr3dP2, basis1FuncDiffs3dP2, basis1FuncDifft3dP2,
    basis2FuncDiffr3dP2, basis2FuncDiffs3dP2, basis2FuncDifft3dP2,
    basis3FuncDiffr3dP2, basis3FuncDiffs3dP2, basis3FuncDifft3dP2,
    basis4FuncDiffr3dP2, basis4FuncDiffs3dP2, basis4FuncDifft3dP2,
    basis5FuncDiffr3dP2, basis5FuncDiffs3dP2, basis5FuncDifft3dP2,
    basis6FuncDiffr3dP2, basis6FuncDiffs3dP2, basis6FuncDifft3dP2,
    basis7FuncDiffr3dP2, basis7FuncDiffs3dP2, basis7FuncDifft3dP2,
    basis8FuncDiffr3dP2, basis8FuncDiffs3dP2, basis8FuncDifft3dP2,
    basis9FuncDiffr3dP2, basis9FuncDiffs3dP2, basis9FuncDifft3dP2,
    basis10FuncDiffr3dP2, basis10FuncDiffs3dP2, basis10FuncDifft3dP2
  };

  static const FunctionXYZ _FuncDiffHess3dP2[90] = {
    basis1FuncDiffrr3dP2, basis1FuncDiffrs3dP2, basis1FuncDiffrt3dP2, basis1FuncDiffsr3dP2, basis1FuncDiffss3dP2, basis1FuncDiffst3dP2, basis1FuncDifftr3dP2, basis1FuncDiffts3dP2, basis1FuncDifftt3dP2,
    basis2FuncDiffrr3dP2, basis2FuncDiffrs3dP2, basis2FuncDiffrt3dP2, basis2FuncDiffsr3dP2, basis2FuncDiffss3dP2, basis2FuncDiffst3dP2, basis2FuncDifftr3dP2, basis2FuncDiffts3dP2, basis2FuncDifftt3dP2,
    basis3FuncDiffrr3dP2, basis3FuncDiffrs3dP2, basis3FuncDiffrt3dP2, basis3FuncDiffsr3dP2, basis3FuncDiffss3dP2, basis3FuncDiffst3dP2, basis3FuncDifftr3dP2, basis3FuncDiffts3dP2, basis3FuncDifftt3dP2,
    basis4FuncDiffrr3dP2, basis4FuncDiffrs3dP2, basis4FuncDiffrt3dP2, basis4FuncDiffsr3dP2, basis4FuncDiffss3dP2, basis4FuncDiffst3dP2, basis4FuncDifftr3dP2, basis4FuncDiffts3dP2, basis4FuncDifftt3dP2,
    basis5FuncDiffrr3dP2, basis5FuncDiffrs3dP2, basis5FuncDiffrt3dP2, basis5FuncDiffsr3dP2, basis5FuncDiffss3dP2, basis5FuncDiffst3dP2, basis5FuncDifftr3dP2, basis5FuncDiffts3dP2, basis5FuncDifftt3dP2,
    basis6FuncDiffrr3dP2, basis6FuncDiffrs3dP2, basis6FuncDiffrt3dP2, basis6FuncDiffsr3dP2, basis6FuncDiffss3dP2, basis6FuncDiffst3dP2, basis6FuncDifftr3dP2, basis6FuncDiffts3dP2, basis6FuncDifftt3dP2,
    basis7FuncDiffrr3dP2, basis7FuncDiffrs3dP2, basis7FuncDiffrt3dP2, basis7FuncDiffsr3dP2, basis7FuncDiffss3dP2, basis7FuncDiffst3dP2, basis7FuncDifftr3dP2, basis7FuncDiffts3dP2, basis7FuncDifftt3dP2,
    basis8FuncDiffrr3dP2, basis8FuncDiffrs3dP2, basis8FuncDiffrt3dP2, basis8FuncDiffsr3dP2, basis8FuncDiffss3dP2, basis8FuncDiffst3dP2, basis8FuncDifftr3dP2, basis8FuncDiffts3dP2, basis8FuncDifftt3dP2,
    basis9FuncDiffrr3dP2, basis9FuncDiffrs3dP2, basis9FuncDiffrt3dP2, basis9FuncDiffsr3dP2, basis9FuncDiffss3dP2, basis9FuncDiffst3dP2, basis9FuncDifftr3dP2, basis9FuncDiffts3dP2, basis9FuncDifftt3dP2,
    basis10FuncDiffrr3dP2, basis10FuncDiffrs3dP2, basis10FuncDiffrt3dP2, basis10FuncDiffsr3dP2, basis10FuncDiffss3dP2, basis10FuncDiffst3dP2, basis10FuncDifftr3dP2, basis10FuncDiffts3dP2, basis10FuncDifftt3dP2,
  };

  const BasisFunction basisFunction3dP2("basisFunction3dP2",10,3,_Func3dP2,_FuncDiff3dP2,_FuncDiffHess3dP2);



  /************************************************************************
   *   basisFunction3dQ1
   *************************************************************************/
  double basis1Func3dQ1(const Point&);
  double basis2Func3dQ1(const Point&);
  double basis3Func3dQ1(const Point&);
  double basis4Func3dQ1(const Point&);
  double basis5Func3dQ1(const Point&);
  double basis6Func3dQ1(const Point&);
  double basis7Func3dQ1(const Point&);
  double basis8Func3dQ1(const Point&);
  double basis1Func3dQ1(const Point& pt) {
    (void) pt;
    return 0.125*( 1. - pt.x() )*( 1. - pt.y() )*( 1. - pt.z() ) ;
  }
  double basis2Func3dQ1(const Point& pt) {
    (void) pt;
    return 0.125*( 1. + pt.x() )*( 1. - pt.y() )*( 1. - pt.z() ) ;
  }
  double basis3Func3dQ1(const Point& pt) {
    return 0.125*( 1. + pt.x() )*( 1. + pt.y() )*( 1. - pt.z() ) ;
  }
  double basis4Func3dQ1(const Point& pt) {
    return 0.125*( 1. - pt.x() )*( 1. + pt.y() )*( 1. - pt.z() ) ;
  }
  double basis5Func3dQ1(const Point& pt) {
    return 0.125*( 1. - pt.x() )*( 1. - pt.y() )*( 1. + pt.z() ) ;
  }
  double basis6Func3dQ1(const Point& pt) {
    return 0.125*( 1. + pt.x() )*( 1. - pt.y() )*( 1. + pt.z() ) ;
  }
  double basis7Func3dQ1(const Point& pt) {
    return 0.125*( 1. + pt.x() )*( 1. + pt.y() )*( 1. + pt.z() ) ;
  }
  double basis8Func3dQ1(const Point& pt) {
    return 0.125*( 1. - pt.x() )*( 1. + pt.y() )*( 1. + pt.z() ) ;
  }
  // First derivatives
  double basis1FuncDiffr3dQ1(const Point&);
  double basis1FuncDiffs3dQ1(const Point&);
  double basis1FuncDifft3dQ1(const Point&);
  double basis1FuncDiffr3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.y() )*( 1. - pt.z() ) ;
  }
  double basis1FuncDiffs3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() )*( 1. - pt.z() ) ;
  }
  double basis1FuncDifft3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() )*( 1. - pt.y() ) ;
  }

  double basis2FuncDiffr3dQ1(const Point&);
  double basis2FuncDiffs3dQ1(const Point&);
  double basis2FuncDifft3dQ1(const Point&);
  double basis2FuncDiffr3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.y() )*( 1. - pt.z() ) ;
  }
  double basis2FuncDiffs3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() )*( 1. - pt.z() ) ;
  }
  double basis2FuncDifft3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() )*( 1. - pt.y() ) ;
  }

  double basis3FuncDiffr3dQ1(const Point&);
  double basis3FuncDiffs3dQ1(const Point&);
  double basis3FuncDifft3dQ1(const Point&);
  double basis3FuncDiffr3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.y() )*(1. - pt.z() ) ;
  }
  double basis3FuncDiffs3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.x() )*(1. - pt.z() ) ;
  }
  double basis3FuncDifft3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() )*(1. + pt.y() ) ;
  }

  double basis4FuncDiffr3dQ1(const Point&);
  double basis4FuncDiffs3dQ1(const Point&);
  double basis4FuncDifft3dQ1(const Point&);
  double basis4FuncDiffr3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.y() )*( 1. - pt.z() ) ;
  }
  double basis4FuncDiffs3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() )*( 1. - pt.z() ) ;
  }
  double basis4FuncDifft3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() )*( 1. + pt.y() ) ;
  }

  double basis5FuncDiffr3dQ1(const Point&);
  double basis5FuncDiffs3dQ1(const Point&);
  double basis5FuncDifft3dQ1(const Point&);
  double basis5FuncDiffr3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.y() )*( 1. + pt.z() ) ;
  }
  double basis5FuncDiffs3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() )*( 1. + pt.z() ) ;
  }
  double basis5FuncDifft3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() )*( 1. - pt.y() ) ;
  }

  double basis6FuncDiffr3dQ1(const Point&);
  double basis6FuncDiffs3dQ1(const Point&);
  double basis6FuncDifft3dQ1(const Point&);
  double basis6FuncDiffr3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.y() )*( 1. + pt.z() ) ;
  }
  double basis6FuncDiffs3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() )*( 1. + pt.z() ) ;
  }
  double basis6FuncDifft3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.x() )*( 1. - pt.y() ) ;
  }

  double basis7FuncDiffr3dQ1(const Point&);
  double basis7FuncDiffs3dQ1(const Point&);
  double basis7FuncDifft3dQ1(const Point&);
  double basis7FuncDiffr3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.y() )*(1. + pt.z() ) ;
  }
  double basis7FuncDiffs3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.x() )*(1. + pt.z() ) ;
  }
  double basis7FuncDifft3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.x() )*(1. + pt.y() ) ;
  }

  double basis8FuncDiffr3dQ1(const Point&);
  double basis8FuncDiffs3dQ1(const Point&);
  double basis8FuncDifft3dQ1(const Point&);
  double basis8FuncDiffr3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.y() )*(1. + pt.z() ) ;
  }
  double basis8FuncDiffs3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() )*(1. + pt.z() ) ;
  }
  double basis8FuncDifft3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() )*(1. + pt.y() );
  }

  // Second derivatives
  double basis1FuncDiffrr3dQ1(const Point&);
  double basis1FuncDiffrs3dQ1(const Point&);
  double basis1FuncDiffrt3dQ1(const Point&);
  double basis1FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis1FuncDiffrs3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.z() ) ;
  }
  double basis1FuncDiffrt3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.y() ) ;
  }

  double basis1FuncDiffsr3dQ1(const Point&);
  double basis1FuncDiffss3dQ1(const Point&);
  double basis1FuncDiffst3dQ1(const Point&);
  double basis1FuncDiffsr3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.z() ) ;
  }
  double basis1FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis1FuncDiffst3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() ) ;
  }

  double basis1FuncDifftr3dQ1(const Point&);
  double basis1FuncDiffts3dQ1(const Point&);
  double basis1FuncDifftt3dQ1(const Point&);
  double basis1FuncDifftr3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.y() ) ;
  }
  double basis1FuncDiffts3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() ) ;
  }
  double basis1FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis2FuncDiffrr3dQ1(const Point&);
  double basis2FuncDiffrs3dQ1(const Point&);
  double basis2FuncDiffrt3dQ1(const Point&);
  double basis2FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDiffrs3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.z() ) ;
  }
  double basis2FuncDiffrt3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.y() ) ;
  }

  double basis2FuncDiffsr3dQ1(const Point&);
  double basis2FuncDiffss3dQ1(const Point&);
  double basis2FuncDiffst3dQ1(const Point&);
  double basis2FuncDiffsr3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.z() ) ;
  }
  double basis2FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis2FuncDiffst3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() ) ;
  }

  double basis2FuncDifftr3dQ1(const Point&);
  double basis2FuncDiffts3dQ1(const Point&);
  double basis2FuncDifftt3dQ1(const Point&);
  double basis2FuncDifftr3dQ1(const Point& pt) {
    return  -0.125*( 1. - pt.y() );
  }
  double basis2FuncDiffts3dQ1(const Point& pt) {
    return   0.125*( 1. + pt.x() );
  }
  double basis2FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return   0. ;
  }

  double basis3FuncDiffrr3dQ1(const Point&);
  double basis3FuncDiffrs3dQ1(const Point&);
  double basis3FuncDiffrt3dQ1(const Point&);
  double basis3FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDiffrs3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.z() ) ;
  }
  double basis3FuncDiffrt3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.y() ) ;
  }

  double basis3FuncDiffsr3dQ1(const Point&);
  double basis3FuncDiffss3dQ1(const Point&);
  double basis3FuncDiffst3dQ1(const Point&);
  double basis3FuncDiffsr3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.z() ) ;
  }
  double basis3FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis3FuncDiffst3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() ) ;
  }

  double basis3FuncDifftr3dQ1(const Point&);
  double basis3FuncDiffts3dQ1(const Point&);
  double basis3FuncDifftt3dQ1(const Point&);
  double basis3FuncDifftr3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.y() ) ;
  }
  double basis3FuncDiffts3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() ) ;
  }
  double basis3FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis4FuncDiffrr3dQ1(const Point&);
  double basis4FuncDiffrs3dQ1(const Point&);
  double basis4FuncDiffrt3dQ1(const Point&);
  double basis4FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffrs3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.z() );
  }
  double basis4FuncDiffrt3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.y() );
  }

  double basis4FuncDiffsr3dQ1(const Point&);
  double basis4FuncDiffss3dQ1(const Point&);
  double basis4FuncDiffst3dQ1(const Point&);
  double basis4FuncDiffsr3dQ1(const Point& pt) {
    return -0.125*( 1. -pt.z() ) ;
  }
  double basis4FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis4FuncDiffst3dQ1(const Point& pt) {
    return -0.125*(1. - pt.x() ) ;
  }

  double basis4FuncDifftr3dQ1(const Point&);
  double basis4FuncDiffts3dQ1(const Point&);
  double basis4FuncDifftt3dQ1(const Point&);
  double basis4FuncDifftr3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.y() ) ;
  }
  double basis4FuncDiffts3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() ) ;
  }
  double basis4FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis5FuncDiffrr3dQ1(const Point&);
  double basis5FuncDiffrs3dQ1(const Point&);
  double basis5FuncDiffrt3dQ1(const Point&);
  double basis5FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis5FuncDiffrs3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.z() ) ;
  }
  double basis5FuncDiffrt3dQ1(const Point& pt) {
    return -0.125*( 1. -pt.y() ) ;
  }

  double basis5FuncDiffsr3dQ1(const Point&);
  double basis5FuncDiffss3dQ1(const Point&);
  double basis5FuncDiffst3dQ1(const Point&);
  double basis5FuncDiffsr3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.z() );
  }
  double basis5FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis5FuncDiffst3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() ) ;
  }

  double basis5FuncDifftr3dQ1(const Point&);
  double basis5FuncDiffts3dQ1(const Point&);
  double basis5FuncDifftt3dQ1(const Point&);
  double basis5FuncDifftr3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.y() ) ;
  }
  double basis5FuncDiffts3dQ1(const Point& pt) {
    return -0.125*( 1. - pt.x() ) ;
  }
  double basis5FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis6FuncDiffrr3dQ1(const Point&);
  double basis6FuncDiffrs3dQ1(const Point&);
  double basis6FuncDiffrt3dQ1(const Point&);
  double basis6FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis6FuncDiffrs3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.z() ) ;
  }
  double basis6FuncDiffrt3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.y() ) ;
  }

  double basis6FuncDiffsr3dQ1(const Point&);
  double basis6FuncDiffss3dQ1(const Point&);
  double basis6FuncDiffst3dQ1(const Point&);
  double basis6FuncDiffsr3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.z() ) ;
  }
  double basis6FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis6FuncDiffst3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() );
  }

  double basis6FuncDifftr3dQ1(const Point&);
  double basis6FuncDiffts3dQ1(const Point&);
  double basis6FuncDifftt3dQ1(const Point&);
  double basis6FuncDifftr3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.y() );
  }
  double basis6FuncDiffts3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.x() ) ;
  }
  double basis6FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis7FuncDiffrr3dQ1(const Point&);
  double basis7FuncDiffrs3dQ1(const Point&);
  double basis7FuncDiffrt3dQ1(const Point&);
  double basis7FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis7FuncDiffrs3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.z() );
  }
  double basis7FuncDiffrt3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.z() );
  }

  double basis7FuncDiffsr3dQ1(const Point&);
  double basis7FuncDiffss3dQ1(const Point&);
  double basis7FuncDiffst3dQ1(const Point&);
  double basis7FuncDiffsr3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.z() ) ;
  }
  double basis7FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis7FuncDiffst3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.x() ) ;
  }

  double basis7FuncDifftr3dQ1(const Point&);
  double basis7FuncDiffts3dQ1(const Point&);
  double basis7FuncDifftt3dQ1(const Point&);
  double basis7FuncDifftr3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.y() );
  }
  double basis7FuncDiffts3dQ1(const Point& pt) {
    return  0.125*( 1. + pt.x() );
  }
  double basis7FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  double basis8FuncDiffrr3dQ1(const Point&);
  double basis8FuncDiffrs3dQ1(const Point&);
  double basis8FuncDiffrt3dQ1(const Point&);
  double basis8FuncDiffrr3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis8FuncDiffrs3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.z() );
  }
  double basis8FuncDiffrt3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.y() );
  }

  double basis8FuncDiffsr3dQ1(const Point&);
  double basis8FuncDiffss3dQ1(const Point&);
  double basis8FuncDiffst3dQ1(const Point&);
  double basis8FuncDiffsr3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.z() ) ;
  }
  double basis8FuncDiffss3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }
  double basis8FuncDiffst3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() ) ;
  }

  double basis8FuncDifftr3dQ1(const Point&);
  double basis8FuncDiffts3dQ1(const Point&);
  double basis8FuncDifftt3dQ1(const Point&);
  double basis8FuncDifftr3dQ1(const Point& pt) {
    return -0.125*( 1. + pt.y() );
  }
  double basis8FuncDiffts3dQ1(const Point& pt) {
    return  0.125*( 1. - pt.x() );
  }
  double basis8FuncDifftt3dQ1(const Point& pt) {
    (void) pt;
    return  0. ;
  }

  static const FunctionXYZ _Func3dQ1[8] = {basis1Func3dQ1, basis2Func3dQ1, basis3Func3dQ1, basis4Func3dQ1,
                                          basis5Func3dQ1, basis6Func3dQ1, basis7Func3dQ1, basis8Func3dQ1
                                          };

  static const FunctionXYZ _FuncDiff3dQ1[24] = {
    basis1FuncDiffr3dQ1, basis1FuncDiffs3dQ1, basis1FuncDifft3dQ1,
    basis2FuncDiffr3dQ1, basis2FuncDiffs3dQ1, basis2FuncDifft3dQ1,
    basis3FuncDiffr3dQ1, basis3FuncDiffs3dQ1, basis3FuncDifft3dQ1,
    basis4FuncDiffr3dQ1, basis4FuncDiffs3dQ1, basis4FuncDifft3dQ1,
    basis5FuncDiffr3dQ1, basis5FuncDiffs3dQ1, basis5FuncDifft3dQ1,
    basis6FuncDiffr3dQ1, basis6FuncDiffs3dQ1, basis6FuncDifft3dQ1,
    basis7FuncDiffr3dQ1, basis7FuncDiffs3dQ1, basis7FuncDifft3dQ1,
    basis8FuncDiffr3dQ1, basis8FuncDiffs3dQ1, basis8FuncDifft3dQ1
  };

  static const FunctionXYZ _FuncDiffHess3dQ1[72] = {
    basis1FuncDiffrr3dQ1, basis1FuncDiffrs3dQ1, basis1FuncDiffrt3dQ1, basis1FuncDiffsr3dQ1, basis1FuncDiffss3dQ1, basis1FuncDiffst3dQ1, basis1FuncDifftr3dQ1, basis1FuncDiffts3dQ1, basis1FuncDifftt3dQ1,
    basis2FuncDiffrr3dQ1, basis2FuncDiffrs3dQ1, basis2FuncDiffrt3dQ1, basis2FuncDiffsr3dQ1, basis2FuncDiffss3dQ1, basis2FuncDiffst3dQ1, basis2FuncDifftr3dQ1, basis2FuncDiffts3dQ1, basis2FuncDifftt3dQ1,
    basis3FuncDiffrr3dQ1, basis3FuncDiffrs3dQ1, basis3FuncDiffrt3dQ1, basis3FuncDiffsr3dQ1, basis3FuncDiffss3dQ1, basis3FuncDiffst3dQ1, basis3FuncDifftr3dQ1, basis3FuncDiffts3dQ1, basis3FuncDifftt3dQ1,
    basis4FuncDiffrr3dQ1, basis4FuncDiffrs3dQ1, basis4FuncDiffrt3dQ1, basis4FuncDiffsr3dQ1, basis4FuncDiffss3dQ1, basis4FuncDiffst3dQ1, basis4FuncDifftr3dQ1, basis4FuncDiffts3dQ1, basis4FuncDifftt3dQ1,
    basis5FuncDiffrr3dQ1, basis5FuncDiffrs3dQ1, basis5FuncDiffrt3dQ1, basis5FuncDiffsr3dQ1, basis5FuncDiffss3dQ1, basis5FuncDiffst3dQ1, basis5FuncDifftr3dQ1, basis5FuncDiffts3dQ1, basis5FuncDifftt3dQ1,
    basis6FuncDiffrr3dQ1, basis6FuncDiffrs3dQ1, basis6FuncDiffrt3dQ1, basis6FuncDiffsr3dQ1, basis6FuncDiffss3dQ1, basis6FuncDiffst3dQ1, basis6FuncDifftr3dQ1, basis6FuncDiffts3dQ1, basis6FuncDifftt3dQ1,
    basis7FuncDiffrr3dQ1, basis7FuncDiffrs3dQ1, basis7FuncDiffrt3dQ1, basis7FuncDiffsr3dQ1, basis7FuncDiffss3dQ1, basis7FuncDiffst3dQ1, basis7FuncDifftr3dQ1, basis7FuncDiffts3dQ1, basis7FuncDifftt3dQ1,
    basis8FuncDiffrr3dQ1, basis8FuncDiffrs3dQ1, basis8FuncDiffrt3dQ1, basis8FuncDiffsr3dQ1, basis8FuncDiffss3dQ1, basis8FuncDiffst3dQ1, basis8FuncDifftr3dQ1, basis8FuncDiffts3dQ1, basis8FuncDifftt3dQ1
  };

  const BasisFunction basisFunction3dQ1("basisFunction3dQ1",8,3,_Func3dQ1,_FuncDiff3dQ1,_FuncDiffHess3dQ1);



  /************************************************************************
  *   basisFunction3dQ1b = Q1 + bubble
  *************************************************************************/
  double basis9Func3dQ1b(const Point&);
  double basis9Func3dQ1b(const Point& pt) {
    return (1. - pt.x()*pt.x())*(1. - pt.y()*pt.y())*(1. - pt.z()*pt.z()) ;
  }

  // First derivatives
  double basis9FuncDiffr3dQ1b(const Point&);
  double basis9FuncDiffs3dQ1b(const Point&);
  double basis9FuncDifft3dQ1b(const Point&);
  double basis9FuncDiffr3dQ1b(const Point& pt) {
    return -2.*pt.x()*(1. - pt.y()*pt.y())*(1. - pt.z()*pt.z());
  }
  double basis9FuncDiffs3dQ1b(const Point& pt) {
    return -2.*pt.y()*(1. - pt.x()*pt.x())*(1. - pt.z()*pt.z());
  }
  double basis9FuncDifft3dQ1b(const Point& pt) {
    return -2.*pt.z()*(1. - pt.x()*pt.x())*(1. - pt.y()*pt.y());
  }

  // Second derivatives
  double basis9FuncDiffrr3dQ1b(const Point&);
  double basis9FuncDiffrs3dQ1b(const Point&);
  double basis9FuncDiffrt3dQ1b(const Point&);
  double basis9FuncDiffrr3dQ1b(const Point& pt) {
    return -2.*(1. - pt.y()*pt.y())*(1. - pt.z()*pt.z());
  }
  double basis9FuncDiffrs3dQ1b(const Point& pt) {
    return 4.*pt.x()*pt.y()*(1.-pt.z());
  }
  double basis9FuncDiffrt3dQ1b(const Point& pt) {
    return 4.*pt.x()*pt.z()*(1.-pt.y());
  }

  double basis9FuncDiffsr3dQ1b(const Point&);
  double basis9FuncDiffss3dQ1b(const Point&);
  double basis9FuncDiffst3dQ1b(const Point&);
  double basis9FuncDiffsr3dQ1b(const Point& pt) {
    return 4.*pt.x()*pt.y()*(1.-pt.z());
  }
  double basis9FuncDiffss3dQ1b(const Point& pt) {
    return -2.*(1. - pt.x()*pt.x())*(1. - pt.z()*pt.z());
  }
  double basis9FuncDiffst3dQ1b(const Point& pt) {
    return 4.*pt.y()*pt.z()*(1.-pt.x());
  }

  double basis9FuncDifftr3dQ1b(const Point&);
  double basis9FuncDiffts3dQ1b(const Point&);
  double basis9FuncDifftt3dQ1b(const Point&);
  double basis9FuncDifftr3dQ1b(const Point& pt) {
    return 4.*pt.x()*pt.z()*(1.-pt.y());
  }
  double basis9FuncDiffts3dQ1b(const Point& pt) {
    return 4.*pt.y()*pt.z()*(1.-pt.x());
  }
  double basis9FuncDifftt3dQ1b(const Point& pt) {
    return -2.*(1. - pt.x()*pt.x())*(1. - pt.y()*pt.y());
  }

  static const FunctionXYZ _Func3dQ1b[9] = {basis1Func3dQ1, basis2Func3dQ1, basis3Func3dQ1, basis4Func3dQ1,
      basis5Func3dQ1, basis6Func3dQ1, basis7Func3dQ1, basis8Func3dQ1,
      basis9Func3dQ1b
                                           };

  static const FunctionXYZ _FuncDiff3dQ1b[27] = {
    basis1FuncDiffr3dQ1, basis1FuncDiffs3dQ1, basis1FuncDifft3dQ1,
    basis2FuncDiffr3dQ1, basis2FuncDiffs3dQ1, basis2FuncDifft3dQ1,
    basis3FuncDiffr3dQ1, basis3FuncDiffs3dQ1, basis3FuncDifft3dQ1,
    basis4FuncDiffr3dQ1, basis4FuncDiffs3dQ1, basis4FuncDifft3dQ1,
    basis5FuncDiffr3dQ1, basis5FuncDiffs3dQ1, basis5FuncDifft3dQ1,
    basis6FuncDiffr3dQ1, basis6FuncDiffs3dQ1, basis6FuncDifft3dQ1,
    basis7FuncDiffr3dQ1, basis7FuncDiffs3dQ1, basis7FuncDifft3dQ1,
    basis8FuncDiffr3dQ1, basis8FuncDiffs3dQ1, basis8FuncDifft3dQ1,
    basis9FuncDiffr3dQ1b, basis9FuncDiffs3dQ1b, basis9FuncDifft3dQ1b
  };

  static const FunctionXYZ _FuncDiffHess3dQ1b[81] = {
    basis1FuncDiffrr3dQ1, basis1FuncDiffrs3dQ1, basis1FuncDiffrt3dQ1, basis1FuncDiffsr3dQ1, basis1FuncDiffss3dQ1, basis1FuncDiffst3dQ1, basis1FuncDifftr3dQ1, basis1FuncDiffts3dQ1, basis1FuncDifftt3dQ1,
    basis2FuncDiffrr3dQ1, basis2FuncDiffrs3dQ1, basis2FuncDiffrt3dQ1, basis2FuncDiffsr3dQ1, basis2FuncDiffss3dQ1, basis2FuncDiffst3dQ1, basis2FuncDifftr3dQ1, basis2FuncDiffts3dQ1, basis2FuncDifftt3dQ1,
    basis3FuncDiffrr3dQ1, basis3FuncDiffrs3dQ1, basis3FuncDiffrt3dQ1, basis3FuncDiffsr3dQ1, basis3FuncDiffss3dQ1, basis3FuncDiffst3dQ1, basis3FuncDifftr3dQ1, basis3FuncDiffts3dQ1, basis3FuncDifftt3dQ1,
    basis4FuncDiffrr3dQ1, basis4FuncDiffrs3dQ1, basis4FuncDiffrt3dQ1, basis4FuncDiffsr3dQ1, basis4FuncDiffss3dQ1, basis4FuncDiffst3dQ1, basis4FuncDifftr3dQ1, basis4FuncDiffts3dQ1, basis4FuncDifftt3dQ1,
    basis5FuncDiffrr3dQ1, basis5FuncDiffrs3dQ1, basis5FuncDiffrt3dQ1, basis5FuncDiffsr3dQ1, basis5FuncDiffss3dQ1, basis5FuncDiffst3dQ1, basis5FuncDifftr3dQ1, basis5FuncDiffts3dQ1, basis5FuncDifftt3dQ1,
    basis6FuncDiffrr3dQ1, basis6FuncDiffrs3dQ1, basis6FuncDiffrt3dQ1, basis6FuncDiffsr3dQ1, basis6FuncDiffss3dQ1, basis6FuncDiffst3dQ1, basis6FuncDifftr3dQ1, basis6FuncDiffts3dQ1, basis6FuncDifftt3dQ1,
    basis7FuncDiffrr3dQ1, basis7FuncDiffrs3dQ1, basis7FuncDiffrt3dQ1, basis7FuncDiffsr3dQ1, basis7FuncDiffss3dQ1, basis7FuncDiffst3dQ1, basis7FuncDifftr3dQ1, basis7FuncDiffts3dQ1, basis7FuncDifftt3dQ1,
    basis8FuncDiffrr3dQ1, basis8FuncDiffrs3dQ1, basis8FuncDiffrt3dQ1, basis8FuncDiffsr3dQ1, basis8FuncDiffss3dQ1, basis8FuncDiffst3dQ1, basis8FuncDifftr3dQ1, basis8FuncDiffts3dQ1, basis8FuncDifftt3dQ1,
    basis9FuncDiffrr3dQ1b, basis9FuncDiffrs3dQ1b, basis9FuncDiffrt3dQ1b, basis9FuncDiffsr3dQ1b, basis9FuncDiffss3dQ1b, basis9FuncDiffst3dQ1b, basis9FuncDifftr3dQ1b, basis9FuncDiffts3dQ1b, basis9FuncDifftt3dQ1b
  };

  const BasisFunction basisFunction3dQ1b("basisFunction3dQ1b",9,3,_Func3dQ1b,_FuncDiff3dQ1b,_FuncDiffHess3dQ1b);



  /************************************************************************
   *   basisFunction3dQ2
   *************************************************************************/

  static const double refcoor_Q2_3D[] = {-1.,-1.,-1.,  1.,-1.,-1.,  1.,1.,-1.,  -1.,1.,-1.,  -1.,-1.,1.,  1.,-1.,1.,  1.,1.,1.,  -1.,1.,1.,  0.,-1.,-1.,  1.,0.,-1.,
                                         0.,1.,-1.,  -1.,0.,-1.,  -1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0.,  0.,-1.,1.,  1.,0.,1.,  0.,1.,1., -1.,0.,1.
                                        };

  template <int i>
  double basisFunc3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*( -2. + refcoor_Q2_3D[3*i]*pt.x() + refcoor_Q2_3D[3*i+1]*pt.y() + refcoor_Q2_3D[3*i+2]*pt.z() )*
             ( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return .25*( 1. - pt.x()*pt.x() )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return .25*( 1. + pt.x()*refcoor_Q2_3D[3*i] )*( 1. - pt.y()*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return .25*( 1. + pt.x()*refcoor_Q2_3D[3*i] )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. - pt.z()*pt.z() );
  }

  // first derivatives

  template <int i>
  double basisFuncDiffr3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i]*( -1. + 2.*refcoor_Q2_3D[3*i]*pt.x() + refcoor_Q2_3D[3*i+1]*pt.y() + refcoor_Q2_3D[3*i+2]*pt.z() )*
             ( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -.5*pt.x()*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return .25*refcoor_Q2_3D[3*i]*( 1. - pt.y()*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return .25*refcoor_Q2_3D[3*i]*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. - pt.z()*pt.z() );
  }

  template <int i>
  double basisFuncDiffs3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i+1]*( -1. + refcoor_Q2_3D[3*i]*pt.x() + 2.*refcoor_Q2_3D[3*i+1]*pt.y() + refcoor_Q2_3D[3*i+2]*pt.z() )
             *( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return .25*( 1. - pt.x()*pt.x() )*refcoor_Q2_3D[3*i+1]*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -.5*pt.y()*( 1. + pt.x()*refcoor_Q2_3D[3*i] )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return .25*( 1. + pt.x()*refcoor_Q2_3D[3*i] )*refcoor_Q2_3D[3*i+1]*( 1. - pt.z()*pt.z() );
  }

  template <int i>
  double basisFuncDifft3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i+2]*( -1. + refcoor_Q2_3D[3*i]*pt.x() + refcoor_Q2_3D[3*i+1]*pt.y() + 2.*refcoor_Q2_3D[3*i+2]*pt.z() )
             *( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return .25*( 1. - pt.x()*pt.x() )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*refcoor_Q2_3D[3*i+2];
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return .25*( 1. + pt.x()*refcoor_Q2_3D[3*i] )*( 1. - pt.y()*pt.y() )*refcoor_Q2_3D[3*i+2];
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -.5*pt.z()*( 1. + pt.x()*refcoor_Q2_3D[3*i] )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
  }

  // Second derivatives

  template <int i>
  double basisFuncDiffrr3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.25*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.;
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.;
  }

  template <int i>
  double basisFuncDiffrs3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i]*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() )*( 2.*pt.y() + 2.*
             refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+1]*pt.x() + refcoor_Q2_3D[3*i+1]*
             refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2_3D[3*i+1]*pt.x()*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2_3D[3*i]*pt.y()*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.25*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+1]*( 1. - pt.z()*pt.z() );
  }

  template <int i>
  double basisFuncDiffrt3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i]*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*
             ( 2.*pt.z() + 2.*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+2]*pt.x() + refcoor_Q2_3D[3*i+2]*pt.y() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2_3D[3*i+2]*pt.x()*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.25*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+2]*( 1. - pt.y()*pt.y() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2_3D[3*i]*pt.z()*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
  }

  template <int i>
  double basisFuncDiffsr3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i+1]*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() )*
             ( 2.*pt.x() + 2.*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+1]*pt.y() + refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2_3D[3*i+1]*pt.x()*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2_3D[3*i]*pt.y()*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.25*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+1]*( 1. - pt.z()*pt.z() );
  }

  template <int i>
  double basisFuncDiffss3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.25*( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.;
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.;
  }

  template <int i>
  double basisFuncDiffst3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2_3D[3*i+1]*( 1. + refcoor_Q2_3D[3*i]*pt.x() )*
             ( 2.*pt.z() + 2.*refcoor_Q2_3D[3*i+1]*refcoor_Q2_3D[3*i+2]*pt.y() + refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+2]*pt.x() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.25*refcoor_Q2_3D[3*i+1]*refcoor_Q2_3D[3*i+2]*(1. - pt.x()*pt.x() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2_3D[3*i+2]*pt.z()*( 1. + refcoor_Q2_3D[3*i]*pt.x() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2_3D[3*i+1]*pt.z()*( 1. + refcoor_Q2_3D[3*i]*pt.x() );
  }

  template <int i>
  double basisFuncDifftr3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*pt.z()*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*
             ( 2.*pt.x() + refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+1]*pt.y() + 2.*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.5*refcoor_Q2_3D[3*i+2]*pt.x()*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.25*refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+2]*( 1. - pt.y()*pt.y() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2_3D[3*i]*pt.z()*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
  }

  template <int i>
  double basisFuncDiffts3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*pt.z()*( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 2.*pt.y() + refcoor_Q2_3D[3*i]*refcoor_Q2_3D[3*i+1]*pt.x() +
             refcoor_Q2_3D[3*i+1]*refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.25*refcoor_Q2_3D[3*i+1]*refcoor_Q2_3D[3*i+2]*( 1. - pt.x()*pt.x() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2_3D[3*i+2]*pt.y()*( 1. + refcoor_Q2_3D[3*i]*pt.x() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2_3D[3*i+1]*pt.z()*( 1. + refcoor_Q2_3D[3*i]*pt.x() );
  }

  template <int i>
  double basisFuncDifftt3dQ2(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() )*
             ( -1. + refcoor_Q2_3D[3*i]*pt.x() + refcoor_Q2_3D[3*i+1]*pt.y() + 4.*refcoor_Q2_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.;
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.;
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*( 1. + refcoor_Q2_3D[3*i]*pt.x() )*( 1. + refcoor_Q2_3D[3*i+1]*pt.y() );
  }


  static const FunctionXYZ _Func3dQ2[20] = { basisFunc3dQ2<0>,basisFunc3dQ2<1>,basisFunc3dQ2<2>,basisFunc3dQ2<3>,basisFunc3dQ2<4>,basisFunc3dQ2<5>,
      basisFunc3dQ2<6>,basisFunc3dQ2<7>,basisFunc3dQ2<8>,basisFunc3dQ2<9>,basisFunc3dQ2<10>,basisFunc3dQ2<11>,basisFunc3dQ2<12>,basisFunc3dQ2<13>,basisFunc3dQ2<14>,
      basisFunc3dQ2<15>,basisFunc3dQ2<16>,basisFunc3dQ2<17>,basisFunc3dQ2<18>,basisFunc3dQ2<19>
                                           };

  static const FunctionXYZ _FuncDiff3dQ2[60] = {
    basisFuncDiffr3dQ2<0>, basisFuncDiffs3dQ2<0>, basisFuncDifft3dQ2<0>,
    basisFuncDiffr3dQ2<1>, basisFuncDiffs3dQ2<1>, basisFuncDifft3dQ2<1>,
    basisFuncDiffr3dQ2<2>, basisFuncDiffs3dQ2<2>, basisFuncDifft3dQ2<2>,
    basisFuncDiffr3dQ2<3>, basisFuncDiffs3dQ2<3>, basisFuncDifft3dQ2<3>,
    basisFuncDiffr3dQ2<4>, basisFuncDiffs3dQ2<4>, basisFuncDifft3dQ2<4>,
    basisFuncDiffr3dQ2<5>, basisFuncDiffs3dQ2<5>, basisFuncDifft3dQ2<5>,
    basisFuncDiffr3dQ2<6>, basisFuncDiffs3dQ2<6>, basisFuncDifft3dQ2<6>,
    basisFuncDiffr3dQ2<7>, basisFuncDiffs3dQ2<7>, basisFuncDifft3dQ2<7>,
    basisFuncDiffr3dQ2<8>, basisFuncDiffs3dQ2<8>, basisFuncDifft3dQ2<8>,
    basisFuncDiffr3dQ2<9>, basisFuncDiffs3dQ2<9>, basisFuncDifft3dQ2<9>,
    basisFuncDiffr3dQ2<10>, basisFuncDiffs3dQ2<10>, basisFuncDifft3dQ2<10>,
    basisFuncDiffr3dQ2<11>, basisFuncDiffs3dQ2<11>, basisFuncDifft3dQ2<11>,
    basisFuncDiffr3dQ2<12>, basisFuncDiffs3dQ2<12>, basisFuncDifft3dQ2<12>,
    basisFuncDiffr3dQ2<13>, basisFuncDiffs3dQ2<13>, basisFuncDifft3dQ2<13>,
    basisFuncDiffr3dQ2<14>, basisFuncDiffs3dQ2<14>, basisFuncDifft3dQ2<14>,
    basisFuncDiffr3dQ2<15>, basisFuncDiffs3dQ2<15>, basisFuncDifft3dQ2<15>,
    basisFuncDiffr3dQ2<16>, basisFuncDiffs3dQ2<16>, basisFuncDifft3dQ2<16>,
    basisFuncDiffr3dQ2<17>, basisFuncDiffs3dQ2<17>, basisFuncDifft3dQ2<17>,
    basisFuncDiffr3dQ2<18>, basisFuncDiffs3dQ2<18>, basisFuncDifft3dQ2<18>,
    basisFuncDiffr3dQ2<19>, basisFuncDiffs3dQ2<19>, basisFuncDifft3dQ2<19>
  };


  static const FunctionXYZ _FuncDiffHess3dQ2[180] = {
    basisFuncDiffrr3dQ2<0>,basisFuncDiffrs3dQ2<0>,basisFuncDiffrt3dQ2<0>,basisFuncDiffsr3dQ2<0>,basisFuncDiffss3dQ2<0>,basisFuncDiffst3dQ2<0>,basisFuncDifftr3dQ2<0>,basisFuncDiffts3dQ2<0>,basisFuncDifftt3dQ2<0>,
    basisFuncDiffrr3dQ2<1>,basisFuncDiffrs3dQ2<1>,basisFuncDiffrt3dQ2<1>,basisFuncDiffsr3dQ2<1>,basisFuncDiffss3dQ2<1>,basisFuncDiffst3dQ2<1>,basisFuncDifftr3dQ2<1>,basisFuncDiffts3dQ2<1>,basisFuncDifftt3dQ2<1>,
    basisFuncDiffrr3dQ2<2>,basisFuncDiffrs3dQ2<2>,basisFuncDiffrt3dQ2<2>,basisFuncDiffsr3dQ2<2>,basisFuncDiffss3dQ2<2>,basisFuncDiffst3dQ2<2>,basisFuncDifftr3dQ2<2>,basisFuncDiffts3dQ2<2>,basisFuncDifftt3dQ2<2>,
    basisFuncDiffrr3dQ2<3>,basisFuncDiffrs3dQ2<3>,basisFuncDiffrt3dQ2<3>,basisFuncDiffsr3dQ2<3>,basisFuncDiffss3dQ2<3>,basisFuncDiffst3dQ2<3>,basisFuncDifftr3dQ2<3>,basisFuncDiffts3dQ2<3>,basisFuncDifftt3dQ2<3>,
    basisFuncDiffrr3dQ2<4>,basisFuncDiffrs3dQ2<4>,basisFuncDiffrt3dQ2<4>,basisFuncDiffsr3dQ2<4>,basisFuncDiffss3dQ2<4>,basisFuncDiffst3dQ2<4>,basisFuncDifftr3dQ2<4>,basisFuncDiffts3dQ2<4>,basisFuncDifftt3dQ2<4>,
    basisFuncDiffrr3dQ2<5>,basisFuncDiffrs3dQ2<5>,basisFuncDiffrt3dQ2<5>,basisFuncDiffsr3dQ2<5>,basisFuncDiffss3dQ2<5>,basisFuncDiffst3dQ2<5>,basisFuncDifftr3dQ2<5>,basisFuncDiffts3dQ2<5>,basisFuncDifftt3dQ2<5>,
    basisFuncDiffrr3dQ2<6>,basisFuncDiffrs3dQ2<6>,basisFuncDiffrt3dQ2<6>,basisFuncDiffsr3dQ2<6>,basisFuncDiffss3dQ2<6>,basisFuncDiffst3dQ2<6>,basisFuncDifftr3dQ2<6>,basisFuncDiffts3dQ2<6>,basisFuncDifftt3dQ2<6>,
    basisFuncDiffrr3dQ2<7>,basisFuncDiffrs3dQ2<7>,basisFuncDiffrt3dQ2<7>,basisFuncDiffsr3dQ2<7>,basisFuncDiffss3dQ2<7>,basisFuncDiffst3dQ2<7>,basisFuncDifftr3dQ2<7>,basisFuncDiffts3dQ2<7>,basisFuncDifftt3dQ2<7>,
    basisFuncDiffrr3dQ2<8>,basisFuncDiffrs3dQ2<8>,basisFuncDiffrt3dQ2<8>,basisFuncDiffsr3dQ2<8>,basisFuncDiffss3dQ2<8>,basisFuncDiffst3dQ2<8>,basisFuncDifftr3dQ2<8>,basisFuncDiffts3dQ2<8>,basisFuncDifftt3dQ2<8>,
    basisFuncDiffrr3dQ2<9>,basisFuncDiffrs3dQ2<9>,basisFuncDiffrt3dQ2<9>,basisFuncDiffsr3dQ2<9>,basisFuncDiffss3dQ2<9>,basisFuncDiffst3dQ2<9>,basisFuncDifftr3dQ2<9>,basisFuncDiffts3dQ2<9>,basisFuncDifftt3dQ2<9>,
    basisFuncDiffrr3dQ2<10>,basisFuncDiffrs3dQ2<10>,basisFuncDiffrt3dQ2<10>,basisFuncDiffsr3dQ2<10>,basisFuncDiffss3dQ2<10>,basisFuncDiffst3dQ2<10>,basisFuncDifftr3dQ2<10>,basisFuncDiffts3dQ2<10>,basisFuncDifftt3dQ2<10>,
    basisFuncDiffrr3dQ2<11>,basisFuncDiffrs3dQ2<11>,basisFuncDiffrt3dQ2<11>,basisFuncDiffsr3dQ2<11>,basisFuncDiffss3dQ2<11>,basisFuncDiffst3dQ2<11>,basisFuncDifftr3dQ2<11>,basisFuncDiffts3dQ2<11>,basisFuncDifftt3dQ2<11>,
    basisFuncDiffrr3dQ2<12>,basisFuncDiffrs3dQ2<12>,basisFuncDiffrt3dQ2<12>,basisFuncDiffsr3dQ2<12>,basisFuncDiffss3dQ2<12>,basisFuncDiffst3dQ2<12>,basisFuncDifftr3dQ2<12>,basisFuncDiffts3dQ2<12>,basisFuncDifftt3dQ2<12>,
    basisFuncDiffrr3dQ2<13>,basisFuncDiffrs3dQ2<13>,basisFuncDiffrt3dQ2<13>,basisFuncDiffsr3dQ2<13>,basisFuncDiffss3dQ2<13>,basisFuncDiffst3dQ2<13>,basisFuncDifftr3dQ2<13>,basisFuncDiffts3dQ2<13>,basisFuncDifftt3dQ2<13>,
    basisFuncDiffrr3dQ2<14>,basisFuncDiffrs3dQ2<14>,basisFuncDiffrt3dQ2<14>,basisFuncDiffsr3dQ2<14>,basisFuncDiffss3dQ2<14>,basisFuncDiffst3dQ2<14>,basisFuncDifftr3dQ2<14>,basisFuncDiffts3dQ2<14>,basisFuncDifftt3dQ2<14>,
    basisFuncDiffrr3dQ2<15>,basisFuncDiffrs3dQ2<15>,basisFuncDiffrt3dQ2<15>,basisFuncDiffsr3dQ2<15>,basisFuncDiffss3dQ2<15>,basisFuncDiffst3dQ2<15>,basisFuncDifftr3dQ2<15>,basisFuncDiffts3dQ2<15>,basisFuncDifftt3dQ2<15>,
    basisFuncDiffrr3dQ2<16>,basisFuncDiffrs3dQ2<16>,basisFuncDiffrt3dQ2<16>,basisFuncDiffsr3dQ2<16>,basisFuncDiffss3dQ2<16>,basisFuncDiffst3dQ2<16>,basisFuncDifftr3dQ2<16>,basisFuncDiffts3dQ2<16>,basisFuncDifftt3dQ2<16>,
    basisFuncDiffrr3dQ2<17>,basisFuncDiffrs3dQ2<17>,basisFuncDiffrt3dQ2<17>,basisFuncDiffsr3dQ2<17>,basisFuncDiffss3dQ2<17>,basisFuncDiffst3dQ2<17>,basisFuncDifftr3dQ2<17>,basisFuncDiffts3dQ2<17>,basisFuncDifftt3dQ2<17>,
    basisFuncDiffrr3dQ2<18>,basisFuncDiffrs3dQ2<18>,basisFuncDiffrt3dQ2<18>,basisFuncDiffsr3dQ2<18>,basisFuncDiffss3dQ2<18>,basisFuncDiffst3dQ2<18>,basisFuncDifftr3dQ2<18>,basisFuncDiffts3dQ2<18>,basisFuncDifftt3dQ2<18>,
    basisFuncDiffrr3dQ2<19>,basisFuncDiffrs3dQ2<19>,basisFuncDiffrt3dQ2<19>,basisFuncDiffsr3dQ2<19>,basisFuncDiffss3dQ2<19>,basisFuncDiffst3dQ2<19>,basisFuncDifftr3dQ2<19>,basisFuncDiffts3dQ2<19>,basisFuncDifftt3dQ2<19>,
  };

  const BasisFunction basisFunction3dQ2("basisFunction3dQ2",20,3,_Func3dQ2,_FuncDiff3dQ2,_FuncDiffHess3dQ2);

  /************************************************************************
   *   basisFunction3dQ2c
   *************************************************************************/

  static const double refcoor_Q2c_3D[] = {-1.,-1.,-1.,  1.,-1.,-1.,  1.,1.,-1.,  -1.,1.,-1.,  -1.,-1.,1.,  1.,-1.,1.,  1.,1.,1.,  -1.,1.,1.,  0.,-1.,-1.,  1.,0.,-1.,
                                          0.,1.,-1.,  -1.,0.,-1.,  -1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0.,  0.,-1.,1.,  1.,0.,1.,  0.,1.,1., -1.,0.,1.,  0.,0.,-1. ,
                                          -1.,0.,0.,  0.,-1.,0., 0.,0.,1.,  1.,0.,0.,  0.,1.,0.,  0.,0.,0.
                                         };

  template <int i>
  double basisFunc3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*refcoor_Q2c_3D[3*i]*pt.x()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*
             refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return .25*( 1. - pt.x()*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*refcoor_Q2c_3D[3*i+1]*pt.y()*
             ( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return .25*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*pt.x()*refcoor_Q2c_3D[3*i]*( 1. - pt.y()*pt.y() )*
             ( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return .25*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*pt.x()*refcoor_Q2c_3D[3*i]*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*
             refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. - pt.z()*pt.z() );
    else if constexpr(i==20 || i == 23)
      return .5*( 1. - pt.x()*pt.x() )*( 1. - pt.y()*pt.y() )*refcoor_Q2c_3D[3*i+2]*pt.z()*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return .5*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*refcoor_Q2c_3D[3*i]*pt.x()*( 1. - pt.y()*pt.y() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==22 || i==25)
      return .5*( 1. - pt.x()*pt.x() )*refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==26)
      return ( 1. - pt.x()*pt.x() )*( 1. - pt.y()*pt.y() )*( 1. - pt.z()*pt.z() );
  }

  static const FunctionXYZ _Func3dQ2c[27] = { basisFunc3dQ2c<0>,basisFunc3dQ2c<1>,basisFunc3dQ2c<2>,basisFunc3dQ2c<3>,basisFunc3dQ2c<4>,basisFunc3dQ2c<5>,
      basisFunc3dQ2c<6>,basisFunc3dQ2c<7>,basisFunc3dQ2c<8>,basisFunc3dQ2c<9>,basisFunc3dQ2c<10>,basisFunc3dQ2c<11>,basisFunc3dQ2c<12>,basisFunc3dQ2c<13>,basisFunc3dQ2c<14>,
      basisFunc3dQ2c<15>,basisFunc3dQ2c<16>,basisFunc3dQ2c<17>,basisFunc3dQ2c<18>,basisFunc3dQ2c<19>,basisFunc3dQ2c<20>,basisFunc3dQ2c<21>,basisFunc3dQ2c<22>,
      basisFunc3dQ2c<23>,basisFunc3dQ2c<24>,basisFunc3dQ2c<25>,basisFunc3dQ2c<26>
                                            };

  // first derivatives

  template <int i>
  double basisFuncDiffr3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*(1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )
             *refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -.5*pt.x()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. +
             refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return .25*( 1. + 2.*pt.x()*refcoor_Q2c_3D[3*i] )*refcoor_Q2c_3D[3*i]*( 1. - pt.y()*pt.y() )
             *( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return .25*( 1. + 2.*pt.x()*refcoor_Q2c_3D[3*i] )*refcoor_Q2c_3D[3*i]*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )
             *refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. - pt.z()*pt.z() );
    else if constexpr(i==20 || i == 23)
      return -pt.x()*( 1. - pt.y()*pt.y() )*refcoor_Q2c_3D[3*i+2]*pt.z()*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return .5*( 1. + 2.*pt.x()*refcoor_Q2c_3D[3*i] )*refcoor_Q2c_3D[3*i]*( 1. - pt.y()*pt.y() )
             *( 1. - pt.z()*pt.z() );
    else if constexpr(i==22 || i==25)
      return -pt.x()*refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )
             *( 1. - pt.z()*pt.z() );
    else if constexpr(i==26)
      return -2*pt.x()*( 1. - pt.y()*pt.y() )*( 1. - pt.z()*pt.z() );
  }

  template <int i>
  double basisFuncDiffs3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*refcoor_Q2c_3D[3*i]*pt.x()*( 1. + 2.*refcoor_Q2c_3D[3*i+1]
             *pt.y() )*refcoor_Q2c_3D[3*i+1]*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return .25*( 1. - pt.x()*pt.x() )*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() )*refcoor_Q2c_3D[3*i+1]*
             ( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -.5*pt.y()*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*pt.x()*refcoor_Q2c_3D[3*i]*( 1. + refcoor_Q2c_3D[3*i+2]
             *pt.z() )*refcoor_Q2c_3D[3*i+2]*pt.z();
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return .25*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*pt.x()*refcoor_Q2c_3D[3*i]*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*
             pt.y() )*refcoor_Q2c_3D[3*i+1]*( 1. - pt.z()*pt.z() );
    else if constexpr(i==20 || i == 23)
      return -pt.y()*( 1. - pt.x()*pt.x() )*refcoor_Q2c_3D[3*i+2]*pt.z()*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -pt.y()*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*refcoor_Q2c_3D[3*i]*pt.x()*( 1. - pt.z()*pt.z() );
    else if constexpr(i==22 || i==25)
      return .5*( 1. - pt.x()*pt.x() )*refcoor_Q2c_3D[3*i+1]*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() )*
             ( 1. - pt.z()*pt.z() );
    else if constexpr(i==26)
      return -2*pt.y()*( 1. - pt.x()*pt.x() )*( 1. - pt.z()*pt.z() );
  }

  template <int i>
  double basisFuncDifft3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*refcoor_Q2c_3D[3*i]*pt.x()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )
             *refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2];
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return .25*( 1. - pt.x()*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*refcoor_Q2c_3D[3*i+1]*pt.y()*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2];
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return .25*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*pt.x()*refcoor_Q2c_3D[3*i]*( 1. - pt.y()*pt.y() )*( 1. + 2.
             *refcoor_Q2c_3D[3*i+2]*pt.z() )*refcoor_Q2c_3D[3*i+2];
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -.5*pt.z()*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*pt.x()*refcoor_Q2c_3D[3*i]*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )
             *refcoor_Q2c_3D[3*i+1]*pt.y();
    else if constexpr(i==20 || i == 23)
      return .5*( 1. - pt.x()*pt.x() )*( 1. - pt.y()*pt.y() )*refcoor_Q2c_3D[3*i+2]*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -pt.z()*( 1. + pt.x()*refcoor_Q2c_3D[3*i] )*refcoor_Q2c_3D[3*i]*pt.x()*( 1. - pt.y()*pt.y() );
    else if constexpr(i==22 || i==25)
      return -pt.z()*( 1. - pt.x()*pt.x() )*refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return -2.*pt.z()*( 1. - pt.x()*pt.x() )*( 1. - pt.y()*pt.y() );
  }

  static const FunctionXYZ _FuncDiff3dQ2c[81] = {
    basisFuncDiffr3dQ2c<0>, basisFuncDiffs3dQ2c<0>, basisFuncDifft3dQ2c<0>,
    basisFuncDiffr3dQ2c<1>, basisFuncDiffs3dQ2c<1>, basisFuncDifft3dQ2c<1>,
    basisFuncDiffr3dQ2c<2>, basisFuncDiffs3dQ2c<2>, basisFuncDifft3dQ2c<2>,
    basisFuncDiffr3dQ2c<3>, basisFuncDiffs3dQ2c<3>, basisFuncDifft3dQ2c<3>,
    basisFuncDiffr3dQ2c<4>, basisFuncDiffs3dQ2c<4>, basisFuncDifft3dQ2c<4>,
    basisFuncDiffr3dQ2c<5>, basisFuncDiffs3dQ2c<5>, basisFuncDifft3dQ2c<5>,
    basisFuncDiffr3dQ2c<6>, basisFuncDiffs3dQ2c<6>, basisFuncDifft3dQ2c<6>,
    basisFuncDiffr3dQ2c<7>, basisFuncDiffs3dQ2c<7>, basisFuncDifft3dQ2c<7>,
    basisFuncDiffr3dQ2c<8>, basisFuncDiffs3dQ2c<8>, basisFuncDifft3dQ2c<8>,
    basisFuncDiffr3dQ2c<9>, basisFuncDiffs3dQ2c<9>, basisFuncDifft3dQ2c<9>,
    basisFuncDiffr3dQ2c<10>, basisFuncDiffs3dQ2c<10>, basisFuncDifft3dQ2c<10>,
    basisFuncDiffr3dQ2c<11>, basisFuncDiffs3dQ2c<11>, basisFuncDifft3dQ2c<11>,
    basisFuncDiffr3dQ2c<12>, basisFuncDiffs3dQ2c<12>, basisFuncDifft3dQ2c<12>,
    basisFuncDiffr3dQ2c<13>, basisFuncDiffs3dQ2c<13>, basisFuncDifft3dQ2c<13>,
    basisFuncDiffr3dQ2c<14>, basisFuncDiffs3dQ2c<14>, basisFuncDifft3dQ2c<14>,
    basisFuncDiffr3dQ2c<15>, basisFuncDiffs3dQ2c<15>, basisFuncDifft3dQ2c<15>,
    basisFuncDiffr3dQ2c<16>, basisFuncDiffs3dQ2c<16>, basisFuncDifft3dQ2c<16>,
    basisFuncDiffr3dQ2c<17>, basisFuncDiffs3dQ2c<17>, basisFuncDifft3dQ2c<17>,
    basisFuncDiffr3dQ2c<18>, basisFuncDiffs3dQ2c<18>, basisFuncDifft3dQ2c<18>,
    basisFuncDiffr3dQ2c<19>, basisFuncDiffs3dQ2c<19>, basisFuncDifft3dQ2c<19>,
    basisFuncDiffr3dQ2c<20>, basisFuncDiffs3dQ2c<20>, basisFuncDifft3dQ2c<20>,
    basisFuncDiffr3dQ2c<21>, basisFuncDiffs3dQ2c<21>, basisFuncDifft3dQ2c<21>,
    basisFuncDiffr3dQ2c<22>, basisFuncDiffs3dQ2c<22>, basisFuncDifft3dQ2c<22>,
    basisFuncDiffr3dQ2c<23>, basisFuncDiffs3dQ2c<23>, basisFuncDifft3dQ2c<23>,
    basisFuncDiffr3dQ2c<24>, basisFuncDiffs3dQ2c<24>, basisFuncDifft3dQ2c<24>,
    basisFuncDiffr3dQ2c<25>, basisFuncDiffs3dQ2c<25>, basisFuncDifft3dQ2c<25>,
    basisFuncDiffr3dQ2c<26>, basisFuncDiffs3dQ2c<26>, basisFuncDifft3dQ2c<26>
  };

  // Second derivatives

  template <int i>
  double basisFuncDiffrr3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             pt.y()*pt.z()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.y()*pt.z()*( 1. + refcoor_Q2c_3D[3*i+1]*
             pt.y() )*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*pt.z()*
             ( 1. - pt.y()*pt.y() )*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*
             (1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==20 || i == 23)
      return -refcoor_Q2c_3D[3*i+2]*pt.z()*(1. - pt.y()*pt.y() )*( 1. +
             refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i]*( 1. - pt.y()*
             pt.y() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==22 || i==25)
      return -refcoor_Q2c_3D[3*i+1]*pt.y()*(1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*
             (1. - pt.z()*pt.z() );
    else if constexpr(i==26)
      return -2.*( 1. - pt.y()*pt.y() )*( 1. - pt.z()*
                                          pt.z() );
  }

  template <int i>
  double basisFuncDiffrs3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.z()*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.x()*( 1. +
             refcoor_Q2c_3D[3*i+2]*pt.z() )*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*pt.y()*pt.z()*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*( 1. + 2.*refcoor_Q2c_3D[3*i]*
             pt.x() )*( 1. - pt.z()*pt.z() )*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==20 || i == 23)
      return 2.*refcoor_Q2c_3D[3*i+2]*pt.x()*pt.y()*pt.z()*( 1. +
             refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -refcoor_Q2c_3D[3*i]*pt.y()*( 1. + 2.*refcoor_Q2c_3D[3*i]*
                                           pt.x() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==22 || i==25)
      return -refcoor_Q2c_3D[3*i+1]*pt.x()*(1. - pt.z()*pt.z() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return 4.*pt.x()*pt.y()*( 1. - pt.z()*pt.z() ) ;
  }

  template <int i>
  double basisFuncDiffrt3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.y()*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.x()*pt.y()*
             ( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*
                 pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*(1. + 2.*
             refcoor_Q2c_3D[3*i]*pt.x() )*( 1. - pt.y()*pt.y() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*pt.z()*
             pt.y()*( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*
                 pt.y() );
    else if constexpr(i==20 || i == 23)
      return -refcoor_Q2c_3D[3*i+2]*pt.x()*( 1. - pt.y()*pt.y() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -refcoor_Q2c_3D[3*i]*pt.z()*( 1. + 2.*refcoor_Q2c_3D[3*i]*
                                           pt.x() )*( 1. - pt.y()*pt.y() ) ;
    else if constexpr(i==22 || i==25)
      return 2.*refcoor_Q2c_3D[3*i+1]*pt.x()*pt.y()*pt.z()*
             ( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return 4.*pt.x()*pt.z()*( 1. - pt.y()*pt.y() ) ;
  }

  template <int i>
  double basisFuncDiffsr3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             pt.z()*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2c_3D[3*i+2]*
                 pt.z() )*( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.5*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.z()*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2c_3D[3*i+2]*
                 pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*pt.y()*
             pt.z()*( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*( 1. + 2.*
             refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. - pt.z()*pt.z() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==20 || i == 23)
      return 2.*refcoor_Q2c_3D[3*i+2]*pt.x()*pt.y()*pt.z()*
             ( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -refcoor_Q2c_3D[3*i]*pt.y()*(1. - pt.z()*pt.z() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==22 || i==25)
      return -refcoor_Q2c_3D[3*i+1]*pt.x()*(1. + 2.*refcoor_Q2c_3D[3*i+1]*
                                            pt.y() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==26)
      return 4.*pt.x()*pt.y()*( 1. - pt.z()*pt.z() ) ;
  }

  template <int i>
  double basisFuncDiffss3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+1]*
             pt.x()*pt.z()*(1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+2]*
                 pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.5*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             pt.y()*pt.z()*( 1. - pt.x()*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+2]*
                 pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*pt.x()*
             pt.z()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. +
                 refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return 0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+1]*
             pt.x()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. - pt.z()*pt.z() );
    else if constexpr(i==20 || i == 23)
      return -refcoor_Q2c_3D[3*i+2]*pt.z()*( 1. - pt.x()*pt.x() )*
             ( 1. + refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -refcoor_Q2c_3D[3*i]*pt.x()*(1.+refcoor_Q2c_3D[3*i]*pt.x() )*
             ( 1. - pt.z()*pt.z() );
    else if constexpr(i==22 || i==25)
      return refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+1]*( 1. -
             pt.x()*pt.x() )*(1. - pt.z()*pt.z() );
    else if constexpr(i==26)
      return -2.*( 1. - pt.x()*pt.x() )*( 1. - pt.z()*pt.z() ) ;
  }

  template <int i>
  double basisFuncDiffst3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             pt.x()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*
                 pt.y() )*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.25*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             ( 1. - pt.x()*pt.x() )*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*pt.x()*pt.y()*
             ( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*
                 pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*pt.x()*
             pt.z()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==20 || i == 23)
      return -refcoor_Q2c_3D[3*i+2]*pt.y()*( 1. - pt.x()*pt.x() )*
             (1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return 2.*refcoor_Q2c_3D[3*i]*pt.x()*pt.y()*pt.z()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() ) ;
    else if constexpr(i==22 || i==25)
      return -refcoor_Q2c_3D[3*i+1]*pt.z()*( 1. - pt.x()*pt.x() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return 4.*pt.y()*pt.z()*( 1. - pt.x()*pt.x() ) ;
  }

  template <int i>
  double basisFuncDifftr3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             pt.y()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + refcoor_Q2c_3D[3*i+2]*
                 pt.z() )*( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return -0.25*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*pt.x()*
             pt.y()*(1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*(1. -
             pt.y()*pt.y() )*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*pt.y()*
             pt.z()*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==20 || i == 23)
      return -refcoor_Q2c_3D[3*i+2]*pt.x()*( 1. - pt.y()*pt.y() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return -refcoor_Q2c_3D[3*i]*pt.z()*( 1. - pt.y()*pt.y() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==22 || i==25)
      return 2.*refcoor_Q2c_3D[3*i+1]*pt.x()*pt.y()*pt.z()*
             ( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return 4.*pt.x()*pt.z()*( 1. - pt.y()*pt.y() ) ;
  }

  template <int i>
  double basisFuncDiffts3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.125*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             pt.x()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*
                 pt.z() )*( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() ) ;
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.125*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*( 1. - pt.x()*
             pt.x() )*( 1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*pt.x()*
             pt.y()*(1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*pt.x()*
             pt.z()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + 2.*
                 refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==20 || i == 23)
      return -refcoor_Q2c_3D[3*i+2]*pt.y()*(1. - pt.x()*pt.x() )*
             (1. + 2.*refcoor_Q2c_3D[3*i+2]*pt.z() );
    else if constexpr(i==21 || i == 24)
      return 2.*refcoor_Q2c_3D[3*i]*pt.x()*pt.y()*pt.z()*(
               1. + refcoor_Q2c_3D[3*i]*pt.x() );
    else if constexpr(i==22 || i==25)
      return -refcoor_Q2c_3D[3*i+1]*pt.z()*( 1. - pt.x()*pt.x() )*
             ( 1. + 2.*refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return 4.*pt.y()*pt.z()*( 1. - pt.x()*pt.x() ) ;
  }

  template <int i>
  double basisFuncDifftt3dQ2c(const Point& pt) {
    if constexpr(i < 8)
      return 0.25*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*
             refcoor_Q2c_3D[3*i+2]*pt.x()*pt.y()*( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*
             ( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==8 || i==10 || i==16 || i==18)
      return 0.25*refcoor_Q2c_3D[3*i+1]*refcoor_Q2c_3D[3*i+2]*refcoor_Q2c_3D[3*i+2]*
             pt.y()*pt.z()*( 1. - pt.x()*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==9 || i==11 || i==17 || i==19)
      return 0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+2]*refcoor_Q2c_3D[3*i+2]*pt.x()*
             ( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*(1. - pt.y()*pt.y() );
    else if constexpr(i==12 || i==13 || i==14 || i==15)
      return -0.5*refcoor_Q2c_3D[3*i]*refcoor_Q2c_3D[3*i+1]*pt.x()*pt.y()*
             ( 1. + refcoor_Q2c_3D[3*i]*pt.x() )*( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==20 || i == 23)
      return -2.*refcoor_Q2c_3D[3*i+2]*refcoor_Q2c_3D[3*i+2]*pt.x()*( 1. -
             pt.y()*pt.y() );
    else if constexpr(i==21 || i == 24)
      return -refcoor_Q2c_3D[3*i]*pt.x()*(1. + refcoor_Q2c_3D[3*i]*pt.x() )*
             ( 1. - pt.y()*pt.y() );
    else if constexpr(i==22 || i==25)
      return -refcoor_Q2c_3D[3*i+1]*pt.y()*( 1. - pt.x()*pt.x() )*
             ( 1. + refcoor_Q2c_3D[3*i+1]*pt.y() );
    else if constexpr(i==26)
      return -2.*( 1. - pt.x()*pt.x() )*( 1. - pt.y()*pt.y() ) ;
  }

  static const FunctionXYZ _FuncDiffHess3dQ2c[243] = {
    basisFuncDiffrr3dQ2c<0>,basisFuncDiffrs3dQ2c<0>,basisFuncDiffrt3dQ2c<0>,basisFuncDiffsr3dQ2c<0>,basisFuncDiffss3dQ2c<0>,basisFuncDiffst3dQ2c<0>,basisFuncDifftr3dQ2c<0>,basisFuncDiffts3dQ2c<0>,basisFuncDifftt3dQ2c<0>,
    basisFuncDiffrr3dQ2c<1>,basisFuncDiffrs3dQ2c<1>,basisFuncDiffrt3dQ2c<1>,basisFuncDiffsr3dQ2c<1>,basisFuncDiffss3dQ2c<1>,basisFuncDiffst3dQ2c<1>,basisFuncDifftr3dQ2c<1>,basisFuncDiffts3dQ2c<1>,basisFuncDifftt3dQ2c<1>,
    basisFuncDiffrr3dQ2c<2>,basisFuncDiffrs3dQ2c<2>,basisFuncDiffrt3dQ2c<2>,basisFuncDiffsr3dQ2c<2>,basisFuncDiffss3dQ2c<2>,basisFuncDiffst3dQ2c<2>,basisFuncDifftr3dQ2c<2>,basisFuncDiffts3dQ2c<2>,basisFuncDifftt3dQ2c<2>,
    basisFuncDiffrr3dQ2c<3>,basisFuncDiffrs3dQ2c<3>,basisFuncDiffrt3dQ2c<3>,basisFuncDiffsr3dQ2c<3>,basisFuncDiffss3dQ2c<3>,basisFuncDiffst3dQ2c<3>,basisFuncDifftr3dQ2c<3>,basisFuncDiffts3dQ2c<3>,basisFuncDifftt3dQ2c<3>,
    basisFuncDiffrr3dQ2c<4>,basisFuncDiffrs3dQ2c<4>,basisFuncDiffrt3dQ2c<4>,basisFuncDiffsr3dQ2c<4>,basisFuncDiffss3dQ2c<4>,basisFuncDiffst3dQ2c<4>,basisFuncDifftr3dQ2c<4>,basisFuncDiffts3dQ2c<4>,basisFuncDifftt3dQ2c<4>,
    basisFuncDiffrr3dQ2c<5>,basisFuncDiffrs3dQ2c<5>,basisFuncDiffrt3dQ2c<5>,basisFuncDiffsr3dQ2c<5>,basisFuncDiffss3dQ2c<5>,basisFuncDiffst3dQ2c<5>,basisFuncDifftr3dQ2c<5>,basisFuncDiffts3dQ2c<5>,basisFuncDifftt3dQ2c<5>,
    basisFuncDiffrr3dQ2c<6>,basisFuncDiffrs3dQ2c<6>,basisFuncDiffrt3dQ2c<6>,basisFuncDiffsr3dQ2c<6>,basisFuncDiffss3dQ2c<6>,basisFuncDiffst3dQ2c<6>,basisFuncDifftr3dQ2c<6>,basisFuncDiffts3dQ2c<6>,basisFuncDifftt3dQ2c<6>,
    basisFuncDiffrr3dQ2c<7>,basisFuncDiffrs3dQ2c<7>,basisFuncDiffrt3dQ2c<7>,basisFuncDiffsr3dQ2c<7>,basisFuncDiffss3dQ2c<7>,basisFuncDiffst3dQ2c<7>,basisFuncDifftr3dQ2c<7>,basisFuncDiffts3dQ2c<7>,basisFuncDifftt3dQ2c<7>,
    basisFuncDiffrr3dQ2c<8>,basisFuncDiffrs3dQ2c<8>,basisFuncDiffrt3dQ2c<8>,basisFuncDiffsr3dQ2c<8>,basisFuncDiffss3dQ2c<8>,basisFuncDiffst3dQ2c<8>,basisFuncDifftr3dQ2c<8>,basisFuncDiffts3dQ2c<8>,basisFuncDifftt3dQ2c<8>,
    basisFuncDiffrr3dQ2c<9>,basisFuncDiffrs3dQ2c<9>,basisFuncDiffrt3dQ2c<9>,basisFuncDiffsr3dQ2c<9>,basisFuncDiffss3dQ2c<9>,basisFuncDiffst3dQ2c<9>,basisFuncDifftr3dQ2c<9>,basisFuncDiffts3dQ2c<9>,basisFuncDifftt3dQ2c<9>,
    basisFuncDiffrr3dQ2c<10>,basisFuncDiffrs3dQ2c<10>,basisFuncDiffrt3dQ2c<10>,basisFuncDiffsr3dQ2c<10>,basisFuncDiffss3dQ2c<10>,basisFuncDiffst3dQ2c<10>,basisFuncDifftr3dQ2c<10>,basisFuncDiffts3dQ2c<10>,basisFuncDifftt3dQ2c<10>,
    basisFuncDiffrr3dQ2c<11>,basisFuncDiffrs3dQ2c<11>,basisFuncDiffrt3dQ2c<11>,basisFuncDiffsr3dQ2c<11>,basisFuncDiffss3dQ2c<11>,basisFuncDiffst3dQ2c<11>,basisFuncDifftr3dQ2c<11>,basisFuncDiffts3dQ2c<11>,basisFuncDifftt3dQ2c<11>,
    basisFuncDiffrr3dQ2c<12>,basisFuncDiffrs3dQ2c<12>,basisFuncDiffrt3dQ2c<12>,basisFuncDiffsr3dQ2c<12>,basisFuncDiffss3dQ2c<12>,basisFuncDiffst3dQ2c<12>,basisFuncDifftr3dQ2c<12>,basisFuncDiffts3dQ2c<12>,basisFuncDifftt3dQ2c<12>,
    basisFuncDiffrr3dQ2c<13>,basisFuncDiffrs3dQ2c<13>,basisFuncDiffrt3dQ2c<13>,basisFuncDiffsr3dQ2c<13>,basisFuncDiffss3dQ2c<13>,basisFuncDiffst3dQ2c<13>,basisFuncDifftr3dQ2c<13>,basisFuncDiffts3dQ2c<13>,basisFuncDifftt3dQ2c<13>,
    basisFuncDiffrr3dQ2c<14>,basisFuncDiffrs3dQ2c<14>,basisFuncDiffrt3dQ2c<14>,basisFuncDiffsr3dQ2c<14>,basisFuncDiffss3dQ2c<14>,basisFuncDiffst3dQ2c<14>,basisFuncDifftr3dQ2c<14>,basisFuncDiffts3dQ2c<14>,basisFuncDifftt3dQ2c<14>,
    basisFuncDiffrr3dQ2c<15>,basisFuncDiffrs3dQ2c<15>,basisFuncDiffrt3dQ2c<15>,basisFuncDiffsr3dQ2c<15>,basisFuncDiffss3dQ2c<15>,basisFuncDiffst3dQ2c<15>,basisFuncDifftr3dQ2c<15>,basisFuncDiffts3dQ2c<15>,basisFuncDifftt3dQ2c<15>,
    basisFuncDiffrr3dQ2c<16>,basisFuncDiffrs3dQ2c<16>,basisFuncDiffrt3dQ2c<16>,basisFuncDiffsr3dQ2c<16>,basisFuncDiffss3dQ2c<16>,basisFuncDiffst3dQ2c<16>,basisFuncDifftr3dQ2c<16>,basisFuncDiffts3dQ2c<16>,basisFuncDifftt3dQ2c<16>,
    basisFuncDiffrr3dQ2c<17>,basisFuncDiffrs3dQ2c<17>,basisFuncDiffrt3dQ2c<17>,basisFuncDiffsr3dQ2c<17>,basisFuncDiffss3dQ2c<17>,basisFuncDiffst3dQ2c<17>,basisFuncDifftr3dQ2c<17>,basisFuncDiffts3dQ2c<17>,basisFuncDifftt3dQ2c<17>,
    basisFuncDiffrr3dQ2c<18>,basisFuncDiffrs3dQ2c<18>,basisFuncDiffrt3dQ2c<18>,basisFuncDiffsr3dQ2c<18>,basisFuncDiffss3dQ2c<18>,basisFuncDiffst3dQ2c<18>,basisFuncDifftr3dQ2c<18>,basisFuncDiffts3dQ2c<18>,basisFuncDifftt3dQ2c<18>,
    basisFuncDiffrr3dQ2c<19>,basisFuncDiffrs3dQ2c<19>,basisFuncDiffrt3dQ2c<19>,basisFuncDiffsr3dQ2c<19>,basisFuncDiffss3dQ2c<19>,basisFuncDiffst3dQ2c<19>,basisFuncDifftr3dQ2c<19>,basisFuncDiffts3dQ2c<19>,basisFuncDifftt3dQ2c<19>,
    basisFuncDiffrr3dQ2c<20>,basisFuncDiffrs3dQ2c<20>,basisFuncDiffrt3dQ2c<20>,basisFuncDiffsr3dQ2c<20>,basisFuncDiffss3dQ2c<20>,basisFuncDiffst3dQ2c<20>,basisFuncDifftr3dQ2c<20>,basisFuncDiffts3dQ2c<20>,basisFuncDifftt3dQ2c<20>,
    basisFuncDiffrr3dQ2c<21>,basisFuncDiffrs3dQ2c<21>,basisFuncDiffrt3dQ2c<21>,basisFuncDiffsr3dQ2c<21>,basisFuncDiffss3dQ2c<21>,basisFuncDiffst3dQ2c<21>,basisFuncDifftr3dQ2c<21>,basisFuncDiffts3dQ2c<21>,basisFuncDifftt3dQ2c<21>,
    basisFuncDiffrr3dQ2c<22>,basisFuncDiffrs3dQ2c<22>,basisFuncDiffrt3dQ2c<22>,basisFuncDiffsr3dQ2c<22>,basisFuncDiffss3dQ2c<22>,basisFuncDiffst3dQ2c<22>,basisFuncDifftr3dQ2c<22>,basisFuncDiffts3dQ2c<22>,basisFuncDifftt3dQ2c<22>,
    basisFuncDiffrr3dQ2c<23>,basisFuncDiffrs3dQ2c<23>,basisFuncDiffrt3dQ2c<23>,basisFuncDiffsr3dQ2c<23>,basisFuncDiffss3dQ2c<23>,basisFuncDiffst3dQ2c<23>,basisFuncDifftr3dQ2c<23>,basisFuncDiffts3dQ2c<23>,basisFuncDifftt3dQ2c<23>,
    basisFuncDiffrr3dQ2c<24>,basisFuncDiffrs3dQ2c<24>,basisFuncDiffrt3dQ2c<24>,basisFuncDiffsr3dQ2c<24>,basisFuncDiffss3dQ2c<24>,basisFuncDiffst3dQ2c<24>,basisFuncDifftr3dQ2c<24>,basisFuncDiffts3dQ2c<24>,basisFuncDifftt3dQ2c<24>,
    basisFuncDiffrr3dQ2c<25>,basisFuncDiffrs3dQ2c<25>,basisFuncDiffrt3dQ2c<25>,basisFuncDiffsr3dQ2c<25>,basisFuncDiffss3dQ2c<25>,basisFuncDiffst3dQ2c<25>,basisFuncDifftr3dQ2c<25>,basisFuncDiffts3dQ2c<25>,basisFuncDifftt3dQ2c<25>,
    basisFuncDiffrr3dQ2c<26>,basisFuncDiffrs3dQ2c<26>,basisFuncDiffrt3dQ2c<26>,basisFuncDiffsr3dQ2c<26>,basisFuncDiffss3dQ2c<26>,basisFuncDiffst3dQ2c<26>,basisFuncDifftr3dQ2c<26>,basisFuncDiffts3dQ2c<26>,basisFuncDifftt3dQ2c<26>
  };

  const BasisFunction basisFunction3dQ2c("basisFunction3dQ2c",27,3,_Func3dQ2c,_FuncDiff3dQ2c,_FuncDiffHess3dQ2c);

  /************************************************************************
   *   basisFunction3dR1
   *************************************************************************/

  static const double refcoor_R1_3D[] = {0.,0.,-1.,  1.,0.,-1.,  0.,1.,-1.,  0.,0.,1.,  1.,0.,1.,  0.,1.,1.};

  template <int i>
  double basisFunc3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*(basisFunc2dP1<i>(pt))*( 1. - pt.z() );
    else {
      return 0.5*(basisFunc2dP1<i-3>(pt))*( 1. + pt.z() );
    }
  }

  static const FunctionXYZ _Func3dR1[] = {basisFunc3dR1<0>, basisFunc3dR1<1>, basisFunc3dR1<2>, basisFunc3dR1<3>, basisFunc3dR1<4>, basisFunc3dR1<5>};

  // first derivatives

  template <int i>
  double basisFuncDiffr3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*( 1. - pt.z() )*basisFuncDiffr2dP1<i>(pt);
    else
      return 0.5*( 1. + pt.z() )*basisFuncDiffr2dP1<i - 3>(pt);
  }

  template <int i>
  double basisFuncDiffs3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*( 1. - pt.z() )*basisFuncDiffs2dP1<i>(pt);
    else
      return 0.5*( 1. + pt.z() )*basisFuncDiffs2dP1<i - 3>(pt);
  }

  template <int i>
  double basisFuncDifft3dR1(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*(basisFunc2dP1<i>(pt));
    else
      return 0.5*(basisFunc2dP1<i-3>(pt));
  }


  static const FunctionXYZ _FuncDiff3dR1[] = {
    basisFuncDiffr3dR1<0>, basisFuncDiffs3dR1<0>, basisFuncDifft3dR1<0>,
    basisFuncDiffr3dR1<1>, basisFuncDiffs3dR1<1>, basisFuncDifft3dR1<1>,
    basisFuncDiffr3dR1<2>, basisFuncDiffs3dR1<2>, basisFuncDifft3dR1<2>,
    basisFuncDiffr3dR1<3>, basisFuncDiffs3dR1<3>, basisFuncDifft3dR1<3>,
    basisFuncDiffr3dR1<4>, basisFuncDiffs3dR1<4>, basisFuncDifft3dR1<4>,
    basisFuncDiffr3dR1<5>, basisFuncDiffs3dR1<5>, basisFuncDifft3dR1<5>
  };

  // Second derivatives
  template <int i>
  double basisFuncDiffrr3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*( 1. - pt.z() )*basisFuncDiffrr2dP1<i>(pt);
    else
      return 0.5*( 1. + pt.z() )*basisFuncDiffrr2dP1<i-3>(pt);
  }

  template <int i>
  double basisFuncDiffrs3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*( 1. - pt.z() )*basisFuncDiffrs2dP1<i>(pt);
    else
      return 0.5*( 1. + pt.z() )*basisFuncDiffrs2dP1<i-3>(pt);
  }

  template <int i>
  double basisFuncDiffrt3dR1(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFuncDiffr2dP1<i>(pt);
    else
      return 0.5*basisFuncDiffr2dP1<i-3>(pt);
  }

  template <int i>
  double basisFuncDiffsr3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*( 1. - pt.z() )*basisFuncDiffsr2dP1<i>(pt);
    else
      return 0.5*( 1. + pt.z() )*basisFuncDiffsr2dP1<i-3>(pt);
  }

  template <int i>
  double basisFuncDiffss3dR1(const Point& pt) {
    if constexpr(i < 3)
      return 0.5*( 1. - pt.z() )*basisFuncDiffss2dP1<i>(pt);
    else
      return 0.5*( 1. + pt.z() )*basisFuncDiffss2dP1<i-3>(pt);
  }

  template <int i>
  double basisFuncDiffst3dR1(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFuncDiffs2dP1<i>(pt);
    else
      return 0.5*basisFuncDiffs2dP1<i-3>(pt);
  }

  template <int i>
  double basisFuncDifftr3dR1(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFuncDiffr2dP1<i>(pt);
    else
      return 0.5*basisFuncDiffr2dP1<i>(pt);
  }

  template <int i>
  double basisFuncDiffts3dR1(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFuncDiffs2dP1<i>(pt);
    else
      return 0.5*basisFuncDiffr2dP1<i>(pt);
  }

  template <int i>
  double basisFuncDifftt3dR1(const Point& pt) {
    (void) pt;
    return 0.;
  }

  static const FunctionXYZ _FuncDiffHess3dR1[] = {
    basisFuncDiffrr3dR1<0>, basisFuncDiffrs3dR1<0>, basisFuncDiffrt3dR1<0>, basisFuncDiffsr3dR1<0>, basisFuncDiffss3dR1<0>, basisFuncDiffst3dR1<0>, basisFuncDifftr3dR1<0>, basisFuncDiffts3dR1<0>, basisFuncDifftt3dR1<0>,
    basisFuncDiffrr3dR1<1>, basisFuncDiffrs3dR1<1>, basisFuncDiffrt3dR1<1>, basisFuncDiffsr3dR1<1>, basisFuncDiffss3dR1<1>, basisFuncDiffst3dR1<1>, basisFuncDifftr3dR1<1>, basisFuncDiffts3dR1<1>, basisFuncDifftt3dR1<1>,
    basisFuncDiffrr3dR1<2>, basisFuncDiffrs3dR1<2>, basisFuncDiffrt3dR1<2>, basisFuncDiffsr3dR1<2>, basisFuncDiffss3dR1<2>, basisFuncDiffst3dR1<2>, basisFuncDifftr3dR1<2>, basisFuncDiffts3dR1<2>, basisFuncDifftt3dR1<2>,
    basisFuncDiffrr3dR1<3>, basisFuncDiffrs3dR1<3>, basisFuncDiffrt3dR1<3>, basisFuncDiffsr3dR1<3>, basisFuncDiffss3dR1<3>, basisFuncDiffst3dR1<3>, basisFuncDifftr3dR1<3>, basisFuncDiffts3dR1<3>, basisFuncDifftt3dR1<3>,
    basisFuncDiffrr3dR1<4>, basisFuncDiffrs3dR1<4>, basisFuncDiffrt3dR1<4>, basisFuncDiffsr3dR1<4>, basisFuncDiffss3dR1<4>, basisFuncDiffst3dR1<4>, basisFuncDifftr3dR1<4>, basisFuncDiffts3dR1<4>, basisFuncDifftt3dR1<4>,
    basisFuncDiffrr3dR1<5>, basisFuncDiffrs3dR1<5>, basisFuncDiffrt3dR1<5>, basisFuncDiffsr3dR1<5>, basisFuncDiffss3dR1<5>, basisFuncDiffst3dR1<5>, basisFuncDifftr3dR1<5>, basisFuncDiffts3dR1<5>, basisFuncDifftt3dR1<5>
  };

  const BasisFunction basisFunction3dR1("basisFunction3dR1",6,3,_Func3dR1,_FuncDiff3dR1,_FuncDiffHess3dR1);

  /************************************************************************
   *   basisFunction3dP1xP2
   *************************************************************************/

  // NOTE: Auxiliary shape functions and derivatives for a second order line
  double basis1Func1dP2t(const Point& pt);
  double basis2Func1dP2t(const Point& pt);
  double basis3Func1dP2t(const Point& pt);

  double basis1Func1dP2t(const Point& pt) {
    return -0.5 * ( 1. - pt.z()  ) * pt.z() ;
  }
  double basis2Func1dP2t(const Point& pt) {
    return 0.5 * ( 1. + pt.z() ) * pt.z()  ;
  }
  double basis3Func1dP2t(const Point& pt) {
    return ( 1. - pt.z() ) * ( 1. + pt.z() ) ;
  }

  double basis1FuncDifft1dP2(const Point& pt);
  double basis2FuncDifft1dP2(const Point& pt);
  double basis3FuncDifft1dP2(const Point& pt);

  double basis1FuncDifft1dP2(const Point& pt) {
    return pt.z() - 0.5 ;
  }
  double basis2FuncDifft1dP2(const Point& pt) {
    return pt.z() + 0.5;
  }
  double basis3FuncDifft1dP2(const Point& pt) {
    return -2. * pt.z();
  }

  static const double refcoor_P1xP2_3D[] = {0.,0.,-1.,  1.,0.,-1.,  0.,1.,-1.,  0.,0.,1.,  1.,0.,1.,  0.,1.,1., 0.,0.,0.,  1.,0.,0.,  0.,1.,0.};

  template <int i>
  double basisFunc3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFunc2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    else if constexpr( i < 6 ) {
      return (basisFunc2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFunc2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }
  }

  static const FunctionXYZ _Func3dP1xP2[] = {basisFunc3dP1xP2<0>, basisFunc3dP1xP2<1>, basisFunc3dP1xP2<2>,
                                             basisFunc3dP1xP2<3>, basisFunc3dP1xP2<4>, basisFunc3dP1xP2<5>,
                                             basisFunc3dP1xP2<6>, basisFunc3dP1xP2<7>, basisFunc3dP1xP2<8>};

  // first derivatives

  template <int i>
  double basisFuncDiffr3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffr2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffr2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFuncDiffr2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }
  }

  template <int i>
  double basisFuncDiffs3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffs2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffs2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFuncDiffs2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }
  }

  template <int i>
  double basisFuncDifft3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFunc2dP1<i  >(pt)) * (basis1FuncDifft1dP2(pt));
    else if constexpr( i < 6 ) {
      return (basisFunc2dP1<i-3>(pt)) * (basis2FuncDifft1dP2(pt));
    } else {
      return (basisFunc2dP1<i-6>(pt)) * (basis3FuncDifft1dP2(pt));
    }
  }

  static const FunctionXYZ _FuncDiff3dP1xP2[] = {
    basisFuncDiffr3dP1xP2<0>, basisFuncDiffs3dP1xP2<0>, basisFuncDifft3dP1xP2<0>,
    basisFuncDiffr3dP1xP2<1>, basisFuncDiffs3dP1xP2<1>, basisFuncDifft3dP1xP2<1>,
    basisFuncDiffr3dP1xP2<2>, basisFuncDiffs3dP1xP2<2>, basisFuncDifft3dP1xP2<2>,
    basisFuncDiffr3dP1xP2<3>, basisFuncDiffs3dP1xP2<3>, basisFuncDifft3dP1xP2<3>,
    basisFuncDiffr3dP1xP2<4>, basisFuncDiffs3dP1xP2<4>, basisFuncDifft3dP1xP2<4>,
    basisFuncDiffr3dP1xP2<5>, basisFuncDiffs3dP1xP2<5>, basisFuncDifft3dP1xP2<5>,
    basisFuncDiffr3dP1xP2<6>, basisFuncDiffs3dP1xP2<6>, basisFuncDifft3dP1xP2<6>,
    basisFuncDiffr3dP1xP2<7>, basisFuncDiffs3dP1xP2<7>, basisFuncDifft3dP1xP2<7>,
    basisFuncDiffr3dP1xP2<8>, basisFuncDiffs3dP1xP2<8>, basisFuncDifft3dP1xP2<8>
  };

  // Second derivatives
  template <int i>
  double basisFuncDiffrr3dP1xP2(const Point& pt) {
    if constexpr(i < 3) {
      return (basisFuncDiffrr2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    } else if constexpr( i < 6 ) {
      return (basisFuncDiffrr2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFuncDiffrr2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }
  }

  template <int i>
  double basisFuncDiffrs3dP1xP2(const Point& pt) {
    if constexpr(i < 3) {
      return (basisFuncDiffrs2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    } else if constexpr( i < 6 ) {
      return (basisFuncDiffrs2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFuncDiffrs2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }
  }

  template <int i>
  double basisFuncDiffrt3dP1xP2(const Point& pt) {
    if constexpr(i < 3) {
      return (basisFuncDiffr2dP1<i  >(pt)) * (basis1FuncDifft1dP2(pt));
    } else if constexpr( i < 6 ) {
      return (basisFuncDiffr2dP1<i-3>(pt)) * (basis2FuncDifft1dP2(pt));
    } else {
      return (basisFuncDiffr2dP1<i-6>(pt)) * (basis3FuncDifft1dP2(pt));
    }
  }

  template <int i>
  double basisFuncDiffsr3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffsr2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffsr2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFuncDiffsr2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }

  }

  template <int i>
  double basisFuncDiffss3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffss2dP1<i  >(pt)) * (basis1Func1dP2t(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffss2dP1<i-3>(pt)) * (basis2Func1dP2t(pt));
    } else {
      return (basisFuncDiffss2dP1<i-6>(pt)) * (basis3Func1dP2t(pt));
    }
  }

  template <int i>
  double basisFuncDiffst3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffs2dP1<i  >(pt)) * (basis1FuncDifft1dP2(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffs2dP1<i-3>(pt)) * (basis2FuncDifft1dP2(pt));
    } else {
      return (basisFuncDiffs2dP1<i-6>(pt)) * (basis3FuncDifft1dP2(pt));
    }
  }


  template <int i>
  double basisFuncDifftr3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffr2dP1<i  >(pt)) * (basis1FuncDifft1dP2(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffr2dP1<i-3>(pt)) * (basis2FuncDifft1dP2(pt));
    } else {
      return (basisFuncDiffr2dP1<i-6>(pt)) * (basis3FuncDifft1dP2(pt));
    }
  }

  template <int i>
  double basisFuncDiffts3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFuncDiffs2dP1<i  >(pt)) * (basis1FuncDifft1dP2(pt));
    else if constexpr( i < 6 ) {
      return (basisFuncDiffs2dP1<i-3>(pt)) * (basis2FuncDifft1dP2(pt));
    } else {
      return (basisFuncDiffs2dP1<i-6>(pt)) * (basis3FuncDifft1dP2(pt));
    }
  }

  template <int i>
  double basisFuncDifftt3dP1xP2(const Point& pt) {
    if constexpr(i < 3)
      return (basisFunc2dP1<i  >(pt)) * (basis1FuncDiffrr1dP2(pt));
    else if constexpr( i < 6 ) {
      return (basisFunc2dP1<i-3>(pt)) * (basis2FuncDiffrr1dP2(pt));
    } else {
      return (basisFunc2dP1<i-6>(pt)) * (basis3FuncDiffrr1dP2(pt));
    }
  }

  static const FunctionXYZ _FuncDiffHess3dP1xP2[] = {
    basisFuncDiffrr3dP1xP2<0>, basisFuncDiffrs3dP1xP2<0>, basisFuncDiffrt3dP1xP2<0>, basisFuncDiffsr3dP1xP2<0>, basisFuncDiffss3dP1xP2<0>, basisFuncDiffst3dP1xP2<0>, basisFuncDifftr3dP1xP2<0>, basisFuncDiffts3dP1xP2<0>, basisFuncDifftt3dP1xP2<0>,
    basisFuncDiffrr3dP1xP2<1>, basisFuncDiffrs3dP1xP2<1>, basisFuncDiffrt3dP1xP2<1>, basisFuncDiffsr3dP1xP2<1>, basisFuncDiffss3dP1xP2<1>, basisFuncDiffst3dP1xP2<1>, basisFuncDifftr3dP1xP2<1>, basisFuncDiffts3dP1xP2<1>, basisFuncDifftt3dP1xP2<1>,
    basisFuncDiffrr3dP1xP2<2>, basisFuncDiffrs3dP1xP2<2>, basisFuncDiffrt3dP1xP2<2>, basisFuncDiffsr3dP1xP2<2>, basisFuncDiffss3dP1xP2<2>, basisFuncDiffst3dP1xP2<2>, basisFuncDifftr3dP1xP2<2>, basisFuncDiffts3dP1xP2<2>, basisFuncDifftt3dP1xP2<2>,
    basisFuncDiffrr3dP1xP2<3>, basisFuncDiffrs3dP1xP2<3>, basisFuncDiffrt3dP1xP2<3>, basisFuncDiffsr3dP1xP2<3>, basisFuncDiffss3dP1xP2<3>, basisFuncDiffst3dP1xP2<3>, basisFuncDifftr3dP1xP2<3>, basisFuncDiffts3dP1xP2<3>, basisFuncDifftt3dP1xP2<3>,
    basisFuncDiffrr3dP1xP2<4>, basisFuncDiffrs3dP1xP2<4>, basisFuncDiffrt3dP1xP2<4>, basisFuncDiffsr3dP1xP2<4>, basisFuncDiffss3dP1xP2<4>, basisFuncDiffst3dP1xP2<4>, basisFuncDifftr3dP1xP2<4>, basisFuncDiffts3dP1xP2<4>, basisFuncDifftt3dP1xP2<4>,
    basisFuncDiffrr3dP1xP2<5>, basisFuncDiffrs3dP1xP2<5>, basisFuncDiffrt3dP1xP2<5>, basisFuncDiffsr3dP1xP2<5>, basisFuncDiffss3dP1xP2<5>, basisFuncDiffst3dP1xP2<5>, basisFuncDifftr3dP1xP2<5>, basisFuncDiffts3dP1xP2<5>, basisFuncDifftt3dP1xP2<5>,
    basisFuncDiffrr3dP1xP2<6>, basisFuncDiffrs3dP1xP2<6>, basisFuncDiffrt3dP1xP2<6>, basisFuncDiffsr3dP1xP2<6>, basisFuncDiffss3dP1xP2<6>, basisFuncDiffst3dP1xP2<6>, basisFuncDifftr3dP1xP2<6>, basisFuncDiffts3dP1xP2<6>, basisFuncDifftt3dP1xP2<6>,
    basisFuncDiffrr3dP1xP2<7>, basisFuncDiffrs3dP1xP2<7>, basisFuncDiffrt3dP1xP2<7>, basisFuncDiffsr3dP1xP2<7>, basisFuncDiffss3dP1xP2<7>, basisFuncDiffst3dP1xP2<7>, basisFuncDifftr3dP1xP2<7>, basisFuncDiffts3dP1xP2<7>, basisFuncDifftt3dP1xP2<7>,
    basisFuncDiffrr3dP1xP2<8>, basisFuncDiffrs3dP1xP2<8>, basisFuncDiffrt3dP1xP2<8>, basisFuncDiffsr3dP1xP2<8>, basisFuncDiffss3dP1xP2<8>, basisFuncDiffst3dP1xP2<8>, basisFuncDifftr3dP1xP2<8>, basisFuncDiffts3dP1xP2<8>, basisFuncDifftt3dP1xP2<8>
  };

  const BasisFunction basisFunction3dP1xP2("basisFunction3dP1xP2",9,3,_Func3dP1xP2,_FuncDiff3dP1xP2,_FuncDiffHess3dP1xP2);


  /************************************************************************
   *   basisFunction3dR2
   *************************************************************************/

  static const double refcoor_R2_3D[] = {0.,0.,-1.,  1.,0.,-1.,  0.,1.,-1.,  0.,0.,1.,  1.,0.,1.,  0.,1.,1.,
                                         0.5,0.,-1.,  0.5,0.5,-1.,  0.,0.5,-1.,  0.5,0.,1.,  0.5,0.5,1.,  0.,0.5,1.,  0.,0.,0.,  1.,0.,0.,  0.,1.,0.
                                        };

  //int ip[] = {2,3,1}
  // #define ip(i) ( (i) == (0) ? (2) : ( (i) == (1) ? (3) : (1) ) )
  template <int i>
  double basisFunc3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFunc2dP2<i>(pt)*pt.z()*(  1. - pt.z() );
    else if constexpr( i < 6 )
      return 0.5*basisFunc2dP2<i-3>(pt)*pt.z()*(  1. + pt.z() );
    else if constexpr( i < 9 )
      return -0.5*basisFunc2dP2<i-3>(pt)*pt.z()*(  1. - pt.z() );
    else if constexpr( i < 12 )
      return 0.5*basisFunc2dP2<i-6>(pt)*pt.z()*(  1. + pt.z() );
    else
      return basisFunc2dP2<i-12>(pt)*( 1. - pt.z() )*(  1. + pt.z() );
  }

  static const FunctionXYZ _Func3dR2[] = {basisFunc3dR2<0>, basisFunc3dR2<1>, basisFunc3dR2<2>, basisFunc3dR2<3>, basisFunc3dR2<4>, basisFunc3dR2<5>,
                                          basisFunc3dR2<6>, basisFunc3dR2<7>, basisFunc3dR2<8>, basisFunc3dR2<9>, basisFunc3dR2<10>, basisFunc3dR2<11>,
                                          basisFunc3dR2<12>, basisFunc3dR2<13>, basisFunc3dR2<14>
                                         };

  // first derivatives

  template <int i>
  double basisFuncDiffr3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffr2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffr2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffr2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffr2dP2<i-6>(pt);
    else
      return ( 1. - pt.z() )*(  1. + pt.z() )*basisFuncDiffr2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffs3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffs2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffs2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffs2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffs2dP2<i-6>(pt);
    else
      return ( 1. - pt.z() )*(  1. + pt.z() )*basisFuncDiffs2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDifft3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFunc2dP2<i>(pt)*( 1. - 2.*pt.z() );
    else if constexpr( i < 6 )
      return 0.5*basisFunc2dP2<i-3>(pt)*( 1. + 2.*pt.z() );
    else if constexpr( i < 9 )
      return -0.5*basisFunc2dP2<i-3>(pt)*( 1. - 2.*pt.z() );
    else if constexpr( i < 12 )
      return 0.5*basisFunc2dP2<i-6>(pt)*( 1. + 2.*pt.z() );
    else
      return -2.*pt.z()*basisFunc2dP2<i-12>(pt);
  }

  static const FunctionXYZ _FuncDiff3dR2[] = {
    basisFuncDiffr3dR2<0>, basisFuncDiffs3dR2<0>, basisFuncDifft3dR2<0>,
    basisFuncDiffr3dR2<1>, basisFuncDiffs3dR2<1>, basisFuncDifft3dR2<1>,
    basisFuncDiffr3dR2<2>, basisFuncDiffs3dR2<2>, basisFuncDifft3dR2<2>,
    basisFuncDiffr3dR2<3>, basisFuncDiffs3dR2<3>, basisFuncDifft3dR2<3>,
    basisFuncDiffr3dR2<4>, basisFuncDiffs3dR2<4>, basisFuncDifft3dR2<4>,
    basisFuncDiffr3dR2<5>, basisFuncDiffs3dR2<5>, basisFuncDifft3dR2<5>,
    basisFuncDiffr3dR2<6>, basisFuncDiffs3dR2<6>, basisFuncDifft3dR2<6>,
    basisFuncDiffr3dR2<7>, basisFuncDiffs3dR2<7>, basisFuncDifft3dR2<7>,
    basisFuncDiffr3dR2<8>, basisFuncDiffs3dR2<8>, basisFuncDifft3dR2<8>,
    basisFuncDiffr3dR2<9>, basisFuncDiffs3dR2<9>, basisFuncDifft3dR2<9>,
    basisFuncDiffr3dR2<10>, basisFuncDiffs3dR2<10>, basisFuncDifft3dR2<10>,
    basisFuncDiffr3dR2<11>, basisFuncDiffs3dR2<11>, basisFuncDifft3dR2<11>,
    basisFuncDiffr3dR2<12>, basisFuncDiffs3dR2<12>, basisFuncDifft3dR2<12>,
    basisFuncDiffr3dR2<13>, basisFuncDiffs3dR2<13>, basisFuncDifft3dR2<13>,
    basisFuncDiffr3dR2<14>, basisFuncDiffs3dR2<14>, basisFuncDifft3dR2<14>
  };


  // Second derivatives
  template <int i>
  double basisFuncDiffrr3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffrr2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffrr2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffrr2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffrr2dP2<i-6>(pt);
    else
      return ( 1. - pt.z() )*(  1. + pt.z() )*basisFuncDiffrr2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffrs3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffrs2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffrs2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffrs2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffrs2dP2<i-6>(pt);
    else
      return ( 1. - pt.z() )*(  1. + pt.z() )*basisFuncDiffrs2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffrt3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFuncDiffr2dP2<i>(pt)*( 1. - 2.*pt.z() );
    else if constexpr( i < 6 )
      return 0.5*basisFuncDiffr2dP2<i-3>(pt)*( 1. + 2.*pt.z() );
    else if constexpr( i < 9 )
      return -0.5*basisFuncDiffr2dP2<i-3>(pt)*( 1. - 2.*pt.z() );
    else if constexpr( i < 12 )
      return 0.5*basisFuncDiffr2dP2<i-6>(pt)*( 1. + 2.*pt.z() );
    else
      return -2.*pt.z()*basisFuncDiffr2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffsr3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffsr2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffsr2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffsr2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffsr2dP2<i-6>(pt);
    else
      return ( 1. - pt.z() )*(  1. + pt.z() )*basisFuncDiffsr2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffss3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffss2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffss2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*pt.z()*( 1. - pt.z() )*basisFuncDiffss2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*pt.z()*( 1. + pt.z() )*basisFuncDiffss2dP2<i-6>(pt);
    else
      return ( 1. - pt.z() )*(  1. + pt.z() )*basisFuncDiffss2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffst3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*basisFuncDiffs2dP2<i>(pt)*( 1. - 2.*pt.z() );
    else if constexpr( i < 6 )
      return 0.5*basisFuncDiffs2dP2<i-3>(pt)*( 1. + 2.*pt.z() );
    else if constexpr( i < 9 )
      return -0.5*basisFuncDiffs2dP2<i-3>(pt)*( 1. - 2.*pt.z() );
    else if constexpr( i < 12 )
      return 0.5*basisFuncDiffs2dP2<i-6>(pt)*( 1. + 2.*pt.z() );
    else
      return -2.*pt.z()*basisFuncDiffs2dP2<i-12>(pt);
  }


  template <int i>
  double basisFuncDifftr3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*( 1. - 2.*pt.z() )*basisFuncDiffr2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*( 1. + 2.*pt.z() )*basisFuncDiffr2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*( 1. - 2.*pt.z() )*basisFuncDiffr2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*( 1. + 2.*pt.z() )*basisFuncDiffr2dP2<i-6>(pt);
    else
      return -2.*pt.z()*basisFuncDiffr2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDiffts3dR2(const Point& pt) {
    if constexpr(i < 3)
      return -0.5*( 1. - 2.*pt.z() )*basisFuncDiffs2dP2<i>(pt);
    else if constexpr( i < 6 )
      return 0.5*( 1. + 2.*pt.z() )*basisFuncDiffs2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return -0.5*( 1. - 2.*pt.z() )*basisFuncDiffs2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return 0.5*( 1. + 2.*pt.z() )*basisFuncDiffs2dP2<i-6>(pt);
    else
      return -2.*pt.z()*basisFuncDiffs2dP2<i-12>(pt);
  }

  template <int i>
  double basisFuncDifftt3dR2(const Point& pt) {
    if constexpr(i < 3)
      return basisFunc2dP2<i>(pt);
    else if constexpr( i < 6 )
      return basisFunc2dP2<i-3>(pt);
    else if constexpr( i < 9 )
      return basisFunc2dP2<i-3>(pt);
    else if constexpr( i < 12 )
      return basisFunc2dP2<i-6>(pt);
    else
      return -2.*basisFunc2dP2<i-12>(pt);
  }

  static const FunctionXYZ _FuncDiffHess3dR2[] = {
    basisFuncDiffrr3dR2<0>, basisFuncDiffrs3dR2<0>, basisFuncDiffrt3dR2<0>, basisFuncDiffsr3dR2<0>, basisFuncDiffss3dR2<0>, basisFuncDiffst3dR2<0>, basisFuncDifftr3dR2<0>, basisFuncDiffts3dR2<0>, basisFuncDifftt3dR2<0>,
    basisFuncDiffrr3dR2<1>, basisFuncDiffrs3dR2<1>, basisFuncDiffrt3dR2<1>, basisFuncDiffsr3dR2<1>, basisFuncDiffss3dR2<1>, basisFuncDiffst3dR2<1>, basisFuncDifftr3dR2<1>, basisFuncDiffts3dR2<1>, basisFuncDifftt3dR2<1>,
    basisFuncDiffrr3dR2<2>, basisFuncDiffrs3dR2<2>, basisFuncDiffrt3dR2<2>, basisFuncDiffsr3dR2<2>, basisFuncDiffss3dR2<2>, basisFuncDiffst3dR2<2>, basisFuncDifftr3dR2<2>, basisFuncDiffts3dR2<2>, basisFuncDifftt3dR2<2>,
    basisFuncDiffrr3dR2<3>, basisFuncDiffrs3dR2<3>, basisFuncDiffrt3dR2<3>, basisFuncDiffsr3dR2<3>, basisFuncDiffss3dR2<3>, basisFuncDiffst3dR2<3>, basisFuncDifftr3dR2<3>, basisFuncDiffts3dR2<3>, basisFuncDifftt3dR2<3>,
    basisFuncDiffrr3dR2<4>, basisFuncDiffrs3dR2<4>, basisFuncDiffrt3dR2<4>, basisFuncDiffsr3dR2<4>, basisFuncDiffss3dR2<4>, basisFuncDiffst3dR2<4>, basisFuncDifftr3dR2<4>, basisFuncDiffts3dR2<4>, basisFuncDifftt3dR2<4>,
    basisFuncDiffrr3dR2<5>, basisFuncDiffrs3dR2<5>, basisFuncDiffrt3dR2<5>, basisFuncDiffsr3dR2<5>, basisFuncDiffss3dR2<5>, basisFuncDiffst3dR2<5>, basisFuncDifftr3dR2<5>, basisFuncDiffts3dR2<5>, basisFuncDifftt3dR2<5>,
    basisFuncDiffrr3dR2<6>, basisFuncDiffrs3dR2<6>, basisFuncDiffrt3dR2<6>, basisFuncDiffsr3dR2<6>, basisFuncDiffss3dR2<6>, basisFuncDiffst3dR2<6>, basisFuncDifftr3dR2<6>, basisFuncDiffts3dR2<6>, basisFuncDifftt3dR2<6>,
    basisFuncDiffrr3dR2<7>, basisFuncDiffrs3dR2<7>, basisFuncDiffrt3dR2<7>, basisFuncDiffsr3dR2<7>, basisFuncDiffss3dR2<7>, basisFuncDiffst3dR2<7>, basisFuncDifftr3dR2<7>, basisFuncDiffts3dR2<7>, basisFuncDifftt3dR2<7>,
    basisFuncDiffrr3dR2<8>, basisFuncDiffrs3dR2<8>, basisFuncDiffrt3dR2<8>, basisFuncDiffsr3dR2<8>, basisFuncDiffss3dR2<8>, basisFuncDiffst3dR2<8>, basisFuncDifftr3dR2<8>, basisFuncDiffts3dR2<8>, basisFuncDifftt3dR2<8>,
    basisFuncDiffrr3dR2<9>, basisFuncDiffrs3dR2<9>, basisFuncDiffrt3dR2<9>, basisFuncDiffsr3dR2<9>, basisFuncDiffss3dR2<9>, basisFuncDiffst3dR2<9>, basisFuncDifftr3dR2<9>, basisFuncDiffts3dR2<9>, basisFuncDifftt3dR2<9>,
    basisFuncDiffrr3dR2<10>, basisFuncDiffrs3dR2<10>, basisFuncDiffrt3dR2<10>, basisFuncDiffsr3dR2<10>, basisFuncDiffss3dR2<10>, basisFuncDiffst3dR2<10>, basisFuncDifftr3dR2<10>, basisFuncDiffts3dR2<10>, basisFuncDifftt3dR2<10>,
    basisFuncDiffrr3dR2<11>, basisFuncDiffrs3dR2<11>, basisFuncDiffrt3dR2<11>, basisFuncDiffsr3dR2<11>, basisFuncDiffss3dR2<11>, basisFuncDiffst3dR2<11>, basisFuncDifftr3dR2<11>, basisFuncDiffts3dR2<11>, basisFuncDifftt3dR2<11>,
    basisFuncDiffrr3dR2<12>, basisFuncDiffrs3dR2<12>, basisFuncDiffrt3dR2<12>, basisFuncDiffsr3dR2<12>, basisFuncDiffss3dR2<12>, basisFuncDiffst3dR2<12>, basisFuncDifftr3dR2<12>, basisFuncDiffts3dR2<12>, basisFuncDifftt3dR2<12>,
    basisFuncDiffrr3dR2<13>, basisFuncDiffrs3dR2<13>, basisFuncDiffrt3dR2<13>, basisFuncDiffsr3dR2<13>, basisFuncDiffss3dR2<13>, basisFuncDiffst3dR2<13>, basisFuncDifftr3dR2<13>, basisFuncDiffts3dR2<13>, basisFuncDifftt3dR2<13>,
    basisFuncDiffrr3dR2<14>, basisFuncDiffrs3dR2<14>, basisFuncDiffrt3dR2<14>, basisFuncDiffsr3dR2<14>, basisFuncDiffss3dR2<14>, basisFuncDiffst3dR2<14>, basisFuncDifftr3dR2<14>, basisFuncDiffts3dR2<14>, basisFuncDifftt3dR2<14>
  };

  const BasisFunction basisFunction3dR2("basisFunction3dR2",15,3,_Func3dR2,_FuncDiff3dR2,_FuncDiffHess3dR2);

  /************************************************************************
   * basisFunction3dRT0Tetra (Raviart-Thomas lowest degree on a tetrahedron)
   *************************************************************************/

  double basis1Func_RT0_x_3D_TETRA(const Point&);
  double basis1Func_RT0_y_3D_TETRA(const Point&);
  double basis1Func_RT0_z_3D_TETRA(const Point&);
  double basis1Func_RT0_x_3D_TETRA(const Point& pt) {
    return 2 * pt.x();
  }
  double basis1Func_RT0_y_3D_TETRA(const Point& pt) {
    return 2 * pt.y();
  }
  double basis1Func_RT0_z_3D_TETRA(const Point& pt) {
    return 2 * pt.z() - 2;
  }

  double basis2Func_RT0_x_3D_TETRA(const Point&);
  double basis2Func_RT0_y_3D_TETRA(const Point&);
  double basis2Func_RT0_z_3D_TETRA(const Point&);
  double basis2Func_RT0_x_3D_TETRA(const Point& pt) {
    return 2 * pt.x();
  }
  double basis2Func_RT0_y_3D_TETRA(const Point& pt) {
    return 2 * pt.y() - 2;
  }
  double basis2Func_RT0_z_3D_TETRA(const Point& pt) {
    return 2 * pt.z();
  }

  double basis3Func_RT0_x_3D_TETRA(const Point&);
  double basis3Func_RT0_y_3D_TETRA(const Point&);
  double basis3Func_RT0_z_3D_TETRA(const Point&);
  double basis3Func_RT0_x_3D_TETRA(const Point& pt) {
    return 2 * pt.x();
  }
  double basis3Func_RT0_y_3D_TETRA(const Point& pt) {
    return 2 * pt.y();
  }
  double basis3Func_RT0_z_3D_TETRA(const Point& pt) {
    return 2 * pt.z();
  }

  double basis4Func_RT0_x_3D_TETRA(const Point&);
  double basis4Func_RT0_y_3D_TETRA(const Point&);
  double basis4Func_RT0_z_3D_TETRA(const Point&);
  double basis4Func_RT0_x_3D_TETRA(const Point& pt) {
    return 2 * pt.x() - 2;
  }
  double basis4Func_RT0_y_3D_TETRA(const Point& pt) {
    return 2 * pt.y();
  }
  double basis4Func_RT0_z_3D_TETRA(const Point& pt) {
    return 2 * pt.z();
  }

  // first derivatives (the 3 basis functions have the same ones)
  double basis1FuncDiff_RT0_x_1_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_y_1_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_z_1_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_x_2_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_y_2_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_z_2_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_x_3_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_y_3_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_z_3_3D_TETRA(const Point&);
  double basis1FuncDiff_RT0_x_1_3D_TETRA(const Point& pt) {
    (void) pt;
    return 2;
  }
  double basis1FuncDiff_RT0_y_1_3D_TETRA(const Point& pt) {
    (void) pt;
    return 0.;
  }
  double basis1FuncDiff_RT0_z_1_3D_TETRA(const Point& pt) {
    (void) pt;
    return 0.;
  }
  double basis1FuncDiff_RT0_x_2_3D_TETRA(const Point& pt) {
    (void) pt;
    return 0;
  }
  double basis1FuncDiff_RT0_y_2_3D_TETRA(const Point& pt) {
    (void) pt;
    return 2.;
  }
  double basis1FuncDiff_RT0_z_2_3D_TETRA(const Point& pt) {
    (void) pt;
    return 0.;
  }
  double basis1FuncDiff_RT0_x_3_3D_TETRA(const Point& pt) {
    (void) pt;
    return 0;
  }
  double basis1FuncDiff_RT0_y_3_3D_TETRA(const Point& pt) {
    (void) pt;
    return 0.;
  }
  double basis1FuncDiff_RT0_z_3_3D_TETRA(const Point& pt) {
    (void) pt;
    return 2.;
  }


  static const FunctionXYZ _Func_RT0_3D_TETRA[] = {
    basis1Func_RT0_x_3D_TETRA,basis1Func_RT0_y_3D_TETRA,basis1Func_RT0_z_3D_TETRA,
    basis2Func_RT0_x_3D_TETRA,basis2Func_RT0_y_3D_TETRA,basis2Func_RT0_z_3D_TETRA,
    basis3Func_RT0_x_3D_TETRA,basis3Func_RT0_y_3D_TETRA,basis3Func_RT0_z_3D_TETRA,
    basis4Func_RT0_x_3D_TETRA,basis4Func_RT0_y_3D_TETRA,basis4Func_RT0_z_3D_TETRA
  };

  static const FunctionXYZ _FuncDiff_RT0_3D_TETRA[] = {
    basis1FuncDiff_RT0_x_1_3D_TETRA,basis1FuncDiff_RT0_y_1_3D_TETRA,basis1FuncDiff_RT0_z_1_3D_TETRA,
    basis1FuncDiff_RT0_x_2_3D_TETRA,basis1FuncDiff_RT0_y_2_3D_TETRA,basis1FuncDiff_RT0_z_2_3D_TETRA,
    basis1FuncDiff_RT0_x_3_3D_TETRA,basis1FuncDiff_RT0_y_3_3D_TETRA,basis1FuncDiff_RT0_z_3_3D_TETRA,

    basis1FuncDiff_RT0_x_1_3D_TETRA,basis1FuncDiff_RT0_y_1_3D_TETRA,basis1FuncDiff_RT0_z_1_3D_TETRA,
    basis1FuncDiff_RT0_x_2_3D_TETRA,basis1FuncDiff_RT0_y_2_3D_TETRA,basis1FuncDiff_RT0_z_2_3D_TETRA,
    basis1FuncDiff_RT0_x_3_3D_TETRA,basis1FuncDiff_RT0_y_3_3D_TETRA,basis1FuncDiff_RT0_z_3_3D_TETRA,

    basis1FuncDiff_RT0_x_1_3D_TETRA,basis1FuncDiff_RT0_y_1_3D_TETRA,basis1FuncDiff_RT0_z_1_3D_TETRA,
    basis1FuncDiff_RT0_x_2_3D_TETRA,basis1FuncDiff_RT0_y_2_3D_TETRA,basis1FuncDiff_RT0_z_2_3D_TETRA,
    basis1FuncDiff_RT0_x_3_3D_TETRA,basis1FuncDiff_RT0_y_3_3D_TETRA,basis1FuncDiff_RT0_z_3_3D_TETRA,

    basis1FuncDiff_RT0_x_1_3D_TETRA,basis1FuncDiff_RT0_y_1_3D_TETRA,basis1FuncDiff_RT0_z_1_3D_TETRA,
    basis1FuncDiff_RT0_x_2_3D_TETRA,basis1FuncDiff_RT0_y_2_3D_TETRA,basis1FuncDiff_RT0_z_2_3D_TETRA,
    basis1FuncDiff_RT0_x_3_3D_TETRA,basis1FuncDiff_RT0_y_3_3D_TETRA,basis1FuncDiff_RT0_z_3_3D_TETRA,
  };

  // no second derivatives

  const BasisFunction basisFunction3dRT0Tetra("basisFunction3dRT0Tetra",4,3,3,_Func_RT0_3D_TETRA,_FuncDiff_RT0_3D_TETRA,nullptr);


  /*========================================================================
    !
    !                          REFERENCE SHAPES
    !
    =======================================================================*/
  const RefShape NULLSHAPE("NULLSHAPE",0,0,0,0,NullShape);
  const RefShape NODE("NODE",1,1,0,0,Node);
  const RefShape SEGMENT("SEGMENT",1,2,1,0,Segment);
  const RefShape TRIANGLE("TRIANGLE",2,3,3,1,Triangle);
  const RefShape QUADRILATERAL("QUADRILATERAL",2,4,4,1,Quadrilateral);
  const RefShape TETRAHEDRON("TETRAHEDRON",3,4,6,4,Tetrahedron);
  const RefShape HEXAHEDRON("HEXAHEDRON",3,8,12,6,Hexahedron);
  const RefShape PRISM("PRISM",3,6,9,5,Prism);
  const RefShape PYRAMID("PYRAMID",3,5,8,5,Pyramid);

  /*========================================================================
    !
    !                          QUADRATURE RULES
    !
    =======================================================================*/
  /************************************************************************
   *                          Quadrature Rules NULL
   ************************************************************************/
  //----------------------------------------------------------------------

  const QuadratureRule quadratureRuleNULL( nullptr,
      "quadratureRuleNULL", NULLSHAPE, 0, -1 );
  const ListOfQuadratureRule listQuadratureRuleNULL("listQuadratureRuleNULL",0,&quadratureRuleNULL);


  /************************************************************************
   *                          Quadrature Rules on Node
   ************************************************************************/
  //----------------------------------------------------------------------
  static const QuadraturePoint pt_node[1] = {QuadraturePoint( 0., 0. )};
  const QuadratureRule quadratureRuleNode1( pt_node, "quadratureRuleNode", NODE, 1, 1 );
  const QuadratureRule quadratureRuleNode2( pt_node, "quadratureRuleNode", NODE, 1, 2 );
  const QuadratureRule quadratureRuleNode3( pt_node, "quadratureRuleNode", NODE, 1, 3 );

  static const QuadratureRule quad_rule_on_node[3] = {quadratureRuleNode1,quadratureRuleNode2,quadratureRuleNode3};

  const ListOfQuadratureRule listQuadratureRuleNode("listQuadratureRuleNode",3, quad_rule_on_node);

  /************************************************************************
   *                          Quadrature Rules on segments
   ************************************************************************/
  //----------------------------------------------------------------------

  static const QuadraturePoint pt_seg_1pt[ 1 ] = {QuadraturePoint( 0., 2. )};
  const QuadratureRule quadratureRuleSeg1pt( pt_seg_1pt,
      "quadratureRuleSeg1pt", SEGMENT, 1, 1 );
  //----------------------------------------------------------------------
  const double q2ptx1 =  - std::sqrt( 1. / 3. ) , q2ptx2 = std::sqrt( 1. / 3. ) ;
  const double q2ptw1 = 1., q2ptw2 = 1.;

  static const QuadraturePoint pt_seg_2pt[] = {QuadraturePoint( q2ptx1 , q2ptw1 ),QuadraturePoint( q2ptx2 , q2ptw2 )};
  const QuadratureRule quadratureRuleSeg2pt( pt_seg_2pt,
      "quadratureRuleSeg2pt", SEGMENT, 2, 3 );
  //----------------------------------------------------------------------
  const double q3ptx1 = - std::sqrt( 3. / 5. ) , q3ptx2 = 0. , q3ptx3 = std::sqrt( 3. / 5. );
  const double q3ptw1 = 10. / 18. , q3ptw2 = 16. / 18., q3ptw3 = 10. / 18.;

  static const QuadraturePoint pt_seg_3pt[] =
  {QuadraturePoint( q3ptx1, q3ptw1 ),QuadraturePoint( q3ptx2, q3ptw2 ),QuadraturePoint( q3ptx3, q3ptw3 )};

  const QuadratureRule quadratureRuleSeg3pt( pt_seg_3pt,
      "quadratureRuleSeg3pt", SEGMENT, 3, 5 );
  //----------------------------------------------------------------------
  //  List of quadrature rules on segments
  //----------------------------------------------------------------------
  static const QuadratureRule quad_rule_on_segment[] = {quadratureRuleSeg1pt,quadratureRuleSeg2pt,quadratureRuleSeg3pt};

  const ListOfQuadratureRule listQuadratureRuleSegment("listQuadratureRuleSegment",3,quad_rule_on_segment);

  /************************************************************************
   *                          Quadrature Rules on triangles
   ************************************************************************/
  //----------------------------------------------------------------------

  static const QuadraturePoint pt_tria_1pt[ 1 ] = {QuadraturePoint( 1. / 3., 1. / 3., 1. / 2. )};
  const QuadratureRule quadratureRuleTria1pt( pt_tria_1pt,
      "quadratureRuleTria1pt", TRIANGLE, 1, 1 );
  //----------------------------------------------------------------------

  const double t3ptx1 = 0.5, t3ptx2 = 0., t3ptw = 1./6.;
    static const QuadraturePoint pt_tria_3pt[ 3 ] =
    {QuadraturePoint( t3ptx1, t3ptx2 , t3ptw ),QuadraturePoint( t3ptx2, t3ptx1, t3ptw ), QuadraturePoint( 0.5, 0.5, t3ptw )};
      const QuadratureRule quadratureRuleTria3pt( pt_tria_3pt,"quadratureRuleTria3pt", TRIANGLE, 3, 2 );
  //
  // WARNING: In what follows the degree of exactnes is increased by 1,
  // in order to complain with the rule which enforces differents
  // degree of exactness for two quadratures rules
  //----------------------------------------------------------------------
  //------------ quadrature points for MITC3 and MITC3+ D of Ex = 3 2 intead of 3
  // const double mitc3x1 = 1./6., mitc3x2 = 2./3. , mitc3w = 1./6;
  static const QuadraturePoint pt_mitc3[ 3 ] =
    {QuadraturePoint(1./6., 1./6., 1./6.), QuadraturePoint(2./3., 1./6., 1./6.), QuadraturePoint( 1./6., 2./3., 1./6.) };
  const QuadratureRule quadratureRuleMitc3( pt_mitc3,"quadratureRuleMitc3", TRIANGLE, 3, 3 ); //

  //----------------------------------------------------------------------
  // 4 points Integration rule for triangle (Ref. e.g. Comincioli pag. 234) D of Ex = 3 intead of 4
  const double t4pt_xb1 = 3. / 5.,
               t4pt_xb2 = 1. / 5.,
               t4pt_w1 = 25. / 96.,
               t4pt_w2 = -9. / 32.,
               t4pt_a = 1. / 3.;

  static const QuadraturePoint pt_tria_4pt[ 4 ] = {
    QuadraturePoint( t4pt_xb1, t4pt_xb2, t4pt_w1 ),QuadraturePoint( t4pt_xb2, t4pt_xb1, t4pt_w1 ),
    QuadraturePoint( t4pt_xb2, t4pt_xb2, t4pt_w1 ),QuadraturePoint( t4pt_a, t4pt_a, t4pt_w2 )
  };

  const QuadratureRule quadratureRuleTria4pt( pt_tria_4pt,
      "quadratureRuleTria4pt", TRIANGLE, 4, 4 );
  //----------------------------------------------------------------------
  // 6 points Integration rule for triangle, D of Ex = 4 insted of 5
  // Ref: G.R. Cowper,  Gaussian quadrature formulas for triangles,
  //      Internat. J. Numer. Methods Engrg.  7 (1973), 405--408.
  const double t6pt_x1 = 0.091576213509770743;
  const double t6pt_x2 = 0.44594849091596488;
  const double t6pt_w1 = 0.054975871827660933;
  const double t6pt_w2 = 0.11169079483900573;
  static const QuadraturePoint pt_tria_6pt[ 6 ] = {
    QuadraturePoint(     t6pt_x1,     t6pt_x1, t6pt_w1 ),QuadraturePoint(     t6pt_x1, 1-2*t6pt_x1, t6pt_w1 ),
    QuadraturePoint( 1-2*t6pt_x1,     t6pt_x1, t6pt_w1 ),QuadraturePoint(     t6pt_x2,     t6pt_x2, t6pt_w2 ),
    QuadraturePoint(     t6pt_x2, 1-2*t6pt_x2, t6pt_w2 ),QuadraturePoint( 1-2*t6pt_x2,     t6pt_x2, t6pt_w2 )
  };
  const QuadratureRule quadratureRuleTria6pt( pt_tria_6pt,
      "quadratureRuleTria6pt",
      TRIANGLE, 6, 5 );
  //----------------------------------------------------------------------
  // 7 points Integration rule for triangle (Ref. Stroud) D of Ex = 6
  const double t7pt_x0 = 1. / 3.;
  const double t7pt_x1 = 0.10128650732345633;
  const double t7pt_x2 = 0.47014206410511508;
  const double t7pt_w0 = 0.1125;
  const double t7pt_w1 = 0.062969590272413576;
  const double t7pt_w2 = 0.066197076394253090;

  static const QuadraturePoint pt_tria_7pt[ 7 ] = {
    QuadraturePoint(     t7pt_x0,     t7pt_x0, t7pt_w0 ),QuadraturePoint(     t7pt_x1,     t7pt_x1, t7pt_w1 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1, t7pt_w1 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1, t7pt_w1 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2, t7pt_w2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2, t7pt_w2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2, t7pt_w2 )
  };
  const QuadratureRule quadratureRuleTria7pt( pt_tria_7pt,
      "quadratureRuleTria7pt",
      TRIANGLE, 7, 6 );
  //----------------------------------------------------------------------
  //  List of quadrature rules on triangles
  //----------------------------------------------------------------------
  static const QuadratureRule quad_rule_on_triangle[] = {
    quadratureRuleTria1pt,quadratureRuleTria3pt,quadratureRuleMitc3,quadratureRuleTria4pt,
    quadratureRuleTria6pt,quadratureRuleTria7pt
  };

  const ListOfQuadratureRule listQuadratureRuleTriangle("listQuadratureRuleTriangle",6,quad_rule_on_triangle);

  /************************************************************************
   *                          Quadrature Rules on quadrilaterals
   ************************************************************************/
  //----------------------------------------------------------------------

  static const QuadraturePoint pt_quad_1pt[ 1 ] = {
    QuadraturePoint( 0., 0., 4. )
  };
  const QuadratureRule quadratureRuleQuad1pt( pt_quad_1pt,
      "quadratureRuleQuad1pt", QUADRILATERAL, 1, 1 );
  //----------------------------------------------------------------------
  // 4 points Integration rule for quadrangle (tensorization of 2 pts on segment)
  static const QuadraturePoint pt_quad_4pt[ 4 ] = {
    QuadraturePoint( q2ptx1, q2ptx1, q2ptw1 * q2ptw1 ),
    QuadraturePoint( q2ptx2, q2ptx1, q2ptw2 * q2ptw1 ),
    QuadraturePoint( q2ptx2, q2ptx2, q2ptw2 * q2ptw2 ),
    QuadraturePoint( q2ptx1, q2ptx2, q2ptw1 * q2ptw2 )
  };
  const QuadratureRule quadratureRuleQuad4pt( pt_quad_4pt,
      "quadratureRuleQuad4pt", QUADRILATERAL, 4, 3 );
  //----------------------------------------------------------------------
  //6 points Integration rule for quadrangle (2 in x and 3 in y)
  static const QuadraturePoint pt_quad_6pt[ 6 ] = {
    QuadraturePoint( q2ptx1, q3ptx1 , q2ptw1 * q3ptw1 ),
    QuadraturePoint( q2ptx2, q3ptx1 , q2ptw2 * q3ptw1 ),
    QuadraturePoint( q2ptx1, q3ptx2 , q2ptw1 * q3ptw2 ),
    QuadraturePoint( q2ptx2, q3ptx2 , q2ptw2 * q3ptw2 ),
    QuadraturePoint( q2ptx1, q3ptx3 , q2ptw1 * q3ptw3 ),
    QuadraturePoint( q2ptx2, q3ptx3 , q2ptw2 * q3ptw3 )
  };
  const QuadratureRule quadratureRuleQuad6pt( pt_quad_6pt,
      "quadratureRuleQuad6pt", QUADRILATERAL, 6, 4 );
  //----------------------------------------------------------------------
  // 9 points Integration rule for quadrangle (tensorization of 3 pts on segment)
  static const QuadraturePoint pt_quad_9pt[ 9 ] = {
    QuadraturePoint( q3ptx1, q3ptx1, q3ptw1 * q3ptw1 ),
    QuadraturePoint( q3ptx2, q3ptx1, q3ptw2 * q3ptw1 ),
    QuadraturePoint( q3ptx3, q3ptx1, q3ptw3 * q3ptw1 ),
    QuadraturePoint( q3ptx1, q3ptx2, q3ptw1 * q3ptw2 ),
    QuadraturePoint( q3ptx2, q3ptx2, q3ptw2 * q3ptw2 ),
    QuadraturePoint( q3ptx3, q3ptx2, q3ptw3 * q3ptw2 ),
    QuadraturePoint( q3ptx1, q3ptx3, q3ptw1 * q3ptw3 ),
    QuadraturePoint( q3ptx2, q3ptx3, q3ptw2 * q3ptw3 ),
    QuadraturePoint( q3ptx3, q3ptx3, q3ptw3 * q3ptw3 )
  };

  const QuadratureRule quadratureRuleQuad9pt( pt_quad_9pt,
      "quadratureRuleQuad9pt", QUADRILATERAL, 9, 5 );
  //----------------------------------------------------------------------
  //  List of quadrature rules on quadrilaterals
  //----------------------------------------------------------------------
  static const QuadratureRule quad_rule_on_quad[] =
  {quadratureRuleQuad1pt,quadratureRuleQuad4pt,quadratureRuleQuad9pt};

  const ListOfQuadratureRule listQuadratureRuleQuadrilateral("listQuadratureRuleQuadrilateral",3,quad_rule_on_quad);


  /************************************************************************
   *                          Quadrature Rules on tetrahedra
   ************************************************************************/
  //----------------------------------------------------------------------

  static const QuadraturePoint pt_tetra_1pt[ 1 ] = {
    QuadraturePoint( 1. / 4., 1. / 4., 1. / 4., 1. / 6. )
  };
  const QuadratureRule quadratureRuleTetra1pt( pt_tetra_1pt,
      "quadratureRuleTetra1pt", TETRAHEDRON, 1, 1 );
  //----------------------------------------------------------------------
  const double tet4ptx1 = ( 5. - std::sqrt( 5. ) ) / 20., tet4ptx2 = ( 5. + 3*std::sqrt( 5. ) ) / 20.;

  static const QuadraturePoint pt_tetra_4pt[ 4 ] = {
    QuadraturePoint( tet4ptx1, tet4ptx1, tet4ptx1, 1. / 24. ),
    QuadraturePoint( tet4ptx1, tet4ptx1, tet4ptx2, 1. / 24. ),
    QuadraturePoint( tet4ptx1, tet4ptx2, tet4ptx1, 1. / 24. ),
    QuadraturePoint( tet4ptx2, tet4ptx1, tet4ptx1, 1. / 24. )
  };
  const QuadratureRule quadratureRuleTetra4pt( pt_tetra_4pt,
      "quadratureRuleTetra4pt", TETRAHEDRON, 4, 2 );
  //----------------------------------------------------------------------
  // 5 points Integration rule for tetraedra (Ref. e.g. Comincioli pag. 236)
  const double tet5ptx1 = 1. / 6. , tet5ptx2 = 1. / 2., tet5ptx3 = 1. / 4.;

  static const QuadraturePoint pt_tetra_5pt[ 5 ] = {
    QuadraturePoint( tet5ptx1, tet5ptx1, tet5ptx1, 9. / 120. ),
    QuadraturePoint( tet5ptx1, tet5ptx1, tet5ptx2, 9. / 120. ),
    QuadraturePoint( tet5ptx1, tet5ptx2, tet5ptx1, 9. / 120. ),
    QuadraturePoint( tet5ptx2, tet5ptx1, tet5ptx1, 9. / 120. ),
    QuadraturePoint( tet5ptx3, tet5ptx3, tet5ptx3, -16. / 120. )
  };

  const QuadratureRule quadratureRuleTetra5pt( pt_tetra_5pt,
      "quadratureRuleTetra5pt", TETRAHEDRON, 5, 3 );
  //
  //----------------------------------------------------------------------
  //                     15 points integration rule for tetra.
  //                   D o E = 5 (Stroud, T3:5-1 pag. 315)
  // r
  const double r5 = 0.25;
  // s
  const double s5[ 4 ] = {
    0.09197107805272303, 0.3197936278296299
  };
  // (7 \mp \std::sqrt(15))/34
  // t
  const double t5[ 4 ] = {
    0.7240867658418310, 0.04061911651111023
  };
  // (13 \pm 3*std::sqrt(15))/34
  // u
  const double u5 = 0.05635083268962915; // (10-2*std::sqrt(15))/40
  // v
  const double v5 = 0.4436491673103708; // (10+2*std::sqrt(15))/40
  // A
  const double A5 = 0.01975308641975309; // 16/135*1/6
  // B
  const double B5[ 2 ] = {
    0.01198951396316977, 0.01151136787104540
  };
  // 1/6*(2665 \pm 14*std::sqrt(15))/37800
  // C
  const double C5 = 0.008818342151675485; // 20/378*1/6
  //
  static const QuadraturePoint pt_tetra_15pt[ 15 ] = {
    QuadraturePoint( r5, r5, r5, A5 ),
    QuadraturePoint( s5[ 0 ], s5[ 0 ], s5[ 0 ], B5[ 0 ] ),
    QuadraturePoint( t5[ 0 ], s5[ 0 ], s5[ 0 ], B5[ 0 ] ),
    QuadraturePoint( s5[ 0 ], t5[ 0 ], s5[ 0 ], B5[ 0 ] ),
    QuadraturePoint( s5[ 0 ], s5[ 0 ], t5[ 0 ], B5[ 0 ] ),
    QuadraturePoint( s5[ 1 ], s5[ 1 ], s5[ 1 ], B5[ 1 ] ),
    QuadraturePoint( t5[ 1 ], s5[ 1 ], s5[ 1 ], B5[ 1 ] ),
    QuadraturePoint( s5[ 1 ], t5[ 1 ], s5[ 1 ], B5[ 1 ] ),
    QuadraturePoint( s5[ 1 ], s5[ 1 ], t5[ 1 ], B5[ 1 ] ),
    QuadraturePoint( u5, u5, v5, C5 ),
    QuadraturePoint( u5, v5, u5, C5 ),
    QuadraturePoint( v5, u5, u5, C5 ),
    QuadraturePoint( v5, v5, u5, C5 ),
    QuadraturePoint( v5, u5, v5, C5 ),
    QuadraturePoint( u5, v5, v5, C5 )
  };
  //
  const QuadratureRule quadratureRuleTetra15pt( pt_tetra_15pt,
      "quadratureRuleTetra15pt",
      TETRAHEDRON, 15, 5 );
  //----------------------------------------------------------------------
  //                     64 points integration rule for tetra.
  //                   D o E = 7 (Stroud, T3:7-1 pag. 315)
  //
  // t
  const double t[ 4 ] = {
    0.0485005494, 0.2386007376, 0.5170472951, 0.7958514179
  };
  // s
  const double s[ 4 ] = {
    0.0571041961, 0.2768430136, 0.5835904324, 0.8602401357
  };
  // r
  const double r[ 4 ] = {
    0.0694318422, 0.3300094782, 0.6699905218, 0.9305681558
  };
  // A
  const double A[ 4 ] = {
    0.1739274226, 0.3260725774, 0.3260725774, 0.1739274226
  };
  // B
  const double B[ 4 ] = {
    0.1355069134, 0.2034645680, 0.1298475476, 0.0311809709
  };
  // C
  const double C[ 4 ] = {
    0.1108884156, 0.1434587898, 0.0686338872, 0.0103522407
  };

  static const QuadraturePoint pt_tetra_64pt[ 64 ] = {
    QuadraturePoint( t[ 0 ], s[ 0 ] * ( 1 - t[ 0 ] ), r[ 0 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 0 ] ), A[ 0 ] * B[ 0 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 0 ] * ( 1 - t[ 1 ] ), r[ 0 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 1 ] ), A[ 0 ] * B[ 0 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 0 ] * ( 1 - t[ 2 ] ), r[ 0 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 2 ] ), A[ 0 ] * B[ 0 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 0 ] * ( 1 - t[ 3 ] ), r[ 0 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 3 ] ), A[ 0 ] * B[ 0 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 1 ] * ( 1 - t[ 0 ] ), r[ 0 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 0 ] ), A[ 0 ] * B[ 1 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 1 ] * ( 1 - t[ 1 ] ), r[ 0 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 1 ] ), A[ 0 ] * B[ 1 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 1 ] * ( 1 - t[ 2 ] ), r[ 0 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 2 ] ), A[ 0 ] * B[ 1 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 1 ] * ( 1 - t[ 3 ] ), r[ 0 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 3 ] ), A[ 0 ] * B[ 1 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 2 ] * ( 1 - t[ 0 ] ), r[ 0 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 0 ] ), A[ 0 ] * B[ 2 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 2 ] * ( 1 - t[ 1 ] ), r[ 0 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 1 ] ), A[ 0 ] * B[ 2 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 2 ] * ( 1 - t[ 2 ] ), r[ 0 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 2 ] ), A[ 0 ] * B[ 2 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 2 ] * ( 1 - t[ 3 ] ), r[ 0 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 3 ] ), A[ 0 ] * B[ 2 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 3 ] * ( 1 - t[ 0 ] ), r[ 0 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 0 ] ), A[ 0 ] * B[ 3 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 3 ] * ( 1 - t[ 1 ] ), r[ 0 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 1 ] ), A[ 0 ] * B[ 3 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 3 ] * ( 1 - t[ 2 ] ), r[ 0 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 2 ] ), A[ 0 ] * B[ 3 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 3 ] * ( 1 - t[ 3 ] ), r[ 0 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 3 ] ), A[ 0 ] * B[ 3 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 0 ] * ( 1 - t[ 0 ] ), r[ 1 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 0 ] ), A[ 1 ] * B[ 0 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 0 ] * ( 1 - t[ 1 ] ), r[ 1 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 1 ] ), A[ 1 ] * B[ 0 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 0 ] * ( 1 - t[ 2 ] ), r[ 1 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 2 ] ), A[ 1 ] * B[ 0 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 0 ] * ( 1 - t[ 3 ] ), r[ 1 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 3 ] ), A[ 1 ] * B[ 0 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 1 ] * ( 1 - t[ 0 ] ), r[ 1 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 0 ] ), A[ 1 ] * B[ 1 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 1 ] * ( 1 - t[ 1 ] ), r[ 1 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 1 ] ), A[ 1 ] * B[ 1 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 1 ] * ( 1 - t[ 2 ] ), r[ 1 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 2 ] ), A[ 1 ] * B[ 1 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 1 ] * ( 1 - t[ 3 ] ), r[ 1 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 3 ] ), A[ 1 ] * B[ 1 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 2 ] * ( 1 - t[ 0 ] ), r[ 1 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 0 ] ), A[ 1 ] * B[ 2 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 2 ] * ( 1 - t[ 1 ] ), r[ 1 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 1 ] ), A[ 1 ] * B[ 2 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 2 ] * ( 1 - t[ 2 ] ), r[ 1 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 2 ] ), A[ 1 ] * B[ 2 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 2 ] * ( 1 - t[ 3 ] ), r[ 1 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 3 ] ), A[ 1 ] * B[ 2 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 3 ] * ( 1 - t[ 0 ] ), r[ 1 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 0 ] ), A[ 1 ] * B[ 3 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 3 ] * ( 1 - t[ 1 ] ), r[ 1 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 1 ] ), A[ 1 ] * B[ 3 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 3 ] * ( 1 - t[ 2 ] ), r[ 1 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 2 ] ), A[ 1 ] * B[ 3 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 3 ] * ( 1 - t[ 3 ] ), r[ 1 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 3 ] ), A[ 1 ] * B[ 3 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 0 ] * ( 1 - t[ 0 ] ), r[ 2 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 0 ] ), A[ 2 ] * B[ 0 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 0 ] * ( 1 - t[ 1 ] ), r[ 2 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 1 ] ), A[ 2 ] * B[ 0 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 0 ] * ( 1 - t[ 2 ] ), r[ 2 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 2 ] ), A[ 2 ] * B[ 0 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 0 ] * ( 1 - t[ 3 ] ), r[ 2 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 3 ] ), A[ 2 ] * B[ 0 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 1 ] * ( 1 - t[ 0 ] ), r[ 2 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 0 ] ), A[ 2 ] * B[ 1 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 1 ] * ( 1 - t[ 1 ] ), r[ 2 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 1 ] ), A[ 2 ] * B[ 1 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 1 ] * ( 1 - t[ 2 ] ), r[ 2 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 2 ] ), A[ 2 ] * B[ 1 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 1 ] * ( 1 - t[ 3 ] ), r[ 2 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 3 ] ), A[ 2 ] * B[ 1 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 2 ] * ( 1 - t[ 0 ] ), r[ 2 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 0 ] ), A[ 2 ] * B[ 2 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 2 ] * ( 1 - t[ 1 ] ), r[ 2 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 1 ] ), A[ 2 ] * B[ 2 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 2 ] * ( 1 - t[ 2 ] ), r[ 2 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 2 ] ), A[ 2 ] * B[ 2 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 2 ] * ( 1 - t[ 3 ] ), r[ 2 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 3 ] ), A[ 2 ] * B[ 2 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 3 ] * ( 1 - t[ 0 ] ), r[ 2 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 0 ] ), A[ 2 ] * B[ 3 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 3 ] * ( 1 - t[ 1 ] ), r[ 2 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 1 ] ), A[ 2 ] * B[ 3 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 3 ] * ( 1 - t[ 2 ] ), r[ 2 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 2 ] ), A[ 2 ] * B[ 3 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 3 ] * ( 1 - t[ 3 ] ), r[ 2 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 3 ] ), A[ 2 ] * B[ 3 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 0 ] * ( 1 - t[ 0 ] ), r[ 3 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 0 ] ), A[ 3 ] * B[ 0 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 0 ] * ( 1 - t[ 1 ] ), r[ 3 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 1 ] ), A[ 3 ] * B[ 0 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 0 ] * ( 1 - t[ 2 ] ), r[ 3 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 2 ] ), A[ 3 ] * B[ 0 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 0 ] * ( 1 - t[ 3 ] ), r[ 3 ] * ( 1 - s[ 0 ] ) * ( 1 - t[ 3 ] ), A[ 3 ] * B[ 0 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 1 ] * ( 1 - t[ 0 ] ), r[ 3 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 0 ] ), A[ 3 ] * B[ 1 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 1 ] * ( 1 - t[ 1 ] ), r[ 3 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 1 ] ), A[ 3 ] * B[ 1 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 1 ] * ( 1 - t[ 2 ] ), r[ 3 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 2 ] ), A[ 3 ] * B[ 1 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 1 ] * ( 1 - t[ 3 ] ), r[ 3 ] * ( 1 - s[ 1 ] ) * ( 1 - t[ 3 ] ), A[ 3 ] * B[ 1 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 2 ] * ( 1 - t[ 0 ] ), r[ 3 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 0 ] ), A[ 3 ] * B[ 2 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 2 ] * ( 1 - t[ 1 ] ), r[ 3 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 1 ] ), A[ 3 ] * B[ 2 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 2 ] * ( 1 - t[ 2 ] ), r[ 3 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 2 ] ), A[ 3 ] * B[ 2 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 2 ] * ( 1 - t[ 3 ] ), r[ 3 ] * ( 1 - s[ 2 ] ) * ( 1 - t[ 3 ] ), A[ 3 ] * B[ 2 ] * C[ 3 ] ),
    QuadraturePoint( t[ 0 ], s[ 3 ] * ( 1 - t[ 0 ] ), r[ 3 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 0 ] ), A[ 3 ] * B[ 3 ] * C[ 0 ] ),
    QuadraturePoint( t[ 1 ], s[ 3 ] * ( 1 - t[ 1 ] ), r[ 3 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 1 ] ), A[ 3 ] * B[ 3 ] * C[ 1 ] ),
    QuadraturePoint( t[ 2 ], s[ 3 ] * ( 1 - t[ 2 ] ), r[ 3 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 2 ] ), A[ 3 ] * B[ 3 ] * C[ 2 ] ),
    QuadraturePoint( t[ 3 ], s[ 3 ] * ( 1 - t[ 3 ] ), r[ 3 ] * ( 1 - s[ 3 ] ) * ( 1 - t[ 3 ] ), A[ 3 ] * B[ 3 ] * C[ 3 ] )
  };
  //
  const QuadratureRule quadratureRuleTetra64pt( pt_tetra_64pt,
      "quadratureRuleTetra64pt",
      TETRAHEDRON, 64, 7 );

  //----------------------------------------------------------------------
  //  List of quadrature rules on tetrahedra
  //----------------------------------------------------------------------
  static const QuadratureRule quad_rule_on_tetra[] =
  {quadratureRuleTetra1pt,quadratureRuleTetra4pt,quadratureRuleTetra5pt,quadratureRuleTetra15pt,quadratureRuleTetra64pt};

  const ListOfQuadratureRule listQuadratureRuleTetrahedron("listQuadratureRuleTetrahedron",5,quad_rule_on_tetra);

  /************************************************************************
   *                          Quadrature Rules on hexahedra
   ************************************************************************/

  static const QuadraturePoint pt_hexa_1pt[ 1 ] = {
    QuadraturePoint( 0., 0., 0., 8. )
  };
  const QuadratureRule quadratureRuleHexa1pt( pt_hexa_1pt,
      "quadratureRuleHexa1pt", HEXAHEDRON, 1, 1 );
  //----------------------------------------------------------------------
  // 8 points Integration rule for hexahedron (tensorization of 2 pts on segment)
  static const QuadraturePoint pt_hexa_8pt[ 8 ] = {
    QuadraturePoint( q2ptx1, q2ptx1, q2ptx1, q2ptw1 * q2ptw1 * q2ptw1 ),
    QuadraturePoint( q2ptx2, q2ptx1, q2ptx1, q2ptw2 * q2ptw1 * q2ptw1 ),
    QuadraturePoint( q2ptx2, q2ptx2, q2ptx1, q2ptw2 * q2ptw2 * q2ptw1 ),
    QuadraturePoint( q2ptx1, q2ptx2, q2ptx1, q2ptw1 * q2ptw2 * q2ptw1 ),
    QuadraturePoint( q2ptx1, q2ptx1, q2ptx2, q2ptw1 * q2ptw1 * q2ptw2 ),
    QuadraturePoint( q2ptx2, q2ptx1, q2ptx2, q2ptw2 * q2ptw1 * q2ptw2 ),
    QuadraturePoint( q2ptx2, q2ptx2, q2ptx2, q2ptw2 * q2ptw2 * q2ptw2 ),
    QuadraturePoint( q2ptx1, q2ptx2, q2ptx2, q2ptw1 * q2ptw2 * q2ptw2 )
  };
  const QuadratureRule quadratureRuleHexa8pt( pt_hexa_8pt,
      "quadratureRuleHexa8pt", HEXAHEDRON, 8, 3 );

  //----------------------------------------------------------------------
  // 27 points Integration rule for hexahedron (tensorization of 3 pts on segment)
  static const QuadraturePoint pt_hexa_27pt[ 27 ] = {
    QuadraturePoint( q3ptx1, q3ptx1, q3ptx1, q3ptw1 * q3ptw1 * q3ptw1),
    QuadraturePoint( q3ptx2, q3ptx1, q3ptx1, q3ptw2 * q3ptw1 * q3ptw1),
    QuadraturePoint( q3ptx3, q3ptx1, q3ptx1, q3ptw3 * q3ptw1 * q3ptw1),
    QuadraturePoint( q3ptx1, q3ptx2, q3ptx1, q3ptw1 * q3ptw2 * q3ptw1),
    QuadraturePoint( q3ptx2, q3ptx2, q3ptx1, q3ptw2 * q3ptw2 * q3ptw1),
    QuadraturePoint( q3ptx3, q3ptx2, q3ptx1, q3ptw3 * q3ptw2 * q3ptw1),
    QuadraturePoint( q3ptx1, q3ptx3, q3ptx1, q3ptw1 * q3ptw3 * q3ptw1),
    QuadraturePoint( q3ptx2, q3ptx3, q3ptx1, q3ptw2 * q3ptw3 * q3ptw1),
    QuadraturePoint( q3ptx3, q3ptx3, q3ptx1, q3ptw3 * q3ptw3 * q3ptw1),

    QuadraturePoint( q3ptx1, q3ptx1, q3ptx2, q3ptw1 * q3ptw1 * q3ptw2),
    QuadraturePoint( q3ptx2, q3ptx1, q3ptx2, q3ptw2 * q3ptw1 * q3ptw2),
    QuadraturePoint( q3ptx3, q3ptx1, q3ptx2, q3ptw3 * q3ptw1 * q3ptw2),
    QuadraturePoint( q3ptx1, q3ptx2, q3ptx2, q3ptw1 * q3ptw2 * q3ptw2),
    QuadraturePoint( q3ptx2, q3ptx2, q3ptx2, q3ptw2 * q3ptw2 * q3ptw2),
    QuadraturePoint( q3ptx3, q3ptx2, q3ptx2, q3ptw3 * q3ptw2 * q3ptw2),
    QuadraturePoint( q3ptx1, q3ptx3, q3ptx2, q3ptw1 * q3ptw3 * q3ptw2),
    QuadraturePoint( q3ptx2, q3ptx3, q3ptx2, q3ptw2 * q3ptw3 * q3ptw2),
    QuadraturePoint( q3ptx3, q3ptx3, q3ptx2, q3ptw3 * q3ptw3 * q3ptw2),

    QuadraturePoint( q3ptx1, q3ptx1, q3ptx3, q3ptw1 * q3ptw1 * q3ptw3),
    QuadraturePoint( q3ptx2, q3ptx1, q3ptx3, q3ptw2 * q3ptw1 * q3ptw3),
    QuadraturePoint( q3ptx3, q3ptx1, q3ptx3, q3ptw3 * q3ptw1 * q3ptw3),
    QuadraturePoint( q3ptx1, q3ptx2, q3ptx3, q3ptw1 * q3ptw2 * q3ptw3),
    QuadraturePoint( q3ptx2, q3ptx2, q3ptx3, q3ptw2 * q3ptw2 * q3ptw3),
    QuadraturePoint( q3ptx3, q3ptx2, q3ptx3, q3ptw3 * q3ptw2 * q3ptw3),
    QuadraturePoint( q3ptx1, q3ptx3, q3ptx3, q3ptw1 * q3ptw3 * q3ptw3),
    QuadraturePoint( q3ptx2, q3ptx3, q3ptx3, q3ptw2 * q3ptw3 * q3ptw3),
    QuadraturePoint( q3ptx3, q3ptx3, q3ptx3, q3ptw3 * q3ptw3 * q3ptw3),

  };
  const QuadratureRule quadratureRuleHexa27pt( pt_hexa_27pt,
      "quadratureRuleHexa27pt", HEXAHEDRON, 27, 5 );

  //----------------------------------------------------------------------
  //  List of quadrature rules on hexahedra
  //----------------------------------------------------------------------
  static const QuadratureRule quad_rule_on_hexa[] =
  {quadratureRuleHexa1pt,quadratureRuleHexa8pt,quadratureRuleHexa27pt};

  const ListOfQuadratureRule listQuadratureRuleHexahedron("listQuadratureRuleHexahedron",3,quad_rule_on_hexa);
  const ListOfQuadratureRule listQuadratureRuleHexahedronCombined("listQuadratureRuleHexahedronCombined",listQuadratureRuleQuadrilateral,listQuadratureRuleSegment);


  /************************************************************************
   *                          Quadrature Rules on Prisms
   ************************************************************************/

  //----------------------------------------------------------------------
  // TODO: verify the deegre of exactness of all the prisms quadrature rules.
  // In the case of the composite rules it has been
  // arbitrarly std::set to a value so that they can be chosen.
  const double p3ptx1 = 1.0/6.0; const double p3ptx2 = 2.0/3.0;
  static const QuadraturePoint pt_prism_6pt[ 6 ] = {
    QuadraturePoint( p3ptx2, p3ptx1 , q2ptx1 , t3ptw * q2ptw1 ),
    QuadraturePoint( p3ptx1, p3ptx2 , q2ptx1 , t3ptw * q2ptw1 ),
    QuadraturePoint( p3ptx1, p3ptx1 , q2ptx1 , t3ptw * q2ptw1 ),
    QuadraturePoint( p3ptx2, p3ptx1 , q2ptx2 , t3ptw * q2ptw2 ),
    QuadraturePoint( p3ptx1, p3ptx2 , q2ptx2 , t3ptw * q2ptw2 ),
    QuadraturePoint( p3ptx1, p3ptx1 , q2ptx2 , t3ptw * q2ptw2 )
  };
  const QuadratureRule quadratureRulePrism6pt( pt_prism_6pt,
       "quadratureRulePrism6pt", PRISM, 6, 3 );

  static const QuadraturePoint pt_prism_12pt_composite[ 12 ] = {
    QuadraturePoint( p3ptx2, p3ptx1, q2ptx1/2-0.5, t3ptw * q2ptw1/2 ),
    QuadraturePoint( p3ptx1, p3ptx2, q2ptx1/2-0.5, t3ptw * q2ptw1/2 ),
    QuadraturePoint( p3ptx2, p3ptx2, q2ptx1/2-0.5, t3ptw * q2ptw1/2 ),
    QuadraturePoint( p3ptx2, p3ptx1, q2ptx2/2-0.5, t3ptw * q2ptw2/2 ),
    QuadraturePoint( p3ptx1, p3ptx2, q2ptx2/2-0.5, t3ptw * q2ptw2/2 ),
    QuadraturePoint( p3ptx2, p3ptx2, q2ptx2/2-0.5, t3ptw * q2ptw2/2 ),

    QuadraturePoint( p3ptx2, p3ptx1, q2ptx1/2+0.5, t3ptw * q2ptw1/2 ),
    QuadraturePoint( p3ptx1, p3ptx2, q2ptx1/2+0.5, t3ptw * q2ptw1/2 ),
    QuadraturePoint( p3ptx2, p3ptx2, q2ptx1/2+0.5, t3ptw * q2ptw1/2 ),
    QuadraturePoint( p3ptx2, p3ptx1, q2ptx2/2+0.5, t3ptw * q2ptw2/2 ),
    QuadraturePoint( p3ptx1, p3ptx2, q2ptx2/2+0.5, t3ptw * q2ptw2/2 ),
    QuadraturePoint( p3ptx2, p3ptx2, q2ptx2/2+0.5, t3ptw * q2ptw2/2 )
  };
  const QuadratureRule quadratureRulePrism12ptComposite( pt_prism_12pt_composite,
    "quadratureRulePrism12ptComposite", PRISM, 12, 4 ); // I do not know the real order of extactness.

  static const QuadraturePoint pt_prism_21pt[ 21 ] = {
    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx1, t7pt_w0*q3ptw1 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx1, t7pt_w1*q3ptw1 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx1, t7pt_w1*q3ptw1 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx1, t7pt_w1*q3ptw1 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx1, t7pt_w2*q3ptw1 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx1, t7pt_w2*q3ptw1 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx1, t7pt_w2*q3ptw1 ),

    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx2, t7pt_w0*q3ptw2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx2, t7pt_w1*q3ptw2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx2, t7pt_w1*q3ptw2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx2, t7pt_w1*q3ptw2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx2, t7pt_w2*q3ptw2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx2, t7pt_w2*q3ptw2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx2, t7pt_w2*q3ptw2 ),

    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx3, t7pt_w0*q3ptw3 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx3, t7pt_w1*q3ptw3 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx3, t7pt_w1*q3ptw3 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx3, t7pt_w1*q3ptw3 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx3, t7pt_w2*q3ptw3 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx3, t7pt_w2*q3ptw3 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx3, t7pt_w2*q3ptw3 )
  };
  const QuadratureRule quadratureRulePrism21pt( pt_prism_21pt,
      "quadratureRulePrism21pt", PRISM, 21, 5 );

  static const QuadraturePoint pt_prism_42ptComposite[ 42 ] = {
    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx1/2 - 0.5, t7pt_w0*q3ptw1/2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx1/2-0.5, t7pt_w1*q3ptw1/2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx1/2-0.5, t7pt_w1*q3ptw1/2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx1/2-0.5, t7pt_w1*q3ptw1/2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx1/2-0.5, t7pt_w2*q3ptw1/2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx1/2-0.5, t7pt_w2*q3ptw1/2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx1/2-0.5, t7pt_w2*q3ptw1/2 ),

    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx2/2-0.5, t7pt_w0*q3ptw2/2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx2/2-0.5, t7pt_w1*q3ptw2/2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx2/2-0.5, t7pt_w1*q3ptw2/2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx2/2-0.5, t7pt_w1*q3ptw2/2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx2/2-0.5, t7pt_w2*q3ptw2/2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx2/2-0.5, t7pt_w2*q3ptw2/2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx2/2-0.5, t7pt_w2*q3ptw2/2 ),

    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx3/2-0.5, t7pt_w0*q3ptw3/2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx3/2-0.5, t7pt_w1*q3ptw3/2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx3/2-0.5, t7pt_w1*q3ptw3/2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx3/2-0.5, t7pt_w1*q3ptw3/2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx3/2-0.5, t7pt_w2*q3ptw3/2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx3/2-0.5, t7pt_w2*q3ptw3/2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx3/2-0.5, t7pt_w2*q3ptw3/2 ),


    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx1/2+0.5, t7pt_w0*q3ptw1/2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx1/2+0.5, t7pt_w1*q3ptw1/2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx1/2+0.5, t7pt_w1*q3ptw1/2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx1/2+0.5, t7pt_w1*q3ptw1/2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx1/2+0.5, t7pt_w2*q3ptw1/2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx1/2+0.5, t7pt_w2*q3ptw1/2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx1/2+0.5, t7pt_w2*q3ptw1/2 ),

    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx2/2+0.5, t7pt_w0*q3ptw2/2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx2/2+0.5, t7pt_w1*q3ptw2/2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx2/2+0.5, t7pt_w1*q3ptw2/2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx2/2+0.5, t7pt_w1*q3ptw2/2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx2/2+0.5, t7pt_w2*q3ptw2/2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx2/2+0.5, t7pt_w2*q3ptw2/2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx2/2+0.5, t7pt_w2*q3ptw2/2 ),

    QuadraturePoint(     t7pt_x0,     t7pt_x0,q3ptx3/2+0.5, t7pt_w0*q3ptw3/2 ),QuadraturePoint(     t7pt_x1,     t7pt_x1,q3ptx3/2+0.5, t7pt_w1*q3ptw3/2 ),
    QuadraturePoint(     t7pt_x1, 1-2*t7pt_x1,q3ptx3/2+0.5, t7pt_w1*q3ptw3/2 ),QuadraturePoint( 1-2*t7pt_x1,     t7pt_x1,q3ptx3/2+0.5, t7pt_w1*q3ptw3/2 ),
    QuadraturePoint(     t7pt_x2,     t7pt_x2,q3ptx3/2+0.5, t7pt_w2*q3ptw3/2 ),QuadraturePoint(     t7pt_x2, 1-2*t7pt_x2,q3ptx3/2+0.5, t7pt_w2*q3ptw3/2 ),
    QuadraturePoint( 1-2*t7pt_x2,     t7pt_x2,q3ptx3/2+0.5, t7pt_w2*q3ptw3/2 )
  };
  const QuadratureRule quadratureRulePrism42ptComposite( pt_prism_42ptComposite,
      "quadratureRulePrism42ptComposite", PRISM, 42, 6 );
  /*
    static const QuadraturePoint pt_prism_15pt[ 15 ] =
    {
    QuadraturePoint( p3ptx2, p3ptx1, q2ptx1, t3ptw * q2ptw1 ),
    QuadraturePoint( p3ptx1, p3ptx2, q2ptx1, t3ptw * q2ptw1 ),
    QuadraturePoint( p3ptx2, p3ptx2, q2ptx1, t3ptw * q2ptw1 ),
    QuadraturePoint( p3ptx2, p3ptx1, q2ptx2, t3ptw * q2ptw2 ),
    QuadraturePoint( p3ptx1, p3ptx2, q2ptx2, t3ptw * q2ptw2 ),
    QuadraturePoint( p3ptx2, p3ptx2, q2ptx2, t3ptw * q2ptw2 )
    };
    const QuadratureRule quadratureRulePrism15pt( pt_prism_15pt,
    "quadratureRulePrism15pt", PRISM, 15, 3 );
  */
  //----------------------------------------------------------------------
  //  List of quadrature rules on Prisms
  //----------------------------------------------------------------------
  static const QuadratureRule quad_rule_on_prism[] =
    {quadratureRulePrism6pt,
     quadratureRulePrism12ptComposite,
     quadratureRulePrism21pt,
     quadratureRulePrism42ptComposite
    };

  const ListOfQuadratureRule listQuadratureRulePrism("listQuadratureRulePrism",4,quad_rule_on_prism);
  const ListOfQuadratureRule listQuadratureRulePrismCombined("listQuadratureRulePrismCombined",listQuadratureRuleTriangle,listQuadratureRuleSegment);

  /*========================================================================
    !
    !                           GEOMETRIC ELEMENT
    !
    =======================================================================*/


  /************************************************************************
   *   GeoElementNULL
   *
   *     VOID
   *
   *************************************************************************/
  const GeoElement geoElementNULL("geoElementNULL",NULLSHAPE,basisFunctionNULL,nullptr,0,0,
                                  nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,refElementSegmentP1/*refElementNULL*/);


  /************************************************************************
   *   GeoElementNode
   *
   *     0d
   *
   *************************************************************************/
  static const double refcoorNode[] = {0.,0.,0.};
  const GeoElement geoElementNode("geoElementNode",NODE,basisFunction0d,refcoorNode,0,0,
                                  nullptr,nullptr,nullptr,nullptr,nullptr,nullptr,refElementNode);


  /************************************************************************
   *   GeoElementSegmentP1
   *
   *     0-----------1
   *
   *************************************************************************/
  static const int m_ptOfEdLinearSeg[] = {0,1};
  static const double refcoor_P1_1D[] = {-1.,0.,0.,  1.,0.,0.};

  static const Point m_pointSegmentP1[] = {
    Point(-1.,0.,0.),Point(1.,0.,0.)
  };

  const GeoElement geoElementSegmentP1("geoElementSegmentP1",SEGMENT,basisFunction1dP1,refcoor_P1_1D,1,0,
                                       m_ptOfEdLinearSeg,nullptr,nullptr,nullptr,nullptr,nullptr,refElementSegmentP1);


  /************************************************************************
   *   GeoElementSegmentP1b
   *
   *     0-----2-----1
   *
   *************************************************************************/
  static const int m_ptOfEdLinearSegP1b[] = {0,1,2};
  static const double refcoor_P1b_1D[] = {-1.,0.,0.,  1.,0.,0., 0.,0.,0.};

  static const Point m_pointSegmentP1b[] = {
    Point(-1.,0.,0.),Point(1.,0.,0.),Point(0.,0.,0.)
  };

  const GeoElement geoElementSegmentP1b("geoElementSegmentP1b",SEGMENT,basisFunction1dP1b,refcoor_P1b_1D,1,0,
                                        m_ptOfEdLinearSegP1b,nullptr,nullptr,nullptr,nullptr,nullptr,refElementSegmentP1b);


  /************************************************************************
   *   GeoElementSegmentP2
   *
   *     0-----2-----1
   *
   *************************************************************************/
  static const int m_ptOfEdLinearSegP2[] = {0,1,2};
  static const double refcoor_P2_1D[] = {-1.,0.,0.,  1.,0.,0., 0.,0.,0.};

  static const Point m_pointSegmentP2[] = {
    Point(-1.,0.,0.),Point(1.,0.,0.),Point(0.,0.,0.)
  };

  const GeoElement geoElementSegmentP2("geoElementSegmentP2",SEGMENT,basisFunction1dP2,refcoor_P2_1D,1,0,
                                       m_ptOfEdLinearSegP2,nullptr,nullptr,nullptr,nullptr,nullptr,refElementSegmentP2);


  /************************************************************************
   *  geoElementTriangleP0
   *
   *     \
   *     | \
   *     |   \
   *     |     \
   *     |  0    \
   *     |         \
   *     ------------
   *
   *************************************************************************/

  /************************************************************************
   *  geoElementTriangleP1
   *
   *     2
   *     | \
   *     |   \
   *     |     \
   *     |       \
   *     |         \
   *     0-----------1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoTriangleP1[] = {&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1};
  static const int m_ptOfEdLinearTriangle[] = {0,1,  1,2,  2,0};
  static const int m_ptOfFaLinearTriangle[] = {0,1,2};
  static const int m_edOfFaLinearTriangle[] = {0,1,2};
  static const bool m_orientEdLinearTriangle[] = {false,false,false};
  static const double refcoor_P1_2D[] = {0.,0.,0.,  1.,0.,0.,  0.,1.,0.};


  const GeoElement geoElementTriangleP1("geoElementTriangleP1",TRIANGLE,basisFunction2dP1,refcoor_P1_2D,3,1,
                                        m_ptOfEdLinearTriangle,m_ptOfFaLinearTriangle,m_edOfFaLinearTriangle,m_orientEdLinearTriangle,
                                        m_boundaryGeoTriangleP1,nullptr,refElementTriangleP1);


  /************************************************************************
     *  geoElementTriangleP1b
     *
     *     2
     *     | \
     *     |   \
     *     |     \
     *     |  3    \
     *     |         \
     *     0-----------1
     *
     *************************************************************************/
  static const int m_ptOfFaLinearTriangleP1b[] = {0,1,2,3};
  static const double refcoor_P1b_2D[] = {0.,0.,0.,  1.,0.,0.,  0.,1.,0., 1./3.,1./3.,0.};

  const GeoElement geoElementTriangleP1b("geoElementTriangleP1b",TRIANGLE,basisFunction2dP1b,refcoor_P1b_2D,3,1,
                                         m_ptOfEdLinearTriangle,m_ptOfFaLinearTriangleP1b,m_edOfFaLinearTriangle,
                                         m_orientEdLinearTriangle,m_boundaryGeoTriangleP1,nullptr,
                                         refElementTriangleP1b);


  /************************************************************************
   *  geoElementTriangleP2
   *
   *     2
   *     | \
   *     |   \
   *     5     4
   *     |       \
   *     |         \
   *     0-----3----1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoTriangleP2[] = {&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2};
  static const int m_ptOfEdLinearTriangleP2[] = {0,1,3,  1,2,4,  2,0,5};
  static const int m_ptOfFaLinearTriangleP2[] = {0,1,2,3,4,5};
  static const int m_edOfFaLinearTriangleP2[] = {0,1,2};
  static const bool m_orientEdLinearTriangleP2[] = {false,false,false};
  static const double refcoor_P2_2D[] = {0.,0.,0.,  1.,0.,0.,  0.,1.,0.,  0.5,0.,0.,  0.5,0.5,0.,  0.,0.5,0.};


  const GeoElement geoElementTriangleP2("geoElementTriangleP2",TRIANGLE,basisFunction2dP2,refcoor_P2_2D,3,1,
                                        m_ptOfEdLinearTriangleP2,m_ptOfFaLinearTriangleP2,m_edOfFaLinearTriangleP2,m_orientEdLinearTriangleP2,
                                        m_boundaryGeoTriangleP2,nullptr,refElementTriangleP2);


  /************************************************************************
   *  geoElementQuadrangleQ0
   *
   *     -------------
   *     |            |
   *     |            |
   *     |     0      |
   *     |            |
   *     |            |
   *     -------------
   *
   *************************************************************************/



  /************************************************************************
   *  geoElementQuadrangleQ1
   *
   *     3-----------2
   *     |           |
   *     |           |
   *     |           |
   *     |           |
   *     |           |
   *     0-----------1
   *
   *************************************************************************/

  static const GeoElement* m_boundaryGeoQuadrangleQ1[] = {&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1};
  static const int m_ptOfEdLinearQuadrangle[] = {0,1,  1,2,  2,3,  3,0};
  static const int m_ptOfFaLinearQuadrangle[] = {0,1,2,3}; // numerotation of the points
  static const int m_edOfFaLinearQuadrangle[] = {0,1,2,3};  // numerotation of the edges
  static const bool m_orientEdLinearQuadrangle[] = {false,false,false,false};
  static const double refcoor_Q1_2D[] = {-1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0};

  const GeoElement geoElementQuadrangleQ1("geoElementQuadrangleQ1",QUADRILATERAL,basisFunction2dQ1,refcoor_Q1_2D,4,1,
                                          m_ptOfEdLinearQuadrangle,m_ptOfFaLinearQuadrangle,m_edOfFaLinearQuadrangle,m_orientEdLinearQuadrangle,
                                          m_boundaryGeoQuadrangleQ1,nullptr,refElementQuadrangleQ1);

  /************************************************************************
   *  geoElementQuadrangleP1xP2 (for the corresponding prism) //I thought it was important for the Prism P1xP2, but the geometric element will be R1
   *
   *     3-----------2
   *     |           |
   *     | 	         |
   *    4|           |5
   *     | 	         |
   *     | 	         |
   *     0-----------1
   *
   *************************************************************************/

  static const GeoElement* m_boundaryGeoQuadrangleP1xP2[] = {&geoElementSegmentP1,&geoElementSegmentP2,&geoElementSegmentP1,&geoElementSegmentP2};
  static const int m_ptOfEdQuadrangleP1xP2[] = {0,1,  1,2,5,  2,3,  3,0,4};
  static const int m_ptOfFaQuadrangleP1xP2[] = {0,1,2,3,4,5}; // numerotation of the points
  static const int m_edOfFaQuadrangleP1xP2[] = {0,1,2,3};  // numerotation of the edges
  static const bool m_orientEdQuadrangleP1xP2[] = {false,false,false,false};
  static const double refcoor_P1xP2_2D[] = {-1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0, -1.,0.,0., 1.,0.,0.};

  const GeoElement geoElementQuadrangleP1xP2("geoElementQuadrangleP1xP2",QUADRILATERAL,basisFunction2dP1xP2,refcoor_P1xP2_2D,4,1,
                                          m_ptOfEdQuadrangleP1xP2,m_ptOfFaQuadrangleP1xP2,m_edOfFaQuadrangleP1xP2,m_orientEdQuadrangleP1xP2,
                                          m_boundaryGeoQuadrangleP1xP2,nullptr,refElementQuadrangleP1xP2);


  /************************************************************************
   *  geoElementQuadrangleQ1b
   *
   *     3-----------2
   *     |           |
   *     |           |
   *     |     4     |
   *     |           |
   *     |           |
   *     0-----------1
   *
   *************************************************************************/

  static const int m_ptOfFaLinearQuadrangleQ1b[] = {0,1,2,3,4}; // numerotation of the points
  static const double refcoor_Q1b_2D[] = {-1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0,  0.,0.,0.};

  const GeoElement geoElementQuadrangleQ1b("geoElementQuadrangleQ1b",QUADRILATERAL,basisFunction2dQ1b,refcoor_Q1b_2D,4,1, m_ptOfEdLinearQuadrangle,m_ptOfFaLinearQuadrangleQ1b,m_edOfFaLinearQuadrangle,m_orientEdLinearQuadrangle,m_boundaryGeoQuadrangleQ1,nullptr,refElementQuadrangleQ1b);


  /************************************************************************
   *  geoElementQuadrangleQ2c
   *
   *     3-----6-----2
   *     |           |
   *     |           |
   *     7     8     5
   *     |           |
   *     |           |
   *     0-----4-----1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoQuadrangleQ2c[] = {&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2};
  static const int m_ptOfEdLinearQuadrangleQ2c[] = {0,1,4,  1,2,5,  2,3,6,  3,0,7};
  static const int m_ptOfFaLinearQuadrangleQ2c[] = {0,1,2,3,4,5,6,7,8}; // numerotation of the points
  static const int m_edOfFaLinearQuadrangleQ2c[] = {0,1,2,3};  // numerotation of the edges
  static const bool m_orientEdLinearQuadrangleQ2c[] = {false,false,false,false};
  static const double refcoor_Q2c_2D[] = {-1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0,  0.,-1.,0., 1.,0.,0.,  0.,1.,0., -1.,0.,0.,  0.,0.,0.};

  const GeoElement geoElementQuadrangleQ2c("geoElementQuadrangleQ2c",QUADRILATERAL,basisFunction2dQ2c,refcoor_Q2c_2D,4,1,
      m_ptOfEdLinearQuadrangleQ2c,m_ptOfFaLinearQuadrangleQ2c,m_edOfFaLinearQuadrangleQ2c,m_orientEdLinearQuadrangleQ2c,
      m_boundaryGeoQuadrangleQ2c,nullptr,refElementQuadrangleQ2c);


  /************************************************************************
   *  geoElementQuadrangleQ2
   *
   *     3-----6-----2
   *     |           |
   *     |           |
   *     7           5
   *     |           |
   *     |           |
   *     0-----4-----1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoQuadrangleQ2[] = {&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2};
  static const int m_ptOfEdLinearQuadrangleQ2[] = {0,1,4,  1,2,5,  2,3,6,  3,0,7};
  static const int m_ptOfFaLinearQuadrangleQ2[] = {0,1,2,3,4,5,6,7}; // numerotation of the points
  static const int m_edOfFaLinearQuadrangleQ2[] = {0,1,2,3};  // numerotation of the edges
  static const bool m_orientEdLinearQuadrangleQ2[] = {false,false,false,false};
  static const double refcoor_Q2_2D[] = {-1.,-1.,0.,  1.,-1.,0.,  1.,1.,0.,  -1.,1.,0,  0.,-1.,0., 1.,0.,0.,  0.,1.,0., -1.,0.,0.};

  const GeoElement geoElementQuadrangleQ2("geoElementQuadrangleQ2",QUADRILATERAL,basisFunction2dQ2,refcoor_Q2_2D,4,1,
                                          m_ptOfEdLinearQuadrangleQ2,m_ptOfFaLinearQuadrangleQ2,m_edOfFaLinearQuadrangleQ2,m_orientEdLinearQuadrangleQ2,
                                          m_boundaryGeoQuadrangleQ2,nullptr,refElementQuadrangleQ2);


  /************************************************************************
   *   geoElementTetrahedronP1
   *
   *           3
   *          /.\
   *         / . \
   *        /  2  \
   *       / .  .  \
   *      /.      . \
   *     0-----------1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoTetrahedronP1[4] = {
    &geoElementTriangleP1,&geoElementTriangleP1,&geoElementTriangleP1,&geoElementTriangleP1
  };
  static const GeoElement* m_boundaryBoundaryGeoTetrahedronP1[6] = {
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1
  };
  static const int m_ptOfEdLinearTetra[] = {0,1,  1,2,  2,0,  0,3,  1,3,  2,3};
  static const int m_ptOfFaLinearTetra[] = {0,2,1,  0,1,3,  1,2,3,  0,3,2};
  static const int m_edOfFaLinearTetra[] = {2,1,0,  0,4,3,  1,5,4,  3,5,2};
  static const bool m_orientEdLinearTetra[] = {false,false,false,  true,true,false,  true,true,false,  true,false,true};
  static const double refcoor_P1_3D[] = {0.,0.,0.,  1.,0.,0.,  0.,1.,0.,  0.,0.,1.};

  const GeoElement geoElementTetrahedronP1("geoElementTetrahedronP1",TETRAHEDRON,basisFunction3dP1,refcoor_P1_3D,6,4,
      m_ptOfEdLinearTetra,m_ptOfFaLinearTetra,m_edOfFaLinearTetra,m_orientEdLinearTetra,
      m_boundaryGeoTetrahedronP1,m_boundaryBoundaryGeoTetrahedronP1,refElementTetrahedronP1);


  /************************************************************************
  *   geoElementTetrahedronP1b
  *
  *           3
  *          /.\
  *         / . \
  *        /  2  \
  *       / . 4.  \
  *      /.      . \
  *     0-----------1
  *
  *************************************************************************/
  static const double refcoor_P1b_3D[] = {0.,0.,0.,  1.,0.,0.,  0.,1.,0.,  0.,0.,1.,  0.25,0.25,0.25};

  const GeoElement geoElementTetrahedronP1b("geoElementTetrahedronP1b",TETRAHEDRON,basisFunction3dP1b,refcoor_P1b_3D,6,4,
      m_ptOfEdLinearTetra,m_ptOfFaLinearTetra,m_edOfFaLinearTetra,m_orientEdLinearTetra,
      m_boundaryGeoTetrahedronP1,m_boundaryBoundaryGeoTetrahedronP1,refElementTetrahedronP1b);


  /************************************************************************
   *   geoElementTetrahedronP2
   *
   *           3
   *          /.\
   *         / 9 \
   *        7  .  8
   *       / . 2 . \
   *      / 6     5 \
   *     0-----4-----1
   *
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoTetrahedronP2[4] = {
    &geoElementTriangleP2,&geoElementTriangleP2,&geoElementTriangleP2,&geoElementTriangleP2
  };
  static const GeoElement* m_boundaryBoundaryGeoTetrahedronP2[6] = {
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2
  };
  static const int m_ptOfEdLinearTetraP2[] = {0,1,4,  1,2,5,  2,0,6,  0,3,7,  1,3,8,  2,3,9};
  static const int m_ptOfFaLinearTetraP2[] = {0,2,1,6,5,4,  0,1,3,4,7,8,  1,2,3,5,9,8,  0,3,2,7,9,6};
  static const int m_edOfFaLinearTetraP2[] = {2,1,0,  0,4,3,  1,5,4,  3,5,2};
  static const bool m_orientEdLinearTetraP2[] = {false,false,false,  true,true,false,  true,true,false,  true,false,true};
  static const double refcoor_P2_3D[] = {0.,0.,0.,  1.,0.,0.,  0.,1.,0.,  0.,0.,1.,  0.5,0.,0.,  0.5,0.5,0.,  0.,0.5,0., 0.,0.,0.5,  0.5,0.,0.5,  0.,0.5,0.5};

  const GeoElement geoElementTetrahedronP2("geoElementTetrahedronP2",TETRAHEDRON,basisFunction3dP2,refcoor_P2_3D,6,4,
      m_ptOfEdLinearTetraP2,m_ptOfFaLinearTetraP2,m_edOfFaLinearTetraP2,m_orientEdLinearTetraP2,
      m_boundaryGeoTetrahedronP2,m_boundaryBoundaryGeoTetrahedronP2,refElementTetrahedronP2);


  /************************************************************************
   *  geoElementHexahedronQ1
   *
   *       7--------6
   *      /.       /|
   *     / .      / |
   *    4________5  |
   *   	|  .     |  |
   *   	|  3.....|..2
   *   	| .      | /
   *   	|.       |/
   *   	0________1
   *
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoHexahedronQ1[6] = {
    &geoElementQuadrangleQ1,&geoElementQuadrangleQ1,&geoElementQuadrangleQ1,&geoElementQuadrangleQ1,&geoElementQuadrangleQ1,&geoElementQuadrangleQ1
  };
  static const GeoElement* m_boundaryBoundaryGeoHexahedronQ1[12] = {
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1
  };
  static const int m_ptOfEdLinearHexa[] = {0,1,  1,2,  2,3,  3,0,  0,4,  1,5,  2,6,  3,7,  4,5,  5,6,  6,7,  7,4};
  static const int m_ptOfFaLinearHexa[] = {0,3,2,1,  0,4,7,3,  0,1,5,4,  4,5,6,7,  1,2,6,5,  2,3,7,6};
  static const int m_edOfFaLinearHexa[] = {3,2,1,0,  4,11,7,3,  0,5,8,4,  8,9,10,11,  1,6,9,5,  2,7,10,6};
  static const bool m_orientEdLinearHexa[] = {false,false,false,false,  true,false,false,true,  true,true,false,false,  true,true,true,true,  true,true,false,false,  true,true,false,false};
  static const double refcoor_Q1_3D[] = {-1.,-1.,-1.,  1.,-1.,-1.,  1.,1.,-1.,  -1.,1.,-1.,  -1.,-1.,1.,  1.,-1.,1.,  1.,1.,1.,  -1.,1.,1.};


  const GeoElement geoElementHexahedronQ1("geoElementHexahedronQ1",HEXAHEDRON,basisFunction3dQ1,refcoor_Q1_3D,12,6,
                                          m_ptOfEdLinearHexa,m_ptOfFaLinearHexa,m_edOfFaLinearHexa,m_orientEdLinearHexa,
                                          m_boundaryGeoHexahedronQ1,m_boundaryBoundaryGeoHexahedronQ1,refElementHexahedronQ1);


  /************************************************************************
     *  geoElementHexahedronQ1b
     *
     *       7--------6
     *      /.       /|
     *     / .      / |
     *    4________5  |
     *    |  .  8  |  |
     *    |  3.....|..2
     *    | .      | /
     *    |.       |/
     *    0________1
     *
     *
     *************************************************************************/
  static const double refcoor_Q1b_3D[] = {-1.,-1.,-1.,  1.,-1.,-1.,  1.,1.,-1.,  -1.,1.,-1.,  -1.,-1.,1.,  1.,-1.,1.,  1.,1.,1.,  -1.,1.,1.,  0.,0.,0.};

  const GeoElement geoElementHexahedronQ1b("geoElementHexahedronQ1b",HEXAHEDRON,basisFunction3dQ1b,refcoor_Q1b_3D,12,6,
      m_ptOfEdLinearHexa,m_ptOfFaLinearHexa,m_edOfFaLinearHexa,m_orientEdLinearHexa,
      m_boundaryGeoHexahedronQ1,m_boundaryBoundaryGeoHexahedronQ1,refElementHexahedronQ1b);


  /************************************************************************
   *  geoElementHexahedronQ2
   *
   *        7---18---6
   *       / .       /|
   *      19 .     17 |
   *     /   15    /  14
   *    4____16___5   |
   *    |    .    |   |
   *    |    3..10|.. 2
   *    12  .     13 /
   *    | 11      | 9
   *    |.        |/
   *    0____8____1
   *
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoHexahedronQ2[6] = {
    &geoElementQuadrangleQ2,&geoElementQuadrangleQ2,&geoElementQuadrangleQ2,&geoElementQuadrangleQ2,&geoElementQuadrangleQ2,&geoElementQuadrangleQ2
  };

  static const GeoElement* m_boundaryBoundaryGeoHexahedronQ2[12] = {
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2
  };

  static const int m_ptOfEdLinearHexaQ2[] = {0,1,8,  1,2,9,  2,3,10,  3,0,11,  0,4,12,  1,5,13,  2,6,14,  3,7,15,  4,5,16,  5,6,17,  6,7,18,  7,4,19};
  static const int m_ptOfFaLinearHexaQ2[] = {0,3,2,1,11,10,9,8,  0,4,7,3,12,19,15,11,  0,1,5,4,8,13,16,12,  4,5,6,7,16,17,18,19,  1,2,6,5,9,14,17,13,  2,3,7,6,10,15,18,14};
  static const int m_edOfFaLinearHexaQ2[] = {3,2,1,0,  4,11,7,3,  0,5,7,4,  8,9,10,11,  1,6,9,5,  2,7,10,6};
  static const bool m_orientEdLinearHexaQ2[] = {false,false,false,false,  true,false,false,true,  true,true,false,false,  true,true,true,true,  true,true,false,false,  true,true,false,false};

  const GeoElement geoElementHexahedronQ2("geoElementHexahedronQ2",HEXAHEDRON,basisFunction3dQ2,refcoor_Q2_3D,12,6,
                                          m_ptOfEdLinearHexaQ2,m_ptOfFaLinearHexaQ2,m_edOfFaLinearHexaQ2,m_orientEdLinearHexaQ2,
                                          m_boundaryGeoHexahedronQ2, m_boundaryBoundaryGeoHexahedronQ2,refElementHexahedronQ2);


  /************************************************************************
   *  geoElementHexahedronQ2c
   *
   *        7---18---6
   *       /.        /|
   *      19 . 23  17 |
   *     /  15  25 /  14
   *    4____16___5   |
   *    |21 .     | 24|
   *    |   3..10.|.. 2
   *    12  . 22  13 /
   *    | 11  20  | 9
   *    |.        |/
   *    0____8____1
   *
   *     + 26 in the middle of the cube (not displayed)
   *************************************************************************/
  static const GeoElement* m_boundaryGeoHexahedronQ2c[6] = {
    &geoElementQuadrangleQ2c,&geoElementQuadrangleQ2c,&geoElementQuadrangleQ2c,&geoElementQuadrangleQ2c,&geoElementQuadrangleQ2c,&geoElementQuadrangleQ2c
  };

  static const GeoElement* m_boundaryBoundaryGeoHexahedronQ2c[12] = {
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2
  };

  static const int m_ptOfEdLinearHexaQ2c[] = {0,1,8,  1,2,9,  2,3,10,  3,0,11,  0,4,12,  1,5,13,  2,6,14,  3,7,15,  4,5,16,  5,6,17,  6,7,18,  7,4,19};
  static const int m_ptOfFaLinearHexaQ2c[] = {0,3,2,1,11,10,9,8,20,  0,4,7,3,12,19,15,11,21,  0,1,5,4,8,13,16,12,22,  4,5,6,7,16,17,18,19,23,  1,2,6,5,9,14,17,13,24,  2,3,7,6,10,15,18,14,25};
  static const int m_edOfFaLinearHexaQ2c[] = {3,2,1,0,  4,11,7,3,  0,5,7,4,  8,9,10,11,  1,6,9,5,  2,7,10,6};
  static const bool m_orientEdLinearHexaQ2c[] = {false,false,false,false,  true,false,false,true,  true,true,false,false,  true,true,true,true,  true,true,false,false,  true,true,false,false} ;

  const GeoElement geoElementHexahedronQ2c("geoElementHexahedronQ2c",HEXAHEDRON,basisFunction3dQ2c,refcoor_Q2c_3D,12,6,
      m_ptOfEdLinearHexaQ2c,m_ptOfFaLinearHexaQ2c,m_edOfFaLinearHexaQ2c,m_orientEdLinearHexaQ2c,
      m_boundaryGeoHexahedronQ2c, m_boundaryBoundaryGeoHexahedronQ2c,refElementHexahedronQ2c);


  /************************************************************************
   *  geoElementPrismR1
   *
   *        5
   *      / . \
   *     /  .  \
   *    3-------4
   *    |   .   |
   *    |   .   |
   *    |  .2.  |
   *    | .   . |
   *    |.     .|
   *    0-------1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoPrismR1[5] = {
    &geoElementTriangleP1,&geoElementQuadrangleQ1,&geoElementQuadrangleQ1,&geoElementTriangleP1,&geoElementQuadrangleQ1
  };
  static const GeoElement* m_boundaryBoundaryGeoPrismR1[9] = {
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1
  };
  static const int m_ptOfEdLinearPrism[] = {0,1,  1,2,  2,0,  0,3,  1,4,  2,5,  3,4,  4,5,  5,3};
  static const int m_ptOfFaLinearPrism[] = {0,2,1,  0,3,5,2,  0,1,4,3, 3,4,5, 1,2,5,4};
  static const int m_edOfFaLinearPrism[] = {2,1,0,  3,8,5,2,  0,4,6,3,  6,7,8,  1,5,7,4};
  static const bool m_orientEdLinearPrism[] = {false,false,false,  true,false,false,true,  true,true,false,false, true,true,true,  true,true,false,false} ;


  const GeoElement geoElementPrismR1("geoElementPrismR1",PRISM,basisFunction3dR1,refcoor_R1_3D,9,5,
                                     m_ptOfEdLinearPrism,m_ptOfFaLinearPrism,m_edOfFaLinearPrism,m_orientEdLinearPrism,
                                     m_boundaryGeoPrismR1,m_boundaryBoundaryGeoPrismR1,refElementPrismR1);


  /************************************************************************
   *  geoElementPrism (P1xP2) //we use, as geometric element, the R1
   *
   *        5
   *      / . \
   *     /  .  \
   *    3-------4
   *    |   .8  |
   *    |   .   |
   *   6|  .2.  |7
   *    | .   . |
   *    |.     .|
   *    0-------1
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoPrismP1xP2[5] = {
    &geoElementTriangleP1,&geoElementQuadrangleP1xP2,&geoElementQuadrangleP1xP2,&geoElementTriangleP1,&geoElementQuadrangleP1xP2
  };
  static const GeoElement* m_boundaryBoundaryGeoPrismP1xP2[9] = {
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,
    &geoElementSegmentP1,&geoElementSegmentP1,&geoElementSegmentP1
  };
  static const int m_ptOfEdPrismP1xP2[] = {0,1,  1,2,  2,0,  0,3,6,  1,4,7,  2,5,8,  3,4,  4,5,  5,3};
  static const int m_ptOfFaPrismP1xP2[] = {0,2,1,  0,3,5,2,6,8,  0,1,4,3,6,7,  3,4,5, 1,2,5,4,7,8};
  static const int m_edOfFaPrismP1xP2[] = {2,1,0,  3,8,5,2,  0,4,6,3,  6,7,8,  1,5,7,4};
  static const bool m_orientEdPrismP1xP2[] = {false,false,false,  true,false,false,true,  true,true,false,false, true,true,true,  true,true,false,false} ;


  const GeoElement geoElementPrismP1xP2("geoElementPrismP1xP2",PRISM,basisFunction3dP1xP2,refcoor_P1xP2_3D,9,5,
                                     m_ptOfEdPrismP1xP2,m_ptOfFaPrismP1xP2,m_edOfFaPrismP1xP2,m_orientEdPrismP1xP2,
                                     m_boundaryGeoPrismP1xP2,m_boundaryBoundaryGeoPrismP1xP2,refElementPrismP1xP2);


  /************************************************************************
   *  geoElementPrismR2
   *
   *        5
   *     11/.\10
   *      / . \
   *    3---9---4
   *    |   14  |
   *    12   .  13
   *    |  .2.  |
   *    | 8   7 |
   *    |.     .|
   *    0---6---1
   *
   *
   *************************************************************************/
  static const GeoElement* m_boundaryGeoPrismR2[5] = {
    &geoElementTriangleP2,&geoElementQuadrangleQ2,&geoElementQuadrangleQ2,&geoElementTriangleP2,&geoElementQuadrangleQ2
  };
  static const GeoElement* m_boundaryBoundaryGeoPrismR2[9] = {
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2,
    &geoElementSegmentP2,&geoElementSegmentP2,&geoElementSegmentP2
  };

  // TODO: the middle points are NOT numbered as the edges!
  // reordering is imperative! VM+JF 07/2011

  static const int m_ptOfEdLinearPrismR2[] = {0,1,6,  1,2,7,  2,0,8,  0,3,12,  1,4,13,  2,5,14,  3,4,9,  4,5,10,  5,3,11};
  static const int m_ptOfFaLinearPrismR2[] = {0,2,1,6,7,8,  0,3,5,2,12,11,14,8,  0,1,4,3,6,13,9,12,  3,4,5,9,10,11,  1,2,5,4,7,14,10,13};
  static const int m_edOfFaLinearPrismR2[] = {2,1,0,  3,8,5,2,  0,4,6,3,  6,7,8,  1,5,7,4};
  static const bool m_orientEdLinearPrismR2[] = {false,false,false,  true,false,false,true,  true,true,false,false, true,true,true,  true,true,false,false} ;


  const GeoElement geoElementPrismR2("geoElementPrismR2",PRISM,basisFunction3dR2,refcoor_R2_3D,9,5,
                                     m_ptOfEdLinearPrismR2,m_ptOfFaLinearPrismR2,m_edOfFaLinearPrismR2,m_orientEdLinearPrismR2,
                                     m_boundaryGeoPrismR2,m_boundaryBoundaryGeoPrismR2,refElementPrismR2);

  /*========================================================================
    !
    !                           REFERENCE ELEMENT
    !
    =======================================================================*/

  // Remarks: When specifying the dofSupportNode, the DOF_NODE_VERTEX should always be the first ones!


  /************************************************************************
   *   RefElementNULL
   *
   *     VOID
   *
   *************************************************************************/

  //const RefElement refElementNULL("refElementNULL",NULLSHAPE,basisFunctionNULL,NULL,
  //                              NULL,NULL,NULL,NULL,m_nodeSegmentP1,0,0,0,0,NULL,NULL);


  /************************************************************************
   *   RefElementNode
   *
   *     0d
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeNode[1] = {
    DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportNode[1] = {
    DOF_NODE_VERTEX
  };

  static const int m_dofIdNode[1] = {
    0
  };

  static const Point m_nodeNode[1] = {
    Point(0.,0.,0.)
  };

  const RefElement refElementNode("refElementNode",NODE,basisFunction0d,listQuadratureRuleNode,
                                  m_dofTypeNode,m_dofSupportNode,m_dofIdNode,m_nodeNode,1,0,0,0,nullptr,nullptr);

  /************************************************************************
   *   RefElementSegmentP1
   *
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeSegmentP1[2] = {
    DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportSegmentP1[2] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdSegmentP1[3] = {
    0,1
  };

  static const Point m_nodeSegmentP1[] = {
    Point(-1.,0.,0.),Point(1.,0.,0.)
  };

  const RefElement refElementSegmentP1("refElementSegmentP1",SEGMENT,basisFunction1dP1,listQuadratureRuleSegment,
                                       m_dofTypeSegmentP1,m_dofSupportSegmentP1,m_dofIdSegmentP1,m_nodeSegmentP1,2,0,0,0,nullptr,nullptr);

  /************************************************************************
   *   RefElementSegmentP1b
   *
   *     0-----2-----1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeSegmentP1b[3] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportSegmentP1b[3] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_EDGE
  };

  static const int m_dofIdSegmentP1b[3] = {
    0,1,
    0
  };

  static const Point m_nodeSegmentP1b[] = {
    Point(-1.,0.,0.),Point(1.,0.,0.),Point(0.,0.,0.)
  };

  const RefElement refElementSegmentP1b("refElementSegmentP1b",SEGMENT,basisFunction1dP1b,listQuadratureRuleSegment,
                                        m_dofTypeSegmentP1b,m_dofSupportSegmentP1b,m_dofIdSegmentP1b,m_nodeSegmentP1b,2,1,0,0,nullptr,nullptr);


  /************************************************************************
   *   RefElementSegmentP2
   *
   *     0-----2-----1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeSegmentP2[3] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportSegmentP2[3] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_EDGE
  };

  static const int m_dofIdSegmentP2[3] = {
    0,1,
    0
  };

  static const Point m_nodeSegmentP2[] = {
    Point(-1.,0.,0.),Point(1.,0.,0.),Point(0.,0.,0.)
  };

  const RefElement refElementSegmentP2("refElementSegmentP2",SEGMENT,basisFunction1dP2,listQuadratureRuleSegment,
                                       m_dofTypeSegmentP2,m_dofSupportSegmentP2,m_dofIdSegmentP2,m_nodeSegmentP2,2,1,0,0,
                                       nullptr,nullptr);

  /************************************************************************
   *   RefElementSegmentP3H
   *
   *     0-1 ---------- 2-3
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeSegmentP3H[4] = {
    DOF_VALUE,DOF_X_DERIVATIVE,DOF_VALUE,DOF_X_DERIVATIVE
  };

  static const DegreeOfFreedomSupport m_dofSupportSegmentP3H[4] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdSegmentP3H[4] = {
    0,0,1,1
  };

  static const Point m_nodeSegmentP3H[] = {
    Point(-1.,0.,0.),Point(-1.,0.,0.),Point(1.,0.,0.),Point(1.,0.,0.)
  };

  const RefElement refElementSegmentP3H("refElementSegmentP3H",SEGMENT,basisFunction1dP3H,listQuadratureRuleSegment,
                                        m_dofTypeSegmentP3H,m_dofSupportSegmentP3H,m_dofIdSegmentP3H,m_nodeSegmentP3H,4,0,0,0,
                                        nullptr,nullptr);

  /************************************************************************
   *  RefElementTriangleP1
   *
   *
   *     2
   *     | \
   *     |   \
   *     |     \
   *     |       \
   *     |         \
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTriangleP1[3] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportTriangleP1[3] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdTriangleP1[3] = {
    0,1,2
  };

  static const Point m_nodeTriangleP1[3] = {
    Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.)
  };

  static const RefElement* m_boundaryTriangleP1[3] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementTriangleP1("refElementTriangleP1",TRIANGLE,basisFunction2dP1,listQuadratureRuleTriangle,
                                        m_dofTypeTriangleP1,m_dofSupportTriangleP1,m_dofIdTriangleP1,m_nodeTriangleP1,3,0,0,0,
                                        m_boundaryTriangleP1,nullptr);

  /************************************************************************
   *  RefElementTriangleP1b
   *
   *
   *     2
   *     | \
   *     |   \
   *     |     \
   *     |   3   \
   *     |         \
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTriangleP1b[4] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportTriangleP1b[4] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_FACE
  };

  static const int m_dofIdTriangleP1b[4] = {
    0,1,2,
    0
  };

  static const Point m_nodeTriangleP1b[4] = {
    Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(1./3.,1./3.,0.)
  };

  static const RefElement* m_boundaryTriangleP1b[3] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementTriangleP1b("refElementTriangleP1b",TRIANGLE,basisFunction2dP1b,listQuadratureRuleTriangle,
                                         m_dofTypeTriangleP1b,m_dofSupportTriangleP1b,m_dofIdTriangleP1b,m_nodeTriangleP1b,3,1,0,0,
                                         m_boundaryTriangleP1b,nullptr);

  /************************************************************************
   *  RefElementTriangleP2
   *
   *     2
   *     | \
   *     |   \
   *     5     4
   *     |       \
   *     |         \
   *     0-----3----1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTriangleP2[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportTriangleP2[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE
  };

  static const int m_dofIdTriangleP2[] = {
    0,1,2, // vertices
    0,1,2 // edges
  };

  static const Point m_nodeTriangleP2[] = {
    Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(0.5,0.,0.),Point(0.5,0.5,0.),Point(0.,0.5,0.)
  };

  static const RefElement* m_boundaryTriangleP2[] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementTriangleP2("refElementTriangleP2",TRIANGLE,basisFunction2dP2,listQuadratureRuleTriangle,
                                        m_dofTypeTriangleP2,m_dofSupportTriangleP2,m_dofIdTriangleP2,m_nodeTriangleP2,3,3,0,0,
                                        m_boundaryTriangleP2,nullptr);

  /************************************************************************
   *  RefElementQuadrangleQ1
   *
   *     3-----------2
   *     |           |
   *     |           |
   *     |           |
   *     |           |
   *     |           |
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeQuadrangleQ1[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportQuadrangleQ1[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdQuadrangleQ1[] = {
    0,1,2,3
  };

  static const Point m_nodeQuadrangleQ1[] = {
    Point(-1.,-1.,0.),Point(1.,-1.,0.),Point(1.,1.,0.),Point(-1.,1.,0.)
  };

  static const RefElement* m_boundaryQuadrangleQ1[] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementQuadrangleQ1("refElementQuadrangleQ1",QUADRILATERAL,basisFunction2dQ1,listQuadratureRuleQuadrilateral,
                                          m_dofTypeQuadrangleQ1,m_dofSupportQuadrangleQ1,m_dofIdQuadrangleQ1,m_nodeQuadrangleQ1,4,0,0,0,
                                          m_boundaryQuadrangleQ1,nullptr);

  /************************************************************************
   *  RefElementQuadrangleP1xP2
   *
   *     3-----------2
   *     |           |
   *     |           |
   *    4|           |5
   *     |           |
   *     |           |
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeQuadrangleP1xP2[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportQuadrangleP1xP2[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_EDGE,DOF_NODE_EDGE
  };

  static const int m_dofIdQuadrangleP1xP2[] = {
    0,1,2,3, //id of the vertex
    1,3 //id of the edges
  };

  static const Point m_nodeQuadrangleP1xP2[] = {
    Point(-1.,-1.,0.),Point(1.,-1.,0.),Point(1.,1.,0.),Point(-1.,1.,0.),Point(1.,0.,0.),Point(-1.,0.,0.)
  };

  static const RefElement* m_boundaryQuadrangleP1xP2[] = {
    &refElementSegmentP1,&refElementSegmentP2,&refElementSegmentP1,&refElementSegmentP2
  };

  const RefElement refElementQuadrangleP1xP2("refElementQuadrangleP1xP2",QUADRILATERAL,basisFunction2dP1xP2,listQuadratureRuleQuadrilateral,
                                          m_dofTypeQuadrangleP1xP2,m_dofSupportQuadrangleP1xP2,m_dofIdQuadrangleP1xP2,m_nodeQuadrangleP1xP2,4,2,0,0,
                                          m_boundaryQuadrangleP1xP2,nullptr,true);

  /************************************************************************
   *  RefElementQuadrangleQ1b
   *
   *     3-----------2
   *     |           |
   *     |           |
   *     |     4     |
   *     |           |
   *     |           |
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeQuadrangleQ1b[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportQuadrangleQ1b[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_FACE
  };

  static const int m_dofIdQuadrangleQ1b[] = {
    0,1,2,3,
    0
  };

  static const Point m_nodeQuadrangleQ1b[] = {
    Point(-1.,-1.,0.),Point(1.,-1.,0.),Point(1.,1.,0.),Point(-1.,1.,0.),Point(0.,0.,0.)
  };

  static const RefElement* m_boundaryQuadrangleQ1b[] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementQuadrangleQ1b("refElementQuadrangleQ1b",QUADRILATERAL,basisFunction2dQ1b,listQuadratureRuleQuadrilateral,
      m_dofTypeQuadrangleQ1b,m_dofSupportQuadrangleQ1b,m_dofIdQuadrangleQ1b,m_nodeQuadrangleQ1b,4,1,0,0,
      m_boundaryQuadrangleQ1b,nullptr);

  /************************************************************************
   *  RefElementQuadrangleQ2
   *
   *     3-----6-----2
   *     |           |
   *     |           |
   *     7           5
   *     |           |
   *     |           |
   *     0-----4-----1
   *
   *************************************************************************/
  static const DegreeOfFreedomType m_dofTypeQuadrangleQ2[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportQuadrangleQ2[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE
  };

  static const int m_dofIdQuadrangleQ2[] = {
    0,1,2,3,
    0,1,2,3
  };

  static const Point m_nodeQuadrangleQ2[] = {
    Point(-1.,-1.,0.),Point(1.,-1.,0.),Point(1.,1.,0.),Point(-1.,1.,0.),
    Point(0.,-1.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(-1.,0.,0.)
  };

  static const RefElement* m_boundaryQuadrangleQ2[] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementQuadrangleQ2("refElementQuadrangleQ2",QUADRILATERAL,basisFunction2dQ2,listQuadratureRuleQuadrilateral,
                                          m_dofTypeQuadrangleQ2,m_dofSupportQuadrangleQ2,m_dofIdQuadrangleQ2,
                                          m_nodeQuadrangleQ2,4,4,0,0,
                                          m_boundaryQuadrangleQ2,nullptr);

  /************************************************************************
   *  RefElementQuadrangleQ2c
   *
   *     3-----6-----2
   *     |           |
   *     |           |
   *     7     8     5
   *     |           |
   *     |           |
   *     0-----4-----1
   *
   *************************************************************************/
  static const DegreeOfFreedomType m_dofTypeQuadrangleQ2c[9] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportQuadrangleQ2c[9] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_FACE
  };

  static const int m_dofIdQuadrangleQ2c[] = {
    0,1,2,3, // vertices
    0,1,2,3, // edges
    0 // face
  };

  static const Point m_nodeQuadrangleQ2c[] = {
    Point(-1.,-1.,0.),Point(1.,-1.,0.),Point(1.,1.,0.),Point(-1.,1.,0.),
    Point(0.,-1.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(-1.,0.,0.),
    Point(0.,0.,0.)
  };

  static const RefElement* m_boundaryQuadrangleQ2c[4] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementQuadrangleQ2c("refElementQuadrangleQ2c",QUADRILATERAL,basisFunction2dQ2c,listQuadratureRuleQuadrilateral,
      m_dofTypeQuadrangleQ2c,m_dofSupportQuadrangleQ2c,m_dofIdQuadrangleQ2c,
      m_nodeQuadrangleQ2c,4,4,1,0,
      m_boundaryQuadrangleQ2c,nullptr);

  /************************************************************************
   *   RefElementTetrahedronP1
   *
   *           3
   *          /.\
   *         / . \
   *        /  2  \
   *       / .  .  \
   *      /.      . \
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTetrahedronP1[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportTetrahedronP1[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdTetrahedronP1[] = {
    0,1,2,3
  };

  static const Point m_nodeTetrahedronP1[] = {
    Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(0.,0.,1.)
  };

  static const RefElement* m_boundaryTetrahedronP1[] = {
    &refElementTriangleP1,&refElementTriangleP1,&refElementTriangleP1,&refElementTriangleP1
  };

  static const RefElement* m_boundaryBoundaryTetrahedronP1[] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementTetrahedronP1("refElementTetrahedronP1",TETRAHEDRON,basisFunction3dP1,listQuadratureRuleTetrahedron,
      m_dofTypeTetrahedronP1,m_dofSupportTetrahedronP1,m_dofIdTetrahedronP1,
      m_nodeTetrahedronP1,4,0,0,0,
      m_boundaryTetrahedronP1,m_boundaryBoundaryTetrahedronP1);

  /************************************************************************
   *   RefElementTetrahedronP1b
   *
   *           3
   *          /.\
   *         / . \
   *        /  2  \
   *       / . 4.  \
   *      /.      . \
   *     0-----------1
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTetrahedronP1b[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportTetrahedronP1b[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VOLUME
  };

  static const int m_dofIdTetrahedronP1b[] = {
    0,1,2,3,
    0
  };

  static const Point m_nodeTetrahedronP1b[] = {
    Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(0.,0.,1.),Point(0.25,0.25,0.25)
  };

  static const RefElement* m_boundaryTetrahedronP1b[] = {
    &refElementTriangleP1,&refElementTriangleP1,&refElementTriangleP1,&refElementTriangleP1
  };

  static const RefElement* m_boundaryBoundaryTetrahedronP1b[] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementTetrahedronP1b("refElementTetrahedronP1b",TETRAHEDRON,basisFunction3dP1b,listQuadratureRuleTetrahedron,
      m_dofTypeTetrahedronP1b,m_dofSupportTetrahedronP1b,m_dofIdTetrahedronP1b,
      m_nodeTetrahedronP1b,4,1,0,0,
      m_boundaryTetrahedronP1b,m_boundaryBoundaryTetrahedronP1b);

  /************************************************************************
   *   RefTetrahedronP2
   *
   *           3
   *          /9\
   *         / . \
   *        7  2  8
   *       / .  .  \
   *      / 6     5 \
   *     0-----4-----1
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTetrahedronP2[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportTetrahedronP2[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,
    DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE
  };

  static const int m_dofIdTetrahedronP2[] = {
    0,1,2,3,
    0,1,2,3,4,5
  };

  static const Point m_nodeTetrahedronP2[] = {
    Point(0.,0.,0.),Point(1.,0.,0.),Point(0.,1.,0.),Point(0.,0.,1.),
    Point(0.5,0.,0.),  Point(0.5,0.5,0.),  Point(0.,0.5,0.), Point(0.,0.,0.5),  Point(0.5,0.,0.5),  Point(0.,0.5,0.5)
  };


  static const RefElement* m_boundaryTetrahedronP2[] = {
    &refElementTriangleP2,&refElementTriangleP2,&refElementTriangleP2,&refElementTriangleP2
  };

  static const RefElement* m_boundaryBoundaryTetrahedronP2[6] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementTetrahedronP2("refElementTetrahedronP2",TETRAHEDRON,basisFunction3dP2,listQuadratureRuleTetrahedron,
      m_dofTypeTetrahedronP2,m_dofSupportTetrahedronP2,m_dofIdTetrahedronP2,
      m_nodeTetrahedronP2,4,6,0,0,
      m_boundaryTetrahedronP2,m_boundaryBoundaryTetrahedronP2);

  /************************************************************************
   *  RefElementHexahedronQ1
   *
   *       7--------6
   *      /.       /|
   *     / .      / |
   *    4________5  |
   *    |  .     |  |
   *    |  3.....|..2
   *    | .      | /
   *    |.       |/
   *    0________1
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeHexahedronQ1[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportHexahedronQ1[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdHexahedronQ1[8] = {
    0,1,2,3,4,5,6,7
  };

  static const Point m_nodeHexahedronQ1[8] = {
    Point(-1.,-1.,-1.),  Point(1.,-1.,-1.),  Point(1.,1.,-1.),  Point(-1.,1.,-1.), Point(-1.,-1.,1.),  Point(1.,-1.,1.),  Point(1.,1.,1.), Point(-1.,1.,1.)
  };

  static const RefElement* m_boundaryHexahedronQ1[6] = {
    &refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1
  };

  static const RefElement* m_boundaryBoundaryHexahedronQ1[12] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementHexahedronQ1("refElementHexahedronQ1",HEXAHEDRON,basisFunction3dQ1,listQuadratureRuleHexahedron,
                                          m_dofTypeHexahedronQ1,m_dofSupportHexahedronQ1,m_dofIdHexahedronQ1,
                                          m_nodeHexahedronQ1,8,0,0,0,
                                          m_boundaryHexahedronQ1,m_boundaryBoundaryHexahedronQ1);

  /************************************************************************
  *  RefElementHexahedronQ1b
  *
  *       7--------6
  *      /.       /|
  *     / .      / |
  *    4________5  |
  *    |  .  8  |  |
  *    |  3.....|..2
  *    | .      | /
  *    |.       |/
  *    0________1
  *
  *
  *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeHexahedronQ1b[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportHexahedronQ1b[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VOLUME
  };

  static const int m_dofIdHexahedronQ1b[9] = {
    0,1,2,3,4,5,6,7,
    0
  };

  static const Point m_nodeHexahedronQ1b[9] = {
    Point(-1.,-1.,-1.),  Point(1.,-1.,-1.),  Point(1.,1.,-1.),  Point(-1.,1.,-1.), Point(-1.,-1.,1.),  Point(1.,-1.,1.),  Point(1.,1.,1.), Point(-1.,1.,1.),Point(0.,0.,0.)
  };

  static const RefElement* m_boundaryHexahedronQ1b[6] = {
    &refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementQuadrangleQ1
  };

  static const RefElement* m_boundaryBoundaryHexahedronQ1b[12] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementHexahedronQ1b("refElementHexahedronQ1b",HEXAHEDRON,basisFunction3dQ1b,listQuadratureRuleHexahedron,
      m_dofTypeHexahedronQ1b,m_dofSupportHexahedronQ1b,m_dofIdHexahedronQ1b,
      m_nodeHexahedronQ1b,8,1,0,0,
      m_boundaryHexahedronQ1b,m_boundaryBoundaryHexahedronQ1b);

  /************************************************************************
   *  RefElementHexahedronQ2
   *
   *       7---18---6
   *      /.        /|
   *     19 .     17 |
   *    /  15     /  14
   *   4____16___5   |
   *   |   .     |   |
   *   |   3..10.|.. 2
   *   12  .     13  /
   *   | 11      | 9
   *   |.        |/
   *   0____8____1
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeHexahedronQ2[20] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportHexahedronQ2[20] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,
    DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,
    DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE
  };

  static const int m_dofIdHexahedronQ2[20] = {
    0,1,2,3,4,5,6,7,
    0,1,2,3,4,5,6,7,8,9,10,11
  };

  static const Point m_nodeHexahedronQ2[20] = {
    Point(-1.,-1.,-1.),  Point(1.,-1.,-1.),  Point(1.,1.,-1.),  Point(-1.,1.,-1.), Point(-1.,-1.,1.),  Point(1.,-1.,1.),  Point(1.,1.,1.), Point(-1.,1.,1.),
    Point(0.,-1.,-1.),  Point(1.,0.,-1.),  Point(0.,1.,-1.),  Point(-1.,0.,-1.),  Point(-1.,-1.,0.),  Point(1.,-1.,0.),  Point(1.,1.,0.),  Point(-1.,1.,0.),
    Point(0.,-1.,1.),  Point(1.,0.,1.),  Point(0.,1.,1.), Point(-1.,0.,1.)
  };

  static const RefElement* m_boundaryHexahedronQ2[6] = {
    &refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2
  };

  static const RefElement* m_boundaryBoundaryHexahedronQ2[12] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementHexahedronQ2("refElementHexahedronQ2",HEXAHEDRON,basisFunction3dQ2,listQuadratureRuleHexahedron,
                                          m_dofTypeHexahedronQ2,m_dofSupportHexahedronQ2,m_dofIdHexahedronQ2,
                                          m_nodeHexahedronQ2,8,12,0,0,m_boundaryHexahedronQ2, m_boundaryBoundaryHexahedronQ2);

  /************************************************************************
   *  geoElementHexahedronQ2c
   *
   *       7---18---6
   *      /.        /|
   *     19 . 24  17 |
   *    /  15  26 /  14
   *   4____16___5   |
   *   | 22 .    | 25|
   *   |   3..10.|.. 2
   *   12  . 23  13  /
   *   | 11  21  | 9
   *   |.        |/
   *   0____8____1
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeHexahedronQ2c[27] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportHexahedronQ2c[27] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,
    DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,
    DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_FACE,DOF_NODE_FACE,DOF_NODE_FACE,DOF_NODE_FACE,DOF_NODE_FACE,DOF_NODE_FACE,
    DOF_NODE_VOLUME
  };

  static const int m_dofIdHexahedronQ2c[27] = {
    0,1,2,3,4,5,6,7,
    0,1,2,3,4,5,6,7,8,9,10,11,
    0,1,2,3,4,5,
    0
  };

  static const Point m_nodeHexahedronQ2c[27] = {
    Point(-1.,-1.,-1.),  Point(1.,-1.,-1.),  Point(1.,1.,-1.),  Point(-1.,1.,-1.), Point(-1.,-1.,1.),  Point(1.,-1.,1.),  Point(1.,1.,1.), Point(-1.,1.,1.),
    Point(0.,-1.,-1.),  Point(1.,0.,-1.),  Point(0.,1.,-1.),  Point(-1.,0.,-1.),  Point(-1.,-1.,0.),  Point(1.,-1.,0.),  Point(1.,1.,0.),  Point(-1.,1.,0.),
    Point(0.,-1.,1.),  Point(1.,0.,1.),  Point(0.,1.,1.), Point(-1.,0.,1.), Point(0.,0.,-1.), Point(-1.,0.,0.), Point(0.,-1.,0.), Point(0.,0.,1.),
    Point(1.,0.,0.), Point(0.,1.,0.), Point(0.,0.,0.)
  };

  static const RefElement* m_boundaryHexahedronQ2c[6] = {
    &refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementQuadrangleQ2
  };

  static const RefElement* m_boundaryBoundaryHexahedronQ2c[12] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementHexahedronQ2c("refElementHexahedronQ2c",HEXAHEDRON,basisFunction3dQ2c,listQuadratureRuleHexahedron,
      m_dofTypeHexahedronQ2c,m_dofSupportHexahedronQ2c,m_dofIdHexahedronQ2c,
      m_nodeHexahedronQ2c,8,12,6,1,m_boundaryHexahedronQ2c, m_boundaryBoundaryHexahedronQ2c);

  /************************************************************************
   *  RefElementPrismR1
   *
   *
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypePrismR1[6] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportPrismR1[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX
  };

  static const int m_dofIdPrismR1[6] = {
    0,1,2,3,4,5
  };

  static const Point m_nodePrismR1[6] = {
    Point(0.,0.,-1.),  Point(1.,0.,-1.),  Point(0.,1.,-1.),  Point(0.,0.,1.),  Point(1.,0.,1.),  Point(0.,1.,1.)
  };

  static const RefElement* m_boundaryPrismR1[5] = {
    &refElementTriangleP1,&refElementQuadrangleQ1,&refElementQuadrangleQ1,&refElementTriangleP1,&refElementQuadrangleQ1
  };

  static const RefElement* m_boundaryBoundaryPrismR1[9] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementPrismR1("refElementPrismR1",PRISM,basisFunction3dR1,listQuadratureRulePrism,
                                     m_dofTypePrismR1,m_dofSupportPrismR1,m_dofIdPrismR1,
                                     m_nodePrismR1,6,0,0,0,
                                     m_boundaryPrismR1,m_boundaryBoundaryPrismR1);

  /************************************************************************
   *  RefElementPrismP1xP2
   *
   *
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypePrismP1xP2[] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE, DOF_VALUE,DOF_VALUE, DOF_VALUE, DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportPrismP1xP2[] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX, DOF_NODE_EDGE, DOF_NODE_EDGE, DOF_NODE_EDGE
  };

  static const int m_dofIdPrismP1xP2[9] = {
    0,1,2,3,4,5, //vertex
    3,4,5//edges
  };

  static const Point m_nodePrismP1xP2[9] = {
    Point(0.,0.,-1.),  Point(1.,0.,-1.),  Point(0.,1.,-1.),  Point(0.,0.,1.),  Point(1.,0.,1.),  Point(0.,1.,1.),  Point(0.,0.,0.),  Point(1.,0.,0.),  Point(0.,1.,0.)
  };

  static const RefElement* m_boundaryPrismP1xP2[] = {
    &refElementTriangleP1,&refElementQuadrangleP1xP2,&refElementQuadrangleP1xP2,&refElementTriangleP1,&refElementQuadrangleP1xP2
  };

  static const RefElement* m_boundaryBoundaryPrismP1xP2[] = {
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP1,&refElementSegmentP1,&refElementSegmentP1
  };

  const RefElement refElementPrismP1xP2("refElementPrismP1xP2",PRISM,basisFunction3dP1xP2,listQuadratureRulePrism,
                                     m_dofTypePrismP1xP2,m_dofSupportPrismP1xP2,m_dofIdPrismP1xP2,
                                     m_nodePrismP1xP2,6,3,0,0,
                                     m_boundaryPrismP1xP2,m_boundaryBoundaryPrismP1xP2,true);

  /************************************************************************
   *  RefElementPrismR2
   *
   *
   *
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypePrismR2[15] = {
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,DOF_VALUE,
    DOF_VALUE,DOF_VALUE,DOF_VALUE
  };

  static const DegreeOfFreedomSupport m_dofSupportPrismR2[15] = {
    DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,DOF_NODE_VERTEX,
    DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,DOF_NODE_EDGE,
    DOF_NODE_EDGE,DOF_NODE_EDGE
  };

  static const int m_dofIdPrismR2[15] = {
    0,1,2,3,4,5,
    0,1,2,3,4,5,6,7,8
  };

  static const Point m_nodePrismR2[15] = {
    Point(0.,0.,-1.),  Point(1.,0.,-1.),  Point(0.,1.,-1.),  Point(0.,0.,1.),  Point(1.,0.,1.),  Point(0.,1.,1.),
    Point(0.5,0.,-1.), Point(0.5,0.5,-1.),  Point(0.,0.5,-1.),  Point(0.5,0.,1.),  Point(0.5,0.5,1.),  Point(0.,0.5,1.),
    Point(0.,0.,0.),  Point(1.,0.,0.),  Point(0.,1.,0.)
  };

  static const RefElement* m_boundaryPrismR2[5] = {
    &refElementTriangleP2,&refElementQuadrangleQ2,&refElementQuadrangleQ2,&refElementTriangleP2,&refElementQuadrangleQ2
  };

  static const RefElement* m_boundaryBoundaryPrismR2[9] = {
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2,
    &refElementSegmentP2,&refElementSegmentP2,&refElementSegmentP2
  };

  const RefElement refElementPrismR2("refElementPrismR2",PRISM,basisFunction3dR2,listQuadratureRulePrism,
                                     m_dofTypePrismR2,m_dofSupportPrismR2,m_dofIdPrismR2,
                                     m_nodePrismR2,6,9,0,0,
                                     m_boundaryPrismR2,m_boundaryBoundaryPrismR2);

  /************************************************************************
   *   RefTetrahedronRT0
   *
   *
   *          /.\
   *         / . \
   *        /2   1\
   *       / . 3 . \
   *      /.   0   .\
   *      -----------
   *
   *************************************************************************/

  static const DegreeOfFreedomType m_dofTypeTetrahedronRT0[] = {
    DOF_FLUX,DOF_FLUX,DOF_FLUX,DOF_FLUX
  };

  static const DegreeOfFreedomSupport m_dofSupportTetrahedronRT0[] = {
    DOF_FACE,DOF_FACE,DOF_FACE,DOF_FACE
  };

  static const int m_dofIdTetrahedronRT0[] = {
    0,1,2,3
  };

  const RefElement refElementTetrahedronRT0("refElementTetrahedronRT0",TETRAHEDRON,basisFunction3dRT0Tetra,listQuadratureRuleTetrahedron,
      m_dofTypeTetrahedronRT0,m_dofSupportTetrahedronRT0,m_dofIdTetrahedronRT0,
      nullptr,0,0,0,0,
      nullptr,nullptr);

  /*const RefElement listRefEle[2] = {
    RefElement(3,0,0,0,basisFunctionP1Tria,&refElementP1Seg),
    RefElement(4,0,0,0,basisFunction3dP1,&refElementP1Tria)
    };*/
}
