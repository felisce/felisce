//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin & Miguel Fernandez
//

// System includes
#include <iomanip>
#include <numeric>

// External includes

// Project includes
#include "Core/felisceTools.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "Geometry/curvatures.hpp"

namespace felisce
{
ElementMatrix::ElementMatrix(const ElementMatrix& rOther)
  : m_mat(rOther.m_mat),
    m_numRow(rOther.m_numRow),
    m_firstRow(rOther.m_firstRow),
    m_numCol(rOther.m_numCol),
    m_firstCol(rOther.m_firstCol),
    m_tmpMat(rOther.m_tmpMat),
    m_tmpVecCoor(rOther.m_tmpVecCoor),
    m_tmpVec2Coor(rOther.m_tmpVec2Coor),
    m_tmpVecDof(rOther.m_tmpVecDof),
    m_tmpVec2Dof(rOther.m_tmpVec2Dof),
    m_tmpVecQuadPoint(rOther.m_tmpVecQuadPoint)
{
}

/***********************************************************************************/
/***********************************************************************************/

ElementMatrix& ElementMatrix::operator=(const ElementMatrix& rOther)
{
  m_mat = rOther.m_mat;
  m_numRow = rOther.m_numRow;
  m_firstRow = rOther.m_firstRow;
  m_numCol =rOther.m_numCol;
  m_firstCol = rOther.m_firstCol;
  m_tmpMat = rOther.m_tmpMat;
  m_tmpVecCoor = rOther.m_tmpVecCoor;
  m_tmpVec2Coor = rOther.m_tmpVec2Coor;
  m_tmpVecDof = rOther.m_tmpVecDof;
  m_tmpVec2Dof = rOther.m_tmpVec2Dof;
  m_tmpVecQuadPoint = rOther.m_tmpVecQuadPoint;
  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::duplicateFrom(const ElementMatrix& elmat)
{
  // Copy values
  *this = elmat;

  // Clear matrices
  m_mat.clear();
  m_tmpMat.clear();
}

/***********************************************************************************/
/***********************************************************************************/

ElementMatrix::ElementMatrix(const std::vector<const CurBaseFiniteElement*>& fe, const std::vector<std::size_t>& nbr, const std::vector<std::size_t>& nbc) :
  ElementMatrix(fe, nbr, fe, nbc)
{

}

/***********************************************************************************/
/***********************************************************************************/

ElementMatrix::ElementMatrix(const std::vector<const CurBaseFiniteElement*>& fer, const std::vector<std::size_t>& nbr, const std::vector<const CurBaseFiniteElement*>& fec,  const std::vector<std::size_t>& nbc) :
  m_mat(    std::inner_product(fer.begin(),fer.end(),nbr.begin(), 0, std::plus<std::size_t>(), [](auto a, auto b){ return a->numDof()*b; }),
            std::inner_product(fec.begin(),fec.end(),nbc.begin(), 0, std::plus<std::size_t>(), [](auto a, auto b){ return a->numDof()*b; }) ),
  m_tmpMat( std::inner_product(fer.begin(),fer.end(),nbr.begin(), 0, std::plus<std::size_t>(), [](auto a, auto b){ return a->numDof()*b; }),
            std::inner_product(fec.begin(),fec.end(),nbc.begin(), 0, std::plus<std::size_t>(), [](auto a, auto b){ return a->numDof()*b; }) )
{
  m_mat.clear();
  m_tmpMat.clear();

  // TODO are the following used whenever there are one or two finite elements??
  // If not would be better allocate the memory using an ad hoc method
  if (fer.size() == 1) {
    m_tmpVecCoor.resize(fer[0]->numCoor());
    m_tmpVecDof.resize(fer[0]->numDof());
    m_tmpVecQuadPoint.resize(fer[0]->numQuadraturePoint());
  } else if (fer.size() == 2) {
    // THIS LOOKS REALLY UGLY!!!!!!!!!!!!!!!! (different from the constructor with curvilinearFE.)
    // CANNOT THIS BE MOVED ELSEWHERE ??? VM 2013/04
    // Needed for the functions a_grad_phi_i_grad_phi_j and a_eps_phi_i_eps_phi_j
    m_tmpVecQuadPoint.resize(fer[1]->numQuadraturePoint());
  }

  //
  std::size_t sumr = std::accumulate(nbr.begin(),nbr.end(),0);
  std::size_t sumc = std::accumulate(nbc.begin(),nbc.end(),0);
  m_numRow.resize(sumr);
  m_firstRow.resize(sumr);
  m_numCol.resize(sumc);
  m_firstCol.resize(sumc);
  //
  std::size_t first = 0;
  std::size_t start = 0;
  std::size_t end   = 0;
  for (std::size_t i=0; i<nbr.size(); i++) {
    end   += nbr[i];
    for (std::size_t n=start; n<end; n++){
      m_numRow[ n ]=fer[i]->numDof();
      m_firstRow[ n ]=first;
      first += fer[i]->numDof();
    }
    start += nbr[i];
  }
  //
  first = 0;
  start = 0;
  end   = 0;
  for (std::size_t i=0; i<nbc.size(); i++) {
    end   += nbc[i];
    for (std::size_t n=start; n<end; n++){
      m_numCol[ n ]=fec[i]->numDof();
      m_firstCol[ n ]=first;
      first += fec[i]->numDof();
    }
    start += nbc[i];
  }
}

/***********************************************************************************/
/***********************************************************************************/

// ElementMatrix::ElementMatrix(const CurBaseFiniteElement& fe, const std::size_t nbr1, const std::size_t nbc1 ) :
//   ElementMatrix(std::vector<const CurBaseFiniteElement*>{&fe},
//                 std::vector<std::size_t>{nbr1},
//                 std::vector<std::size_t>{nbc1})
// {

// }

// /***********************************************************************************/
// /***********************************************************************************/

// ElementMatrix::ElementMatrix(const CurBaseFiniteElement& fe1, const std::size_t nbr1, const std::size_t nbc1,
//                              const CurBaseFiniteElement& fe2, const std::size_t nbr2, const std::size_t nbc2 ) :
//   ElementMatrix(std::vector<const CurBaseFiniteElement*>{&fe1,&fe2},
//                 std::vector<std::size_t>{nbr1,nbr2},
//                 std::vector<std::size_t>{nbc1,nbc2})
// {

// }

// /***********************************************************************************/
// /***********************************************************************************/

// ElementMatrix::ElementMatrix(const CurBaseFiniteElement& fe1, const std::size_t nbr1, const std::size_t nbc1,
//                              const CurBaseFiniteElement& fe2, const std::size_t nbr2, const std::size_t nbc2,
//                              const CurBaseFiniteElement& fe3, const std::size_t nbr3, const std::size_t nbc3 ) :
//   ElementMatrix(std::vector<const CurBaseFiniteElement*>{&fe1,&fe2,&fe3},
//                 std::vector<std::size_t>{nbr1,nbr2,nbr3},
//                 std::vector<std::size_t>{nbc1,nbc2,nbc3})
// {

// }

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::operator+=(const ElementMatrix& rhs)
{
  FEL_ASSERT(m_firstRow == rhs.m_firstRow);
  FEL_ASSERT(m_numCol == rhs.m_numCol);
  FEL_ASSERT(m_firstCol == rhs.m_firstCol);

  m_mat += rhs.m_mat;
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::operator-=(const ElementMatrix& rhs)
{
  FEL_ASSERT(m_firstRow == rhs.m_firstRow);
  FEL_ASSERT(m_numCol == rhs.m_numCol);
  FEL_ASSERT(m_firstCol == rhs.m_firstCol);

  m_mat -= rhs.m_mat;
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::operator*=(const double factor)
{
  m_mat *= factor;
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::print(std::size_t verbose,std::ostream& c) //cannot be "const" because of matrix_range.
{
  if(verbose) {
    for (std::size_t i=0; i<m_firstRow.size(); i++) {
      for (std::size_t j=0; j<m_firstCol.size(); j++) {
        c << "Block (" << i << "," << j << "): ";
        UBlasMatrixRange b(m_mat,UBlasRange( m_firstRow[i], m_firstRow[i]+m_numRow[i]),
                                 UBlasRange( m_firstCol[j], m_firstCol[j]+m_numCol[j]));
        c << std::endl;
        for(unsigned int ir=0; ir<m_numRow[i]; ir++) {
          for(unsigned int ic=0; ic<m_numCol[j]; ic++) {
            c << std::setw(12) << std::setprecision(9) << b(ir,ic) << "   ";
          }
          c<< std::endl;
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_grad_phi_j(const double coef,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0, numQuadPoint = fe.numQuadraturePoint(); ig < numQuadPoint; ig++) {
    tmpBlock += coef * fe.weightMeas(ig) * prod(trans(fe.dPhi[ig]),fe.dPhi[ig]);
  }

  for(std::size_t icoor=0; icoor < num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_grad_phi_j(const double coef,const CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  UBlasMatrix tmpAdPhi(fe.contravariantMetric[0].size1(), fe.dPhiRef(0).size2());
  for(int ig=0, numQuadPoint = fe.numQuadraturePoint(); ig < numQuadPoint; ig++) {
    tmpAdPhi  = prod(fe.contravariantMetric[ig],fe.dPhiRef(ig));
    tmpBlock += coef*fe.weightMeas(ig)*prod(trans(fe.dPhiRef(ig)),tmpAdPhi);
  }

  for(std::size_t icoor=0; icoor < num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::u_grad_phi_j_phi_i(const double coef,const ElementField& vel, const  CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock,jblock);

  const int numDof = fe.numDof();
  const int numRefCoor = fe.numRefCoor();
  const int numCoor = fe.numCoor();
  UBlasVector velContra(numRefCoor);

  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    switch(vel.type()) {
      case CONSTANT_FIELD:
        // nothing to do : the constant field has been computed before the integration loop
        for(int icoor=0; icoor<vel.numComp(); icoor++)
          m_tmpVecCoor(icoor) = vel.val(icoor,0);
        break;
      case DOF_FIELD: {
        m_tmpVecCoor = prod(vel.val,fe.phi[ig]);
        break;
      }
      case QUAD_POINT_FIELD: {
        for(int icoor=0; icoor<vel.numComp(); icoor++)
          m_tmpVecCoor(icoor) = vel.val(icoor,ig);
        break;
    }
    // Default case should appear with a warning at compile time instead of an error in runtime
    // (that's truly the point of using enums as switch cases)
    //  default:
    //          FEL_ERROR("Operator 'u_grad_phi_j_phi_i' not yet implemented with this type of element fields");
    }
    const double weightMas = fe.weightMeas(ig);
    double sum;

    // Compute contravaint components of vel
    for (int icoorRef=0; icoorRef < numRefCoor; icoorRef++){
        sum = 0;
        for (int icoor=0; icoor< numCoor; icoor++)
          sum +=  m_tmpVecCoor(icoor)*fe.contravariantCompleteBasis[ig](icoor,icoorRef);
        velContra(icoorRef) = sum;
    }

    // Compute direction derivative of the ref basis functions
    for(int jdof=0; jdof< numDof; jdof++) {
        sum = 0;
        for (int icoorRef=0; icoorRef < numRefCoor; icoorRef++)
          sum += fe.dPhiRef(ig)(icoorRef,jdof) * velContra(icoorRef) ;
        m_tmpVecDof(jdof) = sum;
    }
    //m_tmpVecDof = prod(trans(fe.dPhi[ig]),m_tmpVecCoor); // m_tmpVecDof = vel \cdot \nabla \phi_i
    for(int idof=0; idof<fe.numDof(); idof++) {
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        tmpblock(idof,jdof) +=  coef * weightMas * m_tmpVecDof(jdof) * fe.phi[ig](idof);
      }
    }
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange block = matBlock(iblock+icoor,jblock+icoor);
    block += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::a_grad_phi_i_grad_phi_j(const double coef,const ElementField& scalfct,const CurrentFiniteElement& fe,const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  switch(scalfct.type()) {
    case CONSTANT_FIELD:
      // nothing to do : the constant field has been computed before the integration loop
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        m_tmpVecQuadPoint(ig) = scalfct.val(0,0);
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(scalfct.val,fe.phi[ig]);
        m_tmpVecQuadPoint(ig) = m_tmpVecCoor(0);
      }
      break;
    case QUAD_POINT_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecQuadPoint(ig) = scalfct.val(0,ig);
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Operator 'a_grad_phi_i_grad_phi_j' not yet implemented with this type of element fields");
  }

  for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
    tmpblock += coef * m_tmpVecQuadPoint(ig) * fe.weightMeas(ig) * prod(trans(fe.dPhi[ig]),fe.dPhi[ig]);

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::a_grad_phi_i_grad_phi_j(const double coef,const ElementField& scalfct,const CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  switch(scalfct.type()) {
    case CONSTANT_FIELD:
      // nothing to do : the constant field has been computed before the integration loop
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        m_tmpVecQuadPoint(ig) = scalfct.val(0,0);
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(scalfct.val,fe.phi[ig]);
        m_tmpVecQuadPoint(ig) = m_tmpVecCoor(0);
      }
      break;
    case QUAD_POINT_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecQuadPoint(ig) = scalfct.val(0,ig);
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Operator 'a_grad_phi_i_grad_phi_j' not yet implemented with this type of element fields");
  }
  UBlasMatrix tmpAdPhi;
  for(int ig=0, numQuadPoint = fe.numQuadraturePoint(); ig < numQuadPoint; ig++) {
    tmpAdPhi  = prod(fe.contravariantMetric[ig],fe.dPhiRef(ig));
    tmpBlock += coef*fe.weightMeas(ig)*m_tmpVecQuadPoint(ig)*prod(trans(fe.dPhiRef(ig)),tmpAdPhi);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange mat = matBlock(iblock+icoor,jblock+icoor);
    mat += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::eps_phi_i_eps_phi_j(const double coef,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  const double f2 = 0.5 *coef;
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  grad_phi_i_grad_phi_j(f2, fe, iblock, jblock, num);

  // Add the cross terms and the remaining diagonal term.
  for (std::size_t icoor=0; icoor<num; icoor++) {
    for (std::size_t jcoor=0; jcoor<num; jcoor++) {
      for(int idof=0; idof<fe.numDof(); idof++) {
        for(int jdof=0; jdof<fe.numDof(); jdof++) {
          tmpBlock(idof,jdof) = 0.;
          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            tmpBlock(idof,jdof) += f2 * fe.weightMeas(ig) * fe.dPhi[ig](icoor,jdof) *  fe.dPhi[ig](jcoor,idof);
          }
        }
      }

      UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+jcoor);
      matrix += tmpBlock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::a_eps_phi_i_eps_phi_j(const double coef,const ElementField& scalfct,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  const double f2 = 0.5 *coef;
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  switch(scalfct.type()) {
    case CONSTANT_FIELD:
      // nothing to do : the constant field has been computed before the integration loop
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        m_tmpVecQuadPoint(ig) = scalfct.val(0,0);
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(scalfct.val,fe.phi[ig]);
        m_tmpVecQuadPoint(ig) = m_tmpVecCoor(0);
      }
      break;
    case QUAD_POINT_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        m_tmpVecQuadPoint(ig) = scalfct.val(0,ig);
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Operator 'a_eps_phi_i_eps_phi_j' not yet implemented with this type of element fields");
  }

  for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
    tmpBlock += f2 * m_tmpVecQuadPoint(ig) * fe.weightMeas(ig) * prod(trans(fe.dPhi[ig]),fe.dPhi[ig]);

  // Laplace operator on each velocity components
  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }

  // Cross terms
  for (int icoor=0; icoor<fe.numRefCoor(); icoor++) {
    for (int jcoor=0; jcoor<fe.numRefCoor(); jcoor++) {
      for(int idof=0; idof<fe.numDof(); idof++) {
        for(int jdof=0; jdof<fe.numDof(); jdof++) {
          tmpBlock(idof,jdof) = 0.;
          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            tmpBlock(idof,jdof) += f2 * m_tmpVecQuadPoint(ig) * fe.weightMeas(ig) * fe.dPhi[ig](icoor,idof) *  fe.dPhi[ig](jcoor,jdof);
          }
        }
      }
      UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+jcoor);
      matrix += tmpBlock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::tau_grad_phi_i_tau_grad_phi_j(const double coef,const std::vector<double>& diffTensor,const CurrentFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num,std::size_t dim)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  double norm;
  UBlasMatrix vec_tensor(dim,1);
  UBlasMatrix tensor(dim,dim);
  //compute diffTensor at quadrature point
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    for (int icoor=0; icoor<static_cast<int>(dim); icoor++) {
      vec_tensor(icoor,0) = 0.;
    }
    for (std::size_t icoor=0; icoor<std::size_t(fe.numCoor()); icoor++) {
      for (int idof=0; idof<fe.numDof(); idof++) {
        vec_tensor(icoor,0) += fe.phi[ig](idof)*diffTensor[icoor+dim*idof];
      }
    }
    norm = 0.;
    for (std::size_t icoor=0; icoor<dim; icoor++) {
      norm += vec_tensor(icoor,0)*vec_tensor(icoor,0);
    }
    if (std::fabs(norm) < 1.0e-8)
      tensor = 0.0 * prod(vec_tensor,trans(vec_tensor));
    else
      tensor = 1./norm * prod(vec_tensor,trans(vec_tensor));

    tmpBlock +=  coef * fe.weightMeas(ig) * prod(trans(prod(tensor,fe.dPhi[ig])),fe.dPhi[ig]);
  }
  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::tau_grad_phi_i_tau_grad_phi_j(const double coef,const std::vector<double>& diffTensor,const CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  //For fibers in cardiac electrophysiology
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  UBlasVector vec_tensor(3);
  double norm;
  UBlasVector sp_covBasis(2);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    vec_tensor.clear();
    for (int icoor=0; icoor<fe.numCoor(); icoor++) {
      for (int idof=0; idof<fe.numDof(); idof++) {
        vec_tensor(icoor) += fe.phi[ig](idof)*diffTensor[icoor+3*idof];
      }
    }
    norm = norm_2(vec_tensor);

    sp_covBasis(0) = vec_tensor(0)*fe.contravariantCompleteBasis[ig](0,0)+vec_tensor(1)*fe.contravariantCompleteBasis[ig](1,0)+vec_tensor(2)*fe.contravariantCompleteBasis[ig](2,0);
    sp_covBasis(1) = vec_tensor(0)*fe.contravariantCompleteBasis[ig](0,1)+vec_tensor(1)*fe.contravariantCompleteBasis[ig](1,1)+vec_tensor(2)*fe.contravariantCompleteBasis[ig](2,1);
    for (int idof=0; idof<fe.numDof(); idof++) {
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        for (int icoor=0; icoor<fe.numRefCoor(); icoor++) {
          for (int jcoor=0; jcoor<fe.numRefCoor(); jcoor++) {
            if (Tools::equal(norm,0.)) {
              tmpBlock(idof,jdof) = 0.;
            } else {
              tmpBlock(idof,jdof) += coef / norm * (fe.weightMeas(ig) * fe.dPhiRef(ig)(icoor,idof) * sp_covBasis(icoor) * sp_covBasis(jcoor) * fe.dPhiRef(ig)(jcoor,jdof));
            }
          }
        }
      }
    }
  }
  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::tau_orthotau_grad_phi_i_grad_phi_j(const double coef,const std::vector<double>& diffTensor,const std::vector<double>& angle,const CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  // tau in quadrature point
  UBlasVector vec_tensor(3);

  // Orthogonal vector of tau in the tangent plane
  UBlasVector vec_ortho_tensor(3);

  // Decomposition of tau in the covariant basis
  UBlasVector tau_covBasis(2);

  // Decomposition of the tau orthogonal in the covariant basis
  UBlasVector tau_ortho_covBasis(2);

  // Norm of vector tau
  double norm = 0.;

  // Fibers in the atria turn
  double Theta = 0.;
  double iTheta = 0.;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    vec_tensor(0) = 0.;
    vec_tensor(1) = 0.;
    vec_tensor(2) = 0.;
    for (int icoor=0; icoor<fe.numCoor(); icoor++) {
      for (int idof=0; idof<fe.numDof(); idof++) {
        vec_tensor(icoor) += fe.phi[ig](idof)*diffTensor[icoor+3*idof];
      }
    }
    for (int idof=0; idof<fe.numDof(); idof++) {
      Theta = angle[idof];

      if (std::fabs(Theta) < 1.0e-8) {
        iTheta = 1.;
      } else {
        iTheta = 1./2.+ std::sin(2.* Theta)/(4.* Theta);
      }

      norm = vec_tensor(0)*vec_tensor(0)+vec_tensor(1)*vec_tensor(1)+vec_tensor(2)*vec_tensor(2);
      //vec_ortho_tensor = vec_tensor vec a_3
      vec_ortho_tensor(0) = - fe.contravariantCompleteBasis[ig](1,2)*vec_tensor(2) + fe.contravariantCompleteBasis[ig](2,2)*vec_tensor(1);
      vec_ortho_tensor(1) = - fe.contravariantCompleteBasis[ig](2,2)*vec_tensor(0) + fe.contravariantCompleteBasis[ig](0,2)*vec_tensor(2);
      vec_ortho_tensor(2) = - fe.contravariantCompleteBasis[ig](0,2)*vec_tensor(1) + fe.contravariantCompleteBasis[ig](1,2)*vec_tensor(0);

      //(vec_tensor.a^1) and (vec_tensor.a^2)
      tau_covBasis(0) = vec_tensor(0)*fe.contravariantCompleteBasis[ig](0,0) + vec_tensor(1)*fe.contravariantCompleteBasis[ig](1,0) + vec_tensor(2)*fe.contravariantCompleteBasis[ig](2,0);
      tau_covBasis(1) = vec_tensor(0)*fe.contravariantCompleteBasis[ig](0,1) + vec_tensor(1)*fe.contravariantCompleteBasis[ig](1,1) + vec_tensor(2)*fe.contravariantCompleteBasis[ig](2,1);

      //(diffTensorOrtho.a^1) and (diffTensorOrtho.a^2)
      tau_ortho_covBasis(0) = vec_ortho_tensor(0)*fe.contravariantCompleteBasis[ig](0,0) + vec_ortho_tensor(1)*fe.contravariantCompleteBasis[ig](1,0) + vec_ortho_tensor(2)*fe.contravariantCompleteBasis[ig](2,0);
      tau_ortho_covBasis(1) = vec_ortho_tensor(0)*fe.contravariantCompleteBasis[ig](0,1) + vec_ortho_tensor(1)*fe.contravariantCompleteBasis[ig](1,1) + vec_ortho_tensor(2)*fe.contravariantCompleteBasis[ig](2,1);
    }
    for (int idof=0; idof<fe.numDof(); idof++) {
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        for (int icoor=0; icoor<fe.numRefCoor(); icoor++) {
          for (int jcoor=0; jcoor<fe.numRefCoor(); jcoor++) {
            if (std::fabs(norm) < 1.0e-8) {
              tmpBlock(idof,jdof) = 0.;
            } else {
              tmpBlock(idof,jdof) += coef / norm * fe.weightMeas(ig) * (iTheta *  fe.dPhiRef(ig)(icoor,idof) * tau_covBasis(icoor) * tau_covBasis(jcoor) * fe.dPhiRef(ig)(jcoor,jdof) + (1. - iTheta) * fe.dPhiRef(ig)(icoor,idof) * tau_ortho_covBasis(icoor) * tau_ortho_covBasis(jcoor) * fe.dPhiRef(ig)(jcoor,jdof));
            }
          }
        }
      }
    }
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_GradientBasedElastTensor_grad_phi_j(const double coef,
    const CurrentFiniteElement& fe,
    UBlasMatrix& GradientBasedElastTensor,
    const std::size_t iblock, const std::size_t jblock)
{
  const std::size_t nC = fe.numRefCoor();
  const int numQuadraturePoint = fe.numQuadraturePoint();
  const std::vector<UBlasMatrix>& dPhi = fe.dPhi; // alias

  for (std::size_t icoor = 0u; icoor < nC; ++icoor) {
    for (std::size_t jcoor = 0u; jcoor < nC; ++jcoor) {
      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);

      UBlasMatrixRange Block(GradientBasedElastTensor,
                              UBlasRange(icoor * nC, icoor * nC + nC),
                              UBlasRange(jcoor * nC, jcoor * nC + nC));

      for(int ig = 0; ig < numQuadraturePoint; ++ig) {
        UBlasMatrix tmpBlock = prod(trans(dPhi[ig]), Block);
        matrix += coef * fe.weightMeas(ig) * prod(tmpBlock, dPhi[ig]);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_GradientBasedHyperElastTensor_grad_phi_j(const double coef,
    const CurrentFiniteElement& finite_element,
    int quadraturePointIndex,
    UBlasMatrix& GradientBasedHyperElastTensor,
    const std::size_t iblock, const std::size_t jblock)
{
  const std::size_t nC = finite_element.numRefCoor();

  const UBlasMatrix& dPhi = finite_element.dPhi[quadraturePointIndex]; // alias
  const double modified_m_weightmeas = finite_element.weightMeas(quadraturePointIndex) * coef;

  for (std::size_t icoor = 0u; icoor < nC; ++icoor) {
    for (std::size_t jcoor = 0u; jcoor < nC; ++jcoor) {
      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);

      UBlasMatrixRange Block(GradientBasedHyperElastTensor,
                              UBlasRange(icoor * nC, icoor * nC + nC),
                              UBlasRange(jcoor * nC, jcoor * nC + nC));

      UBlasMatrix tmpBlock = prod(trans(dPhi), Block);
      matrix += modified_m_weightmeas * prod(tmpBlock, dPhi);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_phi_j(const double coef,const CurBaseFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    tmpBlock += ( coef * fe.weightMeas(ig) ) * outer_prod(fe.phi[ig], fe.phi[ig]);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::setZerosLung(const double coef,const CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  //NOTE: Nicolas : trick to ensure some elements of the matrix have  particular value
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    tmpBlock = ( coef * fe.weightMeas(ig) ) * outer_prod(fe.phi[ig], fe.phi[ig]);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_phi_j_lumped(const double coef,const CurBaseFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int i=0; i <fe.numDof(); ++i) {
    tmpBlock(i,i) +=  coef * fe.measure()/fe.numDof();
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::a_phi_i_phi_j(const double coef,const ElementField& scalfct,const CurBaseFiniteElement& fe,const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  switch(scalfct.type()) {
    case CONSTANT_FIELD:
      // nothing to do : the constant field has been computed before the integration loop
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        m_tmpVecQuadPoint(ig) = scalfct.val(0,0);
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(scalfct.val,fe.phi[ig]);
        m_tmpVecQuadPoint(ig) = m_tmpVecCoor(0);
      }
      break;
    case QUAD_POINT_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        m_tmpVecQuadPoint(ig) = scalfct.val(0,ig);
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'a_phi_i_phi_j' not yet implemented with this type of element fields");
  }

  for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
    tmpblock += coef * m_tmpVecQuadPoint(ig) * fe.weightMeas(ig) * outer_prod(fe.phi[ig], fe.phi[ig]);

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n(const double coef,const std::vector<double>& normal, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap)
{
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementMatlrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n: wrong disp field type");
  FEL_CHECK(gap.type() == QUAD_POINT_FIELD,"ElementMatrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n: wrong gap field type");
  double tmp;
  const double hK2inv = 1./(fe.diameter()*fe.diameter());
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
      UBlasMatrixRange matrix = matBlock(icoor,jcoor);
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmp = 0.;
        // disp at coor point
          for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
            for(int jdof=0; jdof<fe.numDof(); jdof++)
              tmp +=  fe.phi[ig](jdof)*disp.val(jcomp,jdof)*normal[jcomp];
          // gap
          tmp -= gap.val(0,ig);
          // tmp contains f \cdot n at quand node ig
          if (0 <= tmp ) {
            matrix += hK2inv*normal[icoor]*normal[jcoor]*( coef * fe.weightMeas(ig) ) * outer_prod(fe.phi[ig], fe.phi[ig]);
            // std::cout << " contact force acting on quadrature node " << ig << " of element " << fe.id() << std::endl;
          }
        }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::penalty_phi_i_scalar_n_phi_j_scalar_n(const double coef, CurvilinearFiniteElement& fe, const ElementField& vel, std::size_t iblock, std::size_t jblock) {
  FEL_CHECK(vel.type() == DOF_FIELD,"ElementMatrix::penalty_phi_i_scalar_n_phi_j_scalar_n: wrong vel field type");
  double tmp;
  const double penalty = 1./coef;
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
      UBlasMatrixRange matrix = matBlock(icoor+iblock,jcoor+jblock);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.;
            for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
                for(int jdof=0; jdof<fe.numDof(); jdof++)
                  tmp -=  fe.phi[ig](jdof)*vel.val(jcomp,jdof)*fe.normal[ig][jcomp];// compute u_dot_n
          if ( tmp >= 0. ) { //Heaviside
            matrix += penalty*fe.normal[ig][icoor]*fe.normal[ig][jcoor]*outer_prod(fe.phi[ig], fe.phi[ig])*fe.weightMeas(ig);
          }
        }
      }
    }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::nitsche_contact_phi_i_scalar_n_phi_j_scalar_n(double gamma,const std::vector<double>& normal, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap, const ElementField& lambda)
{
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementMatlrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n: wrong disp field type");
  FEL_CHECK(gap.type() == QUAD_POINT_FIELD,"ElementMatrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n: wrong gap field type");
  double tmp;
  const double penalty =  gamma / (fe.diameter()*fe.diameter());
  const double penalty_inv = fe.diameter()*fe.diameter()/gamma;
  int zblock = 2*fe.numCoor()-1; // 3 in 2d, and 5 in 3d
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
        UBlasMatrixRange matrixUV = matBlock(icoor,jcoor);
        UBlasMatrixRange matrixZV = matBlock(icoor,jcoor+zblock);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.;
          // disp.n +  gamma + lambda.h at coor point
          for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
            for(int jdof=0; jdof<fe.numDof(); jdof++)
              tmp +=  fe.phi[ig](jdof)*(disp.val(jcomp,jdof)+penalty_inv*lambda.val(jcomp,jdof)) *normal[jcomp];
          // gap a coor point
          tmp -= gap.val(0,ig);
          // tmp contains f \cdot n at quand node ig
          if (0 <= tmp ) {
            //std::cout << tmp<< std::endl;
            matrixUV += penalty*normal[icoor]*normal[jcoor] * outer_prod(fe.phi[ig], fe.phi[ig]) * fe.weightMeas(ig);
            matrixZV +=         normal[icoor]*normal[jcoor] * outer_prod(fe.phi[ig], fe.phi[ig]) * fe.weightMeas(ig);
          }
        }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::nitsche_contact_phi_i_scalar_n_phi_j_scalar_n(double gamma, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap, const ElementField& normal, const ElementField& lambda)
{
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementMatlrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n: wrong disp field type");
  FEL_CHECK(gap.type() == QUAD_POINT_FIELD,"ElementMatrix::penalty_contact_phi_i_scalar_n_phi_j_scalar_n: wrong gap field type");
  double tmp;
  const double penalty =  gamma / (fe.diameter()*fe.diameter());
  const double penalty_inv = fe.diameter()*fe.diameter()/gamma;
  int zblock = 2*fe.numCoor()-1; // 3 in 2d, and 5 in 3d
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
      UBlasMatrixRange matrixUV = matBlock(icoor,jcoor);
      UBlasMatrixRange matrixZV = matBlock(icoor,jcoor+zblock);
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmp = 0.;
        // disp.n +  gamma + lambda.h at coor point
        for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
          for(int jdof=0; jdof<fe.numDof(); jdof++)
            tmp +=  fe.phi[ig](jdof)*(disp.val(jcomp,jdof)+penalty_inv*lambda.val(jcomp,jdof)) *normal.val(jcomp,ig);
        // gap a coor point
        tmp -= gap.val(0,ig);
        // tmp contains f \cdot n at quand node ig
        if (0 <= tmp ) {
          //std::cout << tmp<< std::endl;
          matrixUV += penalty*normal.val(icoor,ig)*normal.val(jcoor,ig) * outer_prod(fe.phi[ig], fe.phi[ig]) * fe.weightMeas(ig);
          matrixZV +=         normal.val(icoor,ig)*normal.val(jcoor,ig) * outer_prod(fe.phi[ig], fe.phi[ig]) * fe.weightMeas(ig);
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_psi_j_scalar_n(const double coef,const CurvilinearFiniteElement& fe1, const CurvilinearFiniteElement& fe2, const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),"ElementMatrix::phi_i_psi_j_scalar_n: The quadrature rules must be the same for the two elements");
  for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
    for(std::size_t icoor=0; icoor<num; icoor++) {
      UBlasMatrixRange mat_block = matBlock(iblock,jblock+icoor);
      mat_block += (coef * fe1.weightMeas(ig) * fe1.normal[ig][icoor]) * outer_prod(fe2.phi[ig],fe1.phi[ig]);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_div_phi_i_and_psi_i_div_phi_j (const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const std::size_t iblock, const std::size_t jblock,const double coef1,const double coef2)
{
  m_tmpMat.clear();
  for(int icoor=0; icoor<fe1.numCoor(); icoor++) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock);
    for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
      for(int idof=0; idof<fe1.numDof(); idof++) {
        for(int jdof=0; jdof<fe2.numDof(); jdof++) {
          tmpBlock(idof,jdof) += fe1.weightMeas(ig) * fe1.dPhi[ig](icoor,idof) * fe2.phi[ig](jdof);
        }
      }
    }
  }

  for(int icoor=0; icoor<fe1.numCoor(); icoor++) {
    UBlasMatrixRange tmp = getMatBlock(m_tmpMat,iblock+icoor,jblock);
    UBlasMatrixRange mat1 = matBlock(iblock+icoor,jblock);
    UBlasMatrixRange mat2 = matBlock(jblock,iblock+icoor);
    mat1 += coef1 * tmp;
    mat2 += coef2 * trans(tmp);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_div_phi_i(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,const std::size_t iblock, const std::size_t jblock)
{
  m_tmpMat.clear();
  for(int icoor=0; icoor<fei.numCoor(); icoor++) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock);
    for(int ig=0; ig<fei.numQuadraturePoint(); ig++) {
      for(int idof=0; idof<fei.numDof(); idof++) {
        for(int jdof=0; jdof<fej.numDof(); jdof++) {
          tmpBlock(idof,jdof) += coef*fei.weightMeas(ig) * fei.dPhi[ig](icoor,idof) * fej.phi[ig](jdof);
        }
      }
    }
    UBlasMatrixRange block = matBlock(iblock+icoor,jblock);
    block+=tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_j_grad_psi_i(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,const std::size_t iblock, const std::size_t jblock)
{
  m_tmpMat.clear();
  for(int icoor=0; icoor<fej.numCoor(); icoor++) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock,jblock+icoor);
    for(int ig=0; ig<fei.numQuadraturePoint(); ig++) {
      for(int idof=0; idof<fei.numDof(); idof++) {
        for(int jdof=0; jdof<fej.numDof(); jdof++) {
          tmpBlock(idof,jdof) += coef*fei.weightMeas(ig) * fej.phi[ig](jdof) * fei.dPhi[ig](icoor,idof);
        }
      }
    }
    UBlasMatrixRange block = matBlock(iblock,jblock+icoor);
    block+=tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_grad_psi_j(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,const std::size_t iblock, const std::size_t jblock)
{
  m_tmpMat.clear();
  for(int icoor=0; icoor<fei.numCoor(); icoor++) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock);
    for(int ig=0; ig<fei.numQuadraturePoint(); ig++) {
      for(int idof=0; idof<fei.numDof(); idof++) {
        for(int jdof=0; jdof<fej.numDof(); jdof++) {
          tmpBlock(idof,jdof) += coef*fei.weightMeas(ig) * fei.phi[ig](idof) * fej.dPhi[ig](icoor,jdof);
        }
      }
    }
    UBlasMatrixRange block = matBlock(iblock+icoor,jblock);
    block+=tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_i_div_phi_j(const double coef, const CurrentFiniteElement& fej,const CurrentFiniteElement& fei,const std::size_t iblock, const std::size_t jblock)
{
  m_tmpMat.clear();
  for(int jcoor=0; jcoor<fej.numCoor(); jcoor++) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock,jblock+jcoor);
    for(int ig=0; ig<fej.numQuadraturePoint(); ig++) {
      for(int jdof=0; jdof<fej.numDof(); jdof++) {
        for(int idof=0; idof<fei.numDof(); idof++) {
          tmpBlock(idof,jdof) += coef*fej.weightMeas(ig) * fej.dPhi[ig](jcoor,jdof) * fei.phi[ig](idof);
        }
      }
    }
    UBlasMatrixRange block = matBlock(iblock,jblock+jcoor);
    block+=tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::u_grad_phi_j_phi_i(const double coef,const ElementField& vel,const CurrentFiniteElement& fe,const std::size_t iblock, const std::size_t jblock, const std::size_t num, bool transpose)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    switch(vel.type()) {
      case CONSTANT_FIELD:
        // nothing to do : the constant field has been computed before the integration loop
        for(int icoor=0; icoor<vel.numComp(); icoor++)
          m_tmpVecCoor(icoor) = vel.val(icoor,0);
        break;
      case DOF_FIELD: {
        m_tmpVecCoor = prod(vel.val,fe.phi[ig]);
        break;
      }
      case QUAD_POINT_FIELD: {
        for(int icoor=0; icoor<vel.numComp(); icoor++)
          m_tmpVecCoor(icoor) = vel.val(icoor,ig);
        break;
      }
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //          FEL_ERROR("Operator 'u_grad_phi_j_phi_i' not yet implemented with this type of element fields");
    }
    m_tmpVecDof = prod(trans(fe.dPhi[ig]),m_tmpVecCoor); // m_tmpVecDof = vel \cdot \nabla \phi_i
    for(int idof=0; idof<fe.numDof(); idof++) {
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        tmpblock(idof,jdof) +=  coef * fe.weightMeas(ig) * m_tmpVecDof(jdof) * fe.phi[ig](idof);
      }
    }
  }

  if ( transpose == false )
    for(std::size_t icoor=0; icoor<num; icoor++) {
      UBlasMatrixRange block = matBlock(iblock+icoor,jblock+icoor);
      block += tmpblock;
    }
  else {
    for(std::size_t icoor=0; icoor<num; icoor++) {
      UBlasMatrixRange block = matBlock(iblock+icoor,jblock+icoor);
      block += trans(tmpblock);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::u_grad_phi_i_grad_psi_j(const double coef,const ElementField& vel,const CurrentFiniteElement& fe1, const CurrentFiniteElement& fe2, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat,jblock, iblock);
  for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
    switch(vel.type()) {
      case CONSTANT_FIELD:
        // nothing to do : the constant field has been computed before the integration loop
        for(int icoor=0; icoor<vel.numComp(); icoor++)
          m_tmpVecCoor(icoor) = vel.val(icoor,0);
        break;
      case DOF_FIELD: {
        m_tmpVecCoor = prod(vel.val,fe1.phi[ig]);
        break;
      }
      case QUAD_POINT_FIELD: {
        for(int icoor=0; icoor<vel.numComp(); icoor++)
          m_tmpVecCoor(icoor) = vel.val(icoor,ig);
        break;
      }
    }
    m_tmpVecDof = prod(trans(fe1.dPhi[ig]),m_tmpVecCoor); // m_tmpVecDof = vel \cdot \nabla \phi_i
    for(int icoor=0; icoor<fe1.numCoor(); icoor++) {
      for(int idof=0; idof<fe2.numDof(); idof++) {
        for(int jdof=0; jdof<fe1.numDof(); jdof++) {
          tmpblock(jdof, idof) +=  coef * fe1.weightMeas(ig) * m_tmpVecDof(jdof) * fe2.dPhi[ig](icoor,idof);
        }
      }
    }

    for(std::size_t icoor=0; icoor<num; icoor++) {
      UBlasMatrixRange block = matBlock(jblock, iblock+icoor);
      block += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_grad_u(const double coef, const ElementField& vel, const CurrentFiniteElement& fe,
                                const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  double grad;
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for( std::size_t icoor = 0; icoor<num; icoor++) {
    for( std::size_t jcoor = 0; jcoor<num; jcoor++) {

      for(int idof = 0; idof < fe.numDof(); idof++) {
        for(int jdof = 0; jdof < fe.numDof(); jdof++) {
          tmpblock(idof, jdof) = 0.;
        }
      }

      for(int ig = 0; ig < fe.numQuadraturePoint(); ig++) {
        grad = 0.;
        for(int idof = 0; idof<fe.numDof(); idof++) {
          grad += vel.val(icoor, idof) * fe.dPhi[ig](jcoor, idof);
        }
        tmpblock +=  coef * fe.weightMeas(ig) * grad * outer_prod(fe.phi[ig], fe.phi[ig]);
      }
      UBlasMatrixRange matrix = matBlock(iblock+icoor, jblock+jcoor);
      matrix += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

//----------------------------------------------------------------------
// operator \int_{Sigma}  u^{n+1} Grad u^{n} \dot v - v Grad u^{n} \dot u^{n+1} :
// NS total pressure formulation
void ElementMatrix::phi_grad_u_minus_grad_u_phi(const double coef, const ElementField& vel,
    const CurrentFiniteElement& fe, const std::size_t iblock,
    const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  double grad;
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for( std::size_t icoor = 0; icoor<num; icoor++) {
    for( std::size_t jcoor = 0; jcoor<num; jcoor++) {

      if (icoor!=jcoor) {

        for(int idof = 0; idof < fe.numDof(); idof++) {
          for(int jdof = 0; jdof < fe.numDof(); jdof++) {
            tmpblock(idof, jdof) = 0.;
          }
        }

        for(int ig = 0; ig < fe.numQuadraturePoint(); ig++) {
          grad = 0.;
          for(int idof = 0; idof<fe.numDof(); idof++) {
            grad += vel.val(icoor, idof) * fe.dPhi[ig](jcoor, idof) - vel.val(jcoor, idof) * fe.dPhi[ig](icoor, idof);
          }
          tmpblock +=  coef * fe.weightMeas(ig) * grad * outer_prod(fe.phi[ig], fe.phi[ig]);
        }
        UBlasMatrixRange matrix = matBlock(iblock+icoor, jblock+jcoor);
        matrix += tmpblock;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::div_u_phi_j_phi_i(const double coef, const ElementField& vel, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  double div;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    div = 0;
    for (int icoor=0; icoor<fe.numCoor(); icoor++)
      for (int jdof=0; jdof<fe.numDof(); jdof++)
        div += vel.val(icoor,jdof) * fe.dPhi[ig](icoor,jdof);

    tmpBlock += coef * fe.weightMeas(ig) * div * outer_prod(fe.phi[ig], fe.phi[ig]);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange mat = matBlock(iblock+icoor,jblock+icoor);
    mat += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::convective_rot(const double coef, const ElementField& vel, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  IGNORE_UNUSED_ARGUMENT(num);
  m_tmpMat.clear();
  std::vector<double> v(fe.numCoor());

  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  // \int_{\Omega} coef rot(u^{n+1} x u^n \cdot v
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
      for (int idof=0; idof<fe.numDof(); idof++) {
        for (int jdof=0; jdof<fe.numDof(); jdof++) {
          tmpBlock(idof,jdof) = 0.;
          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            tmpBlock(idof,jdof) -= coef * fe.weightMeas(ig) * fe.dPhi[ig](icoor,jdof) * vel.val(jcoor,ig) * fe.phi[ig](idof);
          }
        }
      }
      UBlasMatrixRange mat = matBlock(iblock+icoor,jblock+jcoor);
      mat += tmpBlock;
    }
  }

  // \int_{\Omega} coef/2 u^{n+1} \cdot u^n) div(v)
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
      for(int idof=0; idof<fe.numDof(); idof++) {
        for(int jdof=0; jdof<fe.numDof(); jdof++) {
          tmpBlock(idof,jdof) = 0.;
          for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
            tmpBlock(idof,jdof) -= coef * fe.weightMeas(ig) * 0.5 * fe.dPhi[ig](icoor,idof) * vel.val(jcoor,ig) * fe.phi[ig](jdof);
          }
        }
      }
      UBlasMatrixRange mat = matBlock(iblock+icoor,jblock+jcoor);
      mat += tmpBlock;
    }
  }

  for (int idof=0; idof<fe.numDof(); idof++) {
    for (int jdof=0; jdof<fe.numDof(); jdof++) {
      tmpBlock(idof,jdof) = 0.;
      for(int jcoor=0; jcoor<fe.numCoor(); jcoor++) {
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmpBlock(idof,jdof) += coef * fe.weightMeas(ig) * fe.dPhi[ig](jcoor,jdof) * vel.val(jcoor,ig) * fe.phi[ig](idof);
        }
      }
    }
  }

  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    UBlasMatrixRange mat = matBlock(iblock+icoor,jblock+icoor);
    mat += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_dot_vec_phi_j(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "grad_phi_i_dot_vec_phi_j is not implemented for this type of vec");

  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for(felInt idof=0; idof<fe.numDof(); ++idof)
    for(felInt jdof=0; jdof<fe.numDof(); ++jdof)
      for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig)
        for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor)
          tmpblock(idof, jdof) += coef * fe.dPhi[ig](icoor, idof) * vec.val(icoor, 0) * fe.phi[ig](jdof) * fe.weightMeas(ig);


  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::eps_phi_i_dot_vec_phi_j(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "eps_phi_i_dot_vec_phi_j is not implemented for this type of vec");

  for(std::size_t icoor=0; icoor<num; ++icoor) {
    for(std::size_t jcoor=0; jcoor<num; ++jcoor) {
      m_tmpMat.clear();
      UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

      if(icoor == jcoor) {
        for(felInt idof=0; idof<fe.numDof(); ++idof)
          for(felInt jdof=0; jdof<fe.numDof(); ++jdof)
            for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig)
              for(felInt kcoor=0; kcoor<fe.numRefCoor(); ++kcoor)
                tmpblock(idof, jdof) += 0.5 * coef * fe.dPhi[ig](kcoor, idof) * vec.val(kcoor, 0) * fe.phi[ig](jdof) * fe.weightMeas(ig);
      }

      for(felInt idof=0; idof<fe.numDof(); ++idof)
        for(felInt jdof=0; jdof<fe.numDof(); ++jdof)
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig)
            tmpblock(idof, jdof) += 0.5 * coef * fe.dPhi[ig](jcoor, idof) * vec.val(icoor, 0) * fe.phi[ig](jdof) * fe.weightMeas(ig);

      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);
      matrix += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_grad_phi_j_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "phi_i_grad_phi_j_dot_vec is not implemented for this type of vec");

  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for(felInt idof=0; idof<fe.numDof(); ++idof)
    for(felInt jdof=0; jdof<fe.numDof(); ++jdof)
      for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig)
        for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor)
          tmpblock(idof, jdof) += coef * fe.dPhi[ig](icoor, jdof) * vec.val(icoor, 0) * fe.phi[ig](idof) * fe.weightMeas(ig);


  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_eps_phi_j_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "eps_phi_i_dot_vec_phi_j is not implemented for this type of vec");
  for(std::size_t icoor=0; icoor<num; ++icoor) {
    for(std::size_t jcoor=0; jcoor<num; ++jcoor) {
      m_tmpMat.clear();
      UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

      if(icoor == jcoor) {
        for(felInt idof=0; idof<fe.numDof(); ++idof)
          for(felInt jdof=0; jdof<fe.numDof(); ++jdof)
            for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig)
              for(felInt kcoor=0; kcoor<fe.numRefCoor(); ++kcoor)
                tmpblock(idof, jdof) += 0.5 * coef * fe.dPhi[ig](kcoor, jdof) * vec.val(kcoor, 0) * fe.phi[ig](idof) * fe.weightMeas(ig);
      }

      for(felInt idof=0; idof<fe.numDof(); ++idof)
        for(felInt jdof=0; jdof<fe.numDof(); ++jdof)
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig)
            tmpblock(idof, jdof) += 0.5 * coef * fe.dPhi[ig](icoor, jdof) * vec.val(jcoor, 0) * fe.phi[ig](idof) * fe.weightMeas(ig);

      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);
      matrix += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::eps_phi_j_vec_dot_eps_phi_i_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "ElementMatrix::eps_phi_j_vec_dot_eps_phi_i_vec is not implemented for non constant vec");

  double tmpvali, tmpvalj;
  double deri, derj;

  for(std::size_t icoor=0; icoor<num; ++icoor) {
    for(std::size_t jcoor=0; jcoor<num; ++jcoor) {
      m_tmpMat.clear();
      UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

      if(icoor == jcoor) {
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
            for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
              tmpvali = 0.;
              tmpvalj = 0.;

              for(felInt kcoor=0; kcoor<fe.numRefCoor(); ++kcoor) {
                tmpvali += fe.dPhi[ig](kcoor, idof) * vec.val(kcoor, 0);
                tmpvalj += fe.dPhi[ig](kcoor, jdof) * vec.val(kcoor, 0);
              }

              tmpblock(idof, jdof) += 0.25 * coef * tmpvali * tmpvalj * fe.weightMeas(ig);
            }
          }
        }
      }

      for(felInt idof=0; idof<fe.numDof(); ++idof) {
        for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            tmpvali = 0.; deri = 0.;
            tmpvalj = 0.; derj = 0.;

            for(felInt kcoor=0; kcoor<fe.numRefCoor(); ++kcoor) {
              tmpvali += fe.dPhi[ig](kcoor, idof) * vec.val(kcoor, 0);
              tmpvalj += fe.dPhi[ig](kcoor, jdof) * vec.val(kcoor, 0);
              deri += fe.dPhi[ig](kcoor, idof) * vec.val(icoor, 0);
              derj += fe.dPhi[ig](kcoor, jdof) * vec.val(jcoor, 0);
            }

            tmpblock(idof, jdof) += 0.25 * coef * fe.dPhi[ig](icoor, jdof) * vec.val(jcoor, 0) * tmpvali * fe.weightMeas(ig);
            tmpblock(idof, jdof) += 0.25 * coef * fe.dPhi[ig](jcoor, idof) * vec.val(icoor, 0) * tmpvalj * fe.weightMeas(ig);
            tmpblock(idof, jdof) += 0.25 * coef * deri * derj * fe.weightMeas(ig);
          }
        }
      }

      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);
      matrix += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_j_vec_dot_grad_phi_i_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "ElementMatrix::eps_phi_j_vec_dot_eps_phi_i_vec is not implemented for non constant vec");

  double tmpvali, tmpvalj;

  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for(felInt idof=0; idof<fe.numDof(); ++idof) {
    for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
      for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
        tmpvali = 0.;
        tmpvalj = 0.;

        for(felInt kcoor=0; kcoor<fe.numRefCoor(); ++kcoor) {
          tmpvali += fe.dPhi[ig](kcoor, idof) * vec.val(kcoor, 0);
          tmpvalj += fe.dPhi[ig](kcoor, jdof) * vec.val(kcoor, 0);
        }

        tmpblock(idof, jdof) += coef * tmpvali * tmpvalj * fe.weightMeas(ig);
      }
    }
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_dot_n_psi_j(const CurrentFiniteElementWithBd& fewbd1,
    const CurrentFiniteElementWithBd& fewbd2,
    int iblockBd, int numblockBd,
    const std::size_t iblock, const std::size_t jblock,const std::size_t num,
    const double coef1,const double coef2)
{
  FEL_ASSERT(iblockBd>=0 && iblockBd<fewbd1.numBdEle() && (numblockBd-iblockBd) <= fewbd1.numBdEle() );
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
    FEL_ASSERT( fewbd1.bdEle(ibd).hasNormal() && fewbd1.hasFirstDeriv() );
    for(int ilocg=0; ilocg<fewbd1.numQuadPointBdEle(ibd); ilocg++) {
      const int ig = ilocg+fewbd1.indexQuadPoint(ibd+1); // face ibd starts at ibd+1 (0 for internal quad points)
      //NB: is "ig2 = ig1"??? The quad points should be the same for the 2 currentFEwBd... VM 10/2011

      //! Vector = [n \cdot \grad \phi(jdof)]: (std::vector line)
      m_tmpVecDof = prod(trans(fewbd1.bdEle(ibd).normal[ilocg]), fewbd1.dPhi[ig]);

      // these 2 loops below are equivalent to the 2 outer_prod (below). is a method much faster than the other??? vm 11/2011
      // beware: one does 2 outer_prod, and only one is useful (+ 2 matrix * scalar products). improve this...

      /*  for(int idof=0;idof<fewbd1.numDof();idof++){
        for(int jdof=0;jdof<fewbd2.numDof();jdof++){
        // dphi_1/dn(jdof) * psi_2(idof):
        double theval = fewbd1.bdEle(ibd).weightMeas(ilocg) * fewbd2.phi[ig](idof) * m_tmpVecDof(jdof);
        tmpBlock(idof,jdof) += coef1 * theval;
        tmpBlock(jdof,idof) += coef2 * theval;
        }
        }*/
      //! Matrix = Vector^T * Vector (* val)
      tmpBlock += ( coef1 * fewbd1.bdEle(ibd).weightMeas(ilocg) ) *  outer_prod( fewbd2.phi[ig], m_tmpVecDof );
      tmpBlock += ( coef2 * fewbd1.bdEle(ibd).weightMeas(ilocg) ) *  outer_prod( m_tmpVecDof, fewbd2.phi[ig] );
    }
  }
  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_i_dot_n_grad_phi_j_dot_n(const double coef,const CurrentFiniteElementWithBd& fewbd,
    int iblockBd, int numblockBd,
    const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  FEL_ASSERT(iblockBd>=0 && iblockBd<fewbd.numBdEle() && (numblockBd-iblockBd) <= fewbd.numBdEle() );
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
    FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
    for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
      const int ig = ilocg+fewbd.indexQuadPoint(ibd+1); // face ibd starts at ibd+1 (0 for internal quad points)

      //! Vector = [n \cdot \grad \phi(jdof)]:
      m_tmpVecDof = prod(trans(fewbd.bdEle(ibd).normal[ilocg]), fewbd.dPhi[ig]);
      //! Matrix = Vector^T * Vector (* val)
      tmpBlock += ( coef * fewbd.bdEle(ibd).weightMeas(ilocg) ) * outer_prod( m_tmpVecDof, m_tmpVecDof );
    }
  }
  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_j_dot_n_grad_psi_i_dot_n(const double coef, const CurrentFiniteElementWithBd& fewbdphi, const CurrentFiniteElementWithBd& fewbdpsi, felInt iBdPhi, felInt iBdPsi, const std::size_t iblock, const std::size_t jblock, const std::size_t numBlock)
{
  // Check that the ids of the boundary is in the bound
  FEL_ASSERT(iBdPhi>=0 && iBdPhi<fewbdphi.numBdEle());
  FEL_ASSERT(iBdPsi>=0 && iBdPsi<fewbdpsi.numBdEle());

  // Check that the normal and the derivatives have been computed
  FEL_ASSERT(fewbdphi.bdEle(iBdPhi).hasNormal() && fewbdphi.hasFirstDeriv());
  FEL_ASSERT(fewbdpsi.bdEle(iBdPsi).hasNormal() && fewbdpsi.hasFirstDeriv());

  // Check that the number of integration point is the same on the boundary
  FEL_ASSERT(fewbdphi.numQuadPointBdEle(iBdPhi) == fewbdpsi.numQuadPointBdEle(iBdPsi));

  // Resize if required
  if (m_tmpVec2Dof.size() != m_tmpVecDof.size())
    m_tmpVec2Dof.resize(m_tmpVecDof.size());

  // Clear
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  // Compute the integral over the boundary
  for(felInt ilocg=0; ilocg<fewbdpsi.numQuadPointBdEle(iBdPsi); ++ilocg) {
    // Boundary iBdPhi starts at iBdPhi+1 (0 for internal quad points)
    const felInt igphi = ilocg + fewbdphi.indexQuadPoint(iBdPhi+1);
    const felInt igpsi = ilocg + fewbdpsi.indexQuadPoint(iBdPsi+1);

    // Compute \grad \phi(jdof) \cdot nphi and \grad \psi(jdof) \cdot npsi
    m_tmpVecDof = prod(trans(fewbdphi.bdEle(iBdPhi).normal[ilocg]), fewbdphi.dPhi[igphi]);
    m_tmpVec2Dof = prod(trans(fewbdpsi.bdEle(iBdPsi).normal[ilocg]), fewbdpsi.dPhi[igpsi]);

    // Compute Vector^T * Vector (* val)
    tmpBlock += (coef * fewbdpsi.bdEle(iBdPsi).weightMeas(ilocg)) * outer_prod(m_tmpVec2Dof, m_tmpVecDof);
  }

  for(std::size_t icoor=0; icoor<numBlock; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor, jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_phi_j_grad_psi_i(const double coef, const CurrentFiniteElementWithBd& fewbdphi, const CurrentFiniteElementWithBd& fewbdpsi, felInt iBdPhi, felInt iBdPsi, const std::size_t iblock, const std::size_t jblock, const std::size_t numBlock)
{
  // Check that the ids of the boundary is in the bound
  FEL_ASSERT(iBdPhi>=0 && iBdPhi<fewbdphi.numBdEle());
  FEL_ASSERT(iBdPsi>=0 && iBdPsi<fewbdpsi.numBdEle());

  // Check that the derivatives have been computed
  FEL_ASSERT(fewbdphi.hasFirstDeriv());
  FEL_ASSERT(fewbdpsi.hasFirstDeriv());

  // Check that the number of integration point is the same on the boundary
  FEL_ASSERT(fewbdphi.numQuadPointBdEle(iBdPhi) == fewbdpsi.numQuadPointBdEle(iBdPsi));

  // Clear
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);

  // Compute the integral over the boundary
  for(felInt ilocg=0; ilocg<fewbdpsi.numQuadPointBdEle(iBdPsi); ++ilocg) {
    // boundary iBdPhi starts at iBdPhi+1 (0 for internal quad points)
    const felInt igphi = ilocg + fewbdphi.indexQuadPoint(iBdPhi+1);
    const felInt igpsi = ilocg + fewbdpsi.indexQuadPoint(iBdPsi+1);

    tmpBlock += coef * fewbdpsi.bdEle(iBdPsi).weightMeas(ilocg) * prod(trans(fewbdpsi.dPhi[igpsi]), fewbdphi.dPhi[igphi]);
  }

  for(std::size_t icoor=0; icoor<numBlock; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor, jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::div_phi_j_div_phi_i(const double coef, const CurrentFiniteElement& fe, const std::size_t iblock, std::size_t, const std::size_t num)
{
  m_tmpMat.clear();
  for(std::size_t icoor=iblock; icoor<num; icoor++) {
    for(std::size_t jcoor=icoor; jcoor<num; jcoor++) {
      UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,icoor,jcoor);
      for(felInt idof=0; idof<fe.numDof(); ++idof) {
        for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ig++) {
            tmpBlock(idof, jdof) += coef * fe.weightMeas(ig) * fe.dPhi[ig](jcoor, jdof) * fe.dPhi[ig](icoor, idof);
          }
        }
      }
      UBlasMatrixRange mat1 = matBlock(icoor,jcoor);
      if (jcoor==icoor) {
        mat1 += tmpBlock;
      } else {
        mat1 += tmpBlock;
        UBlasMatrixRange mat2 = matBlock(jcoor,icoor);
        mat2 += trans(tmpBlock);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::stab_supg(const double dt, const double stab1, const double stab2, const double density, const double visc,
                              const CurrentFiniteElement& fe,const ElementField& vel,const ElementField& rhs,
                              ElementVector& elemVec,double& Re_elem,double& tau, const int type)
{
  /**
   * Typical value for stab1 = 0.1
   * This stabilization does not include the 1/dt term (not strongly consistant with the time discrete scheme, may be viewed as a space-time, P0 in time)
   * If type = 1: stabilization parameter depends on dt (Tezduyar)
   * If type = 2: stabilization parameter independent of dt (Franca-Frey CMAME 1992)
   * If type = 3: stabilization Brezzi-Pitkaranta
   */
  FEL_ASSERT(vel.type() == DOF_FIELD);
  FEL_ASSERT(rhs.type() == DOF_FIELD);

  // Resize if required
  if (m_tmpVec2Coor.size() != m_tmpVecCoor.size())
    m_tmpVec2Coor.resize(m_tmpVecCoor.size());

  double weightJ,norm_u,delta,s,adv_j,div,adveps;
  const double h_elem = fe.diameter();
  int i0,j0,i0_pres;

  delta = 0.;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    weightJ = fe.weightMeas(ig);
    // Computation of u and f in the element in the integration point ig
    m_tmpVecCoor = prod(vel.val,fe.phi[ig]); // vel
    norm_u = norm_1(m_tmpVecCoor);
    m_tmpVec2Coor = prod(rhs.val,fe.phi[ig]); // rhs
    if ( type == 1 ) {
      tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
                              (4.*norm_u*norm_u)/(h_elem*h_elem) +
                              (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                            ) / density;
      // coef 10 is to have a typical value of 0.1 as for type 2
      delta = stab2*h_elem*h_elem/tau ;
    }
    else if ( type == 2 ) {
      Re_elem = density*stab1*norm_u*h_elem/(4*visc);
      if(Re_elem<1.) {
        tau = stab1*density*h_elem*h_elem/(8*visc);
        delta = stab2*norm_u*h_elem*Re_elem;
      } else {
        tau = h_elem/(2.*norm_u);
        delta = stab2*norm_u*h_elem;
      }
    }
    else if ( type == 3 ) {
      tau   = stab1*std::pow(h_elem,2)/visc;
      delta = 0;
    }
    else{ 
      FEL_ERROR("Type SUPG not allowed!");
    }

    int ncoor = fe.numCoor();
    i0_pres = m_firstRow[ncoor];
    for(int idof=0; idof<fe.numDof(); idof++) {
      adveps=0.;
      for(int kcoor=0; kcoor<ncoor; kcoor++) {
        adveps += m_tmpVecCoor[kcoor]* fe.dPhi[ig](kcoor,idof);
      }
      adveps *= density*tau*weightJ;// rho (u.\gradv_i)
      s=0.;
      for(int icoor=0; icoor<ncoor; icoor++) {
        i0 = m_firstRow[icoor];
        elemVec.vec()[i0+idof] += m_tmpVec2Coor[icoor]*adveps;
        s += fe.dPhi[ig](icoor,idof)*m_tmpVec2Coor(icoor); // same fe for velocity and pressure
      }
      elemVec.vec()[i0_pres+idof] -= tau*s*weightJ;
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        
        adv_j = 0.;
        for(int kcoor=0; kcoor<ncoor; kcoor++) {
          adv_j +=  m_tmpVecCoor(kcoor)*fe.dPhi[ig](kcoor,jdof);
        }
        adv_j *= density; // rho (u.\grad v_j)
        for(int icoor=0; icoor<ncoor; icoor++) {
          i0 = m_firstRow[icoor];
          div = delta*fe.dPhi[ig](icoor,idof)*weightJ;
          for(int jcoor=0; jcoor<ncoor; jcoor++) {
            j0 = m_firstCol[jcoor];
            if(icoor==jcoor) {
              m_mat(i0+idof,j0+jdof) += adv_j*adveps; // on the diagonal blocks
            }
            m_mat(i0+idof,j0+jdof) += div*fe.dPhi[ig](jcoor,jdof);
          }// jcoor
          m_mat(i0+idof,i0_pres+jdof) += adveps*fe.dPhi[ig](icoor,jdof); // // same fe for velocity and pressure
          m_mat(i0_pres+idof,i0+jdof) -= fe.dPhi[ig](icoor,idof)*tau*weightJ*adv_j;
          m_mat(i0_pres+idof,i0_pres+jdof) -= fe.dPhi[ig](icoor,idof)*fe.dPhi[ig](icoor,jdof)*tau*weightJ;
        }// icoor
      }// jdof
    }// idof
  }// ig
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::stab_supgNSHeat(const double dt, const double stab1, const double alpha, const double density, const double visc,
                                    const CurrentFiniteElement& fe, const ElementField& vel, const int type, const std::vector<double> & gravity)
{
  FEL_ASSERT(vel.type() == DOF_FIELD);

  double weightJ,norm_u,Re_elem,adv_i,tau = 0.0;
  const double h_elem = fe.diameter();
  const felInt numCompVel = fe.numCoor();
  FEL_ASSERT(std::size_t(numCompVel) == gravity.size());

  const felInt jBlockTemp = numCompVel + 1;
  const felInt iBlockPres = numCompVel;

  felInt iStartVel,jStartTemp,iStartPre;

  const double coeffVel=density*density*alpha;
  const double coeffPre=-density*alpha;

  jStartTemp = m_firstCol[jBlockTemp];
  iStartPre = m_firstRow[iBlockPres];

  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    weightJ = fe.weightMeas(ig);

    // Evaluation of the extrapolated velocity on the current quadrature node
    m_tmpVecCoor = prod(vel.val,fe.phi[ig]);

    // Local Reynolds computation and parameter settings
    norm_u = norm_1(m_tmpVecCoor);
    Re_elem = density*stab1*norm_u*h_elem/(4*visc);

    if (type==0) {
      if(Re_elem<1.) {
        tau = stab1*density*h_elem*h_elem/(8*visc);
      } else {
        tau = h_elem/(2.*norm_u);
      }
    } else if (type==1) {
      tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
                              (4.*norm_u*norm_u)/(h_elem*h_elem) +
                              (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                              ) / density;
    }

    for (int idof=0; idof<fe.numDof(); ++idof) {
      //w\dot \nabla \phi_{idof} (x_{ig})
      adv_i = 0;
      for (int kcoor=0; kcoor<numCompVel; kcoor++) {
        adv_i += m_tmpVecCoor[kcoor]*fe.dPhi[ig](kcoor,idof);
      }
      adv_i *= weightJ * coeffVel * tau;
      for (int icoor=0; icoor<numCompVel; icoor++) {
        iStartVel = m_firstRow[icoor];
        adv_i *= gravity[icoor];
        for (int jdof=0; jdof<fe.numDof(); ++jdof) {
          m_mat(iStartVel+idof,jStartTemp+jdof) += adv_i * fe.phi[ig](jdof);
        }
      }
    }

    for (int idof=0; idof<fe.numDof(); ++idof) {
      //nabla \phi_{idof}\cdot gravity
      adv_i = 0;
      for (int kcoor=0; kcoor<numCompVel; kcoor++) {
        adv_i += gravity[kcoor]*fe.dPhi[ig](kcoor,idof);
      }
      adv_i *= weightJ * coeffPre * tau;
      for (int jdof=0; jdof<fe.numDof(); ++jdof) {
        m_mat(iStartPre+idof,jStartTemp+jdof) += adv_i * fe.phi[ig](jdof);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::stab_supgAdvDiffCT(const double dt, const double stab1, const double stab2, const double density, const double visc,
                    const CurrentFiniteElement& fe,const ElementField& vel)
{
  FEL_ASSERT(vel.type() == DOF_FIELD);

  double weightJ,norm_u,delta,adv_j,div,adveps, tau;
  const double h_elem = fe.diameter();
  int i0,j0;

  delta = 0.;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    weightJ = fe.weightMeas(ig);
    // Computation of u and f in the element in the integration point ig
    m_tmpVecCoor = prod(vel.val,fe.phi[ig]); // vel
    norm_u = norm_1(m_tmpVecCoor);
    delta = stab2*norm_u*h_elem;
    tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
                (4.*norm_u*norm_u)/(h_elem*h_elem) +
                              (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                            ) / density;
    int ncoor = fe.numCoor();
    for(int idof=0; idof<fe.numDof(); idof++) {
      adveps=0.;
      for(int kcoor=0; kcoor<ncoor; kcoor++) {
        adveps += m_tmpVecCoor[kcoor]* fe.dPhi[ig](kcoor,idof);
      }
      adveps *= density*tau*weightJ;// rho (u.\gradv_i)
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        adv_j = 0.;
        for(int kcoor=0; kcoor<ncoor; kcoor++) {
          adv_j +=  m_tmpVecCoor(kcoor)*fe.dPhi[ig](kcoor,jdof);
        }
        adv_j *= density; // rho (u.\grad v_j)
        for(int icoor=0; icoor<ncoor; icoor++) {
          i0 = m_firstRow[icoor];
          div = delta*fe.dPhi[ig](icoor,idof)*weightJ;
          for(int jcoor=0; jcoor<ncoor; jcoor++) {
            j0 = m_firstCol[jcoor];
            if(icoor==jcoor) {
              m_mat(i0+idof,j0+jdof) += adv_j*adveps; // on the diagonal blocks
            }
            m_mat(i0+idof,j0+jdof) += div*fe.dPhi[ig](jcoor,jdof);
          }// jcoor
        }// icoor
      }// jdof
    }// idof
  }// ig
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::stab_supgProjCT(const double dt, const double stab1, const double density, const double visc, const CurrentFiniteElement& fe,const ElementField& vel)
{
  FEL_ASSERT(vel.type() == DOF_FIELD);
  UBlasMatrixRange matrix = matBlock(0,0);
  double norm_u;
  const double h_elem = fe.diameter();
  double tau;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    // Computation of u and f in the element in the integration point ig
    m_tmpVecCoor = prod(vel.val,fe.phi[ig]); // vel
    norm_u = norm_1(m_tmpVecCoor);
    tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
              (4.*norm_u*norm_u)/(h_elem*h_elem) +
              (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                            ) / density;
    matrix += tau * fe.weightMeas(ig) * prod(trans(fe.dPhi[ig]),fe.dPhi[ig]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::stab_supgMatrixFS(const double dt, const double stabSUPG, const double density, const double viscosity,
                                      const double c0, const double c1, const double c2,
                                      const CurrentFiniteElement& fe, const ElementField& vel)
{
  // Compute :
  // (int_K stabSUPG * tau * c0 * (vel . \grad) . v ) . (c1 u + c2 (vel . \grad) . u)
  // tau is computed automatically in the function
  FEL_ASSERT(vel.type() == DOF_FIELD);
  double weightJ, norm_u, adv_j, adveps, phij;
  const double h_elem = fe.diameter();
  double tau;
  int i0;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    weightJ = fe.weightMeas(ig);

    // Computation of U in the element in the integration point ig
    m_tmpVecCoor = prod(vel.val,fe.phi[ig]);
    norm_u = norm_1(m_tmpVecCoor);

    tau = stabSUPG / std::sqrt( (4./(dt*dt)) +
                            (4.*norm_u*norm_u)/(h_elem*h_elem) +
                            (16*viscosity*viscosity)/(density*density*h_elem*h_elem*h_elem*h_elem));

    int ncoor = fe.numCoor();
    for(int idof=0; idof<fe.numDof(); idof++) {
      adveps=0.;
      for(int kcoor=0; kcoor<ncoor; kcoor++) {
        adveps += m_tmpVecCoor(kcoor) * fe.dPhi[ig](kcoor, idof);
      }
      adveps *= c0 * tau * weightJ;  // tau * C_0 (U . \grad) . v_i

      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        adv_j = 0.;
        for(int kcoor=0; kcoor<ncoor; kcoor++) {
          adv_j +=  m_tmpVecCoor(kcoor) * fe.dPhi[ig](kcoor, jdof);
        }
        adv_j *= c2; // C_2 (U . \grad) . v_j)

        phij = c1 * fe.phi[ig](jdof); // C_1 v_j

        for(int icoor=0; icoor<ncoor; icoor++) {
          i0 = m_firstRow[icoor];

          // on the diagonal blocks
          m_mat(i0 + idof, i0 + jdof) += adveps * adv_j;
          m_mat(i0 + idof, i0 + jdof) += adveps * phij;

        } // icoor
      } // jdof
    } // idof
  } // ig
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::f_dot_n_grad_phi_i_grad_phi_j(const double coef,const CurvilinearFiniteElement& fe, const ElementField& elfield,const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  UBlasMatrix tmpAdPhi;
  double tmp,f_dot_n;
  for(int ig=0, numQuadPoint = fe.numQuadraturePoint(); ig < numQuadPoint; ig++) {
    f_dot_n = 0.;
    for(int icomp=0; icomp<fe.numCoor(); icomp++) {
      tmp = 0.;
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        tmp +=  fe.phi[ig](jdof) * elfield.val(icomp,jdof);
      }
      f_dot_n += tmp * fe.normal[ig](icomp);
    }
    tmpAdPhi  = prod(fe.contravariantMetric[ig],fe.dPhiRef(ig));

    if (f_dot_n < 0)
      tmpBlock += coef*f_dot_n*fe.weightMeas(ig)*prod(trans(fe.dPhiRef(ig)),tmpAdPhi);
  }

  for(std::size_t icoor=0; icoor < num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix:: f_dot_n_phi_i_phi_j(const double coef, const CurvilinearFiniteElement& fe,const ElementField& elfield, const std::size_t iblock, const std::size_t jblock,const std::size_t num, int stabFlag)
{
  m_tmpMat.clear();
  double tmp,f_dot_n;
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    f_dot_n = 0.;
    for(int icomp=0; icomp<fe.numCoor(); icomp++) {
      tmp = 0.;
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        tmp +=  fe.phi[ig](jdof) * elfield.val(icomp,jdof);
      }
      f_dot_n += tmp * fe.normal[ig](icomp);
    }
    // outflow stabilization
    if (stabFlag == 2) {
      if (f_dot_n < 0)
        tmpBlock += f_dot_n * coef * fe.weightMeas(ig) * outer_prod(fe.phi[ig], fe.phi[ig]);
    } else { // this is no longer outflow stabilization.
      tmpBlock += f_dot_n * coef * fe.weightMeas(ig) * outer_prod(fe.phi[ig], fe.phi[ig]);
    }

  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix:: f_dot_n_phi_i_phi_j(const double coef, const CurrentFiniteElement& fe, const ElementField& elt_f, const ElementField& elt_n, const std::size_t iblock, const std::size_t jblock, const std::size_t num, felInt stabFlag)
{
  m_tmpMat.clear();
  double tmp, f_dot_n;
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    f_dot_n = 0.;
    for(int icomp=0; icomp<fe.numCoor(); icomp++) {
      tmp = 0.;
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        tmp += fe.phi[ig](jdof) * elt_f.val(icomp, jdof);
      }
      f_dot_n += tmp * elt_n.val(icomp, 0);
    }

    if (f_dot_n < 0 || stabFlag != 2)
      tmpBlock += f_dot_n * coef * fe.weightMeas(ig) * outer_prod(fe.phi[ig], fe.phi[ig]);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::abs_u_dot_n_phi_i_phi_j(const double coef,
    ElementField& vel,
    CurvilinearFiniteElement& fe,
    const std::size_t iblock, const std::size_t jblock,int num)
{

  m_tmpMat.clear();

  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  double tmp;
  double u_dot_n;

  //switch(vel.type()){
  //case DOF_FIELD: {
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) { // loop on gauss pts.
    u_dot_n = 0;
    for(int icomp=0; icomp<fe.numCoor(); icomp++) {
      tmp = 0.;
      for(int jdof=0; jdof<fe.numDof(); jdof++) {
        tmp +=  fe.phi[ig](jdof) * vel.val(icomp,jdof);
      }
      u_dot_n += tmp * fe.normal[ig](icomp);
    }
    tmpBlock +=  0.5 * (std::abs(u_dot_n)-u_dot_n) *
                  coef * fe.weightMeas(ig)  * outer_prod(fe.phi[ig], fe.phi[ig]);
  }

  //std::cout << " block" << iblock << "," << jblock << std::endl;
  for(int icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_times_n_phi_j_times_n(const double coef, const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const int numComp)
{
  IGNORE_UNUSED_ARGUMENT(numComp);
  m_tmpMat.clear();
  //   TGV \int_element_in_Gamma [ (phi_i \cdot \tau) \tau ] \cdot [ (phi_j \cdot \tau) \tau ] =
  // = TGV \int_element_in_Gamma (phi_i \cdot \tau) (phi_j \cdot \tau) =
  // = TGV \int_element_in_Gamma [ (I - n x n^t) phi_i ] \cdot [ (I - n x n^t) phi_j ]

  UBlasIdentityMatrix IdeMat(fe.numCoor());

  for( std::size_t icoor=0 ; icoor < (std::size_t) fe.numCoor() ; icoor++ ) {
    for( std::size_t jcoor=0 ; jcoor < (std::size_t) fe.numCoor() ; jcoor++ ) {
      UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock+jcoor);
      for( std::size_t idof=0 ; idof < (std::size_t) fe.numDof() ; idof++ ) {
        for( std::size_t jdof=0 ; jdof < (std::size_t) fe.numDof() ; jdof++ ) {
          for( std::size_t ig=0; ig < (std::size_t) fe.numQuadraturePoint(); ig++ ) {
            tmpBlock(idof,jdof) += coef * fe.weightMeas(ig) * ( IdeMat(icoor,jcoor) - fe.normal[ig](icoor) * fe.normal[ig](jcoor) ) * fe.phi[ig](idof) * fe.phi[ig](jdof);
          }
        }
      }
      UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+jcoor);
      matrix += tmpBlock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_dot_n_phi_j_dot_n(const double coef, const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const int numComp)
{
  IGNORE_UNUSED_ARGUMENT(numComp);

  m_tmpMat.clear();

  for( std::size_t icoor=0 ; icoor < (std::size_t) fe.numCoor() ; icoor++ ) {
    for( std::size_t jcoor=0 ; jcoor < (std::size_t) fe.numCoor() ; jcoor++ ) {

      UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock+jcoor);

      for( std::size_t ig=0; ig < (std::size_t) fe.numQuadraturePoint(); ig++ ) {

        for( std::size_t idof=0 ; idof < (std::size_t) fe.numDof() ; idof++ ) {
          for( std::size_t jdof=0 ; jdof < (std::size_t) fe.numDof() ; jdof++ ) {

            // coef * (phi_i \cdot \n_icoor) * (phi_j \cdot \n_jcoor)
            tmpBlock(idof,jdof) +=   coef * fe.weightMeas(ig) * fe.normal[ig](icoor)  * fe.phi[ig](idof) * fe.normal[ig](jcoor)  * fe.phi[ig](jdof);
          }
        }
      }
      UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+jcoor);
      matrix += tmpBlock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::phi_i_dot_t_phi_j_dot_t(const double coef, const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const int numComp)
{
  // IGNORE_UNUSED_ARGUMENT(numComp);

  m_tmpMat.clear();
  for( std::size_t icoor=0 ; icoor < (std::size_t) fe.numCoor() ; icoor++ ) {
    for( std::size_t jcoor=0 ; jcoor < (std::size_t) fe.numCoor() ; jcoor++ ) {

      UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock+jcoor);

      for( std::size_t ig=0; ig < (std::size_t) fe.numQuadraturePoint(); ig++ ) {

        for( std::size_t idof=0 ; idof < (std::size_t) fe.numDof() ; idof++ ) {
          for( std::size_t jdof=0 ; jdof < (std::size_t) fe.numDof() ; jdof++ ) {

            for( int ic=0 ; ic < numComp ; ic++ ) {
              // coef * (phi_i \cdot \t_icoor) * (phi_j \cdot \t_jcoor)
              tmpBlock(idof,jdof) += coef * fe.weightMeas(ig) * fe.tangent[ig](ic,icoor) * fe.phi[ig](idof) * fe.tangent[ig](ic,jcoor) * fe.phi[ig](jdof);
            }
          }
        }
        UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+jcoor);
        matrix += tmpBlock;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::eps_phi_j_n_dot_n_phi_i_dot_n(const double coeff, const CurrentFiniteElementWithBd& fe,int iblockBd,int numblockBd,const std::size_t iblock,const std::size_t jblock,std::size_t /*num*/)
{
  //matteo feb 14
  FEL_ASSERT(iblockBd>=0 && iblockBd<fe.numBdEle() && (numblockBd-iblockBd) <= fe.numBdEle() );
  m_tmpMat.clear();

  for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
    FEL_ASSERT( fe.bdEle(ibd).hasNormal() && fe.hasFirstDeriv() );
    for( std::size_t icoor=0 ; icoor < (std::size_t) fe.numCoor() ; icoor++ ) {
      for( std::size_t jcoor=0 ; jcoor < (std::size_t) fe.numCoor() ; jcoor++ ) {
        UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,jblock+jcoor);
        //std::cout<<" nRow "<<numBlockRow()<<" nCol "<<numBlockRow()<<std::endl;
        for( std::size_t jdof=0 ; jdof < (std::size_t) fe.numDof() ; jdof++ ) { // number of dof of the volume element
          for( std::size_t idof=0 ; idof < (std::size_t) fe.bdEle(ibd).numDof() ; idof++ ) { //number of dof of the surface element
            for( int ilocg=0; ilocg<fe.numQuadPointBdEle(ibd); ilocg++) {
              int ig = ilocg+fe.indexQuadPoint(ibd+1); // face ibd starts at ibd+1 (0 for internal quad points)
              for( std::size_t jaux=0 ; jaux < (std::size_t) fe.numCoor() ; jaux++ ) {
                //std::cout<<"(i,j) ("<<idof<<","<<jdof<<")"<<std::endl;
                tmpBlock(idof,jdof) +=  coeff * fe.bdEle(ibd).weightMeas(ilocg)  * fe.dPhi[ig](jaux,jdof) * fe.bdEle(ibd).normal[ilocg](jaux)
                                        * fe.bdEle(ibd).normal[ilocg](jcoor) * fe.bdEle(ibd).normal[ilocg](icoor) * fe.bdEle(ibd).phi[ilocg](idof);
              }
            }
          }
        }
        //std::cout << " tmpBlock " << tmpBlock << std::endl;
        UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+jcoor);
        matrix += tmpBlock;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_phi_i_dot_n(const double coeff,const CurvilinearFiniteElement& feVel, const CurvilinearFiniteElement& fePres,const std::size_t iblock,std::size_t presBlock,std::size_t /*num*/)
{
  // Matteo feb 14
  m_tmpMat.clear();
  for( std::size_t icoor=0 ; icoor < (std::size_t) feVel.numCoor() ; icoor++ ) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,presBlock);
    for( std::size_t idof=0 ; idof < (std::size_t) feVel.numDof() ; idof++ ) {
      for( std::size_t jdof=0 ; jdof < (std::size_t) fePres.numDof() ; jdof++ ) {
        for( std::size_t ig=0; ig < (std::size_t) feVel.numQuadraturePoint(); ig++ ) {
          tmpBlock(idof,jdof) +=   coeff * feVel.weightMeas(ig) * feVel.normal[ig](icoor)  * feVel.phi[ig](idof) * fePres.phi[ig](jdof);
        }
      }
    }
    UBlasMatrixRange matrix = matBlock(iblock+icoor,presBlock);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

//this type of elementmatrix is rectangular and it is used in EmbedFSI boundary condition (matteo feb 2014)
ElementMatrix::ElementMatrix(const CurBaseFiniteElement& feVel1, const CurBaseFiniteElement& feVel2, std::size_t nbr1, std::size_t nbc1,
                              const CurBaseFiniteElement& fePres, std::size_t nbr2, std::size_t nbc2) :
  m_mat( feVel2.numDof()*nbr1 + fePres.numDof()*nbr2, feVel1.numDof()*nbc1 + fePres.numDof()*nbc2 ),
  m_tmpMat( feVel2.numDof()*nbr1 + fePres.numDof()*nbr2, feVel1.numDof()*nbc1 + fePres.numDof()*nbc2 )
{

  //
  m_numRow.resize( nbr1 + nbr2 );
  m_firstRow.resize( nbr1 + nbr2 );
  m_numCol.resize( nbc1 + nbc2 );
  m_firstCol.resize( nbc1 + nbc2 );
  //
  std::size_t first=0;
  for (std::size_t n=0; n<nbr1; n++) {
    m_numRow[ n ]=feVel2.numDof();
    m_firstRow[ n ]=first;
    first += feVel2.numDof();
  }
  for (std::size_t n=nbr1; n<nbr1+nbr2; n++) {
    m_numRow[ n ]=fePres.numDof();
    m_firstRow[ n ]=first;
    first += fePres.numDof();
  }
  //
  first=0;
  for (std::size_t n=0; n<nbc1; n++) {
    m_numCol[ n ]=feVel1.numDof();
    m_firstCol[ n ]=first;
    first += feVel1.numDof();
  }
  for (std::size_t n=nbc1; n<nbc1 + nbc2; n++) {
    m_numCol[ n ]=fePres.numDof();
    m_firstCol[ n ]=first;
    first += fePres.numDof();
  }
  m_mat.clear();
  m_tmpMat.clear();
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_phi_i_dot_n_on_nodes( const double coeff, const ElementField& normal, const CurvilinearFiniteElement& feVel, const CurvilinearFiniteElement& fePres,const std::size_t iblock,std::size_t presBlock)
{
  FEL_ASSERT( normal.type() == DOF_FIELD );

  m_tmpMat.clear();
  for( std::size_t icoor=0 ; icoor < (std::size_t) feVel.numCoor() ; icoor++ ) {
    UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat,iblock+icoor,presBlock);
    for( std::size_t idof=0 ; idof < (std::size_t) feVel.numDof() ; idof++ ) {
      for( std::size_t jdof=0 ; jdof < (std::size_t) fePres.numDof() ; jdof++ ) {
        for( std::size_t ig=0; ig < (std::size_t) feVel.numQuadraturePoint(); ig++ ) {
          tmpBlock(idof,jdof) +=   coeff * feVel.weightMeas(ig) * normal.val( icoor, jdof )  * feVel.phi[ig](idof) * fePres.phi[ig](jdof);
        }
      }
    }
    UBlasMatrixRange matrix = matBlock(iblock+icoor,presBlock);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::sGrad_psi_j_tensor_sGrad_phi_i(const double coef, const ElementField& normalField, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor)
{
  for ( int solLocDof(0); solLocDof < fe.numDof(); solLocDof++) {
    UBlasVector gradSol(2);
    gradSol=Curvatures::testFuncGradient(solLocDof);

    UBlasVector KgradSol(2);
    KgradSol(0)=0.;
    KgradSol(1)=0.;
    KgradSol = prod(tensor, gradSol );

    for ( int testLocDof(0); testLocDof < fe.numDof(); testLocDof++) {

      UBlasVector gradTest(2);
      gradTest=Curvatures::testFuncGradient(testLocDof);

      double val = coef*inner_prod(KgradSol, gradTest) * fe.measure(); //TODO only P1! we should integrate, but gradient is constant over the element
      for ( int solCoor(0); solCoor < fe.numCoor(); solCoor++) {
        for ( int testCoor(0); testCoor < fe.numCoor(); testCoor++) {
          double value = val* normalField.val(testCoor,testLocDof)* normalField.val(solCoor,testLocDof);
          UBlasMatrixRange matrix = matBlock(solCoor,testCoor);
          matrix(solLocDof,testLocDof) += value;
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::sGrad_psi_j_tensor_sGrad_phi_i_for_scalar(const double coef, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor, const std::size_t blockId)
{
  for ( int solLocDof(0); solLocDof < fe.numDof(); solLocDof++) {
    UBlasVector gradSol(2);
    gradSol=Curvatures::testFuncGradient(solLocDof);
    UBlasVector KgradSol(2);
    KgradSol(0)=0.;
    KgradSol(1)=0.;
    KgradSol = prod(tensor, gradSol );
    for ( int testLocDof(0); testLocDof < fe.numDof(); testLocDof++) {
      UBlasVector gradTest(2);
      gradTest=Curvatures::testFuncGradient(testLocDof);
      double val = coef*inner_prod(KgradSol, gradTest) * fe.measure(); //TODO only P1! we should integrate, but gradient is constant over the element
      UBlasMatrixRange matrix = matBlock(blockId,blockId);
      matrix(solLocDof,testLocDof) += val;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::sGrad_psi_j_tensor_sGrad_phi_i(const double coef, const ElementField& elemCoef, const ElementField& normalField, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor)
{
  switch(elemCoef.type()) {
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(elemCoef.val,fe.phi[ig]);
        m_tmpVecQuadPoint(ig) = m_tmpVecCoor(0);
      }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("this case has not been implemented");
      break;
  }

  for ( int solLocDof(0); solLocDof < fe.numDof(); solLocDof++) {
    UBlasVector gradSol(2);
    gradSol=Curvatures::testFuncGradient(solLocDof);

    UBlasVector KgradSol(2);
    KgradSol(0)=0.;
    KgradSol(1)=0.;
    KgradSol = prod(tensor, gradSol );

    for ( int testLocDof(0); testLocDof < fe.numDof(); testLocDof++) {
      UBlasVector gradTest(2);
      gradTest=Curvatures::testFuncGradient(testLocDof);

      double val = 0.0;
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
        val += coef * m_tmpVecQuadPoint(ig) * inner_prod(KgradSol, gradTest) * fe.weightMeas(ig); //TODO only P1! we should integrate, but gradient is constant over the element
      for ( int solCoor(0); solCoor < fe.numCoor(); solCoor++) {
        for ( int testCoor(0); testCoor < fe.numCoor(); testCoor++) {
          double value = val* normalField.val(testCoor,testLocDof)* normalField.val(solCoor,testLocDof);
          UBlasMatrixRange matrix = matBlock(solCoor,testCoor);
          matrix(solLocDof,testLocDof) += value;
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::dpsi_j_dh_psi_i(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,std::size_t h, const std::size_t iblock, const std::size_t jblock)
{
  m_tmpMat.clear();

  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fei.numQuadraturePoint(); ig++) {
    for(int idof=0; idof<fei.numDof(); idof++) {
      for(int jdof=0; jdof<fej.numDof(); jdof++) {
        tmpBlock(idof,jdof) += coef*fei.weightMeas(ig) * fei.phi[ig](idof) * fej.dPhi[ig](h,jdof);
      }
    }
  }
  UBlasMatrixRange block = matBlock(iblock,jblock);
  block+=tmpBlock;
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::transpirationSurfStab( const double coef, const ElementField& elemCoef, const ElementField& normalField, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor)
{
  // Tensor is supposed to be the inverse matric tensor
  // TODO compute directly it from fe!
  switch(elemCoef.type()) {
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(elemCoef.val,fe.phi[ig]);
        m_tmpVecQuadPoint(ig) = m_tmpVecCoor(0)*coef;
      }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("this case has not been implemented");
      break;
  }

  // Only P1
  double coeffQuad = 0.0;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    coeffQuad +=  m_tmpVecQuadPoint(ig) * fe.weightMeas(ig) * fe.diameter() ;
  }

  for ( int solLocDof(0); solLocDof < fe.numDof(); solLocDof++) {
    for ( int testLocDof(0); testLocDof < fe.numDof(); testLocDof++) {
      // Computation of (\nabla_c\phi_test)^T A^-1 \nabla_c\phi_sol
      UBlasVector gradSol(2);
      UBlasVector gradTest(2);
      UBlasVector KgradSol(2);
      KgradSol(0)=0.;
      KgradSol(1)=0.;
      gradSol  = Curvatures::testFuncGradient(solLocDof);
      gradTest = Curvatures::testFuncGradient(testLocDof);
      KgradSol = prod(tensor, gradSol );

      const double lap = inner_prod(KgradSol, gradTest);

      for ( int solCoor(0); solCoor < fe.numCoor(); solCoor++) {
        for ( int testCoor(0); testCoor < fe.numCoor(); testCoor++) {

          double projCoef = 0;
          for ( int ausCoor(0); ausCoor < fe.numCoor(); ++ausCoor ) {
            double deltaTest(0), deltaSol(0);
            if ( solCoor - ausCoor == 0 )
              deltaSol = 1;
            if ( testCoor - ausCoor == 0 )
              deltaTest = 1;
            // setting normnorm
            double normnormSol  = normalField.val( solCoor, solLocDof)*normalField.val(ausCoor, solLocDof);
            double normnormTest = normalField.val(testCoor,testLocDof)*normalField.val(ausCoor,testLocDof);
            projCoef += ( deltaSol - normnormSol ) * ( deltaTest - normnormTest );
          }

          const double value = projCoef * lap * coeffQuad;

          UBlasMatrixRange matrix = matBlock(solCoor,testCoor);
          matrix(solLocDof,testLocDof) += value;
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::velocityConstraint(const double coef, const ElementField& disp, const CurvilinearFiniteElement& fe, int iblock)
{
  FEL_CHECK(fe.hasMeas() ,"ElementMatrix::velocityConstraint: requires covariant basis");
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementMatrix::velocityConstraint: wrong displacement field type");

  const UBlasMatrix& d = disp.val;

  UBlasMatrix cbasis(fe.numRefCoor(),fe.numCoor()); // covariant basis in current configuation
  UBlasVector n(fe.numCoor()); // normal std::vector, not unitary

  double sum;

  // loop on quadrature points
  for (int iq=0; iq < fe.numQuadraturePoint(); ++iq) {
    // covariant basis in current configuratgion
    for (int ir=0; ir < fe.numRefCoor(); ++ir) {
      for (int ic=0; ic <fe.numCoor(); ++ic) {
        cbasis(ir,ic) = fe.m_covBasis[iq](ir,ic);
        for(int idof=0; idof< fe.numDof(); idof++)
          cbasis(ir,ic) += d(ic,idof)*fe.dPhiRef(iq)(ir,idof);
      }
    }

    switch ( fe.numRefCoor() ) { // Warning: must be as in CurvilinearFiniteElement::computeNormal()
      case 1:
        n(0) =   cbasis(0,1);
        n(1) = - cbasis(0,0);
        break;
      case 2:
        n(0) = fe.m_sign * ( cbasis(0,1) * cbasis(1,2) - cbasis(0,2)*cbasis(1,1) );
        n(1) = fe.m_sign * ( cbasis(0,2) * cbasis(1,0) - cbasis(0,0)*cbasis(1,2) );
        n(2) = fe.m_sign * ( cbasis(0,0) * cbasis(1,1) - cbasis(0,1)*cbasis(1,0) );
        break;
      default:
        FEL_ERROR("ElementMatrix::velocityConstraint numRefCoor must be 1 or 2");
    }

    // Update elementary matrix
    for (int ic=0; ic < fe.numCoor(); ++ic) {
        UBlasMatrixRange matrix1 = matBlock(ic,iblock);
        UBlasMatrixRange matrix2 = matBlock(iblock,ic);
        for(int idof=0 ; idof <  fe.numDof() ; ++idof) {
          sum = fe.quadratureRule().weight(iq) * n(ic) * fe.phi[iq](idof);
          matrix1(idof,0) += coef*sum;
          matrix2(0,idof) += coef*sum;
        }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::followerPressure(const double coef, const ElementField& press, const ElementField& disp, const CurvilinearFiniteElement& fe)
{
  FEL_CHECK(fe.hasMeas() ,"ElementMatrix::followerPressure: requires covariant basis");
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementMatrix::followerPressure: wrong displacement field type");
  FEL_CHECK(press.numComp() == 1,"ElementMatrix::followerPressure: wrong pressure field");

  const UBlasMatrix& d = disp.val;

  UBlasMatrix cbasis(fe.numRefCoor(),fe.numCoor()); // covariant basis in current configuation
  UBlasVector n(fe.numCoor()); // normal std::vector, not unitary
  UBlasVector patq(fe.numQuadraturePoint()); // pressure at quadrature poins
  std::vector<UBlasMatrix> A(fe.numRefCoor());
  for (int ir=0; ir<fe.numRefCoor(); ++ir) {
    A[ir].resize(fe.numCoor(),fe.numCoor());
    A[ir].clear();
  }
  double tmp;

  // evaluate pressure at quadrature points
  switch(press.type()) {
    case CONSTANT_FIELD:
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq)
        patq(iq) = press.val(0,0);
      break;
    case QUAD_POINT_FIELD:
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq)
        patq(iq) = press.val(0,iq);
      break;
    case DOF_FIELD:
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq) {
        patq(iq)  = 0.;
          for(int idof=0; idof<fe.numDof(); ++idof)
            patq(iq) +=  fe.phi[iq](idof) * press.val(0,idof);
      }
      break;
  default:
    FEL_ERROR("this pressure element field type has not been implemented");
  }

  // loop on quadrature points
  for (int iq=0; iq < fe.numQuadraturePoint(); ++iq) {

    // covariant basis in current configuratgion
    for (int ir=0; ir < fe.numRefCoor(); ++ir) {
        for (int ic=0; ic <fe.numCoor(); ++ic) {
          cbasis(ir,ic) = fe.m_covBasis[iq](ir,ic);
          for(int idof=0; idof< fe.numDof(); idof++)
            cbasis(ir,ic) += d(ic,idof)*fe.dPhiRef(iq)(ir,idof);
        }
    }

    switch ( fe.numRefCoor() ) {
      case 1:
          A[0](0,1) = 1;
          A[0](1,0) = -1;
          break;
      case 2:
          // - A1
          A[1](1,0) =   cbasis(0,2);
          A[1](2,0) = - cbasis(0,1);
          A[1](0,1) = - cbasis(0,2);
          A[1](2,1) =   cbasis(0,0);
          A[1](0,2) =   cbasis(0,1);
          A[1](1,2) = - cbasis(0,0);
          A[1] *= fe.m_sign;
          // A2
          A[0](1,0) = - cbasis(1,2);
          A[0](2,0) =   cbasis(1,1);
          A[0](0,1) =   cbasis(1,2);
          A[0](2,1) = - cbasis(1,0);
          A[0](0,2) = - cbasis(1,1);
          A[0](1,2) =   cbasis(1,0);
          A[0] *= fe.m_sign;
        break;
    default:
      FEL_ERROR("ElementMatrix::followerPressure: numRefCoor must be 1 or 2");
    }

    for (int ic=0; ic < fe.numCoor(); ++ic)
      for (int jc=0; jc < fe.numCoor(); ++jc) {
        UBlasMatrixRange mat =  matBlock(ic,jc);
        for (int ir=0; ir < fe.numRefCoor(); ++ir) {
          tmp = coef*patq(iq)*A[ir](ic,jc)*fe.quadratureRule().weight(iq);
          for(int idof=0; idof< fe.numDof(); idof++)
            for(int jdof=0; jdof< fe.numDof(); jdof++)
              mat(idof,jdof) += tmp * fe.dPhiRef(iq)(ir,jdof) * fe.phi[iq](idof);
          }
      }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_phi_i(const double coef, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock,const std::size_t num)
{
  // check that the number of integration point is the same
  FEL_ASSERT(fePhi.numQuadraturePoint() == fePsi.numQuadraturePoint());

  m_tmpMat.clear();
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fePhi.numQuadraturePoint(); ig++) {
    tmpBlock += ( coef * fePhi.weightMeas(ig) ) * outer_prod(fePhi.phi[ig], fePsi.phi[ig]);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock+icoor,jblock+icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_eps_phi_i_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "psi_j_eps_phi_i_dot_vec is not implemented for this type of vec");
  for(std::size_t icoor=0; icoor<num; ++icoor) {
    for(std::size_t jcoor=0; jcoor<num; ++jcoor) {
      m_tmpMat.clear();
      UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

      if(icoor == jcoor) {
        for(felInt idof=0; idof<fePhi.numDof(); ++idof)
          for(felInt jdof=0; jdof<fePhi.numDof(); ++jdof)
            for(felInt ig=0; ig<fePhi.numQuadraturePoint(); ++ig)
              for(felInt kcoor=0; kcoor<fePhi.numRefCoor(); ++kcoor)
                tmpblock(idof, jdof) += 0.5 * coef * fePhi.dPhi[ig](kcoor, idof) * vec.val(kcoor, 0) * fePsi.phi[ig](jdof) * fePhi.weightMeas(ig);
      }

      for(felInt idof=0; idof<fePhi.numDof(); ++idof)
        for(felInt jdof=0; jdof<fePhi.numDof(); ++jdof)
          for(felInt ig=0; ig<fePhi.numQuadraturePoint(); ++ig)
            tmpblock(idof, jdof) += 0.5 * coef * fePhi.dPhi[ig](icoor, idof) * vec.val(icoor, 0) * fePsi.phi[ig](jdof) * fePhi.weightMeas(ig);

      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);
      matrix += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::psi_j_grad_phi_i_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "psi_j_grad_phi_i_dot_vec is not implemented for this type of vec");

  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for(felInt idof=0; idof<fePhi.numDof(); ++idof)
    for(felInt jdof=0; jdof<fePhi.numDof(); ++jdof)
      for(felInt ig=0; ig<fePhi.numQuadraturePoint(); ++ig)
        for(felInt icoor=0; icoor<fePhi.numRefCoor(); ++icoor)
          tmpblock(idof, jdof) += coef * fePhi.dPhi[ig](icoor, idof) * vec.val(icoor, 0) * fePsi.phi[ig](jdof) * fePhi.weightMeas(ig);


  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::eps_psi_j_dot_vec_phi_i(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "eps_psi_j_dot_vec_phi_i is not implemented for this type of vec");

  for(std::size_t icoor=0; icoor<num; ++icoor) {
    for(std::size_t jcoor=0; jcoor<num; ++jcoor) {
      m_tmpMat.clear();
      UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

      if(icoor == jcoor) {
        for(felInt idof=0; idof<fePhi.numDof(); ++idof)
          for(felInt jdof=0; jdof<fePsi.numDof(); ++jdof)
            for(felInt ig=0; ig<fePhi.numQuadraturePoint(); ++ig)
              for(felInt kcoor=0; kcoor<fePhi.numRefCoor(); ++kcoor)
                tmpblock(idof, jdof) += 0.5 * coef * fePsi.dPhi[ig](kcoor, jdof) * vec.val(kcoor, 0) * fePhi.phi[ig](idof) * fePhi.weightMeas(ig);
      }

      for(felInt idof=0; idof<fePhi.numDof(); ++idof)
        for(felInt jdof=0; jdof<fePhi.numDof(); ++jdof)
          for(felInt ig=0; ig<fePhi.numQuadraturePoint(); ++ig)
            tmpblock(idof, jdof) += 0.5 * coef * fePsi.dPhi[ig](icoor, jdof) * vec.val(jcoor, 0) * fePhi.phi[ig](idof) * fePhi.weightMeas(ig);

      UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + jcoor);
      matrix += tmpblock;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::grad_psi_j_dot_vec_phi_i(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num)
{
  FEL_CHECK(vec.type() == CONSTANT_FIELD, "grad_psi_j_dot_vec_phi_i is not implemented for this type of vec");

  m_tmpMat.clear();
  UBlasMatrixRange tmpblock = getMatBlock(m_tmpMat, iblock, jblock);

  for(felInt idof=0; idof<fePhi.numDof(); ++idof)
    for(felInt jdof=0; jdof<fePsi.numDof(); ++jdof)
      for(felInt ig=0; ig<fePhi.numQuadraturePoint(); ++ig)
        for(felInt icoor=0; icoor<fePhi.numRefCoor(); ++icoor)
          tmpblock(idof, jdof) += coef * fePsi.dPhi[ig](icoor, jdof) * vec.val(icoor, 0) * fePhi.phi[ig](idof) * fePhi.weightMeas(ig);


  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpblock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementMatrix::f_dot_n_psi_j_phi_i(const double coef, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const ElementField& elt_f, const ElementField& elt_n, const std::size_t iblock, const std::size_t jblock, const std::size_t num, felInt stabFlag)
{
  m_tmpMat.clear();
  double tmp, f_dot_n;
  UBlasMatrixRange tmpBlock = getMatBlock(m_tmpMat, iblock, jblock);
  for(int ig=0; ig<fePhi.numQuadraturePoint(); ig++) {
    f_dot_n = 0.;
    for(int icomp=0; icomp<fePhi.numCoor(); icomp++) {
      tmp = 0.;
      for(int jdof=0; jdof<fePhi.numDof(); jdof++) {
        tmp += fePhi.phi[ig](jdof) * elt_f.val(icomp, jdof);
      }
      f_dot_n += tmp * elt_n.val(icomp, 0);
    }

    if (f_dot_n < 0 || stabFlag != 2)
      tmpBlock += f_dot_n * coef * fePhi.weightMeas(ig) * outer_prod(fePhi.phi[ig], fePsi.phi[ig]);
  }

  for(std::size_t icoor=0; icoor<num; icoor++) {
    UBlasMatrixRange matrix = matBlock(iblock + icoor, jblock + icoor);
    matrix += tmpBlock;
  }
}

/***********************************************************************************/
/***********************************************************************************/

// phi_i is the test function
void ElementMatrix::const_phi_i_dot_n(const double coeff,const CurvilinearFiniteElement& fePhi, const std::size_t iBlock, const std::size_t jBlock)
{
  double tmp;
  for (int ic=0; ic<fePhi.numCoor(); ++ic) {
    UBlasMatrixRange matrix = matBlock(iBlock+ic,jBlock);
    for (int idof=0; idof<fePhi.numDof(); ++idof) {
      tmp = 0.;
      for (int iq=0; iq<fePhi.numQuadraturePoint(); ++iq)
        tmp += coeff * fePhi.weightMeas(iq) * fePhi.normal[iq](ic) * fePhi.phi[iq](idof);

      matrix(idof,0) += tmp;
    }
  }
}

// const is the test function
void ElementMatrix::const_phi_j_dot_n(const double coeff,const CurvilinearFiniteElement& fePhi, const std::size_t iBlock, const std::size_t jBlock)
{
  double tmp;
  for (int ic=0; ic<fePhi.numCoor(); ++ic) {
    UBlasMatrixRange matrix = matBlock(iBlock,jBlock+ic);
    for (int idof=0; idof<fePhi.numDof(); ++idof) {
      tmp = 0.;
      for (int iq=0; iq<fePhi.numQuadraturePoint(); ++iq)
        tmp += coeff * fePhi.weightMeas(iq) * fePhi.normal[iq](ic) * fePhi.phi[iq](idof);

      matrix(0,idof) += tmp;
    }
  }
}

// phi_i is the test function
void ElementMatrix::const_phi_i(const double coeff, const CurvilinearFiniteElement& fePhi, const std::size_t iBlock, const std::size_t jBlock)
{
  double tmp;
  UBlasMatrixRange matrix = matBlock(iBlock,jBlock);
  for (int idof=0; idof<fePhi.numDof(); ++idof) {
    tmp = 0.;
    for (int iq=0; iq<fePhi.numQuadraturePoint(); ++iq)
      tmp += coeff * fePhi.weightMeas(iq) * fePhi.phi[iq](idof);

    matrix(idof,0) += tmp;
  }
}

// const is the test function
void ElementMatrix::const_phi_j(const double coeff,const CurvilinearFiniteElement& fePhi, const std::size_t iBlock, const std::size_t jBlock)
{
  double tmp;
  UBlasMatrixRange matrix = matBlock(iBlock,jBlock);
  for (int idof=0; idof<fePhi.numDof(); ++idof) {
    tmp = 0.;
    for (int iq=0; iq<fePhi.numQuadraturePoint(); ++iq)
      tmp += coeff * fePhi.weightMeas(iq) * fePhi.phi[iq](idof);

    matrix(0,idof) += tmp;
  }
}


}
