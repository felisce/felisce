//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

// System includes

// External includes

// Project includes
#include "FiniteElement/listCurrentFiniteElement.hpp"

namespace felisce 
{
void ListCurrentFiniteElement::add(CurrentFiniteElement* fe) {
  m_listCurrentFiniteElement.push_back(fe);
}

/***********************************************************************************/
/***********************************************************************************/

//! Print function
void ListCurrentFiniteElement::print(int verbose,std::ostream& c) const {
  std::cout << "List of Finite Element: " << std::endl;
  for ( unsigned int iFe = 0; iFe < m_listCurrentFiniteElement.size(); iFe++)
    m_listCurrentFiniteElement[iFe]->print(verbose,c);
  std::cout << std::endl;
}
}
