//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes
#include <iomanip>

// External includes

// Project includes
#include "FiniteElement/basisFunction.hpp"

namespace felisce
{

BasisFunction::BasisFunction(const std::string& name,int size,int numCoor, const FunctionXYZ* phi, const FunctionXYZ* dPhi,const FunctionXYZ* d2Phi):
  m_name(name),m_size(size),m_numCoor(numCoor),m_dim(1),m_phi( phi ), m_dPhi( dPhi ), m_d2Phi( d2Phi ) {
  FEL_DEBUG("Constructing " << m_name << std::endl);
}

/***********************************************************************************/
/***********************************************************************************/

BasisFunction::BasisFunction(const std::string& name,int size,int numCoor, int dim, const FunctionXYZ* phi, const FunctionXYZ* dPhi,const FunctionXYZ* d2Phi):
  m_name(name),m_size(size),m_numCoor(numCoor),m_dim(dim),m_phi( phi ), m_dPhi( dPhi ), m_d2Phi( d2Phi ) {
  FEL_DEBUG("Constructing " << m_name << std::endl);
}

/***********************************************************************************/
/***********************************************************************************/

void BasisFunction::print(int verbose,std::ostream& c) const {
  if(verbose) {
    c << "BasisFunction: " << std::endl;
    c << " name: " << m_name << std::endl;
    c << " size: " << m_size << " (number of basis functions)" << std::endl;
    c << " numCoor: " << m_numCoor << " (number of coordinates)" << std::endl;
    c << " dim: " << m_dim << " (number of scalar components of the functions)" << std::endl;
  }
}

#define _JFG_OUT_

#ifndef _JFG_OUT_
  //======================================================================
  //
  //                            P2  (1D)
  //
  //======================================================================
  /*
   1--3--2
   */
  const double fct1_P2_1D( const double x, const double, const double ) {
    return 2. * ( x - 1. ) * ( x - 0.5 );
  }
  const double fct3_P2_1D( const double x, const double, const double ) {
    return 4. * x * ( 1. - x );
  }
  const double fct2_P2_1D( const double x, const double, const double ) {
    return 2. * x * ( x - 0.5 );
  }

  const double derfct1_1_P2_1D( const double x, const double, const double ) {
    return 4. * x - 3.;
  }
  const double derfct3_1_P2_1D( const double x, const double, const double ) {
    return -8. * x + 4.;
  }
  const double derfct2_1_P2_1D( const double x, const double, const double ) {
    return 4. * x - 1.;
  }

  const double der2fct1_11_P2_1D(const Point&) {
    return 4;
  }
  const double der2fct3_11_P2_1D(const Point&) {
    return -8;
  }
  const double der2fct2_11_P2_1D(const Point&) {
    return 4;
  }

  static const FunctionXYZ fct_P2_1D[ 3 ] = {
    fct1_P2_1D, fct2_P2_1D, fct3_P2_1D
  }
  static const FunctionXYZ derfct_P2_1D[ 3 ] = {
    derfct1_1_P2_1D, derfct2_1_P2_1D, derfct3_1_P2_1D
  }
  static const FunctionXYZ der2fct_P2_1D[ 3 ] = {
    der2fct1_11_P2_1D, der2fct2_11_P2_1D, der2fct3_11_P2_1D
  }

  const BasisFunction basisFunctionP2Seg(3,1,fct_P2_1D,derfct_P2_1D,der2fct_P2_1D);


  //======================================================================
  //
  //                            P0  (2D)
  //
  //======================================================================
  /*

   |\
   | \
   | 1\
   ---
   */
  const double fct1_P0_2D(const Point&) {
    return 1. ;
  }

  const double derfct1_P0_2D(const Point&) {
    return 0. ;
  }
  const double der2fct1_P0_2D(const Point&) {
    return 0. ;
  }


  static const FunctionXYZ fct_P0_2D[ 1 ] = {
    fct1_P0_2D
  }

  static const FunctionXYZ derfct_P0_2D[ 2 ] = {
    derfct1_P0_2D, derfct1_P0_2D
  }

  static const FunctionXYZ der2fct_P0_2D[ 4 ] = {
    der2fct1_P0_2D, der2fct1_P0_2D,
    der2fct1_P0_2D, der2fct1_P0_2D
  }

  const BasisFunction basisFunctionP0Tria(1,2,fct_P0_2D,derfct_P0_2D,der2fct_P0_2D);

  //======================================================================
  //
  //                            P2  (2D)
  //
  //======================================================================
  /*
   3
   |\
   6 5
   |  \
   1-4-2
   */
  const double fct1_P2_2D( const double x, const double y, const double ) {
    return ( 1 -x - y ) * ( 1 - x - x - y - y );
  }
  const double fct2_P2_2D( const double x, const double, const double ) {
    return -x * ( 1 - x - x );
  }
  const double fct3_P2_2D( const double, const double y, const double ) {
    return -y * ( 1 - y - y );
  }
  const double fct4_P2_2D( const double x, const double y, const double ) {
    return 4 * x * ( 1 - x - y );
  }
  const double fct5_P2_2D( const double x, const double y, const double ) {
    return 4 * x * y;
  }
  const double fct6_P2_2D( const double x, const double y, const double ) {
    return 4 * y * ( 1 - x - y );
  }

  const double derfct1_1_P2_2D( const double x, const double y, const double ) {
    return 4 * ( x + y ) - 3;
  }
  const double derfct1_2_P2_2D( const double x, const double y, const double ) {
    return 4 * ( x + y ) - 3;
  }
  const double derfct2_1_P2_2D( const double x, const double, const double ) {
    return 4 * x - 1;
  }
  const double derfct2_2_P2_2D(const Point&) {
    return 0;
  }
  const double derfct3_1_P2_2D(const Point&) {
    return 0;
  }
  const double derfct3_2_P2_2D( const double, const double y, const double ) {
    return 4 * y - 1;
  }
  const double derfct4_1_P2_2D( const double x, const double y, const double ) {
    return 4 * ( 1 - x - x - y );
  }
  const double derfct4_2_P2_2D( const double x, const double, const double ) {
    return -4 * x;
  }
  const double derfct5_1_P2_2D( const double, const double y, const double ) {
    return 4 * y;
  }
  const double derfct5_2_P2_2D( const double x, const double, const double ) {
    return 4 * x;
  }
  const double derfct6_1_P2_2D( const double, const double y, const double ) {
    return -4 * y;
  }
  const double derfct6_2_P2_2D( const double x, const double y, const double ) {
    return 4 * ( 1 - x - y - y );
  }

  const double der2fct1_11_P2_2D(const Point&) {
    return 4;
  }
  const double der2fct1_12_P2_2D(const Point&) {
    return 4;
  }
  const double der2fct1_21_P2_2D(const Point&) {
    return 4;
  }
  const double der2fct1_22_P2_2D(const Point&) {
    return 4;
  }

  const double der2fct2_11_P2_2D(const Point&) {
    return 4;
  }
  const double der2fct2_12_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct2_21_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct2_22_P2_2D(const Point&) {
    return 0;
  }

  const double der2fct3_11_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct3_12_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct3_21_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct3_22_P2_2D(const Point&) {
    return 4;
  }

  const double der2fct4_11_P2_2D(const Point&) {
    return -8;
  }
  const double der2fct4_12_P2_2D(const Point&) {
    return -4;
  }
  const double der2fct4_21_P2_2D(const Point&) {
    return -4;
  }
  const double der2fct4_22_P2_2D(const Point&) {
    return 0;
  }

  const double der2fct5_11_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct5_12_P2_2D(const Point&) {
    return 4;
  }
  const double der2fct5_21_P2_2D(const Point&) {
    return 4;
  }
  const double der2fct5_22_P2_2D(const Point&) {
    return 0;
  }

  const double der2fct6_11_P2_2D(const Point&) {
    return 0;
  }
  const double der2fct6_12_P2_2D(const Point&) {
    return -4;
  }
  const double der2fct6_21_P2_2D(const Point&) {
    return -4;
  }
  const double der2fct6_22_P2_2D(const Point&) {
    return -8;
  }


  static const FunctionXYZ fct_P2_2D[ 6 ] = {
    fct1_P2_2D, fct2_P2_2D, fct3_P2_2D,
    fct4_P2_2D, fct5_P2_2D, fct6_P2_2D
  }

  static const FunctionXYZ derfct_P2_2D[ 12 ] = {
    derfct1_1_P2_2D, derfct1_2_P2_2D,
    derfct2_1_P2_2D, derfct2_2_P2_2D,
    derfct3_1_P2_2D, derfct3_2_P2_2D,
    derfct4_1_P2_2D, derfct4_2_P2_2D,
    derfct5_1_P2_2D, derfct5_2_P2_2D,
    derfct6_1_P2_2D, derfct6_2_P2_2D
  }
  static const FunctionXYZ der2fct_P2_2D[ 24 ] = {
    der2fct1_11_P2_2D, der2fct1_12_P2_2D, der2fct1_21_P2_2D, der2fct1_22_P2_2D,
    der2fct2_11_P2_2D, der2fct2_12_P2_2D, der2fct2_21_P2_2D, der2fct2_22_P2_2D,
    der2fct3_11_P2_2D, der2fct3_12_P2_2D, der2fct3_21_P2_2D, der2fct3_22_P2_2D,
    der2fct4_11_P2_2D, der2fct4_12_P2_2D, der2fct4_21_P2_2D, der2fct4_22_P2_2D,
    der2fct5_11_P2_2D, der2fct5_12_P2_2D, der2fct5_21_P2_2D, der2fct5_22_P2_2D,
    der2fct6_11_P2_2D, der2fct6_12_P2_2D, der2fct6_21_P2_2D, der2fct6_22_P2_2D
  }

  const BasisFunction basisFunctionP2Tria(6,2,fct_P2_2D,derfct_P2_2D,der2fct_P2_2D);

  //======================================================================
  //
  //                            Q0  (2D)
  //
  //======================================================================
  /*
    -------
   |       |
   |   1   |
   |       |
    -------
   */
  const double fct1_Q0_2D(const Point&) {
    return 1. ;
  }
  const double derfct1_Q0_2D(const Point&) {
    return 0. ;
  }
  // The second derivative is equal to the first : both are equal to 0.
  const double der2fct1_Q0_2D(const Point&) {
    return 0. ;
  }

  static const FunctionXYZ fct_Q0_2D[ 1 ] = {
    fct1_Q0_2D
  }

  static const FunctionXYZ derfct_Q0_2D[ 2 ] = {
    derfct1_Q0_2D, derfct1_Q0_2D
  }

  static const FunctionXYZ der2fct_Q0_2D[ 4 ] = {
    der2fct1_Q0_2D, der2fct1_Q0_2D,
    der2fct1_Q0_2D, der2fct1_Q0_2D
  }

  const BasisFunction basisFunctionP0Quad(1,2,fct_Q0_2D,derfct_Q0_2D,der2fct_Q0_2D);


  //======================================================================
  //
  //                            Q1  (2D)
  //
  //======================================================================
  /*
   4-------3
   |       |
   |       |
   |       |
   1-------2
   */
  const double fct1_Q1_2D( const double x, const double y, const double ) {
    return ( 1. - x ) * ( 1. - y );
  }
  const double fct2_Q1_2D( const double x, const double y, const double ) {
    return ( 1. - y ) * x;
  }
  const double fct3_Q1_2D( const double x, const double y, const double ) {
    return x * y;
  }
  const double fct4_Q1_2D( const double x, const double y, const double ) {
    return y * ( 1. - x );
  }

  const double derfct1_1_Q1_2D( const double, const double y, const double ) {
    return -( 1. - y );
  }
  const double derfct1_2_Q1_2D( const double x, const double, const double ) {
    return -( 1. - x );
  }
  const double derfct2_1_Q1_2D( const double, const double y, const double ) {
    return ( 1. - y );
  }
  const double derfct2_2_Q1_2D( const double x, const double, const double ) {
    return -x;
  }
  const double derfct3_1_Q1_2D( const double, const double y, const double ) {
    return y;
  }
  const double derfct3_2_Q1_2D( const double x, const double, const double ) {
    return x;
  }
  const double derfct4_1_Q1_2D( const double, const double y, const double ) {
    return -y;
  }
  const double derfct4_2_Q1_2D( const double x, const double, const double ) {
    return ( 1. - x );
  }

  // Second derivatives
  const double der2fctx_xx_Q1_2D(const Point&) {
    return 0;
  }


  static const FunctionXYZ fct_Q1_2D[ 4 ] = {
    fct1_Q1_2D, fct2_Q1_2D, fct3_Q1_2D, fct4_Q1_2D
  }

  static const FunctionXYZ derfct_Q1_2D[ 8 ] = {
    derfct1_1_Q1_2D, derfct1_2_Q1_2D,
    derfct2_1_Q1_2D, derfct2_2_Q1_2D,
    derfct3_1_Q1_2D, derfct3_2_Q1_2D,
    derfct4_1_Q1_2D, derfct4_2_Q1_2D
  }
  static const FunctionXYZ der2fct_Q1_2D[ 16 ] = {
    der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D,
    der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D,
    der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D,
    der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D, der2fctx_xx_Q1_2D
  }

  const BasisFunction basisFunctionQ1Quad(4,2,fct_Q1_2D,derfct_Q1_2D,der2fct_Q1_2D);

  //======================================================================
  //
  //                            Q2  (2D)
  //
  //======================================================================
  /*
   4---7---3
   |       |
   8   9   6
   |       |
   1---5---2
   */
  const double fct1_Q2_2D( const double x, const double y, const double ) {
    return 4. * ( 1 - x ) * ( 0.5 - x ) * ( 1 - y ) * ( 0.5 - y );
  }
  const double fct5_Q2_2D( const double x, const double y, const double ) {
    return 8. * x * ( 1 - x ) * ( 1 - y ) * ( 0.5 - y );
  }
  const double fct2_Q2_2D( const double x, const double y, const double ) {
    return 4. * x * ( x - 0.5 ) * ( 1 - y ) * ( 0.5 - y );
  }
  const double fct6_Q2_2D( const double x, const double y, const double ) {
    return 8. * x * ( x - 0.5 ) * y * ( 1 - y );
  }
  const double fct3_Q2_2D( const double x, const double y, const double ) {
    return 4. * x * ( x - 0.5 ) * y * ( y - 0.5 );
  }
  const double fct7_Q2_2D( const double x, const double y, const double ) {
    return 8. * x * ( 1 - x ) * y * ( y - 0.5 );
  }
  const double fct4_Q2_2D( const double x, const double y, const double ) {
    return 4. * ( 1 - x ) * ( 0.5 - x ) * y * ( y - 0.5 );
  }
  const double fct8_Q2_2D( const double x, const double y, const double ) {
    return 8. * ( 0.5 - x ) * ( 1 - x ) * y * ( 1 - y );
  }
  const double fct9_Q2_2D( const double x, const double y, const double ) {
    return 16. * x * ( 1 - x ) * y * ( 1 - y );
  }

  const double derfct1_1_Q2_2D( const double x, const double y, const double ) {
    return ( 2. * y - 1. ) * ( y - 1. ) * ( 4. * x - 3. );
  }
  const double derfct1_2_Q2_2D( const double x, const double y, const double ) {
    return ( 2. * x - 1. ) * ( x - 1. ) * ( 4. * y - 3. );
  }
  const double derfct5_1_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 2. * y - 1. ) * ( y - 1. ) * ( 2. * x - 1. );
  }
  const double derfct5_2_Q2_2D( const double x, const double y, const double ) {
    return -4. * x * ( x - 1. ) * ( 4. * y - 3. );
  }
  const double derfct2_1_Q2_2D( const double x, const double y, const double ) {
    return ( 2. * y - 1. ) * ( y - 1. ) * ( 4. * x - 1. );
  }
  const double derfct2_2_Q2_2D( const double x, const double y, const double ) {
    return x * ( 2. * x - 1. ) * ( 4. * y - 3. );
  }
  const double derfct6_1_Q2_2D( const double x, const double y, const double ) {
    return -4. * y * ( 4. * x - 1. ) * ( y - 1. );
  }
  const double derfct6_2_Q2_2D( const double x, const double y, const double ) {
    return -4. * x * ( 2. * x - 1. ) * ( 2. * y - 1. );
  }
  const double derfct3_1_Q2_2D( const double x, const double y, const double ) {
    return y * ( 4. * x - 1. ) * ( 2. * y - 1. );
  }
  const double derfct3_2_Q2_2D( const double x, const double y, const double ) {
    return x * ( 2. * x - 1. ) * ( 4. * y - 1. );
  }
  const double derfct7_1_Q2_2D( const double x, const double y, const double ) {
    return -4. * y * ( 2. * x - 1. ) * ( 2. * y - 1. );
  }
  const double derfct7_2_Q2_2D( const double x, const double y, const double ) {
    return -4. * x * ( x - 1. ) * ( 4. * y - 1. );
  }
  const double derfct4_1_Q2_2D( const double x, const double y, const double ) {
    return y * ( 4. * x - 3. ) * ( 2. * y - 1. );
  }
  const double derfct4_2_Q2_2D( const double x, const double y, const double ) {
    return ( 2. * x - 1. ) * ( x - 1. ) * ( 4. * y - 1. );
  }
  const double derfct8_1_Q2_2D( const double x, const double y, const double ) {
    return -4. * y * ( 4. * x - 3. ) * ( y - 1. );
  }
  const double derfct8_2_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 2. * x - 1. ) * ( x - 1. ) * ( 2. * y - 1. );
  }
  const double derfct9_1_Q2_2D( const double x, const double y, const double ) {
    return 16. * y * ( 2. * x - 1. ) * ( y - 1. );
  }
  const double derfct9_2_Q2_2D( const double x, const double y, const double ) {
    return 16. * x * ( x - 1. ) * ( 2. * y - 1. );
  }

  const double der2fct1_11_Q2_2D( const double, const double y, const double ) {
    return ( 2. * y - 1. ) * ( y - 1. ) * 4.;
  }
  const double der2fct1_12_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * y - 3. ) * ( 4. * x - 3. );
  }
  const double der2fct1_21_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * y - 3. ) * ( 4. * x - 3. );
  }
  const double der2fct1_22_Q2_2D( const double x, const double, const double ) {
    return ( 2. * x - 1. ) * ( x - 1. ) * 4.;
  }

  const double der2fct5_11_Q2_2D( const double, const double y, const double ) {
    return -8. * ( 2. * y - 1. ) * ( y - 1. );
  }
  const double der2fct5_12_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 2. * x - 1 ) * ( 4. * y - 3 );
  }
  const double der2fct5_21_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 2. * x - 1 ) * ( 4. * y - 3 );
    ;
  }
  const double der2fct5_22_Q2_2D( const double x, const double, const double ) {
    return -16. * x * ( x - 1. );
  }

  const double der2fct2_11_Q2_2D( const double, const double y, const double ) {
    return ( 2. * y - 1. ) * ( y - 1. ) * 4.;
  }
  const double der2fct2_12_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * x - 1 ) * ( 4. * y - 3. );
  }
  const double der2fct2_21_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * y - 3. ) * ( 4. * x - 1. );
  }
  const double der2fct2_22_Q2_2D( const double x, const double, const double ) {
    return x * ( 2. * x - 1. ) * 4.;
  }

  const double der2fct6_11_Q2_2D( const double, const double y, const double ) {
    return -16. * y * ( y - 1. );
  }
  const double der2fct6_12_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 4. * x - 1. ) * ( 2. * y - 1. );
  }
  const double der2fct6_21_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 4. * x - 1. ) * ( 2. * y - 1. );
  }
  const double der2fct6_22_Q2_2D( const double x, const double, const double ) {
    return -8. * x * ( 2. * x - 1. );
  }

  const double der2fct3_11_Q2_2D( const double, const double y, const double ) {
    return 4. * y * ( 2. * y - 1. );
  }
  const double der2fct3_12_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * x - 1. ) * ( 4. * y - 1. );
  }
  const double der2fct3_21_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * x - 1. ) * ( 4. * y - 1. );
  }
  const double der2fct3_22_Q2_2D( const double x, const double, const double ) {
    return 4. * x * ( 2. * x - 1. );
  }

  const double der2fct7_11_Q2_2D( const double, const double y, const double ) {
    return -8. * y * ( 2. * y - 1. );
  }
  const double der2fct7_12_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 2. * x - 1. ) * ( 4. * y - 1. );
  }
  const double der2fct7_21_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 2. * x - 1. ) * ( 4. * y - 1. );
  }
  const double der2fct7_22_Q2_2D( const double x, const double, const double ) {
    return -16. * x * ( x - 1. );
  }

  const double der2fct4_11_Q2_2D( const double, const double y, const double ) {
    return 4. * y * ( 2. * y - 1. );
  }
  const double der2fct4_12_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * x - 3. ) * ( 4. * y - 1. );
  }
  const double der2fct4_21_Q2_2D( const double x, const double y, const double ) {
    return ( 4. * x - 3. ) * ( 4. * y - 1. );
  }
  const double der2fct4_22_Q2_2D( const double x, const double, const double ) {
    return 4. * ( 2. * x - 1. ) * ( x - 1. );
  }

  const double der2fct8_11_Q2_2D( const double, const double y, const double ) {
    return -16. * y * ( y - 1. );
  }
  const double der2fct8_12_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 4. * x - 3. ) * ( 2. * y - 1. );
  }
  const double der2fct8_21_Q2_2D( const double x, const double y, const double ) {
    return -4. * ( 4. * x - 3. ) * ( 2. * y - 1. );
  }
  const double der2fct8_22_Q2_2D( const double x, const double, const double ) {
    return -8. * ( 2. * x - 1. ) * ( x - 1. );
  }

  const double der2fct9_11_Q2_2D( const double, const double y, const double ) {
    return 32. * y * ( y - 1. );
  }
  const double der2fct9_12_Q2_2D( const double x, const double y, const double ) {
    return 16. * ( 2. * x - 1. ) * ( 2. * y - 1. );
  }
  const double der2fct9_21_Q2_2D( const double x, const double y, const double ) {
    return 16. * ( 2. * x - 1. ) * ( 2. * y - 1. );
  }
  const double der2fct9_22_Q2_2D( const double x, const double, const double ) {
    return 32. * x * ( x - 1. );
  }


  static const FunctionXYZ fct_Q2_2D[ 9 ] = {
    fct1_Q2_2D, fct2_Q2_2D, fct3_Q2_2D, fct4_Q2_2D,
    fct5_Q2_2D, fct6_Q2_2D, fct7_Q2_2D, fct8_Q2_2D,
    fct9_Q2_2D
  }


  static const FunctionXYZ derfct_Q2_2D[ 18 ] = {
    derfct1_1_Q2_2D, derfct1_2_Q2_2D,
    derfct2_1_Q2_2D, derfct2_2_Q2_2D,
    derfct3_1_Q2_2D, derfct3_2_Q2_2D,
    derfct4_1_Q2_2D, derfct4_2_Q2_2D,
    derfct5_1_Q2_2D, derfct5_2_Q2_2D,
    derfct6_1_Q2_2D, derfct6_2_Q2_2D,
    derfct7_1_Q2_2D, derfct7_2_Q2_2D,
    derfct8_1_Q2_2D, derfct8_2_Q2_2D,
    derfct9_1_Q2_2D, derfct9_2_Q2_2D
  }

  static const FunctionXYZ der2fct_Q2_2D[ 36 ] = {
    der2fct1_11_Q2_2D, der2fct1_12_Q2_2D, der2fct1_21_Q2_2D, der2fct1_22_Q2_2D,
    der2fct2_11_Q2_2D, der2fct2_12_Q2_2D, der2fct2_21_Q2_2D, der2fct2_22_Q2_2D,
    der2fct3_11_Q2_2D, der2fct3_12_Q2_2D, der2fct3_21_Q2_2D, der2fct3_22_Q2_2D,
    der2fct4_11_Q2_2D, der2fct4_12_Q2_2D, der2fct4_21_Q2_2D, der2fct4_22_Q2_2D,
    der2fct5_11_Q2_2D, der2fct5_12_Q2_2D, der2fct5_21_Q2_2D, der2fct5_22_Q2_2D,
    der2fct6_11_Q2_2D, der2fct6_12_Q2_2D, der2fct6_21_Q2_2D, der2fct6_22_Q2_2D,
    der2fct7_11_Q2_2D, der2fct7_12_Q2_2D, der2fct7_21_Q2_2D, der2fct7_22_Q2_2D,
    der2fct8_11_Q2_2D, der2fct8_12_Q2_2D, der2fct8_21_Q2_2D, der2fct8_22_Q2_2D,
    der2fct9_11_Q2_2D, der2fct9_12_Q2_2D, der2fct9_21_Q2_2D, der2fct9_22_Q2_2D
  }

  const BasisFunction basisFunctionQ2Quad(9,2,fct_Q2_2D,derfct_Q2_2D,der2fct_Q2_2D);


  //======================================================================
  //
  //                            P0  (3D)
  //
  //======================================================================
  /*
         4
        / .
       /  \.3
      /  . \\
     / .    \\
    /.       \!
   1 ----------2
   */
  const double fct1_P0_3D(const Point&) {
    return 1.;
  }

  const double derfct1_P0_3D(const Point&) {
    return 0.;
  }

  // Second derivatives
  const double der2fct1_P0_3D(const Point&) {
    return 0;
  }



  static const FunctionXYZ fct_P0_3D[ 1 ] = {
    fct1_P0_3D
  }

  static const FunctionXYZ derfct_P0_3D[ 3 ] = {
    derfct1_P0_3D, derfct1_P0_3D, derfct1_P0_3D
  }
  static const FunctionXYZ der2fct_P0_3D[ 9 ] = {
    derfct1_P0_3D, derfct1_P0_3D, derfct1_P0_3D,
    derfct1_P0_3D, derfct1_P0_3D, derfct1_P0_3D,
    derfct1_P0_3D, derfct1_P0_3D, derfct1_P0_3D
  }

  const BasisFunction basisFunctionP0Tetra(1,3,fct_P0_3D,derfct_P0_3D,der2fct_P0_3D);

  //======================================================================
  //======================================================================
  //
  //                            P1bubble  (3D)
  //
  //======================================================================
  /*
          4
        / .
       /  \.3
      /  . \\
     / . .5 \\
    /.       \!
   1 ----------2
   */
  const double fct1_P1bubble_3D( const double x, const double y, const double z ) {
    return 1 -x - y - z;
  }
  const double fct2_P1bubble_3D( const double x, const double, const double ) {
    return x;
  }
  const double fct3_P1bubble_3D( const double, const double y, const double ) {
    return y;
  }
  const double fct4_P1bubble_3D( const double, const double, const double z ) {
    return z;
  }
  const double fct5_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -x - y - z ) * x * y * z;
  }

  const double derfct1_1_P1bubble_3D(const Point&) {
    return -1;
  }
  const double derfct1_2_P1bubble_3D(const Point&) {
    return -1;
  }
  const double derfct1_3_P1bubble_3D(const Point&) {
    return -1;
  }
  const double derfct2_1_P1bubble_3D(const Point&) {
    return 1;
  }
  const double derfct2_2_P1bubble_3D(const Point&) {
    return 0;
  }
  const double derfct2_3_P1bubble_3D(const Point&) {
    return 0;
  }
  const double derfct3_1_P1bubble_3D(const Point&) {
    return 0;
  }
  const double derfct3_2_P1bubble_3D(const Point&) {
    return 1;
  }
  const double derfct3_3_P1bubble_3D(const Point&) {
    return 0;
  }
  const double derfct4_1_P1bubble_3D(const Point&) {
    return 0;
  }
  const double derfct4_2_P1bubble_3D(const Point&) {
    return 0;
  }
  const double derfct4_3_P1bubble_3D(const Point&) {
    return 1;
  }
  const double derfct5_1_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -2 * x - y - z ) * y * z;
  }
  const double derfct5_2_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -x - 2 * y - z ) * x * z;
  }
  const double derfct5_3_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -x - y - 2 * z ) * x * y;
  }

  // Second derivatives
  const double der2fctx_xx_P1bubble_3D(const Point&) {
    return 0;
  }
  const double der2fct5_11_P1bubble_3D( const double, const double y, const double z ) {
    return -2 * y * z;
  }
  const double der2fct5_12_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -2 * x - 2 * y - z ) * z;
  }
  const double der2fct5_13_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -2 * x - y - 2 * z ) * y;
  }
  const double der2fct5_21_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -2 * x - 2 * y - z ) * z;
  }
  const double der2fct5_22_P1bubble_3D( const double x, const double, const double z ) {
    return -2 * x * z;
  }
  const double der2fct5_23_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -x - 2 * y - 2 * z ) * x;
  }
  const double der2fct5_31_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -2 * x - y - 2 * z ) * y;
  }
  const double der2fct5_32_P1bubble_3D( const double x, const double y, const double z ) {
    return ( 1 -x - 2 * y - 2 * z ) * x;
  }
  const double der2fct5_33_P1bubble_3D( const double x, const double y, const double ) {
    return -2 * x * y;
  }


  static const FunctionXYZ fct_P1bubble_3D[ 5 ] = {
    fct1_P1bubble_3D, fct2_P1bubble_3D, fct3_P1bubble_3D, fct4_P1bubble_3D, fct5_P1bubble_3D
  }

  static const FunctionXYZ derfct_P1bubble_3D[ 15 ] = {
    derfct1_1_P1bubble_3D, derfct1_2_P1bubble_3D, derfct1_3_P1bubble_3D,
    derfct2_1_P1bubble_3D, derfct2_2_P1bubble_3D, derfct2_3_P1bubble_3D,
    derfct3_1_P1bubble_3D, derfct3_2_P1bubble_3D, derfct3_3_P1bubble_3D,
    derfct4_1_P1bubble_3D, derfct4_2_P1bubble_3D, derfct4_3_P1bubble_3D,
    derfct5_1_P1bubble_3D, derfct5_2_P1bubble_3D, derfct5_3_P1bubble_3D
  }
  static const FunctionXYZ der2fct_P1bubble_3D[ 45 ] = {
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D, der2fctx_xx_P1bubble_3D,
    der2fct5_11_P1bubble_3D, der2fct5_12_P1bubble_3D, der2fct5_13_P1bubble_3D,
    der2fct5_21_P1bubble_3D, der2fct5_22_P1bubble_3D, der2fct5_23_P1bubble_3D,
    der2fct5_31_P1bubble_3D, der2fct5_32_P1bubble_3D, der2fct5_33_P1bubble_3D
  }

  const BasisFunction basisFunction3dP1bubble(5,3,fct_P1bubble_3D,derfct_P1bubble_3D,der2fct_P1bubble_3D);


  //======================================================================
  //
  //                            P2  (3D)
  //
  //======================================================================
  /*
         4
        / .10
       /  \.3
      8  . 9\
     / 7    \6
    /.       \!
   1 -----5----2
   */
  const double fct1_P2_3D( const double x, const double y, const double z ) {
    return -( 1 - x - y - z ) * ( 1 - 2 * ( 1 - x - y - z ) );
  }
  const double fct2_P2_3D( const double x, const double, const double ) {
    return -x * ( 1 - 2 * x );
  }
  const double fct3_P2_3D( const double, const double y, const double ) {
    return -y * ( 1 - 2 * y );
  }
  const double fct4_P2_3D( const double, const double, const double z ) {
    return -z * ( 1 - 2 * z );
  }
  const double fct5_P2_3D( const double x, const double y, const double z ) {
    return 4 * x * ( 1 - x - y - z );
  }
  const double fct6_P2_3D( const double x, const double y, const double ) {
    return 4 * x * y;
  }
  const double fct7_P2_3D( const double x, const double y, const double z ) {
    return 4 * y * ( 1 - x - y - z );
  }
  const double fct8_P2_3D( const double x, const double y, const double z ) {
    return 4 * z * ( 1 - x - y - z );
  }
  const double fct9_P2_3D( const double x, const double, const double z ) {
    return 4 * x * z;
  }
  const double fct10_P2_3D( const double, const double y, const double z ) {
    return 4 * y * z;
  }


  const double derfct1_1_P2_3D( const double x, const double y, const double z ) {
    return -3 + 4 * x + 4 * y + 4 * z;
  }
  const double derfct1_2_P2_3D( const double x, const double y, const double z ) {
    return -3 + 4 * x + 4 * y + 4 * z;
  }
  const double derfct1_3_P2_3D( const double x, const double y, const double z ) {
    return -3 + 4 * x + 4 * y + 4 * z;
  }

  const double derfct2_1_P2_3D( const double x, const double, const double ) {
    return -1 + 4 * x;
  }
  const double derfct2_2_P2_3D(const Point&) {
    return 0.;
  }
  const double derfct2_3_P2_3D(const Point&) {
    return 0.;
  }

  const double derfct3_1_P2_3D(const Point&) {
    return 0.;
  }
  const double derfct3_2_P2_3D( const double, const double y, const double ) {
    return -1 + 4 * y;
  }
  const double derfct3_3_P2_3D(const Point&) {
    return 0.;
  }

  const double derfct4_1_P2_3D(const Point&) {
    return 0.;
  }
  const double derfct4_2_P2_3D(const Point&) {
    return 0.;
  }
  const double derfct4_3_P2_3D( const double, const double, const double z ) {
    return -1 + 4 * z;
  }

  const double derfct5_1_P2_3D( const double x, const double y, const double z ) {
    return 4 - 8 * x - 4 * y - 4 * z;
  }
  const double derfct5_2_P2_3D( const double x, const double, const double ) {
    return -4 * x;
  }
  const double derfct5_3_P2_3D( const double x, const double, const double ) {
    return -4 * x;
  }

  const double derfct6_1_P2_3D( const double, const double y, const double ) {
    return 4 * y;
  }
  const double derfct6_2_P2_3D( const double x, const double, const double ) {
    return 4 * x;
  }
  const double derfct6_3_P2_3D(const Point&) {
    return 0.;
  }

  const double derfct7_1_P2_3D( const double, const double y, const double ) {
    return -4 * y;
  }
  const double derfct7_2_P2_3D( const double x, const double y, const double z ) {
    return 4 - 4 * x - 8 * y - 4 * z;
  }
  const double derfct7_3_P2_3D( const double, const double y, const double ) {
    return -4 * y;
  }

  const double derfct8_1_P2_3D( const double, const double, const double z ) {
    return -4 * z;
  }
  const double derfct8_2_P2_3D( const double, const double, const double z ) {
    return -4 * z;
  }
  const double derfct8_3_P2_3D( const double x, const double y, const double z ) {
    return 4 - 4 * x - 4 * y - 8 * z;
  }

  const double derfct9_1_P2_3D( const double, const double, const double z ) {
    return 4 * z;
  }
  const double derfct9_2_P2_3D(const Point&) {
    return 0.;
  }
  const double derfct9_3_P2_3D( const double x, const double, const double ) {
    return 4 * x;
  }

  const double derfct10_1_P2_3D(const Point&) {
    return 0.;
  }
  const double derfct10_2_P2_3D( const double, const double, const double z ) {
    return 4 * z;
  }
  const double derfct10_3_P2_3D( const double, const double y, const double ) {
    return 4 * y;
  }


  const double der2fct1_11_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_12_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_13_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_21_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_22_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_23_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_31_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_32_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct1_33_P2_3D(const Point&) {
    return 4;
  }

  const double der2fct2_11_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct2_12_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_13_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_21_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_23_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_31_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_32_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct2_33_P2_3D(const Point&) {
    return 0;
  }

  const double der2fct3_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_12_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_13_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_21_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_22_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct3_23_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_31_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_32_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct3_33_P2_3D(const Point&) {
    return 0;
  }

  const double der2fct4_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_12_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_13_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_21_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_23_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_31_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_32_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct4_33_P2_3D(const Point&) {
    return 4;
  }

  const double der2fct5_11_P2_3D(const Point&) {
    return -8;
  }
  const double der2fct5_12_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct5_13_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct5_21_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct5_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct5_23_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct5_31_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct5_32_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct5_33_P2_3D(const Point&) {
    return 0;
  }

  const double der2fct6_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct6_12_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct6_13_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct6_21_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct6_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct6_23_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct6_31_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct6_32_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct6_33_P2_3D(const Point&) {
    return 0;
  }

  const double der2fct7_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct7_12_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct7_13_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct7_21_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct7_22_P2_3D(const Point&) {
    return -8;
  }
  const double der2fct7_23_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct7_31_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct7_32_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct7_33_P2_3D(const Point&) {
    return 0;
  }

  const double der2fct8_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct8_12_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct8_13_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct8_21_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct8_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct8_23_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct8_31_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct8_32_P2_3D(const Point&) {
    return -4;
  }
  const double der2fct8_33_P2_3D(const Point&) {
    return -8;
  }

  const double der2fct9_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct9_12_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct9_13_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct9_21_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct9_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct9_23_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct9_31_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct9_32_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct9_33_P2_3D(const Point&) {
    return 0;
  }

  const double der2fct10_11_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct10_12_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct10_13_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct10_21_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct10_22_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct10_23_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct10_31_P2_3D(const Point&) {
    return 0;
  }
  const double der2fct10_32_P2_3D(const Point&) {
    return 4;
  }
  const double der2fct10_33_P2_3D(const Point&) {
    return 0;
  }


  static const FunctionXYZ fct_P2_3D[ 10 ] = {
    fct1_P2_3D, fct2_P2_3D, fct3_P2_3D, fct4_P2_3D,
    fct5_P2_3D, fct6_P2_3D, fct7_P2_3D, fct8_P2_3D,
    fct9_P2_3D, fct10_P2_3D
  }

  static const FunctionXYZ derfct_P2_3D[ 30 ] = {
    derfct1_1_P2_3D, derfct1_2_P2_3D, derfct1_3_P2_3D,
    derfct2_1_P2_3D, derfct2_2_P2_3D, derfct2_3_P2_3D,
    derfct3_1_P2_3D, derfct3_2_P2_3D, derfct3_3_P2_3D,
    derfct4_1_P2_3D, derfct4_2_P2_3D, derfct4_3_P2_3D,
    derfct5_1_P2_3D, derfct5_2_P2_3D, derfct5_3_P2_3D,
    derfct6_1_P2_3D, derfct6_2_P2_3D, derfct6_3_P2_3D,
    derfct7_1_P2_3D, derfct7_2_P2_3D, derfct7_3_P2_3D,
    derfct8_1_P2_3D, derfct8_2_P2_3D, derfct8_3_P2_3D,
    derfct9_1_P2_3D, derfct9_2_P2_3D, derfct9_3_P2_3D,
    derfct10_1_P2_3D, derfct10_2_P2_3D, derfct10_3_P2_3D
  }
  /*the perl-script:
   #!/usr/bin/perl
   for($i=1;$i<=10;$i++){
   for($j=1;$j<=3;$j++){
   printf "der2fct$i\_$j"."1\_P2\_3D, der2fct$i\_$j"."2\_P2\_3D, der2fct$i\_$j"."3_P2\_3D,\n";
   }
   }
   */
  static const FunctionXYZ der2fct_P2_3D[ 90 ] = {
    der2fct1_11_P2_3D, der2fct1_12_P2_3D, der2fct1_13_P2_3D,
    der2fct1_21_P2_3D, der2fct1_22_P2_3D, der2fct1_23_P2_3D,
    der2fct1_31_P2_3D, der2fct1_32_P2_3D, der2fct1_33_P2_3D,
    der2fct2_11_P2_3D, der2fct2_12_P2_3D, der2fct2_13_P2_3D,
    der2fct2_21_P2_3D, der2fct2_22_P2_3D, der2fct2_23_P2_3D,
    der2fct2_31_P2_3D, der2fct2_32_P2_3D, der2fct2_33_P2_3D,
    der2fct3_11_P2_3D, der2fct3_12_P2_3D, der2fct3_13_P2_3D,
    der2fct3_21_P2_3D, der2fct3_22_P2_3D, der2fct3_23_P2_3D,
    der2fct3_31_P2_3D, der2fct3_32_P2_3D, der2fct3_33_P2_3D,
    der2fct4_11_P2_3D, der2fct4_12_P2_3D, der2fct4_13_P2_3D,
    der2fct4_21_P2_3D, der2fct4_22_P2_3D, der2fct4_23_P2_3D,
    der2fct4_31_P2_3D, der2fct4_32_P2_3D, der2fct4_33_P2_3D,
    der2fct5_11_P2_3D, der2fct5_12_P2_3D, der2fct5_13_P2_3D,
    der2fct5_21_P2_3D, der2fct5_22_P2_3D, der2fct5_23_P2_3D,
    der2fct5_31_P2_3D, der2fct5_32_P2_3D, der2fct5_33_P2_3D,
    der2fct6_11_P2_3D, der2fct6_12_P2_3D, der2fct6_13_P2_3D,
    der2fct6_21_P2_3D, der2fct6_22_P2_3D, der2fct6_23_P2_3D,
    der2fct6_31_P2_3D, der2fct6_32_P2_3D, der2fct6_33_P2_3D,
    der2fct7_11_P2_3D, der2fct7_12_P2_3D, der2fct7_13_P2_3D,
    der2fct7_21_P2_3D, der2fct7_22_P2_3D, der2fct7_23_P2_3D,
    der2fct7_31_P2_3D, der2fct7_32_P2_3D, der2fct7_33_P2_3D,
    der2fct8_11_P2_3D, der2fct8_12_P2_3D, der2fct8_13_P2_3D,
    der2fct8_21_P2_3D, der2fct8_22_P2_3D, der2fct8_23_P2_3D,
    der2fct8_31_P2_3D, der2fct8_32_P2_3D, der2fct8_33_P2_3D,
    der2fct9_11_P2_3D, der2fct9_12_P2_3D, der2fct9_13_P2_3D,
    der2fct9_21_P2_3D, der2fct9_22_P2_3D, der2fct9_23_P2_3D,
    der2fct9_31_P2_3D, der2fct9_32_P2_3D, der2fct9_33_P2_3D,
    der2fct10_11_P2_3D, der2fct10_12_P2_3D, der2fct10_13_P2_3D,
    der2fct10_21_P2_3D, der2fct10_22_P2_3D, der2fct10_23_P2_3D,
    der2fct10_31_P2_3D, der2fct10_32_P2_3D, der2fct10_33_P2_3D
  }

  const BasisFunction basisFunctionP2Tetra(10,3,fct_P2_3D,derfct_P2_3D,der2fct_P2_3D);

  //======================================================================
  //
  //                            Q1  (3D)
  //
  //======================================================================
  /*
      8-------7
     /.      /|
    / .     / |
   5_______6  |
   |  .    |  |
   |  4....|..3
   | .     | /
   |.      |/
   1_______2
   */
  const double fct1_Q1_3D( const double x, const double y, const double z ) {
    return ( 1. - x ) * ( 1. - y ) * ( 1. - z );
  }
  const double fct2_Q1_3D( const double x, const double y, const double z ) {
    return x * ( 1. - y ) * ( 1. - z );
  }
  const double fct3_Q1_3D( const double x, const double y, const double z ) {
    return x * y * ( 1. - z );
  }
  const double fct4_Q1_3D( const double x, const double y, const double z ) {
    return ( 1. - x ) * y * ( 1. - z );
  }
  const double fct5_Q1_3D( const double x, const double y, const double z ) {
    return ( 1. - x ) * ( 1. - y ) * z;
  }
  const double fct6_Q1_3D( const double x, const double y, const double z ) {
    return x * ( 1. - y ) * z;
  }
  const double fct7_Q1_3D( const double x, const double y, const double z ) {
    return x * y * z;
  }
  const double fct8_Q1_3D( const double x, const double y, const double z ) {
    return ( 1. - x ) * y * z;
  }

  const double derfct1_1_Q1_3D( const double, const double y, const double z ) {
    return -( 1. - y ) * ( 1. - z );
  }
  const double derfct1_2_Q1_3D( const double x, const double, const double z ) {
    return -( 1. - x ) * ( 1. - z );
  }
  const double derfct1_3_Q1_3D( const double x, const double y, const double ) {
    return -( 1. - x ) * ( 1. - y );
  }
  const double derfct2_1_Q1_3D( const double, const double y, const double z ) {
    return ( 1. - y ) * ( 1. - z );
  }
  const double derfct2_2_Q1_3D( const double x, const double, const double z ) {
    return -x * ( 1. - z ) ;
  }
  const double derfct2_3_Q1_3D( const double x, const double y, const double ) {
    return -x * ( 1. - y );
  }
  const double derfct3_1_Q1_3D( const double, const double y, const double z ) {
    return y * ( 1. - z );
  }
  const double derfct3_2_Q1_3D( const double x, const double, const double z ) {
    return x * ( 1. - z );
  }
  const double derfct3_3_Q1_3D( const double x, const double y, const double ) {
    return -x * y ;
  }
  const double derfct4_1_Q1_3D( const double, const double y, const double z ) {
    return -y * ( 1. - z );
  }
  const double derfct4_2_Q1_3D( const double x, const double, const double z ) {
    return ( 1. - x ) * ( 1. - z );
  }
  const double derfct4_3_Q1_3D( const double x, const double y, const double ) {
    return -( 1. - x ) * y;
  }
  const double derfct5_1_Q1_3D( const double, const double y, const double z ) {
    return -( 1. - y ) * z;
  }
  const double derfct5_2_Q1_3D( const double x, const double, const double z ) {
    return -( 1. - x ) * z;
  }
  const double derfct5_3_Q1_3D( const double x, const double y, const double ) {
    return ( 1. - x ) * ( 1. - y );
  }
  const double derfct6_1_Q1_3D( const double, const double y, const double z ) {
    return ( 1. - y ) * z ;
  }
  const double derfct6_2_Q1_3D( const double x, const double, const double z ) {
    return -x * z;
  }
  const double derfct6_3_Q1_3D( const double x, const double y, const double ) {
    return x * ( 1. - y );
  }
  const double derfct7_1_Q1_3D( const double, const double y, const double z ) {
    return y * z;
  }
  const double derfct7_2_Q1_3D( const double x, const double, const double z ) {
    return x * z;
  }
  const double derfct7_3_Q1_3D( const double x, const double y, const double ) {
    return x * y;
  }
  const double derfct8_1_Q1_3D( const double, const double y, const double z ) {
    return -y * z;
  }
  const double derfct8_2_Q1_3D( const double x, const double, const double z ) {
    return ( 1. - x ) * z;
  }
  const double derfct8_3_Q1_3D( const double x, const double y, const double ) {
    return ( 1. - x ) * y;
  }

  const double der2fct1_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct1_12_Q1_3D( const double, const double, const double z ) {
    return 1. - z;
  }
  const double der2fct1_13_Q1_3D( const double, const double y, const double ) {
    return 1. - y;
  }
  const double der2fct1_21_Q1_3D( const double, const double, const double z ) {
    return 1. - z;
  }
  const double der2fct1_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct1_23_Q1_3D( const double x, const double, const double ) {
    return 1. - x;
  }
  const double der2fct1_31_Q1_3D( const double, const double y, const double ) {
    return 1. - y;
  }
  const double der2fct1_32_Q1_3D( const double x, const double, const double ) {
    return 1. - x;
  }
  const double der2fct1_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct2_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct2_12_Q1_3D( const double, const double, const double z ) {
    return -( 1. - z );
  }
  const double der2fct2_13_Q1_3D( const double, const double y, const double ) {
    return -( 1. - y );
  }
  const double der2fct2_21_Q1_3D( const double, const double, const double z ) {
    return -( 1. - z );
  }
  const double der2fct2_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct2_23_Q1_3D( const double x, const double, const double ) {
    return x;
  }
  const double der2fct2_31_Q1_3D( const double, const double y, const double ) {
    return -( 1. - y );
  }
  const double der2fct2_32_Q1_3D( const double x, const double, const double ) {
    return x;
  }
  const double der2fct2_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct3_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct3_12_Q1_3D( const double, const double, const double z ) {
    return ( 1. - z );
  }
  const double der2fct3_13_Q1_3D( const double, const double y, const double ) {
    return -y;
  }
  const double der2fct3_21_Q1_3D( const double, const double, const double z ) {
    return ( 1. - z );
  }
  const double der2fct3_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct3_23_Q1_3D( const double x, const double, const double ) {
    return -x;
  }
  const double der2fct3_31_Q1_3D( const double, const double y, const double ) {
    return -y;
  }
  const double der2fct3_32_Q1_3D( const double x, const double, const double ) {
    return -x;
  }
  const double der2fct3_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct4_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct4_12_Q1_3D( const double, const double, const double z ) {
    return -( 1. - z );
  }
  const double der2fct4_13_Q1_3D( const double, const double y, const double ) {
    return y;
  }
  const double der2fct4_21_Q1_3D( const double, const double, const double z ) {
    return -( 1. - z );
  }
  const double der2fct4_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct4_23_Q1_3D( const double x, const double, const double ) {
    return -( 1. - x );
  }
  const double der2fct4_31_Q1_3D( const double, const double y, const double ) {
    return y;
  }
  const double der2fct4_32_Q1_3D( const double x, const double, const double ) {
    return -( 1. - x );
  }
  const double der2fct4_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct5_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct5_12_Q1_3D( const double, const double, const double z ) {
    return z;
  }
  const double der2fct5_13_Q1_3D( const double, const double y, const double ) {
    return -( 1. - y );
  }
  const double der2fct5_21_Q1_3D( const double, const double, const double z ) {
    return z;
  }
  const double der2fct5_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct5_23_Q1_3D( const double x, const double, const double ) {
    return -( 1. - x );
  }
  const double der2fct5_31_Q1_3D( const double, const double y, const double ) {
    return -( 1. - y );
  }
  const double der2fct5_32_Q1_3D( const double x, const double, const double ) {
    return -( 1. - x );
  }
  const double der2fct5_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct6_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct6_12_Q1_3D( const double, const double, const double z ) {
    return -z;
  }
  const double der2fct6_13_Q1_3D( const double, const double y, const double ) {
    return 1. - y;
  }
  const double der2fct6_21_Q1_3D( const double, const double, const double z ) {
    return -z;
  }
  const double der2fct6_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct6_23_Q1_3D( const double x, const double, const double ) {
    return -x;
  }
  const double der2fct6_31_Q1_3D( const double, const double y, const double ) {
    return 1. - y;
  }
  const double der2fct6_32_Q1_3D( const double x, const double, const double ) {
    return -x;
  }
  const double der2fct6_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct7_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct7_12_Q1_3D( const double, const double, const double z ) {
    return z;
  }
  const double der2fct7_13_Q1_3D( const double, const double y, const double ) {
    return y;
  }
  const double der2fct7_21_Q1_3D( const double, const double, const double z ) {
    return z;
  }
  const double der2fct7_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct7_23_Q1_3D( const double x, const double, const double ) {
    return x;
  }
  const double der2fct7_31_Q1_3D( const double, const double y, const double ) {
    return y;
  }
  const double der2fct7_32_Q1_3D( const double x, const double, const double ) {
    return x;
  }
  const double der2fct7_33_Q1_3D(const Point&) {
    return 0;
  }

  const double der2fct8_11_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct8_12_Q1_3D( const double, const double, const double z ) {
    return -z;
  }
  const double der2fct8_13_Q1_3D( const double, const double y, const double ) {
    return -y;
  }
  const double der2fct8_21_Q1_3D( const double, const double, const double z ) {
    return -z;
  }
  const double der2fct8_22_Q1_3D(const Point&) {
    return 0;
  }
  const double der2fct8_23_Q1_3D( const double x, const double, const double ) {
    return 1. - x;
  }
  const double der2fct8_31_Q1_3D( const double, const double y, const double ) {
    return -y;
  }
  const double der2fct8_32_Q1_3D( const double x, const double, const double ) {
    return 1 -x;
  }
  const double der2fct8_33_Q1_3D(const Point&) {
    return 0;
  }

  static const FunctionXYZ fct_Q1_3D[ 8 ] = {
    fct1_Q1_3D, fct2_Q1_3D, fct3_Q1_3D, fct4_Q1_3D, fct5_Q1_3D,
    fct6_Q1_3D, fct7_Q1_3D, fct8_Q1_3D
  }


  static const FunctionXYZ derfct_Q1_3D[ 24 ] = {
    derfct1_1_Q1_3D, derfct1_2_Q1_3D, derfct1_3_Q1_3D,
    derfct2_1_Q1_3D, derfct2_2_Q1_3D, derfct2_3_Q1_3D,
    derfct3_1_Q1_3D, derfct3_2_Q1_3D, derfct3_3_Q1_3D,
    derfct4_1_Q1_3D, derfct4_2_Q1_3D, derfct4_3_Q1_3D,
    derfct5_1_Q1_3D, derfct5_2_Q1_3D, derfct5_3_Q1_3D,
    derfct6_1_Q1_3D, derfct6_2_Q1_3D, derfct6_3_Q1_3D,
    derfct7_1_Q1_3D, derfct7_2_Q1_3D, derfct7_3_Q1_3D,
    derfct8_1_Q1_3D, derfct8_2_Q1_3D, derfct8_3_Q1_3D
  }
  /*the perl-script:
   #!/usr/bin/perl
   for($i=1;$i<=10;$i++){
   for($j=1;$j<=3;$j++){
   printf "der2fct$i\_$j"."1\_P2\_3D, der2fct$i\_$j"."2\_P2\_3D, der2fct$i\_$j"."3_P2\_3D,\n";
   }
   }
   */
  static const FunctionXYZ der2fct_Q1_3D[ 72 ] = {
    der2fct1_11_Q1_3D, der2fct1_12_Q1_3D, der2fct1_13_Q1_3D,
    der2fct1_21_Q1_3D, der2fct1_22_Q1_3D, der2fct1_23_Q1_3D,
    der2fct1_31_Q1_3D, der2fct1_32_Q1_3D, der2fct1_33_Q1_3D,
    der2fct2_11_Q1_3D, der2fct2_12_Q1_3D, der2fct2_13_Q1_3D,
    der2fct2_21_Q1_3D, der2fct2_22_Q1_3D, der2fct2_23_Q1_3D,
    der2fct2_31_Q1_3D, der2fct2_32_Q1_3D, der2fct2_33_Q1_3D,
    der2fct3_11_Q1_3D, der2fct3_12_Q1_3D, der2fct3_13_Q1_3D,
    der2fct3_21_Q1_3D, der2fct3_22_Q1_3D, der2fct3_23_Q1_3D,
    der2fct3_31_Q1_3D, der2fct3_32_Q1_3D, der2fct3_33_Q1_3D,
    der2fct4_11_Q1_3D, der2fct4_12_Q1_3D, der2fct4_13_Q1_3D,
    der2fct4_21_Q1_3D, der2fct4_22_Q1_3D, der2fct4_23_Q1_3D,
    der2fct4_31_Q1_3D, der2fct4_32_Q1_3D, der2fct4_33_Q1_3D,
    der2fct5_11_Q1_3D, der2fct5_12_Q1_3D, der2fct5_13_Q1_3D,
    der2fct5_21_Q1_3D, der2fct5_22_Q1_3D, der2fct5_23_Q1_3D,
    der2fct5_31_Q1_3D, der2fct5_32_Q1_3D, der2fct5_33_Q1_3D,
    der2fct6_11_Q1_3D, der2fct6_12_Q1_3D, der2fct6_13_Q1_3D,
    der2fct6_21_Q1_3D, der2fct6_22_Q1_3D, der2fct6_23_Q1_3D,
    der2fct6_31_Q1_3D, der2fct6_32_Q1_3D, der2fct6_33_Q1_3D,
    der2fct7_11_Q1_3D, der2fct7_12_Q1_3D, der2fct7_13_Q1_3D,
    der2fct7_21_Q1_3D, der2fct7_22_Q1_3D, der2fct7_23_Q1_3D,
    der2fct7_31_Q1_3D, der2fct7_32_Q1_3D, der2fct7_33_Q1_3D,
    der2fct8_11_Q1_3D, der2fct8_12_Q1_3D, der2fct8_13_Q1_3D,
    der2fct8_21_Q1_3D, der2fct8_22_Q1_3D, der2fct8_23_Q1_3D,
    der2fct8_31_Q1_3D, der2fct8_32_Q1_3D, der2fct8_33_Q1_3D
  }

  const BasisFunction basisFunctionQ1Hexa(8,3,fct_Q1_3D,derfct_Q1_3D,der2fct_Q1_3D);
#endif

}
