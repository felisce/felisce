//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

// System includes
#include <cmath>

// External includes

// Project includes
#include "refElement.hpp"

namespace felisce
{
RefElement::RefElement(const std::string& name,
                        const RefShape& refShape,
                        const BasisFunction& basisFunction,
                        const ListOfQuadratureRule& listOfQuadratureRule,
                        const DegreeOfFreedomType* typeDOF,
                        const DegreeOfFreedomSupport* supportDOF,
                        const int* idDOF,
                        const Point* node,int numNodeVertex,int numNodeEdge,int numNodeFace,int numNodeVolume,
                        const RefElement** boundaryRefElement,
                        const RefElement** boundaryBoundaryRefElement,
                        bool edgesCanHaveDifferentNumberOfDof):
  m_name(name),
  m_refShape(refShape),
  m_basisFunction(basisFunction),
  //  m_listOfQuadratureRule(listOfQuadratureRule),
  m_typeDOF(typeDOF),
  m_supportDOF(supportDOF),
  m_idSupportOfDOF(idDOF),
  m_node(node),
  m_numNodeVertex(numNodeVertex),
  m_numNodeEdge(numNodeEdge),
  m_numNodeFace(numNodeFace),
  m_numNodeVolume(numNodeVolume),
  m_numNode(m_numNodeVertex + m_numNodeEdge + m_numNodeFace + m_numNodeVolume),
  m_numDOF(basisFunction.size()),
  m_numVertex(m_refShape.numVertex()),
  m_numEdge(m_refShape.numEdge()),
  m_numFace(m_refShape.numFace()),
  m_numCoor(m_refShape.numCoor()),
  m_boundaryRefElement(boundaryRefElement),
  m_edgesCanHaveDifferentNumberOfDof(edgesCanHaveDifferentNumberOfDof)//false by default
  //  m_boundaryBoundaryRefElement(boundaryBoundaryRefElement)
{
  IGNORE_UNUSED_ARGUMENT(listOfQuadratureRule);
  IGNORE_UNUSED_ARGUMENT(boundaryBoundaryRefElement);

  FEL_DEBUG("Constructing " << m_name << std::endl);
  initStaticMember();
  m_numDOFNodeVertex = 0;
  m_numDOFNodeEdge = 0;
  m_numDOFNodeFace = 0;
  m_numDOFNodeVolume = 0;
  m_numDOFEdge = 0;
  m_numDOFFace = 0;
  m_numDOFVolume = 0;
  for(int i=0; i<m_numDOF; i++) {
    switch (m_supportDOF[i]) {
      case DOF_NODE_VERTEX:
        m_numDOFNodeVertex++;
        break;
      case DOF_NODE_EDGE:
        m_numDOFNodeEdge++;
        m_numDOFEdge++;
        break;
      case DOF_NODE_FACE:
        m_numDOFNodeFace++;
        m_numDOFFace++;
        break;
      case DOF_NODE_VOLUME:
        m_numDOFNodeVolume++;
        m_numDOFVolume++;
        break;
      case DOF_EDGE:
        m_numDOFEdge++;
        break;
      case DOF_FACE:
        m_numDOFFace++;
        break;
      case DOF_VOLUME:
        m_numDOFVolume++;
        break;
        // Default case should appear with a warning at compile time instead of an error in runtime
        // (that's truly the point of using enums as switch cases)
        //  default:
        //felisce_error("Something is wrong is typeDOF", __FILE__, __LINE__);
        //break;
    }
  }
  m_numDOFNode =  m_numDOFNodeVertex + m_numDOFNodeEdge + m_numDOFNodeFace + m_numDOFNodeVolume;
  FEL_ASSERT( m_numDOF == (m_numDOFNodeVertex + m_numDOFEdge + m_numDOFFace + m_numDOFVolume) );
  m_numDOFPerEdge = 0;
  m_numDOFPerFace = 0;
  if ( m_numEdge > 0 && !m_edgesCanHaveDifferentNumberOfDof ) {
    FEL_ASSERT( m_numDOFEdge % m_numEdge == 0 );
    m_numDOFPerEdge = (int) (m_numDOFEdge / m_numEdge);
  } else if ( m_numEdge > 0 ){
    m_numDOFPerSingleEdge.resize(m_numEdge,0);
    for (int i(0); i<m_numDOFEdge; ++i) {
      m_numDOFPerSingleEdge[m_idSupportOfDOF[m_numDOFNodeVertex + i]]++;
    }
  }
  if ( m_numFace > 0 ) {
    FEL_ASSERT( m_numDOFFace % m_numFace == 0 );
    m_numDOFPerFace = (int) (m_numDOFFace / m_numFace);
  }
  m_idDOFVertex.resize(m_numVertex);
  m_idDOFEdge.resize(m_numEdge);
  m_idDOFFace.resize(m_numFace);
  for(int i=0; i<m_numDOF; i++) {
    switch (m_supportDOF[i]) {
      case DOF_NODE_VERTEX:
        m_idDOFVertex[ m_idSupportOfDOF[i] ].push_back(i);
        break;
      case DOF_NODE_EDGE:
      case DOF_EDGE:
        // in the case of m_edgesCanHaveDifferentNumberOfDof=true some of those vectors will be empty
        m_idDOFEdge[ m_idSupportOfDOF[i] ].push_back(i);
        break;
      case DOF_NODE_FACE:
      case DOF_FACE:
        m_idDOFFace[ m_idSupportOfDOF[i] ].push_back(i);
        break;
      case DOF_NODE_VOLUME:
      case DOF_VOLUME:
        m_idDOFVolume.push_back(i);
        break;
        // Default case should appear with a warning at compile time instead of an error in runtime
        // (that's truly the point of using enums as switch cases)
        //  default:

        //      felisce_error("Something is wrong is typeDOF", __FILE__, __LINE__);
        //      break;
    }
  }

  // This loop is not very general, in case m_numEdge = 2*m_numDOFEdge it does not work anymore maybe replace m_numEdge with m_numDOFEdge
  m_nodeDOF.resize(m_numDOF);
  if ( ! m_edgesCanHaveDifferentNumberOfDof ) {
    for(int i=0; i<m_numDOF; i++) {
      switch (m_supportDOF[i]) {
        case DOF_NODE_VERTEX:
          m_nodeDOF[i] = &(m_node[m_idSupportOfDOF[i]]); // vertices come first in the nodes list
          break;
        case DOF_NODE_EDGE:
          m_nodeDOF[i] = &(m_node[m_numVertex+m_idSupportOfDOF[i]]); // nodes-edges comes just after vertices in the nodes list
          break;
        case DOF_NODE_FACE:
          m_nodeDOF[i] = &(m_node[m_numVertex+m_numEdge+m_idSupportOfDOF[i]]);
          break;
        case DOF_NODE_VOLUME:
          m_nodeDOF[i] = &(m_node[m_numVertex+m_numEdge+m_numFace+m_idSupportOfDOF[i]]);
          break;
        case DOF_EDGE:
        case DOF_FACE:
        case DOF_VOLUME:
          m_nodeDOF[i] = nullptr;
          break;
      }
    }
  } else {
    for(int i=0; i<m_numDOF; i++) {
      switch (m_supportDOF[i]) {
        case DOF_NODE_VERTEX:
          m_nodeDOF[i] = &(m_node[m_idSupportOfDOF[i]]); // vertices come first in the nodes list
          break;
        case DOF_NODE_EDGE:
          m_nodeDOF[i] = &(m_node[i]);// We assume that the points are listed in the same order as the dofs. This is used for instance in PrismsP1xP2
          break;
        case DOF_NODE_FACE:
          m_nodeDOF[i] = &(m_node[m_numVertex+m_numDOFEdge+m_idSupportOfDOF[i]]);
          break;
        case DOF_NODE_VOLUME:
          m_nodeDOF[i] = &(m_node[m_numVertex+m_numDOFEdge+m_numDOFFace+m_idSupportOfDOF[i]]);
          break;
        case DOF_EDGE:
        case DOF_FACE:
        case DOF_VOLUME:
          m_nodeDOF[i] = nullptr;
          break;
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void RefElement::initStaticMember()
{
  //      static bool first_time = true;
  //    if(first_time){
  FEL_DEBUG("Initializing static members of RefElement" << std::endl);

  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_NODE_VERTEX,"DOF_NODE_VERTEX"));
  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_NODE_EDGE,"DOF_NODE_EDGE"));
  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_NODE_FACE,"DOF_NODE_FACE"));
  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_NODE_VOLUME,"DOF_NODE_VOLUME"));
  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_EDGE,"DOF_EDGE"));
  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_FACE,"DOF_FACE"));
  m_nameSupportDOF.insert(std::pair<DegreeOfFreedomSupport,std::string>(DOF_VOLUME,"DOF_VOLUME"));

  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_VALUE,"DOF_VALUE"));
  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_X_DERIVATIVE,"DOF_X_DERIVATIVE"));
  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_Y_DERIVATIVE,"DOF_Y_DERIVATIVE"));
  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_Z_DERIVATIVE,"DOF_Z_DERIVATIVE"));
  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_MEAN,"DOF_MEAN"));
  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_FLUX,"DOF_FLUX"));
  m_nameTypeDOF.insert(std::pair<DegreeOfFreedomType,std::string>(DOF_CIRCULATION,"DOF_CIRCULATION"));

  //  }
  //first_time = false;
}

/***********************************************************************************/
/***********************************************************************************/

void RefElement::check() const
{
  std::cout << "Check Reference Element " << m_name << std::endl;
  if(m_numDOFNode) std::cout << "  Nodal DOF (the following matrix must be identity)" << std::endl;
  double delta_ij=0.;

  for(int i=0; i<m_numDOF; i++) {
    if(dofIsANode(i)) {
      std::cout << "  ";
      for(int j=0; j<m_numDOF; j++) {
        if(dofIsANode(j)) {
          FEL_ASSERT(m_nodeDOF[j]); // check if dof j is really a node
          switch(m_typeDOF[i]) {
          case DOF_VALUE:
            delta_ij = m_basisFunction.phi(i,*(m_nodeDOF[j]));
            break;
          case DOF_X_DERIVATIVE:
            delta_ij = m_basisFunction.dPhi(i,0,*(m_nodeDOF[j]));
            break;
          case DOF_Y_DERIVATIVE:
            delta_ij = m_basisFunction.dPhi(i,1,*(m_nodeDOF[j]));
            break;
          case DOF_Z_DERIVATIVE:
            delta_ij = m_basisFunction.dPhi(i,2,*(m_nodeDOF[j]));
            break;
          case DOF_NORMAL_DERIVATIVE:
          case DOF_MEAN:
          case DOF_FLUX:
          case DOF_CIRCULATION:
            break;
          }
          std::cout  << std::fabs(delta_ij) << " "; // fabs is just to avoid the minus sign for -0 (due rounding off errors)
        }
      }
      std::cout << std::endl;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void RefElement::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "RefElement: " << std::endl;
    c << " name: " << m_name << std::endl;
    c << " shape: " << m_refShape.name() << std::endl;
    c << " basisFunction: " << m_basisFunction.name() << std::endl;
    c << " numDOFNode: " << m_numDOFNode ;
    c << " ( " << m_numDOFNodeVertex << " on vertices, ";
    c << m_numDOFNodeEdge << " on edges, ";
    c << m_numDOFNodeFace << " on faces, ";
    c << m_numDOFNodeVolume << " on volume) " << std::endl;
    if ( ! m_edgesCanHaveDifferentNumberOfDof ) {
      c << " numDOFEdge: " << m_numDOFEdge  << " (numDOFPerEdge=" << m_numDOFPerEdge << ")" << std::endl;
    } else {
      c << " numDOFEdge: " << m_numDOFEdge << std::endl;
      for (int i(0); i<m_numEdge; ++i)
        c << "Edge id: "<<i<<" number of dof supported by this edge " << m_idDOFEdge[i].size() << std::endl;
    }
    c << " numDOFFace: " << m_numDOFFace  << " (numDOFPerFace=" << m_numDOFPerFace << ")" << std::endl;
    c << " numDOFVolume: " << m_numDOFVolume  << std::endl;
    if(m_numNode) c << " Nodes coordinates : " << std::endl;

    for(int ip=0; ip<m_numNode; ip++) {
      c << "  Node " << ip << ": ";
      std::cout << m_node[ip].x() << ", " << m_node[ip].y() << ", " << m_node[ip].z()  << std::endl;
    }

    c << " Degree Of Freedom (DOF Type / Support type / Support id) : " << std::endl;
    for(int i=0; i<m_basisFunction.size(); i++) {
      c << "  DOF " << i << " : " << m_nameTypeDOF[m_typeDOF[i]] << " / "
        << m_nameSupportDOF[m_supportDOF[i]] << " / " << m_idSupportOfDOF[i] << std::endl;
    }

    if(m_numDOFNodeVertex) {
      c << " List of DOF by vertex " << std::endl;
      for(int i=0; i<m_numVertex; i++) {
        c << "  Vertex " << i << " : ";
        for(std::size_t j=0; j<m_idDOFVertex[i].size(); j++) {
          c << m_idDOFVertex[i][j] << " ";
        }
        c << std::endl;
      }
    }
    if(m_numDOFEdge) {
      c << " List of DOF by edge " << std::endl;
      for(int i=0; i<m_numEdge; i++) {
        if ( m_idDOFEdge[i].size() > 0 ){
          c << "  Edge " << i << " : ";
          for(std::size_t j=0; j<m_idDOFEdge[i].size(); j++) {
            c << m_idDOFEdge[i][j] << " ";
          }
          c << std::endl;
        }
      }
    }
    if(m_numDOFFace) {
      c << " List of DOF by face " << std::endl;
      for(int i=0; i<m_numFace; i++) {
        c << "  Face " << i << " : ";
        for(std::size_t j=0; j<m_idDOFFace[i].size(); j++) {
          c << m_idDOFFace[i][j] << " ";
        }
        c << std::endl;
      }
    }
    if(m_numDOFVolume) {
      c << " List of DOF on the volume " << std::endl;
      for(int i=0; i<m_numDOFVolume; i++) {
        c << m_idDOFVolume[i] << " ";
      }
    }
  }
}
}
