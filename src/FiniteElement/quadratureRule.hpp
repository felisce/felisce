//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

#ifndef QUADRATURE_RULE_HPP
#define QUADRATURE_RULE_HPP

// System includes
#include <cassert>
#include <iostream>

// External includes

// Project includes
#include "Geometry/point.hpp"
#include "Core/felisce.hpp"
#include "FiniteElement/refShape.hpp"

///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
namespace felisce
{
/**
 * @class QuadraturePoint
 * @brief The class for the quadrature points i.e. (x,y,z,weight)
 * @authors J-F. Gerbeau
 * @date 12/2009
 */
class QuadraturePoint
  : public Point
{
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor
  QuadraturePoint(){};

  /**
   * @brief Constructor with three coordinates and one weight
   */
  QuadraturePoint(
    const double x,
    const double y,
    const double z,
    const double w
    ) : Point(x,y,z),
        m_weight( w )
  {
  }

  /**
   * @brief Constructor with two coordinates and one weight
   */
  QuadraturePoint(
    const double x,
    const double y,
    const double w
    ) : Point(x,y,0.),
        m_weight( w )
  {
  }

  /**
   * @brief Constructor with one coordinate and one weight
   */
  QuadraturePoint(
    const double x,
    const double w
    ) : Point(x,0.,0.),
        m_weight( w )
  {
  }

  /**
   * @brief Constructor with point and one weight
   */
  QuadraturePoint(
    const Point& rPoint,
    const double w
    ) : Point(rPoint),
        m_weight( w )
  {
  }

  /*! Default destructor */
  ~QuadraturePoint() = default;

  //! Copy constructor, to avoid warning due to user-declared destructor.
  QuadraturePoint(const QuadraturePoint& P)
    : Point(P.m_coor),
      m_weight(P.m_weight)
  {}

  ///@}
  ///@name Operators
  ///@{

  // Assignment.
  QuadraturePoint& operator=(const QuadraturePoint& P) {
    m_coor = P.m_coor;
    m_weight = P.m_weight;

    return *this;
  }

  ///@}
  ///@name Operations
  ///@{

  ///@}
  ///@name Access
  ///@{

  /// Retrieves the weight of the QuadraturePoint
  inline double weight() const {
    return m_weight;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const {
    if(verbose) {
      c << "[" << m_coor[0] << ", " << m_coor[1] << ", " << m_coor[2] << "]" << ", weight = " << m_weight;
    }
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  double m_weight = 0.0; /// Weight associated to the quadrature point

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

/**
  * @class QuadratureRule
  * @brief The class of quadrature rules
  * @authors J-F. Gerbeau
  * @date 12/2009
  * This class contains the quadrature points and the associated weights.
  * @par How to add a new quadrature rule ?
  * Suppose you want to add the quadrature rule qr_Pipo on a tetrahedron:
  * in the file quadratureRule.hpp, add
  * @code
  * extern const QuadratureRule quadratureRulePipo;
  * @endcode
  * In the file definitionGlobalVariables.cpp, add the quadrature rule (follow the example of another one) and add it to the array of all quadrature rule on a tetrahedron, namely :
  * @code
  * static const QuadratureRule quad_rule_on_tetra[] =
  * {quadratureRuleTetra1pt,quadratureRuleTetra4pt,quadratureRuleTetra5pt,quadRulePipo,
  * quadratureRuleTetra15pt,quadratureRuleTetra64pt}
  * @endcode
  * Be careful : it is mandatory that the quadrature are ordered by increasing degree of exactness, and two different
  * quadrature rules of this list cannot share the same degree of exactness. The reason of this limitation is due to the way
  * ListOfQuadratureRule::quadratureRuleByExactness is built.
  * Finally, do not forget to increase the number of quadrature in the file definitionGlobalVariables.cpp
  * @code
  * const ListOfQuadratureRule listQuadratureRuleTetrahedron("listQuadratureRuleTetrahedron",
  * 6,quad_rule_on_tetra);
  * @endcode
  */
class QuadratureRule
{
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /**
   * @brief Default constructor
   */
  QuadratureRule(){};

  /**
   * @brief Constructor with arguments
   */
  QuadratureRule(
    const QuadraturePoint* pt,
    const std::string name,
    const RefShape& shape,
    const int numQuadraturePoint,
    const int defOfExact
    );

  /**
   * @brief Constructor with arguments (combination)
   */
  QuadratureRule(
    const QuadratureRule& rQuadratureRule1,
    const QuadratureRule& rQuadratureRule2,
    const std::string name,
    const RefShape& shape,
    const double Coefficient = 1.0
    );

  /*! Default destructor */
  ~QuadratureRule() = default;

  //! Copy constructor, to avoid warning due to user-declared destructor.
  QuadratureRule(const QuadratureRule& QR)
    : m_point(QR.m_point),
      m_shape(QR.m_shape),
      m_name(QR.m_name),
      m_numQuadraturePoint(QR.m_numQuadraturePoint),
      m_degreeOfExactness(QR.m_degreeOfExactness)
  {
  }

  ///@}
  ///@name Operators
  ///@{

  // Assignment.
  QuadratureRule& operator=(const QuadratureRule& QR);

  ///@}
  ///@name Operations
  ///@{

  ///@}
  ///@name Access
  ///@{

  /*! Return the name of the quadrature rule */
  std::string name() const {
    return m_name;
  }

  /*! Return the shape of the quadrature rule */
  const RefShape& refShape() const {
    return *m_shape;
  }

  /*! Return the number of quadrature points */
  inline int numQuadraturePoint(const std::size_t Index = 0) const {
    return m_numQuadraturePoint[Index];
  }

  /*! quadraturePoint(ig) is the ig-th quadrature point */
  inline const QuadraturePoint* quadraturePoints() const {
    return m_point;
  }

  /*! quadraturePoint(ig) is the ig-th quadrature point */
  inline const QuadraturePoint& quadraturePoint(const int ig) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[0] );
    return m_point[ ig ];
  }

  /*! weight(ig) is the ig-th quadrature weight */
  inline double weight(const int ig) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[0] );
    return m_point[ ig ].weight();
  }

  /*! weight(ig, jg) is the ig-th/jg-th quadrature weight */
  inline double weight(const int ig, const int jg) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[1] );
    FEL_ASSERT( jg < m_numQuadraturePoint[2] );
    return m_point[ig * m_numQuadraturePoint[2] + jg].weight();
  }

  /*! \c quadraturePointCoor(ig) is the array of coordinates of the quadrature point ig */
  inline const std::array<double, 3>& quadraturePointgetCoor(const int ig) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[0] );
    return m_point[ ig ].getCoor();
  }

  /*! \c quadraturePointCoor(ig) is the array of coordinates of the quadrature point ig/jg */
  inline const std::array<double, 3>& quadraturePointgetCoor(const int ig, const int jg) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[1] );
    FEL_ASSERT( jg < m_numQuadraturePoint[2] );
    return m_point[ig * m_numQuadraturePoint[2] + jg].getCoor();
  }

  /*! \c quadraturePointCoor(ig,icoor) is the coordinate icoor of the quadrature point ig */
  inline double quadraturePointCoor(const int ig, const int icoor) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[0] );
    return m_point[ ig ].coor( icoor );
  }

  /*! \c quadraturePointCoor(ig,icoor) is the coordinate icoor of the quadrature point ig/jg */
  inline double quadraturePointCoor(const int ig, const int jg, const int icoor) const {
    FEL_ASSERT( ig < m_numQuadraturePoint[1] );
    FEL_ASSERT( jg < m_numQuadraturePoint[2] );
    return m_point[ig * m_numQuadraturePoint[2] + jg].coor(icoor);
  }

  /*! Return the degree of exactness of the quadrature rule */
  inline int degreeOfExactness(const std::size_t Index = 0) const {
    return m_degreeOfExactness[Index];
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  const QuadraturePoint* m_point;        /// List of quadrature points
  const RefShape* m_shape;               /// Geometric shape on which the quadrature rule can be used
  std::string m_name = "";               /// Name of the quadrature rule
  int* m_numQuadraturePoint= new int[1]; /// Number of quadrature points
  int* m_degreeOfExactness= new int[1];  /// Degree of exactness of the quadrature rule

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

/**
 * @brief This is an enum defined in order to define the degree of exactness
 */
enum DegreeOfExactness {
  DegreeOfExactness_0 = 0,
  DegreeOfExactness_1 = 1,
  DegreeOfExactness_2 = 2,
  DegreeOfExactness_3 = 3,
  DegreeOfExactness_4 = 4,
  DegreeOfExactness_5 = 5,
  DegreeOfExactness_6 = 6,
  DegreeOfExactness_7 = 7
};

/**
 * @class ListOfQuadratureRule
 * @brief List of all quadrature rules on a given geometrical shape
 * @authors J-F. Gerbeau
 * @date 02/2010
 * This class contains the list of quadrature rules on a given geometrical shape.
 * To add a new quadrature rule see the documentation of \c QuadratureRule.
 * @see QuadratureRule
 */
class ListOfQuadratureRule
{
public:

  /*! Default constructor */
  ListOfQuadratureRule(
    const std::string name,
    const int numQuadratureRule,
    const QuadratureRule* quadratureRule
    );

  /*! Default constructor (combination) */
  ListOfQuadratureRule(
    const std::string name,
    const ListOfQuadratureRule& listQuadratureRule1,
    const ListOfQuadratureRule& listQuadratureRule2,
    const double Coefficient = 1.0
    );

  /*! Default destructor */
  ~ListOfQuadratureRule() = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Return the cheapest quadrature rule of the list ensuring that polynoms of degree \c deg are computed exactly.
   * @details For the sake of clarity, it is recommended to use the enum \c DegreeOfExactness instead of \c int. Thus prefer for example deg=DegreeOfExactness_2 instead of deg=2.
   */
  const QuadratureRule& quadratureRuleByExactness(const DegreeOfExactness deg) const;

  /**
   * @brief Return the cheapest combined quadrature rule of the list ensuring that polynoms of degree \c deg are computed exactly.
   * @details For the sake of clarity, it is recommended to use the enum \c DegreeOfExactness instead of \c int. Thus prefer for example deg=DegreeOfExactness_2 instead of deg=2.
   */
  const QuadratureRule& quadratureRuleByExactness(const DegreeOfExactness degi, const DegreeOfExactness degj) const;

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief Retrieves the maximum degree of exactness
   * @return The maximum degree of exactness
   */
  int getMaxDegreeOfExactness(const std::size_t Index = 0) const
  {
    return m_maxDegreeOfExactness[Index];
  }

  /**
   * @brief Retrieves the number of quadrature rules
   * @return The number of quadrature rules
   */
  int getNumQuadratureRule(const std::size_t Index = 0) const
  {
    return m_numQuadratureRule[Index];
  }

  /**
   * @brief Retrieves the quadrature rule
   * @return The quadrature rule
   */
  const QuadratureRule* getQuadratureRule() const
  {
    return m_quadratureRule;
  }

  /**
   * @brief Retrieves the quadrature rule by degree of exactness
   * @return The quadrature rule by degree of exactness
   */
  const int* getquadratureByDegreeOfExactness() const
  {
    return m_quadratureByDegreeOfExactness;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

protected:
  /// TODO: Most of these variables can be computed from m_quadratureRule, therefore reduce the quantity of memory consumed
  std::string m_name;                       /// The name of the quadrature rule
  int* m_numQuadratureRule = new int[1];    /// The number of quadrature rules
  const QuadratureRule* m_quadratureRule;   /// List of quadrature rule in strict increasing order of exactness (compulsory)
  int* m_maxDegreeOfExactness = new int[1]; /// The maximum level of exactness
  int* m_quadratureByDegreeOfExactness;     /// m_quadratureByDegreeOfExactness[i] is a quadrature rule integrating exactly polynoms of degree i

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};

///@}
///@name Type Definitions
///@{

extern const QuadratureRule quadratureRuleNULL;
extern const ListOfQuadratureRule listQuadratureRuleNULL;

extern const QuadratureRule quadratureRuleNode;
extern const ListOfQuadratureRule listQuadratureRuleNode;

extern const QuadratureRule quadratureRuleSeg1pt;
extern const QuadratureRule quadratureRuleSeg2pt;
extern const QuadratureRule quadratureRuleSeg3pt;
extern const ListOfQuadratureRule listQuadratureRuleSegment;

extern const QuadratureRule quadratureRuleTria1pt;
extern const QuadratureRule quadratureRuleTria3pt;
extern const QuadratureRule quadratureRuleMitc3;
extern const QuadratureRule quadratureRuleTria4pt;
extern const QuadratureRule quadratureRuleTria6pt;
extern const QuadratureRule quadratureRuleTria7pt;
extern const ListOfQuadratureRule listQuadratureRuleTriangle;

extern const QuadratureRule quadratureRuleQuad1pt;
extern const QuadratureRule quadratureRuleQuad4pt;
extern const QuadratureRule quadratureRuleQuad9pt;
extern const ListOfQuadratureRule listQuadratureRuleQuadrilateral;

extern const QuadratureRule quadratureRuleTetra1pt;
extern const QuadratureRule quadratureRuleTetra4pt;
extern const QuadratureRule quadratureRuleTetra5pt;
extern const QuadratureRule quadratureRuleTetra15pt;
extern const QuadratureRule quadratureRuleTetra64pt;
extern const ListOfQuadratureRule listQuadratureRuleTetrahedron;

extern const QuadratureRule quadratureRuleHexa1pt;
extern const QuadratureRule quadratureRuleHexa8pt;
extern const QuadratureRule quadratureRuleHexa27pt;
extern const ListOfQuadratureRule listQuadratureRuleHexahedron;
extern const ListOfQuadratureRule listQuadratureRuleHexahedronCombined;

extern const QuadratureRule quadratureRulePrism6pt;
extern const QuadratureRule quadratureRulePrism12ptComposite;
extern const QuadratureRule quadratureRulePrism21pt;
extern const QuadratureRule quadratureRulePrism42ptComposite;
extern const ListOfQuadratureRule listQuadratureRulePrism;
extern const ListOfQuadratureRule listQuadratureRulePrismCombined;

///@}
} /* namespace felisce.*/
#endif /* QUADRATURE_RULE_HPP */
