//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    JJ-F. Gerbeaun & V.Martin
//

#ifndef _CURRENT_FINITE_ELEMENT_WITH_BD_H
#define _CURRENT_FINITE_ELEMENT_WITH_BD_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "currentFiniteElement.hpp"
#include "curvilinearFiniteElement.hpp"
#include "DegreeOfFreedom/dof.hpp"

namespace felisce {
  /*!
   \class CurrentFiniteElementWithBd
   \authors J-F. Gerbeau and V. Martin
   \brief Class implementing a finite element with its boundary faces
   */
  class CurrentFiniteElementWithBd:
    public CurrentFiniteElement {
  protected:

    ///  num of boundary elements
    int m_numBdEle;

    ///  list of the point IDs on the boundary element
    ///  (m_pointBdEle[ibd][jptBd]: jth point ID of the ith boundary element)
    std::vector< std::vector<int> > m_pointBdEle;

    ///  list of num of points on the boundary element
    std::vector<int> m_numPointBdEle;

    ///  list of quadrature points on each boundary element of the reference element
    ///  (m_refQuadPointBdEle[ibd](jg): jth quad point of the ith boundary element)
    std::vector<std::vector<Point>> m_refQuadPointBdEle;

    ///  list of num of quad points on the boundary element
    std::vector<int> m_numQuadPointBdEle;

    ///  num total quad points : internal + boundary
    int m_numQuadraturePointInternAndBd;

    ///  the list of boundary elements
    CurvilinearFiniteElement** m_bdEle;

  public:

    /// constructors (vector of DegreeOfExactness)
    CurrentFiniteElementWithBd(
      const RefElement& refEle,
      const GeoElement& geoEle,
      const std::vector<DegreeOfExactness>& degOfExactness,
      const DegreeOfExactness& degOfExactnessBd
      );

    /// constructors
    CurrentFiniteElementWithBd(
      const RefElement& refEle,const GeoElement& geoEle,
      const DegreeOfExactness& degOfExactness,
      const DegreeOfExactness& degOfExactnessBd
      );

    /// Destructor
    ~CurrentFiniteElementWithBd() override;

    ///  what the use? REMOVE???
    void mapBd(int iface,double& xref3d,double& yref3d,double& zref3d,double xref2d,double yref2d);


    inline std::vector<int> pointBdEle(int ibd) const {
      return m_pointBdEle[ibd];
    }

    inline int numBdEle() const {
      return m_numBdEle;
    }
    inline int numPointBdEle(const std::size_t ibd) const {
      return m_numPointBdEle[ibd];
    }
    const std::vector<Point>& refQuadPointBdEle(const std::size_t ibd) const {
      return m_refQuadPointBdEle[ibd];
    }
    inline int numQuadPointBdEle(const std::size_t ibd) const {
      return m_numQuadPointBdEle[ibd];
    }
    inline int numQuadraturePointInternAndBd() const {
      return m_numQuadraturePointInternAndBd;
    }

    ///  access to the boundary elements
    const CurvilinearFiniteElement &  bdEle( const std::size_t ibd ) const;
    CurvilinearFiniteElement&  bdEle( const std::size_t ibd );
    CurvilinearFiniteElement* ptrBdEle(const std::size_t ibd) const;

    ///  minimal update: we just identify the id of a Bd element of the current element
    void updateBd(); //TODO where is the implementation??

    ///  compute the arrays meas, weightDet, jacobian on all Bd elements of the current element
    void updateBdMeas();
    ///  compute the arrays meas, weightDet, jacobian and quadPt ... TODO : complete the comment!
    ///  on all Bd elements of the current element
    void updateBdMeasNormal();

    ///  compute the images of the quadrature points through the geometrical mapping ON ALL Quad points
    ///  (internal and on the boundary elements)
    void computeCurrentQuadraturePointInternAndBd();

    void printBd(int verbose,std::ostream& c=std::cout) const;

    int Bd2Vol(const std::size_t iElBd, const std::size_t iElVol, const std::size_t dofBd, const std::size_t iCoor, const Dof& dof, const std::size_t idVar) const;
  };
}

#endif
