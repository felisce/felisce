//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin & Miguel Fernandez
//

#ifndef _ELEMVEC_H_INCLUDED
#define _ELEMVEC_H_INCLUDED

// System includes
#include <vector>
#include <atomic>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class ElementVector
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of ElementVector
  FELISCE_CLASS_POINTER_DEFINITION(ElementVector);

  ///@}
  ///@name Life Cycle
  ///@{

  // default constructor
  ElementVector() = default;

  //! constructor for an arbitrary number of finite elements
  ElementVector(const std::vector<const CurBaseFiniteElement*>& fe, const std::vector<std::size_t>& nbr);

  // // constructor for 1 finite element
  // FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor will be removed in the near future. Please use the main constructor which consider constant vector.")
  // ElementVector(const CurBaseFiniteElement& fe1, const std::size_t nbr1);

  // // constructor for 2 finite elements
  // FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor will be removed in the near future. Please use the main constructor which consider constant vector.")
  // ElementVector(const CurBaseFiniteElement& fe1, const std::size_t nbr1,
  //               const CurBaseFiniteElement& fe2, const std::size_t nbr2);

  // // constructor for 3 finite elements
  // FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor will be removed in the near future. Please use the main constructor which consider constant vector.")
  // ElementVector(const CurBaseFiniteElement& fe1, const std::size_t nbr1,
  //               const CurBaseFiniteElement& fe2, const std::size_t nbr2,
  //               const CurBaseFiniteElement& fe3, const std::size_t nbr3);


  // default destructor
  ~ElementVector() = default;

  ///@}
  ///@name Operators
  ///@{

  /*!
   * \brief operator +=
   *
   * This operator acts upon the underlying UBlasVector and is is assumed all the other attributes are the same (it
   * is checked by several assert in debug mode).
   */
  void operator+=(const ElementVector& vec);

  /*!
    * \brief operator -=
    *
    * This operator acts upon the underlying UBlasVector and is is assumed all the other attributes are the same (it
    * is checked by several assert in debug mode).
    */
  void operator-=(const ElementVector& vec);

  //! Operator multiplication by a scalar.
  void operator*=(const double factor);

  /**
   * @brief Access operators
   */
  const double& operator () (const std::size_t i) const {
    return m_vec[i];
  }

  const double& operator [] (const std::size_t i) const {
    return m_vec[i];
  }

  double& operator () (const std::size_t i) {
    return m_vec[i];
  }

  double& operator [] (const std::size_t i) {
    return m_vec[i];
  }

  ///@}
  ///@name Operations
  ///@{

  UBlasVectorRange vecBlock(const int i) {
    FEL_ASSERT(i<static_cast<int>(numBlockRow()));
    return UBlasVectorRange(m_vec,UBlasRange( m_firstRow[i], m_firstRow[i]+m_numRow[i]));
  }

  /**
   * @brief Size
   */
  std::size_t size() const {
    return m_vec.size();
  }

  /**
   * @brief Sets to zero the main vector
   */
  void zero() {
    m_vec.clear();
  }

  void source(const double coef,const CurBaseFiniteElement& fe,const ElementField& elField,const int iblock,const int numComp);

  void source_lumped(const double coef, const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock,const int numComp);

  void sourceForComp(const double coef, const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock,const int iComp);

  void grad_f_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield2,const int iblock=0);

  void curl_f_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield2,const int iblock=0);

  void WGX_dot_phi_i(const CurrentFiniteElement& feWGX, const CurrentFiniteElement& feWSS, const ElementField& WSSField, const int iblock);

  void WGY_dot_phi_i(const CurrentFiniteElement& feWGY, const CurrentFiniteElement& feWSS, const ElementField& WSSField, const int iblock);

  void WGZ_dot_phi_i(const CurrentFiniteElement& feWGZ, const CurrentFiniteElement& feWSS, const ElementField& WSSField, const int iblock);

  void stressX_dot_phi_i(const double viscosity, const CurrentFiniteElement& fePres, const CurrentFiniteElement& feStress,const CurrentFiniteElement& feVel,const ElementField& elfield_pressure,const ElementField& elfield_velocity,const int iblock=0);

  void stressY_dot_phi_i(const double viscosity, const CurrentFiniteElement& fePres, const CurrentFiniteElement& feStress,const CurrentFiniteElement& feVel,const ElementField& elfield_pressure,const ElementField& elfield_velocity,const int iblock=0);

  void stressZ_dot_phi_i(const double viscosity, const CurrentFiniteElement& fePres, const CurrentFiniteElement& feStress,const CurrentFiniteElement& feVel,const ElementField& elfield_pressure,const ElementField& elfield_velocity,const int iblock=0);

  void grad_f_grad_phi_i(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield,const int iblock=0);

  void grad_f_grad_phi_i(const double coef,const CurvilinearFiniteElement& fe, const ElementField& elfield, const std::size_t iblock,const std::size_t num);

  void f_grad_phi_i(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield,const int iblock=0);

  void div_f_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield1,const int iblock=0);

  void f_div_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield2,const int iblock=0);

  // \int_Gamma f v \cdot n (To compute /intGamma p v.n for instance)  Attention ! is this the same of: f_n_dot_phi_i ? (different only for DOf case)
  //----------------------------------------------------------------------
  void f_phi_i_scalar_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock=0);

  // \int_Gamma f v \cdot J F^-T n (to evaluate follower pressure residual)
  //----------------------------------------------------------------------
  void f_phi_i_scalar_piola_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elf,const ElementField& elfd,const int iblock=0);

  /// contact
  // /int_Gamma [disp \cdot n - gap ]_+  v \cdot n
  //----------------------------------------------------------------------
  void penalty_contact_phi_i_scalar_n(const double coef,const std::vector<double>& normal, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap);

  /// penalization valves
  // int_Gamma [u^(n,k-1) cdot n]_+ v
  //----------------------------------------------------------------------
  void penalty_phi_i_scalar_n(const double coef, CurvilinearFiniteElement& fe, const ElementField& vel, std::size_t iblock);

  /// contact
  // /int_Gamma [disp \cdot n - gap ]_+  v \cdot n
  //----------------------------------------------------------------------
  void nitsche_contact_phi_i_scalar_n(const double coef,const std::vector<double>& normal,
                  const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap, const ElementField& lambda);

  /// contact
  // /int_Gamma [disp \cdot n - gap ]_+  v \cdot n
  //----------------------------------------------------------------------
  void nitsche_contact_phi_i_scalar_n(const double coef,
                  const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap, const ElementField& normal, const ElementField& lambda);

  // \int_Gamma [ p^n v \cdot n ] Attention ! is this the same of: f_phi_i_scalar_n ? (different only for DOf case)
  //----------------------------------------------------------------------
  void f_n_dot_phi_i(const double coef,const CurvilinearFiniteElement& fe1,const CurvilinearFiniteElement& fe2, const ElementField& elfield,const int iblock);

  //----------------------------------------------------------------------
  // ex. int_Sigma [ u^{n+1} * N * q ]
  void f_dot_n_phi_i(const double coef,const CurvilinearFiniteElement& fe1,const CurvilinearFiniteElement& fe2, const ElementField& elfield,const int iblock);

  // For Nitsche-XFEM formulation
  void g_dot_n_f_phi_i(const double coef, const CurrentFiniteElement& fe, const ElementField& elt_f, const ElementField& elt_g, const ElementField& elt_n, felInt iblock, felInt numComp);

  //----------------------------------------------------------------------
  // operator \int_{Omega}  Grad u^{n} dot Grad LS^{n} / norm(Grad LS^{n}) v (LS^{n} a level std::set of u^{n})
  void grad_u_dot_grad_LSu_v(const double coef,const ElementField& u,const ElementField& LSu,const std::vector<double>& data,double avHeavU0,double avHeavMinusU0,double insidePar,double outsidePar,const CurvilinearFiniteElement& fe,std::size_t iblock);

  /**
    *Operator \f[\int_\Sigma \nabla v_1 \cdot v_2 \cdot \phi_i\f]
    */
  void grad_vec2_dot_vec1_dot_phi_i(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp);

  //----------------------------------------------------------------------
  // Operator \int_\Sigma \eps vec2 \dot vec1 \dot \phi_i
  void eps_vec2_dot_vec1_dot_phi_i(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp);

  //----------------------------------------------------------------------
  // Operator \int_\Sigma (\grad \phi_i \dot vec1) \dot vec2
  void grad_phi_i_dot_vec1_dot_vec2(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp);

  //----------------------------------------------------------------------
  // Operator \int_\Sigma (\eps \phi_i \dot vec1) \dot vec2
  void eps_phi_i_dot_vec1_dot_vec2(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp);

  //----------------------------------------------------------------------
  // Operator \int_\Sigma (\eps vec2 vec1) \dot \esp \phi_i vec1
  void eps_vec2_vec1_dot_eps_phi_i_vec1(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp);

  /**
    * Operator \f[\int_\Sigma \nabla vec_2 vec_1 \cdot \nabla \phi_i vec1\f]
    */
  void grad_vec2_vec1_dot_grad_phi_i_vec1(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp);

  //----------------------------------------------------------------------
  // Operators \f$ \int_{\Sigma} (\grad \phi_i \dot n) f \f$,
  //! where $\Sigma$ is a boundary element. (For Nitsche's treatment...)
  void grad_phi_i_dot_n_f(const double coef,const CurrentFiniteElementWithBd& fewbd,const ElementField& elfield,
                          const int iblockBd, const int numblockBd,const int iblock, const int numComp);

  //----------------------------------------------------------------------
  // Operators \f$ \int_{\Sigma} (\grad f \dot n) \phi_i \f$,
  //! where $\Sigma$ is a boundary element. (For Nitsche's treatment...)
  void grad_f_dot_n_phi_i(const double coef,const CurrentFiniteElementWithBd& fewbd, const ElementField& elfield,
                          const int iblockBd, const int numblockBd, const int iblock, const int numComp);
  void grad_f_Transp_dot_n_phi_i(const double coef,const CurrentFiniteElementWithBd& fewbd,
                                  const ElementField& elfield,
                                  const int iblockBd, const int numblockBd,
                                  const int iblock, const int numComp) ;  // not tested yet, saverio 8/10/2012

  //operator \int_{\Sigma} [ f^{n-1}\cdot n ] f^{n}\phi_i
  void f_dot_n_f_phi_i(const double coef,const CurvilinearFiniteElement& fe,
                        const ElementField& elfield1,const ElementField& elfield2,
                        const int iblock, const int numComp);

  //ATTENTION RANGEMENT
  //computes std::vector with component int(element fe)(coef*div(elfield)*div(phi_i)) at position i
  void div_u_div_phi_i(const double coef,const CurrentFiniteElement& fe, const ElementField& elfield, const int iblock);

  //computes vector with component int(element fe)(elfield.phi_i) at position i
  void f_scalar_phi_i(const double coef,const CurrentFiniteElement& fe, const ElementField& elfield,const int iblock);

  //! Hyperelasticity case
  void GradientBasedHyperElasticityVector_grad_phi_i(const double coef,
      const CurrentFiniteElement& feDisplacement,
      int quadraturePointIndex,
      UBlasVector& rhsPart,
      std::size_t iblock);

  void sourceHyp1DCont(std::vector<double>& coef,const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,
                        const ElementField& elfield1,const ElementField& elfield2,const int iblock=0,int iterm=0);

  void sourceHyp1DMom(std::vector<double>& coef,const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,
                      const ElementField& elfield1,const ElementField& elfield2,const int iblock=0,int iterm=0);

  //----------------------------------------------------------------------
  // term
  // int_Sigma 1/2(|| w.n || - w.n ) (w.phi)
  void abs_u_dot_n_u_dot_phi_i(const double coef,const ElementField& vel,const CurvilinearFiniteElement& feBd,
                                const int iblock, const int numComp);

  void u_grad_u_phi_i(const double coef,const CurrentFiniteElement& fe,const ElementField& elField,const int iblock,const int numComp);

  void u_grad_u_grad_phi_i(const double coef,const CurrentFiniteElement& fe,const ElementField& elField,const int iblock,const int numComp);

  // /int_Gamma ( f \cdot n ) ( v \cdot n )
  void f_dot_n_phi_i_dot_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock);

  // /int_Gamma ( f \times n ) ( v \times n )
  //----------------------------------------------------------------------
  void f_times_n_phi_i_times_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock);

  // /int_Gamma ( \Grad f n \times n ) ( v \times n )
  //----------------------------------------------------------------------
  void grad_f_dot_n_times_n_phi_i_times_n(const double coef,const CurrentFiniteElementWithBd& fewbd, const ElementField& elfield,
                                          const int iblockBd, const int numblockBd, const int iblock, const int numComp);

  void stab_supgAdvDiffCT(const double dt, const double stab1, const double density, const double visc,
              const CurrentFiniteElement& feVel, const CurrentFiniteElement& fePres,
              const ElementField& vel, const ElementField& pres, const ElementField& rhs);

  void stab_supgProjCT(const double dt, const double stab1, const double density, const double visc, const CurrentFiniteElement& fe,
            const ElementField& beta, const ElementField& vel, const ElementField& pres,
            const ElementField& rhs);

  void stab_supgRhsFS(const double dt, const double stabSUPG, const double density, const double viscosity,
                      const double c0, const double c1, const double c2,
                      const CurrentFiniteElement& feVel, const CurrentFiniteElement& fePre,
                      const ElementField& vel, const ElementField& pres, const ElementField& velRHS);

  //----------------------------------------------------------------------
  // qudratic term: alpha(x) T^2 (for scalars!)
  void quadratic(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield, const int iblock,const int numComp);

  void sGrad_phi_i_tensor_sGrad_f(const double coef, const ElementField& f, const ElementField& normalField, const CurvilinearFiniteElement& fe, UBlasMatrix tensor);
  void sGrad_phi_i_tensor_sGrad_f(const double coef, const ElementField& elemCoef, const ElementField& f, const ElementField& normalField, const CurvilinearFiniteElement& fe, UBlasMatrix tensor);

  //---------------------------------------------------------------------------------------------------------------
  // Some explanation about SUPG:
  // noting T:time, L:length, M:mass
  // WARNING: tau has a dimension of T*L^3*M^{-1} -> remember this when you use P1-P1
  // It's advised to choose your units knowing this.
  //---------------------------------------------------------------------------------------------------------------
  void stab_supg(const double dt, const double stab1, const double density, const double visc,
          const CurrentFiniteElement& fe,const ElementField& vel,const ElementField& rhs,
          double& Re_elem,double& tau, const int type);

  void transpirationSurfStabRHS( const double coef, const ElementField& elemCoef, const ElementField& normalField, const ElementField& force, const CurvilinearFiniteElement& fe, UBlasMatrix tensor);

  // computes non-linear residual associated to \int_G(d) p n_K \cdot v for thin-walled solids
  void followerPressure(const double coef, const ElementField& press, const ElementField& disp, const CurvilinearFiniteElement& fe);

  void velocityConstraint(const double coef, const ElementField& disp, const CurvilinearFiniteElement& fe, const int iblock);

  ///@}
  ///@name Access
  ///@{

  UBlasVector& vec() {
    return m_vec;
  }

  const UBlasVector& vec() const {
    return m_vec;
  }

  std::size_t numBlockRow() const {
    return m_numRow.size();
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout);

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  UBlasVector m_vec; //!< The element vectorrows
  std::vector<std::size_t> m_numRow; // m_numRow[i]=nb of rows in the i-th block row
  std::vector<std::size_t> m_firstRow; //m_firstRow[i]=index of first row of i-th block

  // TODO:: All temp classes should be replaced with on time execution (otherwise much memory consumed)
  UBlasVector m_tmpVecDof;//!< temporary vector (size = numDof)
  UBlasVector m_tmpVecComp;//!< temporary vector (size = numComp)
  UBlasVector m_tmpVecCoor;//!< temporary vector (size = numCoor)
  UBlasVector m_tmpVec2Coor;//!< temporary vector (size = numCoor)
  UBlasMatrix m_tmpMatDof;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif
