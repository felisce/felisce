//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

// System includes
#include <iostream>

// External includes

// Project includes
#include "refShape.hpp"

namespace felisce 
{
RefShape::RefShape(std::string name,int numCoor,int numVertex,int numEdge,int numFace,TypeShape typeShape):
  m_name(name),m_numCoor(numCoor),m_numVertex(numVertex),m_numEdge(numEdge),m_numFace(numFace),m_typeShape(typeShape)
{
  
}

/***********************************************************************************/
/***********************************************************************************/

void RefShape::print(int verbose,std::ostream& c) const {
  if(verbose) {
    c << "RefShape: " << std::endl;
    c << " name: " << m_name << std::endl;
    c << " numCoor: " << m_numCoor << std::endl;
    c << " numVertex: " << m_numVertex << std::endl;
    c << " numEdge: " << m_numEdge << std::endl;
    c << " numFace: " << m_numFace << std::endl;
  }
}
}
