//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes
#include <cmath>

// External includes

// Project includes
#include "FiniteElement/currentFiniteElement.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce
{
CurrentFiniteElement::CurrentFiniteElement(
  const RefElement& refEle,
  const GeoElement& geoEle, 
  const std::vector<DegreeOfExactness>& degOfExactness
  ) : CurBaseFiniteElement(refEle, geoEle, degOfExactness)
{
  dPhi.resize(m_numQuadraturePoint);
  m_jacobian.resize(m_numQuadraturePoint);

  m_indexQuadPoint.resize(2);
  m_indexQuadPoint[0] = 0;
  m_indexQuadPoint[1] = m_numQuadraturePoint;

  for(int ig=m_indexQuadPoint[0]; ig<m_indexQuadPoint[1]; ig++) {
    m_jacobian[ig].resize(m_numCoor,m_numCoor);
    dPhi[ig].resize(m_numRefCoor,m_numDof);
  }
}

/***********************************************************************************/
/***********************************************************************************/

CurrentFiniteElement::CurrentFiniteElement(
  const RefElement& refEle,
  const GeoElement& geoEle, 
  const DegreeOfExactness& degOfExactness
  ) :  CurrentFiniteElement(refEle, geoEle, std::vector<DegreeOfExactness>(1, degOfExactness))
{
}

/***********************************************************************************/
/***********************************************************************************/

CurrentFiniteElement::~CurrentFiniteElement()
{
  m_jacobian.clear();
  dPhi.clear();
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::allocateOnDofsDataStructures()
{
  //Experimental..it has been tested ONLY on P1 tetra, but it should be more general (matteo)
  m_dPhiRefOnDofs.resize(m_numDof);
  m_phiGeoOnDofs.resize(m_numDof);
  m_dPhiGeoOnDofs.resize(m_numDof);
  dPhiOnDofs.resize(m_numDof);
  phiOnDofs.resize(m_numDof);
  m_jacobianOnDofs.resize(m_numDof);
  for ( int idof(0); idof<m_numDof; idof++ ) {
    m_dPhiRefOnDofs[idof].resize(m_numRefCoor,m_numDof);
    m_phiGeoOnDofs[idof].resize(m_numPoint);
    m_dPhiGeoOnDofs[idof].resize(m_numRefCoor,m_numPoint);
    phiOnDofs[idof].resize(m_numDof);
    dPhiOnDofs[idof].resize(m_numRefCoor,m_numDof);
    m_jacobianOnDofs[idof].resize(m_numCoor,m_numCoor);

    QuadraturePoint point(0.,0.,0.,0.);
    for (int pcoor=0; pcoor<m_numRefCoor; pcoor++ ) {
      point.coor(pcoor)=m_refEle.nodeCoor(idof,pcoor);
    }
    for(int i=0; i<m_numDof; i++ ) {
      phiOnDofs[idof](i) = m_refEle.basisFunction().phi(i, point);
      for(int icoor=0; icoor<m_numRefCoor; icoor++ ) {
        m_dPhiRefOnDofs[idof](icoor,i) = m_refEle.basisFunction().dPhi(i,icoor, point);
      }
    }
    for(int k=0; k<m_numPoint; k++) {
      m_phiGeoOnDofs[idof](k)= m_geoEle.basisFunction().phi(k, point);
      for(int icoor=0; icoor<m_numRefCoor; icoor++) {
        m_dPhiGeoOnDofs[idof](icoor,k) = m_geoEle.basisFunction().dPhi(k,icoor, point);
      }
    }
  }
  m_phiDPhiOnDofs=true;
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::computeJacobian(const int iBlockBd,const std::size_t numBlockBd)
{
  FEL_ASSERT( numBlockBd > 0 && numBlockBd < m_indexQuadPoint.size() );
  // derivatives of geo map:
  for ( int ig = m_indexQuadPoint[iBlockBd]; ig <m_indexQuadPoint[iBlockBd+numBlockBd]; ig++) {
    m_jacobian[ig] = prod(m_dPhiGeo[ig],m_point);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::computeJacobianOnDofs()
{
  // derivatives of geo map:
  for ( int idof(0); idof<m_numDof; ++idof) {
    m_jacobianOnDofs[idof] = prod(m_dPhiGeoOnDofs[idof],m_point);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::computeMeas()
{
  for (int ig = 0; ig <m_numQuadraturePoint; ig++) {
    m_meas(ig) = MathUtilities::Det(m_jacobian[ig]);
    weightMeas(ig) = m_meas(ig) * m_quadratureRule->weight(ig);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::computeMeasDer(const int iBlockBd,const std::size_t numBlockBd)
{
  FEL_ASSERT( numBlockBd > 0 && numBlockBd < m_indexQuadPoint.size() );
  UBlasMatrix inv_jacobian(m_numCoor,m_numCoor);
  double det;
  for (int ig = m_indexQuadPoint[iBlockBd]; ig <m_indexQuadPoint[iBlockBd+numBlockBd]; ig++) {
    MathUtilities::InvertMatrix(m_jacobian[ig], inv_jacobian, det);
    //! on internal quad points only (boundary quad pts are set from CurvilinearFE):
    if ( ig >=0 && ig < m_numQuadraturePoint ) {
      m_meas(ig) = det;
      weightMeas(ig) = m_meas(ig) * m_quadratureRule->weight(ig);
    }
    noalias(dPhi[ig]) = prod(inv_jacobian, m_dPhiRef[ig]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::computeDerOnDofs()
{
  UBlasMatrix inv_jacobian(m_numCoor,m_numCoor);
  double det;
  for (int idof(0); idof<m_numDof; ++idof) {
    MathUtilities::InvertMatrix(m_jacobianOnDofs[idof], inv_jacobian, det);
    noalias(dPhiOnDofs[idof]) = prod(inv_jacobian, dPhiOnDofs[idof]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::computePiolaMeasDer(const int iBlockBd,const std::size_t numBlockBd, const std::vector<double>& normal)
{
  FEL_ASSERT( numBlockBd > 0 && numBlockBd < m_indexQuadPoint.size() );
  FEL_ASSERT( (felInt)normal.size() == m_numCoor);

  UBlasMatrix inv_jacobian(m_numCoor,m_numCoor);
  UBlasVector meas(m_numCoor);
  double det;

  for (int ig = m_indexQuadPoint[iBlockBd]; ig <m_indexQuadPoint[iBlockBd+numBlockBd]; ig++) {
    for(felInt i=0; i<m_numCoor; ++i)
      meas(i) = normal[i];

    MathUtilities::InvertMatrix(m_jacobian[ig], inv_jacobian, det);
    noalias(dPhi[ig]) = prod(inv_jacobian, m_dPhiRef[ig]);

    if ( ig >=0 && ig < m_numQuadraturePoint )
      m_meas(ig) = det;

    // Now, the norm of the inverse of the transpose of the jacobian times the normal vector
    meas = prod(inv_jacobian, meas);

    //! On internal quad points only (boundary quad pts are set from CurvilinearFE):
    if ( ig >=0 && ig < m_numQuadraturePoint ) {
      m_meas(ig) *= norm_2(meas);
      weightMeas(ig) = m_meas(ig) * m_quadratureRule->weight(ig);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::update(const int id,const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = false;
  m_hasFirstDeriv = false;
  m_hasSecondDeriv = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::updateMeas(const int id,const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasFirstDeriv = false;
  m_hasSecondDeriv = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeJacobian(0,m_indexQuadPoint.size()-1); // on all quad points
  computeMeas();
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::updateMeasQuadPt(const int id,const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasFirstDeriv = false;
  m_hasSecondDeriv = false;
  m_hasQuadPtCoor = false; // std::set to "true" in computeCurrentQuadraturePoint()
  m_currentId = id;
  computeJacobian(0,m_indexQuadPoint.size()-1); // on all quad points
  computeMeas();
  computeCurrentQuadraturePoint();
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::updateFirstDeriv(const int id,const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = true;
  m_hasFirstDeriv = true;
  m_hasSecondDeriv = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
  computeJacobian(0,m_indexQuadPoint.size()-1); // on all quad points
  computeMeasDer(0,m_indexQuadPoint.size()-1);
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::updateFirstDerivOnDofs(const int id,const std::vector<Point*>& point)
{
  updateFirstDeriv(id,point);
  if ( m_phiDPhiOnDofs == false ) {
    FEL_ERROR("Error: you should call allocateOnDofsDataStructures");
  }
  computeJacobianOnDofs();
  computeDerOnDofs();
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::updateBasisAndFirstDeriv(const int id, const std::vector<Point*>& point) {
  updatePoint(point);
  m_hasMeas = true;
  m_hasFirstDeriv = true;
  m_hasSecondDeriv = false;
  m_hasQuadPtCoor = false;
  m_hasOriginalQuadPoint = true;
  m_currentId = id;

  std::vector<Point> pts(m_numQuadraturePoint);
  for(felInt ipt=0; ipt<m_numQuadraturePoint; ++ipt)
    pts[ipt] = m_quadratureRule->quadraturePoint(ipt);

  // recompute the basis function at the original quadrature point
  updateBasisWithQuadPoint(pts);

  // first deriv
  computeJacobian(0, m_indexQuadPoint.size()-1);

  computeMeasDer(0, m_indexQuadPoint.size()-1);
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::updateSubElementFirstDeriv(const int id, const std::vector<Point*>& ptElem, const std::vector<Point*>& ptSubElem, CurBaseFiniteElement* feSurf)
{
  updatePoint(ptElem);
  m_hasMeas = true;
  m_hasFirstDeriv = true;
  m_hasSecondDeriv = false;
  m_hasQuadPtCoor = false;
  m_hasOriginalQuadPoint = false;
  m_currentId = id;

  // find the coordinate of the sub element in the reference element of the element
  std::vector<Point*> ptSubRef(ptSubElem.size());
  switch(m_geoEle.shape().typeShape()) {

    case Quadrilateral: {
      // square
      for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
        ptSubRef[ipt] = new Point();
        ptSubRef[ipt]->x() = 2.*(ptSubElem[ipt]->x() - ptElem[0]->x())/(ptElem[2]->x() - ptElem[0]->x()) - 1.;
        ptSubRef[ipt]->y() = 2.*(ptSubElem[ipt]->y() - ptElem[0]->y())/(ptElem[2]->y() - ptElem[0]->y()) - 1.;
      }
      break;
    }

    case Triangle: {
      // triangle
      // this value can not be equal to zero unless the triangle is flat.
      const double det = (ptElem[2]->y() - ptElem[0]->y())*(ptElem[1]->x() - ptElem[0]->x())
                       - (ptElem[2]->x() - ptElem[0]->x())*(ptElem[1]->y() - ptElem[0]->y());

      for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
        ptSubRef[ipt] = new Point();

        ptSubRef[ipt]->x() = (ptElem[2]->y() - ptElem[0]->y())*(ptSubElem[ipt]->x() - ptElem[0]->x())/det
                           + (ptElem[0]->x() - ptElem[2]->x())*(ptSubElem[ipt]->y() - ptElem[0]->y())/det;

        ptSubRef[ipt]->y() = (ptElem[0]->y() - ptElem[1]->y())*(ptSubElem[ipt]->x() - ptElem[0]->x())/det
                           + (ptElem[1]->x() - ptElem[0]->x())*(ptSubElem[ipt]->y() - ptElem[0]->y())/det;
      }
      break;
    }

    case Tetrahedron: {
      // coordinates of the subElement in the reference Tetrahedra
      // inversion of the map matrix
      //std::cout << " Vertex Current Elem K: ";
      //for( int it=0; it<ptElem.size(); ++it){
      //  ptElem[it]->print(10,std::cout);
      //  std::cout<<std::endl;
      //}
      //std::cout << " Vertex Current SubElem K_i: ";
      //for( int it=0; it<ptSubElem.size(); ++it){
      //  ptSubElem[it]->print(10,std::cout);
      //  std::cout<<std::endl;
      //}

      UBlasMatrix FLU(m_numCoor,m_numCoor);
      UBlasMatrix inv_FLU(m_numCoor,m_numCoor);
      UBlasVector refPoint(m_numCoor);
      double det;

      // Filling the matrix
      for(int indx = 0; indx < m_numCoor ; ++indx){
        FLU(0,indx) = ptElem[indx+1]->x() - ptElem[0]->x();
        FLU(1,indx) = ptElem[indx+1]->y() - ptElem[0]->y();
        FLU(2,indx) = ptElem[indx+1]->z() - ptElem[0]->z();
      }

      MathUtilities::InvertMatrix(FLU, inv_FLU, det);

      for(std::size_t ipt=0; ipt<ptSubElem.size(); ++ipt) {
        // Filling the rhs
        refPoint[0] = ptSubElem[ipt]->x() - ptElem[0]->x();
        refPoint[1] = ptSubElem[ipt]->y() - ptElem[0]->y();
        refPoint[2] = ptSubElem[ipt]->z() - ptElem[0]->z();

        refPoint = prod(inv_FLU,refPoint);

        ptSubRef[ipt] = new Point();
        ptSubRef[ipt]->x() = refPoint[0];
        ptSubRef[ipt]->y() = refPoint[1];
        ptSubRef[ipt]->z() = refPoint[2];
      }

      break;
    }

    case NullShape:
    case Node:
    case Segment:
    case Hexahedron:
    case Prism:
    case Pyramid: {
      FEL_ERROR("The transformation is not implemented for this type of mesh cells");
      break;
    }
  }

  // create the current finite element for the sub element and update it
  CurBaseFiniteElement* feSubElt;
  if(feSurf == nullptr) {
    // volume sub elem
    feSubElt = new CurrentFiniteElement(m_refEle, m_geoEle, (DegreeOfExactness) m_quadratureRule->degreeOfExactness());
    ((CurrentFiniteElement*) feSubElt)->updateMeasQuadPt(id, ptSubRef);
  } else {
    // curvi sub elem
    feSubElt = new CurvilinearFiniteElement(feSurf->refEle(), feSurf->geoEle(), (DegreeOfExactness) feSurf->quadratureRule().degreeOfExactness());
    ((CurvilinearFiniteElement*) feSubElt)->updateMeasQuadPt(id, ptSubRef);
  }

  // recompute the basis function at the new quadrature points for the element
  updateBasisWithQuadPoint(feSubElt->currentQuadPoint);

  // update the derivatives for the element (no need to recompute the jacobian, it's
  // a constant if the mapping from the reference element to the current element is affine)
  computeJacobian(0, m_indexQuadPoint.size()-1);

  if(feSurf == nullptr){

    computeMeasDer(0, m_indexQuadPoint.size()-1);
  }
  else {
    std::vector<double> normal(m_numCoor);
    double norm;
    if(m_numCoor == 2) {
      normal[0] = ptSubRef[0]->y() - ptSubRef[1]->y();
      normal[1] = ptSubRef[1]->x() - ptSubRef[0]->x();
      norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1]);
      normal[0] /= norm;
      normal[1] /= norm;
    } else {
      // normal in 3d
      normal[0] = (ptSubRef[1]->y() - ptSubRef[0]->y())*(ptSubRef[2]->z() - ptSubRef[0]->z()) - ((ptSubRef[1]->z() - ptSubRef[0]->z())*(ptSubRef[2]->y() - ptSubRef[0]->y()));
      normal[1] = (ptSubRef[1]->z() - ptSubRef[0]->z())*(ptSubRef[2]->x() - ptSubRef[0]->x()) - ((ptSubRef[1]->x() - ptSubRef[0]->x())*(ptSubRef[2]->z() - ptSubRef[0]->z()));
      normal[2] = (ptSubRef[1]->x() - ptSubRef[0]->x())*(ptSubRef[2]->y() - ptSubRef[0]->y()) - ((ptSubRef[2]->x() - ptSubRef[0]->x())*(ptSubRef[1]->y() - ptSubRef[0]->y()));
      norm = std::sqrt(normal[0]*normal[0] + normal[1]*normal[1] + normal[2]*normal[2]);
      normal[0] /= norm;
      normal[1] /= norm;
      normal[2] /= norm;
    }

    computePiolaMeasDer(0, m_indexQuadPoint.size()-1, normal);
  }

  // change the weight of the quadrature point
  for(int ig=0; ig<feSubElt->numQuadraturePoint(); ++ig) {
    weightMeas(ig) = m_meas(ig) * feSubElt->weightMeas(ig);
  }
  for(int ig=feSubElt->numQuadraturePoint(); ig<m_numQuadraturePoint; ++ig) {
    weightMeas(ig) = 0.;
  }

  // delete
  for(std::size_t ipt=0; ipt<ptSubRef.size(); ++ipt)
    delete ptSubRef[ipt];
  ptSubRef.clear();

  delete feSubElt;
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::identifyLocBDDof(const CurvilinearFiniteElement& curvFe, std::vector<int>& map) const
{
  // This function compare the point supporting dofs of a curvilinear finite element,
  // with the point supporting dofs of "this" a current finite element.
  // the return value "std::unordered_map" connects the local ids of the dofs on the curvFe to the loc ids of the dofs
  // on the currentFiniteElement.
  // You probably do not want to use this function and you would like to use the currentFiniteElementWithBD
  // Which does similar things in a better way.
  map.resize(curvFe.numDof());
  Point p,pref,pv,pvref;
  for (int iBD(0); iBD<curvFe.numDof();iBD++){
    map[iBD]=-1;
    for (int pcoor=0; pcoor<m_numRefCoor-1; pcoor++ ) {
      pref.coor(pcoor)=curvFe.refEle().nodeCoor(iBD,pcoor);
    }
    curvFe.coorMap(p,pref);
    for (int iV(0); iV<this->numDof();iV++){
      for (int pcoor=0; pcoor<m_numRefCoor; pcoor++ ) {
        pvref.coor(pcoor)=m_refEle.nodeCoor(iV,pcoor);
      }
      coorMap(pv,pvref);
      if ( p == pv ) {
        map[iBD]=iV;
      }
    }
    if (map[iBD]==-1) {
      FEL_ERROR("err");
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElement::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "CurrentFiniteElement: " << "\n";
    m_printBase(verbose, c);
    int theNumQuadPointTotal = m_indexQuadPoint.back();
    if ( theNumQuadPointTotal != m_numQuadraturePoint ) {
      c << " numQuadPoint: " << m_numQuadraturePoint << "\n";
      c << " numQuadPointTotal(intern+boundary): " << theNumQuadPointTotal << "\n";
    }

    if( m_hasPoint && verbose > 1 ) c << " point = " << m_point << "\n";
    if( m_hasMeas && verbose > 1 )  c << " meas   = " << measure() << "\n";
    if( verbose > 2 ) {
      if ( theNumQuadPointTotal != m_numQuadraturePoint ) {
        c << " The numQuadPoint=" << m_numQuadraturePoint << " first are internal. The others live on the boundary elements of the element.\n";
      }
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " phi[ig=" <<ig<< "] = " << phi[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " phiGeo[ig=" <<ig<< "] = " << m_phiGeo[ig] << "\n";
      }
    }
    if( m_hasFirstDeriv && verbose > 2 ) {
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " dphi[ig=" <<ig<< "] = " << dPhi[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " dphiGeo[ig=" <<ig<< "] = " << m_dPhiGeo[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " dphiRef[ig=" <<ig<< "] = " << m_dPhiRef[ig] << "\n";
      }
    }
    if( m_hasFirstDeriv && verbose > 2 ) {
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " jacobian[ig=" <<ig<< "] = " << m_jacobian[ig] << "\n";
      }
    }
    if( m_hasQuadPtCoor && verbose > 2 ) {
      for (int ig = 0; ig <theNumQuadPointTotal; ig++ ) {
        c << " currentQuadPoint[ig=" <<ig<< "] : ";
        currentQuadPoint[ig].print(verbose,c);
      }
    }
    if( m_phiDPhiOnDofs && verbose > 2 ) {
      for (int idof = 0; idof <m_numDof; idof++ ) {
        c << " phiOnDofs[idof=" <<idof<< "] : "<<phiOnDofs[idof]<<std::endl;
      }
      for (int idof = 0; idof <m_numDof; idof++ ) {
        c << " dPhiOnDofs[idof=" <<idof<< "] : "<<dPhiOnDofs[idof]<<std::endl;
      }
      for (int idof = 0; idof <m_numDof; idof++ ) {
        c << " jacobianOnDodfs[idof=" <<idof<< "] : "<<m_jacobianOnDofs[idof]<<std::endl;
      }
      for (int idof = 0; idof <m_numDof; idof++ ) {
        c << " phiGeoOnDofs[idof=" <<idof<< "] : "<<m_phiGeoOnDofs[idof]<<std::endl;
      }
      for (int idof = 0; idof <m_numDof; idof++ ) {
        c << " dPhiGeoOnDofs[idof=" <<idof<< "] : "<<m_dPhiGeoOnDofs[idof]<<std::endl;
      }
      for (int idof = 0; idof <m_numDof; idof++ ) {
        c << " dPhiRefOnDofs[idof=" <<idof<< "] : "<<m_dPhiRefOnDofs[idof]<<std::endl;
      }
    }
    if ( verbose > 3) {
      m_quadratureRule->print(verbose);
    }
    std::cout << std::endl;
  }
}
}
