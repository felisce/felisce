//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

#ifndef _REFERENCE_ELEMENT_H
#define _REFERENCE_ELEMENT_H

// System includes
#include <unordered_map>
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Geometry/point.hpp"
#include "FiniteElement/basisFunction.hpp"
#include "FiniteElement/refShape.hpp"
#include "FiniteElement/quadratureRule.hpp"

namespace felisce
{
enum DegreeOfFreedomSupport {DOF_NODE_VERTEX,DOF_NODE_EDGE,DOF_NODE_FACE,DOF_NODE_VOLUME,DOF_EDGE,DOF_FACE,DOF_VOLUME};
enum DegreeOfFreedomType {DOF_VALUE,DOF_X_DERIVATIVE,DOF_Y_DERIVATIVE,DOF_Z_DERIVATIVE,DOF_NORMAL_DERIVATIVE,
                          DOF_MEAN,DOF_FLUX,DOF_CIRCULATION
                          };
/*!
  \class RefElement
  \brief The class implementing a reference finite element
  \authors J-F. Gerbeau
  */
class RefElement {
  mutable std::unordered_map<DegreeOfFreedomSupport,std::string> m_nameSupportDOF; //! \todo Make it static
  mutable std::unordered_map<DegreeOfFreedomType,std::string> m_nameTypeDOF; //! \todo Make it static
  std::string m_name;
  const RefShape& m_refShape;
  const BasisFunction& m_basisFunction;
  // Currently not used const ListOfQuadratureRule& m_listOfQuadratureRule;
  const DegreeOfFreedomType* m_typeDOF;
  const DegreeOfFreedomSupport* m_supportDOF;
  const int* m_idSupportOfDOF; //!< if the dof is on a node, this is the id of the node, if the dof is on an edge, it's the id of the edge, etc.
  const Point* m_node; //!< the points supporting the nodes. Compulsory order: nodes on vertex, edge, face, volume
  const int m_numNodeVertex; //!< Number of nodes located on vertices
  const int m_numNodeEdge;//!<  Number of nodes located on edges
  const int m_numNodeFace;//!< Number of nodes located on faces
  const int m_numNodeVolume;//!< Number of nodes located on the volume
  const int m_numNode; //!< m_numNodeVertex + m_numNodeEdge + m_numNodeFace + m_numNodeVolume;
  const int m_numDOF; //!< = m_basisFunction.size()
  const int m_numVertex; //!< m_refShape.numVertex()
  const int m_numEdge; //!< m_refShape.numEdge()
  const int m_numFace; //!< m_refShape.numFace()
  const int m_numCoor; //!< m_refShape.numCoor()
  const RefElement** m_boundaryRefElement; // faces for 3D elements, edges for 2D elements, NULL for 1D elements
  // Currently not Used    const RefElement** m_boundaryBoundaryRefElement; // edges for 3D elements, NULL for 2D and 1D elements
  std::vector< std::vector<int> > m_idDOFVertex; //!< list of DOFs per vertex
  std::vector< std::vector<int> > m_idDOFEdge; //!< list of DOFs per edge
  std::vector< std::vector<int> > m_idDOFFace;//!< list of DOFs per face
  std::vector<int> m_idDOFVolume; //!< list of DOFs on the volume
  std::vector<const Point* > m_nodeDOF; //!< m_nodeDOF[idof] = NULL if idof is not a node, otherwise it is a pointer to the Point supporting the node
  int m_numDOFNode; //!< = m_numDOFNodeVertex + m_numDOFNodeEdge + m_numDOFNodeFace + m_numDOFNodeVolume;
  int m_numDOFNodeVertex;
  int m_numDOFNodeEdge;
  int m_numDOFNodeFace;
  int m_numDOFNodeVolume;
  int m_numDOFEdge;
  int m_numDOFFace;
  int m_numDOFVolume;
  //! num of Dof per edge, per face
  int m_numDOFPerEdge; //= m_numDOFEdge / m_numEdge
  int m_numDOFPerFace; //= m_numDOFFace / m_numFace

  bool m_edgesCanHaveDifferentNumberOfDof;
  std::vector<int> m_numDOFPerSingleEdge;//used only if the above is true, which is false by default. Important in the case of PrismsP1xP2
public:
  RefElement(const std::string& name,
              const RefShape& refShape,
              const BasisFunction& basisFunction,
              const ListOfQuadratureRule& listOfQuadratureRule,
              const DegreeOfFreedomType* typeDOF,
              const DegreeOfFreedomSupport* supportDOF,
              const int* idDOF,
              const Point* node, int numNodeVertex, int numNodeEdge,int numNodeFace,int numNodeVolume,
              const RefElement** boundaryRefElement,
              const RefElement** boundaryBoundaryRefElement,
              bool edgesCanHaveDifferentNumberOfDof = false);



  void initStaticMember();
  inline std::string name() const {
    return m_name;
  }
  inline int numDOF() const {
    return m_basisFunction.size();
  }
  inline const BasisFunction& basisFunction() const {
    return m_basisFunction;
  }
  inline int numVertex() const {
    return m_refShape.numVertex();
  }
  inline int numEdge() const {
    return m_refShape.numEdge();
  }
  inline int numFace() const {
    return m_refShape.numFace();
  }

  inline const int & numDOFNode() const {
    return m_numDOFNode;
  }
  inline int & numDOFNode() {
    return m_numDOFNode;
  }
  inline const int & numDOFNodeVertex() const {
    return m_numDOFNodeVertex;
  }
  inline int & numDOFNodeVertex() {
    return m_numDOFNodeVertex;
  }
  inline const int & numDOFNodeEdge() const {
    return m_numDOFNodeEdge;
  }
  inline int & numDOFNodeEdge() {
    return m_numDOFNodeEdge;
  }
  inline const int & numDOFNodeFace() const {
    return m_numDOFNodeFace;
  }
  inline int & numDOFNodeFace() {
    return m_numDOFNodeFace;
  }
  inline const int & numDOFNodeVolume() const {
    return m_numDOFNodeVolume;
  }
  inline int & numDOFNodeVolume() {
    return m_numDOFNodeVolume;
  }
  inline const int & numDOFEdge() const {
    return m_numDOFEdge;
  }
  inline int & numDOFEdge() {
    return m_numDOFEdge;
  }
  inline const int & numDOFFace() const {
    return m_numDOFFace;
  }
  inline int & numDOFFace() {
    return m_numDOFFace;
  }
  inline const int & numDOFVolume() const {
    return m_numDOFVolume;
  }
  inline int & numDOFVolume() {
    return m_numDOFNodeVolume;
  }

  inline const int & numDOFPerEdge() const {
    FEL_ASSERT( !m_edgesCanHaveDifferentNumberOfDof )
    return m_numDOFPerEdge;
  }
  inline const int & numDOFPerEdge( int i ) const {
    if (m_edgesCanHaveDifferentNumberOfDof)
      return m_numDOFPerSingleEdge[i];
    else
      return m_numDOFPerEdge;
  }
  inline const int & numDOFPerFace() const {
    return m_numDOFPerFace;
  }

  inline const int* idSupportOfDOF() const {
    return m_idSupportOfDOF;
  }
  inline const DegreeOfFreedomSupport* supportDOF() const {
    return m_supportDOF;
  }

  inline const Point* node() const {
    return m_node;
  }

  inline double nodeCoor(int inode,int icoor) const {
    return m_node[inode].coor(icoor);
  }
  inline const RefElement& boundaryRefElement(int i)const {
    FEL_ASSERT( (i< m_numFace && m_numCoor == 3) || (i< m_numEdge && m_numCoor == 2) ) ;
    return *(m_boundaryRefElement[i]);
  }
  void print(int verbose,std::ostream& c=std::cout) const;
  inline bool dofIsANode(int i) const {
    return(m_supportDOF[i] == DOF_NODE_VERTEX ||  m_supportDOF[i] == DOF_NODE_EDGE
            ||  m_supportDOF[i] == DOF_NODE_FACE ||  m_supportDOF[i] == DOF_NODE_VOLUME);
  }
  void check() const;

  bool edgesCanHaveDifferentNumberOfDof() const{
    return m_edgesCanHaveDifferentNumberOfDof;
  }
};

// extern const RefElement refElementNULL;
extern const RefElement refElementNode;
extern const RefElement refElementSegmentP1;
extern const RefElement refElementSegmentP1b;
extern const RefElement refElementSegmentP2;
extern const RefElement refElementSegmentP3H;
extern const RefElement refElementTriangleP1;
extern const RefElement refElementTriangleP1b;
extern const RefElement refElementTriangleP2;
extern const RefElement refElementQuadrangleQ1;
extern const RefElement refElementQuadrangleP1xP2;
extern const RefElement refElementQuadrangleQ1b;
extern const RefElement refElementQuadrangleQ2c;
extern const RefElement refElementQuadrangleQ2;
extern const RefElement refElementTetrahedronP1;
extern const RefElement refElementTetrahedronP1b;
extern const RefElement refElementTetrahedronP2;
extern const RefElement refElementHexahedronQ1;
extern const RefElement refElementHexahedronQ1b;
extern const RefElement refElementHexahedronQ2;
extern const RefElement refElementHexahedronQ2c;
extern const RefElement refElementPrismR1;
extern const RefElement refElementPrismP1xP2;
extern const RefElement refElementPrismR2;
extern const RefElement refElementTetrahedronRT0;

// extern const RefElement listRefElement[2];
}

#endif
