//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

#ifndef LISTCURRENTFINITEELEMENT_H
#define LISTCURRENTFINITEELEMENT_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/currentFiniteElement.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class ListCurrentFiniteElement
 * @author J. Foulon
 * @date 27/01/2011
 * @brief Class containing a list of finite elements in the problem.
 * These finite elements are stored with an STL vector.
 * @todo Replace with a template
 */
class ListCurrentFiniteElement {
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor
  ListCurrentFiniteElement() = default;

  /// Destructor
  ~ListCurrentFiniteElement()
  {
    clear();
  }

  ///@}
  ///@name Operators
  ///@{
  
  CurrentFiniteElement* operator[](int i) {
    return m_listCurrentFiniteElement[i];
  }

  ///@}
  ///@name Operations
  ///@{

  //Set functions
  //=============
  void add(CurrentFiniteElement* fe);

  void clear()
  {
    for (std::size_t i = 0; i < m_listCurrentFiniteElement.size(); i++)
      delete m_listCurrentFiniteElement[i];

    m_listCurrentFiniteElement.clear();
  }

  ///@}
  ///@name Access
  ///@{

  inline const std::vector<CurrentFiniteElement*>& listCurrentFiniteElement() const {
    return m_listCurrentFiniteElement;
  }

  inline std::vector<CurrentFiniteElement*>& listCurrentFiniteElement() {
    return m_listCurrentFiniteElement;
  }

  std::size_t size() {
    return m_listCurrentFiniteElement.size();
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  /// Vector STL which contains Felisce's currentFiniteElement.
  std::vector<CurrentFiniteElement*> m_listCurrentFiniteElement;

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif
