//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

// System includes

// External includes

// Project includes
#include "FiniteElement/listCurvilinearFiniteElementNeumann.hpp"

namespace felisce 
{
void ListCurvilinearFiniteElementNeumann::add(CurvilinearFiniteElement* fe) {
  m_listCurvilinearFiniteElementNeumann.push_back(fe);
}

/***********************************************************************************/
/***********************************************************************************/

//! Print function
void ListCurvilinearFiniteElementNeumann::print() {
  std::cout << "List of curvilinear Finite Element: " << std::endl;
  for ( unsigned int iFe = 0; iFe < m_listCurvilinearFiniteElementNeumann.size(); iFe++)
    //m_listCurvilinearFiniteElement[iFe]->print(); //! /todo create print function for current finite element
    std::cout << std::endl;
}
}
