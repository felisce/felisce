//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin & Miguel Fernandez
//

// System includes
#include <numeric>

// External includes

// Project includes
#include "FiniteElement/elementVector.hpp"
#include "Geometry/curvatures.hpp"

namespace felisce
{

ElementVector::ElementVector(const std::vector<const CurBaseFiniteElement*>& fe, const std::vector<std::size_t>& nbr) :
 m_vec( std::inner_product(fe.begin(),fe.end(),nbr.begin(), 0, std::plus<std::size_t>(), [](auto a, auto b){ return a->numDof()*b; }) )
{
  m_vec.clear();

  // TODO are the following used whenever there is one finite element??
  // If not would be better allocate the memory using an ad hoc method
  if (fe.size() == 1) {
    m_tmpVecDof.resize(fe[0]->numDof());
    m_tmpVecDof.resize(fe[0]->numCoor());
    m_tmpMatDof.resize(fe[0]->numCoor(),fe[0]->numCoor());
    m_tmpVecCoor.resize(fe[0]->numCoor());
  }

  //
  std::size_t sum = std::accumulate(nbr.begin(),nbr.end(), 0);
  m_numRow.resize(sum);
  m_firstRow.resize(sum);
  //
  std::size_t first = 0;
  std::size_t start = 0;
  std::size_t end   = 0;
  for (std::size_t i=0; i<nbr.size(); i++) {
    end   += nbr[i];
    for (std::size_t n=start; n<end; n++){
      m_numRow[ n ]=fe[i]->numDof();
      m_firstRow[ n ]=first;
      first += fe[i]->numDof();
    }
    start += nbr[i];
  }
}

// /***********************************************************************************/
// /***********************************************************************************/

// ElementVector::ElementVector(const CurBaseFiniteElement& fe1, const std::size_t nbr1) :
//   ElementVector(std::vector<const CurBaseFiniteElement*>{&fe1},
//                 std::vector<std::size_t>{nbr1})
// {

// }

// /***********************************************************************************/
// /***********************************************************************************/

// ElementVector::ElementVector(const CurBaseFiniteElement& fe1, const std::size_t nbr1,
//                              const CurBaseFiniteElement& fe2, const std::size_t nbr2) :
//   ElementVector(std::vector<const CurBaseFiniteElement*>{&fe1,&fe2},
//                 std::vector<std::size_t>{nbr1,nbr2})
// {

// }

// /***********************************************************************************/
// /***********************************************************************************/

// ElementVector::ElementVector(const CurBaseFiniteElement& fe1, const std::size_t nbr1,
//                              const CurBaseFiniteElement& fe2, const std::size_t nbr2,
//                              const CurBaseFiniteElement& fe3, const std::size_t nbr3) :
//   ElementVector(std::vector<const CurBaseFiniteElement*>{&fe1,&fe2,&fe3},
//                 std::vector<std::size_t>{nbr1,nbr2,nbr3})
// {

// }

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::source(const double coef, const CurBaseFiniteElement& fe,const ElementField& elfield,const int iblock,const int numComp)
{
  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = elfield.val(icomp,0) * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * tmp * fe.phi[ig](idof);
          }
        }
      }
      break;
    case QUAD_POINT_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = elfield.val(icomp,ig) * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * tmp * fe.phi[ig](idof);
          }
        }
      }
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for(int idof=0; idof<fe.numDof(); idof++) {
            tmp +=  fe.phi[ig](idof) * elfield.val(icomp,idof);
          }
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * tmp * fe.phi[ig](idof) * fe.weightMeas(ig);
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'source' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::source_lumped(const double coef, const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock,const int numComp)
{
  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        tmp = elfield.val(icomp,0) * fe.measure()/fe.numDof();
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) += coef * tmp;
        }
      }
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) += coef  * elfield.val(icomp,idof) * fe.measure()/fe.numDof();
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
    default:
      FEL_ERROR("Operator 'source lumped' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::sourceForComp(const double coef, const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock, const int iComp)
{
  double tmp;
  UBlasVectorRange vec = vecBlock(iblock);
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmp = elfield.val(iComp,0) * fe.weightMeas(ig);
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) += coef * tmp * fe.phi[ig](idof);
        }
      }
      break;
    case QUAD_POINT_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmp = elfield.val(iComp,ig) * fe.weightMeas(ig);
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) += coef * tmp * fe.phi[ig](idof);
        }
      }
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmp = 0.0;
        for(int idof=0; idof<fe.numDof(); idof++) {
          tmp +=  fe.phi[ig](idof) * elfield.val(iComp,idof);
        }
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) += coef * tmp  * fe.phi[ig](idof) * fe.weightMeas(ig);
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Operator 'source' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_phi_i_scalar_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock)
{
  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<fe.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = coef * elfield.val(0,0) * fe.normal[ig](icomp) * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += tmp * fe.phi[ig](idof);
          }
        }
      }
      break;

    case QUAD_POINT_FIELD:
      for(int icomp=0; icomp<fe.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = coef * elfield.val(0,ig) * fe.normal[ig](icomp) * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += tmp * fe.phi[ig](idof);
          }
        }
      }
      break;

    case DOF_FIELD:
      for(int icomp=0; icomp<fe.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for(int jdof=0; jdof<fe.numDof(); jdof++) {
            tmp += fe.phi[ig](jdof) * elfield.val(0,jdof);
          }
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * tmp * fe.phi[ig](idof) * fe.normal[ig](icomp)* fe.weightMeas(ig);
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      default:
      FEL_ERROR("Operator 'f_phi_i_scalar_n' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_phi_i_scalar_piola_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elf,const ElementField& elfd,const int iblock)
{
  (void)coef;
  (void)fe;
  (void)elf;
  (void)elfd;
  (void)iblock;
  //FEL_CHECK(fe.numCoor() == 3,"ElementVector::f_phi_i_scalar_JF_T_n: works only in 3D");

  // double det, val, p = elf.val(0,0);
  // std::vector<UBlasMatrix> piola;
  // UBlasMatrix F, mLU;
  // UBlasPermutationMatrix pm(fe.numCoor());
  // piola.resize(fe.numQuadraturePoint());

  // for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
  //    // evaluate piola at quadrature point
  //    piola[ig].resize(fe.numCoor(),fe.numCoor());
  //    F.clear();
  //    for(int l=0; l<fe.numDof(); ++l)
  // 	 for(int i=0; i<fe.numCoor(); ++i) {
  // 	   val = elfd.val(i,l); // d_i at node l
  // 	   for(int j=0; i<fe.numCoor(); ++i)
  // 	     F(i,j) += val*fe.dPhi[ig](j,l);
  // 	 }
  //    mLU = F;
  //    int ierr = lu_factorize(mLU, pm);
  //    FEL_CHECK(ierr==0,"LU factorization failed in f_phi_i_scalar_piola_n");
  //    det = 1.;
  //    for (std::size_t i=0; i < pm.size(); i++) {
  //     if (pm(i) != i)
  //       det *= -1.;
  //     det *= mLU(i,i);
  //    }
  //    F.assign(identity_matrix<double>(fe.numCoor()));
  //    lu_substitute(mLu,pm,F);
  //    piola[ig] = det * transpose(F);
  // }

  // for(int icomp=0; icomp<3; icomp++) {
  //   UBlasVectorRange vec = vecBlock(iblock+icomp);
  //   for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
  // 	// evaluate F at quadrature point
  // 	F.clear();
  // 	for(int l=0; l<fe.numDof(); ++l)
  // 	  for(int i=0; i<3; ++i) {
  // 	    val = elfield.val(i,l) // d_i at node l
  // 	      for(int j=0; i<3; ++i)
  // 		F(i,j) += val*fe.dPhi[ig](j,l);
  //       tmp = coef * p * fe.normal[ig](icomp) * fe.weightMeas(ig);
  //       for(int idof=0; idof<fe.numDof(); idof++) {
  //         vec(idof) +=  tmp * fe.phi[ig](idof);
  //       }
  //     }
  //   }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::penalty_contact_phi_i_scalar_n(const double coef,const std::vector<double>& normal, const CurvilinearFiniteElement& fe, const ElementField& disp, const ElementField& gap)
{
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementVector::penalty_contact_phi_i_scalar_n: wrong disp field type");
  FEL_CHECK(gap.type() == QUAD_POINT_FIELD,"ElementVector::penalty_contact_phi_i_scalar_n: wrong gap field type");
  double tmp;
  const double hK2inv = 1./(fe.diameter()*fe.diameter());
  for(int icomp=0; icomp<fe.numCoor(); icomp++) {
    UBlasVectorRange vec = vecBlock(icomp);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      tmp = 0.0;
      // disp at coor point
      for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
        for(int jdof=0; jdof<fe.numDof(); jdof++)
          tmp +=  fe.phi[ig](jdof)*disp.val(jcomp,jdof)*normal[jcomp];
      // gap
      tmp -= gap.val(0,ig);
      // tmp contains (u \cdot n - g) at quand node ig
      if (0 <= tmp ) {
        for(int idof=0; idof<fe.numDof(); idof++) {
          //std::cout << coef * (tmp-0.5)  * fe.phi[ig](idof) * normal[icomp]* fe.weightMeas(ig) << std::endl;
          vec(idof) += coef * hK2inv* tmp  * fe.phi[ig](idof) * normal[icomp]* fe.weightMeas(ig);
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::penalty_phi_i_scalar_n(const double coef, CurvilinearFiniteElement& fe, const ElementField& vel, std::size_t iblock)
{
  FEL_CHECK(vel.type() == DOF_FIELD,"ElementVector::penalty_phi_i_scalar_n: wrong vel field type");
  double tmp;
  const double penalty = 1./coef;
  for(int icoor=0; icoor<fe.numCoor(); icoor++) {
    UBlasVectorRange vec = vecBlock(icoor+iblock);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      tmp = 0.;
      for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
        for(int jdof=0; jdof<fe.numDof(); jdof++)
          tmp -=  fe.phi[ig](jdof)*vel.val(jcomp,jdof)*fe.normal[ig][jcomp]; // compute u_dot_n
      if ( tmp >= 0. ) {
        for(int idof=0; idof<fe.numDof(); idof++)
          vec(idof) += penalty*tmp*fe.phi[ig](idof)*fe.normal[ig][icoor]*fe.weightMeas(ig);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::nitsche_contact_phi_i_scalar_n(double gamma,const std::vector<double>& normal, const CurvilinearFiniteElement& fe, const ElementField& disp, const ElementField& gap, const ElementField& lambda)
{
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementVector::penalty_contact_phi_i_scalar_n: wrong disp field type");
  FEL_CHECK(gap.type() == QUAD_POINT_FIELD,"ElementVector::penalty_contact_phi_i_scalar_n: wrong gap field type");
  double tmp;
  const double penalty =  gamma / (fe.diameter()*fe.diameter());
  const double penalty_inv = fe.diameter()*fe.diameter()/gamma;
  for(int icomp=0; icomp<fe.numCoor(); icomp++) {
    UBlasVectorRange vec = vecBlock(icomp);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      tmp = 0.0;
      // disp.n +  1/ gamma * lambda at coor point
      for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
        for(int jdof=0; jdof<fe.numDof(); jdof++)
          tmp +=  fe.phi[ig](jdof)*(disp.val(jcomp,jdof)+ penalty_inv*lambda.val(jcomp,jdof)) *normal[jcomp];
      // gap
      tmp -= gap.val(0,ig);
      // tmp contains (u \cdot n - g + 1/gamma \lambda ) at quand node ig
      if (0 <= tmp ) {
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) +=  penalty * tmp  * fe.phi[ig](idof) * normal[icomp]* fe.weightMeas(ig);
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::nitsche_contact_phi_i_scalar_n(double gamma, const CurvilinearFiniteElement& fe, const ElementField& disp, const ElementField& gap, const ElementField& normal,  const ElementField& lambda)
{
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementVector::penalty_contact_phi_i_scalar_n: wrong disp field type");
  FEL_CHECK(gap.type() == QUAD_POINT_FIELD,"ElementVector::penalty_contact_phi_i_scalar_n: wrong gap field type");
  double tmp;
  const double penalty =  gamma / (fe.diameter()*fe.diameter());
  const double penalty_inv = fe.diameter()*fe.diameter()/gamma;
  for(int icomp=0; icomp<fe.numCoor(); icomp++) {
    UBlasVectorRange vec = vecBlock(icomp);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      tmp = 0.0;
      // disp.n +  1/ gamma * lambda at coor point
      for(int jcomp=0; jcomp<fe.numCoor(); jcomp++)
        for(int jdof=0; jdof<fe.numDof(); jdof++)
          tmp +=  fe.phi[ig](jdof)*(disp.val(jcomp,jdof)+ penalty_inv*lambda.val(jcomp,jdof)) *normal.val(jcomp,ig);
      // gap
      tmp -= gap.val(0,ig);
      // tmp contains (u \cdot n - g + 1/gamma \lambda ) at quand node ig
      if (0 <= tmp ) {
        for(int idof=0; idof<fe.numDof(); idof++) {
          vec(idof) +=  penalty * tmp  * fe.phi[ig](idof) * normal.val(icomp,ig)* fe.weightMeas(ig);
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_n_dot_phi_i(const double coef,const CurvilinearFiniteElement& fe1,const CurvilinearFiniteElement& fe2,const ElementField& elfield,const int iblock)
{
  double tmp;
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),"ElementVector::f_n_dot_phi_i: The quadrature rules must be the same for the two elements");
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<fe1.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
          tmp = coef * elfield.val(0,0) * fe1.normal[ig](icomp) * fe1.weightMeas(ig);
          for(int idof=0; idof<fe1.numDof(); idof++) {
            vec(idof) +=  tmp * fe1.phi[ig](idof);
          }
        }
      }
      break;
    case QUAD_POINT_FIELD:
      for(int icomp=0; icomp<fe1.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
          tmp = coef * elfield.val(0,ig) * fe1.normal[ig](icomp) * fe1.weightMeas(ig);
          for(int idof=0; idof<fe1.numDof(); idof++) {
            vec(idof) +=  tmp * fe1.phi[ig](idof);
          }
        }
      }
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<fe1.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for(int jdof=0; jdof<fe2.numDof(); jdof++) {
            tmp +=  fe2.phi[ig](jdof) * elfield.val(0,jdof);
          }
          for(int idof=0; idof<fe1.numDof(); idof++) {
            vec(idof) += coef * tmp  * fe1.phi[ig](idof) * fe1.normal[ig](icomp)* fe1.weightMeas(ig);
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'f_n_dot_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_dot_n_phi_i(const double coef,const CurvilinearFiniteElement& fe1,const CurvilinearFiniteElement& fe2, const ElementField& elfield,const int iblock)
{
  double tmp,f_dot_n;
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),"ElementVector::f_dot_n_phi_i: The quadrature rules must be the same for the two elements");
  UBlasVectorRange vec = vecBlock(iblock);
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<fe1.numCoor(); icomp++) {
        //  UBlasVectorRange vec = vecBlock(iblock + icomp)
        for(int ig=0; ig<fe2.numQuadraturePoint(); ig++) {
          tmp = coef * elfield.val(0,0) * fe1.normal[ig](icomp) * fe2.weightMeas(ig);
          for(int idof=0; idof<fe2.numDof(); idof++) {
            vec(idof) +=  tmp * fe2.phi[ig](idof);
          }
        }
      }
      break;
    case QUAD_POINT_FIELD:
      for(int icomp=0; icomp<fe1.numCoor(); icomp++) {
        // UBlasVectorRange vec = vecBlock(iblock + icomp)
        for(int ig=0; ig<fe2.numQuadraturePoint(); ig++) {
          tmp = coef * elfield.val(0,ig) * fe1.normal[ig](icomp) * fe2.weightMeas(ig);
          for(int idof=0; idof<fe2.numDof(); idof++) {
            vec(idof) +=  tmp * fe2.phi[ig](idof);
          }
        }
      }
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe2.numQuadraturePoint(); ig++) {
        f_dot_n = 0;
        for(int icomp=0; icomp<fe1.numCoor(); icomp++) {
          tmp = 0.0;
          for(int jdof=0; jdof<fe1.numDof(); jdof++) {
            tmp +=  fe1.phi[ig](jdof) * elfield.val(icomp,jdof);
          }
          f_dot_n += tmp * fe1.normal[ig](icomp);
        }
        for(int idof=0; idof<fe2.numDof(); idof++) {
          vec(idof) += coef * f_dot_n * fe2.phi[ig](idof) * fe2.weightMeas(ig);
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'f_dot_n_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::g_dot_n_f_phi_i(const double coef, const CurrentFiniteElement& fe, const ElementField& elt_f, const ElementField& elt_g, const ElementField& elt_n, felInt iblock, felInt numComp)
{
  FEL_ASSERT(elt_f.type() == QUAD_POINT_FIELD);
  FEL_ASSERT(elt_g.type() == DOF_FIELD);
  FEL_ASSERT(elt_n.type() == CONSTANT_FIELD);

  double tmp;
  double g_dot_n = 0.0;

  for(int icomp=0; icomp<numComp; icomp++) {
    UBlasVectorRange vec = vecBlock(iblock + icomp);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      g_dot_n = 0;
      for(int jcomp=0; jcomp<fe.numCoor(); jcomp++) {
        tmp = 0.0;
        for(int jdof=0; jdof<fe.numDof(); jdof++) {
          tmp += fe.phi[ig](jdof) * elt_g.val(jcomp, jdof);
        }
        g_dot_n += tmp * elt_n.val(jcomp, 0);
      }

      if(g_dot_n < 0)
        for(int idof=0; idof<fe.numDof(); idof++)
          vec(idof) += coef * g_dot_n * elt_f.val(icomp, ig) * fe.phi[ig](idof) * fe.weightMeas(ig);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_vec2_dot_vec1_dot_phi_i(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp)
{
  // Mot implemented for other type yet
  FEL_ASSERT(vec1.type() == CONSTANT_FIELD);

  double tmpval1, tmpval2;

  switch(vec2.type()) {
    case CONSTANT_FIELD:
      // nothing to do
      break;

    case QUAD_POINT_FIELD:
      // cannot compute the derivative of vec2 at integration points
      FEL_ERROR("grad_vec2_dot_vec1_dot_phi_i does not work with vec2 defined at integration points");
      break;

    case DOF_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            tmpval2 = 0.0;
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              tmpval1 = 0.0;
              for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
                tmpval1 += vec2.val(iComp, jdof) * fe.dPhi[ig](icoor, jdof);
              }
              tmpval2 += tmpval1 * vec1.val(icoor, 0);
            }
            vec(idof) += tmpval2 * coef * fe.phi[ig](idof) * fe.weightMeas(ig);
          }
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::eps_vec2_dot_vec1_dot_phi_i(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp)
{
  // Not implemented for other type yet
  FEL_ASSERT(vec1.type() == CONSTANT_FIELD);

  double tmpval1, tmpval2;

  switch(vec2.type()) {
    case CONSTANT_FIELD:
      // nothing to do
      break;

    case QUAD_POINT_FIELD:
      // cannot compute the derivative of vec2 at integration points
      FEL_ERROR("eps_vec2_dot_vec1_dot_phi_i does not work with vec2 defined at integration points");
      break;

    case DOF_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            tmpval2 = 0.0;
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              tmpval1 = 0.0;
              for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
                tmpval1 += vec2.val(iComp, jdof) * fe.dPhi[ig](icoor, jdof);
                tmpval1 += vec2.val(icoor, jdof) * fe.dPhi[ig](iComp, jdof);
              }
              tmpval2 += tmpval1 * vec1.val(icoor, 0);
            }
            vec(idof) += tmpval2 * 0.5 * coef * fe.phi[ig](idof) * fe.weightMeas(ig);
          }
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_phi_i_dot_vec1_dot_vec2(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp)
{
  // Not implemented for other type yet
  FEL_ASSERT(vec1.type() == CONSTANT_FIELD);

  double vecval;

  switch(vec2.type()) {
    case CONSTANT_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += coef * fe.weightMeas(ig) * vec2.val(iComp, 0) * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
            }
          }
        }
      }
      break;

    case QUAD_POINT_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += coef * fe.weightMeas(ig) * vec2.val(iComp, ig) * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
            }
          }
        }
      }
      break;

    case DOF_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            vecval = 0.0;
            for(felInt kdof=0; kdof<fe.numDof(); ++kdof)
              vecval += vec2.val(iComp, kdof) * fe.phi[ig](kdof);

            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += coef * fe.weightMeas(ig) * vecval * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
            }
          }
        }
      }

      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::eps_phi_i_dot_vec1_dot_vec2(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp)
{
  // not implemented for other type yet
  FEL_ASSERT(vec1.type() == CONSTANT_FIELD);

  double vecvalComp, vecvalCoor;

  switch(vec2.type()) {
    case CONSTANT_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += 0.5 * coef * fe.weightMeas(ig) * vec2.val(iComp, 0) * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
              vec(idof) += 0.5 * coef * fe.weightMeas(ig) * vec1.val(iComp, 0) * fe.dPhi[ig](icoor, idof) * vec2.val(icoor, 0);
            }
          }
        }
      }
      break;

    case QUAD_POINT_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += 0.5 * coef * fe.weightMeas(ig) * vec2.val(iComp, ig) * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
              vec(idof) += 0.5 * coef * fe.weightMeas(ig) * vec1.val(iComp, 0) * fe.dPhi[ig](icoor, idof) * vec2.val(icoor, ig);
            }
          }
        }
      }
      break;

    case DOF_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            vecvalComp = 0.0;
            for(felInt kdof=0; kdof<fe.numDof(); ++kdof)
              vecvalComp += vec2.val(iComp, kdof) * fe.phi[ig](kdof);

            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vecvalCoor = 0.0;
              for(felInt kdof=0; kdof<fe.numDof(); ++kdof)
                vecvalCoor += vec2.val(icoor, kdof) * fe.phi[ig](kdof);

              vec(idof) += 0.5 * coef * fe.weightMeas(ig) * vecvalComp * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
              vec(idof) += 0.5 * coef * fe.weightMeas(ig) * vec1.val(iComp, 0) * fe.dPhi[ig](icoor, idof) * vecvalCoor;
            }
          }
        }
      }

      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::eps_vec2_vec1_dot_eps_phi_i_vec1(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp)
{
  // not implemented for other type yet
  FEL_ASSERT(vec1.type() == CONSTANT_FIELD);

  double tmpval1, tmpval2;

  switch(vec2.type()) {
    case CONSTANT_FIELD:
      // nothing to do
      break;

    case QUAD_POINT_FIELD:
      // cannot compute the derivative of vec2 at integration points
      FEL_ERROR("eps_vec2_dot_vec1_dot_phi_i does not work with vec2 defined at integration points");
      break;

    case DOF_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            tmpval2 = 0.0;
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              tmpval1 = 0.0;
              for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
                tmpval1 += vec2.val(iComp, jdof) * fe.dPhi[ig](icoor, jdof);
                tmpval1 += vec2.val(icoor, jdof) * fe.dPhi[ig](iComp, jdof);
              }
              tmpval2 += tmpval1 * vec1.val(icoor, 0);
            }

            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += 0.25 * coef * fe.weightMeas(ig) * tmpval2 * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
              vec(idof) += 0.25 * coef * fe.weightMeas(ig) * vec1.val(iComp, 0) * fe.dPhi[ig](icoor, idof) * tmpval2;
            }
          }
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_vec2_vec1_dot_grad_phi_i_vec1(const double coef, const ElementField& vec1, const ElementField& vec2, const CurrentFiniteElement& fe, const int iblock, const int numComp)
{
  // not implemented for other type yet
  FEL_ASSERT(vec1.type() == CONSTANT_FIELD);

  double tmpval1, tmpval2;

  switch(vec2.type()) {
    case CONSTANT_FIELD:
      // nothing to do
      break;

    case QUAD_POINT_FIELD:
      // cannot compute the derivative of vec2 at integration points
      FEL_ERROR("grad_vec2_dot_vec1_dot_phi_i does not work with vec2 defined at integration points");
      break;

    case DOF_FIELD:
      for(felInt iComp=0; iComp<numComp; ++iComp) {
        UBlasVectorRange vec = vecBlock(iblock + iComp);
        for(felInt idof=0; idof<fe.numDof(); ++idof) {
          for(felInt ig=0; ig<fe.numQuadraturePoint(); ++ig) {
            tmpval2 = 0.0;
            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              tmpval1 = 0.0;
              for(felInt jdof=0; jdof<fe.numDof(); ++jdof) {
                tmpval1 += vec2.val(iComp, jdof) * fe.dPhi[ig](icoor, jdof);
              }
              tmpval2 += tmpval1 * vec1.val(icoor, 0);
            }

            for(felInt icoor=0; icoor<fe.numRefCoor(); ++icoor) {
              vec(idof) += coef * fe.weightMeas(ig) * tmpval2 * fe.dPhi[ig](icoor, idof) * vec1.val(icoor, 0);
            }
          }
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_phi_i_dot_n_f(const double coef,const CurrentFiniteElementWithBd& fewbd,
                                        const ElementField& elfield,
                                        const int iblockBd, const int numblockBd,
                                        const int iblock, const int numComp)
{
  FEL_ASSERT(iblockBd>=0 && iblockBd<fewbd.numBdEle() && (numblockBd-iblockBd) <= fewbd.numBdEle() );
  FEL_ASSERT(static_cast<int>(m_tmpVecDof.size()) == fewbd.numDof() );

  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
          FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
          for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
            int ig = ilocg+fewbd.indexQuadPoint(ibd+1);
            // face ibd starts at ibd+1 (0 for internal quad points)
            //! Vector = [n \cdot \grad \phi]:
            m_tmpVecDof = prod(trans(fewbd.bdEle(ibd).normal[ilocg]), fewbd.dPhi[ig]);
            tmp = coef * elfield.val(icomp,0) * fewbd.bdEle(ibd).weightMeas(ilocg);
            for(int idof=0; idof<fewbd.numDof(); idof++) {
              // dphi_1/dn(idof) * f:
              vec(idof) +=  tmp * m_tmpVecDof(idof);
            }
          }
        }
      }
      break;
    case QUAD_POINT_FIELD:
      FEL_ASSERT( elfield.val.size2() == (std::size_t) fewbd.numQuadraturePointInternAndBd() ); //build the elfield from currentFEwBd
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
          FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
          for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
            int ig = ilocg+fewbd.indexQuadPoint(ibd+1);
            // face ibd starts at ibd+1 (0 for internal quad points)
            //! Vector = [n \cdot \grad \phi]:
            m_tmpVecDof = prod(trans(fewbd.bdEle(ibd).normal[ilocg]), fewbd.dPhi[ig]);
            tmp = coef * elfield.val(icomp,ig) * fewbd.bdEle(ibd).weightMeas(ilocg);
            for(int idof=0; idof<fewbd.numDof(); idof++) {
              // dphi_1/dn(idof) * f:
              vec(idof) +=  tmp * m_tmpVecDof(idof);
            }
          }
        }
      }
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
          FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
          for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
            int ig = ilocg+fewbd.indexQuadPoint(ibd+1);
            // face ibd starts at ibd+1 (0 for internal quad points)
            //! Vector = [n \cdot \grad \phi]:
            m_tmpVecDof = prod(trans(fewbd.bdEle(ibd).normal[ilocg]), fewbd.dPhi[ig]);

            tmp = 0.0;
            for(int idof=0; idof<fewbd.numDof(); idof++) {
              tmp +=  fewbd.phi[ig](idof) * elfield.val(icomp,idof) * fewbd.bdEle(ibd).weightMeas(ilocg);
            }
            for(int idof=0; idof<fewbd.numDof(); idof++) {
              // dphi_1/dn(idof) * f:
              vec(idof) +=  coef * tmp * m_tmpVecDof(idof);
            }
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Operator 'grad_phi_i_dot_n_f' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_f_dot_n_phi_i(const double coef,const CurrentFiniteElementWithBd& fewbd,
                                        const ElementField& elfield,
                                        const int iblockBd, const int numblockBd,
                                        const int iblock, const int numComp)
{
  FEL_ASSERT(iblockBd>=0 && iblockBd<fewbd.numBdEle() && (numblockBd-iblockBd) <= fewbd.numBdEle() );
  FEL_ASSERT( static_cast<int>(m_tmpVecDof.size()) == fewbd.numDof() );

  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      // add nothing: grad cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("Operator 'grad_f_dot_n_phi_i' is not well defined for quad_point_field: how to compute grad f ??");
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
          FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
          for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
            int ig = ilocg+fewbd.indexQuadPoint(ibd+1);
            // face ibd starts at ibd+1 (0 for internal quad points)
            //! Vector = [n \cdot \grad \phi]:
            m_tmpVecDof = prod(trans(fewbd.bdEle(ibd).normal[ilocg]), fewbd.dPhi[ig]);
            tmp = 0.0;
            for(int jdof=0; jdof<fewbd.numDof(); jdof++) {
              //! tmp = [n \cdot \grad f] = \sum f_jdof (grad phi_jdof \cdot n)
              tmp += m_tmpVecDof(jdof) * elfield.val(icomp,jdof);
            }
            tmp *= coef * fewbd.bdEle(ibd).weightMeas(ilocg);
            for(int idof=0; idof<fewbd.bdEle(ibd).numDof(); idof++) {
              //! df/dn *phi(idof):
              vec(idof) +=  tmp * fewbd.bdEle(ibd).phi[ilocg](idof);
            }
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'grad_f_dot_n_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

//----------------------------------------------------------------------
// Operators \f$ \int_{\Sigma} (\grad f^T \dot n) \phi_i \f$,// shoudl be teseted !!!! saverio 8/10/2012
//! where $\Sigma$ is a boundary element. (For Nitsche's treatment...)
void ElementVector::grad_f_Transp_dot_n_phi_i(const double coef,const CurrentFiniteElementWithBd& fewbd,
                                              const ElementField& elfield,
                                              const int iblockBd, const int numblockBd,
                                              const int iblock, const int numComp)
{
  FEL_ASSERT(iblockBd>=0 && iblockBd<fewbd.numBdEle() && (numblockBd-iblockBd) <= fewbd.numBdEle() );
  FEL_ASSERT( static_cast<int>(m_tmpVecDof.size()) == fewbd.numDof() );
  FEL_ASSERT( (int)m_tmpVecComp.size() == fewbd.numCoor());
  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      // add nothing: grad cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("Operator 'grad_f_Transp_dot_n_phi_i' is not well defined for quad_point_field: how to compute grad f ??");
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
          FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
          for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
            int ig = ilocg+fewbd.indexQuadPoint(ibd+1);
            // face ibd starts at ibd+1 (0 for internal quad points)
            //! Matrix = grad \phi cdot f
            m_tmpMatDof = prod(fewbd.dPhi[ig],trans(elfield.val));
            m_tmpVecComp = prod(m_tmpMatDof, fewbd.bdEle(ibd).normal[ilocg]);
            tmp = 0.0;
            //! tmp = [n \cdot \grad f] = \sum f_jdof (grad phi_jdof \cdot n)
            tmp = m_tmpVecComp(icomp);
            tmp *= coef * fewbd.bdEle(ibd).weightMeas(ilocg);
            for(int idof=0; idof<fewbd.bdEle(ibd).numDof(); idof++) {
              //! df/dn *phi(idof):
              vec(idof) +=  tmp * fewbd.bdEle(ibd).phi[ilocg](idof);
            }
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'grad_f_Transp_dot_n_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_dot_n_f_phi_i(const double coef,const CurvilinearFiniteElement& fe,
                                    const ElementField& elfield1,const ElementField& elfield2,
                                    const int iblock, const int numComp)
{
  FEL_ASSERT(elfield1.type() != elfield2.type());
  double tmp1, tmp2, f_dot_n;
  switch(elfield1.type()) {
    case CONSTANT_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock( iblock+icomp );
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          f_dot_n = 0.0;
          for(int icoor=0; icoor<fe.numCoor(); icoor++) {
            f_dot_n +=  elfield1.val(icoor,0)*fe.normal[ig](icoor);
          }
          tmp2 = elfield2.val(icomp,0) * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * f_dot_n * tmp2 * fe.phi[ig](idof);
          }
        }
      }
      break;
    case QUAD_POINT_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          f_dot_n = 0.0;
          for(int icoor=0; icoor<fe.numCoor(); icoor++) {
            f_dot_n +=  elfield1.val(icoor,ig)*fe.normal[ig](icoor);
          }
          tmp2 = elfield2.val(icomp,ig) * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * f_dot_n * tmp2 * fe.phi[ig](idof);
          }
        }
      }
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp2 = 0.0;
          f_dot_n = 0.0;
          for(int icoor=0; icoor<fe.numCoor(); icoor++) {
            tmp1= 0.0;
            for(int jdof=0; jdof<fe.numDof(); jdof++) {
              tmp1 +=  fe.phi[ig](jdof) * elfield1.val(icoor,jdof);
            }
            //! sum f_icoor * n_icoor
            f_dot_n += tmp1 * fe.normal[ig](icoor);
          }
          for(int jdof=0; jdof<fe.numDof(); jdof++) {
            tmp2 +=  fe.phi[ig](jdof) * elfield2.val(icomp,jdof);
          }
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * f_dot_n * tmp2 * fe.phi[ig](idof) * fe.weightMeas(ig);
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //FEL_ERROR("Operator 'f_dot_n_f_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_f_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield2,const int iblock)
{
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),
            "ElementVector::grad_f_phi_i: The quadrature rules must be the same for the two elements");
  double tmp;
  switch(elfield2.type()) {
    case CONSTANT_FIELD:
      // add nothing: grad cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::grad_f_phi_i: Operator 'grad_f_phi_i' is not defined for quad_point_field: how to compute grad f ??");
      break;
    case DOF_FIELD:
      for (int icomp=0; icomp<fe1.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for (int jdof=0; jdof<fe2.numDof(); jdof++) {
            tmp += fe2.dPhi[ig](icomp,jdof)*elfield2.val(0,jdof);
          }
          tmp *= coef * fe1.weightMeas(ig);
          for(int idof=0; idof<fe1.numDof(); idof++) {
            vec(idof) += tmp * fe1.phi[ig](idof);
          }
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::curl_f_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield2,const int iblock)
{
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(), "ElementVector::curl_f_phi_i: The quadrature rules must be the same for the two elements");
  FEL_CHECK(fe1.numCoor() == 3, "ElementVector::curl_f_phi_i: curl not yet implemented in 2D");
  double tmp0,tmp1,tmp2;
  switch(elfield2.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::curl_f_phi_i: Operator 'curl_f_phi_i' is not defined for quad_point_field: how to compute curl f ??");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;
        for (int jdof=0; jdof<fe2.numDof(); jdof++) {
          tmp0 += fe2.dPhi[ig](1,jdof)*elfield2.val(2,jdof) - fe2.dPhi[ig](2,jdof)*elfield2.val(1,jdof);
          tmp1 += fe2.dPhi[ig](2,jdof)*elfield2.val(0,jdof) - fe2.dPhi[ig](0,jdof)*elfield2.val(2,jdof);
          tmp2 += fe2.dPhi[ig](0,jdof)*elfield2.val(1,jdof) - fe2.dPhi[ig](1,jdof)*elfield2.val(0,jdof);
        }
        tmp0 *= coef * fe1.weightMeas(ig);
        tmp1 *= coef * fe1.weightMeas(ig);
        tmp2 *= coef * fe1.weightMeas(ig);
        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec0(idof) += tmp0 * fe1.phi[ig](idof);
          vec1(idof) += tmp1 * fe1.phi[ig](idof);
          vec2(idof) += tmp2 * fe1.phi[ig](idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::WGX_dot_phi_i( const CurrentFiniteElement& feWGX, const CurrentFiniteElement& feWSS, const ElementField& WSSField, const int iblock)
{
  double tmp0,tmp1,tmp2;
  switch(WSSField.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector : not defined for quad_point_field");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<feWGX.numQuadraturePoint(); ig++) {

        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;

        for (int jdof=0; jdof<feWSS.numDof(); jdof++) {
          //std::cout<<"test WGX "<<WSSField.val(0,jdof)<<" "<<WSSField.val(1,jdof)<<" "<<WSSField.val(2,jdof)<<"\r";
          tmp0 += feWSS.dPhi[ig](0,jdof)*WSSField.val(0,jdof)* feWGX.weightMeas(ig);
          tmp1 += feWSS.dPhi[ig](1,jdof)*WSSField.val(0,jdof)* feWGX.weightMeas(ig);
          tmp2 += feWSS.dPhi[ig](2,jdof)*WSSField.val(0,jdof)* feWGX.weightMeas(ig);
        }

        for(int idof=0; idof<feWGX.numDof(); idof++) {
          vec0(idof) += tmp0 * feWGX.phi[ig](idof);
          vec1(idof) += tmp1 * feWGX.phi[ig](idof);
          vec2(idof) += tmp2 * feWGX.phi[ig](idof);

        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::WGY_dot_phi_i( const CurrentFiniteElement& feWGY, const CurrentFiniteElement& feWSS, const ElementField& WSSField, const int iblock)
{
  double tmp0,tmp1,tmp2;
  switch(WSSField.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector : not defined for quad_point_field");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<feWGY.numQuadraturePoint(); ig++) {

        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;

        for (int jdof=0; jdof<feWSS.numDof(); jdof++) {
          //std::cout<<"test WGY "<<normalField.val(0,jdof)<<" "<<normalField.val(1,jdof)<<" "<<normalField.val(2,jdof)<<" "<<feWGY.weightMeas(ig)<<" "<<elfield_StressY.val(0,jdof)<<"\r";
          tmp0 += feWSS.dPhi[ig](0,jdof)*WSSField.val(1,jdof)*feWGY.weightMeas(ig);
          tmp1 += feWSS.dPhi[ig](1,jdof)*WSSField.val(1,jdof)*feWGY.weightMeas(ig);
          tmp2 += feWSS.dPhi[ig](2,jdof)*WSSField.val(1,jdof)*feWGY.weightMeas(ig);

        }

        for(int idof=0; idof<feWGY.numDof(); idof++) {
          vec0(idof) += tmp0 * feWGY.phi[ig](idof);
          vec1(idof) += tmp1 * feWGY.phi[ig](idof);
          vec2(idof) += tmp2 * feWGY.phi[ig](idof);

        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::WGZ_dot_phi_i( const CurrentFiniteElement& feWGZ, const CurrentFiniteElement& feWSS, const ElementField& WSSField, const int iblock)
{
  double tmp0,tmp1,tmp2;
  switch(WSSField.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector : not defined for quad_point_field");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<feWGZ.numQuadraturePoint(); ig++) {

        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;

        for (int jdof=0; jdof<feWSS.numDof(); jdof++) {
          //std::cout<<"test WGZ "<<normalField.val(0,jdof)<<" "<<normalField.val(1,jdof)<<" "<<normalField.val(2,jdof)<<" "<<feWGZ.weightMeas(ig)<<" "<<elfield_StressZ.val(0,jdof)<<"\r";
          tmp0 += feWSS.dPhi[ig](0,jdof)*WSSField.val(2,jdof)*feWGZ.weightMeas(ig);
          tmp1 += feWSS.dPhi[ig](1,jdof)*WSSField.val(2,jdof)*feWGZ.weightMeas(ig);
          tmp2 += feWSS.dPhi[ig](2,jdof)*WSSField.val(2,jdof)*feWGZ.weightMeas(ig);
        }

        for(int idof=0; idof<feWGZ.numDof(); idof++) {
          vec0(idof) += tmp0 * feWGZ.phi[ig](idof);
          vec1(idof) += tmp1 * feWGZ.phi[ig](idof);
          vec2(idof) += tmp2 * feWGZ.phi[ig](idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stressX_dot_phi_i(const double viscosity, const CurrentFiniteElement& fePres,const CurrentFiniteElement& feStress,const CurrentFiniteElement& feVel,const ElementField& elfield_pressure,const ElementField& elfield_velocity,const int iblock)
{
  FEL_CHECK(feStress.numQuadraturePoint() == feVel.numQuadraturePoint(), "ElementVector::stressX_dot_phi_i: The quadrature rules must be the same for all the elements");
  FEL_CHECK(feStress.numCoor() == 3, "ElementVector::stressX_dot_phi_i: not yet implemented in 2D");
  double tmp0,tmp1,tmp2;
  switch(elfield_velocity.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::curl_f_phi_i: Operator 'stressX_dot_phi_i' is not defined for quad_point_field");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<feStress.numQuadraturePoint(); ig++) {
        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;
        for (int jdof=0; jdof<feVel.numDof(); jdof++) {
          tmp0 += -elfield_pressure.val(0,jdof) * fePres.phi[ig](jdof) + 2*viscosity*feVel.dPhi[ig](0,jdof)*elfield_velocity.val(0,jdof);
          tmp1 += feVel.dPhi[ig](1,jdof)*elfield_velocity.val(0,jdof) + feVel.dPhi[ig](0,jdof)*elfield_velocity.val(1,jdof);
          tmp2 += feVel.dPhi[ig](2,jdof)*elfield_velocity.val(0,jdof) + feVel.dPhi[ig](0,jdof)*elfield_velocity.val(2,jdof);
        }
        tmp0 *= feStress.weightMeas(ig);
        tmp1 *= viscosity * feStress.weightMeas(ig);
        tmp2 *= viscosity * feStress.weightMeas(ig);
        for(int idof=0; idof<feStress.numDof(); idof++) {
          vec0(idof) += tmp0 * feStress.phi[ig](idof);
          vec1(idof) += tmp1 * feStress.phi[ig](idof);
          vec2(idof) += tmp2 * feStress.phi[ig](idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stressY_dot_phi_i(const double viscosity, const CurrentFiniteElement& fePres, const CurrentFiniteElement& feStress,const CurrentFiniteElement& feVel,const ElementField& elfield_pressure,const ElementField& elfield_velocity,const int iblock)
{
  FEL_CHECK(feStress.numQuadraturePoint() == feVel.numQuadraturePoint() && fePres.numQuadraturePoint() == feVel.numQuadraturePoint(), "ElementVector::stressY_dot_phi_i: The quadrature rules must be the same for all the elements");
  FEL_CHECK(feStress.numCoor() == 3, "ElementVector::stressY_dot_phi_i: not yet implemented in 2D");
  double tmp0,tmp1,tmp2;
  switch(elfield_velocity.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::stressY_dot_phi_i: Operator 'stressY_dot_phi_i' is not defined for quad_point_field: how to compute curl f ??");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<feStress.numQuadraturePoint(); ig++) {
        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;
        for (int jdof=0; jdof<feVel.numDof(); jdof++) {
          tmp0 += feVel.dPhi[ig](1,jdof)*elfield_velocity.val(0,jdof) + feVel.dPhi[ig](0,jdof)*elfield_velocity.val(1,jdof);
          tmp1 += -elfield_pressure.val(0,jdof) * fePres.phi[ig](jdof) + 2 * viscosity * feVel.dPhi[ig](1,jdof)*elfield_velocity.val(1,jdof);
          tmp2 += feVel.dPhi[ig](2,jdof)*elfield_velocity.val(1,jdof) + feVel.dPhi[ig](1,jdof)*elfield_velocity.val(2,jdof);
        }
        tmp0 *= viscosity * feStress.weightMeas(ig);
        tmp1 *=  feStress.weightMeas(ig);
        tmp2 *= viscosity * feStress.weightMeas(ig);
        for(int idof=0; idof<feStress.numDof(); idof++) {
          vec0(idof) += tmp0 * feStress.phi[ig](idof);
          vec1(idof) += tmp1 * feStress.phi[ig](idof);
          vec2(idof) += tmp2 * feStress.phi[ig](idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stressZ_dot_phi_i(const double viscosity, const CurrentFiniteElement& fePres, const CurrentFiniteElement& feStress,const CurrentFiniteElement& feVel,const ElementField& elfield_pressure,const ElementField& elfield_velocity,const int iblock)
{
  FEL_CHECK(feStress.numQuadraturePoint() == feVel.numQuadraturePoint(), "ElementVector::stressZ_dot_phi_i: The quadrature rules must be the same for all the elements");
  FEL_CHECK(feStress.numCoor() == 3, "ElementVector::stressZ_dot_phi_i: not yet implemented in 2D");
  double tmp0,tmp1,tmp2;
  switch(elfield_velocity.type()) {
    case CONSTANT_FIELD:
      // add nothing: curl cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::stressZ_dot_phi_i: Operator 'stressZ_dot_phi_i' is not defined for quad_point_field: how to compute curl f ??");
      break;
    case DOF_FIELD:
      UBlasVectorRange vec0 = vecBlock(iblock+0);
      UBlasVectorRange vec1 = vecBlock(iblock+1);
      UBlasVectorRange vec2 = vecBlock(iblock+2);
      for(int ig=0; ig<feStress.numQuadraturePoint(); ig++) {
        tmp0 = 0.0;
        tmp1 = 0.0;
        tmp2 = 0.0;
        for (int jdof=0; jdof<feVel.numDof(); jdof++) {
          tmp0 += feVel.dPhi[ig](2,jdof)*elfield_velocity.val(0,jdof) + feVel.dPhi[ig](0,jdof)*elfield_velocity.val(2,jdof);
          tmp1 += feVel.dPhi[ig](2,jdof)*elfield_velocity.val(1,jdof) + feVel.dPhi[ig](1,jdof)*elfield_velocity.val(2,jdof);
          tmp2 += -elfield_pressure.val(0,jdof) * fePres.phi[ig](jdof)+ 2 * viscosity * feVel.dPhi[ig](2,jdof)*elfield_velocity.val(2,jdof);
        }
        tmp0 *= viscosity * feStress.weightMeas(ig);
        tmp1 *= viscosity * feStress.weightMeas(ig);
        tmp2 *=  feStress.weightMeas(ig);
        for(int idof=0; idof<feStress.numDof(); idof++) {
          vec0(idof) += tmp0 * feStress.phi[ig](idof);
          vec1(idof) += tmp1 * feStress.phi[ig](idof);
          vec2(idof) += tmp2 * feStress.phi[ig](idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_f_grad_phi_i(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield,const int iblock)
{
  double tmp;
  UBlasVectorRange vec = vecBlock(iblock);
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      // add nothing: grad cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::grad_f_grad_phi_i: Operator 'grad_f_grad_phi_i' is not defined for quad_point_field: how to compute grad f ??");
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        for (int icomp=0; icomp<fe.numCoor(); icomp++) {
          tmp = 0.0;
          for (int jdof=0; jdof<fe.numDof(); jdof++)
            tmp += fe.dPhi[ig](icomp,jdof)*elfield.val(0,jdof);
          tmp *= coef * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++)
            vec(idof) += tmp * fe.dPhi[ig](icomp,idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_f_grad_phi_i(const double coef,const CurvilinearFiniteElement& fe, const ElementField& elfield, const std::size_t iblock,const std::size_t num)
{
  FEL_ASSERT(elfield.type() == DOF_FIELD);

  UBlasMatrix tmpBlock(fe.numDof(),fe.numDof(),0);
  UBlasMatrix tmpAdPhi(fe.contravariantMetric[0].size1(), fe.dPhiRef(0).size2());
  for(int ig=0, numQuadPoint = fe.numQuadraturePoint(); ig < numQuadPoint; ig++) {
    tmpAdPhi  = prod(fe.contravariantMetric[ig],fe.dPhiRef(ig));
    tmpBlock += coef*fe.weightMeas(ig)*prod(trans(fe.dPhiRef(ig)),tmpAdPhi);
  }

  UBlasVector ff = elfield.valAsVec();

  for(std::size_t icoor=0; icoor < num; icoor++) {
    UBlasVectorRange vec = vecBlock(iblock+icoor);
    UBlasVectorRange ficoor = UBlasVectorRange(ff,UBlasRange( m_firstRow[icoor], m_firstRow[icoor]+m_numRow[icoor]));
    vec += prod(tmpBlock,ficoor);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_grad_phi_i(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield,const int iblock)
{
  double tmp;
  UBlasVectorRange vec = vecBlock(iblock);
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      // add nothing: grad cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::grad_f_grad_phi_i: Operator 'grad_f_grad_phi_i' is not defined for quad_point_field: how to compute grad f ??");
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        for (int icomp=0; icomp<fe.numCoor(); icomp++) {
          tmp = 0.0;
          for (int jdof=0; jdof<fe.numDof(); jdof++)
            tmp += fe.phi[ig](jdof)*elfield.val(icomp,jdof);
          tmp *= coef * fe.weightMeas(ig);
          for(int idof=0; idof<fe.numDof(); idof++)
            vec(idof) += tmp * fe.dPhi[ig](icomp,idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::div_f_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield1,const int iblock)
{
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),"ElementVector::div_f_phi_i: The quadrature rules must be the same for the two elements");
  double tmp;
  UBlasVectorRange vec = vecBlock(iblock);
  switch(elfield1.type()) {
    case CONSTANT_FIELD:
      // add nothing: div cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::div_f_phi_i: Operator 'div_f_phi_i' is not defined for quad_point_field: how to compute div f ??");
      break;
    case DOF_FIELD:
      for(int ig=0; ig<fe2.numQuadraturePoint(); ig++) {
        tmp = 0.0;
        for (int icomp=0; icomp<fe1.numCoor(); icomp++) {
          for (int jdof=0; jdof<fe1.numDof(); jdof++) {
            tmp += fe1.dPhi[ig](icomp,jdof) * elfield1.val(icomp,jdof);
          }
        }
        tmp *= coef * fe2.weightMeas(ig);
        for(int idof=0; idof<fe2.numDof(); idof++) {
          vec(idof) += tmp * fe2.phi[ig](idof);
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_div_phi_i(const double coef, const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield2,const int iblock)
{
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),
            "ElementVector::f_div_phi_i: The quadrature rules must be the same for the two elements");
  double tmp;
  switch(elfield2.type()) {
    case CONSTANT_FIELD:
      FEL_ERROR("ElementVector::f_div_phi_i: Operator 'f_div_phi_i' is not defined yet for constant_field");
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementVector::f_div_phi_i: Operator 'f_div_phi_i' is not defined yet for quad_point_field");
      break;
    case DOF_FIELD:
      for (int icomp=0; icomp<fe1.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe2.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for (int jdof=0; jdof<fe1.numDof(); jdof++) {
            tmp += fe2.phi[ig](jdof)*elfield2.val(0,jdof);
          }
          tmp *= coef * fe1.weightMeas(ig);
          for(int idof=0; idof<fe1.numDof(); idof++) {
            vec(idof) += tmp * fe1.dPhi[ig](icomp,idof);
          }
        }
      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_u_dot_grad_LSu_v(const double coef,const ElementField& u,const ElementField& LSu,const std::vector<double>& data,double avHeavU0,double avHeavMinusU0,double insidePar,double outsidePar,const CurvilinearFiniteElement& fe,std::size_t iblock)
{
  UBlasVectorRange vec = vecBlock(iblock);
  // grad_level_set
  UBlasVector vec_grad_phi(2);
  //norm of grad_level_set
  // double norm_grad_phi = 0.0;
  // grad_transmembrane_pot
  UBlasVector vec_grad_u(2);
  double grad_u_dot_grad_phi = 0.0;
  double dirac = 0.0;
  const double epsilon = 1.0e-5;

  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    vec_grad_phi(0) = 0.0;
    vec_grad_phi(1) = 0.0;
    vec_grad_u(0) = 0.0;
    vec_grad_u(1) = 0.0;
    grad_u_dot_grad_phi = 0.0;
    for( felInt icoor = 0; icoor<fe.numRefCoor(); icoor++) {
      for (int idof=0; idof<fe.numDof(); idof++) {
        vec_grad_phi(icoor) += LSu.val(0, idof) * fe.dPhiRef(ig)(icoor, idof);
        vec_grad_u(icoor) += u.val(0, idof) * fe.dPhiRef(ig)(icoor, idof);
      }
    }

    grad_u_dot_grad_phi = std::sqrt(vec_grad_phi(0)*vec_grad_phi(0)+vec_grad_phi(1)*vec_grad_phi(1));

    for (int idof=0; idof<fe.numDof(); idof++) {
      dirac = ( 1./ std::sqrt(3.14151) )* ( epsilon / ( epsilon*epsilon + LSu.val(0, idof)*LSu.val(0, idof) ) );
      vec(idof) += coef * fe.weightMeas(ig) * grad_u_dot_grad_phi * dirac *(insidePar*(data[idof]-avHeavU0)*(data[idof]-avHeavU0) + outsidePar*(data[idof]-avHeavMinusU0)*(data[idof]-avHeavMinusU0))* fe.phi[ig](idof);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void  ElementVector::sourceHyp1DCont(std::vector<double>& coef,const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield1,const ElementField& elfield2,const int iblock,int iterm)
{
  FEL_CHECK(elfield2.type()==DOF_FIELD,"Operator Continuity for 1D Fsi not yet implemented with this type of element fields");
  FEL_CHECK(elfield1.type()==DOF_FIELD,"Operator Continuity for 1D Fsi not yet implemented with this type of element fields");
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),"The quadrature rules must be the same for the two elements");

  FEL_ASSERT(coef.size()==4);
  const double dt = coef[0];
  const double k = coef[1];
  const double betaDivrho = coef[2];
  //const double sectionAtRest = coef[3];

  double tmpA,tmpU,tmpDxA,tmpDxU,tmpDxF1,tmpDxF2;

  UBlasVectorRange vec = vecBlock(iblock);

  switch (iterm) {

    case 0:

      break;
    case 1: {
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmpU=0.0;
        tmpA=0.0;
        for(int i=0; i<fe1.numDof(); i++) {
          tmpA+= elfield1.val(0,i)*fe1.phi[ig](i);
        }
        for(int j=0; j<fe2.numDof(); j++) {
          tmpU+= elfield2.val(0,j)*fe2.phi[ig](j);
        }
        double Au = tmpA*tmpU;
        double tmp = tmpU*0.5*k*dt;

        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec(idof)+= dt*(Au-tmp)*fe1.dPhi[ig](0,idof)*fe1.weightMeas(ig);
        }
      }
    }
      break;

    case 2:

      break;

    case 3: {
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmpA=0.0;
        tmpU=0.0;
        tmpDxA=0.0;
        tmpDxU=0.0;
        for(int i=0; i<fe1.numDof(); i++) {
          tmpA+= elfield1.val(0,i)*fe1.phi[ig](i);
          tmpDxA+=  elfield1.val(0,i)*fe1.dPhi[ig](0,i);
        }

        for(int j=0; j<fe2.numDof(); j++) {
          tmpU+= elfield2.val(0,j)*fe2.phi[ig](j);
          tmpDxU+= elfield2.val(0,j)*fe2.dPhi[ig](0,j);
        }

        tmpDxF1 = tmpA*tmpDxU + tmpU*tmpDxA;
        tmpDxF2 = tmpU*tmpDxU + (0.5*betaDivrho/(std::sqrt(tmpA)))*tmpDxA;

        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec(idof)+= -0.5*dt*dt*(tmpU*tmpDxF1+tmpA*tmpDxF2)*fe1.dPhi[ig](0,idof)*fe1.weightMeas(ig);
        }
      }
    }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void  ElementVector::sourceHyp1DMom(std::vector<double>& coef,const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2,const ElementField& elfield1,const ElementField& elfield2,const int iblock,int iterm)
{
  FEL_CHECK(elfield2.type()==DOF_FIELD,"Operator Momentum for 1D Fsi not yet implemented with this type of element fields");
  FEL_CHECK(elfield1.type()==DOF_FIELD,"Operator Momentum for 1D Fsi not yet implemented with this type of element fields");
  FEL_CHECK(fe1.numQuadraturePoint() == fe2.numQuadraturePoint(),"The quadrature rules must be the same for the two elements");

  FEL_ASSERT(coef.size()==4);
  const double dt = coef[0];
  const double k = coef[1];
  const double betaDivrho = coef[2];
  const double r_0 = std::sqrt(coef[3]);

  double tmpA,tmpU,tmpDxA,tmpDxU,tmpDxF1,tmpDxF2;

  UBlasVectorRange vec = vecBlock(iblock);

  switch (iterm) {
    case 0: {
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmpU=0.0;
        tmpA=0.0;
        for(int i=0; i<fe1.numDof(); i++) {
          tmpA+= elfield1.val(0,i)*fe1.phi[ig](i);
        }
        for(int j=0; j<fe2.numDof(); j++) {
          tmpU+= elfield2.val(0,j)*fe2.phi[ig](j);
        }
        const double UdivA = tmpU/tmpA;
        const double UdivA2 = tmpU/(tmpA*tmpA);

        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec(idof)+= dt*(-k*UdivA+0.5*dt*k*k*UdivA2)*fe2.phi[ig](idof)*fe2.weightMeas(ig);
        }
      }
    }
      break;
    case 1: {
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmpU=0.0;
        tmpA=0.0;
        for(int i=0; i<fe1.numDof(); i++) {
          tmpA+= elfield1.val(0,i)*fe1.phi[ig](i);
        }
        for(int j=0; j<fe2.numDof(); j++) {
          tmpU+= elfield2.val(0,j)*fe2.phi[ig](j);
        }
        const double tmpP = betaDivrho*(std::sqrt(tmpA)-r_0);
        const double U2DivA = (tmpU*tmpU)/tmpA;

        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec(idof)+= dt*(0.5*tmpU*tmpU+tmpP-0.5*dt*k*U2DivA)*fe2.dPhi[ig](0,idof)*fe2.weightMeas(ig);
        }
      }
    }
      break;
    case 2: {
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmpU=0.0;
        tmpA=0.0;
        tmpDxA=0.0;
        tmpDxU=0.0;
        for(int i=0; i<fe1.numDof(); i++) {
          tmpA+= elfield1.val(0,i)*fe1.phi[ig](i);
          tmpDxA+= elfield1.val(0,i)*fe1.dPhi[ig](0,i);
        }

        for(int j=0; j<fe2.numDof(); j++) {
          tmpU+= elfield2.val(0,j)*fe2.phi[ig](j);
          tmpDxU+= elfield2.val(0,j)*fe2.dPhi[ig](0,j);
        }

        tmpDxF1 = tmpA*tmpDxU + tmpU*tmpDxA;
        tmpDxF2 = tmpU*tmpDxU + (0.5*betaDivrho*1.0/(std::sqrt(tmpA)))*tmpDxA;

        const double UdivA2 = tmpU/(tmpA*tmpA);


        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec(idof)+= -0.5*dt*dt*(k*UdivA2*tmpDxF1-(k/tmpA)*tmpDxF2)*fe2.phi[ig](idof)*fe2.weightMeas(ig);
        }
      }
    }
      break;
    case 3: {
      for(int ig=0; ig<fe1.numQuadraturePoint(); ig++) {
        tmpU=0.0;
        tmpA=0.0;
        tmpDxA=0.0;
        tmpDxU=0.0;
        for(int i=0; i<fe1.numDof(); i++) {
          tmpA+= elfield1.val(0,i)*fe1.phi[ig](i);
          tmpDxA+=  elfield1.val(0,i)*fe1.dPhi[ig](0,i);
        }

        for(int j=0; j<fe2.numDof(); j++) {
          tmpU+= elfield2.val(0,j)*fe2.phi[ig](j);
          tmpDxU+= elfield2.val(0,j)*fe2.dPhi[ig](0,j);
        }

        tmpDxF1 = tmpA*tmpDxU + tmpU*tmpDxA;
        tmpDxF2 = tmpU*tmpDxU + (0.5*betaDivrho/(std::sqrt(tmpA)))*tmpDxA;

        const double tmpR = std::sqrt(tmpA);

        for(int idof=0; idof<fe1.numDof(); idof++) {
          vec(idof)+= -0.5*dt*dt*(0.5*(betaDivrho/tmpR)*tmpDxF1 + tmpU*tmpDxF2)*fe2.dPhi[ig](0,idof)*fe2.weightMeas(ig);
        }
      }
    }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::abs_u_dot_n_u_dot_phi_i(const double coef,
                                            const ElementField& elfield,
                                            const CurvilinearFiniteElement& febd,
                                            const int iblock, const int numComp) {
  double u_n,u_bd;
  switch(elfield.type()) {
    case DOF_FIELD: // P1
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<febd.numQuadraturePoint(); ig++) {

          // compute 1/2[ ||u.n|| - u.n ](ig)
          u_n = 0.0;
          for(int jdof=0; jdof<febd.numDof(); jdof++) {
            for (int jcomp=0; jcomp<numComp; jcomp++) {
              u_n +=  febd.phi[ig](jdof) * elfield.val(jcomp,jdof)
              * febd.normal[ig](jcomp);
            }
          }
          // compute u_icomp (ig)
          u_bd = 0.0;
          for(int jdof=0; jdof<febd.numDof(); jdof++) {
            u_bd += febd.phi[ig](jdof) * elfield.val(icomp,jdof);
          }


          // compute local contribution for component icomp
          for(int idof=0; idof<febd.numDof(); idof++) {
            vec(idof) += coef * 0.5 * (std::abs(u_n) - u_n) * u_bd *
            febd.phi[ig](idof) *  febd.weightMeas(ig);
          }
        }
      } // for i comp
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("Operator 'abs_u_dot_n_u_dot_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::u_grad_u_phi_i(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield,const int iblock,const int numComp)
{
  double tmp,u_i2,di2_u_icomp;
  switch(elfield.type()) {
    case DOF_FIELD:
      // loop over components [b]: sum_a u_a d_a u_b phi
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);

        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for(int i2comp=0; i2comp<numComp; i2comp++) {
            di2_u_icomp = 0.0;
            u_i2 = 0.0;
            // compute d_a u_b
            for (int jdof=0; jdof<fe.numDof(); jdof++) {
              di2_u_icomp += fe.dPhi[ig](i2comp,jdof)*elfield.val(icomp,jdof);
            }
            // compute u_a
            for(int idof=0; idof<fe.numDof(); idof++) {
              u_i2 +=  fe.phi[ig](idof) * elfield.val(i2comp,idof);
            }
            // u_a d_a(u_b)
            tmp += u_i2*di2_u_icomp;
          }
          // add to std::vector
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * tmp  * fe.phi[ig](idof) * fe.weightMeas(ig);
          }

        } // ig
      } // icomp
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:

      FEL_ERROR("Operator 'source' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::u_grad_u_grad_phi_i(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield,const int iblock,const int numComp)
{
  double tmp,u_i2,di2_u_icomp;
  UBlasVectorRange vec = vecBlock(iblock);
  switch(elfield.type()) {
    case DOF_FIELD:
      // loop over components [b]: sum_a u_a d_a u_b phi
      for(int icomp=0; icomp<numComp; icomp++) {

        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for(int i2comp=0; i2comp<numComp; i2comp++) {
            di2_u_icomp = 0.0;
            u_i2 = 0.0;
            // compute d_a u_b
            for (int jdof=0; jdof<fe.numDof(); jdof++) {
              di2_u_icomp += fe.dPhi[ig](i2comp,jdof)*elfield.val(icomp,jdof);
            }
            // compute u_a
            for(int idof=0; idof<fe.numDof(); idof++) {
              u_i2 +=  fe.phi[ig](idof) * elfield.val(i2comp,idof);
            }
            // u_a d_a(u_b)
            tmp += u_i2*di2_u_icomp;
          }
          // add to std::vector
          for(int idof=0; idof<fe.numDof(); idof++) {
            vec(idof) += coef * tmp  * fe.dPhi[ig](icomp,idof) * fe.weightMeas(ig);
          }

        } // ig
      } // icomp
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:

      FEL_ERROR("Operator 'source' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_dot_n_phi_i_dot_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock)
{
  double tmp,f_dot_n;
  switch(elfield.type()) {
    case DOF_FIELD: {
      for(int icomp=0; icomp<fe.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          f_dot_n = 0.0;
          // compute f \cdot n
          for(int icoor=0; icoor<fe.numCoor(); icoor++) {
            tmp = 0.0;
            for(int jdof=0; jdof<fe.numDof(); jdof++) {
              tmp +=  fe.phi[ig](jdof) * elfield.val(icoor,jdof);
            }
            // sum f_icoor * n_icoor
            f_dot_n += tmp * fe.normal[ig](icoor);
          }

          for( std::size_t idof=0 ; idof < (std::size_t) fe.numDof() ; idof++ ) {
            //  coef * f_dot_n * ( phi_i \cdot \n )
            vec(idof) +=  coef * f_dot_n * fe.weightMeas(ig) * fe.normal[ig](icomp)  * fe.phi[ig](idof);
          }
        }
      }
    }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("Operator 'f_dot_n_phi_i_dot_n' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_times_n_phi_i_times_n(const double coef,const CurvilinearFiniteElement& fe,const ElementField& elfield,const int iblock)
{
  double tmp,aux,inDiagonal;
  switch(elfield.type()) {
    case DOF_FIELD: {
      for(int icomp=0; icomp<fe.numCoor(); icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          aux = 0.0;
          for(int icoor=0; icoor<fe.numCoor(); icoor++) {
            tmp = 0.0;
            inDiagonal = 0.0;
            if (icoor==icomp) {
              inDiagonal = 1.;
            }
            for(int jdof=0; jdof<fe.numDof(); jdof++) {
              tmp +=  fe.phi[ig](jdof) * elfield.val(icoor,jdof);
            }
            aux += tmp * (inDiagonal - fe.normal[ig](icoor) * fe.normal[ig](icomp));
          }
          for( std::size_t idof=0 ; idof < (std::size_t) fe.numDof() ; idof++ ) {

            vec(idof) +=  coef * aux * fe.weightMeas(ig) * fe.phi[ig](idof);
          }
        }
      }
    }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("Operator 'f_times_n_phi_i_times_n' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::grad_f_dot_n_times_n_phi_i_times_n(const double coef,const CurrentFiniteElementWithBd& fewbd,
                                                        const ElementField& elfield,
                                                        const int iblockBd, const int numblockBd,
                                                        const int iblock, const int numComp)
{
  FEL_ASSERT(iblockBd>=0 && iblockBd<fewbd.numBdEle() && (numblockBd-iblockBd) <= fewbd.numBdEle() );
  FEL_ASSERT( static_cast<int>(m_tmpVecDof.size()) == fewbd.numDof() );

  double tmp,aux,inDiagonal;
  switch(elfield.type()) {
    case CONSTANT_FIELD:
      // add nothing: grad cst = 0
      break;
    case QUAD_POINT_FIELD:
      FEL_ERROR("Operator 'grad_f_dot_n_times_n_phi_i_times_n' is not well defined for quad_point_field: how to compute grad f ??");
      break;
    case DOF_FIELD:
      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for (int ibd=iblockBd; ibd<iblockBd+numblockBd; ibd++) {
          FEL_ASSERT( fewbd.bdEle(ibd).hasNormal() && fewbd.hasFirstDeriv() );
          for(int ilocg=0; ilocg<fewbd.numQuadPointBdEle(ibd); ilocg++) {
            int ig = ilocg+fewbd.indexQuadPoint(ibd+1);
            // face ibd starts at ibd+1 (0 for internal quad points)
            //! Vector = [n \cdot \grad \phi]:
            m_tmpVecDof = prod(trans(fewbd.bdEle(ibd).normal[ilocg]), fewbd.dPhi[ig]);
            tmp = 0.0;
            for(int icoor=0; icoor<fewbd.numCoor(); icoor++) {
              aux = 0.0;
              inDiagonal = 0.0;
              if (icoor==icomp) {
                inDiagonal = 1.;
              }
              for(int jdof=0; jdof<fewbd.numDof(); jdof++) {
                //! tmp = [n \cdot \grad f] = \sum f_jdof (grad phi_jdof \cdot n)
                aux += m_tmpVecDof(jdof) * elfield.val(icoor,jdof);
              }
              tmp += aux * (inDiagonal - fewbd.bdEle(ibd).normal[ilocg](icoor) * fewbd.bdEle(ibd).normal[ilocg](icomp));
            }
            tmp *= coef * fewbd.bdEle(ibd).weightMeas(ilocg);
            for(int idof=0; idof<fewbd.numDof(); idof++) {
              vec(idof) +=  tmp * fewbd.bdEle(ibd).phi[ilocg](idof);
            }
          }
        }
      }
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //        FEL_ERROR("Operator 'grad_f_dot_n_phi_i' not yet implemented with this type of element fields");
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::print(int verbose,std::ostream& c)
{
  if(verbose) {
    for (std::size_t i=0; i<numBlockRow(); i++) {
      c << "Block (" << i << "), ";
      c << vecBlock( i ) << std::endl;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::GradientBasedHyperElasticityVector_grad_phi_i(const double coef,
                                                                  const CurrentFiniteElement& finite_element,
                                                                  int quadraturePointIndex,
                                                                  UBlasVector& rhsPart,
                                                                  std::size_t iblock)
{
  const std::size_t nC = finite_element.numRefCoor();
  const double modified_m_weightmeas = finite_element.weightMeas(quadraturePointIndex) * coef;

  const UBlasMatrix trans_dPhi = trans(finite_element.dPhi[quadraturePointIndex]);

  for (std::size_t icoor = 0u; icoor < nC; ++icoor) {
    UBlasVectorRange vec = vecBlock(iblock + icoor);
    UBlasVectorRange Block(rhsPart, UBlasRange(icoor * nC, icoor * nC + nC));

    vec += modified_m_weightmeas * prod(trans_dPhi, Block);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stab_supgAdvDiffCT(const double dt, const double stab1, const double density, const double visc, const CurrentFiniteElement& feVel, const CurrentFiniteElement& fePres,
                                        const ElementField& vel, const ElementField& pres, const ElementField& rhs)
{
  FEL_CHECK(feVel.numQuadraturePoint() == fePres.numQuadraturePoint(),"ElementVector::stab_supgAdvDiffCT: The quadrature rules must be the same for the two elements");
  FEL_ASSERT(vel.type() == DOF_FIELD);
  FEL_ASSERT(pres.type() == DOF_FIELD);
  FEL_ASSERT(rhs.type() == DOF_FIELD);

  // Resize if required
  if (m_tmpVec2Coor.size() != m_tmpVecCoor.size())
    m_tmpVec2Coor.resize(m_tmpVecCoor.size());

  double weightJ, norm_u, aux, tau;
  const double h_elem = feVel.diameter();
  UBlasVector tmpVecCoor3(feVel.numCoor());

  for(int ig=0; ig<feVel.numQuadraturePoint(); ig++) {
    weightJ = feVel.weightMeas(ig);

    // u at the integration point ig
    m_tmpVecCoor = prod(vel.val, feVel.phi[ig]); // vel

    //  grad p at the integration point ig
    for(int icomp=0; icomp<feVel.numCoor(); icomp++) { // grad pres
      m_tmpVec2Coor(icomp) = 0.0;
      for(int idof=0; idof<fePres.numDof(); idof++)
        m_tmpVec2Coor(icomp) += fePres.dPhi[ig](icomp, idof)*pres.val(0, idof);
    }

    // rhs at the integration point ig
    tmpVecCoor3 = prod(rhs.val,feVel.phi[ig]); // rhs

    norm_u = norm_1(m_tmpVecCoor);
    tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
                            (4.*norm_u*norm_u)/(h_elem*h_elem) +
                            (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                            ) / density;

    //
    for(int icomp=0; icomp< feVel.numCoor(); icomp++) {
      UBlasVectorRange vec = vecBlock(icomp);

      for(int idof=0; idof<feVel.numDof(); idof++) {
        aux = 0;
        for(int jcomp=0; jcomp< feVel.numCoor(); jcomp++)
          aux+=m_tmpVecCoor(jcomp)*feVel.dPhi[ig](jcomp,idof);
        vec(idof) -= m_tmpVec2Coor(icomp)*aux*tau*weightJ; // - grad p . vel . grad v
        vec(idof) += tmpVecCoor3(icomp)*aux*tau*weightJ; // rhs . vel . grad v
      }//idof
    }//icomp

  }//ig

}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stab_supgProjCT(const double dt,const double stab1,const double density, const double visc, const CurrentFiniteElement& fe,
                                    const ElementField& beta, const ElementField& vel, const ElementField& pres,
                                    const ElementField& rhs)
{
  FEL_ASSERT(vel.type() == DOF_FIELD);
  FEL_ASSERT(pres.type() == DOF_FIELD);
  FEL_ASSERT(rhs.type() == DOF_FIELD);

  // Resize if required
  if (m_tmpVec2Coor.size() != m_tmpVecCoor.size())
    m_tmpVec2Coor.resize(m_tmpVecCoor.size());

  double norm_u,  di2_u_icomp, tau;
  const double h_elem = fe.diameter();

  UBlasVector tmpVecCoor3(fe.numCoor());
  UBlasVector tmpVecCoor4(fe.numCoor());

  UBlasVectorRange vec = vecBlock(0);
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {

    //  u at the ig
    m_tmpVecCoor = prod(beta.val, fe.phi[ig]); // beta

    //  grad p at the integration point ig
    for(int icomp=0; icomp<fe.numCoor(); icomp++) { // grad pres
      m_tmpVec2Coor(icomp) = 0.0;
      for(int idof=0; idof<fe.numDof(); idof++)
        m_tmpVec2Coor(icomp) += fe.dPhi[ig](icomp, idof)*pres.val(0, idof);
    }

    // rhs at the integration point ig
    tmpVecCoor3 = prod(rhs.val,fe.phi[ig]); // rhs

    for(int icomp=0; icomp<fe.numCoor(); icomp++) {
      tmpVecCoor4(icomp) = 0.0;
      for(int i2comp=0; i2comp<fe.numCoor(); i2comp++) {
        di2_u_icomp = 0.0;
        // compute d_a u_b
        for (int jdof=0; jdof<fe.numDof(); jdof++)
          di2_u_icomp += fe.dPhi[ig](i2comp,jdof)*vel.val(icomp,jdof);

        // u_a d_a(u_b)
        tmpVecCoor4(icomp) +=  m_tmpVecCoor(i2comp)*di2_u_icomp;
      }
    }

    // stabilization parameter
    norm_u = norm_1(m_tmpVecCoor);
    tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
                            (4.*norm_u*norm_u)/(h_elem*h_elem) +
                            (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                            ) / density;


    for(int idof=0; idof<fe.numDof(); idof++)
      for(int icomp=0; icomp<fe.numCoor(); icomp++) {
        // - u . grad v
        vec(idof) -= tau * fe.weightMeas(ig) * tmpVecCoor4(icomp)  * fe.dPhi[ig](icomp,idof);
        // + f . grad q
        vec(idof) += tau * fe.weightMeas(ig) * tmpVecCoor3(icomp)  * fe.dPhi[ig](icomp,idof);
        // - grad p .grad q
        vec(idof) -= tau * fe.weightMeas(ig) * m_tmpVec2Coor(icomp)* fe.dPhi[ig](icomp,idof);
      }

  }//ig

}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stab_supgRhsFS(const double dt, const double stabSUPG, const double density, const double viscosity,
                                   const double c0, const double c1, const double c2,
                                    const CurrentFiniteElement& feVel, const CurrentFiniteElement& fePre,
                                    const ElementField& vel, const ElementField& pres, const ElementField& velRHS)
{
  // \int_K [stabSUPG * tau * c0 * (vel . \grad) v] . [ c1 velRHS + c2 \grad pres]

  FEL_ASSERT(vel.type() == DOF_FIELD);
  FEL_ASSERT(pres.type() == DOF_FIELD);
  FEL_ASSERT(velRHS.type() == DOF_FIELD);

  // Resize if required
  if (m_tmpVec2Coor.size() != m_tmpVecCoor.size())
    m_tmpVec2Coor.resize(m_tmpVecCoor.size());

  double weightJ, norm_u, aux;
  const double h_elem = feVel.diameter();
  double tau;
  UBlasVector tmpVecCoor3(feVel.numCoor());

  for(int ig=0; ig<feVel.numQuadraturePoint(); ig++) {
    weightJ = feVel.weightMeas(ig);

    // Computation of u and grad p in the element in the integration point ig
    // vel
    m_tmpVecCoor = prod(vel.val, feVel.phi[ig]);

    // RHS vel
    m_tmpVec2Coor = prod(velRHS.val, feVel.phi[ig]);

    // grad pres
    for(int icomp=0; icomp<feVel.numCoor(); icomp++) {
      tmpVecCoor3(icomp) = 0.0;
      for(int idof=0; idof<fePre.numDof(); idof++)
        tmpVecCoor3(icomp) += fePre.dPhi[ig](icomp, idof) * pres.val(0, idof);
    }

    norm_u = norm_1(m_tmpVecCoor);

    tau = stabSUPG / std::sqrt( (4./(dt*dt)) +
                          (4.*norm_u*norm_u)/(h_elem*h_elem) +
                          (16*viscosity*viscosity)/(density*density*h_elem*h_elem*h_elem*h_elem));

    for(int icomp=0; icomp<feVel.numCoor(); icomp++) {
      UBlasVectorRange vec = vecBlock(icomp);

      for(int idof=0; idof<feVel.numDof(); idof++) {
        aux = 0;
        for(int jcomp=0; jcomp<feVel.numCoor(); jcomp++) {
          aux += m_tmpVecCoor(jcomp) * feVel.dPhi[ig](jcomp, idof);
        }
        aux *= c0 * tau * weightJ; // tau * C_0 (U . \grad) . v_i

        vec(idof) += c1 * aux * m_tmpVec2Coor(icomp);
        vec(idof) += c2 * aux * tmpVecCoor3(icomp);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::quadratic(const double coef, const CurrentFiniteElement& fe,const ElementField& elfield, const int iblock,const int numComp)
{
  if(numComp>1) {
    FEL_ERROR("Not defined for vector fields!")
  }

  double tmp;
  switch(elfield.type()) {
    case CONSTANT_FIELD:

      break;
    case QUAD_POINT_FIELD:

      break;

    case DOF_FIELD:

      for(int icomp=0; icomp<numComp; icomp++) {
        UBlasVectorRange vec = vecBlock(iblock+icomp);
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          tmp = 0.0;
          for(int idof=0; idof<fe.numDof(); idof++) {

            for(int jdof=0; jdof<fe.numDof(); jdof++) {
              for(int hdof=0; hdof<fe.numDof(); hdof++) {
                tmp +=  fe.phi[ig](jdof) * elfield.val(icomp,jdof) *fe.phi[ig](hdof) * elfield.val(icomp,hdof);
              }
            }
            vec(idof) += coef * tmp  * fe.phi[ig](idof) * fe.weightMeas(ig);
          }
        }

      }
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::sGrad_phi_i_tensor_sGrad_f(const double coef, const ElementField& f, const ElementField& normalField, const CurvilinearFiniteElement& fe, UBlasMatrix tensor)
{
  UBlasVector sGradF(2);
  sGradF=Curvatures::elemFieldGradient(f);

  // if tensor is the invers of the first fundamental form you get the laplacian operator on the surface
  UBlasVector KsGradF(2);
  KsGradF(0)=0.0;
  KsGradF(1)=0.0;
  KsGradF = prod(tensor, sGradF );

  for ( int iLocDof(0); iLocDof < fe.numDof(); iLocDof++) {
    UBlasVector sGradPhi_i(2);
    sGradPhi_i=Curvatures::testFuncGradient(iLocDof);

    double val = coef * inner_prod(KsGradF, sGradPhi_i) * fe.measure();
    for ( int iCoor(0); iCoor < fe.numCoor(); iCoor++) {
      double value = val*normalField.val(iCoor,iLocDof); // here we multiply the result by the normal std::vector in order to use the result in the NSSimplifiedFSI model
      UBlasVectorRange rhs = vecBlock(iCoor);
      rhs(iLocDof) += value;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::sGrad_phi_i_tensor_sGrad_f(const double coef, const ElementField& elemCoef, const ElementField& f, const ElementField& normalField, const CurvilinearFiniteElement& fe, UBlasMatrix tensor)
{
  UBlasVector tmpVecQuadPoint;
  switch(elemCoef.type()) {
    case DOF_FIELD:
      tmpVecQuadPoint.resize(fe.numQuadraturePoint());
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmpVecQuadPoint(ig)  = prod(elemCoef.val,fe.phi[ig])(0);
      }
      break;

    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("this case has not been implemented");
      break;
  }

  UBlasVector sGradF(2);
  sGradF=Curvatures::elemFieldGradient(f);

  // if tensor is the invers of the first fundamental form you get the laplacian operator on the surface
  UBlasVector KsGradF(2);
  KsGradF(0)=0.0;
  KsGradF(1)=0.0;
  KsGradF = prod(tensor, sGradF );

  for ( int iLocDof(0); iLocDof < fe.numDof(); iLocDof++) {
    UBlasVector sGradPhi_i(2);
    sGradPhi_i=Curvatures::testFuncGradient(iLocDof);

    double val(0.0);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
      val += coef * tmpVecQuadPoint(ig) * inner_prod(KsGradF, sGradPhi_i) * fe.weightMeas(ig);  //TODO only P1! we should integrate, but gradient is constant over the element
    for ( int iCoor(0); iCoor < fe.numCoor(); iCoor++) {
      double value = val*normalField.val(iCoor,iLocDof); // here we multiply the result by the normal std::vector in order to use the result in the NSSimplifiedFSI model
      UBlasVectorRange rhs = vecBlock(iCoor);
      rhs(iLocDof) += value;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::operator+=(const ElementVector& vec)
{
  m_vec += vec.vec();
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::operator-=(const ElementVector& vec)
{
  m_vec -= vec.vec();
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::operator*=(const double factor)
{
  m_vec *= factor;
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::stab_supg(const double dt, const double stab1, const double density, const double visc,
                              const CurrentFiniteElement& fe,const ElementField& vel,const ElementField& rhs,
                              double& Re_elem,double& tau, const int type)
{
  /*
    Typical value for stab1 = 0.1
    This stabilization does not include the 1/dt term (not strongly consistant with the time discrete scheme, may
    be viewed as a space-time, P0 in time)
    If type = 1: stabilization parameter depends on dt (Tezduyar)
    If type = 2: stabilization parameter independent of dt (Franca-Frey CMAME 1992)
    */
  FEL_ASSERT(vel.type() == DOF_FIELD);
  FEL_ASSERT(rhs.type() == DOF_FIELD);

  // Resize if required
  if (m_tmpVec2Coor.size() != m_tmpVecCoor.size())
    m_tmpVec2Coor.resize(m_tmpVecCoor.size());

  double weightJ,norm_u,s,adveps;
  const double h_elem = fe.diameter();
  int i0,i0_pres;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    weightJ = fe.weightMeas(ig);
    // Computation of u and f in the element in the integration point ig
    m_tmpVecCoor = prod(vel.val,fe.phi[ig]); // vel
    norm_u = norm_1(m_tmpVecCoor);
    m_tmpVec2Coor = prod(rhs.val,fe.phi[ig]); // rhs
    Re_elem = density*stab1*norm_u*h_elem/(4*visc);
    if(Re_elem<1.0) {
      tau = stab1*density*h_elem*h_elem/(8*visc);
    } else {
      tau = h_elem/(2.*norm_u);
    }
    if(type==1) {
      tau = 10.*stab1 / std::sqrt( (4./(dt*dt)) +
                              (4.*norm_u*norm_u)/(h_elem*h_elem) +
                              (16.*visc*visc)/(density*density*h_elem*h_elem*h_elem*h_elem)
                              ) / density;
      // coef 10 is to have a typical value of 0.1 as for type 2
    }
    else if (type==2) {
      // FreeFem
      tau = stab1*h_elem*h_elem/visc;
    }
    else if (type==3) {
      tau = 10.*stab1 / std::sqrt( (16.*visc*visc)/(h_elem*h_elem*h_elem*h_elem) );
    }

    int ncoor = fe.numCoor();
    i0_pres = m_firstRow[ncoor];
    for(int idof=0; idof<fe.numDof(); idof++) {
      adveps=0.0;
      for(int kcoor=0; kcoor<ncoor; kcoor++) {
        adveps += m_tmpVecCoor[kcoor]* fe.dPhi[ig](kcoor,idof);
      }
      adveps *= density*tau*weightJ;// rho (u.\gradv_i)
      s=0.0;
      for(int icoor=0; icoor<ncoor; icoor++) {
        i0 = m_firstRow[icoor];
        m_vec(i0+idof) += m_tmpVec2Coor[icoor]*adveps;
        s += fe.dPhi[ig](icoor,idof)*m_tmpVec2Coor(icoor); // same fe for velocity and pressure
      }
      m_vec(i0_pres+idof) -= tau*s*weightJ;

    }// idof
  }// ig
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::f_scalar_phi_i(const double coef,const CurrentFiniteElement& fe, const ElementField& elfield,const int iblock)
{
  double tmp;
  for(int icomp=0; icomp<fe.numCoor(); icomp++) {
    UBlasVectorRange vec = vecBlock(iblock+icomp);
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      tmp = coef*fe.weightMeas(ig);
      for(int idof=0; idof<fe.numDof(); idof++) {
        vec(idof) +=  tmp * elfield.val(icomp,idof);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::div_u_div_phi_i(const double coef,const CurrentFiniteElement& fe, const ElementField& elfield, const int iblock)
{
  //rem : elfield is a dof field
  for(int kcomp=0; kcomp<fe.numCoor(); kcomp++){
    UBlasVectorRange vec = vecBlock(iblock+kcomp);
    for(int idof=0; idof<fe.numDof(); idof++) {
      double tmpi,tmpj;
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        tmpi = 0.0;
        tmpj = 0.0;
        for(int jdof=0; jdof<fe.numDof(); jdof++){
          for (int jcomp=0; jcomp<fe.numCoor(); jcomp++) {
            tmpi += fe.dPhi[ig](kcomp,idof);
            tmpj += fe.dPhi[ig](jcomp,jdof)*elfield.val(jcomp,jdof);
          }
        }
        vec(idof) += coef*tmpi*tmpj*fe.weightMeas(ig);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::transpirationSurfStabRHS( const double coef, const ElementField& elemCoef, const ElementField& normalField, const ElementField& force, const CurvilinearFiniteElement& fe, UBlasMatrix tensor)
{
  // Tensor is supposed to be the inverse matric tensor
  // TODO compute directly it from fe!
  UBlasVector tmpVecQuadPoint( fe.numQuadraturePoint() );
  switch(elemCoef.type()) {
    case DOF_FIELD:
      for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
        m_tmpVecCoor = prod(elemCoef.val,fe.phi[ig]);
        tmpVecQuadPoint(ig) = m_tmpVecCoor(0)*coef;
      }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("this case has not been implemented");
      break;
  }
  // Only P1
  double coeffQuad = 0.0;
  for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
    coeffQuad +=  tmpVecQuadPoint(ig) * fe.weightMeas(ig) * fe.diameter() ;
  }

  for (int forceLocDof = 0; forceLocDof < fe.numDof(); forceLocDof++) {
    for (int testLocDof = 0; testLocDof < fe.numDof(); testLocDof++) {

      // computation of (\nabla_c\phi_test)^T A^-1 \nabla_c\phi_sol
      UBlasVector gradForce(2);
      UBlasVector gradTest(2);
      UBlasVector KgradForce(2);
      KgradForce(0)=0.0;
      KgradForce(1)=0.0;
      gradForce  = Curvatures::testFuncGradient(forceLocDof);
      gradTest = Curvatures::testFuncGradient(testLocDof);
      KgradForce = prod(tensor, gradForce );

      const double lap = inner_prod(KgradForce, gradTest);

      for ( int forceCoor(0); forceCoor < fe.numCoor(); forceCoor++) {
        for ( int testCoor(0); testCoor < fe.numCoor(); testCoor++) {
          double projCoef = 0;
          for ( int ausCoor(0); ausCoor < fe.numCoor(); ++ausCoor ) {
            double deltaTest(0), deltaSol(0);
            if ( forceCoor - ausCoor == 0 )
              deltaSol = 1;
            if ( testCoor - ausCoor == 0 )
              deltaTest = 1;
            // Setting normnorm
            const double normnormSol  = normalField.val( forceCoor, forceLocDof)*normalField.val(ausCoor, forceLocDof);
            const double normnormTest = normalField.val(testCoor,testLocDof)*normalField.val(ausCoor,testLocDof);
            projCoef += ( deltaSol - normnormSol ) * ( deltaTest - normnormTest );
          }

          const double value = projCoef * lap * coeffQuad * force.val(forceCoor,forceLocDof);

          UBlasVectorRange vec = vecBlock(testCoor);
          vec(testLocDof) += value;
        }
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::velocityConstraint(const double coef, const ElementField& disp, const CurvilinearFiniteElement& fe, const int iblock)
{
  FEL_CHECK(fe.hasMeas() ,"ElementVector::velocityConstraint: requires covariant basis");
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementVector::velocityConstraint: wrong displacement field type");

  const UBlasMatrix& d = disp.val;

  UBlasMatrix cbasis(fe.numRefCoor(),fe.numCoor()); // covariant basis in current configuation
  UBlasVector n(fe.numCoor()); // normal std::vector, not unitary
  UBlasMatrix datq(fe.numCoor(),fe.numQuadraturePoint()); // displacement at quadrature poins

  for (int ic=0; ic <fe.numCoor(); ++ic) {
    for(int iq=0; iq<fe.numQuadraturePoint(); ++iq) {
      datq(ic,iq)  = 0.0;
      for(int idof=0; idof<fe.numDof(); ++idof) {
        datq(ic,iq) +=  fe.phi[iq](idof) * d(ic,idof);
      }
    }
  }

  UBlasVectorRange vec = vecBlock(iblock);

  // Loop on quadrature points
  for (int iq=0; iq < fe.numQuadraturePoint(); ++iq) {

    // Covariant basis in current configuratgion
    for (int ir=0; ir < fe.numRefCoor(); ++ir) {
      for (int ic=0; ic <fe.numCoor(); ++ic) {
        cbasis(ir,ic) = fe.m_covBasis[iq](ir,ic);
        for(int idof=0; idof< fe.numDof(); idof++)
          cbasis(ir,ic) += d(ic,idof)*fe.dPhiRef(iq)(ir,idof);
      }
    }

    switch ( fe.numRefCoor() ) { // Warning: must be as in CurvilinearFiniteElement::computeNormal()
      case 1:
        n(0) =   cbasis(0,1);
        n(1) = - cbasis(0,0);
        break;
      case 2:
        n(0) = fe.m_sign * ( cbasis(0,1) * cbasis(1,2) - cbasis(0,2)*cbasis(1,1) );
        n(1) = fe.m_sign * ( cbasis(0,2) * cbasis(1,0) - cbasis(0,0)*cbasis(1,2) );
        n(2) = fe.m_sign * ( cbasis(0,0) * cbasis(1,1) - cbasis(0,1)*cbasis(1,0) );
        break;
      default:
        FEL_ERROR("ElementVector::velocityConstraint: numRefCoor must be 1 or 2");
    }

    // Update residual in elementary std::vector
    for (int ic=0; ic < fe.numCoor(); ++ic)
      vec(0) += coef*datq(ic,iq)*n(ic)*fe.quadratureRule().weight(iq);
  }

}

/***********************************************************************************/
/***********************************************************************************/

void ElementVector::followerPressure(const double coef, const ElementField& press, const ElementField& disp, const CurvilinearFiniteElement& fe)
{
  FEL_CHECK(fe.hasMeas() ,"ElementVector::followerPressure: requires covariant basis");
  FEL_CHECK(disp.type() == DOF_FIELD,"ElementVector::followerPressure: wrong displacement field type");
  FEL_CHECK(press.numComp() == 1,"ElementVector::followerPressure: wrong pressure field");

  const UBlasMatrix& d = disp.val;

  UBlasMatrix cbasis(fe.numRefCoor(),fe.numCoor()); // covariant basis in current configuation
  UBlasVector n(fe.numCoor()); // normal std::vector, not unitary
  UBlasVector patq(fe.numQuadraturePoint()); // pressure at quadrature poins

  // Evaluate pressure at quadrature points
  switch(press.type()) {
    case CONSTANT_FIELD:
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq)
        patq(iq) = press.val(0,0);
      break;
    case QUAD_POINT_FIELD:
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq)
        patq(iq) = press.val(0,iq);
      break;
    case DOF_FIELD:
      for(int iq=0; iq<fe.numQuadraturePoint(); ++iq) {
        patq(iq)  = 0.0;
        for(int idof=0; idof<fe.numDof(); ++idof)
          patq(iq) +=  fe.phi[iq](idof) * press.val(0,idof);
      }
      break;
    default:
      FEL_ERROR("this pressure element field type has not been implemented");
  }

  // Loop on quadrature points
  for (int iq=0; iq < fe.numQuadraturePoint(); ++iq) {

    // covariant basis in current configuratgion
    for (int ir=0; ir < fe.numRefCoor(); ++ir) {
      for (int ic=0; ic <fe.numCoor(); ++ic) {
        cbasis(ir,ic) = fe.m_covBasis[iq](ir,ic);
        for(int idof=0; idof< fe.numDof(); idof++)
          cbasis(ir,ic) += d(ic,idof)*fe.dPhiRef(iq)(ir,idof);
      }
    }

    switch ( fe.numRefCoor() ) { // Warning: must be as in CurvilinearFiniteElement::computeNormal()
      case 1:
        n(0) =   cbasis(0,1);
        n(1) = - cbasis(0,0);
        break;
      case 2:
        n(0) = fe.m_sign * ( cbasis(0,1) * cbasis(1,2) - cbasis(0,2)*cbasis(1,1) );
        n(1) = fe.m_sign * ( cbasis(0,2) * cbasis(1,0) - cbasis(0,0)*cbasis(1,2) );
        n(2) = fe.m_sign * ( cbasis(0,0) * cbasis(1,1) - cbasis(0,1)*cbasis(1,0) );
        break;
      default:
        FEL_ERROR("ElementVector::followerPressure: numRefCoor must be 1 or 2");
    }

    // Update residual in elementary std::vector
    for (int ic=0; ic < fe.numCoor(); ++ic) {
      UBlasVectorRange vec = vecBlock(ic);
      for(int idof=0; idof< fe.numDof(); idof++)
        vec(idof) += coef*patq(iq)*n(ic)*fe.phi[iq](idof)*fe.quadratureRule().weight(iq);
    }
  }
}

}
