//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "quadratureRule.hpp"

namespace felisce
{
QuadratureRule::QuadratureRule(
  const QuadraturePoint* point,
  const std::string name,
  const RefShape& shape,
  const int numQuadraturePoint,
  const int degreeOfExactness
  ) : m_point( point ),
      m_shape( &shape ),
      m_name( name )
{
  m_numQuadraturePoint[0] = numQuadraturePoint;
  m_degreeOfExactness[0] = degreeOfExactness;
}

/***********************************************************************************/
/***********************************************************************************/

QuadratureRule::QuadratureRule(
  const QuadratureRule& rQuadratureRule1,
  const QuadratureRule& rQuadratureRule2,
  const std::string name,
  const RefShape& shape,
  const double Coefficient
  ) : m_shape( &shape ),
      m_name( name )
{
  // Assign values
  m_numQuadraturePoint = new int[3];
  m_numQuadraturePoint[0] = rQuadratureRule1.numQuadraturePoint() * rQuadratureRule2.numQuadraturePoint();
  m_numQuadraturePoint[1] = rQuadratureRule1.numQuadraturePoint();
  m_numQuadraturePoint[2] = rQuadratureRule2.numQuadraturePoint();
  m_degreeOfExactness = new int[3];
  m_degreeOfExactness[0] = rQuadratureRule1.degreeOfExactness() > rQuadratureRule2.degreeOfExactness() ? rQuadratureRule1.degreeOfExactness() : rQuadratureRule2.degreeOfExactness();
  m_degreeOfExactness[1] = rQuadratureRule1.degreeOfExactness();
  m_degreeOfExactness[2] = rQuadratureRule2.degreeOfExactness();

  // Getting the points
  const QuadraturePoint* point1 = rQuadratureRule1.quadraturePoints();
  const QuadraturePoint* point2 = rQuadratureRule2.quadraturePoints();

  // Fill QuadraturePoint
  QuadraturePoint* points = new QuadraturePoint[m_numQuadraturePoint[0]];
  for (int i = 0; i < m_numQuadraturePoint[1]; ++i) {
    for (int j = 0; j < m_numQuadraturePoint[2]; ++j) {
      points[i * m_numQuadraturePoint[2] + j] = QuadraturePoint(point1[i].x(), point1[i].y(), point2[j].x(), Coefficient * point1[i].weight() * point2[j].weight());
    }
  }
  m_point = points;
}

/***********************************************************************************/
/***********************************************************************************/

QuadratureRule& QuadratureRule::operator=(const QuadratureRule& QR)
{
  m_point = QR.m_point;
  m_shape = QR.m_shape;
  m_name = QR.m_name;
  m_numQuadraturePoint = QR.m_numQuadraturePoint;
  m_degreeOfExactness = QR.m_degreeOfExactness;

  return *this;
}

/***********************************************************************************/
/***********************************************************************************/

void QuadratureRule::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "Quadrature Rule:" << std::endl;
    c << " name: " << m_name << std::endl;
    c << " shape:" <<  m_shape->name() << std::endl;
    c << " degree of exactness: " << m_degreeOfExactness[0] << std::endl;
    c << " numQuadraturePoint: " << m_numQuadraturePoint[0] << std::endl;
    c << " Points: \n";
    for ( int i = 0; i < m_numQuadraturePoint[0]; i++ ) {
      std::cout << "  ";
      m_point[ i ].print(verbose,c);
      std::cout << std::endl;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

ListOfQuadratureRule::ListOfQuadratureRule(
  const std::string name,
  const int numQuadratureRule,
  const QuadratureRule* quadratureRule
  ): m_name(name),
     m_quadratureRule(quadratureRule)
{
  m_numQuadratureRule[0] = numQuadratureRule;
  m_maxDegreeOfExactness[0] = 0;
  for(int i=0; i<m_numQuadratureRule[0]; i++) {
    if(m_quadratureRule[i].degreeOfExactness()>m_maxDegreeOfExactness[0]) m_maxDegreeOfExactness[0] = m_quadratureRule[i].degreeOfExactness();
  }
  m_quadratureByDegreeOfExactness = new int[m_maxDegreeOfExactness[0]+1];
  int iquad=0;
  for(int i=0; i<m_maxDegreeOfExactness[0] + 1; i++) {
    if(m_quadratureRule[iquad].degreeOfExactness()>=i) {
      m_quadratureByDegreeOfExactness[i] = iquad;
    } else {
      iquad++;
      m_quadratureByDegreeOfExactness[i] = iquad;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

ListOfQuadratureRule::ListOfQuadratureRule(
  const std::string name,
  const ListOfQuadratureRule& listQuadratureRule1,
  const ListOfQuadratureRule& listQuadratureRule2,
  const double Coefficient
  ) : ListOfQuadratureRule(name, listQuadratureRule1.getNumQuadratureRule(), listQuadratureRule1.getQuadratureRule())
{
  // Reassign value
  m_numQuadratureRule = new int[3];
  m_numQuadratureRule[1] = listQuadratureRule1.getNumQuadratureRule();
  m_numQuadratureRule[2] = listQuadratureRule2.getNumQuadratureRule();
  m_numQuadratureRule[0] = m_numQuadratureRule[1]  * m_numQuadratureRule[2] ;

  const QuadratureRule* quadratureRule1 = listQuadratureRule1.getQuadratureRule();
  const QuadratureRule* quadratureRule2 = listQuadratureRule2.getQuadratureRule();

  // Update m_quadratureRule with const_cast
  QuadratureRule* quad_rule = new QuadratureRule[m_numQuadratureRule[0]];
  for (int i = 0; i < m_numQuadratureRule[1]; ++i) {
    const auto& r_name_1 = quadratureRule1[i].refShape().name();
    for (int j = 0; j < m_numQuadratureRule[2] ; ++j) {
      const auto& r_name_2 = quadratureRule2[j].refShape().name();
      if (r_name_1 == "QUADRILATERAL" &&  r_name_2== "SEGMENT") {
        quad_rule[i * m_numQuadratureRule[2] + j] = QuadratureRule(quadratureRule1[i], quadratureRule2[j], name, HEXAHEDRON, Coefficient);
      } else if (r_name_1 == "TRIANGLE" && r_name_2 == "SEGMENT") {
        quad_rule[i * m_numQuadratureRule[2] + j] = QuadratureRule(quadratureRule1[i], quadratureRule2[j], name, PRISM, Coefficient);
      } else {
        FEL_ERROR("Combination not defined: " + r_name_1 + " " + r_name_2);
      }
    }
  }
  m_quadratureRule = quad_rule;

  // Degree of exactness of each value
  m_maxDegreeOfExactness = new int[2];
  m_maxDegreeOfExactness[0] = listQuadratureRule1.getMaxDegreeOfExactness();
  m_maxDegreeOfExactness[1] = listQuadratureRule2.getMaxDegreeOfExactness();

  // Append values
  m_quadratureByDegreeOfExactness = new int[m_maxDegreeOfExactness[0]+m_maxDegreeOfExactness[1]+2];
  const int* quad_exact_1 = listQuadratureRule1.getquadratureByDegreeOfExactness();
  const int* quad_exact_2 = listQuadratureRule2.getquadratureByDegreeOfExactness();
  for(int i=0; i<m_maxDegreeOfExactness[0] + 1; i++) {
    m_quadratureByDegreeOfExactness[i] = quad_exact_1[i];
  }
  for(int i=0; i<m_maxDegreeOfExactness[1] + 1; i++) {
    m_quadratureByDegreeOfExactness[i + m_maxDegreeOfExactness[0] + 1] = quad_exact_2[i];
  }
}

/***********************************************************************************/
/***********************************************************************************/

const QuadratureRule& ListOfQuadratureRule::quadratureRuleByExactness(const DegreeOfExactness deg) const 
{
  FEL_CHECK(deg<=m_maxDegreeOfExactness[0],"Maximum degree of exactness is " << m_maxDegreeOfExactness[0] );
  return m_quadratureRule[m_quadratureByDegreeOfExactness[deg]];
}

/***********************************************************************************/
/***********************************************************************************/

const QuadratureRule& ListOfQuadratureRule::quadratureRuleByExactness(const DegreeOfExactness degi, const DegreeOfExactness degj) const 
{
  FEL_CHECK(degi<=m_maxDegreeOfExactness[0],"Maximum degree of exactness 0 is " << m_maxDegreeOfExactness[0] );
  FEL_CHECK(degj<=m_maxDegreeOfExactness[1],"Maximum degree of exactness 1 is " << m_maxDegreeOfExactness[1] );
  const int index_i = m_quadratureByDegreeOfExactness[degi];
  const int index_j = m_quadratureByDegreeOfExactness[degj + m_maxDegreeOfExactness[0] + 1];
  return m_quadratureRule[index_i * m_numQuadratureRule[2] + index_j];
}

/***********************************************************************************/
/***********************************************************************************/

void ListOfQuadratureRule::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "List of Quadrature Rules:" << std::endl;
    c << " name: " << m_name << std::endl;
    c << " maximum degree of exactness: " << m_maxDegreeOfExactness << std::endl;
    c << " Quadrature rules of the list by degree of exactness:" << std::endl;
    for(int i=0; i<=m_maxDegreeOfExactness[0]; i++) {
      int iquad = m_quadratureByDegreeOfExactness[i];
      std::cout << "  Degree " << i << ": " <<  m_quadratureRule[iquad].name()
            << "(degree of exactness: " <<   m_quadratureRule[iquad].degreeOfExactness() << ")" << std::endl;
    }
    c << " Details of the Quadrature rules of the list:" << std::endl;
    for(int i=0; i<m_numQuadratureRule[0]; i++) {
      std::cout << " ";
      m_quadratureRule[i].print(verbose,c);
    }
  }
}

}
