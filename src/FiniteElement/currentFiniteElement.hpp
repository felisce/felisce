//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _CURRENT_FINITE_ELEMENT_H
#define _CURRENT_FINITE_ELEMENT_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"
#include "FiniteElement/curvilinearFiniteElement.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
/**
 * \class CurrentFiniteElement
 * \authors J-F. Gerbeau
 * \brief Class implementing a current finite element
 */
class CurrentFiniteElement:
    public CurBaseFiniteElement
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of CurrentFiniteElement
  FELISCE_CLASS_POINTER_DEFINITION(CurrentFiniteElement);

  ///@}
  ///@name Life Cycle
  ///@{

  /// constructors (vector of DegreeOfExactness)
  CurrentFiniteElement(
    const RefElement& refEle,
    const GeoElement& geoEle,
    const std::vector<DegreeOfExactness>& degOfExactness
    );

  ///  constructors
  CurrentFiniteElement(const RefElement& refEle,const GeoElement& geoEle,const DegreeOfExactness& degOfExactness);

  // Destructor
  ~CurrentFiniteElement() override;

  ///  Copy constructor, to avoid warning due to user-declared destructor.
  CurrentFiniteElement(const CurrentFiniteElement&) = default;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //-----------
  // updates:
  //-----------
  ///  minimal update: we just identify the id of the current element
  void update(const int id,const std::vector<Point*>& point) override;

  ///  compute the arrays meas, weightDet, jacobian on the current element
  void updateMeas(const int id,const std::vector<Point*>& point) override;

  /*!
    compute the arrays meas, weightDet, jacobian and quadPt
    on the current element
    */
  void updateMeasQuadPt(const int id,const std::vector<Point*>& point);
  /*!
    compute the arrays meas, weightDet, jacobian,
    tInvJac, phiDer on the current element
    */
  void updateFirstDeriv(const int id,const std::vector<Point*>& point);
  /*!
    to compute the basis function on some new quadrature point
  */
  void updateBasisAndFirstDeriv(const int id, const std::vector<Point*>& point);

  /*!
    to integrate on a sub element only
  */
  void updateSubElementFirstDeriv(const int id, const std::vector<Point*>& ptElem, const std::vector<Point*>& ptSubElem, CurBaseFiniteElement *feSurf = nullptr);

  /*!
    it calls the standard updateFirstDeriv, but it evaluates the basis functions and their derivatives also
    on the dofs, not only in the quad points
  */
  void updateFirstDerivOnDofs(const int id,const std::vector<Point*>& point);

  void allocateOnDofsDataStructures();

  void identifyLocBDDof(const CurvilinearFiniteElement& curvFe, std::vector<int>& map) const;

  ///@}
  ///@name Access
  ///@{

  inline int indexQuadPoint(const int iBlockBd) const {
    return m_indexQuadPoint[iBlockBd];
  }

  ///  return true if the first derivatives have been updated
  inline bool hasFirstDeriv() const {
    return m_hasFirstDeriv;
  }
  ///  return true if the second derivatives have been updated
  inline bool hasSecondDeriv() const {
    return m_hasSecondDeriv;
  }

  ///@}
  ///@name Member Variables
  ///@{

  ///  dPhi[ig](icoor,jdof) (computed as m_jacobian^{-1}*m_dPhiRef) (in the current FE)
  std::vector<UBlasMatrix> dPhi;

  std::vector<UBlasMatrix> dPhiOnDofs;

  std::vector<UBlasVector> phiOnDofs;

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  bool m_hasFirstDeriv = false;
  bool m_hasSecondDeriv = false;

  ///  The transpose of the Jacobian of the geometrical mapping: m_jacobian[ig](icoor,jcoor) derivative with respect to x_icoor of the component jcoor (in the current FE)
  std::vector<UBlasMatrix> m_jacobian;

  ///  index of beginning/end of the quad points (0->m_numQuadraturePoint-1 : internal quad points)
  std::vector<int> m_indexQuadPoint;

  bool m_phiDPhiOnDofs = false;
  std::vector<UBlasMatrix> m_dPhiRefOnDofs;
  std::vector<UBlasVector> m_phiGeoOnDofs;
  std::vector<UBlasMatrix> m_dPhiGeoOnDofs;
  std::vector<UBlasMatrix> m_jacobianOnDofs;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///  In the sequel: "iBlockBd": internal quad points are in the 0 block
  ///  (CurrFEWithBd: boundary quad points in the >0 blocks (face f in block f+1))
  ///  by default: compute only for the internal quad points.
  //
  ///  update the points and compute the Jacobian matrix
  void computeJacobian(const int iBlockBd=0,const std::size_t numBlockBd=1);

  ///  compute the determinant of the Jacobian (call first computeJacobian)
  void computeMeas() override;

  ///  compute the determinant of the Jacobian and the derivatives of the basis functions (call first computeJacobian)
  void computeMeasDer(const int iBlockBd=0,const std::size_t numBlockBd=1);

  ///  compute the norm of the inverse of the transpose of the jacobian times the normal to the element. Compute also the derivatives.
  void computePiolaMeasDer(const int iBlockBd,const std::size_t numBlockBd, const std::vector<double>& normal);

  void computeJacobianOnDofs();

  void computeDerOnDofs();

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif
