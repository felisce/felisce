//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau and V. Martin
//

// System includes
#include <cmath>

// External includes

// Project includes
#include "FiniteElement/curBaseFiniteElement.hpp"
#include "Tools/math_utilities.hpp"

namespace felisce
{
CurBaseFiniteElement::CurBaseFiniteElement(
  const RefElement& refEle,
  const GeoElement& geoEle,
  const std::vector<DegreeOfExactness>& degOfExactness
  ):
  m_refEle(refEle),
  m_geoEle(geoEle),
  m_numDof(m_refEle.numDOF()),
  m_numRefCoor(m_geoEle.numCoor()),
  m_numCoor(m_numRefCoor), // will be changed by curvilinearFE
  m_numPoint(m_geoEle.numPoint())
{
  switch (m_geoEle.shape().typeShape())
  {
    case Node:
      m_quadratureRule = &listQuadratureRuleNode.quadratureRuleByExactness(degOfExactness[0]);
      break;
    case Segment:
      m_quadratureRule = &listQuadratureRuleSegment.quadratureRuleByExactness(degOfExactness[0]);
      break;
    case Triangle:
       // if (FelisceParam::instance().typeOfShellModel=="MITC3" || FelisceParam::instance().typeOfShellModel=="MITC3+")
       //   m_quadratureRule = &quadratureRuleMitc3;
       // else
        m_quadratureRule = &listQuadratureRuleTriangle.quadratureRuleByExactness(degOfExactness[0]);
      break;
    case Quadrilateral:
      m_quadratureRule = &listQuadratureRuleQuadrilateral.quadratureRuleByExactness(degOfExactness[0]);
      break;
    case Tetrahedron:
      m_quadratureRule = &listQuadratureRuleTetrahedron.quadratureRuleByExactness(degOfExactness[0]);
      break;
    case Hexahedron:
      if (degOfExactness.size() == 1) {
        m_quadratureRule = &listQuadratureRuleHexahedron.quadratureRuleByExactness(degOfExactness[0]);
      } else {
        m_quadratureRule = &listQuadratureRuleHexahedronCombined.quadratureRuleByExactness(degOfExactness[1], degOfExactness[2]);
      }
      break;
    case Prism:
      if (degOfExactness.size() == 1) {
        m_quadratureRule = &listQuadratureRulePrism.quadratureRuleByExactness(degOfExactness[0]);
      } else {
        m_quadratureRule = &listQuadratureRulePrismCombined.quadratureRuleByExactness(degOfExactness[1], degOfExactness[2]);
      }
      break;
    default:
      FEL_ERROR("There is no quadrature rule list defined for this element shape");
      break;
  }
  m_quadratureRule->print(0);
  // TODO: Replace variable with method and call
  m_numQuadraturePoint = m_quadratureRule->numQuadraturePoint();

  m_point.resize(m_numPoint,m_numCoor); // can be MODIFIED if used by curvilinearFE
  currentQuadPoint.resize(m_numQuadraturePoint);
  m_dPhiRef.resize(m_numQuadraturePoint );
  m_phiGeo.resize(m_numQuadraturePoint);
  m_dPhiGeo.resize(m_numQuadraturePoint);
  m_meas.resize(m_numQuadraturePoint);
  phi.resize(m_numQuadraturePoint);
  //dPhi.resize(m_numQuadraturePoint);
  weightMeas.resize(m_numQuadraturePoint);

  for(int ig=0; ig<m_numQuadraturePoint; ig++) {
    m_dPhiRef[ig].resize(m_numRefCoor,m_numDof);
    m_phiGeo[ig].resize(m_numPoint);
    m_dPhiGeo[ig].resize(m_numRefCoor,m_numPoint);
    phi[ig].resize(m_numDof);
    //dPhi[ig].resize(m_numRefCoor,m_numDof);

    const QuadraturePoint& quadPt = m_quadratureRule->quadraturePoint(ig);
    for(int i=0; i<m_numDof; i++ ) {
      phi[ig](i) = m_refEle.basisFunction().phi(i, quadPt);
      for(int icoor=0; icoor<m_numRefCoor; icoor++ ) {
        m_dPhiRef[ig](icoor,i) = m_refEle.basisFunction().dPhi(i,icoor, quadPt);
      }
    }
    for(int k=0; k<m_numPoint; k++) {
      m_phiGeo[ig](k)= m_geoEle.basisFunction().phi(k, quadPt);
      for(int icoor=0; icoor<m_numRefCoor; icoor++) {
        m_dPhiGeo[ig](icoor,k) = m_geoEle.basisFunction().dPhi(k,icoor, quadPt);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

CurBaseFiniteElement::CurBaseFiniteElement(
  const RefElement& refEle,
  const GeoElement& geoEle,
  const DegreeOfExactness& degOfExactness
  ) :  CurBaseFiniteElement(refEle, geoEle, std::vector<DegreeOfExactness>(1, degOfExactness))
{
}

/***********************************************************************************/
/***********************************************************************************/

CurBaseFiniteElement::~CurBaseFiniteElement()
{
  m_point.clear();
  currentQuadPoint.clear();
  m_dPhiRef.clear();
  m_phiGeo.clear();
  m_dPhiGeo.clear();
  m_meas.clear();
  phi.clear();
  //dPhi.clear();
  weightMeas.clear();
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::placeholder() const
{
  // Do nothing! Exists just to avoid emitting vtables in every translation unit.
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::computeCurrentQuadraturePoint()
{
  FEL_ASSERT( hasPoint() );

  for (int ig = 0; ig < m_numQuadraturePoint; ig++) {
    coorMap(currentQuadPoint[ig],m_quadratureRule->quadraturePoint(ig));
  }
  m_hasQuadPtCoor = true;
}

/***********************************************************************************/
/***********************************************************************************/

double CurBaseFiniteElement::measure() const
{
  FEL_ASSERT(m_hasMeas);
  double meas = 0.;
  // The measure must not be computed from the boundary (keep m_numQuadraturePoint)!!
  for ( int ig = 0; ig <m_numQuadraturePoint; ig++ ) meas +=weightMeas( ig );
  return meas;
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::coorMap(Point& pt, const Point& refpt) const
{
  pt.clear();
  for(int icoor=0; icoor<m_numCoor; icoor++) {
    for (int i=0; i<m_numPoint; i++) {
      pt[icoor] += m_point( i, icoor ) * m_geoEle.basisFunction().phi( i, refpt );
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

double CurBaseFiniteElement::diameter() const
{
  double max = 0.;
  double dist = 0.;
  for (int i=0; i<m_numPoint; i++) {
    for (int j=0; j<i; j++) {
      for(int icoor=0; icoor<m_numCoor; icoor++) {
        const double dp = m_point(i,icoor)-m_point(j,icoor);
        dist += dp * dp;
      }
      dist = std::sqrt(dist);
      if ( dist > max )
        max = dist;
      dist = 0.;
    }
  }
  return max;
}

/***********************************************************************************/
/***********************************************************************************/

double CurBaseFiniteElement::measOfSegment(felInt id1, felInt id2) const
{
  double dist=0;
  for(int icoor=0; icoor<m_numCoor; icoor++) {
    const double dp = m_point(id1,icoor)-m_point(id2,icoor);
    dist += dp * dp;
  }
  dist = std::sqrt(dist);
  return dist;
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::updatePoint(const std::vector<Point*>& point)
{
  for (int i=0; i<m_numPoint; i++) {
    for(int icoor=0; icoor<m_numCoor; icoor++) {
      m_point(i,icoor) = point[i]->coor(icoor);
    }
  }
  m_hasPoint = true;
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::updatePoint(const UBlasMatrix& point, const std::vector<int>& ipt)
{
  for (int i=0; i<m_numPoint; i++) {
    for(int icoor=0; icoor<m_numCoor; icoor++) {
      m_point(i,icoor) = point(ipt[i],icoor);
    }
  }
  m_hasPoint = true;
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::update(const int id,const std::vector<Point*>& point)
{
  updatePoint(point);
  m_hasMeas = false;
  m_hasQuadPtCoor = false;
  m_currentId = id;
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::updateBasisWithQuadPoint(const std::vector<Point>& point)
{
  FEL_CHECK(m_numQuadraturePoint >= (felInt)point.size(), "Error when updating the basis function with new quadrature point: not enough points given");

  for(std::size_t ig=0; ig<point.size(); ig++) {
    for(int i=0; i<m_numDof; i++) {
      phi[ig](i) = m_refEle.basisFunction().phi(i, point[ig]);
      for(int icoor=0; icoor<m_numRefCoor; icoor++) {
        m_dPhiRef[ig](icoor, i) = m_refEle.basisFunction().dPhi(i, icoor, point[ig]);
      }
    }
    for(int k=0; k<m_numPoint; k++) {
      m_phiGeo[ig](k) = m_geoEle.basisFunction().phi(k, point[ig]);
      for(int icoor=0; icoor<m_numRefCoor; icoor++) {
        m_dPhiGeo[ig](icoor, k) = m_geoEle.basisFunction().dPhi(k, icoor, point[ig]);
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::mapPointInReferenceElement(const int /* id */, const std::vector<Point*>& ptElem, const Point& inPoint, Point& outPoint)
{
  // Find the coordinate of the input points in the reference element of the element
  switch(m_geoEle.shape().typeShape()) {
    case Quadrilateral: {
      // square
      outPoint.x() = 2.*(inPoint.x() - ptElem[0]->x())/(ptElem[2]->x() - ptElem[0]->x()) - 1.;
      outPoint.y() = 2.*(inPoint.y() - ptElem[0]->y())/(ptElem[2]->y() - ptElem[0]->y()) - 1.;
      outPoint.z() = 0.;
      break;
    }
    case Triangle: {
      // Yriangle
      // based on the identity : (a x b) . (c x d) = (a.c)(b.d) - (a.d)(b.c)
      const Point u(*ptElem[1] - *ptElem[0]);
      const Point v(*ptElem[2] - *ptElem[0]);
      const Point z(   inPoint - *ptElem[0]);

      // Triangle area^2 = 1/4 * (u x v).(u x v)  (actually 1./(4*area^2) )
      const double InvDet = 1./( (u*u) * (v*v) - std::pow(u*v,2) );

      // Coordinate x = 1/4 * (z x v).(u x v) / area^2
      outPoint.x() = ( (z*u) * (v*v) - (z*v) * (v*u) ) * InvDet;

      // Coordinate y = 1/4 * (u x z).(u x v) / area^2
      outPoint.y() = ( (u*u) * (z*v) - (u*v) * (z*u) ) * InvDet;

      // Coordinate z = 0
      outPoint.z() = 0.;

      break;
    }
    case Tetrahedron: {
      UBlasMatrix FLU(m_numRefCoor,m_numRefCoor);
      UBlasMatrix inv_FLU(m_numRefCoor,m_numRefCoor);
      UBlasVector refPoint(m_numRefCoor);
      double det;

      // Filling the matrix
      for(int indx = 0; indx < m_numRefCoor ; ++indx){
        FLU(0,indx) = ptElem[indx+1]->x() - ptElem[0]->x();
        FLU(1,indx) = ptElem[indx+1]->y() - ptElem[0]->y();
        FLU(2,indx) = ptElem[indx+1]->z() - ptElem[0]->z();
      }

      MathUtilities::InvertMatrix(FLU, inv_FLU, det);

      // Filling the rhs
      refPoint[0] = inPoint.x() - ptElem[0]->x();
      refPoint[1] = inPoint.y() - ptElem[0]->y();
      refPoint[2] = inPoint.z() - ptElem[0]->z();

      refPoint = prod(inv_FLU,refPoint);

      outPoint.x() = refPoint[0];
      outPoint.y() = refPoint[1];
      outPoint.z() = refPoint[2];

      break;
    }
    case NullShape:
    case Node:
    case Segment:
    case Hexahedron:
    case Prism:
    case Pyramid: {
      FEL_ERROR("The transformation is not implemented for this type of mesh cells");
      break;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::mapPointInReferenceElement(const int id, std::vector<Point>& ptElem, const Point& inPoint, Point& outPoint)
{
  std::vector<Point*> tmp_vec(ptElem.size(), nullptr);
  for (std::size_t i = 0; i < ptElem.size(); ++i)
    tmp_vec[i] = &ptElem[i];

  mapPointInReferenceElement(id, tmp_vec, inPoint, outPoint);
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::m_printBase(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << " RefElement:          " << m_refEle.name() << "\n";
    c << " GeoElement:          " << m_geoEle.name() << "\n";
    c << " QuadratureRule:      " << m_quadratureRule->name() << "\n";
    c << " numDof:       " << m_numDof << "\n";
    c << " numQuadPoint: " << m_numQuadraturePoint << "\n";
    c << " numRefCoor:   " << m_numRefCoor << "\n";
    c << " numCoor:      " << m_numCoor << "\n";
    c << " numPoint:     " << m_numPoint << "\n";
    c << " currentID:    " << m_currentId << "\n";
    /*
    if( m_hasPoint && verbose > 1 ) c << " point = " << m_point << "\n";
    if( m_hasMeas && verbose > 1 )  c << " meas   = " << measure() << "\n";
    if( verbose > 2 ) {
    if ( theNumQuadPointTotal != m_numQuadraturePoint ) {
    c << " The numQuadPoint=" << m_numQuadraturePoint << " first are internal. The others live on the boundary elements of the element.\n";}
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " phi[ig=" <<ig<< "] = " << phi[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " phiGeo[ig=" <<ig<< "] = " << m_phiGeo[ig] << "\n";
      }
    }
    if( m_hasFirstDeriv && verbose > 2 ) {
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " dphi[ig=" <<ig<< "] = " << dPhi[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " dphiGeo[ig=" <<ig<< "] = " << m_dPhiGeo[ig] << "\n";
      }
    }
    if( verbose > 2 ) {
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " dphiRef[ig=" <<ig<< "] = " << m_dPhiRef[ig] << "\n";
      }
    }
    if( m_hasFirstDeriv && verbose > 2 ) {
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " jacobian[ig=" <<ig<< "] = " << m_jacobian[ig] << "\n";
      }
    }
    if( m_hasQuadPtCoor && verbose > 2 ) {
      for (int ig = 0;ig <theNumQuadPointTotal;ig++ ){
        c << " currentQuadPoint[ig=" <<ig<< "] : "; currentQuadPoint[ig].print(verbose,c);
      }
    }
    */
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurBaseFiniteElement::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "CurBaseFiniteElement: " << "\n";
    m_printBase(verbose, c);
    std::cout << std::endl;
  }
}
}
