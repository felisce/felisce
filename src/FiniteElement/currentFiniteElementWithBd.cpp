//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    JJ-F. Gerbeaun & V.Martin
//

// System includes

// External includes

// Project includes
#include "currentFiniteElementWithBd.hpp"

namespace felisce {

CurrentFiniteElementWithBd::CurrentFiniteElementWithBd(
  const RefElement& refEle,
  const GeoElement& geoEle,
  const std::vector<DegreeOfExactness>& degOfExactness,
  const DegreeOfExactness& degOfExactnessBd
  ): CurrentFiniteElement(refEle,geoEle,degOfExactness),
      m_numBdEle( geoEle.numBdEle() )
{

  FEL_ASSERT( ( m_numBdEle == geoEle.numFace() && numRefCoor() == 3 ) || //3D type of element (tetra, prisms..)
              ( m_numBdEle == geoEle.numEdge() && numRefCoor() == 2 ) || //2D (tria,quadrangles)
              ( m_numBdEle == 2 && numRefCoor() == 1 ) ); //1D (segment)
  //--------------------
  ///  1/ build the curvilinear bd elements (for the measure, normal of the faces)
  //--------------------
  m_bdEle = new CurvilinearFiniteElement* [m_numBdEle];

  ///  NOTE: I construct a CurvilinearFE for *each* bd element of the element
  ///        we could also define m_numBdEle=1 in the constructor, if only 1 face is needed... vm 07/2011
  for(int ibd=0; ibd<m_numBdEle; ibd++ ) {
    m_bdEle[ibd] = new CurvilinearFiniteElement( CurrentFiniteElement::refEle().boundaryRefElement(ibd),
        CurrentFiniteElement::geoEle().boundaryGeoElement(ibd),
        degOfExactnessBd );
  }

  //--------------------
  ///  2/ compute the boundary quadrature points on the reference element (from the bd ref element)
  //--------------------
  ///  coordinates of the points on the Bd element
  UBlasMatrix pointCoorOnBd; //one could also save this info, if necessary...

  m_pointBdEle.resize(m_numBdEle);
  m_numPointBdEle.resize(m_numBdEle);
  m_refQuadPointBdEle.resize(m_numBdEle);
  m_numQuadPointBdEle.resize(m_numBdEle);

  ///  compute the image of the quadrature points (that live on the boundary ref element)
  ///  onto the boundary elements of the reference element.
  /// (eg. : image of the quad points of a ref triangle onto the triangles of the ref tetra)
  for ( int ibd = 0; ibd <m_numBdEle; ibd++ ) {
    m_numPointBdEle[ibd] = bdEle(ibd).geoEle().numPoint();

    // Get the points of the boundary element from the reference element
    m_pointBdEle[ibd].resize( m_numPointBdEle[ibd] );
    pointCoorOnBd.resize(m_numPointBdEle[ibd],m_numCoor);
    for ( int iptBd = 0; iptBd < m_numPointBdEle[ibd] ; iptBd++ ) {
      m_pointBdEle[ibd][ iptBd ] = CurrentFiniteElement::geoEle().pointOfBdEle( ibd, iptBd );

      // update the coordinates of the reference element
      for(int icoor=0; icoor<m_numCoor; icoor++) {
        pointCoorOnBd(iptBd,icoor) = CurrentFiniteElement::geoEle().pointCoor(m_pointBdEle[ibd][iptBd],icoor);
      }
    }

    /*
    std::cout << "pointBdEle (" << m_pointBdEle[ibd].size() << ")" << std::endl << "[";
    for ( int iptBd = 0; iptBd < m_numPointBdEle[ibd] ;iptBd++ ) {std::cout << m_pointBdEle[ibd][iptBd] << ";";}
    std::cout << "]" << std::endl;
    std::cout << "pointCoorOnBd" << std::endl;
    std::cout << pointCoorOnBd << std::endl;
    */

    m_numQuadPointBdEle[ibd] = bdEle(ibd).numQuadraturePoint();
    //std::cout << "numQuadraturePointBd=" << m_numQuadPointBdEle[ibd] <<std::endl;

    std::vector<UBlasVector> phiGeoBd(m_numQuadPointBdEle[ibd]);
    m_refQuadPointBdEle[ibd] = std::vector<Point>(m_numQuadPointBdEle[ibd]);

    for(int ig=0; ig< m_numQuadPointBdEle[ibd]; ig++) {
      phiGeoBd[ig].resize(m_numPointBdEle[ibd]);
      const QuadraturePoint& quadPtRefBd = bdEle(ibd).quadratureRule().quadraturePoint(ig);
      //std::cout << "quadPtRefBd : " ; quadPtRefBd.print(10); std::cout << std::endl;
      ///  get the shape function of the boundary element at the Boundary reference quadrature points
      for(int iptBd=0; iptBd < m_numPointBdEle[ibd] ; iptBd++ ) {
        phiGeoBd[ig](iptBd) = bdEle(ibd).geoEle().basisFunction().phi(iptBd, quadPtRefBd);
      }
      //std::cout << "phiGeoBd[ig=" << ig << "] = " << *phiGeoBd[ig] << std::endl;

      ///  compute the quad points onto the boundaries of the ref element
      ///  ("coorMap a la main" : bd points not stored)
      ///  phiGeoBd[ig] contains "weights" you use them to sum the point of the reference element
      ///  of the boundary (that have m_refCoor -1 coordinate) you obtain back quadPtRefBd.
      ///  if you use them to weigth the corresponding point of the boundary element in the
      ///  the reference space of volume element (that have m_refCoor coordinate) you obtain the same
      ///  point, but mapped from the bd reference space to the vol reference space.
      Point& theQuadPointBd = m_refQuadPointBdEle[ibd][ig];
      theQuadPointBd.x() = theQuadPointBd.y() =  theQuadPointBd.z() = 0.;
      for(int icoor=0; icoor<m_numCoor; icoor++) {
        for(int iptBd=0; iptBd < m_numPointBdEle[ibd] ; iptBd++ ) {
          theQuadPointBd[icoor] += pointCoorOnBd( iptBd, icoor ) * phiGeoBd[ig](iptBd);
        }
      }
      //std::cout << "currentQuadPointBd[ig=" << ig << "] = "; m_refQuadPointBdEle[ibd][ig].print(10);
    }
  }

  //--------------------
  ///  3/ compute the indirections for accessing the quad points on the boundary
  //--------------------
  m_indexQuadPoint.resize(m_numBdEle+2);     // indexQuadPoint[0]=0 and indexQuadPoint[1]=m_numQuadraturePoint;
  for ( int ibd = 0; ibd <m_numBdEle; ibd++ ) {
    m_indexQuadPoint[ibd+2] = m_indexQuadPoint[ibd+1] + m_numQuadPointBdEle[ibd];
  }

  //for ( unsigned int ii = 0;ii < m_indexQuadPoint.size();ii++ ){
  //std::cout << "m_indexQuadPoint[ii=" << ii << "] = " << m_indexQuadPoint[ii] << std::endl;}

  m_numQuadraturePointInternAndBd = m_indexQuadPoint.back();
  //std::cout << "numQuadraturePointInternBd = " << m_numQuadraturePointInternAndBd << std::endl;

  m_dPhiRef.resize(m_numQuadraturePointInternAndBd );
  m_jacobian.resize(m_numQuadraturePointInternAndBd);
  m_phiGeo.resize(m_numQuadraturePointInternAndBd);
  m_dPhiGeo.resize(m_numQuadraturePointInternAndBd);
  phi.resize(m_numQuadraturePointInternAndBd);
  dPhi.resize(m_numQuadraturePointInternAndBd);
  currentQuadPoint.resize(m_numQuadraturePointInternAndBd);

  //--------------------
  ///  4/  compute phi and its derivatives at the boundary quadrature points on the reference element
  //--------------------
  for ( int ibd = 0; ibd <m_numBdEle; ibd++ ) {
    //std::cout << "  ibd=" << ibd << std::endl;
    for(int ilocg=0; ilocg< m_numQuadPointBdEle[ibd]; ilocg++) {
      int ig = ilocg+m_indexQuadPoint[ibd+1];
      //std::cout << "    ilocg=" << ilocg << " ig=" << ig << std::endl;
      m_dPhiRef[ig].resize(m_numRefCoor,m_numDof);
      m_jacobian[ig].resize(m_numCoor,m_numCoor);
      m_phiGeo[ig].resize(m_numPoint);
      m_dPhiGeo[ig].resize(m_numRefCoor,m_numPoint);
      phi[ig].resize(m_numDof);
      dPhi[ig].resize(m_numRefCoor,m_numDof);
      //std::cout << "    resize done" << std::endl << std::flush;

      Point& theQuadPointBd = m_refQuadPointBdEle[ibd][ilocg];

      //std::cout << "    setting phi/dphiref" << std::endl << std::flush;
      for(int i=0; i<m_numDof; i++ ) {
        phi[ig](i) = m_refEle.basisFunction().phi(i, theQuadPointBd);
        for(int icoor=0; icoor<m_numRefCoor; icoor++ ) {
          m_dPhiRef[ig](icoor,i) = m_refEle.basisFunction().dPhi(i,icoor, theQuadPointBd);
        }
      }
      //std::cout << "    setting phiGeo/dphiGeo" << std::endl << std::flush;
      for(int k=0; k<m_numPoint; k++) {
        m_phiGeo[ig](k)= m_geoEle.basisFunction().phi(k, theQuadPointBd);
        for(int icoor=0; icoor<m_numRefCoor; icoor++) {
          m_dPhiGeo[ig](icoor,k) = m_geoEle.basisFunction().dPhi(k,icoor, theQuadPointBd);
        }
      }
    }
  }
  //std::cout << "Constructor CurrentFiniteElementWithBd done" << std::endl;
}

/***********************************************************************************/
/***********************************************************************************/

CurrentFiniteElementWithBd::CurrentFiniteElementWithBd(
    const RefElement& refEle,
    const GeoElement& geoEle,
    const DegreeOfExactness& degOfExactness,
    const DegreeOfExactness& degOfExactnessBd
    ): CurrentFiniteElementWithBd(refEle,geoEle,std::vector<DegreeOfExactness>(1, degOfExactness), degOfExactnessBd)
{

}

/***********************************************************************************/
/***********************************************************************************/

CurrentFiniteElementWithBd::~CurrentFiniteElementWithBd()
{
  for(int ibd=0; ibd<m_numBdEle; ibd++ ) {
    delete m_bdEle[ibd];
  }
  delete [] m_bdEle;
}

/***********************************************************************************/
/***********************************************************************************/

const CurvilinearFiniteElement &  CurrentFiniteElementWithBd::bdEle(const std::size_t ibd ) const
{
  FEL_ASSERT(ibd < static_cast<std::size_t>(m_numBdEle) );
  const CurvilinearFiniteElement* retPtr = m_bdEle[ ibd ];
  FEL_ASSERT(retPtr);
  return *retPtr;
}

/***********************************************************************************/
/***********************************************************************************/

CurvilinearFiniteElement &  CurrentFiniteElementWithBd::bdEle(const std::size_t ibd )
{
  FEL_ASSERT(ibd < static_cast<std::size_t>(m_numBdEle) );
  CurvilinearFiniteElement* retPtr = m_bdEle[ ibd ];
  FEL_ASSERT(retPtr);
  return *retPtr;
}

/***********************************************************************************/
/***********************************************************************************/

CurvilinearFiniteElement* CurrentFiniteElementWithBd::ptrBdEle(const std::size_t ibd ) const
{
  FEL_ASSERT(ibd < static_cast<std::size_t>(m_numBdEle) );
  return m_bdEle[ ibd ];
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElementWithBd::updateBdMeas()
{
  FEL_ASSERT( hasPoint() );  ///  you need a minimum info on the element!

  for ( int ibd = 0; ibd <m_numBdEle; ibd++ ) {
    //note: what to do of the bd elem id?? (default: local numbering in bd elem) vm 08/2011
    CurvilinearFiniteElement* fe = m_bdEle[ibd];
    FEL_ASSERT(fe);
    fe->updateMeas(ibd, m_point, m_pointBdEle[ibd]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElementWithBd::updateBdMeasNormal()
{
  FEL_ASSERT( hasPoint() );  ///  you need a minimum info on the element!

  for ( int ibd = 0; ibd <m_numBdEle; ibd++ ) {
    //note: what to do of the bd elem id?? (default: local numbering in bd elem) vm 08/2011
    CurvilinearFiniteElement* fe = m_bdEle[ibd];
    FEL_ASSERT(fe);
    fe->updateMeasNormal(ibd, m_point, m_pointBdEle[ibd]);
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElementWithBd::computeCurrentQuadraturePointInternAndBd()
{
  ///  internal quad points:
  CurrentFiniteElement::computeCurrentQuadraturePoint();
  ///  quad points on boundary elements: (separate treatment)
  for (int ibd = 0; ibd <m_numBdEle; ibd++ ) {
    for(int ilocg=0; ilocg< m_numQuadPointBdEle[ibd]; ilocg++) {
      const std::size_t ig = ilocg+m_indexQuadPoint[ibd+1];
      coorMap(currentQuadPoint[ig],m_refQuadPointBdEle[ibd][ilocg]);
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void CurrentFiniteElementWithBd::printBd(int verbose,std::ostream& c) const
{
  c << "OOoo-->CurrentFiniteElementWithBd" << std::endl;
  c << " numBdEle:       " << m_numBdEle << "\n";
  for (int ibd = 0; ibd< m_numBdEle; ibd++ ) {
    c << "OOoo----> Boundary Element (ibd="<< ibd << ")" << std::endl;
    c << "    Points (num=" <<  m_numPointBdEle[ibd] << ")";
    if ( verbose >0) {
      c <<" = [";
      for(int iptBd=0; iptBd < m_numPointBdEle[ibd] ; iptBd++ ) {
        c << m_pointBdEle[ibd][iptBd] << " ";
      }
      c << "]";
    }
    c << std::endl;
    c << "    numQuadraturePointBdEle=" << m_numQuadPointBdEle[ibd] <<std::endl;
    if ( verbose >1) {
      for (int ig = 0; ig < m_numQuadPointBdEle[ibd]; ig++ ) {
        c << "    ---->currentQuadPointBdEle[ig=" << ig << "] = ";
        m_refQuadPointBdEle[ibd][ig].print(verbose,c);
      }
    }
    //
    bdEle(ibd).print(verbose,c);
    //
  }
}

/***********************************************************************************/
/***********************************************************************************/

// There must be an easier way to do this!! Right now I found only this one..Matteo 3/14
// ielbd = index of the surface element
// ielvol= index of the volume element
// dofbd = index of the local dof (->0,1,2 for a triangle)
// icoor = component of the variable
// idvar = index of the variable
// tested only for velocity!
int CurrentFiniteElementWithBd::Bd2Vol(const std::size_t iElBd, const std::size_t iElVol, const std::size_t dofBd, const std::size_t iCoor, const Dof& dof, const std::size_t idVar) const
{
  felInt iDofGlob;
  dof.loc2glob( iElBd, dofBd, idVar, iCoor, iDofGlob);
  for ( int iVolDofL = 0; iVolDofL < m_numDof; iVolDofL++) {
    felInt iDofGlobTry;
    dof.loc2glob( iElVol, iVolDofL, idVar, iCoor, iDofGlobTry);
    if( iDofGlobTry == iDofGlob)
      return iVolDofL;
  }
  FEL_ERROR("Dof not found");
  return -1;
}

/***********************************************************************************/
/***********************************************************************************/

/*
  void CurrentFiniteElementWithBd::mapBd(int iface,double& xref3d,double& yref3d,double& zref3d,double xref2d,double yref2d){
  GeoElement& ge=CurrentFiniteElement::m_geoEle;
  for(int ip=0;ip<ge.numPointPerFace(); ip++){
  for(int icoor=0;icoor<ge.numCoor();icoor++){
  xref3d += ge.pointCoor( ge.pointOfFace(iface, ip), icoor)*;
  }
  }
  }
  */
}
