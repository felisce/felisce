//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _ELEMFIELD_H_INCLUDED
#define _ELEMFIELD_H_INCLUDED

// System includes
#include <vector>

// External includes
#include "Core/NoThirdPartyWarning/Petsc/ao.hpp"
#include "Core/NoThirdPartyWarning/Petsc/vec.hpp"

// Project includes
#include "PETScInterface/petscVector.hpp"
#include "Core/felisce.hpp"
#include "FiniteElement/currentFiniteElement.hpp"
#include "FiniteElement/currentFiniteElementWithBd.hpp"
#include "FiniteElement/curvilinearFiniteElement.hpp"
#include "DegreeOfFreedom/variable.hpp"
#include "DegreeOfFreedom/dof.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/*!
  \class ElementField
  \authors J-F. Gerbeau

  \brief Class handling the values of fields at the element level

  An ElementField contains the values of a scalar field, e.g. a diffusion coefficient or a source term, at the element level.
  It can be either constant, defined on the DOF of the finite element or defined on the quadrature points.

  let \f$(c_i)_{i=1..n}$ denote the values of the field, \f$n$ being the member m_dim.

  - Type CONSTANT_FIELD: m_dim = 1, the value of the field is just a constant.

  - Type DOF_FIELD:  m_dim = number of DOF of the element. In the element operators the values on the quadrature points
  will be computed according to \f$\sum_{i=1}^{n} c_i \phi_i(\hat x_{ig}) \f$, where $\f \hat x_{ig} \f$ denote the quadrature points
  coordinates

  - Type QUAD_POINT_FIELD:  m_dim = number of quadrature points of the element. In the element operators the field
  values will used directly in the quadrature points.

  A ElementField can be initialized from a constant, a function of (x,y,z), a std::vector.

  \warning Remind to call the setValue function of the ElementField for every new element,
  after having updated the current finite element (when the value of the ElementField is element dependent)

  \todo ElementVectorField and ElementTensorField
  */
class ElementField
{
public:

  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /// constructors
  ElementField();

  // Destructor
  ~ElementField();

  /// Copy constructor, to avoid warning due to user-declared destructor.
  ElementField(const ElementField&) = default;

  ///@}
  ///@name Operations
  ///@{

  ///@}
  ///@name Operators
  ///@{

  //intialize methods to suppress pointer declaration, 20/11/2011 Jeremie
  void initialize(const ElementFieldType type=CONSTANT_FIELD, const int numComp=1);
  void initialize(const ElementFieldType type,const CurrentFiniteElement& fe,const int numComp=1);
  void initialize(const ElementFieldType type,const CurrentFiniteElementWithBd& fewbd,const int numComp=1);
  void initialize(const ElementFieldType type,const CurvilinearFiniteElement& fec,const int numComp=1);

  void update(const double value) {
    FEL_ASSERT(m_type==CONSTANT_FIELD);
    val(0,0) = value;
  }

  /// set value by functions:
  template <class FunctorXYZ>
  inline void setValue(const FunctorXYZ& functorXYZ, CurrentFiniteElement& fe, const int icomp=0);
  template <class FunctorXYZ>
  inline void setValueVec(const FunctorXYZ& functorXYZ, CurrentFiniteElement& fe, const int icomp);

  template <class FunctorXYZ>
  inline void setValue(const FunctorXYZ& functorXYZ, CurrentFiniteElementWithBd& fewbd, const int icomp=0);

  template <class FunctorXYZ>
  inline void setValue(const FunctorXYZ& functorXYZ, CurvilinearFiniteElement& fe, const int icomp=0);

  template <class FunctorXYZ>
  inline void setValueVec(const FunctorXYZ& functorXYZ, CurvilinearFiniteElement& fe, const int icomp);

  template <class FunctorXYZT>
  inline void setValueVec(const FunctorXYZT& functorXYZT, CurvilinearFiniteElement& fe, const double time, const int icomp);

  template <class FunctorXYZT>
  inline void setValue(const FunctorXYZT& functorXYZT, CurrentFiniteElement& fe, const double time,const int icomp=0);

  template <class FunctorXYZT>
  inline void setValue(const FunctorXYZT& functorXYZT, CurrentFiniteElementWithBd& fewbd, const double time, const int icomp=0);

  template <class FunctorXYZT>
  inline void setValue(const FunctorXYZT& functorXYZT, CurvilinearFiniteElement& fe, const double time, const int icomp=0);

  template <class FunctorT>       //Attention! this function it's hidden by the above one because of the default value of icomp, I don't know if it is possible to call it. (matteo feb 2014)
  inline void setValueT(const FunctorT& functorT, CurvilinearFiniteElement& fe, const double time);

  /// set value by vectors:

  void setValue(const std::vector<double>& value);

  //---any type of field, component-wise constant. (think for instance about gravity and m_elemFieldRHS)
  void setValue(double value, felInt icomp);

  // set value (all components)
  // using AO application to the numbering
  void setValue(const PetscVector& v, const CurBaseFiniteElement& fe, const felInt iel, const int idVariable, const AO& ao, const Dof& dof);
  
  // using ISLocalToGlobalMapping application to the numbering (to BDDC Thin Structures)
  // this fonction is the same that the previous setValue function exect that it work with an ISLocalToGlobalMapping and not a AO mapping (to BDDC Thin Structures)
  //----------------------------------------------------------------------
  void setValue(const PetscVector& v,const CurvilinearFiniteElement& fe, const felInt iel, const int idVariable,ISLocalToGlobalMapping matisMapDof, const Dof& dof);

  // set value (select a specific component)
  void setValue(const PetscVector& v, const CurvilinearFiniteElement& fe, const felInt iel, const int idVariable, const AO& ao, const Dof& dof, const int iComp);

  // set value using a specific component of the petsc std::vector and setting the result in
  // a specific component of the element field
  // this is used to set each component of the elementfield using a different petscvector
  void setValue(const PetscVector& v, const CurvilinearFiniteElement& fe, const felInt iel,  const int idVariable, const AO& ao, const Dof& dof, const int iComp, const int efComp);

  void setValue(const PetscVector& v, const CurBaseFiniteElement& fe, const felInt iel, const int idVariable, const Dof& dof);

  void setValue(const PetscVector& v, const CurrentFiniteElementWithBd& fe, felInt ibd, const felInt iel, const int idVariable, const AO& ao, const Dof& dof);

  // The idea of this function is to take a petscVec with more than one component, let's say the displacement, compute the scalar product
  // with the normal in the element (we assume it is constant for now)
  // and then save the result in a scalar element fields
  void setValueFdotNormal(const PetscVector& v,const CurvilinearFiniteElement& fe, const felInt iel, const int idVariable, const AO& ao, const Dof& dof);

  void setValueMatching(double* DofValues, felInt* DofIdentities, felInt DofDimension, const CurvilinearFiniteElement& fe, const felInt iel, const int idVariable, const Dof& dof);

  ///set value by ElementFieldDynamicValue read in data file
  void setValue(CurrentFiniteElement fe, ElementFieldDynamicValue &elementFieldDynamicValue, const double time);

  ///@}
  ///@name Access
  ///@{

  inline ElementFieldType type() const {
    return m_type;
  }

  inline int numComp() const {
    return m_numComp;
  }

  /// Return the type of finite element
  ElementFieldType getType() const {
    return m_type;
  }

  UBlasMatrix& get_val();

  // There probably is a UBlas function to do this, but I was not able to find it.
  UBlasVector valAsVec() const;

  ///@}
  ///@name Public member Variables
  ///@{

  /*!
  val is the matrix containing the values of the field. It has numComp rows (e.g. numComp=1 for a scalar field,
  or numComp=number of components of a std::vector field). The number of columns is 1 if type=CONSTANT_FIELD, nDof if type=DOF_FIELD,
  and nQuadPoint if type=QUAD_POINT_FIELD
  */
  UBlasMatrix val; // val(icomp,j)
  UBlasMatrix valTmp;

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout);

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{
  ElementFieldType m_type;
  int m_numComp;
  int m_dim;

  ///Arrays use with Petsc vectors.
  ///Use global ordering of felisce and find global ordering for petsc.
  int* m_findDofPositionInPetscOrdering;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  void setShape(CurrentFiniteElement fe, const ElementFieldType type, const int numComp);

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}

}; // class

///@}
///@name Type Definitions
///@{

//----------------------------------------------------------------------
// Implementation of functors' setValue:
//----------------------------------------------------------------------
template <class FunctorXYZ>
inline void ElementField::setValue(const FunctorXYZ& functorXYZ, CurrentFiniteElement& fe,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZ(fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z());
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

template <class FunctorXYZ>
inline void ElementField::setValueVec(const FunctorXYZ& functorXYZ, CurrentFiniteElement& fe,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZ(icomp,fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z());
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}


//----------------------------------------------------------------------

template <class FunctorXYZ>
inline void ElementField::setValue(const FunctorXYZ& functorXYZ, CurrentFiniteElementWithBd& fewbd,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fewbd.hasQuadPtCoor() ) {
      fewbd.computeCurrentQuadraturePointInternAndBd();
    }
    for(int ig=0; ig<fewbd.numQuadraturePointInternAndBd(); ig++) {
      val(icomp,ig) = functorXYZ(fewbd.currentQuadPoint[ig].x(),fewbd.currentQuadPoint[ig].y(),fewbd.currentQuadPoint[ig].z());
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

//----------------------------------------------------------------------
template <class FunctorXYZ>
inline void ElementField::setValue(const FunctorXYZ& functorXYZ, CurvilinearFiniteElement& fe,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZ(fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z());
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

template <class FunctorXYZ>
inline void ElementField::setValueVec(const FunctorXYZ& functorXYZ, CurvilinearFiniteElement& fe,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZ(icomp,fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z());
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValueVec with a functor is not yet implemented for this type of element field");
    break;
  }
}

  template <class FunctorXYZT>
  inline void ElementField::setValueVec(const FunctorXYZT& functorXYZT, CurvilinearFiniteElement& fe, const double time, const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZT(icomp,fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z(),time);
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValueVec with a functor is not yet implemented for this type of element field");
    break;
  }
}

//----------------------------------------------------------------------
template <class FunctorXYZT>
inline void ElementField::setValue(const FunctorXYZT& functorXYZT, CurrentFiniteElement& fe, const double time,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZT(fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z(),time);
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

//----------------------------------------------------------------------
template <class FunctorXYZT>
inline void ElementField::setValue(const FunctorXYZT& functorXYZT, CurrentFiniteElementWithBd& fewbd, const double time,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fewbd.hasQuadPtCoor() ) {
      fewbd.computeCurrentQuadraturePointInternAndBd();
    }
    for(int ig=0; ig<fewbd.numQuadraturePointInternAndBd(); ig++) {
      val(icomp,ig) = functorXYZT(fewbd.currentQuadPoint[ig].x(),fewbd.currentQuadPoint[ig].y(),fewbd.currentQuadPoint[ig].z(),time);
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

//----------------------------------------------------------------------
template <class FunctorXYZT>
inline void ElementField::setValue(const FunctorXYZT& functorXYZT, CurvilinearFiniteElement& fe, const double time,const int icomp) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
      val(icomp,ig) = functorXYZT(fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z(),time);
    }
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

//----------------------------------------------------------------------
template <class FunctorT>
inline void ElementField::setValueT(const FunctorT& functorT, CurvilinearFiniteElement& fe, const double time) {
  switch (m_type) {
  case QUAD_POINT_FIELD:
    if ( !fe.hasQuadPtCoor() ) {
      fe.computeCurrentQuadraturePoint();
    }
    for(int ig=0; ig<fe.numQuadraturePoint(); ig++)
      val(0,ig) = functorT(time);
    break;
  case CONSTANT_FIELD:
  case DOF_FIELD:
    FEL_ERROR("ElementField::setValue with a functor is not yet implemented for this type of element field");
    break;
  }
}

//============================================================================================
/*
  class ElementVectorField{
  public:
  enum Type {
  CONSTANT_FIELD,
  DOF_FIELD,
  QUAD_POINT_FIELD
  }
  private:
  Type m_type;
  std::size_t m_numComp;
  std::size_t m_size;
  public:
  UBlasVector val;
  ElementVectorField(ElementFieldType type,const CurrentFiniteElement& fe,const int numComp);
  void set(UBlasVector value){
  FEL_ASSERT(m_type==CONSTANT_FIELD);
  FEL_ASSERT(m_numComp == value.size());
  val = value;
  }
  inline ElementVectorField::Type type() const {return m_type;}
  template <class Templ_functor>
  inline void update(const Templ_functor& func,const CurrentFiniteElement& fe,const int icomp);
  void print(int verbose,std::ostream& c=std::cout);
  }
  // Implementation
  template <class Templ_functor>
  inline void ElementVectorField::update(const Templ_functor& func,const CurrentFiniteElement& fe,const int icomp=0){
  switch (m_type) {
  case QUAD_POINT_FIELD:
  if ( !fe.hasQuadPtCoor() ) { fe.computeCurrentQuadraturePoint(); }
  for(int ig=0;ig<fe.numQuadraturePoint();ig++){
  val(icomp,ig) = func(fe.currentQuadPoint[ig].x(),fe.currentQuadPoint[ig].y(),fe.currentQuadPoint[ig].z());
  }
  break;
  default:
  FEL_ERROR("ElementField::update with a functor is not yet implemented for this type of element field");
  break;
  }
  }

  */
///@}
} /* namespace felisce.*/

#endif
