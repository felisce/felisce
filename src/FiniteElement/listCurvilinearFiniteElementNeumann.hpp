//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

#ifndef LISTCURVILINEARFINITEELEMENTNEUMANN_H
#define LISTCURVILINEARFINITEELEMENTNEUMANN_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/curvilinearFiniteElement.hpp"

namespace felisce 
{
/*!
  \class ListCurvilinearFiniteElementNeumann
  \author J. Foulon
  \date 08/07/2011
  \brief Class containing a list of curvilinear finite element for boundary condition
  in the problem.
  These finite elements are stored with an STL vector.
  */

class ListCurvilinearFiniteElementNeumann {
private:
  //! std::vector STL which contains Felisce's currentFiniteElement.
  std::vector<CurvilinearFiniteElement*> m_listCurvilinearFiniteElementNeumann;
public:
  //Constructor
  //===========
  ListCurvilinearFiniteElementNeumann() = default;

  //Set functions
  //=============
  void add(CurvilinearFiniteElement* fe);
  int size() {
    return m_listCurvilinearFiniteElementNeumann.size();
  }
  void print();

  //Access Functions
  //================
  inline const std::vector<CurvilinearFiniteElement*>  & listCurvilinearFiniteElementNeumann()
  const {
    return m_listCurvilinearFiniteElementNeumann;
  }
  inline std::vector<CurvilinearFiniteElement*>  & listCurvilinearFiniteElementNeumann() {
    return m_listCurvilinearFiniteElementNeumann;
  }

  //Operators.
  //==========
  CurvilinearFiniteElement* operator[](int i) {
    return m_listCurvilinearFiniteElementNeumann[i];
  }
};
}
#endif
