//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

// System includes

// External includes

// Project includes
#include "FiniteElement/listCurrentFiniteElementWithBd.hpp"

namespace felisce 
{
void ListCurrentFiniteElementWithBd::add(CurrentFiniteElementWithBd* fewbd) {
  FEL_ASSERT(fewbd);
  m_listCurrentFiniteElementWithBd.push_back(fewbd);
}

/***********************************************************************************/
/***********************************************************************************/

//! Print function
void ListCurrentFiniteElementWithBd::print(int verbose,std::ostream& c) const {
  IGNORE_UNUSED_ARGUMENT(verbose);
  c << "List of Finite Element WithBd: " << std::endl;
  for ( unsigned int iFe = 0; iFe < m_listCurrentFiniteElementWithBd.size(); iFe++)
    //m_listCurrentFiniteElement[iFe]->print(); //! /todo create print function for current finite element
    c << std::endl;
}
}
