//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

#ifndef _GEO_ELEMENT_HPP_
#define _GEO_ELEMENT_HPP_

// System includes
#include <iostream>
#include <unordered_map>
#include <string>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/basisFunction.hpp"
#include "FiniteElement/refShape.hpp"
#include "FiniteElement/refElement.hpp"
#include "Geometry/geometricMeshRegion.hpp"

namespace felisce
{
/*!
  \class GeoElement
  \authors J-F. Gerbeau

  \brief Class implementing a geometric element

  A geometric element is defined by
  - a set of basis functions
  - a geometric shape
  - the coordinates of the points

  Let \f$x_{i,icoor}\f$ be the coordinate \c icoor of the point \c. Then the geometric mapping is defined by
  \f$\Phi(\hat x) = \sum_{i=1}^{n} \hat x_i \phi_i(\hat x) \f$

  Any new geometric element must be declared in geoElement.hpp and defined in definitionGlobalVariables.cpp
  */
class GeoElement
{
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  GeoElement(const std::string& name,const RefShape& shape,const BasisFunction& basisFunction,const double* pointCoor,const int numEdge,const int numFace,
              const int* pointOfEdge,const int* pointOfFace,const int* edgeOfFace,const bool* orientationOfEdge,
              const GeoElement** boundaryGeoElement,const GeoElement** boundaryBoundaryGeoElement,
              const RefElement& defaultFiniteEle);
  ~GeoElement();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  const RefElement* defineFiniteEle(const GeometricMeshRegion::ElementType eltType, const int typeOfFiniteElement, GeometricMeshRegion& mesh) const;

  ///@}
  ///@name Access
  ///@{

  inline std::string name() const {
    return m_name;
  }

  inline const RefShape& shape() const {
    return m_shape;
  }

  inline const RefElement& defaultFiniteEle() const {
    return m_defaultFiniteEle;
  }

  inline int numCoor() const {
    return m_numCoor;
  }

  inline int numPoint() const {
    return m_numPoint;
  }

  inline int numEdge() const {
    return m_numEdge;
  }

  inline int numFace() const {
    return m_numFace;
  }

  inline int numBdEle() const {
    return m_numBdEle;
  }

  inline int pointOfEdge(int localEdge, int ipoint) const {
    FEL_ASSERT(localEdge<m_numEdge);
    FEL_ASSERT(ipoint<m_numPointPerEdge[localEdge]);
    return m_ptOfEd[m_indexPointPerEdge[localEdge]+ipoint];
  }

  inline int pointOfFace(int localFace, int ipoint) const {
    FEL_ASSERT(localFace<m_numFace);
    FEL_ASSERT(ipoint<m_numPointPerFace[localFace]);
    return m_ptOfFa[m_indexPointPerFace[localFace]+ipoint];
  }

  //! point on the boundary element (face in 3d, edge in 2d, point in 1d)
  inline int pointOfBdEle(int localBdEle, int ipoint) const {
    FEL_ASSERT(localBdEle<m_numBdEle);
    FEL_ASSERT(ipoint<m_numPointPerBdEle[localBdEle]);
    return m_ptOfBdEle[m_indexPointPerBdEle[localBdEle]+ipoint];
  }

  inline int edgeOfFace(int localFace, int edge, bool& orientation) const {
    FEL_ASSERT(localFace<m_numFace);
    FEL_ASSERT(edge<m_numEdgePerFace[localFace]);
    orientation = m_orientEd[m_indexEdgePerFace[localFace]+edge];
    return m_edOfFa[m_indexEdgePerFace[localFace]+edge];
  }

  //  inline int numPointPerEdge() const {return m_numPointPerEdge;}
  inline int numPointPerFace(int iface) const {
    FEL_ASSERT(iface <  m_numFace && iface >= 0);
    return m_numPointPerFace[iface];
  }

  inline int numPointPerBdEle(int ibdele) const {
    FEL_ASSERT(ibdele <  m_numBdEle && ibdele >= 0);
    return m_numPointPerBdEle[ibdele];
  }

  // inline int numEdgePerFace() const {return m_numEdgePerFace;}
  inline double pointCoor(int ipoint,int icoor) const {
    FEL_ASSERT(ipoint < m_numPoint);
    FEL_ASSERT(icoor < 3);
    return m_pointCoor[ 3*ipoint + icoor];
  }

  inline const BasisFunction& basisFunction() const {
    return m_basisFunction;
  }

  inline const GeoElement& boundaryGeoElement(int i) const {
    return *(m_boundaryGeoElement[i]);
  }

  inline const GeoElement& boundaryBoundaryGeoElement(int i) const {
    return *(m_boundaryBoundaryGeoElement[i]);
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  /*! Name of the geometric element */
  const std::string m_name; //!< Name of the geometric element
  /*! Shape of the geometric element \see refShape.hpp*/
  const RefShape& m_shape;
  /*! For a geometric element, the basis functions are used to compute the application that maps
    the reference element onto the current element*/
  const BasisFunction& m_basisFunction; //!< Basis functions
  const int m_numCoor;  // = m_basisFunction.numCoor()
  const int m_numPoint; // = m_basisFunction.size()
  const int m_numEdge;
  const int m_numFace;
  const int m_numBdEle; // either = m_numFace (3d) or m_numEdge (2d) or 2 (1d)
  const double* m_pointCoor;
  const int* m_ptOfEd;
  const int* m_ptOfFa;
  const int* m_ptOfBdEle; //either = m_ptOfFa (3d) or m_ptOfEd (2d) or m_ptOfEd (1d) (only {0,1} are used)
  const int* m_edOfFa;
  const bool* m_orientEd;
  const GeoElement** m_boundaryGeoElement;
  const GeoElement** m_boundaryBoundaryGeoElement;
  const RefElement& m_defaultFiniteEle;
  int* m_numPointPerEdge;
  int* m_indexPointPerEdge;
  int* m_numPointPerFace;
  int* m_indexPointPerFace;
  int* m_numPointPerBdEle; //either = m_numPointPerFace (3d) or m_numPointPerEdge (2d) or {1,1} (1d)
  int* m_indexPointPerBdEle; //either = m_indexPointPerFace (3d) or m_indexPointPerEdge (2d) or {0,1} (1d)
  int* m_numEdgePerFace;
  int* m_indexEdgePerFace;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

extern const GeoElement geoElementNULL;
extern const GeoElement geoElementNode;
extern const GeoElement geoElementSegmentP1;
extern const GeoElement geoElementSegmentP1b;
extern const GeoElement geoElementSegmentP2;
extern const GeoElement geoElementTriangleP1;
extern const GeoElement geoElementTriangleP1b;
extern const GeoElement geoElementTriangleP2;
extern const GeoElement geoElementQuadrangleQ1;
extern const GeoElement geoElementQuadrangleP1xP2;
extern const GeoElement geoElementQuadrangleQ1b;
extern const GeoElement geoElementQuadrangleQ2;
extern const GeoElement geoElementQuadrangleQ2c;
extern const GeoElement geoElementTetrahedronP1;
extern const GeoElement geoElementTetrahedronP1b;
extern const GeoElement geoElementTetrahedronP2;
extern const GeoElement geoElementHexahedronQ1;
extern const GeoElement geoElementHexahedronQ1b;
extern const GeoElement geoElementHexahedronQ2;
extern const GeoElement geoElementHexahedronQ2c;
extern const GeoElement geoElementPrismR1;
extern const GeoElement geoElementPrismP1xP2;
extern const GeoElement geoElementPrismR2;

///@}
} /* namespace felisce.*/
#endif
