//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  J-F. Gerbeau
//


#ifndef _REF_SHAPE_HPP_
#define _REF_SHAPE_HPP_

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"

namespace felisce 
{
/*!
  \class RefShape
  \brief The class implementing a reference shape
  \authors J-F. Gerbeau
  */

/// All available shapes
enum TypeShape {NullShape,Node,Segment,Triangle,Quadrilateral,Tetrahedron,Hexahedron,Prism,Pyramid};

class RefShape {
  /// A std::string with the name: i.e. the typeShape (all uppercase)
  const std::string m_name;
  /// The dimension of the reference element: 0 for NullShape, 1 for Node and Segment, 2: for Tria and Quad, 3 for Tetra, Hexa, Prism and Pyramid
  const int m_numCoor;
  /// The number of vertices of the element
  const int m_numVertex;
  /// The number of edges of the element, including the element itself : i.e. for the Segment is equal to 1
  const int m_numEdge;
  /// The number of faces of the element, including the element itself : i.e. for Tria and Quad is equal to 1
  const int m_numFace;
  /// The shape of the element (same as name, but it is an enum)
  const TypeShape m_typeShape;

public:
  /// This is a do-nothing constructor that simply assign the values
  RefShape(std::string name,int numCoor,int numVertex,int numEdge,int numFace,TypeShape typeShape);

  /// Getters for the private members
  inline std::string name() const {
    return m_name;
  }
  inline int numCoor() const {
    return m_numCoor;
  }
  inline int numVertex() const {
    return m_numVertex;
  }
  inline int numEdge() const {
    return m_numEdge;
  }
  inline int numFace() const {
    return m_numFace;
  } 
  inline int typeShape() const {
    return m_typeShape;
  }
  // Print function (if verbose>0)
  void print(int verbose, std::ostream& c = std::cout) const;
};

//=================================================================================
// Extern keyword means that they are defined somewhere else.
// In fact they are defined in definitionGlobalVariables.cpp
extern const RefShape NULLSHAPE;
extern const RefShape NODE;
extern const RefShape SEGMENT;
extern const RefShape TRIANGLE;
extern const RefShape QUADRILATERAL;
extern const RefShape TETRAHEDRON;
extern const RefShape HEXAHEDRON;
extern const RefShape PRISM;
extern const RefShape PYRAMID;
}
#endif
