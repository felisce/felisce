//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

#ifndef LISTCURVILINEARFINITEELEMENT_H
#define LISTCURVILINEARFINITEELEMENT_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/curvilinearFiniteElement.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class ListCurvilinearFiniteElement
 * @author J. Foulon
 * @date 27/01/2011
 * @brief Class containing a list of finite elements in the problem.
 * These finite elements are stored with an STL vector.
 * @todo Replace with a template
 */
class ListCurvilinearFiniteElement {
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor
  ListCurvilinearFiniteElement() = default;

  /// Destructor
  ~ListCurvilinearFiniteElement()
  {
    clear();
  }

  ///@}
  ///@name Operators
  ///@{
  
  CurvilinearFiniteElement* operator[](int i) {
    return m_listCurvilinearFiniteElement[i];
  }

  ///@}
  ///@name Operations
  ///@{

  //Set functions
  //=============
  void add(CurvilinearFiniteElement* fe);

  void clear()
  {
    for (std::size_t i = 0; i < m_listCurvilinearFiniteElement.size(); i++)
      delete m_listCurvilinearFiniteElement[i];

    m_listCurvilinearFiniteElement.clear();
  }

  ///@}
  ///@name Access
  ///@{

  inline const std::vector<CurvilinearFiniteElement*>& listCurvilinearFiniteElement() const {
    return m_listCurvilinearFiniteElement;
  }

  inline std::vector<CurvilinearFiniteElement*>& listCurvilinearFiniteElement() {
    return m_listCurvilinearFiniteElement;
  }

  std::size_t size() {
    return m_listCurvilinearFiniteElement.size();
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  /// Vector STL which contains Felisce's curvilinearFiniteElement.
  std::vector<CurvilinearFiniteElement*> m_listCurvilinearFiniteElement;

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif
