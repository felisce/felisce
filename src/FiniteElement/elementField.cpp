//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes

// External includes

// Project includes
#include "FiniteElement/elementField.hpp"
#include "PETScInterface/petscVector.hpp"
#include "PETScInterface/AbstractPETScObjectInterface.hpp"

namespace felisce
{
ElementField::ElementField():
  m_numComp(0),
  m_dim(0)
{
  m_findDofPositionInPetscOrdering = nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

ElementField::~ElementField()
{
  if (m_findDofPositionInPetscOrdering) {
    delete [] m_findDofPositionInPetscOrdering;
    m_findDofPositionInPetscOrdering = nullptr;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::initialize(const ElementFieldType type, const int numComp)
{
  m_type = type;
  m_numComp = numComp;
  m_dim = 1;
  FEL_ASSERT(m_type == CONSTANT_FIELD);
  val.clear();
  val.resize(m_numComp,1);
  val.clear();
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::initialize(const ElementFieldType type, const CurrentFiniteElement& fe,const int numComp)
{
  m_type = type;
  m_numComp = numComp;
  switch (m_type) {
  case CONSTANT_FIELD:
    m_dim = 1;
    break;
  case DOF_FIELD:
    m_dim = fe.numDof();
    break;
  case QUAD_POINT_FIELD:
    m_dim = fe.numQuadraturePoint();
    break;
    // Default case should appear with a warning at compile time instead of an error in runtime
    // (that's truly the point of using enums as switch cases)
    //  default:
    //FEL_ERROR("Unknown element field type");
    // break;
  }
  val.clear();
  val.resize(m_numComp,m_dim);
  val.clear();

  if (m_findDofPositionInPetscOrdering) {
    delete [] m_findDofPositionInPetscOrdering;
    m_findDofPositionInPetscOrdering = nullptr;
  }
  m_findDofPositionInPetscOrdering = new felInt[fe.numDof()*m_numComp];
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::initialize(const ElementFieldType type,const CurvilinearFiniteElement& fe,const int numComp)
{
  m_type = type;
  m_numComp = numComp;
  switch (m_type) {
    case CONSTANT_FIELD:
      m_dim = 1;
      break;
    case DOF_FIELD:
      m_dim = fe.numDof();
      break;
    case QUAD_POINT_FIELD:
      m_dim = fe.numQuadraturePoint();
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      //FEL_ERROR("Unknown element field type");

  }
  val.clear();
  val.resize(m_numComp,m_dim);
  val.clear();

  if (m_findDofPositionInPetscOrdering) {
    delete [] m_findDofPositionInPetscOrdering;
    m_findDofPositionInPetscOrdering = nullptr;
  }
  m_findDofPositionInPetscOrdering = new felInt[fe.numDof()*m_numComp];
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::initialize(const ElementFieldType type, const CurrentFiniteElementWithBd& fewbd,const int numComp)
{
  m_type = type;
  m_numComp = numComp;
  switch (m_type) {
    case CONSTANT_FIELD:
      m_dim = 1;
      break;
    case DOF_FIELD:
      m_dim = fewbd.numDof();
      break;
    case QUAD_POINT_FIELD:
      m_dim = fewbd.numQuadraturePointInternAndBd();
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Unknown element field type");
  }
  val.clear();
  val.resize(m_numComp,m_dim);
  val.clear();

  if (m_findDofPositionInPetscOrdering) {
    delete [] m_findDofPositionInPetscOrdering;
    m_findDofPositionInPetscOrdering = nullptr;
  }
  m_findDofPositionInPetscOrdering = new felInt[fewbd.numDof()*m_numComp];
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::print(int verbose,std::ostream& c)
{
  if(verbose) {
    switch (m_type) {
      case CONSTANT_FIELD:
        c << "Constant ElemField: ";
        break;
      case DOF_FIELD:
        c << "DOF field: " ;
        break;
      case QUAD_POINT_FIELD:
        c << "Quadrature points ElemField: " ;
        break;

        // Default case should appear with a warning at compile time instead of an error in runtime
        // (that's truly the point of using enums as switch cases)
        //  default:
        //        FEL_ERROR("Unknown element field type");
    }

    if(m_numComp == 1) {
      for(int i=0; i<m_dim; i++) c << val(0,i) << " ";
    } else {
      for(int icomp=0; icomp<m_numComp; icomp++) {
        c << " Component " << icomp << ": ";
        for(int i=0; i<m_dim; i++) c << val(icomp,i) << " ";
      }
    }
    c << std::endl;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const std::vector<double>& value)
{
  // Now should do the same but also with quad_point_field.
  switch (m_type) {
    case CONSTANT_FIELD:
      FEL_ASSERT(m_numComp == static_cast<int>(value.size()));
      for (int i=0; i<m_numComp; i++)
        val(i,0) = value[i];
      break;
    case QUAD_POINT_FIELD:
      FEL_ASSERT(m_numComp == 1);
      for (std::size_t i=0; i<value.size(); i++)
        val(0,i) = value[i];
      break;
    case DOF_FIELD:
      FEL_ERROR("ElementField::setValue from std::vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const double value, const felInt icomp)
{
  for (int k(0);k<m_dim;++k) {
    val(icomp,k) = value;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const PetscVector& v,const CurBaseFiniteElement& fe, const felInt iel,
                            const int idVariable, const AO& ao, const Dof& dof)
{
  const int size = fe.numDof()*m_numComp;
  switch (m_type) {
    case DOF_FIELD:
      AOInterface::RetrieveDofPositionInPetscOrdering(m_findDofPositionInPetscOrdering, fe, iel, idVariable, ao, dof, m_numComp);
      v.getValues(size,m_findDofPositionInPetscOrdering,&val.data()[0]);
      break;

    case QUAD_POINT_FIELD: {
      felReal tmp[size];
      AOInterface::RetrieveDofPositionInPetscOrdering(m_findDofPositionInPetscOrdering, fe, iel, idVariable, ao, dof, m_numComp);
      v.getValues(size,m_findDofPositionInPetscOrdering,tmp);

      for(int iComp = 0; iComp<m_numComp; iComp++) {
        for(int ig=0; ig<fe.numQuadraturePoint(); ig++) {
          val(iComp,ig) = 0.;
          for(int iDof=0; iDof<fe.numDof(); iDof++) {
            val(iComp,ig) += tmp[iComp*fe.numDof() + iDof]*fe.phi[ig](iDof);
          }
        }
      }
      break;
    }

    case CONSTANT_FIELD:
      FEL_ERROR("ElementField::setValue from Petsc std::vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const PetscVector& v,const CurvilinearFiniteElement& fe, const felInt iel,
                            const int idVariable,ISLocalToGlobalMapping matisMapDof, const Dof& dof)
{
  const int size = fe.numDof()*m_numComp;
  int* localIndexDof = new felInt[fe.numDof()*m_numComp];
  PetscInt output[size];
  int cpt = 0;
  felInt idDof;
  switch (m_type) {
    case DOF_FIELD:
      for(int iComp = 0; iComp<m_numComp; iComp++) {
        for(int iDof=0; iDof<fe.numDof(); iDof++) {
          dof.loc2glob(iel,iDof,idVariable,iComp,idDof);
          localIndexDof[cpt] = idDof;
          cpt++;
        }
      }
      ISLocalToGlobalMappingApply(matisMapDof,size, localIndexDof, output); //m_findDofPositionInPetscOrdering);
      v.getValues(size,output,&val.data()[0]);

      break;
    case QUAD_POINT_FIELD: {
      FEL_ERROR("ElementField::setValue from Petsc std::vector is not yet implemented for this type of element field");
    }
    break;
    case CONSTANT_FIELD:
      FEL_ERROR("ElementField::setValue from Petsc std::vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValueFdotNormal(const PetscVector& v,const CurvilinearFiniteElement& fe, const felInt iel,
                                      const int idVariable, const AO& ao, const Dof& dof)
{
  //TODO: Attention, we assume a P0 normal!!
  FEL_ASSERT(m_numComp == 1);

  const int size = fe.numDof()*fe.numCoor();
  std::vector<double> tmp(size);

  felInt findDofPositionInPetscOrdering[size];

  int cpt = 0;
  felInt idDof;
  switch (m_type) {
    case DOF_FIELD:
      for(int iComp = 0; iComp<fe.numCoor(); iComp++) {
        for(int iDof=0; iDof<fe.numDof(); iDof++) {
          dof.loc2glob(iel,iDof,idVariable,iComp,idDof);
          findDofPositionInPetscOrdering[cpt] = idDof;
          cpt++;
          val(0,iDof)=0; //I am not sure it is needed.
        }
      }
      AOApplicationToPetsc(ao,size,findDofPositionInPetscOrdering);
      v.getValues(size,findDofPositionInPetscOrdering,tmp.data());
      cpt = 0;
      for(int iComp = 0; iComp<fe.numCoor(); iComp++) {
        for(int iDof=0; iDof<fe.numDof(); iDof++) {
          val(0,iDof) += tmp[cpt]*fe.normal[0](iComp);
          cpt++;
        }
      }
      break;
    case QUAD_POINT_FIELD:
    case CONSTANT_FIELD:
      FEL_ERROR("ElementField::setValueFdotNormal from Petsc std::vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const PetscVector& v,const CurvilinearFiniteElement& fe, const felInt iel,
                            const int idVariable, const AO& ao, const Dof& dof, const int iComp)
{
  const int size = fe.numDof();
  switch (m_type) {
    case DOF_FIELD:
      AOInterface::RetrieveDofPositionInPetscOrderingSingleComponent(m_findDofPositionInPetscOrdering, fe, iel, idVariable, ao, dof, iComp);
      v.getValues(size,m_findDofPositionInPetscOrdering,&val.data()[0]);
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementField::setValue from Petsc std::vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const PetscVector& v,const CurvilinearFiniteElement& fe, const felInt iel,
                            const int idVariable, const AO& ao, const Dof& dof, const int iComp, const int efComp)
{
  const int size = fe.numDof();
  std::vector<double> tmp(size);
  switch (m_type) {
      case DOF_FIELD:
        AOInterface::RetrieveDofPositionInPetscOrderingSingleComponent(m_findDofPositionInPetscOrdering, fe, iel, idVariable, ao, dof, iComp);
        v.getValues(size,m_findDofPositionInPetscOrdering,&tmp[0]);
        for(int iDof=0; iDof<fe.numDof(); iDof++) {
          val(efComp,iDof) = tmp[iDof];
        }
        break;
      case CONSTANT_FIELD:
      case QUAD_POINT_FIELD:
        FEL_ERROR("ElementField::setValue from Petsc std::vector is not yet implemented for this type of element field");
        break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const PetscVector& v,const CurrentFiniteElementWithBd& fe, felInt ibd, const felInt iel,
                            const int idVariable, const AO& ao, const Dof& dof)
{
  CurvilinearFiniteElement* feCurv = fe.ptrBdEle(ibd);
  const int size = feCurv->numDof()*m_numComp;
  int cpt = 0;
  felInt idDof;
  const std::vector<int>& idPointBdEle = fe.pointBdEle(ibd);

  switch (m_type) {
    case DOF_FIELD:
      for(int iComp = 0; iComp<m_numComp; iComp++) {
        for(int iDof=0; iDof<feCurv->numDof(); iDof++) {
          dof.loc2glob(iel,idPointBdEle[iDof],idVariable,iComp,idDof);
          m_findDofPositionInPetscOrdering[cpt] = idDof;
          cpt++;
        }
      }
      AOApplicationToPetsc(ao,size,m_findDofPositionInPetscOrdering);
      v.getValues(size,m_findDofPositionInPetscOrdering,&val.data()[0]);
      break;

    case QUAD_POINT_FIELD: {
      // size is small so no new needed
      felReal tmp[size];

      for(int iComp = 0; iComp<m_numComp; iComp++) {
        for(int iDof=0; iDof<feCurv->numDof(); iDof++) {
          dof.loc2glob(iel,idPointBdEle[iDof],idVariable,iComp,idDof);
          m_findDofPositionInPetscOrdering[cpt] = idDof;
          cpt++;
        }
      }
      AOApplicationToPetsc(ao,size,m_findDofPositionInPetscOrdering);
      v.getValues(size,m_findDofPositionInPetscOrdering,tmp);

      for(int iComp = 0; iComp<m_numComp; iComp++) {
        for(int ig=0; ig<feCurv->numQuadraturePoint(); ig++) {
          val(iComp,ig) = 0.;
          for(int iDof=0; iDof<feCurv->numDof(); iDof++) {
            val(iComp,ig) += tmp[iComp*feCurv->numDof() + iDof]*feCurv->phi[ig](iDof);
          }
        }
      }
      break;
    }

    case CONSTANT_FIELD:
      FEL_ERROR("ElementField::setValue from Petsc std::vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(const PetscVector& v,const CurBaseFiniteElement& fe, const felInt iel,
                            const int idVariable, const Dof& dof)
{
  felInt idLine;
  double value;
  felInt idDof;
  switch (m_type) {
    case DOF_FIELD:
      for(int iComp = 0; iComp<m_numComp; iComp++) {
        for(int iDof=0; iDof<fe.numDof(); iDof++) {
          dof.loc2glob(iel,iDof,idVariable,iComp,idDof);
          idLine = idDof;
          v.getValues(1,&idLine,&value);
          val(iComp,iDof) = value;
        }
      }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementField::setValue from Petsc vector is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setShape(CurrentFiniteElement fe, const ElementFieldType type, const int numComp)
{
  m_type = type;
  m_numComp = numComp;

  switch(m_type) {
    case CONSTANT_FIELD:
      m_dim = 1;
      break;

    case DOF_FIELD:
      m_dim = fe.numDof();
      break;

    case QUAD_POINT_FIELD:
      m_dim = fe.numQuadraturePoint();
      break;
      // Default case should appear with a warning at compile time instead of an error in runtime
      // (that's truly the point of using enums as switch cases)
      //  default:
      // FEL_ERROR("Unexpected ElementField type.")

  }

  val.clear();
  val.resize(m_numComp,m_dim);
  val.clear();

  if (m_findDofPositionInPetscOrdering) {
    delete [] m_findDofPositionInPetscOrdering;
    m_findDofPositionInPetscOrdering = nullptr;
  }
  m_findDofPositionInPetscOrdering = new felInt[fe.numDof()*m_numComp];
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValue(CurrentFiniteElement fe, ElementFieldDynamicValue &efv, const double time)
{
  Component comp;
  const Point *refPointList;
  Point geoPoint;
  double x = 0.;
  double y = 0.;
  double z = 0.;

  // Prepare shape (realloc val)
  setShape(fe, efv.elementFieldType(), efv.numComp());

  // CONSTANT_FIELD
  if (efv.elementFieldType() == CONSTANT_FIELD) {

    for (int icomp=0 ; icomp<efv.numComp() ; icomp++) {
      comp = efv.allComp[icomp];
      FEL_ASSERT( efv.typeValueOfElementField(comp)==FROM_CONSTANT );
      val(icomp,0) = efv.constant(comp);
    }
    return;
  }

  // DOF_FIELD and QUAD_POINT_FIELD
  for (int idim=0 ; idim<m_dim ; ++idim) {

    /* get point coordinates */
    switch (efv.elementFieldType()) {
      case DOF_FIELD:
        refPointList = fe.refEle().node();
        fe.coorMap(geoPoint, refPointList[idim]);
        x = geoPoint.x();
        y = geoPoint.y();
        z = geoPoint.z();
        break;

      case QUAD_POINT_FIELD:
        if ( !fe.hasQuadPtCoor() )
          fe.computeCurrentQuadraturePoint();
        x = fe.currentQuadPoint[idim].x();
        y = fe.currentQuadPoint[idim].y();
        z = fe.currentQuadPoint[idim].z();
        break;

      case CONSTANT_FIELD:
        FEL_ERROR("Wrong ElementFieldType value.");
    }

    /* Set all component for this point */
    for (int icomp=0 ; icomp<efv.numComp() ; icomp++) {
      comp = efv.allComp[icomp];

      switch (efv.typeValueOfElementField(comp)) {
      case FROM_FUNCTION:
        val(icomp,idim) = efv.callbackXYZT(comp)(x,y,z,time);
        break;

      case FROM_FILE:
        FEL_ERROR("Not implemented.");
        break;

      case FROM_CONSTANT:
        val(icomp,idim) = efv.constant(comp);
        break;
        // Default case should appear with a warning at compile time instead of an error in runtime
        // (that's truly the point of using enums as switch cases)
        //  default:
        // FEL_ERROR("Wrong typeValueOfElementField value.");
      }
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

void ElementField::setValueMatching(double* DofValues,felInt* DofIdentities,felInt DofDimension, const CurvilinearFiniteElement& fe, const felInt iel,
                                    const int idVariable, const Dof& dof)
{
  //DofValues = values of the variable to match
  //DofIdentities = identity of the dofs in which to std::set the DofValues
  double value = 0.0;
  bool dofFound;
  felInt idDof;
  switch (m_type) {
    case DOF_FIELD:
      for(int iDof=0; iDof<fe.numDof(); iDof++) {
        for(int iComp = 0; iComp<m_numComp; iComp++) {
          dof.loc2glob(iel,iDof,idVariable,iComp,idDof);
          dofFound=false;
          for(felInt i=0; i<DofDimension && dofFound == false; i++) {
            if(DofIdentities[i]==idDof) {
              value = DofValues[i];
              dofFound = true;
            }
          }
          val(iComp,iDof) = value;
        }
      }
      break;
    case CONSTANT_FIELD:
    case QUAD_POINT_FIELD:
      FEL_ERROR("ElementField::setValueMatching is not yet implemented for this type of element field");
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

UBlasMatrix& ElementField::get_val()
{
  return val;
}

/***********************************************************************************/
/***********************************************************************************/

UBlasVector ElementField::valAsVec() const
{
  UBlasVector tmp(m_dim * m_numComp);
  for ( int i(0);i<m_dim;++i)
    for (int ic(0);ic<m_numComp;++ic) {
      tmp(i + ic*m_numComp)=val(ic,i);
  }
  return tmp;
}

}
