//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau and V. Martin
//

#ifndef _CUR_BASE_FINITE_ELEMENT_H
#define _CUR_BASE_FINITE_ELEMENT_H

// System includes
#include <vector>
#include <atomic>

// External includes

// Project includes
#include "geoElement.hpp"
#include "refElement.hpp"
#include "quadratureRule.hpp"
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class CurBaseFiniteElement
 * @authors J-F. Gerbeau and V. Martin
 * @brief Base class for the current and curvilinear finite element
 */
class CurBaseFiniteElement
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Point
  FELISCE_CLASS_POINTER_DEFINITION(CurBaseFiniteElement);

  ///@}
  ///@name Life Cycle
  ///@{

  /// constructors (vector of DegreeOfExactness)
  CurBaseFiniteElement(
    const RefElement& refEle,
    const GeoElement& geoEle,
    const std::vector<DegreeOfExactness>& degOfExactness
    );

  /// constructors
  CurBaseFiniteElement(
    const RefElement& refEle,
    const GeoElement& geoEle,
    const DegreeOfExactness& degOfExactness
    );

  /// Destructor
  virtual ~CurBaseFiniteElement();

  /// Copy constructor
  CurBaseFiniteElement(const CurBaseFiniteElement& rOther)
    : phi(rOther.phi),
      weightMeas(rOther.weightMeas),
      currentQuadPoint(rOther.currentQuadPoint),
      m_refEle(rOther.m_refEle),
      m_geoEle(rOther.m_geoEle),
      m_quadratureRule(rOther.m_quadratureRule),
      m_numDof(rOther.m_numDof),
      m_numQuadraturePoint(rOther.m_numQuadraturePoint),
      m_numRefCoor(rOther.m_numRefCoor),
      m_numCoor(rOther.m_numCoor),
      m_numPoint(rOther.m_numPoint),
      m_hasPoint(rOther.m_hasPoint),
      m_hasMeas(rOther.m_hasMeas),
      m_hasQuadPtCoor(rOther.m_hasQuadPtCoor),
      m_hasOriginalQuadPoint(rOther.m_hasOriginalQuadPoint),
      m_currentId(rOther.m_currentId),
      m_dPhiRef(rOther.m_dPhiRef),
      m_phiGeo(rOther.m_phiGeo),
      m_dPhiGeo(rOther.m_dPhiGeo),
      m_meas(rOther.m_meas),
      m_point(rOther.m_point)
  {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  // Do nothing: this method is just there to provide a non pure virtual/non inline method to avoid emitting
  // weak tables in every translation unit.
  virtual void placeholder() const;



  /// minimal-minimal update called by all updates: set the points only
  void updatePoint(const std::vector<Point*>& point);

  void updatePoint(const UBlasMatrix& point, const std::vector<int>& ipt);
  /*!
    compute the coordinate (x,y,z)= F(xi,eta,zeta), (F: geo mappping)
    where (xi,eta,zeta) are the coor in the ref element
    and   (x,y,z) the coor in the current element
    (in 2D, z=0 and zeta is discarded)
    */
  void coorMap( Point& pt, const Point& refpt ) const;

  /// Return the measure of the current element
  double measure() const;

  /// Return the diameter of the current element
  double diameter() const;

  /// measure of the segment from id1 and id2
  double measOfSegment(felInt id1, felInt id2) const;
  //-----------
  // updates:
  //-----------
  /// minimal update: we just identify the id of the current element
  virtual void update(const int id,const std::vector<Point*>& point);

  /// compute the arrays meas, weightDet, jacobian on the current element
  virtual void updateMeas(const int /*id*/,const std::vector<Point*>& /*point*/) {};

  /// compute the images of the quadrature points through the geometrical mapping
  /// note: the Boundary quad points are computed separately in CurrFEWithBd
  void computeCurrentQuadraturePoint();

  /// map point in reference element
  void mapPointInReferenceElement(const int id, const std::vector<Point*>& ptElem, const Point& inPoint, Point& outPoint);
  void mapPointInReferenceElement(const int id, std::vector<Point>&  ptElem, const Point& inPoint, Point& outPoint);

  ///@}
  ///@name Access
  ///@{

  inline UBlasVector& jacobian() {
      return m_meas;
  }

  inline const RefElement& refEle() const {
    return m_refEle;
  }

  inline const GeoElement& geoEle() const {
    return m_geoEle;
  }

  inline const QuadratureRule& quadratureRule() const {
    return *m_quadratureRule;
  }

  inline int numDof() const {
    return m_numDof;
  }

  inline int numQuadraturePoint() const {
    return m_numQuadraturePoint;
  }

  inline int numCoor() const {
    return m_numCoor;
  }

  inline int numRefCoor() const {
    return m_numRefCoor;
  }

  inline int numPoint() const {
    return m_numPoint;
  }

  /// return true if the point has been updated
  inline bool hasPoint() const {
    return m_hasPoint;
  }

  /// return true if the measure has been updated
  inline bool hasMeas() const {
    return m_hasMeas;
  }

  /// return true if the coordinates of the quadrature points have been updated
  inline bool hasQuadPtCoor() const {
    return m_hasQuadPtCoor;
  }

  /// return true if the basis function are computed at the original quadrature point
  inline bool hasOriginalQuadPoint() const {
    return m_hasOriginalQuadPoint;
  }

  /// return true id of the element
  inline int id() const {
    return m_currentId;
  }

  /// access to dPhiGeo is necessary
  const UBlasMatrix& dPhiGeo(int ig) const {
    return m_dPhiGeo[ig];
  }

  /// access to phiGeo is necessary
  const UBlasVector& phiGeo(int ig) const {
    return m_phiGeo[ig];
  }

  ///@}
  ///@name Public member Variables
  ///@{

  /// phi[ig](idof):  (in the current FE (and equal to phiRef))
  std::vector<UBlasVector> phi;

  UBlasVector weightMeas;

  /// images of the quadrature points on the current element
  std::vector<Point> currentQuadPoint;

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  void m_printBase(int verbose,std::ostream& c=std::cout) const;

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  // TODO: Define flags!!
  // TODO: Move definitions to geometries
  const RefElement& m_refEle;
  const GeoElement& m_geoEle;
  const QuadratureRule* m_quadratureRule;
  int m_numDof;
  int m_numQuadraturePoint;
  int m_numRefCoor; /// Space dimension in the reference element
  int m_numCoor; /// Space dimension (=numRefCoor (CurrentFE) or numRefCoor+1 (CurvilinearFE))
  int m_numPoint;
  bool m_hasPoint = false;
  bool m_hasMeas = false;
  bool m_hasQuadPtCoor = false;
  bool m_hasOriginalQuadPoint = true;
  int m_currentId = 0;

  /// m_dPhiRef[ig](icoor,jdof) = derivative with respect to x_icoor of the jdof-th reference basis function at integration point ig
  std::vector<UBlasMatrix> m_dPhiRef;

  /// Geometric reference functions m_phiGeo[ig](ipoint) : value of the ipoint-th function at integration point ig
  std::vector<UBlasVector> m_phiGeo;

  /// Derivatives of the geometric reference functions  m_dPhiGeo[ig](icoor,jpoint) : value of the jpoint-th function at integration point ig
  std::vector<UBlasMatrix> m_dPhiGeo;

  /// m_meas(ig): determinant of the jacobian at the integration point ig (in the current FE)
  UBlasVector m_meas;

  /// Coordinates of the points defining the element: m_point(ipoint,jcoor)  (in the current FE)
  UBlasMatrix m_point;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /// compute the integration weights
  virtual void computeMeas() {};

  /// update basis function with the new quadrature point
  void updateBasisWithQuadPoint(const std::vector<Point>& point);

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}

}; // class curBaseFiniteElement

///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif /* _CUR_BASE_FINITE_ELEMENT_H  defined */
