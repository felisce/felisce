//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon
//

#ifndef LISTCURRENTFINITEELEMENTWITHBD_H
#define LISTCURRENTFINITEELEMENTWITHBD_H

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/currentFiniteElementWithBd.hpp"

namespace felisce 
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{

/**
 * @class ListCurrentFiniteElementWithBd
 * @author J. Foulon
 * @date 27/01/2011
 * @brief Class containing a list of finite elements in the problem.
 * These finite elements are stored with an STL vector.
 * @todo Replace with a template
 */
class ListCurrentFiniteElementWithBd 
{
public:
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor
  ListCurrentFiniteElementWithBd() = default;

  /// Destructor
  ~ListCurrentFiniteElementWithBd()
  {
    clear();
  }

  ///@}
  ///@name Operators
  ///@{
  
  CurrentFiniteElementWithBd* operator[](int i) {
    return m_listCurrentFiniteElementWithBd[i];
  }

  ///@}
  ///@name Operations
  ///@{

  //Set functions
  //=============
  void add(CurrentFiniteElementWithBd* fe);

  void clear()
  {
    for (std::size_t i = 0; i < m_listCurrentFiniteElementWithBd.size(); i++)
      delete m_listCurrentFiniteElementWithBd[i];

    m_listCurrentFiniteElementWithBd.clear();
  }

  ///@}
  ///@name Access
  ///@{

  inline const std::vector<CurrentFiniteElementWithBd*>& listCurrentFiniteElementWithBd() const {
    return m_listCurrentFiniteElementWithBd;
  }

  inline std::vector<CurrentFiniteElementWithBd*>& listCurrentFiniteElementWithBd() {
    return m_listCurrentFiniteElementWithBd;
  }

  std::size_t size() {
    return m_listCurrentFiniteElementWithBd.size();
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  /// Vector STL which contains Felisce's currentFiniteElementWithBd.
  std::vector<CurrentFiniteElementWithBd*> m_listCurrentFiniteElementWithBd;

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif

