//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes
#include <vector>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"

#ifndef _CURVILINEAR_FINITE_ELEMENT_H
#define _CURVILINEAR_FINITE_ELEMENT_H

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
/*!
  \class CurvilinearFiniteElement
  \authors J-F. Gerbeau
  \brief Class implementing a curvilinear finite element
  */
class CurvilinearFiniteElement:
  public CurBaseFiniteElement
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of CurvilinearFiniteElement
  FELISCE_CLASS_POINTER_DEFINITION(CurvilinearFiniteElement);

  ///@}
  ///@name Life Cycle
  ///@{

  ///  constructors
  CurvilinearFiniteElement(
    const RefElement& refEle,
    const GeoElement& geoEle,
    const DegreeOfExactness& degOfExactness
    );

  ///  constructors (vector of DegreeOfExactness)
  CurvilinearFiniteElement(
    const RefElement& refEle,
    const GeoElement& geoEle,
    const std::vector<DegreeOfExactness>& degOfExactness
    ) : CurvilinearFiniteElement(refEle, geoEle, degOfExactness[0])
  {
  }

  ///  destructor
  ~CurvilinearFiniteElement() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //-----------
  // updates:
  //-----------
  ///  minimal update: we just identify the id of the current element
  void update(const int id, const std::vector<Point*>& point) override;
  void update(const int id, const UBlasMatrix& point, const std::vector<int>& ipt);

  ///  compute the arrays meas, weightDet, jacobian on the current element
  void updateMeas(const int id, const std::vector<Point*>& point) override;
  void updateMeas(const int id, const UBlasMatrix& point, const std::vector<int>& ipt);

  ///  compute the arrays meas, weightDet, jacobian and quadrature point on the current element
  void updateMeasQuadPt(const int id, const std::vector<Point*>& point);

  ///  compute the arrays normal, meas, weightDet and jacobian on the current element
  ///  note: comment is incomplete! vm 08/2011
  void updateMeasNormal(const int id, const std::vector<Point*>& point);
  void updateMeasNormal(const int id, const UBlasMatrix& point, const std::vector<int>& ipt);

  void updateMeasNormalQuadPt(const int id, const std::vector<Point*>& point);

  void updateMeasNormalContra(const int id, const std::vector<Point*>& point);
  void updateMeasNormalContra(const int id, const UBlasMatrix& point, const std::vector<int>& ipt);

  void updateMeasNormalTangent(const int id, const std::vector<Point*>& point);
  void updateMeasNormalTangent(const int id, const UBlasMatrix& point, const std::vector<int>& ipt);

  void updateBasisAndNormalContra(const int id, const std::vector<Point*>& point);
  void updateSubElementMeasNormal(const int id, const std::vector<Point*>& ptElem, const std::vector<Point*> ptSubElem);

  //----------------------------------------------------------------------
  //----------------------------------------------------------------------
  //****************** Curved Beam Elasticity Linear *********************
  //----------------------------------------------------------------------
  //----------------------------------------------------------------------

  // recuperation from the mesh of normals and tangents on the current FE
  void updateNormalTangentMesh (const std::vector<Point*>& normalFE, const std::vector <std::vector<Point*> >& tangentFE);

  void compute_weightMeas();

  // F = F0 + z F1 covariante basis - linear part
  void  compute_F0_F1_beam(const double thickness);

  void  compute_F0_F1_thinShell(const double thickness);

  // component Grr of the Covariant metric tensor
  // grr = Grr0 + z.Grr1 + z².Grr
  void compute_Grr0_Grr1_Grr2(const UBlasMatrix& rF0, const UBlasMatrix& rF1);

  //compute the component grr tensor metric covariant
  // grs = Grs0 + z.Grs1 + z².Grs
  void compute_Grs0_Grs1_Grs2(const UBlasMatrix& rF0, const UBlasMatrix& rF1);

  //compute the component grr tensor metric covariant
  // gss = Gss0 + z.Gss1 + z².Gss
  void compute_Gss0_Gss1_Gss2(const UBlasMatrix& rF0, const UBlasMatrix& rF1);

  void updateBeam (const int id, const std::vector<Point*>& point, const std::vector<Point*>& normalFE, const std::vector< std::vector<Point*> >& tangentFE, const double thickness);

  void updateThinShell (const int id, const std::vector<Point*>& point, const std::vector<Point*>& normalFE, const std::vector< std::vector<Point*> >& tangentFE, const double thickness);

  //Alexandre THIS
  //29 04 2016
  //Method written to tell if the element is inside a ball centered in c = [x,y,z] with radius R=r
  bool isInBallRange(double*, double); // TODO D.C. this is a fully geometric check, why the hell is in finite element???

  void computeNormalThinShell();

  void computeNormal();

  void computeTangent();

  void computeTangentNormal();


  // Evaluate each basis function on the point.
  // The function is written in the case of R^3. So triangles and quadrangles.
  // The point is assumed to be inside the element.
  void evaluateBasisFunctionsInPoint(const Point* p, std::vector<double>& values) const;

  ///@}
  ///@name Member Variables
  ///@{

  /* // is Protected in curBaseFE
  ///  dPhiRef[ig](icoor,jpoint) = derivative with respect to x_icoor of the jpoint-th reference basis function at integration point ig
  UBlasMatrix* dPhiRef;
  */
  std::vector<UBlasMatrix> tangent;
  std::vector<UBlasVector> normal;
  ///  The covariant metric tensor
  std::vector<UBlasMatrix> covMetric;
  std::vector<UBlasMatrix> m_covBasis;
  std::vector<UBlasMatrix> contravariantMetric;
  ///  The contravariant basis
  std::vector<UBlasMatrix> contravariantCompleteBasis;

  double m_sign = 1.0;

  ///@}
  ///@name Access
  ///@{

  inline UBlasMatrix F0(int ir) const {
    return  m_F0[ir];
  }
  inline UBlasMatrix F1(int ir) const {
    return  m_F1[ir];
  }

  //access to the decomposition component G^rr of the contravariant metric tensor
  inline double Grr0() const {
    return  m_Grr0;
  }
  inline double Grr1() const {
    return  m_Grr1;
  }
  inline double Grr2() const {
    return  m_Grr2;
  }

  //access to the decomposition component G^rs of the contravariant metric tensor
  inline double Grs0() const {
    return  m_Grs0;
  }
  inline double Grs1() const {
    return  m_Grs1;
  }
  inline double Grs2() const {
    return  m_Grs2;
  }

  //access to the decomposition component G^ss of the contravariant metric tensor
  inline double Gss0() const {
    return  m_Gss0;
  }
  inline double Gss1() const {
    return  m_Gss1;
  }
  inline double Gss2() const {
    return  m_Gss2;
  }

  ///  return true if the tangent has been updated
  inline bool hasTangent() const {
    return m_hasTangent;
  }
  ///  return true if the normal has been updated
  inline bool hasNormal() const {
    return m_hasNormal;
  }

  ///  access to dPhiRef is necessary
  const UBlasMatrix& dPhiRef(int ig) const {
    return m_dPhiRef[ig];
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(int verbose,std::ostream& c=std::cout) const;

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  bool m_hasTangent = false;
  bool m_hasNormal = false;
  bool m_hasIteratNormal = false;
  bool m_hasIteratTangent = false;

  ///  The covariant basis: the rows are the basis vectors. m_covBasis[ig](icoor,jcoor)=derivative with respect to x_icoor of the component jcoor (icoor is the index of the std::vector in the basis, jcoor its component)
  //UBlasMatrix* m_covBasis;

  ///  The covariant basis: the rows are the basis vectors. m_covBasis[ig](icoor,jcoor)=derivative with respect to x_icoor of the component jcoor (icoor is the index of the std::vector in the basis, jcoor its component) with the normal
  std::vector<UBlasMatrix> m_covCompleteBasis;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///  compute the covariant basis
  void computeCovariantBasis();

  ///  compute the contravariant basis
  void computeContravariantBasis();

  ///  compute the determinant of the Jacobian (call first computeJacobian)
  void computeContravariantMetric();

  ///  compute the determinant of the Jacobian
  void computeMeas() override;

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  //Polynomial decomposition in z of covariant basis - linear part
  std::vector <UBlasMatrix> m_F0, m_F1;

  //Normals and Tangents calcule at each vertice
  std::vector <UBlasMatrix> m_tangentVertice;
  UBlasMatrix m_normalVertice;

  // Grr = Grr0 + z Grr1 + z^2 Grr2
  double m_Grr0 = 0.0;
  double m_Grr1 = 0.0;
  double m_Grr2 = 0.0;

  // Grs = Grs0 + z Grs1 + z^2 Grs2
  double m_Grs0 = 0.0;
  double m_Grs1 = 0.0;
  double m_Grs2 = 0.0;

  // Gss = Gss0 + z Gss1 + z^2 Gss2
  double m_Gss0 = 0.0;
  double m_Gss1 = 0.0;
  double m_Gss2 = 0.0;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/
#endif
