//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin & Miguel Fernandez
//

#ifndef _ELEMMAT_H_INCLUDED
#define _ELEMMAT_H_INCLUDED

// System includes
#include <vector>
#include <ostream>
#include <atomic>

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "Core/shared_pointers.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/curBaseFiniteElement.hpp"

namespace felisce
{
///@name felisce Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name felisce Classes
///@{
class ElementMatrix
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of ElementMatrix
  FELISCE_CLASS_POINTER_DEFINITION(ElementMatrix);

  ///@}
  ///@name Life Cycle
  ///@{

  // default constructor
  ElementMatrix() = default;

  //! constructor for an arbitrary number of finite elements
  ElementMatrix(const std::vector<const CurBaseFiniteElement*>& fe, const std::vector<std::size_t>& nbr, const std::vector<std::size_t>& nbc);

  //! constructor for an arbitrary number of finite elements
  ElementMatrix(const std::vector<const CurBaseFiniteElement*>& fer, const std::vector<std::size_t>& nbr, const std::vector<const CurBaseFiniteElement*>& fec,  const std::vector<std::size_t>& nbc);

  // // constructor for 1 finite element
  // FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor will be removed in the near future. Please use the main constructor which consider constant vector.")
  // ElementMatrix(const CurBaseFiniteElement& fe, const std::size_t nbr1, const std::size_t nbc1);

  // // constructor for 2 finite elements
  // FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor will be removed in the near future. Please use the main constructor which consider constant vector.")
  // ElementMatrix(const CurBaseFiniteElement& fe1, const std::size_t nbr1, const std::size_t nbc1,
  //               const CurBaseFiniteElement& fe2, const std::size_t nbr2, const std::size_t nbc2);

  // // constructor for 3 finite elements
  // FELISCE_DEPRECATED_MESSAGE("WARNING:: This constructor will be removed in the near future. Please use the main constructor which consider constant vector.")
  // ElementMatrix(const CurBaseFiniteElement& fe1, const std::size_t nbr1, const std::size_t nbc1,
  //               const CurBaseFiniteElement& fe2, const std::size_t nbr2, const std::size_t nbc2,
  //               const CurBaseFiniteElement& fe3, const std::size_t nbr3, const std::size_t nbc3 );


  //! Copy constructor, to avoid warning due to user-declared destructor.
  ElementMatrix(const ElementMatrix& rOther);

  // default destructor
  ~ElementMatrix() = default;

  ///@}
  ///@name Operators
  ///@{

  //! Assignment, to avoid warning due to user-declared destructor.
  ElementMatrix& operator=(const ElementMatrix& rOther);

  /*!
    * \brief operator +=
    *
    * This operator acts upon the underlying UBlasMatrix and is is assumed all the other attributes are the same (it
    * is checked by several assert in debug mode).
    */
  void operator+=(const ElementMatrix& mat);

  /*!
    * \brief operator -=
    *
    * This operator acts upon the underlying UBlasMatrix and is is assumed all the other attributes are the same (it
    * is checked by several assert in debug mode).
    */
  void operator-=(const ElementMatrix& mat);

  //! Operator multiplication by a scalar.
  void operator*=(const double factor);

  /**
   * @brief Access operators
   */
  const double& operator () (const std::size_t i, const std::size_t j) const {
    return m_mat(i, j);
  }

  double& operator () (const std::size_t i, const std::size_t j) {
    return m_mat(i, j);
  }

  ///@}
  ///@name Operations
  ///@{

  void duplicateFrom(const ElementMatrix& mat);

  UBlasMatrixRange matBlock( std::size_t i, std::size_t j ) {
    return getMatBlock(m_mat, i, j);
  }

  UBlasMatrixRange getMatBlock(UBlasMatrix& mat, std::size_t i, std::size_t j ) {
    return UBlasMatrixRange(mat,UBlasRange( m_firstRow[i], m_firstRow[i]+m_numRow[i]),
                                UBlasRange( m_firstCol[j], m_firstCol[j]+m_numCol[j]));
  }

  /**
   * @brief Size 1
   */
  std::size_t size1() const {
    return m_mat.size1();
  }

  /**
   * @brief Size 2
   */
  std::size_t size2() const {
    return m_mat.size2();
  }

  /**
   * @brief Sets to zero the main matrix
   */
  void zero() {
    m_mat.clear();
  }

  //Operator div(grad symmetric): div(\eps(\u))  with \eps(\u) = 1/2 (grad u + grad^T u)
  //     \int \eps(\phi_i):\eps(\phi_j)
  void eps_phi_i_eps_phi_j(const double coef, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //Operator div(grad symmetric): div(a \eps(\u))  with \eps(\u) = 1/2 (grad u + grad^T u)
  //     \int a \eps(\phi_i):\eps(\phi_j)
  void a_eps_phi_i_eps_phi_j(const double coef,const ElementField& scalfct,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  void grad_phi_i_grad_phi_j(const double coef,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  void grad_phi_i_grad_phi_j(const double coef,const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void phi_j_grad_psi_i(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,const std::size_t iblock, const std::size_t jblock);

  void phi_i_grad_psi_j(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,const std::size_t iblock, const std::size_t jblock);

  void a_grad_phi_i_grad_phi_j(const double coef,const ElementField& scalfct,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void a_grad_phi_i_grad_phi_j(const double coef,const ElementField& scalfct,const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  //---------------------------------------------------------------------
  // compute \int_K \grad \phi_i \cdot \vec \phi_j
  void grad_phi_i_dot_vec_phi_j(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //---------------------------------------------------------------------
  // compute \int_K eps \phi_i \cdot \vec \phi_j
  void eps_phi_i_dot_vec_phi_j(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //---------------------------------------------------------------------
  // compute \int_K \grad \phi_j \cdot \vec \phi_i
  void phi_i_grad_phi_j_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //---------------------------------------------------------------------
  // compute \int_K \eps \phi_j \cdot \vec \phi_i
  void phi_i_eps_phi_j_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //---------------------------------------------------------------------
  // compute \int_K \eps \phi_j \vec \cdot \eps \phi_i \vec
  void eps_phi_j_vec_dot_eps_phi_i_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //---------------------------------------------------------------------
  // compute \int_K \grad \phi_j \vec \cdot \grad \phi_i \vec
  void grad_phi_j_vec_dot_grad_phi_i_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  // TODO: instead of diffTensor we should pass some kind of element field
  // diffTensor is in practice a vector dof_field.
  void tau_grad_phi_i_tau_grad_phi_j(const double coef,const std::vector<double>& diffTensor,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num,std::size_t dim = 3);

  void tau_grad_phi_i_tau_grad_phi_j(const double coef,const std::vector<double>& diffTensor,const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void tau_orthotau_grad_phi_i_grad_phi_j(const double coef,const std::vector<double>& diffTensor,const std::vector<double>& angle,const CurvilinearFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  //! Operator used in linear elasticity problem
  void grad_phi_i_GradientBasedElastTensor_grad_phi_j(const double coef,const CurrentFiniteElement& fe, UBlasMatrix& GradientBasedElastTensor, const std::size_t iblock, const std::size_t jblock);

  /*!
    * \brief Operator used in hyperelasticity problem.
    */
  void grad_phi_i_GradientBasedHyperElastTensor_grad_phi_j(const double coef,
      const CurrentFiniteElement& feDisplacement,
      int quadraturePointIndex,
      UBlasMatrix& GradientBasedHyperElastTensor, const std::size_t iblock, const std::size_t jblock);

  void phi_i_phi_j(const double coef,const CurBaseFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void setZerosLung(const double coef,const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void phi_i_phi_j_lumped(const double coef,const CurBaseFiniteElement& fe,const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void a_phi_i_phi_j(const double coef,const ElementField& scalfct,const CurBaseFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void penalty_contact_phi_i_scalar_n_phi_j_scalar_n(const double coef,const std::vector<double>& normal, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap);

  void penalty_phi_i_scalar_n_phi_j_scalar_n(const double coef, CurvilinearFiniteElement& fe, const ElementField& vel, std::size_t iblock, std::size_t jblock);

  void nitsche_contact_phi_i_scalar_n_phi_j_scalar_n(const double coef,const std::vector<double>& normal, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap, const ElementField& lambda);

  void nitsche_contact_phi_i_scalar_n_phi_j_scalar_n(const double coef, const CurvilinearFiniteElement& fe,const ElementField& disp, const ElementField& gap, const ElementField& normal, const ElementField& lambda);

  void phi_i_psi_j_scalar_n(const double coef,const CurvilinearFiniteElement& fe1, const CurvilinearFiniteElement& fe2, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  void u_grad_phi_j_phi_i(const double coef,const ElementField& vel,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num, bool transpose=false);

  void u_grad_phi_j_phi_i(const double coef,const ElementField& vel, const  CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  void u_grad_phi_i_grad_psi_j(const double coef,const ElementField& vel,const CurrentFiniteElement& fe1, const CurrentFiniteElement& fe2, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  // Operator \intOmega coef (rot(u^n+1) x u^n) \cdot v - coef/2 (u^n+1 \cdot u^n) div(v)
  // Used in linearProblemNS :  \int_{\Omega} \rho rot(u^{n+1} x u^n \cdot v - \rho/2 u^{n+1} \cdot u^n) div(v)
  // with coef = \rho
  void convective_rot(const double coef, const ElementField& vel, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  // operator \int_{Sigma}  u^{n+1} Grad u^{n} \dot v
  void phi_grad_u(const double coef,const ElementField& vel,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  void phi_grad_u_minus_grad_u_phi(const double coef,const ElementField& vel,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  // Temam's operator
  // Operator \intOmega (\nabla \cdot u^n) u^n+1 v (Temam's operator to stabilize the convective term)
  void div_u_phi_j_phi_i(const double coef, const ElementField& vel, const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //! Operators \f$ \int_{\Sigma} (\grad \phi_i \dot n) \psi_j \f$, (and its transpose)
  //! where $\Sigma$ is a boundary element.  (For Nitsche's treatment...)
  void grad_phi_i_dot_n_psi_j(const CurrentFiniteElementWithBd& fewbd1, const CurrentFiniteElementWithBd& fewbd2, int iblockBd, int numblockBd, const std::size_t iblock, const std::size_t jblock,const std::size_t num, const double coef1,const double coef2);

  //! Operator \f$ \int_{\Sigma} (\grad \phi_i \dot n) (\grad \phi_j \dot n) \f$,
  //! where $\Sigma$ is a boundary element.  (For Nitsche's treatment...)
  void grad_phi_i_dot_n_grad_phi_j_dot_n(const double coef,const CurrentFiniteElementWithBd& fe, int iblockBd, int numblockBd, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  //! Operator \int_{\Sigma} (\grad \phi_i \dot n) (\grad \phi_j \dot n)
  //! Use to compute the ghost penalty stabilization term
  //! where $\Sigma$ is a boundary element.
  //! iBdPhi is the local id of the boundary for the element fewbdphi
  //! iBdPsi is the local id of the same boundary but for the element fewbdpsi
  void grad_phi_j_dot_n_grad_psi_i_dot_n(const double coef, const CurrentFiniteElementWithBd& fewbdphi, const CurrentFiniteElementWithBd& fewbdpsi, felInt iBdPhi, felInt iBdPsi, const std::size_t iblock, const std::size_t jblock, const std::size_t numBlock);

  //! Operator \int_{\Sigma} (\grad \phi_i) (\grad \phi_j)
  //! Use to compute the pressure jump for face-oriented stabilization
  //! where $\Sigma$ is a boundary element.
  //! iBdPhi is the local id of the boundary for the element fewbdphi
  //! iBdPsi is the local id of the same boundary but for the element fewbdpsi
  void grad_phi_j_grad_psi_i(const double coef, const CurrentFiniteElementWithBd& fewbdphi, const CurrentFiniteElementWithBd& fewbdpsi, felInt iBdPhi, felInt iBdPsi, const std::size_t iblock, const std::size_t jblock, const std::size_t numBlock);

  void div_phi_j_div_phi_i(const double coef,const CurrentFiniteElement& fe, const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  /*!
    Operator \f$ \int \psi_j \mbox{div} \phi_i \f$, multiplied by coef1 in blocks (iblock,jblock), ..., (iblock+numCoor,jblock),
    and \f$ \int \psi_i \mbox{div} \phi_j \f$ calculated by transposing the first term, multiplied by coef2 in blocks (jblock,iblock),...,(jblock,iblock+numCoor)
    Example : \int p \nabla \cdot v and \int q \nabla \cdot u in Stokes problem
    */
  void psi_j_div_phi_i_and_psi_i_div_phi_j (const CurrentFiniteElement& fe1,const CurrentFiniteElement& fe2, const std::size_t iblock, const std::size_t jblock,const double coef1,const double coef2);
  /*
      Operator \f$ coeff \int \psi_j \nabla \cdot \phi_i \f$
  */
  void psi_j_div_phi_i(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,const std::size_t iblock, const std::size_t jblock);
  /*
      Operator \f$ coeff \int \psi_i \nabla \cdot \phi_j \f$
  */
  void psi_i_div_phi_j(const double coef, const CurrentFiniteElement& fej,const CurrentFiniteElement& fei,const std::size_t iblock, const std::size_t jblock);

  //---------------------------------------------------------------------------------------------------------------
  // Some explanation about SUPG:
  // noting T:time, L:length, M:mass
  // WARNING: tau has a dimension of T*L^3*M^{-1} -> remember this when you use P1-P1
  // It's advised to choose your units knowing this.
  //---------------------------------------------------------------------------------------------------------------
  void stab_supg(const double dt, const double stab1, const double stab2, const double density, const double visc,const CurrentFiniteElement& fe, const ElementField& vel,const ElementField& rhs,ElementVector& elemVec, double& Re_elem,double& tau, const int type=1);

  void stab_supgNSHeat(const double dt, const double stab1, const double alpha, const double density, const double visc,  const CurrentFiniteElement& fe, const ElementField& vel, const int type,const std::vector<double> & gravity);

  void stab_supgAdvDiffCT(const double dt, const double stab1, const double stab2, const double density, const double visc, const CurrentFiniteElement& fe,const ElementField& vel);

  void stab_supgProjCT(const double dt, const double stab1, const double density, const double visc, const CurrentFiniteElement& fe,const ElementField& vel);

  void stab_supgMatrixFS(const double dt, const double stabSUPG, const double density, const double viscosity, const double c0, const double c1, const double c2, const CurrentFiniteElement& fe, const ElementField& vel);

  // outflow stabilization a la Bertoglio et al. (in progress)
  void f_dot_n_grad_phi_i_grad_phi_j(const double coef,const CurvilinearFiniteElement& fe, const ElementField& elfield,const std::size_t iblock, const std::size_t jblock,const std::size_t num);

  //void outflow_stabilization(const double coef, const CurvilinearFiniteElement& fe, const ElementField& elfield, const std::size_t iblock, const std::size_t jblock,const std::size_t num);
  // Operator: int_Sigma [ ( u^{n} \cdot normal ) ( u^{n+1}*v ) ]
  // If you want to apply outflow stabilization method as the following example:
  //      Example : if f_dot_n = u^{n} * normal < 0,
  //           adding - (rho/2) * int_Sigma [ ( u^{n} \cdot normal ) ( u^{n+1}*v ) ]
  //      You should activate stabilization flag in your data file (see Core/felisceParam.cpp -> addedBoundaryFlag
  void f_dot_n_phi_i_phi_j(const double coef, const CurvilinearFiniteElement& fe, const ElementField& elfield, const std::size_t iblock, const std::size_t jblock,const std::size_t num, int stabFlag = 0);

  // inflow stabilization for Nitsche-XFEM formulation
  // Same as f_dot_n_phi_i_phi_j but with currentFiniteElement instead of CurvilinearFiniteElement
  // Used in Nitsche-XFEM formulation
  // elt_f is the function f
  // elt_n is the "normal". This is a constant elementField with as many components as elt_f
  void f_dot_n_phi_i_phi_j(const double coef, const CurrentFiniteElement& fe, const ElementField& elt_f, const ElementField& elt_n, const std::size_t iblock, const std::size_t jblock, const std::size_t num, const felInt stabFlag);

  /*!
    Operator
    \int_{\Sigma} ( \|\phi \dot n \| - (\phi \dot n) ) \phi_i \phi_j
    for selecting the inflow boundary (where u.n < 0)
    */
  void abs_u_dot_n_phi_i_phi_j(const double coef, ElementField& vel, CurvilinearFiniteElement& febd, const std::size_t iblock, const std::size_t jblock, const int num);

  // Operator: coef * int_Sigma [ ( u \times normale ) ( v \times normale ) ] (in 3D)
  // To impose the tangential velocity to be zero (for now)
  // The labels must be specified in the datafile (see Core/felisceParam.cpp -> compTangBClabel )
  void phi_i_times_n_phi_j_times_n(const double coef, const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const int numComp);

  // Operator: coef * int_Sigma [ ( u \cdot normale ) ( v \cdot normale ) ]
  void phi_i_dot_n_phi_j_dot_n(const double coef, const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const int numComp);

  // Operator: coef * int_Sigma [ ( u \cdot tangent ) ( v \cdot tangent ) ]
  void phi_i_dot_t_phi_j_dot_t(const double coef, const CurvilinearFiniteElement& fe, const std::size_t iblock, const std::size_t jblock, const int numComp);

  // int_Sigma [coeff eps(phi_j)n dot n phi_i dot n ] (i=test function)
  void eps_phi_j_n_dot_n_phi_i_dot_n(const double coeff, const CurrentFiniteElementWithBd& fe,int iblockBd,int numblockBd,const std::size_t iblock,const std::size_t jblock, const std::size_t num);

  // int_Sigma [coeff psi_j phi_i dot n ] (i=test function) something similar already there in phi_i_psi_j_scalar_n
  void psi_j_phi_i_dot_n(const double coeff,const CurvilinearFiniteElement& feVel, const CurvilinearFiniteElement& fePres,const std::size_t iblock,std::size_t presBlock, const std::size_t num);

  //for each node the std::vector P*N is defined, then it is interpolated via the FE basis into the element
  void psi_j_phi_i_dot_n_on_nodes(const double coeff,const ElementField& normal, const CurvilinearFiniteElement& feVel, const CurvilinearFiniteElement& fePres,const std::size_t iblock, const std::size_t presBlock);

  void sGrad_psi_j_tensor_sGrad_phi_i(const double coef, const ElementField& normalField, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor);

  /**
    * @brief surface laplacian operator for scalar quantities
    * @param[in] coef coefficient in front of the integral
    * @param[in] fe finite element of the surface
    * @param[in] tensor the tensor between the covariant gradients: if you put the inverse of the first fundamental form you obtain a laplacian operator.
    * @param[in] blockId block where to put the result.
    */
  void sGrad_psi_j_tensor_sGrad_phi_i_for_scalar(const double coef, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor, const std::size_t blockId);

  void sGrad_psi_j_tensor_sGrad_phi_i(const double coef, const ElementField& elemCoef, const ElementField& normalField, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor);

  // used in the choroid
  void dpsi_j_dh_psi_i(const double coef, const CurrentFiniteElement& fei,const CurrentFiniteElement& fej,std::size_t h, const std::size_t iblock, const std::size_t jblock);

  void transpirationSurfStab( const double coef, const ElementField& elemCoef, const ElementField& normalField, const CurvilinearFiniteElement& fe, const UBlasMatrix& tensor);

  //  feVel1 correspond to the Volume element, while feVel2 to the surface element
  // this is used to create a rectangular elementary matrix
  ElementMatrix(const CurBaseFiniteElement& feVel1, const CurBaseFiniteElement& feVel2, std::size_t nbr1, std::size_t nbc1,
                const CurBaseFiniteElement& fePres, std::size_t nbr2, std::size_t nbc2);

  void followerPressure(const double coef, const ElementField& press, const ElementField& disp, const CurvilinearFiniteElement& fe);

  void velocityConstraint(const double coef, const ElementField& disp, const CurvilinearFiniteElement& fe, int iblock);

  //----------------------------------------------------------------------
  //! Operator for DG on faces \int_{\Sigma} \psi_j \phi_i $,
  //! where $\Sigma$ is a boundary element.
  //----------------------------------------------------------------------
  void psi_j_phi_i(const double coef, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  //! Operator for DG on faces $ \int_{\Sigma} psi_j_eps_phi_i_dot_vec $,
  //! where $\Sigma$ is a boundary element.
  //---------------------------------------------------------------------
  void psi_j_eps_phi_i_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  //! Operator for DG on faces $ \int_{\Sigma} psi_j_grad_phi_i_dot_vec $,
  //! where $\Sigma$ is a boundary element.
  //---------------------------------------------------------------------
  void psi_j_grad_phi_i_dot_vec(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  //! Operator for DG on faces $ \int_{\Sigma} eps_psi_j_dot_vec_phi_i $,
  //! where $\Sigma$ is a boundary element.
  //---------------------------------------------------------------------
  void eps_psi_j_dot_vec_phi_i(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  //! Operator for DG on faces $ \int_{\Sigma} grad_psi_j_dot_vec_phi_i $,
  //! where $\Sigma$ is a boundary element.
  //---------------------------------------------------------------------
  void grad_psi_j_dot_vec_phi_i(const double coef, const ElementField& vec, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const std::size_t iblock, const std::size_t jblock, const std::size_t num);

  //----------------------------------------------------------------------
  //! Operator for DG on faces $ \int_{\Sigma} f_dot_n_psi_j_phi_i $,
  //! where $\Sigma$ is a boundary element.
  // elt_f is the function f
  // elt_n is the "normal". This is a constant elementField with as many components as elt_f
  //---------------------------------------------------------------------
  void f_dot_n_psi_j_phi_i(const double coef, const CurrentFiniteElement& fePhi, const CurrentFiniteElement& fePsi, const ElementField& elt_f, const ElementField& elt_n, const std::size_t iblock, const std::size_t jblock, const std::size_t num, felInt stabFlag);

  void const_phi_i_dot_n(const double coeff,const CurvilinearFiniteElement& fePhi, /*const CurvilinearFiniteElement& fePres,*/const std::size_t iBlock, const std::size_t jBlock/*, std::size_t num*/);
  void const_phi_j_dot_n(const double coeff,const CurvilinearFiniteElement& fePhi, /*const CurvilinearFiniteElement& fePres,*/ const std::size_t iBlock, const std::size_t jBlock /*, std::size_t num*/);

  void const_phi_i(const double coeff, const CurvilinearFiniteElement& fePhi, const std::size_t iBlock, const std::size_t jBlock);
  void const_phi_j(const double coeff, const CurvilinearFiniteElement& fePhi, const std::size_t iBlock, const std::size_t jBlock);



  ///@}
  ///@name Access
  ///@{

  UBlasMatrix& mat() {
    return m_mat;
  }
  std::size_t numBlockRow() const {
    return m_numRow.size();
  }
  std::size_t numBlockCol() const {
    return m_numCol.size();
  }

  //std::size_t numRow() const {return m_numRow;}

  //std::size_t numCol() const {return m_numCol;}

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  void print(std::size_t verbose,std::ostream& c=std::cout);

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Private static Member Variables
  ///@{

  ///@}
  ///@name Private member Variables
  ///@{

  UBlasMatrix m_mat; //!< The element matrix
  std::vector<std::size_t> m_numRow; //!< m_numRow[i]=number of rows in the i-th block row
  std::vector<std::size_t> m_firstRow; //!< m_firstRow[i]=index of the first row of i-th block row
  std::vector<std::size_t> m_numCol; //!< m_numCol[i]=number of columns in the i-th block col
  std::vector<std::size_t> m_firstCol; //!< m_firstCol[i]=index of the first column of i-th block col

  // TODO:: All temp classes should be replaced with on time execution (otherwise much memory consumed)
  UBlasMatrix m_tmpMat; //!< temporary element matrix (same size as m_mat)
  UBlasVector m_tmpVecCoor;//!< temporary vector (size = numCoor)
  UBlasVector m_tmpVec2Coor;//!< temporary vector (size = numCoor)
  UBlasVector m_tmpVecDof;//!< temporary vector (size = numDof)
  UBlasVector m_tmpVec2Dof;//!< temporary vector (size = numDof)
  UBlasVector m_tmpVecQuadPoint; //!< temporary vector (size = numQuadraturePoint)

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Private LifeCycle
  ///@{

  ///@}
};
///@}
///@name Type Definitions
///@{

///@}
} /* namespace felisce.*/

#endif
