//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J-F. Gerbeau
//

// System includes
#include <iostream>

// External includes

// Project includes
#include "FiniteElement/geoElement.hpp"
#include "FiniteElement/basisFunction.hpp"

namespace felisce
{
GeoElement::GeoElement(const std::string& name,const RefShape& shape,const BasisFunction& basisFunction,const double* pointCoor,const int numEdge,const int numFace,
                        const int* pointOfEdge,const int* pointOfFace,const int* edgeOfFace,const bool* orientationOfEdge,
                        const GeoElement** boundaryGeoElement,const GeoElement** boundaryBoundaryGeoElement,const RefElement& defaultFiniteEle):
  m_name(name),m_shape(shape),m_basisFunction(basisFunction),m_numCoor(m_basisFunction.numCoor()),
  m_numPoint(m_basisFunction.size()),m_numEdge(numEdge),m_numFace(numFace),
  m_numBdEle( m_numCoor == 3 ? numFace : ( m_numCoor == 2 ? m_numEdge : (m_numCoor == 1 ? 2 : 0) )  ),
  m_pointCoor(pointCoor),
  m_ptOfEd(pointOfEdge),m_ptOfFa(pointOfFace),m_edOfFa(edgeOfFace),m_orientEd(orientationOfEdge),
  m_boundaryGeoElement(boundaryGeoElement),m_boundaryBoundaryGeoElement(boundaryBoundaryGeoElement),
  m_defaultFiniteEle(defaultFiniteEle)
{
  FEL_DEBUG("Constructing " << m_name << std::endl);

  m_numPointPerEdge = new int[m_numEdge];
  m_numPointPerFace = new int[m_numFace];
  m_numEdgePerFace = new int[m_numFace];
  m_indexPointPerEdge = new int[m_numEdge];
  m_indexPointPerFace = new int[m_numFace];
  m_indexEdgePerFace = new int[m_numFace];

  switch (m_numCoor) {
    case 0: // test for geoElement{NULL,Node} vm 2012/09
      // m_numEdge = m_numFace = 0
      break;
    case 1:
      // 1D edge = the element
      m_numPointPerEdge[0] = m_numPoint;
      m_indexPointPerEdge[0] = 0;
      //! create/update boundary element arrays: (bdEle=2 points)
      FEL_ASSERT( m_numBdEle == 2);
      m_ptOfBdEle = m_ptOfEd; //={0,1,...}: but only the extremal points {0,1} are used.
      m_numPointPerBdEle    = new int[2]; // 2 vertices per segment!...
      m_numPointPerBdEle[0] = 1;
      m_numPointPerBdEle[1] = 1;
      m_indexPointPerBdEle     = new int[2];
      m_indexPointPerBdEle[0]  = 0;
      m_indexPointPerBdEle[1]  = 1;
      break;
    case 2:
      // 2D Boundary = edges
      for(int i=0; i<m_numEdge; i++) {
        m_numPointPerEdge[i] = m_boundaryGeoElement[i]->numPoint();
      }
      m_indexPointPerEdge[0] = 0;
      for(int i=1; i<m_numEdge; i++) {
        m_indexPointPerEdge[i] = m_indexPointPerEdge[i-1] + m_boundaryGeoElement[i-1]->numPoint();
      }
      // 2D face = the element
      m_numPointPerFace[0] = m_numPoint;
      m_indexPointPerFace[0] = 0;
      m_numEdgePerFace[0]  = m_numEdge;
      m_indexEdgePerFace[0]  = 0;
      //! update boundary element pointers: (bdEle=edges)
      FEL_ASSERT( m_numBdEle == m_numEdge );
      m_ptOfBdEle = m_ptOfEd;
      m_numPointPerBdEle = m_numPointPerEdge;
      m_indexPointPerBdEle  = m_indexPointPerEdge;
      break;
    case 3:
      // 3D Boundary of boundary = edges
      for(int i=0; i<m_numEdge; i++) {
        m_numPointPerEdge[i] = m_boundaryBoundaryGeoElement[i]->numPoint();
      }
      m_indexPointPerEdge[0] = 0;
      for(int i=1; i<m_numEdge; i++) {
        m_indexPointPerEdge[i] = m_indexPointPerEdge[i-1] + m_boundaryBoundaryGeoElement[i-1]->numPoint();
      }
      // 3D Boundary = faces
      for(int i=0; i<m_numFace; i++) {
        m_numPointPerFace[i] = m_boundaryGeoElement[i]->numPoint();
        m_numEdgePerFace[i] = m_boundaryGeoElement[i]->numEdge();
      }
      m_indexPointPerFace[0] = 0;
      m_indexEdgePerFace[0] = 0;
      for(int i=1; i<m_numFace; i++) {
        m_indexPointPerFace[i] = m_indexPointPerFace[i-1] + m_boundaryGeoElement[i-1]->numPoint();
        m_indexEdgePerFace[i] = m_indexEdgePerFace[i-1] + m_boundaryGeoElement[i-1]->numEdge();
      }
      //! update boundary element pointers: (bdEle=edges)
      FEL_ASSERT( m_numBdEle == m_numFace );
      m_ptOfBdEle = m_ptOfFa;
      m_numPointPerBdEle = m_numPointPerFace;
      m_indexPointPerBdEle  = m_indexPointPerFace;
      break;
    default:
      std::cout << "m_numCoor = " << m_numCoor << std::endl;
      felisce_error("Incorrect number of coordinates", __FILE__, __LINE__);
      break;
  }
}

/***********************************************************************************/
/***********************************************************************************/

GeoElement::~GeoElement()
{
  delete [] m_numPointPerEdge;
  delete [] m_numPointPerFace;
  delete [] m_numEdgePerFace;
  delete [] m_indexPointPerEdge;
  delete [] m_indexPointPerFace;
  delete [] m_indexEdgePerFace;
}

/***********************************************************************************/
/***********************************************************************************/

const RefElement* GeoElement::defineFiniteEle(const GeometricMeshRegion::ElementType eltType, const int typeOfFiniteElement, GeometricMeshRegion& mesh) const
{
  switch (typeOfFiniteElement) {
  case 0:
    return &GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->defaultFiniteEle();
  case 1:
    return &GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltQuad[eltType]].second->defaultFiniteEle();
  case 2:
    // for bubble finite element, the boundaries are P1, we must not map them.
    if(mesh.domainDim() == m_numCoor)
      return &GeometricMeshRegion::eltEnumToFelNameGeoEle[GeometricMeshRegion::eltLinearToEltBubble[eltType]].second->defaultFiniteEle();
    else
      return &GeometricMeshRegion::eltEnumToFelNameGeoEle[eltType].second->defaultFiniteEle();
    //    case 3:
    //      return &refElementSegmentP3H;
  default:
    FEL_ERROR("Not yet implemented for this type of finite element option");
    break;
  }
  return nullptr;
}

/***********************************************************************************/
/***********************************************************************************/

void GeoElement::print(int verbose,std::ostream& c) const
{
  if(verbose) {
    c << "GeoElement: " << std::endl;
    c << " name: " << m_name << std::endl;
    c << " shape: " << m_shape.name() << std::endl;
    c << " numCoor: " << m_numCoor << std::endl;
    c << " numPoint: " << m_numPoint << std::endl;
    c << " numEdge: " << m_numEdge << std::endl;
    c << " numFace: " << m_numFace << std::endl;
    c << " numBdEle: " << m_numBdEle << std::endl;
    c << " numPointPerEdge: ";
    for(int ie=0; ie<m_numEdge; ie++) c << m_numPointPerEdge[ie] << " ";
    c << std::endl;
    c << " numPointPerFace: ";
    for(int jf=0; jf<m_numFace; jf++) c << m_numPointPerFace[jf] << " ";
    c << std::endl;
    c << " numEdgePerFace: ";
    for(int jf=0; jf<m_numFace; jf++) c << m_numEdgePerFace[jf] << " ";
    c << std::endl;
    c << " Point coordinates: " << std::endl;
    for(int ip=0; ip<m_numPoint; ip++) {
      c << "  ";
      std::cout << pointCoor(ip, 0) << ", " << pointCoor(ip, 1) << ", "  << pointCoor(ip, 2) << std::endl;
    }
    c << " Points of Edges: " << std::endl;
    for(int ie=0; ie<m_numEdge; ie++) {
      c << "  ";
      for(int ip=0; ip<m_numPointPerEdge[ie]; ip++) c << pointOfEdge(ie,ip) << " ";
      c << std::endl;
    }
    c << " Points of Faces: " << std::endl;
    for(int jf=0; jf<m_numFace; jf++) {
      c << "  ";
      for(int ip=0; ip<m_numPointPerFace[jf]; ip++) c << pointOfFace(jf,ip) << " ";
      c << std::endl;
    }
    c << " Edges of Faces (orientation in parenthesis): " << std::endl;
    bool orient;
    for(int jf=0; jf<m_numFace; jf++) {
      c << "  ";
      for(int ie=0; ie<m_numEdgePerFace[jf]; ie++) {
        int edge = edgeOfFace(jf,ie,orient);
        c << edge << " (" << orient << ") ";
      }
      c << std::endl;
    }
  }
}

/***********************************************************************************/
/***********************************************************************************/

#define OUTJFG
#ifndef OUTJFG

/************************************************************************
  *   QuadraticSegment
  *
  *     0-----2-----1
  *
  *************************************************************************/
static const int m_ptOfEdQuadSeg[3] = {
  0,1,2
}

static const double refcoor_P2_1D[ 9 ] = {
  0. , 0. , 0.,
  1. , 0. , 0.,
  0.5 , 0. , 0.
}

const GeoElement quadSeg("Quadratic Segment",SEGMENT,2,3,1,0,3,0,0,refcoor_P2_1D,m_ptOfEdQuadSeg,NULL,NULL,NULL,basisFunctionP2Seg,NULL);


/************************************************************************
  *   QuadraticTriangle
  *
  *           2
  *          / \
  *         /   \
  *        5     4
  *       /       \
  *      /         \
  *     0-----3-----1
  *
  *************************************************************************/
static const int m_ptOfEdQuadTria[9] = {
  0,1,3,  1,2,4, 2,0,5
}
static const int m_ptOfFaQuadTria[6] = {
  0,1,2,3,4,5
}
static const double refcoor_P2_2D[ 18 ] = {
  0. , 0. , 0.,
  1. , 0. , 0.,
  0. , 1. , 0.,
  0.5 , 0. , 0.,
  0.5 , 0.5 , 0.,
  0. , 0.5 , 0.
}
const GeoElement quadraticTriangle("Quadratic Triangle",TRIANGLE,2,6,3,1,3,6,3,refcoor_P2_2D,m_ptOfEdQuadTria,m_ptOfFaQuadTria,NULL,NULL,
                                    basisFunctionP2Tria,&quadSeg);

/*  GeoElement(ReferenceShape shape,int numCoor,int numPoint,int numEdge,int numFace,
  int numPointPerEdge,int numPointPerFace,int numEdgePerFace,
  const int* eToP,const int* fToP,const int* fToE,const BasisFunction& basisFunction):*/

static const double refcoor_P2_3D[ 30 ] = {
  0. , 0. , 0. ,
  1. , 0. , 0. ,
  0. , 1. , 0. ,
  0. , 0. , 1. ,
  0.5 , 0. , 0. ,
  0.5, 0.5 , 0. ,
  0. , 0.5 , 0. ,
  0. , 0. , 0.5,
  0.5, 0. , 0.5,
  0. , 0.5 , 0.5
}

static const double refcoor_Q1_3D[ 24 ] = {
  0. , 0. , 0. ,
  1. , 0. , 0. ,
  1. , 1. , 0. ,
  0. , 1. , 0. ,
  0. , 0. , 1. ,
  1. , 0. , 1. ,
  1. , 1. , 1. ,
  0. , 1. , 1.
}

//===================================================================================================
/*
  -- LinearQuad

  3-------2
  !       !
  !       !
  0-------1


  */
static const int m_ptOfEdBilinearQuad[ 8 ] = {
  0, 1, 1, 2, 2, 3, 3, 0
}
static const int m_ptOfFaBilinearQuad[4] = {
  0,1,2,3
}

static const double refcoor_Q1_2D[ 12 ] = {
  0. , 0. , 0.,
  1. , 0. , 0.,
  1. , 1. , 0.,
  0. , 1. , 0.
}

const GeoElement bilinearQuadrangle("Bilinear Quadrangle",QUAD,2,4,4,1,2,4,4,refcoor_Q1_2D,m_ptOfEdBilinearQuad,m_ptOfFaBilinearQuad,NULL,NULL,
                                    basisFunctionQ1Quad,&geoElementSegmentP1);

/*
  -- QuadraticQuad

  3---6---2
  !       !
  7   8   5
  !       !
  0---4---1

  */
static const int m_ptOfEdBiquadQuad[ 12 ] = {
  0, 1, 4, 1, 2, 5, 2, 3, 6, 3, 0, 7
}
static const int m_ptOfFaBiquadQuad[ 9 ] = {
  0, 1, 2, 3, 4, 5, 6, 7, 8
}

static const double refcoor_Q2_2D[ 27 ] = {
  0. , 0. , 0.,
  1. , 0. , 0.,
  1. , 1. , 0.,
  0. , 1. , 0.,
  0.5 , 0. , 0.,
  1. , 0.5 , 0.,
  0.5 , 1. , 0.,
  0. , 0.5 , 0.,
  0.5 , 0.5 , 0.
}

const GeoElement biquadQuadrangle("Biquadratic Quadrangle",QUAD,2,9,4,1,3,9,4,refcoor_Q2_2D,m_ptOfEdBiquadQuad,m_ptOfFaBiquadQuad,NULL,NULL,
                                  basisFunctionQ2Quad,&quadSeg);


//===================================================================================================
/*
  -- Prism

  5
  / \
  /   \
  3 ---- 4

  2
  / \
  /   \
  0 ---- 1

  */

static const int m_ptOfEdPrismP1P1[18] = {
  0,1,  1,2,  2,0,  3,4,  4,5,  5,3,  0,3,  1,4,  2,5
}

static const int m_ptOfFaPrismP1P1[18] = {
  0,1,2,  3,4,5,  0,1,4,3,  1,4,5,2,  0,3,5,2
}

static const double refcoor_PrismP1P1[ 18 ] = {
  0. , 0. , 0.,
  1. , 0. , 0.,
  0. , 1. , 0.,
  0. , 0. , 1.,
  1. , 0. , 1.,
  0. , 1. , 1.,
}
/*
  static const GeoElement* bdEle_Prism[ 5 ] =
  {
  &linearTriangle,  &linearTriangle, &bilinearQuadrangle,&bilinearQuadrangle,&bilinearQuadrangle
  }

  */
//const GeoElement prismP1P1("Prism P1 P1",PRISM,2,6,3,1,3,6,3,refcoor_PrismP1P1,m_ptOfEdPrismP1P1,m_ptOfFaPrismP1P1,NULL,NULL,
//              basisFunctionP2Tria,bdEle_Prism);
#endif
}
