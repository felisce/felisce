import os
import sys

class GitSafeConfigurator:
    def __init__(self, source_path):
        """
        Initialize the configurator with the source path.
        """
        self.source_path = os.path.abspath(source_path)
        self.directories = [
            self.source_path,
            os.path.join(self.source_path, "external/bitmap/bitmap"),
            os.path.join(self.source_path, "external/doctest/doctest"),
            os.path.join(self.source_path, "external/libMeshb/libMeshb"),
            os.path.join(self.source_path, "external/sciplot/sciplot"),
            os.path.join(self.source_path, "external/sciplot/sciplot/deps/doxystrap"),
            os.path.join(self.source_path, "external/sciplot/sciplot/deps/gnuplot-palettes"),
            os.path.join(self.source_path, "external/tabulate"),
            os.path.join(self.source_path, "external/getpot-cpp")
        ]

    def add_safe_directories(self):
        """
        Add the directories to Git's safe directory list.
        """
        for directory in self.directories:
            # print(f"Adding {directory} to Git's safe directory list...")
            os.system(f"git config --global --add safe.directory {directory}")

    def update_submodules(self):
        """
        Ensure all submodules are up-to-date.
        """
        # print("Ensuring that everything is up-to-date...")
        os.chdir(self.source_path)
        os.system("git submodule sync --recursive")
        os.system("git submodule update --init --recursive")
        os.system("git submodule update --remote")



def main():
    # Ensure the user provides the source path
    if len(sys.argv) > 1:
        source_path = sys.argv[1]
    else:
        print("Error: Please provide the source folder as an argument.")
        sys.exit(1)

    # Initialize the GitSafeConfigurator
    configurator = GitSafeConfigurator(source_path)
    # print("Source folder:", configurator.source_path)

    # Add the directories to Git's safe directory list
    configurator.add_safe_directories()

    # Ensure that all submodules are up-to-date
    configurator.update_submodules()

if __name__ == "__main__":
    main()

