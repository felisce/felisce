#!/bin/sh

# Adding modules
module purge
module load gnu8 mpich
module load cmake
module load boost
module load petsc/3.15.2
module load slepc/3.15.0
module load openblas
module load scalapack
module load mumps
module load metis
module load parmetis
module load pvm3
module load zeromq
module load lua

# Ensuring that everything is up-to-date
cd ..
git submodule sync --recursive
git submodule update --init --recursive
git submodule update --remote
cd build

# Set variables
export FELISCE_SOURCE="${FELISCE_SOURCE:-"$( cd "$(dirname "$0")" ; pwd -P )"/..}"
export FELISCE_BUILD="${FELISCE_SOURCE}/build"

# Set basic configuration
export FELISCE_BUILD_TYPE=${FELISCE_BUILD_TYPE:-"Release"}

# Folders to compile
export FOLDERS_TO_COMPILE=""
# export FOLDERS_TO_COMPILE="Core;DegreeOfFreedom;FiniteElement;Gamma;Geometry;Hyperelasticity;HyperelasticityLaws;InputOutput;Model;Solver;CurveGenerator"

# CVGraph library folder
export CVGRAPH_ROOT_DIR="$HOME/src/cvgraph/bin/Release/"

# Auxiliary sets
if [ -z "$FELISCE_WITH_ZMQ" ] ;then
    if [ "$FELISCE_WITH_PVM" = ON ] ; then
        export FELISCE_WITH_ZMQ=OFF
    else
        export FELISCE_WITH_ZMQ=ON
    fi
else
    if [ "$FELISCE_WITH_ZMQ" = ON ] ; then
        export FELISCE_WITH_PVM=OFF
    else
        export FELISCE_WITH_PVM=ON
    fi
fi

# Set compiler
export CC=gcc
export CXX=g++

# Set number of cores
export N_CORES=4

# Running tests
export RUN_TEST=true

# Clean
clear
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/cmake_install.cmake"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeCache.txt"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeFiles"

# Configure
cmake --no-warn-unused-cli ..                                                                       \
-H"${FELISCE_SOURCE}"                                                                               \
-B"${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}"                                                          \
-DCMAKE_BUILD_TYPE="${FELISCE_BUILD_TYPE}"                                                          \
-DCMAKE_EXPORT_COMPILE_COMMANDS=ON                                                                  \
-DCMAKE_UNITY_BUILD=ON                                                                              \
-DFELISCE_WITH_TESTS=ON                                                                                  \
-DN_CORES=${N_CORES}                                                                                \
-DFELISCE_WITH_LIBXFM=ON                                                                            \
-DFELISCE_WITH_SLEPC=ON                                                                             \
-DFELISCE_WITH_ZMQ=${FELISCE_WITH_ZMQ}                                                        \
-DFELISCE_WITH_PVM=${FELISCE_WITH_PVM}                                                              \
-DFELISCE_WITH_NSINRIA=ON                                                                           \
-DNSINRIA_DIR="$HOME/src/felisce-ns/"                                                               \
-DFELISCE_WITH_LUA=ON                                                                               \
-DFELISCE_WITH_CURVEGEN=ON                                                                          \
-DFELISCE_DATA_DIR="$HOME/src/data/"                                                                \
-DFELISCE_WITH_CVGRAPH=ON                                                                           \
-DCVGRAPH_ROOT_DIR=${CVGRAPH_ROOT_DIR}                                                              \
-DFELISCE_WITH_QT=OFF                                                                               \
-DBUILD_NIGHTLY_TESTS=ON                                                                            \
-DBUILD_EXAMPLES=ON                                                                                 \
-DEXAMPLES_DIR="$HOME/src/examples/"                                                                \
-DBUILD_BENCHMARKS=OFF                                                                              \
-DBENCHMARKS_DIR="$HOME/src/benchmark/"                                                             \
-DFOLDERS_TO_COMPILE=${FOLDERS_TO_COMPILE}                                                          \

# Compile with Unity
echo "Compile with Unity"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${FELISCE_SOURCE}/bin/${FELISCE_BUILD_TYPE}/lib:${CVGRAPH_ROOT_DIR}/lib
cmake --build "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" --target install    -- -j$(nproc)
#srun cmake --build "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" --target install    -- -j$(nproc)

# Running test
if [ "$RUN_TEST" = true ] ; then
    echo "Running test"
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${FELISCE_SOURCE}/bin/${FELISCE_BUILD_TYPE}/lib:${CVGRAPH_ROOT_DIR}/lib
    ctest
fi

# Exiting
exit 0
