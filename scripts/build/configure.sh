#!/bin/sh

# Ensuring that submodules are up-to-date
git submodule sync --recursive
git submodule update --init --recursive
git submodule update --remote

# Set variables
export FELISCE_SOURCE="${FELISCE_SOURCE:-"$( cd "$(dirname "$0")" ; pwd -P )"/..}"
export FELISCE_BUILD="${FELISCE_SOURCE}/build"

# Set basic configuration
export FELISCE_BUILD_TYPE=${FELISCE_BUILD_TYPE:-"Release"}

# Clean up bin folder if it exists
rm -rf "${FELISCE_SOURCE}/bin/${FELISCE_BUILD_TYPE}"

# Clean up build folder if it exists
rm -rf "${FELISCE_SOURCE}/build/${FELISCE_BUILD_TYPE}"

# Clean previous CMake files
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/cmake_install.cmake"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeCache.txt"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeFiles"

# Configure
cmake --no-warn-unused-cli ..              \
-H"${FELISCE_SOURCE}"                      \
-B"${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" \
-DCMAKE_BUILD_TYPE="${FELISCE_BUILD_TYPE}" \
-DCMAKE_UNITY_BUILD=ON                     \
-DPETSC_DIR="path_to_petsc"                \
-DPETSC_ARCH="petsc_arch"                  \
-DFELISCE_WITH_TESTS=ON                    \
-DFELISCE_WITH_NIGHTLY_TESTS=ON                                                                     

# Compile with Unity
cmake --build "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" --target install -- -j number_of_cores
