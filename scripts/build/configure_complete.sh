#!/bin/sh

# Ensuring that submodules are up-to-date
git submodule sync --recursive
git submodule update --init --recursive
git submodule update --remote

# Set variables
export FELISCE_SOURCE="${FELISCE_SOURCE:-"$( cd "$(dirname "$0")" ; pwd -P )"/..}"
export FELISCE_BUILD="${FELISCE_SOURCE}/build"

# Set basic configuration
export FELISCE_BUILD_TYPE=${FELISCE_BUILD_TYPE:-"Release"}

# External compilations
export EXTERNAL_EXECUTABLE_DIRS="path_to_external_compile_folder"
export EXTERNAL_INCLUDE_DIRS="path_to_external_include_dirs"
export EXTERNAL_LIBRARIES="path_to_external_libraries"

# Define the BUILD_FOR_CPACK flag
export BUILD_FOR_CPACK=OFF

# Clean up bin folder if exists
rm -rf "${FELISCE_SOURCE}/bin/${FELISCE_BUILD_TYPE}"

# Clean up build folder if exists
rm -rf "${FELISCE_SOURCE}/build/${FELISCE_BUILD_TYPE}"

# Clean
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/cmake_install.cmake"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeCache.txt"
rm -rf "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}/CMakeFiles"

# Configure 
cmake --no-warn-unused-cli ..                           \
-H"${FELISCE_SOURCE}"                                   \
-B"${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}"              \
-DCMAKE_BUILD_TYPE="${FELISCE_BUILD_TYPE}"              \
-DCMAKE_UNITY_BUILD=ON                                  \
-DFELISCE_CMAKE_VERBOSE=ON                              \
-DFELISCE_WITH_CURVEGEN=ON                              \
-DFELISCE_WITH_NSINRIA=ON                               \
-DNSINRIA_DIR="path_to_felisce-ns"                      \
-DBOOST_DIR="path_to_boost"                             \
-DMPI_DIR="path_to_mpi"                                 \
-DPETSC_DIR="path_to_petsc"                             \
-DPETSC_ARCH="petsc_arch"                               \
-DSCALAPACK_DIR="path_to_scalapack"                     \
-DMUMPS_DIR="path_to_mumps"                             \
-DMETIS_DIR="path_to_metis"                             \
-DPARMETIS_DIR="path_to_parmetis"                       \
-DFELISCE_WITH_LIBXFM=ON                                \
-DLIBXFM_DIR="path_to_libXfm"                           \
-DFELISCE_WITH_LUA=ON                                   \
-DLUA_DIR="path_to_lua"                                 \
-DFELISCE_WITH_PVM=OFF                                  \
-DLIBPVM_DIR="path_to_pvm"                              \
-DFELISCE_WITH_ZMQ=ON                                   \
-DZMQ_DIR="path_to_zmq"                                 \
-DZMQ_BINDING_DIR="path_to_zmq_binding"                 \
-DFELISCE_WITH_QT=ON                                    \
-DFELISCE_WITH_SCIPLOT=ON                               \
-DFELISCE_WITH_HYPRE=ON                                 \
-DHYPRE_DIR="path_to_hypre"                             \
-DFELISCE_WITH_SLEPC=ON                                 \
-DSLEPC_DIR="path_to_slepc"                             \
-DFELISCE_WITH_SUPERLU=ON                               \
-DSUPERLU_DIST_DIR="path_to_superlu_dist"               \
-DFELISCE_WITH_TESTS=ON                                 \
-DFELISCE_WITH_NIGHTLY_TESTS=OFF                        \
-DFELISCE_FOR_CI=ON                                     \
-DMASTERFSI_PVM="path_to_masterfsi_pvm"                 \
-DMASTERFSI_ZMQ="path_to_masterfsi_zmq"                 \
-DFELISCE_WITH_BENCHMARKS=ON                            \
-DBENCHMARKS_DIR="path_to_benchmarks"                   \
-DFELISCE_FOR_CPACK=${BUILD_FOR_CPACK}                  \
-DFELISCE_WITH_ASAN=ON                                  \
-DFELISCE_WITH_LSAN=ON                                  \
-DFELISCE_WITH_MSAN=ON                                  \
-DFELISCE_WITH_TSAN=ON                                  \
-DFELISCE_WITH_USAN=ON                                  \
-DEXTERNAL_LIBRARIES=${EXTERNAL_LIBRARIES}              \
-DEXTERNAL_INCLUDE_DIRS=${EXTERNAL_INCLUDE_DIRS}        \
-DEXTERNAL_EXECUTABLE_DIRS={EXTERNAL_EXECUTABLE_DIRS}

# Compile with Unity
if [ "$BUILD_FOR_CPACK" = OFF ] ; then
   cmake --build "${FELISCE_BUILD}/${FELISCE_BUILD_TYPE}" --verbose --target install -- -j number_of_cores
else
   cpack --config ${FELISCE_BUILD_TYPE}/CPackConfig.cmake
fi