# Import os
import os

# Import sys
import sys

# Import shutil
import shutil

# Import platform
import platform

# Import the script to compare
from compare_results import TestCompareEnsightResults

# Import additional parameters
try:
    from additional_test_parameters import AdditionalTestParameters
except ImportError:
    AdditionalTestParameters = None

class Color:
   """ANSI escape sequences for colored terminal output."""
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

class SimulationTestRunner:
    """Runs and validates simulation tests."""

    def __init__(self, test_name: str, exec_name: str, install_prefix: str, mpi_enabled: bool = False, mpi_program: str = "mpiexec", datafile: str = "datafile", reference: str = "RefSolution"):
        """Initializes the test runner with required parameters."""
        self.test_name        = test_name

        self.directory        = os.path.dirname(os.path.abspath(__file__))
        self.executable       = os.path.join(self.directory, exec_name)
        self.install_prefix   = os.path.abspath(install_prefix)

        self.mpi_enabled      = mpi_enabled
        self.mpi_program      = mpi_program

        self.datafile_name    = datafile
        self.reference_folder = reference

        self.number_of_cores  = self.determine_number_of_cores()
        self.run_test         = self.get_additional_parameter("run_test", True)
        self.allow_failure    = self.get_additional_parameter("allow_failure", False)

        self.custom_input     = None

    def validate_arguments(self):
        """Validates command-line arguments and captures custom input."""
        if len(sys.argv) > 2:
            raise ValueError(
                "Too many input arguments!\n"
                "Usage:\n"
                "    python3 run_unittest.py             # Default behavior\n"
                "    python3 run_unittest.py <input>     # With custom input\n"
            )
        if len(sys.argv) == 2:
            self.custom_input = sys.argv[1]

    def determine_number_of_cores(self):
        """Determines the optimal number of cores for execution.

        Order of priority:
        1. Environment variable 'FELISCE_CORES' (if valid)
        2. AdditionalTestParameters file (if exists)
        3. Default: Half of available CPU cores (minimum 1)
        """
        available_cores = max(1, os.cpu_count() // 2)
        number_of_cores = self.get_additional_parameter("number_of_cores", available_cores)

        env_cores = os.getenv("FELISCE_CORES", "").strip()
        if env_cores.isdigit():
            number_of_cores = min(int(env_cores), number_of_cores)

        return max(1, number_of_cores)

    def get_additional_parameter(self, attribute, default_value):
        """Retrieves an attribute from `AdditionalTestParameters`, falling back to default."""
        if AdditionalTestParameters:
            additional_params = AdditionalTestParameters()
            return getattr(additional_params, attribute, default_value)
        return default_value

    def get_command(self) -> str:
        """Constructs the command to execute based on system type and MPI settings."""
        machine_name = platform.node()
        command = f"{self.executable} -f {self.datafile_name}"
        
        if "CLEPS" in machine_name or "node" in machine_name:
            if self.mpi_enabled:
                command = f"--mpi=pmi2 -c {self.number_of_cores} {command}"
            command = f"srun {command}"
        else:
            if self.mpi_enabled:
                command = f"{self.mpi_program} -np {self.number_of_cores} {command}"
        
        return command

    def setup_library_path(self):
        """Sets up the appropriate library path based on the operating system."""
        libpath = os.path.join(self.install_prefix, "lib")
        return f"export {'DYLD_LIBRARY_PATH' if platform.system() == 'Darwin' else 'LD_LIBRARY_PATH'}={libpath}; "

    def execute_test(self):
        """Executes the test command and handles potential failure."""
        try:
            command = self.setup_library_path() + self.get_command()
            full_command = f"{command} {self.custom_input}" if self.custom_input else command
            result = os.system(full_command)
            if result != 0:
                raise RuntimeError("Test failed with exit code: {}".format(result))
        except Exception as e:
            print(f"{Color.BOLD}{Color.RED}Test execution failed: {e}{Color.END}")
            raise e

    def validate_reference_folder(self):
        """Ensures that the reference folder exists."""
        if not os.path.exists(self.reference_folder):
            raise FileNotFoundError(f"Folder '{self.reference_folder}' does not exist.")

    def get_result_files(self):
        """Retrieves the list of result files from the reference folder."""
        return [
            fname for _, _, file_list in os.walk(self.reference_folder)
            for fname in file_list if fname.endswith((".scl", ".vct"))
        ]

    def compare_results(self):
        """Compares test results with reference solutions."""
        self.validate_reference_folder()
        file_names = self.get_result_files()

        try:
            for file_name in file_names:
                first_file = os.path.join(self.directory, "Solution", file_name)
                second_file = os.path.join(self.reference_folder, file_name)
                print(f"Checking file:\t{file_name}")

                try:
                    tolerance = self.get_additional_parameter("tolerance", 1.e-5)
                    relative_error = self.get_additional_parameter("relative_error", False)
                    TestCompareEnsightResults(first_file, second_file, tolerance, relative_error).Compare()
                except ImportError:
                    TestCompareEnsightResults(first_file, second_file).Compare()
        except Exception as e:
            print(f"{Color.BOLD}{Color.RED}Comparison failed: {e}{Color.END}")
            raise e

    def cleanup(self):
        """Removes the Solution directory to clean up after the test."""
        shutil.rmtree(os.path.join(self.directory, 'Solution'), ignore_errors=True)

    def execute(self):
        """Runs the test if enabled, and verifies results."""
        if not self.run_test:
            print("WARNING: Test skipped!")
            return

        try:
            self.execute_test()
        except Exception as e:
            raise e

        try:
            self.compare_results()
        except Exception as e:
            raise e
            
        self.cleanup()

if __name__ == '__main__':
    runner = SimulationTestRunner(
        test_name="@test_name@", 
        exec_name="@exec_name@", 
        install_prefix="@install_prefix@", 
        mpi_enabled=@mpi_enabled@,
        mpi_program="@mpi_program@",
        datafile="@datafile_name@",
        reference="@reference_folder@"
    )

    runner.validate_arguments()

    if runner.allow_failure:
        try:
            runner.execute()
        except Exception:
            print(f"{Color.BOLD}{Color.RED}{runner.test_name} did not pass. Failure is allowed, please check the test.{Color.END}")
    else:
        try:
            runner.execute()
        except Exception as e:
            print(f"{Color.BOLD}{Color.RED}Test execution failed: {e}{Color.END}")
            sys.exit(1)
