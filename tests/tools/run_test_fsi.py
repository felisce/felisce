# Import os
import os

# Import sys
import sys

# Import shutil
import shutil

# Import platform
import platform

# Import the script to compare
from compare_results import TestCompareEnsightResults

# Import additional parameters
try:
    from additional_test_parameters import AdditionalTestParameters
except ImportError:
    AdditionalTestParameters = None

# Import the script to prepare FSI
from prepare_fsi import PrepareFSI

class Color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'

class FSITestRunner:
    """Runs and validates fluid structure interaction tests."""

    def __init__(self, test_name: str, install_prefix: str, mesh_folder: str, mpi_enabled: bool = False, mpi_program: str = "mpiexec", datafile: str = "datafile", reference: str = "RefSolution", compiled_with_pvm: bool = False, compiled_with_zmq: bool = False, master_fsi_pvm: str = "masterFSI", master_fsi_zmq: str = "masterFSI"):
        """Initializes the test runner with required parameters."""

        self.test_name        = test_name

        self.directory        = os.path.dirname(os.path.abspath(__file__))
        self.master_fsi       = ""
        self.install_prefix   = install_prefix
        self.mesh_folder      = mesh_folder

        self.mpi_enabled    = mpi_enabled
        self.mpi_program    = mpi_program

        self.master_datafile  = datafile
        self.reference_folder = reference
        
        self.run_test         = self.get_additional_parameter("run_test", True)
        self.allow_failure    = self.get_additional_parameter("allow_failure", False)
        
        self.compiled_with_pvm = compiled_with_pvm
        self.compiled_with_zmq = compiled_with_zmq

        self.master_fsi_pvm = master_fsi_pvm
        self.master_fsi_zmq = master_fsi_zmq

        self.custom_input = ""

        self.preparer = PrepareFSI(self.install_prefix, self.mesh_folder, self.master_datafile, self.mpi_enabled, self.mpi_program)

        success = self.preparer.Prepare()
        if not success:
            self.run_test = False

        self.configure_test()

        self.solution_folders = list(self.preparer.read_json_file("solvers.json").keys())

    def validate_arguments(self):
        """Validates command-line arguments and captures custom input."""
        if len(sys.argv) > 2:
            raise ValueError(
                "Too many input arguments!\n"
                "Usage:\n"
                "    python3 run_unittest.py             # Default behavior\n"
                "    python3 run_unittest.py <input>     # With custom input\n"
            )
        if len(sys.argv) == 2:
            self.custom_input = sys.argv[1]

    def configure_test(self) -> None:
        """Configure the test environment based on build options and verify the master FSI program."""

        if not self.run_test:
            return

        if not (self.compiled_with_pvm or self.compiled_with_zmq):
            self.run_test = False
            return

        # Select the appropriate master FSI program
        if self.compiled_with_pvm:
            self.master_fsi = self.master_fsi_pvm
        elif self.compiled_with_zmq:
            self.master_fsi = self.master_fsi_zmq
        else:
            raise ValueError("No valid FSI communication method defined.")
        
        # Check if the master_fsi is a valid executable path or an alias in PATH
        if shutil.which(self.master_fsi) is None:
            raise FileNotFoundError(f"The master FSI program '{self.master_fsi}' does not exist or is not executable.")

    def setup_library_path(self):
        """Sets up the appropriate library path based on the operating system."""
        libpath = os.path.abspath(os.path.join(self.install_prefix, "lib"))
        return f"export {'DYLD_LIBRARY_PATH' if platform.system() == 'Darwin' else 'LD_LIBRARY_PATH'}={libpath}; "

    def get_additional_parameter(self, attribute, default_value):
        """Retrieves an attribute from `AdditionalTestParameters`, falling back to default."""
        if AdditionalTestParameters:
            additional_params = AdditionalTestParameters()
            return getattr(additional_params, attribute, default_value)
        return default_value                    

    def get_command(self) -> str:
        """Constructs the command to execute based on system type and MPI settings."""
        machine_name = platform.node()
        command = f"{self.master_fsi} -f input_{self.master_datafile}"
        # command = f"{self.master_fsi} -f {self.directory}/input_{self.master_datafile}" todo: it seems that masterFSI doesn't accept absolute path 
        
        if "CLEPS" in machine_name or "node" in machine_name: # TODO
            self.run_test = False
        
        return command

    def execute_test(self):
        """Executes the test command and handles potential failure."""
        try:
            command = self.setup_library_path() + self.get_command()
            full_command = f"{command} {self.custom_input}" if self.custom_input else command
            result = os.system(full_command)
            if result != 0:
                raise RuntimeError("Test failed with exit code: {}".format(result))
        except Exception as e:
            print(f"{Color.BOLD}{Color.RED}Test execution failed: {e}{Color.END}")
            raise e

    def validate_reference_folder(self, reference_folder: str):
        """Ensures that the reference folder exists."""
        if not os.path.exists(reference_folder):
            raise FileNotFoundError(f"Folder '{reference_folder}' does not exist.")

    def get_result_files(self, reference_folder: str):
        """Retrieves the list of result files from the reference folder."""
        return [
            fname for _, _, file_list in os.walk(reference_folder)
            for fname in file_list if fname.endswith((".scl", ".vct"))
        ]

    def compare_results(self):
        """Compares test results with reference solutions."""
        for solution_folder in self.solution_folders:

            folder_reference_solutions = os.path.join(self.directory, self.reference_folder + solution_folder)
            folder_solutions = os.path.join(self.directory, "Solution" + solution_folder)

            self.validate_reference_folder(folder_reference_solutions)
            self.validate_reference_folder(folder_solutions)

            file_names = self.get_result_files(folder_reference_solutions)

            # Check result files
            for file_name in file_names:
                first_file_name  = os.path.join(folder_solutions, file_name)
                second_file_name = os.path.join(folder_reference_solutions, file_name)
                print("Checking file:\t", file_name)

                try:
                    tolerance = self.get_additional_parameter("tolerance", 1.e-5)
                    relative_error = self.get_additional_parameter("relative_error", False)
                    TestCompareEnsightResults(first_file_name, second_file_name, tolerance, relative_error).Compare()
                except:
                    TestCompareEnsightResults(first_file_name, second_file_name).Compare()

    def cleanup(self):
        """Removes the Solution directory to clean up after the test."""
        for solution_folder in self.solution_folders:
            shutil.rmtree(os.path.join(self.directory, "Solution" + solution_folder), ignore_errors=True)

    def execute(self):
        """Runs the test if enabled, and verifies results."""

        if not self.run_test:
            print("WARNING: Test skipped!")
            return
                
        try:
            self.execute_test()
        except Exception as e:
            raise e

        try:
            self.compare_results()
        except Exception as e:
            raise e
            
        self.cleanup()

if __name__ == '__main__':

    ON = True
    OFF = False

    runner = FSITestRunner(
        test_name="@test_name@", 
        install_prefix="@install_prefix@",
        mesh_folder="@mesh_folder@",
        mpi_enabled=@mpi_enabled@,
        mpi_program="@mpi_program@",
        datafile="@datafile_name@",
        reference="@reference_folder@",
        compiled_with_pvm=@FELISCE_WITH_PVM@,
        compiled_with_zmq=@FELISCE_WITH_ZMQ@,
        master_fsi_pvm="@master_fsi_program_pvm@",
        master_fsi_zmq="@master_fsi_program_zmq@"
    )

    runner.validate_arguments()

    if runner.allow_failure:
        try:
            runner.execute()
        except Exception:
            print(f"{Color.BOLD}{Color.RED}{runner.test_name} did not pass. Failure is allowed, please check the test.{Color.END}")
    else:
        try:
            runner.execute()
        except Exception as e:
            print(f"{Color.BOLD}{Color.RED}Test execution failed: {e}{Color.END}")
            sys.exit(1)
