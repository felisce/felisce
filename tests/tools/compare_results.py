# Import unnitest
import unittest

# Impor numpy
import numpy as np

# Import re
import re

# Import sys
import sys

class TestCompareEnsightResults(unittest.TestCase):
    """
    This class can be used in order to compare solution with Ensight files

    Only the member variables listed below should be accessed directly.

    Public member variables:
    first_file_name -- the solution obtained
    second_file_name -- the reference solution
    space_length_ensight -- the number of spaces considered in the comparison
    tolerance -- the tolerance solution
    """
    def __init__(self, first_file_name, second_file_name, tolerance = 1.0e-5, relative_error = False):
        """ The default constructor of the class

        Keyword arguments:
        first_file_name -- the solution obtained
        second_file_name -- the reference solution
        tolerance -- the tolerance solution
        """
        self.first_file_name = first_file_name
        self.second_file_name = second_file_name
        self.space_length_ensight = 12
        self.tolerance = tolerance
        self.relative_error = relative_error

    def Compare(self):
        """ This method compares the solution

        Keyword arguments:
        self -- It signifies an instance of a class.
        """

        # Read log file and load data
        node_values = []
        with open(self.first_file_name) as f:
            for line in f:
                if not line.startswith(('S','V')):
                    node_values.append(line)

        obtained_solution = []
        for j in range(len(node_values)):
            A = re.findall('.{%d}' % self.space_length_ensight, node_values[j])
            for i in range(len(A)):
                obtained_solution.append(eval(A[i]))

        obtained_solution = np.array(obtained_solution)

        # Read Reference file:
        node_values = []
        with open(self.second_file_name) as f:
            for line in f:
                if not line.startswith(('S','V')):
                    node_values.append(line)

        reference_solution = []
        for j in range(len(node_values)):
            A = re.findall('.{%d}' % self.space_length_ensight, node_values[j])
            for i in range(len(A)):
                reference_solution.append(eval(A[i]))

        reference_solution = np.array(reference_solution)

        # Check length
        self.assertTrue(obtained_solution.shape == reference_solution.shape)

        # Compare solution
        error_array = []
        for sol,ref in zip(reference_solution, obtained_solution):
            if abs(ref) > sys.float_info.epsilon and self.relative_error:
                error_array.append((sol - ref)/ref)
            else:
                error_array.append(sol - ref)
        error_array = np.array(error_array)
        self.assertLess(np.linalg.norm(error_array)/np.sqrt(reference_solution.shape[0]), self.tolerance)

if __name__ == '__main__':
    from sys import argv

    if len(argv) > 4:
        err_msg =  'Too many input arguments!\n'
        err_msg += 'Use this script in the following way (tolerance is not mandatory):\n'
        err_msg += '    "python3 compare_results_files.py solution.scl reference.scl 1.0e-5"\n'
        raise Exception(err_msg)

    first_file_name = argv[1]
    second_file_name = argv[2]
    if len(argv) > 3:
        tolerance = argv[3]
        TestCompareEnsightResults(first_file_name, second_file_name, tolerance).Compare()
    else:
        TestCompareEnsightResults(first_file_name, second_file_name).Compare()
