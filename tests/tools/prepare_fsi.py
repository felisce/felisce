# Import os
import os

# Import sys
import sys

# Import platform
import platform

# Import stat
import stat

# Import json
import json

# Import additional parameters
try:
    from additional_test_parameters import AdditionalTestParameters
except ImportError:
    AdditionalTestParameters = None

class PrepareFSI:
    """ This class prepares the FSI tests """

    def __init__(self, install_prefix: str, mesh_folder: str, master_datafile: str, mpi_enabled: bool = False, mpi_program: str = "mpiexec"):
        """ The default constructor of the class"""
      
        self.directory       = os.path.dirname(os.path.abspath(__file__))
        self.install_prefix  = os.path.abspath(install_prefix)
        self.mesh_folder     = os.path.abspath(mesh_folder)
        self.master_datafile = master_datafile
        self.mpi_enabled     = mpi_enabled
        self.mpi_program     = mpi_program

        self.number_of_cores      = self._determine_number_of_cores()
        self.half_number_of_cores = self._determine_half_number_of_cores()

        self.json_file = self.read_json_file("solvers.json")

    def _get_additional_parameter(self, attribute, default_value):
        """Retrieves an attribute from `AdditionalTestParameters`, falling back to default."""
        if AdditionalTestParameters:
            additional_params = AdditionalTestParameters()
            return getattr(additional_params, attribute, default_value)
        return default_value

    def _determine_number_of_cores(self):
        """Determines the optimal number of cores for execution.

        Order of priority:
        1. Environment variable 'FELISCE_CORES' (if valid)
        2. AdditionalTestParameters file (if exists)
        3. Default: Half of available CPU cores (minimum 1)
        """
        available_cores = max(1, os.cpu_count() // 2)
        number_of_cores = self._get_additional_parameter("number_of_cores", available_cores)

        env_cores = os.getenv("FELISCE_CORES", "").strip()
        if env_cores.isdigit():
            number_of_cores = min(int(env_cores), number_of_cores)

        return max(1, number_of_cores)

    def _determine_half_number_of_cores(self):
        """Calculates half the number of cores and ensures it's at least 1."""
    
        return max(int(self.number_of_cores / 2), 1)

    def _update_input_file(self, filename: str, placeholder: str, replacement: str) -> None:
        """Update the given input file by replacing a placeholder with the specified replacement value.

        Args:
            filename (str): Name of the input file to update.
            placeholder (str): Placeholder string to replace (e.g., "{{TEST_FOLDER}}" or "{{MESH_FOLDER}}").
            replacement (str): Value to insert in place of the placeholder.
        """
        input_file_path = os.path.join(self.directory, filename)

        try:
            with open(input_file_path, "r") as file:
                content = file.read()

            updated_content = content.replace(placeholder, replacement)

            with open(input_file_path, "w") as file:
                file.write(updated_content)

        except FileNotFoundError:
            print(f"Error: The file '{input_file_path}' does not exist.")
        except Exception as e:
            print(f"An error occurred while updating '{input_file_path}': {e}")

    def _update_input_file_placeholders(self):

        for domain in self.json_file:
            self._update_input_file("input_"+domain, "{{MESH_FOLDER}}", self.mesh_folder)
        
        self._update_input_file("input_"+self.master_datafile, "{{TEST_FOLDER}}", self.directory)

    def _get_command(self, domain: str, executable: str) -> str:
        """Constructs the command to execute based on system type and MPI settings."""
        machine_name = platform.node()

        # Construct the command
        command = (
            f"{executable} "
            f"-f {os.path.join(self.directory, f'input_{domain.lower()}')} "
            f"&> {os.path.join(self.directory, f'output_{domain.lower()}')}"
        )
        
        if "CLEPS" in machine_name or "node" in machine_name:
            if self.mpi_enabled:
                command = f"--mpi=pmi2 -c {self.half_number_of_cores} {command}"
            command = f"srun {command}"
        else:
            if self.mpi_enabled:
                command = f"{self.mpi_program} -np {self.half_number_of_cores} {command}"
        
        return command

    def _setup_library_path(self):
        """Sets up the appropriate library path based on the operating system."""
        libpath = os.path.abspath(os.path.join(self.install_prefix, "lib"))
        return f"export {'DYLD_LIBRARY_PATH' if platform.system() == 'Darwin' else 'LD_LIBRARY_PATH'}={libpath}\n"

    def read_json_file(self, file_name: str):
        """Reads and parses the JSON file."""
        try:
            with open(file_name, "r") as f:
                return json.load(f)
        except FileNotFoundError:
            print(f"Error: '{file_name}' file not found.")
            return None
        except json.JSONDecodeError:
            print(f"Error: Failed to parse '{file_name}'.")
            return None

    def _get_executable_path(self, domain: str, exec_name: str):
        """Constructs the path to the executable."""
        return os.path.join(self.install_prefix, "tests", "fsi", domain, exec_name, exec_name)

    def _check_executable_exists(self, executable: str):
        """Checks if the executable exists."""
        if not os.path.isfile(executable):
            print(f"Error: Executable '{executable}' not found.")
            return False
        return True

    def _create_shell_file(self, domain: str, executable: str):
        """Creates the shell file to run the executable."""
        lower_domain = domain.lower()
        shell_file_path = os.path.join(self.directory, f"{lower_domain}.sh")

        try:
            with open(shell_file_path, "w") as f:
                f.write("#!/bin/bash\n")
                f.write(self._setup_library_path())
                f.write(self._get_command(domain, executable))
        except IOError as e:
            print(f"Error: Failed to create or write to '{shell_file_path}': {e}")
            return None

        return shell_file_path

    def _make_executable(self, shell_file_path: str):
        """Sets the shell file as executable."""
        try:
            st = os.stat(shell_file_path)
            os.chmod(shell_file_path, st.st_mode | stat.S_IEXEC)
        except Exception as e:
            print(f"Error: Failed to make '{shell_file_path}' executable: {e}")
            return False
        return True
    
    def _create_solution_folders(self) -> None:
        """Create 'Solution<domain>' folders for each domain in the JSON file.

        Args:
            json_file (dict): A dictionary containing domain names as keys.
        """
        for domain in self.json_file:
            folder_name = f"Solution{domain}"
            os.makedirs(folder_name, mode=0o777, exist_ok=True)

    def Prepare(self):
        """Prepares the files by reading the JSON configuration and creating shell files."""
        
        self._update_input_file_placeholders()

        self._create_solution_folders()

        for domain, exec_name in self.json_file.items():
            executable = self._get_executable_path(domain.lower(), exec_name)

            if not self._check_executable_exists(executable):
                return False

            shell_file_path = self._create_shell_file(domain, executable)

            if not self._make_executable(shell_file_path):
                return False

        return True

if __name__ == '__main__':

    fsi_preparer = PrepareFSI(
        install_prefix="@install_prefix@", 
        mesh_folder="@mesh_folder@", 
        mpi_enabled=@mpi_enabled@,
        mpi_program="@mpi_program@"
    )

    fsi_preparer.Prepare()
