"""
This script runs a unit test executable with optional custom input.
It sets up the execution environment, handles MPI execution if needed, and ensures proper
library path configurations depending on the operating system.

Usage:
    python3 run_unittest.py           # Runs with default behavior
    python3 run_unittest.py <input>   # Runs with custom input
"""

import os
import sys
import platform

class UnitTestRunner:

    def __init__(self, exec_name: str, install_prefix: str, mpi_enabled: bool = False, mpi_program: str = "mpiexec"):
        """ The constructor of the class"""
        self.directory      = os.path.dirname(os.path.abspath(__file__))
        self.executable     = os.path.join(self.directory, exec_name)
        self.install_prefix = os.path.abspath(install_prefix)

        self.mpi_enabled    = mpi_enabled
        self.mpi_program    = mpi_program
        self.num_cores      = self.get_num_cores()

        self.custom_input   = ""


    def get_num_cores(self) -> int:
        """Returns the minimum of available cores and the cores specified in FELISCE_CORE environment variable."""
        # Get the number of available cores
        available_cores = max(1, os.cpu_count() // 2)

        # Get the value from the FELISCE_CORE environment variable, if set
        felisce_cores = os.getenv("FELISCE_CORE")
        
        # Set num_cores to the minimum of available_cores and FELISCE_CORE (if set)
        if felisce_cores is not None:
            try:
                return min(available_cores, int(felisce_cores))
            except ValueError:
                print(f"Invalid value for FELISCE_CORE: {felisce_cores}. Using {available_cores} cores instead.")
                return available_cores
        else:
            # Default to available_cores if FELISCE_CORE is not set
            return available_cores


    def validate_arguments(self):
        """Validates command-line arguments."""
        if len(sys.argv) > 2:
            error_message = (
                "Too many input arguments!\n"
                "Use this script in the following way:\n"
                "- With default behavior (nothing is saved):\n"
                "    python3 run_unittest.py\n"
                "- With custom input:\n"
                "    python3 run_unittest.py <input>\n"
            )
            raise ValueError(error_message)
        
        if len(sys.argv) == 2:
            self.custom_input = sys.argv[1]


    def get_command(self) -> str:
        """Constructs the command to execute based on system type and MPI settings."""
        machine_name = platform.node()
        command = self.executable
        
        if "CLEPS" in machine_name or "node" in machine_name:
            if self.mpi_enabled:
                command = f"--mpi=pmi2 -c {self.num_cores} {command}"
            command = f"srun {command}"
        else:
            if self.mpi_enabled:
                command = f"{self.mpi_program} -np {self.num_cores} {command}"
        
        return command

    def setup_library_path(self) -> str:
        """Sets up the appropriate library path based on the operating system."""
        libpath = os.path.join(self.install_prefix, "lib")
        
        if platform.system() == "Darwin":
            return f"export DYLD_LIBRARY_PATH={libpath}; "
        else:
            return f"export LD_LIBRARY_PATH={libpath}; "


    def run(self):
        """Executes the test command."""
        try:
            command = self.setup_library_path() + self.get_command()
            result = os.system(f"{command} {self.custom_input}")
            # if result != 0:
            #     sys.exit("Test failed!")
        except Exception as e:
            sys.exit(f"Test failed! Error: {e}")


if __name__ == "__main__":
    
    runner = UnitTestRunner(exec_name="@exec_name@", 
                            install_prefix="@install_prefix@", 
                            mpi_enabled=@mpi_enabled@,
                            mpi_program="@mpi_program@"
                            )
    
    runner.validate_arguments()
    
    runner.run()