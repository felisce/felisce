# Simulation tests

Simulations run in order to check the proper work of FELiScE:

- **small_tests**: Short tests (max. 5s)
- **nightly_tests**: Larger tests
