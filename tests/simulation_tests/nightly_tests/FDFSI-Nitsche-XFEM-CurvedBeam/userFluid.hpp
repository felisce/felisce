//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: B. Fabreges  
//

#ifndef _UserFluid_HPP
#define _UserFluid_HPP

// System includes
#include <math.h>

// External includes

// Project includes
#include "Solver/linearProblemFSINitscheXFEMFluid.hpp"
#include "Core/felisceParam.hpp"

namespace felisce
{
class HeartValves{
public:
  double operator() (int iComp, double /*x*/, double y, double /*z*/, double t) const
  {
    if(iComp == 0){
      return (5 * std::sin(2 * M_PI * t +1.1)) * y * (1.61 - y);
    }
    else{
      return 0.;
    }
  }
};


class UserFluid:
  public LinearProblemFSINitscheXFEMFluid
{
public:
  UserFluid();
  ~UserFluid();
  void userFinalizeEssBCTransient() override;

  void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;

private:
  HeartValves m_heartValveVelocityProfile;
};
}

#endif
