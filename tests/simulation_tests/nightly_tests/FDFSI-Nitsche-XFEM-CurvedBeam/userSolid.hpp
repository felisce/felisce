//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: B. Fabreges  
//

#ifndef _UserSolidCurvedBeam_HPP
#define _UserSolidCurvedBeam_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemElasticCurvedBeam.hpp"
#include "Solver/linearProblemFSINitscheXFEMSolid.hpp"

namespace felisce
{
  class UserSolid:
    public LinearProblemFSINitscheXFEMSolid<LinearProblemElasticCurvedBeam>
  {
  public:
    UserSolid();
    ~UserSolid();

  private:
    void userSetDirichletBC() override;
  };
}

#endif
