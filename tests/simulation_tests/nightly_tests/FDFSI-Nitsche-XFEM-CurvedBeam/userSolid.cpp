//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: B. Fabreges  
//

// System includes

// External includes

// Project includes
#include "userSolid.hpp"

namespace felisce
{
UserSolid::UserSolid():
  LinearProblemFSINitscheXFEMSolid<LinearProblemElasticCurvedBeam>()
{
  m_leftSide = 0;
  m_rightSide = 0;
}

UserSolid::~UserSolid()
{}

void UserSolid::userSetDirichletBC() {
  // apply BC for the displacement
  m_applyDirichletBC[0] = true;
  
  // apply BC for the rotation
  m_applyDirichletBC[1] = true;
  
  // homogeneous Dirichlet BC for all extremeties of the solid that are not front points
  for(std::size_t i=0; i<m_valBC[0].size(); ++i)
    m_valBC[0][i] = 0.;

  for(std::size_t i=0; i<m_valBC[1].size(); ++i)
    m_valBC[1][i] = 0.;
}
}
