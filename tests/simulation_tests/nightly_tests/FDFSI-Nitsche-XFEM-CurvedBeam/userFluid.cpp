//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: B. Fabreges  
//

// System includes

// External includes

// Project includes
#include "userFluid.hpp"
#include "FiniteElement/elementVector.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{
  UserFluid::UserFluid():
    LinearProblemFSINitscheXFEMFluid()
  {}
  
  UserFluid::~UserFluid()
  {}
  
  void UserFluid::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES)
  {    
    // calling initialize in LinearProblemFDFSI
    LinearProblemFSINitscheXFEMFluid::initialize(mesh, fstransient, comm, doUseSNES);
  }
  
  void UserFluid::userFinalizeEssBCTransient(){
    m_boundaryConditionList[0]->setValueVec(this, m_heartValveVelocityProfile, m_fstransient->time);
  }
}
