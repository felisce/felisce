//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:   
//

// System includes

// External includes

// Project includes
#include "Model/NSModel.hpp"
#include "userNSALE.hpp"
#include "userHarmonicExtension.hpp"
#include "Core/felisceTransient.hpp"
#include "Core/felisceParam.hpp"

using namespace felisce;

int main(const int argc, const char** argv) {
  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  // create model and solver
  NSModel* model;
  std::vector<LinearProblem*> linearPb;
  model = new NSModel();
  model->initializeModel(opt,fstransient);
  linearPb.push_back(new UserNSALE());
  if ( FelisceParam::instance().useALEformulation )
    linearPb.push_back(new UserHarmonicExtension());
  model->initializeLinearProblem(linearPb);

  // Fictitious domain
  std::vector<double> disp, vel;
  if ( FelisceParam::instance().hasImmersedData ){
    model->immersed().setInterfaceExchangedData(&disp,&vel,nullptr,nullptr);
  }

  //Loop time
  model->SolveDynamicProblem();

  delete model;
}

