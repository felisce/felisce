//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _PenalisationUserNS_HPP
#define _PenalisationUserNS_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "InputOutput/io.hpp"
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {

  class VelocityFunc {
  public:
    double operator() (int iComp, double x, double y, double z, double t) const
    {
      IGNORE_UNUSED_ARGUMENT(x);
      IGNORE_UNUSED_ARGUMENT(z);
      if(iComp == 0){
        return 5 * (std::sin(2 * std::acos(-1.) * t) +1.1) * y * (1.61 - y);
      }
      else{
        return 0.;
      }
    }
  };


class PenalisationUserNS
  : public LinearProblemNS
{
public:
    PenalisationUserNS(): LinearProblemNS() {};
    ~PenalisationUserNS() override= default;;

    void userFinalizeEssBCTransient() override {
      m_boundaryConditionList[0]->setValueVec(this, m_velFunc, m_fstransient->time);
    }

private:
    VelocityFunc m_velFunc;

  };
}


#endif
