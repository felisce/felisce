class AdditionalTestParameters:
    def __init__(self):
        """ The default constructor of the class
        """
        self.tolerance = 1.5e-3
        self.relative_error = True
