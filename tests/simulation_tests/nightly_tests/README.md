# Nightly simulations tests

Larger simulations that check that the whole simulation process works properly. List of tests:

- [**Autoregulation**](Autoregulation)
- [**BifurcationWindkesselMonolithic**](BifurcationWindkesselMonolithic)
- [**FDFSI-Nitsche-XFEM**](FDFSI-Nitsche-XFEM)
- [**FDFSI-Nitsche-XFEM-CurvedBeam**](FDFSI-Nitsche-XFEM-CurvedBeam)
- [**Hyperelasticity**](Hyperelasticity)
- [**IopNSCoupled**](IopNSCoupled)
- [**NS_ALE**](NS_ALE)
- [**NS_ALE_FracStep**](NS_ALE_FracStep)
- [**NS_ALE_Iter**](NS_ALE_Iter)
- [**NS_LinearRIS**](NS_LinearRIS)
- [**StaticPenalisation**](StaticPenalisation)
