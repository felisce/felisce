//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: B. Fabreges  
//

// System includes
#include <vector>

// External includes

// Project includes
#include "userSolid.hpp"
#include "userFluid.hpp"
#include "Model/FSINitscheXFEMModel.hpp"
#include "Core/felisceTransient.hpp"
#include "Core/felisceParam.hpp"

using namespace felisce;

int main(const int argc, const char** argv)
{
  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  // create model and solver
  FSINitscheXFEMModel<LinearProblemElasticString> model;
  model.initializeModel(opt, fstransient);

  std::vector<LinearProblem*> solver;
  solver.push_back(new UserFluid());
  solver.push_back(new UserSolid());
  model.initializeLinearProblem(solver);
  
  // time loop
  model.SolveDynamicProblem();		
}	
