//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:  M.A. Fernández  
//

#ifndef _UserHarmonicExtension_HPP
#define _UserHarmonicExtension_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHarmonicExtension.hpp"

// Test 1 //
class HarmonicMyFunctionDirichletTransientVaryingInSpace2D {
public:
  double operator() (double x,double y,double z,double t) const {
    IGNORE_UNUSED_ARGUMENT(y);
    IGNORE_UNUSED_ARGUMENT(z);
    return -0.2*std::sin((x*3.14159)/6)*std::sin(t*3.14159);
  }
};

// Test 2 //
class HarmonicMyFunctionDirichletTransientVaryingInSpace3D_Comp1 {
public:
  double operator() (double x,double y,double z,double t) const {
    return -(0.2*x*std::sin(3.14159*t)*std::sin((3.14159*z)/5))/std::sqrt(x*x + y*y);
  }
};

class HarmonicMyFunctionDirichletTransientVaryingInSpace3D_Comp2 {
public:
  double operator() (double x,double y,double z,double t) const {
    return (0.2*y*std::sin(3.14159*t)*std::sin((3.14159*z)/5))/std::sqrt(x*x + y*y);
  }
};

// Test 3 //
class HarmonicMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp1 {
public:
  double operator() (double x,double y,double z,double t) const {
    return (0.2*x*std::sin(3.14159*t)*std::sin((3.14159*z)/10.))/std::sqrt(x*x + y*y);
  }
};

class HarmonicMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp2 {
public:
  double operator() (double x,double y,double z,double t) const {
    return (0.2*y*std::sin(3.14159*t)*std::sin((3.14159*z)/10.))/std::sqrt(x*x + y*y);
  }
};

// Test 4 //
class HarmonicMyFunctionDirichletTransientVaryingInSpace3DNormal_Comp1 {
public:
  double operator() (double x,double y,double z,double t) const {
    IGNORE_UNUSED_ARGUMENT(x);
    IGNORE_UNUSED_ARGUMENT(y);
    return -std::sin(z)*std::sin(t/(3.14159));
  }
};

namespace felisce {
  class UserHarmonicExtension:
    public LinearProblemHarmonicExtension {
  public:
    UserHarmonicExtension();
    ~UserHarmonicExtension() override;
    void userFinalizeEssBCTransient() override;

  protected:
    HarmonicMyFunctionDirichletTransientVaryingInSpace2D m_fct2D;
    HarmonicMyFunctionDirichletTransientVaryingInSpace3D_Comp1 m_fct3DComp1;
    HarmonicMyFunctionDirichletTransientVaryingInSpace3D_Comp2 m_fct3DComp2;
    HarmonicMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp1 m_fct3DComp1Half;
    HarmonicMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp2 m_fct3DComp2Half;
    HarmonicMyFunctionDirichletTransientVaryingInSpace3DNormal_Comp1 m_fct3DComp1Normal;
  };

}

#endif
