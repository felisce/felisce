//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "userNSALE.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce {

  UserNSALE::UserNSALE():
    LinearProblemNS()
  {}

  UserNSALE::~UserNSALE()
  = default;

  void UserNSALE::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label)
  {

    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
    (void) label;

    if ( FelisceParam::instance().testCase == 4 ) {
      m_Pt.clear();
      if ( label == 1 ) {
        m_Pt.push_back(m_inPress(m_fstransient->time));
        m_elemFieldNeumannNormal[0].setValue(m_Pt);
      }
    }

    // FD-NS_closedValves
    if ( FelisceParam::instance().testCase == 100000 ) {
      m_Pt.clear();
      if ( label == 4 ) {
        if ( m_linearizedFlag )
          m_Pt.push_back(0.);
        else
          m_Pt.push_back(m_inPressFDNSclosedValves(m_fstransient->time));

        m_elemFieldNeumannNormal[0].setValue(m_Pt);
      }
    }
  }

  void UserNSALE::userFinalizeEssBCTransient() {
    const auto& r_felisce_param_instance = FelisceParam::instance();
    if (r_felisce_param_instance.testCase == 1 ) {
      m_boundaryConditionList[1]->setValue(this, m_fct2D, m_fstransient->time);
    } else if (r_felisce_param_instance.testCase == 2 ) {
      m_boundaryConditionList[0]->setValue(this, m_fct3DComp1, m_fstransient->time);
      m_boundaryConditionList[1]->setValue(this, m_fct3DComp2, m_fstransient->time);
    } else if (r_felisce_param_instance.testCase == 3 ) {
      m_boundaryConditionList[0]->setValue(this, m_fct3DComp1Half, m_fstransient->time);
      m_boundaryConditionList[1]->setValue(this, m_fct3DComp2Half, m_fstransient->time);
    } else if (r_felisce_param_instance.testCase == 4 ) {
      m_boundaryConditionList[0]->setValue(this, m_fct3DComp1Normal, m_fstransient->time);
    }
  }

}
