//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef _UserNSALE_HPP
#define _UserNSALE_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Tools/analyticFunction.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {

  // Test 1 //
  class ALEMyFunctionDirichletTransientVaryingInSpace2D {
  public:
    double operator() (double x,double y,double z,double t) const {
      IGNORE_UNUSED_ARGUMENT(y);
      IGNORE_UNUSED_ARGUMENT(z);
      return -0.628319*std::cos(3.14159*t)*std::sin((x*3.14159)/6);
    }
  };

  // Test 2 //
  class ALEMyFunctionDirichletTransientVaryingInSpace3D_Comp1 {
  public:
    double operator() (double x,double y,double z,double t) const {
      return -(0.6283185307179586*x*std::cos(3.14159*t)*std::sin((3.14159*z)/5.))/std::sqrt(x*x + y*y);
    }
  };

  class ALEMyFunctionDirichletTransientVaryingInSpace3D_Comp2 {
  public:
    double operator() (double x,double y,double z,double t) const {
      return -(0.6283185307179586*y*std::cos(3.14159*t)*std::sin((3.14159*z)/5.))/std::sqrt(x*x + y*y);
    }
  };

  // Test 3 //
  class ALEMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp1 {
  public:
    double operator() (double x,double y,double z,double t) const {
      return -(0.6283185307179586*x*std::cos(3.14159*t)*std::sin((3.14159*z)/10.))/std::sqrt(x*x + y*y);
    }
  };

  class ALEMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp2 {
  public:
    double operator() (double x,double y,double z,double t) const {
      return -(0.6283185307179586*y*std::cos(3.14159*t)*std::sin((3.14159*z)/10.))/std::sqrt(x*x + y*y);
    }
  };

  // Test 4 //
  class ALEMyFunctionDirichletTransientVaryingInSpace3DNormal_Comp1 {
  public:
    double operator() (double x,double y,double z,double t) const {
      IGNORE_UNUSED_ARGUMENT(x);
      IGNORE_UNUSED_ARGUMENT(y);
      return -(std::sin(z)*std::cos(t/3.14159))/3.14159;
    }
  };

  class inPressure {
  public:
    double operator() (double t) const { 
      double Tstar = 1;
      if ( t < Tstar ) return  100.0;
      return 0.;
    }
  };

  //----- Fictitious domain -----//
  // Test 100000 - Closed valve  //
  class inPressureFDNSclosedValves
  {
  public:
    double operator() (double t) const {
      return std::tanh(10*t)*3.e5;
    }
  };




  class UserNSALE:
    public LinearProblemNS {
  public:
    UserNSALE();
    ~UserNSALE() override;
    void userFinalizeEssBCTransient() override;
    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;

  protected:
    std::vector<double> m_Pt; 
    // ALE
    ALEMyFunctionDirichletTransientVaryingInSpace2D m_fct2D;
    ALEMyFunctionDirichletTransientVaryingInSpace3D_Comp1 m_fct3DComp1;
    ALEMyFunctionDirichletTransientVaryingInSpace3D_Comp2 m_fct3DComp2;
    ALEMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp1 m_fct3DComp1Half;
    ALEMyFunctionDirichletTransientVaryingInSpace3DHalf_Comp2 m_fct3DComp2Half;
    ALEMyFunctionDirichletTransientVaryingInSpace3DNormal_Comp1 m_fct3DComp1Normal;
    inPressure m_inPress;
    // FD
    inPressureFDNSclosedValves m_inPressFDNSclosedValves;
  
  };

}

#endif
