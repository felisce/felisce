//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M.A. Fernández and M. Landajuela
//

// System includes

// External includes

// Project includes
#include "userHarmonicExtension.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce {

  UserHarmonicExtension::UserHarmonicExtension():
    LinearProblemHarmonicExtension()
  {}

  UserHarmonicExtension::~UserHarmonicExtension()
  = default;

  void UserHarmonicExtension::userFinalizeEssBCTransient() {
    const auto& r_felisce_param_instance = FelisceParam::instance();
    if (r_felisce_param_instance.testCase == 1) {
      m_boundaryConditionList[1]->setValue(this, m_fct2D, m_fstransient->time);
    } else if (r_felisce_param_instance.testCase == 2) {
      m_boundaryConditionList[0]->setValue(this, m_fct3DComp1, m_fstransient->time);
      m_boundaryConditionList[1]->setValue(this, m_fct3DComp2, m_fstransient->time);
    } else if (r_felisce_param_instance.testCase == 3) {
      m_boundaryConditionList[0]->setValue(this, m_fct3DComp1Half, m_fstransient->time);
      m_boundaryConditionList[1]->setValue(this, m_fct3DComp2Half, m_fstransient->time);
    } else if (r_felisce_param_instance.testCase == 4) {
      m_boundaryConditionList[0]->setValue(this, m_fct3DComp1Normal, m_fstransient->time);
    }
  }

}
