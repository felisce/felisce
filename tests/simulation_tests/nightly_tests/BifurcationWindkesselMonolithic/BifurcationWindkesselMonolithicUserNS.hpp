//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//


#ifndef BifurcationWindkesselMonolithic_UserNS_HPP
#define BifurcationWindkesselMonolithic_UserNS_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {
  class BifurcationWindkesselMonolithicUserNS:
    public LinearProblemNS {
  public:
    BifurcationWindkesselMonolithicUserNS();
    ~BifurcationWindkesselMonolithicUserNS() override;
  };
}

#endif
