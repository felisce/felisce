##################
# compute the mean of the pressure field 
# and the mean of the velocity magnitude on all the mesh
##################

# e.g. : 
# python MeanVelocityMagnitude_meanPressure.py Solution/pressure.00003.scl Solution/velocity.00003.vct

from numpy import *
import os, sys

if len(sys.argv) != 3:
  print "Usage: %s FILENAME_PRESSURE FILENAME_VELOCITY" % (sys.argv[0])
  sys.exit(1)

filenamePressure = sys.argv[1]  
filePressure     = open(filenamePressure, "r")

filenameVelocity = sys.argv[2]  
fileVelocity     = open(filenameVelocity, "r")

# test on the argument order (the solution directory name must be 'Solution')
if "v" in filenamePressure[9]:
  print "Usage: %s FILENAME_PRESSURE FILENAME_VELOCITY" % (sys.argv[0])
  sys.exit(1)
if "p" in filenameVelocity[9]:
  print "Usage: %s FILENAME_PRESSURE FILENAME_VELOCITY" % (sys.argv[0])
  sys.exit(1)

###########################
## pressure

# Unused first line
filePressure.readline() 

listLignes  = []
doubleTab = []

# for each line of the file, we get the line without the '/n'
for ligne in filePressure.readlines():
  listLignes.append(ligne[0:len(ligne)-1])
filePressure.close()
# we put each double (12 caracters) in 'term'
for numLine in range(len(listLignes)):
  # chaque double est ecrit sur 12 caracteres
  for jj in range(len(listLignes[numLine])/12):
    term = listLignes[numLine][12*jj+1:12*jj+12]
    # and we put it in 'doubleTab' which contains all the values of the field
    doubleTab.append(term)

cpt = 0
tmp = 0
while cpt < len(doubleTab):
  tmp += double(doubleTab[cpt])
  cpt += 1

# mean of pressure field 
print tmp/len(doubleTab)

###########################
## velocity

# Unused first line
fileVelocity.readline() 

listLignes  = []
doubleTab = []

# for each line of the file, we get the line without the '/n'
for ligne in fileVelocity.readlines():
  listLignes.append(ligne[0:len(ligne)-1])
fileVelocity.close()
# we put each double (12 caracters) in 'term'
for numLine in range(len(listLignes)):
  # chaque double est ecrit sur 12 caracteres
  for jj in range(len(listLignes[numLine])/12):
    term = listLignes[numLine][12*jj+1:12*jj+12]
    # and we put it in 'doubleTab' which contains all the values of the field
    doubleTab.append(term)

norm = 0
for jj in range(len(doubleTab)/3):
  norm += sqrt(double(doubleTab[3*jj+0])**2 + double(doubleTab[3*jj+1])**2 + double(doubleTab[3*jj+2])**2)
 
# mean of velocity magnitude field
print norm/(len(doubleTab)/3)

