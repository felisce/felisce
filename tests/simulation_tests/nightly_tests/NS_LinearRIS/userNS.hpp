//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _UserNS_HPP
#define _UserNS_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {
   
  class InPressure
  {
  public:
    double operator() (double t) const;
  };
  
  class UserNS:
    public LinearProblemNS {
  public:
    UserNS();
    ~UserNS() override;

    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label) override;
    
    
  private:
    ElementField m_elemFieldPreviousVel;
    InPressure m_inPress;
    std::vector<double> m_Pt; 
  };
}


#endif
