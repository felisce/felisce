//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userNS.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce {

  UserNS::UserNS():
    LinearProblemNS()
  {}

  UserNS::~UserNS()
  = default;

  void UserNS::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label)
  {
    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
    (void) label;

    CurvilinearFiniteElement& curvFeVel = *m_listCurvilinearFiniteElement[m_iVelocity];
    m_elemFieldPreviousVel.initialize(DOF_FIELD,curvFeVel, this->dimension());
    
    // Stabilisation of backflow
    if (FelisceParam::instance().addedBoundaryFlag == 2){
      const double coef = FelisceParam::instance().stabilizationCoef * FelisceParam::instance().density;
      for(std::size_t ilab=0; ilab < FelisceParam::instance().stabilizationLabel.size(); ilab++){
        if (label == FelisceParam::instance().stabilizationLabel[ilab]){
          m_elemFieldPreviousVel.setValue(sequentialSolution(), curvFeVel, iel, m_iVelocity, m_ao, dof());      
          m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef, curvFeVel , m_elemFieldPreviousVel, 0, 0, dimension(), FelisceParam::instance().addedBoundaryFlag);
        }
      }
    }

    m_Pt.clear();
    if ( label == 4 ) {
      m_Pt.push_back(m_inPress(m_fstransient->time));
      m_elemFieldNeumannNormal[0].setValue(m_Pt);
    }
  }
    
  double InPressure::operator() (double t) const
  {
    return -100*std::sin(2.*t*M_PI/0.5);
  }

}
