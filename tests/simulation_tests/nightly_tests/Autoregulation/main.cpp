//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Core/felisceTransient.hpp"
#include "Core/felisceParam.hpp"
#include "Model/NSSimplifiedFSIModel.hpp"
#include "AutoregulationUserNSSimplifiedFSI.hpp"
#include "Core/mpiInfo.hpp"

using namespace felisce;

int main(const int argc, const char** argv)
{
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  // create model and solver
  NSSimplifiedFSIModel model;
  model.initializeModel(opt,fstransient);
  AutoregulationUserNSSimplifiedFSI* Us(new AutoregulationUserNSSimplifiedFSI());
  model.initializeLinearProblem( Us );
  Us->initializeAutoregulation(20);

  if ( FelisceParam::verbose() > 1 )
    Us->getAutoregulation()->print(MpiInfo::rankProc());

  model.SolveDynamicProblem();

  return EXIT_SUCCESS;
}
