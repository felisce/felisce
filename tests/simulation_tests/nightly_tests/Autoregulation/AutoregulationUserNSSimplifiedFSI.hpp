//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __USERNSSSIMPLIFIEDFSI_HPP__
#define __USERNSSSIMPLIFIEDFSI_HPP__

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNSSimplifiedFSI.hpp"
#include "Solver/autoregulation.hpp"

namespace felisce {
  class AutoregulationUserNSSimplifiedFSI:
    public LinearProblemNSSimplifiedFSI
  {
  public:
    AutoregulationUserNSSimplifiedFSI();
    void userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;
  };
}
#endif
