//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __IopNSCoupledUSERPERFECTFLUID_HPP__
#define __IopNSCoupledUSERPERFECTFLUID_HPP__

// System includes

// External includes

// Project includes
#include "Solver/linearProblemPerfectFluidRS.hpp"

namespace felisce {
  class IopNSCoupledUserPerfectFluid:
    public LinearProblemPerfectFluidRS
  {
  public:
    IopNSCoupledUserPerfectFluid();
    //Use this function to declare other variables of the model
    void userAddOtherVariables(std::vector<PhysicalVariable>& listVariable, std::vector<std::size_t>& listNumComp) override;
  };
}
#endif
