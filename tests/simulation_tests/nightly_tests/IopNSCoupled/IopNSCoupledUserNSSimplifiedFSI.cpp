//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "IopNSCoupledUserNSSimplifiedFSI.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "Solver/autoregulation.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{

  IopNSCoupledUserNSSimplifiedFSI::IopNSCoupledUserNSSimplifiedFSI():
    LinearProblemNSSimplifiedFSI()
  {
    for( std::size_t i(0); i < FelisceParam::instance().lumpedModelBCLabel.size(); i++) {
      FelisceParam::instance().lumpedModelBC_Pdist_init[i] = FelisceParam::instance().incomingPressure*Autoregulation::mmHg2DynPerCm2*0.95;
    }
  }

  void
  IopNSCoupledUserNSSimplifiedFSI::userGetBC( BoundaryCondition *&BCLat,  BoundaryCondition *&BCIn, BoundaryCondition *&BCOut) {
      BCLat = m_boundaryConditionList.EmbedFSI(0);
      BCIn  = m_boundaryConditionList.NeumannNormal(0);
      BCOut = nullptr;
  }

  void
  IopNSCoupledUserNSSimplifiedFSI::userAddOtherVariables(std::vector<PhysicalVariable>& listVariable, std::vector<std::size_t>& listNumComp) {
    listVariable.push_back(iop);
    listNumComp.push_back(1);  }

  void
  IopNSCoupledUserNSSimplifiedFSI::userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) {
    LinearProblemNSSimplifiedFSI::userElementComputeNaturalBoundaryCondition(elemPoint,elemIdPoint,iel,label);
    BoundaryCondition* BC;
    for (std::size_t iNeumannNormal = 0; iNeumannNormal < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); iNeumannNormal++ ){
      BC = m_boundaryConditionList.NeumannNormal(iNeumannNormal);
      for(auto it_labelNumber = BC->listLabel().begin(); it_labelNumber != BC->listLabel().end(); it_labelNumber++){
        if(*it_labelNumber == label){

          for( std::size_t k(0); k< m_label["inflow"].size(); k++ )
            if( label == m_label["inflow"][k] )
              if( m_elemFieldNeumannNormal[iNeumannNormal].getType()==CONSTANT_FIELD && BC->typeValueBC() == FunctionT){
                m_elemFieldNeumannNormal[iNeumannNormal].setValue(std::vector<double>(1,m_autoregulation.inletPressure( m_fstransient->time, FelisceParam::instance().idCase) ) );
              }
        }
      }
    }
  }
}
