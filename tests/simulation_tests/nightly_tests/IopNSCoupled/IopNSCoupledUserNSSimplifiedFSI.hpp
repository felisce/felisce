//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef __USERNSSSIMPLIFIEDFSI_HPP__
#define __USERNSSSIMPLIFIEDFSI_HPP__

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNSSimplifiedFSI.hpp"
#include "Solver/autoregulation.hpp"

namespace felisce {
  class IopNSCoupledUserNSSimplifiedFSI:
    public LinearProblemNSSimplifiedFSI
  {
  public:
    IopNSCoupledUserNSSimplifiedFSI();
    void userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;
    void userGetBC( BoundaryCondition* &BCLat,  BoundaryCondition* &BCIn, BoundaryCondition* &BCOut) override;
    //Use this function to declare other variables of the model
    void userAddOtherVariables(std::vector<PhysicalVariable>& listVariable, std::vector<std::size_t>& listNumComp) override;
  };
}
#endif
