//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "IopNSCoupledUserPerfectFluid.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce
{
  IopNSCoupledUserPerfectFluid::IopNSCoupledUserPerfectFluid(): LinearProblemPerfectFluidRS() {}

  void
  IopNSCoupledUserPerfectFluid::userAddOtherVariables(std::vector<PhysicalVariable>& listVariable,  std::vector<std::size_t>& listNumComp ) {
    listVariable.push_back(velocity);
    listNumComp.push_back(this->dimension());
    listVariable.push_back(pressure);
    listNumComp.push_back(1); }
}
