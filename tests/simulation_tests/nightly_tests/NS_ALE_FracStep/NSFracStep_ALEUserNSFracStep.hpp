//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

#ifndef NSFracStep_ALE_UserNSFracStep_HPP
#define NSFracStep_ALE_UserNSFracStep_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "Tools/analyticFunction.hpp"
#include "Solver/linearProblemNSFracStepAdvDiff.hpp"
#include "Solver/linearProblemNSFracStepProj.hpp"

namespace felisce {


  class MyFunctionDirichletTransientVaryingInSpace3D_Comp1 {
  public:
    double operator() (double x,double y,double z,double t) const {
      return (0.6283185307179586*x*std::cos(3.14159*t)*std::sin((3.14159*z)/5.))/std::sqrt(x*x + y*y);
    }
  };

  class MyFunctionDirichletTransientVaryingInSpace3D_Comp2 {
  public:
    double operator() (double x,double y,double z,double t) const {
      return (0.6283185307179586*y*std::cos(3.14159*t)*std::sin((3.14159*z)/5.))/std::sqrt(x*x + y*y);
    }
  };

  // Advection-Diffusion step //

  class NSFracStep_ALEUserNSFracStepAdvDiff:
    public LinearProblemNSFracStepAdvDiff {
  public:
    NSFracStep_ALEUserNSFracStepAdvDiff(): LinearProblemNSFracStepAdvDiff(){};
    ~NSFracStep_ALEUserNSFracStepAdvDiff() override= default;;
    void userFinalizeEssBCTransient() override {
      if ( FelisceParam::instance().testCase == 1 ) {
        m_boundaryConditionList[0]->setValue(this, m_fct3DComp1, m_fstransient->time);
        m_boundaryConditionList[1]->setValue(this, m_fct3DComp2, m_fstransient->time);
      }
    }

  private:
    MyFunctionDirichletTransientVaryingInSpace3D_Comp1 m_fct3DComp1;
    MyFunctionDirichletTransientVaryingInSpace3D_Comp2 m_fct3DComp2;

  };

  // Projection step //

  class NSFracStep_ALEUserNSFracStepProj:
    public LinearProblemNSFracStepProj {
  public:
    NSFracStep_ALEUserNSFracStepProj(): LinearProblemNSFracStepProj(){};
    ~NSFracStep_ALEUserNSFracStepProj() override= default;;

  };

}
#endif
