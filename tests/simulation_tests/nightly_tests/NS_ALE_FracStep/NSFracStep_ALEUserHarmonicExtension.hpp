//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M.A. Fernández
//

#ifndef NSFracStep_ALE_UserHarmonicExtension_HPP
#define NSFracStep_ALE_UserHarmonicExtension_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHarmonicExtension.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

class MyFunctionDirichletTransientVaryingInSpace2D {
public:
  double operator() (double x,double y,double z,double t) const {
    IGNORE_UNUSED_ARGUMENT(y);
    IGNORE_UNUSED_ARGUMENT(z);
    //return std::sin((x*3.14159)/6)*std::sin((t*3.14159)/(6*0.015));
    return 0.2*std::sin((x*3.14159)/6)*std::sin(t*3.14159);
  }
};

class MyFunctionDirichletTransientVaryingInSpace3D_Comp1 {
public:
  double operator() (double x,double y,double z,double t) const {
    return (0.2*x*std::sin(3.14159*t)*std::sin((3.14159*z)/5))/std::sqrt(x*x + y*y);
  }
};

class MyFunctionDirichletTransientVaryingInSpace3D_Comp2 {
public:
  double operator() (double x,double y,double z,double t) const {
    return (0.2*y*std::sin(3.14159*t)*std::sin((3.14159*z)/5))/std::sqrt(x*x + y*y);
  }
};

namespace felisce {
  class NSFracStep_ALEUserHarmonicExtension:
    public LinearProblemHarmonicExtension {
  public:
    NSFracStep_ALEUserHarmonicExtension(): LinearProblemHarmonicExtension() {};
    ~NSFracStep_ALEUserHarmonicExtension() override= default;;
    void userFinalizeEssBCTransient() override {
      if ( FelisceParam::instance().testCase == 1) {
        m_boundaryConditionList[0]->setValue(this, m_fct3DComp1, m_fstransient->time);
        m_boundaryConditionList[1]->setValue(this, m_fct3DComp2, m_fstransient->time);
      }
    }

  protected:
    MyFunctionDirichletTransientVaryingInSpace3D_Comp1 m_fct3DComp1;
    MyFunctionDirichletTransientVaryingInSpace3D_Comp2 m_fct3DComp2;
  };

}

#endif
