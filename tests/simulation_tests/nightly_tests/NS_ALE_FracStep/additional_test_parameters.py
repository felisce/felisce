class AdditionalTestParameters:
    def __init__(self):
        """ The default constructor of the class
        """
        self.tolerance = 5.0e-3
        self.relative_error = True
        self.number_of_cores = 2
