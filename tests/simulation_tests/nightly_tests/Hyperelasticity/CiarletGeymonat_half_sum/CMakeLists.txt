# Install test SimTest_Hyperelasticity_CiarletGeymonat_half_sum

felisce_install_tests_data(
    NAME SimTest_Hyperelasticity_CiarletGeymonat_half_sum
    EXEC ../SimTest_Hyperelasticity
    INSTALL simulation_tests/Hyperelasticity/CiarletGeymonat_half_sum
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_Hyperelasticity_CiarletGeymonat_half_sum
    INSTALL simulation_tests/Hyperelasticity/CiarletGeymonat_half_sum
)
