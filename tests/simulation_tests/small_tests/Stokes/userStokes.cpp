//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

/*!
 \file UserStokes.cpp
 \authors J. Foulon & J-F. Gerbeau & V. Martin
 \date 02/02/2011
 \brief File where is implemented UserStokes class.
 */

// System includes

// External includes

// Project includes
#include "userStokes.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce 
{

UserStokes::UserStokes():
  LinearProblemNS()
{}

/***********************************************************************************/
/***********************************************************************************/

UserStokes::~UserStokes() = default;

/***********************************************************************************/
/***********************************************************************************/

void UserStokes::userElementInit() {
  //ElementField initialisation
  m_elemFieldVelocity.initialize(QUAD_POINT_FIELD,*m_feVel,this->dimension());
  m_elemFieldPressure.initialize(QUAD_POINT_FIELD,*m_fePres);
  m_elemFieldUn_1.initialize(DOF_FIELD,*m_feVel,this->dimension());
}

/***********************************************************************************/
/***********************************************************************************/

void UserStokes::userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                  felInt& iel,FlagMatrixRHS flagMatrixRHS) {
  IGNORE_UNUSED_ARGUMENT(elemPoint);
  IGNORE_UNUSED_ARGUMENT(elemIdPoint);
  IGNORE_UNUSED_ARGUMENT(iel);
  IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
  for ( int iDimension = 0; iDimension < this->dimension(); iDimension++)
    m_elemFieldVelocity.setValue(m_fct, *m_feVel,iDimension);
  m_elementVector[0]->source(1.,*m_feVel,m_elemFieldVelocity,0,this->dimension());
}
}
