# Install test SimTest_ElasticString_Mumps

felisce_install_tests_data(
    NAME SimTest_ElasticString_Mumps
    EXEC ../SimTest_ElasticString
    INSTALL simulation_tests/ElasticString/Mumps
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_ElasticString_Mumps
    INSTALL simulation_tests/ElasticString/Mumps
)
