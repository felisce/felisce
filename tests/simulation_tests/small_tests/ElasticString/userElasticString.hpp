//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef _UserElasticString_HPP
#define _UserElasticString_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemElasticString.hpp"

namespace felisce
{
  class GravityForce {
  public:
    double operator()(double /*x*/, double /*y*/, double /*z*/) const
    {
      return -9.81;
    }
  };


  class UserElasticString:
    public LinearProblemElasticString
  {
  public:
    UserElasticString();
    ~UserElasticString() override;

    void initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) override;
    void userElementInitBD() override;
    void userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel) override;
    void userFinalizeEssBCTransient() override;

  private:
    ElementField m_sourceTerm;
    GravityForce m_gravityForce;

    std::vector<felInt> m_idDofBCVertices;
    std::vector<double> m_valBC;
  };
}


#endif
