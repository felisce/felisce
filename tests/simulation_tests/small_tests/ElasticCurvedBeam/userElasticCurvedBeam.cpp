//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userElasticCurvedBeam.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce
{
  UserElasticCurvedBeam::UserElasticCurvedBeam():
    LinearProblemElasticCurvedBeam()
  {}

  UserElasticCurvedBeam::~UserElasticCurvedBeam()
  = default;

  void UserElasticCurvedBeam::initialize(std::vector<GeometricMeshRegion::Pointer>& mesh, FelisceTransient::Pointer fstransient, MPI_Comm& comm, bool doUseSNES) {
    LinearProblemElasticCurvedBeam::initialize(mesh, fstransient, comm, doUseSNES);

  }



  void UserElasticCurvedBeam::userElementInitBD() {
    m_sourceTerm.initialize(QUAD_POINT_FIELD, *m_feDisp, m_listVariable[m_listUnknown.idVariable(0)].numComponent());
  }



  void UserElasticCurvedBeam::userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel) {
    IGNORE_UNUSED_ELEM_ID_POINT;
    IGNORE_UNUSED_ELEM_POINT;
    IGNORE_UNUSED_IEL;

    // add specific terms to the rhs
    felInt numcomp = m_listVariable[m_listUnknown.idVariable(0)].numComponent();
    for(felInt icomp=0; icomp<numcomp; ++icomp)
      m_sourceTerm.setValueVec(m_gravityForce, *m_feDisp, icomp);

    m_elementVectorBD[0]->source(1., *m_feDisp, m_sourceTerm, 0, numcomp);
  }


  void UserElasticCurvedBeam::userFinalizeEssBCTransient() {
    if(m_fstransient->iteration == 1) {
      // get the id of the boundary of the structure
      // we identify the points that are in only one edge. There should only be "boundaries" elements.
      felInt numComp = 0;
      felInt idof = 0;
      std::vector<felInt> numEdgePerVertices(m_supportDofUnknown[0].numSupportDof(), 0);

      for(std::size_t ielSup=0; ielSup<m_supportDofUnknown[0].iEle().size()-1; ++ielSup) {
        for(felInt idSup=0; idSup<m_supportDofUnknown[0].getNumSupportDof(ielSup); ++idSup) {
          ++numEdgePerVertices[m_supportDofUnknown[0].iSupportDof()[m_supportDofUnknown[0].iEle()[ielSup] + idSup]];
        }
      }

      for(std::size_t ivert=0; ivert<numEdgePerVertices.size(); ++ivert) {
        if(numEdgePerVertices[ivert] == 1) {
          // we got a point on the border of the structure
          numComp = m_listVariable[m_listUnknown.idVariable(0)].numComponent();
          for(felInt icoor=0; icoor<numComp; ++icoor) {
            idof = ivert * numComp + icoor;
            AOApplicationToPetsc(ao(), 1, &idof);
            m_idDofBCVertices.push_back(idof);
          }
        }
      }

      m_valBC.resize(m_idDofBCVertices.size(), 0.);
    }

    if(!m_idDofBCVertices.empty()) {
      for(std::size_t ival=0; ival<m_valBC.size(); ++ival) {
        m_valBC[ival] = 0;
      }

      // Enforce the Dirichlet boundary conditions
      matrix(0).zeroRows(m_idDofBCVertices.size(), m_idDofBCVertices.data(), 1.);

      vector().setValues(m_idDofBCVertices.size(), m_idDofBCVertices.data(), m_valBC.data(), INSERT_VALUES);
      vector().assembly();
    }
  }
}
