# Install test SimTest_Prism_Coarse_9

felisce_install_tests_data(
    NAME SimTest_Prism_Coarse_9
    EXEC ../SimTest_Prism
    INSTALL simulation_tests/Prism/Coarse_9
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_Prism_Coarse_9
    INSTALL simulation_tests/Prism/Coarse_9
)
