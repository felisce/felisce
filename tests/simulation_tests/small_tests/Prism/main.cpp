//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J.Foulon && Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "Model/heatModel.hpp"
#include "Solver/linearProblemHeat.hpp"

int main(const int argc, const char** argv) {
  // Read command line and data file
  felisce::CommandLineOption opt(argc,argv);

  felisce::FelisceParam::instance().initialize(opt);
  felisce::FelisceTransient fstransient;

  // Create model and solver
  felisce::HeatModel model;
  model.initializeModel(opt,fstransient);
  model.initializeLinearProblem( new felisce::LinearProblemHeat() );

  // Time loop
  model.SolveDynamicProblem();
}
