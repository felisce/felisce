//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    S. Gilles
//

// System includes

// External includes

// Project includes
#include "Model/hyperElasticityModel.hpp"
#include "Solver/linearProblemHyperElasticity.hpp"
#include "Hyperelasticity/invariants.hpp"

using namespace felisce;

int main(const int argc, const char** argv) {

  // read command line and data file
  CommandLineOption opt(argc, argv);
  FelisceParam::instance().initialize(opt);
  FelisceTransient fstransient;

  HyperElasticityModel model;
  model.initializeModel(opt,fstransient);

  auto volumic_force_cst = [](int iComp, double, double, double) -> double {
    FEL_ASSERT(iComp >= 0 && iComp < 3);
    return (iComp == 1 ? -.1 : 0.);
  };

  auto linear_problem = new HyperElasticityModel::HyperelasticLinearProblem(volumic_force_cst);

  const bool use_snes = true;
  model.initializeLinearProblem(linear_problem, use_snes);

  std::cout << "\nInitialized" << std::endl;

  // Time loop
  model.SolveStaticProblem();
  //while ( ! model.hasFinished() ) {
  //  model.forward();
  //}

  std::cout << "Done hyperelasticity" << std::endl;

  return 0;
}
