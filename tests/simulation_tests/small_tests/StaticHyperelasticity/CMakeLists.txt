# Install test SimTest_StaticHyperelasticity executable

felisce_install_test_exe(
    NAME SimTest_StaticHyperelasticity
    SOURCES main.cpp
    INSTALL simulation_tests/StaticHyperelasticity
)

# Add test directories
add_subdirectory(CiarletGeymonat)
add_subdirectory(StVenantKirchhoff)
