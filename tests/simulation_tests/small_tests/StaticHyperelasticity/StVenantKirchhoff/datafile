# -*- getpot -*-
#----------------------------------------------------------------
#      Data file for test IntRef -> stringLabel
#----------------------------------------------------------------
[debug]

verbose = 0

[solid]
young = 21e5
poisson = 0.3
planeStressStrain = 2 # 1 for planeStress, 2 for planeStrain
volumicOrSurfacicForce = 1 # 1 for volumic, 2 for surfacic
volumic_mass = 1. # adimensioned quantity

# Ciarlet-Geymonat parameters
kappa1 = 500.
kappa2 = 1500.
bulk = 1.e5

[variable]

variable = 'displacement'
typeOfFiniteElement = '1' # 0 for linear, 1 for quadratic
degreeOfExactness = '2'


[mesh]

meshDir = @mesh_folder@
#inputMesh = Bar.mesh # Don't forget to add third Dirichlet component!!!
inputMesh = hyperelasticity_Nx50_Ny20.mesh
#inputMesh = hyperelasticity_Nx2_Ny1.mesh
outputMesh = displacement.geo
resultDir = ./Solution/
prefixName = hyperelasticity

[boundaryCondition]

#### Volumic

type = 'Dirichlet'
typeValue = 'Constant'
numLabel = '1' # First: Dirichlet condition upon both sides
label = '1' # sides for Dirichlet
variable = 'displacement'
component = 'Comp12' # Dirichlet condition on both x and y
value = '0. 0.' # 0 for both components x and y.


[petsc]

solver = preonly

# List of preconditioner :
# none jacobi sor lu bjacobi ilu asm cholesky
#To use mumps: preconditioner = lu

preconditioner = lu
relativeTolerance = 10.e-6
absoluteTolerance = 10.e-10
maxIteration = 10000

[transient]

timeStep = 1.0
timeIterationMax = 1
timeMax = 1.0
time = 0.

[Hyperelastic]

type = 'StVenantKirchhoff'
TimeScheme = 'half_sum'
IsIncompressible = false
