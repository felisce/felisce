//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "Model/heatModel.hpp"
#include "userHeat.hpp"

using namespace felisce;

class InitialConditionFunctor {
public:
  double operator() (double x,double /*y*/,double /*z*/) const {

    if (x <= 0.5) {
      return 2.*x;
    }

    else {
      return 2. - 2.*x;
    }

  }
};

class XCos {
public:
  double operator() (double x,double /*y*/,double /*z*/, double t) {
    return x*std::cos(t);
  }
};

double ysin(double ,double, double , double );
double ysin(double /*x*/,double y,double /*z*/, double t) {
  return y*std::sin(t);
}


int main(const int argc, const char** argv) {
  // Read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  // Create model and solver
  HeatModel model;
  model.initializeModel(opt,fstransient);
  model.initializeLinearProblem( new UserHeat() );

  switch ( FelisceParam::instance(instanceIndex).testCase ) {
    case 2:
      // Set initial condition
      InitialConditionFunctor initial_condition;
      model.linearProblem()->set_U_0_parallel(initial_condition,0,0);
      break;

    case 3:
      // Add possible callbacks
      XCos xcos;
      model.addCallbackXYZT("xcos",xcos);
      model.addCallbackXYZT("ysin",ysin);
      break;

    default:
      break;
  }

  // Time loop
  model.SolveDynamicProblem();
}
