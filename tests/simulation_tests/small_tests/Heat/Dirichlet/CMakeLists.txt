# Install test SimTest_Heat_Dirichlet

felisce_install_tests_data(
    NAME SimTest_Heat_Dirichlet
    EXEC ../SimTest_Heat
    INSTALL simulation_tests/Heat/Dirichlet
    CONFIG datafile
    FILES 
    DIRS RefSolutions Data
)

felisce_add_test(
    NAME SimTest_Heat_Dirichlet
    INSTALL simulation_tests/Heat/Dirichlet
)
