//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userHeat.hpp"
#include "Core/felisceTransient.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{

// set boundary condition value with analytical function
void UserHeat::userFinalizeEssBCTransient() 
{

  switch ( FelisceParam::instance(instanceIndex()).testCase ) {

    case 1 : {
      // Get current time
      const double time = m_fstransient->time;

      // Get the boundary condition we want to set the value
      BoundaryCondition* bc = getBoundaryConditionList().Dirichlet(1);

      // Set boundary condition with analytical function
      bc->setValue(this, m_transientDirichlet, time);

      break;
    }

    default:
      break;
  }
}

// create a ElementField
void UserHeat::userElementInit() 
{
  switch ( FelisceParam::instance(instanceIndex()).testCase ) {
    
    case 4 :
      m_sourceTerm.initialize(QUAD_POINT_FIELD, *m_feTemp);
      break;

    default:
      break;
  }
}

// set source term with analytical function
void UserHeat::userElementCompute(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& /*elemIdPoint*/, int& /*iel*/, FlagMatrixRHS /*flagMatrixRHS*/) 
{
  switch ( FelisceParam::instance(instanceIndex()).testCase ) {
    
    case 4 :
      m_sourceTerm.setValue(m_sourceTermFct, *m_feTemp, m_fstransient->time);
      m_elementVector[0]->source(1.,*m_feTemp,m_sourceTerm,0,1);
      break;

    default:
      break;
  }
}

}
