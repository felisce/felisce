//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _USERHEAT_HPP
#define _USERHEAT_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Solver/linearProblemHeat.hpp"

namespace felisce {
class TransientDirichlet {
public:
  double operator() (double /*x*/,double /*y*/,double /*z*/, double t) const {
    return -std::sin(t);
  }
};

class UserHeat:
  public LinearProblemHeat 
{
  public:
    UserHeat();
    ~UserHeat() override;
    void userFinalizeEssBCTransient() override;

  private:
    TransientDirichlet m_transientDirichlet;
  };
}

#endif
