# Install test SimTest_Prism_Transient_Dirichlet_Fine_6

felisce_install_tests_data(
    NAME SimTest_Prism_Transient_Dirichlet_Fine_6
    EXEC ../SimTest_Prism_Transient_Dirichlet
    INSTALL simulation_tests/Prism_Transient_Dirichlet/Fine_6
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_Prism_Transient_Dirichlet_Fine_6
    INSTALL simulation_tests/Prism_Transient_Dirichlet/Fine_6
)
