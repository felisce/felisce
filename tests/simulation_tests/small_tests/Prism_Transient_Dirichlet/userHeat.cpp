//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "userHeat.hpp"
#include "Core/felisceTransient.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{
// constructor, call LinearProblemheat constructor
UserHeat::UserHeat():
  LinearProblemHeat() {
}

// destructor
UserHeat::~UserHeat() = default;

// set boundary condition value with analytical function
void UserHeat::userFinalizeEssBCTransient() {
  // Get current time
  const double time = m_fstransient->time;

  // Get the boundary condition we want to set the value
  BoundaryCondition* bc = getBoundaryConditionList().Dirichlet(0);

  // Set boundary condition with analytical function
  bc->setValue(this, m_transientDirichlet, time);
}
}
