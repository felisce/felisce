//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include <Model/stokesContinuationModel.hpp>
#include <Solver/linearProblemStokesContinuation.hpp>
#include <Core/chrono.hpp>

using namespace felisce;

int main(int argc, const char** argv) {
  // read command line and data file
  CommandLineOption opt(argc,argv);
  FelisceParam::instance().initialize(opt);
  FelisceTransient fstransient;

  // create model and solver
  StokesContinuationModel model;
  model.initializeModel(opt,fstransient);
  model.initializeLinearProblem( new LinearProblemStokesContinuation() );

  // time loop
  while (!model.hasFinished())
    model.forward();

  return EXIT_SUCCESS;
}
