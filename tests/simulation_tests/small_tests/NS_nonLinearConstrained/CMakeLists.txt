# Install test SimTest_NS_nonLinearConstrained

felisce_install_test_exe(
    NAME SimTest_NS_nonLinearConstrained
    SOURCES main.cpp userNS.cpp
    INSTALL simulation_tests/NS_nonLinearConstrained
)

felisce_install_tests_data(
    NAME SimTest_NS_nonLinearConstrained
    EXEC SimTest_NS_nonLinearConstrained
    INSTALL simulation_tests/NS_nonLinearConstrained
    CONFIG datafile
    FILES .DS_Store
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_NS_nonLinearConstrained
    INSTALL simulation_tests/NS_nonLinearConstrained
)
