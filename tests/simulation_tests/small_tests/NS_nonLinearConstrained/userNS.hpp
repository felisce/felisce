//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

/*!
 \file UserNS.hpp
 */

#ifndef _UserNS_HPP
#define _UserNS_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {
  class InPressure {
  public:
    double operator() (double t) const {
        double pi = acos(-1.);
        return   1.e2*cos(0.75*2.*pi*t);
    }
  };

  /*!
   \class UserNS
   \authors J. Foulon & J-F. Gerbeau & V. Martin
   \date 02/02/2011
   \brief Manage user specific functions for laplacian problem.
   */

  class UserNS:
    public LinearProblemNS {
  public:
    UserNS();
    ~UserNS() override;
    void userElementInit() override;
    void userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                    felInt& iel,FlagMatrixRHS flagMatrixRHS=FlagMatrixRHS::matrix_and_rhs) override;
    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label) override;

  private:
    ElementField m_elemFieldPreviousVel;
    ElementField m_elemFieldVelocity;
    ElementField m_elemFieldPressure;
    InPressure m_inPress;
    std::vector<double> m_Pt;
  };
}


#endif
