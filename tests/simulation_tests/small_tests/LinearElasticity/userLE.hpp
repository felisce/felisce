//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _UserLE_HPP
#define _UserLE_HPP

// System includes

// External includes

// Project includes
#include <Solver/linearProblemLinearElasticity.hpp>

namespace felisce {
  class UserLE:
    public LinearProblemLinearElasticity {
  public:
    UserLE();
    ~UserLE() override;
    void userElementInit() override;
    void userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                    felInt& iel,FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  };
}


#endif
