//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userLE.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"


namespace felisce {

  UserLE::UserLE():LinearProblemLinearElasticity()
  {}

  UserLE::~UserLE()
  = default;

  void UserLE::userElementInit() {}

  void UserLE::userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                                    felInt& iel,FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ARGUMENT(elemPoint);
    IGNORE_UNUSED_ARGUMENT(elemIdPoint);
    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
  }
}
