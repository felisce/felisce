//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "userSolid.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "PETScInterface/AbstractPETScObjectInterface.hpp"

namespace felisce {

UserSolid::UserSolid():
  LinearProblemElasticityDynamic([](int iComp, double, double, double) -> double {IGNORE_UNUSED_ARGUMENT(iComp);return 0.0;})
{}

UserSolid::~UserSolid()
{

}

void UserSolid::userFinalizeEssBCTransient()
{
  const double load_y = m_force(0.0);
  auto& r_rhs = this->vector();
  // // NODE: Node 1071 is in the element 2000 as second node (we need some element as reference)
  const int node_id = 1071;
  const int component_id = 2 - 1;
  int index;
  // const int iel = 2000 - 1;
  // const int vertex_id = 2 - 1;
  // const int i_unkown_disp = this->listUnknown().getUnknownIdList(displacement);
  // const int id_var_disp = this->listUnknown().idVariable(i_unkown_disp);
  // this->dof().loc2glob(iel,vertex_id, id_var_disp, component_id, index);
  // AOApplicationToPetsc(this->ao(),1,&index);
  index = AOInterface::SearchGlobalIndexNode(*this->mesh(), this->listUnknown(), this->ao(), this->dof(), displacement, node_id, component_id, GeometricMeshRegion::Tria3);
  r_rhs.setValueOnce(index, load_y, ADD_VALUES);
  r_rhs.assembly();
}

double PointLoad::operator() (const double t) const
{
  IGNORE_UNUSED_ARGUMENT(t);
  return -1.0e3;
}

}
