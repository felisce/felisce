//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _UserSOLID_HPP
#define _UserSOLID_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemElasticity.hpp"
#include "Core/felisceParam.hpp"

namespace felisce
{

  class PointLoad
  {
  public:
    double operator() (const double t) const;
  };

  class UserSolid:
    public LinearProblemElasticityDynamic {
  public:
    UserSolid();

    ~UserSolid();

    void userFinalizeEssBCTransient() override;

  private:
    PointLoad m_force;
  };

}

#endif
