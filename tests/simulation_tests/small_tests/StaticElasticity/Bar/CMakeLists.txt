# Install test SimTest_StaticElasticity_Bar

felisce_install_tests_data(
    NAME SimTest_StaticElasticity_Bar
    EXEC ../SimTest_StaticElasticity
    INSTALL simulation_tests/StaticElasticity/Bar
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_StaticElasticity_Bar
    INSTALL simulation_tests/StaticElasticity/Bar
)
