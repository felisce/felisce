# Install test SimTest_StaticElasticity_Cantilever

felisce_install_tests_data(
    NAME SimTest_StaticElasticity_Cantilever
    EXEC ../SimTest_StaticElasticity
    INSTALL simulation_tests/StaticElasticity/Cantilever
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_StaticElasticity_Cantilever
    INSTALL simulation_tests/StaticElasticity/Cantilever
)
