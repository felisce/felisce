//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "Model/elasticityModel.hpp"
#include "Solver/linearProblemElasticity.hpp"

using namespace felisce;

int main(const int argc, const char** argv) {
  // Read command line and data file
  CommandLineOption opt(argc, argv);
  FelisceParam::instance().initialize(opt);
  FelisceTransient fstransient;

  ElasticityModel model;
  model.initializeModel(opt,fstransient);

  auto volumic_force_cst = [](int iComp, double, double, double) -> double {
    FEL_ASSERT(iComp >= 0 && iComp < 3);
    return (iComp == 1 ? -.1 : 0.);
  };

  LinearProblemElasticityDynamic* linear_problem = new LinearProblemElasticityDynamic(volumic_force_cst);

  const bool do_use_snes = true;
  model.initializeLinearProblem(linear_problem, do_use_snes);
  PetscPrintf(MpiInfo::petscComm(),"Initialized\n");

  model.SolveDynamicProblem();

  PetscPrintf(MpiInfo::petscComm(),"Done\n");

}
