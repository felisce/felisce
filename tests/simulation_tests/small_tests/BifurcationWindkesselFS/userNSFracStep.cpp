//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

// System includes

// External includes

// Project includes
#include "userNSFracStep.hpp"

namespace felisce {

  UserNSFracStepAdvDiff::UserNSFracStepAdvDiff():
    LinearProblemNSFracStepAdvDiff()
  {}

  UserNSFracStepAdvDiff::~UserNSFracStepAdvDiff()
  = default;

  ///////////////////////

  UserNSFracStepProj::UserNSFracStepProj():
    LinearProblemNSFracStepProj()
  {}

  UserNSFracStepProj::~UserNSFracStepProj()
  = default;
}
