//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    
//

// System includes

// External includes

// Project includes
#include "Model/NSFracStepModel.hpp"
#include "userNSFracStep.hpp"
#include "Core/felisceTransient.hpp"
#include "Core/felisceParam.hpp"

using namespace felisce;

int main(const int argc, const char** argv) {
  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  // create model
  Model* model;
  std::vector<LinearProblem*> linearPb;

  UserNSFracStepAdvDiff* nsFracStepAdvDiff = new UserNSFracStepAdvDiff();
  UserNSFracStepProj* nsFracStepProj = new UserNSFracStepProj();
  model = new NSFracStepModel();
  model->initializeModel(opt,fstransient);
  linearPb.push_back(nsFracStepAdvDiff);
  linearPb.push_back(nsFracStepProj);
  model->initializeLinearProblem(linearPb);

  //Loop time
  model->SolveDynamicProblem();

  delete model ;
}
