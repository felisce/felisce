//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:
//

#ifndef _UserNSFracStep_HPP
#define _UserNSFracStep_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Solver/linearProblemNSFracStepAdvDiff.hpp"
#include "Solver/linearProblemNSFracStepProj.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  class UserNSFracStepAdvDiff:
    public LinearProblemNSFracStepAdvDiff {
  public:
    UserNSFracStepAdvDiff();
    ~UserNSFracStepAdvDiff() override;
  };

  ///////////////////////////////

  class UserNSFracStepProj:
    public LinearProblemNSFracStepProj {
  public:
    UserNSFracStepProj();
    ~UserNSFracStepProj() override;
  };
}

#endif
