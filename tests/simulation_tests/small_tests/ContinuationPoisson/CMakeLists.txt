# Install test SimTest_ContinuationPoisson

felisce_install_test_exe(
    NAME SimTest_ContinuationPoisson
    SOURCES main.cpp
    INSTALL simulation_tests/ContinuationPoisson
)

felisce_install_tests_data(
    NAME SimTest_ContinuationPoisson
    EXEC SimTest_ContinuationPoisson
    INSTALL simulation_tests/ContinuationPoisson
    CONFIG datafile
    FILES 
    DIRS RefSolutions Data
)

felisce_add_test(
    NAME SimTest_ContinuationPoisson
    INSTALL simulation_tests/ContinuationPoisson
)
