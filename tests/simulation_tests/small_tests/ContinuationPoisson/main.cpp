//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include "Model/poissonContinuationModel.hpp"
#include "Solver/linearProblemPoissonContinuation.hpp"

using namespace felisce;

int main(int argc, char** argv) {
  // read command line and data file
  CommandLineOption opt(argc,const_cast<const char**>(argv));
  FelisceParam::instance().initialize(opt);
  FelisceTransient fstransient;

  // create model and solver
  PoissonContinuationModel model;
  model.initializeModel(opt,fstransient);
  model.initializeLinearProblem( new LinearProblemPoissonContinuation() );

  // solve Poisson problem
  model.forward();

  return EXIT_SUCCESS;
}
