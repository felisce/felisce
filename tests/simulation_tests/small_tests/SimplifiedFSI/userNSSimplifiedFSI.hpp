//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _userNSSimplifiedFSI_HPP
#define _userNSSimplifiedFSI_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNSSimplifiedFSI.hpp"

namespace felisce
{
  class UserNSSimplifiedFSI:
    public LinearProblemNSSimplifiedFSI
  {
  public:
    UserNSSimplifiedFSI();
    ~UserNSSimplifiedFSI() override;

    void userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;

  private:

    inline double inletPressure(double &t) const {
      return (t<0.005?-5000.:0.);
    }
  };
}
#endif
