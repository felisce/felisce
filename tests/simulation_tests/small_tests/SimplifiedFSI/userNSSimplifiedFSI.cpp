//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userNSSimplifiedFSI.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{

  UserNSSimplifiedFSI::UserNSSimplifiedFSI():
    LinearProblemNSSimplifiedFSI()
  {}

  UserNSSimplifiedFSI::~UserNSSimplifiedFSI()
  = default;


  void UserNSSimplifiedFSI::userElementComputeNaturalBoundaryCondition( const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) {
    LinearProblemNSSimplifiedFSI::userElementComputeNaturalBoundaryCondition(elemPoint,elemIdPoint,iel,label);
    BoundaryCondition* BC;
    for (std::size_t iNeumannNormal(0);
         iNeumannNormal < m_boundaryConditionList.numNeumannNormalBoundaryCondition();
         iNeumannNormal++ ) {
      BC = m_boundaryConditionList.NeumannNormal(iNeumannNormal);
      for(auto it_labelNumber = BC->listLabel().begin();
           it_labelNumber != BC->listLabel().end();
           it_labelNumber++) {

        if( *it_labelNumber == label ){
          if( m_elemFieldNeumannNormal[iNeumannNormal].getType() == CONSTANT_FIELD && BC->typeValueBC() == FunctionT ){
            m_elemFieldNeumannNormal[iNeumannNormal].setValue( std::vector<double>( 1, inletPressure(m_fstransient->time ) ) );
          }
        }
      }
    }
  }
}
