//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userPoroElasticity.hpp"
#include "Core/felisceTransient.hpp"
#include "FiniteElement/elementVector.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce {

  UserPoroElasticity::UserPoroElasticity():LinearProblemPoroElasticity()
  {}

  UserPoroElasticity::~UserPoroElasticity()
  = default;
  void UserPoroElasticity::userElementInit() {
    switch(FelisceParam::instance().validationTest) {
    case 1:  // Static test //
      m_elemFieldForceX.initialize(QUAD_POINT_FIELD,*m_feDisp,/*numComp*/1);
      m_elemFieldForceY.initialize(QUAD_POINT_FIELD,*m_feDisp,/*numComp*/1);
      m_elemFieldSourceMass.initialize(CONSTANT_FIELD,*m_feDisp,/*numComp*/1);
      m_elemFieldSourceMass.update(2*m_k0);
      m_fxStaticTestCase.setParams(m_lambda,m_mu,m_b);
      m_fyStaticTestCase.setParams(m_mu);
      break;
    case 2: // Transient test //
      {
        double w=1.;
        m_elemFieldForceX.initialize(QUAD_POINT_FIELD,*m_feDisp,/*numComp*/1);
        m_fxTransientTestCase.setParams(m_mu, m_lambda,m_b,w,m_k0,m_M);
        m_xDispTransientTestCase.setParams(w); //Not the best location..
      }
      break;
    case 3:  // Zero Pressure test //
      m_elemFieldForceX.initialize(QUAD_POINT_FIELD,*m_feDisp,/*numComp*/1);
      m_elemFieldForceY.initialize(QUAD_POINT_FIELD,*m_feDisp,/*numComp*/1);
      m_fxZeroPressureTestCase.setParams(m_lambda,m_mu);
      m_fyZeroPressureTestCase.setParams(m_lambda,m_mu);
      break;

    }
  }


  void UserPoroElasticity::userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel,FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ARGUMENT(elemPoint);
    IGNORE_UNUSED_ARGUMENT(elemIdPoint);
    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
    switch(FelisceParam::instance().validationTest) {
    case 1:  // Static test //
      m_elemFieldForceX.setValue(m_fxStaticTestCase,*m_feDisp, m_fstransient->time,/*icomp*/0);
      m_elemFieldForceY.setValue(m_fyStaticTestCase,*m_feDisp, m_fstransient->time,/*icomp*/0);
      m_elementVector[0]->source(1.,*m_feDisp,m_elemFieldForceX,/*iblock*/0,/*numComp*/1);
      m_elementVector[0]->source(1.,*m_feDisp,m_elemFieldForceY,/*iblock*/1,/*numComp*/1);
      m_elementVector[0]->source(1.,*m_feDisp,m_elemFieldSourceMass,/*iblock*/2,/*numComp*/1); //mass source

      break;
    case 2: // Transient test //
      m_elemFieldForceX.setValue(m_fxTransientTestCase,*m_feDisp, m_fstransient->time,/*icomp*/0);
      m_elementVector[0]->source(1.,*m_feDisp,m_elemFieldForceX,/*iblock*/0,/*numComp*/1);
      break;
    case 3: // Zero Pressure //
      m_elemFieldForceX.setValue(m_fxZeroPressureTestCase,*m_feDisp, m_fstransient->time,/*icomp*/0);
      m_elemFieldForceY.setValue(m_fyZeroPressureTestCase,*m_feDisp, m_fstransient->time,/*icomp*/0);
      m_elementVector[0]->source(1.,*m_feDisp,m_elemFieldForceX,/*iblock*/0,/*numComp*/1);
      m_elementVector[0]->source(1.,*m_feDisp,m_elemFieldForceY,/*iblock*/1,/*numComp*/1);
      break;
    }
  }

  void UserPoroElasticity::userFinalizeEssBCTransient() {
    double time = m_fstransient->time;
    BoundaryCondition* bc;
    switch(FelisceParam::instance().validationTest) {
    case 1:  // Static test //
      bc = getBoundaryConditionList().Dirichlet(0);
      // std::set boundary condition with analytical function
      bc->setValue(this, m_yDispStaticTestCase, time);
      break;
    case 2:
      bc = getBoundaryConditionList().Dirichlet(0);
      // std::set boundary condition with analytical function
      bc->setValue(this, m_xDispTransientTestCase, time);
      break;
    case 3:
      bc = getBoundaryConditionList().Dirichlet(0);
      // std::set boundary condition with analytical function
      bc->setValue(this, m_xDispZeroPressureTestCase, time);

      bc = getBoundaryConditionList().Dirichlet(1);
      // std::set boundary condition with analytical function
      bc->setValue(this, m_yDispZeroPressureTestCase, time);
      break;
    }
  }

}
