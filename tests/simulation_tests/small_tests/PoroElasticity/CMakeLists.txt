# Install test SimTest_PoroElasticity

felisce_install_test_exe(
    NAME SimTest_PoroElasticity
    SOURCES main.cpp userPoroElasticity.cpp
    INSTALL simulation_tests/PoroElasticity
)

felisce_install_tests_data(
    NAME SimTest_PoroElasticity
    EXEC SimTest_PoroElasticity
    INSTALL simulation_tests/PoroElasticity
    CONFIG datafile additional_test_parameters.py
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_PoroElasticity
    INSTALL simulation_tests/PoroElasticity
)
