class AdditionalTestParameters:
    def __init__(self):
        """ The default constructor of the class
        """
        self.tolerance = 2.0e-3
        self.number_of_cores = 2
