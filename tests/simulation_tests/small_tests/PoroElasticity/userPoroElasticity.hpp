//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _USERPOROELASTICITY_HPP
#define _USERPOROELASTICITY_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemPoroElasticity.hpp"

namespace felisce {
  //===============FUNCTORS===============//
  //------------------zero-pressure test-case---------------//
  class DisplacementXZeroPressureTestCase {
  public:
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      return std::sin(x+y);
    }
  };
  class DisplacementYZeroPressureTestCase {
  public:
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      return std::cos(x+2*y);
    }
  };

  class ForceTermXCompZeroPressureTestCase {
  public:
    void setParams(double lambda,double mu){
      m_lambda=lambda;
      m_mu=mu;
    }
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      return (m_lambda+2*m_mu)*std::sin(x+y) + 2*(m_lambda+m_mu)*std::cos(x+2*y) + m_mu*std::sin(x+y);
    }
    double m_lambda,m_mu;
  };

  class ForceTermYCompZeroPressureTestCase {
  public:
    void setParams(double lambda,double mu){
      m_lambda=lambda;
      m_mu=mu;
    }
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      return (m_lambda+m_mu)*std::sin(x+y) + (4*m_lambda+9*m_mu)*std::cos(x+2*y);
    }
    double m_lambda,m_mu;
  };
  //------------------static test-case---------------//
  // not exactly static.
  class DisplacementYStaticTestCase {
  public:
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      return -y*x*(1-x);
    }
  };

  class ForceTermXCompStaticTestCase {
  public:
    void setParams(double lambda,double mu,double b){
      m_lambda=lambda;
      m_mu=mu;
      m_b=b;
    }
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      IGNORE_UNUSED_ARGUMENT(y);
      return (-2*x+1)*(m_lambda+m_mu+m_b);
    }
    double m_lambda,m_mu,m_b;
  };

  class ForceTermYCompStaticTestCase {
      public:
    void setParams(double mu){
      m_mu=mu;
    }
    double operator()(double x,double y,double /*z*/, double /*t*/) const {
      IGNORE_UNUSED_ARGUMENT(x);
      return -m_mu*2*y;
    }
    double m_mu;
  };
  //----------------------------------transient-test-case----//
  class TFunction {
  public:
    TFunction() {
      //m_pi=std::atan(1.)*4.;
      m_pi=3.14159265;
    }
    void setParams(double b,double w,double k0,double M) {
      m_b=b;
      m_w=w;
      m_k0=k0;
      m_M=M;

      m_A=(m_b*std::pow(m_pi,3)*m_w*m_k0*std::pow(m_M,2))/(std::pow(m_w,2)+std::pow(m_k0*m_pi*m_M,2));
      m_B=m_w*m_A/(m_k0*m_pi*m_pi*m_M);
      // std::cout<<"b: "<<m_b<<std::endl;
      // std::cout<<"w: "<<m_w<<std::endl;
      // std::cout<<"k0: "<<m_k0<<std::endl;
      // std::cout<<"M: "<<m_M<<std::endl;
      // std::cout<<"A: "<<m_A<<std::endl;
      // std::cout<<"B: "<<m_B<<std::endl;
    }
    double m_pi,m_b,m_w,m_k0,m_M;

    double m_A,m_B;

    double operator()(double t) const {
      return -m_A*exp(-m_k0*m_pi*m_pi*m_M*t) + m_A*std::cos(m_w*t)+m_B*std::sin(m_w*t);
    }
  };

  class ForceTermXCompTransientTestCase {

  public:
    ForceTermXCompTransientTestCase() {
      //m_pi=std::atan(1.)*4.;
      m_pi=3.14159265;
    }

    void setParams(double mu,double lambda,double b,double w,double k0,double M) {
      m_mu=mu;
      m_lambda=lambda;
      m_b=b;
      m_w=w;

      tf.setParams(b,w,k0,M);
    }

    double operator()(double x,double y,double z,double t) const {
      IGNORE_UNUSED_ARGUMENT(y);
      IGNORE_UNUSED_ARGUMENT(z);
      return -(-m_pi*m_pi*(2*m_mu+m_lambda)*std::sin(m_w*t) - m_b*m_pi*tf(t))*std::cos(m_pi*x);
      }

    double m_pi,m_mu,m_lambda,m_b,m_w;

    TFunction tf;
  };

  class DisplacementXTransientTestCase {
  public:
    DisplacementXTransientTestCase(){
      //m_pi=std::atan(1.)*4.;
      m_pi=3.14159265;
    }
    void setParams(double w){
      m_w=w;
    }
    double operator()(double x,double /*y*/,double /*z*/, double t) const {
      return std::cos(m_pi*x)*std::sin(m_w*t);
    }
    double m_pi,m_w;
  };
  //===============THE CLASS===============//
  class UserPoroElasticity: public LinearProblemPoroElasticity {
  public:
    UserPoroElasticity();
    ~UserPoroElasticity() override;

    void userElementInit() override;
    void userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel,FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
    void userFinalizeEssBCTransient() override;


    ElementField m_elemFieldForceX;
    ElementField m_elemFieldForceY;
    ElementField m_elemFieldSourceMass;

    //Data for the Zero Pressure test case
    DisplacementXZeroPressureTestCase m_xDispZeroPressureTestCase;
    DisplacementYZeroPressureTestCase m_yDispZeroPressureTestCase;
    ForceTermXCompZeroPressureTestCase  m_fxZeroPressureTestCase;
    ForceTermYCompZeroPressureTestCase m_fyZeroPressureTestCase;
    //Data for the static test
    DisplacementYStaticTestCase m_yDispStaticTestCase;
    ForceTermXCompStaticTestCase m_fxStaticTestCase;
    ForceTermYCompStaticTestCase m_fyStaticTestCase;
    //Data for the transient test
    ForceTermXCompTransientTestCase m_fxTransientTestCase;
    DisplacementXTransientTestCase m_xDispTransientTestCase;
  };
}

#endif
