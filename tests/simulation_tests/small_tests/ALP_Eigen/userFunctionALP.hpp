//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _USERFUNCTIONALP_HPP
#define _USERFUNCTIONALP_HPP

// System includes
#include <math.h>

// External includes

// Project includes
#include "Solver/cardiacFunction.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {
  //A class to simulate a applied current.
  class constantALPIapp:
    public AppCurrent {
  public:
    constantALPIapp();
    ~constantALPIapp() override {};
    double Iapp(double x, double y, double z, double t=0., double dist=0) const override;
    double operator() (double x, double y, double z, double t=0., double dist=0) const override;
  };

  //A class to apply constant current for y > 0.
  class leftSideALPIapp:
    public AppCurrent {
  public:
    leftSideALPIapp();
    ~leftSideALPIapp() override {};
    double Iapp(double x, double y, double z, double t=0., double dist=0) const override;
    double operator() (double x, double y, double z, double t=0., double dist=0) const override;
  };

  //A class to apply constant current for y > 0.
  class leftSide2ALPIapp:
    public AppCurrent {
  public:
    leftSide2ALPIapp();
    ~leftSide2ALPIapp() override {};
    double Iapp(double x, double y, double z, double t=0., double dist=0) const override;
    double operator() (double x, double y, double z, double t=0., double dist=0) const override;
  };

  //A class to simulate a current in a square.
  class circleALPIapp:
    public AppCurrent {
  public:
    circleALPIapp();
    ~circleALPIapp() override {};
    double Iapp(double x, double y, double z, double t=0., double dist=0) const override;
    double operator() (double x, double y, double z, double t=0., double dist=0) const override;
  };
  //A class to simulate a current in a square.
  class circle2ALPIapp:
    public AppCurrent {
  public:
    circle2ALPIapp();
    ~circle2ALPIapp() override {};
    double Iapp(double x, double y, double z, double t=0., double dist=0) const override;
    double operator() (double x, double y, double z, double t=0., double dist=0) const override;
  };

}


#endif
