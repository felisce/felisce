//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "userEigenProblemALP.hpp"


namespace felisce {
  /*!
   \class UserEigenProblemALP
   \authors E. Schenone
   \date 09/04/2013
   \brief EigenProblemALP with user defined functions.
   */

  UserEigenProblemALP::UserEigenProblemALP():
    EigenProblemALP(),
    m_numLeads(0),
    m_numSourceNode(0),
    m_transferMatrix(NULL),
    m_matchNode(NULL),
    m_ecgOperator(NULL)
  {}

  UserEigenProblemALP::~UserEigenProblemALP() {
    for (int i=0; i<m_numLeads; i++) {
      delete [] m_ecgOperator[i];
    }
    delete [] m_ecgOperator;
  }

  void UserEigenProblemALP::initializeECG() {
    readMatrix();
    readMatch();
    m_outputFileName=FelisceParam::instance().resultDir + "/" + FelisceParam::instance().ECGfileName + ".tab";
    writeECGoperator();
  }

  void UserEigenProblemALP::writeECG(int iteration) {
    if( iteration % FelisceParam::instance().frequencyWriteSolution == 0) {
      double pot[m_numLeads];
      double deltaV = FelisceParam::instance().vMax - FelisceParam::instance().vMin;

      for (int i=0; i<m_numLeads; i++) {
        pot[i] = 0.;
        for(int j=0; j<m_dimRomBasis; j++) {
          pot[i] += m_ecgOperator[i][j] * m_xi[j] / deltaV;
        }
      }

      std::ofstream ecgfile;
      ecgfile.open(m_outputFileName.c_str(), std::ios::out|std::ios::app);

      double time = FelisceParam::instance().time + (iteration)*FelisceParam::instance().timeStep;
      ecgfile << time << " ";
      for (int i=0; i<m_numLeads; i++) {
        ecgfile << pot[i] << " ";
      }
      ecgfile << "\n";
      ecgfile.close();
    }

  }

  void UserEigenProblemALP::writeECGoperator() {
    std::vector<PetscVector> extTransferMatrix;
    extTransferMatrix.resize(m_numLeads);
    for (int i=0; i<m_numLeads; i++) {
      extTransferMatrix[i].duplicateFrom(m_basis[0]);
      extTransferMatrix[i].set(0.);
    }

    felInt indexUe;
    double value;
    for (int i=0; i<m_numLeads; i++) {
      for(int j=0; j<m_numSourceNode  ; j++) {
        indexUe = m_matchNode[j]-1;
        AOApplicationToPetsc(m_ao,1,&indexUe);
        value = m_transferMatrix[i][j];
        extTransferMatrix[i].setValues(1,&indexUe,&value,INSERT_VALUES);
      }
      extTransferMatrix[i].assembly();
    }

    m_ecgOperator = new double*[m_numLeads];
    for (int i=0; i<m_numLeads; i++) {
      m_ecgOperator[i] = new double [m_dimRomBasis];
    }
    for (int i=0; i<m_numLeads; i++) {
      for (int j=0; j<m_dimRomBasis; j++) {
        dot(extTransferMatrix[i],m_basis[j],&value);
        m_ecgOperator[i][j] = value;
      }
    }

    for (int i=0; i<m_numLeads; i++)
      extTransferMatrix[i].destroy();

    for (int i=0; i<m_numLeads; i++) {
      delete [] m_transferMatrix[i];
    }
    delete m_transferMatrix;

    delete [] m_matchNode;

  }

  void UserEigenProblemALP::updateEcgOperator() {
    double** newEcg = new double* [m_numLeads];
    for (int i=0; i<m_numLeads; i++) {
      newEcg[i] = new double[m_dimRomBasis];
    }

    for (int i=0; i<m_numLeads; i++) {
      for (int j=0; j<m_dimRomBasis; j++) {
        newEcg[i][j] = 0.;
        for (int l=0; l<m_dimRomBasis; l++) {
          // delta ecg_ij = sum_l (M_jl * ecg_il )
          newEcg[i][j] += m_matrixM[j][l] * m_ecgOperator[i][l];
        }
      }
    }

    double dt = FelisceParam::instance().timeStep;
    for (int i=0; i<m_numLeads; i++) {
      for (int j=0; j<m_dimRomBasis; j++)
        m_ecgOperator[i][j] += dt * newEcg[i][j];
    }

    for (int i=0; i<m_numLeads; i++) {
      delete [] newEcg[i];
    }
    delete [] newEcg;

  }

  void UserEigenProblemALP::readMatch() {
    // Input files.
    std::string  matchFileName = FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().ECGmatchFile;// load the match file (heart nodes)
    if (FelisceParam::verbose() > 0) {
      std::cout << "Read match file " << matchFileName << std::endl;
    }

    std::ifstream matchFile(matchFileName.c_str());
    if(!matchFile) {
      std::cout << "Fatal error: cannot open match file " << matchFileName << std::endl;
      exit(1);
    }

    int dummy;
    matchFile >> dummy;
    if(dummy != m_numSourceNode) {
      std::cout << "Fatal error : match file nodes(" << dummy << ") is different from nb source nodes ("<< m_numSourceNode <<")" << std::endl;
      exit(1);
    }

    m_matchNode = new int[m_numSourceNode];
    for(int j=0; j<m_numSourceNode; j++) {
      matchFile >> m_matchNode[j];
    }
    matchFile.close();

  }

  void UserEigenProblemALP::readMatrix() {
    std::string matrixFileName=FelisceParam::instance().inputDirectory + "/" + FelisceParam::instance().ECGmatrixFile;
    // load the transfert matrix
    std::ifstream matrixFile(matrixFileName.c_str());
    if ( !matrixFile ) {
      std::cout << " ERROR: Can not open file "+matrixFileName+"."<< std::endl;
      exit(1);
    }
    matrixFile >> m_numLeads;
    matrixFile >> m_numSourceNode;
    m_transferMatrix = new double* [m_numLeads];
    for (int i=0; i<m_numLeads; i++) {
      m_transferMatrix[i] = new double[m_numSourceNode];
    }

    for(int i=0; i<m_numLeads; i++) {
      for(int j=0; j<m_numSourceNode; j++) {
        matrixFile >> m_transferMatrix[i][j];
      }
    }
    matrixFile.close();
  }

}
