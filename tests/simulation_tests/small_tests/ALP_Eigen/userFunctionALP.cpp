//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "userFunctionALP.hpp"

namespace felisce {
  //A class to simulate a constant applied current.
  constantALPIapp::constantALPIapp():
    AppCurrent()
  {}

  double constantALPIapp::Iapp(double x, double y, double z, double t, double) const {
    IGNORE_UNUSED_ARGUMENT(x);
    IGNORE_UNUSED_ARGUMENT(y);
    IGNORE_UNUSED_ARGUMENT(z);
    IGNORE_UNUSED_ARGUMENT(t);
    double iapp=0.0;
    iapp = 2.0/50.0;
    return iapp;
  }

  double constantALPIapp::operator() (double x, double y, double z, double t, double) const {
    return Iapp(x,y,z,t);
  }

  //A class to apply constant current for x < 0.1.
  leftSideALPIapp::leftSideALPIapp():
    AppCurrent()
  {}

  double leftSideALPIapp::Iapp(double x, double y, double z, double t, double) const {
    IGNORE_UNUSED_ARGUMENT(y);
    IGNORE_UNUSED_ARGUMENT(z);
    IGNORE_UNUSED_ARGUMENT(t);
    double iapp=0.0;
    if (x < 0.1) {
      iapp = 2.0/50.0;
    }
    return iapp;
  }

  double leftSideALPIapp::operator() (double x, double y, double z, double t, double) const {
    return Iapp(x,y,z,t);
  }

  //A class to apply constant current for x < 0.1.
  leftSide2ALPIapp::leftSide2ALPIapp():
    AppCurrent()
  {}

  double leftSide2ALPIapp::Iapp(double x, double y, double z, double t, double) const {
    IGNORE_UNUSED_ARGUMENT(z);
    IGNORE_UNUSED_ARGUMENT(t);
    double iapp=0.0;
    if ((x > 0.15)&&(x < 0.2)&&(y < 0.4)) {
      iapp = 2.0/50.0;
    }
    return iapp;
  }

  double leftSide2ALPIapp::operator() (double x, double y, double z, double t, double) const {
    return Iapp(x,y,z,t);
  }

  //A class to simulate a constant applied current.
  circleALPIapp::circleALPIapp():
    AppCurrent()
  {}

  double circleALPIapp::Iapp(double x, double y, double z, double t, double) const {
    IGNORE_UNUSED_ARGUMENT(t);
    double iapp=0.0;
    double x1 = x - FelisceParam::instance().x_infarct;
    double y1 = y - FelisceParam::instance().y_infarct;
    double z1 = z - FelisceParam::instance().z_infarct;
    double sum1 = sqrt(x1*x1 + y1*y1 + z1*z1);

    //if simulation starts later than t=0
    if (sum1 < FelisceParam::instance().radius_infarct) {
      iapp = 2.0/50.0;
    }
    return iapp;
  }

  double circleALPIapp::operator() (double x, double y, double z, double t, double) const {
    return Iapp(x,y,z,t);
  }

  //A class to simulate a constant applied current.
  circle2ALPIapp::circle2ALPIapp():
    AppCurrent()
  {}

  double circle2ALPIapp::Iapp(double x, double y, double z, double t, double) const {
    IGNORE_UNUSED_ARGUMENT(t);
    double iapp=0.0;
    double x2 = x - 0.;
    double y2 = y - 0.75;
    double z2 = z - 0.;
    double sum2 = sqrt(x2*x2 + y2*y2 + z2*z2);

    //if simulation starts later than t=0
    if (sum2 < FelisceParam::instance().radius_infarct) {
      iapp = 2.0/50.0;
    }
    return iapp;
  }

  double circle2ALPIapp::operator() (double x, double y, double z, double t, double) const {
    return Iapp(x,y,z,t);
  }

}

