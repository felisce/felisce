//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "Core/felisce.hpp"
#include "userALPModel.hpp"
#include "userEigenProblemALP.hpp"

using namespace felisce;

int main(const int argc, const char** argv) {
  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  UserALPModel model;
  std::vector<EigenProblemALP*> eigenPb;

  eigenPb.push_back(new UserEigenProblemALP);

  model.initializeModel(opt,fstransient);

  model.initializeEigenProblem(eigenPb);

  if ( FelisceParam::instance().solveEigenProblem ) {
    model.solveEigenProblem();
  } else {
    // time loop
    while( !model.hasFinished() ) {
      model.forward();
    }
  }

}
