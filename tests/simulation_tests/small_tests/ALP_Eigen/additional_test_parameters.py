class AdditionalTestParameters:
    def __init__(self):
        """ The default constructor of the class
        """
        self.number_of_cores = 1 # TODO: This not working in MPI
        ON = True
        OFF = False
        self.run_test = @FELISCE_WITH_SLEPC@ and "@CMAKE_SYSTEM_NAME@" != "Darwin"
