//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _USEREIGENPROBLEMALP_HPP
#define _USEREIGENPROBLEMALP_HPP

// System includes
#include <math.h>

// External includes

// Project includes
#include "Solver/eigenProblemALP.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {

  /*!
   \class UserEigenProblemALP
   \authors E. Schenone
   \date 09/04/2013
   \brief EigenProblemALP with user defined functions.
   */

  class UserEigenProblemALP:
    public EigenProblemALP {
  public:
    UserEigenProblemALP();
    ~UserEigenProblemALP() override;
    void initializeECG() override;
    void writeECG(int iteration) override;
    void writeECGoperator();
    void updateEcgOperator() override;
    void readMatch();
    void readMatrix();
  private:
    int m_numLeads;
    int m_numSourceNode;
    std::string m_outputFileName;
    double** m_transferMatrix;
    int* m_matchNode;
    double** m_ecgOperator;
  };
}


#endif
