//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

// System includes

// External includes

// Project includes
#include "userALPModel.hpp"
#include "userFunctionALP.hpp"

namespace felisce {
  /*!
   \class UserALPModel
   \authors E. Schenone
   \date 09/04/2013
   \brief ALPModel with user defined functions.
   */

  UserALPModel::UserALPModel():
    ALPModel()
  {}

  UserALPModel::~UserALPModel()
  {}


  void UserALPModel::postAssembleMatrix(const int iProblem) {
    std::map<std::string, int> mapOfType;
    mapOfType["ellibi"] = 1;
    mapOfType["constant"] = 2;
    mapOfType["circle"] = 3;
    mapOfType["leftside"] = 4;

    bool warning_Iapp = true;

    for (int idApp=0; idApp<FelisceParam::instance().numberOfSource; idApp++) {
      std::vector <double> iAppValue;
      AppCurrent* iApp = nullptr;

      switch(mapOfType[FelisceParam::instance().typeOfAppliedCurrent]) {
      case 1 :
        if (!(FelisceParam::instance().typeOfAppliedCurrent == "ellibi")) {
          std::cout << "ERROR : no match of applied current type !\n";
          exit(1);
        }
        iApp = new AppCurrent();
        warning_Iapp = false;
        break;
      case 2 :
        if (!(FelisceParam::instance().typeOfAppliedCurrent == "constant")) {
          std::cout << "ERROR : no match of apllied current type ! \n";
          exit(1);
        }
        iApp = new constantALPIapp();
        warning_Iapp = false;
        break;
      case 3 :
        if (!(FelisceParam::instance().typeOfAppliedCurrent == "circle")) {
          std::cout << "ERROR : no match of applied current type ! \n";
          exit(1);
        }
        if (idApp == 0) {
          iApp = new circleALPIapp();
        } else if (idApp ==1) {
          iApp = new circle2ALPIapp();
        }
        warning_Iapp = false;
        break;
      case 4 :
        if (!(FelisceParam::instance().typeOfAppliedCurrent == "leftside")) {
          std::cout << "ERROR : no match of applied current type ! \n";
          exit(1);
        }
        if (idApp == 0) {
          iApp = new leftSideALPIapp();
        } else if (idApp ==1) {
          iApp = new leftSide2ALPIapp();
        }
        warning_Iapp = false;
        break;
      }
      if (warning_Iapp) {
        std::cout << "WARNING !\nApplied current not defined : used non pathological case for ellibi geometry.\n";
        iApp = new AppCurrent();
      }
      iApp->initialize(m_fstransient);
      m_eigenProblem[iProblem]->evalFunctionOnDof(*iApp,iAppValue);
      m_eigenProblem[iProblem]->setIapp(iAppValue, idApp);
      delete iApp;
    }

  }

}
