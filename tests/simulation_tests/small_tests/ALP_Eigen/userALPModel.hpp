//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    E. Schenone
//

#ifndef _USERALPMODEL_HPP
#define _USERALPMODEL_HPP

// System includes
#include <math.h>

// External includes
#include <petscvec.h>

// Project includes
#include "Model/ALPModel.hpp"
#include "Core/felisceParam.hpp"

namespace felisce {

  /*!
   \class UserALPModel
   \authors E. Schenone
   \date 09/04/2013
   \brief ALPModel with user defined functions.
   */

  class UserALPModel:
    public ALPModel {
  public:
    UserALPModel();
    ~UserALPModel() override;
    void postAssembleMatrix(const int iProblem) override;
  };
}


#endif
