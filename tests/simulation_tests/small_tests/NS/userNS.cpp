//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userNS.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce {

  UserNS::UserNS():
    LinearProblemNS()
  {}

  UserNS::~UserNS()
  = default;

  void UserNS::userElementInit() {
    //ElementField initialisation
    m_elemFieldVelocity.initialize(QUAD_POINT_FIELD,*m_feVel,this->dimension());
    m_elemFieldPressure.initialize(QUAD_POINT_FIELD,*m_fePres);
    m_elemFieldUn_1.initialize(DOF_FIELD,*m_feVel,this->dimension());
  }

  void UserNS::userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                            felInt& iel,FlagMatrixRHS flagMatrixRHS) {
    IGNORE_UNUSED_ARGUMENT(elemPoint);
    IGNORE_UNUSED_ARGUMENT(elemIdPoint);
    IGNORE_UNUSED_ARGUMENT(iel);
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
    for ( int iDimension = 0; iDimension < this->dimension(); iDimension++)
      m_elemFieldVelocity.setValue(m_fct, *m_feVel,iDimension);
    m_elementVector[0]->source(1.,*m_feVel,m_elemFieldVelocity,0,this->dimension());
  }
}
