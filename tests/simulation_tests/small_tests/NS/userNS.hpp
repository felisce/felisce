//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _UserNS_HPP
#define _UserNS_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNS.hpp"

namespace felisce {
  class MyFunctionTime {
  public:
    double operator() (double x,double y,double z, double t) const {
      IGNORE_UNUSED_ARGUMENT(z);
      return (1.-x)*x*(1.-y)*y*std::cos(t)+2.*std::sin(t)*(y*(1.-y) + x*(1.-x));
    }
  };
  class MyFunction {
  public:
    double operator() (double x,double y,double z) const {
      IGNORE_UNUSED_ARGUMENT(x);
      IGNORE_UNUSED_ARGUMENT(y);
      IGNORE_UNUSED_ARGUMENT(z);
      return 0.;
    }
  };

  /*!
   \class UserNS
   \authors J. Foulon & J-F. Gerbeau & V. Martin
   \date 02/02/2011
   \brief Manage user specific functions for laplacian problem.
   */

  class UserNS:
    public LinearProblemNS {
  public:
    UserNS();
    ~UserNS() override;
    void userElementInit() override;
    void userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,
                            felInt& iel,FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;
  private:
    ElementField m_elemFieldVelocity;
    ElementField m_elemFieldPressure;
    ElementField m_elemFieldUn_1;
    MyFunction m_fct;
//     MyFunctionTime m_fctTime;
  };
}


#endif
