//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "userBC.hpp"
#include "Model/elasticityModel.hpp"

using namespace felisce;

int main(const int argc, const char** argv) {
  // Read command line and data file
  CommandLineOption opt(argc, argv);
  FelisceParam::instance().initialize(opt);
  FelisceTransient fstransient;

  ElasticityModel model;
  model.initializeModel(opt,fstransient);

  auto volumic_force_cst = [](int, double, double, double) -> double {
    return 0.0;
  };

  UserLinearProblemElasticity* linear_problem = new UserLinearProblemElasticity(volumic_force_cst);

  const bool do_use_snes = true;
  model.initializeLinearProblem(linear_problem, do_use_snes);

  PetscPrintf(MpiInfo::petscComm(),"Initialized\n");
  
  model.SolveStaticProblem();

  PetscPrintf(MpiInfo::petscComm(),"Done\n");
}
