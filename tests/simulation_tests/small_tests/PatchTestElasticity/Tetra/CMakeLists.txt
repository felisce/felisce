# Install test SimTest_PatchTestElasticity_Tetra

felisce_install_tests_data(
    NAME SimTest_PatchTestElasticity_Tetra
    EXEC ../SimTest_PatchTestElasticity
    INSTALL simulation_tests/PatchTestElasticity/Tetra
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_PatchTestElasticity_Tetra
    INSTALL simulation_tests/PatchTestElasticity/Tetra
)
