# Install test SimTest_PatchTestElasticity_Prism

felisce_install_tests_data(
    NAME SimTest_PatchTestElasticity_Prism
    EXEC ../SimTest_PatchTestElasticity
    INSTALL simulation_tests/PatchTestElasticity/Prism
    CONFIG datafile
    FILES 
    DIRS RefSolutions
)

felisce_add_test(
    NAME SimTest_PatchTestElasticity_Prism
    INSTALL simulation_tests/PatchTestElasticity/Prism
)
