//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes

// Project includes
#include "userBC.hpp"
#include "Core/felisceTransient.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{
// constructor, call LinearProblemheat constructor
UserLinearProblemElasticity::UserLinearProblemElasticity(SpatialForce force):
  LinearProblemElasticityDynamic(force) {
}

/***********************************************************************************/
/***********************************************************************************/

// destructor
UserLinearProblemElasticity::~UserLinearProblemElasticity() = default;

/***********************************************************************************/
/***********************************************************************************/

// set boundary condition value with analytical function
void UserLinearProblemElasticity::userFinalizeEssBCTransient() {
  // Get the boundary condition we want to set the value
  BoundaryCondition* bc = m_boundaryConditionList[0];

  // Set boundary condition with analytical function
  if (bc->getComp().size() == 2) {
    bc->setValueVec(this, m_transientDirichlet2D);
  } else {
    bc->setValueVec(this, m_transientDirichlet3D);
  }
}
}
