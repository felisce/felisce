//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

#ifndef _USERBC_HPP
#define _USERBC_HPP

// System includes
#include <cmath>

// External includes

// Project includes
#include "Solver/linearProblemElasticity.hpp"

namespace felisce
{
class TransientDirichlet2D {
public:
  double operator() (felInt icomp, double x,double y,double z) const {
    UBlasMatrix A(3,3);
    A(0,0) = 1.0e-3;  A(0,1) = 2.0e-3; A(0,2) = 0.0;
    A(1,0) = 0.5e-3;  A(1,1) = 0.7e-3; A(1,2) = 0.0;
    A(2,0) = 0.0;     A(2,1) = 0.0;    A(2,2) = 0.0;
    UBlasVector b(3);
    b[0] = 0.5e-3;
    b[1] = -0.2e-3;
    b[2] = 0.0;

    UBlasVector x_vector(3);
    x_vector[0] = x;
    x_vector[1] = y;
    x_vector[2] = z;

    const UBlasVector u = prod(A, x_vector) + b;

    return u[icomp];
  }
};
class TransientDirichlet3D {
public:
  double operator() (felInt icomp, double x,double y,double z) const {
    UBlasMatrix A(3,3);
    A(0,0) = 1.0e-3;  A(0,1) = 2.0e-3; A(0,2) = 0.0;
    A(1,0) = 0.5e-3;  A(1,1) = 0.7e-3; A(1,2) = 0.1e-3;
    A(2,0) = -0.2e-3; A(2,1) = 0.0;     A(2,2) = -0.3e-3;
    UBlasVector b(3);
    b[0] = 0.5e-3;
    b[1] = -0.2e-3;
    b[2] = 0.7e-3;

    UBlasVector x_vector(3);
    x_vector[0] = x;
    x_vector[1] = y;
    x_vector[2] = z;

    const UBlasVector u = prod(A, x_vector) + b;

    return u[icomp];
  }
};

class UserLinearProblemElasticity:
  public LinearProblemElasticityDynamic {
public:
  UserLinearProblemElasticity(SpatialForce force);

  ~UserLinearProblemElasticity() override;

  void userFinalizeEssBCTransient() override;

private:
  TransientDirichlet2D m_transientDirichlet2D;

  TransientDirichlet3D m_transientDirichlet3D;
};
}

#endif
