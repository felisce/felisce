//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userHarmonicExtension.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{
  UserHarmonicExtension::UserHarmonicExtension():LinearProblemHarmonicExtension(){
  }

  UserHarmonicExtension::~UserHarmonicExtension()= default;

  void UserHarmonicExtension::userFinalizeEssBCTransient(){
    double time = m_fstransient->time;
    BoundaryCondition* bc = m_boundaryConditionList[1];
    bc->setValueVec(this, _transientDirichlet, time);
  }
}

