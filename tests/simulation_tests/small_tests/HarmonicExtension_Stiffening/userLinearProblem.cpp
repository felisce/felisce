//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

// System includes

// External includes

// Project includes
#include "userLinearProblem.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "FiniteElement/elementMatrix.hpp"

namespace felisce
{
  UserLinearProblem::UserLinearProblem()= default;

  UserLinearProblem::~UserLinearProblem()= default;

  void UserLinearProblem::userFinalizeEssBCTransient(){
    /*
    BoundaryCondition& BC = *getBoundaryConditionList().Dirichlet(0);

    if(m_fstransient->iteration>1){
    //The boundary condition is the first BC of the fluid linear problem (although it is the third BC listed in the data file)


        //We get the unknown and its id among the variables
        int iUnknown = BC.getUnknown();
        int idVar = m_listUnknown.idVariable(iUnknown);


        //Variable declaration for the mapping
        felInt idEltInSupportLocal = 0;
        felInt idEltInSupportGlobal = 0;
        felInt idDof;
        double aux;

        BC.ValueBCInSupportDof().clear();
        for (unsigned iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++){

            //For each "support dof" of the BC, we get the id of the element
            //Are those the node of the finite element ?
            idEltInSupportLocal = BC.idEltAndIdSupport()[iSupportDofBC].first;

            // m_mapping : mapping between local to global ordering of element
            // Is local/global related to parallel processing ?
            ISLocalToGlobalMappingApply(m_mappingElem,1,&idEltInSupportLocal,&idEltInSupportGlobal);

            //For each component of the variable (here velocity number of component will be 3)
            for(auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++){
                //We want to get the id of the degree of freedom using the id of the element -converted in global system - its support, the variable, and finally its component number
                dof().loc2glob(idEltInSupportGlobal, BC.idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);

                // "Maps a std::set of integers in the application-defined ordering to the PETSc ordering."
                AOApplicationToPetsc(m_externalAO[0],1,&idDof);

                //We get the value of the velocity std::vector corresponding to the idDof
                this->externalVec(0).getValues(1, &idDof, &aux);
                //Finally, we push_back the value of the corresponding component of the mesh velocity into the BC
                BC.ValueBCInSupportDof().push_back(-aux);
            }
        }
    }
    else{
        BC.setValueVec(this, f_velZero, m_fstransient->time);
    }

  */
  }


  void UserLinearProblem::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label)
  {
    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
    (void) label;

    CurvilinearFiniteElement& curvFeVel = *m_listCurvilinearFiniteElement[m_iVelocity];
    m_elemFieldPreviousVel.initialize(DOF_FIELD,curvFeVel, this->dimension());

    // Stabilisation of backflow
    if (FelisceParam::instance().addedBoundaryFlag == 2){
      double coef = FelisceParam::instance().stabilizationCoef * FelisceParam::instance().density;
      for(std::size_t ilab=0; ilab < FelisceParam::instance().stabilizationLabel.size(); ilab++){
        if (label == FelisceParam::instance().stabilizationLabel[ilab]){
          m_elemFieldPreviousVel.setValue(sequentialSolution(), curvFeVel, iel, m_iVelocity, m_ao, dof());
          m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef, curvFeVel , m_elemFieldPreviousVel, 0, 0, dimension(), FelisceParam::instance().addedBoundaryFlag);
        }
      }
    }
  }


}
