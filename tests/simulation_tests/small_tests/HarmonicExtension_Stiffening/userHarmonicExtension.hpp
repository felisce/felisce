//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _UserHarmonicExtension_HPP
#define _UserHarmonicExtension_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHarmonicExtension.hpp"

namespace felisce{
    class MyFctTransientDirichlet{
        public:
            double operator()(int numComp, double x, double y, double z, double t) const{
                IGNORE_UNUSED_ARGUMENT(x);
                IGNORE_UNUSED_ARGUMENT(y);
                IGNORE_UNUSED_ARGUMENT(z);
                double k = 0.1;
                if(numComp==0){
                    return k*t;
                }
                else{
                    return 0;
                }
            }
    };

    class UserHarmonicExtension:public LinearProblemHarmonicExtension{
    private:
        MyFctTransientDirichlet _transientDirichlet;
    public:
        UserHarmonicExtension();
        ~UserHarmonicExtension() override;
        void userFinalizeEssBCTransient() override;
    };
}

#endif
