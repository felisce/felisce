//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    J. Foulon & J-F. Gerbeau & V. Martin
//

#ifndef _UserLinearProblem_HPP
#define _UserLinearProblem_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemNS.hpp"

namespace felisce{

    class VelZero{
        public:
            double operator() (int comp, double x,double y,double z,double t)  const{
                IGNORE_UNUSED_ARGUMENT(comp);
                IGNORE_UNUSED_ARGUMENT(x);
                IGNORE_UNUSED_ARGUMENT(y);
                IGNORE_UNUSED_ARGUMENT(z);
                IGNORE_UNUSED_ARGUMENT(t);
                return 0.;
            }
    };


    class UserLinearProblem:public LinearProblemNS{
        public:
            UserLinearProblem();
            ~UserLinearProblem() override;
            void userFinalizeEssBCTransient() override;
            void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel, int label) override;

        private:
//             VelZero f_velZero;
            ElementField m_elemFieldPreviousVel;
    };
}



#endif
