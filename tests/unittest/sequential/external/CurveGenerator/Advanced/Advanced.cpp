//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

#if FELISCE_WITH_CURVEGEN

// Project includes
#include "auxiliar_definitions.hpp"
#include "curveGen.hpp"
#include "dataCurve.hpp"

TEST_SUITE("TestCurveGeneratorAdvanced")
{
    TEST_CASE("AdvancedCase")
    {
        const int NUM_SUBDIV = 1000;

        DataCurve dc1("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2
        DataCurve dc2("../Resources/q_asc_aorta_rest_miccai_bis.dat"); // same data but type 3
        DataCurve dc3("../Resources/q_asc_aorta_rest_miccai_bis_deriv.dat"); // type 4, analytical derivative
        DataCurve dc4("../Resources/q_asc_aorta_rest_miccai_bis_scd_deriv.dat"); // type 4, analytical scd derivative
        DataCurve dc5("../Resources/testspline.dat");
        DataCurve dc6("../Resources/p_asc_aorta_rest_miccai.dat");

        CurveGen cg1(dc1,NUM_SUBDIV);
        CurveGen cg2(dc2,NUM_SUBDIV);
        CurveGen cg3(dc3,NUM_SUBDIV);
        CurveGen cg4(dc4,NUM_SUBDIV);
        CurveGen cg5(dc5,NUM_SUBDIV);
        CurveGen cg6(dc6,NUM_SUBDIV);

        CurveGen cg1der = cg1.derivative(true);
        CurveGen cg1scdder = cg1der.derivative(true);
        CurveGen cg1_3period(dc1,NUM_SUBDIV);
        cg1_3period.extendByPeriodicity(3);

        // std::cout << "The two following values should be the same: " << std::endl;
        // std::cout << "  Integral 1 = " << cg1.integral() << std::endl;
        // std::cout << "  Integral 2 = " << cg2.integral() << std::endl;
        FELISCE_CHECK_NEAR(cg1.integral(), cg2.integral(), 1.0e-3);

        // std::cout << "The two following values should be close: " << std::endl;
        // std::cout << "  Integral 1der = " << cg1der.integral() << std::endl;
        // std::cout << "  Integral 3    = " << cg3.integral() << std::endl;
        FELISCE_CHECK_NEAR(cg1der.integral(), cg3.integral(), 1.0e-3);

        // std::cout << "The two following values should be close: " << std::endl;
        // std::cout << "  Integral 1scdder = " << cg1scdder.integral() << std::endl;
        // std::cout << "  Integral 4       = " << cg4.integral() << std::endl;
        FELISCE_CHECK_NEAR(cg1scdder.integral(), cg4.integral(), 1.0e-3);

        std::ofstream outputfile1("advanced_output1");
        std::ofstream outputfile1der("advanced_output1der");
        std::ofstream outputfile1scdder("advanced_output1scdder");
        std::ofstream outputfile1_3period("advanced_output1_3period");
        std::ofstream outputfile2("advanced_output2");
        std::ofstream outputfile3("advanced_output3");
        std::ofstream outputfile4("advanced_output4");
        std::ofstream outputfile5("advanced_output5");
        std::ofstream outputfile6("advanced_output6");
        std::ofstream outputfile6rescaled("advanced_output6rescaled");

        // std::cout << "Check that output1 and output2 are the same curve" << std::endl;
        // std::cout << "Check that output1der and output3 are close (finite difference vs. analytical derivative)" << std::endl;
        // std::cout << "Check that output1scdder and output4 are close (finite difference vs. analytical derivative)" << std::endl;

        cg1.dump(outputfile1);
        cg1der.dump(outputfile1der);
        cg1scdder.dump(outputfile1scdder);
        cg1_3period.dump(outputfile1_3period);
        cg2.dump(outputfile2);
        cg3.dump(outputfile3);
        cg4.dump(outputfile4);

        cg5.extendByPeriodicity(3);
        cg5.dump(outputfile5);

        cg6.dump(outputfile6);
        cg6.rescaleValLinear(0,10);
        cg6.rescaleTimeLinear(0,1);
        cg6.dump(outputfile6rescaled);

        const bool check_advanced_output1 = CheckResultFiles("advanced_output1");
        FELISCE_CHECK(check_advanced_output1);
        const bool check_advanced_output1der = CheckResultFiles("advanced_output1der");
        FELISCE_CHECK(check_advanced_output1der);
        const bool check_advanced_output1scdder = CheckResultFiles("advanced_output1scdder");
        FELISCE_CHECK(check_advanced_output1scdder);
        const bool check_advanced_output1_3period = CheckResultFiles("advanced_output1_3period");
        FELISCE_CHECK(check_advanced_output1_3period);
        const bool check_advanced_output2 = CheckResultFiles("advanced_output2");
        FELISCE_CHECK(check_advanced_output2);
        const bool check_advanced_output3 = CheckResultFiles("advanced_output3");
        FELISCE_CHECK(check_advanced_output3);
        const bool check_advanced_output4 = CheckResultFiles("advanced_output4");
        FELISCE_CHECK(check_advanced_output4);
        const bool check_advanced_output5 = CheckResultFiles("advanced_output5");
        FELISCE_CHECK(check_advanced_output5);
        const bool check_advanced_output6 = CheckResultFiles("advanced_output6");
        FELISCE_CHECK(check_advanced_output6);
        const bool check_advanced_output6rescaled = CheckResultFiles("advanced_output6rescaled");
        FELISCE_CHECK(check_advanced_output6rescaled);

        // std::cout << "Old cardiac output = " << cg1.integralOver60() << std::endl;
        FELISCE_CHECK_NEAR(cg1.integralOver60(), 3708.66, 1.0e-3);
        cg1.rescaleValToMatchIntegralOver60(5000);
        // std::cout << "New cardiac output " << cg1.integralOver60() << std::endl;
        FELISCE_CHECK_NEAR(cg1.integralOver60(), 5000.0, 1.0e-3);
    }
}

#endif

ADD_FELISCE_TEST
