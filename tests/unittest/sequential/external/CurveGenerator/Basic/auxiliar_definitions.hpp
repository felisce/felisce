//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <cstring>
#include <fstream>
#include <iterator>
#include <sstream>
#include <vector>

// External includes

// Project includes
#include "Core/filesystemUtil.hpp"
#include "Tools/test_utilities.hpp"

#ifndef FELISCE_AUXILIAR_DEFINITIONS_H_INCLUDED
#define FELISCE_AUXILIAR_DEFINITIONS_H_INCLUDED

std::vector<double> RetrieveValues(const std::string& rValuesString)
{
    std::vector<double> results;
    std::istringstream iss(rValuesString);
    for (std::string line; std::getline(iss, line); ) {
        std::istringstream iss(line);
        std::vector<std::string> values(std::istream_iterator<std::string>{iss},
                                        std::istream_iterator<std::string>());
        for (auto& r_value : values) {
            results.push_back(std::stod(r_value));
        }
    }

    return results;
}

bool CheckResultFiles(const std::string& rCaseName)
{
    const std::string folder_ref = "../RefSolutions";

    std::string name_file_ref = folder_ref + "/" + rCaseName;
    std::ifstream outputfile1_ref(name_file_ref);
    std::ostringstream ss_ref;
    ss_ref << outputfile1_ref.rdbuf(); // reading data
    const std::string str_ref = ss_ref.str();

    std::vector<double> results_ref = RetrieveValues(str_ref);

    const std::string current_path = std::filesystem::current_path();
    std::string name_file_solution = current_path + "/" + rCaseName;
    std::ifstream outputfile1_solution(name_file_solution);
    std::ostringstream ss_solution;
    ss_solution << outputfile1_solution.rdbuf(); // reading data
    const std::string str_solution = ss_solution.str();

    std::vector<double> results_solution = RetrieveValues(str_solution);

    remove(name_file_solution.c_str());

    if (results_ref.size() != results_solution.size()) {
        return false;
    }

    bool check = true;
    for (std::size_t i = 0; i < results_ref.size(); ++i) {
        if (std::abs(results_ref[i]) > 0.0) {
          if ((results_ref[i] - results_ref[i])/results_ref[i] > 1.0e-4) {
              check = false;
          }
        }
    }

    return check;
}

#endif
