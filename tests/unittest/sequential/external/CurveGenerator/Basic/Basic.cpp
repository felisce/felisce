//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

#if FELISCE_WITH_CURVEGEN

// Project includes
#include "auxiliar_definitions.hpp"
#include "curveGen.hpp"
#include "dataCurve.hpp"

TEST_SUITE("TestCurveGeneratorBasic")
{
    TEST_CASE("Output1: the original data ")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        const std::string input = "../Resources/q_asc_aorta_rest_miccai.dat";
        DataCurve dc(input.c_str()); // Type 2: Fourier (cos, sin)

        CurveGen cg1(dc,NUM_SUBDIV);
        const std::string case_name = "output1";
        std::ofstream outputfile1(case_name);
        cg1.dump(outputfile1); // Save on a file

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output2: the derivative of the original data")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)
        CurveGen cg1(dc,NUM_SUBDIV);

        // --- Compute the derivative

        CurveGen cg2 = cg1.derivative(true);// true: periodicity is assumed to compute the derivative at the extremeties of the interval
        const std::string case_name = "output2";
        std::ofstream outputfile2(case_name);
        cg2.dump(outputfile2);

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output3: the original data is extended over 3 periods")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        // --- Extend by periodicity, given a number of periods

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)

        CurveGen cg3(dc,NUM_SUBDIV);
        cg3.extendByPeriodicity(3);
        const std::string case_name = "output3";
        std::ofstream outputfile3(case_name);
        cg3.dump(outputfile3);

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output4: the original data is extended by periodicity until (at least) time = 9.")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        // --- Extend by periodicity, given a final time (the number of periods will be integer)

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)

        CurveGen cg4(dc,NUM_SUBDIV);
        cg4.extendByPeriodicityUntilTime(9.);
        const std::string case_name = "output4";
        std::ofstream outputfile4(case_name);
        cg4.dump(outputfile4);

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output5: rescale the original data such that min = 0 and max = 500")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        //
        // --- Rescale the values given a min and a max
        //

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)

        CurveGen cg5(dc,NUM_SUBDIV);
        cg5.rescaleValLinear(0,500); // new minimum and maximum values
        const std::string case_name = "output5";
        std::ofstream outputfile5(case_name);
        cg5.dump(outputfile5);

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output6: rescale the original time interval such that min time = 0 and max time = 0.8")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        //
        // --- Rescale the time given a min and a max
        //

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)

        CurveGen cg6(dc,NUM_SUBDIV);
        cg6.rescaleTimeLinear(0,0.8); // new min time and max time
        const std::string case_name = "output6";
        std::ofstream outputfile6(case_name);
        cg6.dump(outputfile6);

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output7: rescale the original data to have an integral = 88")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        //
        // --- Rescale the values given the value of the integral
        //

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)

        CurveGen cg7(dc,NUM_SUBDIV);
        const std::string case_name = "output7";
        std::ofstream outputfile7(case_name);
        cg7.dump(outputfile7);
        cg7.rescaleValToMatchIntegral(88); // new integral value

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }

    TEST_CASE("Output8: rescale the original data to have a cardiac output of 5000")
    {
        const int NUM_SUBDIV = 1000; // number of subdivisions to sample the curve

        //
        // --- Rescale the values given the value of the integral of the periodic signal from 0 to 60
        //     (this function is designed to match a give cardiac output)
        //

        DataCurve dc("../Resources/q_asc_aorta_rest_miccai.dat"); // type 2: Fourier (cos, sin)

        CurveGen cg8(dc,NUM_SUBDIV);
        cg8.rescaleValToMatchIntegralOver60(5000); // new cardiac output
        const std::string case_name = "output8";
        std::ofstream outputfile8(case_name);
        cg8.dump(outputfile8);

        const bool check = CheckResultFiles(case_name);
        FELISCE_CHECK(check);
    }
}

#endif

ADD_FELISCE_TEST
