# Import unittest
import unittest

# Import os
import os

# Import platform
import platform

class TestHelloWorld(unittest.TestCase):

    def __init__(self, exec_name: str, binary_folder: str, install_prefix: str, custom_input: str):

        self.binary_folder  = os.path.abspath(binary_folder)
        self.executable     = os.path.join(self.binary_folder, exec_name)
        self.install_prefix = install_prefix
        self.custom_input   = custom_input

    def get_command(self) -> str:
        """Constructs the command to execute based on system type."""
        machine_name = platform.node()
        command = self.executable

        if "CLEPS" in machine_name or "node" in machine_name:
            command = f"srun {command}"

        return command

    def setup_library_path(self) -> str:
        """Sets up the appropriate library path based on the operating system."""
        libpath = os.path.abspath(os.path.join(self.install_prefix, "lib"))
        
        if platform.system() == "Darwin":
            return f"export DYLD_LIBRARY_PATH={libpath}; "
        else:
            return f"export LD_LIBRARY_PATH={libpath}; "

    def run(self):
        """Executes the test command."""
        try:
            command = self.setup_library_path() + self.get_command()
            result = os.system(f"{command} {self.custom_input}")
            if result != 0:
                sys.exit("Test failed!")
        except Exception as e:
            sys.exit(f"Test failed! Error: {e}")

if __name__ == '__main__':

    unittest = TestHelloWorld(exec_name="hello_world",
                              binary_folder="@binary_folder@",
                              install_prefix="@install_prefix@",
                              custom_input=""
                              )
    
    unittest.run()   
