# Install test UnitTest_Seq_Src_Executable_Contour

felisce_add_python_test(
    NAME UnitTest_Seq_Src_Executable_Contour
    EXEC test_generate_contour.py
    INSTALL unittest/sequential/src/Executable/Contour
)