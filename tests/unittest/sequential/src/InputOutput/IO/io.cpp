//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/test_utilities.hpp"

const std::string meshes_folder = "Resources/";
const std::string folder_res = "Resources/";

TEST_SUITE("TestIO")
{
    // TEST_CASE("ConstructorsAndInitialize")
    // {
    //   const std::string file_name = "minimal";
    //   auto p_io_default = felisce::make_shared<felisce::IO>();
    //   p_io_default->initialize(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region_default = new felisce::GeometricMeshRegion();
    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io_default->readMesh(*p_mesh_region_default, 1.0);
    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), p_mesh_region_default->numCoor());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), p_mesh_region_default->domainDim());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), p_mesh_region_default->numPoints());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), p_mesh_region_default->moved());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), p_mesh_region_default->createNormalTangent());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), p_mesh_region_default->statusEdges());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), p_mesh_region_default->statusFaces());
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), p_mesh_region_default->flagFormatMesh());

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), p_mesh_region_default->numElements(eltType));
    //   }

    //   delete p_mesh_region_default;
    // }

    // TEST_CASE("ReadMeditMinimal")
    // {
    //   const std::string file_name = "minimal";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeOutput(*p_mesh_region, 0);

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
    //   const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadMeditMinimalQuad")
    // {
    //   const std::string file_name = "minimal_quad";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Quad4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeOutput(*p_mesh_region, 0);

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
    //   const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadMeditCUBE86")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "cube86";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 39);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 72);
    //     } else if (eltType == felisce::GeometricMeshRegion::Tetra4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 86);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeOutput(*p_mesh_region, 0);

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
    //   const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadMeditCYL248")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "cyl248";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 92);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 154);
    //     } else if (eltType == felisce::GeometricMeshRegion::Tetra4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 248);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeOutput(*p_mesh_region, 0);

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
    //   const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadMeditELL")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "ell";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 65);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 96);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeOutput(*p_mesh_region, 0);

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
    //   const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadMeditHEXAHEXA_2x2x2")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "hexahexa_2x2x2";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 27);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Quad4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 24);
    //     } else if (eltType == felisce::GeometricMeshRegion::Hexa8) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 8);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeOutput(*p_mesh_region, 0);

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
    //   const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadMeditScoordelisPrism9")
    // {
    //   const std::string file_name = "scoordelis_prism9";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 48);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Prism9) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 18);
    //     } else if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 36);
    //     } else if (eltType == felisce::GeometricMeshRegion::Quad6) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 12);
    //     } else if (eltType == felisce::GeometricMeshRegion::Seg2) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 12);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }
    // }

    // TEST_CASE("ReadMeditComplexPrism9")
    // {
    //   const std::string file_name = "rectanglePrism9";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 327);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Prism9) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 184);
    //     } else if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 368);
    //     } else if (eltType == felisce::GeometricMeshRegion::Quad6) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 32);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }
    // }

    // TEST_CASE("ReadEnsigthMinimal")
    // {
    //   const std::string file_name = "minimal";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3); // NOTE: This should be 2
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatEnsight6);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("PostProcessEnsigthMinimal")
    // {
    //   const std::string file_name = "minimal";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, folder_res, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   // Testing writing file
    //   p_io->initializeOutput();
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + file_name + ".out.geo");
    //   double* pressure_array = new double[4];
    //   pressure_array[0] = 0.0;
    //   pressure_array[1] = 1.0;
    //   pressure_array[2] = 2.0;
    //   pressure_array[3] = 3.0;
    //   p_io->addVariable(felisce::EnsightVariable::ScalarPerNode, "pressure", pressure_array, 4, true);
    //   double* velocity_array = new double[12];
    //   velocity_array[0]  = 0.0;
    //   velocity_array[1]  = 1.0;
    //   velocity_array[2]  = 2.0;
    //   velocity_array[3]  = 3.0;
    //   velocity_array[4]  = 0.0;
    //   velocity_array[5]  = 1.0;
    //   velocity_array[6]  = 2.0;
    //   velocity_array[7]  = 3.0;
    //   velocity_array[8]  = 0.0;
    //   velocity_array[9]  = 1.0;
    //   velocity_array[10] = 2.0;
    //   velocity_array[11] = 3.0;
    //   p_io->addVariable(felisce::EnsightVariable::VectorPerNode, "velocity", velocity_array, 4, true);
    //   double time = 1.0;
    //   p_io->postProcess(time, 0);

    //   // Checking files
    //   const std::string file_1 = meshes_folder + file_name + ".geo";
    //   const std::string file_2 = meshes_folder + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(file_1, file_2));

    //   const std::string file_3 = folder_res + "test_ref.case";
    //   const std::string file_4 = folder_res + "test_.case";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(file_3, file_4));

    //   const std::string file_5 = folder_res + "pressure_ref.00000.scl";
    //   const std::string file_6 = folder_res + "pressure.00000.scl";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(file_5, file_6));

    //   const std::string file_7 = folder_res + "velocity_ref.00000.vct";
    //   const std::string file_8 = folder_res + "velocity.00000.vct";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(file_7, file_8));
    // }

    // TEST_CASE("PostProcessEnsigthPrism")
    // {
    //   const std::string file_name = "minimal_prism";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 6);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Prism6) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("PostProcessEnsigthPrism9")
    // {
    //   const std::string file_name = "minimal_prism9";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 9);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Prism9) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadEnsigthMinimalQuad")
    // {
    //   const std::string file_name = "minimal_quad";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3); // NOTE: This should be 2
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatEnsight6);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Quad4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    TEST_CASE("ReadEnsigthCUBE86")
    {
      // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
      const std::string file_name = "cube86";
      auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

      auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

      p_io->readMesh(*p_mesh_region, 1.0);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 39);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatEnsight6);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 72);
        } else if (eltType == felisce::GeometricMeshRegion::Tetra4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 86);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".geo";
      const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    // TEST_CASE("ReadEnsigthCYL248")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "cyl248";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 92);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatEnsight6);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 154);
    //     } else if (eltType == felisce::GeometricMeshRegion::Tetra4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 248);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadEnsigthELL")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "ell";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3); // NOTE: This should be 2
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 65);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatEnsight6);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Tria3) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 96);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }

    // TEST_CASE("ReadEnsigthHEXAHEXA_2x2x2")
    // {
    //   // NOTE: Meshes downloaded from here https://people.sc.fsu.edu/~jburkardt/data/medit/medit.html
    //   const std::string file_name = "hexahexa_2x2x2";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".geo" , file_name + ".geo", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 27);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
    //   FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatEnsight6);

    //   felisce::GeometricMeshRegion::ElementType eltType;
    //   for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
    //     eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
    //     if (eltType == felisce::GeometricMeshRegion::Quad4) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 24);
    //     } else if (eltType == felisce::GeometricMeshRegion::Hexa8) {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 8);
    //     } else {
    //       FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
    //     }
    //   }

    //   // Testing writing file
    //   p_io->writeMesh(*p_mesh_region, meshes_folder + "0" + file_name + ".out.geo");

    //   // Checking files
    //   const std::string& r_file_1 = meshes_folder + file_name + ".geo";
    //   const std::string& r_file_2 = meshes_folder + "0" + file_name + ".out.geo";
    //   FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    // }
}

ADD_FELISCE_TEST
