//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/chrono.hpp"

TEST_SUITE("TestChrono")
{
    TEST_CASE("SeveralChecks")
    {
        felisce::ChronoInstance chrono;
        FELISCE_CHECK_IS_FALSE(chrono.isStarted());
        FELISCE_CHECK_IS_FALSE(chrono.isStopped());
        chrono.start();
        FELISCE_CHECK(chrono.isStarted());
        felisce::Chrono::Sleep(1);
        chrono.stop();
        FELISCE_CHECK(chrono.isStopped());
    }
}

ADD_FELISCE_TEST