# Install test UnitTest_Seq_Src_Core_Indexed_Obj

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_Core_Indexed_Obj
    SOURCES indexed_object.cpp
    INSTALL unittest/sequential/src/Core/Indexed_Obj
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_Core_Indexed_Obj
    EXEC UnitTest_Seq_Src_Core_Indexed_Obj
    INSTALL unittest/sequential/src/Core/Indexed_Obj
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_Core_Indexed_Obj
    INSTALL unittest/sequential/src/Core/Indexed_Obj
)
