//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/indexed_object.hpp"

TEST_SUITE("TestIndexedObject")
{
    TEST_CASE("Constructors")
    {
        auto default_id = felisce::IndexedObject();
        auto one_id = felisce::IndexedObject(1);
        FELISCE_CHECK_EQUAL(default_id.Id(), 0);
        FELISCE_CHECK_EQUAL(one_id.Id(), 1);

        felisce::IndexedObject new_id(default_id);
        FELISCE_CHECK_EQUAL(default_id.Id(), new_id.Id());
    }

    TEST_CASE("operators")
    {
        felisce::IndexedObject id;
        auto id_3 = felisce::IndexedObject(3);
        id = id_3;
        FELISCE_CHECK_EQUAL(id.Id(), 3);

        FELISCE_CHECK_EQUAL(id.Id(), id(id));
    }

    TEST_CASE("Setters-Getters")
    {
        auto id = felisce::IndexedObject();
        FELISCE_CHECK_EQUAL(id.GetId(), 0);
        FELISCE_CHECK_EQUAL(id.GetId(), id.Id());
        id.SetId(1);
        FELISCE_CHECK_EQUAL(id.Id(), 1);
    }

    TEST_CASE("Info")
    {
        auto id = felisce::IndexedObject();
        FELISCE_CHECK_STRING_EQUAL(id.Info(), "indexed object # 0");
    }
}

ADD_FELISCE_TEST
