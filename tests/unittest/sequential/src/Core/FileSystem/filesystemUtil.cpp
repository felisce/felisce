//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/filesystemUtil.hpp"

TEST_SUITE("TestfilesystemUtil")
{
    TEST_CASE("directoryExists")
    {
        const auto current_path = std::filesystem::current_path();
        std::string path_string{current_path.u8string()};
        FELISCE_CHECK_EQUAL(felisce::filesystemUtil::directoryExists(path_string.c_str()), true);
        path_string += ".copy";
        FELISCE_CHECK_EQUAL(felisce::filesystemUtil::directoryExists(path_string.c_str()), false);
    }

    TEST_CASE("fileExists")
    {
        const std::filesystem::path current_path = std::filesystem::current_path();
        const std::filesystem::path current_exex = "run_unittest.py";
        const std::filesystem::path exec_full_path = current_path / current_exex;
        std::string path_string(exec_full_path);
        FELISCE_CHECK_EQUAL(felisce::filesystemUtil::fileExists(path_string), true);
        path_string += ".copy";
        FELISCE_CHECK_EQUAL(felisce::filesystemUtil::fileExists(path_string), false);
    }

    TEST_CASE("copyFileSomewhereElse")
    {
        const std::filesystem::path current_path = std::filesystem::current_path();
        const std::filesystem::path current_exex = "run_unittest.py";
        const std::filesystem::path exec_full_path = current_path / current_exex;
        std::string old_path_string(exec_full_path);
        std::string new_path_string(exec_full_path);
        new_path_string += ".copy";
        felisce::filesystemUtil::copyFileSomewhereElse(old_path_string, new_path_string);
        FELISCE_CHECK_EQUAL(felisce::filesystemUtil::fileExists(new_path_string), true);
        std::filesystem::remove(new_path_string);
    }
}

ADD_FELISCE_TEST
