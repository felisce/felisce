//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/filesystemUtil.hpp"
#include "Core/felisceParam.hpp"

TEST_SUITE("felisceParam")
{
  TEST_CASE("multi_instance")
  {
    // Read command line and data file
    std::string current_path = std::filesystem::current_path();
    current_path += "/";
    const int argc = 5;
    const std::string test_bin_name = current_path + "Test_Src_Core_Parameters";
    const std::string data_file_1 = current_path + "data_1";
    const std::string data_file_2 = current_path + "data_2";
    const std::string data_file_3 = current_path + "data_3";
    const char* argv[] = {test_bin_name.c_str(), "-f", data_file_1.c_str(), data_file_2.c_str(), data_file_3.c_str()};

    const std::size_t instance_0 = 0;
    felisce::FelisceParam::instance(instance_0).initialize(argc, argv);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_0).timeStep, 0.2);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_0).timeMax, 0.2);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_0).time, 0.0);

    const std::size_t instance_1 = 1;
    felisce::FelisceParam::instance(instance_1).initialize(argc, argv);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_1).timeStep, 0.5);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_1).timeMax, 0.5);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_1).time, 0.0);

    const std::size_t instance_2 = 2;
    felisce::FelisceParam::instance(instance_2).initialize(argc, argv);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_2).timeStep, 1.5);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_2).timeMax, 1.5);
    FELISCE_CHECK_EQUAL(felisce::FelisceParam::instance(instance_2).time, 0.0);
  }
}

ADD_FELISCE_TEST
