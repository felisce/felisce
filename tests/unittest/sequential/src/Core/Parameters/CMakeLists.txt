# Install test UnitTest_Seq_Src_Core_Parameters

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_Core_Parameters
    SOURCES felisceParam.cpp
    INSTALL unittest/sequential/src/Core/Parameters
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_Core_Parameters
    EXEC UnitTest_Seq_Src_Core_Parameters
    INSTALL unittest/sequential/src/Core/Parameters
    CONFIG 
    FILES data_1 data_2 data_3
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_Core_Parameters
    INSTALL unittest/sequential/src/Core/Parameters
)
