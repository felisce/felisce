//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/array_1d.hpp"

TEST_SUITE("Testarray_1d")
{
    TEST_CASE("constructors")
    {
        felisce::array_1d<double, 3> test(0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test[0], 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test[1], 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test[2], 0.0);
    }

    TEST_CASE("operators")
    {
        // () operator
        felisce::array_1d<double, 3> test(0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test(0), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test(1), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test(2), 0.0);

        // [] operator
        felisce::array_1d<double, 3> copy_test(1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test[0], 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test[1], 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test[2], 1.0);

        // = operator
        copy_test = test;
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(0), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(1), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(2), 0.0);

        // += operator
        felisce::array_1d<double, 3> one_test(1.0);
        copy_test += one_test;
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(0), 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(1), 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(2), 1.0);

        // /= operator
        copy_test /= 2.0;
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(0), 0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(1), 0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(2), 0.5);

        // -= operator
        copy_test -= one_test;
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(0), -0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(1), -0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(2), -0.5);

        // *= operator
        copy_test *= 2.0;
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(0), -1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(1), -1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test(2), -1.0);
    }

    TEST_CASE("swap")
    {
        // swap
        felisce::array_1d<double, 3> test(1.0);
        felisce::array_1d<double, 3> copy_test(0.0);

        test.swap(copy_test);

        FELISCE_CHECK_DOUBLE_EQUAL(copy_test[0], 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test[1], 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_test[2], 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test(0), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test(1), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(test(2), 0.0);

    }

    TEST_CASE("access")
    {
        // Size
        felisce::array_1d<double, 3> test(0.0);
        FELISCE_CHECK_EQUAL(test.size(), 3);
    }

    TEST_CASE("operationsUblas")
    {
        // Norm
        felisce::array_1d<double, 3> test(1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(norm_1(test), 3);
        FELISCE_CHECK_DOUBLE_EQUAL(norm_2(test), std::sqrt(3));
        FELISCE_CHECK_DOUBLE_EQUAL(inner_prod(test, test), 3);
    }
}

ADD_FELISCE_TEST
