# Install test UnitTest_Seq_Src_HyperelasticityLaws_HyperElasticLaw

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_HyperElasticLaw
    SOURCES HyperElasticLaw.cpp
    INSTALL unittest/sequential/src/HyperelasticityLaws/HyperElasticLaw
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_HyperElasticLaw
    EXEC UnitTest_Seq_Src_HyperelasticityLaws_HyperElasticLaw
    INSTALL unittest/sequential/src/HyperelasticityLaws/HyperElasticLaw
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_HyperElasticLaw
    INSTALL unittest/sequential/src/HyperelasticityLaws/HyperElasticLaw
)
