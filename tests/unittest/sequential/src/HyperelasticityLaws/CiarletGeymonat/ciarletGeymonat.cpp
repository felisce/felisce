//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/felisceParam.hpp"
#include "HyperelasticityLaws/ciarletGeymonat.hpp"

inline void GenerateCiarletGeymonatLaw(felisce::HyperElasticLaw::Pointer& pLaw)
{
  felisce::FelisceParam::instance().hyperelastic_bulk = 1.0e12;
  auto& r_kappas = felisce::FelisceParam::instance().CiarletGeymonat;
  r_kappas.kappa1 = 1.0;
  r_kappas.kappa2 = 2.0;
  pLaw = felisce::make_shared<felisce::CiarletGeymonat>();
  pLaw->InitParameters();
  pLaw->SetInvariants(1.0,2.0,3.0);
}

TEST_SUITE("ciarletGeymonat")
{
    TEST_CASE("W")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->W(), -6.3836392986, 1.0e-4);
    }

    TEST_CASE("FirstDerivativeWFirstInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->FirstDerivativeWFirstInvariant(), 0.0, 1.0e-4);
    }

    TEST_CASE("FirstDerivativeWSecondInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->FirstDerivativeWSecondInvariant(), 0.9614997135, 1.0e-4);
    }

    TEST_CASE("SecondDerivativeWFirstInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWFirstInvariant(), 0.0);
    }

    TEST_CASE("SecondDerivativeWSecondInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWSecondInvariant(), 0.0);
    }

    TEST_CASE("SecondDerivativeWFirstAndSecondInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWFirstAndSecondInvariant(), 0.0);
    }

    TEST_CASE("FirstDerivativeWThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->FirstDerivativeWThirdInvariant(), -0.5043733476, 1.0e-4);
    }

    TEST_CASE("SecondDerivativeWThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->SecondDerivativeWThirdInvariant(), 0.2716473996, 1.0e-4);
    }

    TEST_CASE("SecondDerivativeWFirstAndThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->SecondDerivativeWFirstAndThirdInvariant(), -0.0770401416, 1.0e-4);
    }

    TEST_CASE("SecondDerivativeWSecondAndThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->SecondDerivativeWSecondAndThirdInvariant(), -0.213666603, 1.0e-4);
    }

    TEST_CASE("Wc")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->Wc(5.0), 0.9137233162, 1.0e-4);
    }

    TEST_CASE("FirstDerivativeWcThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->FirstDerivativeWcThirdInvariant(5.0), 0.6100423396, 1.0e-4);
    }

    TEST_CASE("SecondDerivativeWcThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateCiarletGeymonatLaw(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->SecondDerivativeWcThirdInvariant(5.0), 0.0372151656, 1.0e-4);
    }
}

ADD_FELISCE_TEST