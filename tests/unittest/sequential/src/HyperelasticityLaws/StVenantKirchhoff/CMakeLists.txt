# Install test UnitTest_Seq_Src_HyperelasticityLaws_StVenantKirchhoff

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_StVenantKirchhoff
    SOURCES stVenantKirchhoff.cpp
    INSTALL unittest/sequential/src/HyperelasticityLaws/StVenantKirchhoff
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_StVenantKirchhoff
    EXEC UnitTest_Seq_Src_HyperelasticityLaws_StVenantKirchhoff
    INSTALL unittest/sequential/src/HyperelasticityLaws/StVenantKirchhoff
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_StVenantKirchhoff
    INSTALL unittest/sequential/src/HyperelasticityLaws/StVenantKirchhoff
)
