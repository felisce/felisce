//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/felisceParam.hpp"
#include "HyperelasticityLaws/Ogden.hpp"

inline void GenerateOgden(felisce::HyperElasticLaw::Pointer& pLaw)
{
  felisce::FelisceParam::instance().hyperelastic_bulk = 1.0e12;
  auto& r_ogden = felisce::FelisceParam::instance().Ogden;
  r_ogden.C1 = 1.0;
  r_ogden.C2 = 2.0;
  r_ogden.a  = 3.0;
  pLaw = felisce::make_shared<felisce::Ogden>();
  pLaw->InitParameters();
  pLaw->SetInvariants(1.0,2.0,3.0);
}

TEST_SUITE("Ogden")
{
    TEST_CASE("W")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_RELATIVE_NEAR(p_law->W(), -6.7889, 1.0e-4);
    }

    TEST_CASE("FirstDerivativeWFirstInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->FirstDerivativeWFirstInvariant(), 1.0);
    }

    TEST_CASE("FirstDerivativeWSecondInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->FirstDerivativeWSecondInvariant(), 2.0);
    }

    TEST_CASE("SecondDerivativeWFirstInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWFirstInvariant(), 0.0);
    }

    TEST_CASE("SecondDerivativeWSecondInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWSecondInvariant(), 0.0);
    }

    TEST_CASE("SecondDerivativeWFirstAndSecondInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWFirstAndSecondInvariant(), 0.0);
    }

    TEST_CASE("FirstDerivativeWThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->FirstDerivativeWThirdInvariant(), 1.0/3.0);
    }

    TEST_CASE("SecondDerivativeWThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWThirdInvariant(), 8.0/9.0);
    }

    TEST_CASE("SecondDerivativeWFirstAndThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWFirstAndThirdInvariant(), 0.0);
    }

    TEST_CASE("SecondDerivativeWSecondAndThirdInvariant")
    {
      felisce::HyperElasticLaw::Pointer p_law = nullptr;
      GenerateOgden(p_law);
      FELISCE_CHECK_DOUBLE_EQUAL(p_law->SecondDerivativeWSecondAndThirdInvariant(), 0.0);
    }
}

ADD_FELISCE_TEST
