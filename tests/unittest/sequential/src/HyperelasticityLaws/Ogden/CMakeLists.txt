# Install test UnitTest_Seq_Src_HyperelasticityLaws_Ogden

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_Ogden
    SOURCES Ogden.cpp
    INSTALL unittest/sequential/src/HyperelasticityLaws/Ogden
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_Ogden
    EXEC UnitTest_Seq_Src_HyperelasticityLaws_Ogden
    INSTALL unittest/sequential/src/HyperelasticityLaws/Ogden
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_HyperelasticityLaws_Ogden
    INSTALL unittest/sequential/src/HyperelasticityLaws/Ogden
)
