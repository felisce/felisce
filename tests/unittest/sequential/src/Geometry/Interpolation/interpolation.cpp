//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Daniele Carlo Corti
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/Tools/interpolator.hpp"
#include "Tools/test_utilities.hpp"

TEST_SUITE("TestInterpolation")
{
    TEST_CASE("2D")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      int nbrPnt = 3; 
      std::vector<double>          intVal;
      std::vector<felisce::felInt> indElt;
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);

      // List of points
      lstPnt[0] = felisce::Point(0.0, 0.0, 0.0);  lstRes[0] =  0; 
      lstPnt[1] = felisce::Point(0.5, 0.0, 0.0);  lstRes[1] =  0; 
      lstPnt[2] = felisce::Point(0.3, 0.3, 0.0);  lstRes[2] =  0; 

      felisce::FelisceParam::alterVerbosity(10);


      //--- Point interpolation
      felisce::Interpolator interpolator(p_mesh_region.get(), false);
      interpolator.interpolate(indElt, intVal, lstPnt);

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[0], 1.0 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[1], 0.0 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[2], 0.0 );

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[3], 0.5 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[4], 0.5 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[5], 0.0 );

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[6], 0.4 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[7], 0.3 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[8], 0.3 );
    }

    TEST_CASE("3D")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      int nbrPnt = 4; 
      std::vector<double>          intVal;
      std::vector<felisce::felInt> indElt;
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);

      // List of points
      lstPnt[0] = felisce::Point(0.0, 0.0, 0.0);  lstRes[0] =  0; 
      lstPnt[1] = felisce::Point(0.5, 0.0, 0.0);  lstRes[1] =  0; 
      lstPnt[2] = felisce::Point(0.3, 0.3, 0.0);  lstRes[2] =  0; 
      lstPnt[3] = felisce::Point(0.3, 0.3, 0.3);  lstRes[3] =  0; 

      felisce::FelisceParam::alterVerbosity(10);


      //--- Point interpolation
      felisce::Interpolator interpolator(p_mesh_region.get(), false);
      interpolator.interpolate(indElt, intVal, lstPnt);

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
      FELISCE_CHECK_EQUAL(indElt[3], lstRes[3]);

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 0], 1.0 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 1], 0.0 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 2], 0.0 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 3], 0.0 );

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 4], 0.5 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 5], 0.5 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 6], 0.0 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 7], 0.0 );

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 8], 0.4 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[ 9], 0.3 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[10], 0.3 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[11], 0.0 );

      FELISCE_CHECK_DOUBLE_EQUAL( intVal[12], 0.1 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[13], 0.3 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[14], 0.3 );
      FELISCE_CHECK_DOUBLE_EQUAL( intVal[15], 0.3 );      
    }
}

ADD_FELISCE_TEST
