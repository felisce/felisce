# Include nested test directories

add_subdirectory(Interpolation)
add_subdirectory(Intersection)
add_subdirectory(Localization)
add_subdirectory(Mesh)
add_subdirectory(Point)
