# Install test UnitTest_Seq_Src_Geometry_Intersection

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_Geometry_Intersection
    SOURCES intersection.cpp
    INSTALL unittest/sequential/src/Geometry/Intersection
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_Geometry_Intersection
    EXEC UnitTest_Seq_Src_Geometry_Intersection
    INSTALL unittest/sequential/src/Geometry/Intersection
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_Geometry_Intersection
    INSTALL unittest/sequential/src/Geometry/Intersection
)
