//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Daniele Carlo Corti
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/Tools/intersector.hpp"
#include "Geometry/Tools/locator.hpp"
#include "Tools/test_utilities.hpp"

TEST_SUITE("TestIntersection")
{
    TEST_CASE("2D")
    {
      auto p_mesh_region_vol = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      auto p_mesh_region_str = felisce::make_shared<felisce::GeometricMeshRegion>();

      std::map<int,std::vector<felisce::felInt>> list_elements =
      {
          std::pair<int,std::vector<felisce::felInt>>(1, {0,1,2})
      };

      p_mesh_region_str->numCoor() = 2;
      p_mesh_region_str->domainDim() = felisce::GeometricMeshRegion::GeoMesh1D;
      p_mesh_region_str->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
      p_mesh_region_str->numElements(felisce::GeometricMeshRegion::Seg2) = list_elements[1].size();

      auto& r_points = p_mesh_region_str->listPoints();
      r_points.emplace_back(0.0, 0.0, 0.0);
      r_points.emplace_back(0.5, 0.0, 0.0);
      r_points.emplace_back(0.5, 1.0, 0.0);
      r_points.emplace_back(1.0, 1.0, 0.0);

      p_mesh_region_str->allocateElements(felisce::GeometricMeshRegion::Seg2);
      std::vector<felisce::felInt> elem0({1,2});
      p_mesh_region_str->setOneElement(felisce::GeometricMeshRegion::Seg2, 0, elem0, true);
      std::vector<felisce::felInt> elem1({2,3});
      p_mesh_region_str->setOneElement(felisce::GeometricMeshRegion::Seg2, 1, elem1, true);
      std::vector<felisce::felInt> elem2({3,4});
      p_mesh_region_str->setOneElement(felisce::GeometricMeshRegion::Seg2, 2, elem2, true);

      p_mesh_region_str->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Seg2);
      p_mesh_region_str->setBagElementTypeDomain();
      p_mesh_region_str->setBagElementTypeDomainBoundary();

      int nbrPnt = p_mesh_region_str->numPoints(); 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::felInt> lstResLoc(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // Reference solution localization
      lstResLoc[0] =  0; 
      lstResLoc[1] =  0; 
      lstResLoc[2] =  1; 
      lstResLoc[3] =  1; 

      felisce::FelisceParam::alterVerbosity(10);


      //--- Point localization
      felisce::Locator locator(p_mesh_region_vol.get());
      locator.findListPointsInMesh(indElt, typElt, p_mesh_region_str->listPoints());

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstResLoc[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstResLoc[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstResLoc[2]);
      FELISCE_CHECK_EQUAL(indElt[3], lstResLoc[3]);


      //--- Intersection
      std::vector<felisce::felInt> intElt;
      felisce::Intersector intersector(p_mesh_region_vol.get(), p_mesh_region_str.get());
      intersector.intersectMeshes(indElt, intElt);

      // Reference solution intersection
      std::vector<felisce::felInt> lstResInt{ 0, 1}; 

      std::sort(lstResInt.begin(), lstResInt.end());
      std::sort(intElt.begin(), intElt.end());

      // Checks
      FELISCE_CHECK_EQUAL( (lstResInt == intElt) , true);
    }

    TEST_CASE("3D")
    {
      auto p_mesh_region_vol = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      auto p_mesh_region_str = felisce::make_shared<felisce::GeometricMeshRegion>();

      std::map<int,std::vector<felisce::felInt>> list_elements =
      {
          std::pair<int,std::vector<felisce::felInt>>(1, {0,1})
      };

      p_mesh_region_str->numCoor() = 2;
      p_mesh_region_str->domainDim() = felisce::GeometricMeshRegion::GeoMesh2D;
      p_mesh_region_str->flagFormatMesh() = felisce::GeometricMeshRegion::FormatMedit;
      p_mesh_region_str->numElements(felisce::GeometricMeshRegion::Tria3) = list_elements[1].size();

      auto& r_points = p_mesh_region_str->listPoints();
      r_points.emplace_back(0.0, 0.0, 0.0);
      r_points.emplace_back(1.0, 0.0, 1.0);
      r_points.emplace_back(0.0, 1.0, 1.0);
      r_points.emplace_back(1.0, 1.0, 0.0);

      p_mesh_region_str->allocateElements(felisce::GeometricMeshRegion::Tria3);
      std::vector<felisce::felInt> elem0({1,2,3});
      p_mesh_region_str->setOneElement(felisce::GeometricMeshRegion::Tria3, 0, elem0, true);
      std::vector<felisce::felInt> elem1({2,4,3});
      p_mesh_region_str->setOneElement(felisce::GeometricMeshRegion::Tria3, 1, elem1, true);

      p_mesh_region_str->reorderListElePerRef(list_elements, felisce::GeometricMeshRegion::Tria3);
      p_mesh_region_str->setBagElementTypeDomain();
      p_mesh_region_str->setBagElementTypeDomainBoundary();

      int nbrPnt = p_mesh_region_str->numPoints(); 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::felInt> lstResLoc(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // Reference solution localization
      lstResLoc[0] =  0; 
      lstResLoc[1] =  3; 
      lstResLoc[2] =  4; 
      lstResLoc[3] =  1; 

      felisce::FelisceParam::alterVerbosity(10);


      //--- Point localization
      felisce::Locator locator(p_mesh_region_vol.get());
      locator.findListPointsInMesh(indElt, typElt, p_mesh_region_str->listPoints());

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstResLoc[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstResLoc[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstResLoc[2]);
      FELISCE_CHECK_EQUAL(indElt[3], lstResLoc[3]);


      //--- Intersection
      std::vector<felisce::felInt> intElt;
      felisce::Intersector intersector(p_mesh_region_vol.get(), p_mesh_region_str.get(), 1.);
      intersector.intersectMeshes(indElt, intElt);

      // Reference solution intersection
      std::vector<felisce::felInt> lstResInt{ 0, 1, 2, 3, 4}; 

      std::sort(lstResInt.begin(), lstResInt.end());
      std::sort(intElt.begin(), intElt.end());

      // Checks
      FELISCE_CHECK_EQUAL( (lstResInt == intElt) , true);
    }
}

ADD_FELISCE_TEST
