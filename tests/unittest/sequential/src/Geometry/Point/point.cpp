//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Geometry/point.hpp"

TEST_SUITE("TestPoint")
{
    TEST_CASE("Constructors")
    {
        felisce::Point zero_point(0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(zero_point.x(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(zero_point.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(zero_point.z(), 0.0);

        felisce::Point non_zero_point(1.0,0.0,0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(non_zero_point.x(), 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(non_zero_point.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(non_zero_point.z(), 0.0);

        double array[3];
        array[0] = 10.0;
        array[1] = 0.0;
        array[2] = 20.0;
        felisce::Point point_array(array);
        FELISCE_CHECK_DOUBLE_EQUAL(point_array.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_array.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_array.z(), 20.0);

        std::array<double, 3> std_array{10.0, 0.0, 20.0};
        felisce::Point point_std_array(std_array);
        FELISCE_CHECK_DOUBLE_EQUAL(point_std_array.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_std_array.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_std_array.z(), 20.0);

        std::vector<double> std_vector({10.0, 0.0, 20.0});
        felisce::Point point_std_vector(std_vector);
        FELISCE_CHECK_DOUBLE_EQUAL(point_std_vector.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_std_vector.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_std_vector.z(), 20.0);

        felisce::Point copy_point_std_vector(point_std_vector);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_point_std_vector.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_point_std_vector.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_point_std_vector.z(), 20.0);

    }

    TEST_CASE("operator=")
    {
        std::vector<double> std_vector({10.0, 0.0, 20.0});
        felisce::Point point_std_vector(std_vector);

        felisce::Point copy_point_std_vector(0.0);
        copy_point_std_vector = point_std_vector;
        FELISCE_CHECK_DOUBLE_EQUAL(copy_point_std_vector.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_point_std_vector.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(copy_point_std_vector.z(), 20.0);

    }

    TEST_CASE("operator[]")
    {
        felisce::Point non_zero_point(1.0,0.0,0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(non_zero_point[0], 1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(non_zero_point[1], 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(non_zero_point[2], 0.0);
    }

    TEST_CASE("operator!=")
    {
        felisce::Point zero_point(0.0);
        felisce::Point non_zero_point(1.0,0.0,0.0);
        FELISCE_CHECK_NOT_EQUAL(non_zero_point, zero_point);
    }

    TEST_CASE("operator/=")
    {
        felisce::Point point(1.0);
        point /= 2.0;
        FELISCE_CHECK_DOUBLE_EQUAL(point[0], 0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(point[1], 0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(point[2], 0.5);
    }

    TEST_CASE("operator*=")
    {
        felisce::Point point(1.0);
        point *= 2.0;
        FELISCE_CHECK_DOUBLE_EQUAL(point[0], 2.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point[1], 2.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point[2], 2.0);
    }

    TEST_CASE("operator+=")
    {
        felisce::Point point(1.0);
        felisce::Point pointhalf(0.5);
        point += pointhalf;
        FELISCE_CHECK_DOUBLE_EQUAL(point[0], 1.5);
        FELISCE_CHECK_DOUBLE_EQUAL(point[1], 1.5);
        FELISCE_CHECK_DOUBLE_EQUAL(point[2], 1.5);
    }

    TEST_CASE("operator-=")
    {
        felisce::Point point(1.0);
        felisce::Point pointhalf(0.5);
        point -= pointhalf;
        FELISCE_CHECK_DOUBLE_EQUAL(point[0], 0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(point[1], 0.5);
        FELISCE_CHECK_DOUBLE_EQUAL(point[2], 0.5);
    }

    TEST_CASE("norm")
    {
        felisce::Point point(1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.norm(), std::sqrt(3.0));
    }

    TEST_CASE("Clone")
    {
        felisce::Point zero_point(0.0);
        felisce::Point::Pointer p_zero_point = zero_point.Clone();
        FELISCE_CHECK_DOUBLE_EQUAL(p_zero_point->x(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(p_zero_point->y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(p_zero_point->z(), 0.0);
    }

    TEST_CASE("getters")
    {
        felisce::Point point(10.0, 20.0, 30.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.x(), point.coor(0));
        FELISCE_CHECK_DOUBLE_EQUAL(point.y(), point.coor(1));
        FELISCE_CHECK_DOUBLE_EQUAL(point.z(), point.coor(2));

        auto coor = point.coor();
        FELISCE_CHECK_DOUBLE_EQUAL(point.x(), coor[0]);
        FELISCE_CHECK_DOUBLE_EQUAL(point.y(), coor[1]);
        FELISCE_CHECK_DOUBLE_EQUAL(point.z(), coor[2]);

        auto array_coor = point.getCoor();
        FELISCE_CHECK_DOUBLE_EQUAL(array_coor[0], coor[0]);
        FELISCE_CHECK_DOUBLE_EQUAL(array_coor[1], coor[1]);
        FELISCE_CHECK_DOUBLE_EQUAL(array_coor[2], coor[2]);
    }

    TEST_CASE("setters")
    {
        felisce::Point point(0.0);
        point.x() = 10.0;
        point.y() = 20.0;
        point.z() = 30.0;
        FELISCE_CHECK_DOUBLE_EQUAL(point.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.y(), 20.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.z(), 30.0);

        double array[3];
        array[0] = 10.0;
        array[1] = 0.0;
        array[2] = 20.0;

        point.setCoor(array);
        FELISCE_CHECK_DOUBLE_EQUAL(point.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.z(), 20.0);

        std::array<double, 3> std_array{10.0, 50.0, 20.0};

        point.setCoor(std_array);
        FELISCE_CHECK_DOUBLE_EQUAL(point.x(), 10.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.y(), 50.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.z(), 20.0);

        std::vector<double> std_vector({100.0, 0.0, 200.0});

        point.setCoor(std_vector);
        FELISCE_CHECK_DOUBLE_EQUAL(point.x(), 100.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.y(), 0.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point.z(), 200.0);
    }

    TEST_CASE("dist")
    {
        felisce::Point point_a(0.0);
        felisce::Point point_b(1.0);
        FELISCE_CHECK_DOUBLE_EQUAL(point_a.dist(point_b), std::sqrt(3.0));
    }
}

ADD_FELISCE_TEST
