//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Daniele Carlo Corti
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Geometry/Tools/locator.hpp"
#include "Tools/contour_utilities.hpp"
#include "Tools/test_utilities.hpp"

TEST_SUITE("TestLocalizationTria")
{
    TEST_CASE("InsideTria")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 5; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(0.0, 0.0, 0.0);  lstRes[0] =  0; 
      lstPnt[1] = felisce::Point(0.5, 0.0, 0.0);  lstRes[1] =  0; 
      lstPnt[2] = felisce::Point(0.3, 0.3, 0.0);  lstRes[2] =  0; 
      lstPnt[3] = felisce::Point(0.5, 1.0, 0.0);  lstRes[3] =  1; 
      lstPnt[3] = felisce::Point(1.0, 1.0, 0.0);  lstRes[3] =  1; 

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
      FELISCE_CHECK_EQUAL(indElt[3], lstRes[3]);
      FELISCE_CHECK_EQUAL(indElt[4], lstRes[4]);
    } 

    TEST_CASE("NoProjectionTria")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 2; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0,  0.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] = -1; // outside
      lstRes[1] = -1; // outside

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
    } 

    TEST_CASE("ProjectionTria")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 2; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0,  0.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), true);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] =  0; // projection on vert
      lstRes[1] =  0; // projection on edge

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
    } 
}

TEST_SUITE("TestLocalizationQuad")
{
    TEST_CASE("InsideQuad")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshQuadrilaterals();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 3; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(0.0, 0.0, 0.0);  lstRes[0] =  0; 
      lstPnt[1] = felisce::Point(0.5, 0.0, 0.0);  lstRes[1] =  0; 
      lstPnt[2] = felisce::Point(0.5, 0.5, 0.0);  lstRes[2] =  0;

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
    } 

    TEST_CASE("NoProjectionQuad")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshQuadrilaterals();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 2; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0,  0.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] = -1; // outside
      lstRes[1] = -1; // outside

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
    } 

    TEST_CASE("ProjectionQuad")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshQuadrilaterals();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 2; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0,  0.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), true);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] =  0; // projection on vert
      lstRes[1] =  0; // projection on edge

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
    } 
}

TEST_SUITE("TestLocalizationTetra")
{
    TEST_CASE("InsideTetra")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 4; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(0.0, 0.0, 0.0);  lstRes[0] =  0; 
      lstPnt[1] = felisce::Point(1.0, 0.0, 1.0);  lstRes[1] =  3; 
      lstPnt[2] = felisce::Point(0.0, 1.0, 1.0);  lstRes[2] =  4; 
      lstPnt[3] = felisce::Point(0.5, 0.5, 0.5);  lstRes[3] =  2; 

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
      FELISCE_CHECK_EQUAL(indElt[3], lstRes[3]);
    } 

    TEST_CASE("NoProjectionTetra")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 3; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0, -1.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  
      lstPnt[2] = felisce::Point(-1.0,  0.3,  0.3);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] = -1; // outside
      lstRes[1] = -1; // outside
      lstRes[2] = -1; // outside

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
    } 

    TEST_CASE("ProjectionTetra")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 3; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0, -1.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  
      lstPnt[2] = felisce::Point(-1.0,  0.3,  0.3);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), true);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] =  0; // projection on vert
      lstRes[1] =  0; // projection on edge
      lstRes[2] =  0; // projection on face

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
    }
}

TEST_SUITE("TestLocalizationHexa")
{
    TEST_CASE("InsideHexa")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 4; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(0.0, 0.0, 0.0);  lstRes[0] =  0; 
      lstPnt[1] = felisce::Point(0.5, 0.5, 0.0);  lstRes[1] =  0; 
      lstPnt[2] = felisce::Point(0.0, 0.0, 0.5);  lstRes[2] =  0; 
      lstPnt[3] = felisce::Point(0.5, 0.5, 0.5);  lstRes[3] =  0; 

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
      FELISCE_CHECK_EQUAL(indElt[3], lstRes[3]);
    } 

    TEST_CASE("NoProjectionHexa")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      int nbrPnt = 3; 
      std::vector<felisce::felInt> indElt(nbrPnt, -1);
      std::vector<felisce::Point>  lstPnt(nbrPnt);
      std::vector<felisce::felInt> lstRes(nbrPnt);
      std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

      // List of points
      lstPnt[0] = felisce::Point(-1.0, -1.0, -1.0);
      lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  
      lstPnt[2] = felisce::Point(-1.0,  0.5,  0.5);  

      felisce::FelisceParam::alterVerbosity(10);

      // Point localization
      felisce::Locator locator(p_mesh_region.get(), false);
      locator.findListPointsInMesh(indElt, typElt, lstPnt);

      // Point localization
      lstRes[0] = -1; // outside
      lstRes[1] = -1; // outside
      lstRes[2] = -1; // outside

      // Checks
      FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
      FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
      FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
    } 

    // TEST_CASE("ProjectionHexa")
    // {
    //   auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();

    //   felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

    //   int nbrPnt = 3; 
    //   std::vector<felisce::felInt> indElt(nbrPnt, -1);
    //   std::vector<felisce::Point>  lstPnt(nbrPnt);
    //   std::vector<felisce::felInt> lstRes(nbrPnt);
    //   std::vector<felisce::GeometricMeshRegion::ElementType> typElt(nbrPnt, felisce::GeometricMeshRegion::ELEMENT_TYPE_COUNTER);

    //   // List of points
    //   lstPnt[0] = felisce::Point(-1.0, -1.0, -1.0);
    //   lstPnt[1] = felisce::Point(-1.0,  0.5,  0.0);  
    //   lstPnt[2] = felisce::Point(-1.0,  0.5,  0.5);  

    //   felisce::FelisceParam::alterVerbosity(10);

    //   // Point localization
    //   felisce::Locator locator(p_mesh_region.get(), true);
    //   locator.findListPointsInMesh(indElt, typElt, lstPnt);

    //   // Point localization
    //   lstRes[0] =  0; // projection on vert
    //   lstRes[1] =  0; // projection on edge
    //   lstRes[2] =  0; // projection on face

    //   // Checks
    //   FELISCE_CHECK_EQUAL(indElt[0], lstRes[0]);
    //   FELISCE_CHECK_EQUAL(indElt[1], lstRes[1]);
    //   FELISCE_CHECK_EQUAL(indElt[2], lstRes[2]);
    // }
}

ADD_FELISCE_TEST
