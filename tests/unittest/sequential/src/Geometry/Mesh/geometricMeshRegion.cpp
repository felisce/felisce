//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/array_1d.hpp"
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/test_utilities.hpp"
#include "Tools/contour_utilities.hpp"
#include "Tools/math_utilities.hpp"

const std::string meshes_folder = "Resources/";

TEST_SUITE("TestgeometricMeshRegion")
{
    TEST_CASE("Constructors")
    {

        auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

        FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 0);
        FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMeshUndefined);
        FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 0);
        FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
        FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
        FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
        FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
        FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatUndefined);

        felisce::GeometricMeshRegion::ElementType eltType;
        for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
          eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
    }

    TEST_CASE("CompareMeditMinimal")
    {
      const std::string file_name = "minimal";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder,  meshes_folder, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("CompareMeditMinimalQuad")
    {
      const std::string file_name = "minimal_quad";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder,  meshes_folder, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshQuadrilaterals();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Quad4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("CompareMeditMinimalPrism")
    {
      const std::string file_name = "minimal_prism";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder,  meshes_folder, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshPrism();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 6);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Prism6) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }


    TEST_CASE("CompareMeditMinimalPrism9")
    {
      const std::string file_name = "minimal_prism9";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder,  meshes_folder, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshPrism9();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 9);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Prism9) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("CompareMeditMinimalTetrahedra")
    {
      const std::string file_name = "minimal_tetra";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder,  meshes_folder, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 8);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tetra4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 5);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("CompareMeditMinimalHexahedron")
    {
      const std::string file_name = "minimal_hexa";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder,  meshes_folder, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 8);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Hexa8) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("MoveNodesMinimal")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangle();

      // Moving mesh with interface
      std::vector<double> displacement(6, 0.0);
      displacement[0] = 0.25;
      displacement[1] = 0.25;

      p_mesh_region->moveMesh(displacement, 1.0);

      auto point = p_mesh_region->listPoint(0);

      FELISCE_CHECK_DOUBLE_EQUAL(0.25, point[0]);
      FELISCE_CHECK_DOUBLE_EQUAL(0.25, point[1]);
    }

    void TestNormals1D(
      const std::string& rFileName,
      const felisce::Point& rCenter,
      const double Tolerance = 1.0e-1
      )
    {
      auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  rFileName + ".mesh" , rFileName + ".mesh", rFileName + ".out.mesh", meshes_folder, meshes_folder, "test_");

      auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

      p_io->readMesh(*p_mesh_region, 1.0);

      // Dimension
      const auto dimension = p_mesh_region->numCoor();

      // Set the tangent
      if (dimension == 3)  {
        p_mesh_region->allocateListNormalsAndTangents();
        felisce::array_1d<double, 3> tangent_2;
        tangent_2[0] =  1.0/std::sqrt(2);
        tangent_2[1] = -1.0/std::sqrt(2);
        tangent_2[2] =  0.0;

        for (int i_node = 0; i_node < p_mesh_region->numPoints(); ++i_node) {
          auto& r_tangent = p_mesh_region->listTangent(1, i_node);
          r_tangent.getCoor() = tangent_2.data();
        }
      }

      // Compute normals and tangents
      p_mesh_region->computeNormalTangent(felisce::GeometricMeshRegion::InitialNormalTangentProvided::TANGENT_2);

      // Iterate over nodes
      double aux_norm = 0.0;
      for (int i_node = 0; i_node < p_mesh_region->numPoints(); ++i_node) {
        const auto& r_normal = p_mesh_region->listNormal(i_node);
        const double norm = r_normal.norm();
        aux_norm += norm;
        if (norm > std::numeric_limits<double>::epsilon()) {
          felisce::Point aux_point = p_mesh_region->listPoint(i_node);
          if (dimension == 2) aux_point.z() = 0.0;
          aux_point -= rCenter;
          aux_point /= aux_point.norm();
          aux_point -= r_normal;
          aux_point /= r_normal.norm();

          FELISCE_CHECK_LESS_EQUAL(aux_point.norm(), Tolerance);
        }
      }
      FELISCE_CHECK_GREATER(aux_norm, 1.0e-6);
    }

    void TestTangent1D(
      const std::string& rFileName,
      const felisce::Point& rCenter,
      const double Tolerance = 1.0e-1
      )
    {
      auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  rFileName + ".mesh" , rFileName + ".mesh", rFileName + ".out.mesh", meshes_folder, meshes_folder, "test_");

      auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

      p_io->readMesh(*p_mesh_region, 1.0);

      // Compute normals and tangents
      p_mesh_region->computeNormalTangent();

      // Dimension
      const auto dimension = p_mesh_region->numCoor();

      // Iterate over nodes
      double aux_norm = 0.0;
      for (int i_node = 0; i_node < p_mesh_region->numPoints(); ++i_node) {
        const auto& r_tangent = p_mesh_region->listTangent(0, i_node);
        const double norm = r_tangent.norm();
        aux_norm += norm;
        if (norm > std::numeric_limits<double>::epsilon()) {
          felisce::Point aux_point = p_mesh_region->listPoint(i_node);
          if (dimension == 2) aux_point.z() = 0.0;
          aux_point -= rCenter;
          aux_point /= aux_point.norm();
          if (dimension == 2)  {
            const double aux = aux_point.x();
            aux_point.x() = aux_point.y();
            aux_point.y() = -aux;
          } else {
            felisce::array_1d<double, 3> normal, tangent_1, tangent_2;
            normal.data() = aux_point.getCoor();
            tangent_2[0] =  1.0/std::sqrt(2);
            tangent_2[1] = -1.0/std::sqrt(2);
            tangent_2[2] =  0.0;
            felisce::MathUtilities::UnitCrossProduct(tangent_1, normal, tangent_2);
            aux_point.getCoor() = tangent_1.data();
          }
          aux_point -= r_tangent;
          aux_point /= r_tangent.norm();

          FELISCE_CHECK_LESS_EQUAL(aux_point.norm(), Tolerance);
        }
      }
      FELISCE_CHECK_GREATER(aux_norm, 1.0e-6);
    }

    TEST_CASE("ComputeNormals1D")
    {
      auto point_2d = felisce::Point(5.0,0.0,0.0);
      TestNormals1D("arc2D", point_2d);
      TestTangent1D("arc2D", point_2d);
      auto point_3d = felisce::Point(5.0,5.0,0.0);
      TestNormals1D("arc3D", point_3d);
      TestTangent1D("arc3D", point_3d);
    }

    void TestNormals(
      const std::string& rFileName,
      const double Tolerance = 1.0e-1
      )
    {
      auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  rFileName + ".mesh" , rFileName + ".mesh", rFileName + ".out.mesh", meshes_folder, meshes_folder, "test_");

      auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

      p_io->readMesh(*p_mesh_region, 1.0);

      if (p_mesh_region->domainDim() == 3) {
          felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);
      }

      // Compute normals and tangents
      p_mesh_region->computeNormalTangent();

      // Iterate over nodes
      double aux_norm = 0.0;
      for (int i_node = 0; i_node < p_mesh_region->numPoints(); ++i_node) {
        const auto& r_normal = p_mesh_region->listNormal(i_node);
        const double norm = r_normal.norm();
        aux_norm += norm;
        if (norm > std::numeric_limits<double>::epsilon()) {
          felisce::Point aux_point = p_mesh_region->listPoint(i_node);
          aux_point /= aux_point.norm();
          aux_point -= r_normal;
          aux_point /= r_normal.norm();

          FELISCE_CHECK_LESS_EQUAL(aux_point.norm(), Tolerance);
        }
      }
      FELISCE_CHECK_GREATER(aux_norm, 1.0e-6);
    }

    TEST_CASE("ComputeNormals3D") // TODO: Add tests for 2D and 1D meshes
    {
      TestNormals("coarse_sphere", 1.5e-1);
      TestNormals("fine_sphere");
      TestNormals("coarse_sphere_skin", 1.5e-1);
      TestNormals("fine_sphere_skin");
    }

    // TEST_CASE("PostProcesIO")
    // {
    //   const std::string file_name = "coarse_sphere_skin";
    //   auto p_io = felisce::make_shared<felisce::IO>(meshes_folder,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.geo", meshes_folder, meshes_folder, "test_");

    //   auto p_mesh_region = felisce::make_shared<felisce::GeometricMeshRegion>();

    //   p_io->readMesh(*p_mesh_region, 1.0);

    //   // Compute normals and tangents
    //   p_mesh_region->computeNormalTangent();

    //   // Postprocess
    //   p_io->writeOutput(*p_mesh_region, 0);
    //   int rank = 0;
    //   int iteration = 1;
    //   int dimension = 3;
    //   double time = 1.0;
    //   double* solution = new double[p_mesh_region->numPoints() * dimension];
    //   for (int i_node = 0; i_node < p_mesh_region->numPoints(); ++i_node) {
    //     const auto& r_normal = p_mesh_region->listNormal(i_node);
    //     solution[0 + dimension * i_node] = r_normal.x();
    //     solution[1 + dimension * i_node] = r_normal.y();
    //     solution[2 + dimension * i_node] = r_normal.z();
    //   }
    //   p_io->writeSolution(rank, time, iteration, 0, "NORMAL", solution, p_mesh_region->numPoints(), dimension, true);
    //   p_io->postProcess(time);

    //   // Do not need to delete here solution because it is deleted in EnsightCase::postProcess (manageMemory of io.writeSolution = true)
    //   // delete [] solution;
    // }
}

ADD_FELISCE_TEST
