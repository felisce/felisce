# Install test UnitTest_Seq_Src_Tools_Table_Interpolation

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_Tools_Table_Interpolation
    SOURCES test_table_interpolation.cpp
    INSTALL unittest/sequential/src/Tools/Table_Interpolation
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_Tools_Table_Interpolation
    EXEC UnitTest_Seq_Src_Tools_Table_Interpolation
    INSTALL unittest/sequential/src/Tools/Table_Interpolation
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_Tools_Table_Interpolation
    INSTALL unittest/sequential/src/Tools/Table_Interpolation
)
