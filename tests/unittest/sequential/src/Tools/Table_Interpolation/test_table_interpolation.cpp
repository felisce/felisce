//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Tools/table_interpolation.hpp"

TEST_SUITE("TableInterpolation")
{
    TEST_CASE("TableInterpolation")
    {
      felisce::TableInterpolation table;
      for (std::size_t i = 0; i < 6; ++i)
        table.PushBack(static_cast<double>(i), 2.0 * static_cast<double>(i));

      double nearest = (table.GetNearestRow(2.1))[0];
      FELISCE_CHECK_DOUBLE_EQUAL(nearest, 4.0);
      FELISCE_CHECK_DOUBLE_EQUAL(table.GetValue(2.1), 4.2);
      FELISCE_CHECK_DOUBLE_EQUAL(table(2.1), 4.2);
      FELISCE_CHECK_DOUBLE_EQUAL(table.GetDerivative(2.1), 2.0);

      auto& r_data = table.Data();
      FELISCE_CHECK_EQUAL(r_data.size(), 6);

      // Clear database
      table.Clear();
      FELISCE_CHECK_EQUAL(r_data.size(), 0);

      // Inverse filling with insert
      for (std::size_t i = 6; i > 0; --i)
          table.insert(static_cast<double>(i), 2.0 * static_cast<double>(i));

      FELISCE_CHECK_EQUAL(r_data.size(), 6);

      nearest = (table.GetNearestRow(2.1))[0];
      FELISCE_CHECK_DOUBLE_EQUAL(nearest, 4.0);
      FELISCE_CHECK_DOUBLE_EQUAL(table.GetValue(2.1), 4.2);
      FELISCE_CHECK_DOUBLE_EQUAL(table(2.1), 4.2);
      FELISCE_CHECK_DOUBLE_EQUAL(table.GetDerivative(2.1), 2.0);
    }
}

ADD_FELISCE_TEST
