//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/test_utilities.hpp"
#include "Tools/fe_utilities.hpp"

TEST_SUITE("TestFEUtilities")
{
    TEST_CASE("LoopOverElements")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();
      const auto function = [](felisce::GeometricMeshRegion::ElementType& eltType, felisce::felInt iel, std::vector<felisce::Point*>& elemPoint, std::vector<felisce::felInt>& elemIdPoint, felisce::felInt& ielSupportDof) {
        IGNORE_UNUSED_ARGUMENT(eltType);
        IGNORE_UNUSED_ARGUMENT(iel);
        IGNORE_UNUSED_ARGUMENT(elemPoint);
        IGNORE_UNUSED_ARGUMENT(elemIdPoint);
        IGNORE_UNUSED_ARGUMENT(ielSupportDof);
        return 1.0;
      };
      const double aux = felisce::FEUtilities::LoopOverElements(*p_mesh_region, p_mesh_region->bagElementTypeDomain(), &function);
      // Check
      FELISCE_CHECK_DOUBLE_EQUAL(aux, 2.0);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshTriangle")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangle();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region);
      FELISCE_CHECK_EQUAL(inverted, false);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshInvertedTriangle")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshInvertedTriangle();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region);
      FELISCE_CHECK_EQUAL(inverted, true);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshTriangleJacobian")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangle();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region, true);
      FELISCE_CHECK_EQUAL(inverted, false);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshInvertedTriangleJacobian")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshInvertedTriangle();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region, true);
      FELISCE_CHECK_EQUAL(inverted, true);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshTetrahedron")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region);
      FELISCE_CHECK_EQUAL(inverted, false);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshInvertedTetrahedron")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshInvertedTetrahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region);
      FELISCE_CHECK_EQUAL(inverted, true);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshTetrahedronJacobian")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region, true);
      FELISCE_CHECK_EQUAL(inverted, false);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshInvertedTetrahedronJacobian")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshInvertedTetrahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region, true);
      FELISCE_CHECK_EQUAL(inverted, true);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshHexahedron")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region);
      FELISCE_CHECK_EQUAL(inverted, false);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshInvertedHexahedron")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshInvertedHexahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region);
      FELISCE_CHECK_EQUAL(inverted, true);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshHexahedronJacobian")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region, true);
      FELISCE_CHECK_EQUAL(inverted, false);
    }

    TEST_CASE("CheckVolumeIsInvertedMeshInvertedHexahedronJacobian")
    {
      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshInvertedHexahedron();
      const bool inverted = felisce::FEUtilities::CheckVolumeIsInverted(*p_mesh_region, true);
      FELISCE_CHECK_EQUAL(inverted, true);
    }
}

ADD_FELISCE_TEST
