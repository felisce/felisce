# Install test UnitTest_Seq_Src_Tools_Sciplot

felisce_install_test_exe(
    NAME UnitTest_Seq_Src_Tools_Sciplot
    SOURCES sciplot_utilities.cpp
    INSTALL unittest/sequential/src/Tools/Sciplot
)

felisce_install_tests_data(
    NAME UnitTest_Seq_Src_Tools_Sciplot
    EXEC UnitTest_Seq_Src_Tools_Sciplot
    INSTALL unittest/sequential/src/Tools/Sciplot
    CONFIG 
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Seq_Src_Tools_Sciplot
    INSTALL unittest/sequential/src/Tools/Sciplot
)
