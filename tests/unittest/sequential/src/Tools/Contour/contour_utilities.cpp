//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/filesystemUtil.hpp"
#include "InputOutput/io.hpp"
#include "Geometry/geometricMeshRegion.hpp"
#include "Tools/test_utilities.hpp"
#include "Tools/contour_utilities.hpp"

const std::string meshes_folder_tools = "Resources/";

TEST_SUITE("TestContourUtilities")
{
    TEST_CASE("MinimalHexahedron")
    {
      const std::string file_name = "minimal_hexa_with_skin";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder_tools,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder_tools,  meshes_folder_tools, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshHexahedron();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 8);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Hexa8) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 8);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Hexa8) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else if (eltType == felisce::GeometricMeshRegion::Quad4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 6);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder_tools + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder_tools + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("MinimalTria")
    {
      const std::string file_name = "minimal_with_skin";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder_tools,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder_tools,  meshes_folder_tools, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTriangles();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
        } else if (eltType == felisce::GeometricMeshRegion::Seg2) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 4);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder_tools + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder_tools + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("MinimalQuad")
    {
      const std::string file_name = "minimal_quad_with_skin";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder_tools,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder_tools,  meshes_folder_tools, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshQuadrilaterals();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Quad4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 2);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh2D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 4);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Quad4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else if (eltType == felisce::GeometricMeshRegion::Seg2) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 4);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder_tools + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder_tools + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("MinimalTetrahedra")
    {
      const std::string file_name = "minimal_tetra_with_skin";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder_tools,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder_tools,  meshes_folder_tools, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshTetrahedra();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 8);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tetra4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 5);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 8);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Tetra4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 5);
        } else if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 12);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder_tools + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder_tools + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("MinimalPrism6")
    {
      const std::string file_name = "minimal_prism_with_skin";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder_tools,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder_tools,  meshes_folder_tools, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshPrism();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 6);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Prism6) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 6);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Prism6) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
        } else if (eltType == felisce::GeometricMeshRegion::Quad4) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 3);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder_tools + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder_tools + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }

    TEST_CASE("MinimalPrism9")
    {
      const std::string file_name = "minimal_prism9_with_skin";
      auto p_io = felisce::make_shared<felisce::IO>( meshes_folder_tools,  file_name + ".mesh" , file_name + ".mesh", file_name + ".out.mesh",  meshes_folder_tools,  meshes_folder_tools, "test_");

      auto p_mesh_region = felisce::TestUtilities::GenerateMinimalMeshPrism9();

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 9);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      felisce::GeometricMeshRegion::ElementType eltType;
      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Prism9) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      felisce::ContourUtilities::GenerateContourMesh(*p_mesh_region);

      FELISCE_CHECK_EQUAL(p_mesh_region->numCoor(), 3);
      FELISCE_CHECK_EQUAL(p_mesh_region->domainDim(), felisce::GeometricMeshRegion::GeoMesh3D);
      FELISCE_CHECK_EQUAL(p_mesh_region->numPoints(), 9);
      FELISCE_CHECK_EQUAL(p_mesh_region->moved(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->createNormalTangent(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusEdges(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->statusFaces(), false);
      FELISCE_CHECK_EQUAL(p_mesh_region->flagFormatMesh(), felisce::GeometricMeshRegion::FormatMedit);

      for (int ityp = 0; ityp < felisce::GeometricMeshRegion::m_numTypesOfElement; ityp++) {
        eltType = static_cast<felisce::GeometricMeshRegion::ElementType>(ityp);
        if (eltType == felisce::GeometricMeshRegion::Prism9) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 1);
        } else if (eltType == felisce::GeometricMeshRegion::Tria3) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 2);
        } else if (eltType == felisce::GeometricMeshRegion::Quad6) {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 3);
        } else {
          FELISCE_CHECK_EQUAL(p_mesh_region->numElements(eltType), 0);
        }
      }

      // Testing writing file
      p_io->writeOutput(*p_mesh_region, 0);

      // Checking files
      const std::string& r_file_1 = meshes_folder_tools + file_name + ".mesh";
      const std::string& r_file_2 = meshes_folder_tools + file_name + ".out.mesh";
      FELISCE_CHECK(felisce::TestUtilities::CompareTwoFiles(r_file_1, r_file_2));
    }
}

ADD_FELISCE_TEST
