//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/array_1d.hpp"
#include "Tools/math_utilities.hpp"

TEST_SUITE("TestMathUtilities")
{
  TEST_CASE("Det")
  {
    constexpr double tolerance = 1e-6;

    felisce::UBlasBoundedMatrix<double, 1, 1> mat11 = felisce::UBlasZeroMatrix(1, 1);
    mat11(0,0) = 1.0;

    double det = felisce::MathUtilities::Det(mat11);

    FELISCE_CHECK_NEAR(det, 1.0, tolerance);

    felisce::UBlasBoundedMatrix<double, 2, 2> mat22 = felisce::UBlasZeroMatrix(2, 2);
    mat22(0,0) = 1.0;
    mat22(1,1) = 1.0;

    det = felisce::MathUtilities::Det(mat22);

    FELISCE_CHECK_NEAR(det, 1.0, tolerance);

    felisce::UBlasBoundedMatrix<double, 3, 3> mat33 = felisce::UBlasZeroMatrix(3, 3);
    mat33(0,0) = 1.0;
    mat33(1,1) = 1.0;
    mat33(2,2) = 1.0;

    det = felisce::MathUtilities::Det(mat33);

    FELISCE_CHECK_NEAR(det, 1.0, tolerance);

    felisce::UBlasBoundedMatrix<double, 4, 4> mat44 = felisce::UBlasZeroMatrix(4, 4);
    mat44(0,0) = 1.0;
    mat44(1,1) = 1.0;
    mat44(2,2) = 1.0;
    mat44(3,3) = 1.0;

    det = felisce::MathUtilities::Det(mat44);

    FELISCE_CHECK_NEAR(det, 1.0, tolerance);
  }

  TEST_CASE("GenDetMat")
  {
    constexpr double tolerance = 1e-6;

    felisce::UBlasMatrix mat23 = felisce::UBlasZeroMatrix(2, 3);
    mat23(0,0) = 1.0;
    mat23(1,1) = 1.0;

    double det = felisce::MathUtilities::GeneralizedDet(mat23);

    FELISCE_CHECK_NEAR(det, 1.0, tolerance);

    felisce::UBlasMatrix mat55 = felisce::UBlasZeroMatrix(5, 5);
    mat55(0,0) =   1.0;
    mat55(1,1) =   1.0;
    mat55(2,2) =   1.0;
    mat55(3,3) =   1.0;
    mat55(2,3) = - 1.0;
    mat55(3,2) =   1.0;
    mat55(4,4) =   2.0;

    det = felisce::MathUtilities::Det(mat55);

    FELISCE_CHECK_NEAR(det, 4.0, tolerance);
  }

  TEST_CASE("InvertMatrix")
  {
    constexpr double tolerance = 1e-6;

    double det;
    felisce::UBlasMatrix inv(1,1);
    felisce::UBlasMatrix I(1,1);

    std::size_t i_dim = 1;

    felisce::UBlasMatrix mat = felisce::UBlasZeroMatrix(i_dim, i_dim);

    mat(0,0) = 0.346432;

    felisce::MathUtilities::InvertMatrix(mat,inv, det);

    I = prod(inv, mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }

    i_dim = 2;
    mat.resize(i_dim, i_dim, false);
    inv.resize(i_dim, i_dim, false);
    I.resize(i_dim, i_dim, false);

    mat(0,0) = 0.833328;
    mat(0,1) = 0.491166;
    mat(1,0) = 0.81167;
    mat(1,1) = 1.17205;

    felisce::MathUtilities::InvertMatrix(mat,inv, det);

    I = prod(inv, mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }

    i_dim = 3;
    mat.resize(i_dim, i_dim, false);
    inv.resize(i_dim, i_dim, false);
    I.resize(i_dim, i_dim, false);

    mat(0,0) = 0.371083;
    mat(0,1) = 0.392607;
    mat(0,2) = 0.306494;
    mat(1,0) = 0.591012;
    mat(1,1) = 1.00733;
    mat(1,2) = 1.07727;
    mat(2,0) = 0.0976054;
    mat(2,1) = 2.54893;
    mat(2,2) = 1.23981;

    felisce::MathUtilities::InvertMatrix(mat,inv, det);

    I = prod(inv, mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }

    i_dim = 4;
    mat.resize(i_dim, i_dim, false);
    inv.resize(i_dim, i_dim, false);
    I.resize(i_dim, i_dim, false);

    mat(0,0) = 0.0;
    mat(0,1) = 0.979749;
    mat(0,2) = 0.494393;
    mat(0,3) = 0.23073;
    mat(1,0) = 1.79224;
    mat(1,1) = 0.198842;
    mat(1,2) = 0.074485;
    mat(1,3) = 1.45717;
    mat(2,0) = 1.6039;
    mat(2,1) = 0.673926;
    mat(2,2) = 2.63817;
    mat(2,3) = 1.0287;
    mat(3,0) = 0.366503;
    mat(3,1) = 3.02634;
    mat(3,2) = 1.24104;
    mat(3,3) = 3.62022;

    felisce::MathUtilities::InvertMatrix(mat,inv, det);

    I = prod(inv, mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }

    i_dim = 5;
    mat.resize(i_dim, i_dim, false);
    inv.resize(i_dim, i_dim, false);
    I.resize(i_dim, i_dim, false);

    mat = felisce::UBlasZeroMatrix(5, 5);
    mat(0,0) =   1.0;
    mat(1,1) =   1.0;
    mat(2,2) =   1.0;
    mat(3,3) =   1.0;
    mat(2,3) = - 1.0;
    mat(3,2) =   1.0;
    mat(4,4) =   2.0;

    felisce::MathUtilities::InvertMatrix(mat,inv, det);

    FELISCE_CHECK_NEAR(det, 4.0, tolerance);

    I = prod(inv, mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }

    felisce::UBlasBoundedMatrix<double,5,5> b_mat = felisce::UBlasZeroMatrix(5, 5);
    felisce::UBlasBoundedMatrix<double,5,5> b_inv;
    b_mat(0,0) =   1.0;
    b_mat(1,1) =   1.0;
    b_mat(2,2) =   1.0;
    b_mat(3,3) =   1.0;
    b_mat(2,3) = - 1.0;
    b_mat(3,2) =   1.0;
    b_mat(4,4) =   2.0;

    felisce::MathUtilities::InvertMatrix(b_mat,b_inv, det);

    FELISCE_CHECK_NEAR(det, 4.0, tolerance);

    felisce::UBlasBoundedMatrix<double,5,5> b_I = felisce::UBlasZeroMatrix(5);
    noalias(b_I) = prod(b_inv, b_mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(b_I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(b_I(i,j), 0.0, tolerance);
        }
      }
    }
  }

  TEST_CASE("Solve")
  {
      constexpr double tolerance = 1e-6;

      const std::size_t i_dim = 4;
      double det;
      felisce::UBlasMatrix A(i_dim, i_dim);
      felisce::UBlasMatrix inv(i_dim, i_dim);
      felisce::UBlasVector b(i_dim);

      A(0,0) = 0.0;
      A(0,1) = 0.979749;
      A(0,2) = 0.494393;
      A(0,3) = 0.23073;
      A(1,0) = 1.79224;
      A(1,1) = 0.198842;
      A(1,2) = 0.074485;
      A(1,3) = 1.45717;
      A(2,0) = 1.6039;
      A(2,1) = 0.673926;
      A(2,2) = 2.63817;
      A(2,3) = 1.0287;
      A(3,0) = 0.366503;
      A(3,1) = 3.02634;
      A(3,2) = 1.24104;
      A(3,3) = 3.62022;

      b[0] = 0.0;
      b[1] = 1.0;
      b[2] = 2.0;
      b[3] = 3.0;

      felisce::MathUtilities::InvertMatrix(A,inv, det);

      const felisce::UBlasVector ref_x = prod(inv, b);
      felisce::UBlasVector x;

      felisce::MathUtilities::Solve(A,x,b);

      FELISCE_CHECK_VECTOR_NEAR(ref_x, x, tolerance);
  }

  TEST_CASE("GeneralizedInvertMatrix")
  {
    constexpr double tolerance = 1e-6;

    // We check the Left inverse

    const std::size_t i_dim = 2;
    const std::size_t j_dim = 3;

    felisce::UBlasMatrix mat = felisce::UBlasZeroMatrix(i_dim, j_dim);

    mat(0,0) = 0.770724;
    mat(1,0) = 0.573294;
    mat(0,1) = 1.27699;
    mat(1,1) = 1.57776;
    mat(0,2) = 1.30216;
    mat(1,2) = 2.66483;

    double det;
    felisce::UBlasMatrix inv;

    felisce::MathUtilities::GeneralizedInvertMatrix(mat,inv, det);

    felisce::UBlasMatrix I = prod(mat, inv);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }

    // We check the Right inverse
    mat.resize(j_dim, i_dim, false);
    mat = felisce::UBlasZeroMatrix(j_dim, i_dim);

    mat(0,0) = 0.786075;
    mat(1,0) = 0.91272;
    mat(2,0) = 0.745604;
    mat(0,1) = 0.992728;
    mat(1,1) = 1.82324;
    mat(2,1) = 0.19581;

    felisce::MathUtilities::GeneralizedInvertMatrix(mat,inv, det);

    I = prod(inv, mat);

    for (std::size_t i = 0; i < i_dim; i++) {
      for (std::size_t j = 0; j < i_dim; j++) {
        if (i == j) {
          FELISCE_CHECK_NEAR(I(i,j), 1.0, tolerance);
        } else {
          FELISCE_CHECK_NEAR(I(i,j), 0.0, tolerance);
        }
      }
    }
  }

  TEST_CASE("TestBtDBProductOperation")
  {
    constexpr double tolerance = 1e-6;

    felisce::UBlasMatrix matB = felisce::UBlasZeroMatrix(3, 3);
    matB(0,0) = 0.371083;
    matB(0,1) = 0.392607;
    matB(0,2) = 0.306494;
    matB(1,0) = 0.591012;
    matB(1,1) = 1.00733;
    matB(1,2) = 1.07727;
    matB(2,0) = 0.0976054;
    matB(2,1) = 2.54893;
    matB(2,2) = 1.23981;

    felisce::UBlasMatrix matD = felisce::UBlasIdentityMatrix(3, 3);

    felisce::UBlasMatrix result(3,3);
    felisce::MathUtilities::BtDBProductOperation(result,matD, matB);

    const felisce::UBlasMatrix ref = prod(trans(matB), felisce::UBlasMatrix(prod(matD, matB)));

    FELISCE_CHECK_MATRIX_NEAR(ref, result, tolerance);
  }

  TEST_CASE("TestBDBtProductOperation")
  {
    constexpr double tolerance = 1e-6;

    felisce::UBlasMatrix matB = felisce::UBlasZeroMatrix(3, 3);
    matB(0,0) = 0.371083;
    matB(0,1) = 0.392607;
    matB(0,2) = 0.306494;
    matB(1,0) = 0.591012;
    matB(1,1) = 1.00733;
    matB(1,2) = 1.07727;
    matB(2,0) = 0.0976054;
    matB(2,1) = 2.54893;
    matB(2,2) = 1.23981;

    felisce::UBlasMatrix matD = felisce::UBlasIdentityMatrix(3, 3);

    felisce::UBlasMatrix result(3,3);
    felisce::MathUtilities::BDBtProductOperation(result,matD, matB);

    const felisce::UBlasMatrix ref = prod(matB, felisce::UBlasMatrix(prod(matD, trans(matB))));

    FELISCE_CHECK_MATRIX_NEAR(ref, result, tolerance);
  }

  TEST_CASE("CrossProduct")
  {
      felisce::array_1d<double, 3> a = felisce::UBlasZeroVector(3);
      a[1] = 2.0;
      felisce::array_1d<double, 3> b = felisce::UBlasZeroVector(3);
      b[0] = 1.0;

      felisce::array_1d<double, 3>  c, d;

      felisce::MathUtilities::CrossProduct(c, b, a);
      felisce::MathUtilities::UnitCrossProduct(d, b, a);
      felisce::array_1d<double,3> e = felisce::MathUtilities::CrossProduct(b, a);

      FELISCE_CHECK_EQUAL(c[2], 2.0);
      FELISCE_CHECK_EQUAL(d[2], 1.0);
      FELISCE_CHECK_EQUAL(e[2], 2.0);
  }

  TEST_CASE("OrthonormalBasis")
  {
      felisce::array_1d<double, 3> a = felisce::UBlasZeroVector(3);
      a[1] = 1.0;

      felisce::array_1d<double, 3>  b, c;

      felisce::MathUtilities::OrthonormalBasis(a, b, c);

      FELISCE_CHECK_EQUAL(b[0], 1.0);
      FELISCE_CHECK_EQUAL(c[2], -1.0);

      felisce::MathUtilities::OrthonormalBasisHughesMoeller(a, b, c);

      FELISCE_CHECK_EQUAL(b[0], 1.0);
      FELISCE_CHECK_EQUAL(c[2], -1.0);

      felisce::MathUtilities::OrthonormalBasisFrisvad(a, b, c);

      FELISCE_CHECK_EQUAL(b[0], 1.0);
      FELISCE_CHECK_EQUAL(c[2], -1.0);

      felisce::MathUtilities::OrthonormalBasisNaive(a, b, c);

      FELISCE_CHECK_EQUAL(b[0], 1.0);
      FELISCE_CHECK_EQUAL(c[2], -1.0);
  }

  TEST_CASE("LCS2Euler")
  {
    felisce::array_1d<double, 3> a = felisce::UBlasZeroVector(3);
    a[0] = 1.0;
    felisce::array_1d<double, 3> b = felisce::UBlasZeroVector(3);
    b[1] = 1.0;
    felisce::array_1d<double, 3> c = felisce::UBlasZeroVector(3);
    c[2] = 1.0;
    double pre, nut, rot;
    felisce::MathUtilities::LCS2Euler(a, b, c, pre, nut, rot);
    FELISCE_CHECK_EQUAL(pre, 0.0);
    FELISCE_CHECK_EQUAL(nut, 0.0);
    FELISCE_CHECK_EQUAL(rot, 0.0);
    a.clear(); c.clear();
    a[2] = 1.0;
    c[0] = -1.0;
    felisce::MathUtilities::LCS2Euler(a, b, c, pre, nut, rot);
    FELISCE_CHECK_EQUAL(pre, M_PI/2.0);
    FELISCE_CHECK_EQUAL(nut, M_PI/2.0);
    FELISCE_CHECK_EQUAL(rot, -M_PI/2.0);
  }

  TEST_CASE("Euler2RotationMatrix")
  {
    felisce::UBlasMatrix R(3, 3);
    R.clear();
    felisce::MathUtilities::Euler2RotationMatrix(R, 0, 0, 0);
    for (std::size_t i = 0; i < 3; ++i) {
      FELISCE_CHECK_EQUAL(R(i, i), 1.0);
    }
    felisce::MathUtilities::Euler2RotationMatrix(R, M_PI/2.0, M_PI/2.0, M_PI/2.0);
    FELISCE_CHECK_EQUAL(R(0, 2), 1.0);
    FELISCE_CHECK_EQUAL(R(1, 1), 1.0);
    FELISCE_CHECK_EQUAL(R(2, 0), -1.0);
  }

  TEST_CASE("ExtendToAllDofsLocalMatrix")
  {
    felisce::UBlasMatrix A(3, 3);
    A.clear();
    for (std::size_t i = 0; i < 3; ++i) {
      A(i, i) = 1.0;
    }
    felisce::UBlasMatrix B(12, 12);
    felisce::MathUtilities::ExtendToAllDofsLocalMatrix(A, B);
    for (std::size_t i = 0; i < 12; ++i) {
      FELISCE_CHECK_EQUAL(B(i, i), 1.0);
    }
  }

  TEST_CASE("ExtendToAllDofsLocalMatrixFELiScEOrdering")
  {
    felisce::UBlasMatrix A(3, 3);
    A.clear();
    for (std::size_t i = 0; i < 3; ++i) {
      A(i, i) = 1.0;
    }
    felisce::UBlasMatrix B(12, 12);
    felisce::MathUtilities::ExtendToAllDofsLocalMatrixFELiScEOrdering(A, B, 2);
    for (std::size_t i = 0; i < 12; ++i) {
      FELISCE_CHECK_EQUAL(B(i, i), 1.0);
    }
  }
}

ADD_FELISCE_TEST
