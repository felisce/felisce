//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Tools/test_utilities.hpp"

TEST_SUITE("TestelementVector")
{
    TEST_CASE("Access")
    {
      auto vector_3 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);

      FELISCE_CHECK_EQUAL(vector_3.size(), 3);
      FELISCE_CHECK_EQUAL(vector_3.numBlockRow(), 1);

      auto vector_4 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Quad4, 1);

      FELISCE_CHECK_EQUAL(vector_4.size(), 4);
      FELISCE_CHECK_EQUAL(vector_4.numBlockRow(), 1);

      auto vector_3_2 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 2);

      FELISCE_CHECK_EQUAL(vector_3_2.size(), 6);
      FELISCE_CHECK_EQUAL(vector_3_2.numBlockRow(), 2);

      auto vector_4_3 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tetra4, 3);

      FELISCE_CHECK_EQUAL(vector_4_3.size(), 12);
      FELISCE_CHECK_EQUAL(vector_4_3.numBlockRow(), 3);
    }

    TEST_CASE("zero")
    {
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);

      felisce::UBlasVector compare(3);
      compare.clear();

      vector.zero();
      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);
    }

    TEST_CASE("operator*")
    {
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);

      felisce::UBlasVector compare(3);
      compare.clear();
      compare[0] = 12.0;

      vector.zero();
      vector[0] = 1.0;
      vector *= 12.0;
      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);
    }

    TEST_CASE("operator+")
    {
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);

      auto compare = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);
      compare.zero();
      compare[0] = 12.0;

      vector.zero();
      vector += compare;
      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);
    }

    TEST_CASE("operator-")
    {
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);

      auto compare = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, 1);
      compare.zero();
      compare[0] = 12.0;

      vector.zero();
      vector -= compare;
      compare[0] = -12.0;
      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);
    }

    TEST_CASE("sourceTria3")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tria3);

      // Get vector
      std::size_t number_of_components = 1;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector.source(1.0, *p_element, field, 0, number_of_components);

      // Reference solution
      felisce::UBlasVector compare(3);
      compare[0] = 1.0/6.0;
      compare[1] = 1.0/6.0;
      compare[2] = 1.0/6.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute source factor 6
      vector.zero();
      vector.source(6.0, *p_element, field, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Get vector
      number_of_components = 2;
      auto vector_2 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, number_of_components);

      // Set field
      auto field_2 = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector_2.source(1.0, *p_element, field_2, 0, number_of_components);

      // Reference solution
      compare.resize(6);
      compare.clear();
      compare[0] = 1.0/6.0;
      compare[1] = 1.0/6.0;
      compare[2] = 1.0/6.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_2, compare);

      // Compute source factor 6
      vector_2.zero();
      vector_2.source(6.0, *p_element, field_2, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_2, compare);
    }

    TEST_CASE("sourceQuad4")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Quad4);

      // Get vector
      std::size_t number_of_components = 1;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Quad4, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector.source(1.0, *p_element, field, 0, number_of_components);

      // Reference solution
      felisce::UBlasVector compare(4);
      compare[0] = 1.0/4.0;
      compare[1] = 1.0/4.0;
      compare[2] = 1.0/4.0;
      compare[3] = 1.0/4.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute source factor 4
      vector.zero();
      vector.source(4.0, *p_element, field, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Get vector
      number_of_components = 2;
      auto vector_2 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Quad4, number_of_components);

      // Set field
      auto field_2 = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector_2.source(1.0, *p_element, field_2, 0, number_of_components);

      // Reference solution
      compare.resize(8);
      compare.clear();
      compare[0] = 1.0/4.0;
      compare[1] = 1.0/4.0;
      compare[2] = 1.0/4.0;
      compare[3] = 1.0/4.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_2, compare);

      // Compute source factor 4
      vector_2.zero();
      vector_2.source(4.0, *p_element, field_2, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_2, compare);
    }

    TEST_CASE("sourceTetra4")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tetra4);

      // Get vector
      std::size_t number_of_components = 1;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tetra4, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector.source(1.0, *p_element, field, 0, number_of_components);

      // Reference solution
      felisce::UBlasVector compare(4);
      compare[0] = 1.0/24.0;
      compare[1] = 1.0/24.0;
      compare[2] = 1.0/24.0;
      compare[3] = 1.0/24.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute source factor 24
      vector.zero();
      vector.source(24.0, *p_element, field, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Get vector
      number_of_components = 3;
      auto vector_3 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tetra4, number_of_components);

      // Set field
      auto field_3 = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector_3.source(1.0, *p_element, field_3, 0, number_of_components);

      // Reference solution
      compare.resize(12);
      compare.clear();
      compare[0] = 1.0/24.0;
      compare[1] = 1.0/24.0;
      compare[2] = 1.0/24.0;
      compare[3] = 1.0/24.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_3, compare);

      // Compute source factor 24
      vector_3.zero();
      vector_3.source(24.0, *p_element, field_3, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_3, compare);
    }

    TEST_CASE("sourceHexa8")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Hexa8);

      // Get vector
      std::size_t number_of_components = 1;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Hexa8, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector.source(1.0, *p_element, field, 0, number_of_components);

      // Reference solution
      felisce::UBlasVector compare(8);
      compare[0] = 1.0/8.0;
      compare[1] = 1.0/8.0;
      compare[2] = 1.0/8.0;
      compare[3] = 1.0/8.0;
      compare[4] = 1.0/8.0;
      compare[5] = 1.0/8.0;
      compare[6] = 1.0/8.0;
      compare[7] = 1.0/8.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute source factor 8
      vector.zero();
      vector.source(8.0, *p_element, field, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;
      compare[4] = 1.0;
      compare[5] = 1.0;
      compare[6] = 1.0;
      compare[7] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Get vector
      number_of_components = 3;
      auto vector_3 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Hexa8, number_of_components);

      // Set field
      auto field_3 = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector_3.source(1.0, *p_element, field_3, 0, number_of_components);

      // Reference solution
      compare.resize(24);
      compare.clear();
      compare[0] = 1.0/8.0;
      compare[1] = 1.0/8.0;
      compare[2] = 1.0/8.0;
      compare[3] = 1.0/8.0;
      compare[4] = 1.0/8.0;
      compare[5] = 1.0/8.0;
      compare[6] = 1.0/8.0;
      compare[7] = 1.0/8.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_3, compare);

      // Compute source factor 8
      vector_3.zero();
      vector_3.source(8.0, *p_element, field_3, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;
      compare[4] = 1.0;
      compare[5] = 1.0;
      compare[6] = 1.0;
      compare[7] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector_3, compare);
    }

    TEST_CASE("sourcePrism6")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism6);

      // Get vector
      std::size_t number_of_components = 1;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Prism6, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector.source(1.0, *p_element, field, 0, number_of_components);

      // Reference solution
      felisce::UBlasVector compare(6);
      compare[0] = 1.0/12.0;
      compare[1] = 1.0/12.0;
      compare[2] = 1.0/12.0;
      compare[3] = 1.0/12.0;
      compare[4] = 1.0/12.0;
      compare[5] = 1.0/12.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute source factor 12
      vector.zero();
      vector.source(12.0, *p_element, field, 0, number_of_components);

      // Reference solution
      compare[0] = 1.0;
      compare[1] = 1.0;
      compare[2] = 1.0;
      compare[3] = 1.0;
      compare[4] = 1.0;
      compare[5] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Get vector
      number_of_components = 3;
      auto vector_3 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Prism6, number_of_components);

      // Set field
      auto field_3 = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector_3.source(1.0, *p_element, field_3, 0, number_of_components);

      // Reference solution
      compare.resize(18);
      compare.clear();
      compare[0] = 1.0/12.0;
      compare[1] = 1.0/12.0;
      compare[2] = 1.0/12.0;
      compare[3] = 1.0/12.0;
      compare[4] = 1.0/12.0;
      compare[5] = 1.0/12.0;
      FELISCE_CHECK_VECTOR_EQUAL(vector_3, compare);
    }

    TEST_CASE("sourcePrism9")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism9);

      // Get vector
      std::size_t number_of_components = 1;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Prism9, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector.source(1.0, *p_element, field, 0, number_of_components);

      // Reference solution
      felisce::UBlasVector compare(9);
      compare[0] = 1.0/36.0;
      compare[1] = 1.0/36.0;
      compare[2] = 1.0/36.0;
      compare[3] = 1.0/36.0;
      compare[4] = 1.0/36.0;
      compare[5] = 1.0/36.0;
      compare[6] = 1.0/9.0;
      compare[7] = 1.0/9.0;
      compare[8] = 1.0/9.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Get vector
      number_of_components = 3;
      auto vector_3 = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Prism9, number_of_components);

      // Set field
      auto field_3 = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute source
      vector_3.source(1.0, *p_element, field_3, 0, number_of_components);

      // Reference solution
      compare.resize(27);
      compare.clear();
      compare[0] = 1.0/36.0;
      compare[1] = 1.0/36.0;
      compare[2] = 1.0/36.0;
      compare[3] = 1.0/36.0;
      compare[4] = 1.0/36.0;
      compare[5] = 1.0/36.0;
      compare[6] = 1.0/9.0;
      compare[7] = 1.0/9.0;
      compare[8] = 1.0/9.0;
      FELISCE_CHECK_VECTOR_EQUAL(vector_3, compare);
    }

    TEST_CASE("f_phi_i_scalar_nTria3")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurvilinearFiniteElement>(felisce::GeometricMeshRegion::Tria3);
      p_element->computeNormal();

      // Get vector
      const std::size_t number_of_components = 3;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Tria3, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute f_phi_i_scalar_n
      vector.f_phi_i_scalar_n(1.0, *p_element, field, 0);

      // Reference solution
      felisce::UBlasVector compare(9);
      compare.clear();
      compare[6] = 1.0/6.0;
      compare[7] = 1.0/6.0;
      compare[8] = 1.0/6.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute f_phi_i_scalar_n factor 6
      vector.zero();
      vector.f_phi_i_scalar_n(6.0, *p_element, field, 0);

      // Reference solution
      compare[6] = 1.0;
      compare[7] = 1.0;
      compare[8] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);
    }

    TEST_CASE("f_phi_i_scalar_nQuad4")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurvilinearFiniteElement>(felisce::GeometricMeshRegion::Quad4);
      p_element->computeNormal();

      // Get vector
      const std::size_t number_of_components = 3;
      auto vector = felisce::TestUtilities::GenerateElementVector(felisce::GeometricMeshRegion::Quad4, number_of_components);

      // Set field
      auto field = felisce::TestUtilities::GenerateElementField(*p_element, number_of_components, felisce::CONSTANT_FIELD);

      // Compute f_phi_i_scalar_n
      vector.f_phi_i_scalar_n(1.0, *p_element, field, 0);

      // Reference solution
      felisce::UBlasVector compare(12);
      compare.clear();
      compare[8] = 1.0/4.0;
      compare[9] = 1.0/4.0;
      compare[10] = 1.0/4.0;
      compare[11] = 1.0/4.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);

      // Compute f_phi_i_scalar_n factor 4
      vector.zero();
      vector.f_phi_i_scalar_n(4.0, *p_element, field, 0);

      // Reference solution
      compare[8] = 1.0;
      compare[9] = 1.0;
      compare[10] = 1.0;
      compare[11] = 1.0;

      FELISCE_CHECK_VECTOR_EQUAL(vector, compare);
    }
}

ADD_FELISCE_TEST
