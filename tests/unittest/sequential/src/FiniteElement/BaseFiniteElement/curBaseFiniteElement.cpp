//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//


// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Tools/test_utilities.hpp"

TEST_SUITE("TestcurBaseFiniteElement")
{
    TEST_CASE("measureSeg2")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Seg2, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0);
      }
    }

    // TEST_CASE("measureSeg23D")
    // {
    //   // Generate the element
    //   const std::array<double, 3> Delta = {0.0,0.0,1.0};
    //   std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
    //   for (auto& r_quad : quadratures) {
    //     auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Seg2, r_quad, Delta);
    //     FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0);
    //   }
    // }

    TEST_CASE("measureSeg2CurvilinearFiniteElement")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurvilinearFiniteElement>(felisce::GeometricMeshRegion::Seg2, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0);
      }
    }

    // TEST_CASE("measureSeg23DCurvilinearFiniteElement")
    // {
    //   // Generate the element
    //   const std::array<double, 3> Delta = {0.0,0.0,1.0};
    //   std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
    //   for (auto& r_quad : quadratures) {
    //     auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurvilinearFiniteElement>(felisce::GeometricMeshRegion::Seg2, r_quad, Delta);
    //     FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0);
    //   }
    // }

    TEST_CASE("measureTria3")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tria3, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0/2.0);
      }
    }

    TEST_CASE("measureQuad4")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Quad4, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0);
      }
    }

    TEST_CASE("measureTetra4")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tetra4, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0/6.0);
      }
    }

    TEST_CASE("measureHexa8")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Hexa8, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0);
      }

      std::vector<felisce::DegreeOfExactness> quadratures_combined = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1};
      for (auto& r_quad_1 : quadratures) {
        quadratures_combined[1] = r_quad_1;
        for (auto& r_quad_2 : quadratures) {
          quadratures_combined[2] = r_quad_2;
          auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Hexa8, quadratures_combined);
          FELISCE_CHECK_NEAR(p_element->measure(), 1.0, 1.0e-6);
        }
      }
    }

    TEST_CASE("measurePrism6")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism6, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0/2.0);
      }

      std::vector<felisce::DegreeOfExactness> quadratures_combined = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1};
      for (auto& r_quad_1 : quadratures) {
        quadratures_combined[1] = r_quad_1;
        for (auto& r_quad_2 : quadratures) {
          quadratures_combined[2] = r_quad_2;
          auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism6, quadratures_combined);
          FELISCE_CHECK_NEAR(p_element->measure(), 1.0/2.0, 1.0e-6);
        }
      }
    }

    TEST_CASE("measurePrism9")
    {
      // Generate the element
      std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
      for (auto& r_quad : quadratures) {
        auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism9, r_quad);
        FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0/2.0);
      }

      std::vector<felisce::DegreeOfExactness> quadratures_combined = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1};
      for (auto& r_quad_1 : quadratures) {
        quadratures_combined[1] = r_quad_1;
        for (auto& r_quad_2 : quadratures) {
          quadratures_combined[2] = r_quad_2;
          auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism9, quadratures_combined);
          FELISCE_CHECK_NEAR(p_element->measure(), 1.0/2.0, 1.0e-6);
        }
      }
    }

    // TEST_CASE("measurePrism15")
    // {
    //   // Generate the element
    //   std::vector<felisce::DegreeOfExactness> quadratures = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_2, felisce::DegreeOfExactness::DegreeOfExactness_3, felisce::DegreeOfExactness::DegreeOfExactness_4, felisce::DegreeOfExactness::DegreeOfExactness_5};
    //   for (auto& r_quad : quadratures) {
    //     auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism15, r_quad);
    //     FELISCE_CHECK_DOUBLE_EQUAL(p_element->measure(), 1.0/2.0);
    //   }

    //   std::vector<felisce::DegreeOfExactness> quadratures_combined = {felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1, felisce::DegreeOfExactness::DegreeOfExactness_1};
    //   for (auto& r_quad_1 : quadratures) {
    //     quadratures_combined[1] = r_quad_1;
    //     for (auto& r_quad_2 : quadratures) {
    //       quadratures_combined[2] = r_quad_2;
    //       auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism15, quadratures_combined);
    //       FELISCE_CHECK_NEAR(p_element->measure(), 1.0/2.0, 1.0e-6);
    //     }
    //   }
    // }
}

ADD_FELISCE_TEST
