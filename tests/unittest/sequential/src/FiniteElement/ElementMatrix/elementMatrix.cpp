//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Tools/test_utilities.hpp"

TEST_SUITE("TestelementMatrix")
{
    TEST_CASE("Access")
    {
      auto matrix_3 = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);

      FELISCE_CHECK_EQUAL(matrix_3.size1(), 3);
      FELISCE_CHECK_EQUAL(matrix_3.size2(), 3);
      FELISCE_CHECK_EQUAL(matrix_3.numBlockRow(), 1);
      FELISCE_CHECK_EQUAL(matrix_3.numBlockCol(), 1);

      auto matrix_4 = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Quad4, 1);

      FELISCE_CHECK_EQUAL(matrix_4.size1(), 4);
      FELISCE_CHECK_EQUAL(matrix_4.size2(), 4);
      FELISCE_CHECK_EQUAL(matrix_4.numBlockRow(), 1);
      FELISCE_CHECK_EQUAL(matrix_4.numBlockCol(), 1);

      auto matrix_3_2 = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 2);

      FELISCE_CHECK_EQUAL(matrix_3_2.size1(), 6);
      FELISCE_CHECK_EQUAL(matrix_3_2.size2(), 6);
      FELISCE_CHECK_EQUAL(matrix_3_2.numBlockRow(), 2);
      FELISCE_CHECK_EQUAL(matrix_3_2.numBlockCol(), 2);

      auto matrix_4_3 = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tetra4, 3);

      FELISCE_CHECK_EQUAL(matrix_4_3.size1(), 12);
      FELISCE_CHECK_EQUAL(matrix_4_3.size2(), 12);
      FELISCE_CHECK_EQUAL(matrix_4_3.numBlockRow(), 3);
      FELISCE_CHECK_EQUAL(matrix_4_3.numBlockCol(), 3);
    }

    TEST_CASE("zero")
    {
      auto matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);

      felisce::UBlasMatrix compare(3, 3);
      compare.clear();

      matrix.zero();
      FELISCE_CHECK_MATRIX_EQUAL(matrix, compare);
    }

    TEST_CASE("operator*")
    {
      auto matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);

      felisce::UBlasMatrix compare(3, 3);
      compare.clear();
      compare(0,0) = 12.0;

      matrix.zero();
      matrix.mat()(0,0) = 1.0;
      matrix *= 12.0;
      FELISCE_CHECK_MATRIX_EQUAL(matrix, compare);
    }

    TEST_CASE("operator+")
    {
      auto matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);

      auto compare = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);
      compare.zero();
      compare(0,0) = 12.0;

      matrix.zero();
      matrix += compare;
      FELISCE_CHECK_MATRIX_EQUAL(matrix, compare);
    }

    TEST_CASE("operator-")
    {
      auto matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);

      auto compare = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, 1);
      compare.zero();
      compare(0,0) = 12.0;

      matrix.zero();
      matrix -= compare;
      compare(0,0) = -12.0;
      FELISCE_CHECK_MATRIX_EQUAL(matrix, compare);
    }

    TEST_CASE("massMatrixTria3")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tria3);

      // Generate matrix
      const std::size_t dimension = 2;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        for (std::size_t j = 0; j < mass_matrix.size2(); ++j)
          total_mass += mass_matrix(i, j);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixQuad4")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Quad4);

      // Generate matrix
      const std::size_t dimension = 2;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Quad4, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        for (std::size_t j = 0; j < mass_matrix.size2(); ++j)
          total_mass += mass_matrix(i, j);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixTetra4")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tetra4);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tetra4, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        for (std::size_t j = 0; j < mass_matrix.size2(); ++j)
          total_mass += mass_matrix(i, j);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixHexa8")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Hexa8);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Hexa8, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        for (std::size_t j = 0; j < mass_matrix.size2(); ++j)
          total_mass += mass_matrix(i, j);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixPrism6")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism6);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Prism6, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        for (std::size_t j = 0; j < mass_matrix.size2(); ++j)
          total_mass += mass_matrix(i, j);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixPrism9")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism9);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Prism9, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        for (std::size_t j = 0; j < mass_matrix.size2(); ++j)
          total_mass += mass_matrix(i, j);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixTria3Lumped")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tria3);

      // Generate matrix
      const std::size_t dimension = 2;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tria3, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j_lumped(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        total_mass += mass_matrix(i, i);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixQuad4Lumped")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Quad4);

      // Generate matrix
      const std::size_t dimension = 2;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Quad4, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j_lumped(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        total_mass += mass_matrix(i, i);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixTetra4Lumped")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Tetra4);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Tetra4, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j_lumped(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        total_mass += mass_matrix(i, i);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixHexa8Lumped")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Hexa8);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Hexa8, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j_lumped(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        total_mass += mass_matrix(i, i);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixPrism6Lumped")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism6);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Prism6, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j_lumped(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        total_mass += mass_matrix(i, i);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }

    TEST_CASE("massMatrixPrism9Lumped")
    {
      // Generate the element
      auto p_element = felisce::TestUtilities::GenerateFiniteElement<felisce::CurrentFiniteElement>(felisce::GeometricMeshRegion::Prism9);

      // Generate matrix
      const std::size_t dimension = 3;
      auto mass_matrix = felisce::TestUtilities::GenerateElementMatrix(felisce::GeometricMeshRegion::Prism9, dimension);

      // Compute mass matrix
      mass_matrix.phi_i_phi_j_lumped(1.0, *p_element, 0, 0, dimension);

      double total_mass = 0.0;
      for (std::size_t i = 0; i < mass_matrix.size1(); ++i)
        total_mass += mass_matrix(i, i);

      FELISCE_CHECK_LESS_EQUAL(total_mass - dimension * p_element->measure(), 1.0e-3);
    }
}

ADD_FELISCE_TEST
