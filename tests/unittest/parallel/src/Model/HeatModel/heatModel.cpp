//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/filesystemUtil.hpp"
#include "Model/heatModel.hpp"
#include "Solver/linearProblemHeat.hpp"

TEST_SUITE("heatModel")
{
  TEST_CASE("multiple_input_files")
  {
    // Read command line and data file
    std::string current_path = std::filesystem::current_path();
    current_path += "/";
    int argc = 5;
    const std::string test_bin_name = current_path + "MPITest_Src_Model_HeatModel";
    const std::string data_file_1 = current_path + "data_1";
    const std::string data_file_2 = current_path + "data_2";
    const std::string data_file_3 = current_path + "data_3";
    const char* argv[] = {test_bin_name.c_str(), "-f", data_file_1.c_str(), data_file_2.c_str(), data_file_3.c_str()};

    felisce::FelisceTransient fstransient;

    const std::size_t instance_0 = 0;
    felisce::CommandLineOption opt0(argc, argv, instance_0);
    felisce::FelisceParam::instance(instance_0).initialize(opt0);

    const std::size_t instance_1 = 1;
    felisce::CommandLineOption opt1(argc, argv, instance_1);
    felisce::FelisceParam::instance(instance_1).initialize(opt1);

    const std::size_t instance_2 = 2;
    felisce::CommandLineOption opt2(argc, argv, instance_2);
    felisce::FelisceParam::instance(instance_2).initialize(opt2);

    // NOTE: A model manager should be created to avoid the requirement of telling if initilize or finalize is required

    // Create model
    felisce::HeatModel model0;
    model0.initializeModel(opt0, fstransient, MPI_COMM_WORLD, true);
    model0.initializeLinearProblem(new felisce::LinearProblemHeat());

    felisce::HeatModel model1;
    model1.initializeModel(opt1, fstransient, MPI_COMM_WORLD, false);
    model1.initializeLinearProblem(new felisce::LinearProblemHeat());

    felisce::HeatModel model2;
    model2.initializeModel(opt2,fstransient, MPI_COMM_WORLD, false);
    model2.initializeLinearProblem(new felisce::LinearProblemHeat());

    // NOTE: Just not crashing makes the test pass
    FELISCE_CHECK(true);
  }
}

ADD_FELISCE_TEST
