# Install test UnitTest_Par_Src_Model_HeatModel

felisce_install_test_exe(
    NAME UnitTest_Par_Src_Model_HeatModel
    SOURCES heatModel.cpp
    INSTALL unittest/parallel/src/Model/HeatModel
)

felisce_install_tests_data(
    NAME UnitTest_Par_Src_Model_HeatModel
    EXEC UnitTest_Par_Src_Model_HeatModel
    INSTALL unittest/parallel/src/Model/HeatModel
    CONFIG data_1 data_2 data_3
    FILES 
    DIRS 
)

felisce_add_test(
    NAME UnitTest_Par_Src_Model_HeatModel
    INSTALL unittest/parallel/src/Model/HeatModel
)
