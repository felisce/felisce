//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/filesystemUtil.hpp"
#include "Model/heatModel.hpp"
#include "Solver/linearProblemHeat.hpp"

class InitialConditionFunctor {
public:
  double operator() (double x,double y,double z) const {
    IGNORE_UNUSED_ARGUMENT(y);
    IGNORE_UNUSED_ARGUMENT(z);
    if (x <= 0.5) {
      return 2.*x;
    } else {
      return 2. - 2.*x;
    }

  }
};

TEST_SUITE("linearProblemHeat")
{
  TEST_CASE("set_U_0_parallel")
  {
    // Read command line and data file
    std::string current_path = std::filesystem::current_path();
    current_path += "/";
    int argc = 1;
    const std::string test_bin_name = current_path + "MPITest_Src_Solver_LinearProblemHeat";
    const char* argv[] = {test_bin_name.c_str()};
    felisce::CommandLineOption opt(argc, argv);
    const std::string data_file = current_path + "data";
    opt.dataFileName() = data_file;
    felisce::FelisceParam::instance().initialize(opt);
    felisce::FelisceTransient fstransient;

    // Create model and solver
    felisce::HeatModel model;
    model.initializeModel(opt,fstransient);
    auto p_linear_problem = new felisce::LinearProblemHeat();
    model.initializeLinearProblem(p_linear_problem);

    // Set initial condition
    InitialConditionFunctor initial_condition;
    model.linearProblem()->set_U_0_parallel(initial_condition,0,0);

    // NOTE: Solve is not really needed, we are checking the initialization of values
    // Time loop 
    model.SolveDynamicProblem();

    // Retrieve the solution in the current time step
    std::vector<double> valueVec(p_linear_problem->numDof());
    p_linear_problem->sequentialSolution().getAllValuesInAppOrdering(p_linear_problem->ao(), valueVec);

    // Check values
    const auto points = model.mesh()->listPoints();
    const std::size_t number_of_points = points.size();
    for (std::size_t i_node = 0; i_node < number_of_points; ++i_node) {
      FELISCE_CHECK_DOUBLE_EQUAL(valueVec[i_node], initial_condition(points[i_node].x(),0.0,0.0));
    }
  }
}

ADD_FELISCE_TEST
