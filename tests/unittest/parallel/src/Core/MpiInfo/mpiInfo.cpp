//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>
#include <iterator>
#include <cstring>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Core/felisceParam.hpp"
#include "Core/mpiInfo.hpp"

TEST_SUITE("TestmpiInfo")
{
    TEST_CASE("numProc")
    {
        // Set communicator
        felisce::MpiInfo::petscComm() = MPI_COMM_WORLD;
        MPI_Comm_size(felisce::MpiInfo::petscComm(), &felisce::MpiInfo::numProc());

        // Get the number of processes
        int world_size;
        MPI_Comm_size(MPI_COMM_WORLD, &world_size);

        FELISCE_CHECK_EQUAL(felisce::MpiInfo::numProc(), world_size);
    }

    TEST_CASE("rankProc")
    {
        // Set communicator
        felisce::MpiInfo::petscComm() = MPI_COMM_WORLD;
        MPI_Comm_rank(felisce::MpiInfo::petscComm(), &felisce::MpiInfo::rankProc());

        // Get the rank of the process
        int world_rank;
        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

        FELISCE_CHECK_EQUAL(felisce::MpiInfo::rankProc(), world_rank);
    }
}

ADD_FELISCE_TEST_MPI
