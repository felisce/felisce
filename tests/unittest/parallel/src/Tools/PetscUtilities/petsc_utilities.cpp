//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>

// External includes
#include "doctest_checks.h"

// Project includes
#include "Tools/petsc_utilities.hpp"
#include "Core/filesystemUtil.hpp"

TEST_SUITE("PETScUtilities")
{
    TEST_CASE("SparsePlot")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Get current folder
      std::string current_path = std::filesystem::current_path();
      current_path += "/";
      const std::string files_folder = current_path + "Resources/";

      // Constructor and create
      felisce::PetscMatrix mat;

      // Memory allocation
      mat.createSeqAIJ(MPI_COMM_SELF,4225,4225,0,nullptr);
      mat.setOption(MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
      mat.setFromOptions();

      // Read matrix
      mat.loadFromBinaryFormat(PETSC_COMM_WORLD, "matrix", files_folder);

      // Drawing the sparcity plot
      const std::string bmp_file = files_folder + "spy.bmp";
      felisce::PETScUtilities::SparsePlot(mat, bmp_file);
      FELISCE_CHECK(felisce::filesystemUtil::fileExists(bmp_file));
    }
}

ADD_FELISCE_TEST_PETSC