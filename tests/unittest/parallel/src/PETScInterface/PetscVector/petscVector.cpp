//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes
#include <iostream>

// External includes
#include "doctest_checks.h"

// Project includes
#include "PETScInterface/petscVector.hpp"

TEST_SUITE("petscVector")
{
    TEST_CASE("constructorandotherbasicmethods")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      FELISCE_CHECK(vec.isNull());
      FELISCE_CHECK_IS_FALSE(vec.isNotNull());
      vec.createMPIInstance(number_cores);
      FELISCE_CHECK_EQUAL(vec.getSize(), number_cores);
      FELISCE_CHECK_EQUAL(vec.getLocalSize(), 1);

      // Set to zero all
      vec.zeroEntries();
      FELISCE_CHECK_IS_FALSE(vec.isNull());
      FELISCE_CHECK(vec.isNotNull());
      auto data = vec.getArray();
      double aux_value;
      PetscInt indices[number_cores];
      for (int i = 0; i < number_cores; ++i) {
        indices[i] = i;
      }
      PetscScalar values[number_cores];
      //vec.getValues(number_cores, indices, values); // With VecGetValues : Gets values from certain locations of a vector. Currently can only get values on the same processor on which they are owned
      vec.getValue(rank, aux_value);
      FELISCE_CHECK_DOUBLE_EQUAL(aux_value, 0.0);
      FELISCE_CHECK_DOUBLE_EQUAL(data[0], 0.0);
      //FELISCE_CHECK_DOUBLE_EQUAL(values[rank], 0.0);

      // Set value
      vec.setValue(rank, static_cast<PetscScalar>(rank), INSERT_VALUES);
      vec.assembly();

      vec.getValue(rank, aux_value);
      FELISCE_CHECK_DOUBLE_EQUAL(aux_value, static_cast<PetscScalar>(rank));

      // Set value once
      felisce::PetscVector vec_2;
      vec_2.createMPIInstance(number_cores);
      vec_2.zeroEntries();
      vec_2.setValue(0, 1.0, ADD_VALUES);
      vec_2.assembly();
      if (rank == 0) {
        FELISCE_CHECK_DOUBLE_EQUAL(vec_2[rank], static_cast<PetscScalar>(number_cores));
      }
      else{
        FELISCE_CHECK_DOUBLE_EQUAL(vec_2[rank], 0.0);
      }
      vec_2.zeroEntries();
      vec_2.setValueOnce(0, 1.0, ADD_VALUES);
      vec_2.assembly();
      if (rank == 0) {
        FELISCE_CHECK_DOUBLE_EQUAL(vec_2[0], 1.0);
      }

      // Set values
      for (int i = 0; i < number_cores; ++i) {
        values[i] = 0.0;
      }
      values[rank] = static_cast<PetscScalar>(rank);
      vec.setValues(number_cores, indices, values, ADD_VALUES);
      vec.assembly();

      vec.getValue(rank, aux_value);
      FELISCE_CHECK_DOUBLE_EQUAL(aux_value, static_cast<PetscScalar>(2 * rank));

      // Set values once
      vec_2.zeroEntries();
      for (int i = 0; i < number_cores; ++i) values[i] = 1.0;
      vec_2.setValues(number_cores, indices, values, ADD_VALUES);
      vec_2.assembly();

      vec_2.getValue(rank, aux_value);
      FELISCE_CHECK_DOUBLE_EQUAL(aux_value, static_cast<PetscScalar>(number_cores));

      vec_2.zeroEntries();
      vec_2.setValuesOnce(number_cores, indices, values, ADD_VALUES);
      vec_2.assembly();

      vec_2.getValue(rank, aux_value);
      FELISCE_CHECK_DOUBLE_EQUAL(aux_value, 1.0);
    }

    TEST_CASE("setSizes")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Constructor and create
      felisce::PetscVector vec;
      vec.create(PETSC_COMM_WORLD);
      vec.setSizes(2 * number_cores);
      vec.setFromOptions();
      FELISCE_CHECK_EQUAL(vec.getSize(), 2 * number_cores); 
    }

    TEST_CASE("destroy")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      FELISCE_CHECK(vec.isNotNull());
      vec.destroy();
      FELISCE_CHECK(vec.isNull());
    }

    // NOTE: Operator [] is tested in the following tests as well

    TEST_CASE("operator+")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_a.shift(1.5);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.0);
      
      const auto vec_c = vec_a + vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(vec_c[rank], 2.5);

      const auto vec_d = vec_a + 2.0 * vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(vec_d[rank], 3.5);

      // Constant vectors
      const felisce::PetscVector c_vec_a(vec_a);
      const felisce::PetscVector c_vec_b(vec_b);

      const auto c_vec_c = c_vec_a + c_vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(c_vec_c[rank], 2.5);
    }

    TEST_CASE("operator+=")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_a.shift(1.5);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.0);
      vec_a += vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(vec_a[rank], 2.5);
    }

    TEST_CASE("operator-")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_a.shift(1.5);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.0);
      const auto vec_c = vec_a - vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(vec_c[rank], 0.5);

      // Constant vectors
      const felisce::PetscVector c_vec_a(vec_a);
      const felisce::PetscVector c_vec_b(vec_b);

      const auto c_vec_c = c_vec_a - c_vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(c_vec_c[rank], 0.5);
    }

    TEST_CASE("operator-=")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_a.shift(1.5);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.0);
      vec_a -= vec_b;
      FELISCE_CHECK_DOUBLE_EQUAL(vec_a[rank], 0.5);
    }

    TEST_CASE("operator*")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      felisce::PetscVector result = vec * 5;
      FELISCE_CHECK_DOUBLE_EQUAL(result[rank], 5.0);
      result = 5 * vec;
      FELISCE_CHECK_DOUBLE_EQUAL(result[rank], 5.0);
      result = vec * 5.0;
      FELISCE_CHECK_DOUBLE_EQUAL(result[rank], 5.0);
      result = 5.0 * vec;
      FELISCE_CHECK_DOUBLE_EQUAL(result[rank], 5.0);

      // Constant vector
      const felisce::PetscVector c_vec(vec);
      felisce::PetscVector c_result = c_vec * 5;
      FELISCE_CHECK_DOUBLE_EQUAL(c_result[rank], 5.0);
      c_result = 5 * c_vec;
      FELISCE_CHECK_DOUBLE_EQUAL(c_result[rank], 5.0);
      c_result = 5.0 * c_vec;
      FELISCE_CHECK_DOUBLE_EQUAL(c_result[rank], 5.0);
      c_result = c_vec * 5.0;
      FELISCE_CHECK_DOUBLE_EQUAL(c_result[rank], 5.0);
    }

    TEST_CASE("dotproduct")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_b.createMPIInstance(number_cores);
      vec_a.shift(1.0);
      vec_b.shift(2.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec_a * vec_b, static_cast<double>(2 * number_cores));

      // Constant vectors
      const felisce::PetscVector c_vec_a(vec_a);
      const felisce::PetscVector c_vec_b(vec_b);
      FELISCE_CHECK_DOUBLE_EQUAL(c_vec_a * c_vec_b, static_cast<double>(2 * number_cores));
    }

    TEST_CASE("operator*=")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      vec *= 5.0;
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 5.0);
      vec *= 5;
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 25.0);
    }

    TEST_CASE("operator/")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      felisce::PetscVector result = vec / 2;
      FELISCE_CHECK_DOUBLE_EQUAL(result[rank], 0.5);
      result = vec / 2.0;
      FELISCE_CHECK_DOUBLE_EQUAL(result[rank], 0.5);

      // Constant vector
      const felisce::PetscVector c_vec(vec);
      felisce::PetscVector c_result = c_vec / 2;
      FELISCE_CHECK_DOUBLE_EQUAL(c_result[rank], 0.5);
      c_result = c_vec / 2.0;
      FELISCE_CHECK_DOUBLE_EQUAL(c_result[rank], 0.5);
    }

    TEST_CASE("operator/=")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      vec /= 2;
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 0.5);
      vec /= 2.0;
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 0.25);
    }

    TEST_CASE("copyFrom")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.5);
      vec_a.copyFrom(vec_b);
      FELISCE_CHECK_DOUBLE_EQUAL(vec_a[rank], 1.5);
    }

    TEST_CASE("duplicateFrom")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.5);
      vec_a.duplicateFrom(vec_b);
      FELISCE_CHECK_EQUAL(vec_a.getSize(), number_cores); 
    }

    TEST_CASE("axpby")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_a.shift(1.5);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.0);
      vec_a.axpby(vec_b, 0.5, 2.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec_a[rank], 3.5);
    }

    TEST_CASE("axpy")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec_a, vec_b;
      vec_a.createMPIInstance(number_cores);
      vec_a.shift(1.5);
      vec_b.createMPIInstance(number_cores);
      vec_b.shift(1.0);
      vec_a.axpy(vec_b, 2.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec_a[rank], 3.5);
    }

    TEST_CASE("shift")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 1.0);
    }

    TEST_CASE("abs")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(-1.0);
      vec.abs();
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 1.0);
    }

    TEST_CASE("sum")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec.sum(), static_cast<double>(number_cores));
    }

    TEST_CASE("norm")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec.norm(), std::sqrt(static_cast<double>(number_cores)));
    }

    TEST_CASE("scale")
    {
      // In order to avoid issues of size in the checks, the minimal size must be the number of cores of the machine (a priori this is not an issue)
      int number_cores;
      MPI_Comm_size(PETSC_COMM_WORLD, &number_cores);

      // Get rank
      int rank;
      MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

      // Constructor and create
      felisce::PetscVector vec;
      vec.createMPIInstance(number_cores);
      vec.shift(1.0);
      vec.scale(5.0);
      FELISCE_CHECK_DOUBLE_EQUAL(vec[rank], 5.0);
    }
}

ADD_FELISCE_TEST_PETSC