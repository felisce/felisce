//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:		 LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    Vicente Mataix Ferrandiz
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes
#include "doctest_checks.h"

// Project includes
#include "Tools/ksp_solver.hpp"
#include "Tools/test_utilities.hpp"

// Run the test suite with PETSc initialized
TEST_SUITE("TestKSPPETSc")
{

    const std::string matrices_folder = "Resources/";

    // Use the fixture for all tests
    TEST_CASE("BCGS") 
    {
        const std::string filename_mat = matrices_folder+ "arco1_mat";
        const std::string filename_vec = matrices_folder+ "arco1_vec";
        std::cout << "filename_mat = " << filename_mat << std::endl;
        std::cout << "filename_vec = " << filename_vec << std::endl;
        const int check = SolveMatrix(filename_mat, filename_vec, "bcgs");
        FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("BCGSasm")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "bcgs", "asm");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("BCGScholesky")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "bcgs", "cholesky");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("BCGSjacobi")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "bcgs", "jacobi");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("BCGSlu")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "bcgs", "lu");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("CG")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "cg");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("CGasm")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "cg", "asm");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("CGcholesky")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "cg", "cholesky");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("CGjacobi")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "cg", "jacobi");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("CGlu")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "cg", "lu");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("GMRES")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "gmres");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("GMRESasm")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "gmres", "asm");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("GMREScholesky")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "gmres", "cholesky");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("GMRESjacobi")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "gmres", "jacobi");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("GMRESlu")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "gmres", "lu");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("Hypreeuclid"
      #ifndef FELISCE_WITH_HYPRE
        * doctest::skip(true)
      #endif
    )
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "hypre", "euclid");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("Hypreparasails"
      #ifndef FELISCE_WITH_HYPRE
        * doctest::skip(true)
      #endif
    )
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "hypre", "parasails");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("MUMPS")
    {
      const std::string filename_mat = matrices_folder + "arco1_mat";
      const std::string filename_vec = matrices_folder + "arco1_vec";
      const int check = SolveMatrix(filename_mat, filename_vec, "MUMPS", "");
      FELISCE_CHECK_EQUAL(check, 0);
    }

    TEST_CASE("SUPERLU_DIST"
      #ifndef FELISCE_WITH_SUPERLU
        * doctest::skip(true)
      #endif
    )
    {
        const std::string filename_mat = matrices_folder + "arco1_mat";
        const std::string filename_vec = matrices_folder + "arco1_vec";
        const int check = SolveMatrix(filename_mat, filename_vec, "SUPERLU_DIST", "");
        FELISCE_CHECK_EQUAL(check, 0);
    }
}

ADD_FELISCE_TEST_PETSC