#include <iostream>
#include <string>
#include <fstream>
 
// using namespace 
// std;
 
int main()
{
 
  cout << " Number of points " << endl;
  int nbp;
  cin >> nbp;
  int nbs = nbp -1;
  double h = 1./nbs; 

  double x0 = 2;
  double y0 = 0;
  FILE * pFile;
  string fileName = "solid.geo";
  pFile = fopen (fileName.c_str(),"w");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"node id assign\n");
  fprintf(pFile,"element id assign\n");
  fprintf(pFile,"coordinates\n");
  fprintf(pFile,"%8d\n",nbp);
  double z = 0.;
  for(int iv=0; iv< nbp; iv++) 
    fprintf( pFile, "%12.5e%12.5e%12.5e\n", x0, y0+iv*h, z);
  fprintf (pFile, "part%8d\n",1);
  fprintf (pFile, "domain\n" );
  fprintf( pFile, "bar2\n");
  fprintf(pFile,"%8d\n", nbs);
  for (int iel = 0; iel < nbs; iel++) 
    fprintf( pFile, "%8d%8d\n",iel+1,iel+2);
  fprintf (pFile, "part%8d\n",2);
  fprintf (pFile, "bottom\n" );
  fprintf( pFile, "point\n");
  fprintf(pFile,"%8d\n",1);
  fprintf(pFile,"%8d\n",1);
  fprintf (pFile, "part%8d\n",3);
  fprintf (pFile, "top\n" );
  fprintf( pFile, "point\n");
  fprintf(pFile,"%8d\n",1);
  fprintf(pFile,"%8d\n",nbp);
  fclose(pFile);
  
  fileName = "solid.mesh";
  pFile = fopen (fileName.c_str(),"w");
  fprintf(pFile,"MeshVersionFormatted 2\n");
  fprintf(pFile,"Dimension\n2\n");
  fprintf(pFile,"Vertices\n");
  fprintf(pFile,"%8d\n",nbp);
  for(int iv=0; iv< nbp; iv++) 
    fprintf( pFile, "%12.5e%12.5e%8d\n", x0, y0+iv*h, 0);
  fprintf (pFile, "Edges\n" );
  fprintf(pFile,"%8d\n", nbs);
  for (int iel = 0; iel < nbs; iel++) 
    fprintf( pFile, "%8d%8d%8d\n",iel+1,iel+2,1);
  fprintf (pFile, "End\n" );
  fclose(pFile);


  return 0;
}
