#include <string>
#include <iostream>
#include <sstream>
#include <list>
#include <set>
#include <map>
#include <vector>
#include <math.h>   


// using namespace 
// std;

int main(int argc, char ** argv)
{
  double x0 = 0.6;
  double y0 = 0.5;
  int n;
  cout << " number of segements ? " << endl;
  cin >> n;
     

  double R=0.2;
  double ds=2*acos(-1.)*R/n;
  string filename = "solid.mesh";
  FILE * pFile = fopen (filename.c_str(),"w+");
  fprintf(pFile,"MeshVersionFormatted 1\n");  
  fprintf(pFile,"Dimension\n");
  fprintf(pFile,"2\n");
  fprintf(pFile,"Vertices\n");
  fprintf(pFile,"%8d\n",n);
  for (int iv=0; iv< n; iv++) {
    double x = iv*ds;
    double xx = x0+R*cos(x/R);
    double yy = y0+R*sin(x/R);
    fprintf( pFile, "%12.5e%12.5e%8d\n", xx, yy,0);
  }
  string keyword = "Edges";
  fprintf( pFile, "%s\n", keyword.c_str() );
  fprintf(pFile,"%8d\n", n);
  for (int iv=0; iv< n-1; iv++)
    fprintf( pFile, "%8d%8d%8d\n",iv+1,iv+2,0);
  fprintf( pFile, "%8d%8d%8d\n",n,1,0);
  fprintf(pFile,"End"); 
  fclose(pFile);
}
