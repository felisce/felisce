//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Miguel Fernandez
//

// System includes
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <math.h>

// External includes

// Project includes

int main()
{
  std::cout << " File name? (without .geo)" << std::endl;
  std::string fileName ;
  std::cin >> fileName;
  fileName += ".geo" ;

  FILE* pFile = fopen (fileName.c_str(),"w");
 
  std::cout << " Number of elements?  " << std::endl;
  int nE;
  std::cin >> nE;
  int nP = nE+1;

  // Descriptions lines (All Geometry files must contain these first six lines)
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"Geometry file\n");
  fprintf(pFile,"node id assign\n");
  fprintf(pFile,"element id assign\n");
  fprintf(pFile,"coordinates\n");
  fprintf(pFile,"%8d\n",nP);

  // write the vertices
  double zc = 0.;
  double xc = 0.;
  double yc = 0.5;
  double h = 6./nE;
  for(int i=0; i< nP; i++) {
    xc = i*h; 
    fprintf( pFile, "%12.5e%12.5e%12.5e\n", xc, yc, zc);
  }
  //write the elements per references.
  fprintf ( pFile, "part%8d\n",1);
  std::string aux = "domain";
  fprintf (pFile, "%s\n", aux.c_str() );
  aux = "bar2";
  fprintf (pFile, "%s\n", aux.c_str() );
  fprintf(pFile,"%8d\n", nE);
  for (int i = 0; i < nE; i++) 
    fprintf( pFile, "%8d%8d\n",i+1,i+2);
  fclose(pFile);
}
