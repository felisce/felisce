//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Miguel Fernandez
//

#ifndef _UserNSALE_HPP
#define _UserNSALE_HPP

// System includes
#include <math.h>

// External includes

// Project includes
#include "Solver/linearProblemNS.hpp"
#include "Core/felisceParam.hpp"

namespace felisce
{

  class inPressure
  {
  public:
    double operator() (double t) const;
  };


  class velCavity
  {
  public:
    double operator() (double t) const;
  };

  class velCavityZero
  {
  public:
    double operator() (double t) const;
  };

  class UserNSALE:
    public LinearProblemNS
  {
  public:
    UserNSALE();
    ~UserNSALE();

    void userElementInit() override;

    void userFinalizeEssBCTransient() override;

    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label) override;

    void userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint,int& iel, FlagMatrixRHS flagMatrixRHS = FlagMatrixRHS::matrix_and_rhs) override;

    double evaluateMean(PetscVector &vec);

  protected:

    ElementField m_elemFieldInterfaceVel;
    ElementField m_elemFieldPreviousVel;

    std::vector<double> m_Pt;
    inPressure m_inPress;
    velCavity m_velCavity;
    velCavityZero m_velCavityZero;
  };

}

#endif
