# Install test FSITest_NS_ALE_zmq executable

if(NOT FELISCE_WITH_ZMQ)
    return()
endif()

felisce_install_test_exe(
    NAME NS_ALE_zmq
    SOURCES main.cpp userNSALE.cpp userHarmonicExtension.cpp
    INSTALL fsi/fluid/NS_ALE_zmq
)