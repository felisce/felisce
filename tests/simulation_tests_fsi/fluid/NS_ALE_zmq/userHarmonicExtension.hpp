//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

#ifndef _UserHarmonicExtension_HPP
#define _UserHarmonicExtension_HPP

// System includes

// External includes

// Project includes
#include "Solver/linearProblemHarmonicExtension.hpp"

namespace felisce
{
  class UserHarmonicExtension:
    public LinearProblemHarmonicExtension
  {
  public:
    UserHarmonicExtension();
    ~UserHarmonicExtension();
    void userFinalizeEssBCTransient() override;
  };

}

#endif
