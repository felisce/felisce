//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// Project-specific includes
#include "Core/configure.hpp"

// External includes

// Project includes
#include "userNSALE.hpp"
#include "userHarmonicExtension.hpp"
#include "Core/felisceTransient.hpp"
#include "Core/felisceParam.hpp"
#include "Core/fluidToMaster.hpp"
#include "Core/felisceTools.hpp"
#include "Model/NSModel.hpp"

#ifdef FELISCE_WITH_ZMQ

using namespace felisce;

int main(const int argc, const char ** argv)
{

  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;

  // copy the data file in the Post-processing folder
  felisce::filesystemUtil::copyFileSomewhereElse(opt.dataFileName(), FelisceParam::instance().resultDir + "/dataFile");
  // create model and solver
  NSModel model;
  model.initializeModel(opt,fstransient);
  std::vector<LinearProblem*> linearPb;
  linearPb.push_back(new UserNSALE());
  if ( FelisceParam::instance().useALEformulation )
    linearPb.push_back(new UserHarmonicExtension());

  model.initializeLinearProblem(linearPb);

  std::vector<int> nodesSigma;
  std::vector<double> disp_master, disp, dispo;
  std::vector<double> velo_master, vel, velo, veloo;
  std::vector<double> lumpM_master;
  std::vector<double> residual, res, reso;
  std::vector<double> veloF;

  int nbCoor = model.linearProblem(0)->dimension();

#if defined(FELISCE_WITH_PVM)
  FluidToMaster fluidToMaster(-1, nbCoor, nodesSigma, disp_master, velo_master,
                              lumpM_master, residual, veloF);
#endif
#if defined(FELISCE_WITH_ZMQ)
  FluidToMaster fluidToMaster(-1, nbCoor, nodesSigma, disp_master, velo_master,
                              lumpM_master, residual, veloF,FelisceParam::instance().socketTransport,
                              FelisceParam::instance().socketAddress,FelisceParam::instance().socketPort);
#endif
  int rank, nbinter, info, status, hasLumpedMass;
  MPI_Comm_rank(MpiInfo::petscComm(), &rank);
  if (rank == 0)  {
    fluidToMaster.setMaster();
    fluidToMaster.rvFromMasterFSI(140);
    fluidToMaster.sdInterfCoorToMasterFSI();
    //fluidToMaster.rvFromMasterFSI(99);
    hasLumpedMass = fluidToMaster.hasLumpedMass();
    nbinter = fluidToMaster.nbNodes();
  }

  info = MPI_Bcast(&nbinter,1, MPI_INT, 0, MpiInfo::petscComm());
  info = MPI_Bcast(&hasLumpedMass,1, MPI_INT, 0, MpiInfo::petscComm());
  model.hasLumpedMassRN() = hasLumpedMass;

  disp.resize(nbCoor*nbinter);
  vel.resize(nbCoor*nbinter);
  res.resize(nbCoor*nbinter);
  if (rank == 0 ) {
    dispo.resize(nbCoor*nbinter);
    velo.resize(nbCoor*nbinter);
    veloo.resize(nbCoor*nbinter);
    reso.resize(nbCoor*nbinter);
    for (int i=0; i < nbCoor*nbinter; ++i) {
      dispo[i] = 0.;
      velo[i] = 0.;
      veloo[i] = 0.;
      reso[i] = 0.;
      res[i]  = 0.;
      disp[i] = 0;
      vel[i] = 0;
    }
  }
  else {
    if ( not FelisceParam::instance().hasImmersedData )
      nodesSigma.resize(nbinter); // Boilevin-Kayl 11/2018: the following will need to be modified when the FD-FSI will be REALLY coupled with a fluid boudary solved with ALE
    disp_master.resize(nbCoor*nbinter);
    lumpM_master.resize(nbCoor*nbinter);
    residual.resize(nbCoor*nbinter);
  }

  if ( not FelisceParam::instance().hasImmersedData )
    info = MPI_Bcast(nodesSigma.data(),nbinter,MPI_INT, 0, MpiInfo::petscComm()); // Boilevin-Kayl 11/2018: the following will need to be modified when the FD-FSI will be REALLY coupled with a fluid boudary solved with ALE

  std::vector<PetscInt> ao_disp(nbCoor*nbinter), ao_velo(nbCoor*nbinter);

  if ( not FelisceParam::instance().hasImmersedData ) {
    // sequential global numbering on the interface
    int inode;
    for (int i=0; i<nbinter; ++i) {
      inode =  nodesSigma[i] - 1;
      if (FelisceParam::instance().FusionDof)
        inode =  model.linearProblem(0)->supportDofUnknown(0).listPerm()[inode];
      for (int j=0; j<nbCoor; ++j)
        ao_disp[nbCoor*i+j]= nbCoor*inode+j;
    }
    ao_velo = ao_disp;

    // from sequatial mesh global numberings  to petsc global numbering
    AOApplicationToPetsc(model.linearProblem(1)->ao(),nbCoor*nbinter,ao_disp.data());
    AOApplicationToPetsc(model.linearProblem(0)->ao(),nbCoor*nbinter,ao_velo.data());
    info = MPI_Bcast(lumpM_master.data(),  nbCoor*nbinter,   MPI_DOUBLE, 0, MpiInfo::petscComm());
    model.lumpedMassRN().set(0.);
    model.lumpedMassRN().setValues(nbCoor*nbinter,ao_velo.data(),lumpM_master.data(), INSERT_VALUES);
    model.lumpedMassRN().assembly();
  }


  PetscVector dispFluid, veloFluid,resFluid;
  PetscInt mdisp;//, mvelo;
  if ( not FelisceParam::instance().hasImmersedData ) {
    mdisp = model.linearProblem(1)->numDof();
    // mvelo = model.linearProblem(0)->numDof();
    dispFluid.createSeq(PETSC_COMM_SELF,mdisp);
    // veloFluid.createSeq(PETSC_COMM_SELF,mvelo);
    veloFluid.duplicateFrom(model.linearProblem(0)->sequentialSolution());
    resFluid.duplicateFrom(model.linearProblem(0)->vector());
    dispFluid.set(0.0);
    veloFluid.set(0.0);
    resFluid.set(0.0);
    resFluid.assembly();

    model.linearProblem(0)->pushBackExternalVec(veloFluid);
    model.linearProblem(0)->pushBackExternalVec(resFluid);
    model.linearProblem(1)->pushBackExternalVec(dispFluid);

  }
  else {
    model.immersed().setInterfaceExchangedData(&disp,&vel,&residual,&veloF);
  }

  int RNscheme = FelisceParam::instance().fsiRNscheme;
  bool hasSolidMidpointIntegration  = FelisceParam::instance().hasSolidMidpoint;

  //UserNSALE& lpb = *static_cast<UserNSALE*>(model.linearProblem(0));

  int flag = 1;
  int iter = 0;

  //Loop time
  do {

    if ( rank == 0 ) {

      fluidToMaster.rvFromMasterFSI(110);
      status =  fluidToMaster.status();

      if ( FelisceParam::instance().hasImmersedData && ( fluidToMaster.timeStep() != FelisceParam::instance().timeStep ) )
        FEL_ERROR("The fluid data file must set the same dt as in the master data file.");

      model.fstransient()->timeStep = fluidToMaster.timeStep();

      vel = velo_master;
      disp = disp_master;

      // Correction of the solid velocity when using midpoint solid solver
      if (hasSolidMidpointIntegration) {
      	for (int i=0; i < nbCoor*nbinter; ++i)
      	  velo_master[i] =  2* velo_master[i] -  velo[i] ;
      }

      if ( Tools::equal(FelisceParam::instance().theta, 0.5) ){
        // First 2nd order extrapolation of the solid displacement
        if (flag == 1){
          dispo = disp_master;
          flag = 0;
        }
       	for (int i=0; i < nbCoor*nbinter; ++i)
          disp[i] =  2*disp_master[i] - dispo[i] ;
      } else
      {
        if  ( FelisceParam::instance().useElasticExtension and ( not FelisceParam::instance().hasImmersedData ) ) {
          // fsi challenge (felisce version without -dispo)
          if (iter != 0 ) {
            for (int i=0; i < nbCoor*nbinter; ++i)
          disp[i] = disp_master[i] -dispo[i];
          }
          else {
            for (int i=0; i < nbCoor*nbinter; ++i)
          disp[i] = 0.;
          }
        }
        else
          disp  = disp_master;
      }
      // extrapolations of the velocity for RN schemes
      if (RNscheme == 1) {
        for (int i=0; i < nbCoor*nbinter; ++i)
          vel[i] = 2.*velo_master[i] - velo[i];
      }
      else if (RNscheme == 2) {
        for (int i=0; i < nbCoor*nbinter; ++i)
          vel[i] = 3.*velo_master[i] - 3.*velo[i] + veloo[i];
      }
      else
        vel = velo_master;

      if ( status == 1 ) {
      // actualizations: new time-step
        dispo = disp_master;
        veloo = velo;
        velo = velo_master;
        iter++;
      }
      if (  hasLumpedMass )
        for (int i=0; i < nbCoor*nbinter; ++i)
          res[i]-=lumpM_master[i]*vel[i]/model.fstransient()->timeStep;
    }
    info = MPI_Bcast(&status,1, MPI_INT, 0, MpiInfo::petscComm());
    info = MPI_Bcast(&model.fstransient()->timeStep, 1, MPI_DOUBLE, 0, MpiInfo::petscComm());
    info = MPI_Bcast(disp.data(), nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());
    info = MPI_Bcast(vel.data(), nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());
    info = MPI_Bcast(res.data(), nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());

    if ( not FelisceParam::instance().hasImmersedData ) {
      // inserting interface values into petsc vector with petsc numbering
      dispFluid.setValues(nbCoor*nbinter,ao_disp.data(),disp.data(), INSERT_VALUES);
      if (FelisceParam::instance().readPreStressedDisplFromFile)
        dispFluid.axpy(1,static_cast<LinearProblemHarmonicExtension*>(model.linearProblem(1))->HarmExtSol());
      veloFluid.setValues(nbCoor*nbinter,ao_velo.data(),vel.data(), INSERT_VALUES);
      resFluid.setValues(nbCoor*nbinter,ao_velo.data(),res.data(), INSERT_VALUES);

      dispFluid.assembly();
      veloFluid.assembly();
      resFluid.assembly();
    }

    //veloFluid.view(PETSC_VIEWER_STDOUT_WORLD);

    std::vector<int> label(1);
    label[0]=1;
    std::vector<double> flow(1);

    bool linearFlag = false;
    switch ( status  ) {

    case -1: // Normal exit
      //model.writeSolution();
      std::cout << "Normal fluid exit: " << info << "\n" << std::flush;
      goto EndLoop;
      break;
    case 1: // New time step
      // moving mesh and solving fluid
      model.prepareForward();
      model.solveNS();
      break;
    case 0: // Inner fluid solver evaluation
      linearFlag = false;
      model.solveLinearizedNS( linearFlag );

      break;
    case 2: // Inner fluid solver evaluation
      linearFlag = true;
      model.solveLinearizedNS( linearFlag );

      break;
    default:
      std::cout << " This status number is not allowed.\n" << std::endl << std::flush;
      exit(1);
    }

    if ( rank == 0 ) {

      if ( not FelisceParam::instance().hasImmersedData ) {
        model.seqResidual().getValues( nbCoor*nbinter, ao_velo.data(),residual.data());
        model.linearProblem(0)->sequentialSolution().getValues(nbCoor*nbinter, ao_velo.data(),veloF.data());

        // extrapolations of the fluid stress for RN schemes
        if ( (RNscheme == 1) || (RNscheme == -1) )
          res = residual;
        else if ( RNscheme == 2 ) {
          for (int i=0; i < nbCoor*nbinter; ++i)
            res[i] = 2.*residual[i] - reso[i];
          reso = residual;
        }
        else
          for (int i=0; i < nbCoor*nbinter; ++i)
            res[i] = 0.;
      }
      // sending fluid force to master
      fluidToMaster.sdToMasterFSI();

    }

  }
  while ( 1 );  // close infinite do

 EndLoop:
  if(rank==0){
    std::cout << "I reached the end of fluid part" << std::endl << std::flush;
  }
  return 0;
}

#endif
