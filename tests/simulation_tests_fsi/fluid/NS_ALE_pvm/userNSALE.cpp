//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include "userNSALE.hpp"
#include "FiniteElement/elementMatrix.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{

  UserNSALE::UserNSALE():
    LinearProblemNS()
  {
    m_linearizedFlag = false;
  }

  UserNSALE::~UserNSALE()
  {}

  void UserNSALE::userElementInit() {
  }


  // set source term with analytical function
  void   UserNSALE::userElementCompute(const std::vector<Point*>& elemPoint, const std::vector<int>& elemIdPoint,int& iel, FlagMatrixRHS flagMatrixRHS)
  {
    IGNORE_UNUSED_ARGUMENT(elemPoint)
    IGNORE_UNUSED_ARGUMENT(elemIdPoint)
    IGNORE_UNUSED_ARGUMENT(iel)
    IGNORE_UNUSED_ARGUMENT(flagMatrixRHS);
  }


  void UserNSALE::userFinalizeEssBCTransient()
  {
    if  ( (m_fstransient->iteration > 0) and (not FelisceParam::instance().hasImmersedData)  ) {
      if  (FelisceParam::instance().fsiRNscheme != -1000 ) { // robin schemes
      	this->vector().axpby(-1,1,this->externalVec(2));
      }
      else
      {
          //===============getListDofOfBC===============//
          BoundaryCondition& BC = *m_boundaryConditionList[0];
          int iUnknown = BC.getUnknown();
          int idVar = m_listUnknown.idVariable(iUnknown);

          felInt idEltInSupportLocal = 0;
          felInt idEltInSupportGlobal = 0;
          felInt idDof;
          double aux;
          BC.ValueBCInSupportDof().clear();
          for (unsigned iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++)
            {
              idEltInSupportLocal = BC.idEltAndIdSupport()[iSupportDofBC].first;
              ISLocalToGlobalMappingApply(m_mappingElem[currentMesh()],1,&idEltInSupportLocal,&idEltInSupportGlobal);
              for( auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++)
                {
              dof().loc2glob(idEltInSupportGlobal, BC.idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);
              AOApplicationToPetsc(m_ao,1,&idDof);
              this->externalVec(1).getValues(1, &idDof, &aux);
              BC.ValueBCInSupportDof().push_back(aux);
                }
            }
      }
      }

      // cavity with flexible bottom (shell)
      if ( FelisceParam::instance().testCase == 2  )  {
        if ( this->dimension() == 3) {
      if ( m_linearizedFlag )
        m_boundaryConditionList[4]->setValueT(this,m_velCavityZero,m_fstransient->time);
      else
        m_boundaryConditionList[4]->setValueT(this,m_velCavity,m_fstransient->time);
        }
        else {
      if ( m_linearizedFlag )
        m_boundaryConditionList[3]->setValueT(this,m_velCavityZero,m_fstransient->time);
      else
        m_boundaryConditionList[3]->setValueT(this,m_velCavity,m_fstransient->time);
      }
    }
  }


  void UserNSALE::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label)
  {
    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
    (void) label;

    //std::cout << FelisceParam::instance().testCase <<std::endl;

    CurvilinearFiniteElement& curvFeVel = *m_listCurvilinearFiniteElement[m_iVelocity];

    m_elemFieldInterfaceVel.initialize(DOF_FIELD,curvFeVel,this->dimension());
    m_elemFieldPreviousVel.initialize(DOF_FIELD,curvFeVel, this->dimension());

    // Stabilisation of backflow
    if (FelisceParam::instance().addedBoundaryFlag == 2){
      double coef = FelisceParam::instance().stabilizationCoef * FelisceParam::instance().density;
      for(std::size_t ilab=0; ilab < FelisceParam::instance().stabilizationLabel.size(); ilab++){
        if (label == FelisceParam::instance().stabilizationLabel[ilab]){
          m_elemFieldPreviousVel.setValue(sequentialSolution(), curvFeVel, iel, m_iVelocity, m_ao, dof());
          m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef, curvFeVel , m_elemFieldPreviousVel, 0, 0, dimension(), FelisceParam::instance().addedBoundaryFlag);
        }
      }
    }

    if ( (FelisceParam::instance().testCase == 1) and (this->dimension() == 3) ) {
      std::vector<int> ids {  10460, 10459, 10458, 10457, 10570, 10571, 10572, 10573, 10574, 10575, 10576, 10624, 10623, 10622, 10621, 10620, 10619, 10515, 10514, 10516, 10517, 10518, 10519, 10520, 10464, 10463, 10462, 10461};
      std::vector<int> idss {120, 64, 63, 62, 61, 60, 59, 58, 57, 170, 171, 172, 173, 174, 175, 176, 224, 223, 222, 221, 220, 219, 114, 115, 116, 117, 118, 119 };
      if (label == 3 ) {
        for (int i=0; i < 3; ++i) {
          auto it = find (ids.begin(),ids.end(), elemIdPoint[i]+1);
          if (it != ids.end()) {
            for (int ic=0; ic<3;++ic)
              m_elementMatBD[0]->mat()(i+3*ic,i+3*ic) = 1e30;
          }
        }
          }
          else if (label == 2 ) {
        for (int i=0; i < 3; ++i) {
          auto it = find (idss.begin(),idss.end(), elemIdPoint[i]+1);
          if (it != idss.end()) {
            for (int ic=0; ic<3;++ic)
              m_elementMatBD[0]->mat()(i+3*ic,i+3*ic) = 1e30;
          }
        }
      }
    }
    // pressure wave within an straight elastic tube
    if ( FelisceParam::instance().testCase == 1 ) {
      m_Pt.clear();
      if ( ((label == 4) && (this->dimension() == 2)) || (  (label == 2) && (this->dimension() == 3)   )) {
        if ( m_linearizedFlag )
          m_Pt.push_back(0.);
        else
          m_Pt.push_back(m_inPress(m_fstransient->time));

        m_elemFieldNeumannNormal[0].setValue(m_Pt);
      }

      if ( ( FelisceParam::instance().fsiRNscheme != -1000 ) and
	   ( ( ( label == 3 ) and (this->dimension() == 2 ) ) or
	     ( ( label == 1 ) and (this->dimension() == 3 ) ) ) )
        {
          m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());
          double rho     = 1.2;
          double epsilon = 0.1;
          double tau     = m_fstransient->timeStep;
          double a = rho*epsilon/tau ;

          m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[u*v]
          m_elementVectorBD[0]->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[\dotd^{k-1}*v]
        }
    }

    // cavity with flexible bottom (shell)
    if ( FelisceParam::instance().testCase == 2 ) {
      if ( label == 1 ) {
        m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());
        double rho     = 250;
        double epsilon = 0.002;
        double tau     = m_fstransient->timeStep;
        double a = FelisceParam::instance().gammaRN*rho*epsilon/tau;

        m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[u*v]
        m_elementVectorBD[0]->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[\dotd^{k-1}*v]
      }
    }
  }

  double inPressure::operator() (double t) const
  {
    const double Tstar = 0.005;
    const double Pmax  = 2.0E4;
    double val = 0.;
    if ( t < Tstar )
      val=  -0.5*Pmax*(1-cos(2.*t*M_PI/Tstar));
    return val;
  }


  double velCavity::operator() (double t) const
  {
    return  1-cos(0.4*M_PI*t);
  }

  double velCavityZero::operator() (double t) const
  {
    IGNORE_UNUSED_ARGUMENT(t);
    return  0.;
  }

}
