//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors: Julien Castelneau, Dominique Chapelle, Miguel Fernandez
//

// System includes

// External includes

// Project includes
#include "userHarmonicExtension.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"

namespace felisce
{

  UserHarmonicExtension::UserHarmonicExtension():
    LinearProblemHarmonicExtension()
  {}

  UserHarmonicExtension::~UserHarmonicExtension()
  {}

  void UserHarmonicExtension::userFinalizeEssBCTransient()
  {
      // Boilevin-Kayl 11/2018: the following will need to be modified when the FD-FSI will be REALLY coupled with a fluid boudary solved with ALE
      if ( (m_fstransient->iteration > 0) and (not FelisceParam::instance().hasImmersedData) )
      {

        //===============getListDofOfBC===============//
        BoundaryCondition& BC = *m_boundaryConditionList[0];
        int iUnknown = BC.getUnknown();
        int idVar = m_listUnknown.idVariable(iUnknown);


        felInt idEltInSupportLocal = 0;
        felInt idEltInSupportGlobal = 0;
        felInt idDof;
        double aux;
        BC.ValueBCInSupportDof().clear();
        for (std::size_t iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++)
        {
          idEltInSupportLocal = BC.idEltAndIdSupport()[iSupportDofBC].first;
          ISLocalToGlobalMappingApply(m_mappingElem[currentMesh()],1,&idEltInSupportLocal,&idEltInSupportGlobal);
          for( auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++)
          {
              dof().loc2glob(idEltInSupportGlobal, BC.idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);
              AOApplicationToPetsc(m_ao,1,&idDof);
              this->externalVec(0).getValues(1, &idDof, &aux);
              BC.ValueBCInSupportDof().push_back(aux);
            }
        }
      }
    }
}

