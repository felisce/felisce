# ------------------------------------------------------ #
# ------- Define variables ----------------------------- #
# ------------------------------------------------------ #

# Data file name
set(datafile_name "master")

# Reference solution folder name
set(reference_folder "RefSolutions")

# Tool to run test case
set(run_test_tool "run_test_fsi.py")

# Tool to compare solution and reference solution
set(compare_tool "compare_results.py")

# Tool to prepare fsi tests
set(prepare_tool "prepare_fsi.py")

# Tool to run fsi test with pvm
set(master_fsi_program_pvm ${MASTERFSI_PVM})

# Tool to run fsi test with zmq
set(master_fsi_program_zmq ${MASTERFSI_ZMQ})

# ------------------------------------------------------ #
# ------- Compile fluid executables -------------------- #
# ------------------------------------------------------ #
add_subdirectory(fluid)


# ------------------------------------------------------ #
# ------- Compile strucure executables ----------------- #
# ------------------------------------------------------ #
add_subdirectory(structure)


# ------------------------------------------------------ #
# ------- Message -------------------------------------- #
# ------------------------------------------------------ #
message(STATUS "Compiling simulation fsi tests:")


# ------------------------------------------------------ #
# ------- Include small simulation tests --------------- #
# ------------------------------------------------------ #
add_subdirectory(small_tests)


# ------------------------------------------------------ #
# ------- Include nightly simulation tests ------------- #
# ------------------------------------------------------ #
if(FELISCE_WITH_NIGHTLY_TESTS)
    add_subdirectory(nightly_tests)
endif()

# Unset variables
unset(datafile_name)
unset(reference_folder)
unset(run_test_tool)
unset(compare_tool)
unset(prepare_tool)
unset(master_fsi_program_pvm)
unset(master_fsi_program_zmq)
