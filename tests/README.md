# Tests

Here the tests are contained. Here we can contain two categories of tests:

- **Simulation tests**: Small simulations that check that the whole simulation process works properly
- **Unittest**: Minimal tests that check basic functions 
