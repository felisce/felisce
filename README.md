FELiScE
=================

[[_TOC_]]

# Introduction

<!--
<p align="left">
    ![https://gitlab.inria.fr/felisce/felisce/-/blob/master/LICENSE](https://img.shields.io/badge/License-LGPL%20v2.1-blue.svg?style=flat-square)
    ![Pipeline status](https://gitlab.inria.fr/felisce/felisce/badges/master/pipeline.svg?style=flat-square)
    ![C++](https://img.shields.io/badge/c%2B%2B-11/14/17-blue.svg?style=flat-square)
    ![Coverage report](https://gitlab.inria.fr/felisce/felisce/badges/master/coverage.svg?style=flat-square)
    ![Latex documentation](https://gitlab.inria.fr/felisce/extra/documentation/-/tree/master/Guides?ref_type%253Dheads)
<!--
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=felisce%3Afelisce&metric=coverage&template=FLAT)](https://sonarqube.inria.fr/sonarqube/component_measures?id=felisce%3Afelisce&metric=coverage)
[![Lines of code](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=felisce%3Afelisce&metric=ncloc&template=FLAT)](https://sonarqube.inria.fr/sonarqube/component_measures?id=felisce%3Afelisce&metric=ncloc)
[![Bugs](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=felisce%3Afelisce&metric=bugs&template=FLAT)](https://sonarqube.inria.fr/sonarqube/component_measures?id=felisce%3Afelisce&metric=bugs)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=felisce%3Afelisce&metric=vulnerabilities&template=FLAT)](https://sonarqube.inria.fr/sonarqube/component_measures?id=felisce%3Afelisce&metric=vulnerabilities)
[![Code Smells](https://sonarqube.inria.fr/sonarqube/api/project_badges/measure?project=felisce%3Afelisce&metric=code_smells&template=FLAT)](https://sonarqube.inria.fr/sonarqube/component_measures?id=felisce%3Afelisce&metric=code_smells)-->
</p>
-->

[SonarQube analysis](https://sonarqube.inria.fr/sonarqube/component_measures?id=felisce%3Afelisce&metric=coverage)

![](https://gitlab.inria.fr/felisce/extra/documentation/-/raw/master/FELiScE_repo/Logotype/logo_draft.png)

[*FELiScE*](https://team.inria.fr/commedia/software/felisce/) (**Finite Elements for Life Sciences and Engineering**) is a parallel finite element library written in *C++*.  It was initiated by the *Inria* **M3DISIM** and **REO** project-teams and is currently developed by the **COMMEDIA** project-team.

The library provides an unified software environment for the development of *state-of-the-art* finite element solvers for the spatial discretization of partial differential equations (**PDE**) in 1D, 2D and 3D spatial dimensions. Several finite element interpolations are available. The current version handles mainly globally continuous approximations. Interfacial discontinuities can be incorporated via the *Nitsche-XFEM* unfitted mesh framework. Local arrays and linear algebra operations are implemented with the [*boost-uBLAS*](https://www.boost.org/doc/libs/1_65_1/libs/numeric/ublas/doc/index.html) *C++* template class library. The global (parallel) data structure and the (linear and non-linear) solvers are based on the [*PETSc*](https://www.mcs.anl.gov/petsc/) package. The current stable solvers, motivated by the simulation of the cardiovascular and respiratory systems, are:

- *Fluid mechanics (incompressible flow)*: Stabilized finite element methods (**SUPG**/**PSPG**, **CIP**), *Nitsche-XFEM*, monolithic and fractional-step time-marching.
- *Solid mechanics*: Hyper-elastic solids, Reissner–Mindlin beam and shell models (**MITC** elements), contact mechanics.
- *Electrophysiology*: bi-domain and mono-domain models with different ionic dynamics.
- *Fluid-Structure Interaction* (**FSI**): partitioned solution procedures (strongly coupled, semi-implicit and loosely coupled schemes), interfacing between fluid and solid solvers based on the [**PVM**](https://www.netlib.org/pvm3/) and [*ZeroMQ*](https://zeromq.org/) libraries.
- *Data assimilation*: Luenberger observers and stabilized finite element methods for unique continuation problems.

<p align="center">
    <img src="https://gitlab.inria.fr/felisce/extra/documentation/-/raw/master/FELiScE_repo/README_files/Aortic_valve_(systole).gif" alt="drawing" width="275"/>
    <img src="https://gitlab.inria.fr/felisce/extra/documentation/-/raw/master/FELiScE_repo/README_files/Left_heart_hemodynamics_with_EPYGON_artificial_valve_(FSI).gif" alt="drawing" width="275"/>
	<img src="https://gitlab.inria.fr/felisce/extra/documentation/-/raw/master/FELiScE_repo/README_files/Left_heart_hemodynamics_with_reduced_valve_modelling_(velocity).gif" alt="drawing" width="275"/>
</p>


More videos available in [*Miguel A. Fernandez*](https://team.inria.fr/commedia/fernandez/) [youtube channel](https://www.youtube.com/channel/UCFVg09Xt_GPKHO0hry3Hfng).

# Installation

You can check the installation procedure in the [Wiki](https://gitlab.inria.fr/felisce/felisce/-/wikis/home).

# Documentation

## Doxygen
Doxygen documentation for the **FELiScE** master branch is available [here](https://felisce.gitlabpages.inria.fr/extra/doxygen).

# Users

The main users of FELiScE are the members (and former members) of COMMEDIA and members of the M3DISIM and SIMBIOTX project-teams.

## Current developers

- *M. Agbalessi* (PhD COMMEDIA)
- *D. Corti* (PhD COMMEDIA)
- *M.A. Fernández* (DR COMMEDIA, scientific coordinator)
- *F. Lespagnol* (PhD COMMEDIA and Politecnico di Milano)
- *V. Mataix-Ferrandiz* (research engineer SED, code  coordinator)
- *M. Nechita* (researcher TPI Roumanie)
- *O. Ruz* (PhD COMMEDIA and M3DISIM)
- *M. Vidrascu* (emeritus researcher COMMEDIA)

 
## Current users

- *M. Champion* (PhD COMMEDIA)
- *S. Costa* (PhD COMMEDIA)
- *J. Diaz* (research engineer M3DISIM)
- *F. Gerosa* (post-doc Stanford University)
- *F. Vergnet* (assistant professor COMMEDIA)
- *N. Golse* (PhD SIMBIOTX)
- *D. Lombardi* (researcher COMMEDIA)
- *W. Liu* (intern SIMBIOTX)
- *L. Papamanolis* (intern SIMBIOTX)
- *L. Sala* (post-doc SIMBIOTX)
- *I. Vignon-Clementel* (DR SIMBIOTX)

# License

*FELiScE* is licensed under [LGPL 2.1](https://www.gnu.org/licenses/old-licenses/lgpl-2.1.html). See the [license](LICENSE).
