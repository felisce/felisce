---
name: Feature
about: New features or wider changes, 

---

**Description**
A brief description of the PR.

**Changelog**
Please summarize the changes in one a list to generate the changelog:
E.g.
- Added feature X to Y (#XXXX)
- Added Foo Application

**How to test**

List of steps to follow in order to reproduce this changes.
E.g.
- Compile XXXX
- Run script YYYY

**Misc**

Commentaries not related with the previous sections.
